<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}mgroup_test" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>

    <div class="block-content">
        <?php echo form_open('mgroup_test/save','class="form-horizontal" id="form-work"') ?>
        <div class="form-group">
            <label class="col-md-12" for="">Nama Group Test</label>
            <div class="col-md-12">
                <input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-12" for="">User Akses Group Test</label>
            <div class="col-md-12">
                <select class="js-select2 form-control" name="iduser[]" multiple id="iduser" value="" data-placeholder="Pilih Opsi">
                    <?php foreach ($list_user as $row) { ?>
                    <option value="<?=$row->id?>" <?=(in_array($row->id, $list_user_selected) ? 'selected' : '') ?>><?=$row->nama?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <hr>

        <b><span class="label label-success" style="font-size:12px">DAFTAR PEMERIKSAAN</span></b><br /><br />
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="detail_list">
                <thead>
                    <tr>
                        <th style="width:5%" class="text-center">No</th>
                        <th style="width:50%" class="text-center">Nama Pemeriksaan</th>
                        <th style="width:25%" class="text-center">Aksi</th>
                    </tr>
                    <tr>
                        <th class="text-center" style="vertical-align: middle;">X</th>
                        <th>
                            <div class="row">
                                <div class="col-md-8">
                                    <select class="js-select2 form-control" id="idtariflab" value="">
                                        <option value="#">Pilih Opsi</option>
                                        <?php foreach  ($list_tariflab as $row) { ?>
                                        <option value="<?=$row->id;?>"><?=TreeView($row->level, $row->nama)?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <a data-toggle="modal" data-target="#modal-pemeriksaan" id="btn-modal-pemeriksaan" class="btn btn-success"><i class="fa fa-search"></i> Cari Pemeriksaan</a>
                                </div>
                            </div>
                        </th>
                        <th>
                            <a id="detail_add" class="btn btn-success">Tambahkan</a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($this->uri->segment(2) == 'update') { ?>
                        <?php foreach  ($list_pemeriksaan as $index => $row) { ?>
                        <tr>
                            <td class="text-center"><?=$index + 1;?></td>
                            <td style="display: none;"><?=$row->id;?></td>
                            <td><?=$row->nama;?></td>
                            <td>
                                <button type="button" class="btn btn-sm btn-primary detail_edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button>&nbsp;&nbsp;
                                <button type="button" class='btn btn-sm btn-danger detail_remove' data-toggle='tooltip' title='Remove'><i class='fa fa-trash-o'></i></button>
                            </td>
                        </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-md-12" style="text-align: right;">
                <button class="btn btn-success" type="submit">Simpan</button>
                <a href="{base_url}mgroup_test" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
            </div>
        </div>
    </div>

    <input type="hidden" class="form-control" id="rowindex" />
    <input type="hidden" class="form-control" id="number" />
    <input type="hidden" name="detail_value" id="detail_value" />

    <br>

    <?php echo form_hidden('id', $id); ?>
    <?php echo form_close() ?>
</div>

<div class="modal" id="modal-pemeriksaan" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal" id="form_satuan">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">Daftar Pemeriksaan</h3>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" style="margin-bottom: 5px;">
                                    <label class="col-md-3 control-label" for="">Tipe</label>
                                    <div class="col-md-7">
                                        <select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Semua Tipe">
                                            <option value="0">Semua Tipe</option>
                                            <option value="1">Umum</option>
                                            <option value="2">Pathologi Anatomi</option>
                                            <option value="3">PMI</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" style="margin-bottom: 5px;">
                                    <label class="col-md-3 control-label" for="">Head Parent</label>
                                    <div class="col-md-7">
                                        <select name="idparent" id="idparent" class="js-select2 form-control" style="width: 100%;" data-placeholder="Semua Head Parent">
                                            <option value="0">Semua Head Parent</option>
                                            <?php foreach ($list_parent as $row) { ?>
                                            <option value="<?=$row->path;?>"><?=TreeView($row->level, $row->nama)?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" style="margin-bottom: 5px;">
                                    <label class="col-md-3 control-label" for="">Sub Parent</label>
                                    <div class="col-md-7">
                                        <select name="idsubparent[]" id="idsubparent" multiple class="js-select2 form-control" style="width: 100%;" data-placeholder="Semua Sub Parent">
                                            <?php foreach ($list_subparent as $row) { ?>
                                            <option value="<?=$row->path;?>"><?=TreeView($row->level, $row->nama)?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" style="margin-bottom: 5px;">
                                    <label class="col-md-3 control-label" for="">Status</label>
                                    <div class="col-md-7">
                                        <select name="status_paket" id="status_paket" class="js-select2 form-control" style="width: 100%;" data-placeholder="Status">
                                            <option value="9">Semua Status</option>
                                            <option value="1">Paket</option>
                                            <option value="0">Rincian</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <div class="form-group" style="margin-bottom: 5px;">
                                    <label class="col-md-3 control-label" for=""></label>
                                    <div class="col-md-7">
                                        <button class="btn btn-success text-uppercase btn-filter-pemeriksaan" type="button" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-responsive" id="table-pemeriksaan">
                                <thead>
                                    <tr>
                                        <th class="text-center">X</th>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Nama Pemeriksaan</th>
                                        <th class="text-center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success" id="addMultiplePemeriksaan" type="button" data-dismiss="modal">Tambahkan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{js_path}plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<script src="{js_path}plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<link href="{js_path}plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css" rel="stylesheet" type="text/css">
<link href="{js_path}plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
$(document).ready(function() {
    $("#detail_add").click(function() {
        var valid = validateDetail();
        if (!valid) return false;

        var row_index;
        var duplicate = false;

        if ($("#rowindex").val() != '') {
            var content = "";
            var number = $("#number").val();
            row_index = $("#rowindex").val();
        } else {
            var content = "<tr>";
			var number = $('#detail_list tr').length;
            $('#detail_list tbody tr').filter(function() {
                var $cells = $(this).children('td');
                if ($cells.eq(1).text() == $("#idtariflab option:selected").val()) {
                    sweetAlert("Maaf...", "Tarif Laboratorium " + $("#idtariflab option:selected").text() + " sudah ditambahkan.", "error");
                    duplicate = true;
                    return false;
                }
            });
        }

        if (duplicate == false) {
            content += "<td class='text-center'>" + number + "</td>";
            content += "<td style='display:none;'>" + $("#idtariflab option:selected").val() + "</td>";
            content += "<td>" + $("#idtariflab option:selected").text() + "</td>";
            content += "<td><button type='button' class='btn btn-sm btn-primary detail_edit' data-toggle='tooltip' title='Edit'><i class='fa fa-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger detail_remove' data-toggle='tooltip' title='Remove'><i class='fa fa-trash-o'></i></button></td>";

            if ($("#rowindex").val() != '') {
                $('#detail_list tbody tr:eq(' + row_index + ')').html(content);
            } else {
                content += "</tr>";
                $('#detail_list tbody').append(content);
            }

            detailClear();
        }
    });

    $(document).on("click", ".detail_edit", function() {
        $("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
        $("#number").val($(this).closest('tr').find("td:eq(0)").html());
        $("#idtariflab").select2('val', $(this).closest('tr').find("td:eq(1)").html());
        
        return false;
    });

    $(document).on("click", ".detail_remove", function() {
        if (confirm("Hapus data ?") == true) {
            $(this).closest('td').parent().remove();
        }
        return false;
    });

    $(document).on("click", "#addMultiplePemeriksaan", function() {
        addMultiplePemeriksaan();
    });

    $("#form-work").submit(function(e) {
        var form = this;

        if ($("#nama").val() == "") {
            e.preventDefault();
            sweetAlert("Maaf...", "Nama Group Test tidak boleh kosong!", "error");
            return false;
        }

        if ($("#iduser").val() == null) {
            e.preventDefault();
            sweetAlert("Maaf...", "User Akses tidak boleh kosong!", "error");
            return false;
        }

        var detail_tbl = $('table#detail_list tbody tr').get().map(function(row) {
            return $(row).find('td').get().map(function(cell) {
                return $(cell).html();
            });
        });

        $("#detail_value").val(JSON.stringify(detail_tbl));

        swal({
            title: "Berhasil!",
            text: "Proses penyimpanan data.",
            type: "success",
            timer: 1500,
            showConfirmButton: false
        });
    });
});

function detailClear() {
    $("#rowindex").val('');
    $("#number").val('');
    $("#idtariflab").select2('val', '#');
}

function validateDetail() {
    if ($("#idtariflab").val() == "#") {
        sweetAlert("Maaf...", "Tarif Laboratorium tidak boleh kosong!", "error");
        return false;
    }

    return true;
}
</script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript"> 
$(document).ready(function() {
    getDaftarPemeriksaan();

    $(document).on("click", "#btn-modal-pemeriksaan", function() {
        getDaftarPemeriksaan();
    });

    $(document).on("click", ".btn-filter-pemeriksaan", function() {
        getDaftarPemeriksaan();
    });
});

function getDaftarPemeriksaan() {    
    var idtipe = $('#idtipe option:selected').val();
    var idparent = $('#idparent option:selected').val();
    var idsubparent = $('#idsubparent option:selected').val() ?? 0;
    var status_paket = $('#status_paket option:selected').val();
    var tarif_selected = $('table#detail_list tbody tr').get().map(function(row) {
        return $(row).find('td').eq(1).get().map(function(cell) {
            return $(cell).html();
        });
    }).flat();

    BaseTableDatatables.init();
    $('#table-pemeriksaan').DataTable().destroy();
    $('#table-pemeriksaan').DataTable({
        "bSort": false,
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}mgroup_test/getDaftarPemeriksaan',
            type: "POST",
            data: {
                idtipe: idtipe,
                idparent: idparent,
                idsubparent: idsubparent,
                status_paket: status_paket,
                tarif_selected: tarif_selected,
            },
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "1%",
                "targets": 0,
                "orderable": true,
                "className": "text-center",
            },
            {
                "width": "1%",
                "targets": 1,
                "orderable": true,
                "className": "text-center",
            },
            {
                "width": "20%",
                "targets": 2,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 3,
                "orderable": true,
                "className": "text-center",
            },
        ]
    });
}

function addMultiplePemeriksaan() {
    $(".checkbox_pemeriksaan:checked").map(function(){
        const data = $(this).data();

        var row_index;
        var duplicate = false;

        if ($("#rowindex").val() != '') {
            var content = "";
            var number = $("#number").val();
            row_index = $("#rowindex").val();
        } else {
            var content = "<tr>";
            var number = $('#detail_list tr').length;
            $('#detail_list tbody tr').filter(function() {
                var $cells = $(this).children('td');
                if ($cells.eq(1).text() == data.idtarif) {
                    sweetAlert("Maaf...", "Tarif Laboratorium " + data.namatarif + " sudah ditambahkan.", "error");
                    duplicate = true;
                    return false;
                }
            });
        }

        if (duplicate == false) {
            content += "<td class='text-center'>" + number + "</td>";
            content += "<td style='display:none;'>" + data.idtarif + "</td>";
            content += "<td>" + data.namatarif + "</td>";
            content += "<td><button type='button' class='btn btn-sm btn-primary detail_edit' data-toggle='tooltip' title='Edit'><i class='fa fa-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger detail_remove' data-toggle='tooltip' title='Remove'><i class='fa fa-trash-o'></i></button></td>";

            if ($("#rowindex").val() != '') {
                $('#detail_list tbody tr:eq(' + row_index + ')').html(content);
            } else {
                content += "</tr>";
                $('#detail_list tbody').append(content);
            }

            detailClear();
        }
    }).get();
    
    $('.checkbox_pemeriksaan:checked').removeAttr('checked');
}
</script>