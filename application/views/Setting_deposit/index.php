<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<style>
.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
</style>
<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('2604'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" > GENERAL</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2605'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_2" onclick="load_jenis_deposit()"> JENIS DEPOSIT</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2606'))){ ?>
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab_3" onclick="load_detail()"> LOGIC PEMBATALAN</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2607'))){ ?>
		<li class="<?=($tab=='4'?'active':'')?>">
			<a href="#tab_4" onclick="set_tab(4)"> LABEL</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2620'))){ ?>
		<li class="<?=($tab=='5'?'active':'')?>">
			<a href="#tab_5"> PERSETUJUAN TINDAKAN</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('2604'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_info">
			<?php echo form_open('#','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label" for="st_edit_deposit">Izin Edit Tanggal</label>
							<div class="col-md-2">
								<select id="st_edit_deposit" name="st_edit_deposit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_edit_deposit=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_edit_deposit=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							
							<label class="col-md-2 control-label div_catatan" for="st_draft_deposit">Draft Proses</label>
							<div class="col-md-2 div_catatan">
								<select id="st_draft_deposit" name="st_draft_deposit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_draft_deposit=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_draft_deposit=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							
							<label class="col-md-2 control-label div_catatan" for="st_proses_selesai_deposit">Proses Selesai</label>
							<div class="col-md-2 div_catatan">
								<select id="st_proses_selesai_deposit" name="st_proses_selesai_deposit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_proses_selesai_deposit=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_proses_selesai_deposit=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label" for="persen_deposit">Persentase Acuan deposit Tipe Pembedahan</label>
							<div class="col-md-2 div_catatan">
								<div class="input-group">
									<input class="form-control number" type="text" id="persen_deposit" name="persen_deposit" value="{persen_deposit}" placeholder="%">
									<span class="input-group-addon">%</span>
								</div>
							</div>
							
							<label class="col-md-2 control-label div_catatan" for="kali_deposit">Persentase Acuan deposit Tipe Konservatif</label>
							<div class="col-md-2 div_catatan">
								<div class="input-group">
									<input class="form-control number" type="text" id="kali_deposit" name="kali_deposit" value="{kali_deposit}" placeholder="">
									<span class="input-group-addon">Kali</span>
								</div>
							</div>
							<label class="col-md-2 control-label div_catatan" for="hari_batal_deposit">Waktu Pembatalan</label>
							<div class="col-md-2 div_catatan">
								<div class="input-group">
									<input class="form-control number" type="text" id="hari_batal_deposit" name="hari_batal_deposit" value="{hari_batal_deposit}" placeholder="">
									<span class="input-group-addon">Hari</span>
								</div>
							</div>
							
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label div_catatan" for="qr_deposit">QR CODE</label>
							<div class="col-md-2 div_catatan">
								<select id="qr_deposit" name="qr_deposit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($qr_deposit=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($qr_deposit=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							
							<label class="col-md-2 control-label div_catatan" for="kali_deposit">Email</label>
							<div class="col-md-6 div_catatan">
									<input class="js-tags-input form-control" type="text" id="email_deposit" name="email_deposit" value="<?=$email_deposit?>">
							</div>
							
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label div_catatan" for="qr_deposit">Setting Deskripsi Kwitansi</label>
							<div class="col-md-10 div_catatan">
								<textarea class="form-control" id="desk_deposit"><?=$desk_deposit?></textarea>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label div_catatan" for="qr_deposit">&nbsp;</label>
							<div class="col-md-2 ">
								<button class="btn btn-primary"  type="button" onclick="simpan_general()" ><i class="fa fa-save"></i> Simpan</button>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2605'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?> " id="tab_2">
			<?php echo form_open('#','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_tipe">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="40%">Tipe Jenis Deposit</th>
											<th width="40%">Jenis Deposit</th>
											<th width="15%">Action</th>										   
										</tr>
										<?$tipe_id='0';?>
										<tr>
											<th>#<input type="hidden" value="" id="jenis_deposit_id"></th>
											<th>
												<select id="tipe_id" name="tipe_id" class="js-select2 form-control"  style="width:100%" data-placeholder="Choose one..">
													<option value="0" <?=($tipe_id=='0'?'selected':'')?>>-PILIH-</option>
													<option value="1" <?=($tipe_id=='1'?'selected':'')?>>DEPOSIT AWAL</option>
													<option value="2" <?=($tipe_id=='2'?'selected':'')?>>NON DEPOSIT AWAL</option>
												</select>
											</th>
											<th>
												<input class="form-control" type="text" id="nama" style="width:100%" name="nama" value="" placeholder="">
											</th>
											
											<th>
												<button class="btn btn-primary btn-sm"  type="button" id="btn_tambah_tipe" onclick="add_jenis()"><i class="fa fa-plus"></i> Simpan</button>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2606'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?> " id="tab_3">
			<?php echo form_open('#','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table id="tabel_detail" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 5%;">Step</th>								
																		
											<th style="width: 10%;">Jenis Deposit</th>								
											<th style="width: 10%;">Deskripsi</th>								
											<th style="width: 15%;">User </th>
											<th style="width: 10%;">Jika Setuju</th>								
											<th style="width: 10%;">Jika Tolak</th>	
											<th style="width: 10%;">Actions</th>
										</tr>
										<tr>								
											
											<td>
												<select name="step" style="width: 100%" id="step" class="js-select2 form-control input-sm">
													<?for($i=1;$i<10;$i++){?>
														<option value="<?=$i?>"><?=$i?></option>
													<?}?>
												</select>										
											</td>
											
											<td>
												<select name="tipe_deposit" style="width: 100%" id="tipe_deposit" class="js-select2 form-control input-sm">										
													<option value="#">- Pilih Jenis -</option>
													<?foreach(get_all('mdeposit_jenis') as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													
													<?}?>
												</select>										
											</td>
											
											<td>
												
												<input type="text" style="width: 100%"  class="form-control" id="deskrpisi" placeholder="" name="deskrpisi" value="">
												<input type="hidden" class="form-control" id="id_edit" placeholder="0" name="id_edit" value="">
											</td>	
											<td>
												<select name="iduser" tabindex="16"  style="width: 100%" id="iduser" data-placeholder="User" class="js-select2 form-control input-sm"></select>										
											</td>
											<td>
												<select name="proses_setuju" style="width: 100%" id="proses_setuju" class="js-select2 form-control input-sm">										
													<option value="1">Langsung</option>
													<option value="2">Menunggu User</option>										
												</select>										
											</td>
											<td>
												<select name="proses_tolak" style="width: 100%" id="proses_tolak" class="js-select2 form-control input-sm">										
													<option value="1">Langsung</option>
													<option value="2">Menunggu User</option>										
												</select>										
											</td>
											<td>									
												<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_detail" title="Masukan Item"><i class="fa fa-save"></i></button>
												<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_edit" title="Batalkan"><i class="fa fa-refresh"></i></button>
											</td>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
									
							</table>
							</div>					
						</div>
					</div>
				</div>
			<?php echo form_close() ?>
			</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2607'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='4'?'active in':'')?> " id="tab_4">
			<?php echo form_open_multipart('setting_deposit/save_label_lembar','class="js-form1 validation form-horizontal" id="form-work"') ?>
				<div class="col-md-12">
			<div class="row">
				
					<div class="form-group" style="margin-bottom:5px">
						
						<div class="col-md-12">
							<div class="col-md-6">
								<img class="img-avatar"  id="output_img" src="{upload_path}logo_setting/{logo_form}" />
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Logo Header (100x100)</label>
								<div class="box">
									<input type="file" id="file-3" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;" name="logo_form" value="{logo_form}" />
									<label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
								</div>
							</div>
						</div>
					</div>
				
					<div class="form-group" style="margin-bottom:5px">
						
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Alamat</label>
								<input type="text" class="form-control" name="alamat_form" value="{alamat_form}">
							</div>
							<div class="col-md-6">
								<label>Telepon</label>
								<input type="text" class="form-control" name="telepone_form" value="{telepone_form}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						
						<div class="col-md-12">
							<div class="col-md-12">
								<label>Web / Email</label>
								<input type="text" class="form-control" name="email_form" value="{email_form}">
							</div>
							
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Judul</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="judul_ina" value="{judul_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="judul_eng" value="{judul_eng}">
							</div>
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">No Registrasi</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="no_register_ina" value="{no_register_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="no_register_eng" value="{no_register_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">No Kwitansi</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="no_kwitansi_ina" value="{no_kwitansi_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="no_kwitansi_eng" value="{no_kwitansi_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tanggal Deposit</label>&nbsp;&nbsp;&nbsp;&nbsp;
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="tanggal_ina" value="{tanggal_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="tanggal_eng" value="{tanggal_eng}">
							</div>
							
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Sudah Terima Dari</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="sudah_terima_ina" value="{sudah_terima_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="sudah_terima_eng" value="{sudah_terima_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Banyaknya Uang</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="banyaknya_ina" value="{banyaknya_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="banyaknya_eng" value="{banyaknya_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Untuk Pembayaran</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="untuk_ina" value="{untuk_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="untuk_eng" value="{untuk_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Jumlah</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="jumlah_ina" value="{jumlah_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="jumlah_eng" value="{jumlah_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Yang Menerima</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="yg_menerima_ina" value="{yg_menerima_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="yg_menerima_eng" value="{yg_menerima_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Footer</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="footer_ina" value="{footer_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="footer_eng" value="{footer_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Generated</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="generated_ina" value="{generated_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="generated_eng" value="{generated_eng}">
							</div>
							
						</div>
					</div>
					
					<div class="form-group" style="margin-top:25px">
						<div class="col-md-12">
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="submit" id="btn_save_label" name="btn_save_label"><i class="fa fa-save"></i> Simpan</button>
								</div>
						</div>
					</div>
					
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2620'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='5'?'active in':'')?> " id="tab_5">
			<?php echo form_open('#','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label" for="st_setuju_tanpa_deposit">Persetujuan Tanpa Desposit</label>
							<div class="col-md-2">
								<select id="st_setuju_tanpa_deposit" name="st_setuju_tanpa_deposit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_setuju_tanpa_deposit=='1'?'selected':'')?>>IZINKAN</option>
									<option value="0" <?=($st_setuju_tanpa_deposit=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							
							<label class="col-md-2 control-label div_catatan" for="st_edit_tanggal_persetujuan">Edit Tanggal Persetujuan</label>
							<div class="col-md-2 div_catatan">
								<select id="st_edit_tanggal_persetujuan" name="st_edit_tanggal_persetujuan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_edit_tanggal_persetujuan=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_edit_tanggal_persetujuan=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							
							<label class="col-md-2 control-label div_catatan" for="st_qrcode">QRCODE</label>
							<div class="col-md-2 div_catatan">
								<select id="st_qrcode" name="st_qrcode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_qrcode=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_qrcode=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label div_catatan" for="email_tindakan">Email Generate</label>
							<div class="col-md-4 div_catatan">
									<input class="js-tags-input form-control" type="text" id="email_tindakan" name="email_tindakan" value="<?=$email_tindakan?>">
							</div>
							<label class="col-md-2 control-label div_catatan" for="qr_deposit">Label Acc Estimasi</label>
							<div class="col-md-4 div_catatan">
								<input class="form-control" type="text" id="label_estimasi" name="label_estimasi" value="{label_estimasi}" placeholder="">
							</div>
						</div>
						
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label div_catatan" for="qr_deposit">Judul Email</label>
							<div class="col-md-10 div_catatan">
								<textarea class="form-control" id="judul_email"><?=$judul_email?></textarea>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label div_catatan" for="qr_deposit">&nbsp;</label>
							<div class="col-md-2 ">
								<button class="btn btn-primary"  type="button" onclick="simpan_tab_5()" ><i class="fa fa-save"></i> Simpan</button>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
function loadFile_img(event){
	// alert('sini');
	var output_img = document.getElementById('output_img');
	output_img.src = URL.createObjectURL(event.target.files[0]);
}
$(document).ready(function(){	
	$(".number").number(true,0,'.',',');
	var tab=$("#tab").val();
	if (tab=='1'){
		// load_kunjungan();
	}
	if (tab=='2'){
		load_jenis_deposit();
	}
	if (tab=='3'){
		load_detail();
	}
	
	if (tab=='4'){
		
	}
	clear_tipe();
})	
$(document).on("change","#step,#tipe_deposit",function(){
	list_user();
});
function list_user(){
		// alert($("#idjenis").val());
		$('#iduser')
			.find('option')
			.remove()
			.end()
			.append('<option value="">- Pilih User -</option>')
			.val('').trigger("liszt:updated");
		if ($("#tipe_deposit").val()!='#' && $("#step").val()!='#'){
			// console.log($("#id").val()+'/'+$("#step").val()+'/'+$("#tipe_deposit").val()+'/'+$("#idjenis").val());
			$.ajax({
				url: '{site_url}setting_deposit/list_user/'+$("#step").val()+'/'+$("#tipe_deposit").val(),
				dataType: "json",
				success: function(data) {

				$.each(data.detail, function (i,unit) {
				$('#iduser')
				.append('<option value="' + unit.id + '">' + unit.name + '</option>');
				});
				}
			});
		}else{
			$('#iduser')
			.find('option')
			.remove()
			.end()
			.append('<option value="">- Pilih User -</option>')
		}

	}
	
	function load_detail(){
		var idlogic=$("#id").val();
		
		$('#tabel_detail').DataTable().destroy();
		var table = $('#tabel_detail').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}setting_deposit/load_detail/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [7], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	$(document).on("click","#simpan_user",function(){
		
		if (validate_add_user()==false)return false;
		
		simpan_user();
		
	});
	$(document).on("click","#clear_edit",function(){
		clear_all();
		$("#id_edit").val('');
		clear_input_detail();
		$("#iduser").val(null).trigger('change');
	});
	function clear_all(){
		// list_unit();
		// list_user_bendahara();
		$("#id_edit").val('');
		$("#simpan_unit").attr('disabled', false);
	}
	function clear_input_detail(){
		$("#step").val('1').trigger('change');
		$("#tipe_deposit").val('#').trigger('change');
		// $("#idjenis").val('#').trigger('change');
		$("#deskrpisi").val('');
		list_user();
	}
	$(document).on("click","#simpan_detail",function(){
		if (validate_add_detail()==false)return false;
		var id_edit=$("#id_edit").val();
		var iduser=$("#iduser").val();
		var step=$("#step").val();
		var proses_setuju=$("#proses_setuju").val();
		var proses_tolak=$("#proses_tolak").val();
		var tipe_deposit=$("#tipe_deposit").val();
		var deskrpisi=$("#deskrpisi").val();
		$.ajax({
			url: '{site_url}setting_deposit/simpan_detail',
			type: 'POST',
			data: {
				iduser:iduser,step:step,
				proses_setuju: proses_setuju,proses_tolak:proses_tolak,tipe_deposit:tipe_deposit,
				deskrpisi:deskrpisi,id_edit:id_edit
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Logic Berhasil disimpan'});
					$('#tabel_detail').DataTable().ajax.reload( null, false );
					clear_input_detail();
					$("#id_edit").val('');
				}else{
					sweetAlert("Gagal...", "Penyimpanan!", "error");
					
				}
			}
		});
		
		
	});
	
	$(document).on("click",".hapus_det",function(){
		var table = $('#tabel_detail').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,7).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menghapus Logic Detail ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}setting_deposit/hapus_det',
				type: 'POST',
				data: {
					id: id	
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Detail Berhasil dihapus'});
					$('#tabel_detail').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	
	$(document).on("click",".edit",function(){
		// alert('SINI');
		table = $('#tabel_detail').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,7).data()
		$.ajax({
			url: '{site_url}setting_deposit/get_edit',
			type: 'POST',
			dataType: "json",
			data: {
				id: id
			},
			success: function(data) {
				// var item=data.detail;
				$("#id_edit").val(data.id);
				$("#step").val(data.step).trigger('change');
				$("#proses_setuju").val(data.proses_setuju).trigger('change');
				$("#proses_tolak").val(data.proses_tolak).trigger('change');
				$("#tipe_deposit").val(data.tipe_deposit).trigger('change');
				$("#deskrpisi").val(data.deskrpisi).trigger('change');
				$("#nominal").val(data.nominal).trigger('change');
				var newOption = new Option(data.user_nama, data.iduser, true, true);
					
				$("#iduser").append(newOption);
				$("#iduser").val(data.iduser).trigger('change');

				console.log(data.id);
			}
		});
	});
	
	function simpan_edit(){
		$("#simpan_unit").attr('disabled', true);
		var id=$("#id").val();		
		var tanggal_hari=$("#tanggal_hari").val();
		var deskripsi=$("#deskripsi").val();
		var tedit=$("#tedit").val();

			$.ajax({
				url: '{site_url}setting_deposit/simpan_edit',
				type: 'POST',
				data: {
					id: id,tanggal_hari: tanggal_hari,deskripsi: deskripsi,tedit: tedit,
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Tanggal Berhasil Disimpan'});
					$('#tabel_unit').DataTable().ajax.reload( null, false );
					clear_all();
					// $('#deskripsi').val('').trigger('change');
					// $('#tanggal_hari').val('#').trigger('change');
					// $("#simpan_unit").attr('disabled', false);	
					// window.location.href = "{site_url}tko/index/"+idunit;
				}
			});
		
	}
	
	function validate_add_detail()
	{
		if ($("#iduser").val()=='' || $("#iduser").val()==null || $("#iduser").val()=="#"){
			sweetAlert("Maaf...", "User Harus diisi", "error");
			return false;
		}
		if ($("#tipe_deposit").val()=='' || $("#tipe_deposit").val()==null || $("#tipe_deposit").val()=="#"){
			sweetAlert("Maaf...", "Tipe Refund Harus diisi", "error");
			return false;
		}
		
		if ($("#nominal").val()=='0'){
			if (($("#deskrpisi").val()!='>=' && $("#deskrpisi").val()!='>')){
			sweetAlert("Maaf...", "Nominal Harus diisi", "error");
			return false;
			}
		}
		
		return true;
	}
	
function simpan_general(){
	// alert('sini');
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_deposit/simpan_general/',
		dataType: "json",
		type: "POST",
		data: {
			st_edit_deposit : $("#st_edit_deposit").val(),
			st_draft_deposit : $("#st_draft_deposit").val(),
			st_proses_selesai_deposit : $("#st_proses_selesai_deposit").val(),
			persen_deposit : $("#persen_deposit").val(),
			kali_deposit : $("#kali_deposit").val(),
			email_deposit : $("#email_deposit").val(),
			hari_batal_deposit : $("#hari_batal_deposit").val(),
			qr_deposit : $("#qr_deposit").val(),
			desk_deposit : $("#desk_deposit").val(),

		},
		success: function(data) {
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan General'});
		}
	});
}
function simpan_tab_5(){
	// alert('sini');
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_deposit/simpan_tab_5/',
		dataType: "json",
		type: "POST",
		data: {
			st_setuju_tanpa_deposit : $("#st_setuju_tanpa_deposit").val(),
			st_edit_tanggal_persetujuan : $("#st_edit_tanggal_persetujuan").val(),
			st_qrcode : $("#st_qrcode").val(),
			email_tindakan : $("#email_tindakan").val(),
			label_estimasi : $("#label_estimasi").val(),
			judul_email : $("#judul_email").val(),
		},
		success: function(data) {
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Tindakan'});
		}
	});
}

function add_jenis(){
	let tipe_id=$("#tipe_id").val();
	let nama=$("#nama").val();
	if (tipe_id=='0'){
		sweetAlert("Maaf...", "Tentukan Tipe", "error");
		return false;
	}
	if (nama==''){
		sweetAlert("Maaf...", "Tentukan Jenis", "error");
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_deposit/simpan_master_deposit', 
		dataType: "JSON",
		method: "POST",
		data : {
				jenis_deposit_id : $("#jenis_deposit_id").val(),
				tipe_id : $("#tipe_id").val(),
				nama : $("#nama").val(),
				
			},
		success: function(data) {
			// alert(data);
				$("#cover-spin").hide();
			if (data==true){
				clear_tipe();
				load_jenis_deposit();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});

			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
		}
	});
}

function edit_tipe(id){
	clear_tipe();
	$("#jenis_deposit_id").val(id);
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_deposit/edit_tipe', 
		dataType: "JSON",
		method: "POST",
		data : {
				id : id,
			
			},
		success: function(data) {
			
				$("#cover-spin").hide();
			if (data){
				$("#jenis_deposit_id").val(data.id);
				$("#tipe_id").val(data.tipe_id).trigger('change.select2');
				$("#nama").val(data.nama);
				
			}else{
				swal({
					title: "Data Error!",
					text: "Tidak Ditemukan.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
		}
	});
}
function clear_tipe(){
	$("#jenis_deposit_id").val('');
	$("#nama").val('');
	$("#tipe_id").val(0).trigger('change');
	
}
function load_jenis_deposit(){
	$('#index_tipe').DataTable().destroy();	
	table = $('#index_tipe').DataTable({
            searching: true,
			autoWidth: false,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
		
            ajax: { 
                url: '{site_url}setting_deposit/load_jenis_deposit', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function hapus_tipe(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_deposit/hapus_tipe',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_tipe').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
//FORM
</script>
