<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}antrian_poli_display" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="form-group">
                <label class="col-md-12 control-label" for="bg-color-khusus">BG Color <span style="color:red;">*</span></label>
                <div class="col-md-12">
                    <div class="js-colorpicker input-group colorpicker-element">
                        <input class="form-control" type="text" id="bg-color-khusus" value="{bg_color_khusus}">
                        <span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
                    </div>
                </div>
            </div>
        </div>

        <?php if (UserAccesForm($user_acces_form,array('1549'))){ ?>
        <hr>

        <b><span class="label label-success" style="font-size:12px">TUJUAN YANG DITAMPILKAN</span></b>

        <div class="row push-20-t">
            <div class="form-group">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="data-tujuan">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">No</th>
                                    <th width="10%" class="text-center">Urutan</th>
                                    <th width="75%" class="text-center">Tujuan</th>
                                    <th width="10%" class="text-center">Aksi</th>
                                </tr>
                                <tr>
                                    <th>#</th>
                                    <th>
                                        <input tabindex="0" type="text" class="form-control number" id="nomor_urut" placeholder="No Urut" required>
                                    </th>
                                    <th>
                                        <select id="tujuan_id" class="js-select2 form-control" style="width: 100%;">
                                            <option value="#" selected>Pilih Opsi</option>
                                            <?php foreach($list_tujuan as $row) { ?>
                                            <option value="<?= $row->id; ?>"><?= $row->nama_tujuan; ?></option>
                                            <?php } ?>
                                        </select>
                                    </th>
                                    <th>
                                        <div class="btn-group">
                                            <button class="btn btn-primary btn-sm" title="Simpan" type="button" id="btn-add"><i class="fa fa-save"></i></button>
                                            <button class="btn btn-warning btn-sm" title="Reset" type="button" id="btn-reset"><i class="fa fa-refresh"></i></button>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?}?>
    </div>
</div>

<!-- Modal Setting -->
<div class="modal fade" id="modalSetting" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="block-options">
                    <li>
                        <button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
                    </li>
                </ul>
                <h3 class="block-title">Pengaturan Display</h3>
            </div>
            <div class="modal-body">
                <form id="formSetting">
                    <div class="form-group">
                        <label for="namaDisplay">Nama Display</label>
                        <input type="text" class="form-control" id="namaDisplay" disabled>
                    </div>
                    <div class="form-group">
                        <label for="namaTujuan">Nama Tujuan</label>
                        <input type="text" class="form-control" id="namaTujuan" disabled>
                    </div>
                    <div class="form-group">
                        <label for="tampilNoAntrian">Tampilkan No Antrian</label>
                        <select class="form-control" id="tampilNoAntrian">
                            <option value="1">Ya</option>
                            <option value="0">Tidak</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tampilNoMedrec">Tampilkan Medrec</label>
                        <select class="form-control" id="tampilNoMedrec">
                            <option value="1">Ya</option>
                            <option value="0">Tidak</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tampilNamaPasien">Tampilkan Nama Pasien</label>
                        <select class="form-control" id="tampilNamaPasien">
                            <option value="1">Ya</option>
                            <option value="0">Tidak</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tampilNoDaftar">Tampilkan No Daftar</label>
                        <select class="form-control" id="tampilNoDaftar">
                            <option value="1">Ya</option>
                            <option value="0">Tidak</option>
                        </select>
                    </div>
                    <button type="button" class="btn btn-primary" onclick="simpanSetting()">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>

<input type="text" id="display_id" value="{id}" hidden>
<input type="text" id="id" value="" hidden>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $(".number").number(true, 0, '.', ',');
    
    loadDataTujuanKhusus();

    $("#bg-color-khusus").click(function() {
        resetForm();
    });

    $("#btn-reset").click(function() {
        resetForm();
    });

    $("#bg-color-khusus").change(function() {
        let display_id = $("#display_id").val();
        let bg_color = $("#bg-color-khusus").val();

        $.ajax({
            url: '{site_url}antrian_poli_display/simpan_display_khusus',
            dataType: "JSON",
            method: "POST",
            data: {
                display_id: display_id,
                bg_color: bg_color,
            },
            complete: function(data) {
                $.toaster({
                    priority: 'success',
                    title: 'Succes!',
                    message: ' Simpan'
                });
            }
        });
    });

    $("#btn-add").click(function() {
        let id = $("#id").val();
        let display_id = $("#display_id").val();
        let nomor_urut = $("#nomor_urut").val();
        let tujuan_id = $("#tujuan_id").val();

        if ($("#nomor_urut").val() == '') {
            sweetAlert("Maaf...", "Tentukan No Urut", "error");
            return false;
        }
        if ($("#tujuan_id").val() == '#') {
            sweetAlert("Maaf...", "Tentukan Tujuan", "error");
            return false;
        }

        $("#cover-spin").show();

        $.ajax({
            url: '{site_url}antrian_poli_display/simpan_tujuan_khusus',
            dataType: "JSON",
            method: "POST",
            data: {
                id: id,
                display_id: display_id,
                nomor_urut: nomor_urut,
                tujuan_id: tujuan_id,
            },
            complete: function(data) {
                $("#cover-spin").hide();

                $('#data-tujuan').DataTable().ajax.reload(null, false);
                
                resetForm();
                
                $.toaster({
                    priority: 'success',
                    title: 'Succes!',
                    message: ' Simpan'
                });
            }
        });
    });

    
    $(document).on("click", ".setting",function() {
        var id = $(this).data('id');
        var namaDisplay = '{nama_display}';
        var namaTujuan = $(this).data('tujuan');
        var tampilNoAntrian = $(this).data('nomor-antrian');
        var tampilNoMedrec = $(this).data('nomor-medrec');
        var tampilNamaPasien = $(this).data('nama-pasien');
        var tampilNoDaftar = $(this).data('nomor-daftar');

        $("#id").val(id);

        $('#namaDisplay').val(namaDisplay);
        $('#namaTujuan').val(namaTujuan);

        $("#tampilNoAntrian").val(tampilNoAntrian).trigger('change');
        $("#tampilNoMedrec").val(tampilNoMedrec).trigger('change');
        $("#tampilNamaPasien").val(tampilNamaPasien).trigger('change');
        $("#tampilNoDaftar").val(tampilNoDaftar).trigger('change');
    });
})

function simpanSetting() {
    var id = $('#id').val();
    var tampilNoAntrian = $('#tampilNoAntrian option:selected').val();
    var tampilNoMedrec = $('#tampilNoMedrec option:selected').val();
    var tampilNamaPasien = $('#tampilNamaPasien option:selected').val();
    var tampilNoDaftar = $('#tampilNoDaftar option:selected').val();

    $("#cover-spin").show();

    $.ajax({
        url: '{site_url}antrian_poli_display/simpan_tujuan_khusus_setting',
        dataType: "JSON",
        method: "POST",
        data: {
            id: id,
            tampil_nomor_antrian: tampilNoAntrian,
            tampil_nomor_medrec: tampilNoMedrec,
            tampil_nama_pasien: tampilNamaPasien,
            tampil_nomor_daftar: tampilNoDaftar,
        },
        complete: function(data) {
            $("#cover-spin").hide();

            $('#data-tujuan').DataTable().ajax.reload(null, false);
            
            resetForm();
            
            $.toaster({
                priority: 'success',
                title: 'Succes!',
                message: ' Simpan'
            });
        }
    });
    
    $('#modalSetting').modal('hide');
}

function resetForm() {
    $("#id").val('');
    $("#nomor_urut").val('');
    $("#tujuan_id").val('#').trigger('change');
    $("#tampilNoAntrian").val('').trigger('change');
    $("#tampilNoMedrec").val('').trigger('change');
    $("#tampilNamaPasien").val('').trigger('change');
    $("#tampilNoDaftar").val('').trigger('change');
}

function loadDataTujuanKhusus() {
    var display_id = $("#display_id").val();

    $('#data-tujuan').DataTable().destroy();
    $('#data-tujuan').DataTable({
        "autoWidth": false,
        "searching": false,
        "serverSide": true,
        "processing": true,
        "order": [],
        "pageLength": 10,
        "ordering": false,
        "ajax": {
            url: '{site_url}antrian_poli_display/load_tujuan_khusus',
            type: "POST",
            dataType: 'json',
            data: {
                display_id: display_id
            }
        },
        "columnDefs": [
            {
                "width": "5%",
                "targets": [0, 1],
                "className": "text-center"
            },
        ],
    });
}

function editTujuanKhusus(id) {
    $.ajax({
        url: '{site_url}antrian_poli_display/edit_tujuan_khusus',
        dataType: "JSON",
        method: "POST",
        data: {
            id: id
        },
        success: function(data) {
            $("#id").val(data.id);
            $("#nomor_urut").val(data.nourut);
            $("#tujuan_id").val(data.tujuan_id).trigger('change');
        }
    });
}

function hapusTujuanKhusus(id) {
    swal({
        title: "Anda Yakin ?",
        text: "Untuk Hapus Tujuan?",
        type: "success",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#34a263",
        cancelButtonText: "Batalkan",
    }).then(function() {
        $("#cover-spin").show();

        $.ajax({
            url: '{site_url}antrian_poli_display/hapus_tujuan_khusus',
            type: 'POST',
            data: {
                id: id
            },
            complete: function() {
                $.toaster({
                    priority: 'success',
                    title: 'Succes!',
                    message: ' Hapus Data'
                });

                $('#data-tujuan').DataTable().ajax.reload(null, false);
                $("#cover-spin").hide();
            }
        });
    });
}

</script>
