<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mtriage" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('mtriage/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="row">
					
					<div class="form-group">
						<label class="col-md-2 control-label" for="nama">Nama Triage</label>
						<div class="col-md-10">
							<input type="text" class="form-control" id="nama" placeholder="Nama Triage" name="nama" value="{nama}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label" for="nama_lain">Nama Triage (Lain)</label>
						<div class="col-md-10">
							<input type="text" class="form-control" id="nama_lain" placeholder="Nama Triage Lain" name="nama_lain" value="{nama_lain}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label" for="nama_lain">Setting Warna</label>
						<div class="col-md-2">
							<div class="js-colorpicker input-group colorpicker-element">
								<input class="form-control" type="text" id="warna" name="warna" value="{warna}">
								<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
							</div>
						</div>
						<label class="col-md-2 control-label" for="nama_lain">Respon Time</label>
						<div class="col-md-6">
							<input type="text" class="form-control" id="respon_time" placeholder="Respon Time" name="respon_time" value="{respon_time}">
						</div>
					</div>
					
					
					<div class="form-group">
						<div class="col-md-12 col-md-offset-2">
							<button class="btn btn-success" name="btn_simpan" type="submit" value="1">Simpan</button>
							<a href="{base_url}mtriage" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
						</div>
					</div>
					
				
			</div>

			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>

</div>
<input type="hidden" id="id" value="{id}">

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		
	});
	
	function loadFile_img(event){
		// alert('load');
		var output_img = document.getElementById('output_img');
		output_img.src = URL.createObjectURL(event.target.files[0]);
	}
	
	function validate_final(){
		
		
		if ($("#nama").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Triage", "error");
			return false;
		}
		
		$("*[disabled]").not(true).removeAttr("disabled");
		$("#cover-spin").show();
		return true;

		
	}
	
	
</script>