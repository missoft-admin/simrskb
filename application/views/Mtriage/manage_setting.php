<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mtriage" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mtriage/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group" style="margin-bottom:10px">
				<label class="col-md-2 control-label" for="nama">Nama</label>
				<div class="col-md-10">
					<input type="text" class="form-control" disabled id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="mtriage_id" placeholder="mtriage_id" name="mtriage_id" value="{id}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group" style="margin-bottom:10px">
				<label class="col-md-2 control-label" for="nama">Respon Time</label>
				<div class="col-md-10">
					<input type="text" class="form-control" disabled id="respon_time" placeholder="Respon Time" name="respon_time" value="{respon_time}" required="" aria-required="true">
				</div>
			</div>
			
			<?if ($id){?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="">&nbsp;</label>
				<div class="col-md-10">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="index_nilai" style=" vertical-align: baseline;">
							<thead>
								<tr>
									<th width="5%">No</th>
									<th width="20%">Pemeriksaan Triage</th>
									<th width="10%">Value</th>
									<th width="30%">Penilaian</th>
									<th width="15%">Action</th>										   
								</tr>
								<tr>
									<th  style=" vertical-align: top;">#<input class="form-control" type="hidden" id="setting_id" value=""></th>
									<th  style=" vertical-align: top;">
										<select tabindex="8" id="mtriage_pemeriksaan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Referensi Nilai" required>
											<option value="" selected></option>
											<?foreach(get_all('mtriage_pemeriksaan',array('staktif'=>1)) as $r){?>
											<option value="<?=$r->id?>"><?=$r->nama?></option>
											<?}?>
										</select>
									</th>
									<th  style=" vertical-align: top;">
										<input class="form-control decimal" type="text" style="width:100%" id="nilai" placeholder="Nilai" value="" >
									</th>
									<th  style=" vertical-align: top;">
										<input class="form-control" type="text" style="width:100%" id="penilaian" placeholder="Penilaian" value="" >
										
									</th>
								
									<th style=" vertical-align: top;">
										<div class="btn-group">
										<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_nilai" name="btn_tambah_nilai"><i class="fa fa-save"></i> Simpan</button>
										<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_nilai()"><i class="fa fa-refresh"></i> Clear</button>
										</div>
									</th>										   
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>					
				</div>
			</div>
			<?}?>
			
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){	
	$(".decimal").number(true,0,'.',',');
	$('.js-summernote').summernote({
		  height: 60,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
	let mtriage_id=$("#mtriage_id").val();
	if (mtriage_id){
		load_nilai();
	}
	
})	
function clear_nilai(){
	$("#setting_id").val('');
	$("#nilai").val('');
	// $("#mtriage_pemeriksaan_id").val('').trigger('change');
	$("#penilaian").val('');
	
	$("#btn_tambah_nilai").html('<i class="fa fa-save"></i> Save');
	
}
function load_nilai(){
	let mtriage_id=$("#mtriage_id").val();
	$('#index_nilai').DataTable().destroy();	
	table = $('#index_nilai').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mtriage/load_nilai', 
                type: "POST" ,
                dataType: 'json',
				data : {
							mtriage_id:mtriage_id
					   }
            }
        });
}		
function edit_nilai(setting_id){
	$("#setting_id").val(setting_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mtriage/find_nilai',
		type: 'POST',
		dataType: "JSON",
		data: {setting_id: setting_id},
		success: function(data) {
			$("#btn_tambah_nilai").html('<i class="fa fa-save"></i> Edit');
			$("#mtriage_pemeriksaan_id").val(data.mtriage_pemeriksaan_id).trigger('change');
			$("#nilai").val(data.nilai);
			$("#penilaian").val(data.penilaian);
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_nilai(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Referensi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mtriage/hapus_nilai',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_nilai').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

$("#btn_tambah_nilai").click(function() {
	let mtriage_pemeriksaan_id=$("#mtriage_pemeriksaan_id").val();
	let nilai=$("#nilai").val();
	let penilaian=$("#penilaian").val();
	let setting_id=$("#setting_id").val();
	let mtriage_id=$("#mtriage_id").val();
	
	if (nilai==''){
		sweetAlert("Maaf...", "Tentukan Nilai", "error");
		return false;
	}
	if (mtriage_pemeriksaan_id==''){
		sweetAlert("Maaf...", "Tentukan Pengkajian", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mtriage/simpan_nilai', 
		dataType: "JSON",
		method: "POST",
		data : {
				nilai:nilai,
				mtriage_pemeriksaan_id:mtriage_pemeriksaan_id,
				setting_id:setting_id,
				penilaian:penilaian,
				mtriage_id:mtriage_id,
				
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==false){
				sweetAlert("Maaf...", "Ada Duplicate Nilai!", "error");
			}else{
				
				clear_nilai();
				$('#index_nilai').DataTable().ajax.reload( null, false ); 
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}
		}
	});

});
$("#mtriage_pemeriksaan_id").change(function() {
	$("#nilai").val($("#mtriage_pemeriksaan_id option:selected").text());

});

</script>