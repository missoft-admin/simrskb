
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<form action="{site_url}tunit_permintaan/save_acc_pengiriman/<?php echo $this->uri->segment(3) ?>" method="post" class="form-horizontal">
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">{title}</h3>
        </div>
        <div class="block-content">
            <div class="row">
                <div class="col-md-6">
                  <table class="table table-bordered info_permintaan">
                        <tr>
                            <td >No.Transaksi</td>
                            <td><?php echo $head_permintaan['nopermintaan'] ?></td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td><?php echo $head_permintaan['datetime'] ?></td>
                        </tr>
                        <tr>
                            <td>Yang Meminta</td>
                            <td><?php echo $head_permintaan['requester'] ?></td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered info_permintaan">
                        <tr>
                            <td width="40%">Dari Unit</td>
                            <td id="penerima_text"><?php echo $head_permintaan['penerima'] ?></td>
                            <td hidden id="idpeminta"><?php echo $head_permintaan['idunitpenerima'] ?></td>
                        </tr>
                        <tr>
                            <td width="40%">Ke Unit</td>
                            <td><?php echo $head_permintaan['peminta'] ?></td>
                            <td hidden id="idpenerima"><?php echo $head_permintaan['idunitpeminta'] ?></td>
                        </tr>
                        <tr>
                            <td width="40%">Total Permintaan</td>
                            <td><?php echo $head_permintaan['totalbarang'] ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <table class="table table-bordered" id="datatable">
                <thead>
                    <tr>
                        <th width="10%">Tipe</th>
                        <th width="20%">Nama</th>
                        <th width="10%">Pesan</th>
                        <th width="10%">Sudah Kirim</th>
                        <th width="10%">Sudah Terima</th>
                        <th width="10%">Belum Terima</th>
                        <th width="10%">Dialihkan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($detail_permintaan as $r): ?>
                        <tr>
                            <td><?php echo $this->model->get_namabarang($r->idtipe,$r->idbarang)->tipe ?></td>
                            <td><?php echo $this->model->get_namabarang($r->idtipe,$r->idbarang)->nama ?></td>
                            <td><?php echo $r->kuantitas ?></td>
                            <td><?php echo $r->kuantitaskirim ?></td>
                            <td><?php echo $r->sudah_terima ?></td>
                            <td><?php echo ($r->kuantitas)-($r->sudah_terima+$r->kuantitasalih) ?></td>
                            <td><?php echo $r->kuantitasalih ?></td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
            <div class="form-group text-right">
                <div class="col-md-12">
                    <a href="{site_url}tunit_permintaan" class="btn btn-default" type="reset">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</form>


<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
    var table,tr;

    $('#datatable_alihan_permintaan tbody').on('keyup', '.kuantitaskirim', function(){
        var n = $(this).val()
        tr = $(this).closest('tr');
        tr.find('td:eq(9)').text(n)
        detailvalue_alihan()
    })
    $(document).on('click', '.alihkan', function(){



        var penerima_text = '<?php echo $head_permintaan['penerima'] ?>';
        $('input[name=unit_penerima_text]').val(penerima_text);
        $('input[name=idunitpenerima]').val('<?php echo $head_permintaan['idunitpenerima'] ?>');
        var unitid = '<?php echo $head_permintaan['idunitpenerima'] ?>';

        $('select[name=idunitpeminta]').select2({
            placeholder: 'Cari Data ... ',
            allowClear: true,
            ajax: {
                url: '{site_url}tunit_permintaan/selectdariunit_alihan/'+unitid,
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {results:data}
                },
                cache: false,
            }
        })

        detailvalue_alihan()
    })
    $('#datatable tbody').on('click', '#pemesanan', function(){
        var tr = $(this).closest('tr');
        swal({
            text: 'Apakah anda akan mebuat pemesanan baru dengan jumlah yang diperlukan oleh unit?',
            showCancelButton: true,
            confirmButtonColor: '#d26a5c',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            html: false,
            preConfirm: function() {
              return new Promise(function (resolve) {
                setTimeout(function() { resolve() }, 50);
              })
            }
        }).then(
            function () {
                $('#modal_pemesanan').modal('show');
                $('input[name=kuantitas_pemesanan]').val( tr.find('td:eq(2)').text() )
            },
            function () {
                $('#modal_pemesanan').modal('show');
                $('input[name=kuantitas_pemesanan]').val( tr.find('td:eq(2)').text()-tr.find('td:eq(3)').text() )
            }
        )

        $('select[name=iddistributor]').select2({
            placeholder: 'Cari Data ... ',
            allowClear: true,
            ajax: {
                url: '{site_url}tunit_permintaan/selectdistributor/',
                dataType: 'json',
                delay: 500,
                processResults: function (data) {
                    return {results:data}
                },
                cache: false,
            }
        })

        var dataoption = $('select[name=iddistributor]')

        $.ajax({
            url: '{site_url}tunit_permintaan/ajax_distributor_tersering',
            method: 'post',
            dataType: 'json',
            data: {idtipe: tr.find('td:eq(11)').text(), idbarang: tr.find('td:eq(12)').text()},
            success: function(res) {
                $.ajax({
                    url: '{site_url}tunit_permintaan/selectdistributor/'+res,
                    dataType: 'json',
                    success: function(data) {
                        var option = new Option(data.text, data.id, true, true);
                        dataoption.append(option).trigger('change');
                        dataoption.trigger({
                            type: 'select2:select',
                            params: {
                                data: data
                            }
                        })
                    }
                })
            }
        })

        $('input[name=tipebarang_pemesanan]').val( tr.find('td:eq(0)').text() )
        $('input[name=idtipe_pemesanan]').val( tr.find('td:eq(11)').text() )
        $('input[name=namabarang_pemesanan]').val( tr.find('td:eq(1)').text() )
        $('input[name=idbarang_pemesanan]').val( tr.find('td:eq(12)').text() )

    })

    $('#form2').on('submit', (function(e){
        e.preventDefault();
        var formData = new FormData(this)

        if(validate_pengalihan()) {
            $.ajax({
                method:'POST',
                url: '{site_url}tunit_permintaan/save_pengalihan_permintaan',
                data: formData,
                cache: false,
                dataType: 'json',
                contentType: false,
                processData: false,
                success:function(res){
                    if(res.status == '200') {
                        swal('Sukses','Data berhasil disimpan!','success');
                        $.ajax({
                            url: '{site_url}tunit_permintaan/ajax_cekstatuspermintaan',
                            dataType: 'json',
                            method: 'post',
                            data: {id: '<?php echo $this->uri->segment(3) ?>'},
                            success: function(res) {
                                setTimeout(function() {
                                    if(res == 4 || res == 3) {
                                        window.location = '{site_url}tunit_permintaan';
                                    } else {
                                        window.location = '{site_url}tunit_permintaan/acc_pengiriman/<?php echo $this->uri->segment(3) ?>';
                                    }
                                }, 1000);
                            }
                        })
                    } else {
                        swal('Kesalahan','Data gagal disimpan!','danger')
                        setTimeout(function() {
                            location.reload();
                        }, 1000);
                    }
                }
            })
        }

    }))

    $('#form3').on('submit', (function(e){
        e.preventDefault();
        var formData = new FormData(this)

        $.ajax({
            method:'POST',
            url: '{site_url}tunit_permintaan/save_pemesanan_distributor',
            data: formData,
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            success:function(res){
                if(res.status == '200') {
                    swal('Sukses','Data berhasil disimpan!','success');
                    setTimeout(function() {
                        window.location = '{site_url}tunit_permintaan/acc_pengiriman/<?php echo $this->uri->segment(3) ?>';
                    }, 1000);
                } else {
                    swal('Kesalahan','Data gagal disimpan!','danger')
                }
            }
        })        
    }))

    function detailvalue_alihan() {
        var detail_tbl = $('table#datatable_alihan_permintaan tbody tr').get().map(function(row) {
            return $(row).find('td').get().map(function(cell) {
                return $(cell).html();
            });
        });

        $("#detailvalue_alihan").val(JSON.stringify(detail_tbl))
    }

    function validate_pengalihan() {
        if($('select[name=idunitpeminta]').val() == '' || $('select[name=idunitpeminta]').val() == null) {
            alert('Keunit Tidak boleh kosong')
            return false;
        }
        return true;
    }
</script>