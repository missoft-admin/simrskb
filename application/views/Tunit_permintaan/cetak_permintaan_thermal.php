<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Print Permintaan </title>
    <style>
	@page { margin: 15px; 0px;}
    body {
      -webkit-print-color-adjust: exact;
    }
      

      table {
		font-family: "arial", Verdana, sans-serif;
        font-size: 10px !important;
        border-collapse: collapse !important;
        width: 95% !important;
      }
      td {
        padding: 5px;
		 
      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
		
      }
      .content-2 td {
        margin: 3px;
		border: 0px solid #6033FF;
      }

      /* border-normal */
      .border-full {
        border: 0px solid #000 !important;
		
      }
	  .text-header{
		font-size: 13px !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-bottom-top-left {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	   br {
		   display: block;
		   margin: 10px 0;
		}
    </style>
    <script type="text/javascript">
    	
    </script>
  </head>
  <body>
  <div style="width:226">
    <table class="content" style="width:100%">
      <tr style="border-bottom:2px dotted #000 !important;" >
      
		<td class="text-center" rowspan="8" >
          &nbsp;{nama_rs}<br>
          &nbsp;{alamat_rs1}<br>
          &nbsp;{telepon_rs}<br>
          &nbsp;<?=$kota_rs?>, <?=$kodepos_rs?>
        
		</td>
      </tr>

    </table>
	<table class="content-2"  style="width:100%">
	  
      <tr>		
        <td colspan="3" class="text-center"><strong>BUKTI PERMINTAAN</strong></td>
      </tr>
	  <tr>
        <td style="width:30%">NO TRANSAKSI </td>
        <td style="width:1%">:</td>
        <td style="width:69%"><?=$nopermintaan?></td>        
      </tr>
	  <tr>
        <td >TANGGAL &JAM </td>
        <td>:</td>
        <td><?=$datetime?></td>					
      </tr>
	  <tr>
        <td >DARI UNIT</td>
        <td >:</td>
        <td ><?=$unit_dari?></td>		
      </tr>
	  <tr>
		<td>KEPADA UNIT</td>
        <td >:</td>
        <td ><?=$unit_ke?></td>		
	  </tr>
	  
    </table>
	<br>	
    <table class="content-2"  style="width:100%">
	   <tr>		
        <td colspan="3" class="text-center"><strong>Rincian Barang</strong></td>
      </tr>
      <tr>
        <td class="border-full text-center" style="width:5%">NO</td>
        <td class="border-full text-left" style="width:10%">TIPE</td>
        <td class="border-full text-center" style="width:10%">KODE</td>
        <td class="border-full text-left" style="width:40%">BARANG</td>
        <td class="border-full text-left" style="width:20%">QTY</td>
        <td class="border-full text-center" style="width:20%">STOK SISA</td>
      </tr>
      <?php $number = 0; ?>
      <?php foreach  ($detail_permintaan as $row){ ?>
        <?php $number = $number + 1; ?>
        <tr>
          <td class="border-full text-center"> <?=strtoupper(($number))?></td>
          <td class="border-full text-left"> <?=strtoupper(($row->nama_tipe))?></td>
          <td class="border-full text-left"> <?=strtoupper(($row->kode))?></td>
          <td class="border-full text-left"> <?=strtoupper(($row->namabarang))?></td>
          <td class="border-full text-left"> <?=strtoupper(($row->kuantitas))?></td>
          <td class="border-full text-center"><?= number_format($row->sisa_stok,0)?></td>
        </tr>
      <?php } ?>
	  
    </table>
	<br>
	<table>
		<tr>
			<td width="30%" class="text-center">Yang  Meminta<br><br><br><br></td>
			<td width="70%"></td>
		</tr>
		<tr><td width="30%" class="text-center"></td><td width="70%"></td></tr>
		<tr><td width="30%" class="text-center"></td><td width="70%"></td></tr>
		<tr><td width="30%" class="text-center"></td><td width="70%"></td></tr>
		<tr><td width="30%" class="text-center"></td><td width="70%"></td></tr>
		<tr><td width="30%" class="text-center"></td><td width="70%"></td></tr>
		<tr>
			<td width="30%" class="text-center">({namauser})</td>
			<td width="70%"></td>
		</tr>
		<tr>
			<td colspan="2" class="text-left"><strong>Tanggal & Jam Cetak : <?=date('d-m-Y H:i:s')?></strong></td>
		</tr>
	</table>
	
	</div>
  </body>
</html>
