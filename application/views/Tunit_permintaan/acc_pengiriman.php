<?php echo ErrorSuccess($this->session)?> 
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>
<div id="cover-spin"></div>
<form action="{site_url}tunit_permintaan/save_acc_pengiriman/<?php echo $this->uri->segment(3) ?>" method="post" class="form-horizontal">
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">{title}</h3>
        </div>
        <div class="block-content">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered info_permintaan">
                        <tr>
                            <td>No.Transaksi</td>
                            <td><?php echo $head_permintaan['nopermintaan'] ?>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>Yang Meminta</td>
                            <td><?php echo $head_permintaan['requester'] ?>
                            </td>
                        </tr>
						<tr>
                            <td>Tanggal Minta</td>
                            <td><?php echo $head_permintaan['datetime'] ?>
                            </td>
                        </tr>
						<tr>
                            <td>Tanggal Kirim</td>
                            <td><input class="js-datetimepicker form-control input-sm"  type="text" id="tanggal" name="tanggal"  value="<?=date('d-m-Y H:i:s')?>"/>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered info_permintaan">
                        <tr>
                            <td width="40%">Dari Unit</td>
                            <td id="penerima_text"><?php echo $head_permintaan['penerima'] ?>
                            </td>
                            <td hidden id="idpeminta"><?php echo $head_permintaan['idunitpenerima'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%">Ke Unit</td>
                            <td><?php echo $head_permintaan['peminta'] ?>
                            </td>
                            <td hidden id="idpenerima"><?php echo $head_permintaan['idunitpeminta'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%">Total Permintaan</td>
                            <td><?php echo $head_permintaan['totalbarang'] ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <?php if ($head_permintaan['idunitpeminta'] != 0 || $head_permintaan['idunitpeminta'] != 49): ?>
			<?php if (UserAccesForm($user_acces_form,array('1105'))){ ?>
            <div style="margin: 10px 0" class="text-right">
                <!-- <button class="btn btn-warning alihkan" data-toggle="modal" data-target="#modal_alihkan_permintaan" type="button">Alihkan</button>!-->
                <button class="btn btn-warning alihkan" data-toggle="modal" onclick="location.href='{base_url}tunit_permintaan/alihkan/<?php echo $this->uri->segment(3) ?>';" type="button">Alihkan</button>
            </div>
			<?}?>
            <?php endif ?>
            <table class="table table-bordered" id="datatable">
                <thead>
                    <tr>
                        <th width="10%">Tipe</th>
                        <th width="10%">Nama</th>
                        <th width="10%">Pesan</th>
                        <th width="10%">Stok Tersedia</th>
                        <th width="10%">Sudah Kirim</th>
                        <th width="10%">Sisa Belum Kirim</th>
                        <th width="10%">Dialihkan</th>
                        <th width="10%">Stok Unit</th>
                        <th width="10%">Kirim</th>
                        
                        <th width="10%">Aksi</th>
                   
                        <th hidden>idunitpeminta</th>
                        <th hidden>idunitpnerima</th>
                        <th hidden>idtipe</th>
                        <th hidden>idbarang</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($detail_permintaan as $r): ?>
                    <tr>
                        <td><?php echo $this->model->get_namabarang($r->idtipe,$r->idbarang)->tipe ?></td>
                        <td><?php echo $this->model->get_namabarang($r->idtipe,$r->idbarang)->nama ?></td>
                        <td><?php echo $r->kuantitas ?></td>
                        <td><?php echo number_format($r->stok_ke,0) ?></td>
                        <td><?php echo $r->kuantitaskirim ?></td>
                        <td><?php echo $r->kuantitassisa ?></td>
                        <td><?php echo $r->kuantitasalih ?></td>
                        <td><?php echo number_format($r->stok_dari,0) ?></td>
                        <td>
                            <?php $jumlahkirim = ($r->stok_ke < $r->kuantitassisa) ? $r->stok_ke : $r->kuantitassisa ?>
                            <?php if ($jumlahkirim <= 0): ?>
                            <input type="text" name="kuantitaskirim[]" class="form-control" value="<?php echo $jumlahkirim ?>" disabled>
                            <?php else: ?>
                            <input type="text" name="kuantitaskirim[]" id="kuantitaskirim[]" class="form-control angka" value="<?php echo $jumlahkirim ?>">
                            <input type="hidden" name="id[]" class="form-control" value="<?php echo $r->id ?>">
                            <?php endif ?>
                        </td>
                        <?php if ($r->stpemesanan=='0'){ ?>
                        <td><button class="btn btn-warning" id="pemesanan" type="button">Ke Distributor</button></td>
                        <?php }else{ ?>
                        <td><button disabled class="btn btn-success" type="button">Sudah Dipesan</button></td>
                        <?php } ?>
                        <td hidden><?php echo $r->idunitpeminta ?></td> <!-- 10 -->
                        <td hidden><?php echo $r->idunitpenerima ?></td>
                        <td hidden><?php echo $r->idtipe ?></td>
                        <td hidden><?php echo $r->idbarang ?></td>
                        <td hidden><?php echo $r->id ?></td><!-- 14 -->
                        <td hidden><?php echo $r->jumlahsatuanbesar ?></td><!-- 15 -->
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
            <div class="form-group text-right">
                <div class="col-md-12">
                    <a href="{site_url}tunit_permintaan" class="btn btn-default" type="reset">Kembali</a>
                    <button class="btn btn-success" id="btn_simpan" type="submit">Submit</button>
                </div>
            </div>
			
			<div class="row">
				<?if($detail_kirim){?>
                <div class="col-md-6">
						<div class="block-header">
							<h3 class="block-title">History Pengiriman</h3>
						</div>
						<div class="block-content">
							<table class="table table-striped table-borderless table-header-bg">
								<thead>
									<tr>
										<th class="text-center" style="width: 50px;">#</th>
										<th class="text-left">Tanggal</th>
										<th class="text-left">User Kirim</th>
										<th class="text-left">Barang</th>
										<th class="text-right">Jumlah</th>
									</tr>
								</thead>
								<tbody>
								<?
								$no=1;
								foreach($detail_kirim as $row){?>
									<tr>
										<td class="text-right"><?=$no?></td>
										<td class="text-left"><?=$row->tanggal_kirim?></td>
										<td class="text-left"><?=$row->user_nama_kirim?></td>
										<td class="text-left"><?=$row->namabarang?></td>
										<td class="text-right"><?=$row->kuantitas?></td>

									</tr>
								<?
								$no=$no+1;
								}?>

								</tbody>
							</table>
						</div>
                </div>
				<?}?>
				<?if($detail_kirim){?>
				<div class="col-md-6">
						<div class="block-header">
							<h3 class="block-title">History Pengalihan</h3>
						</div>
						<div class="block-content">
							<table class="table table-striped table-borderless table-header-bg">
								<thead>
									<tr>
										<th class="text-center" style="width: 50px;">#</th>
										<th class="text-left">Tanggal & User</th>
										<th class="text-left">Dari</th>
										<th class="text-left">Ke</th>
									</tr>
								</thead>
								<tbody>
								<?
								$no=1;
								foreach($history_alihan as $row){?>
									<tr>
										<td class="text-right"><?=$no?></td>
										<td class="text-left"><?=$row->tanggal.'<br>'.$row->name?></td>
										<td class="text-left"><?=$row->dari?></td>
										<td class="text-left"><?=$row->ke?></td>

									</tr>
								<?
								$no=$no+1;
								}?>

								</tbody>
							</table>
						</div>
                </div>
				<?}?>
            </div>
			
        </div>
    </div>
</form>

<form action="javascript:(0)" method="post" class="form-horizontal" id="form3">
    <div class="modal in" id="modal_pemesanan" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">Alihkan Permintaan</h3>
                    </div>
                    <div class="block-content">
                        <input type="hidden" id="rowIndex">
                        <input type="hidden" id="idpermintaan" name="idpermintaan" value="<?php echo $this->uri->segment(3) ?>">
                        <input type="hidden" id="idtipe_pemesanan" name="idtipe_pemesanan">
                        <input type="hidden" id="idbarang_pemesanan" name="idbarang_pemesanan">
                        <input type="hidden" id="xkuantitas" name="xkuantitas">
                        <input type="hidden" id="unit_peminta" name="unit_peminta" value="<?php echo $head_permintaan['idunitpenerima'] ?>">
                        <input type="hidden" id="iddet_permintaan" name="iddet_permintaan">
                        <input type="hidden" id="no_up_permintaan" name="no_up_permintaan" value="<?php echo $head_permintaan['nopermintaan'] ?>">


                        <div class="form-group">
                            <label class="control-label col-md-3">Tipe Barang</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="tipebarang_pemesanan" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Barang</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="namabarang_pemesanan" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Opsi Satuan</label>
                            <div class="col-md-6">
                                <select id="opsisatuan" required name="opsisatuan" class="form-control" style="width: 100%;">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Distributor</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <select class="form-control" name="iddistributor" style="width:100%" required></select>
                                    <div class="input-group-btn">
                                        <button class='btn btn-info modal_list_distributor'  data-target="#modal_list_distributor" id="btn_cari" type='button' title="Pencarian">
                                            <i class="fa fa-search"></i>
                                        </button>
										<a href="{base_url}mdistributor" class="btn btn-success" target="blank" title="Distributor Baru"><i class="fa fa-plus"></i></a>
										
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Kuantitas</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="kuantitas_pemesanan" readonly required>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Kuantitas Kecil</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="kuantitas_kecil" name="kuantitas_kecil" readonly required>
                                <input type="hidden" class="form-control" id="jumlahsatuanbesar"  name="jumlahsatuanbesar" readonly required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-sm btn-success" type="submit">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="modal in" id="modal_list_distributor" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
            <div class="block-header bg-success">
                <h3 class="block-title">Pencarian Distributor</h3>
            </div>
            <div class="block-content">
                <table class="table table-bordered" id="datatable-1">
                    <thead>
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Telepon</th>
                        <th>Aksi</th>
                        <th hidden>id</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>
</div>
<div class="modal in" id="distributor_baru" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Distributor Baru</h3>
                </div>
                <div class="block-content">
                    <form name="form_distributor" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="nama">Nama</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="" required="" aria-required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="alamat">Alamat</label>
                            <div class="col-md-7">
                                <textarea class="form-control" id="alamat" placeholder="Alamat" name="alamat" required="" aria-required="true"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="telepon">Telepon</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="telepon" placeholder="Telepon" name="telepon" value="" required="" aria-required="true">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                <button class="btn btn-sm btn-success" type="submit" id="btnSaveDistributor">Simpan</button>
            </div>
        </div>
    </div>
</div>

<form action="javascript:(0)" method="post" class="form-horizontal" id="form2">
    <div class="modal in" id="modal_alihkan_permintaan" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">Alihkan Permintaan</h3>
                    </div>
                    <div class="block-content">
                        <input type="hidden" name="idpermintaanhead" value="<?php echo $this->uri->segment(3) ?>">
                        <input type="hidden" name="idpermintaandetail">
                        <input type="hidden" name="idunitpenerima">
                        <input type="hidden" name="detailvalue_alihan" id="detailvalue_alihan">
						<input type="hidden" id="iddari" name="iddari" value="<?=$head_permintaan['idunitpenerima']?>">
                        <input type="hidden" id="idke" name="idke" value="<?=$head_permintaan['idunitpeminta']?>">
                        <div class="form-group">
                            <label class="control-label col-md-3">Ke Unit</label>
                            <div class="col-md-6">
                                <select class="form-control" name="idunitpeminta" style="width:100%"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Dari Unit</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="unit_penerima_text" readonly>
                            </div>
                        </div>
                        <table class="table table-bordered" id="datatable_alihan_permintaan">
                            <thead>
                                <tr>
                                    <th width="10%">Tipe</th>
                                    <th width="10%">Nama</th>
                                    <th width="10%">Pesan</th>
                                    <th width="10%">Jumlah yang mau dialihkan?</th>
                                    <th hidden>idunitpeminta</th>
                                    <th hidden>idunitpnerima</th>
                                    <th hidden>idtipe</th>
                                    <th hidden>idbarang</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($detail_permintaan as $r): ?>
                                <?php $jumlahkirim = ($r->stok_ke < $r->kuantitassisa) ? $r->stok_ke : $r->kuantitassisa ?>
                                <tr>
                                    <td><?php echo $this->model->get_namabarang($r->idtipe,$r->idbarang)->tipe ?>
                                    </td>
                                    <td><?php echo $this->model->get_namabarang($r->idtipe,$r->idbarang)->nama ?>
                                    </td>
                                    <td><?php echo $r->kuantitas ?>
                                    </td>
                                    <td>
                                        <input type="text" name="kuantitaskirim[]" class="form-control kuantitaskirim" value="<?php echo $r->kuantitassisa ?>">
                                        <input type="hidden" name="id[]" class="form-control" value="<?php echo $r->id ?>">
                                    </td>
                                    <td hidden><?php echo $r->idunitpeminta ?>
                                    </td> <!-- 4 -->
                                    <td hidden><?php echo $r->idunitpenerima ?>
                                    </td>
                                    <td hidden><?php echo $r->idtipe ?>
                                    </td>
                                    <td hidden><?php echo $r->idbarang ?>
                                    </td>
                                    <td hidden><?php echo $r->id ?>
                                    </td>
                                    <td hidden><?php echo $r->kuantitassisa ?>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-sm btn-success" type="submit">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</form>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
		$("#tanggal").datetimepicker({
			format: "DD-MM-YYYY HH:mm",
			// stepping: 30
		});
		
        $("#btn_newdistributor").click(function() {
            $("#modal_pemesanan").modal("hide");
            $("#distributor_baru").modal("show");
        });
		$("#btn_cari").click(function() {
            // $("#modal_pemesanan").modal("hide");
            $("#modal_list_distributor").modal("show");
        });

        $("#btnSaveDistributor").click(function(){
            var data={
                nama:$("#nama").val(),
                alamat:$("#alamat").val(),
                telepon:$("#telepon").val(),
            };
            $.post("{site_url}mdistributor/ajaxSave",data,function(cb){
                var cb=$.parseJSON(cb);
                if(cb.code==200){
                    $("#modal_pemesanan").modal("show");
                    $("#distributor_baru").modal("hide");

                    var el="<option value='"+cb.id+"' selected>"+cb.nama+"</option>"
                    $("#iddistributor").append(el).val(cb.id);
                }else{
                    alert("Data gagal di simpan");
                }
            });
        });
    });

	$('.angka').keydown(function(event){
		keys = event.keyCode;
		// Untuk: backspace, delete, tab, escape, and enter
		if (event.keyCode == 116 || event.keyCode == 46 || event.keyCode == 188 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
			 // Untuk: Ctrl+A
			(event.keyCode == 65 && event.ctrlKey === true) ||
			 // Untuk: home, end, left, right
			(event.keyCode >= 35 && event.keyCode <= 39) || event.keyCode == 190 || event.keyCode == 110|| event.keyCode == 188) {
				 // melanjutkan untuk memunculkan angka
				return;
		}
		else {
			// jika bukan angka maka tidak terjadi apa-apa
			if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault();
			}
		}

	});
	$('.modal_list_distributor').on('click', function(){
        $('select[name=iddistributor]').val('').trigger('change');
        // var idtipe = $('select[name=idtipe]').val();
		var idunitpelayanan='';
        // var idunitpelayanan = $('select[name=dariunit]').val();
        // if(idtipe !== '') {
            table = $('#datatable-1').DataTable({
                destroy: true,
                serverSide: true,
                searching: true,
                sort: false,
                autoWidth: false,
                ajax: {
                    url: '{site_url}tunit_permintaan/filter_distributor/',
                    type: 'post',
                    data: {idunitpelayanan: idunitpelayanan}
                },
                columns: [
                    {data: 'id'},
                    {data: 'nama'},
                    {data: 'alamat'},
                    {data: 'telepon'},
                    {defaultContent: '<a href="#" class="btn btn-xs btn-default">Pilih</a>', searchable: false, },
                    {data: 'id', visible: false},
                ]
            })
            setTimeout(function() {
                $('div.dataTables_filter input').focus()
            }, 500);
        // }
    })
	$('#datatable-1 tbody').on('click', 'a', function(){
        tr = table.row( $(this).parents('tr') ).index()

        var data = { 
            id: table.cell(tr,0).data(), 
            text: table.cell(tr,1).data()
        };
        var newOption = new Option(data.text, data.id, true, true);
        $('select[name=iddistributor]').append(newOption).trigger('change');
        // $('#margin').val(data.margin);
        // $('#harga').val(data.harga);

        $('#modal_list_distributor').modal('hide')
    })
    $('#opsisatuan').select2({
        placeholder: 'Select an Option',
        data: [{
                id: '',
                text: ''
            },
            {
                id: '1',
                text: 'Kecil'
            },
            {
                id: '2',
                text: 'Besar'
            },
        ]
    }).val(1).trigger('change')

    $('#opsisatuan').on('change', function() {
        // var idtipe_pemesanan = $('input[name=idtipe_pemesanan]').val();
        // var idbarang_pemesanan = $('input[name=idbarang_pemesanan]').val();
        var xkuantitas = $('input[name=xkuantitas]').val();
        var jumlah_besar = $('#jumlahsatuanbesar').val();
        var n = $(this).val()
		if (n=='1'){
			$('input[name=kuantitas_pemesanan]').val(xkuantitas);
			$('input[name=kuantitas_kecil]').val(xkuantitas);
		}else{
			$('input[name=kuantitas_pemesanan]').val(xkuantitas);
			$('input[name=kuantitas_kecil]').val(xkuantitas * jumlah_besar);
		}
        // $.ajax({
            // url: '{site_url}tunit_permintaan/konversi_satuan/',
            // method: 'POST',
            // dataType: 'json',
            // data: {
                // idtipe: idtipe_pemesanan,
                // idbarang: idbarang_pemesanan,
                // xkuantitas: xkuantitas,
                // opsisatuan: n,
            // },
            // success: function(data) {
                // $('input[name=kuantitas_pemesanan]').val(data);
            // }
        // })
    })


    var table, tr;

    $('#datatable_alihan_permintaan tbody').on('keyup', '.kuantitaskirim', function() {
        var n = $(this).val()
        tr = $(this).closest('tr');
        tr.find('td:eq(9)').text(n)
        detailvalue_alihan()
    })

    $(document).on('click', '.alihkan', function() {
        var penerima_text = '<?php echo $head_permintaan['penerima'] ?>';
        $('input[name=unit_penerima_text]').val(penerima_text);
        $('input[name=idunitpenerima]').val('<?php echo $head_permintaan['idunitpenerima'] ?>');
        var unitid = '<?php echo $head_permintaan['idunitpenerima'] ?>';

        $('select[name=idunitpeminta]').select2({
            placeholder: 'Cari Data ... ',
            allowClear: true,
            ajax: {
                url: '{site_url}tunit_permintaan/selectdariunit_alihan/' + unitid,
                dataType: 'json',
                delay: 250,
                processResults: function(data) {
                    return {
                        results: data
                    }
                },
                cache: false,
            }
        });

        detailvalue_alihan()
    });

    $('#datatable tbody').on('click', '#pemesanan', function() {
        var tr = $(this).closest('tr');
        $('input[name=tipebarang_pemesanan]').val(tr.find('td:eq(0)').text());
        $('input[name=idtipe_pemesanan]').val(tr.find('td:eq(12)').text());
        $('input[name=namabarang_pemesanan]').val(tr.find('td:eq(1)').text());
        $('input[name=idbarang_pemesanan]').val(tr.find('td:eq(13)').text());
        $('#jumlahsatuanbesar').val(tr.find('td:eq(15)').text());
        $('#opsisatuan').val(1).trigger('change');

        swal({
            text: 'Apakah anda akan mebuat pemesanan baru dengan jumlah yang diperlukan oleh unit?',
            showCancelButton: true,
            confirmButtonColor: '#d26a5c',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            html: false,
            preConfirm: function() {
                return new Promise(function(resolve) {
                    setTimeout(function() {
                        resolve()
                    }, 50);
                })
            }
        }).then(
            function() {
                $('#modal_pemesanan').modal('show');
                $('input[name=kuantitas_pemesanan]').val(tr.find('td:eq(2)').text())
                $('input[name=xkuantitas]').val(tr.find('td:eq(2)').text())
                $('input[name=kuantitas_kecil]').val(tr.find('td:eq(2)').text())
            },
            function() {
                $('#modal_pemesanan').modal('show');
                $('input[name=kuantitas_pemesanan]').val(tr.find('td:eq(5)').text())
                $('input[name=xkuantitas]').val(tr.find('td:eq(5)').text())
                $('input[name=kuantitas_kecil]').val(tr.find('td:eq(5)').text())
            }
        )
		$('input[name=iddet_permintaan]').val(tr.find('td:eq(14)').text())
		var c_idtipe = $("#idtipe_pemesanan").val();
        var c_idbarang = $("#idbarang_pemesanan").val();
		// alert(c_idtipe+' : '+idbarang);
        $('select[name=iddistributor]').select2({
            placeholder: 'Cari Data ... ',
            allowClear: true,
            ajax: {
                url: '{site_url}tgudang_pemesanan/getsorted_distributor_2/',
                dataType: 'json',
                delay: 500,
				type: 'post', 
                quietMillis: 50, 
                data: function (params) {
                    var query = { search: params.term, idtipe: c_idtipe, idbarang: c_idbarang } 
                    return query; 
                }, 
                processResults: function(data) {
                    return {
                        results: data
                    }
                },
                cache: false,
            }
        });

        
    });

    $('#form2').on('submit', (function(e) {
        e.preventDefault();
        var formData = new FormData(this)

        if (validate_pengalihan()) {
            $.ajax({
                method: 'POST',
                url: '{site_url}tunit_permintaan/save_pengalihan_permintaan',
                data: formData,
                cache: false,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function(res) {
                    if (res.status == '200') {
                        swal('Sukses', 'Data berhasil disimpan!', 'success');
                        $.ajax({
                            url: '{site_url}tunit_permintaan/ajax_cekstatuspermintaan',
                            dataType: 'json',
                            method: 'post',
                            data: {
                                id: '<?php echo $this->uri->segment(3) ?>'
                            },
                            success: function(res) {
                                setTimeout(function() {
                                    if (res == 4 || res == 3) {
                                        window.location = '{site_url}tunit_permintaan';
                                    } else {
                                        window.location = '{site_url}tunit_permintaan/acc_pengiriman/<?php echo $this->uri->segment(3) ?>';
                                    }
                                }, 1000);
                            }
                        })
                    } else {
                        swal('Kesalahan', 'Data gagal disimpan!', 'danger')
                        setTimeout(function() {
                            location.reload();
                        }, 1000);
                    }
                }
            })
        }

    }))

    $('#form3').on('submit', (function(e) {
        e.preventDefault();
        var formData = new FormData(this)
		console.log(formData);
		// return false;
        $.ajax({
            method: 'POST',
            url: '{site_url}tunit_permintaan/save_pemesanan_distributor',
            data: formData,
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function(res) {
				console.log(res);
                if (res.status == '200') {
                    swal('Sukses', 'Data berhasil disimpan!', 'success');
                    setTimeout(function() {
                        window.location = '{site_url}tunit_permintaan/acc_pengiriman/<?php echo $this->uri->segment(3) ?>';
                    }, 4000);
                } else {
                    swal('Kesalahan', 'Data gagal disimpan!', 'danger')
                }
            }
        })
    }))

    function detailvalue_alihan() {
        var detail_tbl = $('table#datatable_alihan_permintaan tbody tr').get().map(function(row) {
            return $(row).find('td').get().map(function(cell) {
                return $(cell).html();
            });
        });

        $("#detailvalue_alihan").val(JSON.stringify(detail_tbl))
    }

    function validate_pengalihan() {
        if ($('select[name=idunitpeminta]').val() == '' || $('select[name=idunitpeminta]').val() == null) {
            alert('Keunit Tidak boleh kosong')
            return false;
        }
        return true;
    }
	$(document).on("keyup",".angka",function(){
		var jml=$(this).closest('tr').find("td:eq(5)").html();
		// alert($(this).val()+' '+jml);
		if (parseFloat($(this).val())>parseFloat(jml)){
			alert('Pengiriman Lebih Besar Dari Sisa');
			$(this).val(jml);
		}
	});
	$(document).on("click","#btn_simpan",function(){
		 $("#btn_simpan").hide();
		$("#cover-spin").show();
	});
</script>
