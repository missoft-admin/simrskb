<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
            <div class="block-header bg-success">
                <h3 class="block-title">{title}</h3>
            </div>
            <div class="block-content">
                <input type="hidden" class="form-control" name="idpermintaan" value="{idpermintaan}">
                <div class="form-group">
                    <div class="col-md-6">
                        <label>Tipe Barang</label>
                        <select disabled class="form-control" name="idtipe" style="width:100%"></select>
                        <input type="hidden" class="form-control" name="idtipeval" value="{idtipe}">
                    </div>
                    <div class="col-md-6">
                        <label>Dari Unit</label>
                        <input readonly type="text" class="form-control" name="dariunit" value="{dariunit}">
                        <input type="hidden" class="form-control" name="iddariunit" value="{iddariunit}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <label>Nama Barang</label>
                        <input readonly type="text" class="form-control" name="namabarang" value="{namabarang}">
                        <input type="hidden" class="form-control" name="idbarang" value="{idbarang}">
                    </div>
                    <div class="col-md-6">
                        <label>Alihkan Ke Unit</label>
                        <select class="form-control" name="keunit" style="width:100%"></select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <label>Stok Unit</label>
                        <input readonly type="text" class="form-control" name="stokunit">
                    </div>
                    <div class="col-md-6">
                        <label>Kuantitas</label>
                        <input type="text" class="form-control" name="kuantitas" value="{kuantitas}">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-default" type="button" id="btnback-4">Back</button>
            <button class="btn btn-sm btn-success" type="submit">Alihkan</button>
        </div>
    </div>
</div>

<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">

    var table;


    var data = [
        {id: 0, text: 'Select an Option'},
        {id: 1, text: 'Alkes'},
        {id: 2, text: 'Implan'},
        {id: 3, text: 'Obat'},
        {id: 4, text: 'Logistik'},
    ]
    $('select[name=idtipe]').select2({
        data:data,
        placeholder: 'Cari Data ..'
    }).val('{idtipe}').trigger('change')

    $('select[name=idtipe]').on('change', function(){
        var n = $(this).val()
        if(n != 0) {
            $('#modal-1').modal('hide')
            $('#modal-2').modal('show')
            $.ajax({
                url: '{site_url}tunit_permintaan/list_barang',
                success: function(html) {
                    $('#modal-content-2').html(html)
                }
            })
        }
    })
    
    $('#btnback-4').on('click', function(){
        $('#modal-4').modal('hide')
        $('#modal-content-4').html('')
    })

    $(document).ready(function(){
        var n = '{iddariunit}'
        var idtipe = $('input[name=idtipeval]').val()
        var idbarang = $('input[name=idbarang]').val()
        $('select[name=keunit]').empty()
        $('select[name=keunit]').select2({
            placeholder: 'Cari Data ... ',
            ajax: {
                url: '{site_url}tunit_permintaan/list_unitke',
                data: {idbarang: idbarang, idtipe: idtipe, idunitdari: n},
                type: 'post',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {results:data}
                },
            }
        })

        setTimeout(function() { 
            $('select[name=keunit]').focus()
        }, 200)
    })
    $('select[name=keunit]').on('change', function(){
        var keunit = $('select[name=keunit] :selected').val()
        var stok = '-';
        if(keunit !== 0) {
            var splitstok = $('select[name=keunit] :selected').text().split(' || ')
            stok = splitstok[1];
        }
        $('input[name=stokunit]').val(stok)
        setTimeout(function() {
            $('input[name=kuantitas]').focus()
        }, 100);
    })
    $('#tambahkan-1').on('click', function(){
        if(validate()) {
            table = $('#datatable-1').DataTable({
                info: false,
                searching: false,
                paging: false,
                sort: false,
                destroy: true,
                columnDefs: [
                    {targets: [7,8,9,10], visible: false}
                ]
            })

            var keunit = $('select[name=keunit] :selected').text().split(' || ');
            table.row.add([
                $('select[name=idtipe] :selected').text(),
                $('input[name=kodebarang]').val(),
                $('input[name=namabarang]').val(),
                $('select[name=dariunit] :selected').text(),
                keunit[0],
                $('input[name=kuantitas]').val(),
                '<a href="#" class="deletable-1"><i class="fa fa-trash-o"></i></a>',
                $('input[name=idbarang]').val(), //7
                $('select[name=idtipe] :selected').val(),
                $('select[name=dariunit] :selected').val(), 
                $('select[name=keunit] :selected').val(),
            ]).draw( false )

            detailvalue()

            $('select[name=dariunit], select[name=keunit]').select2().empty()
            $('input[name=kodebarang], input[name=namabarang], input[name=stokunit], input[name=kuantitas]').val('')
            $('select[name=idtipe]').val(0).trigger('change').focus()
        }
    })


    $('#form4').on('submit', (function(e){
        e.preventDefault();
        var formData = new FormData(this)

        if(validate()) {            
            $.ajax({
                method:'POST',
                url: '{site_url}tunit_permintaan/save_permintaan_alihan/',
                data: formData,
                cache: false,
                dataType: 'json',
                contentType: false,
                processData: false,
                success:function(res){
                    if(res.status == '200') {
                        swal('Sukses','Data berhasil dialihkan!','success')
                        $('#modal-4').modal('hide')
                        $('#modal-content-4').html('')
                        $('#modal-3').modal('show')
                        $.ajax({
                            url: '{site_url}tunit_permintaan/acc_pengiriman/'+$('input[name=idpermintaan]').val(),
                            success: function(html) {
                                $('#modal-content-3').html(html)
                            }
                        })
                        setTimeout(function() {
                            $('#index_list').DataTable().ajax.reload()
                            window.location = '{site_url}tunit_permintaan';
                        }, 1000);
                    } else {
                        swal('Kesalahan','Data gagal dialihkan!','danger')
                    }
                }
            })
        }

    }))    




    // function 
    function detailvalue() {
        table = $('#datatable-1').DataTable()
        var detailvalue = table.rows().data().toArray();
        
        $('input[name=detailvalue]').val( JSON.stringify(detailvalue) )
    }
    function validate() {
        if($('select[name=keunit]').val() == null) {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Ke unit tidak boleh kosong'});
            return false;
        }
        if($('input[name=kuantitas]').val() == '' || $('input[name=kuantitas]').val() < 0) {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Kuantitas tidak boleh kosong'});
            return false;
        }
        if(parseFloat($('input[name=kuantitas]').val()) > parseFloat($('input[name=stokunit]').val())) {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Kuantitas tidak boleh lebih besar dari stok unit'});
            $('input[name=kuantitas]').val('').focus()
            return false;
        }
        return true;
    }
    function ajaxSelect(data) {
        if(data) {
            $(data.id).empty()
            $(data.id).select2({
                placeholder: 'Cari Data ... ',
                ajax: {
                    url: data.url,
                    data: data.data,
                    type: 'post',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {results:data}
                    },
                    cache: false,
                }
            })
        }
    }    
</script>