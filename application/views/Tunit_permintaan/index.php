<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('1102'))){ ?>
<div class="block">
    <div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('1103'))){ ?>
        <ul class="block-options">
            <li><a href="{site_url}tunit_permintaan/add" class="btn btn-default tambah-permintaan"><i class="fa fa-plus"></i> Tambah</a></li>
        </ul>
		<?}?>
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:25px">
        <div class="row">
            <?php echo form_open('tunit_permintaan/filter', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Unit Tampil</label>
                    <div class="col-md-8">
                        <select id="idunittampil" name="idunittampil[]" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
						<?php foreach($list_tampil as $row){?>
                            <option value="<?=$row->id;?>" <?=in_array($row->id, $array_dari)?'Selected':''?>><?=$row->text;?></option>
                        <?php }?>
                        </select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Dari Unit </label>
                    <div class="col-md-8">
                        <select id="iddariunit" name="iddariunit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" <?($iddariunit=='#')?'Selected':''?>>- Belum Dipilih -</option>
						<?php foreach($list_dari as $row){?>
                            <option value="<?=$row->id;?>" <?=($row->id==$iddariunit)?'Selected':''?>><?=$row->text;?></option>
                        <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Ke Unit</label>
                    <div class="col-md-8">
                        <select id="idkeunit" name="idkeunit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" <?($idkeunit=='#')?'Selected':''?>>- Belum Dipilih -</option>
							<?php foreach($list_ke as $row){?>
								<option value="<?=$row->id;?>" <?=($idkeunit==$row->id)?'Selected':''?>><?=$row->text;?></option>
							<?php }?>
						
						</select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">No Transaksi</label>
                    <div class="col-md-8">
                        <input type="text" name="nopermintaan" class="form-control" value="{nopermintaan}">
                    </div>
                </div>
                
            </div>
            <div class="col-md-6">
                
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Status</label>
                    <div class="col-md-8">
                        <select id="status" name="statuspenerimaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#">Semua Status</option>
                            <option value="1" <?=(1 == $statuspenerimaan ? 'selected' : ''); ?>>Proses Permintaan</option>
                            <option value="2" <?=(2 == $statuspenerimaan ? 'selected' : ''); ?>>Proses Penerimaan / Proses kirim [Sebagian]</option>
                            <option value="3" <?=(3 == $statuspenerimaan ? 'selected' : ''); ?>>Proses Penerimaan / Proses kirim [Semua]</option>
                            <option value="4" <?=(4 == $statuspenerimaan ? 'selected' : ''); ?>>Selesai FULL</option>
                            <option value="5" <?=(5 == $statuspenerimaan ? 'selected' : ''); ?>>Alihkan Semua</option>
                            <option value="6" <?=(6 == $statuspenerimaan ? 'selected' : ''); ?>>Alihkan Sebagian</option>
                            <option value="7" <?=(7 == $statuspenerimaan ? 'selected' : ''); ?>>Selesai [Dialihkan]</option>
                            <option value="11" <?=('11' == $statuspenerimaan ? 'selected' : ''); ?>>Dibatalkan</option>
                            <option value="12" <?=('12' == $statuspenerimaan ? 'selected' : ''); ?>>Ditolak</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" name="tanggaldari" placeholder="From" value="{tanggaldari}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" name="tanggalsampai" placeholder="To" value="{tanggalsampai}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <div class="block-content">
        <table class="table table-bordered table-striped" id="datatable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>No. Transaksi</th>
                    <th>Dari Unit</th>
                    <th>Ke Unit</th>
                    <th>T. Pesan</th>
                    <th>T. Kirim</th>
                    <th>T. Alih</th>
                    <th>T. Terima</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
<?}?>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
    var table;
    $(document).ready(function(){
		// $('#iddariunit').select2();
        table = $('#datatable').DataTable({
            "autoWidth": false,
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
            ajax: { 
                url: '{site_url}tunit_permintaan/get_index', 
                type: "POST" ,
                dataType: 'json'
            }
        });
		
    });
		$("#iddariunit").change(function(){
			var arr=$(this).val();
			// alert(arr);
			$('#idkeunit')
							.find('option')
							.remove()
							.end()
							.append('<option value="#">- Belum Dipilih -</option>')
							.val(null).trigger("liszt:updated");
			// if (arr !=null){
			// var x_str=arr.join(",");
			// alert(x_str);
			if ($(this).val()!=''){
				
				$.ajax({
					url: '{site_url}tunit_permintaan/selectkeunit_index/'+arr,
					dataType: "json",
					success: function(data) {
						

						// $('#idkeunit').append('<option value="#">- Belum Dipilih -</option>');
						$.each(data, function (i,row) {
							$('#idkeunit').append('<option value="' + row.id + '">' + row.text + '</option>');
						});
					}
				});
			}
			// }

		});

            // placeholder: 'Cari Data ... ',
            // allowClear: true,
            // ajax: {
                // url: '{site_url}tunit_permintaan/selectdariunit',
                // dataType: 'json',
                // delay: 250,
                // processResults: function (data) {
                    // return {results:data}
                // },
                // cache: false,
            // }
        // }).on('change', function(){
            // $('select[name=idkeunit]').val(null).trigger("change");
            // var idunitpeminta = $(this).val();
            // $('select[name=idkeunit]').select2({
                // placeholder: 'Cari Data ... ',
                // allowClear: true,
                // ajax: {
                    // url: '{site_url}tunit_permintaan/selectkeunit_index/'+idunitpeminta,
                    // dataType: 'json',
                    // delay: 250,
                    // processResults: function (data) {
                        // return {results:data}
                    // },
                    // cache: false,
                // }
            // })
        // });
   
</script>