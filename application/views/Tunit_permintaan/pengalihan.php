<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>

<form action="{site_url}tunit_permintaan/save_alihkan/<?php echo $this->uri->segment(3) ?>" id="form-work" method="post" class="form-horizontal">
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">{title}</h3>
        </div>
        <div class="block-content">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered info_permintaan">
                        <tr>
                            <td>No.Transaksi</td>
                            <td><?php echo $head_permintaan['nopermintaan'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td><?php echo $head_permintaan['datetime'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Yang Meminta</td>
                            <td><?php echo $head_permintaan['requester'] ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered info_permintaan">
                        <tr>
                            <td width="40%">Dari Unit</td>
                            <td id="penerima_text"><?php echo $head_permintaan['penerima'] ?>
							<input type="hidden" name="dariunit" id="dariunit" class="form-control" value="<?php echo $head_permintaan['idunitpenerima'] ?>">
							<input type="hidden" name="idpermintaan" id="idpermintaan" class="form-control" value="<?php echo $idpermintaan ?>">
							<input type="hidden" name="notrx" id="notrx" class="form-control" value="<?php echo $head_permintaan['nopermintaan'] ?>">
							<input type="hidden" name="id_ke_asal" id="id_ke_asal" class="form-control" value="<?php echo $head_permintaan['idunitpeminta'] ?>">
                            </td>
                            <td hidden id="idpeminta"><?php echo $head_permintaan['idunitpenerima'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%">Alihkan Ke Unit</td>
                            <td>
								<select class="form-control" name="keunit" id="keunit" style="width:100%">
									<option value="999999" selected>-Silahkan Pilih Tujuan-</option>
									<?foreach($list_unit as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>
									<?}?>
								</select>
								
                            </td>
                            <td hidden id="idpenerima"><?php echo $head_permintaan['idunitpeminta'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%">Total Permintaan</td>
                            <td><?php echo $head_permintaan['totalbarang'] ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
					<table class="table table-bordered table-striped table-responsive" id="tabel_barang">
						<thead>
							<tr>
								<th width="25%">Alihkan Ke Unit</th>
								<th width="10%">Tipe</th>
								<th width="20%">Barang</th>
								<th width="10%">Sisa</th>
								<th width="10%">Keterangan</th>
								<th width="20%">JML. Alihkan</th>
							</tr>
						</thead>
						<tbody>
							<?foreach ($list_barang_alihan as $row){?>
								<tr>
									<td>
										<select class="form-control detail" name="keunit_detail[]" id="keunit_detail[]" style="width:100%">
											<option value="999999" selected>-Silahkan Pilih Tujuan-</option>
											<?
											$ke_unit_list_detail=$this->model->ke_unit_list_detail($head_permintaan['idunitpenerima'],$row->idtipe,$row->idbarang,$idkeunit);
											echo $ke_unit_list_detail;
											foreach($ke_unit_list_detail as $r){
												$ket='';
												if ($r->available=='00'){
													$ket=' [TIDAK TERSEDIA]';
												}elseif($r->stok >= $row->kuantitassisa){
													$ket=' *';
												}
												?>
												<option value="<?=$r->id?>"><?=$r->nama.$ket?></option>
											<?}?>
										</select>
									</td>
									<td hidden><?=$row->idtipe?></td>
									<td hidden><?=$row->idbarang?></td>
									<td><?=$row->nama_tipe?></td>
									<td><?=$row->namabarang?></td>
									<td><?=$row->kuantitassisa?></td>
									<td class="text-center"></td>
									<td>
										<input type="text" name="kuantitaskirim[]" id="kuantitaskirim[]" class="form-control angka" readonly value="0">
										
									</td>
									<td hidden>
									<input type="hidden" name="idtipe[]" id="idtipe[]" class="form-control" value="<?php echo $row->idtipe ?>">
									<input type="hidden" name="idbarang[]" id="idtipe[]" class="form-control" value="<?php echo $row->idbarang ?>">
									<input type="hidden" name="iddet[]" id="iddet[]" class="form-control" value="<?php echo $row->id ?>">
									</td>
								</tr>
							<?}?>
						</tbody>
					</table>
				</div>
				</div>
			</div>
			
			
            <div class="form-group text-right">
                <div class="col-md-12">
                    <a href="{site_url}tunit_permintaan" class="btn btn-default" type="reset">Kembali</a>
                    <button class="btn btn-success" id="btn_save" name="btn_save" type="submit">Submit</button>
                </div>
            </div>
			
        </div>
    </div>
</form>



<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
        $('#keunit').select2();
        $('.detail').select2();
	$('.angka').keydown(function(event){
		keys = event.keyCode;
		// Untuk: backspace, delete, tab, escape, and enter
		if (event.keyCode == 116 || event.keyCode == 46 || event.keyCode == 188 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
			 // Untuk: Ctrl+A
			(event.keyCode == 65 && event.ctrlKey === true) || 
			 // Untuk: home, end, left, right
			(event.keyCode >= 35 && event.keyCode <= 39) || event.keyCode == 190 || event.keyCode == 110|| event.keyCode == 188) {
				 // melanjutkan untuk memunculkan angka
				return;
		}
		else {
			// jika bukan angka maka tidak terjadi apa-apa
			if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
		
	});
	
   
	$(document).on("keyup",".angka",function(){
		var jml=$(this).closest('tr').find("td:eq(5)").html();
		// alert($(this).val()+' '+jml);
		if (parseFloat($(this).val())>parseFloat(jml)){
			alert('Jumlah Lebih Besar Dari Sisa');
			$(this).val(jml);
		}		
	});
});
$("#keunit").change(function(){	
	var keunit = $(this).val();
	$(".detail").val(keunit).trigger('change');
});
$(document).on("change",".detail",function(){
	var idunitdari = $("#dariunit").val();
	var idtipe = $(this).closest('tr').find("td:eq(1)").text();
	var idbarang = $(this).closest('tr').find("td:eq(2)").text();
	var idunitke = $(this).val();
	// alert(idunitke);
	var jml_perlu=$(this).closest('tr').find("td:eq(5)").text();
	// console.log('ID UNIT: '+idunitdari+' TIPE :'+idtipe+' Barang:'+idbarang+' KE :'+idunitke);	
	// console.log('STOK: '+jml_perlu);	
	var detail = $(this);
	$.ajax({
		url: '{site_url}tunit_permintaan/selectkeunit_validasi/'+idunitdari+'/'+idtipe+'/'+idbarang+'/'+idunitke,
		dataType: "json",
		success: function(data) {
			if (data != null){
				
				if (data.available==null){
					detail.closest('tr').find('td').eq(6).html('<span class="label label-danger"><i class="fa fa-times"></i> TIDAK TERSEDIA</span>');
					detail.closest('tr').find("td:eq(7) input").val(0);
					detail.closest('tr').find("td:eq(7) input").prop('readonly', true);
					 // $('input').prop('readonly', true); 
				}else if (data.stok <= 0){
					detail.closest('tr').find('td').eq(6).html('<span class="label label-danger"><i class="fa fa-times"></i> STOK TIDAK CUKUP</span>');
					detail.closest('tr').find("td:eq(7) input").val(0);
					detail.closest('tr').find("td:eq(7) input").prop('readonly', true);
					// $(".ket").text('TIDAK BISA');
				}else{
					if (parseFloat(jml_perlu) > parseFloat(data.stok)){
						detail.closest('tr').find('td').eq(6).html('<span class="label label-danger"><i class="fa fa-times"></i> STOK TIDAK CUKUP</span>');
						detail.closest('tr').find("td:eq(7) input").val(0);
						detail.closest('tr').find("td:eq(7) input").prop('readonly', true);
					}else{
						detail.closest('tr').find('td').eq(6).html('<span class="label label-success"><i class="fa fa-check"></i> TERSEDIA</span>');
						detail.closest('tr').find("td:eq(7) input").val(jml_perlu);
						detail.closest('tr').find("td:eq(7) input").prop('readonly', false);
					}
				}
				
				// console.log(data);			
			}else{
				detail.closest('tr').find('td').eq(6).html('<span class="label label-danger"><i class="fa fa-times"></i> TIDAK TERSEDIA</span>');
				detail.closest('tr').find("td:eq(7) input").val(0);
				detail.closest('tr').find("td:eq(7) input").prop('readonly', true);
			}
		}
	});		
});
function validasi_save(){
	// if($("#keunit").val() == "999999"){
		// sweetAlert("Maaf...", "Tujuan Unit Harus diisi!", "error");
		// // $("#idmetode").focus();
		// return false;
	// }
	var st_lolos='0';
	var kuantitaskirim;
	$('#tabel_barang tbody tr').each(function() {
		kuantitaskirim=$(this).closest('tr').find("td:eq(7) input").val();
		if (kuantitaskirim>0){
			st_lolos='1'
		}
	});
	if (st_lolos=='0'){
		sweetAlert("Maaf...", "Belum ada Barang yang dipilih!", "error");
		return false;
	}
	return true;
}
$(document).on("click","#btn_save",function(){
	if(!validasi_save()) return false;


	$("#form-work").submit();
});

</script>