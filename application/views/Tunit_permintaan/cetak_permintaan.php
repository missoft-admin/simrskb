<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Print Permintaan</title>
    <style>
   
     @page { margin-left: 10px;margin-top: 20px;margin-right:15px }
    body {
      -webkit-print-color-adjust: exact;
      margin-left: 10px;margin-top: 20px;margin-right:15px
    }
    @media print {
      table {
        font-size: 15px !important;
		line-height: 200%
        border-collapse: collapse !important;
        width: 100% !important;

		font-family: sans-serif,"Courier", Arial;
      }
      th {
        padding: 5px;
      }
	  td {
        padding: 10px;
      }
      .content th {
        padding: 2px;
      }
      .detail td {
        padding: 0px;
		border: 1px solid #6033FF;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:1px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:1px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 24px !important;
      }
      
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: 600;
      }
    }

    @media screen {
        @page { margin-left: 10px;margin-top: 20px;margin-right:15px }
        body { margin-left: 10px;margin-top: 20px;margin-right:15px}
      table {

		font-family: "Courier", Arial,sans-serif;
        font-size: 20px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;
		 
      }
	  td {
        padding: 5px;
		 
      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
      }
      
      
	  

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-size: 20px !important;
      }
	  .text-judul{
        font-size: 24px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-bottom-top-left {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:1px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:1px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: 600;
      }
	  .text-top{
		font-size: 20px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
    }
	
    </style>
    <script type="text/javascript">
    	
    </script>
  </head>
  <body>
    <table class="content">
      <tr>
        <td rowspan="2" class="text-center"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJkAAACVCAYAAABLnljBAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAJcRJREFUeNrsnXd4XNW57n9r7z19Rr1asmXLuBdsg7ExNj1wCDgQWk6ogTgEEkI5FwgHCC0hQICcQC4QWiDJ4QKhhgCmm467sXFvsiVZXTOSps8u6/4xsoolN1kGY+33efw88uy29trv+tr61reElLINGzb2I4SUUtrdYGN/QrG7wIZNMhs2yWzYsElmwyaZDZtkNmzYJLNhk8yGDZtkNmyS2bBJZsOGTTIbNsls2CSzYaMfoNldsHPEdZNPNzSxsKKVbaEksVQSt8PJoCw3U4dmMGtELgG3o/1sCQi702yS7Sm5dO749wb+/VUTGxoj6AkTNBWhCKQlwTBRnYJD8n38x/gC7vjBcDK9brvjdgI7aXEHLKoI8pNnVrB6YysZuR5y/U5UBbr2khACU0pC0RQtTQnKB/t5/OKJnDA6z+5Am2S7xtyV9Xz/jwvRnBrl+T4sy2LXnSNwqFDRlCAZSfLslVM474hSuyNtkvWOpZUtzLj7SzRVoTTbjWHtebeoiqChLUlLVOeD66dy/KgCu0Nt77I7LMvkvCeWkTRgcM7eEQzAtCSFmS6cDo0LnlhBNJXq4gzYsEkGPPFZJeu2tDGiyItu9o0YhikZmuemtiHOgx9s7VCnNmySYUnJYx9X48l0s6+Wg2VJMrPdPPlpNZGEbrPLJlkaH69r4quqMEWZLvbVOpVAXsBJRV2UuSsbbHbZJEvjy80tSN1C7SfNJtrtsAVbWm122SRLoyqYAE2hv3xsKUFxKFSHkja7bJKlEdNlv/eCIhQM0/YsbZK1w+sErP4lhCklTtX2LG2StaMs1wOGRPSXTSZA6ialOfZcpk2ydkwvz0ZxqlhWP9lk7bGxI8szbXbZJEvj6BG5HFYWoKYlidhHcSYENLalOGRQgJPH5dvsskm23UgX/PzoISTakvscn1eEoC0U5+dHl+J1Omx22STrxCVHlTJxRCYbaiM41L51iUMVbGyIU1bi51fHldmdapOsqw0lUYTCc5dNJuDVqGiMoSp7J9M0RVAVSqJg8fxlk3A57FxQm2Rd7SgEIBlbnMFbVx2GbppsqIuiKmK3NpoANEVhU2OMaDTJq7+cwvTyHJtVO/aTnU/WKdNAsKqmjTnPrGD+qmY8WR4KAg4cmtghMxZ0U9IU0YkG40wYkckTF09k2jCbYDbJ9oJw987dyIuLa1lbHyca1UER6X8WYFl4vBojC7z8YFI+N586Apdmq0ibZH2CyZebWli8tY2qYIxoysTjUCjJ9jK1LMD08mw01SaXTTIbtuFv4+DHd0bWJ4wYilDZMaVZCAshFBS0fUrXEQIsaWFJA0uauDXfnrfNjKGgoggVRSj90A4TS1pdfNhOWzHdNq9Nsv6CbqZ4acNjrGhaQNyMogpnL1H5dJxLEQ6QfZ2AFAgBpjQxLQNDGjhVF8eUnMpp5Rfu9Kr3t77Ce1UvkTASqEJDU9JE6zvL0uEUCxNTWu3hle7vakgDt+phXM5Uzh15OS7NbZOsr4jqYW77Yg5LGj8m11OMW/UgeyWRQCLTx/ZlXkimF+0KoSAQxI0o82vfpyFey6Xjbuhx+gvrH+WRr24lx1OIT8tItwErvU6gP9qB6CBdd0mnkDTjLKj7kEX18/jtjL+S6cq1Df++4O+r/8RTq+5mXO5hSJn+hN9oxyCIm1FaEyEePPZVhmQc0nGsOV7HL+fNRlM0MpzZXVTbN9c6VQhWBZdw/sir+dnEm2zDvy9Y1byAAt8gLGl94wRLCxSJV/MTN2LMr32v27EFdR/SlgwR+FYIlm6dKS2KvGWsDi7BlKZNsr7AwkLj281ksKSFU3PQnKjv9ntDYts+2l79ZOsoGma7o2KTrA/I95TQnGhAFd+u2ShQe3STJpxpL/JbXCGuCo3meD35nmIcitM2/PuCsw/5GV/UvMuWtnUU+QajCrVfPqpEIhDt5O1pWPc6EoXS4wPvWQkC0a7aTNLzUf2Q4y0ElmWyLV6BKlTOHnmZ7V32FeVZY7j9yCf426oHaIzXkDISqL3EyfaWYkIKDGGSsGIEHJlkunLb7Sq5V0TdvaRRaUu10JYK4lI8aNKBFHIfh0c6hubUXJRnjOGi0dcyJmeKTbJ9waT8I5l07EtUhjfQlmhBFWovsaO9tQ9UEjLOhpavmVf1GtWRCgb5h/ajXSNQhUpNdAsFnhJmDzufUdmT8Sg+LPb1GWmp6HdlUpYxku8KvhMR/yGBERDo33tOzJ/GyWXn8D9Lb2Rp46cM8g3B7NVTtNCUPe8mVSjUxaoYkz2F6w67nxyPXUZqQM9d+p2Z3DL9EQq9Q2hLtexEdoCyYzftwquMGmECzixumfZwF4LtbwfhwM5xOKAl2fyKZv73y21Y0upz7v12mJYkoUvOnFLE9ycUdVFugqMHncL/W/9nfI6MPbLBlF1kzAYTjZw67Dy8jkA3FfrRukaenV+DUwNtXxb+SjCstPty9uFF7QX3hE2yvuCDNQ2ceN8C0CV4tH0frAJIWDz13hbevnE6J4/vJFp3QuyBSlS0nTZHSkmGmt19sGwKctw9C0C3wOPYd8kjBCR0Hn27gpeumcpZhw2ySdaX4XrnG5sBhVHD/P1WV0JVBOtro9z55iZOGlfYkcOvW6l+kwVCCAyre22yu97aBFIwclgmptVf7+JlQ32MO97YwA8mFe2zpB+ANpmkJW7g9zv7tXCJaUkKslxsC+k0RfZf1R1LdDoQummxNZQgO9vVbwTb/i65ASdtcYOUYUf8+9SsaUMDRKpbSRoSq90OMcw9+2ftKiFDgkNlr+vC7o1WNqTRhQzpNZk7S0MSpOu9GKa1F+8n0wtZKts4rCwTn+vAXkh8wNpkv/vhKOpaE7y3JohE4FDYpYEr2414hCSpm5hSMDTXg6p0L9qTXmfJPpck2D3VOs0nRQikFDs4D+k5gC1NcQDcju2bBeyuXRLdEkgsjp9eyB/PHW3HyfqKgoCb1381jWVVIVpjFk5N7PHHbQwneGlJLc8tqqc024Njh7lssZcivL/nKEW7hNvaHOeMyfn859RiBmV599ghSJkSn0thall2tyFmk6yPmDw4u0/XnT6pmJLsNTzwzhbK8jzdQlyqqqDthaGsKv3cTQK2NsX52awSHr1gYr9KTtsm+8bchnaVe8ZIynJchBNmz6Niz2+mCXXf2rJDsmwsZZHr0/jtGSMZCDgoSSa6hCwKM50kjX1LLNyX5GFVCFRF0LUFScMiP8OFQ9Fskn3XkdItdLP/qij2iWSKQFPThnpXo183JYkDPPRgk+wAUMd7JgW/9QRam2T7W28KIfp9/vjbzta1SXYAwbQkhmn2e0xMFSp2dQebZJ0ks6x+t8lkR/lhGwOeZGK/R/Zt2DbZzgy1dLK8/fVtku0nz1CCUASqvWOITbL9RjIkmsJeTSvZsEnWB6LZsSubZDZsktnYKzfDJpndBd+Ms4G0SWZjp7BQ1b6nNwuRXgInbZLZ2G/qcjvJbHVpY/+qy4Htytok6wtpsOMfNsm6kkH2eqDbz6Zl7sIPVDAto/vl1q5q2ApMqe/4OJtkB3P4IGVY3WpXKEKQMrpnUaiKwJRGj7JUAoFupVB2yB9zKA4safWYfBcIDKmj7XB+Uu9+rqKI9LK9ATJ/elCTzOt0EvA6aYnpKEKgCghFdfxuFa+zc3HI8KxxILqnSKf9SgtVKIzK6r6iaGT2RJyaG8PqSUzTNBiRNaHbbwGXQjhqoCoCRQjCcRO3QyHLa+f4f/dfTgh+ddwQjKYoG7ZFWL8tihmKc9UJg7tJt0PzZnBozgzWBJeRNBNY0iRlJlkdXMr43CM4vOiYbvcdnTOZwwpmsqp5MQkziiUtdCvJutByRmZPZFrx8d3Ov/qEoRBPsb6yjQ21EWI1bfziuCF4nQODZAf1Bl7b68P+74Iq5q5owAJ+OLmIcw8v6XFuS7yJx7/+PV81fY5upXAoLibmTeen428g39uzak5bMsiTK//A0oaPSRpJNMXBuNwpzJl4M0Xe0h7nv7GijucWbkNacPyYXObMKmOgzAcMCJLtDbaFN9GSDJLpzqXUX96hOHcm9GujlTTHGshwZXXbUMLGACGZjQMDA2bZTUpPYMr0opLt+x95HL4dAg2iXRWGaE0FyXTlkuHM6jjDMHVSRgJNdeLUXN3VZ7yFpBUnz1uEYemYpp4uhy4tVEXFpXl6PMcm2UGGBxbfwIaWlbg1DykziRRQnjWSKybe3lHb1ZQmz655kLe3vEBIbyRLy+P4wWdw5sg55LoL+GDry9yz8L84d9TPuWLybWlFKk3u+OLnzKt6gysPvYNzRl/GQ0tvYVXzIhShkjBjuFQ3M0tP4eKx/9VjTwDbuzyIENQbqElWECcCDohbbfx78z+49cufEko0AoJXNz7NQ8t/g1v1MiP/ZFRF5b6lN7O84QsAojJCVaKWYKqh4773LLqaFzY+xdSSo5k98nyEEDSktlEV34TX6aPEN5Sw0cpfVtzJX1f+gYGY/DNgJJlH9aKbKeaMu5HDCo+lNdHM41/fxcubnuT9ylc5Z+RlLG34FAH84tDbObzoGFqTIZY1fMqsku8D4BRuMh0u8t3FAPxjzf/w9zUP872y2dw/84WOgKtH9WCYBv9nyn2UZY5kZdNCrv74DD6vmctPx/8aMcCk2QB6W0nSTFLiH4Fb9VDoK2VM7mSShkEwnpZMBd4SkPBR9et8XvMOma5sjh38g47SURKJqmh4tQAfbH2VJ1fey4lDTuO2aY/tEP1Pb6vTnKojbkaoj1UDglmlswccwQaUJEMo+J0ZLKx7j9ZUE83xOt6rfIlMVyZj89Jbx5w5/FJWNS/mhfWP8s+NjzMpbwbfG3Im54z8eXsgwyLXU8iSxo9pqKxBFQo/HvlL8jzF3TtVaOR5Crhv0XXEzQjN8QbKAiM5ecjZtnd5cItshTxPIU+vup9IqoUMdw4BRzb/ffifOtThkIxDeOLEd3lt09MsqvuIBXUf8sDS+dREtnL1lN/j1ry4VDeNiToSZpw8TxH/WPsQo3ImkefpLNluIUmYcWYUnkSOo4iEiLCk8VN+8+Wl3Hbk45QFRtjq8uCERTjVyklDz+HCsdfi0zJwKA5OH35Rxxlme2n0M4Zfwl1H/Y37Zj3H8KwxvLXleVJmgmxXLnEj7S3eeNifOGnIOXxR+w5PfP37bk8ypUFLoplLJtzAnMm/5spJv+WkIWfxSc1HfFb9lu1dHrQUkxahZBOnD7uIyybcwoziE1nRNJ8/L0+HImoiW7jw7Vnc+sWl1EUrASjxDcMhnIj2+k8CQTBez/icqUwtPo7zx1zFmOzJvLb5aeZWPNfFIhM4VCdV4Y0YukEo0UhNpBK/QxuQK0sGjLpsS7WkP3a0kvKssVww5hq+rP2AR1f8jon50xiXczgKCq9tfpr1oRUUe4exLVbBlvA6Zg87H6fmoTZaTV0sTspIpL1Izce1U+7huk9/xJ0LLqcsYyRjcw8jqocxpcmflvw3TukiIsNURTcyKmsiR5ecOuBIpt5+++23DwDHkjXNyzCkznGDT6fINxivI0Cpv5xNLStoTtQzu/wiTht2PkKoBFMNNCSqCTgy+Y8hP+LKSXfiUJxUhjdQFV7JlMJjmFxwFAClgXIyHNlsbl2DicHhhcdQ2bYRiSTTlYOqqXgdfibmHcE1k+5mWNaYAUeyATJ3KUGKdlXVc7I7YURRhYZDdbX/P05bMojfmdGx71I6y1buMgQR1dtwq97dVMseeNNK33mSWdIiZaSToVUFHKrS/gnFAcVx3QILA5eqYs9d9hGfbWjk3ne2YEnBKePyuPL4oTs9d/7mZu6eW4FhwEnjc7n6hGF9fu7GhhhnPrqU2mCSM6cW8fiF4w+oj/jOqjoueWYVKd3iklml3HfWwFOX/UayDQ0R3nivAkzQBLskWUVDjNffqwBDIhT2iWRJw2J9XRS9Kca2UNY+bx/d34gmTWprw5CyaGhNMRDRbyQLuDWUYh+WCUWZrl2e63erOIq96IbY7bm7fQFFkOd3UpsyyfAceM6yU1NwBFzouoXfrQ5IkvVjnEwg9rDmg0SAVAbcWrGBmh66X4KxuyvTquxqn8AuCCcMWuL6Hp27R6pVt2iJ6e0eZk+0xQ3advq8nmiN64QTxp61z5BdJK2kJaaTMIzdeKFp6KZJS0wnZVrdju96kbFFW0wnoffckCKuG4RiqZ32YX8vXu5//WJK3I5dq4VcnwPTsnox0NP/f2FRFY99XEVjxMSUkO9XmTOzlIpgnDe+aibDpfLIBWMZWbTzbZ1fXFLD3W9sIjfg5KIjC6loTvDy4gZ0S+J1Kpw4Jpd7zhoLwF8+ruD5hbU0RU0sKSnO0LjkqBIumF7W673/5/2NvLq0nqaoiSJgSLaLOUeXsriijXdXNDN6sI+nLp6AS+vsXo/PQUtM58H3N/HcolrCcQuHJhiR7+HOM0YwpjizR19UNke57fUNfFXdRiIFLgccWurnd2eMYnCOr4Mkzy3axn1vbsTnc3LtiWXUtyZ48tNq1tfEuPdHY/jFsen3WLw1xF1vbGBzcwLdAL9L4cfTinBqghcWNBBJWtx6ejmnH1p0YJMsP9vN4i2tXP/iGiy545iQaIpga3OCwkw3dS09d8+95bU13PXyOvC4wJSQMlhjwYKKNrxOlVBEB8OgLbHrza+2NMVZtrQed2mAJVtbCIUNSJmggOJxsmRdiJrWBDk+Jw++XQFm+8anmsIaVeHDpQ1UhpLcdEr351zw5FKe/WArSoYHK2WCbrJKwqcbQjg0hVBjnLpYCmsHJVGS7ebdVU089kkcokZah7gdLN/YwtsrG/ngumkcMSynI462ti7MMX/4kobaWHrwOQQYkuWrQ7y3Osinv57O8Hw/AJsa0u/qKs3gmufXUtOSwGwzoD6M1b6Z66vL6jjzkcVgApoKcR0sWFTRRpZPJaZLUrURKmeW9Lvz1O8kyws4WVsf5fMVjfQarpIg/A7GFPmoa+1OspeW1HDX82vJL82gOZzk2LE5DM3zURmM8P7qIJkeJ0MLNCIJA6em7NYRIc/D0DwPmxpiZAU0Zh9agiIEb37dgBLw86+vmkjqJopTY0yxj2nDsghFk3y4NoQ7083Nz65hQkmA2RPTqTy3vr6WZz+soqgkk1AsyUmHFVAQcLG6NsyX60KUFHhRCrwMynT3eG1VQFPMwKEITjmyiNyAh2WVISqaUsRSBmc/toxlt8wk1+8iYRic/ZelNART5A8KcPnRpRxRFmBpdZj/+2EVtU1Jznp0KQtvOgqnppLh1iDPS3m+l82NcVRFcNqsQVhScPK4AoKxJOc+toSAx0VCtxhd5GbqsBzaYjpzVzaQNKE838Na3STg6n/l1m933C6x4imLHJ9G8YiMXquYKwIMC5JG94OGZXHv3M04styE4zo3/MdQ7v7h6HazUfL4J1u59fVNRJJ7t+NbY1uKCSV+/njuKI4ZmQ/Ah2sbOe/J5XhdKooQnD0xjwfOGUNhhjstTf+1lvvf3gJOjWc+38bsicVUhWL86f2tZBf6Ccd1Hv7xGH46a2iHjXPXWxv5y8dVO92RLhjVyfFrPP2TcZwxOb2OsykSZ/afl7K2LkZVXZx3Vzfx4yNKeOqzalZtbEHxObl19jCuPDa9NO+0Semtpm9+ZSPLN4R4dmENl8wYTNrjgmDUoDTLxUM/Hs33J3SqvMuf/RojbmFpkhPH5PC3S8aTH0gvbJm3tpFrX1xDdTC13+KL/U7bqmCci2aU8MdzR6dJtoNLpaqCN1fU89O/req2LffSyhZWbAsjhMIxI7O5+4dju9knlx09lFeX1fH2qhby/XseCmgOxrnvnE6CARw/Op9TxufzzMdVBAJO7j17VAfBAK4/uZxnPq+mxpTUh9PG+eItrYQjBg6n5D+PKOwgGIDHofG700czd2UzSzeHeid7S4ILpw/rIBhAnt/DVScO5by/LAMhqW1Ltn/4ZnBrDMpys2hzCz/bvJikJXErgqgpKM5yU9NssWBzK5fMGIwqFEBQH4xxzTndCRbXLT5a24zT6yDbq/HS5YfidTo7jh83Op8Lpoe5/tnVaZV8IJNse/NM3cTnUMnyOHd6blGGm9QOI74qmCRlStB1ZpRn7/QZe70flyqIpXpKF5/T0b5kTVIdTFKS6e30XUxBhsfJtlCS7eXrwgkThESPJzl+dG6P++mmhZbeAmWnLrXei5DzOQSqQ8FMSaSZflY0JfG4VTI8Kn//oBoiqQ5bDbeKyHJDfZStzbHu3rxI70rcqVsEtS1xwgmTlGExeUigG8G2I5EyYT+G8PpfAStiB1e7JyIJA1UIrC5b2vqdCgoSS1VoiCR6vc6lCWQfKuH0Vj1HN82OZ+94XDetjt+2h1scSju7VZXa1p4Oi0NVcGhilzud9NoOw+ocNe1sUYVFKiWJJk0eunQ8boeG2cUoyfQ4qG9NMro4PTCsLtqic/CK9sC3hqamN3atDyd3br+aFijqd4RkfTTmpg7LoijTTUvc4vmFtVw4fRDThnVKjOZYglW1cfxudb9PHMleZPT4Uj+ogpyAm0c+2spPZpRQ3EX6fVXVwqaGKG6X2udnpdrt1JGFPt6cX0sVkkllGcw6JG83g2jnxwoCLsYU+ahsSvBVVYSHPtjEVScM73bOF5tCOD3OHtrlgA7G9gU5PhfnHzmIWGsCSwrOeHgZD8+rYN76Jv76eSUnPLCIujadXL/jWxkFE0oyOXFsLsHWBG1xyfcfXMzf51cxb30zD35QwWkPLSZpgNfd93G7XSJdOnMwql9DURQueHIFLy+tpTmSIhRJ8tbX9Rx6yzxueHlVRyB4d4PuimOHQMogy+PgxpfXc9Ora/hwXTP/Wl7HDx9ZxBsrGhmc6znw1aUpJaZugZne+nh3I0/XLTBkt3NvP20Eb69o5OuNLfiy3fzquTV4nAqxpAlxk7xCLwlTEjesDq1kSUjoFuhW2qbrqpp0C6TE6FVdpo8ndKuHhpNAwkgfT3vBafvmqYsnMOXOz2luSbLGlFz69Ne4HAqxmA6moDDfSzipd/Oc02Zmun299Ytptfeb3tnO8YMyeOKiCVz6yDIq4wY/fmI5h+R7EEKwsSFCqi7OijVBTp1QyDEj8zrfVfT+rqdPKmbO98p48o3N+Ap83DO3gj+9vxXdkhitKQK53rQ01E1M6wCWZKZpQcqAlJ62d3Y5Yi1I6elzu0yteJ0aH143lfOPLcGpSKRhEQuni8edf2wp44t91DdHSSZ1pGV1TAgm9fRzU7rRJSQiO55h9tJzumGmj6X0HraStCxSvbRvSI6PT389nZMPzcMhLMxUun0Bn5M5J5QwLFcjFoqT0vUOCWiZ6eeQ1DF6mUaypOxyvLPfLjlqCH/9xWQmDc9AN0zWbAyyekMQy4Lp04r4503TOeqQ3A4bkmS6vcZOVN4TF07klvPGku1RkZZFPGJgJE2+P72Y743JoKY5DCkDw+r/fdH7LWmxKRJnU2MMJBRmuBma59vpuaFYgg31EUCQF3BSntdzemhdXRsLKlqJJgxGDQpw/Kg8WuJJVleHcTsVRhcF8LocxHSDtbVtmIYk2+9keIEfAdSHE2xpiiEkDMn1UJjp6RbJbo0nWV8XRQgYUeQnw+1oPy5JGgbr6iIkUhK/R2FMcQCxg/u1eEuQ5dVhDBOmlGUwdWg29a0xNtfHCPg0Rg8KoAqFlliSjfVRpIT8DCdD8/zd2hGMJtnQEAYJJTluSrN83RSgxOT91U1saU6iIBlVHGDmITnd2lLblqCyKZKeWchxU5rl32nfN7TF+WRDiKbWJAVZbs6cUgQYfLGhBU1VGJrvJT/g7teo/wAqHSXblZ4gput4HY4eR0V7wEL0apeJXv7ezfN6Sdfe/dV7cf/299kfPWWTbCcIx5Msr47SFEng0hSKMl1MGpKJ6GIVvL+mnouf+hrDgj+fP4ZzDyvBxv7FQUOy+9/dyMMfVlLblkpXvFYEPofCqEIfF88o4aIjSwl4HEy68xOWr28Bt0amV6X6vuPxuxw2E/YjDorFvXe+sY7r//o1jRGDpG4hpYVpmrTFLRatCvKr51YRTqTzxIbneyGShNYkhxR40BR7v4z9je/84t4vNge57aX1FA3ORDdMzpk6iJPG5CKBt79u5h+fbOXFKyYzKDsdOH34vDG4HQq6CXefNWK3uW82bHXJza+t4/evrkd1a/zy+CE8+KNx3Y6vqA4xsTSri1Gt7Fcj18ZBKMkSKROEwDQtMntZqDGxtPtk+7aWKOtrw0gkw/J9DNshfJI0DOaubKSiIYLXpXHsqDxGFQVYurWZ5rYERdlexpf2XBU1b10zK7e1YFkwpSyLWSNyqWhsY31dBJ9bZUpZdo/J6dU1bXy+MUg0oVOa6+Psw4pJGjrzVjfidChMKM3qSMmxSfYt4vjR2fzx3waDCjJ47JNqgjGdq48fyojC3lKzBa8uredXDy8Cy+LGCw/l7jM7U4peWFzNDS+tpzIYh4QFSAYVejl6ZA6frm9k26YQJ80awjvXzugkyrZW5vx9JYu2tGIkTLDAHdA4aVwemxujrNwUwukVrLjjWEYVbSeZxQVPLuf15Q2EIykwAIfgpIn5CGHxzlf1ENN54brpnDt1sG34f9s4dWIRl544lJrqNlIWPPx+JTP/MJ85z3zFvLWNPc53OzXI8kNmAK+rU7K8sqyG//zzMhrDBk5Vw5ehkpXloKYhzvML6tE0F+RnEvB2SpbqUIxZ9y/gyw2teJwaTq9KbrZGImXx+sJ6gjFJYVE2ORk+1HYHQ2JxxsNLeHZeNaqqojgVsrM13G6VdxfV8+XmKMMGZUOGH6d2cNTD+Y6/xfY5xUnk+5089OFWMC2aIiZPfVbDU5/XcMr4XP55+ZSOMIUiOofW9jSelniSy/93Nf5MF8mUyWmH5nPlcUPwOhU+WBvi/ncrSOommiLomvV93YtrCLbqBHwOSrKd3PGDcRyS4+Kr2hgPvFfBtlASv9uJgujYZuf+dzfxr89qKCwJkNR17j9nNDPLM2mKGjz88VbeXRVCwYFD3dWqLptk36Tf0vHXPWeN5bxpxfxjfi0frQuyeHMLuQE3c5c2cMx9X/LutdPI9bmQvRj5c79upDEUx+dzceTwLF654vCOY9PL8xhe4OWnz6zE6jIvuLkxzL9XNOL3O8nxqbxzzVSG5KSn0qaUw8njcjnqngU0RXVyvAKvM83OV5Y0oGW5aAon+cuFY5gzc2jHPU+ZUMQZDy/knZWhg6pcxkEVJJpYms19Z4/l8xuP5M8XjEVKSWmRn6VfB7nnrU0AqDtsSwiwaEsrSIgnDX5z2vAe9z1pTB7ZXq0byb7c3EIsbhCJprjoyJIOgm1HSZaPEQVeosn0pLjfpSIl1IV1LFMyfXhmN4Jtx7GjckjEDq5yBgdlETynqnLlceUE3BpznlmFI8fN4q2taZvMofSQg0ndAkXBqSnk+nqmJ7dEjfRahS7SJam338GSZLh7nzFIGbJD5alKOhPYNC0s06Iwo/fyDNGEPHj05MEkyZZXtRBP9Rz9s0bkIIREEQLDSn+4rgH+7Rk+Q3LcYEkSusXclQ097uNzK8T17uHEoXluUAQOl8aXm4K9titpdG6mmkhZqEIQcKs4XSpfbGqhMRLvcY0pe1v0bJPsW8Ury2qYdNtnzPjD/G7eZCylc/2L6/A4NZLRFKXZrk6p1cVtADh5fB5oCnkBF/fOreCFxdWdcbVQjCueXYUpJapD7bho5iE5jCzyoaoKc1c2c9NrazpysRKGwW/fXMe6hijZ7ZIxnEwfm1AaIBU1CMcNzn1sGbWtsY5nvbS0hqc+30ZhrvugqhPynVaXzdEkP/nrCogYrK2Nc/rDS5kxPIv8gIt1DWHW1cbTdpcmuOqEIcCOWbvpvycNzub86UU8O6+a/AIfc/62imfn1xBwaSypbGNdbZThhT6iqU7J49RUbj6ljIsfWkZ+SQb3zt3Cp+tDlOe6Wd8YZ/7GNgbnurDapdL2xTU3nVLOy0vqUITCooooxz2wkMOHZNCWMHl7dTMuVaEky0Uwqtsk+/aDF5KAW+XlKw7j1tfWM399kASCd5Y0gmWCmo7+ax6F1646nCPL01mkLTEdaiNgSpojnR/y6Z8cSktU5835teBx8O+F9elVyIpCbr6bbaEERnWYbcOyOq65aMZQ1tTFuOdfG0DR+Gx1kM90K10PJMeF36XSmjDTU1fts3cTS7N4+5rDOe2hpSQiOutiKdZtbgND4sh04HKprNsWhWBsF8VcbJJ9Y+ELp6ryvbH5fG9sPn+bX8X8jUFW10SJpCRZHoUpZRlcfGQp40s6i5kcOjjAmacegpQwY3hmhzRzqCpvXDWNJyZV8N6aZjY2JnAqgpkjspg9sYB3VwdZvS3CzFHdp6nuPnMsx47K45+LalhdGyVhSCaV+pk9MY+HP6qiojlMvk+BLsmLJ4wuYPltM3nq060s3NJKa9wi16fx86MHoagOXlxcj2VZjCz0HRQkOwgzY00SusDtEPRaiGOH33Y2QZ4wDFyq0msh4p1dI6VFwpB42jM7Jt/5GStqwgzL9bDgv2eQ63f2KpPjuoHHcfDmtB2EyVRqe5hC9Cr9ev7Suyfn1rQOgsldXHPTK6t5c0Vd+nehdBBs7so61tZFsExJabZrJwRL323nBDs4xv/A2cBrnxRzT+imyW/+tZ57/7Ga7LIA158c4ZhROXhUwcKtbdz15kZ8Lo1ES5TzjijuxyfbJBswkBI21kfBqRJq07npn2sJBBxoAkIxE6EIZEuKM48r4bKjhw7sQWpvdL9vePSjCp5bWMuyylYiMRMkKA7B8Hwvp08p5O4fjh7wKd42yfoJS7Y0s6EhQdKQFGU6mVaeSZbHZXeMTTIbtndpwyaZDRs2yWzYJLNhk8yGDZtkNmyS2bABwP8fAFSAfmYAod2UAAAAAElFTkSuQmCC" alt="" width="150" height="150"></td>
        <td  colspan="7"   class="text-center text-bold text-judul">PERMINTAAN BARANG</td>  
		
      </tr>
	  <tr>
		<td  class="text-left text-top">Tanggal & Jam Permintaan</td>
		<td  class="text-top">:</td>
		<td  class="text-top"> {datetime}</td>
		<td  class="text-left text-top text-top">Unit Pelayanan Peminta</td>
		<td  class="text-top">:</td>
        <td  colspan="2" class=" text-top">{unit_dari}</td>
	  </tr>
	  <tr>
	    <td class="text-center">{alamat_rs1}</td>
		
		<td class="text-left text-top">No. Transaksi</td>
        <td class="text-top">:</td>
		<td class="text-top"> {nopermintaan}</td>
		<td class="text-left  text-top">Unit Pelayanan Diminta</td>
        <td class="text-top">:</td>
		<td colspan="2"  class=" text-top"> {unit_ke}</td>
	  </tr>
	  <tr>
	    <td class="text-center"><?=$telepon_rs?></td>
        <td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	  </tr>
	  <tr>
	    <td width="28%" class="text-center"><?//=$fax_rs?></td>
		<td width="20%"></td>
		<td width="1%"></td>
		<td width="24%"></td>
		<td width="20%"></td>
		<td width="1%"></td>
		<td width="13%"></td>
		<td width="13%"></td>
	  </tr>
    </table>
    
	<br>
    <table class="detail">
      <tr>
        <td width="4%" class="border-bottom-top-left text-center">NO</td>
        <td width="8%" class="border-bottom-top text-center">KODE BARANG </td>
        <td width="30%" class="border-bottom-top text-center">NAMA BARANG </td>
        <td width="8%" class="border-bottom-top text-center">SATUAN</td>
        <td width="10%" class="border-bottom-top text-center">QTY</td>
        <td width="10%" class="border-bottom-top text-center">STOK</td>
        <td width="10%" class="border-bottom-top-right text-center">STOK UNIT</td>
      </tr>
      <?php $number = 0; $total_bayar=0;?>
      <?php foreach ($detail_permintaan as $row){ ?>
        <?php 
		$number = $number + 1; 
		// $total_bayar = $total_bayar + $row->nominal_jalan; 
		if ($row->st_update=='1'){
			$q="SELECT M.stok as sisa_stok_unit FROM mgudang_stok M WHERE M.idunitpelayanan='".$row->iddari."' AND M.idtipe='".$row->idtipe."' AND M.idbarang='".$row->idbarang."'";
			$sisa_stok_unit=$this->db->query($q)->row('sisa_stok_unit');
			$q="UPDATE tunit_permintaan_detail SET sisa_stok_unit=".$sisa_stok." WHERE id='".$row->id."'";
			$this->db->query($q);
		}else{
			$sisa_stok_unit=$row->sisa_stok_unit;
		}
		?>
        <tr>
          <td class="border-bottom-left text-right"> <?=$number?>&nbsp;&nbsp;</td>
          <td class="border-bottom text-center"><?=strtoupper($row->kode)?></td>
		  <td class="border-bottom text-left"><?=strtoupper($row->namabarang)?></td>
		  <td class="border-bottom text-center"><?=($row->satuan)?strtoupper($row->satuan):'PCS'?></td>
		  <td class="border-bottom text-center"><?=$row->kuantitas?></td>
		  <td class="border-bottom text-center"><?=number_format($row->sisa_stok,0)?></td>
		  <td class="border-bottom-right text-center"><?=number_format($sisa_stok_unit,0)?></td>
        </tr>
      <? } ?>
     
    </table><br>	
    <table>
		<tr>
			<td width="30%" class="text-center">Paraf Petugas Peminta <p>&nbsp;</p><p>&nbsp;</p></td>
			<td width="70%"></td>
		</tr>
		<tr><td width="30%" class="text-center"></td><td width="70%"></td></tr>
		<tr><td width="30%" class="text-center"></td><td width="70%"></td></tr>
		<tr><td width="30%" class="text-center"></td><td width="70%"></td></tr>
		<tr><td width="30%" class="text-center"></td><td width="70%"></td></tr>
		<tr><td width="30%" class="text-center"></td><td width="70%"></td></tr>
		<tr>
			<td width="30%" class="text-center">({namauser})</td>
			<td width="70%"></td>
		</tr>
	</table>
  </body>
</html>
