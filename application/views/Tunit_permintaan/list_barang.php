<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
            <div class="block-header bg-success">
                <h3 class="block-title">{title}</h3>
            </div>
            <div class="block-content">
                <table class="table table-bordered" id="list_barang">
                    <thead>
                        <th>Tipe</th>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-default" id="btnback" type="button" data-dismiss="modal">Back</button>
        </div>
    </div>
    <script type="text/javascript">
        var table, tr;
        $(document).ready(function(){

            table = $('#list_barang').DataTable({
                destroy: true,
                serverSide: true,
                searching: true,
                sort: false,
                ajax: {
                    url: '{site_url}tunit_permintaan/dtbarang',
                    method: 'post',
                    dataType: 'json',
                    data: {idtipe: $('select[name=idtipe]').val() }
                },
                columns: [
                    {data: 'tipe'},
                    {data: 'kode'},
                    {data: 'nama'},
                    {defaultContent: '<a href="#" class="btn btn-xs btn-default">Pilih Barang</a>', searchable: false, },
                    {data: 'id', visible: false},
                ]
            })
            $('div.dataTables_filter input').focus()
        })
        $('#list_barang tbody').on('click', 'a', function(){
            tr = table.row( $(this).parents('tr') ).index()
            remove_modal({
                modal_id: '#list_barang',
                content_id: '#modal-content-2'
            })
            $('#modal-1').modal('show')
            $('input[name=namabarang]').val( table.cell(tr,2).data() )
            $('input[name=kodebarang]').val( table.cell(tr,1).data() )
            $('input[name=idbarang]').val( table.cell(tr,4).data() )
            $('input[name=stokunit]').val( 0 )
            ajaxSelect({
                url: '{site_url}tunit_permintaan/list_unitdari',
                id: 'select[name=dariunit]'
            })
            $('select[name=dariunit]').focus()
        })

        function ajaxSelect(data) {
            if(data) {
                $(data.id).empty()
                $(data.id).select2({
                    placeholder: 'Cari Data ... ',
                    ajax: {
                        url: data.url,
                        dataType: 'json',
                        delay: 250,
                        processResults: function (data) {
                            return {results:data}
                        },
                        cache: false,
                    }
                })
            }
        }
    </script>    
</div>