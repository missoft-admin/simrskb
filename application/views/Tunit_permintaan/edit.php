<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>
<div id="cover-spin"></div>
<form action="{site_url}tunit_permintaan/save_edit/<?php echo $this->uri->segment(3) ?>" method="post" class="form-horizontal">
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">{title}</h3>
        </div>
        <div class="block-content">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered info_permintaan">
                        <tr>
							<input type="hidden" id="idpermit" name="idpermit" value="<?php echo $this->uri->segment(3) ?>">
                            <td>No.Transaksi</td>
                            <td><?php echo $head_permintaan['nopermintaan'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td><?php echo $head_permintaan['datetime'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Yang Meminta</td>
                            <td><?php echo $head_permintaan['requester'] ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered info_permintaan">
                        <tr>
                            <td width="40%">Dari Unit</td>
                            <td id="penerima_text"><?php echo $head_permintaan['penerima'] ?>
							<input type="hidden" id="dari_unit" name="dari_unit" value="<?=$head_permintaan['idunitpenerima']?>">
                            </td>
                            <td hidden id="idpeminta"><?php echo $head_permintaan['idunitpenerima'] ?>
							
                            </td>
                        </tr>
                        <tr>
                            <td width="40%">Ke Unit</td>
                            <td><?php echo $head_permintaan['peminta'] ?>
							<input type="hidden" id="ke_unit" name="ke_unit" value="<?=$head_permintaan['idunitpeminta']?>">
                            </td>
                            <td hidden id="idpenerima"><?php echo $head_permintaan['idunitpeminta'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%">Total Permintaan</td>
                            <td><?php echo $head_permintaan['totalbarang'] ?>
                            </td>
                        </tr>
						<tr hidden>
                            <td width="40%">Total Permintaan</td>
                            <td><div class="form-group">
                            <label class="control-label col-md-3">Tipe Barang</label>
							<div class="col-md-6">
							<select class="js-select2 form-control" id="idtipe_available" name="idtipe_available" style="width:100%" multiple>
								<?foreach ($get_list_tipe as $row){?>
									<option value="<?=$row->id?>" selected><?=$row->text?></option>
								<?}?>
							</select>
							<input type="text" id="status_edit" name="status_edit" value="1">
							</div>
                        </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
			<div class="form-group text-left">
                <div class="col-md-12">
                    <button class="btn btn-success btn-sm" id="btn_add_modal" type="button">Add</button>
                </div>
            </div>
            <table class="table table-bordered" id="datatable">
                <thead>
                    <tr>
                        <th width="5%">Tipe</th>
                        <th width="20%">Nama</th>
                        <th width="10%">Pesan</th>
                        <th width="5%">Stok Tersedia</th>
                        <th width="5%">Sudah Kirim</th>
                        <th width="5%">Sisa Belum Kirim</th>
                        <th width="5%">Dialihkan</th>
                        <th width="5%">Stok Unit</th>
                        <th width="10%">Aksi</th>
                        <th hidden>idunitpeminta</th>
                        <th hidden>idunitpnerima</th>
                        <th hidden>idtipe</th>
                        <th hidden>idbarang</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($detail_permintaan as $r): ?>
                    <tr>
                        <td><?php echo $r->nama_tipe ?></td>
                        <td><?php echo $r->namabarang ?></td>
                        <td><?php echo $r->kuantitas ?></td>
                        <td><?php echo number_format($r->stok_ke,0) ?></td>
                        <td><?php echo $r->kuantitaskirim ?></td>
                        <td><?php echo $r->kuantitassisa ?></td>
                        <td><?php echo $r->kuantitasalih ?></td>
                        <td><?php echo number_format($r->stok_dari,0) ?></td>
                        <td>
							<? if($r->kuantitaskirim > 0 ){ ?>
								<label class="label label-danger">Tidak Bisa diedit</label>
							<?}else{?>
							<button class="btn btn-xs btn-success pemesanan"  type="button" title="Edit Barang"><i class="fa fa-pencil"></i> Edit</button>
							<?}?>
						</td>
                        <td hidden><?php echo $r->idunitpeminta ?></td> <!-- 9 -->
                        <td hidden><?php echo $r->idunitpenerima ?></td><!-- 10 -->
                        <td hidden><?php echo $r->idtipe ?></td>
                        <td hidden><?php echo $r->idbarang ?></td><!-- 12 -->
                        <td hidden><?php echo $r->id ?></td><!-- 13 -->
                        <td hidden><input type="text" name="idbarang[]" value="<?=$r->idbarang?>"></td><!-- 14 -->
                        <td hidden><input type="text" name="id_det[]" value="<?=$r->id?>"></td><!-- 15 -->
                        <td hidden><input type="text" name="qty[]" value="<?=$r->kuantitassisa?>"></td><!-- 16 -->
                        <td hidden><input type="text" name="st_edit[]" value="0"></td><!-- 17 -->
                        <td hidden><input type="text" name="nama_barang[]" value="<?php echo $this->model->get_namabarang($r->idtipe,$r->idbarang)->nama ?>"></td>
                        <td hidden><input type="text" name="st_add[]" value="0"></td><!-- 19 -->
                        <td hidden><input type="text" name="e_idtipe[]" value="<?=$r->idtipe?>"></td><!-- 20 -->
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
            <div class="form-group text-right">
                <div class="col-md-12">
                    <a href="{site_url}tunit_permintaan" class="btn btn-default" type="reset">Kembali</a>
                    <button class="btn btn-success" id="btn_simpan" type="submit">Submit</button>
                </div>
            </div>



        </div>
    </div>
</form>
<form action="javascript:(0)" method="post" class="form-horizontal" id="form4">
    <div class="modal in" id="modal_add" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">Add Barang</h3>
                    </div>
                    <div class="block-content">
                        <div class="form-group">
                            <label class="control-label col-md-3">Tipe Barang</label>
							<div class="col-md-6">
							<select class="js-select2 form-control" id="idtipe_baru" name="idtipe_baru" style="width:100%" multiple>
								<?foreach ($get_list_tipe as $row){?>
									<option value="<?=$row->id?>" selected><?=$row->text?></option>
								<?}?>
							</select>
							</div>
                        </div>
                        
						<div class="form-group">
							 <label class="control-label col-md-3">Nama Barang Baru</label>
							<div class="col-md-6">
								<div class="input-group">
									<select name="idbarang_baru" id="idbarang_baru" data-placeholder="Cari Barang" class="form-control" style="width:100%"></select>
									<div class="input-group-btn">
										<button class='btn btn-info modal_list_barang_baru' data-toggle="modal" data-target="#modal_list_barang_baru" type='button' id='btn_cari_baru'>
											<i class="fa fa-search"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
                       <div class="form-group">
                            <label class="control-label col-md-3">Stok</label>
                            <div class="col-md-6">
                                <input type="text" readonly class="form-control angka" id="stok_baru" name="stok_baru" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Kuantitas</label>
                            <div class="col-md-3">
                                <input type="text" class="form-control angka" id="kuantitas_pemesanan_baru" name="kuantitas_pemesanan_baru" required>
                            </div>
                                <input type="hidden" class="form-control" id="t_idtipe" name="t_idtipe" >
                                <input type="hidden" class="form-control" id="t_namatipe" name="t_namatipe" >
                                <input type="hidden" class="form-control" id="stok_dari" name="stok_dari" >
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-sm btn-success" id="btn_add">Add</button>
                </div>
            </div>
        </div>
    </div>
</form>
<form action="javascript:(0)" method="post" class="form-horizontal" id="form3">
    <div class="modal in" id="modal_edit" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">Edit Barang</h3>
                    </div>
                    <div class="block-content">
                        <input type="hidden" id="rowIndex" value="">
                        <input type="hidden" id="idpermintaan" name="idpermintaan" value="<?php echo $this->uri->segment(3) ?>">
                        <input type="hidden" id="idtipe_pemesanan" name="idtipe_pemesanan">
                        <input type="hidden" id="idbarang_pemesanan" name="idbarang_pemesanan">
                        <input type="hidden" id="xkuantitas" name="xkuantitas">
                        <div class="form-group">
                            <label class="control-label col-md-3">Tipe Barang</label>
							<div class="col-md-6">
							<select class="js-select2 form-control" id="idtipe_baru_edit" name="idtipe_baru_edit" style="width:100%" multiple>
								<?foreach ($get_list_tipe as $row){?>
									<option value="<?=$row->id?>" selected><?=$row->text?></option>
								<?}?>
							</select>
							</div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3">Tipe Barang</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="tipebarang_pemesanan" id="tipebarang_pemesanan" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Barang</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="namabarang_pemesanan" readonly>
                            </div>
                        </div>
						<div class="form-group">
							 <label class="control-label col-md-3">Nama Barang Baru</label>
							<div class="col-md-6">
								<div class="input-group">
									<select name="idbarang" id="idbarang" data-placeholder="Cari Barang" class="form-control" style="width:100%"></select>
									<div class="input-group-btn">
										<button class='btn btn-info modal_list_barang' data-toggle="modal" data-target="#modal_list_barang" type='button' id='btn_cari'>
											<i class="fa fa-search"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
                       <div class="form-group">
                            <label class="control-label col-md-3">Stok</label>
                            <div class="col-md-6">
                                <input type="text" readonly class="form-control angka" id="stok" name="stok" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Kuantitas</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control angka" id="kuantitas_pemesanan" name="kuantitas_pemesanan" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-sm btn-success" id="btn_update">Update</button>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="modal in" id="modal_list_obat" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
            <div class="block-header bg-success">
                <h3 class="block-title">Pencarian Barang</h3>
            </div>
            <div class="block-content">
                <table class="table table-bordered" id="datatable-1">
                    <thead>
                        <th>ID</th>
                        <th>TIPE</th>
                        <th>KODE</th>
                        <th>NAMA</th>
                        <th>STOK</th>
                        <th>PILIH</th>
                        <th hidden>id</th>
                        <th hidden>id</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#btn_add_modal").click(function() {
            $("#modal_add").modal("show");
            $("#status_edit").val(0);
            // $("#distributor_baru").modal("show");
        });
		$("#btn_newdistributor").click(function() {
            $("#modal_edit").modal("hide");
            $("#distributor_baru").modal("show");
        });
		$("#btn_cari").click(function() {
            // $("#modal_edit").modal("hide");
            $("#modal_list_obat").modal("show");
			
			$("#status_edit").val('1');
			// var idbarang=$("#idbarang").val();
            load_barang();
        });
		$("#btn_cari_baru").click(function() {
            // $("#modal_edit").modal("hide");
            $("#modal_list_obat").modal("show");
			var ke_unit=$("#ke_unit").val();
			var tipe_id=$("#idtipe_pemesanan").val();
			$("#status_edit").val('0');
			// var idbarang=$("#idbarang").val();
           
			load_barang();
        });
		$("#btn_update").click(function() {
			$("#modal_edit").modal("hide");
			// alert($("#idtipe_pemesanan").val());
			$('#datatable tbody tr').each(function() {
				var tr = $(this).closest('tr');
				// tr[0].sectionRowIndex
				// alert(tr[0].sectionRowIndex);

				var $cells = $(this).children('td');
				if (tr[0].sectionRowIndex==$("#rowIndex").val()){
					tr.find("td:eq(14) input").val($("#idbarang").val());
					tr.find("td:eq(2)").text($("#kuantitas_pemesanan").val());
					tr.find("td:eq(3)").text($("#stok").val());
					tr.find("td:eq(1)").text($("#idbarang option:selected").text());
					tr.find("td:eq(16) input").val($("#kuantitas_pemesanan").val());
					tr.find("td:eq(17) input").val('1');
					tr.find("td:eq(18) input").val($("#idbarang option:selected").text());
					tr.find("td:eq(20) input").val($("#idtipe_pemesanan").val());
				}
			});
           // alert('update');
        });
		$("#idbarang_baru").select2({
			minimumInputLength: 2,
			noResults: 'Obat Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}tunit_permintaan/selectbarang/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,

				data: function(params) {
					var query = {
						search: params.term,
						idtipe: $("#idtipe_baru").val(),
						idunitpelayanan: $("#dari_unit").val(),
						idunitke: $("#ke_unit").val(),
					}
					return query;
				},
				processResults: function(data) {
					return {
						results: $.map(data, function(item) {
							return {
								text: item.nama+' ['+item.namatipe+']',
								id: item.id,
								idtipe: item.idtipe,
								namatipe: item.namatipe
							}
						})
					};
				}
			}
		});
		$("#idbarang").select2({
			minimumInputLength: 2,
			noResults: 'Obat Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}tunit_permintaan/selectbarang/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,

				data: function(params) {
					var query = {
						search: params.term,
						idtipe: $("#idtipe_baru_edit").val(),
						idunitpelayanan: $("#dari_unit").val(),
						idunitke: $("#ke_unit").val(),
					}
					return query;
				},
				processResults: function(data) {
					return {
						results: $.map(data, function(item) {
							return {
								text: item.nama+' ['+item.namatipe+']',
								id: item.id,
								idtipe: item.idtipe,
								namatipe: item.namatipe
							}
						})
					};
				}
			}
		});

    });
	function load_barang(){
		var ke_unit=$("#ke_unit").val();
		var tipe_id=$("#idtipe_available").val();
			
		 table = $('#datatable-1').DataTable({
                destroy: true,
                serverSide: true,
                searching: true,
                sort: false,
                autoWidth: false,
                ajax: {
                    url: '{site_url}tgudang_pemesanan/view_barang/',
                    type: 'post',
                    data: {idtipe: tipe_id, idunitpelayanan: ke_unit}
                },
                columns: [
                    {data: 'id', searchable: false},
                    {data: 'namatipe'},
                    {data: 'kode'},
                    {data: 'nama'},
                    {data: 'stok'},
                    {defaultContent: '<a href="#" class="btn btn-xs btn-success pilih">Pilih</a>', searchable: false },
                    {data: 'id', visible: false},
					{data: 'idtipe', visible: false},
                ],
				columnDefs: [
					{ visible: false, targets:[0] },
					{ visible: false, targets:[6] },
					{ visible: false, targets:[7] },
				]

            })
            setTimeout(function() {
                $('div.dataTables_filter input').focus()
            }, 500);
	}
	$('#idbarang_baru').on('change', function(){
		var data_obat=$("#idbarang_baru").select2('data')[0];
		console.log(data_obat);
		if (data_obat){
			var idtipe=data_obat.idtipe;
			var namatipe=data_obat.namatipe;
			$("#t_idtipe").val(idtipe);
			$("#t_namatipe").val(namatipe);
			cek_stok();
		}else{
			$("#t_idtipe").val('');
			$("#t_namatipe").val('');
		}
		// alert(idtipe);
       
    })
	function cek_stok(){
		var iddari=$("#dari_unit").val();
		var idke=$("#ke_unit").val();
		var idtipe=$("#t_idtipe").val();
		var idbarang=$("#idbarang_baru").val();
		// alert()
		if (idbarang){
			$.ajax({
				url: '{site_url}tunit_permintaan/ajax_cek_stok2/',
				dataType: "JSON",
				method: "POST",
				data: { idke: idke, idtipe: idtipe, idbarang: idbarang, iddari: iddari},
				success: function(data) {
					var content='';
					console.log(data);
					
					$('#stok_baru').val(data.stok);
					$('#stok_dari').val(data.stok_dari);
					
					$("#kuantitas_pemesanan_baru").focus();
					
				}
			});
		}
	}
	function validate_add() {
        if($('#t_idtipe').val() == '') {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Barang Belum Pilih'});
            return false;
        }
        if($('#idbarang_baru').val() == '') {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Kode Barang tidak boleh kosong'});
            return false;
        }
        if($('#idbarang_baru').val() == null) {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Kode Barang tidak boleh kosong'});
            return false;
        }
        if($('#kuantitas_pemesanan_baru').val() == '') {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Kuantitas Harus diisi'});
            return false;
        }
		if($('#kuantitas_pemesanan_baru').val() == '0') {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Kuantitas Harus diisi'});
            return false;
        }
        
        return true;
    }
	$("#btn_add").click(function() {
			if (!validate_add()) return false;
			var stok_pribadi=$("#stok_dari").val();
			// var duplicate = false;
			var content = "";

			
			var content = "<tr>";
			
			content += "<td>" + $("#t_namatipe").val() + "</td>"; //1
			content += "<td>" + $("#idbarang_baru option:selected").text() + "</td>"; //2
			content += "<td>" + $("#kuantitas_pemesanan_baru").val() + "</td>"; //3
			content += "<td>" + $("#stok_baru").val() + "</td>"; //3
			content += "<td>0</td>"; //3
			content += "<td>" + $("#kuantitas_pemesanan_baru").val() + "</td>"; //3
			content += "<td>0</td>"; //3
			content += "<td>"+stok_pribadi+"</td>"; //3
			content += "<td><button class='btn btn-xs btn-danger hapus_biasa' type='button' title='Hapus'><i class='glyphicon glyphicon-remove'></i> Hapus</button></td>"; //3
			content += "<td hidden>" + $("#stok_baru").val() + "</td>"; <!-- 9 -->
			content += "<td hidden>" + $("#stok_baru").val() + "</td>";<!-- 10 -->
			content += "<td hidden>" + $("#stok_baru").val() + "</td>";
			content += "<td hidden>" + $("#stok_baru").val() + "</td>";<!-- 12 -->
			content += "<td hidden>0</td>";<!-- 13 -->
			content += '<td hidden><input type="text" name="idbarang[]" value="'+ $("#idbarang_baru").val() +'"></td>]';<!-- 14 -->
			content += '<td hidden><input type="text" name="id_det[]" value="'+ $("#t_idtipe").val() +'"></td>';<!-- 15 -->
			content += '<td hidden><input type="text" name="qty[]" value="'+ $("#kuantitas_pemesanan_baru").val() +'"></td>';<!-- 16 -->
			content += '<td hidden><input type="text" name="st_edit[]" value="1"></td>';<!-- 17 -->
			content += '<td hidden><input type="text" name="nama_barang[]" value="'+ $("#idbarang_baru option:selected").text() +'"></td>';
			content += '<td hidden><input type="text" name="st_add[]" value="1"></td>';<!-- 19 -->
			content += '<td hidden><input type="text" name="e_idtipe[]" value="'+ $("#t_idtipe").val() +'"></td>';<!-- 19 -->
			
			content += "</tr>";
			$('#datatable tbody').append(content);			
			$("#modal_add").modal('hide');
			

		});
	$(document).on("click", ".hapus_biasa", function() {

			if (confirm("Hapus Data ?") == true) {

				$(this).closest('td').parent().remove();

				// var index = $(this).closest('tr').find("td:eq(0)").html();
				// var idtabel = $(this).closest('tr').find("td:eq(13)").html();
				// $('#' + idtabel).remove();
				// $('#pos_tabel' + index).remove();
				// $("#rac_nama").focus();
			}
			// grand_total2();
			// clear_detail2();
		});
	$('.angka').keydown(function(event){
		keys = event.keyCode;
		// Untuk: backspace, delete, tab, escape, and enter
		if (event.keyCode == 116 || event.keyCode == 46 || event.keyCode == 188 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
			 // Untuk: Ctrl+A
			(event.keyCode == 65 && event.ctrlKey === true) ||
			 // Untuk: home, end, left, right
			(event.keyCode >= 35 && event.keyCode <= 39) || event.keyCode == 190 || event.keyCode == 110|| event.keyCode == 188) {
				 // melanjutkan untuk memunculkan angka
				return;
		}
		else {
			// jika bukan angka maka tidak terjadi apa-apa
			if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault();
			}
		}

	});

	$('#datatable-1 tbody').on('click', 'a', function(){
		var table = $('#datatable-1').DataTable();
        tr = table.row( $(this).parents('tr') ).index()


        var data = { 
            id: table.cell(tr,0).data(), 
            text: table.cell(tr,3).data()
        };
        var newOption = new Option(data.text, data.id, true, true);
		
		if ($("#status_edit").val()=='1'){//Edit			
			$('#idbarang').append(newOption); 
			$("#idtipe_pemesanan").val(table.cell(tr,7).data());
			$("#idbarang").val(table.cell(tr,6).data());
			$("#namabarang_pemesanan").val(table.cell(tr,3).data());
			$("#tipebarang_pemesanan").val(table.cell(tr,1).data());
			cek_stok_edit();
		}else{
			$('#idbarang_baru').append(newOption); 
			$("#t_idtipe").val(table.cell(tr,7).data());
			$("#idbarang_baru").val(table.cell(tr,6).data());
			$("#t_namatipe").val(table.cell(tr,1).data());
			cek_stok();
		}
        $('#modal_list_obat').modal('hide');
		
        // tr = table.row( $(this).parents('tr') ).index()

        // var data = {
            // id: table.cell(tr,0).data(),
            // text: table.cell(tr,3).data()
        // };
        // var newOption = new Option(data.text, data.id, true, true);
        // $('#idbarang').append(newOption).trigger('change');
    })

    var table, tr;


    $('#datatable tbody').on('click', '.pemesanan', function() {
		// alert('sini');
        var tr = $(this).closest('tr');

		var data = {
            id: tr.find('td:eq(14) input').val(),
            text: tr.find('td:eq(18) input').val()
        };
        var newOption = new Option(data.text, data.id, true, true);
        $('#idbarang').append(newOption);
        $('#idbarang').val(tr.find('td:eq(14) input').val());
		
		// alert(tr.find('td:eq(14) input').val());
		// $('#idbarang').val(tr.find('td:eq(14) input').val()).trigger('change');
        // $('#modal_list_obat').modal('hide');


		// alert(tr[0].sectionRowIndex);
        $('input[name=tipebarang_pemesanan]').val(tr.find('td:eq(0)').text());
        $('input[name=idtipe_pemesanan]').val(tr.find('td:eq(11)').text());
        $('#kuantitas_pemesanan').val(tr.find('td:eq(5)').text());
        $('input[name=namabarang_pemesanan]').val(tr.find('td:eq(1)').text());
        // $('#stok').val(tr.find('td:eq(7)').text());
        $('input[name=idbarang_pemesanan]').val(tr.find('td:eq(13)').text());
		$('#modal_edit').modal('show');
        $("#rowIndex").val(tr[0].sectionRowIndex);
		var n = tr.find('td:eq(11)').text();
        var idunitpelayanan = tr.find('td:eq(9)').text();
		cek_stok_edit();
       $("#ke_unit").val(tr.find('td:eq(9)').text());
		// alert(idunitpelayanan+n);
		// $("#idbarang").select2({
            // minimumInputLength: 0,
            // noResults: 'Barang tidak ditemukan.',
            // allowClear: true,
            // ajax: {
                // url: '{site_url}tunit_permintaan/selectbarang_edit/',
                // dataType: 'json',
                // type: 'post',
                // quietMillis: 50,
                // data: function (params) {
                    // var query = { search: params.term, idtipe: n, idunitpelayanan: idunitpelayanan }
                    // return query;
                // },
                // processResults: function (data) {
                    // return {
                        // results: $.map(data, function (item) {
							// // alert(data);
							// console.log(item);
                            // return { id: item.id, text: item.nama }
                        // })
                    // };
                // }
            // }
        // });

    });
	$("#idbarang").change(function(){	
		var data_obat=$("#idbarang").select2('data')[0];
		// console.log(data_obat);
		if (data_obat){
			var idtipe=data_obat.idtipe;
			var namatipe=data_obat.namatipe;
			$("#idtipe_pemesanan").val(idtipe);
			$("#tipebarang_pemesanan").val(namatipe);
			cek_stok();
		}else{
			$("#idtipe_pemesanan").val('');
			$("#tipebarang_pemesanan").val('');
		}
		cek_stok_edit();
	});
	
	function cek_stok_edit(){
		var ke_unit=$("#ke_unit").val();
		var tipe_id=$("#idtipe_pemesanan").val();
		var idbarang=$("#idbarang").val();
		// alert(tipe_id);
		// alert('kesini');
		$.ajax({
			url: '{site_url}tunit_permintaan/selected_barang/',
			dataType: "JSON",
			method: "POST",
			data : {idtipe: tipe_id, idunitpelayanan: ke_unit, idbarang: idbarang},
			success: function(data) {
				console.log(data);
				$("#stok").val(data);
			}
		});
	}
	$(document).on("keyup",".angka",function(){
		var jml=$(this).closest('tr').find("td:eq(5)").html();
		// alert($(this).val()+' '+jml);
		if (parseFloat($(this).val())>parseFloat(jml)){
			alert('Pengiriman Lebih Besar Dari Sisa');
			$(this).val(jml);
		}
	});
	$(document).on("click","#btn_simpan",function(){
		 $("#btn_simpan").hide();
		$("#cover-spin").show();
	});
</script>
