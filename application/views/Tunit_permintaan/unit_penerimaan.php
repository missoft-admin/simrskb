<div id="cover-spin"></div>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<form action="{site_url}tunit_permintaan/save_penerimaan/<?php echo $this->uri->segment(3) ?>" method="post" class="form-horizontal">
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">{title}</h3>
        </div>
        <div class="block-content">
            <div class="row">
                <div class="col-md-6">
                  <table class="table table-bordered info_permintaan">
                        <tr>
                            <td >No.Transaksi</td>
                            <td><?php echo $head_permintaan['nopermintaan'] ?></td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td><?php echo $head_permintaan['datetime'] ?></td>
                        </tr>
                        <tr>
                            <td>Yang Meminta</td>
                            <td><?php echo $head_permintaan['requester'] ?></td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered info_permintaan">
                        <tr>
                            <td width="40%">Dari Unit</td>
                            <td id="penerima_text"><?php echo $head_permintaan['penerima'] ?></td>
                            <td hidden id="idpeminta"><?php echo $head_permintaan['idunitpenerima'] ?></td>
                        </tr>
                        <tr>
                            <td width="40%">Ke Unit</td>
                            <td><?php echo $head_permintaan['peminta'] ?></td>
                            <td hidden id="idpenerima"><?php echo $head_permintaan['idunitpeminta'] ?></td>
                        </tr>
                        <tr>
                            <td width="40%">Total Permintaan</td>
                            <td><?php echo $head_permintaan['totalbarang'] ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <?php if ($head_permintaan['idunitpeminta'] != 0): ?>
                <div style="margin: 10px 0" class="text-right">
                    <button class="btn btn-warning alihkan" data-toggle="modal" data-target="#modal_alihkan_permintaan" type="button">Alihkan</button>
                </div>
            <?php endif ?>
            <table class="table table-bordered" id="datatable">
                <thead>
                    <tr>
                        <th width="10%">Tipe</th>
                        <th width="10%">Nama</th>
                        <th width="10%">Pesan</th>
                        <th width="10%">Jml Dikirim</th>
                        <th width="10%">Sudah Terima</th>
                        <th width="10%">Belum Diberikan</th>
                        <th width="10%">Dialihkan</th>
                        <th width="10%">Jml Terima</th>
                        <?php if ($head_permintaan['idunitpeminta'] == 0): ?>
                            <th hidden width="10%">Aksi</th>
                        <?php else: ?>
                            <th hidden width="10%">Aksi</th>
                        <?php endif ?>
                        <th hidden>idunitpeminta</th>
                        <th hidden>idunitpnerima</th>
                        <th hidden>idtipe</th>
                        <th hidden>idbarang</th>
                        <th hidden>id</th>
                        <th hidden>hargabarang</th>
                        <th hidden>marginbarang</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($detail_permintaan as $r): ?>
                        <tr>
                            <td><?php echo $r->nama_tipe ?></td>
                            <td><?php echo $r->namabarang ?></td>
                            <td><?php echo $r->kuantitas ?></td>
                            <td><?php echo $r->kirim_sekarang ?></td>
                            <td><?php echo $r->sudah_terima ?></td>
                            <td><?php echo $r->kuantitassisa ?></td>
                            <td><?php echo $r->kuantitasalih ?></td>
                            <td>
                                
							<input type="text" name="kuantitasterima[]" readonly class="form-control" value="<?php echo $r->terima_skrg ?>">
							<input type="hidden" name="id[]"  class="form-control" value="<?php echo $r->id ?>">
							
                            </td>
                            <?php if ($head_permintaan['idunitpeminta'] == 0): ?>
                                <td hidden><button class="btn btn-warning" id="pemesanan" type="button">Ke Distributor</button></td>
                            <?php else: ?>
                                <td hidden>
                                    <?php if ($head_permintaan['idunitpeminta'] == 0): ?>
                                        <button class="btn btn-warning" id="pemesanan" type="button">Ke Distributor</button>
                                    <?php else: ?>
                                        <?php if ($r->status == 1): ?>
                                            <button class="btn btn-warning" type="button">Alihkan</button>
                                        <?php endif ?>
                                    <?php endif ?>
                                </td>
                            <?php endif ?>
                            <td hidden><?php echo $r->idunitpeminta ?></td> <!-- 9 -->
                            <td hidden><?php echo $r->idunitpenerima ?></td>
                            <td hidden><?php echo $r->idtipe ?></td>
                            <td hidden><?php echo $r->idbarang ?></td>
                            <td hidden><?php echo $r->id ?></td>
                            <td hidden><?php echo $r->hargabarang ?></td>
                            <td hidden><?php echo $r->marginbarang ?></td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
            <div class="form-group text-right">
                <div class="col-md-12">
                    <a href="{site_url}tunit_permintaan" class="btn btn-default" type="reset">Kembali</a>
                    <button class="btn btn-success" id="btn_simpan" type="submit">Submit</button>
                </div>
            </div>
			<?if($histori_terima){?>
			<div class="row">
                <div class="col-md-6">
						<div class="block-header">							
							<h3 class="block-title">History Penerimaan</h3>
						</div>
						<div class="block-content">
							<table class="table table-striped table-borderless table-header-bg">
								<thead>
									<tr>
										<th class="text-center" style="width: 50px;">#</th>
										<th class="text-left">Tanggal Kirim</th>
										<th class="text-left">User Kirim</th>
										<th class="text-left">Barang Kirim</th>
										<th class="text-right">Jumlah</th>
										<th class="text-left">Tanggal Terima</th>
										<th class="text-left">user Terima</th>
									</tr>
								</thead>
								<tbody>
								<?
								$no=1;
								foreach($histori_terima as $row){?>
									<tr>
										<td class="text-right"><?=$no?></td>
										<td class="text-left"><?=$row->tanggal_kirim?></td>
										<td class="text-left"><?=$row->user_nama_kirim?></td>
										<td class="text-left"><?=$row->namabarang?></td>
										<td class="text-right"><?=$row->kuantitas_terima?></td>
										<td class="text-left"><?=$row->tanggal_terima?></td>
										<td class="text-left"><?=$row->user_nama_terima?></td>
										
									</tr>
								<?
								$no=$no+1;
								}?>
									
								</tbody>
							</table>
						</div>
                </div>                
            </div>
			<?}?>
        </div>
    </div>
</form>

<form action="javascript:(0)" method="post" class="form-horizontal" id="form2">
    <div class="modal in" id="modal_alihkan_permintaan" role="dialog" aria-hidden="true"> 
        <div class="modal-dialog modal-lg">
            <div class="modal-content"> 
                <div class="block block-themed block-transparent remove-margin-b"> 
                    <div class="block-header bg-success"> 
                        <h3 class="block-title">Alihkan Permintaan</h3> 
                    </div> 
                    <div class="block-content">
                        <input type="hidden" name="idpermintaanhead" value="<?php echo $this->uri->segment(3) ?>">
                        <input type="hidden" name="idpermintaandetail">
                        <input type="hidden" name="idunitpenerima">
                        <input type="hidden" name="detailvalue_alihan" id="detailvalue_alihan">
                        <div class="form-group">
                            <label class="control-label col-md-3">Ke Unit</label>
                            <div class="col-md-6">
                                <select class="form-control" name="idunitpeminta" style="width:100%"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Dari Unit</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="unit_penerima_text" readonly>
                            </div>
                        </div>
                        <table class="table table-bordered" id="datatable_alihan_permintaan">
                            <thead>
                                <tr>
                                    <th width="10%">Tipe</th>
                                    <th width="10%">Nama</th>
                                    <th width="10%">Pesan</th>
                                    <th width="10%">Jumlah yang mau dialihkan?</th>
                                    <th hidden>idunitpeminta</th>
                                    <th hidden>idunitpnerima</th>
                                    <th hidden>idtipe</th>
                                    <th hidden>idbarang</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($detail_permintaan as $r): ?>
                                    <?php $jumlahkirim = ($r->stoktersedia < $r->kuantitassisa) ? $r->stoktersedia : $r->kuantitassisa ?>
                                    <tr>
                                        <td><?php echo $this->model->get_namabarang($r->idtipe,$r->idbarang)->tipe ?></td>
                                        <td><?php echo $this->model->get_namabarang($r->idtipe,$r->idbarang)->nama ?></td>
                                        <td><?php echo $r->kuantitas ?></td>
                                        <td>
                                            <input type="text" name="kuantitaskirim[]" class="form-control kuantitaskirim" value="<?php echo $r->kuantitassisa ?>">
                                            <input type="hidden" name="id[]" class="form-control" value="<?php echo $r->id ?>">
                                        </td>
                                        <td hidden><?php echo $r->idunitpeminta ?></td> <!-- 4 -->
                                        <td hidden><?php echo $r->idunitpenerima ?></td>
                                        <td hidden><?php echo $r->idtipe ?></td>
                                        <td hidden><?php echo $r->idbarang ?></td>
                                        <td hidden><?php echo $r->id ?></td>
                                        <td hidden><?php echo $r->kuantitassisa ?></td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>                    
                    </div> 
                </div> 
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button> 
                    <button class="btn btn-sm btn-success" type="submit">Simpan</button> 
                </div> 
            </div> 
        </div>
    </div>
</form>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
    var table,tr;

    $('#datatable_alihan_permintaan tbody').on('keyup', '.kuantitaskirim', function(){
        var n = $(this).val()
        tr = $(this).closest('tr');
        tr.find('td:eq(9)').text(n)
        detailvalue_alihan()
    })
    $(document).on('click', '.alihkan', function(){

        var penerima_text = '<?php echo $head_permintaan['penerima'] ?>';
        $('input[name=unit_penerima_text]').val(penerima_text);
        $('input[name=idunitpenerima]').val('<?php echo $head_permintaan['idunitpenerima'] ?>');
        var unitid = '<?php echo $head_permintaan['idunitpenerima'] ?>';

        $('select[name=idunitpeminta]').select2({
            placeholder: 'Cari Data ... ',
            allowClear: true,
            ajax: {
                url: '{site_url}tunit_permintaan/selectdariunit_alihan/'+unitid,
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {results:data}
                },
                cache: false,
            }
        })
        detailvalue_alihan()

        setTimeout(function() {
            $('select[name=idunitpeminta]').focus()
        }, 500);
    })

    $('#form2').on('submit', (function(e){
        e.preventDefault();
        var formData = new FormData(this)

        if(validate_pengalihan()) {
            $.ajax({
                method:'POST',
                url: '{site_url}tunit_permintaan/save_pengalihan_permintaan',
                data: formData,
                cache: false,
                dataType: 'json',
                contentType: false,
                processData: false,
                success:function(res){
                    if(res.status == '200') {
                        swal('Sukses','Data berhasil disimpan!','success');
                        $.ajax({
                            url: '{site_url}tunit_permintaan/ajax_cekstatuspermintaan',
                            dataType: 'json',
                            method: 'post',
                            data: {id: '<?php echo $this->uri->segment(3) ?>'},
                            success: function(res) {
                                setTimeout(function() {
                                    if(res == 4) {
                                        window.location = '{site_url}tunit_permintaan';
                                    } else {
                                        window.location = '{site_url}tunit_permintaan/unit_penerimaan/<?php echo $this->uri->segment(3) ?>';
                                    }
                                }, 1000);
                            }
                        })
                    } else {
                        swal('Kesalahan','Data gagal disimpan!','danger')
                        setTimeout(function() {
                            location.reload();
                        }, 1000);
                    }
                }
            })
        }

    }))

    function detailvalue_alihan() {
        var detail_tbl = $('table#datatable_alihan_permintaan tbody tr').get().map(function(row) {
            return $(row).find('td').get().map(function(cell) {
                return $(cell).html();
            });
        });

        $("#detailvalue_alihan").val(JSON.stringify(detail_tbl))
    }

    function validate_pengalihan() {
        if($('select[name=idunitpeminta]').val() == '' || $('select[name=idunitpeminta]').val() == null) {
            alert('Keunit Tidak boleh kosong')
            $('select[name=idunitpeminta]').focus()
            return false;
        }
        return true;
    }
	$(document).on("click","#btn_simpan",function(){
		 $("#btn_simpan").hide();
		$("#cover-spin").show();
	});
</script>