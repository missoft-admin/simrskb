<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <form id="form1" class="form-horizontal" action="javascript:void(0)">
            <input type="hidden" id="harga"/>
            <input type="hidden" id="margin"/>
            <div class="form-group">
                <label class="col-md-2">Dari Unit</label>
                <div class="col-md-4">
                    <select class="js-select2 form-control" id="idunitpelayanan" name="dariunit" style="width:100%">
						<?foreach ($list_unit_pelayanan_user as $row){?>
							<option value="<?=$row->id?>" <?=($row->id==$unitpelayanandef?'selected':'')?>><?=$row->text?></option>
						<?}?>
					</select>
                </div>
				<label class="col-md-1">Tipe</label>
                <div class="col-md-3">
                    <select class="form-control" id="idtipe" name="idtipe" style="width:100%"></select>
                </div>
            </div>
           
            <div class="form-group">
                <label class="col-md-2">Barang</label>
                <div class="col-md-8">
                    <div class="input-group">
                        <select name="idbarang" id="idbarang" data-placeholder="Cari Barang" class="form-control" style="width:100%"></select>
                        <div class="input-group-btn">
                            <button class='btn btn-info modal_list_barang' data-toggle="modal" data-target="#modal_list_barang" type='button' id='caribarang'>
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2">Ke Unit</label>
                <div class="col-md-4">
                    <select class="form-control" name="keunit" id="keunit" style="width:100%"></select>
                </div>
				<div id="div_stok">
				<label class="col-md-2">Stok Unit Yang Diminta</label>
                <div class="col-md-2">
                    <input type="text" class="form-control" name="stokunit" readonly>
                </div>
                </div>
            </div>
            
            <div class="form-group">
				<label class="col-md-2">Yang Meminta</label>
				<div class="col-md-8"> 
					<select name="user" id="user" class="form-control" style="width:100%">
						<?foreach($list_user as $row){?>
							<option value="<?=$row->id?>" selected><strong><?=$row->text?></strong></option>
						<?}?>
					</select>
				</div>
			</div>
            <div class="form-group">
                <label class="col-md-2">Jumlah Permintaan</label>
                <div class="col-md-2">
                    <input type="text" class="form-control angka" name="kuantitas">
                </div>
				<div class="col-md-6 pull-left text-left">
                    <button type="submit" class="btn btn-primary btn-sm" id="tambahkan"><li class="fa fa-plus"></li> Tambahkan</button>
                </div>
            </div>
            
        </form>
        <form id="form2" class="form-horizontal" action="{site_url}tunit_permintaan/save" method="post">
            <table class="table table-bordered" id="datatable-2">
                <thead>
                    <th>Tipe</th>
                    <th>Nama</th>
                    <th>Dari Unit</th>
                    <th>Ke Unit</th>
                    <th>Kuantitas</th>
                    <th>Aksi</th>
                    <th hidden>idbarang</th>
                    <th hidden>idtipe</th>
                    <th hidden>dariunit</th>
                    <th hidden>keunit</th>
                    <th hidden>user</th>
                    <th hidden>namauser</th>
                    <th hidden>stokunit</th>
                    <th hidden>harga</th>
                    <th hidden>margin</th>
                    <th hidden>namabarang</th>
                </thead>
                <tbody></tbody>
            </table>
            <div style="margin-top:50px;margin-bottom:20px;" class="text-right">
                <a href="{site_url}tunit_permintaan" class="btn btn-default btn-md">Kembali</a>
                <button type="submit" class="btn btn-success btn-md" id="btn_simpan">Simpan</button>
            </div>
        </form>
    </div>
</div>

<div class="modal in" id="modal_list_barang" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
            <div class="block-header bg-success">
                <h3 class="block-title">Pencarian Barang</h3>
            </div>
            <div class="block-content">
			<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
				<div class="row">
					<div class="col-md-8">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="idkategori">Kategori Barang</label>
							<div class="col-md-8">
								<select name="idkategori" id="idkategori" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="#" selected>-Pilih Semua-</option>
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="statusstok">Status Stok</label>
							<div class="col-md-8">
								<select name="statusstok" id="statusstok" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="#" selected>-Pilih Semua-</option>
									<option value="1" <?=($statusstok == 1) ? "selected" : "" ?>>Available</option>
									<option value="2" <?=($statusstok == 2) ? "selected" : "" ?>>Back To Order</option>
									<option value="3" <?=($statusstok == 3) ? "selected" : "" ?>>Stok Minimum</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-12">
								<button class="btn btn-success text-uppercase" type="button" id="btn_filter_barang" name="btn_filter_barang" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
							</div>
						</div>
					</div>
				</div>
				<?php echo form_close() ?>
				<div class="block-content">
				<div class="row">
                <table class="table table-bordered" id="datatable-1">
                    <thead>
                        <th>Tipe</th>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Stok</th>
                        <th>Satuan</th>
                        <th>Status</th>
                        <th>Aksi</th>
                        <th hidden>id</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>

<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">

    var table;
	// var tabel = $('#datatable-2').DataTable();
    $(document).ready(function(){
        // $('select[name=dariunit]').select2({
            // placeholder: 'Cari Data ... ',
            // allowClear: true,
            // ajax: {
                // url: '{site_url}tunit_permintaan/selectdariunit',
                // dataType: 'json',
                // delay: 250,
                // processResults: function (data) {
                    // return {results:data}
                // },
                // cache: false,
            // }
        // })

        // var dataoption = $('select[name=dariunit]')
        // $.ajax({
            // url: '{site_url}tunit_permintaan/selectdariunit/{unitpelayanandef}',
            // dataType: 'json',
            // success: function(data) {
                // var option = new Option(data.text, data.id, true, true);
                // dataoption.append(option).trigger('change');
                // dataoption.trigger({ type: 'select2:select', params: {data: data} })
            // }
        // })

        $('select[name=user]').select2();
		$("#idbarang").select2({
			minimumInputLength: 2,
			noResults: 'Obat Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}tunit_permintaan/selectbarang/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,

				data: function(params) {
					var query = {
						search: params.term,
						idtipe: $("#idtipe").val(),
						idunitpelayanan: $("#idunitpelayanan").val(),
					}
					return query;
				},
				processResults: function(data) {
					return {
						results: $.map(data, function(item) {
							return {
								text: item.nama,
								id: item.id,
								idtipe: item.idtipe
							}
						})
					};
				}
			}
		});
    })
	$("#form2").on("click","#btn_simpan",function(){
		 $("#btn_simpan").hide();
		$("#cover-spin").show();
	});
	$('.angka').keydown(function(event){
		keys = event.keyCode;
		// Untuk: backspace, delete, tab, escape, and enter
		if (event.keyCode == 116 || event.keyCode == 46 || event.keyCode == 188 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
			 // Untuk: Ctrl+A
			(event.keyCode == 65 && event.ctrlKey === true) || 
			 // Untuk: home, end, left, right
			(event.keyCode >= 35 && event.keyCode <= 39) || event.keyCode == 190 || event.keyCode == 110|| event.keyCode == 188) {
				 // melanjutkan untuk memunculkan angka
				return;
		}
		else {
			// jika bukan angka maka tidak terjadi apa-apa
			if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
		
	});
	
    $('select[name=dariunit]').on('change', function(){
        $('select[name=idtipe]').val(null).trigger('change');
		$('select[name=keunit]').val(null).trigger('change');
        var val = $(this).val();
        if(val != null) {
            $('select[name=idtipe]').select2({
                placeholder: 'Cari Data ... ',
                allowClear: true,
                ajax: {
                    url: '{site_url}tunit_permintaan/select_tipebarang/' + val,
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {results:data}
                    },
                    cache: false,
                }
            })
			// setTimeout(function() {            
				// $('select[name=idtipe]').focus()
			// }, 100);
        }
    })

    $('select[name=idtipe]').on('change', function(){
		$('select[name=keunit]').val(null).trigger('change');
        var n = $(this).val();
        var idunitpelayanan = $('select[name=dariunit]').val();
        // $("#idbarang").select2({
			// // console.log(params.term);
			// minimumInputLength: 3,
            // minimumInputLength: 0,
            // noResults: 'Barang tidak ditemukan.',          
            // allowClear: true,
            // ajax: {
                // url: '{site_url}tunit_permintaan/selectbarang/', 
                // dataType: 'json', 
                // type: 'post', 
                // quietMillis: 50, 
                // data: function (params) {
                    // var query = { search: params.term, idtipe: n, idunitpelayanan: idunitpelayanan } 
                    // return query; 
                // }, 
                // processResults: function (data) {
                    // return {
                        // results: $.map(data, function (item) {
                            // return { id: item.id, text: item.nama }
                        // })
                    // };
                // }
            // }
        // }); 
		// alert(idunitpelayanan);
		if(idunitpelayanan != null && n != null) {
			// alert('sini : '+idunitpelayanan+' tipe :'+n);
			 $('select[name=keunit]').select2({
				placeholder: 'Cari Data ... ',
				allowClear: true,
				ajax: {
					url: '{site_url}tunit_permintaan/selectkeunit_tipe/'+idunitpelayanan+'/'+n,
					dataType: 'json',
					delay: 250,
					processResults: function (data) {
						// alert(data);
						return {results:data}
					},
					cache: false,
				}
			});
		}
        // setTimeout(function() {
            // $('select[name=idbarang]').val('').trigger('change');
            // $('select[name=idbarang]').focus()
        // }, 100);
    })

    // $('.modal_list_barang').on('click', function(){
        // $('select[name=idbarang]').val('').trigger('change');
        // var idtipe = $('select[name=idtipe]').val();
        // var idunitpelayanan = $('select[name=dariunit]').val();
        // if(idtipe !== '') {
            // table = $('#datatable-1').DataTable({
                // destroy: true,
                // serverSide: true,
                // searching: true,
                // sort: false,
                // autoWidth: false,
                // ajax: {
                    // url: '{site_url}tunit_permintaan/dtbarang/'+idtipe,
                    // type: 'post',
                    // data: {idunitpelayanan: idunitpelayanan}
                // },
                // columns: [
                    // {data: 'tipe'},
                    // {data: 'kode'},
                    // {data: 'nama'},
                    // {data: 'stok'},
                    // {defaultContent: '<a href="#" class="btn btn-xs btn-default">Pilih Barang</a>', searchable: false, },
                    // {data: 'id', visible: false},
                    // {data: 'hargadasar', visible: false},
                    // {data: 'marginumum', visible: false},
                // ]
            // })
            // setTimeout(function() {
                // $('div.dataTables_filter input').focus()
            // }, 500);
        // }
    // })
	$('.modal_list_barang').on('click', function(){
        $('select[name=idbarang]').val('').trigger('change');
        var idtipe = $('select[name=idtipe]').val();
        var idunitpelayanan = $('select[name=dariunit]').val();
		load_kategori();
        if(idtipe !== '') {
			loadBarang();
            // table = $('#datatable-1').DataTable({
                // destroy: true,
                // serverSide: true,
                // searching: true,
                // sort: false,
                // autoWidth: false,
                // ajax: {
                    // url: '{site_url}tunit_permintaan/getBarang/'+idtipe,
                    // type: 'post',
                    // data: {idunitpelayanan: idunitpelayanan}
                // },
                // columns: [
                    // {data: 'tipe'},
                    // {data: 'kode'},
                    // {data: 'nama'},
                    // {data: 'stok'},
                    // {defaultContent: '<a href="#" class="btn btn-xs btn-default">Pilih Barang</a>', searchable: false, },
                    // {data: 'id', visible: false},
                    // {data: 'hargadasar', visible: false},
                    // {data: 'marginumum', visible: false},
                // ]
            // })
            // setTimeout(function() {
                // $('div.dataTables_filter input').focus()
            // }, 500);
        }
    })
	$(document).on("click", "#btn_filter_barang", function() {
		
			loadBarang();
		
	});
	function loadBarang() {
		var idunit=$("#idunitpelayanan").val();
		var idtipe=$("#idtipe").val();
		var idkategori=$("#idkategori").val();
		var statusstok=$("#statusstok").val();
		$('#datatable-1').DataTable().destroy();
		var table = $('#datatable-1').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}tunit_permintaan/getBarang/',
			type: "POST",
			dataType: 'json',
			data: {
				idunit: idunit,
				idtipe: idtipe,
				idkategori: idkategori,
				statusstok: statusstok,
			}
		},
		columnDefs: [{ targets: [7], visible: false}]
		});
	}
    $('#datatable-1 tbody').on('click', 'a', function(){
		var table = $('#datatable-1').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		// alert(table.cell(tr,7).data());
        var data = { 
            id: table.cell(tr,7).data(), 
            text: remove_class(table.cell(tr,2).data()),
            harga: table.cell(tr,7).data(),
            margin: table.cell(tr,7).data(),
        };
		
        var newOption = new Option(remove_class(data.text), data.id, true, true);
        $('select[name=idbarang]').append(newOption).trigger('change');
        $('#margin').val(data.margin);
        $('#harga').val(data.harga);

        $('#modal_list_barang').modal('hide')
    })
    $('select[name=idbarang]').on('change', function(){
        var idunitpeminta = $('select[name=dariunit]').val();
        var idtipe = $('select[name=idtipe]').val();
        var idbarang = $('select[name=idbarang]').val();
        // ke unit
		$("#keunit").val(null).trigger('change');
		if(idunitpeminta != null && idtipe != null && idbarang != null) {
        $('select[name=keunit]').select2({
            placeholder: 'Cari Data ... ',
            allowClear: true,
            ajax: {
                url: '{site_url}tunit_permintaan/selectkeunit/'+idunitpeminta+'/'+idtipe+'/'+idbarang,
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {results:data}
                },
                cache: false,
            }
        })
		}
    })
    $('select[name=keunit]').on('change', function(){
        var n = $(this).val();
        var idtipe = $('select[name=idtipe]').val();
        var idbarang = $('select[name=idbarang]').val();
        var dariunit = $('select[name=dariunit]').val();
		if (n !='00'){
			setTimeout(function() {
				$.ajax({
					url: '{site_url}tunit_permintaan/ajax_stokunit/',
					method: 'GET',
					dataType: 'json',
					data: { idunit: n, idtipe: idtipe, idbarang: idbarang, dari: dariunit},
					success: function(res) {
						$('input[name=stokunit]').val(res.stok);
						if (res.st_tampil=='1'){
							$("#div_stok").show();
						}else{
							$("#div_stok").hide();
						}
						
						// alert('Tampil ;'+res.st_tampil+' Stok : '+res.stok);
					}
				})        
				// $('input[name=kuantitas]').focus();
			}, 100);
		}else{
			alert($("#idtipe option:selected").text()+' Tidak Tersedia');
			$('select[name=keunit]').val('').trigger('change');
			$('input[name=stokunit]').val('');
		}
    })
	function remove_class($str){
		var str=$str;
		var res = str.toString().replace('<span class="label label-danger">', '');
			res = res.toString().replace('</span>', '');
			res = res.toString().replace('RO', '');
			res = res.toString().replace('new', '');
			res = res.toString().replace('<span class="label label-success">', '');
		return res;
	}
    $('#form1').on('submit', (function(e){
        e.preventDefault();
        if(validate()) {
            table = $('#datatable-2').DataTable({
                autoWidth: false,
                info: false,
                sort: false,
                paging: false,
                searching: false,
                destroy: true,
            }).columns([6,7,8,9,10,11,12,13,14,15]).visible(false);
            
            var row1=$('select[name=idtipe] :selected').text();

            var row6=$('select[name=idbarang]').val();
            var row6q=$('input[name=kuantitas]').val();
            row1+="<input type='hidden' name='d_idbarang[]' value='"+row6+"'/>";
            row1+="<input type='hidden' name='d_kuantitas[]' value='"+row6q+"'/>";
            
            var row7=$('select[name=idtipe]').val();
            row1+="<input type='hidden' name='d_idtipe[]' value='"+row7+"'/>";
            
            var row8=$('select[name=dariunit]').val();
            row1+="<input type='hidden' name='d_dariunit[]' value='"+row8+"'/>";
            
            var row9=$('select[name=keunit]').val();
            var row9t=$('select[name=keunit]').text();
            row1+="<input type='hidden' name='d_keunit[]' value='"+row9+"'/>";
            row1+="<input type='hidden' name='d_namakeunit[]' value='"+row9t+"'/>";
            
            var row10=$('select[name=user]').val();
            row1+="<input type='hidden' name='d_iduser[]' value='"+row10+"'/>";
            
            var row11=$('select[name=user] option:selected').text();
            row1+="<input type='hidden' name='d_namauser[]' value='"+row11+"'/>";
            
            var row12=$("input[name=stokunit]").val();
            row1+="<input type='hidden' name='d_stokunit[]' value='"+row12+"'/>";
            
            var row13=$("#harga").val();
            row1+="<input type='hidden' name='d_harga[]' value='"+row13+"'/>";
           
            var row14=$("#margin").val();
            row1+="<input type='hidden' name='d_margin[]' value='"+row14+"'/>";
            
            var row15=$('select[name=idbarang] option:selected').text();
            row1+="<input type='hidden' name='d_namabarang[]' value='"+row15+"'/>";
			// alert(row1);
            table.row.add([
                row1,
                $('select[name=idbarang] :selected').text(),
                $('select[name=dariunit] :selected').text(),
                $('select[name=keunit] :selected').text(),
                $('input[name=kuantitas]').val(),
                '<button type="button" class="btn btn-sm btn-danger hapus" title="Hapus"><i class="glyphicon glyphicon-remove"></i></button>',
                row6,row7,row8,row9,row10,row11,row12,row13,row14,row15
            ]).draw();
            clearform();
            setTimeout(function() {
                $('select[name=idbarang]').focus();
            }, 100);
        }        
    }))
	// $(document).on("click",".hapus",function(){
		// if(confirm("Hapus Data ?") == true){
			// // var table1 = $('datatable-2').DataTable();
			// // var row = table1.row( $(this).parents('tr') );
			// // // var rowNode = row.node();
			// // row.remove();
			// // // $(this).closest('td').parent().remove();
			// var table = $('#datatable-2').DataTable();
				// var rows = table
					// .rows(this)
					// .remove()
					// .draw();
		// }

	// });
	$('#datatable-2 tbody').on( 'click', '.hapus', function () {
		if(confirm("Hapus Data ?") == true){
			var table = $('#datatable-2').DataTable(); 
				var rows = table
					.row($(this).parents('tr'))
					.remove()
					.draw();
		 }
	 
	} );
    function validate() {
        if($('select[name=idtipe]').val() == 0) {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Tipe tidak boleh kosong'});
            return false;
        }
        if($('input[name=idbarang]').val() == '') {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Kode Barang tidak boleh kosong'});
            return false;
        }
        if($('select[name=dariunit]').val() == null ) {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Dari Unit tidak boleh kosong'});
            return false;
        }
        if($('select[name=keunit]').val() == null) {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Ke unit tidak boleh kosong'});
            return false;
        }
        if($('input[name=kuantitas]').val() == '' || $('input[name=kuantitas]').val() < 0) {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Kuantitas tidak boleh kosong'});
            return false;
        }
		// alert(parseFloat($('input[name=stokunit]').val()));
        if(parseFloat($('input[name=kuantitas]').val()) > parseFloat($('input[name=stokunit]').val())) {
            if($('select[name=keunit]').val() == 0) {
                return true;
            } else if($('select[name=keunit]').val() == 49) {
                return true;
            } else {
                $.toaster({priority : 'danger', title : 'Error!', message : 'Kuantitas tidak boleh lebih besar dari stok unit'});
                // $('input[name=kuantitas]').val('').focus()
                return false;
            }
        }
        return true;
    }
	function load_kategori(){
		var idtipe=$("#idtipe").val();
		$('#idkategori').find('option').remove();
		$('#idkategori').append('<option value="#" selected>- Pilih Semua -</option>');
		if (idtipe !='#'){
						
				$.ajax({
					url: '{site_url}tstockopname/list_kategori/'+idtipe,
					dataType: "json",
					success: function(data) {
						$.each(data, function (i,row) {
							
						$('#idkategori')
						.append('<option value="' + row.path + '">' + TreeView(row.level, row.nama) + '</option>');
						});
					}
				});

		}
	}
	function TreeView($level, $name)
	{
		// console.log($name);
		$indent = '';
		for ($i = 0; $i < $level; $i++) {
			if ($i > 0) {
				$indent += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			} else {
				$indent += '';
			}
		}

		if ($i > 0) {
			$indent += '└─';
		} else {
			$indent += '';
		}

		return $indent + ' ' +$name;
	}
    function clearform() {
        $('select[name=idbarang]').val('').trigger('change');
        // $('select[name=dariunit], select[name=keunit]').empty();
        $('input[name=kodebarang], input[name=namabarang], input[name=idbarang], input[name=stokunit], input[name=kuantitas]').val('');
        $("#harga").val('');
        $("#margin").val('');
    }
</script>