<?php echo ErrorSuccess($this->session)?> 
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>

<form action="{site_url}tunit_permintaan/save_acc_pengiriman/<?php echo $this->uri->segment(3) ?>" method="post" class="form-horizontal">
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">{title}</h3>
        </div>
        <div class="block-content">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered info_permintaan">
                        <tr>
                            <td>No.Transaksi</td>
                            <td><?php echo $head_permintaan['nopermintaan'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Tanggal Permintaan</td>
                            <td><?php echo $head_permintaan['datetime'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Yang Meminta</td>
                            <td><?php echo $head_permintaan['requester'] ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered info_permintaan">
                        <tr>
                            <td width="40%">Dari Unit</td>
                            <td id="penerima_text"><?php echo $head_permintaan['penerima'] ?>
                            </td>
                            <td hidden id="idpeminta"><?php echo $head_permintaan['idunitpenerima'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%">Ke Unit</td>
                            <td><?php echo $head_permintaan['peminta'] ?>
                            </td>
                            <td hidden id="idpenerima"><?php echo $head_permintaan['idunitpeminta'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%">Total Permintaan</td>
                            <td><?php echo $head_permintaan['totalbarang'] ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
           
			<div class="row">
				<?if($list_cetak_pengiriman){?>
                <div class="col-md-12">
						
						<div class="block-content">
							<table class="table table-striped table-borderless table-header-bg">
								<thead>
									<tr>
										<th class="text-center" style="width: 50px;">#</th>
										<th class="text-left">Tanggal Kirim</th>
										<th class="text-left">User Kirim</th>
										<th class="text-left">Barang</th>
										<th class="text-right">Jumlah</th>
										<th class="text-center">Cetak</th>
									</tr>
								</thead>
								<tbody>
								<?
								$no=1;
								foreach($list_cetak_pengiriman as $row){?>
									<tr>
										<td class="text-right"><?=$no?></td>
										<td class="text-left"><?=$row->tanggal_kirim?></td>
										<td class="text-left"><?=$row->user_nama_kirim?></td>
										<td class="text-left"><?=$row->namabarang?></td>
										<td class="text-right"><?=$row->kuantitas?></td>
										<td class="text-center">
											<div class="btn-group" role="group">
												<button class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
													<span class="fa fa-print"></span>
												</button>
												<ul class="dropdown-menu dropdown-menu-right">
													<li class="dropdown-header">Pengiriman</li>
													<li>
														<a tabindex="-1" href="<?=$base_url.'tunit_permintaan/print_pengiriman/'.$row->id.'/'.replace_string($row->tanggal_kirim,' ','_').'/1'?>" target="blank">Bukti Pengiriman</a>
													</li>
													<li>
														<a tabindex="-1" href="<?=$base_url.'tunit_permintaan/print_pengiriman/'.$row->id.'/'.replace_string($row->tanggal_kirim,' ','_').'/2'?>"  target="blank">Bukti Pengiriman Thermal</a>
													</li>
												</ul>
											</div>	
										</td>

									</tr>
								<?
								$no=$no+1;
								}?>

								</tbody>
							</table>
						</div>
                </div>
				<?}else{?>
					 <div class="col-md-12">
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<h3 class="font-w300 push-15">Informasi</h3>
							<p>Belum ada Proses Pengiriman!</p>
						</div>
					</div>
				<?}?>
            </div>
			
        </div>
    </div>
</form>



<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
