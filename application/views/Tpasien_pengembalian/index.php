<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<script type="text/javascript">
	$(document).ready(function(){
<?php
if(!empty($_SESSION['status'])){
	echo $_SESSION['status'];
$_SESSION['status']='';
}else{
$_SESSION['status']='';
	}
	?>
})
</script>
<?php if (UserAccesForm($user_acces_form,array('1065'))){?>
<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('1066'))){?>
		<ul class="block-options">
            <li><a href="{base_url}tpasien_pengembalian/create" id="createPengembalian" class="btn"><i class="fa fa-plus"></i></a></li>
        </ul>
		<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<div class="row">
				<?php echo form_open('tpasien_pengembalian/filter','class="form-horizontal" id="form-work"') ?>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Nama Pasien</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="namapasien" name="namapasien" value="{namapasien}">
						</div>
					</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="">Asal Pasien</label>
							<div class="col-md-8">
								<select id="idasalpasien" name="idasalpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="#" selected>Semua Asal Pasien</option>
									<option value="0" <?=($idasalpasien == '0' ? 'selected' : '')?>>Pasien Non Rujukan</option>
									<option value="1" <?=($idasalpasien == '1' ? 'selected' : '')?>>Poliklinik</option>
									<option value="2" <?=($idasalpasien == '2' ? 'selected' : '')?>>Instalasi Gawat Darurat</option>
									<option value="3" <?=($idasalpasien == '3' ? 'selected' : '')?>>Rawat Inap</option>
								</select>
							</div>
						</div>
					<div class="form-group">
		          <label class="col-md-4 control-label" for="example-daterange1">Tanggal</label>
		          <div class="col-md-8">
		            <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
		                <input class="form-control" type="text" id="tanggaldari" name="tanggaldari" placeholder="From" value="{tanggaldari}">
		                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
		                <input class="form-control" type="text" id="tanggalsampai" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
		            </div>
		          </div>
		      </div>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Kelompok Pasien</label>
						<div class="col-md-8">
							<select id="idkelompokpasien" name="idkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#" <?=($idkelompokpasien == '#' ? 'selected="selected"':'')?>>Semua Kelompok Pasien</option>
							<?php foreach (get_all('mpasien_kelompok') as $row){?>
							  <option value="<?=$row->id ?>" <?=($idkelompokpasien == $row->id ? 'selected="selected"':'')?>><?=$row->nama ?></option>
							<?php } ?>
						  </select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Status</label>
						<div class="col-md-8">
							 <select id="status" name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="0">Semua Status</option>
								<option value="1" <?=($status == 1 ? 'selected="selected"':'')?>>Belum Ditindak</option>
								<option value="2" <?=($status == 2 ? 'selected="selected"':'')?>>Sudah Ditindak</option>
								<option value="3" <?=($status == 3 ? 'selected="selected"':'')?>>Sudah Diserahkan</option>
							  </select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for=""></label>
						<div class="col-md-8">
							<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						</div>
					</div>
				</div>
				<?php echo form_close() ?>
			</div>
			<div class="table-responsive">
		<table class="table table-bordered table-striped" id="tabelpengembalian">
			<thead>
				<tr>
                    <th>ID</th>
					<th>Tanggal/Jam</th>
					<th>No Pengembalian</th>
					<th>No Penjualan</th>
					<th>No. Medrec</th>
					<th>Nama Pasien</th>
					<th>Total Barang</th>
                    <th>Total Harga</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>

		</div>
		</div>
	</div>
<?}?>
<script type="text/javascript" src="{ajax}basic-ajax.js"></script>
        <link rel="stylesheet" id="css-main" href="{toastr_css}">
        <script src="{toastr_js}"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
	
<script type="text/javascript">
jQuery(function() {
	BaseTableDatatables.init();
	var namapasien = $("#namapasien").val();
	var idasalpasien = $("#idasalpasien").val();
	var tanggaldari = $("#tanggaldari").val();
	var tanggalsampai = $("#tanggalsampai").val();
	var idkelompokpasien = $("#idkelompokpasien").val();
	var status = $("#status").val();
	// alert(tanggaldari);
		delay(function(){
				$('#tabelpengembalian').DataTable().destroy();
				loadDataPengembalian(namapasien, idasalpasien, tanggaldari, tanggalsampai, idkelompokpasien, status);
		  }, 1000 );
	});
	function loadDataPengembalian(namapasien, idasalpasien, tanggaldari, tanggalsampai, idkelompokpasien, status){
		var uri='<?=$this->uri->segment(2);?>';
		// alert(uri);
		$('#tabelpengembalian').DataTable({
		"autoWidth": false,
		"pageLength": 10,
		"ordering": true,
		"processing": true,
		"serverSide": true,
		"order": [],
		"ajax": {
			url: '{site_url}tpasien_pengembalian/getDataPengembalian/' + uri,
			type: "POST",
			dataType: 'json',
					data:{
						namapasien:namapasien,
						idasalpasien:idasalpasien,
						tanggaldari:tanggaldari,
						tanggalsampai:tanggalsampai,
						idkelompokpasien:idkelompokpasien,
						status:status,
					}
		},
		"columnDefs": [{
				"width": "5%",
				"targets": 0,
				"orderable": true
			},
			{
				"width": "15%",
				"targets": 1,
				"orderable": true
			},
			{
				"width": "10%",
				"targets": 2,
				"orderable": true
			},
			{
				"width": "15%",
				"targets": 3,
				"orderable": true
			},
			{
				"width": "10%",
				"targets": 4,
				"orderable": true
			},
			{
				"width": "10%",
				"targets": 5,
				"orderable": true
			},
			{
				"width": "10%",
				"targets": 6,
				"orderable": true
			},{
				"width": "10%",
				"targets": 7,
				"orderable": false
			},
			{
				"width": "10%",
				"targets": 8,
				"orderable": false
			}
		]
	});
	}
	var delay = (function(){
		  var timer = 0;
		  return function(callback, ms){
		  clearTimeout (timer);
		  timer = setTimeout(callback, ms);
		 };
		})();
</script>