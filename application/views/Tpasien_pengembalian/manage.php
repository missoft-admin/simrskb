<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block block-rounded ">
    <div class="block-header">
        <ul class="block-options">
            <li><a href="{site_url}tpasien_pengembalian" class="btn"><i class="fa fa-reply"></i></a></li>
        </ul>
        <h3 class="block-title">{title}</h3>
        <hr>
    </div>
    <?php echo form_open('tpasien_pengembalian/save','class="form-horizontal push-10-t" id="form1"') ?>
	<div class="block-content">
	<div class="row">
		<div class="col-sm-12">
			<div class="control-group">
                <div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0;">
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label input-sm" for="idpenjualan">TANGGAL</label>
								<div class="col-md-7">
									<input class="js-datepicker form-control btn-xs input-sm" type="text" id="tanggal" name="tanggal" data-date-format="dd-mm-yyyy" value="<?=date('d-m-Y')?>"/>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label input-sm" for="idpenjualan">NO. PENJUALAN</label>
								<div class="col-md-7">
									<div class="input-group">
										<input type="text" readonly class="form-control btn-xs input-sm" id="nopenjualan" name="nopenjualan" value="{nopenjualan}"/>
										<input type="hidden" class="form-control btn-xs input-sm" id="idpenjualan" name="idpenjualan"  value="{idpenjualan}"/>
										<?php if ($form_edit=='0'){?>
										<span class="input-group-btn">
											<button type="button" data-toggle="modal" data-target="#modal-data-penjualan"  class="btn btn-info btn-sm"  id="btn_cari_penjualan">CARI NO. PENJUALAN</button>
										</span>
										<?php }?>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
                                <label class="col-md-4 control-label input-sm" for="nomedrec">NO. MEDREC</label>
                                <div class="col-md-7">
                                    <input type="text" readonly class="form-control btn-xs input-sm" id="nomedrec" name="nomedrec"  value="{nomedrec}"/>
                                </div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
                                <label class="col-md-4 control-label input-sm" for="nama_pasien">NAMA PASIEN</label>
                                <div class="col-md-7">
                                    <input type="text" readonly class="form-control btn-xs input-sm" id="nama_pasien" name="nama_pasien"  value="{nama_pasien}"/>
                                </div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label input-sm" for="umur">UMUR</label>
								<div class="col-md-7">
									<input type="text" class="form-control input-sm" id="umur"  value="{umur}" readonly>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-5 control-label input-sm" for="kelompok_pasien">KELOMPOK PASIEN</label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="kelompok_pasien"   value="{kelompok_pasien}" readonly="">
								</div>
							</div>
							<div id="div_kes" style="display: none;">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-5 control-label input-sm" for="kelompok_pasien">BPJS KES</label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="kode_kes"   value="{kode_kes}" readonly="">
								</div>
							</div>
							</div>
							<div id="div_ket" style="display: none;">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-5 control-label input-sm" for="kelompok_pasien">BPJS KET</label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="kode_ket"   value="{kode_ket}" readonly="">
								</div>
							</div>
							</div>
							<div id="div_asuransi" style="display: none;">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-5 control-label input-sm" for="kelompok_pasien">ASURANSI</label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="nama_rekanan"   value="{nama_rekanan}" readonly="">
								</div>
							</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-5 control-label input-sm" for="kelompok_pasien">JENIS PASIEN</label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="jenis_pasien"   value="{jenis_pasien}" readonly="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-5 control-label input-sm" for="dokter_poli">DOKTER POLIKLINIK/IGD</label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="dokter_poli"   value="{dokter_poli}" readonly>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
												<label class="col-md-5 control-label input-sm" for="total_barang">TOTAL BARANG</label>
											<div class="col-md-7">
												<input type="text" readonly class="form-control btn-xs input-sm number"   value="{total_barang}" id="total_barang" name="total_barang" />
											</div>
									</div>
							<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-5 control-label input-sm" for="total_barang">TOTAL HARGA</label>
									<div class="col-md-7">
										<input type="text" readonly class="form-control btn-xs input-sm number"  value="{total_harga}" id="total_harga" name="total_harga" />
									</div>
							</div>

						</div>
                </div>

				<div class="row">
					<div class="col-sm-12">
						<div class="progress progress-mini">
							<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
						</div>
						<blockquote>
						Detail Obat
						</blockquote>
					</div>
				</div><!-- List Obat Non-Racikan -->
				<div class="row">
					<div class="col-sm-12">
						<div class="progress progress-mini">
							<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
						</div>
						<h5 style="margin-bottom: 10px;">Non Racikan</h5>
						<div class="control-group">
							<div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0;">
								<table id="manage-tabel-nonRacikan" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 15%;">Nama Obat</th>
											<th style="width: 7%;">Satuan</th>
											<th style="width: 7%;">Cara Pakai</th>
											<th style="width: 7%;">Qty</th>
											<th style="width: 7%;">Sisa Obat</th>
											<th style="width: 15%;">Harga Obat</th>
											<th style="width: 10%;">QTY Retur</th>
											<th style="width: 10%;">Sub Total</th>
										</tr>
									</thead>
									<tbody>
										<?
										$no=0;
											foreach($rows as $row){?>
											<tr>
											<td><?=$row->nama?></td>
											<td><?=$row->singkatan?></td>
											<td><?=$row->carapakai?></td>
											<td><?=$row->kuantitas?></td>
											<td><?=($row->kuantitas+$row->qty_retur)-($row->total_retur)?></td>
											<td><?=number_format($row->harga-$row->diskon_rp,0)?></td>
											<?$id="jml_retur_".$no?>
											<td><input class="form-control input-sm number qty" type="text" id="<?=$id?>" value="<?=$row->qty_retur?>"/></td>
											<?$id="total_harga_retur_".$no?>
											<td><input readonly class="form-control input-sm number qty" type="text" id="<?=$id?>" value="<?=$row->totalharga?>"/></td>
											<td style='display:none'><?=$no?></td>
											<td style='display:none'><?=$row->harga-$row->diskon_rp?></td><!-- 9-->
											<td style='display:none'><?=$row->idbarang?></td>
											<td style='display:none'><?=$row->idtipe?></td>
											<?$id="label_total_harga".$no?>
											<td style='display:none' id="<?=$id?>"><?=$row->totalharga?></td>
											<?$id="label_kuantitas".$no?>
											<td style='display:none' id="<?=$id?>"><?=$row->qty_retur?></td>
											<td style='display:none'><?=$row->id?></td>
											<td style='display:none'><?=$row->hargadasar?></td>
											<td style='display:none'><?=$row->margin?></td>
											<td style='display:none'><?=$row->diskon?></td>
											<td style='display:none'><?=$row->diskon_rp?></td>
											</tr>
										<?
										$no=$no+1;}?>
									</tbody>
									<tfoot id="foot-total-nonracikan">
										<tr>
											<th colspan="7" class="hidden-phone">
												<span class="pull-right"><b>TOTAL</b></span></th>
											<th colspan="1"><b><span id="gt"><?=number_format($totalharga,0)?></span></b></th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>

        <!-- List Obat Racikan -->


            <div class="progress progress-mini">
                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
            </div>
			<div class="col-md-12">
				<div class="form-group">
					<label class=" control-label input-sm" for="alasan">ALASAN:</label>
					<div class="col-md-12">
						<textarea class="form-control" rows="6" id="alasan" name="alasan"><?=$alasan?></textarea>
					</div>
				</div>
			</div>
            <hr>
            <div class="block-footer text-right bg-light lter">
                <button class="btn btn-info" type="submit" id="btnSubmit-Penjualan" name="btnSubmit-Penjualan">Simpan <img style="display: none;" id="loading-button-ajax" src="{ajax}img/ajax-loader.gif"></button>
                <button class="btn btn-default" type="button" id="btnCancel-Penjualan" name="btnCancel-Penjualan">Batal</button>
            </div>
            </div>
		</div>
	</div>
</div>
<input type="hidden" id="id" name="id" value="{id}">
<input type="hidden" id="idpasien" name="idpasien" value="{idpasien}">
<input type="hidden" id="grand_total" name="grand_total" value="{totalharga}">
<input type="hidden" id="pos_tabel_non_racikan" name="pos_tabel_non_racikan" value="0">
<input type="hidden" name="form_edit"  id="nopenjualan" value="{form_edit}">
<br><br>
<?php echo form_close() ?>
</div>
</div>
<button hidden id="success_msg" class="js-notify" data-notify-type="success" data-notify-message="Success!">Success</button>
<button hidden id="error_msg" class="js-notify" data-notify-type="danger" data-notify-message="Error!">Danger</button>

<!-- Modal Data Pasien -->

<!-- Modal Data Pasien -->
<div class="modal fade in black-overlay" data-backdrop="false" id="modal-data-penjualan" tabindex="-1" role="dialog" aria-hidden="true" style="display: none; padding-right: 30px;">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List Data Penjualan</h3>
				</div>
				<div class="block-content">
					<hr>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snomedrec">No. Medrec</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snomedrec" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snamapasien">Nama Pasien</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snamapasien" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="sbarang">Nama Barang</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="sbarang" value="">
									</div>
								</div>

							</div>
							<div class="col-md-6">

								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snopenjualan">No Penjualan</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snopenjualan" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="sasalpasien">Asal Pasien</label>
									<div class="col-md-8">
										<select name="sasalpasien" id="sasalpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="#" >Semua Asal</option>
											<option value="0">Pasien Non Rujukan</option>
											<option value="1">Poliklinik</option>
											<option value="2">Instalasi Gawat Darurat</option>
											<option value="3">Rawat Inap</option>
										</select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="sasalpasien"></label>
									<div class="col-md-8">
										<button class="btn btn-success btn-block text-uppercase btn-sm" id="btn_cari">
											<i class="fa fa-filter"></i> Filter
										</button>
									</div>
								</div>
							</div>

						</div>
					</div>
					<hr>
					<table width="100%" id="tabel_data_penjualan" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Tanggal</th>
								<th>No. Penjualan</th>
								<th>No. Medrec</th>
								<th>Nama</th>
								<th>Asal Pasien</th>
								<th>Total Barang</th>
								<th>Total Harga</th>
							</tr>
						</thead>
						<tbody id="tbody-data-penjualan"></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				<!-- <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Ok</button> -->
			</div>
		</div>
	</div>
</div>



<!-- End Modal Data Pasien -->
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript" src="{ajax}basic-ajax.js"></script>
<link rel="stylesheet" id="css-main" href="{toastr_css}">
<script src="{toastr_js}"></script>
<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".number").number(true, 0, '.', ',');
		validasi_save();
		$('#alasan').keyup(function(e){
		   validasi_save();
	   });
		// $('.number').number(true, 0, '.', ',');
	})
	function formatNumber (num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}
	// $("#snomedrec,#snamapasien,#snopenjualan,#sbarang").keyup(function() {
			// // var snomedrec = $("#snomedrec").val();
			// // var snamapasien = $("#snamapasien").val();
			// // var snopenjualan = $("#snopenjualan").val();
			// // var sasalpasien = $("#sasalpasien").val();
			// // var sbarang = $("#sbarang").val();
			// // delay(function() {
				// // $('#tabel_data_penjualan').DataTable().destroy();
				// // loadDataPenjualan(snomedrec, snamapasien, snopenjualan,sasalpasien,sbarang);
			// // }, 1000);
		// });

	$("#sasalpasien").change(function() {
			// var snomedrec = $("#snomedrec").val();
			// var snamapasien = $("#snamapasien").val();
			// var snopenjualan = $("#snopenjualan").val();
			// var sasalpasien = $("#sasalpasien").val();
			// var sbarang = $("#sbarang").val();
			// // alert($("#sasalpasien").val());
			// delay(function() {
				// $('#tabel_data_penjualan').DataTable().destroy();
				// loadDataPenjualan(snomedrec, snamapasien, snopenjualan,sasalpasien,sbarang);
			// }, 1000);
		});

	function loadDataPenjualan() {
			var snomedrec = $("#snomedrec").val();
			var snamapasien = $("#snamapasien").val();
			var snopenjualan = $("#snopenjualan").val();
			var sasalpasien = $("#sasalpasien").val();
			var sbarang = $("#sbarang").val();
			$('#tabel_data_penjualan').DataTable().destroy();
			var tablePasien = $('#tabel_data_penjualan').DataTable({
				"pageLength": 8,
				"searching": false,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tpasien_pengembalian/get_penjualan/',
					type: "POST",
					dataType: 'json',
					data: {
						snomedrec: snomedrec,
						snamapasien: snamapasien,
						snopenjualan: snopenjualan,
						sasalpasien: sasalpasien,
						sbarang: sbarang,
					}
				},
				"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": true },
					{ "width": "10%", "targets": 1, "orderable": true },
					{ "width": "10%", "targets": 2, "orderable": true },
					{ "width": "15%", "targets": 3, "orderable": true },
					{ "width": "30%", "targets": 4, "orderable": true },
					{ "width": "10%", "targets": 5, "orderable": true },
					{ "width": "10%", "targets": 6, "orderable": true },
					{ "width": "10%", "targets": 7, "orderable": true },
				]
			});
		}
		var delay = (function() {
			var timer = 0;
			return function(callback, ms) {
				clearTimeout(timer);
				timer = setTimeout(callback, ms);
			};
		})();
	$("#btn_cari_penjualan").click(function() {
		$("#snomedrec").val('');
		$("#snamapasien").val('');
		$("#snopenjualan").val('');
		var snomedrec = $("#snomedrec").val();
		var snamapasien = $("#snamapasien").val();
		var snopenjualan = $("#snopenjualan").val();
		var sasalpasien = $("#sasalpasien").val();
		var sbarang = $("#sbarang").val();

		// delay(function() {
			// $('#tabel_data_penjualan').DataTable().destroy();
			// loadDataPenjualan(snomedrec, snamapasien, snopenjualan,sasalpasien,sbarang);
		// }, 1000);

	});
	$(document).on("click",".selectPenjualan",function(){
		// clear_detail();
		var idpenjualan = ($(this).data('idpenjualan'));
		var nopenjualan = ($(this).data('nopenjualan'));
		$("#nopenjualan").val(nopenjualan);
		$("#idpenjualan").val(idpenjualan);
		$.ajax({
		  url: '{site_url}tpasien_pengembalian/get_penjualan_detail/'+idpenjualan,
		  dataType: "json",
		  success: function(data) {

			$("#idpasien").val(data.idpasien);
			$("#nomedrec").val(data.nomedrec);
			$("#nama_pasien").val(data.nama);
			$("#kode_kes").val(data.kode_kes);
			$("#kode_ket").val(data.kode_ket);
			$("#nama_rekanan").val(data.nama_rekanan);
			$("#jenis_pasien").val(data.jenis_pasien);
			$("#umur").val(data.umurtahun+' Thn ' + data.umurbulan+' Bln ' + data.umurhari + ' hari');
			$("#kelompok_pasien").val(data.nama_kelompok);
			$("#dokter_poli").val(data.asal_pasien);
			$("#total_barang").val(data.totalbarang);
			$("#total_harga").val(data.totalharga);
			show_jenis(data.idkelompokpasien);
		  }

		});
		$.ajax({
		  url: '{site_url}tpasien_pengembalian/get_penjualan_obat_detail/'+idpenjualan,
		  dataType: "json",
		  success: function(data) {
				var content = "";
				var no=0;
				$('#manage-tabel-nonRacikan tbody tr').remove();
				$.each(data.detail, function (i,obat) {
					// var st_readonly='';
					// if (obat.statusverifikasi=='1'){
						// st_readonly=' readonly ';
					// }
					content +='<tr>';
					content += "<td>" + obat.nama + "</td>";//0 Nama Obat
					content += "<td>" + obat.singkatan + "</td>"; //1 Satuan
					content += "<td>" + obat.carapakai + "</td>"; //2 Cara pakai
					content += "<td>" + obat.kuantitas + "</td>"; //3 QTY
					content += "<td>" + (obat.kuantitas-(obat.qty_retur)) + "</td>"; //4 Sisa Retur
					content += "<td>" + formatNumber(obat.harga-obat.diskon_rp) + "</td>"; //5 Harga
					content += "<td><input class='form-control input-sm number qty' type='text' id='jml_retur_"+no+"' value=''/></td>"; //1 Kode barang ID Hidden
					content += "<td><input readonly class='form-control input-sm number gt' type='text' id='total_harga_retur_"+no+"' value='0'/></td>"; //1 Kode barang ID Hidden
					content += "<td style='display:none'>" + no + "</td>"; //8 NO
					content += "<td style='display:none'>" + (obat.harga-obat.diskon_rp) + "</td>"; //9 harga
					content += "<td style='display:none'>" + obat.idbarang + "</td>"; //10 id barang
					content += "<td style='display:none'>" + obat.idtipe + "</td>"; //11 Id tipe
					content += "<td style='display:none' id='label_total_harga"+no+"'></td>"; //12 harga
					content += "<td style='display:none' id='label_kuantitas"+no+"'></td>"; //13 Kuantitas
					content += "<td style='display:none'>"+ obat.id +"</td>"; //14 Kuantitas
					content += "<td style='display:none'>" + obat.hargadasar + "</td>"; //15 Harga Dasar
					content += "<td style='display:none'>" + obat.margin + "</td>"; //16 Margin
					content += "<td style='display:none'>" + obat.diskon + "</td>"; //17 Diskon
					content += "<td style='display:none'>" + obat.diskon_rp + "</td>"; //18 Diskon

					content +='</tr>';
					no+=1;
				});
				$('#manage-tabel-nonRacikan tbody').append(content);
				$(".number").number(true, 0, '.', ',');
		  }

		});
	});
	$(document).on('keyup', '.qty', function() {
		var qty_retur = $(this).val();
		var qty = $(this).closest('tr').find("td:eq(4)").html();
		var harga = $(this).closest('tr').find("td:eq(9)").html();
		var no = $(this).closest('tr').find("td:eq(8)").html();
		var total=0;
		if (parseFloat(qty_retur)>parseFloat(qty)){
			Toastr('QTY retur melebihi QTY sisa.','Maaf...')
			parseInt($(this).val(''))
			total=parseFloat(0) ;
		}else{
			total=parseFloat(qty_retur) * parseFloat(harga) ;
		}

		$("#label_kuantitas"+no).text($(this).val());
		$("#label_total_harga"+no).text(parseInt(total));
		$("#total_harga_retur_"+no).val(total);
		grand_total();
	});
	function grand_total () {
		var total_grand = 0;
		var no=0;
		// console.log(total_grand);
		$('#manage-tabel-nonRacikan tbody tr').each(function() {
			no=$(this).find('td:eq(8)').text();
			total_grand=parseFloat(total_grand) + parseFloat($("#total_harga_retur_"+no).val());
		});
			// console.log($("#total_harga_retur_"+no).val());
			$("#grand_total").val(total_grand);

		$("#gt").text(formatNumber(total_grand));
		validasi_save();
	}
	function validasi_save(){
		if ((parseFloat($("#grand_total").val()) < 1) || ($("#alasan").val() =='') ){
			$("#btnSubmit-Penjualan").attr('disabled',true);
		}else{
			$("#btnSubmit-Penjualan").attr('disabled',false);
		}
	}
	function show_jenis($idkelompok){
		$("#div_asuransi").css("display", "none");
		$("#div_kes").css("display", "none");
		$("#div_ket").css("display", "none");
		if ($idkelompok=='1'){
			$("#div_asuransi").css("display", "block");
		}else if($idkelompok=='3'){
			$("#div_kes").css("display", "block");
		}else if($idkelompok=='4'){
			$("#div_ket").css("display", "block");
		}
	}
	$(document).on("click","#btnSubmit-Penjualan",function(){
		$("#btnSubmit-Penjualan").hide();
		$("#cover-spin").show();
		var tbl_non_racikan = $('table#manage-tabel-nonRacikan tbody tr').get().map(function(row) {
		  return $(row).find('td').get().map(function(cell) {
			return $(cell).html();
		  });
		});

		$("#pos_tabel_non_racikan").val(JSON.stringify(tbl_non_racikan));


		$("#form1").submit();
	});
	$(document).on("click","#btn_cari",function(){
		loadDataPenjualan();
	});
</script>
<script type="text/javascript" src="{ajax}basic-ajax.js"></script>

<!-- Load Lib Validation -->
<script src="{plugins_path}jquery-validation/jquery.validate.min.js"></script>
<script src="{plugins_path}jquery-validation/additional-methods.min.js"></script>
<script src="{plugins_path}js-validation/validation-tpasien-penjualan.js"></script>
<link rel="stylesheet" id="css-main" href="{toastr_css}">
<script src="{toastr_js}"></script>
