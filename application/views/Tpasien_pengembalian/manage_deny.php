<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-rounded ">
    <div class="block-header">
        <ul class="block-options">
            <li><a href="{site_url}tpasien_pengembalian" class="btn"><i class="fa fa-reply"></i></a></li>
        </ul>
        <h3 class="block-title">{title}</h3>
        <hr>
    </div>
    <?php echo form_open('tpasien_pengembalian/save','class="form-horizontal push-10-t" id="form-work"') ?>
<input type="hidden" id="id-penjualan" name="id-penjualan" />
<input type="hidden" id="getIDpasien" name="getIDpasien" />
<input type="hidden" id="counter-hidden" name="counter-hidden" />
<input type="hidden" id="json-detail-nonracikan" name="json-detail-nonracikan" />
<input type="hidden" id="post-totalbarang" name="post-totalbarang" />
<input type="hidden" id="post-TrowNonracikan" name="post-TrowNonracikan" />
<input type="hidden" id="post-Trow" name="post-Trow" />
    <div class="block-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="control-group">
                        <div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0;">
             <div class="col-md-6">
                        <div class="form-group" style="margin-bottom: 5px;">
                                    <label class="col-md-4 control-label input-sm" for="manage-input-nopenjualan">TANGGAL</label>
                                <div class="col-md-7">
                                        <input class="js-datepicker form-control btn-xs input-sm" type="text" id="manage-input-tanggal" name="manage-input-tanggal" data-date-format="dd-mm-yyyy" value="<?=date('d-m-Y')?>"/>
                                </div>
                        </div>
                <div class="form-group" style="margin-bottom: 5px;">
                                    <label class="col-md-4 control-label input-sm" for="manage-input-nopenjualan">NO. PENJUALAN</label>
                                <div class="col-md-7">
                                <div class="input-group">
                                    <input type="text" class="form-control btn-xs input-sm" id="manage-input-nopenjualan" name="manage-input-nopenjualan" autocomplete="off" />
                                                    <span class="input-group-btn">
                                    <button type="button" class="btn btn-info btn-sm"  id="manage-cari-pengembalian">CARI NO. PENJUALAN</button>
                                                    </span>
                                                </div>
                                                </div>
                        </div>
                <div class="form-group" style="margin-bottom: 5px;">
                                    <label class="col-md-4 control-label input-sm" for="manage-input-nomedrec">NO. MEDREC</label>
                                <div class="col-md-7">
                                    <input type="text" readonly class="form-control btn-xs input-sm" id="manage-input-nomedrec" name="manage-input-nomedrec" />
                                </div>
                        </div>
                <div class="form-group" style="margin-bottom: 5px;">
                                    <label class="col-md-4 control-label input-sm" for="manage-input-namapasien">NAMA PASIEN</label>
                                <div class="col-md-7">
                                    <input type="text" readonly class="form-control btn-xs input-sm" id="manage-input-namapasien" name="manage-input-namapasien" />
                                </div>
                        </div>
                <div class="form-group">
                    <label class="col-md-4 control-label input-sm" for="manage-input-umurpasien">UMUR</label>
                    <div class="col-md-7">
                        <input type="text" class="form-control input-sm" id="manage-input-umurpasien" readonly>
                    </div>
                </div>
            </div>
<!-- ======================================================================= -->
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-5 control-label input-sm" for="manage-input-kelompokpasien">KELOMPOK PASIEN</label>
                    <div class="col-md-7">
                        <input type="text" class="form-control" id="manage-input-kelompokpasien" readonly="">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-5 control-label input-sm" for="manage-input-dokterpoli">DOKTER POLIKLINIK/IGD</label>
                    <div class="col-md-7">
                        <input type="text" class="form-control" id="manage-input-dokterpoli" readonly>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                                    <label class="col-md-5 control-label input-sm" for="manage-input-totalbarang">TOTAL BARANG</label>
                                <div class="col-md-7">
                                    <input type="text" readonly class="form-control btn-xs input-sm" id="manage-input-totalbarang" name="manage-input-totalbarang" />
                                </div>
                        </div>
                <div class="form-group" style="margin-bottom: 5px;">
                                    <label class="col-md-5 control-label input-sm" for="manage-input-totalbarang">TOTAL HARGA</label>
                                <div class="col-md-7">
                                    <input type="text" readonly class="form-control btn-xs input-sm" id="manage-input-totalharga" name="manage-input-totalharga" />
                                </div>
                        </div>
            </div>
                </div>

<div class="row">
<div class="col-sm-12">
<div class="progress progress-mini">
<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
</div>
<blockquote>
Detail Obat
</blockquote>
</div>
</div>
                            <!-- List Obat Non-Racikan -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="progress progress-mini">
                        <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                    </div>
                    <h5 style="margin-bottom: 10px;">Non Racikan</h5>
                    <div class="control-group">
                        <div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0;">
                            <table id="manage-tabel-nonRacikan" class="table table-striped table-bordered" style="margin-bottom: 0;">
                                <thead>
                                    <tr>
                                        <th style="width: 15%;">Nama Obat</th>
                                        <th style="width: 7%;">Satuan</th>
                                        <th style="width: 7%;">Cara Pakai</th>
                                        <th style="width: 7%;">Qty</th>
                                        <th style="width: 7%;">Sisa Obat</th>
                                        <th style="width: 15%;">Harga Obat</th>
                                        <th style="width: 10%;">QTY Retur</th>
                                        <th style="width: 10%;">Sub Total</th>
                                    </tr>
                                </thead>
                                <tbody id="manage-tbody-nonRacikan"></tbody>
                                <tfoot id="foot-total-nonracikan">
                                    <tr>
                                        <th colspan="7" class="hidden-phone">
                                            <span class="pull-right"><b>TOTAL</b></span></th>
                                        <th colspan="1"><b><span id="manage-fullTotal-nonRacikan">0</span></b></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        <!-- List Obat Racikan -->
            <div class="row" hidden>
                <div class="col-sm-12">
                    <div class="progress progress-mini">
                        <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                    </div>
                    <h5 style="margin-bottom: 10px;">Racikan</h5>
                    <div class="control-group">
                        <div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0;">
                            <table id="table_racikan_detail" class="table table-striped table-bordered" style="margin-bottom: 0;">
                                <thead>
                                    <tr>
                                        <th style="width: 20%;">Nama</th>
                                        <th style="width: 11%;">Jenis</th>
                                        <th style="width: 7%;">Cara Pakai</th>
                                        <th style="width: 7%;">Qty</th>
                                        <th style="width: 7%;">Sisa Obat</th>
                                        <th style="width: 15%;">Jumlah Harga</th>
                                        <th style="width: 10%;">QTY Retur</th>
                                        <th style="width: 10%;">Sub Total</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_racikan">
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="7" class="hidden-phone">                                        
                                        <span class="pull-right"><b>TOTAL</b></span></th>
                                        <th colspan="1"><b>
                                            <input type="hidden" id="total_racikan" name="total_racikan" value="0" />
                                            <label id="disp_total_racikan">0</label></b>
                                        </th>
                                    </tr>
                                    <tr class="bg-light dker">
                                        <th colspan="7"><label class="pull-right"><b>GRAND TOTAL</b></label></th>
                                        <th colspan="1"><b>
                                            <input type="hidden" id="total" name="grandtotal" value="0" />
                                            <label id="disp_total">0</label></b>
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                            <table style="display: none;">
                                <tbody id="row_racikan_detail_clone">
                                    <tr id="tr_racikan" class="c_tr_racikan">
                                        <td>
                                            <input type="hidden" id="clone_racikan_detail_id" />
                                            <input type="hidden" id="clone_tmp_counter_racikan" />
                                            <input type="hidden" id="clone_racikan" />
                                            <input type="hidden" id="clone_idracikan" />
                                            <input type="hidden" id="clone_list_obat" />
                                            <div id="clone_div_racikan_obat" class="input-group">
                            <input class="form-control input-sm nama_racikan" readonly type="text" id="clone_nama_racikan" />
                            <span class="input-group-btn">
                                <button class="btn btn-info input-sm list_obat_racikan" type="button">List Obat (0)</button>
                            </span>
                        </div>
                    </td>
                            <td>
                                <span id="clone_span_jenis_racikan"></span>
                                <input type="text" class="form-control input-sm jenis_racikan" id="clone_jenis_racikan" name="jenis_racikan[]">
                                            &nbsp;
                                            <input type="text" id="clone_jenis_racikan_lainnya" autocomplete="off" style="display: none;"/>
                                                    </td>
<td>
    <span id="clone_span_cara_pakai_racikan"></span></td>
<td>
    <span id="clone_disp_quantity_racikan"></span></td>
<td>
    <span id="clone_span_sisa_obat">0</span></td>
<td>
    <span id="clone_disp_racikan_jmlharga">0</span></td>
<td>
    <input class="form-control input-sm qty-retur" type="text" id="clone_disp_qty_retur" onkeypress="return hanyaAngka(event)"/>
</td>
                                                    <td>
    <span id="clone_disp_subtotal_racikan">0</span>
    <input type="hidden" class="hide-subtotal-racik" id="clone_hide-subtotal-racik">
                                                    </td>
                                                </tr>
                                    <tr id="tr_racikan_obat" class="c_tr_racikan_obat" style="display: none;"></tr>
                                </tbody>
                            </table>
                    <input type="hidden" id="counter" name="counter" value="0" />
                    <input type="hidden" id="counter_racikan" name="counter_racikan" value="0" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="progress progress-mini">
                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
            </div>
        <div class="col-md-12">
              <div class="form-group">
                                    <label class=" control-label input-sm" for="manage-textarea-alasan">ALASAN:</label>
                                <div class="col-md-12">
                                    <textarea class="form-control" rows="6" id="manage-textarea-alasan" name="manage-textarea-alasan"></textarea>
                                </div>
                        </div>
        </div>
            <hr>
            <div class="block-footer text-right bg-light lter">
                <button class="btn btn-info" type="submit" id="btnSubmit-Penjualan" name="btnSubmit-Penjualan">Simpan <img style="display: none;" id="loading-button-ajax" src="{ajax}img/ajax-loader.gif"></button>
                <button class="btn btn-default" type="button" id="btnCancel-Penjualan" name="btnCancel-Penjualan">Batal</button>
            </div>
                        </div>
                    </div>
                </div>
            </div>
        <input type="hidden" id="detail_value" name="detail_value" value="[]">
        <br><br>
        <?php echo form_close() ?>
    </div>
</div>
<button hidden id="success_msg" class="js-notify" data-notify-type="success" data-notify-message="Success!">Success</button>
<button hidden id="error_msg" class="js-notify" data-notify-type="danger" data-notify-message="Error!">Danger</button>
           
<!-- Modal Data Pasien -->
<div class="modal fade in black-overlay" data-backdrop="false" id="modal-data-ListRacikanObat" tabindex="-1" role="dialog" aria-hidden="true" style="display: none; padding-right: 30px;">
            <div class="modal-dialog modal-lg modal-dialog-popout">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-header bg-success">
                            <ul class="block-options">
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>
                            <h3 class="block-title">List Data Racikan Obat</h3>
                        </div>
                        <div class="block-content">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <blockquote>
<footer id="list_racikan_modal_nama"></footer>
                                    </blockquote>
                                    </div>
                                </div>
                            </div>
            <div class="progress progress-mini">
                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
            </div>
<table id="manage-tabel-list-Racikan" class="table table-striped table-bordered" style="margin-bottom: 0;">
    <thead>
        <tr>
            <th style="width: 15%;">Nama Obat</th>
            <th style="width: 15%;">Tarif</th>
            <th style="width: 10%;">Satuan</th>
            <th style="width: 10%;">Qty</th>
            <th style="width: 9%;">Disc(%)</th>
            <th style="width: 15%;">Jumlah</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                        <!-- <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Ok</button> -->
                    </div>
                </div>
            </div>
        </div>
<!-- Modal Data Pasien -->
<div class="modal fade in black-overlay" data-backdrop="false" id="modal-data-penjualan" tabindex="-1" role="dialog" aria-hidden="true" style="display: none; padding-right: 30px;">
            <div class="modal-dialog modal-lg modal-dialog-popout">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-header bg-success">
                            <ul class="block-options">
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>
                            <h3 class="block-title">List Data Penjualan</h3>
                        </div>
                        <div class="block-content">
                            <table width="100%" id="tabel-data-penjualan" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tanggal</th>
                                        <th>No. Penjualan</th>
                                        <th>No. Medrec</th>
                                        <th>Nama</th>
                                        <th>Total Barang</th>
                                        <th>Total Harga</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody-data-penjualan"></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                        <!-- <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Ok</button> -->
                    </div>
                </div>
            </div>
        </div>



<!-- End Modal Data Pasien -->
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript" src="{ajax}basic-ajax.js"></script>
        <link rel="stylesheet" id="css-main" href="{toastr_css}">
        <script src="{toastr_js}"></script>

<script type="text/javascript">
    $(function () {// Setup AJAX
var kuantitas,harga,sisaobat,total,det_td,det_td2,det_td3,labelTHracikan;
$.ajaxSetup({
type:"post",
cache:false,
dataType: "json"
});
$.when($.getJSON( "getNoPen" ),$.ready).then(function(data){
$("#manage-input-nopenjualan").autoComplete({
            minChars: 1,
            source: function(term, suggest){
                term = term.toLowerCase();

                var $countriesList  = data[0].datapaspen;
                var $suggestions    = [];

                for ($i = 0; $i < $countriesList.length; $i++) {
                    if (~ $countriesList[$i].toLowerCase().indexOf(term)) $suggestions.push($countriesList[$i]);
                }

                suggest($suggestions);
            },
    onSelect: function (a,b) {
        $("#counter_racikan").val(0);
                det_td=$('table#manage-tabel-nonRacikan tbody#manage-tbody-nonRacikan');
                det_td2=$('table#table_racikan_detail tbody');
                det_td.find('tr').remove();
                det_td2.find('tr').remove();
        $.ajax({
            before:function(){
                // det_td.append('<tr class="text-center"><td>Tunggu</td></tr>')
            },
            url:"../get/detailpenjualan",
            // type:"json",
            data:{"nopen":b},
            success:function(data){
                for(var i = 0;i < data.datapaspen.length; i++){
                    // if(data.datapaspen[i].idprov==Hidprov){selectprovid="selected";}else{selectprovid="";}
                $('#id-penjualan').val(data.datapaspen[i].id)
                $('#getIDpasien').val(data.datapaspen[i].idpasien)
                $('#manage-input-nopenjualan').val(data.datapaspen[i].nopenjualan)
                $('#manage-input-nomedrec').val(data.datapaspen[i].nomedrec)
                $('#manage-input-namapasien').val(data.datapaspen[i].nama)
                $('#manage-input-umurpasien').val(data.datapaspen[i].umurpasien)
                $('#manage-input-kelompokpasien').val(data.datapaspen[i].kelompok)
                $('#manage-input-dokterpoli').val(data.datapaspen[i].namadokter)
                $('#manage-input-totalbarang').val(data.datapaspen[i].totalbarang)
                $('#manage-input-totalharga').val('Rp'+addCommas(data.datapaspen[i].totalharga))
                // $('#manage-select-obat').append('<option value="'+data.datapaspen[i].totalharga+'">'++'</option>')
                  }
        $.ajax({
            before:function(){
                console.log('test')
                // det_td.append('<tr class="text-center"><td colspan="8">Tunggu</td></tr>')
            },
            url:"../get/datanonracikan",
            // type:"json",
            data:{"idpenjualan":$('#id-penjualan').val()},
            success:function(data){
                var show =data.dataobatnonracikan;
                $('#post-TrowNonracikan').val(show.length);
            for(var i = 0;i < show.length; i++){
                content = "<tr id='rowke-"+(i+1)+"'>";
                content += '<td>'+show[i].namaobat+'<input type="hidden" name="hide-namaobat[]" value="'+show[i].namaobat+'"></td>';
                content += '<td>'+show[i].singkatan+'<input type="hidden" name="hide-singkatan[]" value="'+show[i].singkatan+'"></td>';
                content += '<td>'+show[i].carapakai+'<input type="hidden" name="hide-carapakai[]" value="'+show[i].carapakai+'"></td>';
                content += '<td>'+show[i].kuantitas+'<input type="hidden" name="hide-kuantitas[]" value="'+show[i].kuantitas+'"></td>';
                content += '<td><span class="label-sisaobat" id="label-sisaobat_'+i+'">'+show[i].sisaobat
                +'</span><input type="hidden" name="hide-sisaobat[]" value="'+show[i].sisaobat+'"></td>';
                content += '<td><span class="label-hargabeli" id="label-hargabeli_'+i+'">'+show[i].hargabeli
                +'</span><input type="hidden" name="hide-hargabeli[]" value="'+show[i].hargabeli+'"></td>';
                content += '<td><input type="text" class="form-control input-sm manage-input-qtyretur" id="manage-input-qtyretur_'+i+'" name="manage-input-qtyretur[]" value="0"><span hidden id="label-qtyretur_'+i+'"></span></td>';
                content += '<td><span class="subtotalNR">0</span><input type="hidden" name="hide-subtotal[]" class="hide-subtotal" id="hide-subtotal_'+i+'"></td>';
                content += '</tr>';
                content += '<input type="hidden" name="hide-idobat[]" value="'+show[i].idobat+'">';
                det_td.append(content);
}
                }
            });
$.ajax({
before:function(){
    // $('#loading-button-ajax').show()
},
url:"../get/dataracikan",
// type:"json",
data:{"idpenjualan":$('#id-penjualan').val()},
success:function(data){
var show=data.dataracikan;
    for(var y=0;y<show.length;y++){
cloningtabelRacikan();
var Y=(y+1);

$('#tr_racikan_'+Y+' .list_obat_racikan').text('List Obat (' + show[y].totalobat +')');
$('#tr_racikan_'+Y+' .list_obat_racikan').attr('data-id',show[y].idracikan)
$('#hide-idracikan_'+Y).val(show[y].idracikan)

$('#nama_racikan_'+Y).val(show[y].namaracikan)
$('#span_jenis_racikan_'+Y).text(show[y].jenisracikan)
$('#span_cara_pakai_racikan_'+Y).text(show[y].carapakai)
$('#disp_quantity_racikan_'+Y).text(show[y].kuantitas)
$('#span_sisa_obat_'+Y).text(show[y].sisaobat)
$('#disp_racikan_jmlharga_'+Y).text(show[y].totalharga)
}
            var ctr =$('#tbody_racikan tr.c_tr_racikan').length;
                $('#post-Trow').val(ctr)
}
});
        }
    });
}
});
})
getDataSSP('#tabel-data-penjualan','../get/datapenjualan')
$(document).on("focus","tr td input",function() { $(this).select();} );

$(document).ready(function(){
$(document).on("click", ".list_obat_racikan", function() {
                det_th=$('table#manage-tabel-list-Racikan thead');
                det_td3=$('table#manage-tabel-list-Racikan tbody');
                det_td3.empty();
        $.ajax({
            before:function(){
                // $('#loading-button-ajax').show()
            },
            url:"../get/dataracikanobat",
            // type:"json",
            data:{"idracikan":$(this).attr('data-id')},
            success:function(data){
                var show=data.dataobatracikan;
    $('#list_racikan_modal_nama').html('<b>Nama Racikan: '+show[0].namaracikan+'</b>')
            for(var t=0;t<show.length;t++){
                content = "<tr class='tr_list_obat'>";
                content += '<td>'+show[t].namaobat+'<input type="hidden" value="'+show[t].namaobat+'" name="det-racikan-namaobat[]"></td>';
                content += '<td>'+show[t].hargabeli+'<input type="hidden" value="'+show[t].hargabeli+'" name="det-racikan-harga[]"></td>';
                content += '<td>'+show[t].singkatan+'<input type="hidden" value="'+show[t].singkatan+'" name="det-racikan-satuan[]"></td>';
                content += '<td>'+show[t].kuantitas+'<input type="hidden" value="'+show[t].kuantitas+'" name="det-racikan-qty[]"></td>';
                content += '<td>'+show[t].diskon+'<input type="hidden" value="'+show[t].diskon+'" name="det-racikan-diskon[]"></td>';
                content += '<td>'+show[t].totalharga+'<input type="hidden" value="'+show[t].totalharga+'" name="det-racikan-jumlah[]"></td>';
                content += '<td style="display:none">'+show[t].idobat+'<input type="hidden" value="'+show[t].idobat+'" name="det-racikan-idobat[]"></td>';
                content += '</tr>';
                det_td3.append(content);
                }
    $('#modal-data-ListRacikanObat').modal('show')
}
})
})
        $(document).on("keyup", ".manage-input-qtyretur", function() {
            kuantitas = parseInt($(this).val().replace(/,/g, ''));
            harga = parseInt($(this).closest('tr').find(".label-hargabeli").html().replace(/,/g, ''));
            sisaobat = parseInt($(this).closest('tr').find(".label-sisaobat").html().replace(/,/g, ''));
if(kuantitas>sisaobat){
    Toastr('QTY retur melebihi QTY sisa.','Maaf...')
        parseInt($(this).val(''))
        total=0;
    return false;
}
            total = harga * kuantitas;
            $('.hide-subtotal').val(total)
            $(this).closest('tr').find(".subtotalNR").html(total.toLocaleString(undefined, {
                minimumFractionDigits: 0
            }));
            // console.log(total);
            checkTotal();
        });

            // })
$('#manage-cari-pengembalian').click(function(){
            $('#modal-data-penjualan').modal('show')
    })
})
  
        $('table#tabel-data-penjualan').on("click","#manage-klik-tanggal",function(){
            var op;
                var id=$(this).attr('data-id')
            $('#modal-data-penjualan').modal('hide')
        $("#counter_racikan").val(0);
                det_td=$('table#manage-tabel-nonRacikan tbody#manage-tbody-nonRacikan');
                det_td2=$('table#table_racikan_detail tbody');
                det_td.find('tr').remove();
                det_td2.find('tr').remove();
        $.ajax({
            before:function(){
                // $('#loading-button-ajax').show()
            },
            url:"../get/detailpenjualan",
            // type:"json",
            data:{"id":id},
            success:function(data){
                for(var i = 0;i < data.datapaspen.length; i++){
                    // if(data.datapaspen[i].idprov==Hidprov){selectprovid="selected";}else{selectprovid="";}
                $('#id-penjualan').val(data.datapaspen[i].id)
                $('#getIDpasien').val(data.datapaspen[i].idpasien)
                $('#manage-input-nopenjualan').val(data.datapaspen[i].nopenjualan)
                $('#manage-input-nomedrec').val(data.datapaspen[i].nomedrec)
                $('#manage-input-namapasien').val(data.datapaspen[i].nama)
                $('#manage-input-umurpasien').val(data.datapaspen[i].umurpasien)
                $('#manage-input-kelompokpasien').val(data.datapaspen[i].kelompok)
                $('#manage-input-dokterpoli').val(data.datapaspen[i].namadokter)
                $('#manage-input-totalbarang').val(data.datapaspen[i].totalbarang)
                $('#manage-input-totalharga').val('Rp'+addCommas(data.datapaspen[i].totalharga))
                // $('#manage-select-obat').append('<option value="'+data.datapaspen[i].totalharga+'">'++'</option>')
                  }
        $.ajax({
            before:function(){
                // $('#loading-button-ajax').show()
            },
            url:"../get/datanonracikan",
            // type:"json",
            data:{"idpenjualan":$('#id-penjualan').val()},
            success:function(data){
                var show =data.dataobatnonracikan;
                $('#post-TrowNonracikan').val(show.length);
            for(var i = 0;i < show.length; i++){
                content = "<tr id='rowke-"+(i+1)+"'>";
                content += '<td>'+show[i].namaobat+'<input type="hidden" name="hide-namaobat[]" value="'+show[i].namaobat+'"></td>';
                content += '<td>'+show[i].singkatan+'<input type="hidden" name="hide-singkatan[]" value="'+show[i].singkatan+'"></td>';
                content += '<td>'+show[i].carapakai+'<input type="hidden" name="hide-carapakai[]" value="'+show[i].carapakai+'"></td>';
                content += '<td>'+show[i].kuantitas+'<input type="hidden" name="hide-kuantitas[]" value="'+show[i].kuantitas+'"></td>';
                content += '<td><span class="label-sisaobat" id="label-sisaobat_'+i+'">'+show[i].sisaobat
                +'</span><input type="hidden" name="hide-sisaobat[]" value="'+show[i].sisaobat+'"></td>';
                content += '<td><span class="label-hargabeli" id="label-hargabeli_'+i+'">'+show[i].hargabeli
                +'</span><input type="hidden" name="hide-hargabeli[]" value="'+show[i].hargabeli+'"></td>';
                content += '<td><input type="text" class="form-control input-sm manage-input-qtyretur" id="manage-input-qtyretur_'+i+'" name="manage-input-qtyretur[]" value="0"><span hidden id="label-qtyretur_'+i+'"></span></td>';
                content += '<td><span class="subtotalNR">0</span><input type="hidden" name="hide-subtotal[]" class="hide-subtotal" id="hide-subtotal_'+i+'"></td>';
                content += '</tr>';
                content += '<input type="hidden" name="hide-idobat[]" value="'+show[i].idobat+'">';
                det_td.append(content);
}
                }
            });
$.ajax({
before:function(){
    // $('#loading-button-ajax').show()
},
url:"../get/dataracikan",
// type:"json",
data:{"idpenjualan":$('#id-penjualan').val()},
success:function(data){
var show=data.dataracikan;
    for(var y=0;y<show.length;y++){
cloningtabelRacikan();
var Y=(y+1);

$('#tr_racikan_'+Y+' .list_obat_racikan').text('List Obat (' + show[y].totalobat +')');
$('#tr_racikan_'+Y+' .list_obat_racikan').attr('data-id',show[y].idracikan)
$('#hide-idracikan_'+Y).val(show[y].idracikan)

$('#nama_racikan_'+Y).val(show[y].namaracikan)
$('#span_jenis_racikan_'+Y).text(show[y].jenisracikan)
$('#span_cara_pakai_racikan_'+Y).text(show[y].carapakai)
$('#disp_quantity_racikan_'+Y).text(show[y].kuantitas)
$('#span_sisa_obat_'+Y).text(show[y].sisaobat)
$('#disp_racikan_jmlharga_'+Y).text(show[y].totalharga)
}
            var ctr =$('#tbody_racikan tr.c_tr_racikan').length;
                $('#post-Trow').val(ctr)
      }
        });
  }
    });
})

// fungsi tombol open modal Non-Racikan 
        $('#manage-tambah-pengembalian').click(function(){
            $('#manage-modal-list-pengembalian').modal('show')
        })

        // tombol hapusdet-obat
        $(document).on('click','#manage-hapusdet-pengembalian',function(){
                var row = $(this).closest('td').parent();
                var totalFull=$('#manage-fullTotal-pengembalian');
                alertDelete(row,'manage-tbody-pengembalian',totalFull,7);
        })

    })

    function checkTotal() {
        var totalhargaretur = 0;
        $('#manage-tabel-nonRacikan tbody tr').each(function() {
            totalhargaretur += parseFloat($(this).find('.subtotalNR').text().replace(/\,/g, ""));
        });

        // console.log(totalhargaretur);

        $("#manage-fullTotal-nonRacikan").html(totalhargaretur.toLocaleString(undefined, {
            minimumFractionDigits: 0
        }));
    }


// Aksi insert penjualan obat
// Submit to database
document.querySelector('#form-work').addEventListener('submit', function(e) {
        // CSS visual button
        // $('#loading-button-ajax').show();
// Convert to JSON detail Non Racikan
        var detail_nonracikan = $('table tbody#manage-tbody-nonRacikan tr').get().map(function(row) {
                return $(row).find('td').get().map(function(cell) {
                        return $(cell).html();
                });
        });
                $("#json-detail-nonracikan").val(JSON.stringify(detail_nonracikan));

        var qtytotal1=0;
        $('tbody#manage-tbody-nonRacikan tr').each(function() {
            qtytotal1 += parseFloat($(this).find('.manage-input-qtyretur').val().replace(/\,/g, ""));
        });
        var qtytotal2=0;
        // var ctr=$('#counter_racikan').val();
        var ctr =$('#tbody_racikan tr.c_tr_racikan').length;
    for(var xxyz=1;xxyz<=ctr;xxyz++){
    $('tbody tr#tr_racikan_'+xxyz).each(function() {
        qtytotal2 += parseFloat($(this).find('.qty-retur').val().replace(/\,/g, ""));
    });
    }
        $('#post-totalbarang').val(parseInt(qtytotal1)+parseInt(qtytotal2))
});
</script>