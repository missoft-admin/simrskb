<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>KWITANSI PENGEMBALIAN </title>
    <style>
	@page {
			size: 75mm 600mm;
            margin-top: 7mm;
            margin-left: 2mm;
            margin-right: 3mm;
            margin-bottom: 5mm;
        }

    body {
      -webkit-print-color-adjust: exact;
    }
      

      table {
		font-family: "Courier", Verdana, sans-serif;
        font-size: 22px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 1px;
		 
      }
      .content td {
        padding: 1px;
		border: 0px solid #6033FF;
		
      }
      .content-2 td {
        margin: 0px;
		border: 0px solid #6033FF;
      }

      /* border-normal */
      .border-full {
        border: 0px solid #000 !important;
		
      }
	  .text-header{
		  font-family: "Courier", Verdana, sans-serif;
		font-size: 25px !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-bottom-top-left {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:1px dotted #000 !important;
      }
      .border-thick-bottom{
        border-bottom:1px dotted #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    </style>
    <script type="text/javascript">
    	
    </script>
  </head>
  <body>
  <div style="width:100%">
    <table class="content" style="width:100%">
      <tr style="border-bottom:2px dotted #000 !important;" >
      
		<td class="text-center" rowspan="8" >
          &nbsp;RSKB HALMAHERA SIAGA<br>
          &nbsp;JL. LLRE. Martadinata No. 28<br>
          &nbsp;Telp. +6222-4206061<br>
          &nbsp;Bandung, 40115
        
		</td>
      </tr>

    </table>
	<table class="content-2"  style="width:100%">
	  
      <tr>		
        <td colspan="3" class="text-center"><strong>PENGEMBALIAN FARMASI</strong></td>
      </tr>
	  <tr>
        <td style="width:30%">NO RETUR</td>
        <td style="width:1%">:</td>
        <td style="width:69%"><?=$nopengembalian?></td>        
      </tr>
	  <tr>
        <td >NO. MEDREC</td>
        <td>:</td>
        <td><?=$nomedrec?></td>					
      </tr>
	  <tr>
        <td >NAMA PASIEN</td>
        <td >:</td>
        <td ><?=$nama?></td>		
      </tr>
	  <tr>
		<td>UMUR</td>
        <td >:</td>
        <td ><?=(($umur)? $umur.' th ':'').(($umur_bulan)? $umur_bulan.' bln ':'').(($umur_hari)? $umur_hari.' hr ':'')?></td>		
	  </tr>
	  <tr>
		<td>NO. FAKTUR</td>
        <td >:</td>
        <td ><?=$nopenjualan?></td>
	   </tr>
	   <tr style="border-bottom:2px dotted #000 !important;" >
		<td >ASAL PASIEN</td>
        <td >:</td>
        <td ><?=$asalrujukan?></td>		
		</tr>
    </table>
	<br>
	
    <table class="content-2"  style="width:100%">
	   <tr>		
        <td colspan="3" class="text-center"><strong>Rincian Obat</strong></td>
      </tr>
      <tr>
        <td class="border-full text-center" style="width:5%">NO</td>
        <td class="border-full text-left" style="width:70%">Nama Obat</td>
        <td class="border-full text-center" style="width:25%">Qty</td>
      </tr>
      <?php $number = 0; ?>
      <?php foreach  ($list_data as $row){ ?>
        <?php $number = $number + 1; ?>
        <tr>
          <td class="border-full text-center"> <?=strtoupper(($number))?></td>
          <td class="border-full text-left"> <?=strtoupper(($row->nama))?><br>Rp.<?= number_format($row->kuantitas*$row->harga,0) ?></td>
          <td class="border-full text-center"><?= number_format($row->kuantitas,0)?></td>
        </tr>
      <?php } ?>
	 
      <tr>
        <td class="border-full text-left"></td>
        <td class="border-full text-right"><span style="float:right">Jumlah Rp.</span></td>
        <td class="border-full text-right"><span style="float:left"> <?=number_format($totalharga,0)?></span></td>
      </tr>
	  
    </table>
	<br>
	<br>
	<table  style="width:100%">      
      <tr>
        <td style="width:20%">Alasan : </td>
        <td style="width:80%"><?=$alasan?></td>
      </tr>
	</table>
	<br>
	
	
	<br>
    <table  style="width:100%">      
      <tr>
        <td style="width:50%">&nbsp;</td>
        <td style="width:50%"  class="text-right">Petugas,</td>
      </tr>
      
	  <tr>
        <td>&nbsp;</td>
        <td></td>
      </tr>
	  <tr>
        <td>&nbsp;</td>
        <td></td>
      </tr>
	  
      <tr>
        <td><?=date('d-m-Y H:i:s')?></td>
        <td class="text-right">(<?=$created_nama?>)</td>
      </tr>
	  <tr>
	  <br>
        <td colspan="2" class="text-center">--Terima kasih--</td>
      </tr>
    </table>
	</div>
  </body>
</html>
