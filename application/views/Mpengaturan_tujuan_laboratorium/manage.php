<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}mpengaturan_tujuan_laboratorium" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?php echo form_open_multipart('mpengaturan_tujuan_laboratorium/save','class="form-horizontal" id="form-work"') ?>
        <div class="form-group">
            <label class="col-md-3 control-label" for="">Nama Tujuan Laboratorium</label>
            <div class="col-md-9">
                <input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="">Tipe Layanan</label>
            <div class="col-md-9">
                <select name="tipe_layanan" id="tipe_layanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                    <option value="">Pilih Opsi</option>
                    <option value="1" <?=($tipe_layanan == "1") ? "selected" : "" ?>>Patologi Klinik</option>
                    <option value="2" <?=($tipe_layanan == "2") ? "selected" : "" ?>>Patologi Anatomi</option>
                    <option value="3" <?=($tipe_layanan == "3") ? "selected" : "" ?>>Bank Darah</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="">Tujuan Antrian</label>
            <div class="col-md-9">
                <select name="tujuan" id="tujuan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                    <option value="">Pilih Opsi</option>
                    <?php foreach (get_all('mtujuan', array('jenis' => 1, 'status' => 1)) as $row) { ?>
                    <option value="<?=$row->id?>" <?=($tujuan == $row->id) ? "selected" : "" ?>><?=$row->nama_tujuan?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    
        <hr>

        <b><span class="label label-success" style="font-size:12px;">File Audio</span></b> &nbsp; # &nbsp;  
        <label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
            <input type="checkbox" name="status_file_audio" <?=($status_file_audio == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
            Aktifkan Suara
        </label>

        <br /><br />

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="audio-list">
                        <thead>
                            <tr>
                                <th style="width:5%" class="text-center">No</th>
                                <th style="width:10%" class="text-center">Urutan</th>
                                <th style="width:50%" class="text-center">Asset Sound</th>
                                <th style="width:5%" class="text-center">Aksi</th>
                            </tr>
                            <tr>
                                <th class="text-center" style="vertical-align: middle;">X</th>
                                <th>
                                    <input type="text" class="form-control" id="urutan_audio" placeholder="Urutan" value="">
                                </th>
                                <th>
                                    <select id="file_audio" class="js-select2 form-control" style="width: 100%;">
                                        <option value="#" selected>Pilih Asset Sound</option>
                                        <? foreach(get_all('antrian_asset_sound') as $row) { ?>
                                            <option value="<?=$row->id;?>"><?=$row->nama_asset;?></option>
                                        <? } ?>
                                    </select>
                                </th>
                                <th class="text-center">
                                    <button type="button" id="audio-add" class="btn btn-success">Tambahkan</button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list_audio as $index => $row) { ?>
                            <tr>
                                <td class="text-center"><?=$index + 1;?></td>
                                <td class="text-center"><?=$row->nourut;?></td>
                                <td data-id="<?=$row->file_audio;?>"><?=$row->file_audio_name;?></td>
                                <td><button class="btn btn-danger audio-remove" data-toggle="tooltip" title="Remove"><i class="fa fa-trash-o"></i></button></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <hr>
        
        <div class="form-group">
            <label class="col-md-3 control-label" for="">Dokter Penanggung Jawab Laboratorium</label>
            <div class="col-md-9">
                <select name="dokter_penanggung_jawab" id="dokter_penanggung_jawab" class="js-select2 form-control" style="width: 98%;" data-placeholder="Pilih Opsi">
                    <option value="">Pilih Opsi</option>
                    <?php foreach (get_all('mdokter', ['status' => 1]) as $row) { ?>
                    <option value="<?=$row->id?>" <?=($dokter_penanggung_jawab == $row->id) ? "selected" : "" ?>><?=$row->nama?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <hr>

        <b><span class="label label-success" style="font-size:12px">Dokter Laboratorium</span></b><br /><br />
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="dokter-laboratorium-list">
                <thead>
                    <tr>
                        <th style="width:5%" class="text-center">No</th>
                        <th style="width:50%" class="text-center">Dokter Laboratorium</th>
                        <th style="width:5%" class="text-center">Aksi</th>
                    </tr>
                    <tr>
                        <th class="text-center" style="vertical-align: middle;">X</th>
                        <th>
                            <select id="dokter_laboratorium" class="js-select2 form-control" style="width: 98%;" data-placeholder="Pilih Opsi">
                                <option value="">Pilih Opsi</option>
                                <?php foreach (get_all('mdokter', ['status' => 1]) as $row) { ?>
                                <option value="<?=$row->id?>"><?=$row->nama?></option>
                                <?php } ?>
                            </select>
                        </th>
                        <th class="text-center">
                            <button type="button" id="dokter-laboratorium-add" class="btn btn-success">Tambahkan</button>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($list_dokter_laboratorium as $index => $row) { ?>
                    <? $display_style = $row->status_default ? 'none' : ''; ?>
                    <tr>
                        <td class="text-center"><?=$index + 1;?></td>
                        <td data-id="<?= $row->iddokter; ?>" data-default="<?= ($row->status_default ? 1 : 0); ?>">
                            <?= $row->nama_dokter; ?>
                            <?= ($row->status_default ? '<span class="label label-danger">DEFAULT</span>' : ''); ?>
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger dokter-laboratorium-remove" data-toggle="tooltip" title="Remove"><i class="fa fa-trash-o"></i></button>
                            &nbsp;&nbsp;<button type="button" class="btn btn-warning dokter-laboratorium-default" data-toggle="tooltip" title="Set Default" style="display: <?= $display_style; ?>"><i class="fa fa-check"></i></button>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="">User Laboratorium Akses</label>
            <div class="col-md-9">
                <select name="user_laboratorium_akses[]" id="user_laboratorium_akses" class="js-select2 form-control" multiple style="width: 98%;" data-placeholder="Pilih Opsi">
                    <option value="">Pilih Opsi</option>
                    <?php foreach (get_all('mppa', ['staktif' => 1]) as $row) { ?>
                    <option value="<?=$row->id?>" <?=(in_array($row->id, $user_laboratorium_akses)) ? "selected" : "" ?>><?=$row->nama?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="">Tipe Layanan</label>
            <div class="col-md-9">
                <select name="tipe_layanan_akses[]" id="tipe_layanan_akses" class="js-select2 form-control" multiple style="width: 98%;" data-placeholder="Pilih Opsi">
                    <option value="">Pilih Opsi</option>
                    <option value="1" <?=(in_array("1", $tipe_layanan_akses)) ? "selected" : "" ?>>Laboratorium Umum</option>
                    <option value="2" <?=(in_array("2", $tipe_layanan_akses)) ? "selected" : "" ?>>Pathologi Anatomi</option>
                    <option value="3" <?=(in_array("3", $tipe_layanan_akses)) ? "selected" : "" ?>>PMI</option>
                </select>
            </div>
        </div>

        <hr>

        <b><span class="label label-success" style="font-size:12px">Akses Unit Laboratorium</span></b><br /><br />
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="akses-unit-laboratorium-list">
                <thead>
                    <tr>
                        <th style="width:5%" class="text-center">No</th>
                        <th style="width:20%" class="text-center">Tipe Layanan</th>
                        <th style="width:20%" class="text-center">Asal Pasien</th>
                        <th style="width:20%" class="text-center">Poliklinik / Kelas</th>
                        <th style="width:20%" class="text-center">Dokter</th>
                        <th style="width:10%" class="text-center">Aksi</th>
                    </tr>
                    <tr>
                        <th class="text-center" style="vertical-align: middle;">X</th>
                        <th>
                            <select id="akses_unit_tipe_layanan" class="js-select2 form-control" style="width: 98%;" data-placeholder="Pilih Opsi">
                                <option value="1">Laboratorium Umum</option>
                                <option value="2">Pathologi Anatomi</option>
                                <option value="3">PMI</option>
                            </select>
                        </th>
                        <th>
                            <select id="akses_unit_asal_pasien" class="js-select2 form-control" style="width: 98%;" data-placeholder="Pilih Opsi">
                                <option value="0">Semua</option>
                                <option value="1">Poliklinik</option>
                                <option value="2">Instalasi Gawat Daurat (IGD)</option>
                                <option value="3">Rawat Inap</option>
                                <option value="4">One Day Surgery (ODS)</option>
                            </select>
                        </th>
                        <th>
                            <select id="akses_unit_poliklinik" class="js-select2 form-control" style="width: 98%;" data-placeholder="Pilih Opsi">
                                <option value="0">Semua</option>
                            </select>
                        </th>
                        <th>
                            <select id="akses_unit_dokter" class="js-select2 form-control" style="width: 98%;" data-placeholder="Pilih Opsi">
                                <option value="0">Semua</option>
                                <?php foreach (get_all('mdokter', ['status' => 1]) as $row) { ?>
                                <option value="<?=$row->id?>"><?=$row->nama?></option>
                                <?php } ?>
                            </select>
                        </th>
                        <th class="text-center">
                            <button type="button" id="akses-unit-laboratorium-add" class="btn btn-success">Tambahkan</button>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($list_akses_unit_lab as $index => $row) { ?>
                    <tr>
                        <td class="text-center"><?=$index + 1;?></td>
                        <td data-id="<?=$row->tipe_layanan;?>"><?=GetTipeLayananLaboratorium($row->tipe_layanan);?></td>
                        <td data-id="<?=$row->asal_pasien;?>"><?=$row->asal_pasien == 0 ? 'Semua' : GetAsalPasienLabel($row->asal_pasien);?></td>
                        <td data-id="<?=$row->idpoliklinik;?>"><?=$row->idpoliklinik == 0 ? 'Semua' : $row->nama_unit;?></td>
                        <td data-id="<?=$row->iddokter;?>"><?=$row->iddokter == 0 ? 'Semua' : $row->nama_dokter;?></td>
                        <td><button type="button" class="btn btn-danger akses-unit-laboratorium-remove" data-toggle="tooltip" title="Remove"><i class="fa fa-trash-o"></i></button></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-12" style="float: right;">
                <button class="btn btn-success" type="submit">Simpan</button>
                <a href="{base_url}mpengaturan_tujuan_laboratorium" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
            </div>
        </div>

        <br><br>

        <input type="hidden" id="audioData" name="audio_data">
        <input type="hidden" id="dokterLaboratoriumData" name="dokter_laboratorium_data">
        <input type="hidden" id="aksesUnitData" name="akses_unit_data">

        <?php echo form_hidden('id', $id); ?>
        <?php echo form_close() ?>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{js_path}plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<script src="{js_path}plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<link href="{js_path}plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css" rel="stylesheet" type="text/css">
<link href="{js_path}plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
$(document).ready(function() {
    $('#audio-add').on('click', function () {
        var urutan = $('#urutan_audio').val();
        var fileAudio = $('#file_audio option:selected').val();
        var fileAudioName = $('#file_audio option:selected').text();

        // Validate input
        if (!urutan || !fileAudio) {
            swal('Error', 'Silakan isi semua kolom.', 'error');
            return;
        }

        // Check for duplicate urutan values
        var isDuplicate = false;
        $('#audio-list tbody tr').each(function () {
            var existingUrutan = $(this).find('td:nth-child(1)').text();
            if (existingUrutan == urutan) {
                isDuplicate = true;
                return false;
            }
        });

        if (isDuplicate) {
            swal('Error', 'Nilai urutan harus unik.', 'error');
            return;
        }

        var counter = $('#audio-list tbody tr').length + 1;
        var newRow = '<tr>' +
            '<td class="text-center">' + counter + '</td>' +
            '<td class="text-center">' + urutan + '</td>' +
            '<td data-id="' + fileAudio + '">' + fileAudioName + '</td>' +
            '<td><button class="btn btn-danger audio-remove" data-toggle="tooltip" title="Remove"><i class="fa fa-trash-o"></i></button></td>' +
            '</tr>';
        $('#audio-list tbody').append(newRow);
        
        // Clear input fields
        $('#urutan_audio').val('');
    });

    // Remove audio entry
    $(document).on('click', '.audio-remove', function () {
        var row = $(this).closest('tr');
        var index = row.index();
        
        row.remove();

        $('#audio-list tbody tr').each(function (index) {
            $(this).find('td:first').text(index + 1);
        });
    });

    // ...

    // Add Dokter Laboratorium entry
    $('#dokter-laboratorium-add').on('click', function () {
        // Get values from input fields
        var dokterLaboratoriumId = $('#dokter_laboratorium').val();
        var dokterLaboratoriumText = $('#dokter_laboratorium option:selected').text();

        // Validate input
        if (!dokterLaboratoriumId) {
            swal('Error', 'Silakan pilih dokter laboratorium.', 'error');
            return;
        }

        // Check for duplicate dokter laboratorium values
        var isDuplicate = $('#dokter-laboratorium-list tbody tr td:nth-child(2)[data-id="' + dokterLaboratoriumId + '"]').length > 0;

        if (isDuplicate) {
            swal('Error', 'Dokter laboratorium sudah ada.', 'error');
            return;
        }

        // Add a new row to the table
        var counter = $('#dokter-laboratorium-list tbody tr').length + 1;
        var defaultStatus = counter == 1; // Set default to true for the first entry
        var displayStyle = defaultStatus ? 'none' : ''; // Hide the button if defaultStatus is true
        var newRow = `<tr>
            <td class="text-center">${counter}</td>
            <td data-id="${dokterLaboratoriumId}" data-default="${defaultStatus ? '1' : '0'}">
                ${dokterLaboratoriumText}
                ${defaultStatus ? ' <span class="label label-danger">DEFAULT</span>' : ''}
            </td>
            <td>
                <button type="button" class="btn btn-danger dokter-laboratorium-remove" data-toggle="tooltip" title="Remove"><i class="fa fa-trash-o"></i></button>
                &nbsp;&nbsp;<button type="button" class="btn btn-warning dokter-laboratorium-default" data-toggle="tooltip" title="Set Default" style="display: ${displayStyle};"><i class="fa fa-check"></i></button>
            </td>
        </tr>`;
        $('#dokter-laboratorium-list tbody').append(newRow);

        // Clear input fields
        $('#dokter_laboratorium').val('').trigger('change'); // Clear and trigger change event for Select2

        // Keep the label visible
        $('#dokter_laboratorium').siblings('label').addClass('active');
    });

    // Set Dokter Laboratorium as Default
    $(document).on('click', '.dokter-laboratorium-default', function () {
        var row = $(this).closest('tr');

        // Remove the default status from all rows
        $('#dokter-laboratorium-list tbody tr td:nth-child(2)').data('default', '0').find('span.label').remove();

        // Set the default status for the selected row
        row.find('td:nth-child(2)').data('default', '1').append(' <span class="label label-danger">DEFAULT</span>');

        // Toggle visibility of the "Set Default" button
        $('.dokter-laboratorium-default').toggle();

        // Show the "Set Default" button only for rows with default status 0
        $('#dokter-laboratorium-list tbody tr td:nth-child(2)').each(function () {
            var defaultStatus = $(this).data('default');
            $(this).closest('tr').find('.dokter-laboratorium-default').toggle(defaultStatus == '0');
        });
    });

    // Remove Dokter Laboratorium entry
    $(document).on('click', '.dokter-laboratorium-remove', function () {
        var row = $(this).closest('tr');
        var defaultStatus = row.find('td:nth-child(2)').data('default');

        // Remove the current row
        row.remove();

        // Check if the removed row had default status 1
        if (defaultStatus == '1') {
            // Set the default status to 1 for the first row
            $('#dokter-laboratorium-list tbody tr:first-child td:nth-child(2)').data('default', '1').append(' <span class="label label-danger">DEFAULT</span>');
        }

        // Update the "No" column after deleting a row
        $('#dokter-laboratorium-list tbody tr').each(function (index) {
            $(this).find('td:first').text(index + 1);
        });

        // Show the "Set Default" button only for rows with default status 0
        $('#dokter-laboratorium-list tbody tr td:nth-child(2)').each(function () {
            var defaultStatus = $(this).data('default');
            $(this).closest('tr').find('.dokter-laboratorium-default').toggle(defaultStatus == '0');
        });
    });

    // ...

    // Event handler for change on #akses_unit_asal_pasien
    $('#akses_unit_asal_pasien').on('change', function () {
        var selectedAsalPasien = $(this).val();

        // Example: Replace {base_url} with your actual base URL
        var apiUrl = '{base_url}mpengaturan_tujuan_laboratorium/getPoliklinikData/' + selectedAsalPasien;

        $.ajax({
            url: apiUrl,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                // Clear existing options
                $('#akses_unit_poliklinik').empty();

                // Add new options based on the fetched data
                $('#akses_unit_poliklinik').append('<option value="0" data-tipe="0">Semua</option>');
                $.each(data, function (index, item) {
                    $('#akses_unit_poliklinik').append('<option value="' + item.id + '" data-tipe="' + item.tipe + '">' + item.nama + '</option>');
                });

                // Set value to "0" (Semua) and trigger change event for Select2
                $('#akses_unit_poliklinik').val('0').trigger('change.select2');

                // Keep the label visible
                $('#akses_unit_poliklinik').siblings('label').addClass('active');
            },
            error: function (error) {
                console.error('Error fetching data:', error);
            }
        });
    });

    // Add Akses Unit Laboratorium entry
    $('#akses-unit-laboratorium-add').on('click', function () {
        // Get values from input fields
        var tipeLayananId = $('#akses_unit_tipe_layanan').val();
        var tipeLayananText = $('#akses_unit_tipe_layanan option:selected').text();
        var asalPasienId = $('#akses_unit_asal_pasien').val();
        var asalPasienText = $('#akses_unit_asal_pasien option:selected').text();
        var poliklinikId = $('#akses_unit_poliklinik').val();
        var poliklinikText = $('#akses_unit_poliklinik option:selected').text();
        var dokterId = $('#akses_unit_dokter').val();
        var dokterText = $('#akses_unit_dokter option:selected').text();

        // Validate input
        if (!tipeLayananId) {
            swal('Error', 'Tipe Layanan harus dipilih.', 'error');
            return;
        }

        // Check for duplicate entries
        var isDuplicate = false;
        $('#akses-unit-laboratorium-list tbody tr').each(function () {
            var existingTipeLayananId = getDataId($(this).find('td:nth-child(2)'));
            var existingAsalPasienId = getDataId($(this).find('td:nth-child(3)'));
            var existingPoliklinikId = getDataId($(this).find('td:nth-child(4)'));
            var existingDokterId = getDataId($(this).find('td:nth-child(5)'));

            if (
                existingTipeLayananId == tipeLayananId &&
                existingAsalPasienId == asalPasienId &&
                existingPoliklinikId == poliklinikId &&
                existingDokterId == dokterId
            ) {
                isDuplicate = true;
                return false; // Exit the loop early if a duplicate is found
            }
        });

        if (isDuplicate) {
            swal('Error', 'Data sudah ada.', 'error');
            return;
        }

        // Add a new row to the table
        var counter = $('#akses-unit-laboratorium-list tbody tr').length + 1;
        var newRow = '<tr>' +
            '<td class="text-center">' + counter + '</td>' +
            '<td data-id="' + tipeLayananId + '">' + tipeLayananText + '</td>' +
            '<td data-id="' + asalPasienId + '">' + asalPasienText + '</td>' +
            '<td data-id="' + poliklinikId + '">' + poliklinikText + '</td>' +
            '<td data-id="' + dokterId + '">' + dokterText + '</td>' +
            '<td><button type="button" class="btn btn-danger akses-unit-laboratorium-remove" data-toggle="tooltip" title="Remove"><i class="fa fa-trash-o"></i></button></td>' +
            '</tr>';
        $('#akses-unit-laboratorium-list tbody').append(newRow);

        // Clear input fields
        $('#akses_unit_tipe_layanan').val('').trigger('change'); // Clear and trigger change event for Select2
        $('#akses_unit_asal_pasien, #akses_unit_poliklinik, #akses_unit_dokter').val('0').trigger('change');

    // Keep the labels visible
        // Keep the labels visible
        $('#akses_unit_tipe_layanan, #akses_unit_asal_pasien, #akses_unit_poliklinik, #akses_unit_dokter').siblings('label').addClass('active');
    });

    // Remove Akses Unit Laboratorium entry
    $(document).on('click', '.akses-unit-laboratorium-remove', function () {
        var row = $(this).closest('tr');

        // Remove the current row
        row.remove();

        // Update the "No" column after deleting a row
        $('#akses-unit-laboratorium-list tbody tr').each(function (index) {
            $(this).find('td:first').text(index + 1);
        });
    });

    // Helper function to get data ID based on the selected option
    function getDataId(element) {
        return $(element).data('id');
    }

    $('#form-work').submit(function (e) {
        // Add JSON data for tables
        var audioData = getAudioData();
        var dokterLaboratoriumData = getDokterLaboratoriumData();
        var aksesUnitData = getAksesUnitData();

        $("#audioData").val(JSON.stringify(audioData));
        $("#dokterLaboratoriumData").val(JSON.stringify(dokterLaboratoriumData));
        $("#aksesUnitData").val(JSON.stringify(aksesUnitData));
    });

    // Functions to get table data
    function getAudioData() {
        var audioData = [];
        $('#audio-list tbody tr').each(function () {
            var urutan = $(this).find('td:nth-child(2)').html();
            var fileAudio = $(this).find('td:nth-child(3)').attr('data-id');
            var fileAudioName = $(this).find('td:nth-child(3)').html();

            audioData.push({
                urutan: urutan,
                file_audio: fileAudio,
                file_audio_name: fileAudioName
            });
        });
        return audioData;
    }

    function getDokterLaboratoriumData() {
        var dokterLaboratoriumData = [];
        $('#dokter-laboratorium-list tbody tr').each(function () {
            var idDokterLaboratorium = $(this).find('td:nth-child(2)').attr('data-id');
            var statusDefault = $(this).find('td:nth-child(2)').attr('data-default');

            dokterLaboratoriumData.push({
                iddokter: idDokterLaboratorium,
                status_default: statusDefault
            });
        });
        return dokterLaboratoriumData;
    }

    function getAksesUnitData() {
        var aksesUnitData = [];
        $('#akses-unit-laboratorium-list tbody tr').each(function () {
            var tipeLayanan = $(this).find('td:nth-child(2)').attr('data-id');
            var asalPasien = $(this).find('td:nth-child(3)').attr('data-id');
            var poliklinik = $(this).find('td:nth-child(4)').attr('data-id');
            var dokter = $(this).find('td:nth-child(5)').attr('data-id');

            aksesUnitData.push({
                tipe_layanan: tipeLayanan,
                asal_pasien: asalPasien,
                poliklinik: poliklinik,
                dokter: dokter
            });
        });
        return aksesUnitData;
    }
});
</script>