<?=ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('203'))){ ?>
<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('204'))){ ?>
		<ul class="block-options">
        <li>
            <a href="{base_url}mpengaturan_tujuan_laboratorium/create" class="btn"><i class="fa fa-plus"></i></a>
        </li>
    </ul>
		<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Tipe Layanan</th>
					<th>Tujuan Antrian</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		</div>
	</div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mpengaturan_tujuan_laboratorium/getIndex',
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "10%", "targets": 0, "orderable": true, "className": "text-center" },
					{ "width": "20%", "targets": 1, "orderable": true },
					{ "width": "15%", "targets": 2, "orderable": true },
					{ "width": "15%", "targets": 3, "orderable": true }
				]
			});
	});
</script>
