<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mkontrabon" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('mkontrabon/save','class="form-horizontal"') ?>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Hari Kontra Bon</label>
				<div class="col-md-7">
					<select id="harikbo" name="harikbo" class="form-control" required>
						<option value="1" <?=($harikbo=='1'?'selected':'')?>> Minggu </option>
						<option value="2" <?=($harikbo=='2'?'selected':'')?>> Senin </option>
						<option value="3" <?=($harikbo=='3'?'selected':'')?>> Selasa </option>
						<option value="4" <?=($harikbo=='4'?'selected':'')?>> Rabu </option>
						<option value="5" <?=($harikbo=='5'?'selected':'')?>> Kamis </option>
						<option value="6" <?=($harikbo=='6'?'selected':'')?>> Jumat </option>
						<option value="7" <?=($harikbo=='7'?'selected':'')?>> Sabtu </option>
					
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Minimum Penggunaan Check</label>
				<div class="col-md-7">
					<input type="text" class="form-control number" id="minnominalceq" placeholder="Nominal Checq" name="minnominalceq" value="{minnominalceq}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Biaya Check</label>
				<div class="col-md-7">
					<input type="text" class="form-control number" id="biayaceq" placeholder="Biaya Check" name="biayaceq" value="{biayaceq}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Biaya Materai</label>
				<div class="col-md-7">
					<input type="text" class="form-control number" id="biayamaterai" placeholder="Biaya Materai" name="biayamaterai" value="{biayamaterai}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Biaya Transfer Antar Bank</label>
				<div class="col-md-7">
					<input type="text" class="form-control number" id="biaya_tf" placeholder="Biaya Transfer Antar Bank" name="biaya_tf" value="{biaya_tf}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Jumlah Hari</label>
				<div class="col-md-7">
					<input type="text" class="form-control number" id="jml_hari" placeholder="Minimal Hari Kontra Bon" name="jml_hari" value="{jml_hari}" required="" aria-required="true">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">User Validasi</label>
				<div class="col-md-7">
					<select id="user_id_validasi" name="user_id_validasi[]" class="form-control "></select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">User Aktivasi</label>
				<div class="col-md-7">
					<select id="user_id_aktivasi" name="user_id_aktivasi[]" class="form-control "></select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Last Edit</label>
				<div class="col-md-7">
					<table class="table table-bordered">
						<tr>
							<td><strong>User</strong></td>
							<td><strong>Tanggal</strong></td>
						</tr>
						<tr>
							<td><?=$user_edit?></td>
							<td><?=HumanDateLong($tgl_edit)?></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Submit</button>
					<a href="{base_url}musers" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>
<div class="block">
	<div class="block-header">
		
		<h3 class="block-title">History Perubahan</h3>
	</div>
	<div class="block-content">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th class="text-center" style="width: 5%;">#</th>
					<th style="width: 15%;">Tanggal</th>
					<th class="text-center" style="width: 10%;">Hari</th>
					<th class="text-center" style="width: 15%;">Minimum</th>
					<th class="text-center" style="width: 10%;">Biaya Check</th>
					<th class="text-center" style="width: 10%;">Biaya Materai</th>
					<th class="text-center" style="width: 10%;">Biaya Transfer</th>
					<th class="text-center" style="width: 10%;">Jml Hari</th>
					<th class="text-center" style="width: 15%;">User</th>
				</tr>
			</thead>
			<tbody>
				<? $no=0;
				foreach ($list_his as $r){
				?>
				<tr>
				<? $no++?>
					<td class="text-center"><?=$no?></td>
					<td><?=HumanDateLong($r->tgl_diganti)?></td>
					<td><?=$r->nama_hari?></td>
					<td><?=number_format($r->minnominalceq,0)?></td>
					<td><?=number_format($r->biayaceq,0)?></td>
					<td><?=number_format($r->biayamaterai,0)?></td>
					<td><?=number_format($r->biaya_tf,0)?></td>
					<td><?=number_format($r->jml_hari,0)?></td>
					<td><?=$r->user_edit?></td>
					
					
				</tr>
				<?}?>
			</tbody>
		</table>	
			
			
	</div>
</div>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".number").number(true,0,'.',',');
		$("#harikbo").select2();
		$.ajax({
			url: '{site_url}mkontrabon/get_user_validasi', 
			type: 'post',
			dataType: 'json',
			success: function(res) {
				// console.log(res);
				$('#user_id_validasi').select2({multiple: true, data: res});
				$('#user_id_aktivasi').select2({multiple: true, data: res});
			}
		})
		

		setTimeout(function() {
			$.ajax({
				type: "POST", 
				url: '{site_url}mkontrabon/get_user_validasi_selected/',
				dataType: 'JSON', 
				success: function(data) {
					var arr = [];
					for (var i in data) { arr.push(data[i].id); }
					$('#user_id_validasi').val(arr).trigger('change'); 
					console.log(data);
				}
			});
			$.ajax({
				type: "POST", 
				url: '{site_url}mkontrabon/get_user_aktivasi_selected/',
				dataType: 'JSON', 
				success: function(data) {
					var arr = [];
					for (var i in data) { arr.push(data[i].id); }
					$('#user_id_aktivasi').val(arr).trigger('change'); 
					console.log(data);
				}
			});
		}, 500);

		
	})
</script>
