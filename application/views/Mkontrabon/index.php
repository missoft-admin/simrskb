<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
        <ul class="block-options">
            <li>
                <a href="javascript:tambah()" class="btn">
                    <i class="fa fa-plus"></i>
                </a>
            </li>
        </ul>
	</div>
	<div class="block-content">
        <div class="form-group" style="margin-bottom: 30px;"></div>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="index_table">
			<thead>
				<tr>
					<th>#</th>
					<th>NO</th>
					<th>HARI KONTRABON</th>
					<th>MIN NOMINAL CEQ</th>
                    <th>BIAYA CEQ</th>
                    <th>BIAYA MATERAI</th>
                    <th>STATUS</th>
                    <th>AKSI</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_manage" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <form class="form-horizontal" action="javascript:simpan()" id="frm1">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">MASTER KONTRABON</h3>
                    </div>
                    <div class="block-content">
                        <input type="hidden" name="id" required>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Hari Kontrabon</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="harikbo" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Min Nominal Ceq</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="minnominalceq" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Biaya Ceq</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="biayaceq" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Biaya Materai</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="biayamaterai" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-check"></i> Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">

	var table;

	$(document).ready(function(){
        $('select[name=nokontrabon]').select2({
            placeholder: 'Cari Nomor KBO',
            allowClear: true,
            ajax: {
                url: '{site_url}tkontrabon/get_nokbo/',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {results:data}
                },
                cache: false,
            }
        })

        $('select[name=stkontrabon]').select2({
            placeholder: 'Plih Status KBO',
            allowClear: true,
            data: [
                {id: '0', text: 'BELUM DIBAYAR'},
                {id: '1', text: 'SUDAH DIBAYAR'},
            ]
        }).val(null).trigger('change')

		index_table();
	})

    function filter() {
        index_table();
    }


	function index_table() {

    	table = $('#index_table').DataTable({
    		pageLength: 50,
    		autoWidth: false,
    		destroy: true,
    		ajax: {
    			url: '{site_url}mkontrabon/index_table',
    			type: 'GET',
                data: {
                    nokontrabon: $('input[name=nokontrabon]').val(),
                    stkontrabon: $('select[name=stkontrabon]').val(),
                }
    		},
    		columns: [
    			{data: 'id', visible: false, orderable: true},
                {data: 'nomor', orderable: false},
                {data: 'harikbo'},
                {data: 'minnominalceq'},
                {data: 'biayaceq'},
                {data: 'biayamaterai'},
                {
                    data: 'status',
                    render: function(data) {
                        if(data == 1) return '<label class="label label-success">AKTIF</label>';
                        else return '<label class="label label-danger">NON AKTIF</label>';
                    }
                },
                {
                    className: 'text-right',
                    render: function(data,type,row) {
                        $aksi = `<div class="btn-group">`;
                        <php if (button_roles('mkontrabon/save')) : ?>
                            $aksi += `<a href="javascript:void(0)" class="btn btn-success btn-xs edit" title="Edit"><i class="fa fa-pencil"></i></a>`;
                        <php endif ?>
                        if(row.status != 1) {
                            <php if (button_roles('mkontrabon/hapus')) : ?>
                                $aksi += `<a href="javascript:hapus(`+row.id+`)" class="btn btn-danger btn-xs" title="Hapus"><i class="fa fa-trash"></i></a>`;
                            <php endif ?>
                            <php if (button_roles('mkontrabon/aktifasi')) : ?>
                                $aksi += `<a href="javascript:aktifasi(`+row.id+`)" class="btn btn-warning btn-xs" title="Aktifkan"><i class="fa fa-check"></i></a>`;
                            <php endif ?>
                        } else {
													$aksi += `<a href="javascript:non_aktifasi(`+row.id+`)" class="btn btn-danger btn-xs" title="Non Aktifkan"><i class="fa fa-times"></i></a>`;
												}
                        $aksi += `</div>`;
                        return $aksi;
                    }
                },
    		]
    	});
	}

    $('#index_table tbody').on('click', '.edit', function() {
        var row = $(this).parents('tr');
        var id = table.cell(row,0).data()
        var harikbo = table.cell(row,2).data()
        var minnominalceq = table.cell(row,3).data()
        var biayaceq = table.cell(row,4).data()
        var biayamaterai = table.cell(row,5).data()

        $('input[name=id]').val( id )
        $('input[name=harikbo]').val( harikbo )
        $('input[name=minnominalceq]').val( minnominalceq )
        $('input[name=biayaceq]').val( biayaceq )
        $('input[name=biayamaterai]').val( biayamaterai )
        $('#modal_manage').modal('show')
    })

    function tambah() {
        var form = $('#frm1')[0];
        form.reset();
        $('#modal_manage').modal('show');
    }

    function simpan() {
        var form = $('#frm1')[0];
        var formData = new FormData(form);
        var id = formData.get('id');
        $.ajax({
            url: '{site_url}mkontrabon/save/'+id,
            method: 'post',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            success: function(res) {
                if(res.status == 'success') {
                    index_table()
                    $('#modal_manage').modal('hide');
                    swal({title: 'Sukses', text: res.msg, type: 'success', timer: 1500});
                } else {
                    swal({title: 'Kesalahan!', text: res.msg, type: 'warning'});
                }
            }
        })
    }

	function aktifasi(id) {
		swal({
            text: 'Apakah yakin akan aktifkan data ini?',
            showCancelButton: true,
            confirmButtonColor: '#46c37b',
            confirmButtonText: 'Ya, aktifkan data!',
            html: false,
            preConfirm: function() {
              return new Promise(function (resolve) {
                setTimeout(function() { resolve() }, 50);
              })
            }
        }).then(
            function (result) {
                $.ajax({url: '{site_url}mkontrabon/aktifasi/'+id})
                swal({title: 'Sukses', text: 'Data berhasil diaktifasi.', type: 'success', timer: 1500});
                index_table();
            }
        );
	}

	function non_aktifasi(id) {
		swal({
            text: 'Apakah yakin akan non-aktifkan data ini?',
            showCancelButton: true,
            confirmButtonColor: '#46c37b',
            confirmButtonText: 'Ya, non-aktifkan data!',
            html: false,
            preConfirm: function() {
              return new Promise(function (resolve) {
                setTimeout(function() { resolve() }, 50);
              })
            }
        }).then(
            function (result) {
                $.ajax({url: '{site_url}mkontrabon/non_aktifasi/'+id})
                swal({title: 'Sukses', text: 'Data berhasil di non aktifasi.', type: 'success', timer: 1500});
                index_table();
            }
        );
	}

    function hapus(id) {
        swal({
            text: 'Apakah yakin akan hapus data ini?',
            showCancelButton: true,
            confirmButtonColor: '#d26a5c',
            confirmButtonText: 'Ya, hapus data!',
            html: false,
            preConfirm: function() {
              return new Promise(function (resolve) {
                setTimeout(function() { resolve() }, 50);
              })
            }
        }).then(
            function (result) {
                $.ajax({url: '{site_url}mkontrabon/hapus/'+id})
                swal({title: 'Sukses', text: 'Data berhasil dihapus.', type: 'success', timer: 1500});
                index_table();
            }
        );
    }


</script>
