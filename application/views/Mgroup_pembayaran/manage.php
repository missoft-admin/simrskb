<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}mgroup_pembayaran" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>

	<?php echo form_open('mgroup_pembayaran/save','class="form-horizontal push-10-t" id="form-work"') ?>
	<div class="block-content block-content-narrow">
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Nama Group</label>
			<div class="col-md-7">
				<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}">
			</div>
		</div>
	</div>

	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="tableAkun">
			<thead>
				<tr>
					<th width="80%">No. Akun</th>
					<th width="20%">Aksi</th>
				</tr>
				<tr>
					<th>
						<select id="noakun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach(get_all('makun_nomor') as $row){ ?>
								<option value="<? echo $row->id ?>"><?php echo $row->noakun.' '.$row->namaakun; ?></option>
							<?php } ?>
						</select>
					</th>
					<th>
						<button id="tambahAkun" class="btn btn-sm btn-success" type="button"><i class="fa fa-plus"></i> Tambahkan</button>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($list_akun as $row){ ?>
					<tr>
						<td style="display:none"><?=$row->idakun?></td>
						<td><?=$row->noakun2.' '.$row->namaakun.' ('.$row->idakun.')'?></td>
						<td>
							<div class="btn btn-group">
								<a href="#" class="btn btn-primary btn-sm editAkun" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>
								<a href="#" class="btn btn-danger btn-sm removeAkun" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash-o"></i></a></td>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	</div>

	<div class="row">
		<div class="block-content block-content-narrow">
			<div class="form-group">
				<label class="col-md-3 control-label"></label>
				<div class="col-md-7">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mvariable_rekapan" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
		</div>
	</div>

	<input type="hidden" id="rowindex" value="">
	<input type="hidden" id="tableAkunValue" name="akun_list_value" value="">

	<br><br>
	<?php echo form_hidden('id', $id); ?>
	<?php echo form_close() ?>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$(".number").number(true, 0, '.', ',');

		$(document).on("click", "#tambahAkun", function() {
			var duplicate = false;

			if ($("#rowindex").val() != '') {
				var content = "";
				rowIndex = $("#rowindex").val();
			} else {
				var content = "<tr>";
				var table = $('#tableAkun tbody tr');
				table.filter(function (){
					var $cells = $(this).children('td');
					// if($cells.eq(0).text() === $("#noakun option:selected").val()){
					if($cells.eq(0).text()){
						sweetAlert("Maaf...", "No AKun tidak boleh lebih dari satu", "error");
						duplicate = true;
						return false;
					}
				});
			}

			if(duplicate == false){
				content += "<td style='display:none'>" + $("#noakun").val() + "</td>";
				content += "<td>" + $("#noakun option:selected").text() + "</td>";
				content += "<td>";
				content += "	<div class='btn btn-group'>";
				content += "		<a href='#' class='btn btn-primary btn-sm editAkun' data-toggle='tooltip' title='Ubah'><i class='fa fa-pencil'></i></a>";
				content += "		<a href='#' class='btn btn-danger btn-sm removeAkun' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a>";
				content += "	</td>";
				content += "</div>";

				if($("#rowindex").val() != ''){
					// $('#manage-tabel-nonRacikan tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
					$('#tableAkun tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
				}else{
					content += "</tr>";
					$('#tableAkun tbody').append(content);
				}

				$("#rowindex").val('');
				$("#noakun").val(null).trigger('change');
			}
		});

		$(document).on("click", ".editAkun", function() {
			$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);

			value = $(this).closest('tr').find("td:eq(0)").html();
			// alert(value);
			$("#noakun").val(value).trigger('change');

			return false;
		});

		$(document).on("click", ".removeAkun", function() {
			if (confirm("Hapus data ?") == true) {
				$(this).closest('td').parent().remove();
			}
			return false;
		});

		$("#form-work").submit(function(e) {
			var form = this;

			var tableAkunValue = $('table#tableAkun tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					// if ($(cell).find("input").length >= 1) {
						// return $(cell).find("input").val();
					// } else {
						return $(cell).html();
					// }
				});
			});
			// alert(tableAkunValue);
			// return false;
			
			$("#tableAkunValue").val(JSON.stringify(tableAkunValue));

			swal({
				title: "Berhasil!",
				text: "Proses penyimpanan data.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});
	});
</script>
