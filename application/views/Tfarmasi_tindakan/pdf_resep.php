<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Resep</title>
	<style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
		@page {
			size: <?=$ukuran?>;
            margin-top: 1.8em;
            margin-left: 1.5em;
            margin-right: 1.5em;
            margin-bottom: 2.5em;
        }
	
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-family: "Courier", Verdana, sans-serif;
        font-size: 18px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }

      th {
        padding: 0px;
      }
	  td {
        padding: 0px;
      }
      .content th {
        padding: 0px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
	  .border-dotted {
        border: 1px;#000 !important;
        
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px; #000 !important;
        border-bottom-style: dotted;
      }

      

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }

      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier", Verdana, sans-serif;
        font-size: 18px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		margin:0;
		padding:0;
		 vertical-align: middle !important;
      }
      th {
        padding: 0px;

      }
	  td {
        padding: 0px;

      }
      .content td {
        padding: 0px;
		border: 0px solid #6030FF;
		vertical-align:top;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .baris_bawah {
        border-bottom:1px dashed #000 !important;
      }
	  .text-normal{
		font-size: 14px !important;
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
        font-weight: bold;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

     

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 20px 0;
		}
		fieldset {
			border:1px solid #000;
			border-radius:4px;
			box-shadow:0 0 0px #000;
			margin-top:-2px;
		}
		legend {
			background:#fff;
		}
    }
	.wrap-text { 
		white-space: normal; 
	  } 
	i{
		font-weight: normal;
	}
	.text-primary {
		color: #307ab7;
	}
  </style>
</head>

<body>
	<header>
		
	</header>
	
	<main>
	<table class="content">
		<tr>
			<td width="18%" rowspan="2" class="text-center"><img src="<?=base_url()?>assets/upload/antrian/<?=$header_logo?>" alt="" width="100" height="100"></td>
			<td width="80%" class="text-judul text-left" style="vertical-align: middle;"><?=$nama_rumah_sakit?></td>
		</tr>
		<tr><td  class="text-left text-normal"><?=$alamat?><br><?=$telepone?><br><?=$website?> Email : <?=$email?></td></tr>
		
		
	</table>
	
	<br>
	<table class="content">
		
		<tr>
			<td  class="text-left text-normal" width="10%">Apoteker</td>
			<td  class="text-left text-normal" width="90%">:&nbsp;&nbsp; <?=$apoteker?></td>
		</tr>
		<tr>
			<td  class="text-left text-normal" width="10%">SIPA</td>
			<td  class="text-left text-normal" width="90%">:&nbsp;&nbsp; <?=$sipa?></td>
		</tr>
		
		
	</table>
	<br>
	<table class="content">
		<tr>
			<td width="100%" colspan="8" class="text-center text-judul"><?=$judul_header_ina?><br><i><?=$judul_header_eng?></i></td>
		</tr>
		
	</table>
	<br>
	<table class="content">
		<?if ($st_presciber=='1' || $st_pasien=='1'){?>
		<tr>
			<?if ($st_presciber){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$presciber_ina?><br><i><?=$presciber_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$dokter?></td>
			<?}?>
			
			<?if ($st_pasien){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$pasien_ina?><br><i><?=$pasien_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$namapasien?> | <?=$no_medrec?> | <?=$jeniskelamin?></td>
			<?}?>
		</tr>
		<?}?>
		<?if ($st_noreg=='1' || $st_tl=='1'){?>
		<tr>
			<?if ($st_noreg){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$noreg_ina?><br><i><?=$noreg_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>s
			<td width="30%" colspan="" class="text-left"><?=$nopendaftaran?> | <?=HumanDateShort($tanggal)?></td>
			<?}?>
			
			<?if ($st_tl){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$tl_ina?><br><i><?=$tl_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=HumanDateShort($tanggal_lahir)?> | <?=$umur?></td>
			<?}?>
		</tr>
		<?}?>
		<?if ($st_noresep=='1' || $st_jenis_pasien=='1'){?>
		<tr>
			<?if ($st_noresep){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$noresep_ina?><br><i><?=$noresep_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$noresep?> | <?=HumanDateLong($created_date)?> </td>
			<?}?>
			
			<?if ($st_jenis_pasien){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$jenis_pasien_ina?><br><i><?=$jenis_pasien_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$nama_kelompok?> <?=($idkelompokpasien=='1'?' | '.$nama_rekanan:'')?></td>
			<?}?>
		</tr>
		<?}?>
		<?if ($st_alergi_setting=='1' || $st_diagnosa=='1'){?>
		<tr>
			<?if ($st_alergi_setting){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$alergi_ina?><br><i><?=$alergi_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$alergi?></td>
			<?}?>
			
			<?if ($st_diagnosa){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$diagnosa_ina?><br><i><?=$diagnosa_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$diagnosa?></td>
			<?}?>
		</tr>
		<?}?>
	</table>
	<table class="content">
		<?if ($st_bb=='1' || $st_tb=='1' || $st_luas_permukaan=='1' || $st_menyusui=='1'){?>
			<tr>
			<?if ($st_bb=='1' || $st_tb=='1'){?>
				<?if ($st_bb=='1'){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$bb_ina?><br><i><?=$bb_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$berat_badan?> Kg</td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
				<?if ($st_tb=='1'){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$tb_ina?><br><i><?=$tb_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$tinggi_badan?> CM</td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
			<?}else{?>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="2%" colspan="" class="text-left">:</td>
				<td width="10%" colspan="" class="text-left"></td>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="10%" colspan="" class="text-left"></td>
			<?}?>
				
			<?if ($st_luas_permukaan=='1' || $st_menyusui=='1'){?>
				<?if ($st_luas_permukaan){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$luas_permukaan_ina?><br><i><?=$luas_permukaan_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$luas_permukaan?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left"></td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
				<?if ($st_menyusui){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$menyusui_ina?><br><i><?=$menyusui_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$menyusui?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left"></td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
			<?}else{?>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="2%" colspan="" class="text-left"></td>
				<td width="10%" colspan="" class="text-left"></td>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="10%" colspan="" class="text-left"></td>
			<?}?>
				
			</tr>
		<?}?>
		
		<?if ($st_puasa_setting=='1' || $st_prioritas=='1' || $st_luas_permukaan=='1' || $st_menyusui=='1'){?>
			<tr>
			<?if ($st_puasa_setting=='1' || $st_prioritas=='1'){?>
				<?if ($st_puasa_setting=='1'){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$puasa_ina?><br><i><?=$puasa_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$puasa?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
				<?if ($st_prioritas=='1'){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$prioritas_ina?><br><i><?=$prioritas_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$prioritas?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
			<?}else{?>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="2%" colspan="" class="text-left">:</td>
				<td width="10%" colspan="" class="text-left"></td>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="10%" colspan="" class="text-left"></td>
			<?}?>
				
			<?if ($st_pulang=='1' || $st_ginjal=='1'){?>
				<?if ($st_pulang){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$pulang_ina?><br><i><?=$pulang_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$resep_pulang?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left"></td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
				<?if ($st_ginjal){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$ginjal_ina?><br><i><?=$ginjal_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$ginjal?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
			<?}else{?>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="2%" colspan="" class="text-left">:</td>
				<td width="10%" colspan="" class="text-left"></td>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="10%" colspan="" class="text-left"></td>
			<?}?>
				
			</tr>
		<?}?>
    

	</table>
	
    <?if ($st_catatan=='1'){?>
	<table class="content">
			<tr>
        <td width="18%" class="text-left text-bold"><?=$catatan_ina?><br><i><?=$catatan_eng?></i></td>
		<td width="2%" colspan="" class="text-left">:</td>
        <td width="80%" class="text-left"><?=$catatan_resep?></td>
      </tr>
    </table>
    <?}?>
	<br>
	<br>
	<?if($list_obat){?>
	<table class="content">
		<? foreach($list_obat as $r){ ?>
		<tr>
			<td width="85%" colspan="" class="text-left baris_bawah">
				<span><b>R/ <?=$r->nama_barang?></b></span><br>
				<?=($st_interval?$r->nama_interval:'')?> <?=($st_dosis?$r->dosis.' '.$r->satuan_dosis:'')?> <?=($st_rute?$r->nama_rute:'')?> <?=($st_aturan?$r->aturan_tambahan:'')?> <?=($st_waktu?$r->waktu_minum:'')?> <?=($r->jumlah_setiap_pemberian?$r->jumlah_setiap_pemberian:'')?> 
				<?=($r->satuan_label?$r->satuan_label:'')?>
			</td>
			<td width="15%" colspan="" class="text-left">
				<b>No. <?=$r->jumlah?></b><br>
				<?=($r->iter?'Iter '.$r->iter.' X':'') ?>
				<?
				if ($st_original!='1'){
					if ($assesmen_id_resep){
						$ket_orig='';
					}else{
						if ($status_tindakan< 2){
							$ket_orig=' (orig)';
						}else{
							$ket_orig='';
							
						}
					}
					
					if ($r->st_ambil=='2'){
						echo '<br>nedet';
					}else{
						if ($r->kuantitas==0){
							echo '<br>nedet';
						}else{
							if ($r->kuantitas != $r->jumlah){
								echo '<br>det '.$r->kuantitas.$ket_orig;;
							}else{
								
								echo '<br>det '.$ket_orig;
								
							}
						}
					}
				}
					
				?>
			</td>
		</tr>
		<?}?>
	</table>
	<?}?>
	<br>
	<?
	
	if($list_obat_racikan){
		$racikan_id='';
		?>
	<table class="content">
		<? foreach($list_obat_racikan as $row){ ?>
			<tr>
				<td width="85%" colspan="2" class="text-left baris_bawah">
					<span><b><?=($st_nama_racikan?$row->nama_racikan:'')?></b></span>
				</td>
				<td width="15%" colspan="" class="text-left">
					<b><?=($st_qty?'No. '.$row->jumlah_racikan:'')?></b><br>
					<?=($row->iter?'Iter '.$row->iter.' X':'') ?>
				</td>
			</tr>
				
				
			<?
			$racikan_id =$row->racikan_id;
			$urutan=1;
			$q="SELECT 
					B.nama as nama_barang,B.hargadasar
					,O.idtipe,O.idbarang,O.nama,O.dosis,O.jumlah,O.p1,O.p2,SL.nama as satuan_label
					,SD.nama as satuan_dosis,SK.nama as satuan_kecil
					,BS.ref sediaan_nama
					FROM tpoliklinik_e_resep_obat_racikan_obat O
					LEFT JOIN view_barang_farmasi_resep B ON B.id=O.idbarang AND B.idtipe=O.idtipe
					LEFT JOIN merm_referensi BS ON BS.nilai=B.bentuk_sediaan AND BS.ref_head_id='94'
					LEFT JOIN msatuan SL ON SL.id=B.idsatuanlabel
					LEFT JOIN msatuan SD ON SD.id=B.idsatuandosis
					LEFT JOIN msatuan SK ON SK.id=B.idsatuankecil
					WHERE O.racikan_id='$racikan_id' 
					ORDER BY O.racikan_id ASC";
			$list_detail=$this->db->query($q)->result();
			?>
			<?foreach($list_detail as $r){?>
			<tr>
				<td width="23%" colspan="" class="text-left">
					<span><b><?=($urutan=='1'?'&nbsp;&nbsp; R/':'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;')?> <?=$r->nama_barang?></b></span>
				</td>
				<td width="22%" colspan="" class="text-left">
					<b><?=($st_qty_obat?'No. '.$r->jumlah:'') ?> | <?=($st_pembagian?$r->p1.' / '.$r->p2.' '.$r->satuan_kecil:'') ?> <?=($st_dosis_rac?$r->dosis.' '.$r->satuan_dosis:'')?></b> 
				</td>
			</tr>
			<?$urutan +=1;}?>
			<tr>
				<td width="95%" colspan="2" class="text-left baris_bawah">
					<?=($st_jenis_rac?$row->racikan_des:'')?> <b><?=($st_qty?'No. '.$row->jumlah_racikan:'')?></b>
					<?=($st_interval_rac?$row->nama_interval:'')?> <?=($row->jenis_racik_nama?$row->jenis_racik_nama:'')?> 
					<?=($st_rute_rac?$row->nama_rute:'')?> <?=($st_aturan?$row->aturan_tambahan:'')?> <br>
				</td>
				<td width="5%" colspan="" class="text-left">
					
				</td>
			</tr>
		<?}?>
	</table>
	<?}?>
	<br>
	<table class="content">
		<tr>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center wrap-text">TANDA TANGAN</td>
		</tr>
		<tr>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center wrap-text">PEMBERI RESEP</td>
		</tr>
		
		
		<tr>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center">
				<img class="" style="width:100px" src="<?=base_url()?>qrcode/qr_code_ppa/<?=$ppa_resep?>" alt="" title="">
				
			</td>
		</tr>
		<tr>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center wrap-text"><?=$nama_pemberi_rsep?><br><?=$nik_dokter?></td>
		</tr>
		<?if ($st_original!='1'){?>
		<?if ($st_penerima=='1'){?>
		<tr>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center wrap-text">PENERIMA</td>
		</tr>
		
		
		<tr>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center">
				<img class="" style="width:100px" src="<?=$penerima_ttd?>" alt="" title="">
				
			</td>
		</tr>
		<tr>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center wrap-text"><?=($nama_penerima?$nama_penerima:$namapasien)?></td>
		</tr>
		<?}?>
		<tr>
			<td width="20%" class="text-center"><?=($user_proses && $st_konfirmas?$konfirmas_ina.'<br>'.$konfirmas_eng:'')?></td>
			<td width="20%" class="text-center"><?=($user_ambil && $st_ambil?$ambil_ina.'<br>'.$ambil_eng:'')?></td>
			<td width="20%" class="text-center"><?=($user_etiket && $st_etiket?$etiket_ina.'<br>'.$etiket_eng:'')?></td>
			<td width="20%" class="text-center"><?=($user_validasi && $st_validasi?$validasi_ina.'<br>'.$validasi_eng:'')?></td>
			<td width="20%" class="text-center wrap-text"><?=($user_penyerah && $st_penyerah?$penyerah_ina.'<br>'.$penyerah_eng:'')?></td>
		</tr>
		<tr>
			<td width="20%" class="text-center">
			<?if ($st_konfirmas){?>
			<?if ($user_proses){?>
			<img class="" style="width:100px" src="<?=base_url()?>qrcode/qr_code_ppa/<?=$user_proses?>" alt="" title="">
			<?}?>
			<?}?>
			</td>
			<td width="20%" class="text-center">
			<?if ($st_ambil){?>
			<?if ($user_ambil){?>
			<img class="" style="width:100px" src="<?=base_url()?>qrcode/qr_code_ppa/<?=$user_ambil?>" alt="" title="">
			<?}?>
			<?}?>
			</td>
			<td width="20%" class="text-center">
			<?if ($st_etiket){?>
			<?if ($user_etiket){?>
			<img class="" style="width:100px" src="<?=base_url()?>qrcode/qr_code_ppa/<?=$user_etiket?>" alt="" title="">
			<?}?>
			<?}?>
			</td>
			<td width="20%" class="text-center">
			<?if ($st_validasi){?>
			<?if ($user_validasi){?>
			<img class="" style="width:100px" src="<?=base_url()?>qrcode/qr_code_ppa/<?=$user_validasi?>" alt="" title="">
			<?}?>
			<?}?>
			</td>
			<td width="20%" class="text-center">
			<?if ($st_penyerah){?>
			<?if ($user_penyerah){?>
			<img class="" style="width:100px" src="<?=base_url()?>qrcode/qr_code_ppa/<?=$user_penyerah?>" alt="" title="">
			<?}?>
			<?}?>
			</td>
		</tr>
		<tr>
			<td width="20%" class="text-center"><?=($user_proses && $st_konfirmas ?get_nama_ppa($user_proses):'')?></td>
			<td width="20%" class="text-center"><?=($user_ambil && $st_ambil ?get_nama_ppa($user_ambil):'')?></td>
			<td width="20%" class="text-center"><?=($user_etiket && $st_etiket?get_nama_ppa($user_etiket):'')?></td>
			<td width="20%" class="text-center"><?=($user_validasi && $st_validasi?get_nama_ppa($user_validasi):'')?></td>
			<td width="20%" class="text-center"><?=($user_penyerah && $st_penyerah?get_nama_ppa($user_penyerah):'')?></td>
			
		</tr>
		<?}?>
		<tr>
			<td colspan="5"><p> <i><?=($st_user?'Printed By : ':'')?><?=($st_user?$login_nama_ppa:'')?> | <?=($st_tanggal_cetak?HumanDateShort(date('Y-m-d')):'')?> <?=($st_waktu_cetak?HumanTime(date('H:i:s')):'')?>  <?=($st_jml_cetak?' | Number Printed : ':'')?> <?=($st_jml_cetak?$printed_number:'')?></i></p></td>
		</tr>
	</table>
	<br>
	<br>
	<?if ($st_original!='1'){?>
	<?if ($st_hasil){?>
	<p style="page-break-before: always;">
	<table class="content">
		<tr>
			<td width="18%" rowspan="2" class="text-center"><img src="<?=base_url()?>assets/upload/antrian/<?=$header_logo?>" alt="" width="100" height="100"></td>
			<td width="80%" class="text-judul text-left" style="vertical-align: middle;"><?=$nama_rumah_sakit?></td>
		</tr>
		<tr><td  class="text-left text-normal"><?=$alamat?><br><?=$telepone?><br><?=$website?> Email : <?=$email?></td></tr>
		
		
	</table>
	
	<br>
	<br>
	<table class="content">
		<tr>
			<td width="100%" colspan="8" class="text-center text-judul"><?=$judul_header_ina?><br><i><?=$judul_header_eng?></i></td>
		</tr>
		
	</table>
	<br>
	<table class="content">
		<?if ($st_presciber=='1' || $st_pasien=='1'){?>
		<tr>
			<?if ($st_presciber){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$presciber_ina?><br><i><?=$presciber_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$dokter?></td>
			<?}?>
			
			<?if ($st_pasien){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$pasien_ina?><br><i><?=$pasien_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$namapasien?> | <?=$no_medrec?> | <?=$jeniskelamin?></td>
			<?}?>
		</tr>
		<?}?>
		<?if ($st_noreg=='1' || $st_tl=='1'){?>
		<tr>
			<?if ($st_noreg){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$noreg_ina?><br><i><?=$noreg_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>s
			<td width="30%" colspan="" class="text-left"><?=$nopendaftaran?> | <?=HumanDateShort($tanggal)?></td>
			<?}?>
			
			<?if ($st_tl){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$tl_ina?><br><i><?=$tl_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=HumanDateShort($tanggal_lahir)?> | <?=$umur?></td>
			<?}?>
		</tr>
		<?}?>
		<?if ($st_noresep=='1' || $st_jenis_pasien=='1'){?>
		<tr>
			<?if ($st_noresep){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$noresep_ina?><br><i><?=$noresep_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$noresep?> | <?=HumanDateLong($created_date)?> </td>
			<?}?>
			
			<?if ($st_jenis_pasien){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$jenis_pasien_ina?><br><i><?=$jenis_pasien_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$nama_kelompok?> <?=($idkelompokpasien=='1'?' | '.$nama_rekanan:'')?></td>
			<?}?>
		</tr>
		<?}?>
		<?if ($st_alergi_setting=='1' || $st_diagnosa=='1'){?>
		<tr>
			<?if ($st_alergi_setting){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$alergi_ina?><br><i><?=$alergi_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$alergi?></td>
			<?}?>
			
			<?if ($st_diagnosa){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$diagnosa_ina?><br><i><?=$diagnosa_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$diagnosa?></td>
			<?}?>
		</tr>
		<?}?>
	</table>
	<table class="content">
		<?if ($st_bb=='1' || $st_tb=='1' || $st_luas_permukaan=='1' || $st_menyusui=='1'){?>
			<tr>
			<?if ($st_bb=='1' || $st_tb=='1'){?>
				<?if ($st_bb=='1'){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$bb_ina?><br><i><?=$bb_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$berat_badan?> Kg</td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
				<?if ($st_tb=='1'){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$tb_ina?><br><i><?=$tb_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$tinggi_badan?> CM</td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
			<?}else{?>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="2%" colspan="" class="text-left">:</td>
				<td width="10%" colspan="" class="text-left"></td>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="10%" colspan="" class="text-left"></td>
			<?}?>
				
			<?if ($st_luas_permukaan=='1' || $st_menyusui=='1'){?>
				<?if ($st_luas_permukaan){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$luas_permukaan_ina?><br><i><?=$luas_permukaan_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$luas_permukaan?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left"></td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
				<?if ($st_menyusui){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$menyusui_ina?><br><i><?=$menyusui_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$menyusui?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left"></td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
			<?}else{?>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="2%" colspan="" class="text-left"></td>
				<td width="10%" colspan="" class="text-left"></td>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="10%" colspan="" class="text-left"></td>
			<?}?>
				
			</tr>
		<?}?>
		
		<?if ($st_puasa_setting=='1' || $st_prioritas=='1' || $st_luas_permukaan=='1' || $st_menyusui=='1'){?>
			<tr>
			<?if ($st_puasa_setting=='1' || $st_prioritas=='1'){?>
				<?if ($st_puasa_setting=='1'){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$puasa_ina?><br><i><?=$puasa_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$puasa?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
				<?if ($st_prioritas=='1'){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$prioritas_ina?><br><i><?=$prioritas_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$prioritas?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
			<?}else{?>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="2%" colspan="" class="text-left">:</td>
				<td width="10%" colspan="" class="text-left"></td>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="10%" colspan="" class="text-left"></td>
			<?}?>
				
			<?if ($st_pulang=='1' || $st_ginjal=='1'){?>
				<?if ($st_pulang){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$pulang_ina?><br><i><?=$pulang_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$resep_pulang?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left"></td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
				<?if ($st_ginjal){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$ginjal_ina?><br><i><?=$ginjal_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$ginjal?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
			<?}else{?>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="2%" colspan="" class="text-left">:</td>
				<td width="10%" colspan="" class="text-left"></td>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="10%" colspan="" class="text-left"></td>
			<?}?>
				
			</tr>
		<?}?>
    

	</table>
	
  <?if ($st_catatan=='1'){?>
	<table class="content">
			<tr>
        <td width="18%" class="text-left text-bold"><?=$catatan_ina?><br><i><?=$catatan_eng?></i></td>
		<td width="2%" colspan="" class="text-left">:</td>
        <td width="80%" class="text-left"><?=$catatan_resep?></td>
      </tr>
    </table>
    <?}?>
	<br>
	<table class="content">
	<tr>
		<td width="49%" class="text-judul text-primary">
			<strong>TELAAH RESEP</strong>
		</td>
		<td width="2%">
		
		</td>
		<td width="49%"  class="text-judul text-primary">
			<strong>TELAAH OBAT</strong>
		</td>
	</tr>
	<tr>
		<td width="49%">
			<table class="table table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center border-full" style="width: 5%;">NO</th>
						<th class="text-center border-full" style="width: 70%;">ASPEK TELAAH</th>
						<th class="text-center border-full" style="width: 25%;">HASIL TELAAH</th>
						
					</tr>
				</thead>
				<tbody>
					<?
					foreach($list_telaah as $r){?>
						<tr>
						<?if ($r->lev=='0'){?>
							<td class=" border-full" colspan="3"><strong><?=$r->nama?></strong></td>
						<?}else{?>
							<td class="text-center border-full"><?=$r->nourut_no?><input type="hidden" class="telaah_id" value="<?=$r->id?>"></td>
							<td class=" border-full"><?=$r->nama?></td>
							<td class="text-center border-full">
							<?=get_nama_ref($r->jawaban_resep,109)?>
								
							</td>
						<?}?>
						</tr>
					<?}?>
				</tbody>
			</table>
		</td>
		<td width="2%"></td>
		<td width="49%">
			<table class="table table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center border-full" style="width: 5%;">NO</th>
						<th class="text-center border-full" style="width: 70%;">ASPEK TELAAH</th>
						<th class="text-center border-full" style="width: 25%;">HASIL TELAAH</th>
						
					</tr>
				</thead>
				<tbody>
					<?
					foreach($list_telaah_obat as $r){?>
						<tr>
						<?if ($r->lev=='0'){?>
							<td class=" border-full" colspan="3"><strong><?=$r->nama?></strong></td>
						<?}else{?>
							<td class="text-center border-full"><?=$r->nourut_no?><input type="hidden" class="telaah_id" value="<?=$r->id?>"></td>
							<td class=" border-full"><?=$r->nama?></td>
							<td class="text-center border-full">
							<?=get_nama_ref($r->jawaban_resep,109)?>
								
							</td>
						<?}?>
						</tr>
					<?}?>
				</tbody>
			</table>
		</td>
	</tr>
	<tr>
		<td width="49%" class="text-center">
		<?if($user_telaah_resep){?>
			Bandung, <?=(HumanDateShort($tanggal_telaah_resep))?>
			<br>Yang Melakukan Telaah Resep
		<?}?>
		</td>
		<td width="2%">
		
		</td>
		<td width="49%"  class="text-center">
			<?if($user_validasi){?>
			Bandung, <?=(HumanDateShort($tanggal_validasi))?>
			<br>Yang Melakukan Telaah Obat
		<?}?>
		</td>
	</tr>
	<tr>
		<td width="49%" class="text-center">
		<?if($user_telaah_resep){?>
			<img class="" style="width:100px" src="<?=base_url()?>qrcode/qr_code_ppa/<?=$user_telaah_resep?>" alt="" title="">
		<?}?>
			
		</td>
		<td width="2%">
		
		</td>
		<td width="49%"  class="text-center">
			<?if($user_validasi){?>
			<img class="" style="width:100px" src="<?=base_url()?>qrcode/qr_code_ppa/<?=$user_validasi?>" alt="" title="">
			<?}?>
		</td>
	</tr>
	<tr>
		<td width="49%" class="text-center">
		<?if($user_telaah_resep){?>
			<?=($user_telaah_resep?get_nama_ppa($user_telaah_resep):'')?>
		<?}?>
			
		</td>
		<td width="2%">
		
		</td>
		<td width="49%"  class="text-center">
			<?if($user_validasi){?>
			<?=($user_validasi?get_nama_ppa($user_validasi):'')?>
			<?}?>
		</td>
	</tr>
   </table>
   <?}?>
	<?if ($st_perubahan && $list_obat_perubahan){?>
	<p style="page-break-before: always;">
	<table class="content">
		<tr>
			<td width="18%" rowspan="2" class="text-center"><img src="<?=base_url()?>assets/upload/antrian/<?=$header_logo?>" alt="" width="100" height="100"></td>
			<td width="80%" class="text-judul text-left" style="vertical-align: middle;"><?=$nama_rumah_sakit?></td>
		</tr>
		<tr><td  class="text-left text-normal"><?=$alamat?><br><?=$telepone?><br><?=$website?> Email : <?=$email?></td></tr>
		
		
	</table>
	
	<br>
	<br>
	<table class="content">
		<tr>
			<td width="100%" colspan="8" class="text-center text-judul"><?=$judul_header_ina?><br><i><?=$judul_header_eng?></i></td>
		</tr>
		
	</table>
	<br>
	<table class="content">
		<?if ($st_presciber=='1' || $st_pasien=='1'){?>
		<tr>
			<?if ($st_presciber){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$presciber_ina?><br><i><?=$presciber_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$dokter?></td>
			<?}?>
			
			<?if ($st_pasien){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$pasien_ina?><br><i><?=$pasien_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$namapasien?> | <?=$no_medrec?> | <?=$jeniskelamin?></td>
			<?}?>
		</tr>
		<?}?>
		<?if ($st_noreg=='1' || $st_tl=='1'){?>
		<tr>
			<?if ($st_noreg){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$noreg_ina?><br><i><?=$noreg_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>s
			<td width="30%" colspan="" class="text-left"><?=$nopendaftaran?> | <?=HumanDateShort($tanggal)?></td>
			<?}?>
			
			<?if ($st_tl){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$tl_ina?><br><i><?=$tl_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=HumanDateShort($tanggal_lahir)?> | <?=$umur?></td>
			<?}?>
		</tr>
		<?}?>
		<?if ($st_noresep=='1' || $st_jenis_pasien=='1'){?>
		<tr>
			<?if ($st_noresep){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$noresep_ina?><br><i><?=$noresep_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$noresep?> | <?=HumanDateLong($created_date)?> </td>
			<?}?>
			
			<?if ($st_jenis_pasien){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$jenis_pasien_ina?><br><i><?=$jenis_pasien_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$nama_kelompok?> <?=($idkelompokpasien=='1'?' | '.$nama_rekanan:'')?></td>
			<?}?>
		</tr>
		<?}?>
		<?if ($st_alergi_setting=='1' || $st_diagnosa=='1'){?>
		<tr>
			<?if ($st_alergi_setting){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$alergi_ina?><br><i><?=$alergi_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$alergi?></td>
			<?}?>
			
			<?if ($st_diagnosa){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$diagnosa_ina?><br><i><?=$diagnosa_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$diagnosa?></td>
			<?}?>
		</tr>
		<?}?>
	</table>
	<table class="content">
		<?if ($st_bb=='1' || $st_tb=='1' || $st_luas_permukaan=='1' || $st_menyusui=='1'){?>
			<tr>
			<?if ($st_bb=='1' || $st_tb=='1'){?>
				<?if ($st_bb=='1'){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$bb_ina?><br><i><?=$bb_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$berat_badan?> Kg</td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
				<?if ($st_tb=='1'){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$tb_ina?><br><i><?=$tb_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$tinggi_badan?> CM</td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
			<?}else{?>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="2%" colspan="" class="text-left">:</td>
				<td width="10%" colspan="" class="text-left"></td>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="10%" colspan="" class="text-left"></td>
			<?}?>
				
			<?if ($st_luas_permukaan=='1' || $st_menyusui=='1'){?>
				<?if ($st_luas_permukaan){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$luas_permukaan_ina?><br><i><?=$luas_permukaan_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$luas_permukaan?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left"></td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
				<?if ($st_menyusui){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$menyusui_ina?><br><i><?=$menyusui_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$menyusui?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left"></td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
			<?}else{?>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="2%" colspan="" class="text-left"></td>
				<td width="10%" colspan="" class="text-left"></td>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="10%" colspan="" class="text-left"></td>
			<?}?>
				
			</tr>
		<?}?>
		
		<?if ($st_puasa_setting=='1' || $st_prioritas=='1' || $st_luas_permukaan=='1' || $st_menyusui=='1'){?>
			<tr>
			<?if ($st_puasa_setting=='1' || $st_prioritas=='1'){?>
				<?if ($st_puasa_setting=='1'){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$puasa_ina?><br><i><?=$puasa_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$puasa?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
				<?if ($st_prioritas=='1'){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$prioritas_ina?><br><i><?=$prioritas_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$prioritas?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
			<?}else{?>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="2%" colspan="" class="text-left">:</td>
				<td width="10%" colspan="" class="text-left"></td>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="10%" colspan="" class="text-left"></td>
			<?}?>
				
			<?if ($st_pulang=='1' || $st_ginjal=='1'){?>
				<?if ($st_pulang){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$pulang_ina?><br><i><?=$pulang_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$resep_pulang?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left"></td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
				<?if ($st_ginjal){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$ginjal_ina?><br><i><?=$ginjal_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$ginjal?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
			<?}else{?>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="2%" colspan="" class="text-left">:</td>
				<td width="10%" colspan="" class="text-left"></td>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="10%" colspan="" class="text-left"></td>
			<?}?>
				
			</tr>
		<?}?>
    

	</table>
	
  <?if ($st_catatan=='1'){?>
	<table class="content">
			<tr>
        <td width="18%" class="text-left text-bold"><?=$catatan_ina?><br><i><?=$catatan_eng?></i></td>
		<td width="2%" colspan="" class="text-left">:</td>
        <td width="80%" class="text-left"><?=$catatan_resep?></td>
      </tr>
    </table>
    <?}?>
	<br>
		<?
		$i_perubahan=0;
		foreach($list_obat_perubahan as $r){?>
			<?
					$i_perubahan ++;
				$id=$r->id;
				$q="
					SELECT C.nama as nama_current,C.dosis as dosis_current,C.jumlah as jumlah_current,C.`interval` as interval_current,C.rute as rute_current,C.aturan_tambahan as aturan_tambahan_current
					,MRC.ref as nama_rute_current,MIC.nama as nama_interval_current
					,MRO.ref as nama_rute_original,MIO.nama as nama_interval_original
					,UE.nama as nama_pengaju,UE.nip as nip_pengaju,C.alasan_perubahan
					,US.nama as nama_setuju,US.nip as nip_setuju
					,C.user_perubahan,C.user_setuju
					,O.nama as nama_original,O.dosis as dosis_original,O.jumlah as jumlah_original,O.`interval` as interval_original,O.rute as rute_original,O.aturan_tambahan as aturan_tambahan_original
					,C.st_setuju,C.st_perubahan
					FROM `tpoliklinik_e_resep_obat` C 
					LEFT JOIN tpoliklinik_e_resep_obat_original O ON C.id=O.id
					LEFT JOIN mppa UE ON UE.id=C.user_perubahan
					LEFT JOIN mppa US ON US.id=C.user_setuju
					LEFT JOIN merm_referensi MRC ON MRC.nilai=C.rute AND MRC.ref_head_id='74'
					LEFT JOIN minterval MIC ON MIC.id=C.`interval` 
					LEFT JOIN merm_referensi MRO ON MRO.nilai=O.rute AND MRO.ref_head_id='74'
					LEFT JOIN minterval MIO ON MIO.id=O.`interval` 
					WHERE C.id='$id'
				";
				$data=$this->db->query($q)->row();
				$label_perubahan='';
				$alasan_perubahan=$data->alasan_perubahan;
				if ($data->st_setuju=='0'){
					$label_perubahan='MENUNGGU';
				}
				if ($data->st_setuju=='1'){
					$label_perubahan='DISETUJUI';
				}
				if ($data->st_setuju=='2'){
					$label_perubahan='DITOLAK';
				}
				$tabel_1='';
				if ($data){
					$tabel_1 .='<tr>';
					$tabel_1 .='<td class="border-full text-left">'.$data->nama_original.'</td>';
					$tabel_1 .='<td class="border-full text-center">'.$data->dosis_original.'</td>';
					$tabel_1 .='<td class="border-full text-center">'.$data->jumlah_original.'</td>';
					$tabel_1 .='<td class="border-full text-center">'.$data->nama_interval_original.'</td>';
					$tabel_1 .='<td class="border-full text-center">'.$data->nama_rute_original.'</td>';
					$tabel_1 .='<td class="border-full text-center">'.$data->aturan_tambahan_original.'</td>';
					$tabel_1 .='</tr>';
				}
				$tabel_2='';
				if ($data){
					$tabel_2 .='<tr>';
					$tabel_2 .='<td class="border-full '.($data->nama_original<>$data->nama_current?'text-danger':'').'"><strong>'.$data->nama_current.'</strong></td>';
					$tabel_2 .='<td class="border-full '.($data->dosis_original<>$data->dosis_current?'text-danger':'').'"><strong>'.$data->dosis_current.'</strong></td>';
					$tabel_2 .='<td class="border-full '.($data->jumlah_original<>$data->jumlah_current?'text-danger':'').'"><strong>'.$data->jumlah_current.'</strong></td>';
					$tabel_2 .='<td class="border-full '.($data->nama_interval_original<>$data->nama_interval_current?'text-danger':'').'"><strong>'.$data->nama_interval_current.'</strong></td>';
					$tabel_2 .='<td class="border-full '.($data->nama_rute_original<>$data->nama_rute_current?'text-danger':'').'"><strong>'.$data->nama_rute_current.'</strong></td>';
					$tabel_2 .='<td class="border-full '.($data->aturan_tambahan_original<>$data->aturan_tambahan_current?'text-danger':'').'"><strong>'.$data->aturan_tambahan_current.'</strong></td>';
					$tabel_2 .='</tr>';
				}
				$nama_setuju=$data->nama_setuju.'<br>'.$data->nip_setuju;
				$nama_pengaju=$data->nama_pengaju.'-'.$data->nip_pengaju;
				$ttd_setuju='';
				$ttd_pengaju='';
				if ($data->user_setuju){
					$ttd_setuju='<img class="" style="width:100px;height:100px; text-align: center;" src="'.base_url().'qrcode/qr_code_ttd/'.$data->user_setuju.'" alt="" title="">';
				}
				if ($data->user_perubahan){
					$ttd_pengaju='<img style="width:100px;height:100px; text-align: center;" src="'.base_url().'qrcode/qr_code_ttd/'.$data->user_perubahan.'" alt="" title="">';
				}
				$tabel_3='<tr>';
				$tabel_3 .='<td>';
				$tabel_3 .='<div class="block-content block-content-full text-center">
							<div>
								'.$ttd_pengaju.'
							</div>
							<div class="h5 push-18-t push-5">'.($data->user_perubahan?$data->nama_pengaju:'').'</div>
							<div class="text-muted">'.($data->user_perubahan?$data->nip_pengaju:'').'</div>
						</div>';
				$tabel_3 .='</td><td></td><td>';
				$tabel_3 .='<div class="block-content block-content-full text-center">
							<div>
								'.$ttd_setuju.'
							</div>
							<div class="h5 push-18-t push-5">'.($data->nama_setuju?$data->nama_setuju:'').'</div>
							<div class="text-muted">'.($data->nama_setuju?$data->nip_setuju:'').'</div>
						</div></td></tr>';
			
			?>
			<label class="text-judul text-primary">PERUBAHAN OBAT KE <?=$i_perubahan?></label>
			<table class="content border-full">
				<tr>
					<td width="80%" class="text-center text-judul"><strong>STATUS PERUBAHAN</strong></td>
				</tr>
				<tr>
					<td width="80%"><strong>STATUS PERUBAHAN</strong></td>
				</tr>
				<tr>
					<td width="80%"><i><?=$label_perubahan?></i></td>
				</tr>
				
				<tr>
					<td width="80%"><strong>TERTULIS</strong></td>
				</tr>
			
				<tr>
					<td width="80%">
				
						<table class="table table-bordered" id="tabel_awal">
							<thead>
								<tr>
									<th class="text-center border-full" style="width:50%">NAMA OBAT</th>
									<th class="text-center border-full" style="width:10%">DOSIS</th>
									<th class="text-center border-full" style="width:10%">JUMLAH</th>
									<th class="text-center border-full" style="width:10%">FREKUENSI / INTERVAL</th>
									<th class="text-center border-full" style="width:10%">RUTE PEMBERIAN</th>
									<th class="text-center border-full" style="width:10%">ATURAN TAMBAHAN</th>
								</tr>
							</thead>
							<tbody>
							   <?=$tabel_1?>
							</tbody>
						</table>
					<</td>
				</tr>
				<tr>
					<td width="80%"><strong>MENJADI</strong></td>
				</tr>
				<tr>
					<td width="80%">
				
						<table class="table table-bordered" id="tabel_awal">
							<thead>
								<tr>
									<th class="text-center border-full" style="width:50%">NAMA OBAT</th>
									<th class="text-center border-full" style="width:10%">DOSIS</th>
									<th class="text-center border-full" style="width:10%">JUMLAH</th>
									<th class="text-center border-full" style="width:10%">FREKUENSI / INTERVAL</th>
									<th class="text-center border-full" style="width:10%">RUTE PEMBERIAN</th>
									<th class="text-center border-full" style="width:10%">ATURAN TAMBAHAN</th>
								</tr>
							</thead>
							<tbody>
							   <?=$tabel_2?>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td width="80%"><strong>ALASAN PERUBAHAN</strong></td>
				</tr>
				<tr>
					<td width="80%"><i><?=$alasan_perubahan?></i></td>
				</tr>
				<tr>
					<td width="80%">
						<table class="table" id="tabel_ttd">
							<thead>
								<tr>
									<th class="text-center" style="30%">PETUGAS FARMASI</th>
									<th class="text-center" style="30%"></th>
									<th class="text-center" style="34%">DISETUJUI DOKTER</th>
								</tr>
							</thead>
							<tbody>
							   <?=$tabel_3?>
							</tbody>
						</table>
					</td>
				</tr>
				
			</table>
			
	   <?}?>
   <?}?>
   <?}?>
	</main>
</body>

</html>
