<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title><?=$title?> <?=$ukuran?></title>
	<style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
		@page {
			size: <?=$ukuran?>;
            margin-top: 1.8em;
            margin-left: 1.2em;
            margin-right: 1em;
            margin-bottom: 2.5em;
        }
	 body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-family: "Courier", Verdana, sans-serif;
        font-size: 15px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }

      th {
        padding: 0px;
      }
	  td {
        padding: 0px;
      }
      .content th {
        padding: 0px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
	  .border-dotted {
        border: 1px;#000 !important;
        
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px; #000 !important;
        border-bottom-style: dotted;
      }

      

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }

      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier", Verdana, sans-serif;
        font-size: 15px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		margin:0;
		padding:0;
		 vertical-align: middle !important;
      }
      th {
        padding: 0px;

      }
	  td {
        padding: 0px;

      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
		vertical-align:top;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .baris_bawah {
        border-bottom:1px dashed #000 !important;
      }
	  .text-normal{
		font-size: 14px !important;
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
        font-weight: bold;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

     

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 20px 0;
		}
		fieldset {
			border:1px solid #000;
			border-radius:4px;
			box-shadow:0 0 0px #000;
			margin-top:-2px;
		}
		legend {
			background:#fff;
		}
    }
	.wrap-text { 
		white-space: normal; 
	  } 
	i{
		font-weight: normal;
	}
	.text-primary {
		color: #337ab7;
	}
  </style>
</head>

<body>
	<header>
		
	</header>
	
	<main>
	<table class="content">
		<tr>
			<td width="15%" rowspan="2" class="text-center"><img src="<?=base_url()?>assets/upload/antrian/<?=$header_logo?>" alt="" width="100" height="100"></td>
			<td width="80%" class="text-judul text-left" style="vertical-align: middle;"><?=$nama_rumah_sakit?></td>
		</tr>
		<tr><td  class="text-left text-normal"><?=$alamat?><br><?=$telepone?><br><?=$website?> Email : <?=$email?></td></tr>
		
		
	</table>
	
	<br>
	<table class="content">
		
		<tr>
			<td  class="text-left text-normal" width="10%">Apoteker </td>
			<td  class="text-left text-normal" width="90%">:&nbsp;&nbsp; <?=$apoteker?></td>
		</tr>
		<tr>
			<td  class="text-left text-normal" width="10%">SIPA</td>
			<td  class="text-left text-normal" width="90%">:&nbsp;&nbsp; <?=$sipa?></td>
		</tr>
		
		
	</table>
	<br>
	<table class="content">
		<tr>
			<td width="100%" colspan="8" class="text-center text-judul"><?=$judul_header_ina?><br><i><?=$judul_header_eng?></i></td>
		</tr>
		
	</table>
	<br>
	<table class="content">
		<?if ($st_presciber=='1' || $st_pasien=='1'){?>
		<tr>
			<?if ($st_presciber){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$presciber_ina?><br><i><?=$presciber_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$dokter?></td>
			<?}?>
			
			<?if ($st_pasien){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$pasien_ina?><br><i><?=$pasien_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$namapasien?> | <?=$no_medrec?> | <?=$jeniskelamin?></td>
			<?}?>
		</tr>
		<?}?>
		<?if ($st_noreg=='1' || $st_tl=='1'){?>
		<tr>
			<?if ($st_noreg){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$noreg_ina?><br><i><?=$noreg_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>s
			<td width="30%" colspan="" class="text-left"><?=$nopendaftaran?> | <?=HumanDateShort($tanggal)?></td>
			<?}?>
			
			<?if ($st_tl){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$tl_ina?><br><i><?=$tl_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=HumanDateShort($tanggal_lahir)?> | <?=$umur?></td>
			<?}?>
		</tr>
		<?}?>
		<?if ($st_noresep=='1' || $st_jenis_pasien=='1'){?>
		<tr>
			<?if ($st_noresep){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$noresep_ina?><br><i><?=$noresep_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$noresep?> | <?=HumanDateLong($created_date)?> </td>
			<?}?>
			
			<?if ($st_jenis_pasien){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$jenis_pasien_ina?><br><i><?=$jenis_pasien_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$nama_kelompok?> <?=($idkelompokpasien=='1'?' | '.$nama_rekanan:'')?></td>
			<?}?>
		</tr>
		<?}?>
		<?if ($st_alergi_setting=='1' || $st_diagnosa=='1'){?>
		<tr>
			<?if ($st_alergi_setting){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$alergi_ina?><br><i><?=$alergi_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$alergi?></td>
			<?}?>
			
			<?if ($st_diagnosa){?>
			<td width="18%" colspan="" class="text-left text-bold"><?=$diagnosa_ina?><br><i><?=$diagnosa_eng?></i></td>
			<td width="2%" colspan="" class="text-left">:</td>
			<td width="30%" colspan="" class="text-left"><?=$diagnosa?></td>
			<?}?>
		</tr>
		<?}?>
	</table>
	<table class="content">
		<?if ($st_bb=='1' || $st_tb=='1' || $st_luas_permukaan=='1' || $st_menyusui=='1'){?>
			<tr>
			<?if ($st_bb=='1' || $st_tb=='1'){?>
				<?if ($st_bb=='1'){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$bb_ina?><br><i><?=$bb_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$berat_badan?> Kg</td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
				<?if ($st_tb=='1'){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$tb_ina?><br><i><?=$tb_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$tinggi_badan?> CM</td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
			<?}else{?>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="2%" colspan="" class="text-left">:</td>
				<td width="10%" colspan="" class="text-left"></td>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="10%" colspan="" class="text-left"></td>
			<?}?>
				
			<?if ($st_luas_permukaan=='1' || $st_menyusui=='1'){?>
				<?if ($st_luas_permukaan){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$luas_permukaan_ina?><br><i><?=$luas_permukaan_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$luas_permukaan?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left"></td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
				<?if ($st_menyusui){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$menyusui_ina?><br><i><?=$menyusui_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$menyusui?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left"></td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
			<?}else{?>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="2%" colspan="" class="text-left"></td>
				<td width="10%" colspan="" class="text-left"></td>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="10%" colspan="" class="text-left"></td>
			<?}?>
				
			</tr>
		<?}?>
		
		<?if ($st_puasa_setting=='1' || $st_prioritas=='1' || $st_luas_permukaan=='1' || $st_menyusui=='1'){?>
			<tr>
			<?if ($st_puasa_setting=='1' || $st_prioritas=='1'){?>
				<?if ($st_puasa_setting=='1'){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$puasa_ina?><br><i><?=$puasa_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$puasa?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
				<?if ($st_prioritas=='1'){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$prioritas_ina?><br><i><?=$prioritas_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$prioritas?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
			<?}else{?>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="2%" colspan="" class="text-left">:</td>
				<td width="10%" colspan="" class="text-left"></td>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="10%" colspan="" class="text-left"></td>
			<?}?>
				
			<?if ($st_pulang=='1' || $st_ginjal=='1'){?>
				<?if ($st_pulang){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$pulang_ina?><br><i><?=$pulang_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$resep_pulang?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left"></td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
				<?if ($st_ginjal){?>
					<td width="18%" colspan="" class="text-left text-bold"><?=$ginjal_ina?><br><i><?=$ginjal_eng?></i></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"><?=$ginjal?></td>
				<?}else{?>
					<td width="18%" colspan="" class="text-left text-bold"></td>
					<td width="2%" colspan="" class="text-left">:</td>
					<td width="10%" colspan="" class="text-left"></td>
				<?}?>
			<?}else{?>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="2%" colspan="" class="text-left">:</td>
				<td width="10%" colspan="" class="text-left"></td>
				<td width="18%" colspan="" class="text-left text-bold"></td>
				<td width="10%" colspan="" class="text-left"></td>
			<?}?>
				
			</tr>
		<?}?>
    

	</table>
	
  <?if ($st_catatan=='1'){?>
	<table class="content">
			<tr>
        <td width="18%" class="text-left text-bold"><?=$catatan_ina?><br><i><?=$catatan_eng?></i></td>
		<td width="2%" colspan="" class="text-left">:</td>
        <td width="80%" class="text-left"><?=$catatan_resep?></td>
      </tr>
    </table>
    <?}?>
	<br>
	<br>
	<?if($list_obat){?>
	<table class="content">
		<? foreach($list_obat as $r){ ?>
		<tr>
			<td width="85%" colspan="" class="text-left baris_bawah">
				<span><b>R/ <?=$r->nama_barang?></b></span><br>
				<?=($st_interval?$r->nama_interval:'')?> <?=($st_dosis?$r->dosis.' '.$r->satuan_dosis:'')?> <?=($st_rute?$r->nama_rute:'')?> <?=($st_aturan?$r->aturan_tambahan:'')?> <?=($st_waktu?$r->waktu_minum:'')?> <?=($r->jumlah_setiap_pemberian?$r->jumlah_setiap_pemberian:'')?> 
				<?=($r->satuan_label?$r->satuan_label:'')?>
			</td>
			<td width="15%" colspan="" class="text-left">
				<b>No. <?=$r->jumlah?></b><br>
				<?=($r->iter?'Iter '.$r->iter.' X':'') ?>
				<?
					
						if ($assesmen_id_resep){
							$ket_orig='';
						}else{
							if ($r->iter>0){
								$ket_orig=' (orig)';
							}else{
								$ket_orig='';
								
							}
						}
						
						if ($r->st_ambil=='2'){
							echo '<br>nedet';
						}else{
							if ($r->kuantitas==0){
								echo '<br>nedet';
							}else{
								if ($r->kuantitas != $r->jumlah){
									echo '<br>det '.$r->kuantitas.$ket_orig;;
								}else{
									
									echo '<br>det '.$ket_orig;
									
								}
							}
						}
					
					
				?>
			</td>
		</tr>
		<?}?>
	</table>
	<?}?>
	<br>
	<?
	
	if($list_obat_racikan){
		$racikan_id='';
		?>
	<table class="content">
		<? foreach($list_obat_racikan as $row){ ?>
			<tr>
				<td width="90%" colspan="2" class="text-left baris_bawah">
					<span><b><?=($st_nama_racikan?$row->nama_racikan:'')?></b></span>
				</td>
				<td width="10%" colspan="" class="text-left">
					<b><?=($st_qty?'No. '.$row->jumlah_racikan:'')?></b><br>
					<?=($row->iter?'Iter '.$row->iter.' X':'') ?>
					
				</td>
			</tr>
				
				
			<?
			$racikan_id =$row->racikan_id;
			$urutan=1;
			$q="SELECT 
					B.nama as nama_barang,B.hargadasar
					,O.idtipe,O.idbarang,O.nama,O.dosis,O.jumlah,O.p1,O.p2,SL.nama as satuan_label
					,SD.nama as satuan_dosis,SK.nama as satuan_kecil
					,BS.ref sediaan_nama
					FROM tpoliklinik_e_resep_obat_racikan_obat O
					LEFT JOIN view_barang_farmasi_resep B ON B.id=O.idbarang AND B.idtipe=O.idtipe
					LEFT JOIN merm_referensi BS ON BS.nilai=B.bentuk_sediaan AND BS.ref_head_id='94'
					LEFT JOIN msatuan SL ON SL.id=B.idsatuanlabel
					LEFT JOIN msatuan SD ON SD.id=B.idsatuandosis
					LEFT JOIN msatuan SK ON SK.id=B.idsatuankecil
					WHERE O.racikan_id='$racikan_id' 
					ORDER BY O.racikan_id ASC";
			$list_detail=$this->db->query($q)->result();
			?>
			<?foreach($list_detail as $r){?>
			<tr>
				<td width="23%" colspan="" class="text-left">
					<span><b><?=($urutan=='1'?'&nbsp;&nbsp; R/':'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;')?> <?=$r->nama_barang?></b></span>
				</td>
				<td width="22%" colspan="" class="text-left">
					<b><?=($st_qty_obat?'No. '.$r->jumlah:'') ?> | <?=($st_pembagian?$r->p1.' / '.$r->p2.' '.$r->satuan_kecil:'') ?> <?=($st_dosis_rac?$r->dosis.' '.$r->satuan_dosis:'')?></b> 
				</td>
			</tr>
			<?$urutan +=1;}?>
			<tr>
				<td width="90%" colspan="2" class="text-left baris_bawah">
					<?=($st_jenis_rac?$row->racikan_des:'')?> <b><?=($st_qty?'No. '.$row->jumlah_racikan:'')?></b>
					<?=($st_interval_rac?$row->nama_interval:'')?> <?=($row->jenis_racik_nama?$row->jenis_racik_nama:'')?> 
					<?=($st_rute_rac?$row->nama_rute:'')?> <?=($st_aturan?$row->aturan_tambahan:'')?> <br>
				</td>
				<td width="10%" colspan="" class="text-left">
					
				</td>
			</tr>
		<?}?>
	</table>
	<?}?>
	<br>
	<br>
	<table class="content">
		<tr>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center wrap-text">Bandung, <?=HumanDateShort($created_date)?></td>
		</tr>
		<tr>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center">
				<img class="" style="width:100px" src="<?=base_url()?>qrcode/qr_code_ppa/<?=$login_ppa_id?>" alt="" title="">
				
			</td>
		</tr>
		<tr>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center wrap-text"><?=$login_nama_ppa?><br><?=$login_nip_ppa?></td>
		</tr>
		<?if ($st_ttd){?>
		<tr>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center"></td>
			<td width="20%" class="text-center wrap-text">P.c.c</td>
		</tr>
		<?}?>
		<tr>
			<td colspan="5"><p> <i><?=($st_user?'Printed By : ':'')?><?=($st_user?$login_nama_ppa:'')?> | <?=($st_tanggal_cetak?HumanDateShort(date('Y-m-d')):'')?> <?=($st_waktu_cetak?HumanTime(date('H:i:s')):'')?>  <?=($st_jml_cetak?' | Number Printed : ':'')?> <?=($st_jml_cetak?$printed_number:'')?></i></p></td>
		</tr>
	</table>
	
	</main>
</body>

</html>
