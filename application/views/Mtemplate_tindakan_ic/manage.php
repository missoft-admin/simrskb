<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mtemplate_tindakan_ic" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mtemplate_tindakan_ic/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group" style="margin-bottom:10px">
				<label class="col-md-2 control-label" for="nama">Nama</label>
				<div class="col-md-10">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="mtemplate_tindakan_ic_id" placeholder="mtemplate_tindakan_ic_id" name="mtemplate_tindakan_ic_id" value="{id}" required="" aria-required="true">
				</div>
			</div>
			
			<?if ($id){?>
			<div class="form-group">
				<div class="col-md-12">
					<h5><?=text_primary('TEMPLATE PEMBEDAHAN ')?></h5>
				</div>
			</div>
			<div class="form-group push-5-t">
				<div class="col-md-12 ">
					<div class="col-md-12 ">
						<div class="table-responsive">
					
							<table class="table table-bordered" id="tabel_informasi">
								<thead>
									<tr>
										<th width="5%" class="text-center">No</th>
										<th width="25%" class="text-center">Jenis Informasi / Type Of Information	</th>
										<th width="50%" class="text-center">Isi Informasi / Content Information</th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<?}?>
			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mtemplate_tindakan_ic" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){	
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
	$('.js-summernote').summernote({
		  height: 60,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
	let mtemplate_tindakan_ic_id=$("#mtemplate_tindakan_ic_id").val();
	if (mtemplate_tindakan_ic_id){
		load_index_informasi();
	}
	
})	
function load_index_informasi(){
	$("#cover-spin").show();
	let mtemplate_tindakan_ic_id=$("#mtemplate_tindakan_ic_id").val();
	
	$.ajax({
		url: '{site_url}mtemplate_tindakan_ic/load_index_informasi_tindakan_ic/',
		dataType: "json",
		type: 'POST',
		  data: {
				id:mtemplate_tindakan_ic_id,
				
		  },
		success: function(data) {
			$("#tabel_informasi tbody").empty();
			$("#tabel_informasi tbody").append(data.tabel);
			$("#cover-spin").hide();
			$('.auto_blur_tabel').summernote({
				height: 100,
			    codemirror: { // codemirror options
					theme: 'monokai'
				  },	
			  callbacks: {
				onBlur: function(contents, $editable) {
						assesmen_id=$("#assesmen_id").val();
						var tr=$(this).closest('tr');
						var informasi_id=tr.find(".informasi_id").val();
						var isi=$(this).val();
						$.ajax({
							url: '{site_url}mtemplate_tindakan_ic/update_isi_informasi_tindakan_ic/',
							dataType: "json",
							type: 'POST',
							data: {
							informasi_id:informasi_id,isi:isi,assesmen_id:assesmen_id
							},success: function(data) {

							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
							}
						});
				}
			  }
			});
				
		}
	});
}
</script>
