<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}msetting_piutang" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('msetting_piutang/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>	
			
			<div class="form-group">
				<div class="col-md-12">
					<table id="tabel_setting" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 0%;">#</th>															
								<th style="width: 20%;">Tipe Kunjungan</th>															
								<th style="width: 25%;">Jatuh Tempo Bayar</th>
								<th style="width: 25%;">Reminder</th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td></td>								
								<td>
									<select id="idtipe" disabled name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>Tipe Kunjungan</option>
										<option value="1">Poliklinik</option>
										<option value="2">IGD</option>
										<option value="3">Rawat Inap</option>
										<option value="4">ODS</option>
									</select>										
								</td>
								<td>
									<input type="text" style="width: 100%"  class="form-control number" id="jatuh_tempo" placeholder="Jatuh Tempo" name="jatuh_tempo" value="">										
								</td>								
								
								<td>
									<input type="text" style="width: 100%"  class="form-control number" id="reminder" placeholder="Reminder" name="reminder" value="">
									<input type="hidden" style="width: 100%" readonly class="form-control number" id="iddet" placeholder="Batas Waktu Batal" name="iddet" value="">
								</td>	
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_setting" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_setting" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						
				</table>
				</div>
			</div>
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		load_setting();
		clear_input_setting();

	});	
	
	function validate_add_setting()
	{
		if ($("#idtipe").val()=='#'){
			sweetAlert("Maaf...", "Tipe Kunjungan Harus diisi", "error");
			return false;
		}
		if ($("#jatuh_tempo").val()=='' || $("#jatuh_tempo").val()=='0'){
			sweetAlert("Maaf...", "Jatuh Tempo Harus diisi", "error");
			return false;
		}
		if ($("#reminder").val()=='0' || $("#reminder").val()==''){
			sweetAlert("Maaf...", "Reminder Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_setting(){
		$("#idtipe").val('#').trigger('change');		
		$("#jatuh_tempo").val('');		
		$("#reminder").val('');		
		$("#iddet").val('');		
	}
	$(document).on("click","#simpan_setting",function(){
		if (validate_add_setting()==false)return false;
		var idtipe=$("#idtipe").val();
		var jatuh_tempo=$("#jatuh_tempo").val();
		var reminder=$("#reminder").val();
		
		$.ajax({
			url: '{site_url}msetting_piutang/simpan_setting',
			type: 'POST',
			data: {
				idtipe:idtipe,jatuh_tempo:jatuh_tempo,reminder: reminder
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_setting').DataTable().ajax.reload( null, false );
					clear_input_setting();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	$(document).on("click","#clear_setting",function(){
		
		clear_input_setting();
	});
	$(document).on("click",".edit",function(){
		var table = $('#tabel_setting').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		$("#idtipe").val(table.cell(tr,0).data()).trigger('change');
		$("#jatuh_tempo").val(table.cell(tr,2).data());
		$("#reminder").val(table.cell(tr,3).data());

	});
	function load_setting(){
		var idlogic=$("#id").val();
		$('#tabel_setting').DataTable().destroy();
		var table = $('#tabel_setting').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_piutang/load_setting/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	
	
</script>
