<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Rekanan</label>
                    <div class="col-md-8">
                        <select id="idrekanan" name="idrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Perusahaan -</option>
							<?foreach($list_rekanan as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tipe</label>
                    <div class="col-md-8">
						<select id="tipe" name="tipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Tipe -</option>
							<option value="1" <?=("1" == $tipe ? 'selected' : ''); ?>>Rawat Jalan</option>
							<option value="2" <?=("2" == $tipe ? 'selected' : ''); ?>>Ranap / ODS</option>
							
						</select>
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Medrec</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_medrec" placeholder="No. Medrec" name="no_medrec" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="nama_pasien" placeholder="Nama Pasien" name="nama_pasien" value="">
                    </div>
                </div>
				
				          
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No. Penagihan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_klaim" placeholder="No. Penagihan" name="no_klaim" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Status</label>
                    <div class="col-md-8">
						<select id="st_validasi" name="status_kirim" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua Status -</option>
							<option value="0">Belum Diproses</option>
							<option value="1">Sudah Diproses</option>							
						</select>
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Metode</label>
                    <div class="col-md-8">
						<select id="metode" name="tipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua -</option>
							<option value="1">Transfer</option>
							<option value="2">Tunai</option>
							
						</select>
					</div>
                </div>
				
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Bayar</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
                            <input class="form-control" type="text" id="tanggal_tagihan" name="tanggal_tagihan" placeholder="From" value="{tanggal_tagihan}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_tagihan2" name="tanggal_tagihan2" placeholder="To" value="{tanggal_tagihan2}"/>
                        </div>
                    </div>
                </div>
               
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-4" hidden>
                        <button class="btn btn-danger text-uppercase" type="button" name="btn_verif_filter" id="btn_verif_filter" style="font-size:13px;width:100%;"><i class="fa fa-check-square-o"></i> Verifikasi By Filter</button>
                    </div>
					<div class="col-md-8" hidden>
                        <button disabled class="btn btn-primary text-uppercase" type="button" name="btn_verif_page" id="btn_verif_page" style="font-size:13px;width:100%;"><i class="fa fa-check"></i> Verifikasi Per page</button>
                    </div>
                </div>
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th hidden width="5%">TIPE</th>
                    <th hidden width="2%">ID</th>
                    <th hidden width="2%">ID</th>
                    <th width="10%">NO</th>
                    <th width="10%">TANGGAL BAYAR</th>
                    <th width="10%">NO. MEDREC / NAMA PASIEN</th>
                    <th width="10%"> NAMA REKANAN</th>
                    <th width="10%">JENIS LAYANAN</th>
                    <th width="10%">TOTAL TRX</th>
                    <th width="10%">NOMINAL</th>
                    <th width="10%">METODE / BANK</th>
                    <th width="15%">SUMBER KAS</th>
                    <th width="15%">STATUS </th>
                    <th width="15%" class="text-center">
						<div class="btn-group">
							<button class="btn btn-default" type="button">Aksi</button>
							<div class="btn-group">
								<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li>
										<a tabindex="-1" href="javascript:void(0)" class="validasi_all">Validasi All</a>
									</li>
									
								</ul>
							</div>
						</div>
					</th>
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
</div>


<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	// 	
	$(".number").number(true,0,'.',',');
	load_index();
   
});
function load_index(){
	var metode=$("#metode").val();
	var idrekanan=$("#idrekanan").val();
	var tipe=$("#tipe").val();
	var tanggal_tagihan=$("#tanggal_tagihan").val();
	var tanggal_tagihan2=$("#tanggal_tagihan2").val();
	var st_validasi=$("#st_validasi").val();
	var no_medrec=$("#no_medrec").val();
	var nama_pasien=$("#nama_pasien").val();
	var no_klaim=$("#no_klaim").val();
	
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0,1,2], "visible": false },
							{ "width": "3%", "targets": [3] },
							{ "width": "5%", "targets": [7] },
							{ "width": "8%", "targets": [4,8,9,10,11] },
							{ "width": "10%", "targets": [5,6] },
							{ "width": "12%", "targets": [12] },
						 // {"targets": [4,13], className: "text-left" },
						 {"targets": [3,8,9], className: "text-right" },
						 {"targets": [4,7,10,11,12], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tklaim_validasi/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						no_klaim:no_klaim,
						no_medrec:no_medrec,
						nama_pasien:nama_pasien,
						metode:metode,
						idrekanan:idrekanan,
						tipe:tipe,st_validasi:st_validasi,
						tanggal_tagihan2:tanggal_tagihan2,tanggal_tagihan:tanggal_tagihan,
					   }
            }
        });
}
$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_index();
});

$(document).on("click", ".verifikasi", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var tipe=table.cell(tr,0).data()
	var noreg=table.cell(tr,6).data()
	var tipekontraktor=table.cell(tr,20).data()
	var idkontraktor=table.cell(tr,21).data()
	var tanggal_jt=table.cell(tr,15).data()
	alert(idkontraktor);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Verifikasi No. Registrasi "+noreg+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		verifikasi(id,tipe,tipekontraktor,idkontraktor,tanggal_jt);
	});
});
$(document).on("click", ".validasi_all", function() {
	
	verifikasi_semua();
});
function verifikasi_semua(){
	var table = $('#index_list').DataTable();		
	 var data = table.rows().data();
	 var arr_id=[];
	 var arr_tipe=[];
	 var arr_tanggal_id=[];
	 var arr_idpengaturan=[];
	 var arr_tanggal_jt=[];
	 data.each(function (value, index) {
			// alert('ID : '+table.cell(index,22).data());return false;
			// console.log(table.cell(index,11).data());
		 if (table.cell(index,1).data()=='1' && table.cell(index,2).data() =='0'){
			arr_id.push(table.cell(index,0).data()); 
			
		 }
		
	 });
	 if (arr_id){
		swal({
			title: "Anda Yakin ?",
			text : "Akan Validasi ALL?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			 $.ajax({
				url: '{site_url}tklaim_validasi/verif_semua',
				type: 'POST',
				data: {
						arr_id: arr_id,
						
					 },
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Selesai Verifikasi'});
					$('#index_list').DataTable().ajax.reload(null, false )
					$("#cover-spin").hide();
				}
			});
		});
		
	 // alert(arr_id);
	 // if (arr_id){
		 // $("#cover-spin").show();
		 // // alert(arr_tanggal_jt);return false;
		// $.ajax({
			// url: '{site_url}tpiutang_verifikasi/verif_semua',
			// type: 'POST',
			// data: {
					// arr_id: arr_id,
					// arr_tipe: arr_tipe,
					// arr_tanggal_id: arr_tanggal_id,
					// arr_idpengaturan: arr_idpengaturan,
					// arr_tanggal_jt: arr_tanggal_jt,
				 // },
			// complete: function() {
				// $.toaster({priority : 'success', title : 'Succes!', message : ' Selesai Verifikasi'});
				// $('#index_list').DataTable().ajax.reload(null, false )
				// $("#cover-spin").hide();
			// }
		// });
	 // }
	 }
}
$(document).on("click", ".validasi", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,0).data()
	
	swal({
		title: "Anda Yakin ?",
		text : "Akan Validasi Pembayaran ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}tklaim_validasi/validasi',
			type: 'POST',
			data: {id: id},
			complete: function(data) {
				console.log(data);
				$.toaster({priority : 'success', title : 'Succes!', message : ' Validasi'});
				table.ajax.reload( null, false ); 
			}
		});
	});
});


function posting($id,$tanggal){
	console.log($id);
	var id=$id;		
	var tanggal=$tanggal;	
	// alert(tanggal);
	swal({
		title: "Anda Yakin ?",
		text : "Akan Posting ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		table = $('#index_list').DataTable()	
		$.ajax({
			url: '{site_url}tklaim_validasi/posting',
			type: 'POST',
			data: {id: id,tanggal:tanggal},
			complete: function(data) {
				console.log(data);
				$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi'});
				table.ajax.reload( null, false ); 
			}
		});
	});
	
}

$(document).on("click", ".batal_verifikasi", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var nopenerimaan=table.cell(tr,4).data()
	// alert(id);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Membatalkan Verifikasi No. Penerimaan "+nopenerimaan+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		batal_verifikasi(id);
	});
});
function batal_verifikasi($id){
	// console.log($id);
	var id=$id;		
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tklaim_validasi/batal_verifikasi',
		type: 'POST',
		data: {id: id},
		complete: function() {
			$.toaster({priority : 'success', title : 'Succes!', message : ' Membatalkan Verifikasi'});
			table.ajax.reload( null, false ); 
		}
	});
}

</script>