<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}erm_jenis_operasi" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('erm_jenis_operasi/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Kelompok Operasi</label>
				<div class="col-md-7">
					<select id="kelompok_id" name="kelompok_id" class="js-select2 form-control  opsi_change" style="width: 100%;"   required=""data-placeholder="Pilih Opsi" >
						<option value="" <?=($kelompok_id==""?'selected':'')?>>-Pilih Kelompok-</option>
						<?foreach(list_variable_ref(124) as $row){?>
						<option value="<?=$row->id?>" <?=($kelompok_id==$row->id?'selected':'')?>><?=$row->nama?></option>
						<?}?>
						
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}erm_jenis_operasi" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>
