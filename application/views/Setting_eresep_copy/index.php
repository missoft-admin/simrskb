<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		
		<?php if (UserAccesForm($user_acces_form,array('2102'))){ ?>
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab_3"><i class="fa fa-check-square-o"></i> Label Form</a>
		</li>
		<?}?>
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?> " id="tab_3">
			
			<?php echo form_open_multipart('setting_eresep_copy/save_eresep_label','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="form-group">
					<label class="col-md-2 control-label" for="example-hf-email">Logo Header (100x100)</label>
					<div class="col-md-7">
						<div class="box">
							<input type="file" id="file-3" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;" name="header_logo" value="{header_logo}" />
							<label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="example-hf-email"></label>
					<div class="col-md-7">

						<img class="img-avatar"  id="output_img" src="{upload_path}antrian/{header_logo}" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-2 control-label" for="nama_rumah_sakit">Nama Rumah Sakit<span style="color:red;">*</span></label>
					<div class="col-md-10">
						<input  type="text" class="form-control" id="nama_rumah_sakit" placeholder="Rumah Sakit" name="nama_rumah_sakit" value="{nama_rumah_sakit}" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="alamat">Alamat Rumah Sakit<span style="color:red;">*</span></label>
					<div class="col-md-10">
						<input  type="text" class="form-control" id="alamat" placeholder="Alamat Rumah Sakit" name="alamat" value="{alamat}" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="telepone">Nomor Telphone Rumah Sakit<span style="color:red;">*</span></label>
					<div class="col-md-10">
						<input  type="text" class="form-control" id="telepone" placeholder="Nomor Telphone Rumah Sakit" name="telepone" value="{telepone}" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="website">Website Rumah Sakit<span style="color:red;">*</span></label>
					<div class="col-md-10">
						<input  type="text" class="form-control" id="website" placeholder="Website Rumah Sakit" name="website" value="{website}" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="email">Email Rumah Sakit<span style="color:red;">*</span></label>
					<div class="col-md-10">
						<input  type="text" class="form-control" id="email" placeholder="Email Rumah Sakit" name="email" value="{email}" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="apoteker">Apoteker </label>
					<div class="col-md-10">
						<input  type="text" class="form-control" id="apoteker" placeholder="Apoteker" name="apoteker" value="{apoteker}" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="sipa">SIPA </label>
					<div class="col-md-10">
						<input  type="text" class="form-control" id="sipa" placeholder="SIPA" name="sipa" value="{sipa}" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="judul_header_ina">Judul Header <span style="color:red;">INA</span></label>
					<div class="col-md-10">
						<input  type="text" class="form-control" id="judul_header_ina" placeholder="Judul Header" name="judul_header_ina" value="{judul_header_ina}" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="judul_header_eng">Judul Header <span style="color:blue;">ENG</span></label>
					<div class="col-md-10">
						<input  type="text" class="form-control" id="judul_header_eng" placeholder="Judul Sub Header" name="judul_header_eng" value="{judul_header_eng}" required>
					</div>
				</div>
				<div class="form-group" style="margin-top:20px">
					<div class="col-md-12">		
						<div class="col-md-12">		
							<h5><?=text_success('HEADER DATA')?></h5>
						</div>
					</div>
				</div>

				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">PRESCIBER</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_presciber" <?=($st_presciber=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="presciber_ina" value="{presciber_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="presciber_eng" value="{presciber_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">PASIEN</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_pasien" <?=($st_pasien=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="pasien_ina" value="{pasien_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="pasien_eng" value="{pasien_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">NO REGISTRASI</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_noreg" <?=($st_noreg=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="noreg_ina" value="{noreg_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="noreg_eng" value="{noreg_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">TANGGAL LAHIR</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_tl" <?=($st_tl=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="tl_ina" value="{tl_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="tl_eng" value="{tl_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">NO RESEP</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_noresep" <?=($st_noresep=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="noresep_ina" value="{noresep_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="noresep_eng" value="{noresep_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">JENIS PASIEN</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_jenis_pasien" <?=($st_jenis_pasien=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="jenis_pasien_ina" value="{jenis_pasien_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="jenis_pasien_eng" value="{jenis_pasien_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">ALERGI</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_alergi" <?=($st_alergi=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="alergi_ina" value="{alergi_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="alergi_eng" value="{alergi_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">DIAGNOSA</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_diagnosa" <?=($st_diagnosa=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="diagnosa_ina" value="{diagnosa_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="diagnosa_eng" value="{diagnosa_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">BERAT BADAN</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_bb" <?=($st_bb=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="bb_ina" value="{bb_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="bb_eng" value="{bb_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">TINGGI BADAN</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_tb" <?=($st_tb=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="tb_ina" value="{tb_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="tb_eng" value="{tb_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">LUAS PERMUKAAN</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_luas_permukaan" <?=($st_luas_permukaan=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="luas_permukaan_ina" value="{luas_permukaan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="luas_permukaan_eng" value="{luas_permukaan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">MENYUSUI</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_menyusui" <?=($st_menyusui=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="menyusui_ina" value="{menyusui_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="menyusui_eng" value="{menyusui_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">PASIEN PUASA</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_puasa" <?=($st_puasa=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="puasa_ina" value="{puasa_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="puasa_eng" value="{puasa_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">PRIORITAS</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_prioritas" <?=($st_prioritas=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="prioritas_ina" value="{prioritas_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="prioritas_eng" value="{prioritas_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">RESEP PASIEN PULANG</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_pulang" <?=($st_pulang=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="pulang_ina" value="{pulang_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="pulang_eng" value="{pulang_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">GANGGUAN GINJAL</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_ginjal" <?=($st_ginjal=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="ginjal_ina" value="{ginjal_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="ginjal_eng" value="{ginjal_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">CATATAN</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_catatan" <?=($st_catatan=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="catatan_ina" value="{catatan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="catatan_eng" value="{catatan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-top:20px">
					<div class="col-md-12">		
						<div class="col-md-12">		
							<h5><?=text_success('DETAIL PRESCRIPTION NON RACIKAN')?></h5>
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">INTERVAL</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_interval" <?=($st_interval=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="interval_ina" value="{interval_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="interval_eng" value="{interval_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">DOSIS</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_dosis" <?=($st_dosis=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="dosis_ina" value="{dosis_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="dosis_eng" value="{dosis_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">RUTE PEMBERIAN</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_rute" <?=($st_rute=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="rute_ina" value="{rute_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="rute_eng" value="{rute_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">ATURAN TAMBAHAN</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_aturan" <?=($st_aturan=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="aturan_ina" value="{aturan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="aturan_eng" value="{aturan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">WAKTU MINUM</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_waktu" <?=($st_waktu=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="waktu_ina" value="{waktu_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="waktu_eng" value="{waktu_eng}">
						</div>
						
					</div>
				</div>
				

				<div class="form-group" style="margin-top:20px">
					<div class="col-md-12">		
						<div class="col-md-12">		
							<h5><?=text_success('DETAIL PRESCRIPTION RACIKAN')?></h5>
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">NAMA RACIKAN</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_nama_racikan" <?=($st_nama_racikan=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="nama_racikan_ina" value="{nama_racikan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="nama_racikan_eng" value="{nama_racikan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">QTY HEAD</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_qty" <?=($st_qty=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="qty_ina" value="{qty_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="qty_eng" value="{qty_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">QTY PER OBAT</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_qty_obat" <?=($st_qty_obat=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="qty_obat_ina" value="{qty_obat_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="qty_obat_eng" value="{qty_obat_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">PEMBAGIAN (P1/P2)</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_pembagian" <?=($st_pembagian=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="pembagian_ina" value="{pembagian_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="pembagian_eng" value="{pembagian_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">DOSIS</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_dosis_rac" <?=($st_dosis_rac=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="dosis_rac_ina" value="{dosis_rac_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="dosis_rac_eng" value="{dosis_rac_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">INTERVAL</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_interval_rac" <?=($st_interval_rac=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="interval_rac_ina" value="{interval_rac_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="interval_rac_eng" value="{interval_rac_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">WAKTU MINUM</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_waktu_rac" <?=($st_waktu_rac=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="waktu_rac_ina" value="{waktu_rac_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="waktu_rac_eng" value="{waktu_rac_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">RUTE PEMBERIAN</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_rute_rac" <?=($st_rute_rac=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="rute_rac_ina" value="{rute_rac_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="rute_rac_eng" value="{rute_rac_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">ATURAN TAMBAHAN</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_aturan_rac" <?=($st_aturan_rac=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="aturan_rac_ina" value="{aturan_rac_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="aturan_rac_eng" value="{aturan_rac_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">DES JENIS RACIK</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_jenis_rac" <?=($st_jenis_rac=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="jenis_rac_ina" value="{jenis_rac_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="jenis_rac_eng" value="{jenis_rac_eng}">
						</div>
						
					</div>
				</div>
				
				<div class="form-group" style="margin-top:20px">
					<div class="col-md-12">		
						<div class="col-md-12">		
							<h5><?=text_success('FOOTER')?></h5>
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">TANDA TANGAN P.C.C</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_ttd" <?=($st_ttd=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="ttd_ina" value="{ttd_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="ttd_eng" value="{ttd_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">TANGGAL CETAK</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_tanggal_cetak" <?=($st_tanggal_cetak=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12" hidden>
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="tanggal_cetak_ina" value="{tanggal_cetak_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="tanggal_cetak_eng" value="{tanggal_cetak_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">WAKTU CETAK</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_waktu_cetak" <?=($st_waktu_cetak=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12" hidden>
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="waktu_cetak_ina" value="{waktu_cetak_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="waktu_cetak_eng" value="{waktu_cetak_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">USER CETAK</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_user" <?=($st_user=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12" hidden>
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="user_ina" value="{user_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="user_eng" value="{user_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">JUMLAH CETAK</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="st_jml_cetak" <?=($st_jml_cetak=='1'?'checked':'')?>><span></span> Tampilkan
							</label>
						</div>
					</div>
					<div class="col-md-12" hidden>
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="jml_cetak_ina" value="{jml_cetak_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="jml_cetak_eng" value="{jml_cetak_eng}">
						</div>
						
					</div>
				</div>
				
				
				<div class="form-group" style="margin-top:20px">
					<div class="col-md-12">		
						<div class="col-md-12">		
							<h5><?=text_success('FOOTER NOTES')?></h5>
						</div>
					</div>
				</div>	
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="footer_notes_ina" value="{footer_notes_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="footer_notes_eng" value="{footer_notes_eng}">
						</div>
						
					</div>
				</div>
				
				<div class="form-group" style="margin-top:15px">
				
					<div class="col-md-12">
						<div class="col-md-6">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_label" name="btn_save_general"><i class="fa fa-save"></i> Simpan</button>
						</div>
						
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
function loadFile_img(event){
	// alert('sini');
	var output_img = document.getElementById('output_img');
	output_img.src = URL.createObjectURL(event.target.files[0]);
}

$(document).ready(function(){	
	

})	


</script>