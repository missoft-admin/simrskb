<?php  $generator = new Picqer\Barcode\BarcodeGeneratorPNG(); ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Hasil Pemeriksaan Radiologi</title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      * {
        font-size: 16px !important;
      }
      table {
        font-size: 16px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      * {
        font-size: 16px !important;
      }
      table {
        font-size: 16px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    .col-12 {
      float: left;
      width: 100%;
      padding-left: 12px;
    }

    .col-6 {
      float: left;
      width: 50%;
      padding-left: 12px;
    }

    .col-4 {
      float: left;
      width: 32.22%;
      padding-left: 12px;
    }

    .row {
      margin-bottom: 10px;
    }
    
    .row:after {
      content: "";
      display: table;
      clear: both;
    }
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <img src="<?= base_url() ?>assets/upload/pengaturan_printout_radiologi_expertise/<?= strip_tags($pengaturan_printout['logo_header']); ?>" style="width: 100%; postion: absolute; top: 0;">
    
    <br><br>

    <p class="text-center" style="margin:0">
      <b><?= strip_tags($pengaturan_printout['label_header']); ?></b>
      <br>
      <i><?= strip_tags($pengaturan_printout['label_header_eng']); ?></i>
    </p>
    
    <br>
    
    <table class="content-2">
      <tr>
        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_noregister']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_noregister_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$nomor_register;?></td>

        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_nomedrec']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_nomedrec_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$nomor_medrec;?></td>
      </tr>
      <tr>
        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_nama']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_nama_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$nama_pasien;?></td>

        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_kelompok_pasien']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_kelompok_pasien_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$kelompok_pasien;?></td>
      </tr>
      <tr>
        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_alamat']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_alamat_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$alamat_pasien;?></td>

        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_nama_asuransi']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_nama_asuransi_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=($rekanan_asuransi ? $rekanan_asuransi : '-');?></td>
      </tr>
      <tr>
        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_dokter_perujuk']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_dokter_perujuk_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$dokter_perujuk;?></td>

        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_tanggal_lahir']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_tanggal_lahir_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=date("Y-m-d", strtotime($tanggal_lahir));?></td>
      </tr>
      <tr>
        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_nomor_radiologi']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_nomor_radiologi_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $nomor_radiologi; ?></td>

        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_umur']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_umur_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$umur_tahun;?> Tahun <?=$umur_bulan;?> Bulan <?=$umur_hari;?> Hari</td>
      </tr>
      <tr>
        <td colspan="3">
          <img width="300px" src="data:image/png;base64, <?= base64_encode($generator->getBarcode($nomor_radiologi, $generator::TYPE_CODE_128)); ?>">
        </td>

        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_rujukan']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_rujukan_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=GetAsalRujukan($asal_rujukan);?></td>
      </tr>
    </table>
    
    <br>

    <?php foreach ($daftar_pemeriksaan as $row) { ?>
      <div class="row">
        <div class="col-12"><b>Nama Pemeriksaan</b></div>
      </div>
      <div class="row">
        <div class="col-12">
          <div style="border: 1px solid #ccc; border-radius: 5px; padding: 10px;">
            <?php echo $row->namatarif; ?>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php echo $pengaturan_printout['tampilkan_flag_kritis'] ? '<span style="color: red;">'.(2 == $row->flag_kritis ? '!' : '').'</span>' : ''; ?>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-4"><b>Klinis</b></div>
        <div class="col-4"><b>Kesan</b></div>
        <div class="col-4"><b>Usul</b></div>
      </div>
      <div class="row">
        <div class="col-4">
          <div style="border: 1px solid #ccc; border-radius: 5px; padding: 10px;">
            <?=$row->klinis?>
          </div>
        </div>
        <div class="col-4">
          <div style="border: 1px solid #ccc; border-radius: 5px; padding: 10px;">
            <?=$row->kesan?>
          </div>
        </div>
        <div class="col-4">
          <div style="border: 1px solid #ccc; border-radius: 5px; padding: 10px;">
            <?=$row->usul?>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12"><b>Hasil Pemeriksaan</b></div>
      </div>
      <div class="row">
        <div class="col-12">
          <div style="border: 1px solid #ccc; border-radius: 5px; padding: 10px;">
            <?=$row->hasil?>
          </div>
        </div>
      </div>

      <br>
    <?php } ?>

    <table>
      <tr>
        <td>
          <table>
            <?php if ($pengaturan_printout['tampilkan_penanggung_jawab']) { ?>
              <tr>
                <td width="150px">
                  <?= strip_tags($pengaturan_printout['label_penanggung_jawab']); ?><br>
                  <i><?= strip_tags($pengaturan_printout['label_penanggung_jawab_eng']); ?></i>
                </td>
                <td width="10px">:</td>
                <td><?=$dokter_penanggung_jawab?></td>
              </tr>
            <? } else { ?>
              <tr>
                <td width="150px"></td>
                <td width="10px"></td>
                <td></td>
              </tr>
            <? } ?>
            <tr>
              <td colspan="3" style="font-size:14px!important"><?= strip_tags($pengaturan_printout['label_waktu_input_order']); ?> : <?= $tanggal_input_pemeriksaan; ?> | <?= strip_tags($pengaturan_printout['label_waktu_pemeriksaan']); ?> : <?= $tanggal_input_pemeriksaan; ?> | <?= strip_tags($pengaturan_printout['label_waktu_expertise']); ?> : <?= $tanggal_input_expertise; ?> | <?= strip_tags($pengaturan_printout['label_waktu_cetak']); ?> : <?= $tanggal_cetak; ?></td>
            </tr>
            <tr>
              <td colspan="3" style="font-size:14px!important"><?= strip_tags($pengaturan_printout['label_jumlah_cetak']); ?> : <?= $cetakan_ke; ?> - <?= strip_tags($pengaturan_printout['label_user_input_order']); ?>: <?= $user_created; ?> / <?= strip_tags($pengaturan_printout['label_user_pemeriksaan']); ?>: <?= $user_input_pemeriksaan; ?> / <?= strip_tags($pengaturan_printout['label_user_expertise']); ?>: <?= $user_input_expertise; ?> / <?= strip_tags($pengaturan_printout['label_user_cetak']); ?>: <?= $user_cetak; ?></i></td>
            </tr>
            <tr>
              <td colspan="3" style="font-size:14px!important"><?= strip_tags($pengaturan_printout['footer_notes']); ?></td>
            </tr>
          </table>
        </td>
        <td>
          <table>
            <tr>
              <td style="width:30%" class="text-center text-bold"><?= strip_tags($pengaturan_printout['label_tanda_tangan']); ?> <br> <i><?= strip_tags($pengaturan_printout['label_tanda_tangan_eng']); ?></i></td>
            </tr>
            <tr>
              <td style="width:30%" class="text-center"><img src="<?= base_url(); ?>qrcode/qr_code_ttd_dokter/<?= $dokter_radiologi_id; ?>" width="100px"></td>
            </tr>
            <tr>
              <td style="width:30%" class="text-center text-bold">( <?= $dokter_radiologi; ?> )<br><?= $dokter_radiologi_nip; ?></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <img src="<?= base_url() ?>assets/upload/pengaturan_printout_radiologi_expertise/<?= strip_tags($pengaturan_printout['logo_footer']); ?>" style="width: 100%; position: absolute; bottom: 0">
  </body>
</html>
