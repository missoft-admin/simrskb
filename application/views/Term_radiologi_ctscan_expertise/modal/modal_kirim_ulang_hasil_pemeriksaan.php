<!-- Modal Kirim Ulang Hasil Pemeriksaan -->
<div class="modal fade" id="modal-kirim-ulang-hasil" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="block-options">
                    <li>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </li>
                </ul>
                <h5 class="modal-title">Pengiriman Ulang Hasil</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="emailKirimUlangHasil">E-mail Kirim Hasil</label>
                            <input type="text" class="js-tags-input form-control" id="emailKirimUlangHasil" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="btn-kirim-ulang-hasil-pemeriksaan">Kirim Hasil</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on("click", "#btn-kirim-ulang-hasil-pemeriksaan", function() {
        $("#cover-spin").show();

        let transaksi_id = $('#transaksiId').val();
        let email = $('#emailKirimUlangHasil').val();

        let requestData = {
            transaksi_id: transaksi_id,
            email: email
        };

        $.ajax({
            url: '{base_url}term_radiologi_ctscan_expertise/kirim_ulang_email_hasil_pemeriksaan',
            type: 'POST',
            dataType: 'json',
            data: requestData,
            success: function(response) {
                if (response.status == true) {
                    $.toaster({
                        priority: 'success',
                        title: 'Berhasil!',
                        message: 'Email berhasil dikirim ulang.'
                    });
                } else {
                    swal({
                        title: 'Maaf, Email Tidak Terkirim',
                        text: response.message,
                        type: 'error',
                        showCancelButton: false,
                    });
                }

                $("#cover-spin").hide();
            },
            error: function(xhr, status, error) {
                console.error('Error dalam permintaan AJAX: ' + error);
            }
        });
    });
});

</script>
