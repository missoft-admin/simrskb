<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content" style="margin-top: -20px;font-size:">
		<?php echo form_open('msetting_jurnal_pendapatan_jual_rajal/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>	
		<?
		$q="SELECT *FROM mdata_tipebarang WHERE id IN(1,3)";
		$list_tipe=$this->db->query($q)->result();
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="block block-themed ">
					<div class="block-header bg-primary">
						<ul class="block-options">

							<li>
								<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">SETTING AKUN PENJUALAN OBAT & ALKES RAWAT JALAN</h3>
					</div>
					<div class="block-content">						
						<input type="hidden" class="form-control" id="id_edit_rajal" placeholder="0" name="id_edit_rajal" value="">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Jenis Kunjungan</label>
							
							<div class="col-md-4">
								<select name="asalrujukan_rajal" id="asalrujukan_rajal" style="width: 100%" id="step" class="js-select2 form-control input-sm">										
									<option value="#" selected>- Pilih Jenis Kunjungan -</option>																			
									<option value="1">Poliklinik</option>																			
									<option value="2">IGD</option>																			
								</select>			
							</div>
							<label class="col-md-2 control-label" >Poliklinik</label>
							<div class="col-md-4">
								<select name="idpoli_kelas_rajal" id="idpoli_kelas_rajal" style="width: 100%" class="js-select2 form-control input-sm"></select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Tipe Barang</label>							
							<div class="col-md-4">
								<select name="idtipe_rajal" id="idtipe_rajal" style="width: 100%" class="js-select2 form-control input-sm idbarang">
									<option value="#" selected>- Semua Tipe -</option>		
									<?foreach($list_tipe as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama_tipe?></option>																				
									<?}?>
								</select>	
							</div>
							
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Kategori</label>
							
							<div class="col-md-4">
								<select name="idkategori_rajal" id="idkategori_rajal" style="width: 100%" class="js-select2 form-control input-sm idkategori"></select>
							</div>
							<label class="col-md-2 control-label" >Group Penjualan</label>
							<div class="col-md-4">
								<select name="idgroup_penjualan_rajal" id="idgroup_penjualan_rajal" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" >Barang</label>
							
							<div class="col-md-4">
								<select name="idbarang_rajal" id="idbarang_rajal" style="width: 100%" class="js-select2 form-control input-sm"></select>
							</div>
							<label class="col-md-2 control-label" >Group Diskon</label>
							<div class="col-md-4">
								<select name="idgroup_diskon_rajal" id="idgroup_diskon_rajal" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" ></label>
							
							<div class="col-md-4">
								<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_rajal" title="Masukan Item"><i class="fa fa-save"></i> Simpan</button>
								<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_rajal" title="Batalkan"><i class="fa fa-refresh"></i> Clear Input</button>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<table id="tabel_rajal" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 0%;">#</th>															
											<th style="width: 8%;">Jenis Kunjungan</th>															
											<th style="width: 8%;">Poli</th>															
											<th style="width: 8%;">Tipe</th>															
											<th style="width: 9%;">Kategori</th>															
											<th style="width: 11%;">Barang</th>															
											<th style="width: 12%;">Group Pembelian</th>															
											<th style="width: 12%;">Group Diskon</th>															
											<th style="width: 5%;">Actions</th>
										</tr>
										
									</thead>
									<tbody>
										
									</tbody>
									
							</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		list_group();
		list_poli_kelas('#');
		list_kategori_rajal('#');
		clear_input_rajal();
		list_barang_rajal();
		load_rajal();
		
	});	
	function list_group(){
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_general/list_akun_group',
			dataType: "json",
			success: function(data) {
				$(".idgroup").empty();
				$('.idgroup').append('<option value="#" selected>- Pilih Group -</option>');
				$('.idgroup').append(data.detail);
				
			}
		});		
	}
	
	//Non Racikan
	$(document).on("change","#asalrujukan_rajal",function(){
		list_poli_kelas($(this).val());
	});
	function list_poli_kelas($id){
		if ($id !='#'){			
			$.ajax({
				url: '{site_url}msetting_jurnal_pendapatan_jual_rajal/list_poli_kelas/'+$id,
				dataType: "json",
				success: function(data) {
					$("#idpoli_kelas_rajal").empty();
					$('#idpoli_kelas_rajal').append('<option value="0" selected>- Semua -</option>');
					$('#idpoli_kelas_rajal').append(data.detail);
				}
			});
		}else{
			$("#idpoli_kelas_rajal").empty();
			$('#idpoli_kelas_rajal').append('<option value="0" selected>- Semua -</option>');
		}
		
	}
	
	$(document).on("change","#idtipe_rajal",function(){
		list_kategori_rajal($(this).val());
		list_barang_rajal();
	});
	$(document).on("change","#idkategori_rajal",function(){
		list_barang_rajal();
	});
	function list_kategori_rajal($id){
		if ($id!='#'){
			$.ajax({
				url: '{site_url}msetting_jurnal_pembelian/list_kategori/'+$id,
				dataType: "json",
				success: function(data) {
					$("#idkategori_rajal").empty();
					$('#idkategori_rajal').append('<option value="0" selected>- Semua Kategori -</option>');
					$('#idkategori_rajal').append(data.detail);
				}
			});
		}else{
			$("#idkategori_rajal").empty();
			$('#idkategori_rajal').append('<option value="0" selected>- Semua Kategori -</option>');
		}		
	}
	function list_barang_rajal(){
		var newOption = new Option('- Semua Barang -', '0', true, true);					
		$("#idbarang_rajal").append(newOption);
		$("#idbarang_rajal").val("0").trigger('change');
		
		var idtipe=$("#idtipe_rajal").val();
		var idkategori=$("#idkategori_rajal").val();
		$("#idbarang_rajal").select2({
			minimumInputLength: 0,
			noResults: 'Obat Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}msetting_jurnal_pembelian/list_barang/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,idtipe: idtipe,idkategori:idkategori,
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.kode+' - '+item.nama,
								id: item.id,
								idtipe : item.idtipe
							}
						})
					};
				}
			}
		});

	}
	
	function validate_add_rajal()
	{
			var idtipe_rajal=$("#idtipe_rajal").val();
			var asalrujukan_rajal=$("#asalrujukan_rajal").val();
			var idgroup_penjualan_rajal=$("#idgroup_penjualan_rajal").val();
			var idgroup_diskon_rajal=$("#idgroup_diskon_rajal").val();
			var idgroup_tuslah_rajal=$("#idgroup_tuslah_rajal").val();
		
			if (asalrujukan_rajal=='#'){
				sweetAlert("Maaf...", "Jenis Kunjungan Harus diisi", "error");
				return false;
			}
			// if (idtipe_rajal=='#'){
				// sweetAlert("Maaf...", "Tipe Barang Harus diisi", "error");
				// return false;
			// }
			
			
			if (idgroup_penjualan_rajal=='#'){
				sweetAlert("Maaf...", "Group Penjualan Harus diisi", "error");
				return false;
			}
			if (idgroup_diskon_rajal=='#'){
				sweetAlert("Maaf...", "Group Diskon Harus diisi", "error");
				return false;
			}
			// if (idgroup_tuslah_rajal=='#'){
				// sweetAlert("Maaf...", "Group Tuslah Harus diisi", "error");
				// return false;
			// }
			
		return true;
	}
	function clear_input_rajal(){
		$("#idtipe_rajal").val('#').trigger('change');	
		$("#asalrujukan_rajal").val('#').trigger('change');	
		
		$("#idgroup_penjualan_rajal").val('#').trigger('change');
		$("#idgroup_diskon_rajal").val('#').trigger('change');
		
		$("#idkategori_rajal").val('0').trigger('change');
		$("#idpoli_kelas_rajal").val('0').trigger('change');
		$("#idbarang_rajal").val('0').trigger('change');
	}
	$(document).on("click","#simpan_rajal",function(){
		if (validate_add_rajal()==false)return false;
		var asalrujukan=$("#asalrujukan_rajal").val();
		var idpoli_kelas=$("#idpoli_kelas_rajal").val();
		var idtipe=$("#idtipe_rajal").val();
		var idkategori=$("#idkategori_rajal").val();
		var idbarang=$("#idbarang_rajal").val();
		var idgroup_penjualan=$("#idgroup_penjualan_rajal").val();
		var idgroup_diskon=$("#idgroup_diskon_rajal").val();
	
		
		// alert('SINI');
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_jual_rajal/simpan_rajal',
			type: 'POST',
			data: {
				asalrujukan:asalrujukan
				,idpoli_kelas:idpoli_kelas
				,idtipe:idtipe
				,idkategori:idkategori
				,idbarang:idbarang
				,idgroup_penjualan:idgroup_penjualan
				,idgroup_diskon:idgroup_diskon
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_rajal').DataTable().ajax.reload( null, false );
					clear_input_rajal();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	function hapus_rajal($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_pendapatan_jual_rajal/hapus_rajal/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_rajal').DataTable().ajax.reload( null, false );
						clear_input_rajal();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_rajal",function(){		
		clear_input_rajal();
	});
	function load_rajal(){
		var idlogic=$("#id").val();
		
		$('#tabel_rajal').DataTable().destroy();
		var table = $('#tabel_rajal').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pendapatan_jual_rajal/load_rajal/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
					]
		});
	}
	
	// Racikan
	$(document).on("change","#asalrujukan_r",function(){
		list_poli_kelas_r($(this).val());
	});
	
</script>
