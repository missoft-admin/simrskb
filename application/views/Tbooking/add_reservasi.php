<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
  <div class="js-wizard-classic-validation block">
		<!-- Step Tabs -->
		<ul class="nav nav-tabs nav-justified">
			<li class="active">
				<a class="inactive" href="#validation-classic-step1" data-toggle="tab"> <i class="si si-user-follow"></i> Pilih Pasien</a>
			</li>
			<li>
				<a class="inactive" href="#validation-classic-step2"  data-toggle="tab"><i class="fa fa-calendar-check-o"></i> Tentukan Perjanjian</a>
			</li>
			<li>
				<a class="inactive" href="#validation-classic-step3" data-toggle="tab"><i class="fa fa-calendar"></i>  Rincian</a>
			</li>
		</ul>
		<!-- END Step Tabs -->

		<!-- Form -->
		<!-- jQuery Validation (.js-form1 class is initialized in js/pages/base_forms_wizard.js) -->
		<!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
		<?php echo form_open_multipart('tbooking/save_register_poli', 'class="js-form1 validation form-horizontal" onsubmit="return validate_final()"') ?>
			<!-- Steps Content -->
			<div class="block-content tab-content">
				<!-- Step 1 -->
				<div class="tab-pane push-5-t push-50 active" id="validation-classic-step1">
					<div class="row">
					
						<div class="col-md-6">
							
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 col-xs-12 push-5 control-label" for="tglpendaftaran">Tanggal Pendaftaran<span style="color:red;">*</span></label>
								<div class="col-md-8">
									<div class="row">
										<div class="col-md-6 col-xs-6 push-5">
											<input tabindex="2" type="text" readonly class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
										</div>
										<div class="col-md-6 col-xs-6 push-5">
											<input tabindex="3" type="text" readonly class="time-datepicker form-control" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="statuspasienbaru">Status Pasien <span style="color:red;">*</span></label>
								<div class="col-md-8">
									<select tabindex="4" id="statuspasienbaru" name="statuspasienbaru" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="">Pilih Opsi</option>
											<option value="1" <?=($statuspasienbaru == 1 ? 'selected="selected"' : '')?>>PASIEN BARU</option>
											<option value="0" <?=($statuspasienbaru == 0 ? 'selected="selected"' : '')?>>PASIEN LAMA</option>
										</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="tglpendaftaran">No Medrec<span style="color:red;">*</span></label>
								<div class="col-md-8">
									<div class="input-group">
										<input readonly class="form-control" id="no_medrec" placeholder="NO. MEDREC" name="no_medrec" value="{no_medrec}" required>
										<div id="div_medrec">
										<select tabindex="4" id="tmp_medrec" name="tmp_medrec" class="form-control" style="width: 100%;" data-placeholder="Cari No Medrec Pasien" required>
										</select>
										</div>
										<span class="input-group-btn">
											<button class="btn btn-success" data-toggle="modal" data-target="#cari-pasien-modal"type="button" id="btn_cari_pasien"><i class="fa fa-search"></i> Cari Pasien</button>
											
										</span>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px; padding-bottom:0px;">
								<label class="col-md-4 col-xs-12 push-5 control-label" for="nama">Nama<span style="color:red;">*</span></label>
								<div class="col-md-3 col-xs-4 push-5" style="padding-right: 10px;">
									<select tabindex="5" id="titlepasien" name="title" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<?php foreach (get_all('mpasien_title') as $r):?>
											<option value="<?= $r->singkatan; ?>" <?=($title === $r->singkatan ? 'selected="selected"' : '')?>><?= $r->singkatan; ?></option>
										<?php endforeach; ?>
										</select>
								</div>
								<div class="col-md-5 col-xs-8 push-5" style="padding-left: 0px;">
									<input tabindex="6" type="text" oninput="this.value = this.value.toUpperCase()" class="form-control" id="namapasien" placeholder="Nama Lengkap" name="namapasien" value="{namapasien}" required>
								</div>
							</div>

							
							<input type="hidden" id="status_cari" name="status_cari" value="0">
							<input type="hidden" id="ckabupaten" name="ckabupaten" value="0">
							<input type="hidden" id="ckecamatan" name="ckecamatan" value="0">
							<input type="hidden" id="ckelurahan" name="ckelurahan" value="0">

							<input type="hidden" id="idpasien" name="idpasien" value="{idpasien}">
							<input type="hidden" id="kode_booking" name="kode_booking" value="{kode_booking}">

							<input type="hidden" name="created" value="<?= date(' d-M-Y H:i:s ')?>">
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="jenis_kelamin">Jenis Kelamin <span style="color:red;">*</span></label>
								<div class="col-md-8">
									<select tabindex="7" id="jenis_kelamin" name="jenis_kelamin" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(1) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_kelamin == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="statuskawin">Status Kawin</label>
								<div class="col-md-8">
									<select tabindex="8" id="statuskawin" name="statuskawin" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(7) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_kelamin == $row->id ? 'selected="selected"' : '')?> <?=(($jenis_kelamin == '' || $jenis_kelamin == '0') && $row->st_default=='1' ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="tempatlahir">Tempat Lahir</label>
								<div class="col-md-8">
									<input tabindex="9" type="text" class="form-control" oninput="this.value = this.value.toUpperCase()" id="tempatLahir" placeholder="Tempat Lahir" name="tempatlahir" value="{tempat_lahir}" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="tgllahir">Tanggal Lahir<span style="color:red;">*</span></label>
								<div class="col-md-3  col-xs-12 push-5">
									<select tabindex="10" name="tgl_lahir" id="hari" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="0">Hari</option>
										<?php for ($i = 1; $i < 32; $i++) {?>
											<?php $hari = str_pad($i, 2, '0', STR_PAD_LEFT); ?>
											<option value="<?= $hari; ?>" <?=($tgl_hari == $hari ? 'selected="selected"' : '')?>><?= $hari; ?></option>
										<?php }?>
									</select>
								</div>
								<div class="col-md-2  col-xs-12 push-5">
									<select tabindex="11" name="bulan_lahir" id="bulan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="00">Bulan</option>
										<option value="01" <?=($tgl_bulan == '01' ? 'selected="selected"' : '')?>>Januari</option>
										<option value="02" <?=($tgl_bulan == '02' ? 'selected="selected"' : '')?>>Februari</option>
										<option value="03" <?=($tgl_bulan == '03' ? 'selected="selected"' : '')?>>Maret</option>
										<option value="04" <?=($tgl_bulan == '04' ? 'selected="selected"' : '')?>>April</option>
										<option value="05" <?=($tgl_bulan == '05' ? 'selected="selected"' : '')?>>Mei</option>
										<option value="06" <?=($tgl_bulan == '06' ? 'selected="selected"' : '')?>>Juni</option>
										<option value="07" <?=($tgl_bulan == '07' ? 'selected="selected"' : '')?>>Juli</option>
										<option value="08" <?=($tgl_bulan == '08' ? 'selected="selected"' : '')?>>Agustus</option>
										<option value="09" <?=($tgl_bulan == '09' ? 'selected="selected"' : '')?>>September</option>
										<option value="10" <?=($tgl_bulan == '10' ? 'selected="selected"' : '')?>>Oktober</option>
										<option value="11" <?=($tgl_bulan == '11' ? 'selected="selected"' : '')?>>November</option>
										<option value="12" <?=($tgl_bulan == '12' ? 'selected="selected"' : '')?>>Desember</option>
									</select>
								</div>
								<div class="col-md-3  col-xs-12 push-5" >
									<select tabindex="12" name="tahun_lahir" id="tahun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="0">Tahun</option>
										<?php for ($i = date('Y'); $i >= 1900; $i--) {?>
											<option value="<?= $i; ?>" <?=($tgl_tahun == $i ? 'selected="selected"' : '')?>><?= $i; ?></option>
										<?php }?>
									</select>
								</div>
							</div>
							<div class="form-group " >
								<label class="col-md-4 col-xs-12 control-label" for="umur">Umur</label>
								<div class="col-md-3 col-xs-12 push-5">
									<input type="text" class="form-control" id="umurtahun" name="umurtahun" value="{umurtahun}" placeholder="Tahun" readonly="true" required>
								</div>
								<div class="col-md-2 col-xs-12 push-5">
									<input type="text" class="form-control" id="umurbulan" name="umurbulan" value="{umurbulan}" placeholder="Bulan" readonly="true" required>
								</div>
								<div class="col-md-3 col-xs-12 push-5">
									<input type="text" class="form-control" id="umurhari" name="umurhari" value="{umurhari}" placeholder="Hari" readonly="true" required>
								</div>
							</div>
							<div class="form-group" >
								<label class="col-md-4 control-label" for="alamat">Alamat<span style="color:red;">*</span></label>
								<div class="col-md-8">
									<textarea tabindex="13" class="form-control" oninput="this.value = this.value.toUpperCase()" id="alamatpasien" placeholder="Alamat" name="alamatpasien" required><?= $alamatpasien?></textarea>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 0px;">
								<div class="col-md-4"></div>
								<label class="col-md-3 control-label" for="provinsi"><p align="left">Provinsi</p></label>
								<div class="col-md-5">
									<?php if ($id == null) {?>
									<select tabindex="14" name="provinsi" id="provinsi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="">Pilih Opsi</option>
											<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
											<option value="<?= $a->id; ?>" <?= ($a->id == '12') ? 'selected' : ''; ?>><?= $a->nama; ?></option>
										<?php endforeach; ?>
										</select>
									<?php } else {?>
									<select tabindex="14" name="provinsi" id="provinsi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="">Pilih Opsi</option>
											<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
											<option value="<?= $a->id; ?>" <?=($provinsi_id == $a->id ? 'selected="selected"' : '')?>><?= $a->nama; ?></option>
										<?php endforeach; ?>
										</select>
									<?php }?>
								</div>
							</div>
							<div id="kabupaten" style="display: block;">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-4"></div>
									<label class="col-md-3" for="kabupaten"><left>Kabupaten/Kota</left></label>
									<div class="col-md-5">
										<?php if ($id == null) {?>
										<select tabindex="15" name="kabupaten" id="kabupaten1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="">Pilih Opsi</<option>
												<?php foreach (get_all('mfwilayah', ['parent_id' => '12']) as $r):?>
												<option value="<?= $r->id; ?>" <?= ($r->id == '213') ? 'selected' : ''; ?>><?= $r->nama; ?></<option>
												<?php endforeach; ?>
											</select>
										<?php } else {?>
										<select tabindex="15" name="kabupaten" id="kabupaten1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
												<option value="">Pilih Opsi</<option>
												<?php foreach (get_all('mfwilayah', ['parent_id' => $provinsi_id]) as $r):?>
												<option value="<?= $r->id; ?>" <?=($kabupaten_id == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
												<?php endforeach; ?>
											</select>
										<?php }?>
									</div>
								</div>
							</div>
							<div id="kecamatan" style="display: block;">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-4"></div>
									<label class="col-md-3 control-label" for="kecamatan" style="text-align:left;">Kecamatan</label>
									<div class="col-md-5">
										<?php if ($id == null) {?>
										<select tabindex="16" name="kecamatan" id="kecamatan1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="">Pilih Opsi</<option>
											<?php foreach (get_all('mfwilayah', ['parent_id' => '213']) as $r):?>
											<option value="<?= $r->id; ?>"><?= $r->nama; ?></<option>
											<?php endforeach; ?>
										</select>
										<?php } else {?>
										<select tabindex="16" name="kecamatan" id="kecamatan1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="">Pilih Opsi</<option>
											<?php foreach (get_all('mfwilayah', ['parent_id' => $kabupaten_id]) as $r):?>
											<option value="<?= $r->id; ?>"<?=($kecamatan_id == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
											<?php endforeach; ?>
										</select>
										<?php }?>
									</div>
								</div>
							</div>
							
							<div id="kelurahan" style="display: block;">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-4"></div>
									<label class="col-md-3 control-label" for="kelurahan" style="text-align:left;">Kelurahan</label>
									<div class="col-md-5">
										<select tabindex="17" name="kelurahan" id="kelurahan1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="">Pilih Opsi</<option>
											<?php foreach (get_all('mfwilayah', ['parent_id' => $kecamatan_id]) as $r):?>
											<option value="<?= $r->id; ?>"<?=($kelurahan_id == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-4"></div>
									<label class="col-md-3 control-label" for="kodepos" style="text-align:left;">Kode Pos</label>
									<div class="col-md-5">
										<input tabindex="18" type="text" class="form-control" id="kodepos" placeholder="Kode Pos" name="kodepos" value="{kodepos}" readonly="true" required>
									</div>
								</div>
							</div>
							
							
							
						</div>

						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 col-xs-12 push-5 control-label" for="noidentitas">No Identitas</label>
								<div class="col-md-3 col-xs-4 push-5" style="padding-right: 10px;">
									<select tabindex="18" id="jenis_id" name="jenis_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="1" <?=($jenis_id == 1 ? 'selected="selected"' : '')?>>KTP</option>
										<option value="2" <?=($jenis_id == 2 ? 'selected="selected"' : '')?>>SIM</option>
										<option value="3" <?=($jenis_id == 3 ? 'selected="selected"' : '')?>>Pasport</option>
										<option value="4" <?=($jenis_id == 4 ? 'selected="selected"' : '')?>>Kartu Pelajar/Mahasiswa</option>
									</select>
								</div>
								<div class="col-md-5 col-xs-8 push-5" style="margin-left:0px;padding-left: 0px;">
									<input tabindex="19" type="text" class="form-control" id="noidentitas" placeholder="No Identitas" name="noidentitas" value="{ktp}" required>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="nohp">No Hp</label>
								<div class="col-md-8">
									<input tabindex="20" type="text" class="form-control" id="nohp" placeholder="No HP" name="nohp" value="{nohp}" required>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="telprumah">Telepon Rumah</label>
								<div class="col-md-8">
									<input tabindex="21" type="text" class="form-control" id="telprumah" placeholder="Telepon Rumah" name="telprumah" value="{telepon}">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="email">Email</label>
								<div class="col-md-8">
									<input tabindex="22" type="text" class="form-control" id="email" placeholder="Email" name="email" value="{email}">
								</div>
							</div>
							
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="golongan_darah">Golongan Darah</label>
								<div class="col-md-8">
									<select tabindex="23" id="golongan_darah" name="golongan_darah" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(11) as $row){?>
										<option value="<?=$row->id?>" <?=(($golongan_darah == '' || $golongan_darah == '0') && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($golongan_darah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="agama">Agama</label>
								<div class="col-md-8">
									<select tabindex="24" id="agama_id" name="agama_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(3) as $row){?>
										<option value="<?=$row->id?>" <?=(($agama_id == '' || $agama_id == '0') && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($agama_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="warganegara">Kewarganegaraan</label>
								<div class="col-md-8">
									<select tabindex="25" id="warganegara" name="warganegara" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(18) as $row){?>
										<option value="<?=$row->id?>" <?=(($warganegara == '' || $warganegara == '0' || $warganegara == null) && $row->st_default=='1' ? 'selected="selected"' : '')?>  <?=($warganegara == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="suku">Suku</label>
								<div class="col-md-8">
									<select tabindex="26" id="suku_id" name="suku_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(4) as $row){?>
										<option value="<?=$row->id?>" <?=($suku_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="pendidikan">Pendidikan</label>
								<div class="col-md-8">
									<select tabindex="27" id="pendidikan" name="pendidikan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(13) as $row){?>
										<option value="<?=$row->id?>" <?=($pendidikan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="pekerjaan">Pekerjaan</label>
								<div class="col-md-8">
									<select tabindex="28" id="pekerjaan" name="pekerjaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(6) as $row){?>
										<option value="<?=$row->id?>" <?=($pekerjaan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<hr>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="hubungan"></label>
								<label class="col-md-9 control-label text-primary"><h5 align="left"><b>Data Penanggung Jawab</b></h5></label>
								<div class="col-md-12">
									<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="namapenanggungjawab">Nama</label>
								<div class="col-md-8">
									<input tabindex="29" type="text" class="form-control" oninput="this.value = this.value.toUpperCase()" id="namapenanggungjawab" placeholder="Nama Penanggung Jawab" name="namapenanggungjawab" value="{namapenanggungjawab}" required>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="hubungan">Hubungan</label>
								<div class="col-md-8">
									<select tabindex="30" id="hubungan" name="hubungan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(9) as $row){?>
										<option value="<?=$row->id?>" <?=($hubungan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
									
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="alamatpenanggungjawab">Alamat</label>
								<div class="col-md-8">
									<textarea tabindex="31" class="form-control" oninput="this.value = this.value.toUpperCase()" id="alamatpenanggungjawab" placeholder="Alamat Penanggung Jawab" name="alamatpenanggungjawab"><?= $alamat_keluarga?></textarea>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-4 control-label" for="telepon">Telepon</label>
								<div class="col-md-8">
									<input tabindex="32" type="text" class="form-control" id="telepon" placeholder="Telepon" name="telepon" value="{teleponpenanggungjawab}">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 20px;">
								<label class="col-md-4 control-label" for="noidentitaspenanggung">No Identitas</label>
								<div class="col-md-8">
									<input tabindex="33" type="text" class="form-control" id="noidentitaspenanggung" placeholder="No Identitas Penanggung Jawab" name="noidentitaspenanggung" value="{noidentitaspenanggungjawab}" required>
									
								</div>
							</div>
							
						</div>
						
						<?php echo form_hidden('id', $id); ?>
				</div>
				</div>
				<!-- END Step 1 -->

				<!-- Step 2 -->
				<div class="tab-pane push-5-t push-50" id="validation-classic-step2">
					<div class="col-md-10">
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="reservasi_tipe_id">Pendaftaran <span style="color:red;">*</span></label>
							<div class="col-md-8">
								<select tabindex="5" id="reservasi_tipe_id" name="reservasi_tipe_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?if ($reservasi_tipe_id=='1'){?>
										<option value="1" <?=($reservasi_tipe_id == 1 ? 'selected="selected"' : '')?>>POLIKLINIK</option>
										<?}else{?>
										<option value="2" <?=($reservasi_tipe_id == 2 ? 'selected="selected"' : '')?>>REHABILITAS MEDIS</option>
										<?}?>
									</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="reservasi_cara">Jenis Booking <span style="color:red;">*</span></label>
							<div class="col-md-8">
								<select tabindex="5" id="reservasi_cara" name="reservasi_cara" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value=""  <?=($reservasi_cara == "" ? 'selected="selected"' : '')?>>Pilih Opsi</option>
										<option value="3" <?=($reservasi_cara == 3 ? 'selected="selected"' : '')?>>WHATSAPP</option>
										<option value="4" <?=($reservasi_cara == 4 ? 'selected="selected"' : '')?>>PHONE</option>
										<option value="5" <?=($reservasi_cara == 5 ? 'selected="selected"' : '')?>>DATANG LANGSUNG</option>
									</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="idpoli">Poliklinik Tujuan <span style="color:red;">*</span></label>
							<div class="col-md-8">
								<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
									<option value="" <?=($idpoli==''?'selected':'')?>>- Pilih Poliklinik -</option>
									<?foreach($list_poli as $r){?>
									<option value="<?=$r->id?>" <?=($idpoli==$r->id?'selected':'')?>><?=$r->nama?></option>
									<?}?>
									
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="iddokter">Dokter <span style="color:red;">*</span></label>
							<div class="col-md-8">
								<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
									<option value="" <?=($iddokter==''?'selected':'')?>>- Pilih Dokter -</option>
									<?foreach($list_dokter as $r){?>
									<option value="<?=$r->id?>" <?=($iddokter==$r->id?'selected':'')?>><?=$r->nama?></option>
									<?}?>
									
								</select>
							</div>
						</div>
						
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="tanggal">Pilih Tanggal Janji Temu <span style="color:red;">*</span></label>
							<div class="col-md-8">
								<select id="tanggal" name="tanggal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
									<?foreach($list_tanggal as $row){?>
									<option value="<?=$row->tanggal?>" <?=($tanggal == $row->tanggal ? 'selected="selected"' : '')?>><?=$row->tanggal_nama?></option>
									<?}?>
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="reservasi_tipe_id">Pilih Waktu Janji Temu <span style="color:red;">*</span></label>
							<div class="col-md-8">
								<select id="jadwal_id" name="jadwal_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
									<?foreach($list_jadwal as $row){?>
									<option value="<?=$row->jadwal_id?>" <?=($jadwal_id == $row->jadwal_id ? 'selected="selected"' : '')?>><?=$row->jam_nama?></option>
									<?}?>
								</select>
							</div>
						</div>
						<div class="form-group div_duplicate" hidden style="margin-bottom: 10px;">
							<label class="col-md-4 control-label"></label>
							<div class="col-md-8">
									<div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h3 class="font-w300 push-15">Warning</h3>
                                        <p>Pasien Tersebut Sudah Mendaftar</p>
                                    </div>
                            </div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="pertemuan_id">Jenis Pertemuan <span style="color:red;">*</span></label>
							<div class="col-md-8">
								<select id="pertemuan_id" name="pertemuan_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
									<option value="" <?=($pertemuan_id==''?'selected':'')?>>- Pilih Pertemuan -</option>
									<?foreach(list_variable_ref(15) as $r){?>
									<option value="<?=$r->id?>" <?=($pertemuan_id==$r->id?'selected':'')?> selected><?=$r->nama?></option>
									<?}?>
									
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="idasalpasien">Asal Pasien <span style="color:red;">*</span></label>
							<div class="col-md-8">
								<select tabindex="32" name="idasalpasien" id="idasalpasien" class="js-select2  form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="" <?=($idasalpasien==''?'selected':'')?>>Pilih Opsi</option>
									<?php foreach (get_all('mpasien_asal') as $r):?>
										<option value="<?= $r->id; ?>" <?=($idasalpasien == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="idtipepasien">Tipe Pasien <span style="color:red;">*</span></label>
							<div class="col-md-8">
								<select tabindex="32" name="idtipepasien" class="js-select2  form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="1" <?=($idtipepasien == 1 ? 'selected="selected"' : '')?>>Pasien RS</option>
									<option value="2" <?=($idtipepasien == 2 ? 'selected="selected"' : '')?>>Pasien Pribadi</option>
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="kelompokpasien">Cara Pembayaran <span style="color:red;">*</span></label>
							<div class="col-md-8">
								<select tabindex="34" name="kelompokpasien" id="kelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="" <?=($kelompokpasien==''?'selected':'')?>>Pilih Opsi</option>
									<?php foreach ($list_cara_bayar as $r):?>
									<option value="<?= $r->id; ?>" <?=($r->id == $kelompokpasien ? 'selected' : '')?>><?= $r->nama; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="form-group div_asuransi" style="margin-bottom: 10px;display:none">
							<label class="col-md-4 control-label" for="idrekanan">Nama Perusahaan / Asuransi <span style="color:red;">*</span></label>
							<div class="col-md-8">
								<select tabindex="35" name="idrekanan" id="idrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="" <?=($idrekanan==''?'selected':'')?>>Pilih Opsi</option>
									<?php foreach ($list_asuransi as $r) {?>
									<option value="<?= $r->id?>" <?=($idrekanan == $r->id ? 'selected="selected"' : '')?>><?= $r->nama?></option>
									<?php }?>
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px" hidden>
							<label class="col-md-4 control-label" for="idrekanan">Skrinning<span style="color:red;">*</span></label>
							<div class="col-md-8">
								<input class="form-control" name="st_gc" id="st_gc" value="{st_gc}" type="text">
								<input class="form-control" name="st_sp" id="st_sp" value="{st_sp}" type="text">
								<input class="form-control" name="st_sc" id="st_sc" value="{st_sc}" type="text">
								<input class="form-control" name="suku" id="suku" value="{suku}" type="text">
							</div>
						</div>
						<?
						if ($foto_kartu==''){
							$foto_kartu='default.png';
						}
						?>
						<div class="form-group div_foto" style="margin-bottom: 10px;display:none">
							<label class="col-md-4 control-label" for="foto_kartu">Foto Kartu Asuransi<span style="color:red;">*</span></label>
							<div class="col-md-8">
								<a href="{upload_path}foto_kartu/<?=$foto_kartu?>" >
									<img class="img-responsive" for="foto_kartu"  src="{upload_path}foto_kartu/<?=$foto_kartu?>" id="output_img" style="width:30%" />
								</a>
							</div>
						</div>
						<div class="form-group div_foto" style="margin-bottom: 10px;display:none">
							<label class="col-md-4 control-label" for="foto_kartu"> <span style="color:red;">*</span></label>
							<div class="col-md-8">
							
									<input type="file"  accept="image/*" onchange="loadFile_foto(event)"  class="btn btn-sm btn-primary" id="foto_kartu" name="foto_kartu" value="{foto_kartu}" />
							</div>
						</div>
						
					</div>
				</div>
				<!-- END Step 2 -->

				<!-- Step 3 -->
				<div class="tab-pane push-10-t push-50" id="validation-classic-step3">
					
						<div class="form-group push-5 push-5-r push-5-l">
							<div class="col-md-6 push-5">
								<label for="validation-classic-city">Jenis Booking</label>
								<input class="form-control lbl_jenis_booking" disabled type="text">
								
							</div>
							<div class="col-md-6 push-5">
								<label for="validation-classic-city">Status Pasien</label>
								<input class="form-control lbl_status_pasien" disabled type="text">
							</div>
						</div>
						<div class="form-group push-5 push-5-r push-5-l">
							<div class="col-md-6 push-5">
								<label for="validation-classic-city">Nama Pasien</label>
								<input class="form-control lbl_nama" disabled type="text">
							</div>
							<div class="col-md-6 push-5">
								<label for="validation-classic-city">No Rekam Medis</label>
								<input class="form-control lbl_norekammedis" disabled type="text">
							</div>
						</div>
						<div class="form-group push-5 push-5-r push-5-l">
							<div class="col-md-6 push-5">
								<label for="validation-classic-city">Klinik Tujuan</label>
								<input class="form-control lbl_tujuan" disabled type="text">
							</div>
							<div class="col-md-6 push-5">
								<label for="validation-classic-city">Dokter</label>
								<input class="form-control lbl_dokter" disabled type="text">
							</div>
						</div>
						<div class="form-group push-5 push-5-r push-5-l">
							<div class="col-md-6 push-5">
								<label for="validation-classic-city">Tanggal dan Waktu Janji Temu</label>
								<input class="form-control lbl_tanggal_janji" disabled type="text">
							</div>
							<div class="col-md-6 push-5">
								<label for="validation-classic-city">Jenis Pertemuan</label>
								<input class="form-control lbl_jenis_pertemuan" disabled type="text">
							</div>
						</div>
						<div class="form-group push-5 push-5-r push-5-l">
							<div class="col-md-6 push-5">
								<label for="validation-classic-city">Cara Pembayaran</label>
								<input class="form-control lbl_cara_pembayaran" disabled type="text">
							</div>
							<div class="col-md-6 push-5 div_asuransi">
								<label for="validation-classic-city">Nama Perusahaan Asuransi</label>
								<input class="form-control lbl_asuransi" disabled type="text">
							</div>
						</div>
						<div class="form-group push-5 push-5-r push-5-l">
							<div class="col-md-6 push-5">
								<div class="col-md-8 push-5">
									<div class="checkbox">
										<label for="example-checkbox1">
											<input type="checkbox" id="example-checkbox1" name="notif_email" value="1"> Kirim Pemberitahuan melalui email
										</label>
										
									</div>
									<div class="checkbox">
										<label for="example-checkbox2">
											<input type="checkbox" id="example-checkbox2" name="notif_wa" value="1"> Kirim Pemberitahuan melalui whatsapp
										</label>
										
									</div>
								</div>
								<div class="col-md-4 push-5">
									<a class="block block-link-hover2" href="javascript:void(0)">
										<div class="block-content block-content-full text-center bg-image">
											<img class="img-responsive center" style="width:100%" src="<?=$base_url?>qrcode/get_barcode/<?= $kode_booking ?>" alt="<?= $kode_booking ?>" title="<?= $kode_booking ?>">
										</div>
										<div class="block-content block-content-full text-center">
											<div class="font-w600 push-5"><?=$kode_booking?></div>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-6 push-5 div_not_duplicate">
									<button class="wizard-finish btn btn-success" type="submit" value="1" name="btn_simpan"><i class="fa fa-save"></i> Simpan</button>
									<button class="btn btn-info " type="submit" value="2" id="btn_skrining"  name="btn_simpan"><i class="fa fa-check"></i> Lanjut Skrining</button>
									<a href="{base_url}tbooking_rehab/add_reservasi" class="wizard-finish btn btn-danger" type="submit" value="3" ><i class="fa fa-reply"></i> Batal</a>
							</div>
							<div class="col-md-6 push-5 div_duplicate">
										<div class="alert alert-danger alert-dismissable">
                                        <h3 class="font-w300 push-15">Warning</h3>
                                        <p>Pasien Tersebut Sudah Mendaftar</p>
                                    </div>
							</div>
						</div>
						
						<div class="form-group push-5 push-5-r push-5-l">
							
						</div>
					
					
				</div>
				<!-- END Step 3 -->
			</div>
			<!-- END Steps Content -->

			<!-- Steps Navigation -->
			<div class="block-content block-content-mini block-content-full border-t">
				<div class="row">
					<div class="col-xs-6">
						<button class="wizard-prev btn btn-default" type="button"><i class="fa fa-arrow-left"></i> Previous</button>
					</div>
					<div class="col-xs-6 text-right">
						<button class="wizard-next btn btn-default" type="button">Next <i class="fa fa-arrow-right"></i></button>
					
					</div>
				</div>
			</div>
			<!-- END Steps Navigation -->
		</form>
		<!-- END Form -->
	</div>
</div>

<?$this->load->view('Tbooking/modal_pasien')?>
<script src="{js_path}plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}	plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{js_path}pages/base_forms_wizard.js"></script>
<script type="text/javascript">
var table;
var st_load_manual;
$(document).ready(function(){	
	
	
	load_status_pasien();
	show_hide_button();
	$("#tmp_medrec").select2({
        minimumInputLength: 6,
		noResults: 'Data Tidak ditemukan',
		// allowClear: true
        // tags: [],
        ajax: {
            url: '{site_url}tbooking/select2_pasien/',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
		 data: function (params) {
              var query = {
                search: params.term,
              }
              return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.no_medrec +' - '+item.nama,
                            id: item.id,
							// idtipe : item.idtipe
                        }
                    })
                };
            }
        }
    });

	st_load_manual=false;
})	
function validate_final(){
	$("#cover-spin").show();
	return true;
}
$("#statuspasienbaru").change(function(){
	load_status_pasien();
});
$("#reservasi_cara").change(function(){
	auto_pertemuan();
});

function auto_pertemuan(){
	let statuspasienbaru=$("#pertemuan_id").val();
	if ($("#pertemuan_id").val()==''){
	$("#pertemuan_id").val(statuspasienbaru).trigger('change');
		
	}
}
function load_status_pasien(){
	let statuspasienbaru=$("#statuspasienbaru").val();
	if (statuspasienbaru=='1'){//Baru
		$("#idpasien").val('');
		document.getElementById('no_medrec').style.display = 'block';
        document.getElementById('div_medrec').style.display = 'none';
		$('#btn_cari_pasien').attr('disabled', true);
		st_load_manual=false;
	}else{
		document.getElementById('no_medrec').style.display = 'none';
        document.getElementById('div_medrec').style.display = 'block';
		$('#btn_cari_pasien').attr('disabled', false)
		$("#btn_cari_pasien").disabled=false;
		st_load_manual=true;
	}
	// alert('sini');
	
}

$(document).on("click", ".selectPasien", function() {
	$("#cover-spin").show();
	var idpasien = ($(this).data('idpasien'));
	load_detail_pasien(idpasien);
	setTimeout(function() {
	  let newOption = new Option($("#no_medrec").val(), $("#idpasien").val(), true, true);
		$("#tmp_medrec").append(newOption);
	}, 1000);

	
	return false;
});
$(document).on("change", "#tmp_medrec", function() {
	$("#cover-spin").show();
	var idpasien = $(this).val();
	load_detail_pasien(idpasien);
	return false;
});
function load_detail_pasien(idpasien){
	$.ajax({
		url: "{site_url}tpoliklinik_pendaftaran/getDataPasien/" + idpasien,
		method: "POST",
		dataType: "json",
		success: function(data) {
			if (data[0].noid_lama){
				sweetAlert("Informasi", "Data PASIEN <strong>'"+data[0].noid_lama+"'</strong> tidak akan digunakan karena <br> telah digabungkan dengan data PASIEN <strong>'"+data[0].no_medrec+"'</strong> <br><br> data PASIEN <strong>'"+data[0].no_medrec+"'</strong> akan digunakan.", "info");
			}
			st_load_manual=true;
			var status_kawin = (data[0].status_kawin != 0 ? data[0].status_kawin : 1);
			var jenis_kelamin = (data[0].jenis_kelamin != 0 ? data[0].jenis_kelamin : 1);

			$('#status_cari').val(1);
			$('#ckabupaten').val(data[0].kabupaten_id);
			$('#ckecamatan').val(data[0].kecamatan_id);
			$('#ckelurahan').val(data[0].kelurahan_id);

			$('#idpasien').val(data[0].id);
			$('#no_medrec').val(data[0].no_medrec);
			$('#no_medrec_lama').val(data[0].noid_lama);
			$('#namapasien').val(data[0].nama);
			$('#alamatpasien').val(data[0].alamat_jalan);
			$('#jenis_kelamin').select2("val", jenis_kelamin);
			$('#jenis_id').select2("val", data[0].jenis_id);
			$('#noidentitas').val(data[0].ktp);
			$('#nohp').val(data[0].hp);
			$('#telprumah').val(data[0].telepon);
			$('#email').val(data[0].email);
			$('#tempatLahir').val(data[0].tempat_lahir);


			// Pecahan Tanggal Lahir (Hari, Bulan, Tahun)
			var tanggalLahir = data[0].tanggal_lahir;
			var tahun = tanggalLahir.substr(0, 4);
			var bulan = tanggalLahir.substr(5, 2);
			var hari = tanggalLahir.substr(8, 2);
			// alert(bulan);
			// $('#tahun').val(tahun);
			$("#tahun").val(tahun).trigger('change');
			$("#bulan").val(bulan).trigger('change');
			$("#hari").val(hari).trigger('change');

			generate_tanggal_lahir();
			$('#golongan_darah').val(data[0].golongan_darah).trigger('change');
			$('#agama_id').val(data[0].agama_id).trigger('change');
			$('#warganegara').val(data[0].warganegara).trigger('change');
			$('#suku_id').val(data[0].suku_id).trigger('change');
			$('#statuskawin').val(status_kawin).trigger('change').trigger('change');
			$('#pendidikan').val(data[0].pendidikan_id).trigger('change').trigger('change');
			$('#pekerjaan').val(data[0].pekerjaan_id).trigger('change').trigger('change');
			$('#catatan').val(data[0].catatan);
			$('#namapenanggungjawab').val(data[0].nama_keluarga);
			$('#hubungan').val(data[0].hubungan_dengan_pasien).trigger('change');
			$('#alamatpenanggungjawab').val(data[0].alamat_keluarga);
			$('#telepon').val(data[0].telepon_keluarga);
			$('#noidentitaspenanggung').val(data[0].ktp_keluarga);

			$('#provinsi').val(data[0].provinsi_id).trigger('change');
			// console.log(data[0].provinsi_id);
			$('#kabupaten1').val(data[0].kabupaten_id).trigger('change');
			// $('#kecamatan1').val(data[0].kecamatan_id).trigger('change');
			// $('#kelurahan').val(data[0].kelurahan_id).trigger('change');
			$("#cover-spin").hide();
			// getHistoryKunjungan(idpasien);
			// get_duplicate_hariini();
			generate_rincian();
			
			st_load_manual=false;

		}
	});
}
$("#hari,#bulan,#tahun").change(function() {
	console.log(st_load_manual);
	if (st_load_manual==false){
		generate_tanggal_lahir();
	}
});	
$("#statuspasienbaru").change(function() {
	if ($("#statuspasienbaru").val()=='1'){//Baru
		$.ajax({
			url: '{site_url}tbooking/get_nomedrec',
			method: "POST",
			dataType: "json",
			data: {
				
			},
			success: function(data) {
				$("#no_medrec").val(data.no_medrec);
			}
		});
	}else{
		$("#no_medrec").val('');
	}
});	
function generate_tanggal_lahir(){
// $("#hari").change(function() {
	var hari = $("#hari").val();
	var bulan = $("#bulan").val();
	var tahun = $("#tahun").val();
	var tanggal = tahun + "-" + bulan + "-" + hari;
	$.ajax({
		url: '{site_url}tpoliklinik_pendaftaran/getDate',
		method: "POST",
		dataType: "json",
		data: {
			"hari": hari,
			"bulan": bulan,
			"tahun": tahun,
			"tanggal": tanggal
		},
		success: function(data) {
			if (tahun == "0" && bulan == "0") {
				$("#umurtahun").val("0");
				$("#umurbulan").val("0");
				$("#umurhari").val(hari);
			} else {
				$("#umurtahun").val(data.date[0].tahun);
				$("#umurbulan").val(data.date[0].bulan);
				$("#umurhari").val(data.date[0].hari);
			}

			let statuskawin = ($("#statuskawin option:selected").val() != '' ? $("#statuskawin option:selected").val() : 1);
			let jeniskelamin = ($("#jenis_kelamin option:selected").val() != '' ? $("#jenis_kelamin option:selected").val() : 1);

			getTitlePasien(data.date[0].tahun, data.date[0].bulan, data.date[0].hari, statuskawin, jeniskelamin);
		}
	});
		// });
}
$("#provinsi").change(function() {
	$("#kabupaten").css("display", "block");
	var kodeprovinsi = $("#provinsi").val();
	$.ajax({
		url: '{site_url}tpoliklinik_pendaftaran/getKabupaten',
		method: "POST",
		dataType: "json",
		data: {
			"kodeprovinsi": kodeprovinsi
		},
		success: function(data) {
			$("#kabupaten1").empty();
			$("#kabupaten1").append("<option value=''>Pilih Opsi</option>");
			for (var i = 0; i < data.length; i++) {
					$("#kabupaten1").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
			}

			$("#kabupaten1").selectpicker("refresh");
			if ($("#status_cari").val()=='1'){
				$('#kabupaten1').val($("#ckabupaten").val()).trigger('change');
			}
		},
		error: function(data) {
			console.log(data);
		}
	});
});

$("#kabupaten1").change(function() {
	$("#kecamatan").css("display", "block");
	var kodekab = $("#kabupaten1").val();
	$.ajax({
		url: '{site_url}tpoliklinik_pendaftaran/getKecamatan',
		method: "POST",
		dataType: "json",
		data: {
			"kodekab": kodekab
		},
		success: function(data) {
			$("#kecamatan1").empty();
			$("#kecamatan1").append("<option value=''>Pilih Opsi</option>");
			for (var i = 0; i < data.length; i++) {
				$("#kecamatan1").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
			}
			$("#kecamatan1").selectpicker("refresh");
			if ($("#status_cari").val()=='1'){
				$('#kecamatan1').val($("#ckecamatan").val()).trigger('change');
			}
		},
		error: function(data) {
			alert(data);
		}
	});
});

$("#kecamatan1").change(function() {
	$("#kelurahan").css("display", "block");
	var kodekec = $("#kecamatan1").val();
	$.ajax({
		url: '{site_url}tpoliklinik_pendaftaran/getKelurahan',
		method: "POST",
		dataType: "json",
		data: {
			"kodekec": kodekec
		},
		success: function(data) {
			$("#kelurahan1").empty();
			$("#kelurahan1").append("<option value=''>Pilih Opsi</option>");
			for (var i = 0; i < data.length; i++) {
				$("#kelurahan1").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
			}
			$("#kelurahan1").selectpicker("refresh");
			if ($("#status_cari").val()=='1'){
				$('#kelurahan1').val($("#ckelurahan").val()).trigger('change');
			}
		},
		error: function(data) {
			alert(data);
		}
	});
});

$("#kelurahan1").change(function() {
	var kelurahan = $("#kelurahan1").val();
	$.ajax({
		url: '{site_url}tpoliklinik_pendaftaran/getKodepos',
		method: "POST",
		dataType: "json",
		data: {
			"kodekel": kelurahan
		},
		success: function(data) {
			$("#kodepos").empty();
			if (data.length > 0) {
				$("#kodepos").val(data[0].kodepos);
			}
		}
	});
});
function getTitlePasien(tahun, bulan, hari, statuskawin, jenis_kelamin) {
	console.log(tahun+' Bulan '+bulan+' Jenis K '+jenis_kelamin);
		if (tahun == 0 && bulan <=5) {
			$('#titlepasien').val("By").trigger('change');
		} else if ((tahun == 0 && bulan > 5) || tahun <= 15) {
	console.log('Anak Bro');
			$('#titlepasien').val("An").trigger('change');
		} else if (tahun >= 16 && jenis_kelamin == '1') {
			$('#titlepasien').val("Tn").trigger('change');
		} else if (tahun >= 15 && jenis_kelamin == '2' && statuskawin == 1) {
			$('#titlepasien').val("Nn").trigger('change');
		} else if (jenis_kelamin == '2' && statuskawin == 2) {
			$('#titlepasien').val("Ny").trigger('change');
		} else if (jenis_kelamin == '2' && statuskawin == 3) {
			$('#titlepasien').val("Ny").trigger('change');
		} else if (jenis_kelamin == '2' && statuskawin == 4) {
			$('#titlepasien').val("Tn").trigger('change');
	console.log('Tuan Bro');
			
		}
	console.log($('#titlepasien').val());
	}
	
// Perjanjian
function change_poli(){
	var idpoli = $("#idpoli").val();
	generate_rincian();
	$.ajax({
		url: '{site_url}tbooking/get_dokter_poli',
		method: "POST",
		dataType: "json",
		data: {
			"idpoli": idpoli
		},
		success: function(data) {
			$("#iddokter").empty();
			$("#tanggal").empty();
			$("#iddokter").append("<option value=''>Pilih Dokter</option>");
			for (var i = 0; i < data.length; i++) {
					$("#iddokter").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
			}

			$("#iddokter").selectpicker("refresh");
			// if ($("#status_cari").val()=='1'){
				// $('#kabupaten1').val($("#ckabupaten").val()).trigger('change');
			// }
		},
		error: function(data) {
			console.log(data);
		}
	});
}
$("#iddokter").change(function() {
	change_dokter();
	cek_duplicate();
});
$("#jenis_kelamin,#statuskawin").change(function() {
	let statuskawin = ($("#statuskawin option:selected").val() != '' ? $("#statuskawin option:selected").val() : 1);
	let jeniskelamin = ($("#jenis_kelamin option:selected").val() != '' ? $("#jenis_kelamin option:selected").val() : 1);

	getTitlePasien($("#umurtahun").val(), $("#umurbulan").val(), $("#umurhari").val(), statuskawin, jeniskelamin);
});

function change_dokter(){
	generate_rincian();
	console.log($("#iddokter option:selected").text());
	var idpoli = $("#idpoli").val();
	var iddokter = $("#iddokter").val();
	$("#jadwal_id").empty();
	$("#jadwal_id").append("<option value=''>Pilih Waktu</option>");
	$.ajax({
		url: '{site_url}tbooking/get_jadwal_dokter_poli',
		method: "POST",
		dataType: "json",
		data: {
			"idpoli": idpoli,
			"iddokter": iddokter,
		},
		success: function(data) {
			$("#tanggal").empty();
			let alasan='';
			$("#tanggal").append("<option value=''>Pilih Tanggal</option>");
			for (var i = 0; i < data.length; i++) {
				if (data[i].cuti_id){
					alasan=' ( ' +data[i].sebab+' )';					
				}else{
					alasan='';
				}
					$("#tanggal").append("<option value='" + data[i].tanggal + "' "+ data[i].st_disabel +">" + data[i].tanggal_nama + alasan+ "</option>");
			}

			$("#tanggal").selectpicker("refresh");
			// if ($("#status_cari").val()=='1'){
				// $('#kabupaten1').val($("#ckabupaten").val()).trigger('change');
			// }
		},
		error: function(data) {
			console.log(data);
		}
	});
}
$("#idpoli").change(function() {
	change_poli();
	cek_duplicate();
});
function change_jadwal_id(){
	var idpoli = $("#idpoli").val();
	var iddokter = $("#iddokter").val();
	var tanggal = $("#tanggal").val();
	$.ajax({
		url: '{site_url}tbooking/get_jam_dokter_poli',
		method: "POST",
		dataType: "json",
		data: {
			"idpoli": idpoli,
			"iddokter": iddokter,
			"tanggal": tanggal,
		},
		success: function(data) {
			$("#jadwal_id").empty();
			$("#jadwal_id").append("<option value=''>Pilih Waktu</option>");
			let alasan='';
			for (var i = 0; i < data.length; i++) {
				if (data[i].saldo_kuota<=0){
					alasan=' ( TIDAK TERSEDIA )';					
				}else{
					alasan='';
				}
					$("#jadwal_id").append("<option value='" + data[i].jadwal_id + "' "+ data[i].st_disabel +">" + data[i].jam_nama+ alasan+"</option>");
			}

			$("#jadwal_id").selectpicker("refresh");
			// if ($("#status_cari").val()=='1'){
				// $('#kabupaten1').val($("#ckabupaten").val()).trigger('change');
			// }
		},
		error: function(data) {
			console.log(data);
		}
	});
}
$("#tanggal").change(function() {
	change_jadwal_id();
	generate_rincian();
	cek_duplicate();
});
$("#kelompokpasien").change(function() {
	let kelompokpasien=$("#kelompokpasien").val();
	if (kelompokpasien=='1'){
		$(".div_asuransi").css("display", "block");
	}else{
		
		$(".div_asuransi").css("display", "none");
	}
	if (kelompokpasien=='5'){
		$(".div_foto").css("display", "none");
	}else{
		
		$(".div_foto").css("display", "block");
	}
	generate_rincian();
});
var loadFile_foto = function(event) {
var output_img = document.getElementById('output_img');
output_img.src = URL.createObjectURL(event.target.files[0]);
};
$("#iddokter,#namapasien,#no_medrec,#pertemuan_id,#idrekanan,#jadwal_id,#statuspasienbaru").change(function() {
	generate_rincian();
});
$("#pertemuan_id,#idpoli,#iddokter").change(function() {
	get_logic_pendaftaran_poli();
});
function generate_rincian(){
	$(".lbl_dokter").val($("#iddokter option:selected").text());
	$(".lbl_tujuan").val($("#idpoli option:selected").text());
	$(".lbl_jenis_pertemuan").val($("#pertemuan_id option:selected").text());
	$("#suku").val($("#suku_id option:selected").text());
	$(".lbl_asuransi").val($("#idrekanan option:selected").text());
	$(".lbl_status_pasien").val($("#statuspasienbaru option:selected").text());
	$(".lbl_jenis_booking").val($("#reservasi_cara option:selected").text());
	$(".lbl_cara_pembayaran").val($("#kelompokpasien option:selected").text());
	$(".lbl_tanggal_janji").val($("#tanggal option:selected").text() + ' ('+$("#jadwal_id option:selected").text()+')');
	$(".lbl_nama").val($("#namapasien").val());
	$(".lbl_norekammedis").val($("#no_medrec").val());
	// $("#suku").val($("#no_medrec").val());
	// $(".lbl_asuransi").val($("#no_medrec").val());
	cek_duplicate();
}
function get_logic_pendaftaran_poli(){
	 let jenis_pertemuan_id=$("#pertemuan_id").val();
	  let idpoliklinik=$("#idpoli").val();;
	  let iddokter=$("#iddokter").val();
	  
	  $.ajax({
		url: '{site_url}tbooking/get_logic_pendaftaran_poli',
		method: "POST",
		dataType: "json",
		data: {
			"jenis_pertemuan_id": jenis_pertemuan_id,
			"idpoliklinik": idpoliklinik,
			"iddokter": iddokter,
		},
		success: function(data) {
			$("#st_gc").val(data.st_gc);
			$("#st_sp").val(data.st_sp);
			$("#st_sc").val(data.st_sc);
			show_hide_button()
		}
	});
}
function show_hide_button(){
	let total_skrining=0;
	total_skrining = parseFloat($("#st_sp").val()) + parseFloat($("#st_sc").val())+ parseFloat($("#st_gc").val());
	if (total_skrining>0){
		$("#btn_skrining").show();
	}else{
		$("#btn_skrining").hide();
		// alert('sini');
		
	}
	if ($("#st_sp").val()=='1' || $("#st_sc").val()=='1'){
		$("#btn_skrining").text('Lanjut Skrining Pasien');
	}
	if ($("#st_gc").val()=='1'){
		$("#btn_skrining").text('Lanjut General Consent');
	}
}
function cek_duplicate(){
	let iddokter=$("#iddokter").val();
	let jadwal_id=$("#jadwal_id").val();
	let idpoli=$("#idpoli").val();
	let idpasien=$("#idpasien").val();
	let tanggal=$("#tanggal").val();
	if (idpoli!='' && idpasien!=''){
		$.ajax({
			url: '{site_url}tbooking/cek_duplicate',
			method: "POST",
			dataType: "json",
			data: {
				"iddokter": iddokter,
				"jadwal_id": jadwal_id,
				"idpoli": idpoli,
				"idpasien": idpasien,
				"tanggal": tanggal,
			},
			success: function(data) {
				console.log(data);
				if (data){//Duplicate
					$(".div_duplicate").show();
					$(".div_not_duplicate").hide();
				}else{
					$(".div_duplicate").hide();
					$(".div_not_duplicate").show();
				}
			}
		});
	}
}
</script>