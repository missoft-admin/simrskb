<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<style>
	.scrollme {
		overflow-x: auto;
	}
    .profile-dockter {
        background-color: #379ee7;
    }
    .profile-dockter .image {
        padding: 10px 10px 0 10px;
    }
    .profile-dockter .image img {
        width: 200px;
    }
    .profile-dockter .information {
        text-align: center;
        padding: 16px;
        color: #fff;
    }
    .profile-dockter .information .name {
        font-weight: bold;
        font-size: 14px;
    }
    .profile-dockter .information .uniqueId {
        font-size: 12px;
    }

    .timetable {
        color: #fff;
        width: 100%;
        padding: 8px;
        margin-bottom: 10px;
    }
    .timetable .time {
        font-size: 14px;
        font-weight: bold;
    }
    .timetable.success {
        background-color: #e9ad25;
    }
    .timetable.danger {
        background-color: #c54736;
    }
	.timetable.default {
        background-color: #7a7a7a;
    }
	.pasien-hadir {
        background-color: #379ee7;
    }
	.pasien-hapus {
        background-color: #c54736;
    }
	.pasien-belum_hadir {
        background-color: #a59e9d;
    }
	.pasien-beres {
        background-color: #1cbd46;
    }
    .column-day {
        background-color: #000000;
        font-weight: bold;
        color: #fff;
    }
	.column-libur {
        background-color: #f31313;
        font-weight: bold;
        color: #fff;
    }
	.column-poli {
        background-color: #379ee7;
        font-weight: bold;
        color: #fff;
    }

    .header-section {
        padding: 8px;
        font-weight: bold;
        text-decoration: underline;
        text-transform: uppercase;
    }
</style>
<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1497'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_1"  onclick="load_index_proses()"><i class="fa fa-calendar-plus-o"></i> Proses Reservasi</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1504'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_2"  onclick="load_index_daftar()"><i class="fa fa-calendar-check-o"></i> Daftar Reservasi</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1506'))){ ?>
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab_3"  onclick="hasil_generate_ls()"><i class="fa fa-calendar"></i> Schedule Reservasi</a>
		</li>
		<?}?>
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('1497'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_1">
			<?php if (UserAccesForm($user_acces_form,array('1498'))): ?>
			<div class="row">
				<div class="col-xs-6">
					
				</div>
				<div class="col-xs-6 text-right">
					<a class="btn btn-xs btn-danger  push-10 " href="{base_url}tbooking/add_reservasi" ><i class="fa fa-plus"></i> Tambah Reservasi</a>
					
				</div>
			</div>
			<?endif;?>
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Jenis Reservasi</label>
						<div class="col-md-9">
							<select id="reservasi_cara" name="reservasi_cara" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Reservasi -</option>
								<?foreach($list_reservasi_cara as $r){?>
								<option value="<?=$r['id']?>"><?=$r['nama']?></option>
								<?}?>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Status Pasien</label>
						<div class="col-md-9">
							<select id="statuspasienbaru" name="statuspasienbaru" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Status -</option>
								<option value="1">BARU</option>
								<option value="0">LAMA</option>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Status Reservasi</label>
						<div class="col-md-9">
							<select id="status_reservasi" name="status_reservasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Status -</option>
								<option value="2">SUDAH DIPROSES</option>
								<option value="1">BELUM DIPROSES</option>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Status Kehadiran</label>
						<div class="col-md-9">
							<select id="status_kehadiran" name="status_kehadiran" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Status -</option>
								<option value="1">HADIR</option>
								<option value="0">BELUM HADIR</option>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tanggal Reservasi</label>
						<div class="col-md-9">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" id="tanggal_1" name="tanggal_1" placeholder="From" value="{tanggal_1}"/>
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" id="tanggal_2" name="tanggal_2" placeholder="To" value="{tanggal_2}"/>
							</div>
						</div>
					</div>
					
					
				</div>
				<div class="col-md-6 push-10">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">No Medrec</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="no_medrec" placeholder="No. Medrec" name="no_medrec" value="{no_medrec}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Nama Pasien</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="namapasien" placeholder="Nama Pasien" name="namapasien" value="{namapasien}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
						<div class="col-md-9">
							<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Poliklinik -</option>
								<?foreach($list_poli as $r){?>
								<option value="<?=$r->id?>"><?=$r->nama?></option>
								<?}?>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tujuan Dokter</label>
						<div class="col-md-9">
							<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Dokter -</option>
								<?foreach($list_dokter as $r){?>
								<option value="<?=$r->id?>"><?=$r->nama?></option>
								<?}?>
								
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tanggal Transaksi</label>
						<div class="col-md-9">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" id="waktu_reservasi_1" name="waktu_reservasi_1" placeholder="From" value="{waktu_reservasi_1}"/>
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" id="waktu_reservasi_2" name="waktu_reservasi_2" placeholder="To" value="{waktu_reservasi_2}"/>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label" for="btn_filter_proses"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="button" id="btn_filter_proses" name="btn_filter_proses" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						</div>
					</div>
						
				</div>
			</div>
			<?php echo form_close() ?>
			<div class="row   ">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="index_proses">
							<thead>
								<tr>
									<th width="10%">No</th>
									<th width="10%">Jenis Reservasi</th>
									<th width="10%">Tanggal Transaksi</th>
									<th width="10%">Status Pasien</th>
									<th width="10%">Pasien</th>
									<th width="10%">Tujuan</th>
									<th width="10%">Tanggal Reservasi</th>
									<th width="10%">Antrian</th>
									<th width="10%">Status Reservasi</th>
									<th width="10%">Status Kehadiran</th>
									<th width="15%">Action</th>
								   
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1504'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_2">
			<?php if (UserAccesForm($user_acces_form,array('1498'))): ?>
			<div class="row">
				<div class="col-xs-6">
					
				</div>
				<div class="col-xs-6 text-right">
					<a class="btn btn-xs btn-danger  push-10 " href="{base_url}tbooking/add_reservasi" ><i class="fa fa-plus"></i> Tambah Reservasi</a>
					
				</div>
			</div>
			<?endif;?>
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Jenis Reservasi</label>
						<div class="col-md-9">
							<select id="reservasi_cara2" name="reservasi_cara2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Reservasi -</option>
								<?foreach($list_reservasi_cara as $r){?>
								<option value="<?=$r['id']?>"><?=$r['nama']?></option>
								<?}?>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Status Pasien</label>
						<div class="col-md-9">
							<select id="statuspasienbaru2" name="statuspasienbaru2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Status -</option>
								<option value="1">BARU</option>
								<option value="0">LAMA</option>
								
							</select>
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Status Kehadiran</label>
						<div class="col-md-9">
							<select id="status_kehadiran2" name="status_kehadiran2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Status -</option>
								<option value="1">HADIR</option>
								<option value="0">BELUM HADIR</option>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tanggal Reservasi</label>
						<div class="col-md-9">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" id="tanggal_12" name="tanggal_1" placeholder="From" value="{tanggal_1}"/>
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" id="tanggal_22" name="tanggal_2" placeholder="To" value="{tanggal_2}"/>
							</div>
						</div>
					</div>
					
					
				</div>
				<div class="col-md-6 push-10">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">No Medrec</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="no_medrec2" placeholder="No. Medrec" name="no_medrec2" value="{no_medrec}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Nama Pasien</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="namapasien2" placeholder="Nama Pasien" name="namapasien2" value="{namapasien}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
						<div class="col-md-9">
							<select id="idpoli2" name="idpoli2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Poliklinik -</option>
								<?foreach($list_poli as $r){?>
								<option value="<?=$r->id?>"><?=$r->nama?></option>
								<?}?>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tujuan Dokter</label>
						<div class="col-md-9">
							<select id="iddokter2" name="iddokter2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Dokter -</option>
								<?foreach($list_dokter as $r){?>
								<option value="<?=$r->id?>"><?=$r->nama?></option>
								<?}?>
								
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tanggal Transaksi</label>
						<div class="col-md-9">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" id="waktu_reservasi_12" name="waktu_reservasi_12" placeholder="From" value="{waktu_reservasi_1}"/>
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" id="waktu_reservasi_22" name="waktu_reservasi_22" placeholder="To" value="{waktu_reservasi_2}"/>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label" for="btn_filter_proses"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="button" id="btn_filter_daftar" name="btn_filter_daftar" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						</div>
					</div>
						
				</div>
			</div>
			<?php echo form_close() ?>
			<div class="row   ">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="index_daftar">
							<thead>
								<tr>
									<th width="10%">No</th>
									<th width="10%">Jenis Reservasi</th>
									<th width="10%">Tanggal Transaksi</th>
									<th width="10%">Status Pasien</th>
									<th width="10%">Pasien</th>
									<th width="10%">Tujuan</th>
									<th width="10%">Tanggal Reservasi</th>
									<th width="10%">Antrian</th>
									<th width="10%">Status Reservasi</th>
									<th width="10%">Kelompok Pasien</th>
									<th width="15%">Action</th>
								   
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<?}?>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?>" id="tab_3">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label class="col-md-12" for="login1-username">Tujuan Kunjungan</label>
						<div class="col-md-12">
							<select id="idpoli3" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
								<?foreach($list_poli as $r){?>
								<option value="<?=$r->id?>"><?=$r->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label class="col-md-12" for="login1-username">Dokter</label>
						<div class="col-md-12">
							<select id="iddokter3" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
								<?foreach($list_dokter as $r){?>
								<option value="<?=$r->id?>"><?=$r->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label class="col-md-12" for="login1-username">View</label>
						<div class="col-md-12">
							<select id="present" name="present" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								
								<option value="1" <?=($present=="1"?'selected':'')?>>MONTHLY</option>
								<option value="2" <?=($present=="2"?'selected':'')?>>WEEKLY</option>
								<option value="3" <?=($present=="3"?'selected':'')?>>DAILY</option>
								
							</select>
						</div>
					</div>
					
				</div>
				<div class="col-md-2 div_present">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-12" for="login1-username">&nbsp;</label>
						<div class="col-md-12">
						<div class="btn-group btn-group-justified">
							<div class="btn-group">
								<button class="btn btn-default" id="btn_back2" type="button"><i class="fa fa-arrow-left"></i></button>
							</div>
							
							<div class="btn-group">
								<button class="btn btn-default" id="btn_next2" type="button"><i class="fa fa-arrow-right"></i></button>
							</div>
							
						</div>
						</div>
					</div>
					
				</div>
				<div class="col-md-3 div_date">
					<div class="form-group">
						<label class="col-md-12" for="login1-username">Date</label>
						<div class="col-md-12">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" id="tanggaldari" name="tanggaldari" placeholder="From" value="{tanggaldari}">
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" id="tanggalsampai" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
							</div>
						</div>
					</div>
					
				</div>
				<div class="col-md-1 div_date">
					<div class="form-group">
						<label class="col-md-12" for="login1-username">&nbsp;</label>
						<div class="col-md-12">
							<button class="btn btn-success present" id="btn_filter_ls" type="button"><i class="fa fa-search"></i> Cari Jadwal</button>
						</div>
					</div>
					
				</div>
			</div>
			<div class="row push-20-t">
					<div class="col-md-12">
						<div class="scrollme">
						<div id="div_ls"></div>
						</div>
					</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		
	</div>
</div>

<div class="modal" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width:50%">
        <form class="form-horizontal" action="javascript:void(0)" id="frm1">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">Detail Slot</h3>
                    </div>
                    <div class="block-content">
						<div class="block">
						<ul class="nav nav-tabs" data-toggle="tabs" id="tabs">
							<li class="active" id="tab_kuota">
								<a href="#btabs-animated-slideleft-kuota"><i class="fa fa-pencil"></i> Pengaturan Slot</a>
							</li>
							<li id="tab_add">
								<a href="#btabs-animated-slideleft-note"><i class="fa fa-pencil"></i> Update Notes</a>
							</li>
							<li id="tab_note">
								<a href="#btabs-animated-slideleft-list"><i class="fa fa-list-ol"></i> History Notes</a>
							</li>
						</ul>
						<div class="block-content tab-content">
							<div class="tab-pane fade fade-left in active" id="btabs-animated-slideleft-kuota">
								<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Hari</label>
											<div class="col-md-10">
												<input class="form-control input-sm " readonly type="text" id="namahari" name="namahari" value=""/>	
												<input class="form-control input-sm " readonly type="hidden" id="xtanggal" name="xtanggal" value=""/>	
												<input class="form-control input-sm " readonly type="hidden" id="xiddokter" name="xiddokter" value=""/>	
												<input class="form-control input-sm " readonly type="hidden" id="xjadwal_id" name="xjadwal_id" value=""/>	
												<input class="form-control input-sm " readonly type="hidden" id="nama_dokter" name="nama_dokter" value=""/>	
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Poliklinik</label>
											<div class="col-md-10">
												<input class="form-control input-sm " readonly type="text" id="xnama_poli" name="xnama_poli" value=""/>	
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Dokter</label>
											<div class="col-md-10">
												<input class="form-control input-sm " readonly type="text" id="xnama_dokter" name="xnama_dokter" value=""/>	
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Jam</label>
											<div class="col-md-10">
												<input class="form-control input-sm " readonly type="text" id="jam" name="jam" value=""/>	
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="tipeid">Kuota</label>
											<div class="col-md-10">
												<select id="kuota" name="kuota" class="form-control js-select2" style="width: 100%;" data-placeholder="Choose one..">
													<option value="999">TANPA BATAS</option>
													<option value="0" selected>ISI PENUH</option>
													<?for ($x = 1; $x <= 100; $x++) {?>
														<option value="<?=$x?>"><?=$x?></option>
													<?}?>
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="tipeid">Reservasi Online</label>
											<div class="col-md-10">
												<select id="st_reservasi_online" name="st_reservasi_online" class="form-control js-select2" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1">RESERVASI ONLINE</option>
													<option value="0">HANYA OFFLINE</option>
													
												</select>
											</div>
										</div>
										
										
									</div>
									<div class="col-md-12">
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-2 control-label" for="btn_save_kuota"></label>
											<div class="col-md-10">
												<button class="btn btn-success text-uppercase"  type="button" name="btn_save_kuota" id="btn_save_kuota"><i class="fa fa-save"></i> Simpan</button>
												<button class="btn btn-danger text-uppercase"  type="button" name="btn_save_penuh" id="btn_save_penuh"><i class="si si-ban"></i> Tutup Pendaftaran</button>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_close() ?>
							</div>
							<div class="tab-pane fade fade-left" id="btabs-animated-slideleft-note">
								<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
								<div class="row">
									<div class="col-md-12">
										
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Notes</label>
											<div class="col-md-10">
												<input class="form-control input-sm " type="text" id="catatan" name="catatan" value=""/>	
											</div>
										</div>
										
									</div>
									<div class="col-md-12">
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-2 control-label" for="idprespektif"></label>
											<div class="col-md-10">
												<button class="btn btn-success text-uppercase"  type="button" name="btn_save_catatan" id="btn_save_catatan"><i class="fa fa-save"></i> Simpan</button>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_close() ?>
							</div>
							<div class="tab-pane fade fade-left " id="btabs-animated-slideleft-list">
								<h5 class="font-w300 push-15" id="lbl_list_rekening">History Notes </h5>
								<table class="table table-bordered table-striped" id="index_Catatan">
									<thead>
										<th style="width:5%">No</th>
										<th style="width:60%">Notes</th>
										<th style="width:35%">Posted By</th>
									</thead>
									<tbody></tbody>
								  
								</table>
							</div>
						</div>
					</div>
                  </div>
                </div>
                <div class="modal-footer" style="margin-top:20px">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>	
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
$(document).ready(function(){	
	var tab=$("#tab").val();
	if (tab=='1'){
		load_index_proses();
		
	}
	if (tab=='2'){
		load_index_daftar();
		
	}
	refresh_present();
	if (tab=='3'){
		hasil_generate_ls();
		
	}
	
})
function add_kuota($tanggal,$iddokter,$jadwal_id){
	// alert($tanggal);
	$("#xjadwal_id").val($jadwal_id);
	$("#xiddokter").val($iddokter);
	$("#modal_edit").modal('show');
	$('[href="#btabs-animated-slideleft-kuota"]').tab('show');
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mjadwal_dokter/get_kuota_detail/',
		method: "POST",
		dataType: "JSON",
		data: {
			iddokter:$iddokter,
			tanggal:$tanggal,
			jadwal_id:$jadwal_id
		},
		success: function(data) {
			$("#jam").val(data.jam);
			$("#xnama_dokter").val(data.nama_dokter);
			$("#xnama_poli").val(data.nama_poli);
			$("#xtanggal").val(data.tanggal);
			$("#namahari").val(data.periode_nama);
			$("#catatan").val(data.catatan);
			$("#nama_dokter").val(data.nama_dokter);
			$("#kuota").val(data.kuota).trigger('change');
			$("#st_reservasi_online").val(data.st_reservasi_online).trigger('change');
			$("#cover-spin").hide();
			load_catatan();
		}
	});
	
}
$("#btn_save_kuota").click(function() {
	$iddokter=$("#xiddokter").val();
	$tanggal=$("#xtanggal").val();
	$jadwal_id=$("#xjadwal_id").val();
	$kuota=$("#kuota").val();
	$st_reservasi_online=$("#st_reservasi_online").val();
	// alert($idpoli+' '+$kodehari+' '+$jam_id);
	$.ajax({
		url: '{site_url}mjadwal_dokter/save_kuota_detail/',
		method: "POST",
		dataType: "JSON",
		data: {
			iddokter:$iddokter,
			tanggal:$tanggal,
			jadwal_id:$jadwal_id,
			kuota:$kuota,
			st_reservasi_online:$st_reservasi_online,
		},
		success: function(data) {
			$("#modal_edit").modal('hide');
			hasil_generate_ls();
		}
	});
});
$("#btn_save_catatan").click(function() {
	$iddokter=$("#xiddokter").val();
	$tanggal=$("#xtanggal").val();
	$jadwal_id=$("#xjadwal_id").val();
	$catatan=$("#catatan").val();
	// alert($idpoli+' '+$kodehari+' '+$jam_id);
	$.ajax({
		url: '{site_url}mjadwal_dokter/save_catatan_detail/',
		method: "POST",
		dataType: "JSON",
		data: {
			iddokter:$iddokter,
			tanggal:$tanggal,
			jadwal_id:$jadwal_id,
			catatan:$catatan
		},
		success: function(data) {
			$("#modal_edit").modal('hide');
			hasil_generate_ls();
		}
	});
});
$("#btn_filter_ls").click(function() {
	hasil_generate_ls();
});	
function refresh_present(){
	present=$("#present").val();
	if (present=='3'){
		$(".div_date").show();
		$(".div_present").hide();
	}else{
		$(".div_date").hide();
		$(".div_present").show();
		
	}
}
$("#present").change(function() {
	refresh_present();
	// if ($("#present").val()!='3'){
		st_plus_minus=0;
		refresh_tanggal2();
	// }
});
function refresh_tanggal2(){
	var tanggaldari=$("#tanggaldari").val();
	var tanggalsampai=$("#tanggalsampai").val();
	present=$("#present").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/refresh_tanggal2/',
		method: "POST",
		dataType: "JSON",
		data: {
			tanggaldari:tanggaldari,
			tanggalsampai:tanggalsampai,
			present:present,
			st_plus_minus:st_plus_minus,
		},
		success: function(data) {
			$("#tanggaldari").val(data.tanggaldari);
			$("#tanggalsampai").val(data.tanggalsampai);
			$("#cover-spin").hide();
			hasil_generate_ls();
		}
	});
}
$("#btn_back2").click(function() {
	st_plus_minus='-1'
	refresh_tanggal2();
	
});
$("#btn_next2").click(function() {
	st_plus_minus='1'
	refresh_tanggal2();
	
});
$("#idpoli3").change(function() {
	hasil_generate_ls();
});
$("#iddokter3").change(function() {
	hasil_generate_ls();
});
function hasil_generate_ls(){
	tanggaldari=$("#tanggaldari").val();
	tanggalsampai=$("#tanggalsampai").val();
	let idpoli=$("#idpoli3").val();
	let iddokter=$("#iddokter3").val();
	let present=$("#present").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tbooking/hasil_generate/',
		method: "POST",
		dataType: "JSON",
		data: {
			tanggaldari:tanggaldari,
			tanggalsampai:tanggalsampai,
			present:present,
			idpoli:idpoli,
			iddokter:iddokter,
		},
		success: function(data) {
			$("#div_ls").html(data.tabel);
			$("#cover-spin").hide();
			
			// window.table = $('#index_ls').DataTable(
			  // {
			   // "autoWidth": false,
					// // "pageLength": 50,
					// autoWidth: true,
					// scrollX: true,
					// scrollY: 400,
					// "paging": true,
					// paging: false,
					// ordering: false,
					// scrollCollapse: true,
					// "processing": true,
					
				// ordering: [
				  // [1, 'asc']
				// ],
				// colReorder:
				// {
				  // fixedColumnsLeft: 3,
				  // fixedColumnsRight: 1
				// }
			  // });
			$("#cover-spin").hide();
		}
	});
}
$(document).on("click","#btn_filter_proses",function(){
	load_index_proses();
});
$(document).on("click","#btn_filter_daftar",function(){
	load_index_daftar();
});

function load_index_proses(){
	$('#index_proses').DataTable().destroy();	
	let reservasi_cara=$("#reservasi_cara").val();
	let statuspasienbaru=$("#statuspasienbaru").val();
	let status_reservasi=$("#status_reservasi").val();
	let tanggal_1=$("#tanggal_1").val();
	let tanggal_2=$("#tanggal_2").val();
	let no_medrec=$("#no_medrec").val();
	let namapasien=$("#namapasien").val();
	let idpoli=$("#idpoli").val();
	let iddokter=$("#iddokter").val();
	let waktu_reservasi_1=$("#waktu_reservasi_1").val();
	let waktu_reservasi_2=$("#waktu_reservasi_2").val();
	let status_kehadiran=$("#status_kehadiran").val();
	
	
	table = $('#index_proses').DataTable({
            autoWidth: false,
            searching: false,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "3%", "targets": 0,  className: "text-right" },
					{ "width": "8%", "targets": [1,2,3,6,7,8,9],  className: "text-center" },
					{ "width": "12%", "targets": [4,10],  className: "text-center" },
					{ "width": "15%", "targets": 5,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}tbooking/getIndex_proses', 
                type: "POST" ,
                dataType: 'json',
				data : {
						reservasi_cara:reservasi_cara,
						statuspasienbaru:statuspasienbaru,
						status_reservasi:status_reservasi,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						no_medrec:no_medrec,
						namapasien:namapasien,
						idpoli:idpoli,
						iddokter:iddokter,
						waktu_reservasi_1:waktu_reservasi_1,
						waktu_reservasi_2:waktu_reservasi_2,
						status_kehadiran:status_kehadiran,
						
					   }
            }
        });
}
function load_index_daftar(){
	$('#index_daftar').DataTable().destroy();	
	let reservasi_cara=$("#reservasi_cara2").val();
	let statuspasienbaru=$("#statuspasienbaru2").val();
	let tanggal_1=$("#tanggal_12").val();
	let tanggal_2=$("#tanggal_22").val();
	let no_medrec=$("#no_medrec2").val();
	let namapasien=$("#namapasien2").val();
	let idpoli=$("#idpoli2").val();
	let iddokter=$("#iddokter2").val();
	let waktu_reservasi_1=$("#waktu_reservasi_12").val();
	let waktu_reservasi_2=$("#waktu_reservasi_22").val();
	let status_kehadiran=$("#status_kehadiran2").val();
	
	
	table = $('#index_daftar').DataTable({
            autoWidth: false,
            searching: false,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "3%", "targets": 0,  className: "text-right" },
					{ "width": "8%", "targets": [1,2,3,6,7,8,9],  className: "text-center" },
					{ "width": "12%", "targets": [4,10],  className: "text-center" },
					{ "width": "15%", "targets": 5,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}tbooking/getIndex_daftar', 
                type: "POST" ,
                dataType: 'json',
				data : {
						reservasi_cara:reservasi_cara,
						statuspasienbaru:statuspasienbaru,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						no_medrec:no_medrec,
						namapasien:namapasien,
						idpoli:idpoli,
						iddokter:iddokter,
						waktu_reservasi_1:waktu_reservasi_1,
						waktu_reservasi_2:waktu_reservasi_2,
						status_kehadiran:status_kehadiran,
						
					   }
            }
        });
}
function verifikasi_poli(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan verifikasi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}tbooking/verifikasi_poli',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_proses').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Publish'});
					
				}
			});
	});
}
function batalkan_poli(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Batal Verifikasi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}tbooking/batal_poli',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_proses').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Publish'});
					
				}
			});
	});
}
function hapus_poli(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Reservasi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}tbooking/hapus_poli',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_proses').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Publish'});
					
				}
			});
	});
}
function load_catatan(){
	$jadwal_id=$("#xjadwal_id").val();
	$iddokter=$("#xiddokter").val();
	$tanggal=$("#xtanggal").val();
	$('#index_Catatan').DataTable().destroy();	
	table = $('#index_Catatan').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "5%", "targets": 0,  className: "text-right" },
					{ "width": "60%", "targets": [1],  className: "text-left" },
					{ "width": "35%", "targets": [2],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}tbooking/load_catatan', 
                type: "POST" ,
                dataType: 'json',
				data : {
						iddokter:$iddokter,
						tanggal:$tanggal,
						jadwal_id:$jadwal_id,
					   }
            }
        });
}

</script>