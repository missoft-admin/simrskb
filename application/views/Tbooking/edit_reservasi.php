<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<style>
    body {
      -webkit-print-color-adjust: exact;
    }
	
	
   table {
		font-family: Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;
		 
      }
	  td {
        padding: 13px;
		 
      }
      .content td {
        padding: 10px;
		border: 0px solid #6033FF;
      }
	  .has-error2 {
		border-color: #d26a5c;
	}
	  .select2-selection {
		  border-color: green; /* example */
		}
      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 14px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-bottom-top-left {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
		border-bottom:1px solid #000 !important;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
	
    </style>
<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_1" ><i class="si si-user-follow"></i> Pasien</a>
		</li>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_2"  ><i class="fa fa-calendar-check-o"></i> Tanggal Perjanjian</a>
		</li>
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab_3"><i class="fa fa-calendar"></i>  Rincian</a>
		</li>
		<?if($st_gc=='1'){?>
		<li class="<?=($tab=='4'?'active':'')?>">
			<a href="#tab_4"><i class="fa fa-check-circle"></i>  General Consent</a>
		</li>
		<?}?>
		<?if($st_sp=='1' && $st_general=='1'){?>
		<li class="<?=($tab=='5'?'active':'')?>">
			<a href="#tab_5"><i class="fa fa-check-square-o"></i>  Skrining Pasien</a>
		</li>
		<?}?>
		<?if($st_sc=='1' && $st_skrining=='1'){?>
		<li class="<?=($tab=='6'?'active':'')?>">
			<a href="#tab_6"><i class="fa fa-check-square"></i>  Skrining Covid</a>
		</li>
		<?}?>
		<li class="<?=($tab=='7'?'active':'')?>">
			<a href="#tab_7" onclick="load_index_history()"><i class="fa fa-history"></i>  History Activity</a>
		</li>
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="st_general" name="st_general" value="{st_general}">
		<input type="hidden" id="st_skrining" name="st_skrining" value="{st_skrining}">
		<input type="hidden" id="st_covid" name="st_covid" value="{st_covid}">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<input type="hidden" id="id" name="id" value="{id}">
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_1">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">					
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 col-xs-12 push-5 control-label" for="tglpendaftaran">Tanggal Pendaftaran<span style="color:red;">*</span></label>
						<div class="col-md-8">
							<div class="row">
								<div class="col-md-6 col-xs-6 push-5">
									<input tabindex="2" type="text" readonly class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
								</div>
								<div class="col-md-6 col-xs-6 push-5">
									<input tabindex="3" type="text" readonly class="time-datepicker form-control" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="statuspasienbaru">Status Pasien <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<select tabindex="4" disabled id="statuspasienbaru" name="statuspasienbaru" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</option>
									<option value="1" <?=($statuspasienbaru == 1 ? 'selected="selected"' : '')?>>PASIEN BARU</option>
									<option value="0" <?=($statuspasienbaru == 0 ? 'selected="selected"' : '')?>>PASIEN LAMA</option>
								</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="tglpendaftaran">No Medrec<span style="color:red;">*</span></label>
						<div class="col-md-8">
							<div class="input-group">
								<input readonly type="text" class="form-control" id="no_medrec" placeholder="NO. MEDREC" name="no_medrec" value="{no_medrec}" required>
								<span class="input-group-btn">
									<button disabled  class="btn btn-success" data-toggle="modal" data-target="#cari-pasien-modal"type="button" id="btn_cari_pasien"><i class="fa fa-search"></i> Cari Pasien</button>
									
								</span>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px; padding-bottom:0px;">
						<label class="col-md-4 col-xs-12 push-5 control-label" for="nama">Nama<span style="color:red;">*</span></label>
						<div class="col-md-3 col-xs-4 push-5" style="padding-right: 10px;">
							<select tabindex="5"  disabled id="titlepasien" name="title" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<?php foreach (get_all('mpasien_title') as $r):?>
									<option value="<?= $r->singkatan; ?>" <?=($title === $r->singkatan ? 'selected="selected"' : '')?>><?= $r->singkatan; ?></option>
								<?php endforeach; ?>
								</select>
						</div>
						<div class="col-md-5 col-xs-8 push-5" style="padding-left: 0px;">
							<input tabindex="6" type="text"  disabled class="form-control" id="namapasien" placeholder="Nama Lengkap" name="namapasien" value="{namapasien}" required>
						</div>
					</div>

					
					<input type="hidden" id="status_cari" name="status_cari" value="0">
					<input type="hidden" id="ckabupaten" name="ckabupaten" value="0">
					<input type="hidden" id="ckecamatan" name="ckecamatan" value="0">
					<input type="hidden" id="ckelurahan" name="ckelurahan" value="0">

					<input type="hidden" id="idpasien" name="idpasien" value="{idpasien}">


					<input type="hidden" name="created" value="<?= date(' d-M-Y H:i:s ')?>">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="jenis_kelamin">Jenis Kelamin <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<select tabindex="7"  disabled id="jenis_kelamin" name="jenis_kelamin" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="">Pilih Opsi</option>
								<?foreach(list_variable_ref(1) as $row){?>
								<option value="<?=$row->id?>" <?=($jenis_kelamin == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="statuskawin">Status Kawin</label>
						<div class="col-md-8">
							<select tabindex="8"  disabled id="statuskawin" name="statuskawin" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="">Pilih Opsi</option>
								<?foreach(list_variable_ref(7) as $row){?>
								<option value="<?=$row->id?>" <?=($statuskawin == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
								<?}?>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="tempatlahir">Tempat Lahir</label>
						<div class="col-md-8">
							<input tabindex="9"  disabled type="text" class="form-control" id="tempatLahir" placeholder="Tempat Lahir" name="tempatlahir" value="{tempat_lahir}" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="tgllahir">Tanggal Lahir<span style="color:red;">*</span></label>
						<div class="col-md-3  col-xs-12 push-5">
							<select tabindex="10"  disabled name="tgl_lahir" id="hari" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="0">Hari</option>
								<?php for ($i = 1; $i < 32; $i++) {?>
									<?php $hari = str_pad($i, 2, '0', STR_PAD_LEFT); ?>
									<option value="<?= $hari; ?>" <?=($tgl_hari == $hari ? 'selected="selected"' : '')?>><?= $hari; ?></option>
								<?php }?>
							</select>
						</div>
						<div class="col-md-2  col-xs-12 push-5">
							<select tabindex="11"  disabled name="bulan_lahir" id="bulan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="00">Bulan</option>
								<option value="01" <?=($tgl_bulan == '01' ? 'selected="selected"' : '')?>>Januari</option>
								<option value="02" <?=($tgl_bulan == '02' ? 'selected="selected"' : '')?>>Februari</option>
								<option value="03" <?=($tgl_bulan == '03' ? 'selected="selected"' : '')?>>Maret</option>
								<option value="04" <?=($tgl_bulan == '04' ? 'selected="selected"' : '')?>>April</option>
								<option value="05" <?=($tgl_bulan == '05' ? 'selected="selected"' : '')?>>Mei</option>
								<option value="06" <?=($tgl_bulan == '06' ? 'selected="selected"' : '')?>>Juni</option>
								<option value="07" <?=($tgl_bulan == '07' ? 'selected="selected"' : '')?>>Juli</option>
								<option value="08" <?=($tgl_bulan == '08' ? 'selected="selected"' : '')?>>Agustus</option>
								<option value="09" <?=($tgl_bulan == '09' ? 'selected="selected"' : '')?>>September</option>
								<option value="10" <?=($tgl_bulan == '10' ? 'selected="selected"' : '')?>>Oktober</option>
								<option value="11" <?=($tgl_bulan == '11' ? 'selected="selected"' : '')?>>November</option>
								<option value="12" <?=($tgl_bulan == '12' ? 'selected="selected"' : '')?>>Desember</option>
							</select>
						</div>
						<div class="col-md-3  col-xs-12 push-5" >
							<select tabindex="12"  disabled name="tahun_lahir" id="tahun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="0">Tahun</option>
								<?php for ($i = date('Y'); $i >= 1900; $i--) {?>
									<option value="<?= $i; ?>" <?=($tgl_tahun == $i ? 'selected="selected"' : '')?>><?= $i; ?></option>
								<?php }?>
							</select>
						</div>
					</div>
					<div class="form-group " >
						<label class="col-md-4 col-xs-12 control-label" for="umur">Umur</label>
						<div class="col-md-3 col-xs-12 push-5">
							<input type="text" class="form-control" id="umurtahun" name="umurtahun" value="{umurtahun}" placeholder="Tahun" readonly="true" required>
						</div>
						<div class="col-md-2 col-xs-12 push-5">
							<input type="text" class="form-control" id="umurbulan" name="umurbulan" value="{umurbulan}" placeholder="Bulan" readonly="true" required>
						</div>
						<div class="col-md-3 col-xs-12 push-5">
							<input type="text" class="form-control" id="umurhari" name="umurhari" value="{umurhari}" placeholder="Hari" readonly="true" required>
						</div>
					</div>
					<div class="form-group" >
						<label class="col-md-4 control-label" for="alamat">Alamat<span style="color:red;">*</span></label>
						<div class="col-md-8">
							<textarea tabindex="13"  disabled class="form-control" id="alamatpasien" placeholder="Alamat" name="alamatpasien" required><?= $alamatpasien?></textarea>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 0px;">
						<div class="col-md-4"></div>
						<label class="col-md-3 control-label" for="provinsi"><p align="left">Provinsi</p></label>
						<div class="col-md-5">
							<?php if ($id == null) {?>
							<select tabindex="14" name="provinsi" disabled  id="provinsi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</option>
									<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
									<option value="<?= $a->id; ?>" <?= ($a->id == '12') ? 'selected' : ''; ?>><?= $a->nama; ?></option>
								<?php endforeach; ?>
								</select>
							<?php } else {?>
							<select tabindex="14" name="provinsi"  disabled id="provinsi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</option>
									<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
									<option value="<?= $a->id; ?>" <?=($provinsi_id == $a->id ? 'selected="selected"' : '')?>><?= $a->nama; ?></option>
								<?php endforeach; ?>
								</select>
							<?php }?>
						</div>
					</div>
					<div id="kabupaten" style="display: block;">
						<div class="form-group" style="margin-bottom: 10px;">
							<div class="col-md-4"></div>
							<label class="col-md-3" for="kabupaten"><left>Kabupaten/Kota</left></label>
							<div class="col-md-5">
								<?php if ($id == null) {?>
								<select tabindex="15" name="kabupaten"  disabled id="kabupaten1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</<option>
										<?php foreach (get_all('mfwilayah', ['parent_id' => '12']) as $r):?>
										<option value="<?= $r->id; ?>" <?= ($r->id == '213') ? 'selected' : ''; ?>><?= $r->nama; ?></<option>
										<?php endforeach; ?>
									</select>
								<?php } else {?>
								<select tabindex="15" name="kabupaten"  disabled id="kabupaten1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="">Pilih Opsi</<option>
										<?php foreach (get_all('mfwilayah', ['parent_id' => $provinsi_id]) as $r):?>
										<option value="<?= $r->id; ?>" <?=($kabupaten_id == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
										<?php endforeach; ?>
									</select>
								<?php }?>
							</div>
						</div>
					</div>
					<div id="kecamatan" style="display: block;">
						<div class="form-group" style="margin-bottom: 10px;">
							<div class="col-md-4"></div>
							<label class="col-md-3 control-label"  disabled for="kecamatan" style="text-align:left;">Kecamatan</label>
							<div class="col-md-5">
								<?php if ($id == null) {?>
								<select tabindex="16" name="kecamatan"  disabled id="kecamatan1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</<option>
									<?php foreach (get_all('mfwilayah', ['parent_id' => '213']) as $r):?>
									<option value="<?= $r->id; ?>"><?= $r->nama; ?></<option>
									<?php endforeach; ?>
								</select>
								<?php } else {?>
								<select tabindex="16" name="kecamatan"  disabled id="kecamatan1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</<option>
									<?php foreach (get_all('mfwilayah', ['parent_id' => $kabupaten_id]) as $r):?>
									<option value="<?= $r->id; ?>"<?=($kecamatan_id == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
									<?php endforeach; ?>
								</select>
								<?php }?>
							</div>
						</div>
					</div>
					
					<div id="kelurahan" style="display: block;">
						<div class="form-group" style="margin-bottom: 10px;">
							<div class="col-md-4"></div>
							<label class="col-md-3 control-label" for="kelurahan" style="text-align:left;">Kelurahan</label>
							<div class="col-md-5">
								<select tabindex="17" name="kelurahan"  disabled id="kelurahan1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</<option>
									<?php foreach (get_all('mfwilayah', ['parent_id' => $kecamatan_id]) as $r):?>
									<option value="<?= $r->id; ?>"<?=($kelurahan_id == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<div class="col-md-4"></div>
							<label class="col-md-3 control-label" for="kodepos" style="text-align:left;">Kode Pos</label>
							<div class="col-md-5">
								<input tabindex="18" type="text" class="form-control"  disabled id="kodepos" placeholder="Kode Pos" name="kodepos" value="{kodepos}" readonly="true" required>
							</div>
						</div>
					</div>
					
					
					
				</div>

				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 col-xs-12 push-5 control-label" for="noidentitas">No Identitas</label>
						<div class="col-md-3 col-xs-4 push-5" style="padding-right: 10px;">
							<select tabindex="18" id="jenis_id" name="jenisidentitas"  disabled class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="1" <?=($jenis_id == 1 ? 'selected="selected"' : '')?>>KTP</option>
								<option value="2" <?=($jenis_id == 2 ? 'selected="selected"' : '')?>>SIM</option>
								<option value="3" <?=($jenis_id == 3 ? 'selected="selected"' : '')?>>Pasport</option>
								<option value="4" <?=($jenis_id == 4 ? 'selected="selected"' : '')?>>Kartu Pelajar/Mahasiswa</option>
							</select>
						</div>
						<div class="col-md-5 col-xs-8 push-5" style="margin-left:0px;padding-left: 0px;">
							<input tabindex="19" type="text" class="form-control"  disabled id="noidentitas" placeholder="No Identitas" name="noidentitas" value="{ktp}" required>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="nohp">No Hp</label>
						<div class="col-md-8">
							<input tabindex="20" type="text" class="form-control"  disabled id="nohp" placeholder="No HP" name="nohp" value="{nohp}" required>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="telprumah">Telepon Rumah</label>
						<div class="col-md-8">
							<input tabindex="21" type="text" class="form-control"  disabled id="telprumah" placeholder="Telepon Rumah" name="telprumah" value="{telepon}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="email">Email</label>
						<div class="col-md-8">
							<input tabindex="22" type="text" class="form-control"  disabled id="email" placeholder="Email" name="email" value="{email}">
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="golongan_darah">Golongan Darah</label>
						<div class="col-md-8">
							<select tabindex="23" id="golongan_darah" name="golongan_darah"  disabled class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="">Pilih Opsi</option>
								<?foreach(list_variable_ref(11) as $row){?>
								<option value="<?=$row->id?>" <?=($golongan_darah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
								<?}?>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="agama">Agama</label>
						<div class="col-md-8">
							<select tabindex="24" id="agama_id" name="agama_id" disabled  class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="">Pilih Opsi</option>
								<?foreach(list_variable_ref(3) as $row){?>
								<option value="<?=$row->id?>" <?=($agama_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
								<?}?>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="warganegara">Kewarganegaraan</label>
						<div class="col-md-8">
							<select tabindex="25" id="warganegara" name="warganegara"  disabled class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="">Pilih Opsi</option>
								<?foreach(list_variable_ref(18) as $row){?>
								<option value="<?=$row->id?>" <?=($warganegara == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
								<?}?>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="suku">Suku</label>
						<div class="col-md-8">
							<select tabindex="26" id="suku_id" name="suku_id" disabled  class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="">Pilih Opsi</option>
								<?foreach(list_variable_ref(4) as $row){?>
								<option value="<?=$row->id?>" <?=($suku_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
								<?}?>
								
							</select>
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="pendidikan">Pendidikan</label>
						<div class="col-md-8">
							<select tabindex="27" id="pendidikan" name="pendidikan"  disabled class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="">Pilih Opsi</option>
								<?foreach(list_variable_ref(13) as $row){?>
								<option value="<?=$row->id?>" <?=($pendidikan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="pekerjaan">Pekerjaan</label>
						<div class="col-md-8">
							<select tabindex="28" id="pekerjaan" name="pekerjaan"  disabled class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="">Pilih Opsi</option>
								<?foreach(list_variable_ref(6) as $row){?>
								<option value="<?=$row->id?>" <?=($pekerjaan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					<hr>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="hubungan"></label>
						<label class="col-md-9 control-label text-primary"><h5 align="left"><b>Data Penanggung Jawab</b></h5></label>
						<div class="col-md-12">
							<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="namapenanggungjawab">Nama</label>
						<div class="col-md-8">
							<input tabindex="29" type="text" class="form-control"  disabled id="namapenanggungjawab" placeholder="Nama Penanggung Jawab" name="namapenanggungjawab" value="{namapenanggungjawab}" required>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="hubungan">Hubungan</label>
						<div class="col-md-8">
							<select tabindex="30" id="hubungan" name="hubungan" disabled  class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="">Pilih Opsi</option>
								<?foreach(list_variable_ref(9) as $row){?>
								<option value="<?=$row->id?>" <?=($hubungan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="alamatpenanggungjawab">Alamat</label>
						<div class="col-md-8">
							<textarea tabindex="31" class="form-control"  disabled id="alamatpenanggungjawab" placeholder="Alamat Penanggung Jawab" name="alamatpenanggungjawab"><?= $alamatpenanggungjawab?></textarea>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="telepon">Telepon</label>
						<div class="col-md-8">
							<input tabindex="32" type="text" class="form-control"  disabled id="telepon" placeholder="Telepon" name="telepon" value="{teleponpenanggungjawab}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 20px;">
						<label class="col-md-4 control-label" for="noidentitaspenanggung">No Identitas</label>
						<div class="col-md-8">
							<input tabindex="33" type="text"  disabled class="form-control" id="noidentitaspenanggung" placeholder="No Identitas Penanggung Jawab" name="noidentitaspenanggung" value="{noidentitaspenanggungjawab}" required>
							
						</div>
					</div>
					
				</div>
				
				<?php echo form_hidden('id', $id); ?>
			</div>
			<?php echo form_close() ?>
		</div>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_2">
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				<div class="col-md-10">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="reservasi_tipe_id">Pendaftaran <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<select tabindex="5" id="reservasi_tipe_id"  disabled name="reservasi_tipe_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</option>
									<?if ($reservasi_tipe_id=='1'){?>
									<option value="1" <?=($reservasi_tipe_id == 1 ? 'selected="selected"' : '')?>>POLIKLINIK</option>
									<?}else{?>
									<option value="2" <?=($reservasi_tipe_id == 2 ? 'selected="selected"' : '')?>>REHABILITAS MEDIS</option>
									<?}?>
								</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="reservasi_cara">Jenis Booking <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<select tabindex="5" id="reservasi_cara"  disabled name="reservasi_cara" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value=""  <?=($reservasi_cara == "" ? 'selected="selected"' : '')?>>Pilih Opsi</option>
									
									<option value="1" <?=($reservasi_cara == 1 ? 'selected="selected"' : '')?>>ONLINE</option>
									<option value="2" <?=($reservasi_cara == 2 ? 'selected="selected"' : '')?>>APPS</option>
									<option value="3" <?=($reservasi_cara == 3 ? 'selected="selected"' : '')?>>WHATSAPP</option>
									<option value="4" <?=($reservasi_cara == 4 ? 'selected="selected"' : '')?>>PHONE</option>
									<option value="5" <?=($reservasi_cara == 5 ? 'selected="selected"' : '')?>>DATANG LANGSUNG</option>
									<option value="6" <?=($reservasi_cara == 6 ? 'selected="selected"' : '')?>>APM</option>
									
								</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="idpoli">Poliklinik Tujuan <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<select id="idpoli" name="idpoli" disabled  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
								<option value="" <?=($idpoli==''?'selected':'')?>>- Pilih Poliklinik -</option>
								<?foreach($list_poli as $r){?>
								<option value="<?=$r->id?>" <?=($idpoli==$r->id?'selected':'')?>><?=$r->nama?></option>
								<?}?>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" disabled  for="iddokter">Dokter <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<select id="iddokter" name="iddokter"  disabled class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
								<option value="" <?=($iddokter==''?'selected':'')?>>- Pilih Dokter -</option>
								<?foreach($list_dokter as $r){?>
								<option value="<?=$r->id?>" <?=($iddokter==$r->id?'selected':'')?>><?=$r->nama?></option>
								<?}?>
								
							</select>
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="tanggal">Tanggal Janji Temu <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<label><?=text_primary($janji_temu)?></label>
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="pertemuan_id">Jenis Pertemuan <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<select id="pertemuan_id"  disabled name="pertemuan_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
								<option value="" <?=($pertemuan_id==''?'selected':'')?>>- Pilih Pertemuan -</option>
								<?foreach(list_variable_ref(15) as $r){?>
								<option value="<?=$r->id?>" <?=($pertemuan_id==$r->id?'selected':'')?>><?=$r->nama?></option>
								<?}?>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="idasalpasien">Asal Pasien <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<select tabindex="32"  disabled name="idasalpasien" id="idasalpasien" class="js-select2  form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="" <?=($idasalpasien==''?'selected':'')?>>Pilih Opsi</option>
								<?php foreach (get_all('mpasien_asal') as $r):?>
									<option value="<?= $r->id; ?>" <?=($idasalpasien == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="idtipepasien">Tipe Pasien <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<select tabindex="32"  disabled name="idtipepasien" class="js-select2  form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="1" <?=($idtipepasien == 1 ? 'selected="selected"' : '')?>>Pasien RS</option>
								<option value="2" <?=($idtipepasien == 2 ? 'selected="selected"' : '')?>>Pasien Pribadi</option>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="kelompokpasien">Cara Pembayaran <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<select tabindex="34"  disabled name="kelompokpasien" id="kelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="" <?=($kelompokpasien==''?'selected':'')?>>Pilih Opsi</option>
								<?php foreach ($list_cara_bayar as $r):?>
								<option value="<?= $r->id; ?>" <?=($r->id == $kelompokpasien ? 'selected' : '')?>><?= $r->nama; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group div_asuransi" style="margin-bottom: 10px;display:none">
						<label class="col-md-4 control-label" for="idrekanan">Nama Perusahaan / Asuransi <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<select tabindex="35"  disabled name="idrekanan" id="idrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="" <?=($idrekanan==''?'selected':'')?>>Pilih Opsi</option>
								<?php foreach ($list_asuransi as $r) {?>
								<option value="<?= $r->id?>" <?=($idrekanan == $r->id ? 'selected="selected"' : '')?>><?= $r->nama?></option>
								<?php }?>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px" hidden>
						<label class="col-md-4 control-label" for="idrekanan">Skrinning<span style="color:red;">*</span></label>
						<div class="col-md-8">
							<input class="form-control" name="st_gc" id="st_gc" value="{st_gc}" type="text">
							<input class="form-control" name="st_sp" id="st_sp" value="{st_sp}" type="text">
							<input class="form-control" name="st_sc" id="st_sc" value="{st_sc}" type="text">
						</div>
					</div>
					
					
				</div>
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?>" id="tab_3">
			
			<?php echo form_open('tbooking/save_register_rincian', 'class="js-form1 validation form-horizontal" onsubmit="return validate_sp_final()"') ?>
			<div class="row">
				<div class="form-group push-5 push-5-r push-5-l">
					<div class="col-md-6 push-5">
						<label for="validation-classic-city">Jenis Booking</label>
						<input class="form-control lbl_jenis_booking" disabled type="text">
						
					</div>
					<div class="col-md-6 push-5">
						<label for="validation-classic-city">Status Pasien</label>
						<input class="form-control lbl_status_pasien" disabled type="text">
					</div>
				</div>
				<div class="form-group push-5 push-5-r push-5-l">
					<div class="col-md-6 push-5">
						<label for="validation-classic-city">Nama Pasien</label>
						<input class="form-control lbl_nama" disabled type="text">
					</div>
					<div class="col-md-6 push-5">
						<label for="validation-classic-city">No Rekam Medis</label>
						<input class="form-control lbl_norekammedis" disabled type="text">
					</div>
				</div>
				<div class="form-group push-5 push-5-r push-5-l">
					<div class="col-md-6 push-5">
						<label for="validation-classic-city">Klinik Tujuan</label>
						<input class="form-control lbl_tujuan" disabled type="text">
					</div>
					<div class="col-md-6 push-5">
						<label for="validation-classic-city">Dokter</label>
						<input class="form-control lbl_dokter" disabled type="text">
					</div>
				</div>
				<div class="form-group push-5 push-5-r push-5-l">
					<div class="col-md-6 push-5">
						<label for="validation-classic-city">Tanggal dan Waktu Janji Temu</label>
						<input class="form-control lbl_tanggal_janji" disabled type="text" value="{janji_temu}">
					</div>
					<div class="col-md-6 push-5">
						<label for="validation-classic-city">Jenis Pertemuan</label>
						<select id="pertemuan_id2"  name="pertemuan_id2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
							<option value="" <?=($pertemuan_id==''?'selected':'')?>>- Pilih Pertemuan -</option>
							<?foreach(list_variable_ref(15) as $r){?>
							<option value="<?=$r->id?>" <?=($pertemuan_id==$r->id?'selected':'')?>><?=$r->nama?></option>
							<?}?>
							
						</select>
					</div>
				</div>
				<div class="form-group push-5 push-5-r push-5-l">
					<div class="col-md-6 push-5">
						<label for="validation-classic-city">Cara Pembayaran</label>
						<select tabindex="34"  name="kelompokpasien2" id="kelompokpasien2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
							<option value="" <?=($kelompokpasien==''?'selected':'')?>>Pilih Opsi</option>
							<?php foreach (get_all('mpasien_kelompok') as $r):?>
							<option value="<?= $r->id; ?>" <?=($r->id == $kelompokpasien ? 'selected' : '')?>><?= $r->nama; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="col-md-6 push-5 div_asuransi">
						<label for="validation-classic-city">Nama Perusahaan Asuransi</label>
							<select tabindex="35"  name="idrekanan2" id="idrekanan2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
							<option value="" <?=($idrekanan==''?'selected':'')?>>Pilih Opsi</option>
							<?php foreach ($list_asuransi as $r) {?>
							<option value="<?= $r->id?>" <?=($idrekanan == $r->id ? 'selected="selected"' : '')?>><?= $r->nama?></option>
							<?php }?>
						</select>
					</div>
				</div>
				<?
					if ($foto_kartu==''){
						$foto_kartu='default.png';
					}
					?>
					
				<div class="form-group push-5 push-5-r push-5-l ">
					<div class="col-md-6 push-5">
						<div class="col-md-12 push-5">
							<div class="checkbox">
								<label for="example-checkbox1">
									<input type="checkbox" disabled id="example-checkbox1" name="notif_email"  <?=($notif_email=='1'?'checked':'')?> value="1"> Kirim Pemberitahuan melalui email
								</label>
								
							</div>
							<div class="checkbox">
								<label for="example-checkbox2">
									<input type="checkbox" disabled id="example-checkbox2" <?=($notif_wa=='1'?'checked':'')?> name="notif_wa" value="1"> Kirim Pemberitahuan melalui whatsapp
								</label>
								
							</div>
						</div>
						
					</div>
					<div class="col-md-3 push-5">
						<a class="block block-link-hover2" href="javascript:void(0)">
							<div class="block-content block-content-full text-center bg-image">
								<img class="img-responsive center" style="width:100%" src="<?=$base_url?>qrcode/get_barcode/<?= $kode_booking ?>" alt="<?= $kode_booking ?>" title="<?= $kode_booking ?>">
							</div>
							<div class="block-content block-content-full text-center">
								<div class="input-group">
									<input class="form-control text-center" readonly type="input" id="kode_booking" name="kode_booking" value="{kode_booking}" placeholder="Kode Booking">
									<span class="input-group-btn">
										<button class="btn btn-primary" onclick="copy_text()" id="btn_copy" type="button"><i class="fa fa-copy"></i> Copy</button>
									</span>
								</div>
							</div>
						</a>
						
						
					</div>
					<div class="col-md-3 push-5 div_foto">
						<a class="block block-link-hover2" href="javascript:void(0)">
							<div class="block-content block-content-full text-center bg-image">
								<img class="img-responsive" for="foto_kartu" target="_blank"  src="{upload_path}foto_kartu/<?=$foto_kartu?>" id="output_img" style="width:100%" />
							</div>
							<div class="block-content block-content-full text-center">
								<div class="font-w600 push-5">Foto Kartu Asuransi</div>
							</div>
						</a>
						
					</div>
					
					
				</div>
				
				
				<div class="form-group push-5 push-5-r push-5-l">
					<input class="form-control" readonly name="id" id="id" value="{id}" type="hidden">
				</div>
			</div>
			<div class="row">
				<div class="modal-footer">
					<button class="btn btn-sm btn-primary" type="submit" value="1"><i class="fa fa-save"></i> Update</button>
					
				</div>
			</div>
			<?php echo form_close() ?>
		
		</div>
		<?if($st_gc=='1'){?>
		<div class="tab-pane fade fade-left <?=($tab=='4'?'active in':'')?>" id="tab_4">
			
			<?php echo form_open('tbooking/save_register_gc', 'class="js-form1 validation form-horizontal" onsubmit="return validate_sp_final()"') ?>
			<div class="row">
				<div class="col-md-12">
					<table>
						<tr>
							<td width="10%"><img class="img-avatar"  id="output_img" src="{upload_path}app_setting/<?=($logo_gc?$logo_gc:'no_image.png')?>" /></td>
							<td  width="90%" class="text-center text-bold"><h3>{judul_gc}</h3></td>
						</tr>
					</table>
					<table class="table table-bordered table-striped"  id="tabel_gc">
						<thead>
							<tr>
								<td width="2%" class="text-center text-bold text-italic text-header"></td>
								<td width="70%" class="text-center text-bold text-italic text-header">{sub_header_gc}</td>
								<td  width="28%" class="text-center text-bold text-italic text-header">{sub_header_side_gc}</td>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
					
					<div class="col-md-6 push-5 push-0-l" <?=($st_general=='1'?'hidden':'')?>>
						<label class="col-md-12" for="validation-classic-city">MENGIJINKAN / TIDAK MENGIJINKAN	</label>
						<div class="col-md-12 push-5">
							<select name="jawaban_perssetujuan" <?=($st_general=='1'?'disabled':'')?> id="jawaban_perssetujuan" class="js-select2 form-control" style="width: 100%;" data-placeholder="MENGIJINKAN / TIDAK MENGIJINKAN">
								<option value="" <?=($jawaban_perssetujuan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
								<option value="MENGIJINKAN" <?=($jawaban_perssetujuan == 'MENGIJINKAN' ? 'selected="selected"' : '')?>>MENGIJINKAN</option>
								<option value="TIDAK MENGIJINKAN" <?=($jawaban_perssetujuan == 'TIDAK MENGIJINKAN' ? 'selected="selected"' : '')?>>TIDAK MENGIJINKAN</option>
							</select>
						</div>
					</div>
					<table>
						<tr>
							<td  width="100%" class="text-left">{footer_1_gc}</td>
						</tr>
						
						<tr>
							<td  width="100%" class="text-left"><?=trim($footer_2_gc)?></td>
						</tr>
					</table>
					<table>
						<tr>
							<td  width="70%" class="text-left">NAMA PASIEN</td>
							<td  width="30%" class="text-left">JENIS KELAMIN</td>
						</tr>
						<tr>
							<td  width="70%" class="text-left">
								<input type="text" disabled class="form-control" id="namapasien" placeholder="Nama Lengkap" name="namapasien" value="{namapasien}" required>
							</td>
							<td  width="30%" class="text-left">
									<select tabindex="7" disabled  class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(1) as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_kelamin == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
							</td>
						</tr>
						<tr>
							<td  width="70%" class="text-left">{ttd_1}</td>
							<td  width="30%" class="text-left">HUBUNGAN</td>
						</tr>
						<tr>
							<td  width="70%" class="text-left">
								<input disabled type="text" class="form-control" placeholder="Nama Penanggung Jawab" name="namapenanggungjawab" value="{namapenanggungjawab}" required>
							</td>
							<td  width="30%" class="text-left">
									<select disabled  class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(9) as $row){?>
										<option value="<?=$row->id?>" <?=($hubungan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
							</td>
						</tr>
						
					</table>
					<div class="col-md-6 push-5 push-0-l">
						<label class="col-md-12" for="validation-classic-city">Tanda Tangan</label>
						<div class="col-md-6">
							<?if ($jawaban_ttd){?>
								<div class="img-container fx-img-rotate-r">
									<img class="img-responsive" src="<?=$jawaban_ttd?>" alt="">
									<div class="img-options">
										<div class="img-options-content">
											<div class="btn-group btn-group-sm">
												<a class="btn btn-default" onclick="modal_ttd()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
												<a class="btn btn-default btn-danger" onclick="hapus_ttd()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
											</div>
										</div>
									</div>
								</div>
							<?}else{?>
								<button class="btn btn-sm btn-success" onclick="modal_ttd()" id="btn_ttd_final" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
							<?}?>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<input class="form-control" readonly name="id" id="id" value="{id}" type="hidden">
					<input class="form-control" readonly name="jawaban_ttd" id="jawaban_ttd" value="{jawaban_ttd}" type="hidden">
				</div>
			</div>
			<div class="row">
				<div class="modal-footer">
					<?if ($st_general=='0'){?>
						<?if ($st_sp=='1' && $st_skrining=='0'){?>
						<button class="btn btn-sm btn-primary btn_gc" type="submit" value="1"><i class="fa fa-arrow-right"></i> Lanjut Skrining Pasien</button>
						<?}else{?>
							<?if ($st_sc=='1' && $st_covid=='0'){?>
							<button class="btn btn-sm btn-primary btn_gc" type="submit" value="1"><i class="fa fa-arrow-right"></i> Lanjut Skrining Covid</button>
							<?}else{?>
							<button class="btn btn-sm btn-success btn_gc" type="submit" value="2"><i class="fa fa-arrow-right"></i> Selesai </button>
							
							<?}?>
						<?}?>
					<?}else{?>
						<a href="{base_url}tbooking/poliklinik" class="btn btn-sm btn-default" name="btn_simpan" value="2" type="submit" ><i class="fa fa-reply"></i> Kembali</a>
					<?}?>
				</div>
			</div>
			<?php echo form_close() ?>
		
		</div>
		<?}?>
		<?if($st_sp=='1'){?>
		<div class="tab-pane fade fade-left <?=($tab=='5'?'active in':'')?>" id="tab_5">
			
			<?php echo form_open('tbooking/save_register_sp', 'class="js-form1 validation form-horizontal" onsubmit="return validate_sp_final()"') ?>
			<div class="row">
				<div class="col-md-12">
					
					<table class="table table-bordered table-striped"  id="tabel_sp">
						<thead>
							<tr>
								<td width="50%" class="text-center text-bold text-italic text-header">{judul_sp}</td>
								<td  width="50%" class="text-center text-bold text-italic text-header">Opsi Jawaban</td>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
					
				</div>
				<div class="col-md-12">
					
				</div>
			</div>
			<input class="form-control" readonly name="id" id="id" value="{id}" type="hidden">
			<div class="row">
				<div class="modal-footer">
					<?if ($st_skrining=='0'){?>
						<?if ($st_sc=='1' && $st_covid=='0'){?>
							<button class="btn btn-sm btn-primary btn_sp" name="btn_simpan" value="1" type="submit" ><i class="fa fa-arrow-right"></i> Lanjut Skrining Covid</button>
						<?}else{?>
							
							<button class="btn btn-sm btn-success btn_sp" name="btn_simpan" value="2" type="submit" ><i class="fa fa-arrow-right"></i> Selesai</button>
							
						<?}?>
					<?}else{?>
						<a href="{base_url}tbooking/poliklinik" class="btn btn-sm btn-default" name="btn_simpan" value="2" type="submit" ><i class="fa fa-reply"></i> Kembali</a>
					<?}?>
				</div>
			</div>
			<?php echo form_close() ?>
		
		</div>
		<?}?>
		<?if($st_sc=='1'){?>
		<div class="tab-pane fade fade-left <?=($tab=='6'?'active in':'')?>" id="tab_6">
			<?php echo form_open('tbooking/save_register_sc', 'class="js-form1 validation form-horizontal" onsubmit="return validate_sp_final()"') ?>
			<div class="row">
				<div class="col-md-12">
					<input class="form-control" readonly name="id" id="id" value="{id}" type="hidden">
					<table class="table table-bordered table-striped"  id="tabel_sc">
						<thead>
							<tr>
								<td width="50%" class="text-center text-bold text-italic text-header">{judul_sc}</td>
								<td  width="50%" class="text-center text-bold text-italic text-header">Opsi Jawaban</td>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
					
				</div>
				<div class="col-md-12">
					
				</div>
			</div>
			<div class="row">
				<div class="modal-footer">
					<?if ($st_covid=='0'){?>
					<button class="btn btn-sm btn-success btn_sc" name="btn_simpan" value="2" type="submit" ><i class="fa fa-arrow-right"></i> Selesai</button>
					<?}else{?>	
					<a href="{base_url}tbooking/poliklinik" class="btn btn-sm btn-default" name="btn_simpan" value="2" type="submit" ><i class="fa fa-reply"></i> Kembali</a>
					<?}?>	
				</div>
			</div>
			<?php echo form_close() ?>
		
		</div>
		<?}?>
		<div class="tab-pane fade fade-left <?=($tab=='7'?'active in':'')?>" id="tab_7">
			<?php echo form_open('#', 'class="js-form1 validation form-horizontal"') ?>
			<div class="row">
				<div class="col-md-12">
					<input class="form-control" readonly name="id" id="id" value="{id}" type="hidden">
					<table class="table table-bordered table-striped"  id="tabel_ha">
						<thead>
							<tr>
								<td width="5%" class="text-center text-bold text-italic text-header">No</td>
								<td  width="20%" class="text-center text-bold text-italic text-header">Activity</td>
								<td  width="40%" class="text-center text-bold text-italic text-header">Detail Activity</td>
								<td  width="35%" class="text-center text-bold text-italic text-header">User & Date</td>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
					
				</div>
				<div class="col-md-12">
					
				</div>
			</div>
			
			<?php echo form_close() ?>
		
		</div>
</div>
<div class="modal in" id="modal_paraf" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Paraf Pasien</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<input type="hidden" readonly id="ttd_id" name="ttd_id" value="">
								<input type="hidden" readonly id="nama_tabel" name="nama_tabel" value="">
								<textarea id="signature64" name="signed" style="display: none"></textarea>
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" id="btn_save_paraf"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Penanggung Jawab </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig_2" ></div>
									</div>
								</div>
								<textarea id="signature64_2" name="signed_2" style="display: none"><?=$jawaban_ttd?></textarea>
							</div>
							<input type="hidden" readonly id="ttd_id_2" name="ttd_id_2" value="{id}">
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear_2"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" id="btn_save_ttd"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{js_path}pages/base_forms_wizard.js"></script>
<script type="text/javascript">
var table;
var st_skrining=$("#st_skrining").val();
var st_general=$("#st_general").val();
var st_covid=$("#st_covid").val();
$(document).ready(function(){	
	generate_rincian();
	load_index_gc();
	load_index_sp();
	load_index_sc();
})	
function copy_text(){
	$("#kode_booking").removeAttr('readonly');
	 var copyText = document.getElementById("kode_booking");
		
	  // Select the text field
	  copyText.select();
	  copyText.setSelectionRange(0, 99999); // For mobile devices

	  // Copy the text inside the text field
	  navigator.clipboard.writeText(copyText.value);
	  $('#kode_booking').attr('readonly', true);
}
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
$('#clear').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
var sig_2 = $('#sig_2').signature({syncField: '#signature64_2', syncFormat: 'PNG'});
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig_2.signature('clear');
	$("#signature64_2").val('');
});
function validate_sp_final(){
	$("#cover-spin").show();
	return true;
}
function modal_faraf($app_id,$nama_tabel){
	$("#ttd_id").val($app_id);
	$("#nama_tabel").val($nama_tabel);
	$("#modal_paraf").modal('show');
}
function modal_ttd(){
	$("#modal_ttd").modal('show');
}
function hapus_paraf($app_id,$nama_tabel){
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}tbooking/hapus_paraf/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				app_id:$app_id,
				nama_tabel:$nama_tabel,
		  },success: function(data) {
				$("#modal_paraf").modal('hide');
				if ($nama_tabel=='app_reservasi_tanggal_pasien_gc'){
					load_index_gc();
				}
				if ($nama_tabel=='app_reservasi_tanggal_pasien_sp'){
					load_index_sp();
				}
				if ($nama_tabel=='app_reservasi_tanggal_pasien_sc'){
					load_index_sc();
				}
				$("#cover-spin").hide();
			}
		});
}

$(document).on("click","#btn_save_paraf",function(){
	var app_id=$("#ttd_id").val();
	var signature64=$("#signature64").val();
	var nama_tabel=$("#nama_tabel").val();
	$nama_tabel=$("#nama_tabel").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}tbooking/save_ttd/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				app_id:app_id,signature64:signature64,nama_tabel:nama_tabel,
		  },success: function(data) {
				$("#modal_paraf").modal('hide');
				if ($nama_tabel=='app_reservasi_tanggal_pasien_gc'){
					load_index_gc();
				}
				if ($nama_tabel=='app_reservasi_tanggal_pasien_sp'){
					load_index_sp();
				}
				if ($nama_tabel=='app_reservasi_tanggal_pasien_sc'){
					load_index_sc();
				}
				$("#cover-spin").hide();
			}
		});
	
});
$(document).on("click","#btn_save_ttd",function(){
	var app_id=$("#ttd_id_2").val();
	var signature64=$("#signature64_2").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}tbooking/save_ttd_2/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				app_id:app_id,signature64:signature64
		  },success: function(data) {
				location.reload();
			}
		});
	
});
function hapus_ttd(){
	var app_id=$("#ttd_id_2").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Batal Tandatangan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		$.ajax({
			  url: '{site_url}tbooking/hapus_ttd/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					app_id:app_id,
					// nama_tabel:nama_tabel,
			  },success: function(data) {
					location.reload();
				}
		});

	});
	
}
function validate_gc(){
	let st_next=true;
	let jawaban_perssetujuan=$("#jawaban_perssetujuan").val();
	if (jawaban_perssetujuan==''){
		st_next=false;
	}
	if ($("#jawaban_ttd").val()==''){
		st_next=false;
		
	}
	if (st_next==false){
		$('.btn_gc').attr('disabled', true);
	}else{
		$('.btn_gc').attr('disabled', false);
		
	}
	if (st_general=='1'){
		$('.nilai_gc').attr('disabled', true);
		$('.nilai_gc_free').attr('disabled', true);
		$('.btn_ttd').attr('disabled', true);
	}else{
		
		$('.nilai_gc').attr('disabled', false);
		$('.nilai_gc_free').attr('disabled', false);
		$('.btn_ttd').attr('disabled', false);
	}
}
function validate_sp(){
	let st_next=true;
	$('#tabel_sp tbody tr').each(function() {
		let opsi=$(this).find(".nilai_sp").val();
		if (opsi==''){
			st_next=false;
		}
			
	});
	if (st_next==false){
		$('.btn_sp').attr('disabled', true);
	}else{
		$('.btn_sp').attr('disabled', false);
		
	}
	if (st_skrining=='1'){
		$('.nilai_sp').attr('disabled', true);
		$('.nilai_sp_free').attr('disabled', true);
		$('.btn_ttd_sp').attr('disabled', true);
	}else{
		
		$('.nilai_sp').attr('disabled', false);
		$('.nilai_sp_free').attr('disabled', false);
		$('.btn_ttd_sp').attr('disabled', false);
	}
}
function validate_sc(){
	let st_next=true;
	$('#tabel_sc tbody tr').each(function() {
		let opsi=$(this).find(".nilai_sc").val();
		if (opsi==''){
			st_next=false;
		}
			
	});
	if (st_next==false){
		$('.btn_sc').attr('disabled', true);
		$('.btn_sc').attr('disabled', true);
	}else{
		$('.btn_sc').attr('disabled', false);
		
	}
	if (st_covid=='1'){
		$('.nilai_sc').attr('disabled', true);
		$('.nilai_sc_free').attr('disabled', true);
		$('.btn_ttd_sc').attr('disabled', true);
	}else{
		
		$('.nilai_sc').attr('disabled', false);
		$('.nilai_sc_free').attr('disabled', false);
		$('.btn_ttd_sc').attr('disabled', false);
	}
}
function load_index_gc(){
	$("#cover-spin").show();
	var id=$("#id").val();
	// alert(id);
	$.ajax({
		url: '{site_url}tbooking/load_index_gc/'+id,
		dataType: "json",
		success: function(data) {
			$("#tabel_gc tbody").empty();
			$("#tabel_gc tbody").append(data.tabel);
			$(".js-select2").select2();
			validate_gc();
			$("#cover-spin").hide();
		}
	});
}

$(document).on("blur",".nilai_gc_free",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var approval_id=tr.find(".approval_id").val();
	var nilai_id=$(this).val();
	$.ajax({
	  url: '{site_url}tbooking/update_nilai_gc_free/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			approval_id:approval_id,nilai_id:nilai_id,
	  },success: function(data) {
			validate_gc();
		}
	});
});
$(document).on("blur",".nilai_sp_free",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var approval_id=tr.find(".sp_id").val();
	var nilai_id=$(this).val();
	$.ajax({
	  url: '{site_url}tbooking/update_nilai_sp_free/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			approval_id:approval_id,nilai_id:nilai_id,
	  },success: function(data) {
			validate_sp();
		}
	});
});
$(document).on("blur",".nilai_sc_free",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var approval_id=tr.find(".sc_id").val();
	var nilai_id=$(this).val();
	$.ajax({
	  url: '{site_url}tbooking/update_nilai_sc_free/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			approval_id:approval_id,nilai_id:nilai_id,
	  },success: function(data) {
			validate_sp();
		}
	});
});
$(document).on("change",".nilai_gc",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var approval_id=tr.find(".approval_id").val();
	var nilai_id=$(this).val();
	$.ajax({
		  url: '{site_url}tbooking/update_nilai_gc/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				approval_id:approval_id,nilai_id:nilai_id,
		  },success: function(data) {
				validate_gc();
			}
		});
	
});
$(document).on("change","#jawaban_perssetujuan",function(){
	// alert($(this));
	var app_id=$("#id").val();
	var jawaban_perssetujuan=$("#jawaban_perssetujuan").val();
	$.ajax({
		  url: '{site_url}tbooking/update_persetujuan_gc/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				app_id:app_id,
				jawaban_perssetujuan:jawaban_perssetujuan,
		  },success: function(data) {
				validate_gc();
			}
		});
	
});
function load_index_sp(){
	$("#cover-spin").show();
	var id=$("#id").val();
	// alert(id);
	$.ajax({
		url: '{site_url}tbooking/load_index_sp/'+id,
		dataType: "json",
		success: function(data) {
			$("#tabel_sp tbody").empty();
			$("#tabel_sp tbody").append(data.tabel);
			$(".js-select2").select2();
			validate_sp();
			$("#cover-spin").hide();
		}
	});
}
$(document).on("change",".nilai_sp",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var sp_id=tr.find(".sp_id").val();
	var nilai_id=$(this).val();
	$.ajax({
		  url: '{site_url}tbooking/update_nilai_sp/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				sp_id:sp_id,nilai_id:nilai_id,
		  },success: function(data) {
				validate_sp();
			}
		});
	
});
function load_index_sc(){
	$("#cover-spin").show();
	var id=$("#id").val();
	// alert(id);
	$.ajax({
		url: '{site_url}tbooking/load_index_sc/'+id,
		dataType: "json",
		success: function(data) {
			$("#tabel_sc tbody").empty();
			$("#tabel_sc tbody").append(data.tabel);
			$(".js-select2").select2();
			validate_sc();
			$("#cover-spin").hide();
		}
	});
}
$(document).on("change",".nilai_sc",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var sc_id=tr.find(".sc_id").val();
	var nilai_id=$(this).val();
	$.ajax({
		  url: '{site_url}tbooking/update_nilai_sc/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				sc_id:sc_id,nilai_id:nilai_id,
		  },success: function(data) {
				validate_sc();
			}
		});
	
});
$("#kelompokpasien2").change(function() {
	let kelompokpasien=$("#kelompokpasien2").val();
	if (kelompokpasien=='1'){
		$(".div_asuransi").css("display", "block");
	}else{
		
		$(".div_asuransi").css("display", "none");
	}
	if (kelompokpasien=='5'){
		$(".div_foto").css("display", "none");
	}else{
		
		$(".div_foto").css("display", "block");
	}
});

function generate_rincian(){
	
	$(".lbl_dokter").val($("#iddokter option:selected").text());
	$(".lbl_tujuan").val($("#idpoli option:selected").text());
	$(".lbl_jenis_pertemuan").val($("#pertemuan_id option:selected").text());
	// $(".lbl_asuransi").val($("#idrekanan option:selected").text());
	$(".lbl_status_pasien").val($("#statuspasienbaru option:selected").text());
	$(".lbl_jenis_booking").val($("#reservasi_cara option:selected").text());
	// $(".lbl_cara_pembayaran").val($("#kelompokpasien option:selected").text());
	// $(".lbl_tanggal_janji").val($("#tanggal option:selected").text() + ' ('+$("#jadwal_id option:selected").text()+')');
	$(".lbl_nama").val($("#namapasien").val());
	$(".lbl_norekammedis").val($("#no_medrec").val());
	let kelompokpasien=$("#kelompokpasien2").val();
	if (kelompokpasien=='1'){
		$(".div_asuransi").css("display", "block");
	}else{
		
		$(".div_asuransi").css("display", "none");
	}
	if (kelompokpasien=='5'){
		$(".div_foto").css("display", "none");
	}else{
		
		$(".div_foto").css("display", "block");
	}
	
}
function load_index_history(){
	$('#tabel_ha').DataTable().destroy();	
	let id=$("#id").val();
	// alert(id);
	table = $('#tabel_ha').DataTable({
            autoWidth: false,
            searching: false,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					// { "width": "3%", "targets": 0,  className: "text-right" },
					// { "width": "8%", "targets": [1,2,3,6,7,8,9],  className: "text-center" },
					// { "width": "12%", "targets": [4,10],  className: "text-center" },
					// { "width": "15%", "targets": 5,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}tbooking/load_index_history', 
                type: "POST" ,
                dataType: 'json',
				data : {
						id:id,
						
					   }
            }
        });
}
</script>