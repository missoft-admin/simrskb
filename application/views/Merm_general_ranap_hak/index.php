<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block" >
	<div class="block-header">
		<div class="block-header">
			
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('merm_general_ranap_hak/save', 'class="form-horizontal push-10-t"') ?>
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="example-hf-email"></label>
            <div class="col-md-7">

                <img class="img-avatar"  id="output_img" src="{upload_path}app_setting/<?=($logo?$logo:'no_image.png')?>" />
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="example-hf-email">Logo  (100x100)</label>
            <div class="col-md-7">
                <div class="box">
                    <input type="file" id="file-3" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;" name="logo" value="{logo}" />
                    <label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                </div>
            </div>
        </div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="alamat_form">Alamat<span style="color:red;">*</span></label>
			<div class="col-md-10">
				<input tabindex="0" type="text" class="form-control" id="alamat_form" placeholder="Alamat" name="alamat_form" value="{alamat_form}" required>
			</div>
			
		</div>
		<div class="form-group">
			
			<label class="col-md-2 control-label" for="telepone_form">Phone<span style="color:red;">*</span></label>
			<div class="col-md-4">
				<input tabindex="0" type="text" class="form-control" id="telepone_form" placeholder="Phone" name="telepone_form" value="{telepone_form}" required>
			</div>
			<label class="col-md-2 control-label" for="email_form">Email<span style="color:red;">*</span></label>
			<div class="col-md-4">
				<input tabindex="0" type="text" class="form-control" id="email_form" placeholder="Email" name="email_form" value="{email_form}" required>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="judul">Judul <span style="color:red;">*</span></label>
			<div class="col-md-10">
				<input tabindex="0" type="text" class="form-control" id="judul" placeholder="Judul " name="judul" value="{judul}" required>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="judul">Judul (Eng)<span style="color:red;">*</span></label>
			<div class="col-md-10">
				<input tabindex="0" type="text" class="form-control" id="judul_eng" placeholder="English " name="judul_eng" value="{judul_eng}" required>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-md-2 control-label" for="sub_header">Content<span style="color:red;">*</span></label>
			<div class="col-md-4">
				<button class="btn btn-danger btn-xs" type="button" id="add_content"><i class="fa fa-plus"></i> Content</button>
			</div>
			
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="sub_header"></label>
			<div class="col-md-10">
				<table class="table" id="index_content">
					<thead>
						<tr>
							<th class="text-center" style="width: 50px;">#</th>
							<th>Content</th>
							<th class="hidden-xs" style="width: 15%;">Opsi</th>
							<th class="hidden-xs" style="width: 15%;">Created</th>
							<th class="text-center" style="width: 100px;">Actions</th>
						</tr>
					</thead>
					<tbody>
						
						
					</tbody>
				</table>
			</div>
			
		</div>
		
		<div class="form-group">
			<div class="col-md-12 col-md-offset-2">
				<button class="btn btn-success" type="submit">Simpan</button>
				<a href="{base_url}merm_general_ranap_hak" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
			</div>
		</div>
		<?php echo form_hidden('id', $id); ?>
		<?php echo form_close() ?>
	</div>
</div>

<div class="modal" id="modal_content" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Content</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal push-10-t">
					<div class="row">
                        <input type="hidden" readonly id="header_id" name="header_id" value="">
                        <input type="hidden" readonly id="idcontent" name="idcontent" value="">

						<div class="col-md-12">                               
							<div class="form-group">
								<label class="col-md-2 control-label">No Urut</label>
								<div class="col-md-9"> 
									<input tabindex="0" type="text" class="form-control number" maxlength="3" id="no" placeholder="No Urut" value="">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Content</label>
								<div class="col-md-9"> 
									<textarea class="form-control js-summernote footer" name="isi" id="isi"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Jenis Jawaban</label>
								<div class="col-md-9"> 
									<select tabindex="5" id="jenis_isi"   name="reservasi_cara" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="0"  <?=($jenis_isi == "0" ? 'selected="selected"' : '')?>>Label</option>
										<option value="1"  <?=($jenis_isi == "1" ? 'selected="selected"' : '')?>>Option</option>
										<option value="2" <?=($jenis_isi == 2 ? 'selected="selected"' : '')?>>Free Text</option>
										<option value="3" <?=($jenis_isi == 3 ? 'selected="selected"' : '')?>>Tanda Tangan</option>
										
									</select>
								</div>
							</div>
							<div class="form-group div_opsi">
								<label class="col-md-2 control-label">Value</label>
								<div class="col-md-9"> 
									<select id="ref_id" name="ref_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
																		
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">                               
							<br>
						</div>
                    </div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-primary" type="submit" type="button" id="btn_simpan_content"><i class="fa fa-check"></i> Simpan</button>
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.number').number(true, 0);
		$('.footer').summernote({
		  height: 130,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})	
		load_content();
		
	})	
	$("#jenis_isi").change(function() {
		let jenis_isi=$("#jenis_isi").val();
		if (jenis_isi=='1'){
			$(".div_opsi").css("display", "block");
		}else{
			
			$(".div_opsi").css("display", "none");
		}
		
	});

	//add_content
	$("#btn_simpan_content").click(function() {
		if ($("#no").val()==''){
			sweetAlert("Maaf...", "Tentukan No Urut", "error");
			return false;
		}
		if ($("#no").val()=='0'){
			sweetAlert("Maaf...", "Tentukan No Urut", "error");
			return false;
		}

		if ($("#isi").val()==''){
			sweetAlert("Maaf...", "Isi Content ", "error");
			return false;
		}
		var idcontent=$("#idcontent").val();
		var no=$("#no").val();
		var header_id=$("#header_id").val();
		var isi=$("#isi").val();
		var jenis_isi=$("#jenis_isi").val();
		var ref_id=$("#ref_id").val();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}merm_general_ranap_hak/simpan_content/',
			dataType: "json",
			type: "POST",
			data: {
				idcontent:idcontent,
				header_id:header_id,
				jenis_isi:jenis_isi,
				ref_id:ref_id,
				no: no,isi:isi
			},
			success: function(data) {
				$("#modal_content").modal('hide');
				$("#cover-spin").hide();
				load_content();
			}
		});

	});
	$("#add_content").click(function() {
		$("#modal_content").modal('show');
		$("#header_id").val('0');
		$("#idcontent").val('');
		var rowCount = $('#index_content tr').length;
		$("#no").val(rowCount);
		$('#isi').summernote('code','');
		var id='';
		$.ajax({
				url: '{site_url}merm_general_ranap_hak/load_jawaban',
				type: 'POST',
				dataType: "json",
				data: {id: id},
				success: function(data) {
					console.log(data);
					$("#ref_id").empty();
					$('#ref_id').append(data);
					$("#cover-spin").hide();
				}
		});
	});
	function add_detail(id){
		$("#modal_content").modal('show');
		$("#header_id").val(id);
		$("#idcontent").val('');
		var rowCount = $('#index_content tr').length;
		$("#no").val(rowCount);
		$('#isi').summernote('code','');
		var id='';
		$.ajax({
				url: '{site_url}merm_general_ranap_hak/load_jawaban',
				type: 'POST',
				dataType: "json",
				data: {id: id},
				success: function(data) {
					console.log(data);
					$("#ref_id").empty();
					$('#ref_id').append(data);
					$("#cover-spin").hide();
				}
		});
	};
	function edit_content(id,header_id){
		$("#idcontent").val(id);
		$("#header_id").val(header_id);
		$("#modal_content").modal('show');
		$("#cover-spin").show();
			
		 $.ajax({
				url: '{site_url}merm_general_ranap_hak/load_jawaban',
				type: 'POST',
				dataType: "json",
				data: {id: id},
				success: function(data) {
					console.log(data);
					$("#ref_id").empty();
					$('#ref_id').append(data);
					$("#cover-spin").hide();
				}
		});
		$("#cover-spin").show();
		$.ajax({
				url: '{site_url}merm_general_ranap_hak/get_edit',
				type: 'POST',
				dataType: "json",
				data: {id: id},
				success: function(data) {
					console.log(data);
					$('#isi').summernote('code',data.isi);
					// $("#isi").html(data.isi);
					$("#jenis_isi").val(data.jenis_isi).trigger('change');
					$("#no").val(data.no);
					$("#cover-spin").hide();
					
				}
		});
	}
	function loadFile_img(event){
		// alert('load');
		var output_img = document.getElementById('output_img');
		output_img.src = URL.createObjectURL(event.target.files[0]);
	}
	function load_content(){
		$.ajax({
			url: '{site_url}merm_general_ranap_hak/load_content/',
			dataType: "json",
			success: function(data) {
				$("#index_content tbody").empty();
				$('#index_content tbody').append(data);
			}
		});

	}
	function hapus(id){
		swal({
			title: "Anda Yakin ?",
			text : "Akan Hapus?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			
			 $.ajax({
					url: '{site_url}merm_general_ranap_hak/hapus',
					type: 'POST',
					data: {id: id},
					complete: function() {
						load_content();
						$("#cover-spin").hide();
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						
					}
				});
		});
	}
</script>