<?php echo ErrorSuccess($this->session) ?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}
?>

<!-- Developer : @Muhamad Ali Nurdin (&Kamar Operasi) -->

<style>
	table, tbody, tr, td{
		cursor: pointer;
	}
	label {
		font-size: 12px;
	}
	input[type=text] {
		font-size: 12px;
	}
	.modal-xl {
		width: 1140px;
	}
	.font-eight {
		font-size: 8px;
	}
	.input-custom, .btn-custom {
		float: left;
	}
	.input-custom {
		width: 80%;
		border-radius: 3px 0 0 3px;
	}
	.btn-custom {
		width: 20%;
		border-radius: 0 3px 3px 0
	}
	.btn-cs {
		width: 70px;
	}
	.input-group {
		z-index: 1;
	}
	#jenisoperasi-error {
		color: red;
		font-size: 11px;
		position: relative;
	}
	.total{
		font-size: 18px;
		position: relative;
	}
	.total_red{
		color:#d64d4d;
		font-size: 18px;
		position: relative;
	}

	.my-valid-class {
    color:red;
	}

</style>

<?php if (UserAccesForm($user_acces_form,array('957'))){ ?>
<?php echo form_open('tko/save_tindakan', 'class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
<!-- @Tindakan Utama Kamar Operasi -->

<div class="block">
	<div class="block-header">
		<ul class="block-options">
      <li>
        <a href="{base_url}tko" class="btn"><i class="fa fa-reply"></i></a>
      </li>
    </ul>
		<h3 class="block-title">HEADER TINDAKAN OPERASI</h3>
		<hr style="margin-bottom: 0px;">
	</div>
	<div class="block-content" style="margin-top: -20px;font-size:">

		<div class="row">
			<div class="col-md-12">
				<!-- @Bag kiri -->
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="nomedrec">No. Medrec</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="nomedrec" id="nomedrec" value="{no_medrec}" readonly>
							<input type="hidden" class="form-control" name="idkelompokpasien" id="idkelompokpasien" value="{idkelompokpasien}" readonly>
							<input type="hidden" class="form-control" name="st_lock" id="st_lock" value="{st_lock}" readonly>
							<input type="hidden" class="form-control" name="id_tindakan" id="id_tindakan" value="{id}" readonly>
							<input type="hidden" class="form-control" name="tarif_default" id="tarif_default" value="{tarif_default}" readonly>
							<input type="hidden" class="form-control" name="disabel" id="tarif_default2" value="{tarif_default}" readonly>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="namapasien">Nama Pasien</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="namapasien" value="{namapasien}" readonly>
						</div>
					</div>

					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="tanggallahir">Tanggal Lahir</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="tanggallahir" value="<?=$tanggal_lahir?>" readonly>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="umurpasien">Umur</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="umurpasien" value="<?=$umurtahun?> Th <?=$umurbulan?> Bln <?=$umurhari?> Hr" readonly>
						</div>
					</div>

					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="dokterdpjp">Dokter DPJP</label>
						<div class="col-md-8">
							<select name="dpjp" readonly id="dpjp" class="js-select2 form-control disabel" style="width: 100%;">
								<option value="0">Semua Dokter </option>
								<?foreach ($dokterumum as $row){?>
									<option value="<?=$row->id?>" <?=($dpjp==$row->id)?'selected':''?>><?=$row->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="tanggaloperasi" style="text-transform:text-uppercase;">Tanggal Operasi</label>
						<div class="col-md-8 date">
							<input type="text" class="form-control input-kasir disabel" id="tanggaloperasi" name="tanggaloperasi" value="<?=date('d-m-Y', strtotime($tanggaloperasi))?>" required="" aria-required="true">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="waktuoperasi" style="text-transform:text-uppercase;">Waktu Operasi</label>
						<div class="col-md-3" style="float:left;">
							<input type="text" class="form-control input-kasir jam disabel" id="waktuawal" name="waktuawal" value="<?=$waktumulaioperasi?>" aria-required="true">
						</div>
						<div class="col-md-1"><span style="float:left;line-height:33px;">S/D</span></div>
						<div class="col-md-3 bootstrap-timepicker timepicker">
							<input type="text" class="form-control input-kasir jam disabel" id="waktuakhir" name="waktuakhir" value="<?=date("H:i");?>" aria-required="true">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="diagnosa">Diagnosa</label>
						<div class="col-md-8">
							<input type="text" name="diagnosa" class="form-control input-kasir disabel" id="diagnosa" value="<?=$diagnosa?>">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="tindakanoperasi">Operasi</label>
						<div class="col-md-8">
							<input type="text" name="operasi" class="form-control input-kasir disabel" id="operasi" value="<?=$operasi?>">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="tkelas">Jenis Operasi</label>
							<div class="col-md-8">
								<select name="jenis_operasi_id" <?=(UserAccesForm($user_acces_form,array('958'))?'':'disabled')?> id="jenis_operasi_id" class="js-select2 form-control ubahdata disabel" style="width: 100%;">
									<option value="0">Pilih Jenis Operasi</option>
									<?foreach ($mjenis_operasi as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_operasi_id==$row->id)?'selected':''?>><?=$row->nama?></option>
									<?}?>
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="tkelas">Jenis Anesthesi</label>
							<div class="col-md-8">
								<select name="jenis_anestesi_id" id="jenis_anestesi_id" class="js-select2 form-control disabel" style="width: 100%;">
									<option value="0">Pilih Jenis Anesthesi</option>

									<?foreach ($mjenis_anaesthesi as $row){?>
										<option value="<?=$row->id?>" <?=($jenis_anestesi_id==$row->id)?'selected':''?>><?=$row->nama?></option>
									<?}?>
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="tkelas">Kelompok Operasi</label>
							<div class="col-md-8">
								<select name="kelompok_operasi_id" id="kelompok_operasi_id" class="js-select2 form-control disabel" style="width: 100%;">
									<option value="0">Pilih Kelompok Operasi</option>
										<?foreach ($mkelompok_operasi as $row){?>
											<option value="<?=$row->id?>" <?=($kelompok_operasi_id==$row->id)?'selected':''?>><?=$row->nama?></option>
										<?}?>
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="tkelas">Kelompok Tindakan</label>
							<div class="col-md-8">
								<select multiple="multiple" name="kelompok_tindakan_id[]" id="kelompok_tindakan_id" class="js-select2 form-control disabel" style="width: 100%;">
									<?foreach($kelompok_tindakan_list as $row){?>
										<option value="<?=$row->id?>" <?=$row->selected?>><?=$row->text?></option>
									<?}?>
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-top: 15px;" id="div_new">
							<label class="col-md-4 control-label" for="tkelas"></label>
							<div class="col-md-4">
								<button class="btn btn-primary text-uppercase" type="button" id="btn_add" name="btn_add" style="font-size:13px;width:100%;float:right;"><i class="fa fa-file-archive-o"></i> Show Estimasi</button>
							</div>
							<div class="col-md-4">
								<button class="btn btn-danger text-uppercase" type="button" id="btn_gabung" name="btn_gabung" style="font-size:13px;width:100%;float:right;"><i class="fa fa-arrow-down"></i> Input Mutu </button>
							</div>
						</div>
				</div>
				<!-- @Bag kanan -->
				<div class="col-md-6">
					<label class="col-md-12">Mengajukan Jadwal Operasi</label>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="tanggaloperasi">Tanggal</label>
						<div class="col-md-7">
							<input type="text" class="form-control" id="tanggal_diajukan" value="<?=$tanggal_diajukan?>" readonly>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="tanggaloperasi">Petugas</label>
						<div class="col-md-7">
							<input type="text" class="form-control" id="namapetugas" value="<?=$namapetugas?>" readonly>
						</div>
					</div>
					<hr style="margin-bottom: 10px;margin-top: 10px;">
					<label class="col-md-12">Menyetujui Jadwal Operasi</label>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="tanggal_disetujui">Tanggal</label>
						<div class="col-md-7">
							<input type="text" class="form-control" id="tanggal_disetujui" value="<?=$tanggal_disetujui?>" readonly>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 0px;">
						<label class="col-md-4 control-label" for="tanggaloperasi">Petugas</label>
						<div class="col-md-7">
							<input type="text" class="form-control" id="user_setuju" value="<?=$user_setuju?>" readonly>
						</div>
					</div>
						<hr style="margin-bottom: 10px;margin-top: 10px;">

					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="tkelompok_pasien">Kelompok Pasien</label>
						<div class="col-md-7">
							<input type="text" class="form-control" readonly  id="tkelompok_pasien" value="{kelompokpasien}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="tasuransi">Perusahaan / Asuransi</label>
						<div class="col-md-7">
							<input type="text" class="form-control" readonly  id="tasuransi" value="{asuransi}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="ttipe">Tipe</label>
						<div class="col-md-7">
							<input type="text" class="form-control" readonly  id="ttipe" value="{tipe}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="tkelas">Kelas Perawatan</label>
						<div class="col-md-7">
							<input type="text" class="form-control" readonly  id="tkelas" value="<?=$kelas.($bed?' / '.$bed:'')?>">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="tperujuk">Dokter Perujuk</label>
						<div class="col-md-7">
							<input type="text" class="form-control"  readonly id="tperujuk" value="{perujuk}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="kelas_tarif_id">Kelas Tarif</label>
						<div class="col-md-7">
							<select name="kelas_tarif_id" <?=(UserAccesForm($user_acces_form,array('959'))?'':'disabled')?> id="kelas_tarif_id" class="js-select2 form-control ubahdata  disabel" style="width: 100%;">
								<option value="0">Pilih Kelas Tarif</option>
								<?foreach ($mkelas as $row){?>
									<option value="<?=$row->id?>"  <?=($kelas_tarif_id==$row->id)?'selected':''?>><?=$row->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="ruang_id">Ruang Operasi</label>
						<div class="col-md-7">
							<select name="ruang_id" id="ruang_id" class="js-select2 form-control disabel" style="width: 100%;">
								<option value="0">Pilih Ruang Operasi</option>
								<?foreach ($mruangan as $row){?>
									<option value="<?=$row->id?>" <?=($ruang_id==$row->id)?'selected':''?>><?=$row->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="tkelompok_diagnosa">Kelompok Diagnosa</label>
						<div class="col-md-7">
							<select multiple="multiple" name="kelompok_diagnosa_id[]" id="kelompok_diagnosa_id" class="form-control disabel" style="width: 100%;">
								<?foreach($kelompok_diagnosa_list as $row){?>
									<option value="<?=$row->id?>" <?=$row->selected?>><?=$row->text?></option>
								<?}?>
							</select>
						</div>
					</div>


				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-2 control-label" for="tnamapasien">Catatan</label>
						<div class="col-md-10">
							<textarea class="form-control js-summernote-custom-height disabel" id="tcatatan" placeholder="Catatan" name="tcatatan" rows="5"><?=$catatan?></textarea>
						</div>
					</div>
				</div>
				<hr>
			</div>
		</div>
	</div>
</div>
<div class="block">
	<div class="block-header">

		<h3 class="block-title">DETAIL TINDAKAN OPERASI</h3>
	</div>
	<div class="block-content" style="margin-top: -20px;font-size:">
	<div class="row">
		<div class="col-md-12">
			<div class="block block-themed ">
				<div class="block-header bg-primary">
					<ul class="block-options">

						<li>
							<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
						</li>
					</ul>
					<h3 class="block-title">LAYANAN FULL CARE</h3>
				</div>
				<div class="block-content">
					<table id="tabel_do" class="table table-striped table-bordered" style="margin-bottom: 0;">
							<thead>
								<tr>
									<th style="width: 30%;">NAMA TARIF</th>
									<th style="width: 20%;">RUANGAN</th>
									<th style="width: 20%;">KELAS</th>
									<th style="width: 20%;">TARIF</th>

								</tr>
								<tr>
									<td><input type="text" name="nama_tarif_fullcare" class="form-control nama_tarif_fullcare"  value="" readonly></td>
									<td><input type="text" name="ruangan_fullcare" class="form-control ruangan_fullcare"  value="" readonly></td>
									<td><input type="text" name="kelas_operasi_alat2"  class="form-control kelas_operasi_alat"  value="" readonly></td>
									<td><input type="text" name="tarif_fullcare" id="tarif_fullcare" class="form-control tarif_fullcare number" value="0" readonly></td>


									<input type="hidden" name="id_tarif_fullcare" id="id_tarif_fullcare" class="form-control id_tarif_fullcare"  value="" readonly>
									<input type="hidden" name="sarana_fullcare" id="sarana_fullcare" class="form-control sarana_fullcare"  value="" readonly>
									<input type="hidden" name="pelayanan_fullcare" id="pelayanan_fullcare" class="form-control pelayanan_fullcare"  value="" readonly>
									<input type="hidden" name="bhp_fullcare" id="bhp_fullcare" class="form-control bhp_fullcare"  value="" readonly>
									<input type="hidden" name="perawatan_fullcare" id="perawatan_fullcare" class="form-control perawatan_fullcare"  value="" readonly>
								</tr>
							</thead>

							<tfoot id="foot-total-nonracikan">
								<tr>

									<th class="hidden-phone">
										<span class="pull-right total"><b>TOTAL FULLCARE</b></span></th>
									<th ><b><span id="lbl_total_fullcare" class="total">0</span></b></th>
									<th colspan="3"><input type="hidden" id="total_fullcare" name="total_fullcare" class="tarif_fullcare" value="0"> </th>
								</tr>
							</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="block block-themed ">
				<div class="block-header bg-primary">
					<ul class="block-options">

						<li>
							<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
						</li>
					</ul>
					<h3 class="block-title">LAYANAN SEWA RUANGAN</h3>
				</div>
				<div class="block-content">
					<table id="tabel_do" class="table table-striped table-bordered" style="margin-bottom: 0;">
							<thead>
								<tr>
									<th style="width: 30%;">NAMA TARIF</th>
									<th style="width: 20%;">OPERASI</th>
									<th style="width: 20%;">KELAS</th>
									<th style="width: 20%;">TARIF</th>

								</tr>
								<tr>
									<td><input type="text" name="nama_tarif_ruangan" class="form-control nama_tarif_ruangan"  value="" readonly></td>
									<td><input type="text" name="jenis_tarif_ruangan" class="form-control jenis_operasi_do"  value="" readonly></td>
									<td><input type="text" name="kelas_operasi_sewa_ruangan" id="kelas_operasi_sewa_ruangan" class="form-control kelas_operasi_alat"  value="" readonly></td>
									<td><input type="text" name="tarif_ruangan" id="tarif_ruangan" class="form-control tarif_ruangan number" value="0" readonly></td>


									<input type="hidden" name="id_tarif_ruangan" id="id_tarif_ruangan" class="form-control id_tarif_ruangan"  value="" readonly>
									<input type="hidden" name="sarana_ruangan" id="sarana_ruangan" class="form-control sarana_ruangan"  value="" readonly>
									<input type="hidden" name="pelayanan_ruangan" id="pelayanan_ruangan" class="form-control pelayanan_ruangan"  value="" readonly>
									<input type="hidden" name="bhp_ruangan" id="bhp_ruangan" class="form-control bhp_ruangan"  value="" readonly>
									<input type="hidden" name="perawatan_ruangan" id="perawatan_ruangan" class="form-control perawatan_ruangan"  value="" readonly>
								</tr>
							</thead>

							<tfoot id="foot-total-nonracikan">
								<tr>

									<th class="hidden-phone">
										<span class="pull-right total"><b>TOTAL SEWA RUANGAN</b></span></th>
									<th ><b><span id="lbl_total_ruangan" class="total">0</span></b></th>
									<th colspan="3"><input type="hidden" id="total_ruangan" name="total_ruangan" class="tarif_ruangan" value="0"> </th>
								</tr>
							</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="block block-themed <?=($tind_dokter_operator?'':'block-opt-hidden')?>">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
						</li>
					</ul>
					<h3 class="block-title">DOKTER OPERATOR</h3>
				</div>
				<div class="block-content">
					<table id="tabel_do" class="table table-striped table-bordered" style="margin-bottom: 0;">
							<thead>
								<tr>
									<th style="width: 30%;">DOKTER</th>
									<th style="width: 20%;">TARIF</th>
									<th style="width: 20%;">NAMA TARIF</th>
									<th style="width: 15%;">JENIS OPERASI</th>

									<th style="width: 15%;">Actions</th>
								</tr>
								<tr>
									<td>
										<div class="input-group">
											<select name="dokter_operator_id" tabindex="16"  style="width: 100%" id="dokter_operator_id" data-placeholder="Cari Dokter" class="form-control input-sm  disabel">
												<option value="#" selected>-Pilih Dokter-</option>
												<?foreach ($dokterumum as $row){?>
													<option value="<?=$row->id?>"><?=$row->nama?></option>
												<?}?>
											</select>
											<span class="input-group-btn">
												<button data-toggle="modal" data-target="#modal_cari_do" tabindex="17" class="btn btn-sm btn-info" type="button" id="btn_search_do"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</td>
									<td><input type="text" name="tarif_do" id="tarif_do" class="form-control tarif_do number" value="0" readonly></td>
									<td><input type="text" name="nama_tarif_do" id="nama_tarif_do" class="form-control nama_tarif_do"  value="" readonly></td>
									<td><input type="text" name="jenis_operasi_do" id="jenis_operasi_do" class="form-control jenis_operasi_do"  value="" readonly></td>

									<td>
										<?php if (UserAccesForm($user_acces_form,array('960'))){ ?>
										<button type="button" class="btn btn-sm btn-primary disabel dis2" tabindex="8" id="add_do" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
										<?}?>
										<button type="button" class="btn btn-sm btn-danger disabel dis2" tabindex="9" id="cancel_do" title="Refresh"><i class="fa fa-times"></i></button>
									</td>
									<input type="hidden" name="id_tarif_do" id="id_tarif_do" class="id_tarif_do"  value="" readonly>
									<input type="hidden" name="sarana_do" id="sarana_do" class="form-control sarana_do"  value="" readonly>
									<input type="hidden" name="pelayanan_do" id="pelayanan_do" class="form-control pelayanan_do"  value="" readonly>
									<input type="hidden" name="bhp_do" id="bhp_do" class="form-control bhp_do"  value="" readonly>
									<input type="hidden" name="perawatan_do" id="perawatan_do" class="form-control perawatan_do"  value="" readonly>
									<input type="hidden" name="nomor_do" id="nomor_do" class="form-control"  value="" readonly>
									<input type="hidden" name="row_do" id="row_do" class="form-control"  value="" readonly>
									<input type="hidden" name="auto_number_do" id="auto_number_do" class="form-control"  value="1" readonly>
									<input type="hidden" name="st_beda" id="st_beda" class="form-control"  value="0" readonly>
								</tr>
							</thead>
							<tbody>
								<?
									$no=1;
								foreach ($tind_dokter_operator as $row){?>
									<tr>
									<td style='display:none'><?=$no?> </td>
									<td style='display:none'><?=$row->iddokteroperator?></td>
									<td><?=$row->namadokteroperator?> </td>
									<td><input type="text" name="tarif_do_detail[]" class="form-control <?=($row->id_tarif==$tarif_default?'tarif_do':'')?> number" value="<?=number_format($row->total)?>" readonly></td>
									<td><input type="text" name="nama_tarif_do_detail[]" class="form-control <?=($row->id_tarif==$tarif_default?'nama_tarif_do':'')?>" value="<?=$row->tarif_nama?>" readonly></td>
									<td><input type="text" name="jenis_operasi_do_detail[]" class="form-control <?=($row->id_tarif==$tarif_default?'jenis_operasi_do':'')?>" value="<?=$row->jenis_operasi_nama?>" readonly></td>
									<td>
										<?php if (UserAccesForm($user_acces_form,array('961'))){ ?>
										<button type='button' title="Edit Dokter" class='btn btn-sm btn-info edit disabel'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;
										<button type='button' title="Adjust Tarif" class='btn btn-sm btn-warning adjust disabel'><i class='si si-note'></i></button>&nbsp;&nbsp;
										<?}?>
										<?php if (UserAccesForm($user_acces_form,array('962'))){ ?>
										<button type='button' title="Hapus Dokter" class='btn btn-sm btn-danger hapus disabel'><i class='glyphicon glyphicon-remove'></i></button>
										<?}?>
									</td>
									<td style='display:none'><input type="text" name="nama_do_detail[]" value="<?=$row->namadokteroperator?>" readonly></td>
									<td style='display:none'><input type="text" name="id_do_detail[]" value="<?=$row->iddokteroperator?>" readonly></td>
									<td style='display:none'><input type="text" class="<?=($row->id_tarif==$tarif_default?'id_tarif_do':'')?>" name="id_tarif_do_detail[]" value="<?=$row->id_tarif?>" readonly></td>
									<td style='display:none'><input type="text" class="<?=($row->id_tarif==$tarif_default?'sarana_do':'')?>" name="sarana_do_detail[]" value="<?=$row->jasasarana?>" readonly></td>
									<td style='display:none'><input type="text" class="<?=($row->id_tarif==$tarif_default?'pelayanan_do':'')?>" name="jasapelayanan_do_detail[]" value="<?=$row->jasapelayanan?>" readonly></td>
									<td style='display:none'><input type="text" class="<?=($row->id_tarif==$tarif_default?'bhp_do':'')?>" name="bhp_do_detail[]" value="<?=$row->bhp?>" readonly></td>
									<td style='display:none'><input type="text" class="<?=($row->id_tarif==$tarif_default?'perawatan_do':'')?>" name="biayaperawatan_do_detail[]" value="<?=$row->biayaperawatan?>" readonly></td>
									</tr>
								<?
								$no=$no+1;
								}?>
							</tbody>
							<tfoot id="foot-total-nonracikan">
								<tr>

									<th class="hidden-phone">
										<span class="pull-right total"><b>TOTAL JASA DOKTER OPERATOR</b></span></th>
									<th ><b><span id="lbl_total_do" class="total">0</span></b></th>
									<th colspan="3"><input type="hidden" id="total_do" name="total_do" value="0"> </th>
								</tr>
							</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="block block-themed <?=($tind_dokter_anatesi?'':'block-opt-hidden')?>">
				<div class="block-header bg-primary">
					<ul class="block-options">

						<li>
							<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
						</li>
					</ul>
					<h3 class="block-title">DOKTER Anesthesi </h3>
				</div>
				<div class="block-content">
					<table id="tabel_ref_da" class="table table-striped table-bordered" style="margin-bottom: 0;">
							<thead>
								<tr>
									<th style="width: 30%;">DOKTER OPERATOR</th>
									<th style="width: 20%;">JENIS OPERASI</th>
									<th style="width: 15%;">TOTAL OPERATOR</th>
									<th style="width: 10%;">PESENTASE %</th>
									<th style="width: 15%;">TOTAL ANESTHESI</th>

								</tr>
								<tr>
									<td>
										<span id="lbl_dokter_operator"></span>
									</td>
									<td><input type="text" name="jenis_operasi_da" id="jenis_operasi_da" class="form-control jenis_operasi_do"  value="" readonly></td>
									<td><input type="text" name="total_o" id="total_o" class="form-control total_o number" value="0" readonly></td>
									<td><input type="text" name="persen_da" id="persen_da" class="form-control persen disabel"  value="<?=($persen_da)?$persen_da:'40'?>" ></td>
									<td><input type="text" name="total_da" id="total_da" class="form-control number"  value="0" readonly></td>
								</tr>

							</thead>
					</table>
					<hr>
					<table id="tabel_da" class="table table-striped table-bordered" style="margin-bottom: 0;">
							<thead>
								<tr>
									<th style="width: 30%;">DOKTER ANESTHESI</th>
									<th style="width: 20%;">PERSENTASE</th>
									<th style="width: 15%;">JASA MEDIS ANESTHESI</th>
									<th style="width: 10%;">Actions</th>
								</tr>
								<tr>
									<td>
										<div class="input-group">
											<select name="dokter_anestesi_id" tabindex="16"  style="width: 100%" id="dokter_anestesi_id" data-placeholder="Cari Dokter" class="form-control input-sm disabel">
												<option value="#" selected>-Pilih Dokter-</option>

											</select>
											<span class="input-group-btn">
												<button tabindex="17" class="btn btn-sm btn-info" type="button" id="btn_search_da"><i class="fa fa-search disabel"></i></button>
											</span>
										</div>
									</td>
									<td><input type="text" name="persen_dokter_da" id="persen_dokter_da" class="form-control persen disabel"  value="0" ></td>
									<td><input type="text" name="tarif_da" id="tarif_da" class="form-control number"  value="0" readonly></td>
									<td>
										<?php if (UserAccesForm($user_acces_form,array('963'))){ ?>
										<button type="button" class="btn btn-sm btn-primary disabel" tabindex="8" id="add_da" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
										<?}?>
										<button type="button" class="btn btn-sm btn-danger disabel" tabindex="9" id="cancel_da" title="Refresh"><i class="fa fa-times"></i></button>
										<input type="hidden" name="dokter_anestesi_tipe" id="dokter_anestesi_tipe" class="form-control"  value="" readonly>
										<input type="hidden" name="persen_da_before" id="persen_da_before" class="form-control"  value="" readonly>
									</td>
									<input type="hidden" name="nomor_da" id="nomor_da" class="form-control"  value="" readonly>
									<input type="hidden" name="row_da" id="row_da" class="form-control"  value="" readonly>
									<input type="hidden" name="auto_number_da" id="auto_number_da" class="form-control"  value="1" readonly>
								</tr>
							</thead>
							<tbody>
								<?
										$no=1;
									foreach ($tind_dokter_anatesi as $row){?>
										<tr>
										<td style='display:none'><?=$no?> </td>
										<td style='display:none'>1</td>
										<td style='display:none'><?=$row->iddokteranastesi?></td>
										<td><?=$row->namadokteranastesi?> </td>
										<td><input type="text" name="persen_da_detail[]" class="form-control persen persen_da disabel" value="<?=number_format($row->persen)?>"></td>
										<td><input type="text" name="tarif_da_detail[]" class="form-control number tarif_da_detail disabel" value="<?=$row->totalkeseluruhan?>" readonly></td>
										<td>
											<?php if (UserAccesForm($user_acces_form,array('964'))){ ?>
											<button type='button' class='btn btn-sm btn-info edit_da disabel'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;
											<?}?>
											<?php if (UserAccesForm($user_acces_form,array('965'))){ ?>
											<button type='button' class='btn btn-sm btn-danger hapus_da disabel'><i class='glyphicon glyphicon-remove'></i></button>
											<?}?>
										</td>
										<td style='display:none'><input type="text" name="id_da_detail[]" value="<?=$row->iddokteranastesi?>" readonly></td>
										<td style='display:none'><input type="text" name="id_nama_da_detail[]" value="<?=$row->namadokteranastesi?>" readonly></td>
										</tr>
									<?
									$no=$no+1;
									}?>
							</tbody>
							<tfoot id="foot-total-nonracikan">
								<tr>

									<th class="hidden-phone">
										<span class="pull-right total"><b>TOTAL JASA DOKTER ANESTHESI</b></span></th>
									<th ><b><span id="lbl_total_persen_da" class="total_red">0</span></b></th>
									<th ><b><span id="lbl_total_da" class="total_red">0</span></b></th>
									<th colspan="2">
											<input type="hidden" id="gt_da" name="gt_da" value="0">
											<input type="hidden" id="gt_persen_da" name="gt_persen_da" value="0">
									</th>
								</tr>
							</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	<div class="col-md-12">
		<div class="block block-themed <?=($tind_daa?'':'block-opt-hidden')?>">
			<div class="block-header bg-primary">
				<ul class="block-options">

					<li>
						<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title">Asisten Anesthesi </h3>
			</div>
			<div class="block-content">
				<table id="tabel_ref_daa" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 30%;">DOKTER ANESTHESI</th>
								<th style="width: 20%;">JENIS OPERASI</th>
								<th style="width: 15%;">TOTAL ANASTHESI</th>
								<th style="width: 10%;">PESENTASE %</th>
								<th style="width: 15%;">TOTAL ASISTEN</th>

							</tr>
							<tr>
								<td>
									<span id="lbl_dokter_anasthesi"></span>
								</td>
								<td><input type="text" name="jenis_operasi_daa" id="jenis_operasi_daa" class="form-control jenis_operasi_do"  value="" readonly></td>
								<td><input type="text" name="total_a" id="total_a" class="form-control total_a number" value="0" readonly></td>
								<td><input type="text" name="persen_daa" id="persen_daa" class="form-control persen disabel"  value="<?=($persen_daa?$persen_daa:'0')?>" ></td>
								<td><input type="text" name="total_daa" id="total_daa" class="form-control number"  value="0" readonly></td>
							</tr>

						</thead>
				</table>
				<hr>
				<table id="tabel_daa" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 30%;">DOKTER / PEGAWAI ANESTHESI</th>
								<th style="width: 20%;">PERSENTASE</th>
								<th style="width: 15%;">JASA MEDIS ANESTHESI</th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>
								<td>
									<div class="input-group">
										<select name="asisten_anestesi_id" tabindex="16"  style="width: 100%" id="asisten_anestesi_id" data-placeholder="Cari Dokter" class="form-control input-sm disabel">
											<option value="#" selected>-Pilih Asisten-</option>

										</select>
										<span class="input-group-btn">
											<button tabindex="17" class="btn btn-sm btn-info disabel" type="button" id="btn_search_daa"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</td>
								<td><input type="text" name="persen_dokter_daa" id="persen_dokter_daa" class="form-control persen"  value="0" ></td>
								<td><input type="text" name="tarif_daa" id="tarif_daa" class="form-control number"  value="0" readonly></td>
								<td>
									<?php if (UserAccesForm($user_acces_form,array('966'))){ ?>
									<button type="button" class="btn btn-sm btn-primary disabel" tabindex="8" id="add_daa" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
									<?}?>
									<button type="button" class="btn btn-sm btn-danger disabel" tabindex="9" id="cancel_daa" title="Refresh"><i class="fa fa-times"></i></button>
									<input type="hidden" name="asisten_anestesi_tipe" id="asisten_anestesi_tipe" class="form-control"  value="" readonly>
									<input type="hidden" name="persen_daa_before" id="persen_daa_before" class="form-control"  value="" readonly>
								</td>
								<input type="hidden" name="nomor_daa" id="nomor_daa" class="form-control"  value="" readonly>
								<input type="hidden" name="row_daa" id="row_daa" class="form-control"  value="" readonly>
								<input type="hidden" name="auto_number_daa" id="auto_number_daa" class="form-control"  value="1" readonly>
							</tr>
						</thead>
						<tbody>
							<?
										$no=1;
									foreach ($tind_daa as $row){?>
										<tr>
										<td style='display:none'><?=$no?> </td>
										<td style='display:none'><?=$row->jenis_asisten_da?></td>
										<td style='display:none'><?=$row->id_asisten_da?></td>
										<td><?=$row->namaasisten_da?> </td>
										<td><input type="text" name="persen_daa_detail[]" class="form-control persen persen_daa disabel" value="<?=number_format($row->persen)?>"></td>
										<td><input type="text" name="tarif_daa_detail[]" class="form-control number tarif_daa_detail disabel" value="<?=$row->totalkeseluruhan?>" readonly></td>
										<td >
											<?php if (UserAccesForm($user_acces_form,array('967'))){ ?>
											<button type='button' class='btn btn-sm btn-info edit_daa disabel'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;
											<?}?>
											<?php if (UserAccesForm($user_acces_form,array('968'))){ ?>
											<button type='button' class='btn btn-sm btn-danger hapus_daa disabel'><i class='glyphicon glyphicon-remove'></i></button>
											<?}?>
										</td>
										<td style='display:none'><input type="text" name="id_daa_detail[]" value="<?=$row->id_asisten_da?>" readonly></td>
										<td style='display:none'><input type="text" name="id_nama_daa_detail[]" value="<?=$row->namaasisten_da?>" readonly></td>
										<td style='display:none'><input type="text" name="jenis_sisten_detail[]" value="<?=$row->jenis_asisten_da?>" readonly></td>
										</tr>
									<?
									$no=$no+1;
									}?>
						</tbody>
						<tfoot id="foot-total-nonracikan">
							<tr>

								<th class="hidden-phone">
									<span class="pull-right total"><b>TOTAL JASA ASISTEN ANESTHESI</b></span></th>
								<th ><b><span id="lbl_total_persen_daa" class="total_red">0</span></b></th>
								<th ><b><span id="lbl_total_daa" class="total_red">0</span></b></th>
								<th colspan="2">
										<input type="hidden" id="gt_daa" name="gt_daa" value="0">
										<input type="hidden" id="gt_persen_daa" name="gt_persen_daa" value="0">
								</th>
							</tr>
						</tfoot>
				</table>
				</div>
		</div>
		</div>
	</div>
	<div class="row">
	<div class="col-md-12">
		<div class="block block-themed <?=($tind_dao?'':'block-opt-hidden')?>">
			<div class="block-header bg-primary">
				<ul class="block-options">

					<li>
						<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title">ASISTEN OPERATOR </h3>
			</div>
			<div class="block-content">
				<table id="tabel_ref_dao" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 30%;">DOKTER OPERATOR</th>
								<th style="width: 20%;">JENIS OPERASI</th>
								<th style="width: 15%;">TOTAL OPERATOR</th>
								<th style="width: 10%;">PESENTASE %</th>
								<th style="width: 15%;">TOTAL ASISTEN OPERATOR</th>

							</tr>
							<tr>
								<td>
									<span id="lbl_dokter_operator2"></span>
								</td>
								<td><input type="text" name="jenis_operasi_dao" id="jenis_operasi_dao" class="form-control jenis_operasi_do"  value="" readonly></td>
								<td><input type="text" name="total_o2" id="total_o2" class="form-control total_o2 number" value="0" readonly></td>
								<td><input type="text" name="persen_dao" id="persen_dao" class="form-control persen disabel"  value="<?=($tind_dao?$persen_dao:'12.5')?>" ></td>
								<td><input type="text" name="total_dao" id="total_dao" class="form-control number"  value="0" readonly></td>
							</tr>

						</thead>
				</table>
				<hr>
				<table id="tabel_dao" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 30%;">ASISTEN OPERATOR</th>
								<th style="width: 20%;">PERSENTASE</th>
								<th style="width: 15%;">JASA OPERATOR</th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>
								<td>
									<div class="input-group">
										<select name="asisten_operator_id" tabindex="16"  style="width: 100%" id="asisten_operator_id" data-placeholder="Cari Dokter" class="form-control input-sm disabel">
											<option value="#" selected>-Pilih Asisten Operator-</option>

										</select>
										<span class="input-group-btn">
											<button tabindex="17" class="btn btn-sm btn-info disabel" type="button" id="btn_search_dao"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</td>
								<td><input type="text" name="persen_dokter_dao" id="persen_dokter_dao" class="form-control persen disabel"  value="0" ></td>
								<td><input type="text" name="tarif_dao" id="tarif_dao" class="form-control number"  value="0" readonly></td>
								<td>
									<?php if (UserAccesForm($user_acces_form,array('969'))){ ?>
									<button type="button" class="btn btn-sm btn-primary disabel" tabindex="8" id="add_dao" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
									<?}?>
									<button type="button" class="btn btn-sm btn-danger disabel" tabindex="9" id="cancel_dao" title="Refresh"><i class="fa fa-times"></i></button>
									<input type="hidden" name="asisten_operator_tipe" id="asisten_operator_tipe" class="form-control"  value="1" readonly>
									<input type="hidden" name="persen_dao_before" id="persen_dao_before" class="form-control"  value="" readonly>
								</td>
								<input type="hidden" name="nomor_dao" id="nomor_dao" class="form-control"  value="" readonly>
								<input type="hidden" name="row_dao" id="row_dao" class="form-control"  value="" readonly>
								<input type="hidden" name="auto_number_dao" id="auto_number_dao" class="form-control"  value="1" readonly>
							</tr>
						</thead>
						<tbody>
							<?
										$no=1;
									foreach ($tind_dao as $row){?>
										<tr>
										<td style='display:none'><?=$no?> </td>
										<td style='display:none'><?=$row->jenis_asisten_dao?></td>
										<td style='display:none'><?=$row->id_asisten_dao?></td>
										<td><?=$row->namaasisten_dao?> </td>
										<td><input type="text" name="persen_dao_detail[]" class="form-control persen persen_dokter disabel" value="<?=number_format($row->persen)?>"></td>
										<td><input type="text" name="tarif_dao_detail[]" class="form-control number tarif_dao_detail disabel" value="<?=$row->totalkeseluruhan?>" readonly></td>
										<td >
											<?php if (UserAccesForm($user_acces_form,array('970'))){ ?>
											<button type='button' class='btn btn-sm btn-info edit_dao disabel'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;
											<?}?>
											<?php if (UserAccesForm($user_acces_form,array('971'))){ ?>
											<button type='button' class='btn btn-sm btn-danger hapus_dao disabel'><i class='glyphicon glyphicon-remove'></i></button>
											<?}?>
										</td>
										<td style='display:none'><input type="text" name="id_dao_detail[]" value="<?=$row->id_asisten_dao?>" readonly></td>
										<td style='display:none'><input type="text" name="id_nama_dao_detail[]" value="<?=$row->namaasisten_dao?>" readonly></td>
										<td style='display:none'><input type="text" name="jenis_asisten_dao_detail[]" value="<?=$row->jenis_asisten_dao?>" readonly></td>
										</tr>
									<?
									$no=$no+1;
									}?>

						</tbody>
						<tfoot id="foot-total-nonracikan">
							<tr>

								<th class="hidden-phone">
									<span class="pull-right total"><b>TOTAL ASISTEN OPERATOR</b></span></th>
								<th ><b><span id="lbl_total_persen_dao" class="total_red">0</span></b></th>
								<th ><b><span id="lbl_total_dao" class="total_red">0</span></b></th>
								<th colspan="2">
										<input type="hidden" id="gt_dao" name="gt_dao" value="0">
										<input type="hidden" id="gt_persen_dao" name="gt_persen_dao" value="0">
								</th>
							</tr>
						</tfoot>
				</table>
			</div>
		</div>
		</div>
	</div>
	<div class="row">
	<div class="col-md-12">
		<div class="block block-themed <?=($tind_konsulen || $tind_instrumen || $tind_sirkuler ?'':'block-opt-hidden')?>">
			<div class="block-header bg-primary">
				<ul class="block-options">

					<li>
						<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title">Dokter Konsulen, Petugas Instrumen & Perawat Sirkuler </h3>
			</div>
			<div class="block-content">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="statusstok">Dokter Konsulen</label>
						<div class="col-md-10">
							<select class="js-select2 form-control disabel" <?=(UserAccesForm($user_acces_form,array('972'))?'':'disabled')?> id="konsulen_id" name="konsulen_id[]" style="width: 100%;" data-placeholder="Choose many.." multiple>
								<?foreach($dokterumum as $row){?>
								<option value="<?=$row->id?>" <?=(in_array($row->id, $tind_konsulen)?'selected':'')?>><?=$row->nama?></option>

								<?}?>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="statusstok">Petugas Instrumen</label>
						<div class="col-md-10">
							<select class="js-select2 form-control disabel" <?=(UserAccesForm($user_acces_form,array('973'))?'':'disabled')?> id="instrumen_id" name="instrumen_id[]" style="width: 100%;" data-placeholder="Choose many.." multiple>
								<?foreach($list_instrumen as $row){?>
								<option value="<?=$row->id?>" <?=(in_array($row->id, $tind_instrumen)?'selected':'')?>><?=$row->nama?></option>

								<?}?>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="statusstok">Perawat Sirkuler</label>
						<div class="col-md-10">
							<select class="js-select2 form-control disabel" <?=(UserAccesForm($user_acces_form,array('974'))?'':'disabled')?>  id="sirkuler_id" name="sirkuler_id[]" style="width: 100%;" data-placeholder="Choose many.." multiple>
								<?foreach($list_instrumen as $row){?>
								<option value="<?=$row->id?>" <?=(in_array($row->id, $tind_sirkuler)?'selected':'')?>><?=$row->nama?></option>

								<?}?>
							</select>
						</div>
					</div>

				</div>

			</div>
		</div>
		</div>
	</div>
	<div class="row">
	<div class="col-md-12">
		<div class="block block-themed <?=($tind_narcose?'':'block-opt-hidden')?>">
			<div class="block-header bg-primary">
				<ul class="block-options">

					<li>
						<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title">PENGGUNAAN NARCOSE</h3>
			</div>
			<div class="block-content">
				<table id="tabel_narcose" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 15%;">UNIT PELAYANAN</th>
								<th style="width: 20%;">NAMA OBAT</th>
								<th style="width: 10%;">SATUAN</th>
								<th style="width: 10%;">HARGA</th>
								<th style="width: 10%;">QTY</th>
								<th style="width: 10%;">TOTAL</th>
								<th style="width: 15%;">USER & DATE</th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>
								<td>
									<select name="idunit_narcose" id="idunit_narcose" tabindex="16"  style="width: 100%"  data-placeholder="Cari Dokter" class="js-select2 form-control input-sm disable_narcose disabel">
										<option value="#" selected>-Pilih Unit Pelayanan-</option>
										<?foreach ($list_unit as $row){?>
											<option value="<?=$row->id?>" <?=($unitpelayananiddefault==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>

								</td>
								<td>
									<div class="input-group">
										<select name="id_narcose" id="id_narcose" tabindex="16"  style="width: 100%"  data-placeholder="Cari Obat Narcose" class="form-control input-sm disable_narcose disabel">
											<option value="#" selected>-Pilih Obat Narcose-</option>

										</select>
										<span class="input-group-btn">
											<button data-toggle="modal" tabindex="17" class="btn btn-sm btn-info disable_narcose disabel" type="button" id="btn_search_narcose"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</td>
								<td><input type="text" name="satuan_narcose" id="satuan_narcose" class="form-control" value="" readonly></td>
								<td><input type="text" name="harga_narcose" id="harga_narcose" class="form-control number" value="" readonly></td>
								<td><input type="text" name="kuantitas_narcose" id="kuantitas_narcose" class="form-control number disabel" value=""></td>
								<td><input type="text" name="total_narcose" id="total_narcose" class="form-control number" value="" readonly></td>
								<td><input type="text" name="user_narcose" id="user_narcose" class="form-control" value="<?=$this->session->userdata('user_name').' - '.date('d-m-Y H:i:s')?>" readonly></td>
								<td>
									<?php if (UserAccesForm($user_acces_form,array('975'))){ ?>
									<button type="button" class="btn btn-sm btn-primary disabel" tabindex="8" id="add_narcose" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
									<?}?>
								<button type="button" class="btn btn-sm btn-danger disabel" tabindex="9" id="cancel_narcose" title="Refresh"><i class="fa fa-times"></i></button>

								</td>
								<input type="hidden" name="idtipe_narcose" id="idtipe_narcose" class="form-control"  value="" readonly>
								<input type="hidden" name="harga_dasar_narcose" id="harga_dasar_narcose" class="form-control"  value="" readonly>
								<input type="hidden" name="margin_narcose" id="margin_narcose" class="form-control"  value="" readonly>
								<input type="hidden" name="nomor_narcose" id="nomor_narcose" class="form-control"  value="" readonly>
								<input type="hidden" name="row_narcose" id="row_narcose" class="form-control"  value="" readonly>
								<input type="hidden" name="auto_number_narcose" id="auto_number_narcose" class="form-control"  value="1" readonly>
								<input type="hidden" name="id_trx_narcose" id="id_trx_narcose" class="form-control"  value="0" readonly>

							</tr>
						</thead>
						<tbody>
							<?
							$no=1;
							foreach ($tind_narcose as $row){?>
								<tr>
								<td style='display:none'><?=$no?> </td>
								<td style='display:none'><?=$row->idunit?></td>
								<td style='display:none'><?=$row->idobat?></td>
								<td><?=$row->nama_unit?> </td>
								<td><?=$row->nama?> </td>
								<td><?=$row->satuan?> </td>
								<td align='right'><?= number_format($row->hargajual)?> </td>
								<td align='right'><?= number_format($row->kuantitas)?> </td>
								<td align='right'><?= number_format($row->totalkeseluruhan)?> </td>
								<td><?= $row->nama_user_transaksi.' - '.HumanDateLong($row->tanggal_transaksi)?> </td>
								<td style='display:none'></td>
								<td style='display:none'></td>
								<td style='display:none'></td>
								<td >
									<?php if (UserAccesForm($user_acces_form,array('976'))){ ?>
									<button type='button' class='btn btn-sm btn-info edit_narcose disabel'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;
									<?}?>
									<?php if (UserAccesForm($user_acces_form,array('977'))){ ?>
									<button type='button' class='btn btn-sm btn-danger hapus_narcose disabel'><i class='glyphicon glyphicon-remove'></i></button>
									<?}?>

								</td>
								<td style='display:none'><input type="text" name="idunit_narcose_detail[]" value="<?=$row->idunit?>" readonly></td>
								<td style='display:none'><input type="text" name="id_narcose_detail[]" value="<?=$row->idobat?>" readonly></td>
								<td style='display:none'><input type="text" name="harga_narcose_detail[]" value="<?=$row->hargajual?>" readonly></td>
								<td style='display:none'><input type="text" name="kuantitas_narcose_detail[]" value="<?=$row->kuantitas?>" readonly></td>
								<td style='display:none'><input type="text" name="total_narcose_detail[]" value="<?=$row->totalkeseluruhan?>" readonly></td>
								<td style='display:none'><input type="text" name="harga_dasar_narcose_detail[]" value="<?=$row->hargadasar?>" readonly></td>
								<td style='display:none'><input type="text" name="margin_narcose_detail[]" value="<?=$row->margin?>" readonly></td>
								<td style='display:none'><input type="text" name="idtipe_narcose_detail[]" value="<?=$row->idtipe?>" readonly></td>
								<td style='display:none'><input type="text" name="id_trx_narcose_detail[]" value="<?=$row->id?>" readonly></td>
								<td style='display:none'><input type="text" name="st_edit_narcose[]" value="0" readonly></td>
								</tr>
							<?
									$no=$no+1;
									}?>
						</tbody>
						<tfoot id="foot-total-nonracikan">
							<tr>
								<th colspan="5" class="hidden-phone">
									<span class="pull-right total"><b>TOTAL NARCOSE</b></span></th>
								<th align="right"><b><span id="lbl_gt_narcose" class="total pull-right"><?=number_format($total_narcose)?></span></b></th>
								<th colspan="2"><input type="hidden" id="gt_narcose" name="gt_narcose" value="<?=$total_narcose?>"> </th>
							</tr>
						</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
	<div class="row">
	<div class="col-md-12">
		<div class="block block-themed <?=($tind_obat?'':'block-opt-hidden')?>">
			<div class="block-header bg-primary">
				<ul class="block-options">

					<li>
						<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title">PENGGUNAAN OBAT</h3>
			</div>
			<div class="block-content">
				<table id="tabel_obat" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 15%;">UNIT PELAYANAN</th>
								<th style="width: 20%;">NAMA OBAT</th>
								<th style="width: 10%;">SATUAN</th>
								<th style="width: 10%;">HARGA</th>
								<th style="width: 10%;">QTY</th>
								<th style="width: 10%;">TOTAL</th>
								<th style="width: 15%;">USER & DATE</th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>
								<td>
									<select name="idunit_obat" id="idunit_obat" tabindex="16"  style="width: 100%"  data-placeholder="Cari Dokter" class="js-select2 form-control input-sm disable_obat disabel">
										<option value="#" selected>-Pilih Unit Pelayanan-</option>
										<?foreach ($list_unit as $row){?>
											<option value="<?=$row->id?>" <?=($unitpelayananiddefault==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>

								</td>
								<td>
									<div class="input-group">
										<select name="id_obat" id="id_obat" tabindex="16"  style="width: 100%"  data-placeholder="Cari Obat" class="form-control  disable_obat input-sm disabel">
											<option value="#" selected>-Pilih Obat-</option>

										</select>
										<span class="input-group-btn">
											<button data-toggle="modal" tabindex="17" class="btn btn-sm btn-info  disable_obat disabel" type="button" id="btn_search_obat"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</td>
								<td><input type="text" name="satuan_obat" id="satuan_obat" class="form-control" value="" readonly></td>
								<td><input type="text" name="harga_obat" id="harga_obat" class="form-control number" value="" readonly></td>
								<td><input type="text" name="kuantitas_obat" id="kuantitas_obat" class="form-control number disabel" value=""></td>
								<td><input type="text" name="total_obat" id="total_obat" class="form-control number" value="" readonly></td>
								<td><input type="text" name="user_obat" id="user_obat" class="form-control" value="<?=$this->session->userdata('user_name').' - '.date('d-m-Y H:i:s')?>" readonly></td>
								<td>
									<?php if (UserAccesForm($user_acces_form,array('978'))){ ?>
									<button type="button" class="btn btn-sm btn-primary disabel" tabindex="8" id="add_obat" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
									<?}?>
									<button type="button" class="btn btn-sm btn-danger disabel" tabindex="9" id="cancel_obat" title="Refresh"><i class="fa fa-times"></i></button>
								</td>
								<input type="hidden" name="idtipe_obat" id="idtipe_obat" class="form-control"  value="" readonly>
								<input type="hidden" name="harga_dasar_obat" id="harga_dasar_obat" class="form-control"  value="" readonly>
								<input type="hidden" name="margin_obat" id="margin_obat" class="form-control"  value="" readonly>
								<input type="hidden" name="nomor_obat" id="nomor_obat" class="form-control"  value="" readonly>
								<input type="hidden" name="row_obat" id="row_obat" class="form-control"  value="" readonly>
								<input type="hidden" name="auto_number_obat" id="auto_number_obat" class="form-control"  value="1" readonly>
								<input type="hidden" name="id_trx_obat" id="id_trx_obat" class="form-control"  value="0" readonly>
							</tr>
						</thead>
						<tbody>
							<?
							$no=1;
							foreach ($tind_obat as $row){?>
								<tr>
								<td style='display:none'><?=$no?> </td>
								<td style='display:none'><?=$row->idunit?></td>
								<td style='display:none'><?=$row->idobat?></td>
								<td><?=$row->nama_unit?> </td>
								<td><?=$row->nama?> </td>
								<td><?=$row->satuan?> </td>
								<td align='right'><?= number_format($row->hargajual)?> </td>
								<td align='right'><?= number_format($row->kuantitas)?> </td>
								<td align='right'><?= number_format($row->totalkeseluruhan)?> </td>
								<td><?= $row->nama_user_transaksi.' - '.HumanDateLong($row->tanggal_transaksi)?> </td>
								<td style='display:none'></td>
								<td style='display:none'></td>
								<td style='display:none'></td>
								<td >
									<?php if (UserAccesForm($user_acces_form,array('979'))){ ?>
									<button type='button' class='btn btn-sm btn-info edit_obat disabel'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;
									<?}?>
									<?php if (UserAccesForm($user_acces_form,array('980'))){ ?>
									<button type='button' class='btn btn-sm btn-danger hapus_obat disabel'><i class='glyphicon glyphicon-remove'></i></button>
									<?}?>
								</td>
								<td style='display:none'><input type="text" name="idunit_obat_detail[]" value="<?=$row->idunit?>" readonly></td>
								<td style='display:none'><input type="text" name="id_obat_detail[]" value="<?=$row->idobat?>" readonly></td>
								<td style='display:none'><input type="text" name="harga_obat_detail[]" value="<?=$row->hargajual?>" readonly></td>
								<td style='display:none'><input type="text" name="kuantitas_obat_detail[]" value="<?=$row->kuantitas?>" readonly></td>
								<td style='display:none'><input type="text" name="total_obat_detail[]" value="<?=$row->totalkeseluruhan?>" readonly></td>
								<td style='display:none'><input type="text" name="harga_dasar_obat_detail[]" value="<?=$row->hargadasar?>" readonly></td>
								<td style='display:none'><input type="text" name="margin_obat_detail[]" value="<?=$row->margin?>" readonly></td>
								<td style='display:none'><input type="text" name="idtipe_obat_detail[]" value="<?=$row->idtipe?>" readonly></td>
								<td style='display:none'><input type="text" name="id_trx_obat_detail[]" value="<?=$row->id?>" readonly></td>
								<td style='display:none'><input type="text" name="st_edit_obat[]" value="0" readonly></td>
								</tr>
							<?
									$no=$no+1;
									}?>
						</tbody>
						<tfoot id="foot-total-nonracikan">
							<tr>
								<th colspan="5" class="hidden-phone">
									<span class="pull-right total"><b>TOTAL OBAT</b></span></th>
								<th align="right"><b><span id="lbl_gt_obat" class="total pull-right"><?=number_format($total_obat)?></span></b></th>
								<th colspan="2"><input type="hidden" id="gt_obat" name="gt_obat" value="<?=$total_obat?>"> </th>
							</tr>
						</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="block block-themed <?=($tind_alkes?'':'block-opt-hidden')?>">
			<div class="block-header bg-primary">
				<ul class="block-options">

					<li>
						<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title">PENGGUNAAN ALKES</h3>
			</div>
			<div class="block-content">
				<table id="tabel_alkes" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 15%;">UNIT PELAYANAN</th>
								<th style="width: 20%;">NAMA ALKES</th>
								<th style="width: 10%;">SATUAN</th>
								<th style="width: 10%;">HARGA</th>
								<th style="width: 10%;">QTY</th>
								<th style="width: 10%;">TOTAL</th>
								<th style="width: 15%;">USER & DATE</th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>
								<td>
									<select name="idunit_alkes" id="idunit_alkes" tabindex="16"  style="width: 100%"  data-placeholder="Cari Dokter" class="js-select2 form-control input-sm  disable_alkes disabel">
										<option value="#" selected>-Pilih Unit Pelayanan-</option>
										<?foreach ($list_unit as $row){?>
											<option value="<?=$row->id?>" <?=($unitpelayananiddefault==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>

								</td>
								<td>
									<div class="input-group">
										<select name="id_alkes" id="id_alkes" tabindex="16"  style="width: 100%"  data-placeholder="Cari Alkes" class="form-control input-sm disable_alkes disabel">
											<option value="#" selected>-Pilih Alkes-</option>

										</select>
										<span class="input-group-btn">
											<button data-toggle="modal" tabindex="17" class="btn btn-sm btn-info disable_alkes disabel" type="button" id="btn_search_alkes"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</td>
								<td><input type="text" name="satuan_alkes" id="satuan_alkes" class="form-control" value="" readonly></td>
								<td><input type="text" name="harga_alkes" id="harga_alkes" class="form-control number" value="" readonly></td>
								<td><input type="text" name="kuantitas_alkes" id="kuantitas_alkes" class="form-control number disabel" value=""></td>
								<td><input type="text" name="total_alkes" id="total_alkes" class="form-control number" value="" readonly></td>
								<td><input type="text" name="user_alkes" id="user_alkes" class="form-control" value="<?=$this->session->userdata('user_name').' - '.date('d-m-Y H:i:s')?>" readonly></td>
								<td>
									<?php if (UserAccesForm($user_acces_form,array('981'))){ ?>
									<button type="button" class="btn btn-sm btn-primary disabel" tabindex="8" id="add_alkes" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
									<?}?>
									<button type="button" class="btn btn-sm btn-danger disabel" tabindex="9" id="cancel_alkes" title="Refresh"><i class="fa fa-times"></i></button>
								</td>
								<input type="hidden" name="idtipe_alkes" id="idtipe_alkes" class="form-control"  value="" readonly>
								<input type="hidden" name="harga_dasar_alkes" id="harga_dasar_alkes" class="form-control"  value="" readonly>
								<input type="hidden" name="margin_alkes" id="margin_alkes" class="form-control"  value="" readonly>
								<input type="hidden" name="nomor_alkes" id="nomor_alkes" class="form-control"  value="" readonly>
								<input type="hidden" name="row_alkes" id="row_alkes" class="form-control"  value="" readonly>
								<input type="hidden" name="auto_number_alkes" id="auto_number_alkes" class="form-control"  value="1" readonly>
								<input type="hidden" name="id_trx_alkes" id="id_trx_alkes" class="form-control"  value="0" readonly>
							</tr>
						</thead>
						<tbody>
							<?
							$no=1;
							foreach ($tind_alkes as $row){?>
								<tr>
								<td style='display:none'><?=$no?> </td>
								<td style='display:none'><?=$row->idunit?></td>
								<td style='display:none'><?=$row->idobat?></td>
								<td><?=$row->nama_unit?> </td>
								<td><?=$row->nama?> </td>
								<td><?=$row->satuan?> </td>
								<td align='right'><?= number_format($row->hargajual)?> </td>
								<td align='right'><?= number_format($row->kuantitas)?> </td>
								<td align='right'><?= number_format($row->totalkeseluruhan)?> </td>
								<td><?= $row->nama_user_transaksi.' - '.HumanDateLong($row->tanggal_transaksi)?> </td>
								<td style='display:none'></td>
								<td style='display:none'></td>
								<td style='display:none'></td>
								<td >
									<?php if (UserAccesForm($user_acces_form,array('982'))){ ?>
									<button type='button' class='btn btn-sm btn-info edit_alkes disabel'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;
									<?}?>
									<?php if (UserAccesForm($user_acces_form,array('983'))){ ?>
									<button type='button' class='btn btn-sm btn-danger hapus_alkes disabel'><i class='glyphicon glyphicon-remove'></i></button>
									<?}?>
								</td>
								<td style='display:none'><input type="text" name="idunit_alkes_detail[]" value="<?=$row->idunit?>" readonly></td>
								<td style='display:none'><input type="text" name="id_alkes_detail[]" value="<?=$row->idobat?>" readonly></td>
								<td style='display:none'><input type="text" name="harga_alkes_detail[]" value="<?=$row->hargajual?>" readonly></td>
								<td style='display:none'><input type="text" name="kuantitas_alkes_detail[]" value="<?=$row->kuantitas?>" readonly></td>
								<td style='display:none'><input type="text" name="total_alkes_detail[]" value="<?=$row->totalkeseluruhan?>" readonly></td>
								<td style='display:none'><input type="text" name="harga_dasar_alkes_detail[]" value="<?=$row->hargadasar?>" readonly></td>
								<td style='display:none'><input type="text" name="margin_alkes_detail[]" value="<?=$row->margin?>" readonly></td>
								<td style='display:none'><input type="text" name="idtipe_alkes_detail[]" value="<?=$row->idtipe?>" readonly></td>
								<td style='display:none'><input type="hidden" name="id_trx_alkes_detail[]" value="<?=$row->id?>" readonly></td>
								<td style='display:none'><input type="hidden" name="st_edit_alkes[]" value="0" readonly></td>
								</tr>
							<?
							$no=$no+1;
							}?>
						</tbody>
						<tfoot id="foot-total-nonracikan">
							<tr>
								<th colspan="5" class="hidden-phone">
									<span class="pull-right total"><b>TOTAL ALKES</b></span></th>
								<th align="right"><b><span id="lbl_gt_alkes" class="total pull-right"><?=number_format($total_alkes)?></span></b></th>
								<th colspan="2"><input type="hidden" id="gt_alkes" name="gt_alkes" value="<?=$total_alkes?>"> </th>
							</tr>
						</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="block block-themed <?=($tind_implan?'':'block-opt-hidden')?>">
			<div class="block-header bg-primary">
				<ul class="block-options">

					<li>
						<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title">PENGGUNAAN IMPLAN</h3>
			</div>
			<div class="block-content">
				<table id="tabel_implan" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 15%;">UNIT PELAYANAN</th>
								<th style="width: 20%;">NAMA IMPLAN</th>
								<th style="width: 10%;">SATUAN</th>
								<th style="width: 10%;">HARGA</th>
								<th style="width: 10%;">QTY</th>
								<th style="width: 10%;">TOTAL</th>
								<th style="width: 15%;">USER & DATE</th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>
								<td>
									<select name="idunit_implan" id="idunit_implan" tabindex="16"  style="width: 100%"  data-placeholder="Cari Dokter" class="js-select2 form-control input-sm disable_implan disabel">
										<option value="#" selected>-Pilih Unit Pelayanan-</option>
										<?foreach ($list_unit as $row){?>
											<option value="<?=$row->id?>" <?=($unitpelayananiddefault==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>

								</td>
								<td>
									<div class="input-group">
										<select name="id_implan" id="id_implan" tabindex="16"  style="width: 100%"  data-placeholder="Cari Implan" class="form-control input-sm disable_implan disabel">
											<option value="#" selected>-Pilih Implan-</option>

										</select>
										<span class="input-group-btn">
											<button data-toggle="modal" tabindex="17" class="btn btn-sm btn-info disable_implan disabel" type="button" id="btn_search_implan"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</td>
								<td><input type="text" name="satuan_implan" id="satuan_implan" class="form-control" value="" readonly></td>
								<td><input type="text" name="harga_implan" id="harga_implan" class="form-control number" value="" readonly></td>
								<td><input type="text" name="kuantitas_implan" id="kuantitas_implan" class="form-control number disabel" value=""></td>
								<td><input type="text" name="total_implan" id="total_implan" class="form-control number" value="" readonly></td>
								<td><input type="text" name="user_implan" id="user_implan" class="form-control" value="<?=$this->session->userdata('user_name').' - '.date('d-m-Y H:i:s')?>" readonly></td>
								<td>
									<?php if (UserAccesForm($user_acces_form,array('984'))){ ?>
									<button type="button" class="btn btn-sm btn-primary disabel" tabindex="8" id="add_implan" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
									<?}?>
									<button type="button" class="btn btn-sm btn-danger disabel" tabindex="9" id="cancel_implan" title="Refresh"><i class="fa fa-times"></i></button>
								</td>
								<input type="hidden" name="idtipe_implan" id="idtipe_implan" class="form-control"  value="" readonly>
								<input type="hidden" name="harga_dasar_implan" id="harga_dasar_implan" class="form-control"  value="" readonly>
								<input type="hidden" name="margin_implan" id="margin_implan" class="form-control"  value="" readonly>
								<input type="hidden" name="nomor_implan" id="nomor_implan" class="form-control"  value="" readonly>
								<input type="hidden" name="row_implan" id="row_implan" class="form-control"  value="" readonly>
								<input type="hidden" name="auto_number_implan" id="auto_number_implan" class="form-control"  value="1" readonly>
								<input type="hidden" name="id_trx_implan" id="id_trx_implan" class="form-control"  value="0" readonly>
							</tr>
						</thead>
						<tbody>
						<?
						$no=1;
						foreach ($tind_implan as $row){?>
							<tr>
							<td style='display:none'><?=$no?> </td>
							<td style='display:none'><?=$row->idunit?></td>
							<td style='display:none'><?=$row->idobat?></td>
							<td><?=$row->nama_unit?> </td>
							<td><?=$row->nama?> </td>
							<td><?=$row->satuan?> </td>
							<td align='right'><?= number_format($row->hargajual)?> </td>
							<td align='right'><?= number_format($row->kuantitas)?> </td>
							<td align='right'><?= number_format($row->totalkeseluruhan)?> </td>
							<td><?= $row->nama_user_transaksi.' - '.DMYFormat2($row->tanggal_transaksi)?> </td>
							<td style='display:none'></td>
							<td style='display:none'></td>
							<td style='display:none'></td>
							<td >
								<?php if (UserAccesForm($user_acces_form,array('985'))){ ?>
								<button type='button' class='btn btn-sm btn-info edit_implan disabel'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;
								<?}?>
								<?php if (UserAccesForm($user_acces_form,array('986'))){ ?>
								<button type='button' class='btn btn-sm btn-danger hapus_implan disabel'><i class='glyphicon glyphicon-remove'></i></button>
								<?}?>
							</td>
							<td style='display:none'><input type="text" name="idunit_implan_detail[]" value="<?=$row->idunit?>" readonly></td>
							<td style='display:none'><input type="text" name="id_implan_detail[]" value="<?=$row->idobat?>" readonly></td>
							<td style='display:none'><input type="text" name="harga_implan_detail[]" value="<?=$row->hargajual?>" readonly></td>
							<td style='display:none'><input type="text" name="kuantitas_implan_detail[]" value="<?=$row->kuantitas?>" readonly></td>
							<td style='display:none'><input type="text" name="total_implan_detail[]" value="<?=$row->totalkeseluruhan?>" readonly></td>
							<td style='display:none'><input type="text" name="harga_dasar_implan_detail[]" value="<?=$row->hargadasar?>" readonly></td>
							<td style='display:none'><input type="text" name="margin_implan_detail[]" value="<?=$row->margin?>" readonly></td>
							<td style='display:none'><input type="text" name="idtipe_implan_detail[]" value="<?=$row->idtipe?>" readonly></td>
							<td style='display:none'><input type="text" name="id_trx_implan_detail[]" value="<?=$row->id?>" readonly></td>
							<td style='display:none'><input type="text" name="st_edit_implan[]" value="0" readonly></td>
							</tr>
						<?
						$no=$no+1;
						}?>
						</tbody>
						<tfoot id="foot-total-nonracikan">
							<tr>
								<th colspan="5" class="hidden-phone">
									<span class="pull-right total"><b>TOTAL IMPLAN</b></span></th>
								<th align="right"><b><span id="lbl_gt_implan" class="total pull-right"><?=number_format($total_implan)?></span></b></th>
								<th colspan="2"><input type="hidden" id="gt_implan" name="gt_implan" value="<?=$total_implan?>"> </th>
							</tr>
						</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="block block-themed <?=($tind_sewa?'':'block-opt-hidden')?>">
			<div class="block-header bg-primary">
				<ul class="block-options">

					<li>
						<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title">PENGGUNAAN SEWA ALAT</h3>
			</div>
			<div class="block-content">
				<table id="tabel_alat" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 25%;">NAMA ALAT</th>
								<th style="width: 10%;">JENIS OPERASI</th>
								<th style="width: 7%;">KELAS</th>
								<th style="width: 10%;">TARIF</th>
								<th style="width: 7%;">KUANTITAS</th>
								<th style="width: 10%;">SUB TOTAL</th>
								<th style="width: 15%;">USER</th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>
								<td>
									<div id="" class="input-group">
										<select name="id_alat" tabindex="16"  style="width: 100%" id="id_alat" data-placeholder="Cari Dokter" class="js-select2 form-control input-sm disabel">
											<option value="#" selected>-Pilih Alat-</option>
											<?foreach ($list_alat as $row){?>
												<option value="<?=$row->id?>"><?=TreeView($row->level, $row->nama)?></option>
											<?}?>


										</select>
										<span class="input-group-btn">
											<button data-toggle="modal" tabindex="17" class="btn btn-sm btn-info disabel" type="button" id="btn_search_alat"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</td>
								<td><input type="text" name="jenis_operasi_alat" id="jenis_operasi_alat" class="form-control jenis_operasi_do"  value="" readonly></td>
								<td><input type="text" name="kelas_operasi_alat" id="kelas_operasi_alat" class="form-control kelas_operasi_alat"  value="" readonly></td>
								<td><input type="text" name="tarif_alat" id="tarif_alat" class="form-control number disabel"  value="" readonly></td>
								<td><input type="text" name="kuantitas_alat" id="kuantitas_alat" class="form-control number disabel" value=""></td>
								<td><input type="text" name="total_alat" id="total_alat" class="form-control number"  value="" readonly></td>
								<td><input type="text" name="user_alat" id="user_alat" class="form-control" value="<?=$this->session->userdata('user_name').' - '.date('d-m-Y H:i:s')?>" readonly></td>
								<td>
									<?php if (UserAccesForm($user_acces_form,array('987'))){ ?>
									<button type="button" class="btn btn-sm btn-primary disabel" tabindex="8" id="add_alat" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
									<?}?>
									<button type="button" class="btn btn-sm btn-danger disabel" tabindex="9" id="cancel_alat" title="Refresh"><i class="fa fa-times"></i></button>
								</td>
								<input type="hidden" name="sarana_alat" id="sarana_alat" class="form-control sarana_alat"  value="" readonly>
								<input type="hidden" name="pelayanan_alat" id="pelayanan_alat" class="form-control pelayanan_alat"  value="" readonly>
								<input type="hidden" name="bhp_alat" id="bhp_alat" class="form-control bhp_alat"  value="" readonly>
								<input type="hidden" name="perawatan_alat" id="perawatan_alat" class="form-control perawatan_alat"  value="" readonly>
								<input type="hidden" name="nomor_alat" id="nomor_alat" class="form-control"  value="" readonly>
								<input type="hidden" name="row_alat" id="row_alat" class="form-control"  value="" readonly>
								<input type="hidden" name="id_alat_trx" id="id_alat_trx" class="form-control"  value="0" readonly>
								<input type="hidden" name="auto_number_alat" id="auto_number_alat" class="form-control"  value="1" readonly>
							</tr>
						</thead>
						<tbody>
							<?
						$no=1;
						foreach ($tind_sewa as $row){?>
							<tr>
							<td style='display:none'><?=$no?> </td>
							<td style='display:none'><?=$row->idalat?></td>
							<td><?=$row->nama_alat?> </td>
							<td><input type="text" name="jenis_operasi_alat_detail[]" class="form-control jenis_operasi_do"  value="" readonly></td>
							<td><input type="text" name="kelas_operasi_alat_detail[]" class="form-control kelas_operasi_alat"  value="" readonly></td>
							<td><input type="text" name="tarif_alat_detail[]"  class="form-control number"  value="<?=$row->total?>" readonly></td>
							<td><input type="text" name="kuantitas_alat_detail[]"  class="form-control number"  value="<?=$row->kuantitas?>" readonly></td>
							<td><input type="text" name="total_alat_detail[]"  class="form-control number"  value="<?=$row->totalkeseluruhan?>" readonly></td>
							<td><?= $row->nama_user_transaksi.' - '.HumanDateLong($row->tanggal_transaksi)?> </td>
							<td >
								<?php if (UserAccesForm($user_acces_form,array('988'))){ ?>
								<button type='button' class='btn btn-sm btn-info edit_alat disabel'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;
								<?}?>
								<?php if (UserAccesForm($user_acces_form,array('989'))){ ?>
								<button type='button' class='btn btn-sm btn-danger hapus_alat disabel'><i class='glyphicon glyphicon-remove'></i></button>
								<?}?>
							</td>
							<td style='display:none'><input type="text" name="sarana_alat_detail[]" value="<?=$row->jasasarana?>" readonly></td>
							<td style='display:none'><input type="text" name="pelayanan_alat_detail[]" value="<?=$row->jasapelayanan?>" readonly></td>
							<td style='display:none'><input type="text" name="bhp_alat_detail[]" value="<?=$row->bhp?>" readonly></td>
							<td style='display:none'><input type="text" name="biayaperawatan_alat_detail[]" value="<?=$row->biayaperawatan?>" readonly></td>
							<td style='display:none'><input type="text" name="id_alat_detail[]" value="<?=$row->idalat?>" readonly></td>
							<td style='display:none'><input type="text" name="id_alat_trx_detail[]" value="<?=$row->id?>" readonly></td>
							<td style='display:none'><input type="text" name="st_edit_alat[]" value="0" readonly></td>
							</tr>
						<?
						$no=$no+1;
						}?>
						</tbody>
						<tfoot id="foot-total-nonracikan">
							<tr>

								<th colspan="5" class="hidden-phone">
									<span class="pull-right total"><b>TOTAL SEWA ALAT</b></span></th>
								<th ><b><span id="lbl_gt_alat" class="total pull-right"><?=number_format($total_sewa)?></span></b></th>

								<th colspan="2"><input type="hidden" id="gt_alat" name="gt_alat" value="<?=$total_sewa?>"> </th>
							</tr>
						</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
<table id="tabel_delete" class="table table-striped table-bordered" style="margin-bottom: 0;" hidden>
		<thead>
			<tr>
				<th style="width: 10%;">JENIS</th><th style="width: 10%;">IDTRX</th>
			</tr>
		</thead>
		<tbody></tbody>
</table>
<hr>
<div id="cover-spin"></div>
<div id="loader" hidden></div>
<div class="row" style="padding-bottom:20px;margin-top:15px;">
	<div class="col-md-12">
		<div class="col-md-5">

		</div>
		<?php if (UserAccesForm($user_acces_form,array('990'))){ ?>
		<div class="col-md-7">
			<button class="btn btn-sm btn-kasir btn-success text-uppercase disabel" type="submit" id="simpan_tko" style="float:right;padding: 6px 0;margin-top:-5px;width:275px;font-size:11px;" data-toggle="modal" data-target="#simpanoo"><i class="fa fa-check"></i> Simpan</button>
		</div>
		<?}?>
	</div>
</div>
<?php echo form_close() ?>
<div class="modal fade in black-overlay" id="modal_barang" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:60%; margin-top:100px">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Obat</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter_obat','class="form-horizontal" id="form-work"') ?>
						<div class="row">
							<div class="col-md-8">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="tipeid2">Unit Pelayanan</label>
									<div class="col-md-8">
										<select name="idunit_pilih" id="idunit_pilih" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
											<?foreach ($list_unit as $row){?>
												<option value="<?=$row->id?>" <?=($unitpelayananiddefault==$row->id?'selected':'')?>><?=$row->nama?></option>
											<?}?>
										</select>
									</div>
								</div>
							</div>
							<input type="hidden" name="idtipe_pilih" id="idtipe_pilih" class="form-control"  value="" readonly>
							<input type="hidden" name="form_pilih" id="form_pilih" class="form-control"  value="" readonly>
							<input type="hidden" name="idkat2" id="idkat2" class="form-control"  value="" readonly>

						</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content">

						<table width="100%" id="tabel_cari_barang" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>#</th>
									<th>Kode Obat</th>
									<th>Nama Obat</th>
									<th>Satuan</th>
									<th>Stok</th>
									<th>Catatan</th>
									<th>Kartu Stok</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
				</div>

			</div>
		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_adjust" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:30%; margin-top:100px">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Ganti Tarif</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter_obat','class="form-horizontal" id="form-sheet"') ?>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="tipeid2">Jenis Operasi</label>
									<div class="col-md-8">
										<select name="jenis_operasi_id2" id="jenis_operasi_id2" class="js-select2 form-control  disabel" style="width: 100%;">
											<option value="0">Pilih Jenis Operasi</option>
											<?foreach ($mjenis_operasi as $row){?>
												<option value="<?=$row->id?>" <?=($jenis_operasi_id==$row->id)?'selected':''?>><?=$row->nama?></option>
											<?}?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="tipeid2">Kelas Tarif</label>
									<div class="col-md-8">
										<select name="kelas_tarif_id2"  id="kelas_tarif_id2" class="js-select2 form-control   disabel" style="width: 100%;">
											<option value="0">Pilih Kelas Tarif</option>
											<?foreach ($mkelas as $row){?>
												<option value="<?=$row->id?>"  <?=($kelas_tarif_id==$row->id)?'selected':''?>><?=$row->nama?></option>
											<?}?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="tipeid2">Tarif</label>
									<div class="col-md-8">
										<select name="id_tarif_edit"  id="id_tarif_edit" class="js-select2 form-control   disabel" style="width: 100%;">

										</select>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="tipeid2">Nominal</label>
									<div class="col-md-8">
										<input type="text" name="nominal_tarif_edit" id="nominal_tarif_edit" class="form-control number" value="0" readonly="">
									</div>
								</div>
							</div>
							<input type="hidden" name="bhp_edit" id="bhp_edit" class="form-control"  value="" readonly>
							<input type="hidden" name="biayaperawatan_edit" id="biayaperawatan_edit" class="form-control"  value="" readonly>
							<input type="hidden" name="jasapelayanan_edit" id="jasapelayanan_edit" class="form-control"  value="" readonly>
							<input type="hidden" name="jasasarana_edit" id="jasasarana_edit" class="form-control"  value="" readonly>
							<input type="hidden" name="total_edit" id="total_edit" class="form-control"  value="" readonly>

						</div>
						<hr>
						<div class="text-right bg-light lter">
							<button class="btn btn-info" type="button" id="btn_edit_tarif" name="btn_edit_tarif">Update</button>
							<button class="btn btn-default" type="button" id="btn_cancel_tarif" name="btn_cancel_tarif">Batal</button>
						</div>
						<br>
					<?php echo form_close() ?>
				</div>


			</div>
		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_sewa" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:60%; margin-top:100px">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Sewa Alat</h3>
				</div>

				<div class="block-content">

						<table width="100%" id="tabel_cari_sewa" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Kode</th>
									<th>#</th>
									<th>Nama</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
				</div>

			</div>
		</div>
	</div>
</div>


	</div>
</div>
<?}?>
<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#dokter_operator_id').select2();
		$("#kelompok_diagnosa_id,#kelompok_tindakan_id").select2({
			tag: true
		  });
		if ($("#st_lock").val()=='1'){
			$(".disabel").attr('disabled',true);
			$(".dis2").attr('disabled',true);
		}
		$('.number').number(true, 0, '.', ',');
		$('.persen').number(true, 2, '.', ',');
		$(".jam").datetimepicker({
			format: "HH:mm",
			stepping: 30
		});
		load_tarif_do();
		load_dokter_anestesi();
		load_asisten_anestesi();
		load_asisten_operator();
		load_tarif_ruangan();
		load_tarif_fullcare();
		// load_kelompok_diagnosa();
		// load_kelompok_tindakan();
		// js_obat();
	});
	$("#kelompok_diagnosa_id,#kelompok_tindakan_id").on("select2:select", function (evt) {
	    var element = evt.params.data.element;
	    var $element = $(element);

	    $element.detach();
	    $(this).append($element);
	    $(this).trigger("change");
	  });
	function formatNumber (num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}
	$(document).on("change", ".ubahdata", function() {
		load_tarif_ruangan();
		load_tarif_fullcare();
		load_tarif_do();
		load_tarif_sewa();
		update_detail_alat();
	});

	function set_config_number(){
		$('.number').number(true, 0, '.', ',');
		$('.persen').number(true, 2, '.', ',');
	}

	function load_tarif_sewa(){
		var jenis_operasi_id = $("#jenis_operasi_id").val();
		var kelas_tarif_id = $("#kelas_tarif_id").val();
		var id_alat = $("#id_alat").val();
		if (id_alat !='0' && id_alat !='#'){
			$.ajax({
				url: '{site_url}tko/load_tarif_sewa/',
				type: "POST",
				dataType: 'json',
				data: {
					jenis_operasi_id: jenis_operasi_id,
					kelas_tarif_id: kelas_tarif_id,
					id_alat: id_alat,

				},
				success: function(data) {

					if (data=='error') {
						$("#tarif_alat").val(0);
						$("#sarana_alat").val(0);
						$("#pelayanan_alat").val(0);
						$("#bhp_alat").val(0);
						$("#perawatan_alat").val(0);
					}else{
						$("#tarif_alat").val(data.total);
						$("#sarana_alat").val(data.jasasarana);
						$("#pelayanan_alat").val(data.jasapelayanan);
						$("#bhp_alat").val(data.bhp);
						$("#perawatan_alat").val(data.biayaperawatan);
						hitung_perkalian_alat();
						// $("#kuantitas_alat").focus();
					}
					$('.number').number(true, 0, '.', ',');
					// clear_do();

					// console.log(data);
				}
			});
		}else{
			$("#tarif_alat").val(0);
			$("#sarana_alat").val(0);
			$("#pelayanan_alat").val(0);
			$("#bhp_alat").val(0);
			$("#perawatan_alat").val(0);
		}
	}

	function load_dokter_anestesi(){

		$("#dokter_anestesi_id").select2({
			noResults: 'Dokter Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}tko/get_dokter_anestesi/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,
				  }
				  return query;
				},
				processResults: function (data) {
					// console.log(data);
					return {
						results: $.map(data, function (item) {
							return {
								text: item.nama,
								id: item.id,
								tipe : item.tipe
							}
						})
					};
				}
			}
		});

	}
	function load_asisten_anestesi(){

		$("#asisten_anestesi_id").select2({
			noResults: 'Assisten Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}tko/get_dokter_pegawai_anestesi/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,
				  }
				  return query;
				},
				processResults: function (data) {
					// console.log(data);
					return {
						results: $.map(data, function (item) {
							return {
								text: item.nama,
								id: item.id,
								tipe : item.tipe
							}
						})
					};
				}
			}
		});

	}
	function load_asisten_operator(){

		$("#asisten_operator_id").select2({
			noResults: 'Assisten Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}tko/get_dokter_asisten_operator/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,
				  }
				  return query;
				},
				processResults: function (data) {
					// console.log(data);
					return {
						results: $.map(data, function (item) {
							return {
								text: item.nama,
								id: item.id,
								tipe : item.tipe
							}
						})
					};
				}
			}
		});

	}
	function load_tarif_do(){
		var jenis_operasi_id = $("#jenis_operasi_id").val();
		var kelas_tarif_id = $("#kelas_tarif_id").val();
		$(".kelas_operasi_alat").val($("#kelas_tarif_id option:selected").text());
		$.ajax({
			url: '{site_url}tko/load_tarif_do/',
			type: "POST",
			dataType: 'json',
			data: {
				jenis_operasi_id: jenis_operasi_id,
				kelas_tarif_id: kelas_tarif_id,

			},
			success: function(data) {
				// console.log(data);
				if (data=='error') {
					$(".tarif_do").val(0);
					$(".nama_tarif_do").val('');
					$(".jenis_operasi_do").val('');
					$(".sarana_do").val(0);
					$(".pelayanan_do").val(0);
					$(".bhp_do").val(0);
					$(".perawatan_do").val(0);
				}else{
					$(".id_tarif_do").val(data.id);
					$(".tarif_do").val(data.total);
					$(".nama_tarif_do").val(data.nama_tarif);
					$(".jenis_operasi_do").val(data.jenis_operasi);
					$(".sarana_do").val(data.jasasarana);
					$(".pelayanan_do").val(data.jasapelayanan);
					$(".bhp_do").val(data.bhp);
					$(".perawatan_do").val(data.biayaperawatan);
				}
				$('.number').number(true, 0, '.', ',');
				clear_do();

				// console.log(data);
			}
		});
	}
	function get_tarif_edit(){
		var jenis=$("#jenis_operasi_id2").val();
		var kelas=$("#kelas_tarif_id2").val();
		$('#id_tarif_edit')
					.find('option')
					.remove()
					.end()
					.append('<option value="">- Pilih Jabatan -</option>')
					.val('').trigger("liszt:updated");
		if (kelas && jenis){
			$.ajax({
			  url: '{site_url}tko/get_tarif_edit/'+jenis+'/'+kelas,
			  dataType: "json",
			  success: function(data) {

				$.each(data, function (i,row) {
					$('#id_tarif_edit').append('<option value="' + row.id + '" selected>' + row.nama + '</option>');
					// D.bhp,D.biayaperawatan,D.jasapelayanan,D.jasasarana,D.total
					$("#bhp_edit").val(row.bhp);
					$("#biayaperawatan_edit").val(row.biayaperawatan);
					$("#jasapelayanan_edit").val(row.jasapelayanan);
					$("#jasasarana_edit").val(row.jasasarana);
					$("#nominal_tarif_edit").val(row.total);
					$("#total_edit").val(row.total);
				});
			  }
			});
		}
		// alert('hilang');
	}
	$("#jenis_operasi_id2,#kelas_tarif_id2").change(function(){
		get_tarif_edit();
	});
	function load_tarif_ruangan(){
		var jenis_operasi_id = $("#jenis_operasi_id").val();
		var kelas_tarif_id = $("#kelas_tarif_id").val();
		$(".kelas_operasi_alat").val($("#kelas_tarif_id option:selected").text());
		$.ajax({
			url: '{site_url}tko/load_tarif_ruangan/',
			type: "POST",
			dataType: 'json',
			data: {
				jenis_operasi_id: jenis_operasi_id,
				kelas_tarif_id: kelas_tarif_id,

			},
			success: function(data) {
				if (data=='error') {
					$(".id_tarif_ruangan").val(0);
					$(".nama_tarif_ruangan").val('');
					$(".tarif_ruangan").val('');
					$(".sarana_ruangan").val(0);
					$(".pelayanan_ruangan").val(0);
					$(".bhp_ruangan").val(0);
					$(".perawatan_ruangan").val(0);
				}else{
					$(".id_tarif_ruangan").val(data.id);
					$(".nama_tarif_ruangan").val(data.nama_tarif);
					$(".tarif_ruangan").val(data.total);
					$(".sarana_ruangan").val(data.jasasarana);
					$(".pelayanan_ruangan").val(data.jasapelayanan);
					$(".bhp_ruangan").val(data.bhp);
					$(".perawatan_ruangan").val(data.biayaperawatan);
				}
				$('.number').number(true, 0, '.', ',');
				$("#lbl_total_ruangan").text(formatNumber($(".tarif_ruangan").val()));
				// $("#lbl_total_da").text(formatNumber(total_grand));
				clear_do();

				// console.log(data);
			}
		});
	}
	function load_tarif_fullcare(){
		var idkelompokpasien = $("#idkelompokpasien").val();
		var ruang_id = $("#ruang_id").val();
		var kelas_tarif_id = $("#kelas_tarif_id").val();
		$(".ruangan_fullcare").val($("#ruang_id option:selected").text());
		$.ajax({
			url: '{site_url}tko/load_tarif_fullcare/',
			type: "POST",
			dataType: 'json',
			data: {
				idkelompokpasien: idkelompokpasien,
				ruang_id: ruang_id,
				kelas_tarif_id: kelas_tarif_id,

			},
			success: function(data) {
				if (data=='error') {
					$(".id_tarif_fullcare").val(0);
					$(".nama_tarif_fullcare").val('');
					$(".tarif_fullcare").val('');
					$(".sarana_fullcare").val(0);
					$(".pelayanan_fullcare").val(0);
					$(".bhp_fullcare").val(0);
					$(".perawatan_fullcare").val(0);
				}else{
					$(".id_tarif_fullcare").val(data.idtarif);
					$(".nama_tarif_fullcare").val(data.nama);
					$(".tarif_fullcare").val(data.total);
					$(".sarana_fullcare").val(data.jasasarana);
					$(".pelayanan_fullcare").val(data.jasapelayanan);
					$(".bhp_fullcare").val(data.bhp);
					$(".perawatan_fullcare").val(data.biayaperawatan);
				}
				$('.number').number(true, 0, '.', ',');
				$("#lbl_total_fullcare").text(formatNumber($(".tarif_fullcare").val()));
				// $("#lbl_total_da").text(formatNumber(total_grand));
				// clear_do();

				// console.log(data);
			}
		});
	}
	// coding DO
	function validate_do()
	{
		if($("#dokter_operator_id").val() == "#"){
			sweetAlert("Maaf...", "Dokter Harus Diisi!", "error");
			// $("#lbl_obat_id").focus();
			return false;
		}
		return true;
	}
	$("#add_do").click(function() {
			if(!validate_do()) return false;
			var duplicate = false;
			var content = "";

			if($("#nomor_do").val() != ''){
					var no = $("#nomor_do").val();
			}else{
				// var no = $('#manage-tabel-nonRacikan tr').length - 2;
				var no = $('#auto_number_do').val();
				var content = "<tr>";
				$('#tabel_do tbody tr').filter(function (){
					var $cells = $(this).children('td');
					if($cells.eq(1).text() === $("#dokter_operator_id").val()){
						// big_notification("Barang sudah ada didalam daftar","error");
						duplicate = true;
						sweetAlert("Duplicate...", "Data Dokter Duplicate!", "error");
					}
				});
			}
			if(duplicate) return false;
			// alert($("#jenis_operasi_do").val());
			content += "<td style='display:none'>" + no + "</td>";//0 nomor
			content += "<td style='display:none'>" + $("#dokter_operator_id").val() + "</td>"; //1 Dokter
			content += "<td>" + $("#dokter_operator_id option:selected").text() + "</td>";//2 Nama Dokter
			content += '<td><input type="text" name="tarif_do_detail[]" class="form-control tarif_do number" value="'+ $("#tarif_do").val() +'" readonly></td>';//3 Nama Obat
			content += '<td><input type="text" name="nama_tarif_do_detail[]" class="form-control nama_tarif_do" value="'+ $("#nama_tarif_do").val() +'" readonly></td>';//4 Nama Obat
			content += '<td><input type="text" name="jenis_operasi_do_detail[]" class="form-control jenis_operasi_do" value="'+ $("#jenis_operasi_do").val() +'" readonly></td>';//5 Nama Obat
			content += "<td ><button type='button' class='btn btn-sm btn-info edit'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' title='Adjust Tarif' class='btn btn-sm btn-warning adjust disabel'><i class='si si-note'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus'><i class='glyphicon glyphicon-remove'></i></button></td>";
			content += '<td style="display:none"><input type="text" name="nama_do_detail[]" value="'+ $("#dokter_operator_id option:selected").text() +'" readonly></td>';//7 Nama Obat
			content += '<td style="display:none"><input type="text" name="id_do_detail[]" value="'+ $("#dokter_operator_id").val() +'" readonly></td>';//8 Nama Obat
			content += '<td style="display:none"><input type="text" name="id_tarif_do_detail[]" value="'+ $("#id_tarif_do").val() +'" readonly></td>';//9 Nama Obat
			content += '<td style="display:none"><input type="text" class="sarana_do" name="sarana_do_detail[]" value="'+ $("#sarana_do").val() +'" readonly></td>';//10 Nama Obat
			content += '<td style="display:none"><input type="text" class="pelayanan_do" name="jasapelayanan_do_detail[]" value="'+ $("#pelayanan_do").val() +'" readonly></td>';//11 Nama Obat
			content += '<td style="display:none"><input type="text" class="bhp_do" name="bhp_do_detail[]" value="'+ $("#bhp_do").val() +'" readonly></td>';//12 Nama Obat
			content += '<td style="display:none"><input type="text" class="perawatan_do" name="biayaperawatan_do_detail[]" value="'+ $("#perawatan_do").val() +'" readonly></td>';//13 Nama Obat
			// alert(content display:none);
			if($("#row_do").val() != ''){
				$('#tabel_do tbody tr:eq(' + $("#row_do").val() + ')').html(content);
			}else{
				content += "</tr>";
				$('#tabel_do tbody').append(content);
			}

			$(".edit").attr('disabled',false);
			$(".hapus").attr('disabled',false);
			$('.number').number(true, 0, '.', ',');
			$("#auto_number_do").val(parseInt($("#auto_number_do").val()) + 1);
			clear_do();
	});
	$("#btn_edit_tarif").click(function() {
		var row_do=$("#row_do").val();
		$('#tabel_do tbody tr').each(function() {
				var tr = $(this).closest('tr');
				var $cells = $(this).children('td');
				if (tr[0].sectionRowIndex==$("#row_do").val()){

					tr.find("td:eq(3) input").val($("#nominal_tarif_edit").val());//IDtarif
					tr.find("td:eq(4) input").val($("#id_tarif_edit option:selected").text());
					tr.find("td:eq(5) input").val($("#jenis_operasi_id2 option:selected").text());
					tr.find("td:eq(9) input").val($("#id_tarif_edit").val());//IDtarif
					tr.find("td:eq(10) input").val($("#jasasarana_edit").val());//IDtarif
					tr.find("td:eq(11) input").val($("#jasapelayanan_edit").val());//IDtarif
					tr.find("td:eq(12) input").val($("#bhp_edit").val());//IDtarif
					tr.find("td:eq(13) input").val($("#biayaperawatan_edit").val());//IDtarif

				}
		});
		total_do();
		$("#modal_adjust").modal('hide');
			// clear_do();
	});
	$("#cancel_do").click(function() {

		clear_do();
	});
	function clear_do(){
		// alert('sini');
		$("#row_do").val('');
		$("#nomor_do").val('');
		$("#dokter_operator_id").val("#").trigger('change');
		if ($("#st_lock")=='0'){
		$(".edit").attr('disabled',false);
		$(".hapus").attr('disabled',false);
		}
		total_do();
	}
	$(document).on("click",".edit",function(){
		$(".edit").attr('disabled',true);
		$(".hapus").attr('disabled',true);
		$("#btn_search_do").attr('disabled',true);
		$id = $(this).closest('tr').find("td:eq(1)").html();
		// alert(':'+$id+':');
		$("#dokter_operator_id").val($id).trigger('change');
		$("#nomor_do").val($(this).closest('tr').find("td:eq(0)").html());
		$("#row_do").val($(this).closest('tr')[0].sectionRowIndex);


	});
	$(document).on("click",".adjust",function(){

		$id = $(this).closest('tr').find("td:eq(1)").html();
		get_tarif_edit();
		$("#modal_adjust").modal('show');
		$("#nomor_do").val($(this).closest('tr').find("td:eq(0)").html());
		$("#row_do").val($(this).closest('tr')[0].sectionRowIndex);


	});
	$(document).on("click","#btn_cancel_tarif",function(){

		$("#modal_adjust").modal('hide');

	});
	$(document).on("click",".hapus",function(){
		var td=$(this).closest('td');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			td.parent().remove();
			clear_do();
		});


	});
	function total_do()
	{
		var total_grand = 0;
		var nama_operator='';
		$('#tabel_do tbody tr').each(function() {
			total_grand +=parseFloat($(this).find('td:eq(3) input').val());
			if (nama_operator!=''){
				nama_operator =nama_operator + ' & ' +$(this).find('td:eq(2)').text();
			}else{
				nama_operator =$(this).find('td:eq(2)').text();
			}
		});
			// console.log(nama_operator);
		// alert(nama_operator);
		$("#total_do").val(total_grand);
		$("#total_o").val(total_grand);
		$("#total_o2").val(total_grand);

		$("#lbl_total_do").text(formatNumber(total_grand));
		$("#lbl_dokter_operator").text(nama_operator);
		$("#lbl_dokter_operator2").text(nama_operator);
		total_tarif_anesthesi();
		total_tarif_asisten_operator()
		total_da();

	}
	function total_tarif_anesthesi()
	{
		var total_o = parseFloat($("#total_o").val());
		var persen_da = parseFloat($("#persen_da").val());
		var total=total_o*persen_da/100;
		$("#total_da").val(total);
		total_da();
	}
	// ENDING coding DO
	// BEGIN Coding DA
	$("#dokter_anestesi_id").change(function(){
		data=$("#dokter_anestesi_id").select2('data')[0];
		var tipe_dokter=data.tipe;
		$("#dokter_anestesi_tipe").val(tipe_dokter);
		total_da();
		tarif_anesthesi();
	});
	$("#ruang_id").change(function(){
		load_tarif_fullcare();
	});
	$("#persen_da").keyup(function(){
		if (parseFloat($("#persen_da").val())>100){
			$("#persen_da").val(100);
		}

		total_tarif_anesthesi();

	});

	$("#persen_dokter_da").keyup(function(){
		if ($("#row_da").val() !=''){//EDIT
			var batas=100-parseFloat($("#gt_persen_da").val())+parseFloat($("#persen_da_before").val());
		}else{
			var batas=100-parseFloat($("#gt_persen_da").val());
		}
		// console.log(batas);
		if (parseFloat($("#persen_dokter_da").val())>batas){
			$("#persen_dokter_da").val(batas);
			$.toaster({priority : 'danger', title : 'Warning!', message : 'Melebihi 100%'});
		}

		tarif_anesthesi();

	});
	function tarif_anesthesi()
	{
		var total_o = parseFloat($("#total_da").val());
		var persen_da = parseFloat($("#persen_dokter_da").val());
		var total=total_o*persen_da/100;
		$("#tarif_da").val(total);
	}
	function validate_da()
	{
		if($("#dokter_anestesi_id").val() == "#"){
			sweetAlert("Maaf...", "Dokter Harus Diisi!", "error");
			$("#dokter_operator_id").focus();
			return false;
		}
		// if(parseFloat($("#tarif_da").val()) <= 0){
			// sweetAlert("Maaf...", "Jasa Medis Anesthesi harus Diisi dengan benar!", "error");
			// $("#persen_dokter_da").focus();
			// return false;
		// }
		return true;
	}
	$("#add_da").click(function() {
			if(!validate_da()) return false;
			var duplicate = false;
			var content = "";

			if($("#nomor_da").val() != ''){
					var no = $("#nomor_da").val();
			}else{
				// var no = $('#manage-tabel-nonRacikan tr').length - 2;
				var no = $('#auto_number_da').val();
				var content = "<tr>";
				$('#tabel_da tbody tr').filter(function (){
					var $cells = $(this).children('td');
					if($cells.eq(2).text() === $("#dokter_anestesi_id").val() && $cells.eq(1).text() === $("#dokter_anestesi_tipe").val()){
						// big_notification("Barang sudah ada didalam daftar","error");
						duplicate = true;
						sweetAlert("Duplicate...", "Data Dokter Duplicate!", "error");
					}
				});
			}
			if(duplicate) return false;

			content += "<td style='display:none'>" + no + "</td>";//0 nomor
			content += "<td style='display:none'>" + $("#dokter_anestesi_tipe").val(); + "</td>";//1 Tipe Dokter
			content += "<td style='display:none'>" + $("#dokter_anestesi_id").val() + "</td>"; //1 ID Dokter
			content += "<td>" + $("#dokter_anestesi_id option:selected").text() + "</td>";//0 Nama Dokter
			content += '<td><input type="text" name="persen_da_detail[]" class="form-control persen persen_da" value="'+ $("#persen_dokter_da").val() +'" ></td>';//0 Persen
			content += '<td><input type="text" name="tarif_da_detail[]" class="form-control number tarif_da_detail" value="'+ $("#tarif_da").val() +'" readonly></td>';//0 Nama Obat
			content += "<td ><button type='button' class='btn btn-sm btn-info edit_da'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus_da'><i class='glyphicon glyphicon-remove'></i></button></td>";
			content += '<td style="display:none"><input type="text" name="id_da_detail[]" value="'+ $("#dokter_anestesi_id").val() +'" readonly></td>';//0 Nama Obat
			content += '<td style="display:none"><input type="text" name="id_nama_da_detail[]" value="'+ $("#dokter_anestesi_id option:selected").text() +'" readonly></td>';//0 Nama Obat
			// alert(content);
			if($("#row_da").val() != ''){
				$('#tabel_da tbody tr:eq(' + $("#row_da").val() + ')').html(content);
			}else{
				content += "</tr>";
				$('#tabel_da tbody').append(content);
			}

			$(".edit_da").attr('disabled',false);
			$(".hapus_da").attr('disabled',false);
			set_config_number();
			$("#auto_number_da").val(parseInt($("#auto_number_da").val()) + 1);
			clear_da();
	});
	$(document).on("keyup",".persen_dokter",function(){
		var total=parseFloat($("#gt_persen_da").val());
		var total_after=total+parseFloat($(this).val());
		var value_before=total-total_after;
		// // alert('s');

		// // var batas=100-parseFloat($("#gt_persen_da").val())+parseFloat($(this).val());
		// console.log(value_before + ' : '+total +' '+total_after);
		// if (total_after>100){
			// // return false;
			// $(this).val(total_after-total);
			// // $.toaster({priority : 'danger', title : 'Warning!', message : 'Melebihi 100%'});
		// }

		var nominal;
		nominal=parseFloat($("#total_da").val())*$(this).val()/100;
		$(this).closest('tr').find("td:eq(5) input").val(nominal);
		// console.log(this);
		total_da();

	})
	// $(".persen_dokter").keyup(function(){

	// });
	$("#cancel_da").click(function() {
		clear_da();
	});
	function clear_da(){
		$("#row_da").val('');
		$("#nomor_da").val('');
		$("#dokter_anestesi_id").val("#").trigger('change');
		$(".edit_da").attr('disabled',false);
		$(".hapus_da").attr('disabled',false);
		total_da();
	}
	$(document).on("click",".edit_da",function(){
		$(".edit_da").attr('disabled',true);
		$(".hapus_da").attr('disabled',true);
		// $("#btn_search_da").attr('disabled',true);
		$id = $(this).closest('tr').find("td:eq(2)").html();
		$tipe = $(this).closest('tr').find("td:eq(1)").html();
		$nama = $(this).closest('tr').find("td:eq(3)").html();
		var data2 = {
			id: $id,
			text: $nama,
			tipe: $tipe
		};
		var newOption = new Option(data2.text, data2.id, true, true);
		$('#dokter_anestesi_id').append(newOption);
		// $("#dokter_anestesi_id").val($id).trigger('change');
		$("#dokter_anestesi_tipe").val($tipe);
		$("#tarif_da").val($(this).closest('tr').find("td:eq(5) input").val());
		$("#persen_dokter_da").val($(this).closest('tr').find("td:eq(4) input").val());
		$("#persen_da_before").val($(this).closest('tr').find("td:eq(4) input").val());
		// alert($(this).closest('tr').find("td:eq(4) input").val());

		$("#nomor_da").val($(this).closest('tr').find("td:eq(0)").html());
		$("#row_da").val($(this).closest('tr')[0].sectionRowIndex);


	});
	$(document).on("click",".hapus_da",function(){
		var td=$(this).closest('td');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			td.parent().remove();
			clear_da();
		});


	});
	$(document).on("keyup",".persen_da",function(){
		var total=parseFloat($("#gt_persen_da").val());
		var total_after=total+parseFloat($(this).val());
		var value_before=total-total_after;

		var nominal;
		nominal=parseFloat($("#total_da").val())*$(this).val()/100;
		$(this).closest('tr').find("td:eq(5) input").val(nominal);
		// console.log(nominal);
		total_da();

	})
	function total_da()
	{
		var total_persen = 0;
		var total_grand = 0;
		var nama_operator='';
		$('#tabel_da tbody tr').each(function() {
			total_persen =parseFloat($(this).find('td:eq(4) input').val());
			total_grand = parseFloat($("#total_da").val()) * total_persen / 100;
			$(this).find('td:eq(5) input').val(total_grand);
		});

		total_persen = 0;
		total_grand = 0;
		$('#tabel_da tbody tr').each(function() {
			total_persen +=parseFloat($(this).find('td:eq(4) input').val());
			total_grand +=parseFloat($(this).find('td:eq(5) input').val());
			if (nama_operator!=''){
				nama_operator =nama_operator + ' & ' +$(this).find('td:eq(3)').text();
			}else{
				nama_operator =$(this).find('td:eq(3)').text();
			}
		});

		$("#persen_dokter_da").val(100-total_persen);
		tarif_anesthesi();
		$("#gt_da").val(total_grand);
		$("#total_a").val(total_grand);
		total_tarif_asisten_anesthesi();
		$("#gt_persen_da").val(total_persen);
		$("#lbl_total_persen_da").text(formatNumber(total_persen)+' %');
		$("#lbl_total_da").text(formatNumber(total_grand));
		if (total_persen !=100){
			$("#lbl_total_persen_da").removeClass("total").addClass("total_red");
			$("#lbl_total_da").removeClass("total").addClass("total_red");
		}else{
			$("#lbl_total_persen_da").removeClass("total_red").addClass("total");
			$("#lbl_total_da").removeClass("total_red").addClass("total");
			if (total_grand==0){
				$("#lbl_total_da").removeClass("total").addClass("total_red");
			}

		}
		$("#lbl_dokter_anasthesi").text(nama_operator);


	}

	// ENDING DA
	// BEGIN Coding ASISTEN ANASTHESI
	function total_tarif_asisten_anesthesi()
	{
		var total_o = parseFloat($("#total_a").val());
		var persen_daa = parseFloat($("#persen_daa").val());
		var total=total_o*persen_daa/100;
		$("#total_daa").val(total);
		total_daa();
	}
	$("#asisten_anestesi_id").change(function(){
		data=$("#asisten_anestesi_id").select2('data')[0];
		var tipe_dokter=data.tipe;
		// alert(tipe_dokter);
		$("#asisten_anestesi_tipe").val(tipe_dokter);
		total_daa();
		tarif_asisten_anesthesi();
	});
	$("#persen_daa").keyup(function(){
		if (parseFloat($("#persen_daa").val())>100){
			$("#persen_daa").val(100);
		}

		total_tarif_asisten_anesthesi();

	});

	$("#persen_dokter_daa").keyup(function(){
		if ($("#row_daa").val() !=''){//EDIT
			var batas=100-parseFloat($("#gt_persen_daa").val())+parseFloat($("#persen_daa_before").val());
		}else{
			var batas=100-parseFloat($("#gt_persen_daa").val());
		}
		// console.log(batas);
		if (parseFloat($("#persen_dokter_daa").val())>batas){
			$("#persen_dokter_daa").val(batas);
			$.toaster({priority : 'danger', title : 'Warning!', message : 'Melebihi 100%'});
		}

		tarif_asisten_anesthesi();

	});
	function tarif_asisten_anesthesi()
	{
		var total_o = parseFloat($("#total_daa").val());
		var persen_daa = parseFloat($("#persen_dokter_daa").val());
		var total=total_o*persen_daa/100;
		$("#tarif_daa").val(total);
	}
	function validate_daa()
	{
		if($("#asisten_anestesi_id").val() == "#"){
			sweetAlert("Maaf...", "Asisten Harus Diisi!", "error");
			$("#dokter_operator_id").focus();
			return false;
		}
		// if(parseFloat($("#tarif_daa").val()) <= 0){
			// sweetAlert("Maaf...", "Jasa Asisten harus Diisi dengan benar!", "error");
			// $("#persen_dokter_daa").focus();
			// return false;
		// }
		return true;
	}
	$("#add_daa").click(function() {
			if(!validate_daa()) return false;
			var duplicate = false;
			var content = "";

			if($("#nomor_daa").val() != ''){
					var no = $("#nomor_daa").val();
			}else{
				// var no = $('#manage-tabel-nonRacikan tr').length - 2;
				var no = $('#auto_number_daa').val();
				var content = "<tr>";
				$('#tabel_daa tbody tr').filter(function (){
					var $cells = $(this).children('td');
					if($cells.eq(2).text() === $("#asisten_anestesi_id").val() && $cells.eq(1).text() === $("#asisten_anestesi_tipe").val()){
						// big_notification("Barang sudah ada didalam daftar","error");
						duplicate = true;
						sweetAlert("Duplicate...", "Data Dokter Duplicate!", "error");
					}
				});
			}
			if(duplicate) return false;

			content += "<td style='display:none'>" + no + "</td>";//0 nomor
			content += "<td style='display:none'>" + $("#asisten_anestesi_tipe").val(); + "</td>";//1 Tipe Dokter
			content += "<td style='display:none'>" + $("#asisten_anestesi_id").val() + "</td>"; //1 ID Dokter
			content += "<td>" + $("#asisten_anestesi_id option:selected").text() + "</td>";//0 Nama Dokter
			content += '<td><input type="text" name="persen_daa_detail[]" class="form-control persen persen_daa" value="'+ $("#persen_dokter_daa").val() +'" ></td>';//0 Persen
			content += '<td><input type="text" name="tarif_daa_detail[]" class="form-control number tarif_daa_detail" value="'+ $("#tarif_daa").val() +'" readonly></td>';//0 Nama Obat
			content += "<td ><button type='button' class='btn btn-sm btn-info edit_daa'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus_daa'><i class='glyphicon glyphicon-remove'></i></button></td>";
			content += '<td style="display:none"><input type="text" name="id_daa_detail[]" value="'+ $("#asisten_anestesi_id").val() +'" readonly></td>';//0 Nama Obat
			content += '<td style="display:none"><input type="text" name="id_nama_daa_detail[]" value="'+ $("#asisten_anestesi_id option:selected").text() +'" readonly></td>';//0 Nama Obat
			content += '<td style="display:none"><input type="text" name="jenis_sisten_detail[]" value="'+ $("#asisten_anestesi_tipe").val() +'" readonly></td>';//0 Nama Obat

			// alert(content);
			if($("#row_daa").val() != ''){
				$('#tabel_daa tbody tr:eq(' + $("#row_daa").val() + ')').html(content);
			}else{
				content += "</tr>";
				$('#tabel_daa tbody').append(content);
			}

			$(".edit_daa").attr('disabled',false);
			$(".hapus_daa").attr('disabled',false);
			set_config_number();
			$("#auto_number_daa").val(parseInt($("#auto_number_daa").val()) + 1);
			clear_daa();
	});
	$(document).on("keyup",".persen_dokter",function(){
		var total=parseFloat($("#gt_persen_daa").val());
		var total_after=total+parseFloat($(this).val());
		var value_before=total-total_after;

		var nominal;
		nominal=parseFloat($("#total_daa").val())*$(this).val()/100;
		$(this).closest('tr').find("td:eq(5) input").val(nominal);
		// console.log(nominal);
		total_daa();

	})
	$(document).on("click","#simpan_tko",function(){

		// $("#loader").show();
		// var total=parseFloat($("#gt_persen_daa").val());
		// var total_after=total+parseFloat($(this).val());
		// var value_before=total-total_after;

		// var nominal;
		// nominal=parseFloat($("#total_daa").val())*$(this).val()/100;
		// $(this).closest('tr').find("td:eq(5) input").val(nominal);
		// // console.log(nominal);
		// total_daa();

	})
	// $(".persen_dokter").keyup(function(){

	// });
	$("#cancel_daa").click(function() {
		clear_daa();
	});
	function clear_daa(){
		$("#row_daa").val('');
		$("#nomor_daa").val('');
		$("#asisten_anestesi_id").val("#").trigger('change');
		$(".edit_daa").attr('disabled',false);
		$(".hapus_daa").attr('disabled',false);
		total_daa();
	}
	$(document).on("click",".edit_daa",function(){
		$(".edit_daa").attr('disabled',true);
		$(".hapus_daa").attr('disabled',true);
		$("#btn_search_daa").attr('disabled',true);
		$id = $(this).closest('tr').find("td:eq(2)").html();
		$tipe = $(this).closest('tr').find("td:eq(1)").html();
		$nama = $(this).closest('tr').find("td:eq(3)").html();
		var data2 = {
			id: $id,
			text: $nama,
			tipe: $tipe
		};
		var newOption = new Option(data2.text, data2.id, true, true);
		$('#asisten_anestesi_id').append(newOption);
		// $("#asisten_anestesi_id").val($id).trigger('change');
		$("#asisten_anestesi_tipe").val($tipe);
		$("#persen_dokter_daa").val($(this).closest('tr').find("td:eq(4) input").val());
		$("#persen_daa_before").val($(this).closest('tr').find("td:eq(4) input").val());
		$("#tarif_daa").val($(this).closest('tr').find("td:eq(5) input").val());
		$("#nomor_daa").val($(this).closest('tr').find("td:eq(0)").html());
		$("#row_daa").val($(this).closest('tr')[0].sectionRowIndex);


	});
	$(document).on("click",".hapus_daa",function(){
		var td=$(this).closest('td');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			td.parent().remove();
			clear_daa();
		});


	});
	$(document).on("keyup",".persen_daa",function(){
		var total=parseFloat($("#gt_persen_daa").val());
		var total_after=total+parseFloat($(this).val());
		var value_before=total-total_after;

		var nominal;
		nominal=parseFloat($("#total_daa").val())*$(this).val()/100;
		$(this).closest('tr').find("td:eq(5) input").val(nominal);
		// console.log(nominal);
		total_daa();

	})
	function total_daa()
	{
		var total_persen = 0;
		var total_grand = 0;
		$('#tabel_daa tbody tr').each(function() {
			total_persen =parseFloat($(this).find('td:eq(4) input').val());
			total_grand = parseFloat($("#total_daa").val()) * total_persen / 100;
			$(this).find('td:eq(5) input').val(total_grand);
		});

		total_persen = 0;
		total_grand = 0;
		$('#tabel_daa tbody tr').each(function() {
			total_persen +=parseFloat($(this).find('td:eq(4) input').val());
			total_grand +=parseFloat($(this).find('td:eq(5) input').val());

		});

		$("#persen_dokter_daa").val(100-total_persen);
		tarif_asisten_anesthesi();
		$("#gt_daa").val(total_grand);
		$("#gt_persen_daa").val(total_persen);
		$("#lbl_total_persen_daa").text(formatNumber(total_persen)+' %');
		$("#lbl_total_daa").text(formatNumber(total_grand));
		if (total_persen !=100){
			$("#lbl_total_persen_daa").removeClass("total").addClass("total_red");
			$("#lbl_total_daa").removeClass("total").addClass("total_red");
		}else{
			$("#lbl_total_persen_daa").removeClass("total_red").addClass("total");
			$("#lbl_total_daa").removeClass("total_red").addClass("total");
			if (total_grand==0){
				$("#lbl_total_daa").removeClass("total").addClass("total_red");
			}

		}


	}

	// ENDING ASSITEN ANASTHESI
	// BEGIN Coding ASISTEN OPERATOR
function total_tarif_asisten_operator()
	{
		var total_o = parseFloat($("#total_o2").val());
		var persen_dao = parseFloat($("#persen_dao").val());
		var total=total_o*persen_dao/100;
		$("#total_dao").val(total);
		total_dao();
	}
	$("#asisten_operator_id").change(function(){
		data=$("#asisten_operator_id").select2('data')[0];
		var tipe_dokter=data.tipe;
		$("#asisten_operator_tipe").val(tipe_dokter);
		total_dao();
		tarif_asisten_operator();
	});
	$("#persen_dao").keyup(function(){
		if (parseFloat($("#persen_dao").val())>100){
			$("#persen_dao").val(100);
		}

		total_tarif_asisten_operator();

	});

	$("#persen_dokter_dao").keyup(function(){
		if ($("#row_dao").val() !=''){//EDIT
			var batas=100-parseFloat($("#gt_persen_dao").val())+parseFloat($("#persen_dao_before").val());
		}else{
			var batas=100-parseFloat($("#gt_persen_dao").val());
		}
		// console.log(batas);
		if (parseFloat($("#persen_dokter_dao").val())>batas){
			$("#persen_dokter_dao").val(batas);
			$.toaster({priority : 'danger', title : 'Warning!', message : 'Melebihi 100%'});
		}

		tarif_asisten_operator();

	});
	function tarif_asisten_operator()
	{
		var total_o = parseFloat($("#total_dao").val());
		var persen_dao = parseFloat($("#persen_dokter_dao").val());
		var total=total_o*persen_dao/100;
		$("#tarif_dao").val(total);
	}
	function validate_dao()
	{
		if($("#asisten_operator_id").val() == "#"){
			sweetAlert("Maaf...", "Asisten Operator Harus Diisi!", "error");
			$("#dokter_operator_id").focus();
			return false;
		}
		// if(parseFloat($("#tarif_dao").val()) <= 0){
			// sweetAlert("Maaf...", "Jasa Asisten Operator harus Diisi dengan benar!", "error");
			// $("#persen_dokter_dao").focus();
			// return false;
		// }
		return true;
	}
	$("#add_dao").click(function() {
			if(!validate_dao()) return false;
			var duplicate = false;
			var content = "";

			if($("#nomor_dao").val() != ''){
					var no = $("#nomor_dao").val();
			}else{
				// var no = $('#manage-tabel-nonRacikan tr').length - 2;
				var no = $('#auto_number_dao').val();
				var content = "<tr>";
				$('#tabel_dao tbody tr').filter(function (){
					var $cells = $(this).children('td');
					if($cells.eq(2).text() === $("#asisten_operator_id").val() && $cells.eq(1).text() === $("#asisten_operator_tipe").val()){
						// big_notification("Barang sudah ada didalam daftar","error");
						duplicate = true;
						sweetAlert("Duplicate...", "Data Dokter Duplicate!", "error");
					}
				});
			}
			if(duplicate) return false;

			content += "<td style='display:none'>" + no + "</td>";//0 nomor
			content += "<td style='display:none'>" + $("#asisten_operator_tipe").val(); + "</td>";//1 Tipe Dokter
			content += "<td style='display:none'>" + $("#asisten_operator_id").val() + "</td>"; //1 ID Dokter
			content += "<td>" + $("#asisten_operator_id option:selected").text() + "</td>";//0 Nama Dokter
			content += '<td><input type="text" name="persen_dao_detail[]" class="form-control persen persen_dokter" value="'+ $("#persen_dokter_dao").val() +'" ></td>';//0 Persen
			content += '<td><input type="text" name="tarif_dao_detail[]" class="form-control number tarif_dao_detail" value="'+ $("#tarif_dao").val() +'" readonly></td>';//0 Nama Obat
			content += "<td ><button type='button' class='btn btn-sm btn-info edit_dao'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus_dao'><i class='glyphicon glyphicon-remove'></i></button></td>";
			content += '<td style="display:none"><input type="text" name="id_dao_detail[]" value="'+ $("#asisten_operator_id").val() +'" readonly></td>';//0 Nama Obat
			content += '<td style="display:none"><input type="text" name="id_nama_dao_detail[]" value="'+ $("#asisten_operator_id option:selected").text() +'" readonly></td>';//0 Nama Obat
			content += '<td style="display:none"><input type="text" name="jenis_asisten_dao_detail[]" value="'+ $("#asisten_operator_tipe").val() +'" readonly></td>';//0 Nama Obat

			// alert(content);
			if($("#row_dao").val() != ''){
				$('#tabel_dao tbody tr:eq(' + $("#row_dao").val() + ')').html(content);
			}else{
				content += "</tr>";
				$('#tabel_dao tbody').append(content);
			}

			$(".edit_dao").attr('disabled',false);
			$(".hapus_dao").attr('disabled',false);
			set_config_number();
			$("#auto_number_dao").val(parseInt($("#auto_number_dao").val()) + 1);
			clear_dao();
	});
	$(document).on("keyup",".persen_dokter",function(){
		var total=parseFloat($("#gt_persen_dao").val());
		var total_after=total+parseFloat($(this).val());
		var value_before=total-total_after;
		// // alert('s');

		// // var batas=100-parseFloat($("#gt_persen_dao").val())+parseFloat($(this).val());
		// console.log(value_before + ' : '+total +' '+total_after);
		// if (total_after>100){
			// // return false;
			// $(this).val(total_after-total);
			// // $.toaster({priority : 'danger', title : 'Warning!', message : 'Melebihi 100%'});
		// }

		var nominal;
		nominal=parseFloat($("#total_dao").val())*$(this).val()/100;
		$(this).closest('tr').find("td:eq(5) input").val(nominal);
		// console.log(nominal);
		total_dao();

	})

	// $(".persen_dokter").keyup(function(){

	// });
	$("#cancel_dao").click(function() {
		clear_dao();
	});
	function clear_dao(){
		$("#row_dao").val('');
		$("#nomor_dao").val('');
		$("#asisten_operator_id").val("#").trigger('change');
		$(".edit_dao").attr('disabled',false);
		$(".hapus_dao").attr('disabled',false);
		total_dao();
	}
	$(document).on("click",".edit_dao",function(){
		$(".edit_dao").attr('disabled',true);
		$(".hapus_dao").attr('disabled',true);
		$("#btn_search_dao").attr('disabled',true);
		$id = $(this).closest('tr').find("td:eq(2)").html();
		$tipe = $(this).closest('tr').find("td:eq(1)").html();
		$nama = $(this).closest('tr').find("td:eq(3)").html();
		var data2 = {
			id: $id,
			text: $nama,
			tipe: $tipe
		};
		var newOption = new Option(data2.text, data2.id, true, true);
		$('#asisten_operator_id').append(newOption);
		// alert($tipe);
		// $("#asisten_operator_id").val($id).trigger('change');
		$("#asisten_operator_tipe").val($tipe);
		// $("#asisten_operator_id").val($id).trigger('change');
		$("#persen_dokter_dao").val($(this).closest('tr').find("td:eq(4) input").val());
		$("#persen_dao_before").val($(this).closest('tr').find("td:eq(4) input").val());
		$("#tarif_dao").val($(this).closest('tr').find("td:eq(5) input").val());
		$("#nomor_dao").val($(this).closest('tr').find("td:eq(0)").html());
		$("#row_dao").val($(this).closest('tr')[0].sectionRowIndex);


	});
	$(document).on("click",".hapus_dao",function(){
		var td=$(this).closest('td');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			td.parent().remove();
			clear_dao();
		});


	});
	function total_dao()
	{
		var total_persen = 0;
		var total_grand = 0;
		$('#tabel_dao tbody tr').each(function() {
			total_persen =parseFloat($(this).find('td:eq(4) input').val());
			total_grand = parseFloat($("#total_dao").val()) * total_persen / 100;
			$(this).find('td:eq(5) input').val(total_grand);
		});

		total_persen = 0;
		total_grand = 0;
		$('#tabel_dao tbody tr').each(function() {
			total_persen +=parseFloat($(this).find('td:eq(4) input').val());
			total_grand +=parseFloat($(this).find('td:eq(5) input').val());

		});

		$("#persen_dokter_dao").val(100-total_persen);
		tarif_asisten_operator();
		$("#gt_dao").val(total_grand);
		$("#gt_persen_dao").val(total_persen);
		$("#lbl_total_persen_dao").text(formatNumber(total_persen)+' %');
		$("#lbl_total_dao").text(formatNumber(total_grand));
		if (total_persen !=100){
			$("#lbl_total_persen_dao").removeClass("total").addClass("total_red");
			$("#lbl_total_dao").removeClass("total").addClass("total_red");
		}else{
			$("#lbl_total_persen_dao").removeClass("total_red").addClass("total");
			$("#lbl_total_dao").removeClass("total_red").addClass("total");
			if (total_grand==0){
				$("#lbl_total_dao").removeClass("total").addClass("total_red");
			}

		}
	}

	// ENDING ASISTEN OPERATOR

	// CODING Obat
	$("#id_obat").select2({
			minimumInputLength: 2,
			noResults: 'Dokter Tidak Ditemukan.',
		// allowClear: true
		// tags: [],
		// var idunit=$("#idunit_obat").val();
		ajax: {
			url: '{site_url}tko/js_obat/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,
		 data: function (params) {
			  var query = {
				search: params.term,idtipe:'3',idunit:$("#idunit_obat").val(),
			  }
			  return query;
			},
			processResults: function (data) {
				console.log($("#idunit_obat").val());
				return {
					results: $.map(data, function (item) {
						return {
							text: item.nama,
							id: item.idbarang,
							tipe : item.idtipe
						}
					})
				};
			}
		}
	});
	$("#id_obat").change(function(){
		var idtipe;
		var data;
		data=$("#id_obat").select2('data')[0];
		if (data != null){
			idtipe=data.tipe;
		}else{
			idtipe='3';
		}
		// alert(idtipe);
		console.log(idtipe);
		if ($(this).val()!=null){
			$.ajax({
			  url: '{site_url}tko/get_barang/'+$(this).val()+'/'+idtipe+'/'+$("#idkelompokpasien").val(),
			  dataType: "json",
			  success: function(data) {
				  console.log(data);
				$("#idtipe_obat").val(data.idtipe);
				$("#margin_obat").val(data.margin);
				$("#harga_dasar_obat").val(data.hargadasar);
				$("#satuan_obat").val(data.satuan);
				$("#harga_obat").val(ceilling(parseFloat(data.hargadasar)+parseFloat(data.hargadasar*data.margin/100)));
				$("#kuantitas_obat").focus();
				hitung_perkalian_obat();
			  }
			});
		}else{

		}
	});
	function hitung_perkalian_obat(){
		$("#total_obat").val(parseFloat($("#kuantitas_obat").val())*parseFloat($("#harga_obat").val()));
	}
	$("#kuantitas_obat").keyup(function(){
		if ($(this).val()==''){
			$(this).val(0);
		}
		hitung_perkalian_obat();
	});
	$("#add_obat").click(function() {
			if(!validate_obat()) return false;
			var duplicate = false;
			var content = "";

			if($("#nomor_obat").val() != ''){
					var no = $("#nomor_obat").val();
			}else{
				// var no = $('#manage-tabel-nonRacikan tr').length - 2;
				var no = $('#auto_number_obat').val();
				var content = "<tr>";
				$('#tabel_obat tbody tr').filter(function (){
					var $cells = $(this).children('td');
					if($cells.eq(2).text() === $("#id_obat").val() && $cells.eq(1).text() === $("#idunit_obat").val()){
						// big_notification("Barang sudah ada didalam daftar","error");
						duplicate = true;
						sweetAlert("Duplicate...", "Data Obat Duplicate!", "error");
					}
				});
			}
			if(duplicate) return false;

			content += "<td style='display:none'>" + no + "</td>";//0 nomor
			content += "<td style='display:none'>" + $("#idunit_obat").val(); + "</td>";//1 UNIT
			content += "<td style='display:none'>" + $("#id_obat").val() + "</td>"; //2 ID BARANG
			content += "<td>" + $("#idunit_obat option:selected").text() + "</td>";//3 Nama
			content += "<td>" + $("#id_obat option:selected").text() + "</td>";//4 Nama
			content += "<td>" + $("#satuan_obat").val() + "</td>";//5 Nama
			content += "<td align='right'>" + formatNumber($("#harga_obat").val()) + "</td>";//6 Nama
			content += "<td align='right'>" + formatNumber($("#kuantitas_obat").val()) + "</td>";//7 Nama
			content += "<td align='right'>" + formatNumber($("#total_obat").val()) + "</td>";//8 Nama
			content += "<td>" + $("#user_obat").val() + "</td>";//9 Nama
			content += "<td style='display:none'></td>"; //10 KOSONG
			content += "<td style='display:none'></td>"; //11 KOSONG
			content += "<td style='display:none'></td>"; //12 KOSONG
			content += "<td ><button type='button' class='btn btn-sm btn-info edit_obat'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus_obat'><i class='glyphicon glyphicon-remove'></i></button></td>";
			content += '<td style="display:none"><input type="text" name="idunit_obat_detail[]" value="'+ $("#idunit_obat").val() +'" ></td>';//14 Persen
			content += '<td style="display:none"><input type="text" name="id_obat_detail[]" value="'+ $("#id_obat").val() +'" ></td>';//15 Persen
			content += '<td style="display:none"><input type="text" name="harga_obat_detail[]" value="'+ $("#harga_obat").val() +'" ></td>';//16 Persen
			content += '<td style="display:none"><input type="text" name="kuantitas_obat_detail[]" value="'+ $("#kuantitas_obat").val() +'" ></td>';//17 Persen
			content += '<td style="display:none"><input type="text" name="total_obat_detail[]" value="'+ $("#total_obat").val() +'" ></td>';//18 Persen
			content += '<td style="display:none"><input type="text" name="harga_dasar_obat_detail[]" value="'+ $("#harga_dasar_obat").val() +'" ></td>';//19 Persen
			content += '<td style="display:none"><input type="text" name="margin_obat_detail[]" value="'+ $("#margin_obat").val() +'" ></td>';//20 Persen
			content += '<td style="display:none"><input type="text" name="idtipe_obat_detail[]" value="'+ $("#idtipe_obat").val() +'" ></td>';//21 Persen
			content += '<td style="display:none"><input type="text" name="id_trx_obat_detail[]" value="'+ $("#id_trx_obat").val() +'" ></td>';//21 Persen
			content += '<td style="display:none"><input type="text" name="st_edit_obat[]" value="1" ></td>';//21 Persen
			// alert(content);
			if($("#row_obat").val() != ''){
				$('#tabel_obat tbody tr:eq(' + $("#row_obat").val() + ')').html(content);
			}else{
				content += "</tr>";
				$('#tabel_obat tbody').append(content);
			}

			$(".edit_obat").attr('disabled',false);
			$(".hapus_obat").attr('disabled',false);
			set_config_number();
			$("#auto_number_obat").val(parseInt($("#auto_number_obat").val()) + 1);
			clear_obat();
	});
	$(document).on("click",".edit_obat",function(){
		$(".edit_obat").attr('disabled',true);
		$(".hapus_obat").attr('disabled',true);
		$idunit_obat = $(this).closest('tr').find("td:eq(1)").html();
		var id_obat = $(this).closest('tr').find("td:eq(2)").html();
		var nama = $(this).closest('tr').find("td:eq(4)").html();
		var satuan = $(this).closest('tr').find("td:eq(5)").html();
		var tipe = $(this).closest('tr').find("td:eq(21) input").val();

		var data2 = {
			id: id_obat,
			text: nama,
			tipe: tipe
		};
		var newOption = new Option(data2.text, data2.id, true, true);
		$('#id_obat').append(newOption);
		$("#id_trx_obat").val($(this).closest('tr').find("td:eq(22) input").val());
		$("#satuan_obat").val(satuan);
		$("#idtipe_obat").val(tipe);
		$("#idunit_obat").val($idunit_obat).trigger('change');
		// $("#id_obat").val(id_obat).trigger('change');
		$("#kuantitas_obat").val($(this).closest('tr').find("td:eq(17) input").val());
		$("#harga_obat").val($(this).closest('tr').find("td:eq(16) input").val());
		$("#nomor_obat").val($(this).closest('tr').find("td:eq(0)").html());
		$("#row_obat").val($(this).closest('tr')[0].sectionRowIndex);
		if ($("#id_trx_obat").val()!='0'){
			$(".disable_obat").attr('disabled',true);
		}else{
			$(".disable_obat").attr('disabled',false);
		}
		hitung_perkalian_obat();


	});
	$(document).on("click",".hapus_obat",function(){
		var td=$(this).closest('td');
		var tr=$(this).closest('tr');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			if (tr.find("td:eq(22) input").val()!='0'){
				var content = "<tr>";
				content += '<td ><input type="text" name="jenis_hapus[]" value="obat" ></td>';
				content += '<td ><input type="text" name="id_hapus[]" value="'+ tr.find("td:eq(22) input").val() +'" ></td>';
				content += "</tr>";
				$('#tabel_delete tbody').append(content);
			}
			td.parent().remove();
			clear_obat();
		});


	});
	$(document).on("click","#cancel_obat",function(){
		clear_obat();
	});
	function validate_obat()
	{
		if($("#idunit_obat").val() == "#"){
			sweetAlert("Maaf...", "Unit Harus Diisi!", "error");
			$("#idunit_obat").focus();
			return false;
		}
		if($("#id_obat").val() == "" || $("#id_obat").val() == null || $("#id_obat").val() == '#'){
			sweetAlert("Maaf...", "Obat Harus Diisi!", "error");
			$("#id_obat").focus();
			return false;
		}
		if($("#total_obat").val() == "0"){
			sweetAlert("Maaf...", "Total Obat Harus Diisi!", "error");
			$("#total_obat").focus();
			return false;
		}
		return true;
	}
	function clear_obat(){
		$("#id_trx_obat").val('0');
		$("#row_obat").val('');
		$("#nomor_obat").val('');
		$("#satuan_obat").val('');
		$("#harga_obat").val('');
		$("#total_obat").val('');
		$("#margin_obat").val('');
		$("#idtipe_obat").val('');
		$("#kuantitas_obat").val('');
		$("#id_obat").val(null).trigger('change');
		if ($("#st_lock")=='0'){
		$(".edit_obat").attr('disabled',false);
		$(".hapus_obat").attr('disabled',false);
		$(".disable_obat").attr('disabled',false);
		}
		total_obat();
	}
	function total_obat()
	{
		var total_grand = 0;
		$('#tabel_obat tbody tr').each(function() {
			total_grand +=parseFloat($(this).find('td:eq(18) input').val());
		});
		console.log(total_grand);
		$("#gt_obat").val(total_grand);
		$("#lbl_gt_obat").text(formatNumber(total_grand));

	}
	$("#btn_search_obat").click(function() {
		$("#idtipe_pilih").val('3');
		$("#idkat2").val('');
		$("#form_pilih").val('obat');
		$("#idunit_pilih").val($("#idunit_obat").val()).trigger('change');
		$("#modal_barang").modal('show');
		// loadBarang();
	});



	// END Obat
	// CODING ALKES
		$("#id_alkes").select2({
			minimumInputLength: 2,
			noResults: 'Dokter Tidak Ditemukan.',
		// allowClear: true
		// tags: [],
		// var idunit=$("#idunit_alkes").val();
		ajax: {
			url: '{site_url}tko/js_obat/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,
		 data: function (params) {
			  var query = {
				search: params.term,idtipe:'1',idunit:$("#idunit_alkes").val(),
			  }
			  return query;
			},
			processResults: function (data) {
				console.log($("#idunit_alkes").val());
				return {
					results: $.map(data, function (item) {
						return {
							text: item.nama,
							id: item.idbarang,
							tipe : item.idtipe
						}
					})
				};
			}
		}
	});
	$("#id_alkes").change(function(){
		var idtipe;
		var data;
		data=$("#id_alkes").select2('data')[0];
		if (data != null){
			idtipe=data.tipe;
		}else{
			idtipe='1';
		}
		// alert(idtipe);
		console.log(idtipe);
		if ($(this).val()!=null){
			$.ajax({
			  url: '{site_url}tko/get_barang/'+$(this).val()+'/'+idtipe+'/'+$("#idkelompokpasien").val(),
			  dataType: "json",
			  success: function(data) {
				  console.log(data);
				$("#idtipe_alkes").val(data.idtipe);
				$("#margin_alkes").val(data.margin);
				$("#harga_dasar_alkes").val(data.hargadasar);
				$("#satuan_alkes").val(data.satuan);
				$("#harga_alkes").val(ceilling(parseFloat(data.hargadasar)+parseFloat(data.hargadasar*data.margin/100)));
				$("#kuantitas_alkes").focus();
				hitung_perkalian_alkes();
			  }
			});
		}else{

		}
	});
	function hitung_perkalian_alkes(){
		$("#total_alkes").val(parseFloat($("#kuantitas_alkes").val())*parseFloat($("#harga_alkes").val()));
	}
	$("#kuantitas_alkes").keyup(function(){
		if ($(this).val()==''){
			$(this).val(0);
		}
		hitung_perkalian_alkes();
	});
	$("#add_alkes").click(function() {
			if(!validate_alkes()) return false;
			var duplicate = false;
			var content = "";

			if($("#nomor_alkes").val() != ''){
					var no = $("#nomor_alkes").val();
			}else{
				// var no = $('#manage-tabel-nonRacikan tr').length - 2;
				var no = $('#auto_number_alkes').val();
				var content = "<tr>";
				$('#tabel_alkes tbody tr').filter(function (){
					var $cells = $(this).children('td');
					if($cells.eq(2).text() === $("#id_alkes").val() && $cells.eq(1).text() === $("#idunit_alkes").val()){
						// big_notification("Barang sudah ada didalam daftar","error");
						duplicate = true;
						sweetAlert("Duplicate...", "Data Barang Duplicate!", "error");
					}
				});
			}
			if(duplicate) return false;

			content += "<td style='display:none'>" + no + "</td>";//0 nomor
			content += "<td style='display:none'>" + $("#idunit_alkes").val(); + "</td>";//1 UNIT
			content += "<td style='display:none'>" + $("#id_alkes").val() + "</td>"; //2 ID BARANG
			content += "<td>" + $("#idunit_alkes option:selected").text() + "</td>";//3 Nama
			content += "<td>" + $("#id_alkes option:selected").text() + "</td>";//4 Nama
			content += "<td>" + $("#satuan_alkes").val() + "</td>";//5 Nama
			content += "<td align='right'>" + formatNumber($("#harga_alkes").val()) + "</td>";//6 Nama
			content += "<td align='right'>" + formatNumber($("#kuantitas_alkes").val()) + "</td>";//7 Nama
			content += "<td align='right'>" + formatNumber($("#total_alkes").val()) + "</td>";//8 Nama
			content += "<td>" + $("#user_alkes").val() + "</td>";//9 Nama
			content += "<td style='display:none'></td>"; //10 KOSONG
			content += "<td style='display:none'></td>"; //11 KOSONG
			content += "<td style='display:none'></td>"; //12 KOSONG
			content += "<td ><button type='button' class='btn btn-sm btn-info edit_alkes'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus_alkes'><i class='glyphicon glyphicon-remove'></i></button></td>";
			content += '<td style="display:none"><input type="text" name="idunit_alkes_detail[]" value="'+ $("#idunit_alkes").val() +'" ></td>';//14 Persen
			content += '<td style="display:none"><input type="text" name="id_alkes_detail[]" value="'+ $("#id_alkes").val() +'" ></td>';//15 Persen
			content += '<td style="display:none"><input type="text" name="harga_alkes_detail[]" value="'+ $("#harga_alkes").val() +'" ></td>';//16 Persen
			content += '<td style="display:none"><input type="text" name="kuantitas_alkes_detail[]" value="'+ $("#kuantitas_alkes").val() +'" ></td>';//17 Persen
			content += '<td style="display:none"><input type="text" name="total_alkes_detail[]" value="'+ $("#total_alkes").val() +'" ></td>';//18 Persen
			content += '<td style="display:none"><input type="text" name="harga_dasar_alkes_detail[]" value="'+ $("#harga_dasar_alkes").val() +'" ></td>';//19 Persen
			content += '<td style="display:none"><input type="text" name="margin_alkes_detail[]" value="'+ $("#margin_alkes").val() +'" ></td>';//20 Persen
			content += '<td style="display:none"><input type="text" name="idtipe_alkes_detail[]" value="'+ $("#idtipe_alkes").val() +'" ></td>';//21 Persen
			content += '<td style="display:none"><input type="text" name="id_trx_alkes_detail[]" value="'+ $("#id_trx_alkes").val() +'" ></td>';//21 Persen
			content += '<td style="display:none"><input type="text" name="st_edit_alkes[]" value="1" ></td>';//21 Persen
			// alert(content);
			if($("#row_alkes").val() != ''){
				$('#tabel_alkes tbody tr:eq(' + $("#row_alkes").val() + ')').html(content);
			}else{
				content += "</tr>";
				$('#tabel_alkes tbody').append(content);
			}

			$(".edit_alkes").attr('disabled',false);
			$(".hapus_alkes").attr('disabled',false);
			set_config_number();
			$("#auto_number_alkes").val(parseInt($("#auto_number_alkes").val()) + 1);
			clear_alkes();
	});
	$(document).on("click",".edit_alkes",function(){
		$(".edit_alkes").attr('disabled',true);
		$(".hapus_alkes").attr('disabled',true);
		$idunit_alkes = $(this).closest('tr').find("td:eq(1)").html();
		var id_alkes = $(this).closest('tr').find("td:eq(2)").html();
		var nama = $(this).closest('tr').find("td:eq(4)").html();
		var satuan = $(this).closest('tr').find("td:eq(5)").html();
		var tipe = $(this).closest('tr').find("td:eq(21) input").val();

		var data2 = {
			id: id_alkes,
			text: nama,
			tipe: tipe
		};
		var newOption = new Option(data2.text, data2.id, true, true);
		$('#id_alkes').append(newOption);
		$("#id_trx_alkes").val($(this).closest('tr').find("td:eq(22) input").val());
		$("#satuan_alkes").val(satuan);
		$("#idtipe_alkes").val(tipe);
		$("#idunit_alkes").val($idunit_alkes).trigger('change');
		// $("#id_alkes").val(id_alkes).trigger('change');
		$("#kuantitas_alkes").val($(this).closest('tr').find("td:eq(17) input").val());
		$("#harga_alkes").val($(this).closest('tr').find("td:eq(16) input").val());
		$("#nomor_alkes").val($(this).closest('tr').find("td:eq(0)").html());
		$("#row_alkes").val($(this).closest('tr')[0].sectionRowIndex);
		if ($("#id_trx_alkes").val()!='0'){
			$(".disable_alkes").attr('disabled',true);
		}else{
			$(".disable_alkes").attr('disabled',false);
		}
		hitung_perkalian_alkes();


	});
	$(document).on("click",".hapus_alkes",function(){
		var td=$(this).closest('td');
		var tr=$(this).closest('tr');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			if (tr.find("td:eq(22) input").val()!='0'){
				var content = "<tr>";
				content += '<td ><input type="text" name="jenis_hapus[]" value="alkes" ></td>';
				content += '<td ><input type="text" name="id_hapus[]" value="'+ tr.find("td:eq(22) input").val() +'" ></td>';
				content += "</tr>";
				$('#tabel_delete tbody').append(content);
			}
			td.parent().remove();
			clear_alkes();
		});


	});
	$(document).on("click","#cancel_alkes",function(){
		clear_alkes();
	});
	function validate_alkes()
	{
		// alert($("#id_alkes").val());
		if($("#idunit_alkes").val() == "#"){
			sweetAlert("Maaf...", "Unit Harus Diisi!", "error");
			$("#idunit_alkes").focus();
			return false;
		}
		if($("#id_alkes").val() == "" || $("#id_alkes").val() == null || $("#id_alkes").val() == '#'){
			sweetAlert("Maaf...", "Barang Harus Diisi!", "error");
			$("#id_alkes").focus();
			return false;
		}
		if($("#total_alkes").val() == "0"){
			sweetAlert("Maaf...", "Total Barang Harus Diisi!", "error");
			$("#total_alkes").focus();
			return false;
		}
		return true;
	}
	function clear_alkes(){
		$("#id_trx_alkes").val('0');
		$("#row_alkes").val('');
		$("#nomor_alkes").val('');
		$("#satuan_alkes").val('');
		$("#harga_alkes").val('');
		$("#total_alkes").val('');
		$("#margin_alkes").val('');
		$("#idtipe_alkes").val('');
		$("#kuantitas_alkes").val('');
		$("#id_alkes").val(null).trigger('change');
		$(".edit_alkes").attr('disabled',false);
		$(".hapus_alkes").attr('disabled',false);
		$(".disable_alkes").attr('disabled',false);
		total_alkes();
	}
	function total_alkes()
	{
		var total_grand = 0;
		$('#tabel_alkes tbody tr').each(function() {
			total_grand +=parseFloat($(this).find('td:eq(18) input').val());
		});
		console.log(total_grand);
		$("#gt_alkes").val(total_grand);
		$("#lbl_gt_alkes").text(formatNumber(total_grand));

	}
	$("#btn_search_alkes").click(function() {
		$("#idtipe_pilih").val('1');
		$("#idkat2").val('');
		$("#form_pilih").val('alkes');
		$("#idunit_pilih").val($("#idunit_alkes").val()).trigger('change');
		$("#modal_barang").modal('show');
		// loadBarang();
	});

	// END ALKES
	// CODING IMPLANT
	$("#id_implan").select2({
			minimumInputLength: 2,
			noResults: 'Dokter Tidak Ditemukan.',
		// allowClear: true
		// tags: [],
		// var idunit=$("#idunit_implan").val();
		ajax: {
			url: '{site_url}tko/js_obat/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,
		 data: function (params) {
			  var query = {
				search: params.term,idtipe:'2',idunit:$("#idunit_implan").val(),
			  }
			  return query;
			},
			processResults: function (data) {
				console.log($("#idunit_implan").val());
				return {
					results: $.map(data, function (item) {
						return {
							text: item.nama,
							id: item.idbarang,
							tipe : item.idtipe
						}
					})
				};
			}
		}
	});
	$("#id_implan").change(function(){
		var idtipe;
		var data;
		data=$("#id_implan").select2('data')[0];
		if (data != null){
			idtipe=data.tipe;
		}else{
			idtipe='2';
		}
		// alert(idtipe);
		console.log(idtipe);
		if ($(this).val()!=null){
			$.ajax({
			  url: '{site_url}tko/get_barang/'+$(this).val()+'/'+idtipe+'/'+$("#idkelompokpasien").val(),
			  dataType: "json",
			  success: function(data) {
				  console.log(data);
				$("#idtipe_implan").val(data.idtipe);
				$("#margin_implan").val(data.margin);
				$("#harga_dasar_implan").val(data.hargadasar);
				$("#satuan_implan").val(data.satuan);
				$("#harga_implan").val(ceilling(parseFloat(data.hargadasar)+parseFloat(data.hargadasar*data.margin/100)));
				$("#kuantitas_implan").focus();
				hitung_perkalian_implan();
			  }
			});
		}else{

		}
	});
	function hitung_perkalian_implan(){
		$("#total_implan").val(parseFloat($("#kuantitas_implan").val())*parseFloat($("#harga_implan").val()));
	}
	$("#kuantitas_implan").keyup(function(){
		if ($(this).val()==''){
			$(this).val(0);
		}
		hitung_perkalian_implan();
	});
	$("#add_implan").click(function() {
			if(!validate_implan()) return false;
			var duplicate = false;
			var content = "";

			if($("#nomor_implan").val() != ''){
					var no = $("#nomor_implan").val();
			}else{
				// var no = $('#manage-tabel-nonRacikan tr').length - 2;
				var no = $('#auto_number_implan').val();
				var content = "<tr>";
				$('#tabel_implan tbody tr').filter(function (){
					var $cells = $(this).children('td');
					if($cells.eq(2).text() === $("#id_implan").val() && $cells.eq(1).text() === $("#idunit_implan").val()){
						// big_notification("Barang sudah ada didalam daftar","error");
						duplicate = true;
						sweetAlert("Duplicate...", "Data Barang Duplicate!", "error");
					}
				});
			}
			if(duplicate) return false;

			content += "<td style='display:none'>" + no + "</td>";//0 nomor
			content += "<td style='display:none'>" + $("#idunit_implan").val(); + "</td>";//1 UNIT
			content += "<td style='display:none'>" + $("#id_implan").val() + "</td>"; //2 ID BARANG
			content += "<td>" + $("#idunit_implan option:selected").text() + "</td>";//3 Nama
			content += "<td>" + $("#id_implan option:selected").text() + "</td>";//4 Nama
			content += "<td>" + $("#satuan_implan").val() + "</td>";//5 Nama
			content += "<td align='right'>" + formatNumber($("#harga_implan").val()) + "</td>";//6 Nama
			content += "<td align='right'>" + formatNumber($("#kuantitas_implan").val()) + "</td>";//7 Nama
			content += "<td align='right'>" + formatNumber($("#total_implan").val()) + "</td>";//8 Nama
			content += "<td>" + $("#user_implan").val() + "</td>";//9 Nama
			content += "<td style='display:none'></td>"; //10 KOSONG
			content += "<td style='display:none'></td>"; //11 KOSONG
			content += "<td style='display:none'></td>"; //12 KOSONG
			content += "<td ><button type='button' class='btn btn-sm btn-info edit_implan'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus_implan'><i class='glyphicon glyphicon-remove'></i></button></td>";
			content += '<td style="display:none"><input type="text" name="idunit_implan_detail[]" value="'+ $("#idunit_implan").val() +'" ></td>';//14 Persen
			content += '<td style="display:none"><input type="text" name="id_implan_detail[]" value="'+ $("#id_implan").val() +'" ></td>';//15 Persen
			content += '<td style="display:none"><input type="text" name="harga_implan_detail[]" value="'+ $("#harga_implan").val() +'" ></td>';//16 Persen
			content += '<td style="display:none"><input type="text" name="kuantitas_implan_detail[]" value="'+ $("#kuantitas_implan").val() +'" ></td>';//17 Persen
			content += '<td style="display:none"><input type="text" name="total_implan_detail[]" value="'+ $("#total_implan").val() +'" ></td>';//18 Persen
			content += '<td style="display:none"><input type="text" name="harga_dasar_implan_detail[]" value="'+ $("#harga_dasar_implan").val() +'" ></td>';//19 Persen
			content += '<td style="display:none"><input type="text" name="margin_implan_detail[]" value="'+ $("#margin_implan").val() +'" ></td>';//20 Persen
			content += '<td style="display:none"><input type="text" name="idtipe_implan_detail[]" value="'+ $("#idtipe_implan").val() +'" ></td>';//21 Persen
			content += '<td style="display:none"><input type="text" name="id_trx_implan_detail[]" value="'+ $("#id_trx_implan").val() +'" ></td>';//21 Persen
			content += '<td style="display:none"><input type="text" name="st_edit_implan[]" value="1" ></td>';//21 Persen
			// alert(content);
			if($("#row_implan").val() != ''){
				$('#tabel_implan tbody tr:eq(' + $("#row_implan").val() + ')').html(content);
			}else{
				content += "</tr>";
				$('#tabel_implan tbody').append(content);
			}

			$(".edit_implan").attr('disabled',false);
			$(".hapus_implan").attr('disabled',false);
			set_config_number();
			$("#auto_number_implan").val(parseInt($("#auto_number_implan").val()) + 1);
			clear_implan();
	});
	$(document).on("click",".edit_implan",function(){
		$(".edit_implan").attr('disabled',true);
		$(".hapus_implan").attr('disabled',true);
		$idunit_implan = $(this).closest('tr').find("td:eq(1)").html();
		var id_implan = $(this).closest('tr').find("td:eq(2)").html();
		var nama = $(this).closest('tr').find("td:eq(4)").html();
		var satuan = $(this).closest('tr').find("td:eq(5)").html();
		var tipe = $(this).closest('tr').find("td:eq(21) input").val();

		var data2 = {
			id: id_implan,
			text: nama,
			tipe: tipe
		};
		var newOption = new Option(data2.text, data2.id, true, true);
		$('#id_implan').append(newOption);
		$("#id_trx_implan").val($(this).closest('tr').find("td:eq(22) input").val());
		$("#satuan_implan").val(satuan);
		$("#idtipe_implan").val(tipe);
		$("#idunit_implan").val($idunit_implan).trigger('change');
		// $("#id_implan").val(id_implan).trigger('change');
		$("#kuantitas_implan").val($(this).closest('tr').find("td:eq(17) input").val());
		$("#harga_implan").val($(this).closest('tr').find("td:eq(16) input").val());
		$("#nomor_implan").val($(this).closest('tr').find("td:eq(0)").html());
		$("#row_implan").val($(this).closest('tr')[0].sectionRowIndex);
		if ($("#id_trx_implan").val()!='0'){
			$(".disable_implan").attr('disabled',true);
		}else{
			$(".disable_implan").attr('disabled',false);
		}
		hitung_perkalian_implan();


	});
	$(document).on("click",".hapus_implan",function(){
		var td=$(this).closest('td');
		var tr=$(this).closest('tr');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			if (tr.find("td:eq(22) input").val()!='0'){
				var content = "<tr>";
				content += '<td ><input type="text" name="jenis_hapus[]" value="implan" ></td>';
				content += '<td ><input type="text" name="id_hapus[]" value="'+ tr.find("td:eq(22) input").val() +'" ></td>';
				content += "</tr>";
				$('#tabel_delete tbody').append(content);
			}

			td.parent().remove();
			clear_implan();
		});


	});
	$(document).on("click","#cancel_implan",function(){
		clear_implan();
	});
	function validate_implan()
	{
		if($("#idunit_implan").val() == "#"){
			sweetAlert("Maaf...", "Unit Harus Diisi!", "error");
			$("#idunit_implan").focus();
			return false;
		}
		if($("#id_implan").val() == "" && $("#id_implan").val() == null){
			sweetAlert("Maaf...", "Barang Harus Diisi!", "error");
			$("#id_implan").focus();
			return false;
		}
		if($("#total_implan").val() == "0"){
			sweetAlert("Maaf...", "Total Barang Harus Diisi!", "error");
			$("#total_implan").focus();
			return false;
		}
		return true;
	}
	function clear_implan(){
		$("#id_trx_implan").val('0');
		$("#row_implan").val('');
		$("#nomor_implan").val('');
		$("#satuan_implan").val('');
		$("#harga_implan").val('');
		$("#total_implan").val('');
		$("#margin_implan").val('');
		$("#idtipe_implan").val('');
		$("#kuantitas_implan").val('');
		$("#id_implan").val(null).trigger('change');
		$(".edit_implan").attr('disabled',false);
		$(".hapus_implan").attr('disabled',false);
		$(".disable_implan").attr('disabled',false);
		total_implan();
	}
	function total_implan()
	{
		var total_grand = 0;
		$('#tabel_implan tbody tr').each(function() {
			total_grand +=parseFloat($(this).find('td:eq(18) input').val());
		});
		console.log(total_grand);
		$("#gt_implan").val(total_grand);
		$("#lbl_gt_implan").text(formatNumber(total_grand));

	}
	$("#btn_search_implan").click(function() {
		$("#idtipe_pilih").val('2');
		$("#idkat2").val('');
		$("#form_pilih").val('implan');
		$("#idunit_pilih").val($("#idunit_implan").val()).trigger('change');
		$("#modal_barang").modal('show');
		// loadBarang();
	});
	$("#btn_search_alat").click(function() {
		loadSewa();
		$("#modal_sewa").modal('show');
	});



	// END IMPLAN

	// CODING NARCOSE
		$("#id_narcose").select2({
			minimumInputLength: 2,
			noResults: 'Dokter Tidak Ditemukan.',
		// allowClear: true
		// tags: [],
		// var idunit=$("#idunit_narcose").val();
		ajax: {
			url: '{site_url}tko/js_obat_narcose/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,
		 data: function (params) {
			  var query = {
				search: params.term,idtipe:'3',idunit:$("#idunit_narcose").val(),
			  }
			  return query;
			},
			processResults: function (data) {
				console.log($("#idunit_narcose").val());
				return {
					results: $.map(data, function (item) {
						return {
							text: item.nama,
							id: item.idbarang,
							tipe : item.idtipe
						}
					})
				};
			}
		}
	});
	$("#id_narcose").change(function(){
		var idtipe;
		var data;
		data=$("#id_narcose").select2('data')[0];
		if (data != null){
			idtipe=data.tipe;
		}else{
			idtipe='3';
		}
		// alert(idtipe);
		// console.log(idtipe);
		if ($(this).val()!=null){
			$.ajax({
			  url: '{site_url}tko/get_barang/'+$(this).val()+'/'+idtipe+'/'+$("#idkelompokpasien").val(),
			  dataType: "json",
			  success: function(data) {
				  console.log(data);
				$("#idtipe_narcose").val(data.idtipe);
				$("#margin_narcose").val(data.margin);
				$("#harga_dasar_narcose").val(data.hargadasar);
				$("#satuan_narcose").val(data.satuan);
				$("#harga_narcose").val(ceilling(parseFloat(data.hargadasar)+parseFloat(data.hargadasar*data.margin/100)));
				console.log(parseFloat(data.hargadasar)+parseFloat(data.hargadasar*data.margin/100));
				$("#kuantitas_narcose").focus();
				hitung_perkalian_narcose();
			  }
			});
		}else{

		}
	});
	function hitung_perkalian_narcose(){
		$("#total_narcose").val(parseFloat($("#kuantitas_narcose").val())*parseFloat($("#harga_narcose").val()));
	}
	$("#kuantitas_narcose").keyup(function(){
		if ($(this).val()==''){
			$(this).val(0);
		}
		hitung_perkalian_narcose();
	});
	$("#add_narcose").click(function() {
			if(!validate_narcose()) return false;
			var duplicate = false;
			var content = "";

			if($("#nomor_narcose").val() != ''){
					var no = $("#nomor_narcose").val();
			}else{
				// var no = $('#manage-tabel-nonRacikan tr').length - 2;
				var no = $('#auto_number_narcose').val();
				var content = "<tr>";
				$('#tabel_narcose tbody tr').filter(function (){
					var $cells = $(this).children('td');
					if($cells.eq(2).text() === $("#id_narcose").val() && $cells.eq(1).text() === $("#idunit_narcose").val()){
						// big_notification("Barang sudah ada didalam daftar","error");
						duplicate = true;
						sweetAlert("Duplicate...", "Data Barang Duplicate!", "error");
					}
				});
			}
			if(duplicate) return false;

			content += "<td style='display:none'>" + no + "</td>";//0 nomor
			content += "<td style='display:none'>" + $("#idunit_narcose").val(); + "</td>";//1 UNIT
			content += "<td style='display:none'>" + $("#id_narcose").val() + "</td>"; //2 ID BARANG
			content += "<td>" + $("#idunit_narcose option:selected").text() + "</td>";//3 Nama
			content += "<td>" + $("#id_narcose option:selected").text() + "</td>";//4 Nama
			content += "<td>" + $("#satuan_narcose").val() + "</td>";//5 Nama
			content += "<td align='right'>" + formatNumber($("#harga_narcose").val()) + "</td>";//6 Nama
			content += "<td align='right'>" + formatNumber($("#kuantitas_narcose").val()) + "</td>";//7 Nama
			content += "<td align='right'>" + formatNumber($("#total_narcose").val()) + "</td>";//8 Nama
			content += "<td>" + $("#user_narcose").val() + "</td>";//9 Nama
			content += "<td style='display:none'></td>"; //10 KOSONG
			content += "<td style='display:none'></td>"; //11 KOSONG
			content += "<td style='display:none'></td>"; //12 KOSONG
			content += "<td ><button type='button' class='btn btn-sm btn-info edit_narcose'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus_narcose'><i class='glyphicon glyphicon-remove'></i></button></td>";
			content += '<td style="display:none"><input type="text" name="idunit_narcose_detail[]" value="'+ $("#idunit_narcose").val() +'" ></td>';//14 Persen
			content += '<td style="display:none"><input type="text" name="id_narcose_detail[]" value="'+ $("#id_narcose").val() +'" ></td>';//15 Persen
			content += '<td style="display:none"><input type="text" name="harga_narcose_detail[]" value="'+ $("#harga_narcose").val() +'" ></td>';//16 Persen
			content += '<td style="display:none"><input type="text" name="kuantitas_narcose_detail[]" value="'+ $("#kuantitas_narcose").val() +'" ></td>';//17 Persen
			content += '<td style="display:none"><input type="text" name="total_narcose_detail[]" value="'+ $("#total_narcose").val() +'" ></td>';//18 Persen
			content += '<td style="display:none"><input type="text" name="harga_dasar_narcose_detail[]" value="'+ $("#harga_dasar_narcose").val() +'" ></td>';//19 Persen
			content += '<td style="display:none"><input type="text" name="margin_narcose_detail[]" value="'+ $("#margin_narcose").val() +'" ></td>';//20 Persen
			content += '<td style="display:none"><input type="text" name="idtipe_narcose_detail[]" value="'+ $("#idtipe_narcose").val() +'" ></td>';//21 Persen
			content += '<td style="display:none"><input type="text" name="id_trx_narcose_detail[]" value="'+ $("#id_trx_narcose").val() +'" ></td>';//21 Persen
			content += '<td style="display:none"><input type="text" name="st_edit_narcose[]" value="1" ></td>';//22 Edit
			// alert(content);
			if($("#row_narcose").val() != ''){
				$('#tabel_narcose tbody tr:eq(' + $("#row_narcose").val() + ')').html(content);
			}else{
				content += "</tr>";
				$('#tabel_narcose tbody').append(content);
			}

			$(".edit_narcose").attr('disabled',false);
			$(".hapus_narcose").attr('disabled',false);
			set_config_number();
			$("#auto_number_narcose").val(parseInt($("#auto_number_narcose").val()) + 1);
			clear_narcose();
	});
	$(document).on("click",".edit_narcose",function(){
		$(".edit_narcose").attr('disabled',true);
		$(".hapus_narcose").attr('disabled',true);
		$idunit_narcose = $(this).closest('tr').find("td:eq(1)").html();
		var id_narcose = $(this).closest('tr').find("td:eq(2)").html();
		var nama = $(this).closest('tr').find("td:eq(4)").html();
		var satuan = $(this).closest('tr').find("td:eq(5)").html();
		var tipe = $(this).closest('tr').find("td:eq(21) input").val();

		var data2 = {
			id: id_narcose,
			text: nama,
			tipe: tipe
		};
		var newOption = new Option(data2.text, data2.id, true, true);
		$('#id_narcose').append(newOption);

		$("#satuan_narcose").val(satuan);
		$("#idtipe_narcose").val(tipe);
		$("#idunit_narcose").val($idunit_narcose).trigger('change');
		// $("#id_narcose").val(id_narcose).trigger('change');
		$("#id_trx_narcose").val($(this).closest('tr').find("td:eq(22) input").val());
		$("#kuantitas_narcose").val($(this).closest('tr').find("td:eq(17) input").val());
		$("#harga_narcose").val($(this).closest('tr').find("td:eq(16) input").val());
		$("#nomor_narcose").val($(this).closest('tr').find("td:eq(0)").html());
		$("#row_narcose").val($(this).closest('tr')[0].sectionRowIndex);
		if ($("#id_trx_narcose").val()!='0'){
			$(".disable_narcose").attr('disabled',true);
		}else{
			$(".disable_narcose").attr('disabled',false);
		}
		hitung_perkalian_narcose();


	});
	$(document).on("click",".hapus_narcose",function(){
		var td=$(this).closest('td');
		var tr=$(this).closest('tr');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			if (tr.find("td:eq(22) input").val()!='0'){
				var content = "<tr>";
				content += '<td ><input type="text" name="jenis_hapus[]" value="narcose" ></td>';
				content += '<td ><input type="text" name="id_hapus[]" value="'+ tr.find("td:eq(22) input").val() +'" ></td>';
				content += "</tr>";
				$('#tabel_delete tbody').append(content);
			}
			td.parent().remove();
			clear_narcose();
		});


	});
	$(document).on("click","#cancel_narcose",function(){
		clear_narcose();
	});
	function validate_narcose()
	{
		if($("#idunit_narcose").val() == "#"){
			sweetAlert("Maaf...", "Unit Harus Diisi!", "error");
			$("#idunit_narcose").focus();
			return false;
		}
		if($("#id_narcose").val() == "" && $("#id_narcose").val() == null){
			sweetAlert("Maaf...", "Barang Harus Diisi!", "error");
			$("#id_narcose").focus();
			return false;
		}
		if($("#total_narcose").val() == "0"){
			sweetAlert("Maaf...", "Total Barang Harus Diisi!", "error");
			$("#total_narcose").focus();
			return false;
		}
		return true;
	}
	function clear_narcose(){
		$("#id_trx_narcose").val('0');
		$("#row_narcose").val('');
		$("#nomor_narcose").val('');
		$("#satuan_narcose").val('');
		$("#harga_narcose").val('');
		$("#total_narcose").val('');
		$("#margin_narcose").val('');
		$("#idtipe_narcose").val('');
		$("#kuantitas_narcose").val('');
		$("#id_narcose").val(null).trigger('change');
		$(".edit_narcose").attr('disabled',false);
		$(".hapus_narcose").attr('disabled',false);
		$(".disable_narcose").attr('disabled',false);
		total_narcose();
	}
	function total_narcose()
	{
		var total_grand = 0;
		$('#tabel_narcose tbody tr').each(function() {
			total_grand +=parseFloat($(this).find('td:eq(18) input').val());
		});
		console.log(total_grand);
		$("#gt_narcose").val(total_grand);
		$("#lbl_gt_narcose").text(formatNumber(total_grand));

	}
	$("#btn_search_narcose").click(function() {
		$("#idtipe_pilih").val('3');
		$("#form_pilih").val('narcose');
		$("#idkat2").val('00001');
		// alert($("#idkat2").val());
		$("#modal_barang").modal('show');
		$("#idunit_pilih").val($("#idunit_narcose").val()).trigger('change');
		// loadBarang();
	});
	$("#btn_filter_barang2").click(function() {
		loadBarang();
	});


	// END NARCOSE
	function loadBarang() {
		var idunit=$("#idunit_pilih").val();
		var idtipe=$("#idtipe_pilih").val();
		var idkategori=$("#idkat2").val();
		// alert(idkategori);
		$('#tabel_cari_barang').DataTable().destroy();
		var table = $('#tabel_cari_barang').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}tko/get_data_obat/',
			type: "POST",
			dataType: 'json',
			data: {
				idunit: idunit,
				idtipe: idtipe,
				idkategori: idkategori,
			}
		},
		columnDefs: [
						{ "width": "5%", "targets": [0], "orderable": true },
						{ "width": "10%", "targets": [1,3,4], "orderable": true },
						{ "width": "15%", "targets": [5], "orderable": true },
						{ "width": "40%", "targets": [2], "orderable": true },
					]
		});
	}
	function loadSewa() {
    var idkelompokpasien = $("#idkelompokpasien").val();

		$('#tabel_cari_sewa').DataTable().destroy();
		var table = $('#tabel_cari_sewa').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}tko/get_data_sewa/' + idkelompokpasien,
			type: "POST",
			dataType: 'json'

		},
		columnDefs: [
						{ "width": "5%", "targets": [0], "orderable": true,"visible":false },
						{ "width": "10%", "targets": [1], "orderable": false },
						{ "width": "45%", "targets": [2], "orderable": true },
						{ "width": "15%", "targets": [3], "orderable": false },
					]
		});
	}
	$(document).on("click",".selectObat2",function(){
		clear_obat();
		var idobat = ($(this).data('idobat'));
		var idtipe = ($(this).data('idtipe'));

		// alert(data2);exit();


		if ($(this).val()!=null){
			$.ajax({
			  url: '{site_url}tko/get_barang/'+idobat+'/'+idtipe+'/'+$("#idkelompokpasien").val(),
			  dataType: "json",
			  success: function(data) {
				  console.log(data);
				if ($("#form_pilih").val()=='obat'){
					var data2 = {
						id: idobat,
						text: data.nama,
						tipe: data.idtipe
					};
					var newOption = new Option(data2.text, data2.id, true, true);
					$('#id_obat').append(newOption);
					// $("#idunit_obat").val($("#idunit_pilih").val()).trigger('change');
					$("#idtipe_obat").val(data.idtipe);
					$("#margin_obat").val(data.margin);
					$("#harga_dasar_obat").val(data.hargadasar);
					$("#satuan_obat").val(data.satuan);
					$("#harga_obat").val(ceilling(parseFloat(data.hargadasar)+parseFloat(data.hargadasar*data.margin/100)));
					$("#kuantitas_obat").focus();
				}
				if ($("#form_pilih").val()=='alkes'){
					// alert('sini');
					var data2 = {
						id: idobat,
						text: data.nama,
						tipe: data.idtipe
					};
					var newOption = new Option(data2.text, data2.id, true, true);
					$('#id_alkes').append(newOption);
					// $("#idunit_alkes").val($("#idunit_pilih").val()).trigger('change');
					$("#idtipe_alkes").val(data.idtipe);
					$("#margin_alkes").val(data.margin);
					$("#harga_dasar_alkes").val(data.hargadasar);
					$("#satuan_alkes").val(data.satuan);
					$("#harga_alkes").val(ceilling(parseFloat(data.hargadasar)+parseFloat(data.hargadasar*data.margin/100)));
					$("#kuantitas_alkes").focus();
				}
				if ($("#form_pilih").val()=='implan'){
					// alert('sini');
					var data2 = {
						id: idobat,
						text: data.nama,
						tipe: data.idtipe
					};
					var newOption = new Option(data2.text, data2.id, true, true);
					$('#id_implan').append(newOption);
					// $("#idunit_implan").val($("#idunit_pilih").val()).trigger('change');
					$("#idtipe_implan").val(data.idtipe);
					$("#margin_implan").val(data.margin);
					$("#harga_dasar_implan").val(data.hargadasar);
					$("#satuan_implan").val(data.satuan);
					$("#harga_implan").val(ceilling(parseFloat(data.hargadasar)+parseFloat(data.hargadasar*data.margin/100)));
					$("#kuantitas_implan").focus();
				}
				if ($("#form_pilih").val()=='narcose'){
					// alert('sini');
					var data2 = {
						id: idobat,
						text: data.nama,
						tipe: data.idtipe
					};
					var newOption = new Option(data2.text, data2.id, true, true);
					$('#id_narcose').append(newOption);
					// $("#idunit_narcose").val($("#idunit_pilih").val()).trigger('change');
					$("#idtipe_narcose").val(data.idtipe);
					$("#margin_narcose").val(data.margin);
					$("#harga_dasar_narcose").val(data.hargadasar);
					$("#satuan_narcose").val(data.satuan);
					$("#harga_narcose").val(ceilling(parseFloat(data.hargadasar)+parseFloat(data.hargadasar*data.margin/100)));
					$("#kuantitas_narcose").focus();
				}

			  }
			});
		}

	});
	$(document).on("click",".pilih_sewa",function(){
		var table = $('#tabel_cari_sewa').DataTable();
        tr = table.row( $(this).parents('tr') ).index();
		var idsewa=table.cell(tr,0).data();

		$("#id_alat").val(idsewa).trigger('change');
		$("#modal_sewa").modal('hide');
		return false;
	});
	$("#idunit_pilih").change(function(){
		if ($("#form_pilih").val()=='obat'){
			$("#idunit_obat").val($("#idunit_pilih").val()).trigger('change');
		}
		if ($("#form_pilih").val()=='alkes'){
			$("#idunit_alkes").val($("#idunit_pilih").val()).trigger('change');
		}
		if ($("#form_pilih").val()=='implan'){
			$("#idunit_implan").val($("#idunit_pilih").val()).trigger('change');
		}
		if ($("#form_pilih").val()=='narcose'){
			$("#idunit_narcose").val($("#idunit_pilih").val()).trigger('change');
		}
		loadBarang();
	});

	//SEWA ALAT
	$("#add_alat").click(function() {
			if(!validate_alat()) return false;
			var duplicate = false;
			var content = "";

			if($("#nomor_alat").val() != ''){
					var no = $("#nomor_alat").val();
			}else{
				// var no = $('#manage-tabel-nonRacikan tr').length - 2;
				var no = $('#auto_number_alat').val();
				var content = "<tr>";
				$('#tabel_alat tbody tr').filter(function (){
					var $cells = $(this).children('td');
					if($cells.eq(1).text() === $("#id_alat").val()){
						// big_notification("Barang sudah ada didalam daftar","error");
						duplicate = true;
						sweetAlert("Duplicate...", "Data Dokter Duplicate!", "error");
					}
				});
			}
			if(duplicate) return false;

			content += "<td style='display:none'>" + no + "</td>";//0 nomor
			content += "<td style='display:none'>" + $("#id_alat").val() + "</td>"; //1
			content += "<td>" + $("#id_alat option:selected").text() + "</td>";//2 Nama
			content += '<td><input type="text" name="jenis_operasi_alat_detail[]" class="form-control jenis_operasi_do"  value="'+$("#jenis_operasi_alat").val()+'" readonly></td>';
			content += '<td><input type="text" name="kelas_operasi_alat_detail[]"  class="form-control kelas_operasi_alat"  value="'+$("#kelas_operasi_alat").val()+'" readonly></td>';
			content += '<td><input type="text" name="tarif_alat_detail[]"  class="form-control number"  value="'+$("#tarif_alat").val()+'" readonly></td>';//5
			content += '<td><input type="text" name="kuantitas_alat_detail[]" class="form-control number" value="'+$("#kuantitas_alat").val()+'" readonly></td>';//6
			content += '<td><input type="text" name="total_alat_detail[]" class="form-control number"  value="'+$("#total_alat").val()+'" readonly></td>';//7
			content += '<td><input type="text" name="user_alat_detail[]" class="form-control"  value="'+$("#user_alat").val()+'" readonly></td>';//8
			content += "<td ><button type='button' class='btn btn-sm btn-info edit_alat'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus_alat'><i class='glyphicon glyphicon-remove'></i></button></td>";
			content += '<td style="display:none"><input type="text" name="sarana_alat_detail[]" value="'+$("#sarana_alat").val()+'" readonly></td>';//9
			content += '<td style="display:none"><input type="text" name="pelayanan_alat_detail[]" value="'+$("#pelayanan_alat").val()+'" readonly></td>';//10
			content += '<td style="display:none"><input type="text" name="bhp_alat_detail[]" value="'+$("#bhp_alat").val()+'" readonly></td>';//11
			content += '<td style="display:none"><input type="text" name="biayaperawatan_alat_detail[]" value="'+$("#perawatan_alat").val()+'" readonly></td>';//11
			content += '<td style="display:none"><input type="text" name="id_alat_detail[]" value="'+$("#id_alat").val()+'" readonly></td>';//12
			content += '<td style="display:none"><input type="text" name="id_alat_trx_detail[]" value="'+$("#id_alat_trx").val()+'" readonly></td>';//12
			content += '<td style="display:none"><input type="text" name="st_edit_alat[]" value="1" readonly></td>';//12


			// alert(content);

			if($("#row_alat").val() != ''){
				$('#tabel_alat tbody tr:eq(' + $("#row_alat").val() + ')').html(content);
			}else{
				content += "</tr>";
				$('#tabel_alat tbody').append(content);
			}

			$(".edit_alat").attr('disabled',false);
			$(".hapus_alat").attr('disabled',false);
			$('.number').number(true, 0, '.', ',');
			$("#auto_number_alat").val(parseInt($("#auto_number_alat").val()) + 1);
			clear_alat();
	});
	function update_detail_alat()
	{
		var gt_alat=0;
		$('#tabel_alat tbody tr').each(function() {
			// total_grand +=parseFloat($(this).find('td:eq(7) input').val());
			var jenis_operasi_id = $("#jenis_operasi_id").val();
			var kelas_tarif_id = $("#kelas_tarif_id").val();
			var id_alat =$(this).find('td:eq(1)').html();
			var td=$(this);
			// console.log(id_alat);

				$.ajax({
					url: '{site_url}tko/load_tarif_sewa/',
					type: "POST",
					dataType: 'json',
					data: {
						jenis_operasi_id: jenis_operasi_id,
						kelas_tarif_id: kelas_tarif_id,
						id_alat: id_alat,

					},
					success: function(data) {

						if (data=='error') {
							td.find('td:eq(5) input').val(0)

							td.find('td:eq(7) input').val(0)
							td.find('td:eq(9) input').val(0)
							td.find('td:eq(10) input').val(0)
							td.find('td:eq(11) input').val(0)
							td.find('td:eq(12) input').val(0)
						}else{
							td.find('td:eq(5) input').val(data.total)
							var jml=td.find('td:eq(6) input').val();
							// console.log(jml);
							td.find('td:eq(7) input').val(parseFloat(data.total)*parseFloat(jml))
							td.find('td:eq(9) input').val(data.jasasarana)
							td.find('td:eq(10) input').val(data.jasapelayanan)
							td.find('td:eq(11) input').val(data.bhp)
							td.find('td:eq(12) input').val(data.biayaperawatan)
							gt_alat +=parseFloat(data.total)*parseFloat(jml);
						}
						$('.number').number(true, 0, '.', ',');
						$("#gt_alat").val(gt_alat);
						$("#lbl_gt_alat").text(formatNumber(gt_alat));
						// clear_do();

						// console.log(data);
					}
				});

		});
		// total_alat();
	}
	$("#kuantitas_alat").keyup(function(){
		hitung_perkalian_alat();
	});
	function hitung_perkalian_alat(){
		$("#total_alat").val(parseFloat($("#kuantitas_alat").val())*parseFloat($("#tarif_alat").val()));
	}
	$(document).on("change", "#id_alat", function() {
		load_tarif_sewa();
	});
	function validate_final()
	{
		// alert($("#id_alat").val());
		if ($("#jenis_operasi_id").val()=='0'){
			sweetAlert("Maaf...", "Tentukan Jenis Operasi", "error");
			return false;
		}
		if ($("#kelas_tarif_id").val()=='0'){
			sweetAlert("Maaf...", "Tentukan Tarif Operasi", "error");
			return false;
		}
		if ($("#ruang_id").val()=='0'){
			sweetAlert("Maaf...", "Tentukan Ruang Operasi", "error");
			return false;
		}
		if ($("#diagnosa").val()==''){
			sweetAlert("Maaf...", "Tentukan Diagnosa", "error");
			return false;
		}
		if ($("#operasi").val()==''){
			sweetAlert("Maaf...", "Tentukan Operasi", "error");
			return false;
		}
		if ($("#tanggaloperasi").val()==''){
			sweetAlert("Maaf...", "Tentukan Tanggal Operasi", "error");
			return false;
		}
		if ($("#waktuawal").val()==''){
			sweetAlert("Maaf...", "Tentukan Waktu Operasi", "error");
			return false;
		}
		if ($("#waktuakhir").val()==''){
			sweetAlert("Maaf...", "Tentukan Waktu Selesai Operasi", "error");
			return false;
		}
		if ($("#persen_da").val() > 0 && $("#total_da").val() > 0 && $("#gt_persen_da").val() < 100){
			sweetAlert("Maaf...", "Persentase Dokter Anathesi Belum Lengkap", "error");
			return false;
		}
		if ($("#persen_daa").val() > 0 && $("#total_daa").val() > 0 && $("#gt_persen_daa").val() < 100){
			sweetAlert("Maaf...", "Persentase Asisten Anathesi Belum Lengkap", "error");
			return false;
		}
		if ($("#persen_dao").val() > 0 && $("#total_dao").val() > 0 && $("#gt_persen_dao").val() < 100){
			sweetAlert("Maaf...", "Persentase Asisten Operator Belum Lengkap", "error");
			return false;
		}
		$("*[disabled]").not(true).removeAttr("disabled");
		$("#simpan_tko").hide();
		$("#cover-spin").show();
		return true;
	}
	function validate_alat()
	{
		// alert($("#id_alat").val());
		if ($("#id_alat").val()=='0'){
			sweetAlert("Maaf...", "Alat Tersebut tidak Memiliki Tarif!", "error");
			return false;
		}
		if ($("#id_alat").val()=='#'){
			sweetAlert("Maaf...", "Pilih Alat Dengan benar!", "error");
			return false;
		}

		if (parseFloat($("#total_alat").val())=='0'){
			sweetAlert("Maaf...", "Isi Biaya Dengan benar!", "error");
			return false;
		}
		return true;
	}
	function clear_alat(){
		$("#id_alat_trx").val('0');
		$("#row_alat").val('');
		$("#nomor_alat").val('');
		$("#id_alat").val("#").trigger('change');
		$("#kuantitas_alat").val('');
		$("#total_alat").val('');
		$(".edit_alat").attr('disabled',false);
		$(".hapus_alat").attr('disabled',false);
		total_alat();
	}
	function total_alat()
	{
		var total_grand = 0;
		$('#tabel_alat tbody tr').each(function() {
			total_grand +=parseFloat($(this).find('td:eq(7) input').val());
		});
		// console.log(total_grand);
		$("#gt_alat").val(total_grand);
		$("#lbl_gt_alat").text(formatNumber(total_grand));

	}

	$("#cancel_alat").click(function() {
		clear_alat();
	});
	$(document).on("click",".edit_alat",function(){
		$(".edit_alat").attr('disabled',true);
		$(".hapus_alat").attr('disabled',true);
		$id = $(this).closest('tr').find("td:eq(1)").html();

		$("#id_alat").val($id).trigger('change');
		$("#id_alat_trx").val($(this).closest('tr').find("td:eq(15) input").val());
		$("#tarif_alat").val($(this).closest('tr').find("td:eq(5) input").val());
		$("#kuantitas_alat").val($(this).closest('tr').find("td:eq(6) input").val());
		$("#total_alat").val($(this).closest('tr').find("td:eq(7) input").val());
		$("#nomor_alat").val($(this).closest('tr').find("td:eq(0)").html());


		$("#sarana_alat").val($(this).closest('tr').find("td:eq(9) input").val());
		$("#pelayanan_alat").val($(this).closest('tr').find("td:eq(10) input").val());
		$("#bhp_alat").val($(this).closest('tr').find("td:eq(11) input").val());
		$("#perawatan_alat").val($(this).closest('tr').find("td:eq(12) input").val());
		$("#row_alat").val($(this).closest('tr')[0].sectionRowIndex);
		$("#kuantitas_alat").focus();

	});
	$(document).on("click",".hapus_alat",function(){
		var td=$(this).closest('td');
		var tr=$(this).closest('tr');
		// var td=$(this).closest('td');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			if (tr.find("td:eq(22) input").val()!='0'){
				var content = "<tr>";
				content += '<td ><input type="text" name="jenis_hapus[]" value="alat" ></td>';
				content += '<td ><input type="text" name="id_hapus[]" value="'+ tr.find("td:eq(15) input").val() +'" ></td>';
				content += "</tr>";
				$('#tabel_delete tbody').append(content);
			}
			td.parent().remove();
			clear_alat();
		});


	});
	function load_kelompok_tindakan(){
		// alert($jenis);
		$id=$("#id_tindakan").val();
		arr_tindakan=[];
		var kt;

		kt=$('#kelompok_tindakan_id');

		kt.empty();
		// $('#Tkelompok_tindakan').empty();
		$.ajax({
			url: '{site_url}tko/kelompok_tindakan_list/'+$id,
			dataType: "json",
			success: function(data) {
				 $.each(data, function (i,item) {
					var newOption = new Option(item.text, item.id, true, true);

					kt.append(newOption);
					kt.val(null).trigger('change');
					if (item.selected){
						arr_tindakan.push(item.id);
					}
				});
				setTimeout(function() {
					kt.val(arr_tindakan).trigger('change');
				}, 200);

			}
		});

	}
	function load_kelompok_diagnosa(){
		$id=$("#id_tindakan").val();


		arr_diagnosa=[];
		var kt;

			kt=$('#kelompok_diagnosa_id');

		kt.empty();
		// $('#Tkelompok_tindakan').empty();
		$.ajax({
			url: '{site_url}tko/kelompok_diagnosa_list/'+$id,
			dataType: "json",
			success: function(data) {
				 $.each(data, function (i,item) {
					var newOption = new Option(item.text, item.id, false, false);
					kt.append(newOption).trigger('change');
					if (item.selected=='selected'){
						console.log(item.text);
						arr_diagnosa.push(item.id);
					}
					kt.val(null).trigger('change');
				});
				setTimeout(function() {
					kt.val(arr_diagnosa).trigger('change');
				}, 200);

			}
		});
	}
	function ceilling(num) {
		return Math.ceil(num/100)*100;
	}

</script>
