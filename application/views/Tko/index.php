<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>
<?php if (UserAccesForm($user_acces_form,array('282'))){ ?>
<div class="block">
    <div class="block-header">

        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:25px">
        <div class="row">
            <?php echo form_open('tko/filter', 'class="form-horizontal" id="form-work"'); ?>

            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">No Medrec</label>
					<div class="col-md-8">
						<input type="text" class="form-control" id="nomedrec" name="nomedrec" value="{nomedrec}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Nama Pasien</label>
					<div class="col-md-8">
						<input type="text" class="form-control" id="namapasien" name="namapasien" value="{namapasien}">
					</div>
				</div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Tipe</label>
                    <div class="col-md-8">
                        <select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;">
							<option value="#">Semua Tipe</option>
							<option value="1">Rawat Inap</option>
							<option value="2">ODS</option>
						</select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
		                <input class="form-control" type="text" id="tanggaldari" name="tanggaldari" placeholder="From" value="{tanggaldari}">
		                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
		                <input class="form-control" type="text" id="tanggalsampai" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
						</div>
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-4 control-label" for="waktu">Jam</label>
                    <div class="col-md-4">
                        <div class="bootstrap-timepicker timepicker">
									<input type="text" class="form-control" id="waktu" name="waktu" value="">
						</div>
					</div>
                </div>
                <?php if (UserAccesForm($user_acces_form,array('949'))){ ?>
                <div class="form-group" style="margin-bottom: 5px;" id="div_new">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" id="btn_filter" name="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				<?}?>

            </div>
			<div class="col-md-6">

                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label">Dokter Operator</label>
                    <div class="col-md-8">
                        <select name="iddokter_operator" id="iddokter_operator" class="js-select2 form-control" style="width: 100%;">
							<option value="#">Semua Dokter Operator</option>
							<?foreach ($dokterumum as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>
							<?}?>
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-4 control-label" for="tanggal">Dokter Anesthesi</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group">
                            <select name="iddokter_anestesi" id="iddokter_anestesi" class="js-select2 form-control" style="width: 100%;">
								<option value="#">Semua Dokter Anesthesi</option>
								<?foreach ($dokterumum as $row){?>
									<option value="<?=$row->id?>"><?=$row->nama?></option>
								<?}?>
							</select>

                        </div>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-4 control-label" for="tanggal">Status</label>
                    <div class="col-md-8">
                            <select name="status" id="status" class="js-select2 form-control" style="width: 100%;">
								<option value="#">Semua Status</option>
								<option value="1">Persetujuan - Pasien Belum Datang</option>
								<option value="2">Disetujui - Pasien Belum Datang</option>
								<option value="3">Persetujuan - Pasien Sudah Datang</option>
								<option value="4">Disetujui - Pasien Sudah Datang</option>
								<option value="5">Ditolak</option>
								<option value="6">Dibatalkan</option>
								<option value="7">Selesai</option>

							</select>

                    </div>
                </div>
				<?php if (UserAccesForm($user_acces_form,array('950','951'))){ ?>
				<div class="form-group" style="margin-bottom: 5px;" id="div_new">
                    <label class="col-md-4 control-label" for=""></label>
					<?php if (UserAccesForm($user_acces_form,array('950'))){ ?>
                    <div class="col-md-4">
                        <button class="btn btn-info text-uppercase" type="button" id="btn_add" name="btn_add" style="font-size:13px;width:100%;float:right;"><i class="fa fa-plus"></i> Jadwal</button>
                    </div>
					<?}?>
					<?php if (UserAccesForm($user_acces_form,array('951'))){ ?>
					<div class="col-md-4">
                        <button class="btn btn-primary text-uppercase" type="button" id="btn_gabung" name="btn_gabung" style="font-size:13px;width:100%;float:right;"><i class="fa fa-external-link-square"></i> Gabung </button>
                    </div>
					<?}?>
                </div>
				<?}?>
            </div>


            <?php echo form_close(); ?>

        </div>
    </div>
		<div class="block-content" id="div_tabel">
			<div class="table-responsive">
			<table class="table table-bordered table-striped" id="index_list" style="width: 100%;">
			<thead>
				<tr>
					<th style="width:5%">#</th>
					<th style="width:5%">#</th>
					<th style="width:5%">Tipe</th>
					<th style="width:15%">Tanggal</th>
					<th style="width:15%">Jam</th>
					<th style="width:15%">Ruang Operasi</th>
					<th style="width:15%">No Medrec</th>
					<th style="width:15%">Pasien</th>
					<th style="width:15%">Tgl Lahir</th>
					<th style="width:15%">Kelas - Bed</th>
					<th style="width:15%">Diagnosa</th>
					<th style="width:15%">Operasi</th>
					<th style="width:15%">Status</th>
					<th style="width:15%">Aksi</th>

				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>



</div>
<?}?>
<div class="modal" id="modalTambah" role="dialog" aria-hidden="true" aria-labelledby="modalAdd" font size="15">
    <div class="modal-dialog modal-lg" style="width:65%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Tambah Jadwal</h3>
                </div>
                    <form class="form-horizontal" id="form_detail_add">
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tnomedrec">No. Medrec</label>
								<div class="col-md-8">
									<input type="text" readonly class="form-control" id="tnomedrec" value="">
									<input type="hidden" readonly class="form-control" id="tid" value="">
									<input type="hidden" readonly class="form-control" id="tidpasien" value="">
									<input type="hidden" readonly class="form-control" id="tidpendaftaran" value="">
									<input type="hidden" readonly class="form-control" id="tidasalpendaftaran" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tnamapasien">Nama Pasien</label>
								<div class="col-md-8">
									<input type="text" readonly class="form-control" id="tnamapasien" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="ttanggallahir">Tanggal Lahir</label>
								<div class="col-md-8">
								<div class="js-datetimepicker input-group date">
									<input type="text"  disabled class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="ttanggallahir" value="">
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tumur">Umur Pasien</label>
								<div class="col-md-8">
									<input type="text" readonly class="form-control" id="tumur" value="">
								</div>
							</div>

							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tdpjp">DPJP</label>
								<div class="col-md-8">
									<select name="tdpjp" readonly id="tdpjp" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Semua Dokter</option>
										<?foreach ($dokterumum as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>

								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tt">Tanggal Operasi</label>
								<div class="col-md-8">
								<div class="js-datetimepicker input-group date">
									<input type="text" class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="ttanggaloperasi" value="">
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tt">Jam Operasi</label>
								<div class="col-md-8">
								<div class="js-datetimepicker input-group date">
									<input type="text" class="form-control"id="twaktu1" value="">
									<span class="input-group-addon">
										<span class="fa fa-angle-right"></span>
									</span>
									<input type="text" class="form-control" id="twaktu2" value="">
								</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tdiagnosa">Diagnosa</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="tdiagnosa" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="toperasi">Operasi</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="toperasi" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tkelas">Jenis Operasi</label>
								<div class="col-md-8">
									<select name="tjenis_operasi" id="tjenis_operasi" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Jenis Operasi</option>

										<?foreach ($mjenis_operasi as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tkelas">Jenis Anesthesi</label>
								<div class="col-md-8">
									<select name="tjenis_anestesi" id="tjenis_anestesi" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Jenis Anesthesi</option>

										<?foreach ($mjenis_anaesthesi as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tkelas">Kelompok Operasi</label>
								<div class="col-md-8">
									<select name="tkelompok_operasi" id="tkelompok_operasi" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Kelompok Operasi</option>
											<?foreach ($mkelompok_operasi as $row){?>
												<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tkelas">Kelompok Tindakan</label>
								<div class="col-md-8">
									<select  multiple="multiple"  name="tkelompok_tindakan[]" id="tkelompok_tindakan" class="js-select2 form-control" style="width: 100%;">

									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tpetugas_mengajukan">Petugas Mengajukan</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly id="tpetugas_mengajukan" value="<?=$this->session->userdata('user_name')?>">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="ttgl_mengajukan">Tanggal Mengajukan</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly id="ttgl_mengajukan" value="<?=date('d-m-Y H:i:s')?>">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tpetugas_menyetujui">Petugas Menyetujui</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly id="tpetugas_menyetujui" value="<?=$this->session->userdata('user_name')?>">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="ttgl_menyetujui">Tanggal & Jam Menyetujui</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly id="ttgl_menyetujui" value="<?=date('d-m-Y H:i:s')?>">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tkelompok_pasien">Kelompok Pasien</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly  id="tkelompok_pasien" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tasuransi">Perusahaan / Asuransi</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly  id="tasuransi" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="ttipe">Tipe</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly  id="ttipe" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tkelas">Kelas Perawatan</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly  id="tkelas" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tperujuk">Dokter Perujuk</label>
								<div class="col-md-8">
									<input type="text" class="form-control"  readonly id="tperujuk" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tkelas_tarif">Kelas Tarif</label>
								<div class="col-md-8">
									<select name="tkelas_tarif" id="tkelas_tarif" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Kelas Tarif</option>
										<?foreach ($mkelas as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="truang_operasi">Ruang Operasi</label>
								<div class="col-md-8">
									<select name="truang_operasi" id="truang_operasi" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Ruang Operasi</option>
										<?foreach ($mruangan as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tkelompok_diagnosa">Kelompok Diagnosa</label>
								<div class="col-md-8">
									<select multiple="multiple" name="tkelompok_diagnosa[]" id="tkelompok_diagnosa" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Kelompok Diagnosa</option>

									</select>
								</div>
							</div>


						</div>
						<div class="col-md-12">
							<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" for="tnamapasien">Catatan</label>
								<div class="col-md-10">
									<textarea class="form-control" name="tcatatan" id="tcatatan"></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">

						</div>
						<div class="col-md-12">
							<div class="form-group" style="margin-top: 10px;">
								<label class="col-md-2 control-label" for="stanggallahir"></label>
								<div class="col-md-10" style="float:left;">
									<button class="btn btn-sm btn-primary" type="button" id="btn_save" name="btn_save" ><i class="fa fa-save"></i> Simpan</button>
									<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
								</div>

							</div>
						</div>
                    </form>
            </div>
            <div class="modal-footer" >

            </div>
        </div>
    </div>
</div>
<div class="modal" id="modalEdit" role="dialog" aria-hidden="true" aria-labelledby="modalAdd" font size="15">
    <div class="modal-dialog modal-lg" style="width:65%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Edit Jadwal Operasi</h3>
                </div>
                    <form class="form-horizontal" id="form_detail_add">
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tnomedrec">No. Medrec</label>
								<div class="col-md-8">
									<input type="text" readonly class="form-control" id="Enomedrec" value="">
									<input type="hidden" readonly class="form-control" id="Eid" value="">
									<input type="hidden" readonly class="form-control" id="Eidpasien" value="">
									<input type="hidden" readonly class="form-control" id="Eidpendaftaran" value="">
									<input type="hidden" readonly class="form-control" id="Eidasalpendaftaran" value="">
									<input type="hidden" readonly class="form-control" id="st_acc" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tnamapasien">Nama Pasien</label>
								<div class="col-md-8">
									<input type="text" readonly class="form-control" id="Enamapasien" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="ttanggallahir">Tanggal Lahir</label>
								<div class="col-md-8">
								<div class="js-datetimepicker input-group date">
									<input type="text"  disabled class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="Etanggallahir" value="">
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tumur">Umur Pasien</label>
								<div class="col-md-8">
									<input type="text" readonly class="form-control" id="Eumur" value="">
								</div>
							</div>

							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tdpjp">DPJP</label>
								<div class="col-md-8">
									<select name="tdpjp" readonly id="Edpjp" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Dokter</option>
										<?foreach ($dokterumum as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>

								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tt">Tanggal Operasi</label>
								<div class="col-md-8">
								<div class="js-datetimepicker input-group date">
									<input type="text" class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="Etanggaloperasi" value="">
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tt">Jam Operasi</label>
								<div class="col-md-8">
								<div class="js-datetimepicker input-group date">
									<input type="text" class="form-control"id="Ewaktu1" value="">
									<span class="input-group-addon">
										<span class="fa fa-angle-right"></span>
									</span>
									<input type="text" class="form-control" id="Ewaktu2" value="">
								</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tdiagnosa">Diagnosa</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="Ediagnosa" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="toperasi">Operasi</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="Eoperasi" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tkelas">Jenis Operasi</label>
								<div class="col-md-8">
									<select name="tjenis_operasi" id="Ejenis_operasi" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Jenis Operasi</option>

										<?foreach ($mjenis_operasi as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tkelas">Jenis Anesthesi</label>
								<div class="col-md-8">
									<select name="tjenis_anestesi" id="Ejenis_anestesi" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Jenis Anesthesi</option>

										<?foreach ($mjenis_anaesthesi as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tkelas">Kelompok Operasi</label>
								<div class="col-md-8">
									<select name="tkelompok_operasi" id="Ekelompok_operasi" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Kelompok Operasi</option>
											<?foreach ($mkelompok_operasi as $row){?>
												<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tkelas">Kelompok Tindakan</label>
								<div class="col-md-8">
									<select multiple="multiple" name="Ekelompok_tindakan[]" id="Ekelompok_tindakan" class="js-select2 form-control Ekelompok_tindakan" style="width: 100%;">

									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tpetugas_mengajukan">Petugas Mengajukan</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly id="Epetugas_mengajukan" value="<?=$this->session->userdata('user_name')?>">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="ttgl_mengajukan">Tanggal Mengajukan</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly id="Etgl_mengajukan" value="<?=date('d-m-Y H:i:s')?>">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tpetugas_menyetujui">Petugas Menyetujui</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly id="Epetugas_menyetujui" value="<?=$this->session->userdata('user_name')?>">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="ttgl_menyetujui">Tanggal & Jam Menyetujui</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly id="Etgl_menyetujui" value="<?=date('d-m-Y H:i:s')?>">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tkelompok_pasien">Kelompok Pasien</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly  id="Ekelompok_pasien" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tasuransi">Perusahaan / Asuransi</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly  id="Easuransi" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="ttipe">Tipe</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly  id="Etipe" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tkelas">Kelas Perawatan</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly  id="Ekelas" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tperujuk">Dokter Perujuk</label>
								<div class="col-md-8">
									<input type="text" class="form-control"  readonly id="Eperujuk" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tkelas_tarif">Kelas Tarif</label>
								<div class="col-md-8">
									<select name="tkelas_tarif" id="Ekelas_tarif" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Kelas Tarif</option>
										<?foreach ($mkelas as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="truang_operasi">Ruang Operasi</label>
								<div class="col-md-8">
									<select name="truang_operasi" id="Eruang_operasi" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Ruang Operasi</option>
										<?foreach ($mruangan as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="Ekelompok_diagnosa">Kelompok Diagnosa</label>
								<div class="col-md-8">
									<select multiple="multiple" name="Ekelompok_diagnosa[]" id="Ekelompok_diagnosa" class="js-select2 form-control" style="width: 100%;">

									</select>
								</div>
							</div>


						</div>
						<div class="col-md-12">
							<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" for="Ecatatan">Catatan</label>
								<div class="col-md-10">
									<textarea class="form-control" name="Ecatatan" id="Ecatatan"></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" class="form-control" readonly id="Estatus" value="0">
						</div>
						<div class="col-md-12">
							<div class="form-group" style="margin-top: 10px;">
								<label class="col-md-2 control-label" for="stanggallahir"></label>
								<div class="col-md-10" style="float:left;">
									<?php if (UserAccesForm($user_acces_form,array('956'))){ ?>
									<button class="btn btn-sm btn-primary" type="button" id="btn_save_edit" name="btn_save_edit" ><i class="fa fa-save"></i> Simpan</button>
									<?}?>
									<?php if (UserAccesForm($user_acces_form,array('953'))){ ?>
									<button class="btn btn-sm btn-success" type="button" id="btn_save_setuju" name="btn_save_setuju" ><i class="fa fa-check"></i> Setujui Jadwal</button>
									<?}?>
									<?php if (UserAccesForm($user_acces_form,array('954'))){ ?>
									<button class="btn btn-sm btn-danger" type="button" id="btn_save_tolak" name="btn_save_tolak" ><i class="fa fa-close"></i> Tolak Jadwal</button>
									<?}?>
									<?php if (UserAccesForm($user_acces_form,array('955'))){ ?>
									<button class="btn btn-sm btn-primary" type="button" id="btn_add_gabung" name="btn_add_gabung" ><i class="fa fa-external-link-square"></i> Gabung Jadwal</button>
									<?}?>
									<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
								</div>

							</div>
						</div>
                    </form>
            </div>
            <div class="modal-footer" >

            </div>
        </div>
    </div>
</div>
<div class="modal" id="modalGabung" role="dialog" aria-hidden="true" aria-labelledby="modalAdd" font size="15">
    <div class="modal-dialog modal-lg" style="width:60%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
                    <h3 class="block-title">Gabung Jadwal Operasi</h3>
                </div>
                    <form class="form-horizontal" id="form_detail_add">
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="block-content block-content-full block-content-mini bg-default text-white">
										Pilih Pasien
							</div>
							<div class="form-group" style="margin-bottom: 5px;margin-top: 15px;">
								<label class="col-md-4 control-label" for="Gnomedrec_pilih">No. Medrec</label>
								<div class="col-md-8">
									<div class="input-group">
											<input type="text" readonly class="form-control" id="Gnomedrec_pilih" value="">
											<span class="input-group-btn">
												<button tabindex="17" class="btn btn-success" type="button" id="btn_search_pilih"><i class="fa fa-search"></i></button>
											</span>
									</div>
								</div>
									<input type="hidden" readonly class="form-control" id="Gid_pilih" value="">
									<input type="hidden" readonly class="form-control" id="Gidpasien_pilih" value="">
									<input type="hidden" readonly class="form-control" id="Gidpendaftaran_pilih" value="">
									<input type="hidden" readonly class="form-control" id="Gidasalpendaftaran_pilih" value="">
									<input type="hidden" readonly class="form-control" id="Gtipe_id_pilih" value="">
									<input type="hidden" readonly class="form-control" id="st_acc_pilih" value="">

							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tnamapasien">Nama Pasien</label>
								<div class="col-md-8">
									<input type="text" readonly class="form-control" id="Gnamapasien_pilih" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="ttanggallahir">Tanggal Lahir</label>
								<div class="col-md-8">
								<div class="js-datetimepicker input-group date">
									<input type="text"  disabled class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="Gtanggallahir_pilih" value="">
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tperujuk">Dokter Perujuk</label>
								<div class="col-md-8">
									<input type="text" class="form-control"  readonly id="Gperujuk_pilih" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="ttipe">Tipe</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly  id="Gtipe_pilih" value="">
								</div>
							</div>
						</div>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="block-content block-content-full block-content-mini bg-default text-white">
										Pilih Jadwal
							</div>
							<div class="form-group" style="margin-bottom: 5px;margin-top: 15px;">
								<label class="col-md-4 control-label" for="Gnomedrec_jadwal">No. Medrec</label>
								<div class="col-md-8">
									<div class="input-group">
											<input type="text" readonly class="form-control" id="Gnomedrec_jadwal" value="">
											<span class="input-group-btn">
												<button tabindex="17" class="btn btn-success" type="button" id="btn_search_jadwal"><i class="fa fa-search"></i></button>
											</span>
									</div>
								</div>
									<input type="hidden" readonly class="form-control" id="Gid_jadwal" value="">
									<input type="hidden" readonly class="form-control" id="Gidpasien_jadwal" value="">
									<input type="hidden" readonly class="form-control" id="Gidpendaftaran_jadwal" value="">
									<input type="hidden" readonly class="form-control" id="Gidasalpendaftaran_jadwal" value="">
									<input type="hidden" readonly class="form-control" id="st_acc_jadwal" value="">
								</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tnamapasien">Nama Pasien</label>
								<div class="col-md-8">
									<input type="text" readonly class="form-control" id="Gnamapasien_jadwal" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="ttanggallahir">Tanggal Lahir</label>
								<div class="col-md-8">
								<div class="js-datetimepicker input-group date">
									<input type="text"  disabled class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="Gtanggallahir_jadwal" value="">
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tperujuk">Dokter Perujuk</label>
								<div class="col-md-8">
									<input type="text" class="form-control"  readonly id="Gperujuk_jadwal" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="ttipe">Tipe</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly  id="Gtipe_jadwal" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tt">Tanggal Operasi</label>
								<div class="col-md-8">
								<div class="js-datetimepicker input-group date">
									<input type="text" disabled class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="Gtanggaloperasi_jadwal" value="">
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tt">Jam Operasi</label>
								<div class="col-md-8">
								<div class="js-datetimepicker input-group date">
									<input type="text" disabled class="form-control"id="Gwaktu1_jadwal" value="">
									<span class="input-group-addon">
										<span class="fa fa-angle-right"></span>
									</span>
									<input type="text" disabled class="form-control" id="Gwaktu2_jadwal" value="">
								</div>
								</div>
							</div>

						</div>

						<div class="modal-footer">
						</div>
						<div class="col-md-12">
							<div class="form-group" style="margin-top: 10px;">
								<label class="col-md-8 control-label" for="stanggallahir"></label>
								<div class="col-md-4" style="float:left;">
									<button class="btn btn-sm btn-primary" type="button" id="btn_save_gabung" name="btn_save_gabung" ><i class="fa fa-save"></i> Simpan</button>
									<button class="btn btn-sm btn-default" type="button" id="btn_save_reset" name="btn_save_reset" ><i class="fa fa-refresh"></i> Reset</button>
								</div>

							</div>
						</div>
                    </form>
            </div>
            <div class="modal-footer" >

            </div>
        </div>
    </div>
</div>
<div class="modal" id="modalAdd" role="dialog" aria-hidden="true" aria-labelledby="modalAdd" font size="12">
    <div class="modal-dialog modal-lg"  style="width:65%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
                    <h3 class="block-title">Cari Pasien</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal" id="form_detail">
                        <div class="col-md-6">

								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snomedrec">No. Medrec</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snomedrec" value="">
										<input type="hidden" class="form-control" id="form_pilih" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snamapasien">Nama Pasien</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snamapasien" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="stanggallahir">Tanggal Lahir</label>
									<div class="col-md-8">
									<div class="js-datetimepicker input-group date">
										<input type="text" class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="stanggallahir" value="">
										<span class="input-group-addon">
											<span class="fa fa-calendar"></span>
										</span>
									</div>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="skelas">Kelas Perawatan</label>
									<div class="col-md-8">
										<select name="skelas" id="skelas" class="js-select2 form-control" style="width: 100%;">
											<option value="#">Semua Kelas</option>
											<?foreach ($kelas_list as $row){?>
												<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>

										</select>
									</div>
								</div>
						</div>
						<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="salamat">DPJP</label>
									<div class="col-md-8">
										<select name="sdpjp" id="sdpjp" class="js-select2 form-control" style="width: 100%;">
											<option value="#">Pilih Dokter</option>
											<?foreach ($dokterumum as $row){?>
												<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>
										</select>

									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="sperujuk">Dokter Perujuk</label>
									<div class="col-md-8">
										<select name="sperujuk" id="sperujuk" class="js-select2 form-control" style="width: 100%;">
											<option value="#">Semua Dokter</option>
											<?foreach ($dokterumum as $row){?>
												<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>
										</select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="stanggallahir">Asal Pasien</label>
									<div class="col-md-8">
										<select name="sasal" id="sasal" class="js-select2 form-control" style="width: 100%;">
											<option value="#">Semua Asal</option>
											<option value="1">Rawat Inap</option>
											<option value="2">ODS</option>
										</select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 15px;">
									<label class="col-md-4 control-label" for="stanggallahir"></label>
									<div class="col-md-8">
										<button class="btn btn-success text-uppercase" type="button" id="btn_filter_pasien" name="btn_filter_pasien" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
									</div>
								</div>


						</div>
						<hr>
                        <table class="table table-bordered" id="tabel_pasien">
                            <thead>
                                <tr>
									<th>Tipe</th>
									<th>No Modrec</th>
									<th>Nama Pasien</th>
									<th>Tanggal Lahir</th>
									<th>Kelas & Bed</th>
									<th>Dokter DPJP</th>
									<th>Dokter Perujuk</th>
								</tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal" id="modalJadwal" role="dialog" aria-hidden="true" aria-labelledby="modalAdd" font size="12">
    <div class="modal-dialog modal-lg"  style="width:65%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
                    <h3 class="block-title">Cari Jadwal</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal" id="form_detail">
                        <div class="col-md-6">

								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snomedrec">No. Medrec</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="jnomedrec" value="">
										<input type="hidden" class="form-control" id="form_pilih" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="jnamapasien">Nama Pasien</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="jnamapasien" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="jtanggallahir">Tanggal Lahir</label>
									<div class="col-md-8">
									<div class="js-datetimepicker input-group date">
										<input type="text" class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="jtanggallahir" value="">
										<span class="input-group-addon">
											<span class="fa fa-calendar"></span>
										</span>
									</div>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="jkelas">Kelas Tarif</label>
									<div class="col-md-8">
										<select name="jkelas" id="jkelas" class="js-select2 form-control" style="width: 100%;">
											<option value="#">Semua Kelas</option>
											<?foreach ($kelas_list as $row){?>
												<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>

										</select>
									</div>
								</div>
						</div>
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="jalamat">Diagnosa</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="jdiagnosa" value="">

								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="jperujuk">Operasi</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="joperasi" value="">

								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="jtanggallahir">Asal Pasien</label>
								<div class="col-md-8">
									<select name="jasal" id="jasal" class="js-select2 form-control" style="width: 100%;">
										<option value="#">Semua Asal</option>
										<option value="1">Rawat Inap</option>
										<option value="2">ODS</option>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 15px;">
								<label class="col-md-4 control-label" for="jtanggallahir"></label>
								<div class="col-md-8">
									<button class="btn btn-success text-uppercase" type="button" id="btn_filter_jadwal" name="btn_filter_jadwal" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
								</div>
							</div>


						</div>
						<hr>
                        <table class="table table-bordered" id="tabel_jadwal">
                            <thead>
                                <tr>
									<th>Tipe</th>
									<th>No Modrec</th>
									<th>Nama Pasien</th>
									<th>Tanggal Lahir</th>
									<th>Kelas & Bed</th>
									<th>Diagnosa</th>
									<th>Operasi</th>
								</tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
    var table;
    $(document).ready(function() {
		$("#waktu,#twaktu1,#twaktu2,#Ewaktu1,#Ewaktu2").datetimepicker({
			format: "HH:mm",
			stepping: 30
		});
        $(".js-select2").select2();
        $(".number").number(true,0,'.',',');
		loadKO();
		$("#Ekelompok_tindakan,#Ekelompok_diagnosa,#tkelompok_tindakan,#tkelompok_diagnosa").select2({
			tag: true
		  });
		// $("#tid").val('14');
		// get_pasien_rawat_inap();

		// $("#modalGabung").modal('show');
    });
	$("#Ekelompok_tindakan,#Ekelompok_diagnosa,#tkelompok_tindakan,#tkelompok_diagnosa").on("select2:select", function (evt) {
	    var element = evt.params.data.element;
	    var $element = $(element);

	    $element.detach();
	    $(this).append($element);
	    $(this).trigger("change");
	  });
	function setuju_tolak($id,$status){
		var id=$id;
		var status=$status;
		$.ajax({
			url: '{site_url}tko/setuju_tolak',
			type: 'POST',
			data: {id: id,status: status},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Persetujuan Berhasil diproses'});
				$('#index_list').DataTable().ajax.reload( null, false );
				// window.location.href = "{site_url}tko/index/"+idunit;
			}
		});
	}
	function batalkan($id){
		var id=$id;
		$.ajax({
			url: '{site_url}tko/batalkan',
			type: 'POST',
			data: {id: id},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Pembatalan Berhasil diproses'});
				$('#index_list').DataTable().ajax.reload( null, false );
				// window.location.href = "{site_url}tko/index/"+idunit;
			}
		});
	}

	$(document).on("click", "#btn_filter", function() {
		loadKO();
	});
	$(document).on("click", "#btn_filter_pasien", function() {
		LoadPasien();
	});
	$(document).on("click", "#btn_filter_jadwal", function() {
		LoadJadwal();
	});
	$(document).on("click", "#btn_add", function() {
		$("#snomedrec").val('');
		$("#snamapasien").val('');
		$("#stanggallahir").val('');
		$("#form_pilih").val('tambah');
		$("#modalAdd").modal('show');
		// load_kelompok_tindakan('0','add')
		LoadPasien();
	});
	$(document).on("click", "#btn_gabung", function() {
		reset_gabung();
		$("#modalGabung").modal('show');
	});
	$(document).on("click", "#btn_add_gabung", function() {
		reset_gabung();
		var id=$("#Eid").val();
		get_jadwal(id);
		$("#modalEdit").modal('hide');
		$("#modalGabung").modal('show');
	});
	function reset_gabung(){
		$("#Gnomedrec_pilih").val('');
		$("#Gnamapasien_pilih").val('');
		$("#Gtanggallahir_pilih").val('');
		$("#Gperujuk_pilih").val('');
		$("#Gtipe_pilih").val('');
		$("#Gnomedrec_jadwal").val('');
		$("#Gnamapasien_jadwal").val('');
		$("#Gtanggallahir_jadwal").val('');
		$("#Gperujuk_jadwal").val('');
		$("#Gtipe_jadwal").val('');
	}
	$(document).on("click", "#btn_search_pilih", function() {
		$("#snomedrec").val('');
		$("#snamapasien").val('');
		$("#stanggallahir").val('');
		$("#form_pilih").val('gabung');
		$("#modalAdd").modal('show');
		LoadPasien();
	});
	$(document).on("click", "#btn_search_jadwal", function() {
		$("#modalJadwal").modal('show');
		LoadJadwal();
	});
	$(document).on("click", "#btn_save_reset", function() {
		reset_gabung();
	});
	$(document).on("click", "#btn_save_gabung", function() {
		simpan_gabung();
	});
	$(document).on("click", ".batal", function() {
		table = $('#index_list').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		// var idunit = table.cell(tr,7).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Membatalkan Jadwal Operasi ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			batalkan(id);
		});


	});
	$(document).on("click", ".gabung", function() {
		table = $('#index_list').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		reset_gabung();
		get_jadwal(id);(id);

		$("#modalGabung").modal('show');

	});
	$(document).on("click", "#btn_save", function() {
		// alert('sini');
		if (validasi_add()){
			swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Jadwal Operasi Baru ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
			}).then(function() {
				simpan();
			});


		}
	});
	$(document).on("click", "#btn_save_edit", function() {
		// alert('sini');
		$("#st_acc").val('0');
		if (validasi_edit()){
			swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Jadwal Operasi Baru ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
			}).then(function() {
				simpan_edit();
			});


		}
	});
	$(document).on("click", "#btn_save_setuju", function() {
		// alert('sini');
		$("#st_acc").val('1');
		if (validasi_edit()){
			swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit & Setujui Jadwal Operasi Baru ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
			}).then(function() {
				simpan_edit();
			});


		}
	});
	$(document).on("click", "#btn_save_tolak", function() {
		// alert('sini');
		$("#st_acc").val('2');
		// if (validasi_edit()){
			swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Edit Tolak Jadwal Operasi Baru ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
			}).then(function() {
				simpan_edit();
			});


		// }
	});
	function simpan(){
		var id=$("#tid").val();
		var idasalpendaftaran=$("#tidasalpendaftaran").val();
		var idpendaftaran=$("#tidpendaftaran").val();
		var idpasien=$("#tidpasien").val();
		var tanggaloperasi=$("#ttanggaloperasi").val();
		var waktumulaioperasi=$("#twaktu1").val();
		var waktuselesaioperasi=$("#twaktu2").val();
		var diagnosa=$("#tdiagnosa").val();
		var operasi=$("#toperasi").val();
		var jenis_operasi_id=$("#tjenis_operasi").val();
		var jenis_anestesi_id=$("#tjenis_anestesi").val();
		var kelompok_operasi_id=$("#tkelompok_operasi").val();
		var kelompok_tindakan_id=$("#tkelompok_tindakan").val();
		var kelas_tarif_id=$("#tkelas_tarif").val();
		var ruang_id=$("#truang_operasi").val();
		var kelompk_diagnosa_id=$("#tkelompok_diagnosa").val();
		var catatan=$("#tcatatan").val();
		var dpjp_id=$("#tdpjp").val();
		var tnamapasien=$("#tnamapasien").val();
		// alert(kelompk_diagnosa_id);return false;
		$.ajax({
			url: '{site_url}tko/simpan_add',
			type: 'POST',
			data: {
				idasalpendaftaran: idasalpendaftaran,idpendaftaran: id,
				idpasien: idpasien,tanggaloperasi: tanggaloperasi,
				waktumulaioperasi: waktumulaioperasi,waktuselesaioperasi: waktuselesaioperasi,
				diagnosa: diagnosa,operasi: operasi,
				jenis_operasi_id: jenis_operasi_id,jenis_anestesi_id: jenis_anestesi_id,
				kelompok_operasi_id: kelompok_operasi_id,kelompok_tindakan_id: kelompok_tindakan_id,
				kelas_tarif_id: kelas_tarif_id,ruang_id: ruang_id,
				kelompk_diagnosa_id: kelompk_diagnosa_id,catatan: catatan,dpjp_id: dpjp_id,namapasien: tnamapasien
			},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Jadwal Berhasil Disimpan'});
				$('#modalTambah').modal('hide');
				$('#index_list').DataTable().ajax.reload( null, false );
				// window.location.href = "{site_url}tko/index/"+idunit;
			}
		});

	}
	function simpan_edit(){
		var id=$("#Eid").val();


		$("#Ejenis_operasi").attr('disabled',false);
		$("#Ekelas_tarif").attr('disabled',false);
		var idasalpendaftaran=$("#Eidasalpendaftaran").val();
		var idpendaftaran=$("#Eidpendaftaran").val();
		var idpasien=$("#Eidpasien").val();
		var tanggaloperasi=$("#Etanggaloperasi").val();
		var waktumulaioperasi=$("#Ewaktu1").val();
		var waktuselesaioperasi=$("#Ewaktu2").val();
		var diagnosa=$("#Ediagnosa").val();
		var operasi=$("#Eoperasi").val();
		var jenis_operasi_id=$("#Ejenis_operasi").val();
		var jenis_anestesi_id=$("#Ejenis_anestesi").val();
		var kelompok_operasi_id=$("#Ekelompok_operasi").val();
		var kelompok_tindakan_id=$("#Ekelompok_tindakan").val();
		var kelas_tarif_id=$("#Ekelas_tarif").val();
		var ruang_id=$("#Eruang_operasi").val();
		var kelompk_diagnosa_id=$("#Ekelompok_diagnosa").val();
		var catatan=$("#Ecatatan").val();
		var statussetuju=$("#st_acc").val();
		var dpjp_id=$("#Edpjp").val();
		// alert(kelompok_tindakan_id);
		$.ajax({
			url: '{site_url}tko/simpan_edit',
			type: 'POST',
			data: {id:id,statussetuju:statussetuju,
				idasalpendaftaran: idasalpendaftaran,idpendaftaran: idpendaftaran,
				idpasien: idpasien,tanggaloperasi: tanggaloperasi,
				waktumulaioperasi: waktumulaioperasi,waktuselesaioperasi: waktuselesaioperasi,
				diagnosa: diagnosa,operasi: operasi,
				jenis_operasi_id: jenis_operasi_id,jenis_anestesi_id: jenis_anestesi_id,
				kelompok_operasi_id: kelompok_operasi_id,kelompok_tindakan_id: kelompok_tindakan_id,
				kelas_tarif_id: kelas_tarif_id,ruang_id: ruang_id,
				kelompk_diagnosa_id: kelompk_diagnosa_id,catatan: catatan,dpjp_id: dpjp_id
			},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Jadwal Berhasil Disimpan'});
				$('#modalEdit').modal('hide');
				$('#index_list').DataTable().ajax.reload( null, false );
				// window.location.href = "{site_url}tko/index/"+idunit;
			}
		});

	}
	function simpan_gabung(){

		if (validasi_gabung()==false) {return false}
		var idjadwal=$("#Gid_jadwal").val();
		var idpendaftaran=$("#Gidpendaftaran_pilih").val();
		// var idasalpendaftaran=$("#Gidasalpendaftaran_pilih").val();
		var idasalpendaftaran='2';
		var idpasien=$("#Gidpasien_pilih").val();
		var idtipe=$("#Gtipe_id_pilih").val();
		// alert('IDJADWAL :'+idjadwal+'ID PENDAFTARAN:'+idpendaftaran+' IDASAL :'+idasalpendaftaran+' IDABSSEN :'+idpasien);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menggabung Jadwal Operasi Baru ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
			}).then(function() {
				$.ajax({
					url: '{site_url}tko/simpan_gabung',
					type: 'POST',
					data: {idjadwal:idjadwal,idpendaftaran:idpendaftaran,
						idasalpendaftaran: idasalpendaftaran,idpasien: idpasien,idtipe: idtipe,
					},
					complete: function() {
						$.toaster({priority : 'success', title : 'Succes!', message : ' Jadwal Berhasil digabung'});
						$('#modalGabung').modal('hide');
						$('#index_list').DataTable().ajax.reload( null, false );
						// window.location.href = "{site_url}tko/index/"+idunit;
					}
				});
			});

	}
	function validasi_add(){
		if ($("#ttanggaloperasi").val()==''){
			sweetAlert("Maaf...", "Tanggal Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#twaktu1").val()==''){
			sweetAlert("Maaf...", "Waktu  Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#twaktu2").val()==''){
			sweetAlert("Maaf...", "Waktu  Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#tdiagnosa").val()==''){
			sweetAlert("Maaf...", "Diagnosa Harus diisi!", "error");
			return false;
		}
		if ($("#toperasi").val()==''){
			sweetAlert("Maaf...", "Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#tjenis_operasi").val()=='0'){
			sweetAlert("Maaf...", "Jenis Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#tkelas_tarif").val()=='0'){
			sweetAlert("Maaf...", "Kelas Tarif Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#truang_operasi").val()=='0'){
			sweetAlert("Maaf...", "Ruang Operasi Harus diisi!", "error");
			return false;
		}

		return true;
	}
	function validasi_gabung(){
		if ($("#Gnomedrec_pilih").val()==''){
			sweetAlert("Maaf...", "Pilih Pasien Harus diisi!", "error");
			return false;
		}
		if ($("#Gnomedrec_jadwal").val()==''){
			sweetAlert("Maaf...", "Jadwal Harus Harus diisi!", "error");
			return false;
		}

		return true;
	}
	function validasi_edit(){
		if ($("#Etanggaloperasi").val()==''){
			sweetAlert("Maaf...", "Tanggal Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#Ewaktu1").val()==''){
			sweetAlert("Maaf...", "Waktu  Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#Ewaktu2").val()==''){
			sweetAlert("Maaf...", "Waktu  Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#Ediagnosa").val()==''){
			sweetAlert("Maaf...", "Diagnosa Harus diisi!", "error");
			return false;
		}
		if ($("#Eoperasi").val()==''){
			sweetAlert("Maaf...", "Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#Ejenis_operasi").val()=='0'){
			sweetAlert("Maaf...", "Jenis Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#Ekelas_tarif").val()=='0'){
			sweetAlert("Maaf...", "Kelas Tarif Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#Eruang_operasi").val()=='0'){
			sweetAlert("Maaf...", "Ruang Operasi Harus diisi!", "error");
			return false;
		}

		return true;
	}
	$(document).on("click", ".setuju", function() {
		 table = $('#index_list').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		// var idunit = table.cell(tr,7).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyetujui Jadwal Operasi ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			setuju_tolak(id,'1');
		});


		// alert(id);
	});

	$(document).on("click", ".tolak", function() {
		table = $('#index_list').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		// var idunit = table.cell(tr,7).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menolak Jadwal Operasi ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Batalkan",
		}).then(function() {
			setuju_tolak(id,'2');
		});


		// alert(id);
	});
	$(document).on("click", ".selectPasien", function() {
			var id = ($(this).data('id'));
			$("#tid").val(id);

			if ($("#form_pilih").val()=='tambah'){
				$("#modalTambah").modal('show');
				get_pasien_rawat_inap();
			}else{
				get_pasien_rawat_inap_gabung(id);
			}

			// return false;
	});
	$(document).on("click", ".selectJadwal", function() {
			var id = ($(this).data('id'));
			get_jadwal(id);

	});
	function get_pasien_rawat_inap(){
		var id = $("#tid").val();
		$.ajax({
			url: '{site_url}tko/find_pasien/'+id,
			dataType: "json",
			success: function(data) {
				$("#tnomedrec").val(data.no_medrec);
				$("#tnamapasien").val(data.namapasien);
				$("#tdpjp").val(data.iddokterpenanggungjawab).trigger('change');
				$("#tumur").val(data.umurtahun + ' Tahun '+data.umurbulan + ' Bulan ' + data.umurhari + ' Hari');
				$("#ttanggallahir").val(data.tanggal_lahir);
				$("#ttipe").val(data.tipe);
				$("#tperujuk").val(data.perujuk);
				if (data.idtipe=='1'){
					$("#tkelas").val(data.kelas+' / '+data.bed);
				}else{
					$("#tkelas").val('');
				}
				$("#tkelompok_pasien").val(data.kelompokpasien);
				$("#tasuransi").val(data.asuransi);
				$("#tidpendaftaran").val(data.idpendaftaran);
				$("#tidasalpendaftaran").val(data.idasalpendaftaran);
				$("#tidpasien").val(data.idpasien);
				$("#tdiagnosa").val('');
				$("#toperasi").val('');
				$("#ttanggaloperasi").val('');
				$("#twaktu1").val('');
				$("#twaktu2").val('');
				$("#tjenis_operasi").val('0').trigger('change');
				$("#tjenis_anestesi").val('0').trigger('change');
				$("#tkelompok_operasi").val('0').trigger('change');
				// $("#tkelompok_tindakan").val('0').trigger('change');
				$("#truang_operasi").val('0').trigger('change');
				if (data.idkelas){
					$("#tkelas_tarif").val(data.idkelas).trigger('change');
				}
				load_kelompok_tindakan('0','add');
				load_kelompok_diagnosa('0','add');
				// alert(data.no_medrec);
				console.log(data);
			}
		});

	}
	function get_pasien_rawat_inap_gabung($id){
		var id = $id;
		$.ajax({
			url: '{site_url}tko/find_pasien/'+id,
			dataType: "json",
			success: function(data) {
				$("#Gnomedrec_pilih").val(data.no_medrec);
				$("#Gnamapasien_pilih").val(data.namapasien);
				$("#Gtanggallahir_pilih").val(data.tanggal_lahir);
				$("#Gtipe_pilih").val(data.tipe);
				$("#Gtipe_id_pilih").val(data.idtipe);
				$("#Gperujuk_pilih").val(data.perujuk);
				$("#Gidpasien_pilih").val(data.idpasien);
				$("#Gidpendaftaran_pilih").val(data.id);
				$("#Gidasalpendaftaran_pilih").val(data.idasalpendaftaran);
				$('#modalAdd').modal('hide');
			}
		});

	}
	function load_kelompok_tindakan($id,$jenis){
		// alert($jenis);
		arr_tindakan=[];
		var kt;
		if ($jenis=='edit'){
			kt=$('#Ekelompok_tindakan');
		}else{
			kt=$('#tkelompok_tindakan');
		}
		kt.empty();
		// $('#Tkelompok_tindakan').empty();
		$.ajax({
			url: '{site_url}tko/kelompok_tindakan_list/'+$id,
			dataType: "json",
			success: function(data) {
				 $.each(data, function (i,item) {
					var newOption = new Option(item.text, item.id, true, true);

					kt.append(newOption);
					kt.val(null).trigger('change');
					if (item.selected){
						arr_tindakan.push(item.id);
					}
				});
				setTimeout(function() {
					kt.val(arr_tindakan).trigger('change');
				}, 200);

			}
		});

	}
	function load_kelompok_diagnosa($id,$jenis){
		arr_diagnosa=[];
		var kt;
		if ($jenis=='edit'){
			kt=$('#Ekelompok_diagnosa');
		}else{
			kt=$('#tkelompok_diagnosa');
		}
		kt.empty();
		// $('#Tkelompok_tindakan').empty();
		$.ajax({
			url: '{site_url}tko/kelompok_diagnosa_list/'+$id,
			dataType: "json",
			success: function(data) {
				 $.each(data, function (i,item) {
					var newOption = new Option(item.text, item.id, false, false);
					kt.append(newOption).trigger('change');
					if (item.selected=='selected'){
						console.log(item.text);
						arr_diagnosa.push(item.id);
					}
					kt.val(null).trigger('change');
				});
				setTimeout(function() {
					kt.val(arr_diagnosa).trigger('change');
				}, 200);

			}
		});
	}
	function get_edit(){
		var id = $("#Eid").val();
		load_kelompok_tindakan(id,'edit');
		load_kelompok_diagnosa(id,'edit');
		$.ajax({
			url: '{site_url}tko/get_edit/'+id,
			dataType: "json",
			success: function(data) {
				$("#Enomedrec").val(data.no_medrec);
				$("#Enamapasien").val(data.namapasien);
				$("#Edpjp").val(data.dpjp).trigger('change');
				$("#Eumur").val(data.umurtahun + ' Tahun '+data.umurbulan + ' Bulan ' + data.umurhari + ' Hari');
				$("#Etanggallahir").val(data.tanggal_lahir);
				$("#Etipe").val(data.tipe);
				$("#Eperujuk").val(data.perujuk);
				if (data.idtipe=='1'){
					$("#Ekelas").val(data.kelas+' / '+data.bed);
				}else{
					$("#Ekelas").val('');
				}
				$("#Ekelompok_pasien").val(data.kelompokpasien);
				$("#Easuransi").val(data.asuransi);
				$("#Eidpendaftaran").val(data.idpendaftaran);
				$("#Eidasalpendaftaran").val(data.idasalpendaftaran);
				$("#Eidpasien").val(data.idpasien);
				$("#Ewaktu1").val(data.waktumulaioperasi);
				$("#Ewaktu2").val(data.waktuselesaioperasi);
				$("#Etanggaloperasi").val(data.tanggaloperasi);
				$("#Ediagnosa").val(data.diagnosa);
				$("#Eoperasi").val(data.operasi);
				$("#Ejenis_operasi").val(data.jenis_operasi_id).trigger('change');
				$("#Ejenis_anestesi").val(data.jenis_anestesi_id).trigger('change');
				$("#Ekelompok_operasi").val(data.kelompok_operasi_id).trigger('change');
				$("#Ekelompok_tindakan").val(data.kelompok_tindakan_id).trigger('change');
				$("#Ekelas_tarif").val(data.kelas_tarif_id).trigger('change');
				$("#Eruang_operasi").val(data.ruang_id).trigger('change');
				// $("#Ekelompok_diagnosa").val(data.kelompk_diagnosa_id).trigger('change');
				$("#Estatus").val(data.status);
				$("#Ecatatan").val(data.catatan);
				// alert(data.catatan);
				if (data.status=='2'){
					$("#Ejenis_operasi").attr('disabled', true)
					$("#Ekelas_tarif").attr('disabled', true)
				}else{
					$("#Ejenis_operasi").attr('disabled',false);
					$("#Ekelas_tarif").attr('disabled',false);

				}
				// alert(data.no_medrec);
				console.log(data);
			}
		});

	}
	function get_jadwal($id){
		var id = $id;
		$.ajax({
			url: '{site_url}tko/get_edit/'+id,
			dataType: "json",
			success: function(data) {
				$("#Gnomedrec_jadwal").val(data.no_medrec);
				$("#Gnamapasien_jadwal").val(data.namapasien);
				$("#Gtanggallahir_jadwal").val(data.tanggal_lahir);
				$("#Gperujuk_jadwal").val(data.perujuk);
				$("#Gtipe_jadwal").val(data.tipe);
				$("#Gtanggaloperasi_jadwal").val(data.tanggaloperasi);
				$("#Gwaktu1_jadwal").val(data.waktumulaioperasi);
				$("#Gwaktu2_jadwal").val(data.waktuselesaioperasi);
				$("#Gid_jadwal").val(id);

				console.log(data);
			}
		});

	}
	// $('#form').on('keyup keypress', function(e) {
	  // var keyCode = e.keyCode || e.which;
	  // if (keyCode === 13) {
		// loadKO();
		// return false;
	  // }
	// });
	$(document).on("click", ".edit", function() {
		 table = $('#index_list').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		$("#Eid").val(id);
		get_edit();
		$("#modalEdit").modal('show');
	});
	function LoadPasien() {
		var snomedrec=$("#snomedrec").val();
		var snamapasien=$("#snamapasien").val();
		var stanggallahir=$("#stanggallahir").val();
		var skelas=$("#skelas").val();
		var sdpjp=$("#sdpjp").val();
		var sperujuk=$("#sperujuk").val();
		var sasal=$("#sasal").val();
		$('#tabel_pasien').DataTable().destroy();
			// alert(skelas);
		var table = $('#tabel_pasien').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"searching": true,
		"serverSide": true,
		"autoWidth": false,
		"order": [],
		"ajax": {
			url: '{site_url}tko/LoadPasien/',
			type: "POST",
			dataType: 'json',
			data: {
				snomedrec: snomedrec,
				snamapasien: snamapasien,
				stanggallahir: stanggallahir,
				skelas: skelas,
				sdpjp: sdpjp,
				sperujuk: sperujuk,
				sasal: sasal,
			}
		},
		columnDefs: [
					 // {"targets": [0], "visible": false },
					 // {  className: "text-right", targets:[1,4,5,6,7,9,10,11] },
					 {  className: "text-center", targets:[0,1,3,4] },
					 { "width": "8%", "targets": [0,1] },
					 { "width": "10%", "targets": [3,4] },
					 { "width": "15%", "targets": [2,5,6] }
					]
		});
	}
	function LoadJadwal() {
		var snomedrec=$("#jnomedrec").val();
		var snamapasien=$("#jnamapasien").val();
		var stanggallahir=$("#jtanggallahir").val();
		var skelas=$("#jkelas").val();
		var sdiagnosa=$("#jdiagnosa").val();
		var soperasi=$("#joperasi").val();
		var sasal=$("#jasal").val();
		$('#tabel_jadwal').DataTable().destroy();
			// alert(sasal);
		var table = $('#tabel_jadwal').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"searching": true,
		"serverSide": true,
		"autoWidth": false,
		"order": [],
		"ajax": {
			url: '{site_url}tko/LoadJadwal/',
			type: "POST",
			dataType: 'json',
			data: {
				snomedrec: snomedrec,
				snamapasien: snamapasien,
				stanggallahir: stanggallahir,
				skelas: skelas,
				sdiagnosa: sdiagnosa,
				soperasi: soperasi,
				sasal: sasal,
			}
		},
		columnDefs: [
					 // {"targets": [0], "visible": false },
					 // {  className: "text-right", targets:[1,4,5,6,7,9,10,11] },
					 {  className: "text-center", targets:[0,1,3,4] },
					 { "width": "8%", "targets": [0,1] },
					 { "width": "10%", "targets": [3,4] },
					 { "width": "15%", "targets": [2,5,6] }
					]
		});
	}
	function loadKO() {
		var idtipe=$("#idtipe").val();
		var tanggaldari=$("#tanggaldari").val();
		var tanggalsampai=$("#tanggalsampai").val();
		var waktu=$("#waktu").val();
		var iddokter_operator=$("#iddokter_operator").val();
		var iddokter_anestesi=$("#iddokter_anestesi").val();
		var status=$("#status").val();
		var nomedrec=$("#nomedrec").val();
		var namapasien=$("#namapasien").val();
		// alert(status);
		$('#index_list').DataTable().destroy();
			// alert(role_id+' : '+menu_id+' : '+sub_id);
		var table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"order": [],
		"ajax": {
			url: '{site_url}tko/loadKO/',
			type: "POST",
			dataType: 'json',
			data: {
				idtipe: idtipe,
				tanggaldari: tanggaldari,
				tanggalsampai: tanggalsampai,
				waktu: waktu,
				iddokter_operator: iddokter_operator,
				iddokter_anestesi: iddokter_anestesi,
				status: status,
				nomedrec: nomedrec,
				namapasien: namapasien,
			}
		},
		columnDefs: [
					 {"targets": [0], "visible": false },
					 // {  className: "text-right", targets:[1,4,5,6,7,9,10,11] },
					 {  className: "text-center", targets:[2,3,4,12] },
					 { "width": "3%", "targets": [1] },
					 { "width": "5%", "targets": [2,4,6] },
					 { "width": "8%", "targets": [3,5,8,9] },
					 { "width": "10%", "targets": [7,13] },
					 // { "width": "15%", "targets": [12] }
					]
		});
	}
</script>
