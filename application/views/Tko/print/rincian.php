<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Rincian Kamar Operasi</title>
    <style>
  		body {
  			-webkit-print-color-adjust: exact;
  		}

      @media print {
        * {
          font-size: 9px !important;
        }
        table {
          font-size: 9px !important;
          border-collapse: collapse !important;
          width: 100% !important;
        }
        td {
          padding: 3px;
        }
        .content td {
          padding: 0px;
        }
        .content-2 td {
          margin: 3px;
        }

        /* border-normal */
        .border-full {
          border: 1px solid #000 !important;
        }
        .border-bottom {
          border-bottom:1px solid #000 !important;
        }

        /* border-thick */
        .border-thick-top{
          border-top:2px solid #000 !important;
        }
        .border-thick-bottom{
          border-bottom:2px solid #000 !important;
        }

        .border-dotted{
          border-width: 1px;
          border-bottom-style: dotted;
        }

        /* text-position */
        .text-center{
          text-align: center !important;
        }
        .text-right{
          text-align: right !important;
        }

        /* text-style */
        .text-italic{
          font-style: italic;
        }
        .text-bold{
          font-weight: bold;
        }
      }

      @media screen {
        * {
          font-size: 9px !important;
        }
        table {
          font-size: 9px !important;
          border-collapse: collapse !important;
          width: 100% !important;
        }
        td {
          padding: 3px;
        }
        .content td {
          padding: 0px;
        }
        .content-2 td {
          margin: 3px;
        }

        /* border-normal */
        .border-full {
          border: 1px solid #000 !important;
        }
        .border-bottom {
          border-bottom:1px solid #000 !important;
        }

        /* border-thick */
        .border-thick-top{
          border-top:2px solid #000 !important;
        }
        .border-thick-bottom{
          border-bottom:2px solid #000 !important;
        }

        .border-dotted{
          border-width: 1px;
          border-bottom-style: dotted;
        }

        /* text-position */
        .text-center{
          text-align: center !important;
        }
        .text-right{
          text-align: right !important;
        }

        /* text-style */
        .text-italic{
          font-style: italic;
        }
        .text-bold{
          font-weight: bold;
        }
      }
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <table class="content">
      <tr>
        <td style="text-align:center">
          &nbsp;<b>RINCIAN KAMAR OPERASI</b>
        </td>
      </tr>
    </table>
    <br>
    <table class="content-2">
      <tr>
        <td rowspan="11" style="width:10%"><img src="<?= base_url() ?>assets/upload/logo/logo.png" width="100px"></td>
      </tr>
      <tr>
        <td style="width:100px">NO REGISTER/NO MEDREC</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $pasienkod[0]->nopendaftaran ?>/<?= $pasienkod[0]->nomedrec ?></td>

        <td style="width:100px">DR OPERATOR</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $dokterOperator ?></td>
      </tr>
      <tr>
        <td style="width:100px">TANGGAL OPERASI</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $pasienkod[0]->tanggaloperasi ?></td>

        <td style="width:100px">TIPE</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $pasienkod[0]->tipe ?></td>
      </tr>
      <tr>
        <td style="width:100px">NAMA PASIEN</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $pasienkod[0]->nama ?></td>

        <td style="width:100px">JENIS OPERASI</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $pasienkod[0]->jenisoperasi ?></td>
      </tr>
      <tr>
        <td style="width:100px">TANGGAL LAHIR</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $pasienkod[0]->tanggallahir ?></td>

        <td style="width:100px">KET. OPERASI</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $pasienkod[0]->keteranganoperasi ?></td>
      </tr>
      <tr>
        <td style="width:100px">UMUR</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $pasienkod[0]->umur_tahun ?> Th <?= $pasienkod[0]->umur_bulan ?> Bln <?= $pasienkod[0]->umur_hari ?> Hr </td>

        <td style="width:100px">ANESTHESI</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $pasienkod[0]->jenisanesthesi ?></td>
      </tr>
      <tr>
        <td style="width:100px">DIAGNOSA</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $pasienkod[0]->diagnosa ?></td>

        <td style="width:100px">TINDAKAN</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= '' ?></td>
      </tr>
      <tr>
        <td style="width:100px">JAM OPERASI</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $pasienkod[0]->waktumulaioperasi ?></td>

        <td style="width:100px">RUANG OPERASI</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $pasienkod[0]->ruangoperasi ?></td>
      </tr>
      <tr>
        <td style="width:100px">DR ANAESTHESI</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $dokterAnesthesi ?></td>

        <td style="width:100px">ASS ANAESTHESI</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $pasienkod[0]->assanaesthesi ?> (<?= $pasienkod[0]->persenassanaesthesi ?>%)</td>
      </tr>
      <tr>
        <td style="width:100px">INSTRUMENT</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $pasienkod[0]->instrument ?></td>

        <td style="width:100px">ASS OPERATOR</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $pasienkod[0]->assoperator ?> (<?= $pasienkod[0]->persenassoperator ?>%)</td>
      </tr>
      <tr>
        <td style="width:100px">PETUGAS DAFTAR OPERASI</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $pasienkod[0]->namauser_mengajukan ?></td>

        <td style="width:100px">PETUGAS ACC</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= $pasienkod[0]->namauser_menyetujui ?></td>
      </tr>
    </table>

    <table>
      <tr>
        <td class="text-left" colspan="7" width="10px">&nbsp;</td>
      </tr>
      <tr>
        <td class="text-left" colspan="7" width="10px"><b>PENGGUNAAN NARCOSE</b></td>
      </tr>
      <tr>
        <td class="text-center border-full" width="10px">NO</td>
        <td class="text-center border-full" width="10px">KODE</td>
        <td class="text-center border-full" width="10px">KETERANGAN</td>
        <td class="text-center border-full" width="10px">HARGA</td>
        <td class="text-center border-full" width="10px">QTY</td>
        <td class="text-center border-full" width="10px">UNIT</td>
        <td class="text-center border-full" width="10px">JUMLAH</td>
      </tr>
      <?php $number = 1; ?>
      <?php $subTotalNarcose = 0; ?>
      <?php foreach ($penggunaanNarcose as $row) { ?>
        <tr>
          <td class="text-center border-full" width="10px"><?=$number++?></td>
          <td class="text-center border-full" width="10px"><?=strtoupper($row->kode)?></td>
          <td class="text-center border-full" width="10px"><?=strtoupper($row->nama)?></td>
          <td class="text-right border-full" width="10px"><?=$row->harga?></td>
          <td class="text-center border-full" width="10px"><?=number_format($row->kuantitas)?></td>
          <td class="text-center border-full" width="10px"><?=strtoupper($row->unitpelayanan)?></td>
          <td class="text-right border-full" width="10px"><?=number_format($row->subtotal)?></td>
        </tr>
        <?php $subTotalNarcose = $subTotalNarcose + $row->subtotal; ?>
      <?php } ?>
      <tr>
        <td class="text-center border-full" colspan="6"><b>SUB TOTAL NARCOSE</b></td>
        <td class="text-right border-full"><b><?=number_format($subTotalNarcose)?></b></td>
      </tr>


      <tr>
        <td class="text-left" width="10px">&nbsp;</td>
      </tr>
      <tr>
        <td class="text-left" width="10px"><b>PENGGUNAAN OBAT</b></td>
      </tr>
      <tr>
        <td class="text-center border-full" width="10px">NO</td>
        <td class="text-center border-full" width="10px">KODE</td>
        <td class="text-center border-full" width="10px">KETERANGAN</td>
        <td class="text-center border-full" width="10px">HARGA</td>
        <td class="text-center border-full" width="10px">QTY</td>
        <td class="text-center border-full" width="10px">UNIT</td>
        <td class="text-center border-full" width="10px">JUMLAH</td>
      </tr>
      <?php $number = 1; ?>
      <?php $subTotalObat = 0; ?>
      <?php foreach ($penggunaanObat as $row) { ?>
        <tr>
          <td class="text-center border-full" width="10px"><?=$number++?></td>
          <td class="text-center border-full" width="10px"><?=strtoupper($row->kode)?></td>
          <td class="text-center border-full" width="10px"><?=strtoupper($row->nama)?></td>
          <td class="text-right border-full" width="10px"><?=$row->harga?></td>
          <td class="text-center border-full" width="10px"><?=number_format($row->kuantitas)?></td>
          <td class="text-center border-full" width="10px"><?=strtoupper($row->unitpelayanan)?></td>
          <td class="text-right border-full" width="10px"><?=number_format($row->subtotal)?></td>
        </tr>
        <?php $subTotalObat = $subTotalObat + $row->subtotal; ?>
      <?php } ?>
      <tr>
        <td class="text-center border-full" colspan="6"><b>SUB TOTAL OBAT</b></td>
        <td class="text-right border-full"><b><?=number_format($subTotalObat)?></b></td>
      </tr>


      <tr>
        <td class="text-left" width="10px">&nbsp;</td>
      </tr>
      <tr>
        <td class="text-left" width="10px"><b>PENGGUNAAN ALKES</b></td>
      </tr>
      <tr>
        <td class="text-center border-full" width="10px">NO</td>
        <td class="text-center border-full" width="10px">KODE</td>
        <td class="text-center border-full" width="10px">KETERANGAN</td>
        <td class="text-center border-full" width="10px">HARGA</td>
        <td class="text-center border-full" width="10px">QTY</td>
        <td class="text-center border-full" width="10px">UNIT</td>
        <td class="text-center border-full" width="10px">JUMLAH</td>
      </tr>
      <?php $number = 1; ?>
      <?php $subTotalAlkes = 0; ?>
      <?php foreach ($penggunaanAlkes as $row) { ?>
        <tr>
          <td class="text-center border-full" width="10px"><?=$number++?></td>
          <td class="text-center border-full" width="10px"><?=strtoupper($row->kode)?></td>
          <td class="text-center border-full" width="10px"><?=strtoupper($row->nama)?></td>
          <td class="text-right border-full" width="10px"><?=$row->harga?></td>
          <td class="text-center border-full" width="10px"><?=number_format($row->kuantitas)?></td>
          <td class="text-center border-full" width="10px"><?=strtoupper($row->unitpelayanan)?></td>
          <td class="text-right border-full" width="10px"><?=number_format($row->subtotal)?></td>
        </tr>
        <?php $subTotalAlkes = $subTotalAlkes + $row->subtotal; ?>
      <?php } ?>
      <tr>
        <td class="text-center border-full" colspan="6"><b>SUB TOTAL OBAT</b></td>
        <td class="text-right border-full"><b><?=number_format($subTotalObat)?></b></td>
      </tr>


      <tr>
        <td class="text-left" width="10px">&nbsp;</td>
      </tr>
      <tr>
        <td class="text-left" width="10px"><b>PENGGUNAAN IMPLAN</b></td>
      </tr>
      <tr>
        <td class="text-center border-full" width="10px">NO</td>
        <td class="text-center border-full" width="10px">KODE</td>
        <td class="text-center border-full" width="10px">KETERANGAN</td>
        <td class="text-center border-full" width="10px">HARGA</td>
        <td class="text-center border-full" colspan="2" width="10px">QTY</td>
        <td class="text-center border-full" width="10px">JUMLAH</td>
      </tr>
      <?php $number = 1; ?>
      <?php $subTotalImplant = 0; ?>
      <?php foreach ($penggunaanImplant as $row) { ?>
        <tr>
          <td class="text-center border-full" width="10px"><?=$number++?></td>
          <td class="text-center border-full" width="10px"><?=strtoupper($row->kode)?></td>
          <td class="text-center border-full" width="10px"><?=strtoupper($row->nama)?></td>
          <td class="text-right border-full" width="10px"><?=$row->harga?></td>
          <td class="text-center border-full" colspan="2" width="10px"><?=number_format($row->kuantitas)?></td>
          <td class="text-right border-full" width="10px"><?=number_format($row->subtotal)?></td>
        </tr>
        <?php $subTotalImplant = $subTotalImplant + $row->subtotal; ?>
      <?php } ?>
      <tr>
        <td class="text-center border-full" colspan="6"><b>SUB TOTAL IMPLANT</b></td>
        <td class="text-right border-full"><b><?=number_format($subTotalImplant)?></b></td>
      </tr>


      <tr>
        <td class="text-left" width="10px">&nbsp;</td>
      </tr>
      <tr>
        <td class="text-left" width="10px"><b>PENGGUNAAN SEWA IMPLANT</b></td>
      </tr>
      <tr>
        <td class="text-center border-full" width="10px">NO</td>
        <td class="text-center border-full" width="10px">KODE</td>
        <td class="text-center border-full" width="10px">KETERANGAN</td>
        <td class="text-center border-full" width="10px">HARGA</td>
        <td class="text-center border-full" colspan="2" width="10px">QTY</td>
        <td class="text-center border-full" width="10px">JUMLAH</td>
      </tr>
      <?php $number = 1; ?>
      <?php $subTotalSewaAlat = 0; ?>
      <?php foreach ($penggunaanSewaAlat as $row) { ?>
        <tr>
          <td class="text-center border-full" width="10px"><?=$number++?></td>
          <td class="text-center border-full" width="10px"><?=strtoupper($row->kode)?></td>
          <td class="text-center border-full" width="10px"><?=strtoupper($row->nama)?></td>
          <td class="text-right border-full" width="10px"><?=$row->harga?></td>
          <td class="text-center border-full" colspan="2" width="10px"><?=number_format($row->kuantitas)?></td>
          <td class="text-right border-full" width="10px"><?=number_format($row->subtotal)?></td>
        </tr>
        <?php $subTotalSewaAlat = $subTotalSewaAlat + $row->subtotal; ?>
      <?php } ?>
      <tr>
        <td class="text-center border-full" colspan="6"><b>SUB TOTAL SEWA ALAT</b></td>
        <td class="text-right border-full"><b><?=number_format($subTotalSewaAlat)?></b></td>
      </tr>
      <tr>
        <td class="text-right" colspan="5"></td>
        <td class="text-right"></td>
      </tr>

      <?php $grandTotal = ($subTotalNarcose + $subTotalObat + $subTotalAlkes + $subTotalImplant + $subTotalSewaAlat) ?>

      <tr>
        <td class="text-right" colspan="6"><b>GRAND TOTAL</b></td>
        <td class="text-right"><b><?=number_format($grandTotal)?></b></td>
      </tr>

      <tr>
        <td class="text-right" colspan="6"></td>
        <td class="text-right"></td>
      </tr>
      <tr>
        <td class="text-left" colspan="6"><b>CATATAN : </b></td>
      </tr>
      <tr>
        <td class="text-left" colspan="6"><?= $catatan ?></td>
      </tr>
    </table>
  </body>
</html>
