<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>FAKTUR REFUND TRANSAKSI</title>
	<style>
		@page {
			margin-top: 2em;
			margin-left: 2.2em;
			margin-bottom: 5.5em;
		}

		table {
			font-family: "arial";
			font-size: 14px !important;
			border-collapse: collapse !important;
			width: 100% !important;
		}

		p {
			font-family: "arial";
			font-size: 12px !important;
		}

		h5 {
			font-family: "arial";
			font-size: 14px !important;
			font-weight: bold;
			margin-top: 0;
			margin-bottom: 0;
			margin-left: 0;
			margin-right: 0;
		}

		td {
			padding: 0px;
			border: 0px solid #000 !important;
		}

		.header {
			font-family: "arial";
			padding-left: 1px;
			font-size: 19px !important;
			font-weight: bold;
		}

		.text-header {
			font-size: 15px !important;
		}

		.text-uppercase {
			text-transform: uppercase;
		}

		.border-bottom {
			border-bottom: 1px solid #000 !important;
		}

		.border-top {
			border-top: 1px solid #000 !important;
		}

		.border-full {
			border: 1px solid #000 !important;
		}

		.border-bottom-top {
			border-bottom: 1px solid #000 !important;
			border-top: 1px solid #000 !important;
		}

		.border-bottom-top-left {
			border-bottom: 1px solid #000 !important;
			border-top: 1px solid #000 !important;
			border-left: 1px solid #000 !important;
		}

		.border-left {
			border-left: 1px solid #000 !important;
		}

		/* border-thick */
		.border-thick-top {
			border-top: 2px solid #000 !important;
		}

		.border-thick-bottom {
			border-bottom: 2px solid #000 !important;
		}

		.border-dotted {
			border-width: 1px;
			border-bottom-style: dotted;
		}

		/* text-position */
		.text-center {
			text-align: center !important;
		}

		.text-left {
			text-align: left !important;
		}

		.text-right {
			text-align: right !important;
		}

		/* text-style */
		.text-italic {
			font-style: italic;
		}

		.text-bold {
			font-weight: bold;
		}

		fieldset {
			border: 1px solid #000;
			border-radius: 8px;
			box-shadow: 0 0 10px #000;
		}

		legend {
			background: #fff;
		}

		.p-0 {
				padding: 0;
				margin: 0;
		}

		.p-5 {
				padding: 5px;
		}
		@media print {

			html,
			body {
				height: 99%;
			}
		}
	</style>
</head>

<body>
	<table>
		<tr>
			<td colspan="5" class="text-center header">BUKTI PENGEMBALIAN / REFUND</td>
		</tr>
		<tr>
			<td style="width:20%" rowspan="10" class="text-center">
				<img src="<?=base_url()?>assets/upload/logo/logo.png" alt="" style="width: 100px;height: 100px;">
			</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td style="width:20%">No. Register</td>
			<td style="width:30%">: <?=$notransaksi?></td>
			<td style="width:20%">No. Refund</td>
			<td style="width:30%">: <?=$norefund?></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>Tgl Transaksi</td>
			<td>: <?=HumanDateLong($tanggaltransaksi)?></td>
			<td>Tgl Refund</td>
			<td>: <?=$tanggalrefund?></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>Tipe Refund</td>
			<td>: <?=$tiperefund?></td>
			<td>Payor</td>
			<td>: <?=$kelompokpasien?></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>Nama Pasien</td>
			<td>: <?=$namapasien?></td>
			<td>No. Medrec</td>
			<td>: <?=$nomedrec?></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
	</table>
	<br>

	<h5>DETAIL TRANSAKSI - REFUND TRANSAKSI</h5>
	<table class="content-2" style="margin-top: 10px">
		<tr>
			<td class="border-bottom-top text-center p-5" width="5%">NO</td>
			<td class="border-bottom-top text-center p-5" width="15%">TANGGAL TRANSAKSI</td>
			<td class="border-bottom-top text-center p-5" width="20%">TIPE TRANSAKSI</td>
			<td class="border-bottom-top text-center p-5" width="30%">NAMA TINDAKAN</td>
			<td class="border-bottom-top text-center p-5" width="10%">QTY</td>
			<td class="border-bottom-top text-center p-5" width="10%">HARGA SATUAN</td>
			<td class="border-bottom-top text-center p-5" width="10%">TOTAL</td>
		</tr>
		<?php $number = 0; ?>
		<?php foreach  ($list_transaksi as $row) { ?>
		<?php $number = $number + 1; ?>
		<tr>
			<td class="border-bottom text-center p-5"><?=$number?></td>
			<td class="border-bottom text-center p-5"><?=DMYFormat($row->tanggal)?></td>
			<td class="border-bottom text-center p-5"><?=$row->tipe?></td>
			<td class="border-bottom text-center p-5"><?=$row->tindakan?></td>
			<td class="border-bottom text-right p-5"><?= number_format($row->kuantitas, 0)?></td>
			<td class="border-bottom text-right p-5"><?= number_format($row->harga, 0)?></td>
			<td class="border-bottom text-right p-5"><?= number_format($row->totalharga, 0)?></td>
		</tr>
		<?php } ?>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="text-left border-full" rowspan="2" colspan="3" style=" margin-top: 10px">
				<div style="padding: 5px 0 5px 5px;">
					<h5 style="margin-bottom: 0">Alasan Pengembalian :</h5>
					<p style="margin-top: 2px"><?=$alasan?></p>
				</div>
			</td>
			<td class="text-right p-0">TOTAL PENGEMBALIAN :</td>
			<td class="text-right p-0"><?=number_format($totalrefund)?></td>
		</tr>
		<tr>
			<td class="text-right p-0">METODE REFUND :</td>
			<td class="text-right p-0 text-uppercase"><?=$metode?> <?=($idmetode != 1 ? '('.$bank.' : '.$norekening.')' : '')?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="text-left border-full" rowspan="2" colspan="3" style="margin-top: 10px">
				<div style="padding: 5px 0 5px 5px;">
					<h5 style="margin-bottom: 0">Terbilang :</h5>
					<p style="margin-top: 2px"><?=numbers_to_words($totalrefund)?></p>
				</div>
			</td>
			<td class="text-right p-0"></td>
			<td class="text-right p-0"></td>
		</tr>
		<tr>
			<td class="text-right p-0"></td>
			<td class="text-right p-0 text-uppercase"></td>
		</tr>
	</table>
	<br>

	<table>
			<tr>
					<td class="text-center">Yang Mengajukan / Kasir</td>
					<td></td>
					<td class="text-center">Bendahara</td>
					<td></td>
					<td class="text-center">Pasien / Keluarga Pasien</td>
			</tr>
			<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
			</tr>
			<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
			</tr>
			<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
			</tr>
			<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
			</tr>
			<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
			</tr>
			<tr>
					<td class="text-center">( Sri Puji Astuti )</td>
					<td></td>
					<td class="text-center">( Indri Septiyani )</td>
					<td></td>
					<td class="text-center">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
			</tr>
	</table>
</body>

</html>
