<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title> REFUND PENGAJUAN </title>
    <style>
        body {
            -webkit-print-color-adjust: exact;
        }
        @media print {
            table {
                font-size: 11px !important;
                border-collapse: collapse !important;
                width: 100% !important;
                font-family: "Segoe UI", Arial, sans-serif;
            }
            td {
                padding: 5px;
            }
            .content td {
                padding: 0px;
                margin: 0px;
            }
            .content-2 td {
                margin: 3px;
            }

            /* border-normal */
            .border-full {
                border: 1px solid #000 !important;
            }
            .border-bottom {
                border-bottom:1px solid #000 !important;
            }

            /* border-thick */
            .border-thick-top{
                border-top:2px solid #000 !important;
            }
            .border-thick-bottom{
                border-bottom:2px solid #000 !important;
            }

            .border-dotted{
                border-width: 1px;
                border-bottom-style: dotted;
            }

            /* text-position */
            .text-center{
                text-align: center !important;
            }
            .text-header{
                font-size: 20px !important;
            }
            .text-right{
                text-align: right !important;
            }

            /* text-style */
            .text-italic{
                font-style: italic;
            }
            .text-bold{
                font-weight: bold;
            }
            .text-upper {
                text-transform: uppercase;
            }
        }

        @media screen {
            table {
                font-family: "Courier New", Verdana, sans-serif;
                font-size: 11px !important;
                border-collapse: collapse !important;
                width: 100% !important;
            }
            td {
                padding: 5px;

            }
            .content td {
                padding: 0px;
                border: 0px solid #6033FF;
            }
            .content-2 td {
                margin: 3px;
                border: 0px solid #6033FF;
            }

            /* border-normal */
            .border-full {
                border: 0px solid #000 !important;

            }
            .text-header{
                font-size: 13px !important;
            }
            .border-bottom {
                border-bottom:1px solid #000 !important;
            }
            .border-bottom-top {
                border-bottom:1px solid #000 !important;
                border-top:1px solid #000 !important;
            }
            .border-bottom-top-left {
                border-bottom:1px solid #000 !important;
                border-top:1px solid #000 !important;
                border-left:1px solid #000 !important;
            }
            .border-left {
                border-left:1px solid #000 !important;
            }

            /* border-thick */
            .border-thick-top{
                border-top:2px solid #000 !important;
            }
            .border-thick-bottom{
                border-bottom:2px solid #000 !important;
            }

            .border-dotted{
                border-width: 1px;
                border-bottom-style: dotted;
            }

            /* text-position */
            .text-center{
                text-align: center !important;
            }
            .text-left{
                text-align: left !important;
            }
            .text-right{
                text-align: right !important;
            }

            /* text-style */
            .text-italic{
                font-style: italic;
            }
            .text-bold{
                font-weight: bold;
            }
            .text-upper {
                text-transform: uppercase;
            }
        }
    </style>
    <script type="text/javascript">

    </script>
</head>
<body>
    <table class="content">
        <tr>
            <td class="text-header" rowspan="8" width="15%">
                <img src="assets/upload/logo/logomenu.jpg" alt="" style="width: 150px;height: 50px;">
            </td>
        </tr>

    </table>

    <h4 class="text-upper" style="text-decoration: underline;text-align: center;">Permohonan Refund/Pengembalian</h4>

    <table>
        <tr>
            <td class="text-upper">Nama Pasien</td>
            <td width="5%">:</td>
            <td><?= $nama ?></td>
        </tr>
        <tr>
            <td class="text-upper">No. Rekam Medis</td>
            <td width="5%">:</td>
            <td> <?= $nomedrec ?> </td>
        </tr>
        <tr>
            <td class="text-upper">Tanggal Pemeriksaan</td>
            <td width="5%">:</td>
            <td><?= date_format(date_create($tanggal), "d F Y") ?></td>
        </tr>
        <tr>
            <td class="text-upper">Jumlah Pengembalian</td>
            <td width="5%">:</td>
            <td><?= $nominal ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td class="text-upper" width="22%">Alasan Pengembalian</td>
            <td width="5%">:</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3">
                <?= $alasan ?>
            </td>
        </tr>
    </table>

    <br>
    <br>

    <table>
        <tr>
            <td colspan="2">Bandung, <?= date("d F Y"); ?> </td>
        </tr>
        <tr>
            <td style="width:25%">Yang mengajukan,</td>
            <td style="width:75%">&nbsp;</td>
        </tr>

        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td>(Sri Puji Astuti)</td>
            <td></td>
        </tr>
    </table>
</body>
</html>
