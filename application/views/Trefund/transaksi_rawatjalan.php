<?php if (count($listPelayanan)) { ?>
<hr>

<b><span class="label label-success" style="font-size:12px">PELAYANAN</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
  <thead>
    <tr>
      <th style="width:3%">Refund</th>
      <th style="width:10%">No. Pendaftaran</th>
      <th style="width:10%">Tanggal</th>
      <th style="width:30%">Nama Tindakan</th>
      <th style="width:10%">Tarif</th>
      <th style="width:10%">Kuantitas</th>
      <th style="width:10%">Jumlah</th>
    </tr>
  </thead>
  <tbody>
    <?php $totalPelayanan = 0;?>
    <?php foreach ($listPelayanan as $row) {?>
    <tr>
      <td>
          <input type="checkbox" class="checkboxRefund" 
            data-reference="tpoliklinik_pelayanan" 
            data-iddetail="<?=$row->iddetail?>" 
            data-idtarif="<?=$row->idtarif?>" 
            data-tanggal="<?=$row->tanggal?>" 
            data-tipe="TINDAKAN RAWAT JALAN - PELAYANAN" 
            data-tindakan="<?=$row->namatarif?>" 
            data-jasasarana="<?=$row->jasasarana?>"
            data-jasasarana_disc="<?=$row->jasasarana_disc?>"
            data-jasapelayanan="<?=$row->jasapelayanan?>"
            data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
            data-bhp="<?=$row->bhp?>"
            data-bhp_disc="<?=$row->bhp_disc?>"
            data-biayaperawatan="<?=$row->biayaperawatan?>"
            data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
            data-total="<?=$row->total?>"
            data-kuantitas="<?=$row->kuantitas?>"
            data-diskon="<?=$row->diskon?>"
            data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
          >
      </td>
      <td><?=$row->nopendaftaran?></td>
      <td><?=DMYFormat($row->tanggal)?></td>
      <td><?=$row->namatarif?></td>
      <td><?=number_format($row->total)?></td>
      <td><?=number_format($row->kuantitas)?></td>
      <td><?=number_format($row->totalkeseluruhan)?></td>
    </tr>
    <?php $totalPelayanan = $totalPelayanan + $row->totalkeseluruhan;?>
    <?php } ?>
  </tbody>
  <tfoot>
    <tr>
      <td align="center" colspan="6"><b>TOTAL</b></td>
      <td class="text-bold" colspan="2"><?=number_format($totalPelayanan)?></td>
    </tr>
  </tfoot>
</table>
<? } ?>

<?php if (count($listObat)) { ?>
<hr>

<b><span class="label label-success" style="font-size:12px">OBAT</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
  <thead>
    <tr>
      <th style="width:3%">Refund</th>
      <th style="width:10%">No. Pendaftaran</th>
      <th style="width:10%">Tanggal</th>
      <th style="width:30%">Nama Obat</th>
      <th style="width:10%">Tarif</th>
      <th style="width:10%">Kuantitas</th>
      <th style="width:10%">Jumlah</th>
    </tr>
  </thead>
  <tbody>
    <?php $totalObat = 0;?>
    <?php foreach ($listObat as $row) {?>
    <tr>
      <td>
          <input type="checkbox" class="checkboxRefund" 
            data-reference="tpoliklinik_obat" 
            data-iddetail="<?=$row->iddetail?>" 
            data-idtarif="<?=$row->idtarif?>" 
            data-tanggal="<?=$row->tanggal?>" 
            data-tipe="TINDAKAN RAWAT JALAN - OBAT" 
            data-tindakan="<?=$row->namatarif?>" 
            data-jasasarana=""
            data-jasasarana_disc=""
            data-jasapelayanan=""
            data-jasapelayanan_disc=""
            data-bhp=""
            data-bhp_disc=""
            data-biayaperawatan=""
            data-biayaperawatan_disc=""
            data-total="<?=$row->hargajual?>"
            data-kuantitas="<?=$row->kuantitas?>"
            data-diskon="<?=$row->diskon?>"
            data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
          >
      </td>
      <td><?=$row->nopendaftaran?></td>
      <td><?=DMYFormat($row->tanggal)?></td>
      <td><?=$row->namatarif?></td>
      <td><?=number_format($row->hargajual)?></td>
      <td><?=number_format($row->kuantitas)?></td>
      <td><?=number_format($row->totalkeseluruhan)?></td>
    </tr>
    <?php $totalObat = $totalObat + $row->totalkeseluruhan;?>
    <?php } ?>
  </tbody>
  <tfoot>
    <tr>
      <td align="center" colspan="6"><b>TOTAL</b></td>
      <td class="text-bold" colspan="2"><?=number_format($totalObat)?></td>
    </tr>
  </tfoot>
</table>
<? } ?>

<?php if (count($listAlkes)) { ?>
<hr>

<b><span class="label label-success" style="font-size:12px">ALAT KESEHATAN</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
  <thead>
    <tr>
      <th style="width:3%">Refund</th>
      <th style="width:10%">No. Pendaftaran</th>
      <th style="width:10%">Tanggal</th>
      <th style="width:30%">Nama Obat</th>
      <th style="width:10%">Tarif</th>
      <th style="width:10%">Kuantitas</th>
      <th style="width:10%">Jumlah</th>
    </tr>
  </thead>
  <tbody>
    <?php $totalAlkes = 0;?>
    <?php foreach ($listAlkes as $row) {?>
    <tr>
      <td>
          <input type="checkbox" class="checkboxRefund" 
            data-reference="tpoliklinik_alkes" 
            data-iddetail="<?=$row->iddetail?>" 
            data-idtarif="<?=$row->idtarif?>" 
            data-tanggal="<?=$row->tanggal?>" 
            data-tipe="TINDAKAN RAWAT JALAN - ALAT KESEHATAN" 
            data-tindakan="<?=$row->namatarif?>" 
            data-jasasarana=""
            data-jasasarana_disc=""
            data-jasapelayanan=""
            data-jasapelayanan_disc=""
            data-bhp=""
            data-bhp_disc=""
            data-biayaperawatan=""
            data-biayaperawatan_disc=""
            data-total="<?=$row->hargajual?>"
            data-kuantitas="<?=$row->kuantitas?>"
            data-diskon="<?=$row->diskon?>"
            data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
          >
      </td>
      <td><?=$row->nopendaftaran?></td>
      <td><?=DMYFormat($row->tanggal)?></td>
      <td><?=$row->namatarif?></td>
      <td><?=number_format($row->hargajual)?></td>
      <td><?=number_format($row->kuantitas)?></td>
      <td><?=number_format($row->totalkeseluruhan)?></td>
    </tr>
    <?php $totalAlkes = $totalAlkes + $row->totalkeseluruhan;?>
    <?php } ?>
  </tbody>
  <tfoot>
    <tr>
      <td align="center" colspan="6"><b>TOTAL</b></td>
      <td class="text-bold" colspan="2"><?=number_format($totalAlkes)?></td>
    </tr>
  </tfoot>
</table>
<? } ?>

<?php if (count($listLaboratorium)) { ?>
<hr>

<b><span class="label label-success" style="font-size:12px">LABORATORIUM</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
  <thead>
    <tr>
      <th style="width:3%">Refund</th>
      <th style="width:10%">No. Pendaftaran</th>
      <th style="width:10%">Tanggal</th>
      <th style="width:30%">Nama Tindakan</th>
      <th style="width:10%">Tarif</th>
      <th style="width:10%">Kuantitas</th>
      <th style="width:10%">Jumlah</th>
    </tr>
  </thead>
  <tbody>
    <?php $totalLaboratorium = 0;?>
    <?php foreach ($listLaboratorium as $row) {?>
    <tr>
      <td>
          <input type="checkbox" class="checkboxRefund" 
            data-reference="trujukan_laboratorium" 
            data-iddetail="<?=$row->iddetail?>" 
            data-idtarif="<?=$row->idtarif?>" 
            data-tanggal="<?=$row->tanggal?>" 
            data-tipe="LABORATORIUM" 
            data-tindakan="<?=$row->namatarif?>" 
            data-jasasarana="<?=$row->jasasarana?>"
            data-jasasarana_disc="<?=$row->jasasarana_disc?>"
            data-jasapelayanan="<?=$row->jasapelayanan?>"
            data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
            data-bhp="<?=$row->bhp?>"
            data-bhp_disc="<?=$row->bhp_disc?>"
            data-biayaperawatan="<?=$row->biayaperawatan?>"
            data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
            data-total="<?=$row->total?>"
            data-kuantitas="<?=$row->kuantitas?>"
            data-diskon="<?=$row->diskon?>"
            data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
          >
      </td>
      <td><?=$row->norujukan?></td>
      <td><?=DMYFormat($row->tanggal)?></td>
      <td><?=$row->namatarif?></td>
      <td><?=number_format($row->total)?></td>
      <td><?=number_format($row->kuantitas)?></td>
      <td><?=number_format($row->totalkeseluruhan)?></td>
    </tr>
    <?php $totalLaboratorium = $totalLaboratorium + $row->totalkeseluruhan;?>
    <?php } ?>
  </tbody>
  <tfoot>
    <tr>
      <td align="center" colspan="6"><b>TOTAL</b></td>
      <td class="text-bold" colspan="2"><?=number_format($totalLaboratorium)?></td>
    </tr>
  </tfoot>
</table>
<? } ?>

<?php if (count($listRadiologi)) { ?>
<hr>

<b><span class="label label-success" style="font-size:12px">RADIOLOGI</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
  <thead>
    <tr>
      <th style="width:3%">Refund</th>
      <th style="width:10%">No. Pendaftaran</th>
      <th style="width:10%">Tanggal</th>
      <th style="width:30%">Nama Tindakan</th>
      <th style="width:10%">Tarif</th>
      <th style="width:10%">Kuantitas</th>
      <th style="width:10%">Jumlah</th>
    </tr>
  </thead>
  <tbody>
    <?php $totalRadiologi = 0;?>
    <?php foreach ($listRadiologi as $row) {?>
    <tr>
      <td>
          <input type="checkbox" class="checkboxRefund" 
            data-reference="trujukan_radiologi" 
            data-iddetail="<?=$row->iddetail?>" 
            data-idtarif="<?=$row->idtarif?>" 
            data-tanggal="<?=$row->tanggal?>" 
            data-tipe="RADIOLOGI" 
            data-tindakan="<?=$row->namatarif?>" 
            data-jasasarana="<?=$row->jasasarana?>"
            data-jasasarana_disc="<?=$row->jasasarana_disc?>"
            data-jasapelayanan="<?=$row->jasapelayanan?>"
            data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
            data-bhp="<?=$row->bhp?>"
            data-bhp_disc="<?=$row->bhp_disc?>"
            data-biayaperawatan="<?=$row->biayaperawatan?>"
            data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
            data-total="<?=$row->total?>"
            data-kuantitas="<?=$row->kuantitas?>"
            data-diskon="<?=$row->diskon?>"
            data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
          >
      </td>
      <td><?=$row->norujukan?></td>
      <td><?=DMYFormat($row->tanggal)?></td>
      <td><?=$row->namatarif?></td>
      <td><?=number_format($row->total)?></td>
      <td><?=number_format($row->kuantitas)?></td>
      <td><?=number_format($row->totalkeseluruhan)?></td>
    </tr>
    <?php $totalRadiologi = $totalRadiologi + $row->totalkeseluruhan;?>
    <?php } ?>
  </tbody>
  <tfoot>
    <tr>
      <td align="center" colspan="6"><b>TOTAL</b></td>
      <td class="text-bold" colspan="2"><?=number_format($totalRadiologi)?></td>
    </tr>
  </tfoot>
</table>
<? } ?>

<?php if (count($listFisioterapi)) { ?>
<hr>

<b><span class="label label-success" style="font-size:12px">FISIOTERAPI</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
  <thead>
    <tr>
      <th style="width:3%">Refund</th>
      <th style="width:10%">No. Pendaftaran</th>
      <th style="width:10%">Tanggal</th>
      <th style="width:30%">Nama Tindakan</th>
      <th style="width:10%">Tarif</th>
      <th style="width:10%">Kuantitas</th>
      <th style="width:10%">Jumlah</th>
    </tr>
  </thead>
  <tbody>
    <?php $totalFisioterapi = 0;?>
    <?php foreach ($listFisioterapi as $row) {?>
    <tr>
      <td>
          <input type="checkbox" class="checkboxRefund" 
            data-reference="trujukan_fisioterapi" 
            data-iddetail="<?=$row->iddetail?>" 
            data-idtarif="<?=$row->idtarif?>" 
            data-tanggal="<?=$row->tanggal?>" 
            data-tipe="FISIOTERAPI" 
            data-tindakan="<?=$row->namatarif?>" 
            data-jasasarana="<?=$row->jasasarana?>"
            data-jasasarana_disc="<?=$row->jasasarana_disc?>"
            data-jasapelayanan="<?=$row->jasapelayanan?>"
            data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
            data-bhp="<?=$row->bhp?>"
            data-bhp_disc="<?=$row->bhp_disc?>"
            data-biayaperawatan="<?=$row->biayaperawatan?>"
            data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
            data-total="<?=$row->total?>"
            data-kuantitas="<?=$row->kuantitas?>"
            data-diskon="<?=$row->diskon?>"
            data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
          >
      </td>
      <td><?=$row->norujukan?></td>
      <td><?=DMYFormat($row->tanggal)?></td>
      <td><?=$row->namatarif?></td>
      <td><?=number_format($row->total)?></td>
      <td><?=number_format($row->kuantitas)?></td>
      <td><?=number_format($row->totalkeseluruhan)?></td>
    </tr>
    <?php $totalFisioterapi = $totalFisioterapi + $row->totalkeseluruhan;?>
    <?php } ?>
  </tbody>
  <tfoot>
    <tr>
      <td align="center" colspan="6"><b>TOTAL</b></td>
      <td class="text-bold" colspan="2"><?=number_format($totalFisioterapi)?></td>
    </tr>
  </tfoot>
</table>
<? } ?>

<?php if (count($listFarmasi)) { ?>
<hr>

<b><span class="label label-success" style="font-size:12px">FARMASI</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
  <thead>
    <tr>
      <th style="width:3%">Refund</th>
      <th style="width:10%">No. Pendaftaran</th>
      <th style="width:10%">Tanggal</th>
      <th style="width:30%">Nama Barang</th>
      <th style="width:10%">Tarif</th>
      <th style="width:10%">Kuantitas</th>
      <th style="width:10%">Jumlah</th>
    </tr>
  </thead>
  <tbody>
    <?php $totalFarmasi = 0;?>
    <?php foreach ($listFarmasi as $row) {?>
    <?php
      if ($row->idtipe == 1) {
        $jenisBarang = 'ALAT KESEHATAN';
      } else if ($row->idtipe == 3) {
        $jenisBarang = 'OBAT';
      }
      if ($row->statusracikan) {
        $reference = 'tpasien_penjualan_racikan';
      } else if ($row->idtipe == 3) {
        $reference = 'tpasien_penjualan_nonracikan';
      }
    ?>
    <tr>
      <td>
          <input type="checkbox" class="checkboxRefund" 
            data-reference="<?=$reference?>" 
            data-iddetail="<?=$row->iddetail?>" 
            data-idtarif="<?=$row->idtarif?>" 
            data-tanggal="<?=$row->tanggal?>" 
            data-tipe="TINDAKAN FARMASI - <?=$jenisBarang?>" 
            data-tindakan="<?=$row->namatarif?>" 
            data-jasasarana=""
            data-jasasarana_disc=""
            data-jasapelayanan=""
            data-jasapelayanan_disc=""
            data-bhp=""
            data-bhp_disc=""
            data-biayaperawatan=""
            data-biayaperawatan_disc=""
            data-total="<?=$row->hargajual?>"
            data-kuantitas="<?=$row->kuantitas?>"
            data-diskon="<?=$row->diskon?>"
            data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
          >
      </td>
      <td><?=$row->nopenjualan?></td>
      <td><?=DMYFormat($row->tanggal)?></td>
      <td><?=$row->namatarif?></td>
      <td><?=number_format($row->hargajual)?></td>
      <td><?=number_format($row->kuantitas)?></td>
      <td><?=number_format($row->totalkeseluruhan)?></td>
    </tr>
    <?php $totalFarmasi = $totalFarmasi + $row->totalkeseluruhan;?>
    <?php } ?>
  </tbody>
  <tfoot>
    <tr>
      <td align="center" colspan="6"><b>TOTAL</b></td>
      <td class="text-bold" colspan="2"><?=number_format($totalFarmasi)?></td>
    </tr>
  </tfoot>
</table>
<? } ?>

<?php if (count($listAdministrasi)) { ?>
<hr>

<b><span class="label label-success" style="font-size:12px">ADMINISTRASI</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
  <thead>
    <tr>
      <th style="width:3%">Refund</th>
      <th style="width:10%">No. Pendaftaran</th>
      <th style="width:10%">Tanggal</th>
      <th style="width:30%">Nama Tindakan</th>
      <th style="width:10%">Tarif</th>
      <th style="width:10%">Kuantitas</th>
      <th style="width:10%">Jumlah</th>
    </tr>
  </thead>
  <tbody>
    <?php $totalAdministrasi = 0;?>
    <?php foreach ($listAdministrasi as $row) {?>
    <tr>
      <td>
          <input type="checkbox" class="checkboxRefund" 
            data-reference="tpoliklinik_administrasi" 
            data-iddetail="<?=$row->iddetail?>" 
            data-idtarif="<?=$row->idtarif?>" 
            data-tanggal="<?=$row->tanggal?>" 
            data-tipe="ADMINISTRASI" 
            data-tindakan="<?=$row->namatarif?>" 
            data-jasasarana="<?=$row->jasasarana?>"
            data-jasasarana_disc="<?=$row->jasasarana_disc?>"
            data-jasapelayanan="<?=$row->jasapelayanan?>"
            data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
            data-bhp="<?=$row->bhp?>"
            data-bhp_disc="<?=$row->bhp_disc?>"
            data-biayaperawatan="<?=$row->biayaperawatan?>"
            data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
            data-total="<?=$row->total?>"
            data-kuantitas="<?=$row->kuantitas?>"
            data-diskon="<?=$row->diskon?>"
            data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
          >
      </td>
      <td><?=$row->nopendaftaran?></td>
      <td><?=DMYFormat($row->tanggal)?></td>
      <td><?=$row->namatarif?></td>
      <td><?=number_format($row->total)?></td>
      <td><?=number_format($row->kuantitas)?></td>
      <td><?=number_format($row->totalkeseluruhan)?></td>
    </tr>
    <?php $totalAdministrasi = $totalAdministrasi + $row->totalkeseluruhan;?>
    <?php } ?>
  </tbody>
  <tfoot>
    <tr>
      <td align="center" colspan="6"><b>TOTAL</b></td>
      <td class="text-bold" colspan="2"><?=number_format($totalAdministrasi)?></td>
    </tr>
  </tfoot>
</table>
<? } ?>
