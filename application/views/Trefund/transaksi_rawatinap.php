		<?php $ranapRuangan = $this->Trawatinap_verifikasi_model->viewRincianRanapRuanganValidasi($idpendaftaran); ?>
		<?php if (count($ranapRuangan)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">RUANGAN</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Pendaftaran</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Ruangan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Hari</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:20%">Jumlah Setelah Diskon</th>
				</tr>
			</thead>
			<tbody id="tempRuanganRanap">
				<?php $totalRanapRuangan = 0;?>
				<?php foreach ($ranapRuangan as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trawatinap_ruangan" 
							data-iddetail="<?=$row->id?>" 
							data-tanggal="<?=$row->tanggaldari?>" 
							data-tipe="RUANGAN" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->jumlahhari?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggaldari)?> - <?=DMYFormat($row->tanggalsampai)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->total)?></td>
					<td><?=number_format($row->jumlahhari)?></td>
					<td><?=number_format($row->total * $row->jumlahhari)?></td>
					<td><?= calcDiscountPercent($row->diskon, ($row->total * $row->jumlahhari)); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
				</tr>
				<?php $totalRanapRuangan = $totalRanapRuangan + $row->totalkeseluruhan;?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="2"><?=number_format($totalRanapRuangan)?></td>
				</tr>
			</tfoot>
		</table>
		<? } ?>

		<?php $ranapFullCare = $this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 1); ?>
		<?php if (count($ranapFullCare)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
		<b><span class="label label-default" style="font-size:12px">FULL CARE</span></b>
		<table id="historyTindakan" class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Pendaftaran</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Dokter</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalRanapFullCare = 0;?>
				<?php $totalRanapFullCareVerif = 0;?>
				<?php foreach ($ranapFullCare as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trawatinap_tindakan" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RAWAT INAP - FULL CARE" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
				</tr>
				<?php
				$totalRanapFullCare = $totalRanapFullCare + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalRanapFullCareVerif = $totalRanapFullCareVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalRanapFullCare)?></td>
				</tr>
			</tfoot>
		</table>
		<? } ?>

		<?php $ranapECG = $this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 2); ?>
		<?php if (count($ranapECG)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
		<b><span class="label label-default" style="font-size:12px">ECG</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Pendaftaran</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Dokter</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalRanapECG = 0;?>
				<?php $totalRanapECGVerif = 0;?>
				<?php foreach ($ranapECG as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trawatinap_tindakan" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RAWAT INAP - ECG" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
				</tr>
				<?php
				$totalRanapECG = $totalRanapECG + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalRanapECGVerif = $totalRanapECGVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalRanapECG)?></td>
				</tr>
			</tfoot>
		</table>
		<? } ?>

		<?php $ranapVisite = $this->Trawatinap_verifikasi_model->viewRincianRanapVisite($idpendaftaran); ?>
		<?php if (count($ranapVisite)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
		<b><span class="label label-default" style="font-size:12px">VISITE DOKTER</span></b>
		<table id="historyVisiteDokter" class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Pendaftaran</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:35%">Dokter</th>
					<th style="width:10%">Ruangan</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalRanapVisite = 0;?>
				<?php $totalRanapVisiteVerif = 0;?>
				<?php foreach ($ranapVisite as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trawatinap_visite" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RAWAT INAP - VISITE DOKTER" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namadokter?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namadokter?></td>
					<td><?=$row->namaruangan?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
				</tr>
				<?php
				$totalRanapVisite = $totalRanapVisite + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalRanapVisiteVerif = $totalRanapVisiteVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="6"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalRanapVisite)?></td>
				</tr>
			</tfoot>
		</table>
		<? } ?>

		<?php $ranapSewaAlat = $this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 4); ?>
		<?php if (count($ranapSewaAlat)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
		<b><span class="label label-default" style="font-size:12px">SEWA ALAT</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Pendaftaran</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Dokter</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalRanapSewaAlat = 0;?>
				<?php $totalRanapSewaAlatVerif = 0;?>
				<?php foreach ($ranapSewaAlat as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trawatinap_tindakan" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RAWAT INAP - SEWA ALAT" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
				</tr>
				<?php
				$totalRanapSewaAlat = $totalRanapSewaAlat + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalRanapSewaAlatVerif = $totalRanapSewaAlatVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalRanapSewaAlat)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $ranapAmbulance = $this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 5); ?>
		<?php if (count($ranapAmbulance)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
		<b><span class="label label-default" style="font-size:12px">AMBULANCE</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Pendaftaran</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Dokter</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalRanapAmbulance = 0;?>
				<?php $totalRanapAmbulanceVerif = 0;?>
				<?php foreach ($ranapAmbulance as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trawatinap_tindakan" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RAWAT INAP - AMBULANCE" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
				</tr>
				<?php
				$totalRanapAmbulance = $totalRanapAmbulance + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalRanapAmbulanceVerif = $totalRanapAmbulanceVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalRanapAmbulance)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $ranapObat = $this->Trawatinap_verifikasi_model->viewRincianRanapObat($idpendaftaran); ?>
		<?php $ranapFarmasiObat = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapObat($idpendaftaran); ?>
		<?php $ranapFarmasiObatRetur = $this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapObat($idpendaftaran); ?>
		<?php if (count($ranapObat) || count($ranapFarmasiObat) || count($ranapFarmasiObatRetur)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
		<b><span class="label label-default" style="font-size:12px">OBAT</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Transaksi</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Obat</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Unit Pelayanan</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalRanapObat = 0;?>
				<?php $totalRanapObatVerif = 0;?>
				<?php foreach ($ranapObat as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tpasien_penjualan_nonracikan" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RAWAT INAP - OBAT" 
							data-idtarif="<?=$row->idobat?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none"><?=$row->idunit?></td>
					<td><?=$row->namaunit?></td>
				</tr>
				<?php
				$totalRanapObat = $totalRanapObat + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalRanapObatVerif = $totalRanapObatVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>


				<?php $totalFarmasiObat = 0;?>
				<?php $totalFarmasiObatVerif = 0;?>
				<?php foreach ($ranapFarmasiObat as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tpasien_penjualan_nonracikan" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RAWAT INAP - OBAT" 
							data-idtarif="<?=$row->idbarang?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopenjualan?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none"><?=$row->idunitpelayanan?></td>
					<td><?=$row->unitpelayanan?></td>
				</tr>
				<?php
				$totalFarmasiObat = $totalFarmasiObat + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalFarmasiObatVerif = $totalFarmasiObatVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>


				<?php $totalFarmasiReturObat = 0;?>
				<?php $totalFarmasiReturObatVerif = 0;?>
				<?php foreach ($ranapFarmasiObatRetur as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tpasien_pengembalian_detail" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RAWAT INAP - OBAT" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopengembalian?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none"><?=$row->idunitpelayanan?></td>
					<td><?=$row->unitpelayanan?></td>
				</tr>
				<?php
				$totalFarmasiReturObat = $totalFarmasiReturObat + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalFarmasiReturObatVerif = $totalFarmasiReturObatVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalRanapObat + $totalFarmasiObat + $totalFarmasiReturObat)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $ranapAlkes = $this->Trawatinap_verifikasi_model->viewRincianRanapAlkes($idpendaftaran); ?>
		<?php $ranapFarmasiAlkes = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapAlkes($idpendaftaran); ?>
		<?php $ranapFarmasiAlkesRetur = $this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapAlkes($idpendaftaran); ?>
		<?php if (count($ranapAlkes) || count($ranapFarmasiAlkes) || count($ranapFarmasiAlkesRetur)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
		<b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Transaksi</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Alat Kesehatan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Unit Pelayanan</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalRanapAlkes = 0;?>
				<?php $totalRanapAlkesVerif = 0;?>
				<?php foreach ($ranapAlkes as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trawatinap_alkes" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RAWAT INAP - ALAT KESEHATAN" 
							data-idtarif="<?=$row->idalkes?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none"><?=$row->idunit?></td>
					<td><?=$row->namaunit?></td>
				</tr>
				<?php
				$totalRanapAlkes = $totalRanapAlkes + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalRanapAlkesVerif = $totalRanapAlkesVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>

				
				<?php $totalFarmasiAlkes = 0;?>
				<?php $totalFarmasiAlkesVerif = 0;?>
				<?php foreach ($ranapFarmasiAlkes as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tpasien_penjualan_nonracikan" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RAWAT INAP - ALAT KESEHATAN" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopenjualan?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none">1</td>
					<td>Farmasi</td>
				</tr>
				<?php
				$totalFarmasiAlkes = $totalFarmasiAlkes + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalFarmasiAlkesVerif = $totalFarmasiAlkesVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>


				<?php $totalFarmasiReturAlkes = 0;?>
				<?php $totalFarmasiReturAlkesVerif = 0;?>
				<?php foreach ($ranapFarmasiAlkesRetur as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tpasien_pengembalian_detail" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RAWAT INAP - ALAT KESEHATAN" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopengembalian?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none"><?=$row->idunitpelayanan?></td>
					<td><?=$row->unitpelayanan?></td>
				</tr>
				<?php
				$totalFarmasiReturAlkes = $totalFarmasiReturAlkes + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalFarmasiReturAlkesVerif = $totalFarmasiReturAlkesVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalRanapAlkes + $totalFarmasiAlkes + $totalFarmasiReturAlkes)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>


		<?php $ranapAlkesBantu = $this->Trawatinap_verifikasi_model->viewRincianRanapAlkesBantu($idpendaftaran); ?>
		<?php $ranapFarmasiAlkesBantu = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapAlkesBantu($idpendaftaran); ?>
		<?php $ranapFarmasiAlkesBantuRetur = $this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapAlkesBantu($idpendaftaran); ?>
		<?php if (count($ranapAlkesBantu) || count($ranapFarmasiAlkesBantu) || count($ranapFarmasiAlkesBantuRetur)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
		<b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
		<b><span class="label label-default" style="font-size:12px">ALAT BANTU</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Transaksi</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Alat Kesehatan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Unit Pelayanan</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalRanapAlkesBantu = 0;?>
				<?php $totalRanapAlkesBantuVerif = 0;?>
				<?php foreach ($ranapAlkesBantu as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trawatinap_alkes" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RAWAT INAP - ALAT BANTU" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none"><?=$row->idunit?></td>
					<td><?=$row->namaunit?></td>
				</tr>
				<?php
				$totalRanapAlkesBantu = $totalRanapAlkesBantu + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalRanapAlkesBantuVerif = $totalRanapAlkesBantuVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>

				<?php $totalFarmasiAlkesBantu = 0;?>
				<?php $totalFarmasiAlkesBantuVerif = 0;?>
				<?php foreach ($ranapFarmasiAlkesBantu as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tpasien_penjualan_nonracikan" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RAWAT INAP - ALAT BANTU" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none">1</td>
					<td>Farmasi</td>
				</tr>
				<?php
				$totalFarmasiAlkesBantu = $totalFarmasiAlkesBantu + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalFarmasiAlkesBantuVerif = $totalFarmasiAlkesBantuVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>

				<?php $totalFarmasiReturAlkesBantu = 0;?>
				<?php $totalFarmasiReturAlkesBantuVerif = 0;?>
				<?php foreach ($ranapFarmasiAlkesBantuRetur as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tpasien_pengembalian_detail" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RAWAT INAP - ALAT BANTU" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopengembalian?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none"><?=$row->idunitpelayanan?></td>
					<td><?=$row->unitpelayanan?></td>
				</tr>
				<?php
				$totalFarmasiReturAlkesBantu = $totalFarmasiReturAlkesBantu + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalFarmasiReturAlkesBantuVerif = $totalFarmasiReturAlkesBantuVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalRanapAlkesBantu + $totalFarmasiAlkesBantu + $totalFarmasiReturAlkesBantu)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $ranapLainLain = $this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 6); ?>
		<?php if (count($ranapLainLain)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
		<b><span class="label label-default" style="font-size:12px">LAIN-LAIN</span></b>
		<table id="historyTindakan" class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Pendaftaran</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Dokter</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalRanapLainLain = 0;?>
				<?php $totalRanapLainLainVerif = 0;?>
				<?php foreach ($ranapLainLain as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trawatinap_tindakan" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RAWAT INAP - LAIN-LAIN" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
				</tr>
				<?php
				$totalRanapLainLain = $totalRanapLainLain + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalRanapLainLainVerif = $totalRanapLainLainVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalRanapLainLain)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $igdTindakan = $this->Trawatinap_verifikasi_model->viewRincianRajalTindakanFilteredIGD($idpendaftaran); ?>
		<?php if (count($igdTindakan)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN IGD</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Pendaftaran</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Dokter</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalPoli = 0;?>
				<?php $totalPoliVerif = 0;?>
				<?php foreach ($igdTindakan as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tpoliklinik_pelayanan" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN IGD" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
				</tr>
				<?php
				$totalPoli = $totalPoli + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalPoliVerif = $totalPoliVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalPoli)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $igdObat = $this->Trawatinap_verifikasi_model->viewRincianRajalObatFilteredIGD($idpendaftaran, 2); ?>
		<?php $farmasiObat = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRajal($idpendaftaran, 3); ?>
		<?php $igdFarmasiObatRetur = $this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRajalObat($idpendaftaran); ?>
		<?php if (count($igdObat) || count($farmasiObat) || count($igdFarmasiObatRetur)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN IGD</span></b>
		<b><span class="label label-default" style="font-size:12px">OBAT</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Transaksi</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Obat</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Unit Pelayanan</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalPoliObat = 0;?>
				<?php $totalPoliObatVerif = 0;?>
				<?php foreach ($igdObat as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tpoliklinik_obat" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN IGD - OBAT" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none"><?=$row->idunit?></td>
					<td><?=$row->namaunit?></td>
				</tr>
				<?php
				$totalPoliObat = $totalPoliObat + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalPoliObatVerif = $totalPoliObatVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>

				<?php $totalFarmasiObatIGD = 0;?>
				<?php $totalFarmasiObatIGDVerif = 0;?>
				<?php foreach ($farmasiObat as $row) { ?>
				<?php $subtotal = $row->hargajual * $row->kuantitas ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tpasien_penjualan_nonracikan" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN IGD - OBAT" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopenjualan?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none">1</td>
					<td>Farmasi</td>
				</tr>
				<?php
				$totalFarmasiObatIGD = $totalFarmasiObatIGD + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalFarmasiObatIGDVerif = $totalFarmasiObatIGDVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>

				<?php $totalFarmasiRajalReturObat = 0; ?>
				<?php $totalFarmasiRajalReturObatVerif = 0; ?>
				<?php foreach ($igdFarmasiObatRetur as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tpasien_pengembalian_detail" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN IGD - OBAT" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopengembalian?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none"><?=$row->idunitpelayanan?></td>
					<td><?=$row->unitpelayanan?></td>
				</tr>
				<?php
				$totalFarmasiRajalReturObat = $totalFarmasiRajalReturObat + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalFarmasiRajalReturObatVerif = $totalFarmasiRajalReturObatVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalPoliObat + $totalFarmasiObatIGD + $totalFarmasiRajalReturObat)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $igdAlkes = $this->Trawatinap_verifikasi_model->viewRincianRajalAlkesFilteredIGD($idpendaftaran); ?>
		<?php $igdFarmasiAlkes = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRajal($idpendaftaran, 1); ?>
		<?php $igdFarmasiAlkesRetur = $this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRajalAlkes($idpendaftaran); ?>
		<?php if (count($igdAlkes) || count($igdFarmasiAlkes) || count($igdFarmasiAlkesRetur)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN IGD</span></b>
		<b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Transaksi</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Alat Kesehatan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Unit Pelayanan</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalPoliAlkes = 0;?>
				<?php $totalPoliAlkesVerif = 0;?>
				<?php foreach ($igdAlkes as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tpoliklinik_alkes" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN IGD - ALAT KESEHATAN" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopenjualan?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none"><?=$row->idunitpelayanan?></td>
					<td><?=$row->unitpelayanan?></td>
				</tr>
				<?php
				$totalPoliAlkes = $totalPoliAlkes + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalPoliAlkesVerif = $totalPoliAlkesVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>


				<?php $totalFarmasiAlkes = 0;?>
				<?php $totalFarmasiAlkesVerif = 0;?>
				<?php foreach ($igdFarmasiAlkes as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tpoliklinik_alkes" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN IGD - ALAT KESEHATAN" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopenjualan?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none"><?=$row->idunitpelayanan?></td>
					<td><?=$row->unitpelayanan?></td>
				</tr>
				<?php
				$totalFarmasiAlkes = $totalFarmasiAlkes + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalFarmasiAlkesVerif = $totalFarmasiAlkesVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>

				<?php $totalFarmasiRajalReturAlkes = 0; ?>
				<?php $totalFarmasiRajalReturAlkesVerif = 0; ?>
				<?php foreach ($igdFarmasiAlkesRetur as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tpasien_pengembalian_detail" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN IGD - ALAT KESEHATAN" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopengembalian?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none"><?=$row->idunitpelayanan?></td>
					<td><?=$row->unitpelayanan?></td>
				</tr>
				<?php
				$totalFarmasiRajalReturAlkes = $totalFarmasiRajalReturAlkes + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalFarmasiRajalReturAlkesVerif = $totalFarmasiRajalReturAlkesVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalPoliAlkes + $totalFarmasiAlkes + $totalFarmasiRajalReturAlkes)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $laboratorium = $this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 1); ?>
		<?php if (count($laboratorium)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN LABORATORIUM</span></b>
		<b><span class="label label-default" style="font-size:12px">UMUM</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Rujukan</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Dokter</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalLab = 0;?>
				<?php $totalLabVerif = 0;?>
				<?php foreach ($laboratorium as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trujukan_laboratorium_detail" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN LABORATORIUM - UMUM" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->norujukan?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
				</tr>
				<?php
				$totalLab = $totalLab + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalLabVerif = $totalLabVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalLab)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $laboratoriumPA = $this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 2); ?>
		<?php if (count($laboratoriumPA)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN LABORATORIUM</span></b>
		<b><span class="label label-default" style="font-size:12px">PATHOLOGI ANATOMI</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Rujukan</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Dokter</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalLabPA = 0;?>
				<?php $totalLabPAVerif = 0;?>
				<?php foreach ($laboratoriumPA as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trujukan_laboratorium_detail" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN LABORATORIUM - PATHOLOGI ANATOMI" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->norujukan?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
				</tr>
				<?php
				$totalLabPA = $totalLabPA + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalLabPAVerif = $totalLabPAVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalLabPA)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $laboratoriumPMI = $this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 3); ?>
		<?php if (count($laboratoriumPMI)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN LABORATORIUM</span></b>
		<b><span class="label label-default" style="font-size:12px">PMI</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Rujukan</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Dokter</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalLabPMI = 0;?>
				<?php $totalLabPMIVerif = 0;?>
				<?php foreach ($laboratoriumPMI as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trujukan_laboratorium_detail" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN LABORATORIUM - PMI" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->norujukan?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
				</tr>
				<?php
				$totalLabPMI = $totalLabPMI + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalLabPMIVerif = $totalLabPMIVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalLabPMI)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $radiologiXRAY = $this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 1); ?>
		<?php if (count($radiologiXRAY)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
		<b><span class="label label-default" style="font-size:12px">X-RAY</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Rujukan</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Dokter</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalRadXray = 0;?>
				<?php $totalRadXrayVerif = 0;?>
				<?php foreach ($radiologiXRAY as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trujukan_radiologi_detail" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RADIOLOGI - X-RAY" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->norujukan?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
				</tr>
				<?php
				$totalRadXray = $totalRadXray + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalRadXrayVerif = $totalRadXrayVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalRadXray)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $radiologiUSG = $this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 2); ?>
		<?php if (count($radiologiUSG)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
		<b><span class="label label-default" style="font-size:12px">USG</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Rujukan</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Dokter</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalRadUSG = 0;?>
				<?php $totalRadUSGVerif = 0;?>
				<?php foreach ($radiologiUSG as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trujukan_radiologi_detail" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RADIOLOGI - USG" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->norujukan?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
				</tr>
				<?php
				$totalRadUSG = $totalRadUSG + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalRadUSGVerif = $totalRadUSGVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalRadUSG)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $radiologiCTSCAN = $this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 3); ?>
		<?php if (count($radiologiCTSCAN)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
		<b><span class="label label-default" style="font-size:12px">CT SCAN</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Rujukan</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Dokter</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalRadCTScan = 0;?>
				<?php $totalRadCTScanVerif = 0;?>
				<?php foreach ($radiologiCTSCAN as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trujukan_radiologi_detail" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RADIOLOGI - CT-SCAN" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->norujukan?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
				</tr>
				<?php
				$totalRadCTScan = $totalRadCTScan + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalRadCTScanVerif = $totalRadCTScanVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalRadCTScan)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $radiologiMRI = $this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 4); ?>
		<?php if (count($radiologiMRI)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
		<b><span class="label label-default" style="font-size:12px">MRI</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Rujukan</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Dokter</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalRadMRI = 0;?>
				<?php $totalRadMRIVerif = 0;?>
				<?php foreach ($radiologiMRI as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trujukan_radiologi_detail" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RADIOLOGI - MRI" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->norujukan?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
				</tr>
				<?php
				$totalRadMRI = $totalRadMRI + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalRadMRIVerif = $totalRadMRIVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalRadMRI)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $radiologiBMD = $this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 5); ?>
		<?php if (count($radiologiBMD)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
		<b><span class="label label-default" style="font-size:12px">BMD</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Rujukan</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Dokter</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalRadBMD = 0;?>
				<?php $totalRadBMDVerif = 0;?>
				<?php foreach ($radiologiBMD as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trujukan_radiologi_detail" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN RADIOLOGI - BMD" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->norujukan?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
				</tr>
				<?php
				$totalRadBMD = $totalRadBMD + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalRadBMDVerif = $totalRadBMDVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalRadBMD)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $fisioterapi = $this->Trawatinap_verifikasi_model->viewRincianFisioterapi($idpendaftaran); ?>
		<?php if (count($fisioterapi)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN FISIOTERAPI</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Rujukan</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Dokter</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalFisio = 0;?>
				<?php $totalFisioVerif = 0;?>
				<?php foreach ($fisioterapi as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trujukan_fisioterapi_detail" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN FISIOTERAPI" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->norujukan?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
				</tr>
				<?php
				$totalFisio = $totalFisio + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalFisioVerif = $totalFisioVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalFisio)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $operasiSewaAlat = $this->Trawatinap_verifikasi_model->viewRincianOperasiSewaAlat($idpendaftaran); ?>
		<?php if (count($operasiSewaAlat)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
		<b><span class="label label-default" style="font-size:12px">SEWA ALAT</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Pendaftaran</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Dokter</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalOKSewaAlat = 0;?>
				<?php $totalOKSewaAlatVerif = 0;?>
				<?php foreach ($operasiSewaAlat as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tkamaroperasi_sewaalat" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggaloperasi?>" 
							data-tipe="TINDAKAN OPERASI - SEWA ALAT" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td>-</td>
				</tr>
				<?php
				$totalOKSewaAlat = $totalOKSewaAlat + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalOKSewaAlatVerif = $totalOKSewaAlatVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalOKSewaAlat)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $operasiAlkes = $this->Trawatinap_verifikasi_model->viewRincianOperasiAlkes($idpendaftaran); ?>
		<?php if (count($operasiAlkes)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
		<b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Transaksi</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Alat Kesehatan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Unit Pelayanan</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalOKAlkes = 0;?>
				<?php $totalOKAlkesVerif = 0;?>
				<?php foreach ($operasiAlkes as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tkamaroperasi_alkes" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN OPERASI - ALAT KESEHATAN" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none"><?=$row->idunitpelayanan?></td>
					<td><?=$row->unitpelayanan?></td>
				</tr>
				<?php
				$totalOKAlkes = $totalOKAlkes + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalOKAlkesVerif = $totalOKAlkesVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalOKAlkes)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $operasiObat = $this->Trawatinap_verifikasi_model->viewRincianOperasiObat($idpendaftaran); ?>
		<?php if (count($operasiObat)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
		<b><span class="label label-default" style="font-size:12px">OBAT</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Transaksi</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Obat</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Unit Pelayanan</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalOKObat = 0;?>
				<?php $totalOKObatVerif = 0;?>
				<?php foreach ($operasiObat as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tkamaroperasi_obat" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN OPERASI - OBAT" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none"><?=$row->idunitpelayanan?></td>
					<td><?=$row->unitpelayanan?></td>
				</tr>
				<?php
				$totalOKObat = $totalOKObat + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalOKObatVerif = $totalOKObatVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalOKObat)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $operasiObatNarcose = $this->Trawatinap_verifikasi_model->viewRincianOperasiNarcose($idpendaftaran); ?>
		<?php if (count($operasiObatNarcose)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
		<b><span class="label label-default" style="font-size:12px">OBAT NARCOSE</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Transaksi</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Obat</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Unit Pelayanan</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalOKObatNarcose = 0;?>
				<?php $totalOKObatNarcoseVerif = 0;?>
				<?php foreach ($operasiObatNarcose as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tkamaroperasi_narcose" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN OPERASI - OBAT NARCOSE" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none"><?=$row->idunitpelayanan?></td>
					<td><?=$row->unitpelayanan?></td>
				</tr>
				<?php
				$totalOKObatNarcose = $totalOKObatNarcose + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalOKObatNarcoseVerif = $totalOKObatNarcoseVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalOKObatNarcose)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $operasiImplan = $this->Trawatinap_verifikasi_model->viewRincianOperasiImplan($idpendaftaran); ?>
		<?php if (count($operasiImplan)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
		<b><span class="label label-default" style="font-size:12px">IMPLAN</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Transaksi</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Implan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
					<th style="width:10%">Unit Pelayanan</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalOKImplan = 0;?>
				<?php $totalOKImplanVerif = 0;?>
				<?php foreach ($operasiImplan as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tkamaroperasi_implan" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN OPERASI - IMPLAN" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 							
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total="<?=$row->hargajual?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td style="display: none"><?=number_format($row->hargadasar)?></td>
					<td style="display: none"><?=number_format($row->margin)?></td>
					<td><?=number_format($row->hargajual)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
					<td style="display: none"><?=$row->idunitpelayanan?></td>
					<td><?=$row->unitpelayanan?></td>
				</tr>
				<?php
				$totalOKImplan = $totalOKImplan + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalOKImplanVerif = $totalOKImplanVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalOKImplan)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $operasiSewaKamar = $this->Trawatinap_verifikasi_model->viewRincianOperasiKamar($idpendaftaran); ?>
		<?php if (count($operasiSewaKamar)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
		<b><span class="label label-default" style="font-size:12px">SEWA KAMAR</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Pendaftaran</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:20%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:5%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:20%">Jumlah Setelah Diskon</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalOKSewaKamar = 0;?>
				<?php $totalOKSewaKamarVerif = 0;?>
				<?php foreach ($operasiSewaKamar as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tkamaroperasi_ruangan" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN OPERASI - SEWA KAMAR" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
				</tr>
				<?php
				$totalOKSewaKamar = $totalOKSewaKamar + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalOKSewaKamarVerif = $totalOKSewaKamarVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalOKSewaKamar)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $operasiJasaDO = $this->Trawatinap_verifikasi_model->viewRincianOperasiJasaDokterOpertor($idpendaftaran); ?>
		<?php if (count($operasiJasaDO)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
		<b><span class="label label-default" style="font-size:12px">JASA DOKTER OPERATOR</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Pendaftaran</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:10%">Tindakan</th>
					<th style="width:15%">Nama Dokter</th>
					<th style="width:10%">Tarif</th>
					<th style="width:10%">Kuantitas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:5%">Diskon (%)</th>
					<th style="width:10%">Jumlah Setelah Diskon</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalOKJasaDokterOperator = 0;?>
				<?php $totalOKJasaDokterOperatorVerif = 0;?>
				<?php foreach ($operasiJasaDO as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tkamaroperasi_jasado" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN OPERASI - JASA DOKTER OPERATOR" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=$row->namadokter?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->kuantitas)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
				</tr>
				<?php
				$totalOKJasaDokterOperator = $totalOKJasaDokterOperator + $row->totalkeseluruhan;
				if ($row->statusverifikasi == 1) {
					$totalOKJasaDokterOperatorVerif = $totalOKJasaDokterOperatorVerif + $row->totalkeseluruhan;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalOKJasaDokterOperator)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $operasiJasaDA = $this->Trawatinap_verifikasi_model->viewRincianOperasiJasaDokterAnesthesi($idpendaftaran); ?>
		<?php if (count($operasiJasaDA)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
		<b><span class="label label-default" style="font-size:12px">JASA DOKTER ANESTHESI</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Pendaftaran</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:10%">Tindakan</th>
					<th style="width:15%">Nama Dokter</th>
					<th style="width:10%">Nominal Acuan</th>
					<th style="width:7%">Persentase (%)</th>
					<th style="width:10%">Tarif</th>
					<th style="width:8%">Diskon (%)</th>
					<th style="width:10%">Total Tarif</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalOKJasaDokterAnesthesi = 0;?>
				<?php $totalOKJasaDokterAnesthesiVerif = 0;?>
				<?php foreach ($operasiJasaDA as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tkamaroperasi_jasada" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN OPERASI - JASA DOKTER ANESTHESI" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total=""
							data-kuantitas=""
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->grandtotal?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=$row->namadokter?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->persen)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->grandtotal)?></td>
				</tr>
				<?php
				$totalOKJasaDokterAnesthesi = $totalOKJasaDokterAnesthesi + $row->grandtotal;
				if ($row->statusverifikasi == 1) {
					$totalOKJasaDokterAnesthesiVerif = $totalOKJasaDokterAnesthesiVerif + $row->grandtotal;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="8"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalOKJasaDokterAnesthesi)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $operasiJasaAsisten = $this->Trawatinap_verifikasi_model->viewRincianOperasiJasaAsisten($idpendaftaran); ?>
		<?php if (count($operasiJasaAsisten)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
		<b><span class="label label-default" style="font-size:12px">JASA ASISTEN</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Pendaftaran</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:10%">Tindakan</th>
					<th style="width:15%">Nama Dokter</th>
					<th style="width:10%">Nominal Acuan</th>
					<th style="width:7%">Persentase (%)</th>
					<th style="width:10%">Tarif</th>
					<th style="width:8%">Diskon (%)</th>
					<th style="width:10%">Total Tarif</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalOKJasaAsisten = 0;?>
				<?php $totalOKJasaAsistenVerif = 0;?>
				<?php foreach ($operasiJasaAsisten as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tkamaroperasi_jasaa" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="TINDAKAN OPERASI - JASA ASISTEN" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana=""
							data-jasasarana_disc=""
							data-jasapelayanan=""
							data-jasapelayanan_disc=""
							data-bhp=""
							data-bhp_disc=""
							data-biayaperawatan=""
							data-biayaperawatan_disc=""
							data-total=""
							data-kuantitas=""
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->grandtotal?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=$row->namapegawai?></td>
					<td><?=number_format($row->subtotal)?></td>
					<td><?=number_format($row->persen)?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->grandtotal)?></td>
				</tr>
				<?php
				$totalOKJasaAsisten = $totalOKJasaAsisten + $row->grandtotal;
				if ($row->statusverifikasi == 1) {
					$totalOKJasaAsistenVerif = $totalOKJasaAsistenVerif + $row->grandtotal;
				}
				?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="7"><b>TOTAL</b></td>
					<td class="text-bold" colspan="3"><?=number_format($totalOKJasaAsisten)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $administrasiRajal = $this->Trawatinap_verifikasi_model->viewRincianAdministrasiRawatJalan($idpendaftaran); ?>
		<?php if (count($administrasiRajal)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">ADMINISTRASI</span></b>
		<b><span class="label label-default" style="font-size:12px">RAWAT JALAN</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Pendaftaran</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:30%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:10%">Diskon (%)</th>
					<th style="width:20%">Tarif Setelah Diskon</th>
				</tr>
			</thead>
			<tbody>
				<?php $totalAdmRajal = 0;?>
				<?php foreach ($administrasiRajal as $row) { ?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="tpoliklinik_administrasi" 
							data-iddetail="<?=$row->iddetail?>" 
							data-tanggal="<?=$row->tanggal?>" 
							data-tipe="ADMINISTRASI - RAWAT JALAN" 
							data-idtarif="<?=$row->idtarif?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->total?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->totalkeseluruhan?>"
						>
					</td>
					<td><?=$row->nopendaftaran?></td>
					<td><?=DMYFormat($row->tanggal)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->total)?></td>
					<td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
					<td><?=number_format($row->totalkeseluruhan)?></td>
				</tr>
				<?php $totalAdmRajal = $totalAdmRajal + $row->totalkeseluruhan;?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="5"><b>TOTAL</b></td>
					<td class="text-bold" colspan="2"><?=number_format($totalAdmRajal)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>

		<?php $administrasiRanap = get_all('trawatinap_administrasi', ['idpendaftaran' => $idpendaftaran]); ?>
		<?php if (count($administrasiRanap)) { ?>
		<hr>

		<b><span class="label label-success" style="font-size:12px">ADMINISTRASI</span></b>
		<b><span class="label label-default" style="font-size:12px">RAWAT INAP</span></b>
		<table class="table table-bordered table-striped" style="margin-top: 10px;">
			<thead>
				<tr>
					<th style="width:3%">Refund</th>
					<th style="width:10%">No. Pendaftaran</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:30%">Tindakan</th>
					<th style="width:10%">Tarif</th>
					<th style="width:10%">Diskon (%)</th>
					<th style="width:20%">Tarif Setelah Diskon</th>
				</tr>
			</thead>
			<tbody id="tempAdministrasiRanap">
				<?php $totalAdmRanap = 0;?>
				<?php foreach ($administrasiRanap as $row) { ?>
				<?php $ranap = get_by_field('id', $row->idpendaftaran, 'trawatinap_pendaftaran')?>
				<tr>
					<td>
						<input type="checkbox" class="checkboxRefund" 
							data-reference="trawatinap_administrasi" 
							data-iddetail="<?=$row->id?>" 
							data-tanggal="<?=$ranap->tanggaldaftar?>" 
							data-tipe="ADMINISTRASI - RAWAT INAP" 
							data-idtarif="<?=$row->idadministrasi?>" 
							data-tindakan="<?=$row->namatarif?>" 
							data-jasasarana="<?=$row->jasasarana?>"
							data-jasasarana_disc="<?=$row->jasasarana_disc?>"
							data-jasapelayanan="<?=$row->jasapelayanan?>"
							data-jasapelayanan_disc="<?=$row->jasapelayanan_disc?>"
							data-bhp="<?=$row->bhp?>"
							data-bhp_disc="<?=$row->bhp_disc?>"
							data-biayaperawatan="<?=$row->biayaperawatan?>"
							data-biayaperawatan_disc="<?=$row->biayaperawatan_disc?>"
							data-total="<?=$row->tarif?>"
							data-kuantitas="<?=$row->kuantitas?>"
							data-diskon="<?=$row->diskon?>"
							data-totalkeseluruhan="<?=$row->tarifsetelahdiskon?>"
						>
					</td>
					<td><?=$ranap->nopendaftaran?></td>
					<td><?=DMYFormat($ranap->tanggaldaftar)?></td>
					<td><?=$row->namatarif?></td>
					<td><?=number_format($row->tarif)?></td>
					<td><?=number_format($row->diskon / $row->tarif * 100, 2)?></td>
					<td><?=number_format($row->tarifsetelahdiskon)?></td>
				</tr>
				<?php $totalAdmRanap = $totalAdmRanap + $row->tarifsetelahdiskon;?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td align="center" colspan="5"><b>TOTAL</b></td>
					<td class="text-bold" colspan="2"><?=number_format($totalAdmRanap)?></td>
				</tr>
			</tfoot>
		</table>
		<?php } ?>
