<div class="modal in" id="modalEditRefund" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popout">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button" class="close">
                                <i class="fa fa-close" style="display:block;font-size:15px;"></i>
                            </button>
                        </li>
                    </ul>
                    <h3 class="block-title">Edit Pembayaran Refund</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal">
                        <input type="hidden" id="idTransaksi" name="" value="">
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="" style="text-align:left;">No Refund</label>
                            <div class="col-md-8">
                                <input id="norefund" class="form-control" type="text" value="" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="" style="text-align:left;">No Transaksi</label>
                            <div class="col-md-8">
                                <input id="notransaksi" class="form-control" type="text" value="" readonly>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="" style="text-align:left;">Metode Refund</label>
                            <div class="col-md-8">
                                <select id="metodeRefund" class="js-select2 form-control" style="width: 100%;">
                                    <option value="1" selected>Tunai</option>
                                    <option value="2">Debit</option>
                                    <option value="3">Kredit</option>
                                    <option value="4">Transfer</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group formNonTunai" hidden>
                            <label class="col-md-4 control-label" for="" style="text-align:left;">Nama Bank</label>
                            <div class="col-md-8">
                                <input id="bankRefund" class="form-control" type="text" value="" readonly>
                            </div>
                        </div>
                        <div class="form-group formNonTunai" hidden>
                            <label class="col-md-4 control-label" for="" style="text-align:left;">Nomor Rekening</label>
                            <div class="col-md-8">
                                <input id="norekeningRefund" class="form-control" type="text" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="" style="text-align:left;">Nominal</label>
                            <div class="col-md-8">
                                <input id="nominalRefund" class="form-control" type="text" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="" style="text-align:left;">Alasan</label>
                            <div class="col-md-8">
                                <textarea id="alasanRefund" class="form-control" rows="4" cols="80" required></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-sm btn-success" id="updatePembayaranRefund" type="submit"><i class="fa fa-check"></i> Simpan</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#metodeRefund').change(function() {
        if ($(this).val() == '1') {
            $('.formNonTunai').hide();
            $('#bankRefund').val('').attr('readonly', true);
            $('#norekeningRefund').val('').attr('readonly', true);
        } else {
            $('.formNonTunai').show();
            $('#bankRefund').removeAttr('readonly');
            $('#norekeningRefund').removeAttr('readonly');
        }
    });

    $('#updatePembayaranRefund').click(function() {
        $.ajax({
            url: "{base_url}Trefund/updatePembayaranRefund",
            type: 'POST',
            data: {
                idtransaksi: $('#idTransaksi').val(),
                metode: $('#metodeRefund').val(),
                bank: $('#bankRefund').val(),
                norekening: $('#norekeningRefund').val(),
                alasan: $('#alasanRefund').val()
            },
            dataType: 'json',
            success: function(data) {
                swal({
                    title: "Berhasil!",
                    text: "Proses penyimpanan data.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                window.location.assign('{base_url}Trefund');
            }
        });
    });
});

</script>
