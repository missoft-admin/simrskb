<?= ErrorSuccess($this->session) ?>
<?php if ($error != '') echo ErrorMessage($error) ?>
<?php if (UserAccesForm($user_acces_form,array('1090'))){?>
<div class="block">
    <div class="block-header">
        <?php if (UserAccesForm($user_acces_form,array('1092'))){?>
        <ul class="block-options">
            <li>
                <a href="{site_url}trefund/manage" class="btn"><i class="fa fa-plus"></i></a>
            </li>
        </ul>
        <?}?>
        <h3 class="block-title">{title}</h3>
        <hr style="margin-bottom: 0px;">
    </div>
    <div class="block-content">
        <form class="form-horizontal" action="{site_url}trefund/filter" method="post">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-3 control-label">No Medrec</label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" name="nomedrec" value="<?= $nomedrec ?>">
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-3 control-label">Nama Pasien</label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" name="namapasien" value="<?= $namapasien ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-3 control-label">Tipe Refund</label>
                        <div class="col-md-9">
                            <select name="tiperefund" class="js-select2 form-control" style="width: 100%;">
                                <option value="">Semua Tipe</option>
                                <option value="0" <?= ($tiperefund == '0' ? 'selected' : '') ?>>Deposit</option>
                                <option value="1" <?= ($tiperefund == '1' ? 'selected' : '') ?>>Pengembalian Obat</option>
                                <option value="2" <?= ($tiperefund == '2' ? 'selected' : '') ?>>Transaksi</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-3 control-label">Tanggal</label>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-6">
                                    <input class="form-control datepicker" type="text" name="tanggal_refund_awal" value="<?= ($tanggalawal != NULL ? $tanggalawal : date('d/m/Y')) ?>">
                                </div>
                                <div class="col-md-6">
                                    <input class="form-control datepicker" type="text" name="tanggal_refund_akhir" value="<?= ($tanggalakhir != NULL ? $tanggalakhir : date('d/m/Y')) ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if (UserAccesForm($user_acces_form,array('1091'))){?>
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-9">
                            <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                        </div>
                    </div>
                    <?}?>
                </div>
            </div>
        </form>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <table id="datatable-refund" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal & Waktu</th>
                            <th>No Refund</th>
                            <th>No Transaksi</th>
                            <th>Tipe</th>
                            <th>No Medrec</th>
                            <th>Nama Pasien</th>
                            <th>Nominal</th>
                            <th width="9%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?}?>
<div class="modal fade in" id="modal-alasan-refund" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popout">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button" class="close">
                                <i class="fa fa-close" style="display:block;font-size:15px;"></i>
                            </button>
                        </li>
                    </ul>
                    <h3 class="block-title">Hapus Transaksi</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal">
                        <div class="form-group" style="margin-bottom: 5px;">
                            <div class="col-md-12">
                                <label class="control-label-custom text-uppercase" for="">Alasan</label>
                                <textarea id="alasan" class="form-control" name="name" rows="7" cols="80"></textarea>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 5px;">
                            <div class="col-md-12">
                                <button class="btn btn-sm btn-success text-uppercase" id="delete-refund" type="button" style="margin:10px 0;" data-dismiss="modal">Simpan
                                </button>
                            </div>
                        </div>
                        <input id="idrefund" type="hidden" readonly>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('Trefund/modal/modal_edit_transaksi'); ?>

<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        todayHighlight: true,
    });

    $('#datatable-refund').DataTable({
        "oLanguage": {
            "sEmptyTable": "<i class='text-uppercase'>NO DATA AVAILABLE IN TABLE</i>",
        },
        "ajax": {
            "url": "{site_url}Trefund/showAllIndex",
            "type": "POST"
        },
        "autoWidth": true,
        "pageLength": 10,
        "lengthChange": true,
        "ordering": false,
        "processing": true,
        "order": []
    });

    $(document).on('click', '.show-refund', function() {
        var id = $(this).data('idtransaksi');

        $('#modal-alasan-refund').modal('show');
        $('#idrefund').val(id);
    });

    $(document).on('click', '#delete-refund', function() {
        var id = $('#idrefund').val();
        var alasan = $('#alasan').val();

        $.ajax({
            type: 'POST',
            url: "{site_url}Trefund/removeRefund/" + id,
            dataType: 'json',
            data: {
                alasan: alasan
            },
            success: function(data) {
                swal({
                    title: "Berhasil!",
                    text: "Proses penyimpanan data.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                location.reload();
            }
        });
    });

    $(document).on('click', '.edit-refund', function() {
        let id = $(this).data('idtransaksi');
        $('#idTransaksi').val(id);

        $('#modalEditRefund').modal('show');

        $.ajax({
            url: "{base_url}Trefund/getInfoRefund/" + id,
            dataType: 'json',
            success: function(data) {
                if (data.idmetode == '1') {
                    $('.formNonTunai').hide();
                    $('#bankRefund').attr('readonly', true);
                    $('#norekeningRefund').attr('readonly', true);
                } else {
                    $('.formNonTunai').show();
                    $('#bankRefund').val('').removeAttr('readonly');
                    $('#norekeningRefund').val('').removeAttr('readonly');
                }

                $('#norefund').val(data.norefund);
                $('#notransaksi').val(data.notransaksi);
                $('#nominalRefund').val($.number(data.totalrefund));
                $('#metodeRefund').select2('val', data.idmetode);
                $('#bankRefund').val(data.bank);
                $('#norekeningRefund').val(data.norekening);
                $('#alasanRefund').val(data.alasan);
            }
        });
    });
});

</script>
