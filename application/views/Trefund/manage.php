<?= ErrorSuccess($this->session) ?>
<?php if ($error != '') echo ErrorMessage($error) ?>

<style media="screen">
	.highlight-refund {
		background-color: #8bc34a2b !important;
	}
</style>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}trefund/index" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-bottom: 0px;">
	</div>
	<div class="block-content">
		<form class="form-horizontal">
			<div class="row">
				<div class="col-md-8">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label">Tanggal</label>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-6">
									<input type="text" id="tanggalRefund" class="form-control datepicker" value="<?= date('d/m/Y') ?>">
								</div>
								<div class="col-md-6">
									<input type="text" id="jamRefund" class="js-masked-time form-control js-masked-enabled" placeholder="00:00" value="<?= date('h:i') ?>">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label">Tipe Refund</label>
						<div class="col-md-9">
							<select id="tipeRefund" class="js-select2 form-control" style="width: 100%;" required>
								<?php if (UserAccesForm($user_acces_form,array('1093'))){?>
								<option value="0">Deposit</option>
								<?}?>
								<?php if (UserAccesForm($user_acces_form,array('1095'))){?>
								<option value="1">Pengembalian Obat</option>
								<?}?>
								<?php if (UserAccesForm($user_acces_form,array('1094'))){?>
								<option value="2">Transaksi</option>
								<?}?>
							</select>
						</div>
					</div>
					<input type="hidden" id="tipeTransaksi" value="">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label" for="cari-transaksi">Cari Transaksi</label>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-7">
									<select id="idTransaksi" class="js-select2 form-control" style="width: 100%;" data-placeholder="No. Transaksi">
									</select>
								</div>
								<div class="col-md-5">
									<button class="btn btn-primary" id="btnFind" type="button" style="width:100%;" data-toggle="modal" data-target="#modalCariTransaksi">Cari</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

		<hr>

		<div class="row" id="informasiTransaksi" hidden>
			<div class="col-md-12">
				<h5 class="text-uppercase">Informasi Transaksi</h5>
				<div class="row" style="margin-top: 15px;">
					<div class="col-md-6">
						<div class="input-group" style="width:100%">
							<span class="input-group-addon" style="width:150px; text-align:left;">No. Medrec</span>
							<input type="text" class="form-control" id="itNoMedrec" readonly="true">
						</div>
						<div class="input-group" style="margin-top:5px; width:100%">
							<span class="input-group-addon" style="width:150px; text-align:left;">Nama Pasien</span>
							<input type="text" class="form-control" id="itNamaPasien" readonly="true">
						</div>
						<div class="input-group" style="margin-top:5px; width:100%">
							<span class="input-group-addon" style="width:150px; text-align:left;">Alamat Pasien</span>
							<input type="text" class="form-control" id="itAlamatPasien" readonly="true">
						</div>
						<hr>
						<div class="input-group" style="margin-top:5px; width:100%">
							<span class="input-group-addon" style="width:150px; text-align:left;">Tanggal Transaksi</span>
							<input type="text" class="form-control" id="itTanggalTransaksi" readonly="true">
						</div>
						<div class="input-group" style="margin-top:5px; width:100%">
							<span class="input-group-addon" style="width:150px; text-align:left;">Tipe Transaksi</span>
							<input type="text" class="form-control" id="itTipeTransaksi" readonly="true">
						</div>
						<div class="input-group itRajalGroup" style="margin-top:5px; width:100%">
							<span class="input-group-addon" style="width:150px; text-align:left;">Nama Poliklinik</span>
							<input type="text" class="form-control" id="itNamaPoliklinik" readonly="true">
						</div>
					</div>
					<div class="col-md-6">
						<div class="input-group" style="margin-top:5px; width:100%">
							<span class="input-group-addon" style="width:150px; text-align:left;">Kelompok Pasien</span>
							<input type="text" class="form-control" id="itKelompokPasien" readonly="true">
						</div>
						<div class="input-group" style="margin-top:5px; width:100%">
							<span class="input-group-addon" style="width:150px; text-align:left;">Nama Perusahaan</span>
							<input type="text" class="form-control" id="itNamaPerusahaan" readonly="true">
						</div>
						<hr>
						<div class="input-group itRanapGroup" style="margin-top:5px; width:100%">
							<span class="input-group-addon" style="width:150px; text-align:left;">Kelas Perawatan</span>
							<input type="text" class="form-control" id="itNamaKelas" readonly="true">
						</div>
						<div class="input-group itRanapGroup" style="margin-top:5px; width:100%">
							<span class="input-group-addon" style="width:150px; text-align:left;">Bed</span>
							<input type="text" class="form-control" id="itNamaBed" readonly="true">
						</div>
						<div class="input-group" style="margin-top:5px; width:100%">
							<span class="input-group-addon" style="width:150px; text-align:left;">Dokter</span>
							<input type="text" class="form-control" id="itDokterPenanggungJawab" readonly="true">
						</div>
					</div>
				</div>
			</div>
		</div>

		<hr class="separator" hidden>

		<div class="row" id="detailTransaksi" hidden>
			<div class="col-md-12">
				<h5 class="text-uppercase" style="padding:10px 0;">Detail Transaksi</h5>
				<div id="detailRefundDeposit" hidden>
					<table id="dtRefundDeposit" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Tanggal</th>
								<th>Metode</th>
								<th>Nominal</th>
								<th>Terima Dari</th>
								<th>User Input</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>

				<div id="detailRefundReturObat" hidden>
					<table id="dtRefundReturObat" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No. Transaksi</th>
								<th>Tanggal</th>
								<th>Obat</th>
								<th>Tarif</th>
								<th>Kuantitas</th>
								<th>Jumlah</th>
								<th>Unit Pelayanan</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>

				<div id="detailRefundRawatJalan" hidden>
				</div>

				<div id="detailRefundRawatInap" hidden>
				</div>
			</div>
			<hr>
		</div>

		<div class="row" id="summaryTransaksi" hidden>
			<div class="col-md-5"></div>
			<div class="col-md-7">
				<div class="row" style="padding: 3px 0;" id="containerTotalTransaksi">
					<div class="col-md-6">
						<span class="text-uppercase" style="float:right;padding:10px 0;font-weight:bold;">Total Transaksi</span>
					</div>
					<div class="col-md-6">
						<div class="input-group">
							<div class="input-group-addon">RP.</div>
							<input id="totalTransaksi" class="form-control" type="text" name="" value="0" readonly="">
						</div>
					</div>
				</div>
				<div class="row" style="padding: 3px 0;" id="containerTotalPembayaran">
					<div class="col-md-6">
						<span class="text-uppercase" style="float:right;padding:10px 0;font-weight:bold;">Total Pembayaran</span>
					</div>
					<div class="col-md-6">
						<div class="input-group">
							<div class="input-group-addon">RP.</div>
							<input id="totalPembayaran" class="form-control" type="text" name="" value="0" readonly="">
						</div>
					</div>
				</div>
				<div class="row" style="padding: 3px 0;" id="containerTotalDeposit">
					<div class="col-md-6">
						<span class="text-uppercase" style="float:right;padding:10px 0;font-weight:bold;">Total Deposit</span>
					</div>
					<div class="col-md-6">
						<div class="input-group">
							<div class="input-group-addon">RP.</div>
							<input id="totalDeposit" class="form-control" type="text" name="" value="0" readonly="">
						</div>
					</div>
				</div>
				<div class="row" style="padding: 3px 0;" id="containerTotalRefund">
					<div class="col-md-6">
						<span class="text-uppercase" style="float:right;padding:10px 0;font-weight:bold;">Total Refund</span>
					</div>
					<div class="col-md-6">
						<div class="input-group">
							<div class="input-group-addon">RP.</div>
							<input id="totalRefund" class="form-control" type="text" name="" value="0" readonly="">
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row" id="prosesRefund" hidden>
			<hr>
			<div class="col-md-12">
				<h5 class="text-uppercase">Proses Refund</h5>
				<div class="row">
					<div class="col-md-6">
						<form class="form-horizontal" style="margin-top:20px;">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="">Metode Refund</label>
								<div class="col-md-9">
									<select id="metodeRefund" class="js-select2 form-control" style="width: 100%;" required>
										<option value="1" selected>Tunai</option>
										<option value="2">Debit</option>
										<option value="3">Kredit</option>
										<option value="4">Transfer</option>
									</select>
								</div>
							</div>
							<div class="form-group formNonTunai" style="margin-bottom: 5px;" hidden>
								<label class="col-md-3 control-label" for="">Nama Bank</label>
								<div class="col-md-9">
									<input id="bankRefund" class="form-control" type="text" value="" readonly>
								</div>
							</div>
							<div class="form-group formNonTunai" style="margin-bottom: 5px;" hidden>
								<label class="col-md-3 control-label" for="">Nomor Rekening</label>
								<div class="col-md-9">
									<input id="norekeningRefund" class="form-control" type="text" readonly>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="">Nominal</label>
								<div class="col-md-9">
									<input id="nominalRefund" class="form-control" type="text" readonly>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="">Alasan</label>
								<div class="col-md-9">
									<textarea id="alasanRefund" class="form-control" rows="4" cols="80" required></textarea>
								</div>
							</div>
						</form>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-12">
						<div style="float:right;padding:0 0 14px 0;">
							<?php if (UserAccesForm($user_acces_form,array('1096'))){?>
							<button id="saveRefund" class="btn btn-success" type="button" name="button" style="width:100px;font-size:12px;">Proses</button>
							<?}?>
							<a href="{base_url}trefund/index" class="btn btn-danger" type="button" name="button" style="width:100px;font-size:12px;">Batal</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/number_format.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		getRefundDeposit();

		$('#tipeRefund').change(function() {
			$('#idTransaksi option').remove();
			$('#idTransaksi').select2('destroy');

			$("#informasiTransaksi").hide();
			$("#detailTransaksi").hide();
			$("#summaryTransaksi").hide();
			$("#prosesRefund").hide();
			$(".separator").hide();

			$('#totalTransaksi').val(0);
			$('#totalPembayaran').val(0);
			$('#totalDeposit').val(0);
			$('#totalRefund').val(0);
			$('#nominalRefund').val(0);

			let tipeRefund = $(this).val();
			if (tipeRefund == '0') {
				getRefundDeposit();
			} else if (tipeRefund == '1') {
				getRefundObat();
			} else if (tipeRefund == '2') {
				getRefundTransaksi();
			}
		});

		$('#idTransaksi').change(function() {
			$(".separator").show();
			$('#tipeTransaksi').val('');

			let reference = $(this).find(":selected").data("reference");
			let tipeRefund = $('#tipeRefund').val();
			let idTransaksi = $(this).val();

			if (tipeRefund == '0') {
				getInfoRefundDeposit(idTransaksi);
			} else if (tipeRefund == '1') {
				getInfoRefundObat(idTransaksi);
			} else if (tipeRefund == '2') {
				getInfoRefundTransaksi(reference, idTransaksi);
			}
		});

		$('#metodeRefund').change(function() {
			if ($(this).val() == '1') {
				$('.formNonTunai').hide();
				$('#bankRefund').attr('readonly', true);
				$('#norekeningRefund').attr('readonly', true);
			} else {
				$('.formNonTunai').show();
				$('#bankRefund').val('').removeAttr('readonly');
				$('#norekeningRefund').val('').removeAttr('readonly');
			}
		});

		$(document).on("click", ".checkboxRefund",function() {
			if ($(this).is(':checked')) {
				$(this).closest('tr').addClass('highlight-refund');
			}else {
				$(this).closest('tr').removeClass('highlight-refund');
			}

			getTotalRefundTransaksi();
		});

		$('#saveRefund').click(function() {
			let detail;
			if ($('#tipeRefund').val() == '0') {
				let data = $('#dtRefundDeposit tbody tr').get().map(function(row) {
					return $(row).find('td:not(:eq(7))').get().map(function(cell) {
						return $(cell).html();
					});
				});

				detail = JSON.stringify(data);
			} else if ($('#tipeRefund').val() == '1') {
				let data = $('#dtRefundReturObat tbody tr').get().map(function(row) {
					return $(row).find('td:not(:eq(9))').get().map(function(cell) {
						return $(cell).html();
					});
				});

				detail = JSON.stringify(data);
			} else if ($('#tipeRefund').val() == '2') {
				let data = $(".checkboxRefund:checked").map(function(){
					return $(this).data();
				}).get();

				detail = JSON.stringify(data);
			}

			$.ajax({
				url: "{base_url}trefund/saveRefund/",
				type: 'POST',
				data: {
					tanggal: $('#tanggalRefund').val(),
					jam: $('#jamRefund').val(),
					tiperefund: $('#tipeRefund').val(),
					tipetransaksi: $('#tipeTransaksi').val(),
					idtransaksi: $('#idTransaksi').val(),
					notransaksi: $('#idTransaksi').find(":selected").data("notransaksi"),
					idpasien: $('#idTransaksi').find(":selected").data("idpasien"),
					idkelompokpasien: $('#idTransaksi').find(":selected").data("idkelompokpasien"),
					metode: $('#metodeRefund').val(),
					totaltransaksi: $('#totalTransaksi').val(),
					totalpembayaran: $('#totalPembayaran').val(),
					totaldeposit: $('#totalDeposit').val(),
					totalrefund: $('#totalRefund').val(),
					bank: $('#bankRefund').val(),
					norekening: $('#norekeningRefund').val(),
					nominal: $('#nominalRefund').val(),
					alasan: $('#alasanRefund').val(),
					detail: detail
				},
				dataType: 'json',
				success: function(data) {
					swal({
						title: "Berhasil!",
						text: "Proses penyimpanan data.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					window.location.assign('{base_url}trefund');
				}
			});
		});
	});

	// Transaksi Deposit
	function getRefundDeposit() {
		$.ajax({
			type: 'POST',
			url: "{base_url}Trefund/getRefundDeposit",
			dataType: 'json',
			success: function(data) {
				data.map((item) => {
					$('#idTransaksi').append(`<option data-notransaksi="${item.notransaksi}" data-idpasien="${item.idpasien}" data-idkelompokpasien="${item.idkelompokpasien}" value="${item.id}">${item.text}</option>`);
				});

				$('#idTransaksi').select2();
			}
		});
	}

	function getInfoRefundDeposit(idTransaksi) {
		$("#informasiTransaksi").show();

		$.ajax({
			type: 'POST',
			url: "{base_url}Trefund/getInfoRefundDeposit/" + idTransaksi,
			dataType: 'json',
			success: function(data) {
				$('.itRajalGroup').hide();
				$('.itRanapGroup').show();

				$('#itNoMedrec').val(data.nomedrec);
				$('#itNamaPasien').val(data.namapasien);
				$('#itAlamatPasien').val(data.alamatpasien);
				$('#itTanggalTransaksi').val(data.tanggaltransaksi);
				$('#itTipeTransaksi').val(data.tipetransaksi);
				$('#itKelompokPasien').val(data.namakelompokpasien);
				$('#itNamaPerusahaan').val(data.namaperusahaan);
				$('#itNamaKelas').val(data.namakelas);
				$('#itNamaBed').val(data.namabed);
				$('#itDokterPenanggungJawab').val(data.namadokter);

				$('#totalTransaksi').val($.number(data.totaltransaksi));
				$('#totalPembayaran').val($.number(data.totalpembayaran));
				$('#totalDeposit').val($.number(data.totaldeposit));
				$('#totalRefund').val($.number(data.totalrefund));
				$('#nominalRefund').val($.number(data.totalrefund));

				getDetailTransaksiDeposit(idTransaksi);
			}
		});
	}

	function getDetailTransaksiDeposit(idTransaksi) {
		$("#detailTransaksi").show();
		$("#detailRefundDeposit").show();
		$("#detailRefundReturObat").hide();
		$("#detailRefundRawatJalan").hide();
		$("#detailRefundRawatInap").hide();
		$("#summaryTransaksi").show();
		$("#prosesRefund").show();

		$("#containerTotalTransaksi").show();
		$("#containerTotalPembayaran").show();
		$("#containerTotalDeposit").show();
		$("#containerTotalRefund").show();

		$.ajax({
			type: 'POST',
			url: "{base_url}trawatinap_tindakan/getHistoryDeposit/" + idTransaksi,
			dataType: 'json',
			success: function(data) {
				$('#dtRefundDeposit tbody').empty();
				data.map(item => {

					let metode
					if(item.idmetodepembayaran == 1){
						metode = item.metodepembayaran;
					}else{
						metode = item.metodepembayaran + ' ( ' + item.namabank+ ' )';
					}

					$('#dtRefundDeposit tbody').append(`
						<tr>
							<td hidden>trawatinap_deposit</td>
							<td hidden>${item.id}</td>
							<td>${item.tanggal}</td>
							<td>${metode}</td>
							<td>${$.number(item.nominal)}</td>
							<td>${item.terimadari ? item.terimadari : "-"}</td>
							<td>${item.namapetugas}</td>
							<td>
								<a href="{base_url}trawatinap_tindakan/print_kwitansi_deposit/${item.id}" class="btn btn-xs btn-primary text-uppercase" style="font-size:10px;" target="_blank">
									<i class="fa fa-eye"></i>&nbsp;Preview Transaksi
								</a>
							</td>
						</tr>
					`);
				});
			}
		});
	}

	// Transaksi Retur Obat Farmasi Rawat Jalan
	function getRefundObat() {
		$.ajax({
			type: 'POST',
			url: "{base_url}Trefund/getRefundObat",
			dataType: 'json',
			success: function(data) {
				data.map((item) => {
					$('#idTransaksi').append(`<option data-notransaksi="${item.notransaksi}" data-idpasien="${item.idpasien}" data-idkelompokpasien="${item.idkelompokpasien}" value="${item.id}">${item.text}</option>`);
				});

				$('#idTransaksi').select2();
			}
		});
	}

	function getInfoRefundObat(idTransaksi) {
		$("#informasiTransaksi").show();

		$.ajax({
			type: 'POST',
			url: "{base_url}Trefund/getInfoRefundObat/" + idTransaksi,
			dataType: 'json',
			success: function(data) {
				$('.itRajalGroup').show();
				$('.itRanapGroup').hide();

				$('#itNoMedrec').val(data.nomedrec);
				$('#itNamaPasien').val(data.namapasien);
				$('#itAlamatPasien').val(data.alamatpasien);
				$('#itTanggalTransaksi').val(data.tanggaltransaksi);
				$('#itTipeTransaksi').val(data.tipetransaksi);
				$('#itNamaPoliklinik').val(data.namapoliklinik);
				$('#itKelompokPasien').val(data.namakelompokpasien);
				$('#itNamaPerusahaan').val(data.namaperusahaan);
				$('#itDokterPenanggungJawab').val(data.namadokter);

				$('#totalRefund').val($.number(data.totalretur));
				$('#nominalRefund').val($.number(data.totalretur));

				getDetailTransaksiReturObat(idTransaksi);
			}
		});
	}

	function getDetailTransaksiReturObat(idTransaksi) {
		$("#detailTransaksi").show();
		$("#detailRefundDeposit").hide();
		$("#detailRefundReturObat").show();
		$("#detailRefundRawatJalan").hide();
		$("#detailRefundRawatInap").hide();
		$("#summaryTransaksi").show();
		$("#prosesRefund").show();

		$("#containerTotalTransaksi").hide();
		$("#containerTotalPembayaran").hide();
		$("#containerTotalDeposit").hide();
		$("#containerTotalRefund").show();

		$.ajax({
			type: 'POST',
			url: "{base_url}trefund/getDetailRefundObat/" + idTransaksi,
			dataType: 'json',
			success: function(data) {
				$('#dtRefundReturObat tbody').empty();
				data.map(item => {
					$('#dtRefundReturObat tbody').append(`
						<tr>
							<td hidden>tpasien_pengembalian_detail</td>
							<td hidden>${item.id}</td>
							<td>${item.nopengembalian}</td>
							<td>${item.tanggal}</td>
							<td>${item.namatarif}</td>
							<td>${$.number(item.harga)}</td>
							<td>${$.number(item.kuantitas)}</td>
							<td>${$.number(item.totalharga)}</td>
							<td>${item.namaunitpelayanan}</td>
							<td>
								<div class="dropdown">
									<button class="btn btn-xs btn-primary text-uppercase dropdown-toggle" data-toggle="dropdown" style="font-size:10px;" target="_blank">
										<i class="fa fa-eye"></i>&nbsp;Preview Transaksi
									</button>
									<ul class="dropdown-menu">
										<li><a href="{base_url}tpasien_pengembalian/print_transaksi/${item.id}" target="_blank">Kwitansi Besar</a></li>
										<li><a href="{base_url}tpasien_pengembalian/print_transaksi_small/${item.id}" target="_blank">Kwitansi Kecil</a></li>
									</ul>
								</div>
							</td>
						</tr>
					`);
				});
			}
		});
	}

	// Transaksi Rawat Jalan / Rawat Inap
	function getRefundTransaksi() {
		$.ajax({
			type: 'POST',
			url: "{base_url}Trefund/getRefundTransaksi",
			dataType: 'json',
			success: function(data) {
				data.map((item) => {
					$('#idTransaksi').append(`<option data-reference="${item.reference}" data-notransaksi="${item.notransaksi}" data-idpasien="${item.idpasien}" data-idkelompokpasien="${item.idkelompokpasien}" value="${item.id}">${item.text}</option>`);
				});

				$('#idTransaksi').select2();
			}
		});
	}

	function getInfoRefundTransaksi(reference, idTransaksi) {
		$("#informasiTransaksi").show();
		$("#detailRefundRawatJalan").html('<div style="text-align: center"><img src="{base_url}assets/img/spinner1.gif" style="width: 200px; margin-left: auto; margin-right: auto; display: block;"> Loading</div>');
		$("#detailRefundRawatInap").html('<div style="text-align: center"><img src="{base_url}assets/img/spinner1.gif" style="width: 200px; margin-left: auto; margin-right: auto; display: block;"> Loading</div>');

		$.ajax({
			type: 'POST',
			url: "{base_url}Trefund/getInfoRefundTransaksi/" + reference + '/' + idTransaksi,
			dataType: 'json',
			success: function(data) {
				if (data.reference == 'rawatjalan') {
					$('.itRajalGroup').show();
					$('.itRanapGroup').hide();
					$('#tipeTransaksi').val('rawatjalan');

					getDetailRefundTransaksiRawatJalan(idTransaksi);
				} else {
					$('.itRajalGroup').hide();
					$('.itRanapGroup').show();
					$('#tipeTransaksi').val('rawatinap');

					getDetailRefundTransaksiRawatInap(idTransaksi);
				}

				$('#itNoMedrec').val(data.nomedrec);
				$('#itNamaPasien').val(data.namapasien);
				$('#itAlamatPasien').val(data.alamatpasien);
				$('#itTanggalTransaksi').val(data.tanggaltransaksi);
				$('#itTipeTransaksi').val(data.tipetransaksi);
				$('#itNamaPoliklinik').val(data.namapoliklinik);
				$('#itKelompokPasien').val(data.namakelompokpasien);
				$('#itNamaPerusahaan').val(data.namaperusahaan);
				$('#itDokterPenanggungJawab').val(data.namadokter);

				$('#itNamaKelas').val(data.namakelas);
				$('#itNamaBed').val(data.namabed);
			}
		});
	}

	function getDetailRefundTransaksiRawatJalan(idTransaksi) {
		$("#detailTransaksi").show();
		$("#detailRefundDeposit").hide();
		$("#detailRefundReturObat").hide();
		$("#detailRefundRawatJalan").show();
		$("#detailRefundRawatInap").hide();
		$("#summaryTransaksi").show();
		$("#prosesRefund").show();

		$("#containerTotalTransaksi").hide();
		$("#containerTotalPembayaran").hide();
		$("#containerTotalDeposit").hide();
		$("#containerTotalRefund").show();

		$.ajax({
			type: 'POST',
			url: "{base_url}trefund/getDetailRefundTransaksiRawatJalan/" + idTransaksi,
			success: function(data) {
				$("#detailRefundRawatJalan").html(data);
			}
		});
	}

	function getDetailRefundTransaksiRawatInap(idTransaksi) {
		$("#detailTransaksi").show();
		$("#detailRefundDeposit").hide();
		$("#detailRefundReturObat").hide();
		$("#detailRefundRawatJalan").hide();
		$("#detailRefundRawatInap").show();
		$("#summaryTransaksi").show();
		$("#prosesRefund").show();

		$("#containerTotalTransaksi").hide();
		$("#containerTotalPembayaran").hide();
		$("#containerTotalDeposit").hide();
		$("#containerTotalRefund").show();

		$.ajax({
			type: 'POST',
			url: "{base_url}trefund/getDetailRefundTransaksiRawatInap/" + idTransaksi,
			success: function(data) {
				$("#detailRefundRawatInap").html(data);
			}
		});
	}

	function getTotalRefundTransaksi() {
		let totalRefund = 0;
		$('.checkboxRefund').each(function () {
			totalRefund += parseFloat(this.checked ? $(this).data('totalkeseluruhan') : 0);
		});

		$('#totalRefund').val($.number(totalRefund));
		$('#nominalRefund').val($.number(totalRefund));
	}
</script>

<!-- Modal Included -->
<? $this->load->view('Trefund/modal/modal_cari_transaksi') ?>
