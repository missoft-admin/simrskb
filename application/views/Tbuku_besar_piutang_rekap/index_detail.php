<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">		
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:10px">
		<div class="row">
            <?php echo form_open('Tbuku_besar_piutang_rekap/export','class="form-horizontal" id="form-work" target="_blank"') ?>
           
			<div class="col-md-10">				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="tanggal">Asuransi</label>
                    <div class="col-md-10">
						<select name="idkelompok[]" id="idkelompok" class="js-select2 form-control" style="width: 100%;" multiple>
							<?php foreach  ($list_KP as $row) { ?>
							<option value="'<?=$row->id?>'"  <?=(in_array($row->id,$arr_kelompok)?'selected':'')?>><?=$row->nama?></option>
							<?php } ?>						
						</select>					
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-2 control-label" for="tanggal">Periode</label>
                    <div class="col-md-4">
						<input class="form-control" type="hidden" id="periode" name="periode" placeholder="From" value="{periode}"/>				
						<input class="form-control" readonly type="text" id="periode" name="periode" placeholder="From" value="{periode_nama}"/>				
					</div>
                </div>
				
				
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for=""></label>
                    <div class="col-md-10">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter"><i class="fa fa-filter"></i> Filter</button>
						
					</div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="index_list">
			<thead>
				<tr>
					<th>Tanggal</th>
					<th>Kode</th>
					<th>Distributor</th>
					<th>Tipe</th>
					<th>Keterangan</th>
					<th>Saldo Awal</th>
					<th>Debit</th>
					<th>Kredit</th>
					<th>Saldo Akhir</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".decimal").number(true,2,'.',',');
	load_rekap();
})

$(document).on("click","#btn_filter",function(){	
	load_rekap();		
});
function load_rekap() {
	// alert('sini');
	var idkelompok=$("#idkelompok").val();
	var periode=$("#periode").val();
	
	$('#index_list').DataTable().destroy();
	var table = $('#index_list').DataTable({
	"pageLength": 100,
	"ordering": false,
	"processing": true,
	"serverSide": true,
	"autoWidth": false,
	"fixedHeader": true,
	"searching": false,
	"order": [],
	"ajax": {
		url: '{site_url}Tbuku_besar_piutang_rekap/getDetail/',
		type: "POST",
		dataType: 'json',
		data: {
			idkelompok: idkelompok,
			periode: periode,
		}
	},
	columnDefs: [
				 {"targets": [9], "visible": false },
				 {  className: "text-right", targets:[5,6,7,8] },
				 {  className: "text-center", targets:[0,1,3,4] },
				 { "width": "5%", "targets": [1] },
				 { "width": "8%", "targets": [0,3,9] },
				 { "width": "15%", "targets": [2] },
				 { "width": "10%", "targets": [3,4,5,6,7,8] },
				 // { "width": "15%", "targets": [2,13] }

				]
	});
}

</script>
