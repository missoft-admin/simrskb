<!-- Modal Riwayat Cetak Hasil Pemeriksaan -->
<div class="modal fade" id="modal-riwayat-cetak-hasil-pemeriksaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Riwayat Cetak Hasil Pemeriksaan</h3>
				</div>
                <div class="block-content">
                    <table class="table table-striped table-striped table-hover table-bordered" id="table-history-cetak">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Urutan</th>
                                <th class="text-center">Dicetak Oleh</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>