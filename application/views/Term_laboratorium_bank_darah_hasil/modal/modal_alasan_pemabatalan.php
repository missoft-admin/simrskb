<!-- Modal Alasan Pembatalan -->
<div class="modal fade" id="modalAlasanPembatalan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
                <div class="block-content">
                    <h5 class="text-center">Apakah anda yakin akan merubah hasil pemeriksaan ini. Jika anda membatalkan maka hasil cetak akan berubah</h5>
                    <br><br>
                    <div class="form-group">
                        <label for="alasan-pembatalan">Alasan Pembatalan</label>
                        <input type="text" class="form-control" id="alasan-pembatalan" placeholder="Isikan Alasan Pembatalan (Required)" required value="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal" id="btn-update-alasan-pembatalan">Ya</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#btn-update-alasan-pembatalan').click(function () {
            let transaksiId = '{id}';
            let hasilPemeriksaanId = '{hasil_pemeriksaan_id}';
            let alasanPembatalan = $('#alasan-pembatalan').val();

            batalValidasiTransaksi(transaksiId, hasilPemeriksaanId, alasanPembatalan);
        });
    });

    function batalValidasiTransaksi(transaksiId, hasilPemeriksaanId, alasanPembatalan) {
        $.ajax({
            url: '{site_url}term_laboratorium_bank_darah_hasil/batal_validasi_transaksi/' + transaksiId,
            method: 'POST',
            data: {
                hasil_pemeriksaan_id: hasilPemeriksaanId,
                alasan: alasanPembatalan
            },
            success: function (result) {
                window.location = '{base_url}term_laboratorium_bank_darah_hasil/proses/' + transaksiId;
            },
            error: function (error) {
                console.error('Error deleting order:', error);
                alert('Error deleting order. Please try again.');
            }
        });
    }
</script>