<!-- Modal Rujukan Keluar -->
<div class="modal fade" id="modal-rujukan-keluar" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Rujukan Keluar</h3>
				</div>
                <div class="block-content">
                    <div class="form-group">
                        <select id="rujukan_keluar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <?php foreach (get_all('merm_referensi_rujukan_keluar', array('status' => 1)) as $row) { ?>
                            <option value="<?=$row->id?>"><?=$row->nama_lengkap?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal" id="btn-update-rujukan-keluar">Ya</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#modal-rujukan-keluar').on('show.bs.modal', function (event) {
            targetButton = $(event.relatedTarget);
            var dataId = targetButton.data('id');

            $('#rujukan_keluar').val(dataId).trigger('change');
        });

        $('#btn-update-rujukan-keluar').click(function () {
            var selectedValue = $('#rujukan_keluar option:selected').val();
            targetButton.data('id', selectedValue);

            $('#modal-rujukan-keluar').modal('hide');
        });
    });
</script>