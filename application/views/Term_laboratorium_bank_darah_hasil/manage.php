<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>
<?php $user_id = $this->session->userdata('user_id'); ?>

<div class="block">
    <div class="block-content">
        <?php echo form_open('term_laboratorium_bank_darah_hasil/save', 'id="form-work"') ?>
        
        <div class="row">
            <div class="col-md-3">
                <?php if ($status_form == '') { ?>
                    <h4>INPUT HASIL PEMERIKSAAN</h4> <br>
                <?php } else if ($status_form == 'edit_hasil_pemeriksaan') { ?>
                    <h4>EDIT HASIL PEMERIKSAAN</h4> <br>
                <?php } else if ($status_form == 'lihat_hasil_pemeriksaan') { ?>
                    <h4>LIHAT HASIL PEMERIKSAAN</h4> <br>
                <?php } ?>
            </div>
            <div class="col-md-9 text-right">
                <?php if ($status_form == '') { ?>
                    <button type="submit" class="btn btn-warning" name="form_submit" value="form-submit"><i class="fa fa-save"></i> Simpan</button>
                    <button type="submit" class="btn btn-success" name="form_submit" value="form-submit-and-validation"><i class="fa fa-check"></i> Simpan & Validasi</button>
                    <button type="button" class="btn btn-danger" onclick="batalTransaksi('{id}')"><i class="fa fa-times"></i> Batalkan Permintaan</button>
                    <?php if ($hasil_pemeriksaan_id) { ?>
                        <div class="btn-group">
                            <button class="btn btn-primary btn-block dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-print"></i> Cetak Hasil
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li class=""><a href="#" onclick="cetakHasilPemeriksaan('{hasil_pemeriksaan_id}', 'FORMAT_1')">Format 1</a></li>
                                <li class=""><a href="#" onclick="cetakHasilPemeriksaan('{hasil_pemeriksaan_id}', 'FORMAT_2')">Format 2</a></li>
                            </ul>
                        </div>
                    <? } ?>
                    <a href="{site_url}term_laboratorium_bank_darah_hasil" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
                <? } else { ?>
                    <?php if ($status_form == 'edit_hasil_pemeriksaan') { ?>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalAlasanPembatalan"><i class="fa fa-pencil"></i> Ubah Hasil</button>
                    <? } ?>
                    <?php if ($hasil_pemeriksaan_id) { ?>
                        <div class="btn-group">
                            <button class="btn btn-primary btn-block dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-print"></i> Cetak Hasil
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li class=""><a href="#" onclick="cetakHasilPemeriksaan('{hasil_pemeriksaan_id}', 'FORMAT_1')">Format 1</a></li>
                                <li class=""><a href="#" onclick="cetakHasilPemeriksaan('{hasil_pemeriksaan_id}', 'FORMAT_2')">Format 2</a></li>
                            </ul>
                        </div>
                    <? } ?>
                    <a href="{site_url}term_laboratorium_bank_darah_hasil" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
                <? } ?>
            </div>
        </div>

        <hr>
        
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nomorRekamMedis">Nomor Rekam Medis & Nama Pasien</label>
                            <input type="text" class="form-control" placeholder="Nomor Rekam Medis" disabled value="{nomor_medrec} {nama_pasien}">
                        </div>

                        <div class="form-group">
                            <label for="jenisKelamin">Jenis Kelamin</label>
                            <input type="text" class="form-control" placeholder="Jenis Kelamin" disabled value="{jenis_kelamin}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nik">Nomor Induk Kependudukan</label>
                            <input type="text" class="form-control" placeholder="NIK" disabled value="{nomor_ktp}">
                        </div>

                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="text" class="form-control" placeholder="E-mail" disabled value="{email}">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <input type="text" class="form-control" placeholder="Alamat" disabled value="{alamat_pasien}">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="tglLahir">Tanggal Lahir</label>
                                    <input type="date" class="form-control" placeholder="Tanggal Lahir" disabled value="{tanggal_lahir}">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="umur">Umur</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Tahun" disabled value="{umur_tahun}">
                                                <div class="input-group-addon">
                                                    <span class="input-group-text">Tahun</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="umur">&nbsp;</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Bulan" disabled value="{umur_bulan}">
                                                <div class="input-group-addon">
                                                    <span class="input-group-text">Bulan</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="umur">&nbsp;</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Hari" disabled value="{umur_hari}">
                                                <div class="input-group-addon">
                                                    <span class="input-group-text">Hari</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kelompokPasien">Kelompok Pasien</label>
                            <input type="text" class="form-control" placeholder="Kelompok Pasien" disabled value="{kelompok_pasien}">
                        </div>

                        <div class="form-group">
                            <label for="namaAsuransi">Nama Asuransi</label>
                            <input type="text" class="form-control" placeholder="Nama Asuransi" disabled value="{nama_asuransi}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tipeKunjungan">Tipe Kunjungan</label>
                            <input type="text" class="form-control" placeholder="Tipe Kunjungan" disabled value="{tipe_kunjungan}">
                        </div>

                        <div class="form-group">
                            <label for="namaPoliklinik">Nama Poliklinik</label>
                            <input type="text" class="form-control" placeholder="Nama Poliklinik" disabled value="{nama_poliklinik}">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nomorPendaftaran">Nomor Pendaftaran</label>
                            <input type="text" class="form-control" placeholder="Nomor Pendaftaran" disabled value="{nomor_pendaftaran}">
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nomorPermintaan">Nomor Permintaan</label>
                            <input type="text" class="form-control" placeholder="Nomor Permintaan" disabled value="{nomor_permintaan}">
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nomorTransaksiLaboratorium">Nomor Transaksi Laboratorium</label>
                            <input type="text" class="form-control" placeholder="Nomor Transaksi Laboratorium" disabled value="{nomor_transaksi_laboratorium}">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="waktuPembuatan">Waktu Pembuatan</label>
                            <input type="text" class="form-control" placeholder="Waktu Pembuatan" disabled value="{waktu_pembuatan}">
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="waktuPermintaan">Waktu Permintaan</label>
                            <input type="text" class="form-control" placeholder="Nomor Permintaan" disabled value="{waktu_permintaan}">
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="rencanaPemeriksaan">Rencana Pemeriksaan</label>
                            <input type="text" class="form-control" placeholder="Rencana Pemeriksaan" disabled value="{rencana_pemeriksaan}">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="diagnosa">Diagnosa</label>
                            <input type="text" class="form-control" placeholder="Diagnosa" disabled value="{diagnosa}">
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="catatanPermintaan">Catatan Permintaan</label>
                            <input type="text" class="form-control" placeholder="Catatan Permintaan" disabled value="{catatan_permintaan}">
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nomorLaboratorium">Nomor Laboratorium</label>
                            <input type="text" class="form-control" id="nomorLaboratorium" name="nomor_laboratorium" <?= ($pengaturan_form['edit_nomor_laboratorium'] ? '' : 'disabled'); ?> placeholder="Nomor Laboratorium" value="{nomor_laboratorium}">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-xs-12" for="">Prioritas Pemeriksaan</label>
                            <div class="col-xs-12">
                                <select id="prioritasPemeriksaan" name="prioritas_pemeriksaan" class="js-select2 form-control" style="width: 100%;" <?= ($pengaturan_form['edit_prioritas_pemeriksaan'] ? '' : 'disabled'); ?> data-placeholder="Pilih Opsi">
                                    <?php foreach (list_variable_ref(85) as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?php echo '' == $prioritas_pemeriksaan && '1' == $row->st_default ? 'selected' : ''; ?> <?php echo $row->id == $prioritas_pemeriksaan ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-xs-12" for="">Pasien Puasa</label>
                            <div class="col-xs-12">
                                <select id="pasienPuasa" name="pasien_puasa" class="js-select2 form-control" style="width: 100%;" <?= ($pengaturan_form['edit_pasien_puasa'] ? '' : 'disabled'); ?> data-placeholder="Pilih Opsi">
                                    <?php foreach (list_variable_ref(87) as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?php echo '' == $pasien_puasa && '1' == $row->st_default ? 'selected' : ''; ?> <?php echo $row->id == $pasien_puasa ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-xs-12" for="">Pengiriman Hasil</label>
                            <div class="col-xs-12">
                                <select id="pengirimanHasil" name="pengiriman_hasil" class="js-select2 form-control" style="width: 100%;" <?= ($pengaturan_form['edit_pengiriman_hasil'] ? '' : 'disabled'); ?> data-placeholder="Pilih Opsi">
                                    <?php foreach (list_variable_ref(88) as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?php echo '' == $pengiriman_hasil && '1' == $row->st_default ? 'selected' : ''; ?> <?php echo $row->id == $pengiriman_hasil ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-xs-12" for="">Waktu Pengambilan Sample</label>
                        <div class="col-xs-12">
                            <div class="row">
                            <div class="col-md-6">
                                <div class="input-group date">
                                <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_pengambilan_sample" id="tanggalPengambilanSample" <?= ($pengaturan_form['edit_waktu_pengambilan'] ? '' : 'disabled'); ?> placeholder="HH/BB/TTTT" value="{tanggal_pengambilan_sample}">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                <input type="text" class="time-datepicker form-control" name="waktu_pengambilan_sample" id="waktuPengambilanSample" <?= ($pengaturan_form['edit_waktu_pengambilan'] ? '' : 'disabled'); ?> value="{waktu_pengambilan_sample}">
                                <span class="input-group-addon"><i class="si si-clock"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-xs-12" for="">Petugas Pengambilan Sample</label>
                        <div class="col-xs-12">
                            <select name="petugas_pengambilan_sample" class="js-select2 form-control" id="petugasPengambilSample" style="width: 100%;" <?= ($pengaturan_form['edit_petugas_pengambilan'] ? '' : 'disabled'); ?> data-placeholder="Pilih Opsi">
                            </select>
                        </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-xs-12" for="">Waktu Pengujian dan Pengolahan</label>
                        <div class="col-xs-12">
                            <div class="row">
                            <div class="col-md-6">
                                <div class="input-group date">
                                <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_pengujian_sample" id="tanggalPengujianSample" placeholder="HH/BB/TTTT" value="{tanggal_pengujian_sample}">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                <input type="text" class="time-datepicker form-control" name="waktu_pengujian_sample" id="waktuPengujianSample" value="{waktu_pengujian_sample}">
                                <span class="input-group-addon"><i class="si si-clock"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-xs-12" for="">Petugas Pengujian dan Pengolahan</label>
                        <div class="col-xs-12">
                            <select name="petugas_pengujian_sample" class="js-select2 form-control" id="petugasPengujian" style="width: 100%;" data-placeholder="Pilih Opsi">
                            </select>
                        </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-xs-12" for="">Dokter Penanggung Jawab</label>
                            <div class="col-xs-12">
                                <select class="js-select2 form-control" disabled style="width: 100%;">
                                    <?php foreach (get_all('mdokter', array('status' => 1)) as $row) { ?>
                                    <option value="<?=$row->id?>" <?= $row->id == $dokter_penanggung_jawab ? 'selected' : ''; ?>><?=$row->nama?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-xs-12" for="">Dokter Laboratorium</label>
                        <div class="col-xs-12">
                            <select name="dokter_laboratorium" class="js-select2 form-control" id="dokterLaboratorium" style="width: 100%;" data-placeholder="Pilih Opsi">
                            </select>
                        </div>
                        </div>
                    </div>
                        
                </div>
            </div>
        </div>

        <input type="hidden" id="data-pemeriksaan" name="data_pemeriksaan">
        <input type="hidden" id="catatan" name="catatan">
        <input type="hidden" id="interpretasi-hasil-pemeriksaan" name="interpretasi_hasil_pemeriksaan">

        <?php if ($pengaturan_form['edit_nomor_laboratorium'] == 0) { ?>
            <input type="hidden" name="nomor_laboratorium" value="{nomor_laboratorium}">
        <? } ?>

        <?php if ($pengaturan_form['edit_prioritas_pemeriksaan'] == 0) { ?>
            <input type="hidden" name="prioritas_pemeriksaan" value="{prioritas_pemeriksaan}">
        <? } ?>

        <?php if ($pengaturan_form['edit_pasien_puasa'] == 0) { ?>
            <input type="hidden" name="pasien_puasa" value="{pasien_puasa}">
        <? } ?>

        <?php if ($pengaturan_form['edit_pengiriman_hasil'] == 0) { ?>
            <input type="hidden" name="pengiriman_hasil" value="{pengiriman_hasil}">
        <? } ?>

        <?php if ($pengaturan_form['edit_waktu_pengambilan'] == 0) { ?>
            <input type="hidden" name="tanggal_pengambilan_sample" value="{tanggal_pengambilan_sample}">
            <input type="hidden" name="waktu_pengambilan_sample" value="{waktu_pengambilan_sample}">
        <? } ?>

        <?php if ($pengaturan_form['edit_petugas_pengambilan'] == 0) { ?>
            <input type="hidden" name="petugas_pengambilan_sample" value="{petugas_pengambilan_sample}">
        <? } ?>
        
        <?php echo form_hidden('id', $id); ?>
        <?php echo form_close() ?>

        <div class="row">
            <div class="col-md-12">
                <?php foreach ($daftar_pemeriksaan as $index => $row) : ?>
                    <?php if ($row->idkelompok != 1) :
                        $tarif_laboratorium = $this->model->get_tarif_laboratorium($row->idlaboratorium);
                        $satuan = $tarif_laboratorium->singkatan_satuan ?? '0';
                        $nilai_normal = $tarif_laboratorium->nilai_normal ?? '';
                        $nilai_kritis = $tarif_laboratorium->nilai_kritis ?? '';
                        $metode = $tarif_laboratorium->metode ?? '';
                        $sumber_spesimen = $tarif_laboratorium->sumber_spesimen ?? '';
                    ?>
                    <table class="table table-pemeriksaan" style="border: 1px solid #e6e6e6;">
                        <thead>
                            <tr>
                                <th width="30%">Nama Pemeriksaan</th>
                                <th width="20%">Petugas Profesi</th>
                                <th width="50%">Flag</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr class="tbody-data" style="background-color: #f5f5f5;">
                            <td><?= $row->nama ?></td>
                            <td>
                                <select class="js-select2 form-control pemeriksaan-petugas-proses" style="width: 100%;">
                                    <?php foreach ($petugas_proses as $item) : ?>
                                        <option value="<?= $item->id ?>" <?= ($row->petugas_proses ? ($item->id == $row->petugas_proses) : ($item->id == $this->session->userdata('login_ppa_id'))) ? 'selected' : ''; ?>><?= $item->nama ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                            <td>
                                <div style="display: flex; width: 100%;">
                                    <select class="js-select2 form-control pemeriksaan-flag" style="width: 20%; <?= ($pengaturan_form['action_flag'] ? '' : 'display: none;'); ?>">
                                        <?php foreach (list_variable_ref(118) as $item) : ?>
                                            <option value="<?= $item->id; ?>" <?= '' == $row->flag && '1' == $item->st_default ? 'selected' : ''; ?> <?= $item->id == $row->flag ? 'selected' : ''; ?>><?= $item->nama; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    &nbsp;&nbsp;
                                    <button type="button" class="btn <?= ($row->flag_normal == 1 ? 'btn-primary' : 'btn-warning'); ?> btn-toggle-normal pemeriksaan-flag-normal" style="width: 15%; <?= ($pengaturan_form['action_tidak_normal'] ? '' : 'display: none;'); ?>" data-status="<?= $row->flag_normal ?>">
                                        <i class="fa fa-certificate"></i>
                                    </button>
                                    &nbsp;&nbsp;
                                    <button type="button" class="btn <?= ($row->flag_kritis == 1 ? 'btn-primary' : 'btn-danger'); ?> btn-toggle-kritis pemeriksaan-flag-kritis" style="width: 15%; <?= ($pengaturan_form['action_nilai_kritis'] ? '' : 'display: none;'); ?>" data-status="<?= $row->flag_kritis ?>">
                                        <i class="fa fa-exclamation-circle"></i>
                                    </button>
                                    &nbsp;&nbsp;
                                    <button type="button" class="btn btn-primary pemeriksaan-rujukan-keluar" data-id="<?= $row->rujukan_keluar ?>" data-toggle="modal" data-target="#modal-rujukan-keluar" <?= ($pengaturan_form['action_periksa_keluar'] ? '' : 'style="display: none;"'); ?>>
                                        <i class="fa fa-arrow-circle-o-right pemeriksaan-id" data-id="<?= $row->id ?>"></i> Periksa Keluar
                                    </button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6"><b>HASIL PEMERIKSAAN</b></td>
                        </tr>
                        <tr class="tbody-data">
                            <td colspan="6">
                                <textarea type="text" class="form-control pemeriksaan-hasil" style="width: 100%"><?= $row->hasil ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6"><b>UPLOAD</b></td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <form class="dropzone" id="dropzone-<?= $row->id ?>" action="{base_url}term_laboratorium_bank_darah_hasil/uploadFiles" method="post" enctype="multipart/form-data">
                                    <input name="pemeriksaan_id" type="hidden" value="<?= $row->id ?>">
                                </form>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-responsive" id="list-file-<?= $row->id ?>">
                                        <thead>
                                            <tr>
                                                <th width="50%">File</th>
                                                <th width="20%">Size</th>
                                                <th width="20%">User</th>
                                                <th width="10%">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            initDropzone("#dropzone-<?php echo $row->id; ?>", "<?php echo $row->id; ?>");
                            refreshFile('#list-file-<?= $row->id; ?> tbody', <?= $row->id; ?>);
                        });
                    </script>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="row">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-12" for="">Catatan</label>
                        <div class="col-md-12">
                            <textarea class="form-control summernote summernote-catatan" placeholder="Catatan" rows="10">{catatan}</textarea>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-12" for="">Interpretasi Hasil Pemeriksaan</label>
                        <div class="col-md-12">
                            <textarea class="form-control summernote summernote-interpretasi" placeholder="Interpretasi Hasil Pemeriksaan" rows="10">{interpretasi_hasil_pemeriksaan}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('Term_laboratorium_bank_darah_hasil/modal/modal_ubah_petugas_proses'); ?>
<?php $this->load->view('Term_laboratorium_bank_darah_hasil/modal/modal_rujukan_keluar'); ?>
<?php $this->load->view('Term_laboratorium_bank_darah_hasil/modal/modal_alasan_pemabatalan'); ?>
<?php $this->load->view('Term_laboratorium_bank_darah_hasil/services'); ?>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
    var targetButton;

    $(document).ready(function() { 
        loadDataPetugasPengambilSample('{tujuan_laboratorium}', '{petugas_pengambilan_sample}');
        loadDataPetugasPengujian('{tujuan_laboratorium}', '{petugas_pengujian_sample}');
        loadDataDokterLaboratorium('{tujuan_laboratorium}', '{dokter_laboratorium}');

        <?php if ($status_form == 'lihat_hasil_pemeriksaan' || $status_form == 'edit_hasil_pemeriksaan') { ?>
            $('input, select, textarea').not('#alasan-pembatalan').prop('disabled', true);

            var summernoteElements = $('.summernote');
            summernoteElements.each(function() {
                $(this).summernote('disable');
                $(this).siblings('.note-editor').find('.note-toolbar').hide();
            });
        <? } else { ?>
            $('.pemeriksaan-hasil, .summernote-catatan, .summernote-interpretasi').summernote({
                height: 150,
                minHeight: null,
                maxHeight: null
            });
    
            <?php if ($pengaturan_form['action_flag'] == '0') { ?>
                setTimeout(function() {
                    $('.pemeriksaan-flag').select2().next('.select2-container').hide();
                }, 100);
            <?php } ?>
        <? } ?>

        $('.btn-toggle-normal').on('click', function() {
            var $button = $(this);
            var toggleType = $button.data('status');
            
            if (toggleType == 1) {
                $button.removeClass('btn-primary').addClass('btn-warning');
                $button.data('status', 2);
            } else if (toggleType == 2) {
                $button.removeClass('btn-warning').addClass('btn-primary');
                $button.data('status', 1);
            }
        });

        $('.btn-toggle-kritis').on('click', function() {
            var $button = $(this);
            var toggleType = $button.data('status');

            if (toggleType == 1) {
                swal({
                    text: "Apakah anda yakin akan merubah hasil pemeriksaan ini menjadi kritis ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Ya",
                    confirmButtonColor: "#34a263",
                    cancelButtonText: "Batalkan",
                }).then(function() {
                    $button.removeClass('btn-primary').addClass('btn-danger');
                    $button.data('status', 2);
                });
            } else if (toggleType == 2) {
                $button.removeClass('btn-danger').addClass('btn-primary');
                $button.data('status', 1);
            }
        });

        $('button[name="form_submit"]').on('click', function (event) {
            $("#cover-spin").show();

            var disabledInputs = document.querySelectorAll('[disabled]');
            disabledInputs.forEach(function(input) {
                input.removeAttribute('disabled');
            });
            
            $("#catatan").val($(".summernote-catatan").summernote('code'));
            $("#interpretasi-hasil-pemeriksaan").val($(".summernote-interpretasi").summernote('code'));
            $("#data-pemeriksaan").val(JSON.stringify(gatherAllDataFromTable()));
        });
    });

    function gatherDataFromRow(row) {
        var data = {
            'id': row.find('.pemeriksaan-id').data('id'),
            'rujukan_keluar': row.find('.pemeriksaan-rujukan-keluar').data('id'),
            'flag': row.find('.pemeriksaan-flag option:selected').val(),
            'flag_normal': row.find('.pemeriksaan-flag-normal').data('status'),
            'flag_kritis': row.find('.pemeriksaan-flag-kritis').data('status'),
            'petugas_proses': row.find('.pemeriksaan-petugas-proses option:selected').val(),
            'hasil': row.find('.pemeriksaan-hasil').summernote('code'),
        };
        return data;
    }

    function gatherAllDataFromTable() {
        var allData = [];

        $('.table-pemeriksaan').each(function() {
            var row = $(this).find('.tbody-data');
            var rowData = gatherDataFromRow(row);
            allData.push(rowData);
        });

        return allData;
    }
</script>

<script type="text/javascript">
    function initDropzone(selector, pemeriksaanId) {
        Dropzone.autoDiscover = false;
        
        var myDropzone = 'myDropzone-' + pemeriksaanId;
        myDropzone = new Dropzone(selector, { 
            autoProcessQueue: true,
            maxFilesize: 30,
        });

        myDropzone.on("complete", function(file) {
            var element = `#list-file-${pemeriksaanId} tbody`;

            refreshFile(element, pemeriksaanId);
            myDropzone.removeFile(element, pemeriksaanId, file);
        });
    }

    function refreshFile(element, pemeriksaanId){
        console.log(element, pemeriksaanId);
        $(element).empty();

        $.ajax({
            url: '{site_url}term_laboratorium_bank_darah_hasil/refreshFile/' + pemeriksaanId,
            dataType: "json",
            success: function(data) {
                $(element).html(data.detail);
            }
        });
    }

    function removeFile(element, pemeriksaanId, fileId) {
        swal({
            title: "Anda Yakin ?",
            text : "Untuk Hapus data?",
            type : "success",
            showCancelButton: true,
            confirmButtonText: "Ya",
            confirmButtonColor: "#34a263",
            cancelButtonText: "Batalkan",
        }).then(function() {
            $.ajax({
                url: '{site_url}term_laboratorium_bank_darah_hasil/removeFile/' + fileId,
                complete: function() {
                    $.toaster({
                        priority : 'success',
                        title : 'Succes!',
                        message : ' Hapus Data'
                    });

                    refreshFile(element, pemeriksaanId);
                }
            });
        });
    }
</script>