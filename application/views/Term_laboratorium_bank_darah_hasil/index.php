<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>
<?php $user_id = $this->session->userdata('user_id'); ?>

<div class="block push-10">
    <div class="block-content bg-primary" style="border-radius: 10px;">
        <div class="form-horizontal">
            <div class="row pull-10">
                <div class="col-md-2" style="width: 14.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Tujuan Laboratorium</label>
                        <div class="col-xs-12">
                            <select id="tujuan_laboratorium" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="0" selected>Semua</option>
                                <?php foreach ($tujuan_laboratorium as $r) { ?>
                                <option value="<?= $r->id; ?>"><?= $r->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="width: 14.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Group Test</label>
                        <div class="col-xs-12">
                            <select id="group_test" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="0" selected>Semua</option>
                                <?php foreach (get_all('merm_group_test', ['status' => 1]) as $row) { ?>
                                <option value="<?= $row->id; ?>"><?= $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="width: 14.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="dokter_peminta">Dokter Peminta</label>
                        <div class="col-xs-12">
                            <select id="dokter_peminta" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="0" selected>Semua</option>
                                <?php foreach (get_all('mdokter', ['status' => 1]) as $row) { ?>
                                <option value="<?= $row->id; ?>"><?= $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="width: 18.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Tanggal Permintaan</label>
                        <div class="col-xs-12">
                            <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                <input class="form-control" type="text" id="tanggal_dari" value="{tanggal}">
                                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                <input class="form-control" type="text" id="tanggal_sampai" value="{tanggal}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="width: 14.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="">No. Medrec</label>
                        <div class="col-xs-12">
                            <input class="form-control" type="text" id="nomor_medrec" value="">
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="width: 14.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Nama Pasien</label>
                        <div class="col-xs-12">
                            <input class="form-control" type="text" id="nama_pasien" value="">
                        </div>
                    </div>
                </div>

                <div class="col-md-2" style="width: 10.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="">&nbsp;&nbsp;</label>
                        <div class="col-xs-12">
                            <button class="btn btn-success" type="button" title="Cari" id="btn-search"><i class="fa fa-filter"></i> Cari</button>
                            <button class="btn btn-warning" type="button" title="Filter" data-toggle="modal" data-target="#modal-filter"><i class="fa fa-expand"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="block">
    <div class="block-content">
        <ul class="nav nav-pills">
            <li id="tab-semua" class="<?= ($status_pemeriksaan == '#' ? 'active' : ''); ?>">
                <a href="javascript:void(0)" onclick="setTab('#')"><i class="si si-list"></i> Semua</a>
            </li>
            <li id="tab-menunggu-hasil" class="<?= ($status_pemeriksaan == '4' ? 'active' : ''); ?>">
                <a href="javascript:void(0)"  onclick="setTab(4)"><i class="si si-pin"></i> Menunggu Hasil </a>
            </li>
            <li id="tab-menunggu-validasi" class="<?= ($status_pemeriksaan == '5' ? 'active' : ''); ?>">
                <a href="javascript:void(0)" onclick="setTab(5)"><i class="fa fa-check-square-o"></i> Menunggu Validasi</a>
            </li>
            <li id="tab-selesai-validasi" class="<?= ($status_pemeriksaan == '6' ? 'active' : ''); ?>">
                <a href="javascript:void(0)"  onclick="setTab(6)"><i class="fa fa-flag"></i> Selesai Validasi</a>
            </li>
            <li id="tab-telah-dikirim" class="<?= ($status_pemeriksaan == '7' ? 'active' : ''); ?>">
                <a href="javascript:void(0)"  onclick="setTab(7)"><i class="fa fa-send"></i> Telah Dikirim</a>
            </li>
            <li id="tab-dibatalkan" class="<?= ($status_pemeriksaan == '0' ? 'active' : ''); ?>">
                <a href="javascript:void(0)"  onclick="setTab(0)"><i class="fa fa-trash"></i> Dibatalkan</a>
            </li>
        </ul>

        <br>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="table-transaksi">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Asal Pasien</th>
                                <th>Nomor Pendaftaran</th>
                                <th>Nomor Permintaan</th>
                                <th>Nomor Laboratorium</th>
                                <th>Tanggal Permintaan</th>
                                <th>Urutan Antrian</th>
                                <th>Antrian Daftar</th>
                                <th>No. Medrec</th>
                                <th>Nama Pasien</th>
                                <th>Kelompok Pasien</th>
                                <th>Status Tindakan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="status_pemeriksaan" name="status_pemeriksaan" value="{status_pemeriksaan}">
<input type="hidden" id="transaksiId" value="">

<?php $this->load->view('Term_laboratorium_bank_darah_hasil/modal/modal_filter'); ?>
<?php $this->load->view('Term_laboratorium_bank_darah_hasil/modal/modal_kirim_hasil_pemeriksaan'); ?>
<?php $this->load->view('Term_laboratorium_bank_darah_hasil/modal/modal_kirim_ulang_hasil_pemeriksaan'); ?>
<?php $this->load->view('Term_laboratorium_bank_darah_hasil/modal/modal_ubah_kelas_tarif'); ?>
<?php $this->load->view('Term_laboratorium_bank_darah_hasil/services'); ?>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        setTab($("#status_pemeriksaan").val());

        $(document).on("click", "#btn-search", function() {
            $("#cover-spin").show();

            loadListTransaksi();
        });
    });

    function setTab(tab) {
        $("#cover-spin").show();

        document.getElementById("tab-semua").classList.remove("active");
        document.getElementById("tab-menunggu-hasil").classList.remove("active");
        document.getElementById("tab-menunggu-validasi").classList.remove("active");
        document.getElementById("tab-selesai-validasi").classList.remove("active");
        document.getElementById("tab-telah-dikirim").classList.remove("active");
        document.getElementById("tab-dibatalkan").classList.remove("active");
        
        if (tab == '#') {
            document.getElementById("tab-semua").classList.add("active");
        }
        
        if (tab == '4') {
            document.getElementById("tab-menunggu-hasil").classList.add("active");
        }
        
        if (tab == '5') {
            document.getElementById("tab-menunggu-validasi").classList.add("active");
        }
        
        if (tab == '6') {
            document.getElementById("tab-selesai-validasi").classList.add("active");
        }
        
        if (tab == '7') {
            document.getElementById("tab-telah-dikirim").classList.add("active");
        }
        
        if (tab == '0') {
            document.getElementById("tab-dibatalkan").classList.add("active");
        }

        $("#status_pemeriksaan").val(tab);
        $("#selectStatusPemeriksaan").val(tab).trigger('change');

        loadListTransaksi();
    }

    function loadListTransaksi() {
        let tujuan_laboratorium = $("#selectTujuanLab option:selected").val();
        let group_test = $("#selectGroupTest option:selected").val();
        let dokter_peminta = $("#selectDokterPeminta option:selected").val();
        let tanggal_dari = $("#inputTanggalPermintaanDari").val();
        let tanggal_sampai = $("#inputTanggalPermintaanSampai").val();
        let nomor_medrec = $("#inputNomorMedrec").val();
        let nama_pasien = $("#inputNamaPasien").val();
        let nomor_pendaftaran = $("#inputNomorPendaftaran").val();
        let nomor_permintaan = $("#inputNomorPermintaan").val();
        let nomor_laboratorium = $("#inputNomorLaboratorium").val();
        let status_pemeriksaan = $("#selectStatusPemeriksaan option:selected").val();
        let kelompok_pasien = $("#selectKelompokPasien option:selected").val();
        let asal_pasien = $("#selectAsalPasien option:selected").val();

        let requestData = {
            tujuan_laboratorium: tujuan_laboratorium,
            group_test: group_test,
            dokter_peminta: dokter_peminta,
            tanggal_dari: tanggal_dari,
            tanggal_sampai: tanggal_sampai,
            nomor_medrec: nomor_medrec,
            nama_pasien: nama_pasien,
            nomor_pendaftaran: nomor_pendaftaran,
            nomor_permintaan: nomor_permintaan,
            nomor_laboratorium: nomor_laboratorium,
            status_pemeriksaan: status_pemeriksaan,
            kelompok_pasien: kelompok_pasien,
            asal_pasien: asal_pasien,
        }

        $('#table-transaksi').DataTable().destroy();
        $('#table-transaksi').DataTable({
            "autoWidth": false,
            "searching": true,
            "pageLength": 50,
            "serverSide": true,
            "processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
            "columnDefs": [
                {
                    "width": "5%",
                    "targets": [0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11],
                    "className": "text-center"
                },
                {
                    "width": "5%",
                    "targets": [8],
                    "className": "text-left"
                },
                {
                    "width": "10%",
                    "targets": [12],
                    "className": "text-center"
                },
            ],
            ajax: {
                url: '{site_url}term_laboratorium_bank_darah_hasil/getIndex',
                type: "POST",
                dataType: 'json',
                data: requestData
            }
        });

        $("#cover-spin").hide();
    }
</script>
