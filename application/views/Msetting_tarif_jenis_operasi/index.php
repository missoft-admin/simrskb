<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<h4><?=text_success('Kelompok Pasien')?></h4>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<h4><?=text_primary('Asuransi')?></h4>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-responsive" id="datatable-simrs_2">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
		
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
$(document).ready(function(){
   load_kelompok();
   load_asuransi();
});

function load_kelompok(){
	$('#datatable-simrs').DataTable().destroy();	
	  $('#datatable-simrs').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}msetting_tarif_jenis_operasi/getIndex',
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "10%",
                "targets": 0,
                "orderable": true
            },
            {
                "width": "50%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "20%",
                "targets": 2,
                "orderable": true
            }
        ]
    });
}
function load_asuransi(){
	$('#datatable-simrs_2').DataTable().destroy();	
	  $('#datatable-simrs_2').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}msetting_tarif_jenis_operasi/getIndex2',
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "10%",
                "targets": 0,
                "orderable": true
            },
            {
                "width": "50%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "20%",
                "targets": 2,
                "orderable": true
            }
        ]
    });
}
</script>
