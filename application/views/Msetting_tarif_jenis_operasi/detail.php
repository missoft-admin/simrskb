<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-rounded">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}msetting_tarif_jenis_operasi" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
		<div class="row">
           <input type="hidden" class="form-control" readonly id="idkelompokpasien" value="{idkelompokpasien}">
           <input type="hidden" class="form-control" readonly id="idrekanan" value="{idrekanan}">
            <div class="col-md-12">
			<div class="form-group">
                <label class="col-md-12" for="">Kelompok Pasien</label>
                <div class="col-md-12">
                    <input type="text" class="form-control" id="nama_kelompok" readonly value="{nama_kelompok}">
                </div>
            </div>
            </div>
			<?if ($idkelompokpasien=='1'){?>
			<div class="col-md-12">
			<div class="form-group">
                <label class="col-md-12" for="">Rekanan / Asuransi</label>
                <div class="col-md-12">
                    <input type="text" class="form-control" id="nama_rekanan" readonly value="{nama_rekanan}">
                </div>
            </div>
            </div>
			<?}?>
			<div class="col-md-12 push-20-t">
			<div class="form-group">
                <div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_tarif">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="20%">Kelompok Operasi</th>
										<th width="10%">Cito</th>
										<th width="20%">Jenis Operasi</th>
										<th width="20%">Tarif Jenis Operasi</th>
										<th width="15%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="kelompok_operasi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-Kelompok Operasi-</option>
												<?foreach(get_all('mkelompok_operasi',array('status'=>1)) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</th>
										<th>
											<select id="cito" name="cito" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-PILIH-</option>
												<?foreach(list_variable_ref(123) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										<th>
											<select id="jenis_operasi_id"  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-Jenis Operasi-</option>
												<?foreach(get_all('erm_jenis_operasi',array('status'=>1)) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
											</select>
										</th>
										
										<th>
											<select id="idjenisoperasi"  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-Jenis Operasi-</option>
												<?foreach($list_tarif as $r){?>
													<option value="<?=$r->id?>" ><?=$r->nama?></option>
												<?}?>
											</select>
										</th>
										
										<th>
											<button class="btn btn-primary btn-sm" type="button" onclick="simpan_tarif()"><i class="fa fa-plus"></i> Tambah</button>
											<button class="btn btn-warning btn-sm" type="button" onclick="clear_tarif()"><i class="fa fa-refresh"></i></button>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
					</div>
                </div>
            </div>
            </div>
       </div>
    </div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
   load_tarif();
});
function clear_tarif(){
	$("#kelompok_operasi_id").val("#").trigger('change');
	$("#cito").val("#").trigger('change');
	$("#jenis_operasi_id").val("#").trigger('change');
	$("#idjenisoperasi").val("#").trigger('change');
	
}
function simpan_tarif(){
	let idkelompokpasien = $("#idkelompokpasien").val();
	let idrekanan = $("#idrekanan").val();
	let kelompok_operasi_id = $("#kelompok_operasi_id").val();
	let cito = $("#cito").val();
	let jenis_operasi_id = $("#jenis_operasi_id").val();
	let idjenisoperasi = $("#idjenisoperasi").val();

	
	if (kelompok_operasi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Kelompok Operasi", "error");
		return false;
	}
	if (cito=='#'){
		sweetAlert("Maaf...", "Tentukan Cito", "error");
		return false;
	}
	if (jenis_operasi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Jenis Operasi", "error");
		return false;
	}
	if (idjenisoperasi=='#'){
		sweetAlert("Maaf...", "Tentukan Tarif", "error");
		return false;
	}
	
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Msetting_tarif_jenis_operasi/simpan_tarif', 
		dataType: "JSON",
		method: "POST",
		data : {
				idkelompokpasien : idkelompokpasien,
				idrekanan : idrekanan,
				kelompok_operasi_id : kelompok_operasi_id,
				cito : cito,
				jenis_operasi_id : jenis_operasi_id,
				idjenisoperasi : idjenisoperasi,

			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				clear_tarif();
				$('#index_tarif').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});
}
function hapus_tarif(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}Msetting_tarif_jenis_operasi/hapus_tarif',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_tarif').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function load_tarif(){
	// alert('sini');
	let idkelompokpasien = $("#idkelompokpasien").val();
	let idrekanan = $("#idrekanan").val();
	$('#index_tarif').DataTable().destroy();	
	table = $('#index_tarif').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}Msetting_tarif_jenis_operasi/load_tarif', 
                type: "POST" ,
                dataType: 'json',
				data : {
						idkelompokpasien : idkelompokpasien,
						idrekanan : idrekanan,
					   }
            }
        });
}
//RANAP
</script>