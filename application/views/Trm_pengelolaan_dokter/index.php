<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">

		<hr style="margin-top:0px">
		<div class="row">
			<?php echo form_open('trm_pengelolaan_dokter/filter', 'class="form-horizontal" id="form-work"') ?>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Dokter</label>
					<div class="col-md-8">
						<select class="js-select2 form-control" name="iddokter" style="width: 100%;" data-placeholder="">
							<option value="#">Semua Dokter</option>
							<?php foreach (get_all('mdokter', array('status' => 1)) as $row) { ?>
								<option value="<?= $row->id; ?>" <?= ($row->id == $iddokter ? 'selected' : ''); ?>"><?= $row->nama; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Jam</label>
					<div class="col-md-8">
						<input class="form-control input_time" type="text" name="jam" placeholder="Jam" value="{jam}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<div class="col-md-6">
			</div>
			<?php echo form_close() ?>
		</div>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>Nama Dokter</th>
					<th>Belum Selesai</th>
					<th>Sudah Selesai</th>
					<th>Jadwal Hari Ini</th>
					<th>Ruangan</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	$(".input_time").datetimepicker({
		format: "HH:mm",
		stepping: 30
	});
	
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}trm_pengelolaan_dokter/getIndex/' + '<?= $this->uri->segment(2); ?>',
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "10%", "targets": 0, "orderable": false },
					{ "width": "10%", "targets": 1, "orderable": false },
					{ "width": "10%", "targets": 2, "orderable": false },
					{ "width": "10%", "targets": 3, "orderable": false },
					{ "width": "10%", "targets": 4, "orderable": false },
					{ "width": "10%", "targets": 5, "orderable": false },
				]
			});
	});
</script>