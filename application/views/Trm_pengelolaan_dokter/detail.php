<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
		<ul class="block-options">
			<li>
				<a href="{base_url}trm_pengelolaan_dokter" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
	</div>
	<div class="block-content">

		<hr style="margin-top:0px">
		<div class="row">
			<?php echo form_open('trm_pengelolaan_dokter/detail_filter/' . $iddokter, 'class="form-horizontal" id="form-work"') ?>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">No. Pengajuan</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="no_pengajuan" placeholder="No. Pengajuan" value="{no_pengajuan}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">No. Medrec</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="no_medrec" placeholder="No. Medrec" value="{no_medrec}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Nama Pasien</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="nama_pasien" placeholder="Nama Pasien" value="{nama_pasien}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Jenis Kunjungan</label>
					<div class="col-md-8">
						<select class="js-select2 form-control" name="jenis_kunjungan" style="width: 100%;" data-placeholder="">
							<option value="#">Semua Jenis Kunjungan</option>
							<option value="1" <?= ('1' == $jenis_kunjungan ? 'selected' : ''); ?>>Rawat Jalan</option>
							<option value="2" <?= ('2' == $jenis_kunjungan ? 'selected' : ''); ?>>Instalasi Gawat Darurat (IGD)</option>
							<option value="3" <?= ('3' == $jenis_kunjungan ? 'selected' : ''); ?>>Rawat Inap</option>
							<option value="4" <?= ('4' == $jenis_kunjungan ? 'selected' : ''); ?>>One Day Surgery (ODS)</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Estimasi</label>
					<div class="col-md-8">
						<input class="form-control js-datepicker" type="text" name="estimasi_selesai" placeholder="Estimasi" value="{estimasi_selesai}" data-date-format="dd/mm/yyyy">
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Jenis Pengajuan</label>
					<div class="col-md-8">
						<select class="js-select2 form-control" name="jenis_pengajuan" style="width: 100%;" data-placeholder="">
							<option value="#">Semua Jenis Pengajuan</option>
							<?php foreach (get_all('mpengajuan_skd', array('status' => 1)) as $row) { ?>
								<option value="<?= $row->id; ?>" <?= ($row->id == $jenis_pengajuan ? 'selected' : ''); ?>><?= $row->nama; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Status Rawat</label>
					<div class="col-md-8">
						<select class="js-select2 form-control"" name="status_rawat" style="width: 100%;" data-placeholder="">
							<option value="#">Semua Status Rawat</option>
							<option value="0" <?= ($status_rawat == '0' ? 'selected' : ''); ?>>Masih Rawat</option>
							<option value="1" <?= ($status_rawat == '1' ? 'selected' : ''); ?>>Sudah Pulang</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Status Waktu</label>
					<div class="col-md-8">
						<select class="js-select2 form-control" name="status_waktu" style="width: 100%;" data-placeholder="">
							<option value="#">Semua Status Waktu</option>
							<option value="merah" <?= ($status_waktu == 'merah' ? 'selected' : ''); ?>>Merah ( < 5 Hari )</option>
							<option value="kuning" <?= ($status_waktu == 'kuning' ? 'selected' : ''); ?>>Kuning  ( < 10 Hari )</option>
							<option value="hijau" <?= ($status_waktu == 'hijau' ? 'selected' : ''); ?>>Hijau ( >= 10 Hari )</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No. Pengajuan</th>
					<th>Tanggal Pengajuan</th>
					<th>No. Medrec</th>
					<th>Nama Pasien</th>
					<th>Jenis Kunjungan</th>
					<th>Dokter</th>
					<th>Jenis Pengajuan</th>
					<th>Nama Pemohon</th>
					<th>Keterangan</th>
					<th>Estimasi</th>
					<th>Status Rawat</th>
					<th>Status Waktu</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}trm_pengelolaan_dokter/getDetail/' + '<?= $this->uri->segment(2); ?>' + '/' + '{iddokter}',
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": false },
					{ "width": "5%", "targets": 1, "orderable": false },
					{ "width": "5%", "targets": 2, "orderable": true },
					{ "width": "5%", "targets": 3, "orderable": false },
					{ "width": "10%", "targets": 4, "orderable": true },
					{ "width": "10%", "targets": 5, "orderable": false },
					{ "width": "10%", "targets": 6, "orderable": false },
					{ "width": "5%", "targets": 7, "orderable": false },
					{ "width": "10%", "targets": 8, "orderable": false },
					{ "width": "10%", "targets": 9, "orderable": false },
					{ "width": "5%", "targets": 10, "orderable": false },
					{ "width": "5%", "targets": 11, "orderable": false },
					{ "width": "10%", "targets": 12, "orderable": false },
				]
			});
	});
</script>