<div class="modal" id="modal_user" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width:50%">
        <form class="form-horizontal" action="javascript:void(0)" id="frm1">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">ADD User</h3>
                    </div>
                    <div class="block-content">
						<div class="block">
						<ul class="nav nav-tabs" data-toggle="tabs" id="tabs">
							<li class="active" id="tab_list">
								<a href="#tab_list_user" onclick="LoadUser()"><i class="fa fa-list-ol"></i> List User</a>
							</li>
							<?php if (UserAccesForm($user_acces_form,array('257'))): ?>
							<li id="tab_add">
								<a href="#tab_add_user"><i class="fa fa-pencil"></i> Add</a>
							</li>
							<?php endif ?>
						</ul>
						<div class="block-content tab-content">
							<div class="tab-pane fade fade-left in active" id="tab_list_user">
								<input type="text" class="form-control" readonly id="mppa_id" placeholder="Nama" name="nama" value="">
								<h5 class="font-w300 push-15" id="lbl_list_user_add">Dafar User</h5>
								<table class="table table-bordered table-striped" id="index_user_add">
                                    <thead>
                                        <th>NO</th>
                                        <th>Nama</th>
                                        <th>Username</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Tools</th>
                                    </thead>
                                    <tbody></tbody>
                                  
                                </table>
							</div>
							<?php if (UserAccesForm($user_acces_form,array('257'))): ?>
							<div class="tab-pane fade fade-left" id="tab_add_user">
								<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-12">
											<a href="{base_url}musers/create" target="_blank" class="btn btn-success text-uppercase" type="button" name="btn_save_user_add" id="btn_save_user_add"><i class="fa fa-plus"></i> Tambah User</a>
											
										</div>
									</div>
								<?php echo form_close() ?>
							</div>
							<?php endif ?>
						</div>
					</div>
                        
                                     
                  </div>
                </div>
                <div class="modal-footer" style="margin-top:20px">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		// $('#modal_user').modal('show');
		// $('[href="#tab_list_user"]').tab('show');
		// LoadUser();
	})	

function upload_user(id){
	$("#id_foto_add").val(id);
	$("#modal_upload_foto_user").modal('show');
}
$("#btn_clear_user_add").click(function(){
	clear_user_add();
});
$("#btn_save_user_add").click(function(){
	save_user_add();
});

function pilih_user(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghubungkan User Dengan MPPA ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
			let mppa_id= $("#mppa_id").val();
			// alert(id+' '+mppa_id);
		// $("#cover-spin").show();
			$.ajax({
					url: '{site_url}mppa/pilih_user',
					type: 'POST',
					dataType: 'json',
					data: {
						user_id: id,
						mppa_id: mppa_id,
						
					},
					success: function(data) {
						$("#index_all").DataTable().ajax.reload( null, false );
						$("#cover-spin").hide();
						$("#modal_user").modal('hide');
					}
				});
	});


	
}


function LoadUser() {
		var idpasien=$("#idpasien").val();
		
		$('#index_user_add').DataTable().destroy();
		var table = $('#index_user_add').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}mppa/LoadUser/',
			type: "POST",
			dataType: 'json',
			data: {
				idpasien: idpasien,
				
			}
		},
		columnDefs: [
					 {  className: "text-center", targets:[0,4] },
					 { "width": "5%", "targets": [0] },
					 { "width": "10%", "targets": [2,3,4] },
					 { "width": "15%", "targets": [1,5] },

					]
		});
	}
function clear_user_add(){
	$("#id_user").val('')
	$("#kelompokpasien_user_add").val('').trigger('change');
	$("#idrekanan_user_add").val('').trigger('change');
	$("#nouser2").val('');
	$("#nama_partisipant").val('');
}
	function save_user_add(){
		

		var id_user=$("#id_user").val();		
		var idpasien=$("#idpasien").val();		
		var kelompokpasien=$("#kelompokpasien_user_add").val();		
		var idrekanan=$("#idrekanan_user_add").val();		
		var nouser=$("#nouser2").val();	
		// alert(nouser);
// return false;		
		var nama_partisipant=$("#nama_partisipant").val();		
		if (kelompokpasien==''){
			sweetAlert("Maaf...", "Tentukan Tipe", "error");
			return false;

		}
		if (kelompokpasien=='1'){
			if (idrekanan==''){
			sweetAlert("Maaf...", "Tentukan Asuransi", "error");
			return false;
				
			}

		}
		if (nouser==''){
			sweetAlert("Maaf...", "Tentukan No User", "error");
			return false;

		}
		if (nama_partisipant==''){
			sweetAlert("Maaf...", "Tentukan Nama Participant", "error");
			return false;

		}
		
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}mppa/save_user',
			type: 'POST',
			data: {
				id_user: id_user,
				idpasien: idpasien,
				kelompokpasien: kelompokpasien,
				idrekanan: idrekanan,
				nouser: nouser,
				nama_partisipant: nama_partisipant,
			},
			success: function(data) {
				// $.toaster({priority : 'success', title : 'Succes!', message : ' Update Pembayaran'});
				if (data==false){
					swal({
							title: "Gagal!",
							text: "Data tidak diproses.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});
					
				}else{
					clear_user_add();
						$('[href="#tab_list_user"]').tab('show');
						LoadUser();
						swal({
							title: "Berhasil!",
							text: "Data diproses.",
							type: "success",
							timer: 1500,
							showConfirmButton: false
						});
					
				}
				// location.reload();
				$("#cover-spin").hide();
				// table.ajax.reload( null, false ); 
			}
		});
	
	}
</script>