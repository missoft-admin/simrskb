<!-- Modal Cari Pasien -->
<div class="modal in" id="modal_pegawai" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:70%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Data Pegawai / Dokter</h3>
				</div>
				<div class="block-content">
					<hr>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snip">Nip</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snip" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snamapegawai">Nama</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snamapegawai" value="">
									</div>
								</div>
								
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="stipepegawai">Tipe</label>
									<div class="col-md-8">
										<select id="stipepegawai" name="stipepegawai" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
											<option value="#" >- All -</option>
											<option value="1" >Pegawai</option>
											<option value="2" >Dokter</option>
											
										</select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="sstaktif">Status</label>
									<div class="col-md-8">
										<select name="staktif" id="sstaktif" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
											<option value="#" >- All -</option>
											<option value="1" >Terdaftar</option>
											<option value="0" >Tidak Terdaftar</option>
										</select>	
									</div>
								</div>
								
								<div class="form-group" style="margin-top: 15px;">
									<label class="col-md-4 control-label" for="snotelepon"></label>
									<div class="col-md-8">
										<button class="btn btn-primary" type="button" id="btn-cari-pegawai" style="width:100%"><i class="fa fa-search"></i> Cari</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>

					<table width="100%" class="table table-bordered table-striped" id="datatable-pegawai">
						<thead>
							<tr>
								<th>No.</th>
								<th>Tipe</th>
								<th>Nip</th>
								<th>Nama</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="pegawaimodaltabel">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$("#btn-cari-pegawai").click(function(){
	
	loadDataPegawai();
});

$("#btn_cari_pegawai").click(function(){
	
	loadDataPegawai();
});

function loadDataPegawai() {
	let snip=$("#snip").val();
	let snamapegawai=$("#snamapegawai").val();
	let stipepegawai=$("#stipepegawai").val();
	let sstaktif=$("#sstaktif").val();
	$('#datatable-pegawai').DataTable().destroy();
		var tablePasien = $('#datatable-pegawai').DataTable({
			"pageLength": 8,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}mppa/getPegawai',
				type: "POST",
				dataType: 'json',
				data: {
					snip: snip,
					snamapegawai: snamapegawai,
					stipepegawai: stipepegawai,
					sstaktif: sstaktif,
				}
			},
			columnDefs: [
			{  className: "text-right", targets:[0] },
			{  className: "text-center", targets:[1,4] },
			 { "width": "5%", "targets": [0] },
			 { "width": "10%", "targets": [1,4] },
			 { "width": "40%", "targets": [3] },
			 { "width": "20%", "targets": [2] }
		]

		});
		
}

</script>
<!-- End Modal Cari Pasien -->