<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mppa" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mppa/save','class="js-form1 validation form-horizontal" id="form1"') ?>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px; padding-bottom:0px;">
						<label class="col-md-4 control-label" for="nama">Tipe <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<select id="tipepegawai" <?=($id?'disabled':'')?> name="tipepegawai" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="0" <?=($tipepegawai == 0 ? 'selected="selected"':'')?>>- All -</option>
							<option value="1" <?=($tipepegawai == 1 ? 'selected="selected"':'')?>>Pegawai</option>
							<option value="2" <?=($tipepegawai == 2 ? 'selected="selected"':'')?>>Dokter</option>
							
						</select>
						</div>
						
					</div>
					<input type="hidden" id="pegawai_id" name="pegawai_id" value="{pegawai_id}">
					
					
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="tglpendaftaran">NIP<span style="color:red;">*</span></label>
						<div class="col-md-8">
							<div class="input-group">
								<input readonly class="form-control" id="nip" placeholder="NIP" name="nip" value="{nip}" required>
								
								<span class="input-group-btn">
									<button class="btn btn-success" onclick="show_modal()" type="button" id="btn_cari_pasien"><i class="fa fa-search"></i> Cari</button>
									
								</span>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px; padding-bottom:0px;">
						<label class="col-md-4 control-label" for="nama">NAMA <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<input tabindex="6" type="text" class="form-control" id="nama" placeholder="Nama Lengkap" name="nama" value="{nama}" required>
						</div>
						
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="nik">NIK <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<input tabindex="6" type="text" class="form-control number" onkeyup="this.value=this.value.replace(/[^\d]/,'')" id="nik" placeholder="NIK" name="nik" value="{nik}" minlength="16" maxlength="16" required>
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="jenis_kelamin">Jenis Kelamin <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<select tabindex="7" id="jenis_kelamin" name="jenis_kelamin" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="">Pilih Opsi</option>
								<?foreach(list_variable_ref(1) as $row){?>
								<option value="<?=$row->id?>" <?=($jenis_kelamin == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="tempat_lahir">Tempat Lahir <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<input tabindex="9" type="text" class="form-control pas_tidak_dikenal" id="tempat_lahir" placeholder="Tempat Lahir" name="tempat_lahir" value="{tempat_lahir}" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="tgllahir">Tanggal Lahir<span style="color:red;">*</span></label>
						<div class="col-md-3  col-xs-12 push-5">
							<select tabindex="10" name="tgl_lahir" id="hari" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="0">Hari</option>
								<?php for ($i = 1; $i < 32; $i++) {?>
									<?php $hari = str_pad($i, 2, '0', STR_PAD_LEFT); ?>
									<option value="<?= $hari; ?>" <?=($tgl_hari == $hari ? 'selected="selected"' : '')?>><?= $hari; ?></option>
								<?php }?>
							</select>
						</div>
						<div class="col-md-2  col-xs-12 push-5">
							<select tabindex="11" name="bulan_lahir" id="bulan" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="00">Bulan</option>
								<option value="01" <?=($tgl_bulan == '01' ? 'selected="selected"' : '')?>>Januari</option>
								<option value="02" <?=($tgl_bulan == '02' ? 'selected="selected"' : '')?>>Februari</option>
								<option value="03" <?=($tgl_bulan == '03' ? 'selected="selected"' : '')?>>Maret</option>
								<option value="04" <?=($tgl_bulan == '04' ? 'selected="selected"' : '')?>>April</option>
								<option value="05" <?=($tgl_bulan == '05' ? 'selected="selected"' : '')?>>Mei</option>
								<option value="06" <?=($tgl_bulan == '06' ? 'selected="selected"' : '')?>>Juni</option>
								<option value="07" <?=($tgl_bulan == '07' ? 'selected="selected"' : '')?>>Juli</option>
								<option value="08" <?=($tgl_bulan == '08' ? 'selected="selected"' : '')?>>Agustus</option>
								<option value="09" <?=($tgl_bulan == '09' ? 'selected="selected"' : '')?>>September</option>
								<option value="10" <?=($tgl_bulan == '10' ? 'selected="selected"' : '')?>>Oktober</option>
								<option value="11" <?=($tgl_bulan == '11' ? 'selected="selected"' : '')?>>November</option>
								<option value="12" <?=($tgl_bulan == '12' ? 'selected="selected"' : '')?>>Desember</option>
							</select>
						</div>
						<div class="col-md-3  col-xs-12 push-5" >
							<select tabindex="12" name="tahun_lahir" id="tahun" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="0">Tahun</option>
								<?php for ($i = date('Y'); $i >= 1900; $i--) {?>
									<option value="<?= $i; ?>" <?=($tgl_tahun == $i ? 'selected="selected"' : '')?>><?= $i; ?></option>
								<?php }?>
							</select>
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-12 control-label text-primary"><h5 align="left"><b>Alamat Domisili</b></h5></label>
						<div class="col-md-12">
							<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
						</div>
					</div>
					<div class="form-group" >
						<label class="col-md-4 control-label" for="alamat">Alamat<span style="color:red;">*</span></label>
						<div class="col-md-8">
							<textarea tabindex="13" class="form-control " id="alamat" placeholder="Alamat" name="alamat" required><?= $alamat?></textarea>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 0px;">
						<div class="col-md-4"></div>
						<label class="col-md-3 control-label" for="provinsi_id"><p align="left">Provinsi</p></label>
						<div class="col-md-5">
							<?php if ($id == null) {?>
							<select tabindex="14" name="provinsi_id" id="provinsi_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</option>
									<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
									<option value="<?= $a->id; ?>" <?= ($a->id == '12') ? 'selected' : ''; ?>><?= $a->nama; ?></option>
								<?php endforeach; ?>
								</select>
							<?php } else {?>
							<select tabindex="14" name="provinsi_id" id="provinsi_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</option>
									<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
									<option value="<?= $a->id; ?>" <?=($provinsi_id == $a->id ? 'selected="selected"' : '')?>><?= $a->nama; ?></option>
								<?php endforeach; ?>
								</select>
							<?php }?>
						</div>
					</div>
					<div style="display: block;">
						<div class="form-group" style="margin-bottom: 10px;">
							<div class="col-md-4"></div>
							<label class="col-md-3" for="kabupaten_id"><left>Kabupaten/Kota</left> <?=$kabupaten_id?></label>
							<div class="col-md-5">
								<?php if ($id == '') {?>
								<select tabindex="15" name="kabupaten_id" id="kabupaten_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</<option>
										<?php foreach (get_all('mfwilayah', ['parent_id' => '12']) as $r):?>
										<option value="<?= $r->id; ?>" <?= ($r->id == $kabupaten_id) ? 'selected' : ''; ?>><?= $r->nama; ?></<option>
										<?php endforeach; ?>
									</select>
								<?php } else {?>
								<select tabindex="15" name="kabupaten_id" id="kabupaten_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="">Pilih Opsi</<option>
										<?php foreach (get_all('mfwilayah', ['parent_id' => $provinsi_id]) as $r):?>
										<option value="<?= $r->id; ?>" <?=($kabupaten_id == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
										<?php endforeach; ?>
									</select>
								<?php }?>
							</div>
						</div>
					</div>
					<div style="display: block;">
						<div class="form-group" style="margin-bottom: 10px;">
							<div class="col-md-4"></div>
							<label class="col-md-3 control-label" for="kecamatan_id" style="text-align:left;">Kecamatan</label>
							<div class="col-md-5">
								<?php if ($id == '') {?>
								<select tabindex="16" name="kecamatan_id" id="kecamatan_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Kecamatan" required>
									<option value="">Pilih Opsi</<option>
									<?php foreach (get_all('mfwilayah', ['parent_id' => '213']) as $r):?>
									<option value="<?= $r->id; ?>"><?= $r->nama; ?></<option>
									<?php endforeach; ?>
								</select>
								<?php } else {?>
								<select tabindex="16" name="kecamatan_id" id="kecamatan_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</<option>
									<?php foreach (get_all('mfwilayah', ['parent_id' => $kabupaten_id]) as $r):?>
									<option value="<?= $r->id; ?>"<?=($kecamatan_id == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
									<?php endforeach; ?>
								</select>
								<?php }?>
							</div>
						</div>
					</div>
					
					<div  style="display: block;">
						<div class="form-group" style="margin-bottom: 10px;">
							<div class="col-md-4"></div>
							<label class="col-md-3 control-label" for="kelurahan_id" style="text-align:left;">Kelurahan</label>
							<div class="col-md-5">
								<select tabindex="17" name="kelurahan_id" id="kelurahan_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</<option>
									<?php foreach (get_all('mfwilayah', ['parent_id' => $kecamatan_id]) as $r):?>
									<option value="<?= $r->id; ?>"<?=($kelurahan_id == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<div class="col-md-4"></div>
							<label class="col-md-3 control-label" for="kodepos" style="text-align:left;">Kode Pos</label>
							<div class="col-md-5">
								<input tabindex="18" type="text" class="form-control " id="kodepos" placeholder="Kode Pos" name="kodepos" value="{kodepos}" readonly="true" required>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 0px;">
						<div class="col-md-4"></div>
						<label class="col-md-3 control-label" for="rw"><p align="left">RW</p></label>
						<div class="col-md-5">
							
							<input type="text" class="form-control " id="rw" name="rw" value="{rw}" placeholder="RW" required>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 0px;">
						<div class="col-md-4"></div>
						<label class="col-md-3 control-label" for="rt"><p align="left">RT</p></label>
						<div class="col-md-5">
							
							<input type="text" class="form-control " id="rt" name="rt" value="{rt}" placeholder="RT" required>
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom: 0px;">
							<div class="col-md-4"></div>
							<label class="col-md-3 control-label" for="warganegara_id"><p align="left">Kewarganegaraan</p></label>
							<div class="col-md-5">
								<select tabindex="25" id="warganegara_id" name="warganegara_id" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</option>
									<?foreach(list_variable_ref(18) as $row){?>
									<option value="<?=$row->id?>" <?=($warganegara_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
									<?}?>
									
								</select>
							</div>
						</div>
					<hr>
					<input type="hidden" id="id" name="id" value="{id}">
					<input type="hidden" id="status_cari" name="status_cari" value="0">
					<input type="hidden" id="ckabupaten" name="ckabupaten" value="{kabupaten_id}">
					<input type="hidden" id="ckecamatan" name="ckecamatan" value="{kecamatan_id}">
					<input type="hidden" id="ckelurahan" name="ckelurahan" value="{kelurahan_id}">
					
					<input type="hidden" id="ckabupaten_ktp" name="ckabupaten_ktp" value="{kabupaten_id_ktp}">
					<input type="hidden" id="ckecamatan_ktp" name="ckecamatan_ktp" value="{kecamatan_id_ktp}">
					<input type="hidden" id="ckelurahan_ktp" name="ckelurahan_ktp" value="{kelurahan_id_ktp}">
					
				</div>

				<div class="col-md-6">
					
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="agama">Agama <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<select tabindex="24" id="agama_id" name="agama_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="">Pilih Opsi</option>
								<?foreach(list_variable_ref(3) as $row){?>
								<option value="<?=$row->id?>" <?=($agama_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
								<?}?>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px; padding-bottom:0px;">
						<label class="col-md-4 control-label" for="gelar_depan">Gelar Depan <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<input tabindex="6" type="text" class="form-control" id="gelar_depan" placeholder="Gelar Depan" name="gelar_depan" value="{gelar_depan}" required>
						</div>
						
					</div>
					<div class="form-group" style="margin-bottom: 10px; padding-bottom:0px;">
						<label class="col-md-4 control-label" for="gelar_belakang">Gelar Belakang <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<input tabindex="6" type="text" class="form-control" id="gelar_belakang" placeholder="Gelar Belakang" name="gelar_belakang" value="{gelar_belakang}" required>
						</div>
						
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="jenis_profesi_id">Jenis Profesi</label>
						<div class="col-md-8">
							<select tabindex="27" id="jenis_profesi_id" name="jenis_profesi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="">Pilih Opsi</option>
								<?foreach(list_variable_ref(21) as $row){?>
								<option value="<?=$row->id?>" <?=($jenis_profesi_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="spesialisasi_id">Spesialist <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<select tabindex="27" id="spesialisasi_id" name="spesialisasi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="">Pilih Opsi</option>
								<?foreach(list_variable_ref(22) as $row){?>
								<option value="<?=$row->id?>" <?=($spesialisasi_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="statuskawin">Pendidikan <span style="color:red;">*</span></label>
						<div class="col-md-8">
							<select tabindex="27" id="pendidikan_id" name="pendidikan_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="">Pilih Opsi</option>
								<?foreach(list_variable_ref(13) as $row){?>
								<option value="<?=$row->id?>" <?=($pendidikan_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					<div class="form-group " >
						<label class="col-md-4 col-xs-12 control-label" for="umur">Umur <span style="color:red;">*</span></label>
						<div class="col-md-3 col-xs-12 push-5">
							<input type="text" class="form-control pas_tidak_dikenal" id="umur_tahun" name="umur_tahun" value="{umur_tahun}" placeholder="Tahun" readonly="true" required>
						</div>
						<div class="col-md-2 col-xs-12 push-5">
							<input type="text" class="form-control pas_tidak_dikenal" id="umur_bulan" name="umur_bulan" value="{umur_bulan}" placeholder="Bulan" readonly="true" required>
						</div>
						<div class="col-md-3 col-xs-12 push-5">
							<input type="text" class="form-control pas_tidak_dikenal" id="umur_hari" name="umur_hari" value="{umur_hari}" placeholder="Hari" readonly="true" required>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label text-success"><h5 align="left"><b>Alamat Kartu Identitas</b></h5></label>
						<div class="col-md-8">
							<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
								<input type="checkbox" id="chk_st_domisili"  name="chk_st_domisili" <?=($chk_st_domisili?'checked':'')?>><span></span> Sama dengan Domisili
							</label>
						</div>
						<div class="col-md-12">
							<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
						</div>
					</div>
					
					<div class="form-group" >
						<label class="col-md-4 control-label" for="alamat">Alamat<span style="color:red;">*</span></label>
						<div class="col-md-8">
							<textarea tabindex="13"   class="form-control" id="alamat_ktp" placeholder="Alamat" name="alamat_ktp" required><?= $alamat_ktp?></textarea>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 0px;">
						<div class="col-md-4"></div>
						<label class="col-md-3 control-label" for="provinsi"><p align="left">Provinsi</p></label>
						<div class="col-md-5">
							
							<select tabindex="14" name="provinsi_id_ktp"   id="provinsi_id_ktp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</option>
									<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
									<option value="<?= $a->id; ?>" <?=($provinsi_id_ktp == $a->id ? 'selected="selected"' : '')?>><?= $a->nama; ?></option>
								<?php endforeach; ?>
								</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<div class="col-md-4"></div>
						<label class="col-md-3" for="kabupaten_id_ktp"><left>Kabupaten/Kota</left></label>
						<div class="col-md-5">
							
							<select tabindex="15" name="kabupaten_id_ktp"   id="kabupaten_id_ktp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</<option>
									<?php foreach (get_all('mfwilayah', ['parent_id' => $provinsi_id_ktp]) as $r):?>
									<option value="<?= $r->id; ?>" <?=($kabupaten_id_ktp == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
									<?php endforeach; ?>
								</select>
						</div>
					</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<div class="col-md-4"></div>
							<label class="col-md-3 control-label"   for="kecamatan_id_ktp" style="text-align:left;">Kecamatan</label>
							<div class="col-md-5">
								
								<select tabindex="16" name="kecamatan_id_ktp"   id="kecamatan_id_ktp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required >
									<option value="">Pilih Opsi</<option>
									<?php foreach (get_all('mfwilayah', ['parent_id' => $kabupaten_id_ktp]) as $r):?>
									<option value="<?= $r->id; ?>"<?=($kecamatan_id_ktp == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<div class="col-md-4"></div>
							<label class="col-md-3 control-label" for="kelurahan_id_ktp" style="text-align:left;">Kelurahan</label>
							<div class="col-md-5">
								<select tabindex="17" name="kelurahan_id_ktp"   id="kelurahan_id_ktp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</<option>
									<?php foreach (get_all('mfwilayah', ['parent_id' => $kecamatan_id_ktp]) as $r):?>
									<option value="<?= $r->id; ?>"<?=($kelurahan_id_ktp == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<div class="col-md-4"></div>
							<label class="col-md-3 control-label" for="kodepos_ktp" style="text-align:left;">Kode Pos</label>
							<div class="col-md-5">
								<input tabindex="18" type="text" class="form-control"   id="kodepos_ktp" placeholder="Kode Pos" name="kodepos_ktp" value="{kodepos_ktp}" readonly="true" required>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 0px;">
							<div class="col-md-4"></div>
							<label class="col-md-3 control-label" for="rw_ktp"><p align="left">RW</p></label>
							<div class="col-md-5">
								
								<input type="text" class="form-control" id="rw_ktp" name="rw_ktp" value="{rw_ktp}" placeholder="RW" required>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 0px;">
							<div class="col-md-4"></div>
							<label class="col-md-3 control-label" for="rt_ktp"><p align="left">RT</p></label>
							<div class="col-md-5">
								
								<input type="text" class="form-control" id="rt_ktp" name="rt_ktp" value="{rt_ktp}" placeholder="RT" required>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 0px;">
							<div class="col-md-4"></div>
							<label class="col-md-3 control-label" for="rt_ktp"><p align="left">Kewarganegaraan</p></label>
							<div class="col-md-5">
								<select tabindex="25" id="warganegara_id_ktp" name="warganegara_id_ktp" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</option>
									<?foreach(list_variable_ref(18) as $row){?>
									<option value="<?=$row->id?>" <?=($warganegara_id_ktp == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
									<?}?>
									
								</select>
							</div>
						</div>
				</div>
				
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="block block-themed">
						<div class="block-header bg-info">
							<ul class="block-options">
								
								<li>
									<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="fa fa-address-book-o"></i> KONTAK TAMBAHAN &nbsp;&nbsp;&nbsp; <button type="button" onclick="tambah_kontak()" class="btn btn-xs btn-success" title="Tambah Kontak Tambahan"><i class="fa fa-plus"></i></button></h3>
						</div>
						<div class="block-content">
								<div hidden>
									<table>
										<tr class="copied">
											<td>
												<select tabindex="23" name="level_kontak[]" class="div_select2  form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
													<option value="2" selected>Secondary</option>
													<option value="1">Primary</option>
												</select>
											</td>
											<td>
												<select tabindex="23" name="jenis_kontak[]" class="div_select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
													<option value="">Pilih Opsi</option>
													<?foreach(list_variable_ref(20) as $row){?>
													<option value="<?=$row->id?>" ><?=$row->nama?></option>
													<?}?>
												</select>
														
											</td>
											<td>
												<input tabindex="29" type="text" class="form-control" placeholder="Kontak" name="nama_kontak[]" value=""  >
												<input type="hidden" name="id_kontak_tambahan[]" value="">
											</td>
											<td>
												<div class="btn-group">
													<button class="btn btn-xs btn-danger" onclick="hapus_kontak(this)" type="button" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-trash"></i></button>
												</div>
											</td>
										</tr>
									</table>
								</div>
								<div class="pasted">
									<table id="pasted" class="table table-bordered">
										<thead>
											<tr>
											<th class="text-center">Primary / Secondary</th>
											<th class="text-center">Jenis Kontak</th>
											<th class="text-center">Kontak</th>
											<th class="text-center">Action</th>
											</tr>
										</thead>
										<tbody>
											
											
											<?foreach($list_kontak as $r_kontak){
										$jenis_kontak_id=$r_kontak->jenis_kontak;
										$level_kontak_id=$r_kontak->level_kontak;
										?>
										<tr>
											<td>
													<select tabindex="23" name="level_kontak[]" class="js_select2  form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
														<option value="2" <?=($level_kontak_id=='2'?'selected':'')?>>Secondary</option>
														<option value="1" <?=($level_kontak_id=='1'?'selected':'')?>>Primary</option>
													</select>
											</td>
											<td>
													<select tabindex="23" name="jenis_kontak[]" class="js_select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(20) as $row){?>
														<option value="<?=$row->id?>" <?=($jenis_kontak_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
											</td>
											<td>
													<input tabindex="29" type="text" class="form-control" placeholder="Kontak" name="nama_kontak[]" value="<?=$r_kontak->nama_kontak?>"  >
													<input type="hidden" name="id_kontak_tambahan[]" value="<?=$r_kontak->id?>">
											</td>
											<td>
												<div class="btn-group">
													<button class="btn btn-xs btn-danger" onclick="hapus_kontak_id(this,<?=$r_kontak->id?>)" type="button" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-trash"></i></button>
												</div>
											</td>
										</tr>
										
										<?}?>
										</tbody>
									</table>
									
								</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="block block-themed">
						<div class="block-header bg-info">
							<ul class="block-options">
								
								<li>
									<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="fa fa-address-card-o"></i> KARTU IDENTITAS &nbsp;&nbsp;&nbsp; <button type="button" onclick="tambah_identitas()" class="btn btn-xs btn-success" title="Tambah Kontak Tambahan"><i class="fa fa-plus"></i></button></h3>
						</div>
						<div class="block-content">
								<div hidden>
									<table>
										<tr class="copied2">
											
											<td>
												<select tabindex="23" name="jenis_id[]" class="div_select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
													<option value="">Pilih </option>
													<?foreach(list_variable_ref(10) as $row){?>
													<option value="<?=$row->id?>" ><?=$row->nama?></option>
													<?}?>
												</select>
														
											</td>
											<td>
												<input tabindex="29" type="text" class="form-control" placeholder="Identitas" name="nama_identitas[]" value=""  >
												<input type="hidden" name="id_identitas_tambahan[]" value="">
											</td>
											<td>
												<div class="btn-group">
													<button class="btn btn-xs btn-danger" onclick="hapus_identitas(this)" type="button" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-trash"></i></button>
												</div>
											</td>
										</tr>
									</table>
								</div>
								<div class="pasted2">
									<table id="pasted2" class="table table-bordered">
										<thead>
											<tr>
											<th class="text-center" style="width:35%">Jenis Identitas</th>
											<th class="text-center" style="width:55%">Identitas</th>
											<th class="text-center" style="width:10%">Action</th>
											</tr>
										</thead>
										<tbody>
											
											
											<?foreach($list_identitas as $r_identitas){
										$jenis_id=$r_identitas->jenis_id;
										?>
										<tr>
											
											<td>
													<select tabindex="23" name="jenis_id[]" class="js_select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
														<option value="">Pilih Opsi</option>
														<?foreach(list_variable_ref(10) as $row){?>
														<option value="<?=$row->id?>" <?=($jenis_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
														<?}?>
														
													</select>
											</td>
											<td>
													<input tabindex="29" type="text" class="form-control" placeholder="Identitas" name="nama_identitas[]" value="<?=$r_identitas->nama_identitas?>"  >
													<input type="hidden" name="id_identitas_tambahan[]" value="<?=$r_identitas->id?>">
											</td>
											<td>
												<div class="btn-group">
													<button class="btn btn-xs btn-danger" onclick="hapus_identitas_id(this,<?=$r_identitas->id?>)" type="button" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-trash"></i></button>
												</div>
											</td>
										</tr>
										
										<?}?>
										</tbody>
									</table>
									
								</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3 text-right">
					<button class="btn btn-success" type="submit" name="btn_simpan" value="2">Simpan</button>
					<a href="{base_url}mppa" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
		<?php echo form_close() ?>
	</div>
</div>

<?$this->load->view('Mppa/modal_pegawai')?>

<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{js_path}pages/base_forms_wizard.js"></script>
<script type="text/javascript">
var sumber_depan;
var table;

$(document).ready(function(){	
	$(".js_select2").select2("");
	if($("#id").val()==''){
		show_modal();
		
	}else{
		generate_tanggal_lahir();
	}
	// load_kontak($("#id").val());
})	
$("#form1").on("submit", function(){
	if($("#form1").valid()){
	//loader
	 $("#cover-spin").show();
	   $("*[disabled]").not(true).removeAttr("disabled");
	}
}); 
function show_modal(){
	$("#modal_pegawai").modal({backdrop: 'static', keyboard: false}).show();
	// $("#snamapegawai").val('Yusti Nawang Mukti');
	loadDataPegawai();
}
function pilih(tipe,id){
	$("#cover-spin").show();
	$("#tipepegawai").val(tipe).trigger('change');
	$.ajax({
		url: "{site_url}mppa/get_detail_pegwai/",
		method: "POST",
		dataType: "json",
		data:{
			tipe:tipe,
			id:id,
			
		},
		success: function(data) {
			
			st_load_manual=true;
			
			$('#status_cari').val(1);

			// Pecahan Tanggal Lahir (Hari, Bulan, Tahun)
			var tanggallahir = data.tanggallahir;
			var tahun = tanggallahir.substr(0, 4);
			var bulan = tanggallahir.substr(5, 2);
			var hari = tanggallahir.substr(8, 2);
			// alert(bulan);
			// $('#tahun').val(tahun);
			$("#tahun").val(tahun).trigger('change');
			$("#bulan").val(bulan).trigger('change');
			$("#hari").val(hari).trigger('change');

			generate_tanggal_lahir();
			$("#jenis_kelamin").val(data.jenis_kelamin).trigger('change');
			$("#nama").val(data.nama);
			$("#nip").val(data.nip);
			$("#alamat").val(data.alamat);
			$("#tempat_lahir").val(data.tempatlahir);
			$("#pegawai_id").val(data.pegawai_id);
			
			$("#cover-spin").hide();
			$("#modal_pegawai").modal('hide')
			generate_tanggal_lahir();
			check = $("#chk_st_domisili").is(":checked");
			if (check){
				set_sama_domisili();
			}
			st_load_manual=false;
		}
	});
}
function set_sama_domisili(){
	$("#status_cari").val(1);
	$("#ckabupaten_ktp").val($("#ckabupaten").val());
	$("#ckecamatan_ktp").val($("#ckecamatan").val());
	$("#ckelurahan_ktp").val($("#ckelurahan").val());
	$("#alamat_ktp").val($("#alamat").val());
	
	// console.log('functionsama');
	$("#rw_ktp").val($("#rw").val());
	$("#rt_ktp").val($("#rt").val());
	$("#kodepos_ktp").val($("#kodepos").val());
	
}
function set_beda_domisili(){
	$("#alamat_ktp").val('');
	$("#provinsi_id_ktp").val(null).trigger('change');
	$("#kabupaten_id_ktp").empty();
	$("#kecamatan_id_ktp").empty();
	$("#kelurahan_id_ktp").empty();
	$("#kodepos_ktp").val('');
	$("#rw_ktp").val('');
	$("#rt_ktp").val('');
}
$("#chk_st_domisili").on("click", function(){
check = $("#chk_st_domisili").is(":checked");
    if(check) {
		$("#status_cari").val(1);
		set_sama_domisili();
        $("#provinsi_id_ktp").val($("#provinsi_id").val()).trigger('change');
		$("#kabupaten_id_ktp").val($("#ckabupaten_ktp").val()).trigger('change');
		$("#kecamatan_id_ktp").val($("#ckecamatan_ktp").val()).trigger('change');
		$("#kelurahan_id_ktp").val($("#ckelurahan_ktp").val()).trigger('change');
		console.log($("#kelurahan_id").val());
    } else {
        set_beda_domisili();
    }
}); 

// $("#provinsi_id").change(function() {

// });
$("#kabupaten_id").change(function() {

console.log($("#kabupaten_id_ktp").val())
});
$("#kelurahan_id").change(function() {
check = $("#chk_st_domisili").is(":checked");
    if(check) {
       $("#kelurahan_id_ktp").val($("#kelurahan_id").val()).trigger('change');
    }else{
       $("#kelurahan_id_ktp").val(null).trigger('change');
		
	}
});
$("#warganegara_id").change(function() {
check = $("#chk_st_domisili").is(":checked");
    if(check) {
       $("#warganegara_id_ktp").val($("#warganegara_id").val()).trigger('change');
    }else{
       $("#warganegara_id_ktp").val(null).trigger('change');
		
	}
});
$("#kecamatan_id").change(function() {
check = $("#chk_st_domisili").is(":checked");
    if(check) {
       $("#kecamatan_id_ktp").val($("#kecamatan_id").val()).trigger('change');
    }else{
       $("#kecamatan_id_ktp").val(null).trigger('change');
		
	}
});
$("#kelurahan_id").change(function() {
check = $("#chk_st_domisili").is(":checked");
    if(check) {
       $("#kelurahan_id_ktp").val($("#kelurahan_id").val()).trigger('change');
    }else{
       $("#kecamatan_id_ktp").val(null).trigger('change');
		
	}
});
$("#alamat,#rw,#rt").keyup(function() {
check = $("#chk_st_domisili").is(":checked");
    if(check) {
		console.log(check)
        set_sama_domisili();
    } 
});
$("#alamat,#rw,#rt").change(function() {
check = $("#chk_st_domisili").is(":checked");
    if(check) {
        set_sama_domisili();
    } 
});
$("#provinsi_id").change(function() {
	// $("#kabupaten").css("display", "block");
	var kodeprovinsi = $("#provinsi_id").val();
	$.ajax({
		url: '{site_url}tpoliklinik_pendaftaran/getKabupaten',
		method: "POST",
		dataType: "json",
		data: {
			"kodeprovinsi": kodeprovinsi
		},
		success: function(data) {
			$("#kabupaten_id").empty();
			$("#kabupaten_id").append("<option value=''>Pilih Opsi</option>");
			for (var i = 0; i < data.length; i++) {
					$("#kabupaten_id").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
			}

			$("#kabupaten_id").selectpicker("refresh");
			if ($("#status_cari").val()=='1'){
				$('#kabupaten_id').val($("#ckabupaten").val()).trigger('change');
			}
		},
		error: function(data) {
			console.log(data);
		}
	});
	check = $("#chk_st_domisili").is(":checked");
    if(check) {
        $("#provinsi_id_ktp").val($("#provinsi_id").val()).trigger('change');
    } else{
        $("#provinsi_id_ktp").val(null).trigger('change');
		
	}
});

$("#kabupaten_id").change(function() {
	$("#kecamatan").css("display", "block");
	var kodekab = $("#kabupaten_id").val();
	check = $("#chk_st_domisili").is(":checked");
    if(check) {
       $("#kabupaten_id_ktp").val($("#kabupaten_id").val()).trigger('change');
    }else{
       $("#kabupaten_id_ktp").val(null).trigger('change');
		
	}
	$.ajax({
		url: '{site_url}tpoliklinik_pendaftaran/getKecamatan',
		method: "POST",
		dataType: "json",
		data: {
			"kodekab": kodekab
		},
		success: function(data) {
			$("#kecamatan_id").empty();
			$("#kecamatan_id").append("<option value=''>Pilih Opsi</option>");
			for (var i = 0; i < data.length; i++) {
				$("#kecamatan_id").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
			}
			$("#kecamatan_id").selectpicker("refresh");
			if ($("#status_cari").val()=='1'){
				$('#kecamatan_id').val($("#ckecamatan").val()).trigger('change');
			}
		},
		error: function(data) {
			alert(data);
		}
	});
});
$("#kabupaten_id_ktp").change(function() {
	var kodekab = $("#kabupaten_id_ktp").val();
	$.ajax({
		url: '{site_url}tpoliklinik_pendaftaran/getKecamatan',
		method: "POST",
		dataType: "json",
		data: {
			"kodekab": kodekab
		},
		success: function(data) {
			$("#kecamatan_id_ktp").empty();
			$("#kecamatan_id_ktp").append("<option value=''>Pilih Opsi</option>");
			for (var i = 0; i < data.length; i++) {
				$("#kecamatan_id_ktp").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
			}
			$("#kecamatan_id_ktp").selectpicker("refresh");
			if ($("#status_cari").val()=='1'){
				$('#kecamatan_id_ktp').val($("#ckecamatan_ktp").val()).trigger('change');
			}
		},
		error: function(data) {
			alert(data);
		}
	});
});

$("#kecamatan_id").change(function() {
	$("#kelurahan").css("display", "block");
	var kodekec = $("#kecamatan_id").val();
	$.ajax({
		url: '{site_url}tpoliklinik_pendaftaran/getKelurahan',
		method: "POST",
		dataType: "json",
		data: {
			"kodekec": kodekec
		},
		success: function(data) {
			$("#kelurahan_id").empty();
			$("#kelurahan_id").append("<option value=''>Pilih Opsi</option>");
			for (var i = 0; i < data.length; i++) {
				$("#kelurahan_id").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
			}
			$("#kelurahan_id").selectpicker("refresh");
			if ($("#status_cari").val()=='1'){
				$('#kelurahan_id').val($("#ckelurahan").val()).trigger('change');
			}
		},
		error: function(data) {
			alert(data);
		}
	});
});
$("#kecamatan_id_ktp").change(function() {
	
	var kodekec = $("#kecamatan_id_ktp").val();
	$.ajax({
		url: '{site_url}tpoliklinik_pendaftaran/getKelurahan',
		method: "POST",
		dataType: "json",
		data: {
			"kodekec": kodekec
		},
		success: function(data) {
			$("#kelurahan_id_ktp").empty();
			$("#kelurahan_id_ktp").append("<option value=''>Pilih Opsi</option>");
			for (var i = 0; i < data.length; i++) {
				$("#kelurahan_id_ktp").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
			}
			$("#kelurahan_id_ktp").selectpicker("refresh");
			if ($("#status_cari").val()=='1'){
				$('#kelurahan_id_ktp').val($("#ckelurahan_ktp").val()).trigger('change');
			}
		},
		error: function(data) {
			alert(data);
		}
	});
});

$("#kelurahan_id").change(function() {
	console.log('cari keluarahan');
	var kelurahan = $("#kelurahan_id").val();
	$.ajax({
		url: '{site_url}tpoliklinik_pendaftaran/getKodepos',
		method: "POST",
		dataType: "json",
		data: {
			"kodekel": kelurahan
		},
		success: function(data) {
			$("#kodepos").val('');
			if (data.length > 0) {
				$("#kodepos").val(data[0].kodepos);
			}
		}
	});
});

$("#provinsi_id_ktp").change(function() {
	var kodeprovinsi = $("#provinsi_id_ktp").val();
	$.ajax({
		url: '{site_url}tpoliklinik_pendaftaran/getKabupaten',
		method: "POST",
		dataType: "json",
		data: {
			"kodeprovinsi": kodeprovinsi
		},
		success: function(data) {
			$("#kabupaten_id_ktp").empty();
			$("#kabupaten_id_ktp").append("<option value=''>Pilih Opsi</option>");
			for (var i = 0; i < data.length; i++) {
					$("#kabupaten_id_ktp").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
			}

			$("#kabupaten_id_ktp").selectpicker("refresh");
			if ($("#status_cari").val()=='1'){
				$('#kabupaten_id_ktp').val($("#ckabupaten_ktp").val()).trigger('change');
			}
		},
		error: function(data) {
			console.log(data);
		}
	});
});


$("#kelurahan_id_ktp").change(function() {
	console.log('cari keluarahan');
	var kelurahan = $("#kelurahan_id_ktp").val();
	$.ajax({
		url: '{site_url}tpoliklinik_pendaftaran/getKodepos',
		method: "POST",
		dataType: "json",
		data: {
			"kodekel": kelurahan
		},
		success: function(data) {
			$("#kodepos_ktp").val('');
			if (data.length > 0) {
				$("#kodepos_ktp").val(data[0].kodepos);
			}
		}
	});
});
function generate_tanggal_lahir(){
// $("#hari").change(function() {
	var hari = $("#hari").val();
	var bulan = $("#bulan").val();
	var tahun = $("#tahun").val();
	var tanggal = tahun + "-" + bulan + "-" + hari;
	$.ajax({
		url: '{site_url}tpoliklinik_pendaftaran/getDate',
		method: "POST",
		dataType: "json",
		data: {
			"hari": hari,
			"bulan": bulan,
			"tahun": tahun,
			"tanggal": tanggal
		},
		success: function(data) {
			if (tahun == "0" && bulan == "0") {
				$("#umur_tahun").val("0");
				$("#umur_bulan").val("0");
				$("#umur_hari").val(hari);
			} else {
				$("#umur_tahun").val(data.date[0].tahun).trigger('change');
				$("#umur_bulan").val(data.date[0].bulan).trigger('change');
				$("#umur_hari").val(data.date[0].hari).trigger('change');
			}

			
		}
	});
		// });
}
function tambah_kontak(){
	var nextKontak = $(".copied").clone();
        // add corresponding classes (remove old first):
        nextKontak.removeClass('copied').addClass('new_kontak');
		console.log(nextKontak);
		nextKontak.find('select.div_select2').removeClass('div_select2').addClass('js_select2');
		// console.log(xselect2);		
		$('#pasted tbody').append(nextKontak);
		$(".js_select2").select2("");
	
}
function load_kontak(idpasien){
	$('#pasted tbody').empty();;
	$("#cover-spin").show();
	if (idpasien){
		$.ajax({
			url: "{site_url}mppa/load_kontak/" + idpasien,
			method: "POST",
			dataType: "json",
			success: function(data) {
				$('#pasted tbody').append(data);
				$(".js_select2").select2("");
				$("#cover-spin").hide();
			}
		});
		
	}
	
}

function hapus_kontak(row){
	 row.closest("tr").remove();
}
function hapus_kontak_id(row,id){
	// alert(id);
	let idpasien=$("#id").val();
	$("#cover-spin").show();
	 $.ajax({
		url: "{site_url}mppa/hapus_kontak/",
		method: "POST",
		dataType: "json",
		data:{id:id},
		success: function(data) {
			// row.closest("tr").remove();
			// alert('sini');
			load_kontak(idpasien);
			$("#cover-spin").hide();
		}
	});
}
$("#hari,#bulan,#tahun").change(function() {
		generate_tanggal_lahir();
});	
function tambah_identitas(){
	var nextIdentitas = $(".copied2").clone();
        // add corresponding classes (remove old first):
        nextIdentitas.removeClass('copied2').addClass('new_identitas');
		console.log(nextIdentitas);
		nextIdentitas.find('select.div_select2').removeClass('div_select2').addClass('js_select2');
		// console.log(xselect2);		
		$('#pasted2 tbody').append(nextIdentitas);
		$(".js_select2").select2("");
	
}
function load_identitas(idpasien){
	$('#pasted2 tbody').empty();;
	$("#cover-spin").show();
	if (idpasien){
		$.ajax({
			url: "{site_url}mppa/load_identitas/" + idpasien,
			method: "POST",
			dataType: "json",
			success: function(data) {
				$('#pasted2 tbody').append(data);
				$(".js_select2").select2("");
				$("#cover-spin").hide();
			}
		});
		
	}
	
}

function hapus_identitas(row){
	 row.closest("tr").remove();
}
function hapus_identitas_id(row,id){
	// alert(id);
	let idpasien=$("#id").val();
	$("#cover-spin").show();
	 $.ajax({
		url: "{site_url}mppa/hapus_identitas/",
		method: "POST",
		dataType: "json",
		data:{id:id},
		success: function(data) {
			// row.closest("tr").remove();
			// alert('sini');
			load_identitas(idpasien);
			$("#cover-spin").hide();
		}
	});
}
</script>