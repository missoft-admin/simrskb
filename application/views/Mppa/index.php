<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('1590'))): ?>
			<ul class="block-options">
		        <li>
		            <a href="{base_url}mppa/add" title="Tambah PPA" class="btn"><i class="fa fa-plus"></i> Tambah</a>
		        </li>
		    </ul>
		<?php endif ?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<?php if (UserAccesForm($user_acces_form,array('1589'))): ?>
		<hr style="margin-top:0px">
		<?php echo form_open('mppa/filter','class="form-horizontal" id="form-work"') ?>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-3" for="tipepegawai">Tipe</label>
					<div class="col-md-9">
						<select id="tipepegawai" name="tipepegawai" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#" <?=($tipepegawai == 0 ? 'selected="selected"':'')?>>- All -</option>
							<option value="1" <?=($tipepegawai == 1 ? 'selected="selected"':'')?>>Pegawai</option>
							<option value="2" <?=($tipepegawai == 2 ? 'selected="selected"':'')?>>Dokter</option>
							
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-3" for="nama">Nama</label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-3" for="jenis_profesi_id">Profesi</label>
					<div class="col-md-9">
						<select id="jenis_profesi_id" name="jenis_profesi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#" selected>- All -</option>
							<?foreach(list_variable_ref(21) as $row){?>
								<option value="<?=$row->id?>" ><?=$row->nama?></option>
								<?}?>
								
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-3 control-label" for="staktif">Status</label>
					<div class="col-md-9">
						<select name="staktif" id="staktif" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#" >- All -</option>
							<option value="1" >Aktif</option>
							<option value="0" >Tidak Aktif</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-3 control-label" for="nip">NIP</label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="nip" placeholder="NIP" name="nip" value="">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 15px;">
					<label class="col-md-3 control-label" for="spesialisasi_id">Spesialisasi</label>
					<div class="col-md-9">
						<select name="spesialisasi_id" id="spesialisasi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#" selected>- All -</option>
							<?foreach(list_variable_ref(22) as $row){?>
								<option value="<?=$row->id?>" ><?=$row->nama?></option>
								<?}?>
								
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-3 control-label" for="btn_filter"></label>
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-12">
								<button class="btn btn-success text-uppercase btn-block" type="button" id="btn_filter" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<?php echo form_close() ?>
		<hr>
		<?php endif ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-bordered table-striped" id="index_all" style="width:100%">
						<thead>
							<tr>
								<th>No</th>
								<th>Tipe</th>
								<th>NIP</th>
								<th>Nama</th>
								<th>Jenis Propesi</th>
								<th>Jensi Spesialisasi</th>
								<th>User</th>
								<th>Status</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<?$this->load->view('Mppa/modal_add_user')?>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var tabel;
$(document).ready(function(){	
	get_index();
})	
$("#btn_filter").click(function(){
	
	get_index();
});
function get_index() {
	let tipepegawai=$("#tipepegawai").val();
	let nama=$("#nama").val();
	let nip=$("#nip").val();
	let jenis_profesi_id=$("#jenis_profesi_id").val();
	let spesialisasi_id=$("#spesialisasi_id").val();
	let staktif=$("#staktif").val();
	$('#index_all').DataTable().destroy();
		var tabel = $('#index_all').DataTable({
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			 "scrollX": false,
			"order": [],
			"ajax": {
				url: '{site_url}mppa/getIndex_all',
				type: "POST",
				dataType: 'json',
				data: {
					tipepegawai: tipepegawai,
					nama: nama,
					nip: nip,
					jenis_profesi_id: jenis_profesi_id,
					spesialisasi_id: spesialisasi_id,
					staktif: staktif,
				}
			},
			columnDefs: [
			{  className: "text-right", targets:[0] },
			{  className: "text-center", targets:[1,6,7] },
			 { "width": "5%", "targets": [0] },
			 { "width": "10%", "targets": [1,2,4,5,7] },
			 { "width": "12%", "targets": [6,8] },
			 { "width": "20%", "targets": [3] }
		]

		});
		
}
function myDelete(id){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus PPA?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
				$("#cover-spin").show();
				 $.ajax({
					url: "{site_url}mppa/hapus_ppa/",
					method: "POST",
					dataType: "json",
					data:{id:id},
					success: function(data) {
						get_index();
						$("#cover-spin").hide();
					}
				});
		});

}
function setting_user(id){
	$("#mppa_id").val(id);
	$('#modal_user').modal('show');
	$('[href="#tab_list_user"]').tab('show');
	LoadUser();
}
</script>
