<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-pills">
		<li id="div_1" class="<?=($tab=='1'?'active':'')?>">
			<a href="javascript:void(0)" onclick="set_tab(1)"><i class="si si-list"></i> Semua</a>
		</li>
		<li  id="div_2" class="<?=($tab=='2'?'active':'')?>">
			<a href="javascript:void(0)"  onclick="set_tab(2)"><i class="si si-pin"></i> Telah Ditindak</a>
		</li>
		<li  id="div_3" class="<?=($tab=='3'?'active':'')?>">
			<a href="javascript:void(0)" onclick="set_tab(3)"><i class="fa fa-check-square-o"></i> Telah Ditransaksikan</a>
		</li>
		<li  id="div_4" class="<?=($tab=='4'?'active':'')?>">
			<a href="javascript:void(0)"  onclick="set_tab(4)"><i class="fa fa-times"></i> Batal</a>
		</li>
	</ul>
	<div class="block-content">
			<input type="hidden" id="tab" name="tab" value="{tab}">
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row pull-10">
				<?php if (UserAccesForm($user_acces_form,array('1579'))): ?>
				<div class="col-xs-6">
					
				</div>
				<div class="col-xs-6 text-right">
					<a class="btn btn-xs btn-danger  push-15 " href="{base_url}tpendaftaran_poli/add_daftar" ><i class="fa fa-plus"></i> Tambah Pendaftaran</a>
					
				</div>
				<?endif;?>
				<div class="col-md-6">
					
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Jenis Reservasi</label>
						<div class="col-md-9">
							<select id="reservasi_cara" name="reservasi_cara" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Reservasi -</option>
								<option value="0" >Tanpa Reservasi</option>
								<?foreach($list_reservasi_cara as $r){?>
								<option value="<?=$r['id']?>"><?=$r['nama']?></option>
								<?}?>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Status Transaksi</label>
						<div class="col-md-9">
							<select id="status_transaksi" name="status_transaksi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" selected>- Semua Status -</option>
								<option value="2">TELAH DITINDAK</option>
								<option value="3">TELAH DITRANSAKSIKAN</option>
								<option value="4">DIBATALKAN</option>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Status Pasien</label>
						<div class="col-md-9">
							<select id="statuspasienbaru" name="statuspasienbaru" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Status -</option>
								<option value="1">BARU</option>
								<option value="0">LAMA</option>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Status Pendaftaran</label>
						<div class="col-md-9">
							<select id="status_reservasi" name="status_reservasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Status -</option>
								<option value="2">SUDAH DIPROSES</option>
								<option value="1">BELUM DIPROSES</option>
								<option value="0">BATAL</option>
								
							</select>
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tanggal Layanan</label>
						<div class="col-md-9">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" id="tanggal_1" name="tanggal_1" placeholder="From" value="{tanggal_1}"/>
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" id="tanggal_2" name="tanggal_2" placeholder="To" value="{tanggal_2}"/>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">No Antrian</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="kode_booking" placeholder="No Antrian" name="kode_booking" value="">
						</div>
					</div>
					
				</div>
				<div class="col-md-6 push-10">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">No Medrec</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="no_medrec" placeholder="No. Medrec" name="no_medrec" value="{no_medrec}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Nama Pasien</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="namapasien" placeholder="Nama Pasien" name="namapasien" value="{namapasien}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
						<div class="col-md-9">
							<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Poliklinik -</option>
								<?foreach($list_poli as $r){?>
								<option value="<?=$r->id?>"><?=$r->nama?></option>
								<?}?>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tujuan Dokter</label>
						<div class="col-md-9">
							<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Dokter -</option>
								<?foreach($list_dokter as $r){?>
								<option value="<?=$r->id?>"><?=$r->nama?></option>
								<?}?>
								
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tanggal Reservasi</label>
						<div class="col-md-9">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" id="waktu_reservasi_1" name="waktu_reservasi_1" placeholder="From" value="{waktu_reservasi_1}"/>
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" id="waktu_reservasi_2" name="waktu_reservasi_2" placeholder="To" value="{waktu_reservasi_2}"/>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label" for="btn_filter_all"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="button" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						</div>
					</div>
						
				</div>
			</div>
			<?php echo form_close() ?>
			<div class="row   ">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="index_all">
							<thead>
								<tr>
									<th width="10%">No</th>
									<th width="10%">No Pendaftaran</th>
									<th width="10%">Tanggal Reservasi</th>
									<th width="10%">Tipe</th>
									<th width="10%">Jenis Reservasi</th>
									<th width="10%">Status Pasien</th>
									<th width="10%">Antrian</th>
									<th width="10%">Pasien</th>
									<th width="10%">Tujuan</th>
									<th width="10%">Tanggal Kunjungan</th>
									<th width="10%">Kelompok Pasien</th>
									<th width="10%">Status Reservasi</th>
									<th width="15%">Action</th>
								   
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		
	
	</div>
</div>
<!-- Modal Action Batal Pendaftaran -->
<div class="modal in" id="alasan-batal-modal" role="dialog" style="text-transform:uppercase;">
    <div class="modal-dialog modal-xs modal-dialog-popout">
        <div class="modal-content">

            <input type="hidden" name="nomedrecdet" id="nomedrecdet" value="">
            <div class="block block-themed remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Alasan Batal</h3>
                </div>
            </div>
            <div class="col-xs-12" style="margin-top: 10px; margin-bottom: 10px;">
                <select name="idalasan" id="idalasan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
                    <option value="#" selected>- Alasan -</option>
                    <?php foreach ($list_alasan as $row) { ?>
                    <option value="<?=$row->id?>" <?=($idalasan == $row->id ? 'selected' : '')?>><?=$row->keterangan?></option>
                    <?php } ?>
                </select>
            </div>


            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                <button class="btn btn-sm btn-success" id="alasanbatal" type="submit" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
            </div>

        </div>
    </div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
var tab;
$(document).ready(function(){	
	tab=$("#tab").val();
	// $("#div_1").classlist.remove("active");
	set_tab(tab);
	
})
function set_tab($tab){
	tab=$tab;
	// alert(tab);
	$("#cover-spin").show();
	document.getElementById("div_1").classList.remove("active");
	document.getElementById("div_2").classList.remove("active");
	document.getElementById("div_3").classList.remove("active");
	document.getElementById("div_4").classList.remove("active");
		$("#status_transaksi").removeAttr("disabled");
		$("#status_reservasi").removeAttr("disabled");
	if (tab=='1'){
		document.getElementById("div_1").classList.add("active");
		$("#status_transaksi").val('1').trigger('change');
		$("#status_transaksi").attr('disabled', 'disabled');
	}
	if (tab=='2'){
		document.getElementById("div_2").classList.add("active");
		$("#status_transaksi").val(2).trigger('change');
		// $("#status_transaksi").removeAttr("disabled");
		$("#status_transaksi").attr('disabled', 'disabled');
	}
	if (tab=='3'){
		$("#status_transaksi").val('3').trigger('change');
		document.getElementById("div_3").classList.add("active");
		$("#status_transaksi").attr('disabled', 'disabled');
	}
	if (tab=='4'){
		document.getElementById("div_4").classList.add("active");
		$("#status_transaksi").val('4').trigger('change');
		// $("#status_reservasi").val('0').trigger('change');
		$("#status_transaksi").attr('disabled', 'disabled');
	}
	load_index_all();
}
$(document).on("click","#btn_filter_all",function(){
	load_index_all();
});

function load_index_all(){
	$('#index_all').DataTable().destroy();	
	let reservasi_cara=$("#reservasi_cara").val();
	let statuspasienbaru=$("#statuspasienbaru").val();
	let status_reservasi=$("#status_reservasi").val();
	let tanggal_1=$("#tanggal_1").val();
	let tanggal_2=$("#tanggal_2").val();
	let no_medrec=$("#no_medrec").val();
	let namapasien=$("#namapasien").val();
	let idpoli=$("#idpoli").val();
	let iddokter=$("#iddokter").val();
	let waktu_reservasi_1=$("#waktu_reservasi_1").val();
	let waktu_reservasi_2=$("#waktu_reservasi_2").val();
	let status_transaksi=$("#status_transaksi").val();
	let kode_booking=$("#kode_booking").val();
	let notransaksi=$("#notransaksi").val();
	
	// alert(tab);
	table = $('#index_all').DataTable({
            autoWidth: false,
            searching: false,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "3%", "targets": 0,  className: "text-right" },
					{ "width": "8%", "targets": [1,2,3,6,9],  className: "text-center" },
					{ "width": "12%", "targets": [8,7,11,10],  className: "text-center" },
					{ "width": "15%", "targets": 12,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}tpendaftaran_poli/getIndex_all', 
                type: "POST" ,
                dataType: 'json',
				data : {
						reservasi_cara:reservasi_cara,
						statuspasienbaru:statuspasienbaru,
						status_reservasi:status_reservasi,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						no_medrec:no_medrec,
						namapasien:namapasien,
						idpoli:idpoli,
						iddokter:iddokter,
						waktu_reservasi_1:waktu_reservasi_1,
						waktu_reservasi_2:waktu_reservasi_2,
						status_transaksi:status_transaksi,
						kode_booking:kode_booking,
						notransaksi:notransaksi,
						tab:tab,
						
					   }
            }
        });
	$("#cover-spin").hide();
}
function myDelete($id) {
    $('#nomedrecdet').val($id);
    $("#alasan-batal-modal").modal();
}
 $('#alasanbatal').click(function() {
        var idalasan = $("#idalasan").val();
        var idpendaftaran = $("#nomedrecdet").val();
        $.ajax({
			url: '{site_url}tpendaftaran_poli/delete/'+idpendaftaran, 
            // url: "<?= base_url('tpoliklinik_pendaftaran/delete/')?>" + idpendaftaran,
            method: "POST",
            data: {
                idalasan: idalasan,
                idpendaftaran: idpendaftaran
            },
            success: function(data) {
                load_index_all();
            }
        });
    });
</script>