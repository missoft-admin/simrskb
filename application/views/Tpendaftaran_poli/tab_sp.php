<?if($st_sp=='1'){?>
		<div class="row">
			<div class="col-md-12">
				
				<table class="table table-bordered table-striped"  id="tabel_sp">
					<thead>
						<tr>
							<td width="50%" class="text-center text-bold text-italic text-header">{judul_sp}</td>
							<td  width="50%" class="text-center text-bold text-italic text-header">Opsi Jawaban</td>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
				
			</div>
			<div class="col-md-12">
				
			</div>
		</div>
		<div class="row">
			<div class="modal-footer">
				<?if ($st_skrining=='0'){?>
					<?if ($st_sc=='1' && $st_covid=='0'){?>
						<button class="btn btn-sm btn-primary btn_sp" name="btn_simpan" value="5" type="submit" ><i class="fa fa-arrow-right"></i> Lanjut Skrining Covid</button>
					<?}else{?>
						
						<button class="btn btn-sm btn-success btn_sp" name="btn_simpan" value="51" type="submit" ><i class="fa fa-arrow-right"></i> Selesai</button>
						
					<?}?>
				<?}else{?>
					<a href="{base_url}tpendaftaran_poli" class="btn btn-sm btn-default" name="btn_simpan" value="2" type="submit" ><i class="fa fa-reply"></i> Kembali</a>
				<?}?>
			</div>
		</div>
	
	<?}?>