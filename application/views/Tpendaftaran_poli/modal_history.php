<!-- Modal Cari Pasien -->
<div class="modal in" id="modal_history" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:70%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">History Kunjungan Pasien</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="tmp_medrec">No. Medrec</label>
									<div class="col-md-8">
										<input type="text" class="form-control" readonly id="tmp_modal_medrec" value="">
									</div>
								</div>
								
								
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="tmp_nama_pasien">Nama Pasien</label>
									<div class="col-md-8">
										<input type="text" class="form-control" readonly id="tmp_modal_nama_pasien" value="">
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<table width="100%" class="table table-bordered table-striped" id="index_history">
						<thead>
							<tr>
								<th>No</th>
								<th>Tanggal</th>
								<th>Tujuan</th>
								<th>Klinik  / Kelas</th>
								<th>Dokter</th>
								<th>User Daftar</th>
								<th>Status</th>
								<th>Tindak Lanjut</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function show_history_kunjungan(){
	$("#modal_history").modal('show');
	loadDataPasienHistory();
}

function loadDataPasienHistory() {
	let idpasien=$("#idpasien").val();
	// alert(idpasien)
	$.ajax({
			url: '{site_url}tpendaftaran_poli/get_pasien/'+idpasien,
			method: "POST",
			dataType: "json",
			data: {
				
			},
			success: function(data) {
				$("#tmp_modal_nama_pasien").val(data.nama);
				$("#tmp_modal_medrec").val(data.no_medrec);
			}
		});
		 $('#index_history').DataTable().destroy();
	table_2 = $('#index_history').DataTable({
            autoWidth: false,
            searching: false,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			// "columnDefs": [
					// { "width": "3%", "targets": 0,  className: "text-right" },
					// { "width": "8%", "targets": [1,2,3,6,7,8,9],  className: "text-center" },
					// { "width": "12%", "targets": [4,10],  className: "text-center" },
					// { "width": "15%", "targets": 5,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}tpendaftaran_poli/loadDataPasienHistory', 
                type: "POST" ,
                dataType: 'json',
				data : {
						idpasien:idpasien,
						
					   }
            }
        });
	// $('#index_history').DataTable().destroy();
		// var tableHis = $('#index_history').DataTable({
			// "pageLength": 8,
			// "ordering": true,
			// "processing": true,
			// "serverSide": true,
			// "order": [],
			// "ajax": {
				// url: '{site_url}tpendaftaran_poli/loadDataPasienHistory',
				// type: "POST",
				// dataType: 'json',
				// data: {
					// idpasien: idpasien,
				// }
			// },
			// // "columnDefs": [{
				// // "targets": [0],
				// // "orderable": true
			// // }]
		// });
		
	}
</script>
<!-- End Modal Cari Pasien -->