<div class="row">
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label text-success"><h5 align="left"><b></b></h5></label>
			<div class="col-md-5">
				
			</div>
			
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 col-xs-12 push-5 control-label" for="tglpendaftaran">Tanggal Pendaftaran<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-6 col-xs-6 push-5">
						<input tabindex="2" type="text" readonly class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="tglpendaftaran" placeholder="HH/BB/TTTT" name="tglpendaftaran" value="<?= $tanggaldaftar ?>" required>
					</div>
					<div class="col-md-6 col-xs-6 push-5">
						<input tabindex="3" type="text" readonly class="time-datepicker form-control" id="waktupendaftaran" name="waktupendaftaran" value="<?= $waktudaftar ?>" required>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="antrian_id">No. Antrian <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select tabindex="34"  name="antrian_id" id="antrian_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="#" selected>TANPA NO ANTRIAN</option>
					<?php foreach ($list_antrian as $r):?>
					<option value="<?= $r->id; ?>" <?=($r->id==$antrian_id?'selected':'')?>><?= $r->kodeantrian.' - '.HumanDateShort($r->tanggal); ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="tipepoli">Tipe <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select tabindex="1" name="idtipe" id="tipepoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="1" <?=($idtipe == 1 ? 'selected="selected"' : '')?>>Poliklinik</option>
					<option value="2" <?=($idtipe == 2 ? 'selected="selected"' : '')?>>Instalasi Gawat Darurat</option>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="statuspasienbaru">Status Pasien <?=$statuspasienbaru?><span style="color:red;">*</span></label>
			<div class="col-md-4">
					<select tabindex="4" id="statuspasienbaru" <?=($id!=''?'disabled':'')?> name="statuspasienbaru" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($statuspasienbaru == 1 ? 'selected="selected"' : '')?>>PASIEN BARU</option>
						<option value="0" <?=($statuspasienbaru == 0 ? 'selected="selected"' : '')?>>PASIEN LAMA</option>
					</select>
			</div>
			<div class="col-md-4" id="div_block_tidak_dikenal">
				<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
					<input type="checkbox" id="chk_st_tidakdikenal" name="chk_st_tidakdikenal" <?=($chk_st_tidakdikenal?'checked':'')?>><span></span><strong> Pasien Tidak Dikenal</strong>
				</label>
			</div>
		</div>
		
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="tglpendaftaran">No Medrec<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<div class="input-group">
					<input readonly class="form-control" id="no_medrec" placeholder="NO. MEDREC" name="no_medrec" value="{no_medrec}" required>
					<div id="div_medrec">
					<select tabindex="4" id="tmp_medrec" name="tmp_medrec" class="form-control" style="width: 100%;" data-placeholder="Cari No Medrec Pasien">
					</select>
					</div>
					<span class="input-group-btn">
						<button class="btn btn-success" data-toggle="modal" data-target="#cari-pasien-modal"type="button" id="btn_cari_pasien"><i class="fa fa-search"></i> Cari Pasien</button>
						
					</span>
				</div>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px; padding-bottom:0px;">
			<label class="col-md-4 col-xs-12 push-5 control-label" for="nama">Nama<span style="color:red;">*</span></label>
			<div class="col-md-3 col-xs-4 push-5" style="padding-right: 10px;">
				<select tabindex="5" id="titlepasien" name="title" class="form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi">
					<?php foreach (get_all('mpasien_title') as $r):?>
						<option value="<?= $r->singkatan; ?>" <?=($title === $r->singkatan ? 'selected="selected"' : '')?>><?= $r->singkatan; ?></option>
					<?php endforeach; ?>
					</select>
			</div>
			<div class="col-md-5 col-xs-8 push-5" style="padding-left: 0px;">
				<input tabindex="6" type="text" class="form-control pas_tidak_dikenal" id="nama" placeholder="Nama Lengkap" name="nama" value="{nama}" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="nik">NIK <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<input tabindex="6" type="text" class="form-control number pas_tidak_dikenal" onkeyup="this.value=this.value.replace(/[^\d]/,'')" id="nik" placeholder="NIK" name="nik" value="{nik}" minlength="16" maxlength="16" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="ibu_kandung">Nama Ibu Kandung <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<input tabindex="6" type="text" class="form-control pas_tidak_dikenal" id="nama_ibu_kandung" placeholder="Nama Ibu Kandung" name="nama_ibu_kandung" value="{nama_ibu_kandung}" required>
			</div>
		</div>
		
		

		<input type="hidden" id="idpasien" name="idpasien" value="{idpasien}">
		

		<input type="hidden" name="created" value="<?= date(' d-M-Y H:i:s ')?>">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="jenis_kelamin">Jenis Kelamin <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select tabindex="7" id="jenis_kelamin" name="jenis_kelamin" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(1) as $row){?>
					<option value="<?=$row->id?>" <?=($jenis_kelamin == $row->id ? 'selected="selected"' : '')?> <?=(($jenis_kelamin == '' || $jenis_kelamin == '0') && $row->st_default=='1' ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="statuskawin">Status Kawin</label>
			<div class="col-md-8">
				<select tabindex="8" id="statuskawin" name="statuskawin" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(7) as $row){?>
					<option value="<?=$row->id?>" <?=(($statuskawin == '' || $statuskawin == '0') && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($statuskawin == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
					
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="tempat_lahir">Tempat Lahir</label>
			<div class="col-md-8">
				<input tabindex="9" type="text" class="form-control pas_tidak_dikenal" id="tempat_lahir" placeholder="Tempat Lahir" name="tempat_lahir" value="{tempat_lahir}" required>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label" for="tgllahir">Tanggal Lahir<span style="color:red;">*</span></label>
			<div class="col-md-3  col-xs-12 push-5">
				<select tabindex="10" name="tgl_lahir" id="hari" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="0">Hari</option>
					<?php for ($i = 1; $i < 32; $i++) {?>
						<?php $hari = str_pad($i, 2, '0', STR_PAD_LEFT); ?>
						<option value="<?= $hari; ?>" <?=($tgl_hari == $hari ? 'selected="selected"' : '')?>><?= $hari; ?></option>
					<?php }?>
				</select>
			</div>
			<div class="col-md-2  col-xs-12 push-5">
				<select tabindex="11" name="bulan_lahir" id="bulan" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="00">Bulan</option>
					<option value="01" <?=($tgl_bulan == '01' ? 'selected="selected"' : '')?>>Januari</option>
					<option value="02" <?=($tgl_bulan == '02' ? 'selected="selected"' : '')?>>Februari</option>
					<option value="03" <?=($tgl_bulan == '03' ? 'selected="selected"' : '')?>>Maret</option>
					<option value="04" <?=($tgl_bulan == '04' ? 'selected="selected"' : '')?>>April</option>
					<option value="05" <?=($tgl_bulan == '05' ? 'selected="selected"' : '')?>>Mei</option>
					<option value="06" <?=($tgl_bulan == '06' ? 'selected="selected"' : '')?>>Juni</option>
					<option value="07" <?=($tgl_bulan == '07' ? 'selected="selected"' : '')?>>Juli</option>
					<option value="08" <?=($tgl_bulan == '08' ? 'selected="selected"' : '')?>>Agustus</option>
					<option value="09" <?=($tgl_bulan == '09' ? 'selected="selected"' : '')?>>September</option>
					<option value="10" <?=($tgl_bulan == '10' ? 'selected="selected"' : '')?>>Oktober</option>
					<option value="11" <?=($tgl_bulan == '11' ? 'selected="selected"' : '')?>>November</option>
					<option value="12" <?=($tgl_bulan == '12' ? 'selected="selected"' : '')?>>Desember</option>
				</select>
			</div>
			<div class="col-md-3  col-xs-12 push-5" >
				<select tabindex="12" name="tahun_lahir" id="tahun" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="0">Tahun</option>
					<?php for ($i = date('Y'); $i >= 1900; $i--) {?>
						<option value="<?= $i; ?>" <?=($tgl_tahun == $i ? 'selected="selected"' : '')?>><?= $i; ?></option>
					<?php }?>
				</select>
			</div>
		</div>
		<div class="form-group " >
			<label class="col-md-4 col-xs-12 control-label" for="umur">Umur</label>
			<div class="col-md-3 col-xs-12 push-5">
				<input type="text" class="form-control pas_tidak_dikenal" id="umur_tahun" name="umur_tahun" value="{umur_tahun}" placeholder="Tahun" readonly="true" required>
			</div>
			<div class="col-md-2 col-xs-12 push-5">
				<input type="text" class="form-control pas_tidak_dikenal" id="umur_bulan" name="umur_bulan" value="{umur_bulan}" placeholder="Bulan" readonly="true" required>
			</div>
			<div class="col-md-3 col-xs-12 push-5">
				<input type="text" class="form-control pas_tidak_dikenal" id="umur_hari" name="umur_hari" value="{umur_hari}" placeholder="Hari" readonly="true" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-12 control-label text-primary"><h5 align="left"><b>Alamat Domisili</b></h5></label>
			<div class="col-md-12">
				<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
			</div>
		</div>
		<div class="form-group" >
			<label class="col-md-4 control-label" for="alamat">Alamat<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<textarea tabindex="13" class="form-control pas_tidak_dikenal" id="alamat_jalan" placeholder="Alamat" name="alamat_jalan" required><?= $alamat_jalan?></textarea>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 0px;">
			<div class="col-md-4"></div>
			<label class="col-md-3 control-label" for="provinsi_id"><p align="left">Provinsi</p></label>
			<div class="col-md-5">
				<?php if ($id == null) {?>
				<select tabindex="14" name="provinsi_id" id="provinsi_id" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
						<option value="<?= $a->id; ?>" <?= ($a->id == '12') ? 'selected' : ''; ?>><?= $a->nama; ?></option>
					<?php endforeach; ?>
					</select>
				<?php } else {?>
				<select tabindex="14" name="provinsi_id" id="provinsi_id" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
						<option value="<?= $a->id; ?>" <?=($provinsi_id == $a->id ? 'selected="selected"' : '')?>><?= $a->nama; ?></option>
					<?php endforeach; ?>
					</select>
				<?php }?>
			</div>
		</div>
		<div id="kabupaten_id" style="display: block;">
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-4"></div>
				<label class="col-md-3" for="kabupaten_id"><left>Kabupaten/Kota</left> </label>
				<div class="col-md-5">
					<?php if ($id == '') {?>
					<select tabindex="15" name="kabupaten_id" id="kabupaten1" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
							<option value="">Pilih Opsi</<option>
							<?php foreach (get_all('mfwilayah', ['parent_id' => '12']) as $r):?>
							<option value="<?= $r->id; ?>" <?= ($r->id == $kabupaten_id) ? 'selected' : ''; ?>><?= $r->nama; ?></<option>
							<?php endforeach; ?>
						</select>
					<?php } else {?>
					<select tabindex="15" name="kabupaten_id" id="kabupaten1" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</<option>
							<?php foreach (get_all('mfwilayah', ['parent_id' => $provinsi_id]) as $r):?>
							<option value="<?= $r->id; ?>" <?=($kabupaten_id == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
							<?php endforeach; ?>
						</select>
					<?php }?>
				</div>
			</div>
		</div>
		<div id="kecamatan" style="display: block;">
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label" for="kecamatan_id" style="text-align:left;">Kecamatan</label>
				<div class="col-md-5">
					<?php if ($id == '') {?>
					<select tabindex="16" name="kecamatan_id" id="kecamatan1" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Kecamatan" required>
						<option value="">Pilih Opsi</<option>
						<?php foreach (get_all('mfwilayah', ['parent_id' => '213']) as $r):?>
						<option value="<?= $r->id; ?>"><?= $r->nama; ?></<option>
						<?php endforeach; ?>
					</select>
					<?php } else {?>
					<select tabindex="16" name="kecamatan_id" id="kecamatan1" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</<option>
						<?php foreach (get_all('mfwilayah', ['parent_id' => $kabupaten_id]) as $r):?>
						<option value="<?= $r->id; ?>"<?=($kecamatan_id == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
						<?php endforeach; ?>
					</select>
					<?php }?>
				</div>
			</div>
		</div>
		
		<div id="kelurahan" style="display: block;">
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label" for="kelurahan_id" style="text-align:left;">Kelurahan</label>
				<div class="col-md-5">
					<select tabindex="17" name="kelurahan_id" id="kelurahan1" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</<option>
						<?php foreach (get_all('mfwilayah', ['parent_id' => $kecamatan_id]) as $r):?>
						<option value="<?= $r->id; ?>"<?=($kelurahan_id == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label" for="kodepos" style="text-align:left;">Kode Pos</label>
				<div class="col-md-5">
					<input tabindex="18" type="text" class="form-control pas_tidak_dikenal" id="kodepos" placeholder="Kode Pos" name="kodepos" value="{kodepos}" readonly="true" required>
				</div>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 0px;">
			<div class="col-md-4"></div>
			<label class="col-md-3 control-label" for="rw"><p align="left">RW</p></label>
			<div class="col-md-5">
				
				<input type="text" class="form-control pas_tidak_dikenal" id="rw" name="rw" value="{rw}" placeholder="RW" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 0px;">
			<div class="col-md-4"></div>
			<label class="col-md-3 control-label" for="rt"><p align="left">RT</p></label>
			<div class="col-md-5">
				
				<input type="text" class="form-control pas_tidak_dikenal" id="rt" name="rt" value="{rt}" placeholder="RT" required>
			</div>
		</div>
		<hr>
		<input type="hidden" id="status_cari" name="status_cari" value="0">
		<input type="hidden" id="ckabupaten" name="ckabupaten" value="{kabupaten_id}">
		<input type="hidden" id="ckecamatan" name="ckecamatan" value="{kecamatan_id}">
		<input type="hidden" id="ckelurahan" name="ckelurahan" value="{kelurahan_id}">
		
		<input type="hidden" id="ckabupaten_ktp" name="ckabupaten_ktp" value="{kabupaten_id_ktp}">
		<input type="hidden" id="ckecamatan_ktp" name="ckecamatan_ktp" value="{kecamatan_id_ktp}">
		<input type="hidden" id="ckelurahan_ktp" name="ckelurahan_ktp" value="{kelurahan_id_ktp}">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label text-success"><h5 align="left"><b>Alamat Kartu Identitas</b></h5></label>
			<div class="col-md-5">
				<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
					<input type="checkbox" id="chk_st_domisili"  name="chk_st_domisili" <?=($chk_st_domisili?'checked':'')?>><span></span> Sama dengan Domisili
				</label>
			</div>
			<div class="col-md-12">
				<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
			</div>
		</div>
		
		<div class="form-group" >
			<label class="col-md-4 control-label" for="alamat">Alamat<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<textarea tabindex="13"   class="form-control pas_tidak_dikenal" id="alamat_jalan_ktp" placeholder="Alamat" name="alamat_jalan_ktp" required><?= $alamat_jalan_ktp?></textarea>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 0px;">
			<div class="col-md-4"></div>
			<label class="col-md-3 control-label" for="provinsi"><p align="left">Provinsi</p></label>
			<div class="col-md-5">
				
				<select tabindex="14" name="provinsi_id_ktp"   id="provinsi_id_ktp" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
						<option value="<?= $a->id; ?>" <?=($provinsi_id_ktp == $a->id ? 'selected="selected"' : '')?>><?= $a->nama; ?></option>
					<?php endforeach; ?>
					</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<div class="col-md-4"></div>
			<label class="col-md-3" for="kabupaten_id_ktp"><left>Kabupaten/Kota</left></label>
			<div class="col-md-5">
				
				<select tabindex="15" name="kabupaten_id_ktp"   id="kabupaten_id_ktp" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</<option>
						<?php foreach (get_all('mfwilayah', ['parent_id' => $provinsi_id_ktp]) as $r):?>
						<option value="<?= $r->id; ?>" <?=($kabupaten_id_ktp == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
						<?php endforeach; ?>
					</select>
			</div>
		</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label"   for="kecamatan_id_ktp" style="text-align:left;">Kecamatan</label>
				<div class="col-md-5">
					
					<select tabindex="16" name="kecamatan_id_ktp"   id="kecamatan_id_ktp" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required >
						<option value="">Pilih Opsi</<option>
						<?php foreach (get_all('mfwilayah', ['parent_id' => $kabupaten_id_ktp]) as $r):?>
						<option value="<?= $r->id; ?>"<?=($kecamatan_id_ktp == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label" for="kelurahan_id_ktp" style="text-align:left;">Kelurahan</label>
				<div class="col-md-5">
					<select tabindex="17" name="kelurahan_id_ktp"   id="kelurahan_id_ktp" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</<option>
						<?php foreach (get_all('mfwilayah', ['parent_id' => $kecamatan_id_ktp]) as $r):?>
						<option value="<?= $r->id; ?>"<?=($kelurahan_id_ktp == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label" for="kodepos_ktp" style="text-align:left;">Kode Pos</label>
				<div class="col-md-5">
					<input tabindex="18" type="text" class="form-control pas_tidak_dikenal"   id="kodepos_ktp" placeholder="Kode Pos" name="kodepos_ktp" value="{kodepos_ktp}" readonly="true" required>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 0px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label" for="rw_ktp"><p align="left">RW</p></label>
				<div class="col-md-5">
					
					<input type="text" class="form-control pas_tidak_dikenal" id="rw_ktp" name="rw_ktp" value="{rw_ktp}" placeholder="RW" required>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 0px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label pas_tidak_dikenal" for="rt_ktp"><p align="left">RT</p></label>
				<div class="col-md-5">
					
					<input type="text" class="form-control pas_tidak_dikenal" id="rt_ktp" name="rt_ktp" value="{rt_ktp}" placeholder="RT" required>
				</div>
			</div>
		
	</div>

	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="nama_panggilan">Nama Panggilan </label>
			<div class="col-md-8">
				<input tabindex="6" type="text" class="form-control pas_tidak_dikenal" id="nama_panggilan" placeholder="Nama Panggilan" name="nama_panggilan" value="{nama_panggilan}" >
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="bahasa">Bahasa Yang Dikuasai </label>
			<div class="col-md-8">
				<select tabindex="23" id="bahasa" name="bahasa[]" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Bahasa" multiple>
					
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 col-xs-12 push-5 control-label" for="noidentitas">No Identitas</label>
			<div class="col-md-3 col-xs-4 push-5" style="padding-right: 10px;">
				<select tabindex="18" id="jenis_id" name="jenis_id" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="1" <?=($jenis_id == 1 ? 'selected="selected"' : '')?>>KTP</option>
					<option value="2" <?=($jenis_id == 2 ? 'selected="selected"' : '')?>>SIM</option>
					<option value="3" <?=($jenis_id == 3 ? 'selected="selected"' : '')?>>Pasport</option>
					<option value="4" <?=($jenis_id == 4 ? 'selected="selected"' : '')?>>Kartu Pelajar/Mahasiswa</option>
				</select>
			</div>
			<div class="col-md-5 col-xs-8 push-5" style="margin-left:0px;padding-left: 0px;">
				<input tabindex="19" type="text" class="form-control pas_tidak_dikenal" id="noidentitas" placeholder="No Identitas" name="noidentitas" value="{noidentitas}" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="nohp">No Hp</label>
			<div class="col-md-8">
				<input tabindex="20" type="text" class="form-control pas_tidak_dikenal" id="nohp" placeholder="No HP" name="nohp" value="{nohp}" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="telprumah">Telepon Rumah</label>
			<div class="col-md-8">
				<input tabindex="21" type="text" class="form-control pas_tidak_dikenal" id="telprumah" placeholder="Telepon Rumah" name="telprumah" value="{telepon}" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="email">Email</label>
			<div class="col-md-8">
				<input tabindex="22" type="text" class="form-control pas_tidak_dikenal" id="email" placeholder="Email" name="email" value="{email}" required>
			</div>
		</div>
		
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="golongan_darah">Golongan Darah</label>
			<div class="col-md-8">
				<select tabindex="23" id="golongan_darah" name="golongan_darah" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(11) as $row){?>
					<option value="<?=$row->id?>" <?=(($golongan_darah == '' || $golongan_darah == '0') && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($golongan_darah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
					
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="agama">Agama</label>
			<div class="col-md-8">
				<select tabindex="24" id="agama_id" name="agama_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(3) as $row){?>
					<option value="<?=$row->id?>" <?=(($agama_id == '' || $agama_id == '0') && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($agama_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
					
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="warganegara">Kewarganegaraan <?='WN:'.$warganegara?></label>
			<div class="col-md-8">
				<select tabindex="25" id="warganegara" name="warganegara" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(18) as $row){?>
					<option value="<?=$row->id?>" <?=(($warganegara == '' || $warganegara == '0' || $warganegara == null) && $row->st_default=='1' ? 'selected="selected"' : '')?>  <?=($warganegara == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
					
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="suku">Suku</label>
			<div class="col-md-8">
				<select tabindex="26" id="suku_id" name="suku_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(4) as $row){?>
					<option value="<?=$row->id?>" <?=(($suku_id == '' || $suku_id == '0') && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($suku_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
					
				</select>
				<input tabindex="29" type="hidden" class="form-control" id="suku" placeholder="Nama Penanggung Jawab" name="suku" value="{suku}" required>
			</div>
		</div>
		
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="pendidikan">Pendidikan</label>
			<div class="col-md-8">
				<select tabindex="27" id="pendidikan" name="pendidikan" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(13) as $row){?>
					<option value="<?=$row->id?>" <?=(($pendidikan == '' || $pendidikan == '0') && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pendidikan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="pekerjaan">Pekerjaan</label>
			<div class="col-md-8">
				<select tabindex="28" id="pekerjaan" name="pekerjaan" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(6) as $row){?>
					<option value="<?=$row->id?>" <?=(($pekerjaan == '' || $pekerjaan == '0') && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($pekerjaan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		<hr>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="hubungan"></label>
			<label class="col-md-9 control-label text-primary"><h5 align="left"><b>Data Penanggung Jawab</b></h5></label>
			<div class="col-md-12">
				<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="namapenanggungjawab">Nama</label>
			<div class="col-md-8">
				<input tabindex="29" type="text" class="form-control pas_tidak_dikenal div_pj" id="namapenanggungjawab" placeholder="Nama Penanggung Jawab" name="namapenanggungjawab" value="{namapenanggungjawab}" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="hubungan">Hubungan</label>
			<div class="col-md-8">
				<select tabindex="30" id="hubungan" name="hubungan" class="js-select2 form-control pas_tidak_dikenal div_pj" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(9) as $row){?>
					<option value="<?=$row->id?>" <?=($hubungan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
				</select>
				
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="alamatpenanggungjawab">Alamat</label>
			<div class="col-md-8">
				<textarea tabindex="31" class="form-control pas_tidak_dikenal div_pj" id="alamatpenanggungjawab" placeholder="Alamat Penanggung Jawab" name="alamatpenanggungjawab"><?= $alamat_keluarga?></textarea>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="teleponpenanggungjawab">Telepon</label>
			<div class="col-md-8">
				<input tabindex="32" type="text" class="form-control pas_tidak_dikenal div_pj" id="teleponpenanggungjawab" placeholder="Telepon" name="teleponpenanggungjawab" value="{teleponpenanggungjawab}">
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 20px;">
			<label class="col-md-4 control-label" for="noidentitaspenanggung">No Identitas</label>
			<div class="col-md-8">
				<input tabindex="33" type="text" class="form-control pas_tidak_dikenal div_pj" id="noidentitaspenanggung" placeholder="No Identitas Penanggung Jawab" name="noidentitaspenanggung" value="{noidentitaspenanggungjawab}" required>
				
			</div>
		</div>
		
		<hr style="margin-top: 10px;margin-bottom: 15px;background: #000;">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4  text-success h5" for="namapengantar"><strong>Data Pengantar</strong></label>
			<div class="col-md-5">
				<select tabindex="28" id="chk_st_pengantar" name="chk_st_pengantar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<option value="0" <?=($chk_st_pengantar ==0 ? 'selected="selected"' : '')?>>Tidak Sama Dengan Penanggungjawab</option>
					<option value="1" <?=($chk_st_pengantar ==1 ? 'selected="selected"' : '')?>>Sama Penanggungjawab</option>
				</select>
				
			</div>
		</div>
		<hr style="margin-top: 10px;margin-bottom: 15px;background: #000;">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="namapengantar">Nama</label>
			<div class="col-md-8">
				<input tabindex="29" type="text" class="form-control div_data_pengantar  pas_tidak_dikenal"   id="namapengantar" placeholder="Nama Pengantar" name="namapengantar" value="{namapengantar}" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="hubungan_pengantar">Hubungan</label>
			<div class="col-md-8">
				<select tabindex="30" id="hubungan_pengantar" name="hubungan_pengantar"   class="js-select2 form-control div_data_pengantar  pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(9) as $row){?>
					<option value="<?=$row->id?>" <?=($hubungan_pengantar == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="alamatpengantar">Alamat</label>
			<div class="col-md-8">
				<textarea tabindex="31" class="form-control div_data_pengantar  pas_tidak_dikenal"   id="alamatpengantar" placeholder="Alamat Pengantar" name="alamatpengantar" required><?= $alamatpengantar?></textarea>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="telepon">Telepon</label>
			<div class="col-md-8">
				<input tabindex="32" type="text" class="form-control div_data_pengantar  pas_tidak_dikenal"   id="teleponpengantar" placeholder="Telepon" name="teleponpengantar" value="{teleponpengantar}" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 20px;">
			<label class="col-md-4 control-label" for="noidentitaspengantar">No Identitas</label>
			<div class="col-md-8">
				<input tabindex="33" type="text"   class="form-control div_data_pengantar  pas_tidak_dikenal" id="noidentitaspengantar" placeholder="No Identitas Pengantar" name="noidentitaspengantar" value="{noidentitaspengantar}" required>
				
			</div>
		</div>
		
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-8 control-label text-danger"><h5 align="left"><b>Informasi Pasien Tidak Dikenal</b></h5></label>
			<div class="col-md-4">
				
			</div>
			<div class="col-md-12">
				<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
			</div>
		</div>
		<div class="form-group form_tidak_dikenal" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="lokasi_ditemukan">Lokasi Ditemukan</label>
			<div class="col-md-8">
				<input tabindex="29" type="text" class="form-control div_tidak_dikenal"   id="lokasi_ditemukan" placeholder="Lokasi ditemukan" name="lokasi_ditemukan" value="{lokasi_ditemukan}" >
			</div>
		</div>
		<div class="form-group form_tidak_dikenal" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="hubungan_pengantar">Perkiraan Umur</label>
			<div class="col-md-8">
				<input tabindex="29" type="text" class="form-control div_tidak_dikenal"   id="perkiraan_umur" placeholder="Perkiraan Umur" name="perkiraan_umur" value="{perkiraan_umur}" >
				
			</div>
		</div>
		
		<div class="form-group form_tidak_dikenal" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="tgl_ditemukan">Tanggal Ditemukan</label>
			<div class="col-md-8">
				<div class="js-datepicker input-group date">
					<input class="js-datepicker form-control " type="text" id="tgl_ditemukan" name="tgl_ditemukan" data-date-format="dd-mm-yyyy"  value="{tgl_ditemukan}" placeholder="Tanggal ditemukan">
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-12 control-label"><h5 align="left"><b>Data Lain Lain</b></h5></label>
			<div class="col-md-12">
				<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="catatan">Catatan</label>
			<div class="col-md-8">
				<textarea tabindex="46" class="form-control" rows="4" id="catatan" placeholder="Catatan" name="catatan"><?= $catatan?></textarea>
			</div>
		</div>
		<hr>
		
	</div>
	
</div>
<div class="row">
	<div class="col-md-6">
		<div class="block block-themed">
			<div class="block-header bg-info">
				<ul class="block-options">
					
					<li>
						<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="fa fa-address-book-o"></i> KONTAK TAMBAHAN &nbsp;&nbsp;&nbsp; <button type="button" onclick="tambah_kontak()" class="btn btn-xs btn-success" title="Tambah Kontak Tambahan"><i class="fa fa-plus"></i></button></h3>
			</div>
			<div class="block-content">
					<div hidden>
						<table>
							<tr class="copied">
								<td>
									<select tabindex="23" name="level_kontak[]" class="div_select2  form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
										<option value="2" selected>Secondary</option>
										<option value="1">Primary</option>
									</select>
								</td>
								<td>
									<select tabindex="23" name="jenis_kontak[]" class="div_select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(20) as $row){?>
										<option value="<?=$row->id?>" <?=($golongan_darah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
											
								</td>
								<td>
									<input tabindex="29" type="text" class="form-control" placeholder="Kontak" name="nama_kontak[]" value=""  >
									<input type="hidden" name="id_kontak_tambahan[]" value="">
								</td>
								<td>
									<div class="btn-group">
										<button class="btn btn-xs btn-danger" onclick="hapus_kontak(this)" type="button" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-trash"></i></button>
									</div>
								</td>
							</tr>
						</table>
					</div>
					<div class="pasted">
						<table id="pasted" class="table table-bordered">
							<thead>
								<tr>
								<th class="text-center">Primary / Secondary</th>
								<th class="text-center">Jenis Kontak</th>
								<th class="text-center">Kontak</th>
								<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								
								
								<?foreach($list_kontak as $r_kontak){
							$jenis_kontak_id=$r_kontak->jenis_kontak;
							$level_kontak_id=$r_kontak->level_kontak;
							?>
							<tr>
								<td>
										<select tabindex="23" name="level_kontak[]" class="js_select2  form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
											<option value="2" <?=($level_kontak_id=='2'?'selected':'')?>>Secondary</option>
											<option value="1" <?=($level_kontak_id=='1'?'selected':'')?>>Primary</option>
										</select>
								</td>
								<td>
										<select tabindex="23" name="jenis_kontak[]" class="js_select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
											<option value="">Pilih Opsi</option>
											<?foreach(list_variable_ref(20) as $row){?>
											<option value="<?=$row->id?>" <?=($jenis_kontak_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
								</td>
								<td>
										<input tabindex="29" type="text" class="form-control" placeholder="Kontak" name="nama_kontak[]" value="<?=$r_kontak->nama_kontak?>"  >
										<input type="hidden" name="id_kontak_tambahan[]" value="<?=$r_kontak->id?>">
								</td>
								<td>
									<div class="btn-group">
										<button class="btn btn-xs btn-danger" onclick="hapus_kontak_id(this,<?=$r_kontak->id?>)" type="button" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-trash"></i></button>
									</div>
								</td>
							</tr>
							
							<?}?>
							</tbody>
						</table>
						
					</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="block block-themed">
			<div class="block-header bg-info">
				<ul class="block-options">
					
					<li>
						<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="fa fa-history"></i> Kunjungan Terakhir &nbsp;&nbsp;&nbsp;</h3>
			</div>
			<div class="block-content">
					<div class="form-group " id="div_history" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label text-primary"><h5 align="left"><b>Kunjungan Terakhir</b></h5> </label> 
						<div class="col-md-6 historykunjungan">
							<button class="btn btn-minw btn-rounded btn-noborder btn-success push-5 btn-xs" type="button" onclick="show_history_kunjungan()"><i class="fa fa-history"></i> Lihat</button>
						</div>
						
						<div class="col-md-12 historykunjungan">
								<div class="block">
									<div class="block-content">
										<ul class="list list-activity push">
											<li>
												<i class="si si-pencil text-primary"></i>
												<div class="font-w600" id="his_poli">
													Poliklinik - OPSIONAL
												</div>
												<div id="div_nama_dokter"><a href="javascript:void(0)"><i class="fa fa-user-md text-info"></i> <span  id="his_dokter">NAMA DOKTER</span></a></div>
												<div><small class="text-muted" id="his_tanggal">12-08-2023</small></div>
											</li>
										</ul>
									</div>
								</div>
						</div>
					</div>
					
			</div>
		</div>
	</div>
</div>
<?if($id!=''){?>
<div class="block-content block-content-mini block-content-full border-t">
<div class="row">
	<div class="form-group" style="margin-bottom: 10px;">
	<div class="col-md-12 text-right">
		<a href="{base_url}tpendaftaran_poli" class="btn btn-sm btn-default" name="btn_simpan" value="2" type="submit" ><i class="fa fa-reply"></i> Kembali</a>
		<button class="btn btn-sm btn-primary " type="submit" name="btn_simpan"  value="1"><i class="fa fa-save"></i> Update</button>			
		<button class="btn btn-sm btn-danger " type="submit" name="btn_simpan"  value="2"><i class="fa fa-save"></i> Update & Keluar</button>		
	</div>
	</div>
</div>
</div>
<?}?>