<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<style>
    body {
      -webkit-print-color-adjust: exact;
    }
	
	
   table {
		font-family: Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;
		 
      }
	  td {
        padding: 13px;
		 
      }
      .content td {
        padding: 10px;
		border: 0px solid #6033FF;
      }
	  .has-error2 {
		border-color: #d26a5c;
	}
	  .select2-selection {
		  border-color: green; /* example */
		}
      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 14px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-bottom-top-left {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
		border-bottom:1px solid #000 !important;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
	
    </style>
<div class="block">
<?if ($id==''){
	$nama_class="inactive";
	?>
  <div class="js-wizard-classic-validation block">

<?}else{
	$nama_class="";
}
	?>
		<!-- Step Tabs -->
		<ul class="nav nav-tabs nav-justified">
			<li class="<?=($tab=='1'?'active':'')?>">
				<a class="<?=$nama_class?>" href="#validation-classic-step1" data-toggle="tab"> <i class="si si-user-follow"></i> Pilih Pasien </a>
			</li>
			<li class="<?=($tab=='2'?'active':'')?>">
				<a class="<?=$nama_class?>" href="#validation-classic-step2" data-toggle="tab"><i class="fa fa-calendar-check-o"></i> Tentukan Pendaftaran </a>
			</li>
			<li class="<?=($tab=='3'?'active':'')?>">
				<a class="<?=$nama_class?>" href="#validation-classic-step3" data-toggle="tab"><i class="fa fa-calendar"></i>  Detail Pendaftaran</a>
			</li>
			
			<?if($st_gc=='1'){?>
			<li class="<?=($tab=='4'?'active':'')?>">
				<a class="<?=$nama_class?>"  href="#validation-classic-step4" data-toggle="tab"><i class="fa fa-check-circle"></i>  General Consent <?=($st_general?'&nbsp;&nbsp;<span class="badge badge-success pull-right"> Sudah </span>':'&nbsp;&nbsp;<span class="badge badge-danger pull-right"> Belum </span>')?></a>
			</li>
			<?}?>
			<?if($st_sp=='1' && $st_general=='1'){?>
			<li class="<?=($tab=='5'?'active':'')?>">
				<a class="<?=$nama_class?>"  href="#validation-classic-step5" data-toggle="tab"><i class="fa fa-check-square-o"></i>  Skrining Pasien <?=($st_skrining?'&nbsp;&nbsp;<span class="badge badge-success pull-right"> Sudah </span>':'&nbsp;&nbsp;<span class="badge badge-danger pull-right"> Belum </span>')?></a>
			</li>
			<?}?>
			<?if($st_sc=='1' && $st_skrining=='1'){?>
			<li class="<?=($tab=='6'?'active':'')?>" data-toggle="tab">
				<a class="<?=$nama_class?>"  href="#validation-classic-step6"  data-toggle="tab"><i class="fa fa-check-square"></i>  Skrining Covid <?=($st_covid?'&nbsp;&nbsp;<span class="badge badge-success pull-right"> Sudah </span>':'&nbsp;&nbsp;<span class="badge badge-danger pull-right"> Belum </span>')?></a>
			</li>
			<?}?>
			
		</ul>
			<!-- Steps Content -->
			<?php echo form_open_multipart('tpendaftaran_poli/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
			
			<div class="block-content tab-content">
				<!-- Step 1 -->
				<input type="hidden" id="id" name="id" value="{id}">
				<input type="hidden" id="nama_tabel" name="nama_tabel" value="">
				<div class="tab-pane push-5-t push-50 <?=($tab=='1'?'active':'')?>" id="validation-classic-step1">
					<?$this->load->view('Tpendaftaran_poli/tab_pasien')?>
					
				</div>
				<!-- END Step 1 -->

				<!-- Step 2 -->
				<div class="tab-pane push-5-t push-50 <?=($tab=='2'?'active':'')?>" id="validation-classic-step2">
					<?$this->load->view('Tpendaftaran_poli/tab_pendaftaran')?>
				</div>
				<!-- END Step 2 -->

				<!-- Step 3 -->
				<div class="tab-pane push-10-t push-50 <?=($tab=='3'?'active':'')?>" id="validation-classic-step3">					
					<?$this->load->view('Tpendaftaran_poli/tab_detail')?>
				</div>
				
				
				
				<div class="tab-pane push-10-t push-50 <?=($tab=='4'?'active':'')?>" id="validation-classic-step4">					
					<?$this->load->view('Tpendaftaran_poli/tab_gc')?>
					
				</div>
				<div class="tab-pane push-10-t push-50 <?=($tab=='5'?'active':'')?>" id="validation-classic-step5">					
					
					<?$this->load->view('Tpendaftaran_poli/tab_sp')?>
				</div>
				<div class="tab-pane push-10-t push-50 <?=($tab=='6'?'active':'')?>" id="validation-classic-step6">					
					
					<?$this->load->view('Tpendaftaran_poli/tab_sc')?>
				</div>
				
				<!-- END Step 3 -->
			</div>
			
			<!-- END Steps Content -->
			<?if ($id==''){?>
			<!-- Steps Navigation -->
			<div class="block-content block-content-mini block-content-full border-t">
				<div class="row">
					<div class="col-xs-6">
						<button class="wizard-prev btn btn-default" type="button"><i class="fa fa-arrow-left"></i> Previous</button>
					</div>
					<div class="col-xs-6 text-right">
						<button class="wizard-next btn btn-success" type="button">Next <i class="fa fa-arrow-right"></i></button>
						<button class="wizard-finish btn btn-primary" type="submit" value="1" name="btn_simpan"><i class="fa fa-check"></i> Submit</button>
					
					</div>
				</div>
			</div>
			<?}?>
		<?php echo form_close() ?>
		<!-- END Form -->
		<?if ($id==''){?>
	</div>
		<?}?>
</div>
<div class="modal in" id="modal_paraf" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Paraf Pasien</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<input type="hidden" readonly id="ttd_id" name="ttd_id" value="">
								<input type="hidden" readonly id="nama_tabel" name="nama_tabel" value="">
								<textarea id="signature64" name="signed" style="display: none"></textarea>
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" id="btn_save_paraf"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Penanggung Jawab </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig_2" ></div>
									</div>
								</div>
								<textarea id="signature64_2" name="signed_2" style="display: none"><?=$jawaban_ttd?></textarea>
							</div>
							<input type="hidden" readonly id="ttd_id_2" name="ttd_id_2" value="{id}">
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear_2"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" id="btn_save_ttd"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<?$this->load->view('Tpendaftaran_poli/modal_history')?>
<?$this->load->view('Tbooking/modal_pasien')?>
<script src="{js_path}plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{js_path}pages/base_forms_wizard.js"></script>
<?$this->load->view('Tpendaftaran_poli/modal_add_kartu')?>
<script type="text/javascript">
var opsi_provinsi;var opsi_kabupaten;var opsi_kecamatan;var opsi_kelurahan;
var sumber_depan;
var table;
var idpasien;
var st_copy;
var st_load_manual;
var st_skrining=$("#st_skrining").val();
var st_general=$("#st_general").val();
var st_covid=$("#st_covid").val();
$(document).ready(function(){	
	sumber_depan=true;
	
	st_load_manual=false;
	idpasien= $('#idpasien').val();
	// alert(idpasien)
	load_bahasa(idpasien);
	load_status_pasien();
	set_dikenal();
	// load_detail_pasien('149');
	show_hide_button();
	$("#tmp_medrec").select2({
        minimumInputLength: 6,
		noResults: 'Data Tidak ditemukan',
		// allowClear: true
        // tags: [],
        ajax: {
            url: '{site_url}tbooking/select2_pasien/',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
		 data: function (params) {
              var query = {
                search: params.term,
              }
              return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.no_medrec +' - '+item.nama,
                            id: item.id,
							// idtipe : item.idtipe
                        }
                    })
                };
            }
        }
    });
	set_jenis_kecelakaan();
	set_asal_rujukan();
	if ($("#id").val()!=''){
		get_last_kunjungan(idpasien);
		get_logic_pendaftaran();
		load_index_gc();
		load_index_sp();
		load_index_sc();
		$(".js_select2").select2("");
		get_list_kota('kabupaten1',$("#provinsi_id").val(),$("#ckabupaten").val());
		get_list_kota('kecamatan1',$("#ckabupaten").val(),$("#ckecamatan").val());
		get_list_kota('kelurahan1',$("#ckecamatan").val(),$("#ckelurahan").val());
		check = $("#chk_st_domisili").is(":checked");
		if (check){
			set_sama_domisili();
		}
		if ($("#tipepoli").val()=='2'){
			$("#jadwal_id").removeAttr('required');
		}
	}else{
		set_sama_domisili();
		get_list_kota('kabupaten1',$("#provinsi_id").val(),'');
	}
})	

function tambah_kontak(){
	var nextKontak = $(".copied").clone();
        // add corresponding classes (remove old first):
        nextKontak.removeClass('copied').addClass('new_kontak');
		console.log(nextKontak);
		nextKontak.find('select.div_select2').removeClass('div_select2').addClass('js_select2');
		// console.log(xselect2);		
		$('#pasted tbody').append(nextKontak);
		$(".js_select2").select2("");
	
}
function load_kontak(idpasien){
				$('#pasted tbody').empty();;
	$("#cover-spin").show();
	if (idpasien){
		$.ajax({
			url: "{site_url}tpendaftaran_poli/load_kontak/" + idpasien,
			method: "POST",
			dataType: "json",
			success: function(data) {
				$('#pasted tbody').append(data);
				$(".js_select2").select2("");
				$("#cover-spin").hide();
			}
		});
		
	}
	
}
function get_bahasa(idpasien){
	$('#pasted tbody').empty();;
	$("#cover-spin").show();
				$("#bahasa").val(288).trigger('change');
	// if (idpasien){
		// $.ajax({
			// url: "{site_url}tpendaftaran_poli/get_bahasa/" + idpasien,
			// method: "POST",
			// dataType: "json",
			// success: function(data) {
				// $("#bahasa").val(288).trigger('change');
				// // $(".js_select2").select2("");
				// $("#cover-spin").hide();
			// }
		// });
		
	// }
	
}
function load_bahasa(idpasien){
	// $("#cover-spin").show();
	// if (idpasien){
		$.ajax({
			url: "{site_url}tpendaftaran_poli/load_bahasa/" + idpasien,
			method: "POST",
			dataType: "json",
			success: function(data) {
				$('#bahasa').empty();
				$('#bahasa').append(data);
				// $(".js_select2").select2("");
				// $("#cover-spin").hide();
			}
		});
	// }
	
}
function hapus_kontak(row){
	 row.closest("tr").remove();
}
function hapus_kontak_id(row,id){
	// alert(id);
	let idpasien=$("#idpasien").val();
	$("#cover-spin").show();
	 $.ajax({
		url: "{site_url}tpendaftaran_poli/hapus_kontak/",
		method: "POST",
		dataType: "json",
		data:{id:id},
		success: function(data) {
			// row.closest("tr").remove();
			load_kontak(idpasien);
			$("#cover-spin").hide();
		}
	});
}

function cek_hasil(){
	  window.history.back();
}

// $("#form1").validate({
  // submitHandler: function(form) {
   // $("#cover-spin").show();
   // $("*[disabled]").not(true).removeAttr("disabled");
   // form.submit();
// }
// });

$("#form1").on("submit", function(){
	if($("#form1").valid()){
	//loader
	   $("*[disabled]").not(true).removeAttr("disabled");
	 $("#cover-spin").show();
	}
}); 
$("#chk_st_tidakdikenal").on("click", function(){
check = $("#chk_st_tidakdikenal").is(":checked");
    if(check) {
        set_tidak_dikenal();
    } else {
        set_dikenal();
    }
}); 
$(".div_pj").on("keyup", function(){
	check =$("#chk_st_pengantar").val();
	if (check=='1'){
		set_sama_pengantar();
	}
});
$("#hubungan").on("change", function(){
	check =$("#chk_st_pengantar").val();
	if (check=='1'){
		set_sama_pengantar();
	}
}); 
function set_tidak_dikenal(){
	$(".div_tidak_dikenal").removeAttr('disabled');
	$(".div_tidak_dikenal").attr('required',"true");
	$(".pas_tidak_dikenal").removeAttr('required');
	$(".pas_tidak_dikenal").removeClass('has-error');

}
function set_dikenal(){
	$(".pas_tidak_dikenal").attr('required',"true");
	$(".div_tidak_dikenal").attr('disabled', 'disabled');
	$(".div_tidak_dikenal").removeAttr('required');
	$(".form_tidak_dikenal").removeClass('has-error');
}
$("#chk_st_pengantar").on("change", function(){
check =$("#chk_st_pengantar").val();
// alert(check);
	$("#chk_st_pengantar2").val(check).trigger('change');
    if(check=='1') {
        set_sama_pengantar();
		 
    } else {
        set_beda_pengantar();
    }
}); 
$("#chk_st_pengantar2").on("change", function(){
check =$("#chk_st_pengantar2").val();
    if(check=='1') {
        set_sama_pengantar2();
    } else {
        set_beda_pengantar2();
    }
	$("#chk_st_pengantar").val(check).trigger('change.select2');
}); 
function set_sama_pengantar(){
	st_copy=true;
	$("#namapengantar").val($("#namapenanggungjawab").val());
	$("#hubungan_pengantar").val($("#hubungan").val()).trigger('change.select2');
	$("#alamatpengantar").val($("#alamatpenanggungjawab").val());
	$("#teleponpengantar").val($("#teleponpenanggungjawab").val());
	$("#noidentitaspengantar").val($("#noidentitaspenanggung").val());
	// $('#chk_st_pengantar2').attr('checked', 'checked');
	$(".div_data_pengantar").attr('readonly',true);
	copy_data_penanggunjawab_ke_detail();
	st_copy=false;
}
function set_beda_pengantar(){
	st_copy=true;
	$("#namapengantar").val('');
	$("#hubungan_pengantar").val("").trigger('change');
	$("#alamatpengantar").val('');
	$("#teleponpengantar").val('');
	$("#noidentitaspengantar").val('');
	$(".div_data_pengantar").removeAttr('readonly');
	// $('#chk_st_pengantar2').removeAttr('checked');
	copy_data_penanggunjawab_ke_detail();
	st_copy=false;
}
function set_sama_pengantar2(){
	st_copy=true;
	$("#namapengantar2").val($("#namapenanggungjawab2").val());
	$("#hubungan_pengantar2").val($("#hubungan2").val()).trigger('change.select2');
	$("#alamatpengantar2").val($("#alamatpenanggungjawab2").val());
	$("#teleponpengantar2").val($("#teleponpenanggungjawab2").val());
	$("#noidentitaspengantar2").val($("#noidentitaspenanggung2").val());
	$(".div_data_pengantar").attr('readonly',true);
	// $('#chk_st_pengantar').attr('checked', 'checked');
	copy_data_penanggunjawab_ke_head();
	st_copy=false;
}
function set_beda_pengantar2(){
	$("#namapengantar2").val('');
	$("#hubungan_pengantar2").val("").trigger('change');
	$("#alamatpengantar2").val('');
	$("#teleponpengantar2").val('');
	$("#noidentitaspengantar2").val('');
	$(".div_data_pengantar").removeAttr('readonly');
	copy_data_penanggunjawab_ke_head();
}

// $("#provinsi_id").change(function() {
// check = $("#chk_st_domisili").is(":checked");
    // if(check) {
        // $("#provinsi_id_ktp").val($("#provinsi_id").val()).trigger('change');
    // } else{
        // $("#provinsi_id_ktp").val(null).trigger('change');
		
	// }
// });
// $("#kabupaten1").change(function() {
// check = $("#chk_st_domisili").is(":checked");
    // if(check) {
       // $("#kabupaten_id_ktp").val($("#kabupaten1").val()).trigger('change');
    // }else{
       // $("#kabupaten_id_ktp").val(null).trigger('change');
		
	// }
// });
// $("#kecamatan1").change(function() {
// check = $("#chk_st_domisili").is(":checked");
    // if(check) {
       // $("#kecamatan_id_ktp").val($("#kecamatan1").val()).trigger('change');
    // }else{
       // $("#kecamatan_id_ktp").val(null).trigger('change');
		
	// }
// });
// $("#kelurahan1").change(function() {
// check = $("#chk_st_domisili").is(":checked");
    // if(check) {
       // $("#kelurahan_id_ktp").val($("#kelurahan1").val()).trigger('change');
    // }else{
       // $("#kecamatan_id_ktp").val(null).trigger('change');
		
	// }
// });
// $(document).on("change","#kodepos",function(){
	// alert('sini')
// });

// $("#kodepos").change(function() {
// });
$("#alamat_jalan,#rw,#rt").keyup(function() {
check = $("#chk_st_domisili").is(":checked");
    if(check) {
        set_sama_domisili();
    } 
});
$("#alamat_jalan,#rw,#rt").change(function() {
check = $("#chk_st_domisili").is(":checked");
    if(check) {
        set_sama_domisili();
    } 
});
$("#suku_id").change(function() {
	$("#suku").val($("#suku_id option:selected").text());
});
function set_sama_domisili(){
	$("#status_cari").val(1);
	$(".domisili").attr('disabled',true);
	// alert(opsi_kabupaten);
	// $("#kabupaten_id_ktp").empty();
	// $("#kabupaten_id_ktp").append(opsi_kabupaten);
	// $("#ckabupaten_ktp").val($("#ckabupaten").val());
	// $("#ckecamatan_ktp").val($("#ckecamatan").val());
	// $("#ckelurahan_ktp").val($("#ckelurahan").val());
	$("#alamat_jalan_ktp").val($("#alamat_jalan").val());
	
	// console.log('functionsama');
	$("#rw_ktp").val($("#rw").val());
	$("#rt_ktp").val($("#rt").val());
	$("#kodepos_ktp").val($("#kodepos").val());
	
}
function set_beda_domisili(){
	$(".domisili").attr('disabled',false);
	// $("#alamat_jalan_ktp").val('');
	// $("#provinsi_id_ktp").val($("#provinsi_id").val()).trigger('change');
	// $("#kabupaten_id_ktp").empty();
	// $("#kecamatan_id_ktp").empty();
	// $("#kelurahan_id_ktp").empty();
	// $("#kodepos_ktp").val('');
	// $("#rw_ktp").val('');
	// $("#rt_ktp").val('');
}

function load_status_pasien(){
	let id=$("#id").val();
	// alert(id);
	// $("#pertemuan_id").val(1).trigger('change');
	// $("#idkelompokpasien").val(1).trigger('change');
	let statuspasienbaru=$("#statuspasienbaru").val();
	if (statuspasienbaru=='1'){//Baru
		if (id==''){
		$("#idpasien").val('');
			
		}
		document.getElementById('no_medrec').style.display = 'block';
		document.getElementById('div_block_tidak_dikenal').style.display = 'block';
        document.getElementById('div_medrec').style.display = 'none';
        document.getElementById('div_history').style.display = 'none';
		$('#btn_cari_pasien').attr('disabled', true)
		$('#btn_cari_kartu').attr('disabled', true)
		$('#btn_tambah_kartu').attr('disabled', true)
		// st_load_manual=false;
	}else{
		document.getElementById('no_medrec').style.display = 'none';
        document.getElementById('div_block_tidak_dikenal').style.display = 'none';
        document.getElementById('div_medrec').style.display = 'block';
        document.getElementById('div_history').style.display = 'block';
		$('#btn_cari_kartu').attr('disabled', false)
		$('#btn_tambah_kartu').attr('disabled', false)
		$('#btn_cari_pasien').attr('disabled', false)
		$("#btn_cari_pasien").disabled=false;
		$("#his_poli").text('');
		$("#his_dokter").text('');
		$("#his_tanggal").text('');
		// st_load_manual=true;
		$("#tmp_medrec").val(null).trigger('change')
	}
	if ($("#id").val()!=''){
		setTimeout(function() {
		  let newOption = new Option($("#no_medrec").val(), $("#idpasien").val(), true, true);
			$("#tmp_medrec").append(newOption);
		}, 1000);

	}
	get_last_kunjungan('');
}
function get_last_kunjungan(idpasien){
	$id=$("#id").val();
	if (idpasien){
		$.ajax({
			url: "{site_url}tpendaftaran_poli/get_last_kunjungan/" + idpasien+'/'+$id,
			method: "POST",
			dataType: "json",
			success: function(data) {
				if (data !=null){
					$('.historykunjungan').show();
					$("#his_poli").text(data.nama_poli);
					$("#his_dokter").text(data.nama_dokter);
					if (data.nama_dokter){
						document.getElementById('div_nama_dokter').style.display = 'block';
					}else{
						document.getElementById('div_nama_dokter').style.display = 'none';
					}
					$("#his_tanggal").text(data.tanggaldaftar);
				}else{
					$('.historykunjungan').hide();
					
				}
				
			}
		});
	}else{
		$('.historykunjungan').hide();
		
	}
	
}
$(document).on("click", ".selectPasien", function() {
	$("#cover-spin").show();
	var idpasien = ($(this).data('idpasien'));
	load_detail_pasien(idpasien);
	setTimeout(function() {
	  let newOption = new Option($("#no_medrec").val(), $("#idpasien").val(), true, true);
		$("#tmp_medrec").append(newOption);
	}, 1000);

	
	return false;
});
$(document).on("change", "#tmp_medrec", function() {
	if ($(this).val()!=null){
		$("#cover-spin").show();
			
		var idpasien = $(this).val();
		load_detail_pasien(idpasien);
	}
	// return false;
});

$(document).on("click", "#btn_cari_kartu", function() {
	show_kartu(1);
});
$(document).on("click", "#btn_tambah_kartu", function() {
	show_kartu(2);
});
function load_detail_pasien(idpasien){
	$("#cover-spin").show();
	$.ajax({
		url: "{site_url}tpendaftaran_poli/getDataPasien/" + idpasien,
		method: "POST",
		dataType: "json",
		success: function(data) {
			if (data[0].noid_lama){
				sweetAlert("Informasi", "Data PASIEN <strong>'"+data[0].noid_lama+"'</strong> tidak akan digunakan karena <br> telah digabungkan dengan data PASIEN <strong>'"+data[0].no_medrec+"'</strong> <br><br> data PASIEN <strong>'"+data[0].no_medrec+"'</strong> akan digunakan.", "info");
			}
			st_load_manual=true;
			var status_kawin = (data[0].status_kawin != 0 ? data[0].status_kawin : 1);
			var jenis_kelamin = (data[0].jenis_kelamin != 0 ? data[0].jenis_kelamin : 1);

			$('#status_cari').val(1);
			$('#ckabupaten').val(data[0].kabupaten_id);
			$('#ckecamatan').val(data[0].kecamatan_id);
			$('#ckelurahan').val(data[0].kelurahan_id);
			
			$('#ckabupaten_ktp').val(data[0].kabupaten_id_ktp);
			// alert(data[0].kabupaten_id_ktp);
			$('#ckecamatan_ktp').val(data[0].kecamatan_id_ktp);
			$('#ckelurahan_ktp').val(data[0].kelurahan_id_ktp);

			$('#idpasien').val(data[0].id);
			$('#no_medrec').val(data[0].no_medrec);
			$('#no_medrec_lama').val(data[0].noid_lama);
			$('#nama').val(data[0].nama);
			$('#alamat_jalan').val(data[0].alamat_jalan);
			$('#jenis_kelamin').select2("val", jenis_kelamin);
			$('#jenis_id').select2("val", data[0].jenis_id);
			$('#noidentitas').val(data[0].ktp);
			$('#nohp').val(data[0].hp);
			$('#telprumah').val(data[0].telepon);
			$('#email').val(data[0].email);
			$('#tempat_lahir').val(data[0].tempat_lahir);


			// Pecahan Tanggal Lahir (Hari, Bulan, Tahun)
			var tanggalLahir = data[0].tanggal_lahir;
			var tahun = tanggalLahir.substr(0, 4);
			var bulan = tanggalLahir.substr(5, 2);
			var hari = tanggalLahir.substr(8, 2);
			// alert(bulan);
			// $('#tahun').val(tahun);
			$("#tahun").val(tahun).trigger('change');
			$("#bulan").val(bulan).trigger('change');
			$("#hari").val(hari).trigger('change');

			generate_tanggal_lahir();
			$('#golongan_darah').val(data[0].golongan_darah).trigger('change');
			$('#agama_id').val(data[0].agama_id).trigger('change');
			$('#warganegara').val(data[0].warganegara).trigger('change');
			$('#suku_id').val(data[0].suku_id).trigger('change');
			$('#suku').val(data[0].suku).trigger('change');
			$('#statuskawin').val(status_kawin).trigger('change').trigger('change');
			$('#pendidikan').val(data[0].pendidikan_id).trigger('change').trigger('change');
			$('#pekerjaan').val(data[0].pekerjaan_id).trigger('change').trigger('change');
			$('#catatan').val(data[0].catatan);
			$('#namapenanggungjawab').val(data[0].nama_keluarga);
			$('#hubungan').val(data[0].hubungan_dengan_pasien).trigger('change');
			$('#alamatpenanggungjawab').val(data[0].alamat_keluarga);
			$('#teleponpenanggungjawab').val(data[0].telepon_keluarga);
			$('#rw').val(data[0].rw);
			$('#rt').val(data[0].rt);
			$('#noidentitaspenanggung').val(data[0].ktp_keluarga);
			$('#nik').val(data[0].nik);
			$('#nama_ibu_kandung').val(data[0].nama_ibu_kandung);
			$('#nama_panggilan').val(data[0].nama_panggilan);
			// $('#bahasa').val(data[0].bahasa).trigger('change');
			$('#provinsi_id').val(data[0].provinsi_id).trigger('change.select2');
			$('#kodepos').val(data[0].kodepos);
			get_list_kota('kabupaten1',data[0].provinsi_id,data[0].kabupaten_id);
			get_list_kota('kecamatan1',data[0].kabupaten_id,data[0].kecamatan_id);
			get_list_kota('kelurahan1',data[0].kecamatan_id,data[0].kelurahan_id);
			// $('#kabupaten1').val(data[0].kabupaten_id).trigger('change');
			$("#cover-spin").hide();
			
			
			generate_rincian();
			get_last_kunjungan(idpasien);
			load_bahasa(idpasien);
			load_kontak(idpasien);
			check = $("#chk_st_domisili").is(":checked");
			if (check=='1'){
				set_sama_domisili();
			}
			// if (data[0].chk_st_domisili==null){
			// }
				// set_sama_domisili();
			
			// show_kartu(1);
			$("#cover-spin").hide();
			setTimeout(function() {
				let check =$("#chk_st_pengantar").val();
				if (check=='1'){
					set_sama_pengantar();
				}
			  copy_data_penanggunjawab_ke_detail();
			}, 2000);

			
			st_load_manual=false;
		}
	});
}
function get_list_kota(nama_var,kode,id){
	check = $("#chk_st_domisili").is(":checked");
    $("#cover-spin").show();
	$.ajax({
		url: "{site_url}tpendaftaran_poli/get_list_kota/",
		method: "POST",
		dataType: "json",
		data: {
			kode : kode,
			id : id,
		},
		success: function(data) {
			
			$('#'+nama_var).empty();
			$('#'+nama_var).append(data);
			if (nama_var=='kabupaten1'){
				opsi_kabupaten=data;
				console.log(opsi_kabupaten);
				if (check){
					$('#provinsi_id_ktp').val($('#provinsi_id').val()).trigger('change.select2');
					$('#kabupaten_id_ktp').empty().append(opsi_kabupaten);
					// $('#kecamatan_id_ktp').empty();
					// $('#kelurahan_id_ktp').empty();
				}
				// alert(opsi_kabupaten);
			}
			if (nama_var=='kecamatan1'){
				opsi_kecamatan=data;
				if (check){
					$('#kabupaten_id_ktp').val($('#kabupaten1').val()).trigger('change.select2');
					$('#kecamatan_id_ktp').empty().append(opsi_kecamatan);
					// $('#kelurahan_id_ktp').empty();
				}
			}
			if (nama_var=='kelurahan1'){
				// getKodepos($('#'+nama_var).val(),nama_var);
				opsi_kelurahan=data;
				if (check){
					$('#kecamatan_id_ktp').val($('#kecamatan1').val()).trigger('change.select2');
					$('#kelurahan_id_ktp').empty().append(opsi_kelurahan);
					$('#kodepos_ktp').val($("#kodepos").val());
					$('#rw_ktp').val($("#rw").val());
					$('#rt_ktp').val($("#rt").val());
				}
			}
			$("#cover-spin").hide();
		}
	});
}
function getKodepos(kelurahan,nama_var){
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpoliklinik_pendaftaran/getKodepos',
		method: "POST",
		dataType: "json",
		data: {
			"kodekel": kelurahan
		},
		success: function(data) {
			
			$("#"+nama_var).val('');
			if (data.length > 0) {
				$("#"+nama_var).val(data[0].kodepos);
				if (nama_var=='kodepos'){
					if (check){
						$('#kelurahan_id_ktp').val($('#kelurahan1').val()).trigger('change.select2');
						$("#kodepos_ktp").val(data[0].kodepos);
					}
				}
			}
		}
	});
}
$("#namapenanggungjawab,#hubungan,#alamatpenanggungjawab,#teleponpenanggungjawab,#noidentitaspenanggung").change(function() {
	if (st_copy==false){
		copy_data_penanggunjawab_ke_detail();
		
	}
});
$("#suku_id").change(function() {
	$("#suku").val($("#suku_id option:selected").text());
});
$("#namapengantar,#hubungan_pengantar,#alamatpengantar,#teleponpengantar,#noidentitaspengantar").change(function() {
	if (st_copy==false){
	copy_data_penanggunjawab_ke_detail();
	}
});

function copy_data_penanggunjawab_ke_detail(){
	$('#namapenanggungjawab2').val($('#namapenanggungjawab').val());
	$('#hubungan2').val($('#hubungan').val()).trigger('change.select2');
	$('#alamatpenanggungjawab2').val($('#alamatpenanggungjawab').val());
	$('#teleponpenanggungjawab2').val($('#teleponpenanggungjawab').val());
	$('#noidentitaspenanggung2').val($('#noidentitaspenanggung').val());
	
	$('#namapengantar2').val($('#namapengantar').val());
	$('#hubungan_pengantar2').val($('#hubungan_pengantar').val()).trigger('change.select2');
	$('#alamatpengantar2').val($('#alamatpengantar').val());
	$('#teleponpengantar2').val($('#teleponpengantar').val());
	$('#noidentitaspengantar2').val($('#noidentitaspengantar').val());
	// $("#hubungan2").selectpicker("refresh");
}
$("#namapenanggungjawab2,#hubungan2,#alamatpenanggungjawab2,#teleponpenanggungjawab2,#noidentitaspenanggung2").change(function() {
	// if (st_copy==false){
	copy_data_penanggunjawab_ke_head();
	// }
});
$("#namapengantar2,#hubungan_pengantar2,#alamatpengantar2,#teleponpengantar2,#noidentitaspengantar2").change(function() {
	if (st_copy==false){
		if (sumber_depan==false){
			copy_data_penanggunjawab_ke_head();
		}
	}
});
function copy_data_penanggunjawab_ke_head(){
	$('#namapenanggungjawab').val($('#namapenanggungjawab2').val());
	$('#hubungan').val($('#hubungan2').val()).trigger('change.select2');
	$('#alamatpenanggungjawab').val($('#alamatpenanggungjawab2').val());
	// $('#teleponpenanggungjawab').val($('#teleponpenanggungjawab2').val());
	$('#noidentitaspenanggung').val($('#noidentitaspenanggung2').val());
	
	$('#namapengantar').val($('#namapengantar2').val());
	$('#hubungan_pengantar').val($('#hubungan_pengantar2').val()).trigger('change.select2');
	$('#alamatpengantar').val($('#alamatpengantar2').val());
	$('#teleponpengantar').val($('#teleponpengantar2').val());
	$('#noidentitaspengantar').val($('#noidentitaspengantar2').val());
	// $("#hubungan2").selectpicker("refresh");
}
function show_kartu($tab){
	$("#modal_kartu").modal('show');
	if ($tab=='1'){
	$('[href="#tab_list_kartu"]').tab('show');
		LoadKartu();
	}else{
	$('[href="#tab_add_kartu"]').tab('show');
		
	}
}
$("#hari,#bulan,#tahun").change(function() {
	console.log(st_load_manual);
	if (st_load_manual==false){
		generate_tanggal_lahir();
	}
});	
$("#statuspasienbaru").change(function() {
	load_status_pasien();
	clear_pasien();
	get_medrec();
});	
function get_medrec(){
	if ($("#statuspasienbaru").val()=='1'){//Baru
		$.ajax({
			url: '{site_url}tbooking/get_nomedrec',
			method: "POST",
			dataType: "json",
			data: {
				
			},
			success: function(data) {
				$("#no_medrec").val(data.no_medrec);
			}
		});
	}else{
		$("#no_medrec").val('');
	}
}
function generate_tanggal_lahir(){
// $("#hari").change(function() {
	var hari = $("#hari").val();
	var bulan = $("#bulan").val();
	var tahun = $("#tahun").val();
	var tanggal = tahun + "-" + bulan + "-" + hari;
	$.ajax({
		url: '{site_url}tpoliklinik_pendaftaran/getDate',
		method: "POST",
		dataType: "json",
		data: {
			"hari": hari,
			"bulan": bulan,
			"tahun": tahun,
			"tanggal": tanggal
		},
		success: function(data) {
			if (tahun == "0" && bulan == "0") {
				$("#umur_tahun").val("0");
				$("#umur_bulan").val("0");
				$("#umur_hari").val(hari);
			} else {
				$("#umur_tahun").val(data.date[0].tahun);
				$("#umur_bulan").val(data.date[0].bulan);
				$("#umur_hari").val(data.date[0].hari);
			}

			let statuskawin = ($("#statuskawin option:selected").val() != '' ? $("#statuskawin option:selected").val() : 1);
			let jeniskelamin = ($("#jenis_kelamin option:selected").val() != '' ? $("#jenis_kelamin option:selected").val() : 1);

			getTitlePasien(data.date[0].tahun, data.date[0].bulan, data.date[0].hari, statuskawin, jeniskelamin);
		}
	});
		// });
}

function getTitlePasien(tahun, bulan, hari, statuskawin, jenis_kelamin) {
	console.log(tahun+' Bulan '+bulan+' Jenis K '+jenis_kelamin);
		if (tahun == 0 && bulan <=5) {
			$('#titlepasien').val("By").trigger('change');
		} else if ((tahun == 0 && bulan > 5) || tahun <= 15) {
	console.log('Anak Bro');
			$('#titlepasien').val("An").trigger('change');
		} else if (tahun >= 16 && jenis_kelamin == '1') {
			$('#titlepasien').val("Tn").trigger('change');
		} else if (tahun >= 15 && jenis_kelamin == '2' && statuskawin == 1) {
			$('#titlepasien').val("Nn").trigger('change');
		} else if (jenis_kelamin == '2' && statuskawin == 2) {
			$('#titlepasien').val("Ny").trigger('change');
		} else if (jenis_kelamin == '2' && statuskawin == 3) {
			$('#titlepasien').val("Ny").trigger('change');
		} else if (jenis_kelamin == '2' && statuskawin == 4) {
			$('#titlepasien').val("Tn").trigger('change');
	console.log('Tuan Bro');
			
		}
	console.log($('#titlepasien').val());
	}
	
// Perjanjian
$("#reservasi_tipe_id").change(function() {
	change_reservasi_tipe();
});
$("#tipepoli").change(function() {
	$("#reservasi_tipe_id").val(1).trigger('change');
});
function change_reservasi_tipe(){
	var idtipe = $("#tipepoli").val();
	var reservasi_tipe_id = $("#reservasi_tipe_id").val();
	if (reservasi_tipe_id=='1'){//Dokter
		$("#iddokter").attr('required',"true");
		if (idtipe!='2'){
			$(".btn_cari_dokter").removeAttr('disabled');
		}else{
			$(".btn_cari_dokter").attr('disabled',"true");
		}
		
	}else{
		$("#iddokter").removeAttr('required');
		$(".btn_cari_dokter").attr('disabled',"true");
		
	}
	$.ajax({
		url: '{site_url}tpendaftaran_poli/get_poli',
		method: "POST",
		dataType: "json",
		data: {
			"reservasi_tipe_id": reservasi_tipe_id,
			"idtipe": idtipe,
		},
		success: function(data) {
			$("#idpoli").empty();
			$("#iddokter").empty();
			$("#tanggal").empty();
			$("#jadwal_id").empty();
			$("#idpoli").append("<option value=''>Pilih Poliklinik</option>");
			for (var i = 0; i < data.length; i++) {
					$("#idpoli").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
			}
			if (idtipe=='2'){//IGD
				
				$("#reservasi_tipe_id").attr('disabled',true);
				$("#idpoli").val(22).trigger('change');
				$("#jadwal_id").removeAttr('required');
				
			}else{
				$("#reservasi_tipe_id").removeAttr('disabled');
				$("#jadwal_id").attr('required',"true");
				
			}
			$("#idpoli").selectpicker("refresh");
			// if ($("#status_cari").val()=='1'){
				// $('#kabupaten1').val($("#ckabupaten").val()).trigger('change');
			// }
		},
		error: function(data) {
			console.log(data);
		}
	});
}
function get_dokter(){
	var idpoli = $("#idpoli").val();
	$("#iddokter").empty();
	if ($("#reservasi_tipe_id").val()=='1'){
			$("#tanggal").empty();
		
	}
	generate_rincian();
	$.ajax({
		url: '{site_url}tpendaftaran_poli/get_dokter_poli',
		method: "POST",
		dataType: "json",
		data: {
			"idpoli": idpoli
		},
		success: function(data) {
			
			$("#iddokter").append("<option value=''>Pilih Dokter</option>");
			for (var i = 0; i < data.length; i++) {
					$("#iddokter").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
			}

			$("#iddokter").selectpicker("refresh");
			// if ($("#status_cari").val()=='1'){
				// $('#kabupaten1').val($("#ckabupaten").val()).trigger('change');
			// }
		},
		error: function(data) {
			console.log(data);
		}
	});
}

$("#iddokter").change(function() {
	let idtipe=$("#tipepoli").val();
	if (idtipe=='1'){
		get_tanggal();
		
	}else{
		get_tanggal_igd();
	}
	// cek_duplicate();
});
$("#jenis_kelamin,#statuskawin").change(function() {
	let statuskawin = ($("#statuskawin option:selected").val() != '' ? $("#statuskawin option:selected").val() : 1);
	let jeniskelamin = ($("#jenis_kelamin option:selected").val() != '' ? $("#jenis_kelamin option:selected").val() : 1);

	getTitlePasien($("#umur_tahun").val(), $("#umur_bulan").val(), $("#umur_hari").val(), statuskawin, jeniskelamin);
});

function get_tanggal(){
	// generate_rincian();
	// console.log($("#iddokter option:selected").text());
	var reservasi_tipe_id = $("#reservasi_tipe_id").val();
	var iddokter = $("#iddokter").val();
	var idpoli = $("#idpoli").val();
	$("#jadwal_id").empty();
	$("#jadwal_id").append("<option value=''>Pilih Waktu</option>");
	$.ajax({
		url: '{site_url}tpendaftaran_poli/get_jadwal_dokter_poli',
		method: "POST",
		dataType: "json",
		data: {
			"idpoli": idpoli,
			"iddokter": iddokter,
			"reservasi_tipe_id": reservasi_tipe_id,
		},
		success: function(data) {
			console.log(reservasi_tipe_id);
			$("#tanggal").empty();
			let alasan='';
			$("#tanggal").append("<option value=''>Pilih Tanggal</option>");
			if (reservasi_tipe_id=='1'){
				
				for (var i = 0; i < data.length; i++) {
					if (data[i].ket){
						alasan=' ( ' +data[i].ket+' )';					
					}else{
						alasan='';
					}
						$("#tanggal").append("<option value='" + data[i].tanggal + "' "+ data[i].st_disabel +">" + data[i].tanggal_nama + alasan+ "</option>");
				}
			}else{
				for (var i = 0; i < data.length; i++) {
						$("#tanggal").append("<option value='" + data[i].tanggal + "' "+data[i].st_disabel+">" + data[i].tanggal_nama + "</option>");
						console.log("<option value='" + data[i].tanggal + "' "+data[i].st_disabel+">" + data[i].tanggal_nama + "</option>");
				}
			}

			$("#tanggal").selectpicker("refresh");
			
		},
		error: function(data) {
			console.log(data);
		}
	});
}
function get_tanggal_igd(){
	// generate_rincian();
	// console.log($("#iddokter option:selected").text());
	var reservasi_tipe_id = $("#reservasi_tipe_id").val();
	var iddokter = $("#iddokter").val();
	var idpoli = $("#idpoli").val();
	var id = $("#id").val();
	var tmp_tanggal = $("#tmp_tanggal").val();
	$("#jadwal_id").empty();
	$("#jadwal_id").append("<option value=''>Pilih Waktu</option>");
	$.ajax({
		url: '{site_url}tpendaftaran_poli/get_jadwal_dokter_igd',
		method: "POST",
		dataType: "json",
		data: {
			"idpoli": idpoli,
			"iddokter": iddokter,
			"reservasi_tipe_id": reservasi_tipe_id,
			"id": id,
			"tmp_tanggal": tmp_tanggal,
		},
		success: function(data) {
			
				for (var i = 0; i < data.length; i++) {
						$("#tanggal").append("<option value='" + data[i].tanggal + "' "+data[i].st_disabel+">" + data[i].tanggal_nama + "</option>");
				console.log("<option value='" + data[i].tanggal + "' "+data[i].st_disabel+">" + data[i].tanggal_nama + "</option>");
				}

			$("#tanggal").selectpicker("refresh");
			
		},
		error: function(data) {
			console.log(data);
		}
	});
}
$("#idpoli").change(function() {
	get_dokter();
	let reservasi_tipe_id = $("#reservasi_tipe_id").val();
	let idtipe = $("#tipepoli").val();
	if (reservasi_tipe_id=='2'){
		if (idtipe=='1'){
		get_tanggal();
			
		}else{
		get_tanggal_igd();
			
		}
		// alert('sini');
	}
	cek_duplicate();
});
function get_waktu(){
	var reservasi_tipe_id = $("#reservasi_tipe_id").val();
	var idpoli = $("#idpoli").val();
	var iddokter = $("#iddokter").val();
	var tanggal = $("#tanggal").val();
	$.ajax({
		url: '{site_url}tpendaftaran_poli/get_jam_dokter_poli',
		method: "POST",
		dataType: "json",
		data: {
			"idpoli": idpoli,
			"iddokter": iddokter,
			"tanggal": tanggal,
			"reservasi_tipe_id": reservasi_tipe_id,
		},
		success: function(data) {
			$("#jadwal_id").empty();
			if (reservasi_tipe_id=='1'){
				
				$("#jadwal_id").append("<option value=''>Pilih Waktu</option>");
				let alasan='';
				for (var i = 0; i < data.length; i++) {
					if (data[i].saldo_kuota<=0){
						alasan=' ( TIDAK TERSEDIA )';					
					}else{
						alasan='';
					}
						$("#jadwal_id").append("<option value='" + data[i].jadwal_id + "' "+ data[i].st_disabel +">" + data[i].jam_nama+ alasan+"</option>");
				}
			}else{
				$("#jadwal_id").append("<option value=''>Pilih Waktu</option>");
				for (var i = 0; i < data.length; i++) {
					
						$("#jadwal_id").append("<option value='" + data[i].jam_id + "' "+ data[i].st_disabel +">" + data[i].jam_nama+" | Sisa Kuota : "+data[i].saldo_kuota+"</option>");
				}
			}

			$("#jadwal_id").selectpicker("refresh");
			// if ($("#status_cari").val()=='1'){
				// $('#kabupaten1').val($("#ckabupaten").val()).trigger('change');
			// }
		},
		error: function(data) {
			console.log(data);
		}
	});
}
$("#tanggal").change(function() {
	let nama_tanggal=$("#tanggal option:selected").text();
	let position = nama_tanggal.search('TIDAK');
	// alert(position);
	if (position > 0){
		$("#st_special_case").val(1);
		$("#jadwal_id").removeAttr('required');
	}else{
		$("#jadwal_id").attr('required',"true");
		
		$("#st_special_case").val(0);
	}
	get_waktu();
	generate_rincian();
	cek_duplicate();
});
$("#idkelompokpasien").change(function() {
	let kelompokpasien=$("#idkelompokpasien").val();
	if (kelompokpasien=='1'){
		$(".div_asuransi").css("display", "block");
	}else{
		
		$(".div_asuransi").css("display", "none");
	}
	if (kelompokpasien=='5'){
		$(".div_foto").css("display", "none");
	}else{
		
		$(".div_foto").css("display", "block");
	}
	generate_rincian();
});
$("#idasalpasien").change(function() {
	set_asal_rujukan();
	load_faskes_1();
});
function set_asal_rujukan(){
	var asal = $("#idasalpasien").val();
	if (asal == "1") {
		$(".div_rujukan").css("display", "none");
	} else {
		$(".div_rujukan").css("display", "block");
		
	}
}
function load_faskes_1(){
	$("#cover-spin").show();
	var asal = $("#idasalpasien").val();
	$.ajax({
		url: '{site_url}tpoliklinik_pendaftaran/getRujukan',
		method: "POST",
		dataType: "json",
		data: {
			"asal": asal
		},
		success: function(data) {
			$("#idrujukan").empty();
			$("#idrujukan").append("<option value=''>Pilih Opsi</option>");
			for (var i = 0; i < data.length; i++) {
				$("#idrujukan").append("<option value='" + data[i].id + "' >" + data[i].nama + "</option>");
			}
			$("#idrujukan").selectpicker("refresh");
			$("#cover-spin").hide();
		}
	});
}
var loadFile_foto = function(event) {
var output_img = document.getElementById('output_img');
output_img.src = URL.createObjectURL(event.target.files[0]);
};
$("#iddokter,#nama,#no_medrec,#pertemuan_id,#idrekanan,#jadwal_id").change(function() {
	generate_rincian();
});

function clear_pasien(){
	$('#status_cari').val(1);
	$('#ckabupaten').val(213);
	$('#ckecamatan').val(null);
	$('#ckelurahan').val(null);

	
	$('#email').val('');
	$('#idpasien').val('');
	$('#no_medrec').val('');
	$('#no_medrec_lama').val('');
	$('#nama').val('');
	$('#alamat_jalan').val('');
	// $('#jenis_kelamin').val(null).trigger('change');
	// $('#jenis_id').val(null).trigger('change');
	$('#noidentitas').val('');
	$('#nohp').val('');
	$('#telprumah').val('');
	$('#email').val('');
	$('#tempat_lahir').val('');


	// Pecahan Tanggal Lahir (Hari, Bulan, Tahun)
	var tanggalLahir = '';
	// var tahun = tanggalLahir.substr(0, 4);
	// var bulan = tanggalLahir.substr(5, 2);
	// var hari = tanggalLahir.substr(8, 2);
	// alert(bulan);
	// $('#tahun').val(tahun);
	$("#tahun").val(0).trigger('change.select2');
	$("#bulan").val('00').trigger('change.select2');
	$("#hari").val(0).trigger('change.select2');

	$('#umur_tahun').val('');
	$('#umur_bulan').val('');
	$('#umur_hari').val('');
	
	// $('#golongan_darah').val(null).trigger('change');
	// $('#agama_id').val(null).trigger('change');
	// $('#warganegara').val(null).trigger('change');
	// $('#suku_id').val(null).trigger('change');
	$('#suku').val('');
	// $('#statuskawin').val(null).trigger('change');
	// $('#pendidikan').val(null).trigger('change');
	// $('#pekerjaan').val(null).trigger('change');
	$('#catatan').val('');
	$('#namapenanggungjawab').val('');
	$('#kelurahan1').val(null).trigger('change');
	$('#kecamatan1').val(null).trigger('change');
	$('#kecamatan_id_ktp').val(null).trigger('change');
	$('#hubungan').val(null).trigger('change');
	$('#alamatpenanggungjawab').val('');
	$('#teleponpenanggungjawab').val('');
	$('#ckabupaten').val(213);
	$('#kodepos').val('');
	$('#rw').val('');
	$('#rt').val('');
	$('#noidentitaspenanggung').val('');
	$('#provinsi_id').val(12).trigger('change');
	$('#kabupaten1').val(213).trigger('change');
	$('#nik').val('');
	$('#nama_ibu_kandung').val('');
	$('#nama_panggilan').val('');
	$('#bahasa').val(null).trigger('change');
	$("#cover-spin").hide();
	generate_rincian();
	get_last_kunjungan('');
	load_kontak('');
	$("#cover-spin").hide();
	copy_data_penanggunjawab_ke_detail();
}
$("#idpoli,#iddokter").change(function() {
	get_logic_pendaftaran();
	
});
$("#pertemuan_id").change(function() {
	get_logic_pendaftaran();
	$("#pertemuan_id2").val($("#pertemuan_id").val()).trigger('change.select2');
});
$(document).on("change", "#pertemuan_id2", function() {
	$("#pertemuan_id").val($("#pertemuan_id2").val()).trigger('change.select2');
	get_logic_pendaftaran();
});
$("#st_kecelakaan").change(function() {
	set_jenis_kecelakaan();
});
function set_jenis_kecelakaan(){
	if ($("#st_kecelakaan").val()=='1'){
		$(".div_kecelakaan").show();
	}else{
		
		$(".div_kecelakaan").hide();
	}
}
function generate_rincian(){
	// $(".lbl_dokter").val($("#iddokter option:selected").text());
	// $(".lbl_tujuan").val($("#idpoli option:selected").text());
	// $(".lbl_jenis_pertemuan").val($("#pertemuan_id option:selected").text());
	// $("#suku").val($("#suku_id option:selected").text());
	// $(".lbl_asuransi").val($("#idrekanan option:selected").text());
	// $(".lbl_status_pasien").val($("#statuspasienbaru option:selected").text());
	// $(".lbl_jenis_booking").val($("#reservasi_cara option:selected").text());
	// $(".lbl_cara_pembayaran").val($("#idkelompokpasien option:selected").text());
	// $(".lbl_tanggal_janji").val($("#tanggal option:selected").text() + ' ('+$("#jadwal_id option:selected").text()+')');
	// $(".lbl_nama").val($("#nama").val());
	// $(".lbl_norekammedis").val($("#no_medrec").val());
	// // $("#suku").val($("#no_medrec").val());
	// $(".lbl_asuransi").val($("#no_medrec").val());
	cek_duplicate();
}
function get_logic_pendaftaran(){
	 let reservasi_tipe_id=$("#reservasi_tipe_id").val();
	 let jenis_pertemuan_id=$("#pertemuan_id").val();
	  let idpoliklinik=$("#idpoli").val();;
	  let iddokter=$("#iddokter").val();
	  
	  $.ajax({
		url: '{site_url}tpendaftaran_poli/get_logic_pendaftaran',
		method: "POST",
		dataType: "json",
		data: {
			"jenis_pertemuan_id": jenis_pertemuan_id,
			"idpoliklinik": idpoliklinik,
			"iddokter": iddokter,
			"reservasi_tipe_id": reservasi_tipe_id,
		},
		success: function(data) {
			$("#st_gc").val(data.st_gc);
			$("#st_sp").val(data.st_sp);
			$("#st_sc").val(data.st_sc);
			$("#text_skrining").html(data.text_skrining);
			show_hide_button()
			if (data.st_sc=='1'){
				$(".wizard-finish").html('<i class="fa fa-check"></i>  Lanjut Skrining Covid');
			}
			if (data.st_sp=='1'){
				$(".wizard-finish").html('<i class="fa fa-check"></i>  Lanjut Skrining Pasien');
			}
			if (data.st_gc=='1'){
				$(".wizard-finish").html('<i class="fa fa-check"></i>  Lanjut General Consent');
			}
		}
	});
}
function show_hide_button(){
	let total_skrining=0;
	total_skrining = parseFloat($("#st_sp").val()) + parseFloat($("#st_sc").val())+ parseFloat($("#st_gc").val());
	if (total_skrining>0){
		$("#btn_skrining").show();
	}else{
		$("#btn_skrining").hide();
		// alert('sini');
		
	}
	if ($("#st_sp").val()=='1' || $("#st_sc").val()=='1'){
		$("#btn_skrining").text('Lanjut Skrining Pasien');
	}
	if ($("#st_gc").val()=='1'){
		$("#btn_skrining").text('Lanjut General Consent');
	}
}
function cek_duplicate(){
	
	let idtipe=$("#tipepoli").val();
	let iddokter=$("#iddokter").val();
	let jadwal_id=$("#jadwal_id").val();
	let idpoli=$("#idpoli").val();
	let idpasien=$("#idpasien").val();
	let tanggal=$("#tanggal").val();
	if (idtipe=='1'){
		
	$.ajax({
		url: '{site_url}tbooking/cek_duplicate',
		method: "POST",
		dataType: "json",
		data: {
			"iddokter": iddokter,
			"jadwal_id": jadwal_id,
			"idpoli": idpoli,
			"idpasien": idpasien,
			"tanggal": tanggal,
		},
		success: function(data) {
			console.log(data);
			if (data){//Duplicate
				$(".div_duplicate").show();
				$(".div_not_duplicate").hide();
			}else{
				$(".div_duplicate").hide();
				$(".div_not_duplicate").show();
			}
		}
	});
	}
}
function load_index_gc(){
	// $("#cover-spin").show();
	var id=$("#id").val();
	// alert(id);
	$.ajax({
		url: '{site_url}tpendaftaran_poli/load_index_gc/'+id,
		dataType: "json",
		success: function(data) {
			$("#tabel_gc tbody").empty();
			$("#tabel_gc tbody").append(data.tabel);
			$(".js-select2").select2();
			validate_gc();
			$("#cover-spin").hide();
		}
	});
}
function modal_faraf($pendaftaran_id,$nama_tabel){
	$("#ttd_id").val($pendaftaran_id);
	$("#nama_tabel").val($nama_tabel);
	$("#modal_paraf").modal('show');
}
function modal_ttd(){
	$("#modal_ttd").modal('show');
}
function hapus_paraf($pendaftaran_id,$nama_tabel){
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}tpendaftaran_poli/hapus_paraf/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				pendaftaran_id:$pendaftaran_id,
				nama_tabel:$nama_tabel,
		  },success: function(data) {
				$("#modal_paraf").modal('hide');
				if ($nama_tabel=='tpoliklinik_pendaftaran_gc'){
	// alert($nama_tabel);
					load_index_gc();
				}
				if ($nama_tabel=='tpoliklinik_pendaftaran_sp'){
					load_index_sp();
				}
				if ($nama_tabel=='tpoliklinik_pendaftaran_sc'){
					load_index_sc();
				}
				$("#cover-spin").hide();
			}
		});
}

$(document).on("click","#btn_save_paraf",function(){
	var pendaftaran_id=$("#ttd_id").val();
	var signature64=$("#signature64").val();
	var nama_tabel=$("#nama_tabel").val();
	$nama_tabel=$("#nama_tabel").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}tpendaftaran_poli/save_ttd/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				pendaftaran_id:pendaftaran_id,signature64:signature64,nama_tabel:nama_tabel,
		  },success: function(data) {
				$("#modal_paraf").modal('hide');
				if ($nama_tabel=='tpoliklinik_pendaftaran_gc'){
					load_index_gc();
				}
				if ($nama_tabel=='tpoliklinik_pendaftaran_sp'){
					load_index_sp();
				}
				if ($nama_tabel=='tpoliklinik_pendaftaran_sc'){
					load_index_sc();
				}
				$("#cover-spin").hide();
			}
		});
	
});
$(document).on("click","#btn_save_ttd",function(){
	var pendaftaran_id=$("#ttd_id_2").val();
	var signature64=$("#signature64_2").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}tpendaftaran_poli/save_ttd_2/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				pendaftaran_id:pendaftaran_id,signature64:signature64
		  },success: function(data) {
				location.reload();
			}
		});
	
});
function hapus_ttd(){
	var pendaftaran_id=$("#ttd_id_2").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Batal Tandatangan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		$.ajax({
			  url: '{site_url}tpendaftaran_poli/hapus_ttd/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					pendaftaran_id:pendaftaran_id,
					// nama_tabel:nama_tabel,
			  },success: function(data) {
					location.reload();
				}
		});

	});
	
}
function validate_gc(){
	let st_next=true;
	let jawaban_perssetujuan=$("#jawaban_perssetujuan").val();
	if (jawaban_perssetujuan==''){
		st_next=false;
	}
	if ($("#jawaban_ttd").val()==''){
		st_next=false;
		
	}
	
	if (st_next==false){
		$('.btn_gc').attr('disabled', true);
	}else{
		$('.btn_gc').attr('disabled', false);
		
	}
	if (st_general=='1'){
		$('.nilai_gc').attr('disabled', true);
		$('.nilai_gc_free').attr('disabled', true);
		$('.btn_ttd').attr('disabled', true);
	}else{
		
		$('.nilai_gc').attr('disabled', false);
		$('.nilai_gc_free').attr('disabled', false);
		$('.btn_ttd').attr('disabled', false);
	}
}
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
$('#clear').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
var sig_2 = $('#sig_2').signature({syncField: '#signature64_2', syncFormat: 'PNG'});
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig_2.signature('clear');
	$("#signature64_2").val('');
});


$(document).on("change","#jawaban_perssetujuan",function(){
	// alert($(this));
	var pendaftaran_id=$("#id").val();
	var jawaban_perssetujuan=$("#jawaban_perssetujuan").val();
	$.ajax({
		  url: '{site_url}tpendaftaran_poli/update_persetujuan_gc/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				pendaftaran_id:pendaftaran_id,
				jawaban_perssetujuan:jawaban_perssetujuan,
		  },success: function(data) {
				validate_gc();
			}
		});
	
});
$(document).on("blur",".nilai_sp_free",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var approval_id=tr.find(".sp_id").val();
	var nilai_id=$(this).val();
	$.ajax({
	  url: '{site_url}tpendaftaran_poli/update_nilai_sp_free/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			approval_id:approval_id,nilai_id:nilai_id,
	  },success: function(data) {
			validate_sp();
		}
	});
});
$(document).on("blur",".nilai_sc_free",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var approval_id=tr.find(".sc_id").val();
	var nilai_id=$(this).val();
	$.ajax({
	  url: '{site_url}tpendaftaran_poli/update_nilai_sc_free/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			approval_id:approval_id,nilai_id:nilai_id,
	  },success: function(data) {
			validate_sp();
		}
	});
});
$(document).on("change",".nilai_gc",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var approval_id=tr.find(".approval_id").val();
	var nilai_id=$(this).val();
	$.ajax({
		  url: '{site_url}tpendaftaran_poli/update_nilai_gc/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				approval_id:approval_id,nilai_id:nilai_id,
		  },success: function(data) {
				validate_gc();
			}
		});
	
});
//SP
function load_index_sp(){
	$("#cover-spin").show();
	var id=$("#id").val();
	// alert(id);
	$.ajax({
		url: '{site_url}tpendaftaran_poli/load_index_sp/'+id,
		dataType: "json",
		success: function(data) {
			$("#tabel_sp tbody").empty();
			$("#tabel_sp tbody").append(data.tabel);
			$(".js-select2").select2();
			validate_sp();
			$("#cover-spin").hide();
		}
	});
}
$(document).on("change",".nilai_sp",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var sp_id=tr.find(".sp_id").val();
	var nilai_id=$(this).val();
	$.ajax({
		  url: '{site_url}tpendaftaran_poli/update_nilai_sp/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				sp_id:sp_id,nilai_id:nilai_id,
		  },success: function(data) {
				validate_sp();
			}
		});
	
});
function validate_sp(){
	let st_next=true;
	$('#tabel_sp tbody tr').each(function() {
		let opsi=$(this).find(".nilai_sp").val();
		if (opsi==''){
			st_next=false;
		}
			
	});
	if (st_next==false){
		$('.btn_sp').attr('disabled', true);
	}else{
		$('.btn_sp').attr('disabled', false);
		
	}
	if (st_skrining=='1'){
		$('.nilai_sp').attr('disabled', true);
		$('.nilai_sp_free').attr('disabled', true);
		$('.btn_ttd_sp').attr('disabled', true);
	}else{
		
		$('.nilai_sp').attr('disabled', false);
		$('.nilai_sp_free').attr('disabled', false);
		$('.btn_ttd_sp').attr('disabled', false);
	}
}
function load_index_sc(){
	$("#cover-spin").show();
	var id=$("#id").val();
	// alert(id);
	$.ajax({
		url: '{site_url}tpendaftaran_poli/load_index_sc/'+id,
		dataType: "json",
		success: function(data) {
			$("#tabel_sc tbody").empty();
			$("#tabel_sc tbody").append(data.tabel);
			$(".js-select2").select2();
			validate_sc();
			$("#cover-spin").hide();
		}
	});
}
$(document).on("change",".nilai_sc",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var sc_id=tr.find(".sc_id").val();
	var nilai_id=$(this).val();
	$.ajax({
		  url: '{site_url}tpendaftaran_poli/update_nilai_sc/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				sc_id:sc_id,nilai_id:nilai_id,
		  },success: function(data) {
				validate_sc();
			}
		});
	
});
function validate_sc(){
	let st_next=true;
	$('#tabel_sc tbody tr').each(function() {
		let opsi=$(this).find(".nilai_sc").val();
		if (opsi==''){
			st_next=false;
		}
			
	});
	if (st_next==false){
		$('.btn_sc').attr('disabled', true);
		$('.btn_sc').attr('disabled', true);
	}else{
		$('.btn_sc').attr('disabled', false);
		
	}
	if (st_covid=='1'){
		$('.nilai_sc').attr('disabled', true);
		$('.nilai_sc_free').attr('disabled', true);
		$('.btn_ttd_sc').attr('disabled', true);
	}else{
		
		$('.nilai_sc').attr('disabled', false);
		$('.nilai_sc_free').attr('disabled', false);
		$('.btn_ttd_sc').attr('disabled', false);
	}
}
$("#chk_st_domisili").on("click", function(){
check = $("#chk_st_domisili").is(":checked");
    if(check) {
		set_sama_domisili();
		get_list_kota('provinsi_id_ktp','',$("#provinsi_id").val());
		get_list_kota('kabupaten_id_ktp',$("#provinsi_id").val(),$("#kabupaten1").val());
		get_list_kota('kecamatan_id_ktp',$("#kabupaten1").val(),$("#kecamatan1").val());
		get_list_kota('kelurahan_id_ktp',$("#kecamatan1").val(),$("#kelurahan1").val());
		// $("#kabupaten_id_ktp").empty().append(opsi_kabupaten);
		// $("#kecamatan_id_ktp").empty().append(opsi_kecamatan);
		// $("#kelurahan_id_ktp").empty().append(opsi_kelurahan);
		// $("#kabupaten_id_ktp").append(opsi_kabupaten);
        // $("#provinsi_id_ktp").val($("#provinsi_id").val()).trigger('change');
		// $("#kabupaten_id_ktp").val($("#ckabupaten_ktp").val()).trigger('change');
		// $("#kecamatan_id_ktp").val($("#ckecamatan_ktp").val()).trigger('change');
		// $("#kelurahan_id_ktp").val($("#ckelurahan_ktp	").val()).trigger('change');
		// console.log($("#kabupaten1").val());
		
		// var $clone = $("#provinsi_id").cloneSelect2();
		// $($clone ).find('select').select2({width:'100%'});
		// $("#provinsi_id_ktp").append($clone);

    } else {
        set_beda_domisili();
    }
}); 

$("#provinsi_id").change(function() {
	// $("#kabupaten").css("display", "block");
	var kodeprovinsi = $("#provinsi_id").val();
	get_list_kota('kabupaten1',kodeprovinsi,$("#kabupaten1").val());
	$("#kecamatan1").empty();
	$("#kelurahan1").empty();
	$("#kodepos").val('');
	
});
$("#provinsi_id_ktp").change(function() {
	var kodeprovinsi = $("#provinsi_id_ktp").val();
	get_list_kota('kabupaten_id_ktp',kodeprovinsi,$("#kabupaten_id_ktp").val());
	$("#kecamatan_id_ktp").empty();
	$("#kelurahan_id_ktp").empty();
	$("#kodepos_ktp").val('');
});

$("#kabupaten1").change(function() {
	var kodekab = $("#kabupaten1").val();
	get_list_kota('kecamatan1',kodekab,$("#kecamatan1").val());
	$("#kelurahan1").empty();
	$("#kodepos").val('');
});
$("#kabupaten_id_ktp").change(function() {
	var kodekab = $("#kabupaten_id_ktp").val();
	get_list_kota('kecamatan_id_ktp',kodekab,$("#kecamatan_id_ktp").val());
	$("#kelurahan_id_ktp").empty();
	$("#kodepos_ktp").val('');
});

$("#kecamatan1").change(function() {
	$("#kelurahan").css("display", "block");
	var kodekec = $("#kecamatan1").val();
	get_list_kota('kelurahan1',kodekec,$("#kelurahan1").val());
	$("#kodepos").val('');
});
$("#kecamatan_id_ktp").change(function() {
	
	var kodekec = $("#kecamatan_id_ktp").val();
	get_list_kota('kelurahan_id_ktp',kodekec,$("#kelurahan_id_ktp").val());
	$("#kodepos_ktp").val('');
});

$("#kelurahan1").change(function() {
	var kelurahan = $("#kelurahan1").val();
	getKodepos(kelurahan,'kodepos');
});
$("#kelurahan_id_ktp").change(function() {
	var kelurahan = $("#kelurahan_id_ktp").val();
	getKodepos(kelurahan,'kodepos_ktp');
});
</script>