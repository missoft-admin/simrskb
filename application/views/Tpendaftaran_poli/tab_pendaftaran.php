<div class="row">
	<div class="form-group" style="margin-bottom: 10px;">
		<label class="col-md-2 control-label" for="reservasi_tipe_id">Tipe Pendaftaran <span style="color:red;">*</span></label>
		<div class="col-md-9">
			<select tabindex="5" id="reservasi_tipe_id" <?=($idtipe=='2'?'disabled':'')?> name="reservasi_tipe_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<option value="1" <?=($reservasi_tipe_id == 1 ? 'selected="selected"' : '')?>>POLIKLINIK</option>
					<option value="2" <?=($reservasi_tipe_id == 2 ? 'selected="selected"' : '')?>>REHABILITAS MEDIS</option>
				</select>
		</div>
	</div>
	
	<div class="form-group" style="margin-bottom: 10px;">
		<label class="col-md-2 control-label" for="idpoli">Poliklinik Tujuan<span style="color:red;">*</span></label>
		<div class="col-md-9">
			<select id="idpoli" name="idpoliklinik" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
					<option value="" <?=(''==$idpoli?'selected':'')?>>-Pilih Poliklinik-</option>
				<?foreach($list_poli as $r){?>
					<option value="<?=$r->id?>" <?=($r->id==$idpoli?'selected':'')?>><?=$r->nama?></option>
				<?}?>
			</select>
		</div>
	</div>
	<div class="form-group" style="margin-bottom: 10px;">
		<label class="col-md-2 control-label" for="iddokter">Dokter <span style="color:red;">*</span></label>
		<div class="col-md-9">
			<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
				<?foreach($list_dokter as $r){?>
					<option value="<?=$r->id?>" <?=($r->id==$iddokter?'selected':'')?>><?=$r->nama?></option>
				<?}?>
			</select>
		</div>
	</div>
	
	<div class="form-group" style="margin-bottom: 10px;">
		<label class="col-md-2 control-label" for="tanggal">Pilih Tanggal Janji Temu <span style="color:red;">*</span></label>
		<div class="col-md-6">
				<select id="tanggal" name="tanggal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
					<?foreach($list_jadwal_dokter as $r){?>
						<option value="<?=$r->tanggal?>" <?=($r->tanggal==$tanggal?'selected':'')?>><?=$r->tanggal_nama.($r->ket?' ('.$r->ket.')':'')?></option>
					<?}?>
				</select>
				
				
			
			
		</div>
	</div>
	<div class="form-group" style="margin-bottom: 10px;">
		<input class="form-control" readonly name="st_special_case" id="st_special_case" value="{st_special_case}" type="hidden">
		<label class="col-md-2 control-label" for="jadwal_id">Pilih Waktu Janji Temu <span style="color:red;">*</span></label>
		<div class="col-md-6">
			<select id="jadwal_id" name="jadwal_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." <?=($st_special_case=='1'?'':'required')?>>
				<?foreach($list_jam_dokter_poli as $r){?>
					<option value="<?=$r->jadwal_id?>" <?=($r->jadwal_id==$jadwal_id?'selected':'')?>><?=$r->jam_nama?></option>
				<?}?>
			</select>
		</div>
	</div>
	<div class="form-group div_duplicate" hidden style="margin-bottom: 10px;">
		<label class="col-md-2 control-label"></label>
		<div class="col-md-9">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h3 class="font-w300 push-15">Warning</h3>
					<p>Pasien Tersebut Sudah Mendaftar</p>
				</div>
		</div>
	</div>
	<div class="form-group" style="margin-bottom: 10px;">
		<label class="col-md-2 control-label" for="pertemuan_id">Jenis Kedatangan <span style="color:red;">*</span></label>
		<div class="col-md-9">
			<select id="pertemuan_id" name="pertemuan_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
				<option value="" <?=($pertemuan_id==''?'selected':'')?>>- Pilih Jenis Kedatangan -</option>
				<?foreach(list_variable_ref(15) as $r){?>
				<option value="<?=$r->id?>" <?=($pertemuan_id==$r->id?'selected':'')?>><?=$r->nama?></option>
				<?}?>
				
			</select>
		</div>
	</div>
	<div class="form-group" style="margin-bottom: 10px">
		<label class="col-md-2 control-label" for="idrekanan">Informasi Skrinning</label>
		<div class="col-md-9" hidden>
			<input class="form-control" name="st_gc" id="st_gc" value="{st_gc}" type="text">
			<input class="form-control" name="st_sp" id="st_sp" value="{st_sp}" type="text">
			<input class="form-control" name="st_sc" id="st_sc" value="{st_sc}" type="text">
			<input class="form-control" name="tmp_tanggal" id="tmp_tanggal" value="{tanggal}" type="text">
		</div>
		<div class="col-md-9">
			<p class="nice-copy" id="text_skrining">
					
			 </p>
		</div>
	</div>
	<div class="form-group" style="margin-bottom: 10px;">
		<label class="col-md-2 control-label" for="idkelompokpasien">Cara Pembayaran <span style="color:red;">*</span></label>
		<div class="col-md-9">
			<select tabindex="34" name="idkelompokpasien" id="idkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
				<option value="" <?=($idkelompokpasien==''?'selected':'')?>>Pilih Opsi</option>
				<?php foreach ($list_cara_bayar as $r):?>
				<option value="<?= $r->id; ?>" <?=($r->id == $idkelompokpasien ? 'selected' : '')?>><?= $r->nama; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>
	<div class="form-group div_asuransi" style="margin-bottom: 10px;display:none">
		<label class="col-md-2 control-label" for="idrekanan">Nama Perusahaan / Asuransi <span style="color:red;">*</span></label>
		<div class="col-md-9">
			<select tabindex="35" name="idrekanan" id="idrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
				<option value="" <?=($idrekanan=='0'?'selected':'')?>>Pilih Opsi</option>
				<?php foreach ($list_asuransi as $r) {?>
				<option value="<?= $r->id?>" <?=($idrekanan == $r->id ? 'selected="selected"' : '')?>><?= $r->nama?></option>
				<?php }?>
			</select>
		</div>
	</div>
	<div class="form-group div_foto" style="margin-bottom: 10px;display:none">
		<label class="col-md-2 control-label" for="idrekanan">No Kartu<span style="color:red;">*</span></label>
		<div class="col-md-9">
			<div class="input-group">
				<input class="form-control" name="nokartu" id="nokartu" value="{nokartu}" type="text">
				<span class="input-group-btn">
					<button class="btn btn-success" type="button" id="btn_cari_kartu"><i class="fa fa-search"></i></button>
					<button class="btn btn-primary" type="button" id="btn_tambah_kartu"><i class="fa fa-plus"></i></button>
					
				</span>
			</div>
		</div>
	</div>
	<div class="form-group div_foto" style="margin-bottom: 10px;display:none">
		<label class="col-md-2 control-label" for="idrekanan">Nama Participant<span style="color:red;">*</span></label>
		<div class="col-md-9">
				<input class="form-control" name="nama_kartu" id="nama_kartu" value="{nama_kartu}" type="text">
		</div>
	</div>
	<div class="form-group div_foto" style="margin-bottom: 10px;display:none">
		<label class="col-md-2 control-label" for="idrekanan">No Reference<span style="color:red;">*</span></label>
		<div class="col-md-9">
				<input class="form-control" name="noreference_kartu" id="noreference_kartu" value="{noreference_kartu}" type="text">
				<input class="form-control" name="kartu_id" id="kartu_id" value="{kartu_id}" type="hidden">
		</div>
	</div>
	
	
	<?
	if ($foto_kartu==''){
		$foto_kartu='default.png';
						}
						?>
	<div class="form-group div_foto" style="margin-bottom: 10px;display:none">
		<label class="col-md-2 control-label" for="foto_kartu">Foto Kartu Asuransi<span style="color:red;">*</span></label>
		<div class="col-md-9">
			<a id="url_foto" href="{upload_path}foto_kartu/<?=$foto_kartu?>" target="_blank">
				<img class="img-responsive" for="foto_kartu"  id="foto_kartu_view" src="{upload_path}foto_kartu/<?=$foto_kartu?>" id="output_img" style="width:20%" />
			</a>
		</div>
	</div>
	<div class="form-group div_foto" style="margin-bottom: 10px;display:none">
		<label class="col-md-2 control-label" for="foto_kartu"> <span style="color:red;">*</span></label>
		<div class="col-md-9">
		
				<input type="file"  accept="image/*" onchange="loadFile_foto(event)"  class="btn btn-sm btn-primary" id="foto_kartu" name="foto_kartu" value="{foto_kartu}" />
		</div>
	</div>
	
</div>
<?if($id!=''){?>
<div class="block-content block-content-mini block-content-full border-t">
<div class="row">
	<div class="form-group" style="margin-bottom: 10px;">
	<div class="col-md-12 text-right">
		<a href="{base_url}tpendaftaran_poli" class="btn btn-sm btn-default" name="btn_simpan" value="2" type="submit" ><i class="fa fa-reply"></i> Kembali</a>
		<button class="btn btn-sm btn-primary " type="submit" name="btn_simpan"  value="1"><i class="fa fa-save"></i> Update</button>				
		<button class="btn btn-sm btn-danger " type="submit" name="btn_simpan"  value="2"><i class="fa fa-save"></i> Update & Keluar</button>				
	</div>
	</div>
</div>
</div>
<?}?>