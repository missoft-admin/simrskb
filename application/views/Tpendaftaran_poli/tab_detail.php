<div class="row">
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="jenis_kelamin">Tipe Pasien <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select tabindex="32" name="idtipepasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="1" <?=($idtipepasien == 1 ? 'selected="selected"' : '')?>>Pasien RS</option>
					<option value="2" <?=($idtipepasien == 2 ? 'selected="selected"' : '')?>>Pasien Pribadi</option>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="jenis_kelamin">Jenis Kedatangan <span style="color:red;">*</span></label>
			<div class="col-md-5">
				<select id="pertemuan_id2" name="pertemuan_id2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
					<option value="" <?=($pertemuan_id==''?'selected':'')?>>- Pilih Jenis Kedatangan -</option>
					<?foreach(list_variable_ref(15) as $r){?>
					<option value="<?=$r->id?>" <?=($pertemuan_id==$r->id?'selected':'')?>><?=$r->nama?></option>
					<?}?>
					
				</select>
			</div>
			
		</div>
	
		<?php if ($id != '' && ($idasalpasien == '2' || $idasalpasien == '3')) {?>
			<?php
				if ($idasalpasien == 2) {
					$idtipeRS = 1;
				} elseif ($idasalpasien == 3) {
					$idtipeRS = 2;
				}
				
			}else{
				$idtipeRS='';
			}
			?>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="idasalpasien">Rujukan <?=$idrujukan?><span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select tabindex="32" name="idasalpasien" id="idasalpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<?php foreach (get_all('mpasien_asal') as $r):?>
						<option value="<?= $r->id; ?>" <?=($idasalpasien == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="form-group div_rujukan" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="idrujukan">Nama Fasilitas Kesehatan <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<div class="input-group">
					<select tabindex="33" name="idrujukan" id="idrujukan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mrumahsakit', ['idtipe' => $idtipeRS]) as $r) {?>
							<option value="<?= $r->id?>" <?=($idrujukan == $r->id ? 'selected="selected"' : '')?>><?= $r->nama?></option>
						<?php }?>
					</select>
					
					<span class="input-group-btn">
						<button class="btn btn-warning" title="refresh" onclick="set_asal_rujukan()" type="button"><i class="fa fa-refresh"></i></button>
						<a href="{base_url}mrumahsakit/create" title="Tambah Faskes" target="_blannk" class="btn btn-success" type="button"><i class="fa fa-plus"></i></a>
						
					</span>
				</div>
				
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="st_kecelakaan">Kasus Kecelakaan</label>
			<div class="col-md-8">
				<select tabindex="8" id="st_kecelakaan" name="st_kecelakaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
					<option value="0" <?=($st_kecelakaan=='0'?'selected':'')?>>TIDAK</option>
					<option value="1" <?=($st_kecelakaan=='1'?'selected':'')?>>YA</option>
					
				</select>
			</div>
		</div>
		<div class="form-group div_kecelakaan" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="st_kecelakaan">Jenis Kecelakaan</label>
			<div class="col-md-8">
				<select tabindex="8" id="jenis_kecelakaan" name="jenis_kecelakaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
					<?foreach(list_variable_ref(19) as $row){?>
					<option value="<?=$row->id?>" <?=($jenis_kecelakaan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
					
				</select>
			</div>
		</div>
		<div class="form-group div_kecelakaan" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="st_kecelakaan">Tanggal Kecelakaan</label>
			<div class="col-md-4">
				<div class="js-datepicker input-group date">
					<input class="js-datepicker form-control" type="text" id="tgl_kecelakaan" name="tgl_kecelakaan" data-date-format="dd-mm-yyyy"  value="{tgl_kecelakaan}" placeholder="Tanggal Kecelakaan">
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
			</div>
		</div>
		<div class="form-group div_kecelakaan" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="st_kecelakaan">Keterangan</label>
			<div class="col-md-8">
				<textarea tabindex="31" class="form-control" id="ket_kecelakaan" placeholder="" name="ket_kecelakaan"><?= $ket_kecelakaan?></textarea>
			</div>
		</div>
		
		
	</div>

	<div class="col-md-6">
		
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-9 control-label text-primary"><h5 align="left"><b>Data Penanggung Jawab</b></h5></label>
			<div class="col-md-12">
				<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-2 control-label" for="hubungan">Hubungan</label>
			<div class="col-md-4">
				<select tabindex="30" id="hubungan2" name="hubungan2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(9) as $row){?>
					<option value="<?=$row->id?>" <?=($hubungan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
				</select>
				
			</div>
			<label class="col-md-1 control-label" for="hubungan">Nama</label>
			<div class="col-md-5">
				<input tabindex="29" type="text" class="form-control" id="namapenanggungjawab2" placeholder="Nama Penanggung Jawab" name="namapenanggungjawab2" value="{namapenanggungjawab}" required>
			</div>
		</div>
		
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-2 control-label" for="alamatpenanggungjawab">Alamat</label>
			<div class="col-md-10">
				<textarea tabindex="31" class="form-control" id="alamatpenanggungjawab2" placeholder="Alamat Penanggung Jawab" name="alamatpenanggungjawab2"><?= $alamat_keluarga?></textarea>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-2 control-label" for="noidentitaspenanggung">No Identitas</label>
			<div class="col-md-4">
				<input tabindex="33" type="text" class="form-control" id="noidentitaspenanggung2" placeholder="No Identitas Penanggung Jawab" name="noidentitaspenanggung2" value="{noidentitaspenanggungjawab}" required>
				
			</div>
			<label class="col-md-1 control-label" for="telepon">Telepon</label>
			<div class="col-md-5">
				<input tabindex="32" type="text" class="form-control" id="teleponpenanggungjawab2" placeholder="Telepon" name="teleponpenanggungjawab2" value="{teleponpenanggungjawab}">
			</div>
		</div>
		
		<hr style="margin-top: 10px;margin-bottom: 15px;background: #000;">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-2  text-success h5" for="namapengantar"><strong>Data Pengantar</strong></label>
			<div class="col-md-10">
				<select tabindex="28" id="chk_st_pengantar2" name="chk_st_pengantar2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
					<option value="">Pilih Opsi</option>
					<option value="0" <?=($chk_st_pengantar ==0 ? 'selected="selected"' : '')?>>Tidak Sama Dengan Penanggungjawab</option>
					<option value="1" <?=($chk_st_pengantar ==1 ? 'selected="selected"' : '')?>>Sama Penanggungjawab</option>
				</select>
				
			</div>
		</div>
		<hr style="margin-top: 10px;margin-bottom: 15px;background: #000;">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-2 control-label" for="hubungan_pengantar">Hubungan</label>
			<div class="col-md-4">
				<select tabindex="30" id="hubungan_pengantar2" name="hubungan_pengantar2"   class="js-select2 form-control div_data_pengantar" style="width: 100%;" data-placeholder="Pilih Opsi" >
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(9) as $row){?>
					<option value="<?=$row->id?>" <?=($hubungan_pengantar == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
			<label class="col-md-1 control-label" for="namapengantar">Nama</label>
			<div class="col-md-5">
				<input tabindex="29" type="text" class="form-control div_data_pengantar"   id="namapengantar2" placeholder="Nama Pengantar" name="namapengantar2" value="{namapengantar}" >
			</div>
		</div>
		
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-2 control-label" for="alamatpengantar">Alamat</label>
			<div class="col-md-10">
				<textarea tabindex="31" class="form-control div_data_pengantar"   id="alamatpengantar2" placeholder="Alamat Pengantar" name="alamatpengantar2"><?= $alamatpengantar?></textarea>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-2 control-label" for="noidentitaspengantar">No Identitas</label>
			<div class="col-md-4">
				<input tabindex="33" type="text"   class="form-control div_data_pengantar" id="noidentitaspengantar2" placeholder="No Identitas Pengantar" name="noidentitaspengantar2" value="{noidentitaspengantar}" >
				
			</div>
			<label class="col-md-1 control-label" for="telepon">Telepon</label>
			<div class="col-md-5">
				<input tabindex="32" type="text" class="form-control div_data_pengantar"   id="teleponpengantar2" placeholder="Telepon" name="teleponpengantar2" value="{teleponpengantar}">
			</div>
		</div>
		
	</div>
	
	
</div>
<?if($id!=''){?>
<div class="block-content block-content-mini block-content-full border-t">
<div class="row">
	<div class="form-group" style="margin-bottom: 10px;">
	<div class="col-md-12 text-right">
		<a href="{base_url}tpendaftaran_poli" class="btn btn-sm btn-default" name="btn_simpan" value="2" type="submit" ><i class="fa fa-reply"></i> Kembali</a>
		<button class="btn btn-sm btn-primary " type="submit" name="btn_simpan"  value="1"><i class="fa fa-save"></i> Update</button>		
		<button class="btn btn-sm btn-danger " type="submit" name="btn_simpan"  value="2"><i class="fa fa-save"></i> Update & Keluar</button>				
	</div>
	</div>
</div>
</div>
<?}?>