<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}mlembaran_rm" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>

	<?php echo form_open('mlembaran_rm/save','class="push-10-t" id="form-work"') ?>
	<div class="block-content">
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Nama Lembaran</label>
			<div class="col-md-7">
				<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}">
			</div>
		</div>
	</div>
	<div class="block-content">
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Referensi Master</label>
			<div class="col-md-7">
				<select class="js-select2 form-control" id="referensi" name="referensi[]" style="width: 100%;" data-placeholder="Referensi Master" multiple>
					<option value="master_pegawai" <?= (in_array('master_pegawai', $referensi) ? 'selected':'') ?>>Master Pegawai</option>
					<option value="master_dokter" <?= (in_array('master_dokter', $referensi) ? 'selected':'') ?>>Master Dokter</option>
				</select>
			</div>
		</div>
	</div>

	<div class="block-content" style="padding-left: 35px;">
		<b><span class="label label-success" style="font-size:12px">KRITERIA</span></b>
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="tableKriteria" style="margin-top: 10px;">
			<thead>
				<tr>
					<th width="60%">Kriteria</th>
					<th width="20%">Nilai</th>
					<th width="20%">Aksi</th>
				</tr>
				<tr>
					<th>
						<input type="text" class="form-control" id="kriteria" placeholder="Kriteria" value="">
					</th>
					<th>
						<select id="nilai" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="+">+</option>
							<option value="-">-</option>
							<option value="=">=</option>
						</select>
					</th>
					<th>
						<button id="tambahKriteria" class="btn btn-sm btn-success" type="button"><i class="fa fa-plus"></i> Tambahkan</button>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($kriteria as $row){ ?>
					<tr>
						<td><?=$row->keterangan?></td>
						<td hidden><?=$row->nilai?></td>
						<td><?=styleNilaiLembaranRM($row->nilai)?></td>
						<td>
							<div class="btn btn-group">
								<a href="#" class="btn btn-primary btn-sm editKriteria" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>
								<a href="#" class="btn btn-danger btn-sm removeKriteria" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash-o"></i></a></td>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		</div>
		<b><span class="label label-success" style="font-size:12px">DETAIL LAINNYA</span></b>
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="tableDetail" style="margin-top: 10px;">
			<thead>
				<tr>
					<th width="80%">Kriteria</th>
					<th width="20%">Aksi</th>
				</tr>
				<tr>
					<th>
						<input type="text" class="form-control" id="detailKriteria" placeholder="Kriteria" value="">
					</th>
					<th>
						<button id="tambahDetailKriteria" class="btn btn-sm btn-success" type="button"><i class="fa fa-plus"></i> Tambahkan</button>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($detail as $row){ ?>
					<tr>
						<td><?=$row->keterangan?></td>
						<td>
							<div class="btn btn-group">
								<a href="#" class="btn btn-primary btn-sm editDetailKriteria" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>
								<a href="#" class="btn btn-danger btn-sm removeDetailKriteria" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash-o"></i></a></td>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		</div>
		<b><span class="label label-success" style="font-size:12px">DESKRIPSI</span></b>
		<br><br>
		<textarea class="form-control js-summernote" id="deskripsi" placeholder="Deskripsi" name="deskripsi" rows="5"><?=$deskripsi ?></textarea>
	</div>

	<div class="row">
		<div class="block-content">
			<div class="form-group">
				<div class="col-md-12 text-right">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mvariable_rekapan" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
		</div>
	</div>

	<input type="hidden" id="rowindex" value="">
	<input type="hidden" id="tableKriteriaValue" name="kriteria" value="">
	<input type="hidden" id="tableDetailValue" name="detail" value="">

	<br><br>
	<?php echo form_hidden('id', $id); ?>
	<?php echo form_close() ?>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	function formatStyle (state) {
		if (state == '+') {
			 var color = 'label-success';
			 var icon = '<i class="fa fa-plus"></i>';
		} else if (state == '-') {
	 		 var color = 'label-danger';
			 var icon = '<i class="fa fa-minus"></i>';
		} else {
			 var color = 'label-warning';
			 var icon = `<svg height="8" width="8" aria-hidden="true" style="padding: 2px 0 0 0;" focusable="false" data-prefix="fas" data-icon="equals" class="svg-inline--fa fa-equals fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
			 	<path fill="currentColor" d="M416 304H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32zm0-192H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path>
			 </svg>`;
		}

		return '<span class="label label-xl ' + color + '">' + icon + '</span>';
	};

	function formatState (state) {
		if (!state.id) {
			return state.text;
		}

		return $(formatStyle(state.text));
	};

	$(document).ready(function() {
		$(".number").number(true, 0, '.', ',');

		$("#nilai").select2({
		  templateResult: formatState
		});

		// Kriteria
		$(document).on("click", "#tambahKriteria", function() {
			var rowindex;

			if ($("#rowindex").val() != '') {
				var content = "";
				rowindex = $("#rowindex").val();
			} else {
				var content = "<tr>";
			}

			content += "<td>" + $("#kriteria").val() + "</td>";
			content += "<td hidden>" + $("#nilai option:selected").val() + "</td>";
			content += "<td>" + formatStyle($("#nilai option:selected").val()) + "</td>";
			content += "<td>";
			content += "	<div class='btn btn-group'>";
			content += "		<a href='#' class='btn btn-primary btn-sm editKriteria' data-toggle='tooltip' title='Ubah'><i class='fa fa-pencil'></i></a>";
			content += "		<a href='#' class='btn btn-danger btn-sm removeKriteria' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a>";
			content += "	</div>";
			content += "</td>";

			if($("#rowindex").val() != ''){
				$('#tableKriteria tbody tr:eq(' + rowindex + ')').html(content);
			}else{
				content += "</tr>";
				$('#tableKriteria tbody').append(content);
			}

			$("#rowindex").val('');
			$("#kriteria").val('');
			$("#nilai").val('+');
		});

		$(document).on("click", ".editKriteria", function() {
			$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
			$("#kriteria").val($(this).closest('tr').find("td:eq(0)").html());
			$("#nilai").val($(this).closest('tr').find("td:eq(1)").html());

			return false;
		});

		$(document).on("click", ".removeKriteria", function() {
			if (confirm("Hapus data ?") == true) {
				$(this).closest('td').parent().remove();
			}
			return false;
		});

		// Detail Lainnya
		$(document).on("click", "#tambahDetailKriteria", function() {
			var rowindex;

			if ($("#rowindex").val() != '') {
				var content = "";
				rowindex = $("#rowindex").val();
			} else {
				var content = "<tr>";
			}

			content += "<td>" + $("#detailKriteria").val() + "</td>";
			content += "<td>";
			content += "	<div class='btn btn-group'>";
			content += "		<a href='#' class='btn btn-primary btn-sm editDetailKriteria' data-toggle='tooltip' title='Ubah'><i class='fa fa-pencil'></i></a>";
			content += "		<a href='#' class='btn btn-danger btn-sm removeDetailKriteria' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a>";
			content += "	</div>";
			content += "</td>";

			if($("#rowindex").val() != ''){
				$('#tableDetail tbody tr:eq(' + rowindex + ')').html(content);
			}else{
				content += "</tr>";
				$('#tableDetail tbody').append(content);
			}

			$("#rowindex").val('');
			$("#detailKriteria").val('');
		});

		$(document).on("click", ".editDetailKriteria", function() {
			$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
			$("#detailKriteria").val($(this).closest('tr').find("td:eq(0)").html());

			return false;
		});

		$(document).on("click", ".removeDetailKriteria", function() {
			if (confirm("Hapus data ?") == true) {
				$(this).closest('td').parent().remove();
			}
			return false;
		});

		// Save
		$("#form-work").submit(function(e) {
			var form = this;

			var tableKriteriaValue = $('table#tableKriteria tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					if ($(cell).find("input").length >= 1) {
						return $(cell).find("input").val();
					} else {
						return $(cell).html();
					}
				});
			});

			var tableDetailValue = $('table#tableDetail tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					if ($(cell).find("input").length >= 1) {
						return $(cell).find("input").val();
					} else {
						return $(cell).html();
					}
				});
			});

			$("#tableKriteriaValue").val(JSON.stringify(tableKriteriaValue));
			$("#tableDetailValue").val(JSON.stringify(tableDetailValue));

			swal({
				title: "Berhasil!",
				text: "Proses penyimpanan data.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});
	});
</script>
