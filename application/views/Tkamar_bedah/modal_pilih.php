
<div class="modal" id="modalPilihJadwal" role="dialog" aria-hidden="true" aria-labelledby="modalAdd" font size="12">
    <div class="modal-dialog modal-lg"  style="width:65%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
                    <h3 class="block-title">Cari Transaksi Pembedahan</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal" id="form_detail">
                        <table class="table table-bordered" id="tabel_jadwal_pilih">
                            <thead>
                                <tr>
									<th>No</th>
									<th>No Transaksi</th>
									<th>Nama Pasien</th>
									<th>Tanggal</th>
									<th>Dokter</th>
									<th>Diagnosa</th>
									<th>Operasi</th>
									<th>Jenis</th>
									<th>Action</th>
								</tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">
// 2856
	$(document).ready(function() {
		
	});
	
	function pilih_ko($id,$pendaftaran_bedah_id) {
		$("#modalPilihJadwal").modal('show');
		$('#tabel_jadwal_pilih').DataTable().destroy();
			// alert(sasal);
		var table = $('#tabel_jadwal_pilih').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"searching": true,
		"serverSide": true,
		"autoWidth": false,
		"order": [],
		"ajax": {
			url: '{site_url}tkamar_bedah/LoadJadwalPilih/',
			type: "POST",
			dataType: 'json',
			data: {
				id: $id,
				pendaftaran_bedah_id: $pendaftaran_bedah_id,
				
			}
		},
		columnDefs: [
					 // {"targets": [0], "visible": false },
					 {  className: "text-center", targets:[0,1,3,5,6,7,8,4] },
					 { "width": "4%", "targets": [0] },
					 { "width": "8%", "targets": [1] },
					 { "width": "15%", "targets": [3] }
					]
		});
	}
	
</script>