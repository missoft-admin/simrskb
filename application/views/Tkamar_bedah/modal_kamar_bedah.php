<div class="modal" id="modalTambah" role="dialog" aria-hidden="true" aria-labelledby="modalAdd" font size="15">
    <div class="modal-dialog modal-lg" style="width:65%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Tambah Jadwal</h3>
                </div>
                    <form class="form-horizontal" id="form_detail_add">
						<input type="hidden" readonly class="form-control" id="trxid" value="">
						<input type="hidden" readonly class="form-control" id="tid" value="">
						<input type="hidden" readonly class="form-control" id="tidpasien" value="">
						<input type="hidden" readonly class="form-control" id="tidpendaftaran" value="">
						<input type="hidden" readonly class="form-control" id="tidasalpendaftaran" value="">
						<input type="hidden" readonly class="form-control" id="tidkelompokpasien" value="">
						<input type="hidden" readonly class="form-control" id="tidrekanan" value="">
						<input type="hidden" readonly class="form-control" id="ttipe" value="">
						<input type="hidden" readonly class="form-control" id="statusdatang" value="">
						<input type="hidden" readonly class="form-control" id="st_ranap" value="">
						<div class="col-md-12"  style="margin-top: 15px;">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-2 control-label" for="tnomedrec">No. Medrec</label>
								<div class="col-md-4">
									<input type="text" readonly class="form-control" id="tnomedrec" value="">
									
								</div>
								<label class="col-md-2 control-label" for="tnamapasien">Nama Pasien</label>
								<div class="col-md-4">
									<input type="text" readonly class="form-control" id="tnamapasien" value="">
								</div>
							</div>
							
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-2 control-label" for="ttanggallahir">Tanggal Lahir</label>
								<div class="col-md-4">
								<div class="js-datetimepicker input-group date">
									<input type="text"  disabled class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="ttanggallahir" value="">
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
								</div>
								<label class="col-md-2 control-label" for="tumur">Umur Pasien</label>
								<div class="col-md-4">
									<input type="text" readonly class="form-control" id="tumur" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-2 control-label" for="tdiagnosa">No Perencanaan</label>
								<div class="col-md-4">
									<input type="text" disabled class="form-control" id="tnopermintaan" value="">
								</div>
								<label class="col-md-2 control-label" for="tumur">Kelompok Pasien</label>
								<div class="col-md-4">
									<input type="text" readonly class="form-control" id="tkelompokpasien" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-2 control-label" for="tdiagnosa">Diagnosa</label>
								<div class="col-md-4">
									<input type="text" class="form-control" id="tdiagnosa" value="">
								</div>
								<label class="col-md-2 control-label" for="tdpjp">Dokter Bedah</label>
								<div class="col-md-4">
									<select name="tdpjp" readonly id="tdpjp" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Semua Dokter</option>
										<?foreach ($dokterumum as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>

								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-2 control-label" for="tt">Tanggal Pembedahan</label>
								<div class="col-md-4">
								<div class="js-datetimepicker input-group date">
									<input type="text" class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="ttanggaloperasi" value="">
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
								</div>
								<label class="col-md-2 control-label" for="tt">Jam Operasi</label>
								<div class="col-md-4">
								<div class="js-datetimepicker input-group date">
									<input type="text" class="form-control"id="twaktu1" value="">
									<span class="input-group-addon">
										<span class="fa fa-angle-right"></span>
									</span>
									<input type="text" class="form-control" id="twaktu2" value="">
								</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-2 control-label" for="tkelas">Jenis Operasi</label>
								<div class="col-md-4">
									<select name="tjenis_operasi" id="tjenis_operasi" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Jenis Operasi</option>

										<?foreach(list_variable_ref(124) as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<label class="col-md-2 control-label" for="toperasi">Tindakan</label>
								<div class="col-md-4">
									<input type="text" class="form-control" id="toperasi" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-2 control-label" for="tkelas">Jenis Anesthesi</label>
								<div class="col-md-4">
									<select name="tjenis_anestesi" id="tjenis_anestesi" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Jenis Anesthesi</option>

										<?foreach(list_variable_ref(394) as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<label class="col-md-2 control-label" for="tkelas">Jenis Pembedahan</label>
								<div class="col-md-4">
									<select name="tjenis_bedah" id="tjenis_bedah" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Jenis Operasi</option>

										<?foreach(list_variable_ref(389) as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-2 control-label" for="truang_operasi">Ruang Operasi</label>
								<div class="col-md-4">
									<select name="truang_operasi" id="truang_operasi" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Ruang Operasi</option>
										<?foreach ($mruangan as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<label class="col-md-2 control-label" for="tkelas">Kelompok Operasi</label>
								<div class="col-md-4">
									<select name="tkelompok_operasi" id="tkelompok_operasi" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Kelompok Operasi</option>
											<?foreach ($mkelompok_operasi as $row){?>
												<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-2 control-label" for="tkelas_tarif">Kelas Tarif</label>
								<div class="col-md-4">
									<select name="tkelas_tarif" <?=($st_kelas_tarif=='0'?'disabled':'')?> id="tkelas_tarif" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Kelas Tarif</option>
										<?foreach ($mkelas as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								<label class="col-md-2 control-label" for="tkelas">Tarif Jenis Operasi</label>
								<div class="col-md-4">
									<select name="ttarif_jenis_opr" <?=($st_pilih_tarif_jenis_opr=='0'?'disabled':'')?> id="ttarif_jenis_opr" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Tarif Jenis Operasi</option>
											<?foreach ($mjenis_operasi as $row){?>
												<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>
									</select>
								</div>
							</div>
							
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-2 control-label" for="tkelompok_diagnosa">Kelompok Diagnosa</label>
								<div class="col-md-4">
									<select multiple="multiple" name="tkelompok_diagnosa[]" id="tkelompok_diagnosa" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Pilih Kelompok Diagnosa</option>

									</select>
								</div>
								<label class="col-md-2 control-label" for="tkelas">Kelompok Tindakan</label>
								<div class="col-md-4">
									<select  multiple="multiple"  name="tkelompok_tindakan[]" id="tkelompok_tindakan" class="js-select2 form-control" style="width: 100%;">

									</select>
								</div>
							</div>
							
						</div>
						<div class="col-md-12">
							<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" for="tnamapasien">Catatan</label>
								<div class="col-md-10">
									<textarea class="form-control" name="tcatatan" id="tcatatan"></textarea>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" for="tnamapasien"></label>
							<label class="col-md-10 offset-2 text-left text-danger" id="label_pembuat">Dibuat Oleh : </label>
								
							</div>
						</div>
						<div class="modal-footer">

						</div>
						<div class="col-md-12">
							<div class="form-group" style="margin-top: 10px;">
								<label class="col-md-2 control-label" for="stanggallahir"></label>
								<div class="col-md-10" style="float:left;">
									<button class="btn btn-sm btn-primary" type="button" id="btn_save" onclick="simpan_jadwal(0)" name="btn_save" ><i class="fa fa-save"></i> Simpan</button>
									<button class="btn btn-sm btn-success" type="button" id="btn_setuju" onclick="simpan_jadwal(1)" ><i class="fa fa-check"></i> Setujui Jadwal</button>
									<button class="btn btn-sm btn-danger" type="button" id="btn_batal" name="btn_batal" ><i class="fa fa-times"></i> Batalkan Jadwal</button>
									<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
								</div>

							</div>
						</div>
                    </form>
            </div>
            <div class="modal-footer" >

            </div>
        </div>
    </div>
</div>
<div class="modal" id="modalGabung" role="dialog" aria-hidden="true" aria-labelledby="modalAdd" font size="15">
    <div class="modal-dialog modal-lg" style="width:60%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
                    <h3 class="block-title">Gabung Jadwal Operasi</h3>
                </div>
                    <form class="form-horizontal" id="form_detail_add">
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="block-content block-content-full block-content-mini bg-default text-white">
										Pilih Pasien
							</div>
							<div class="form-group" style="margin-bottom: 5px;margin-top: 15px;">
								<label class="col-md-4 control-label" for="Gnomedrec_pilih">No. Medrec</label>
								<div class="col-md-8">
									<div class="input-group">
											<input type="text" readonly class="form-control" id="Gnomedrec_pilih" value="">
											<span class="input-group-btn">
												<button tabindex="17" class="btn btn-success" type="button" id="btn_search_pilih"><i class="fa fa-search"></i></button>
											</span>
									</div>
								</div>
									<input type="hidden" readonly class="form-control" id="Gid_pilih" value="">
									<input type="hidden" readonly class="form-control" id="Gidpasien_pilih" value="">
									<input type="hidden" readonly class="form-control" id="Gidpendaftaran_pilih" value="">
									<input type="hidden" readonly class="form-control" id="Gidasalpendaftaran_pilih" value="">
									<input type="hidden" readonly class="form-control" id="Gtipe_id_pilih" value="">
									<input type="hidden" readonly class="form-control" id="st_acc_pilih" value="">

							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tnamapasien">Nama Pasien</label>
								<div class="col-md-8">
									<input type="text" readonly class="form-control" id="Gnamapasien_pilih" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="ttanggallahir">Tanggal Lahir</label>
								<div class="col-md-8">
								<div class="js-datetimepicker input-group date">
									<input type="text"  disabled class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="Gtanggallahir_pilih" value="">
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tperujuk">Dokter Perujuk</label>
								<div class="col-md-8">
									<input type="text" class="form-control"  readonly id="Gperujuk_pilih" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="ttipe">Tipe</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly  id="Gtipe_pilih" value="">
								</div>
							</div>
						</div>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="block-content block-content-full block-content-mini bg-default text-white">
										Pilih Jadwal
							</div>
							<div class="form-group" style="margin-bottom: 5px;margin-top: 15px;">
								<label class="col-md-4 control-label" for="Gnomedrec_jadwal">No. Medrec</label>
								<div class="col-md-8">
									<div class="input-group">
											<input type="text" readonly class="form-control" id="Gnomedrec_jadwal" value="">
											<span class="input-group-btn">
												<button tabindex="17" class="btn btn-success" type="button" id="btn_search_jadwal"><i class="fa fa-search"></i></button>
											</span>
									</div>
								</div>
									<input type="hidden" readonly class="form-control" id="Gid_jadwal" value="">
									<input type="hidden" readonly class="form-control" id="Gidpasien_jadwal" value="">
									<input type="hidden" readonly class="form-control" id="Gidpendaftaran_jadwal" value="">
									<input type="hidden" readonly class="form-control" id="Gidasalpendaftaran_jadwal" value="">
									<input type="hidden" readonly class="form-control" id="st_acc_jadwal" value="">
								</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tnamapasien">Nama Pasien</label>
								<div class="col-md-8">
									<input type="text" readonly class="form-control" id="Gnamapasien_jadwal" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="ttanggallahir">Tanggal Lahir</label>
								<div class="col-md-8">
								<div class="js-datetimepicker input-group date">
									<input type="text"  disabled class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="Gtanggallahir_jadwal" value="">
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tperujuk">Dokter Perujuk</label>
								<div class="col-md-8">
									<input type="text" class="form-control"  readonly id="Gperujuk_jadwal" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="ttipe">Tipe</label>
								<div class="col-md-8">
									<input type="text" class="form-control" readonly  id="Gtipe_jadwal" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tt">Tanggal Operasi</label>
								<div class="col-md-8">
								<div class="js-datetimepicker input-group date">
									<input type="text" disabled class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="Gtanggaloperasi_jadwal" value="">
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tt">Jam Operasi</label>
								<div class="col-md-8">
								<div class="js-datetimepicker input-group date">
									<input type="text" disabled class="form-control"id="Gwaktu1_jadwal" value="">
									<span class="input-group-addon">
										<span class="fa fa-angle-right"></span>
									</span>
									<input type="text" disabled class="form-control" id="Gwaktu2_jadwal" value="">
								</div>
								</div>
							</div>

						</div>

						<div class="modal-footer">
						</div>
						<div class="col-md-12">
							<div class="form-group" style="margin-top: 10px;">
								<label class="col-md-8 control-label" for="stanggallahir"></label>
								<div class="col-md-4" style="float:left;">
									<button class="btn btn-sm btn-primary" type="button" id="btn_save_gabung" name="btn_save_gabung" ><i class="fa fa-save"></i> Simpan</button>
									<button class="btn btn-sm btn-default" type="button" id="btn_save_reset" name="btn_save_reset" ><i class="fa fa-refresh"></i> Reset</button>
								</div>

							</div>
						</div>
                    </form>
            </div>
            <div class="modal-footer" >

            </div>
        </div>
    </div>
</div>
<div class="modal" id="modalAdd" role="dialog" aria-hidden="true" aria-labelledby="modalAdd" font size="12">
    <div class="modal-dialog modal-lg"  style="width:65%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
                    <h3 class="block-title">Cari Pasien</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal" id="form_detail">
                        <div class="col-md-6">

								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snomedrec">No. Medrec</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snomedrec" value="">
										<input type="hidden" class="form-control" id="form_pilih" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snamapasien">Nama Pasien</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snamapasien" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="stanggallahir">Tanggal Lahir</label>
									<div class="col-md-8">
									<div class="js-datetimepicker input-group date">
										<input type="text" class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="stanggallahir" value="">
										<span class="input-group-addon">
											<span class="fa fa-calendar"></span>
										</span>
									</div>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="skelas">Kelas Perawatan</label>
									<div class="col-md-8">
										<select name="skelas" id="skelas" class="js-select2 form-control" style="width: 100%;">
											<option value="#">Semua Kelas</option>
											<?foreach ($kelas_list as $row){?>
												<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>

										</select>
									</div>
								</div>
						</div>
						<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="salamat">DPJP</label>
									<div class="col-md-8">
										<select name="sdpjp" id="sdpjp" class="js-select2 form-control" style="width: 100%;">
											<option value="#">Pilih Dokter</option>
											<?foreach ($dokterumum as $row){?>
												<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>
										</select>

									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="sperujuk">Dokter Perujuk</label>
									<div class="col-md-8">
										<select name="sperujuk" id="sperujuk" class="js-select2 form-control" style="width: 100%;">
											<option value="#">Semua Dokter</option>
											<?foreach ($dokterumum as $row){?>
												<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>
										</select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="stanggallahir">Asal Pasien</label>
									<div class="col-md-8">
										<select name="sasal" id="sasal" class="js-select2 form-control" style="width: 100%;">
											<option value="#">Semua Asal</option>
											<option value="1">Rawat Inap</option>
											<option value="2">ODS</option>
										</select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 15px;">
									<label class="col-md-4 control-label" for="stanggallahir"></label>
									<div class="col-md-8">
										<button class="btn btn-success text-uppercase" type="button" id="btn_filter_pasien" name="btn_filter_pasien" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
									</div>
								</div>


						</div>
						<hr>
                        <table class="table table-bordered" id="tabel_pasien">
                            <thead>
                                <tr>
									<th>Tipe</th>
									<th>No Modrec</th>
									<th>Nama Pasien</th>
									<th>Tanggal Lahir</th>
									<th>Kelas & Bed</th>
									<th>Dokter DPJP</th>
									<th>Dokter Perujuk</th>
								</tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="modal" id="modalJadwal" role="dialog" aria-hidden="true" aria-labelledby="modalAdd" font size="12">
    <div class="modal-dialog modal-lg"  style="width:65%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
                    <h3 class="block-title">Cari Jadwal</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal" id="form_detail">
                        <div class="col-md-6">

								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snomedrec">No. Medrec</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="jnomedrec" value="">
										<input type="hidden" class="form-control" id="form_pilih" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="jnamapasien">Nama Pasien</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="jnamapasien" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="jtanggallahir">Tanggal Lahir</label>
									<div class="col-md-8">
									<div class="js-datetimepicker input-group date">
										<input type="text" class="js-datepicker form-control" data-date-format="dd-mm-yyyy" id="jtanggallahir" value="">
										<span class="input-group-addon">
											<span class="fa fa-calendar"></span>
										</span>
									</div>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="jkelas">Kelas Tarif</label>
									<div class="col-md-8">
										<select name="jkelas" id="jkelas" class="js-select2 form-control" style="width: 100%;">
											<option value="#">Semua Kelas</option>
											<?foreach ($kelas_list as $row){?>
												<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>

										</select>
									</div>
								</div>
						</div>
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="jalamat">Diagnosa</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="jdiagnosa" value="">

								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="jperujuk">Operasi</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="joperasi" value="">

								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="jtanggallahir">Asal Pasien</label>
								<div class="col-md-8">
									<select name="jasal" id="jasal" class="js-select2 form-control" style="width: 100%;">
										<option value="#">Semua Asal</option>
										<option value="1">Rawat Inap</option>
										<option value="2">ODS</option>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 15px;">
								<label class="col-md-4 control-label" for="jtanggallahir"></label>
								<div class="col-md-8">
									<button class="btn btn-success text-uppercase" type="button" id="btn_filter_jadwal" name="btn_filter_jadwal" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
								</div>
							</div>


						</div>
						<hr>
                        <table class="table table-bordered" id="tabel_jadwal">
                            <thead>
                                <tr>
									<th>Tipe</th>
									<th>No Modrec</th>
									<th>Nama Pasien</th>
									<th>Tanggal Lahir</th>
									<th>Kelas & Bed</th>
									<th>Diagnosa</th>
									<th>Operasi</th>
								</tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">
// 2856
	$(document).ready(function() {
		
		// add_jawal('2856');
		// get_pasien_rawat_inap();
	});
	$(document).on("click", "#btn_save_reset", function() {
		reset_gabung();
	});
	$(document).on("click", "#btn_search_jadwal", function() {
		$("#modalJadwal").modal('show');
		LoadJadwal();
	});
	$(document).on("click", ".selectJadwal", function() {
			var id = ($(this).data('id'));
			get_jadwal(id);

	});
	$(document).on("click", "#btn_save_gabung", function() {
		simpan_gabung();
	});
	function validasi_gabung(){
		if ($("#Gnomedrec_pilih").val()==''){
			sweetAlert("Maaf...", "Pilih Pasien Harus diisi!", "error");
			return false;
		}
		if ($("#Gnomedrec_jadwal").val()==''){
			sweetAlert("Maaf...", "Jadwal Harus Harus diisi!", "error");
			return false;
		}

		return true;
	}
	function simpan_gabung(){

		if (validasi_gabung()==false) {return false}
		var idjadwal=$("#Gid_jadwal").val();
		var idpendaftaran=$("#Gidpendaftaran_pilih").val();
		// var idasalpendaftaran=$("#Gidasalpendaftaran_pilih").val();
		var idasalpendaftaran='2';
		var idpasien=$("#Gidpasien_pilih").val();
		var idtipe=$("#Gtipe_id_pilih").val();
		// alert('IDJADWAL :'+idjadwal+'ID PENDAFTARAN:'+idpendaftaran+' IDASAL :'+idasalpendaftaran+' IDABSSEN :'+idpasien);
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menggabung Jadwal Operasi Baru ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
			}).then(function() {
				$.ajax({
					url: '{site_url}tkamar_bedah/simpan_gabung',
					type: 'POST',
					data: {idjadwal:idjadwal,idpendaftaran:idpendaftaran,
						idasalpendaftaran: idasalpendaftaran,idpasien: idpasien,idtipe: idtipe,
					},
					complete: function() {
						$.toaster({priority : 'success', title : 'Succes!', message : ' Jadwal Berhasil digabung'});
						$('#modalGabung').modal('hide');
						$('#index_list').DataTable().ajax.reload( null, false );
						// window.location.href = "{site_url}tko/index/"+idunit;
					}
				});
			});

	}
	function LoadJadwal() {
		var snomedrec=$("#jnomedrec").val();
		var snamapasien=$("#jnamapasien").val();
		var stanggallahir=$("#jtanggallahir").val();
		var skelas=$("#jkelas").val();
		var sdiagnosa=$("#jdiagnosa").val();
		var soperasi=$("#joperasi").val();
		var sasal=$("#jasal").val();
		$('#tabel_jadwal').DataTable().destroy();
			// alert(sasal);
		var table = $('#tabel_jadwal').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"searching": true,
		"serverSide": true,
		"autoWidth": false,
		"order": [],
		"ajax": {
			url: '{site_url}tko/LoadJadwal/',
			type: "POST",
			dataType: 'json',
			data: {
				snomedrec: snomedrec,
				snamapasien: snamapasien,
				stanggallahir: stanggallahir,
				skelas: skelas,
				sdiagnosa: sdiagnosa,
				soperasi: soperasi,
				sasal: sasal,
			}
		},
		columnDefs: [
					 // {"targets": [0], "visible": false },
					 // {  className: "text-right", targets:[1,4,5,6,7,9,10,11] },
					 {  className: "text-center", targets:[0,1,3,4] },
					 { "width": "8%", "targets": [0,1] },
					 { "width": "10%", "targets": [3,4] },
					 { "width": "15%", "targets": [2,5,6] }
					]
		});
	}
	$(document).on("click", "#btn_search_pilih", function() {
		// $("#snomedrec").val('');
		$("#snamapasien").val('');
		$("#stanggallahir").val('');
		$("#form_pilih").val('gabung');
		$("#modalAdd").modal('show');
		LoadPasien();
	});
	$(document).on("click", ".selectPasien", function() {
		var id = ($(this).data('id'));
		$("#tid").val(id);
		get_pasien_rawat_inap_gabung(id);
			
	});
	function LoadPasien() {
		var snomedrec=$("#snomedrec").val();
		var snamapasien=$("#snamapasien").val();
		var stanggallahir=$("#stanggallahir").val();
		var skelas=$("#skelas").val();
		var sdpjp=$("#sdpjp").val();
		var sperujuk=$("#sperujuk").val();
		var sasal=$("#sasal").val();
		$('#tabel_pasien').DataTable().destroy();
			// alert(skelas);
		var table = $('#tabel_pasien').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"searching": true,
		"serverSide": true,
		"autoWidth": false,
		"order": [],
		"ajax": {
			url: '{site_url}tko/LoadPasien/',
			type: "POST",
			dataType: 'json',
			data: {
				snomedrec: snomedrec,
				snamapasien: snamapasien,
				stanggallahir: stanggallahir,
				skelas: skelas,
				sdpjp: sdpjp,
				sperujuk: sperujuk,
				sasal: sasal,
			}
		},
		columnDefs: [
					 // {"targets": [0], "visible": false },
					 // {  className: "text-right", targets:[1,4,5,6,7,9,10,11] },
					 {  className: "text-center", targets:[0,1,3,4] },
					 { "width": "8%", "targets": [0,1] },
					 { "width": "10%", "targets": [3,4] },
					 { "width": "15%", "targets": [2,5,6] }
					]
		});
	}
	function gabung_proses(id){
		reset_gabung();
		$("#Eid").val(id);
		get_jadwal(id);
		$("#modalEdit").modal('hide');
		$("#modalGabung").modal('show');
	}
	function reset_gabung(){
		$("#Gnomedrec_pilih").val('');
		$("#Gnamapasien_pilih").val('');
		$("#Gtanggallahir_pilih").val('');
		$("#Gperujuk_pilih").val('');
		$("#Gtipe_pilih").val('');
		$("#Gnomedrec_jadwal").val('');
		$("#Gnamapasien_jadwal").val('');
		$("#Gtanggallahir_jadwal").val('');
		$("#Gperujuk_jadwal").val('');
		$("#Gtipe_jadwal").val('');
	}
	function get_jadwal($id){
		var id = $id;
		$.ajax({
			url: '{site_url}tkamar_bedah/get_edit/'+id,
			dataType: "json",
			success: function(data) {
				if ($("#snomedrec").val()==''){
				$("#snomedrec").val(data.no_medrec);
					
				}
				$("#Gnomedrec_jadwal").val(data.no_medrec);
				$("#Gnamapasien_jadwal").val(data.namapasien);
				$("#Gtanggallahir_jadwal").val(data.tanggal_lahir);
				// $("#Gperujuk_jadwal").val(data.perujuk);
				$("#Gtipe_jadwal").val(data.tipe_nama);
				$("#Gtanggaloperasi_jadwal").val(data.tanggaloperasi);
				$("#Gwaktu1_jadwal").val(data.waktumulaioperasi);
				$("#Gwaktu2_jadwal").val(data.waktuselesaioperasi);
				$("#Gperujuk_jadwal").val(data.dokter_perujuk);
				$("#Gid_jadwal").val(id);

				console.log(data);
			}
		});

	}
	function add_jawal(id){
		$("#tid").val(id);
		$("#tidasalpendaftaran").val('2');
		$("#trxid").val('');
		$("#st_ranap").val('1');
		$("#form_pilih").val('tambah');
		$("#modalTambah").modal('show');
		// if ($("#form_pilih").val()=='tambah'){
			$("#modalTambah").modal('show');
			get_pasien_rawat_inap();
			$("#btn_setuju").hide();
			$("#btn_batal").hide();
		// }else{
			// get_pasien_rawat_inap_gabung(id);
		// }
	}
	
	function show_proses(id){
		$("#modalTambah").modal('show');
		$("#trxid").val(id);
		load_kelompok_tindakan(id,'edit');
		load_kelompok_diagnosa(id,'edit');
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tkamar_bedah/get_edit/'+id,
			dataType: "json",
			success: function(data) {
				$("#tnomedrec").val(data.no_medrec);
				$("#tnamapasien").val(data.namapasien);
				$("#tid").val(data.idpendaftaran);
				$("#tdpjp").val(data.dpjp).trigger('change');
				$("#tumur").val(data.umurtahun + ' Tahun '+data.umurbulan + ' Bulan ' + data.umurhari + ' Hari');
				$("#ttanggallahir").val(data.tanggal_lahir);
				$("#ttipe").val(data.tipe);
				$("#tperujuk").val(data.perujuk);
				if (data.idtipe=='1'){
					$("#tkelas").val(data.kelas+' / '+data.bed);
				}else{
					$("#tkelas").val('');
				}
				$("#btn_setuju").hide();
				// $("#btn_batal").hide();
				if (data.statussetuju=='0'){
					$("#btn_setuju").show();
				}
				// if (data.statusdatang=='0'){
					// $("#btn_setuju").show();
				// }
				$("#label_pembuat").text(data.label_pembuat);
				$("#tkelompokpasien").val(data.kelompokpasien);
				$("#tidkelompokpasien").val(data.idkelompokpasien);
				if (data.jenis_bedah==null || data.jenis_bedah=='0'){
				$("#tjenis_bedah").val(data.def_jenis_bedah).trigger('change');
					
				}else{
				$("#tjenis_bedah").val(data.jenis_bedah).trigger('change');
					
				}
				$("#ttarif_jenis_opr").val(data.tarif_jenis_opr).trigger('change');
				$("#tasuransi").val(data.asuransi);
				$("#tidpendaftaran").val(data.idpendaftaran);
				$("#tidasalpendaftaran").val(data.idasalpendaftaran);
				$("#tidpasien").val(data.idpasien);
				$("#twaktu1").val(data.waktumulaioperasi);
				if (data.waktuselesaioperasi==null){
				$("#twaktu2").val('00:00:00');
					
				}else{
					
				$("#twaktu2").val(data.waktuselesaioperasi);
				}
				$("#ttanggaloperasi").val(data.tanggaloperasi);
				$("#tdiagnosa").val(data.diagnosa);
				$("#toperasi").val(data.operasi);
				$("#statusdatang").val(data.statusdatang);
				if (data.jenis_operasi_id!='0'){
				$("#tjenis_operasi").val(data.jenis_operasi_id).trigger('change');
					
				}else{
				$("#tjenis_operasi").val(data.def_jenis_opr).trigger('change');
					
				}
				if (data.jenis_anestesi_id!='0'){
				$("#tjenis_anestesi").val(data.jenis_anestesi_id).trigger('change');
				}else{
					
				$("#tjenis_anestesi").val(data.def_jenis_anes).trigger('change');
				}
				if (data.kelompok_operasi_id!='0'){
				$("#tkelompok_operasi").val(data.kelompok_operasi_id).trigger('change');
					
				}else{
				$("#tkelompok_operasi").val(data.def_kel_opr).trigger('change');
					
				}
				$("#tkelompok_tindakan").val(data.kelompok_tindakan_id).trigger('change');
				$("#tkelas_tarif").val(data.kelas_tarif_id).trigger('change');
				$("#truang_operasi").val(data.ruang_id).trigger('change');
				// $("#tkelompok_diagnosa").val(data.kelompk_diagnosa_id).trigger('change');
				$("#tstatus").val(data.status);
				$("#tcatatan").val(data.catatan);
				$("#tnopermintaan").val(data.nopermintaan);
				$("#st_ranap").val(data.st_ranap);
				// alert(data.catatan);
				if (data.status=='2'){
					$("#tjenis_operasi").attr('disabled', true)
					$("#tkelas_tarif").attr('disabled', true)
				}else{
					$("#tjenis_operasi").attr('disabled',false);
					$("#tkelas_tarif").attr('disabled',false);

				}
				$("#cover-spin").hide();
				// alert(data.no_medrec);
				console.log(data);
			}
		});
	}
	function get_pasien_rawat_inap(){
		// alert('sini');
		$("#cover-spin").show();
		let id=$("#tid").val();
		$.ajax({
			url: '{site_url}tkamar_bedah/find_pasien/'+id,
			dataType: "json",
			success: function(data) {
				$("#cover-spin").hide();
				// alert(data.namapasien);
				$("#tidkelompokpasien").val(data.idkelompokpasien);
				$("#tidrekanan").val(data.idrekanan);
				$("#tnomedrec").val(data.no_medrec);
				$("#tdiagnosa").val('');
				$("#tnamapasien").val(data.namapasien);
				$("#tdpjp").val(data.iddokterpenanggungjawab).trigger('change');
				$("#tumur").val(data.umurtahun + ' Tahun '+data.umurbulan + ' Bulan ' + data.umurhari + ' Hari');
				$("#ttanggallahir").val(data.tanggal_lahir);
				$("#ttipe").val(data.tipe);
				$("#tperujuk").val(data.perujuk);
				if (data.idtipe=='1'){
					$("#tkelas").val(data.kelas+' / '+data.bed);
				}else{
					$("#tkelas").val('');
				}
				// $("#tkelompok_pasien").val(data.kelompokpasien);
				// $("#tasuransi").val(data.asuransi);
				$("#label_pembuat").text(data.label_pembuat);
				$("#tidpendaftaran").val(data.idpendaftaran);
				$("#tidasalpendaftaran").val(data.idasalpendaftaran);
				$("#tidpasien").val(data.idpasien);
				$("#statusdatang").val(1);
				$("#tcatatan").val('');
				// $("#tdiagnosa").val('');
				//default
				
				$("#tkelompok_operasi").val(data.def_kel_opr).trigger('change.select2');
				$("#tjenis_operasi").val(data.def_jenis_opr).trigger('change.select2');
				$("#tjenis_bedah").val(data.def_jenis_bedah).trigger('change.select2');
				$("#tjenis_anestesi").val(data.def_jenis_anes).trigger('change.select2');
				$("#tnopermintaan").val(data.nopermintaan);
				
				$("#toperasi").val('');
				$("#ttanggaloperasi").val('');
				$("#twaktu1").val('');
				$("#twaktu2").val('');
				// $("#tkelompok_tindakan").val('0').trigger('change');
				$("#truang_operasi").val('0').trigger('change');
				if (data.idkelas){
					$("#tkelas_tarif").val(data.idkelas).trigger('change');
				}
				load_kelompok_tindakan('0','add');
				load_kelompok_diagnosa('0','add');
				// get_tarif();
				console.log(data);
			}
		});

	}
	$("#tkelompok_operasi,#tjenis_operasi,#tjenis_bedah,#tjenis_anestesi").change(function(){
		get_tarif();
	});

	function get_tarif(){
		let idkelompokpasien=$("#tidkelompokpasien").val();
		let idrekanan=$("#tidrekanan").val();
		let kelompok_operasi=$("#tkelompok_operasi").val();
		let jenis_operasi=$("#tjenis_operasi").val();
		let jenis_bedah=$("#tjenis_bedah").val();
		let jenis_anestesi=$("#tjenis_anestesi").val();
		
		$.ajax({
			url: '{site_url}tkamar_bedah/get_tarif/',
			dataType: "json",
			method: "POST",
			data : {
					idkelompokpasien:idkelompokpasien,
					idrekanan:idrekanan,
					kelompok_operasi:kelompok_operasi,
					jenis_operasi:jenis_operasi,
					jenis_bedah:jenis_bedah,
					jenis_anestesi:jenis_anestesi,
				},

			success: function(data) {
				$("#cover-spin").hide();
				if (data){
				$("#ttarif_jenis_opr").val(data.tarif_jenis_opr).trigger('change');
					
				}else{
					
				$("#ttarif_jenis_opr").val(0).trigger('change');
				}
			}
		});
	}
	function get_pasien_rawat_inap_gabung($id){
		var id = $id;
		$.ajax({
			url: '{site_url}tko/find_pasien/'+id,
			dataType: "json",
			success: function(data) {
				$("#Gnomedrec_pilih").val(data.no_medrec);
				$("#Gnamapasien_pilih").val(data.namapasien);
				$("#Gtanggallahir_pilih").val(data.tanggal_lahir);
				$("#Gtipe_pilih").val(data.tipe);
				$("#Gtipe_id_pilih").val(data.idtipe);
				$("#Gperujuk_pilih").val(data.perujuk);
				$("#Gidpasien_pilih").val(data.idpasien);
				$("#Gidpendaftaran_pilih").val(data.id);
				$("#Gidasalpendaftaran_pilih").val(data.idasalpendaftaran);
				$('#modalAdd').modal('hide');
			}
		});

	}
	function validasi_add(){
		if ($("#ttanggaloperasi").val()==''){
			sweetAlert("Maaf...", "Tanggal Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#twaktu1").val()==''){
			sweetAlert("Maaf...", "Waktu  Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#twaktu2").val()==''){
			sweetAlert("Maaf...", "Waktu  Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#tdiagnosa").val()==''){
			sweetAlert("Maaf...", "Diagnosa Harus diisi!", "error");
			return false;
		}
		if ($("#toperasi").val()==''){
			sweetAlert("Maaf...", "Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#tjenis_operasi").val()=='0'){
			sweetAlert("Maaf...", "Jenis Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#tjenis_bedah").val()=='0'){
			sweetAlert("Maaf...", "Jenis Pembedahan Harus diisi!", "error");
			return false;
		}
		if ($("#tjenis_anestesi").val()=='0'){
			sweetAlert("Maaf...", "Jenis Anestesi Harus diisi!", "error");
			return false;
		}
		if ($("#tkelompok_operasi").val()=='0'){
			sweetAlert("Maaf...", "Kelompok Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#tkelas_tarif").val()=='0'){
			sweetAlert("Maaf...", "Kelas Tarif Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#truang_operasi").val()=='0'){
			sweetAlert("Maaf...", "Ruang Operasi Harus diisi!", "error");
			return false;
		}
		if ($("#ttarif_jenis_opr").val()=='0'){
			sweetAlert("Maaf...", "Tarif Jenis Operasi Harus diisi!", "error");
			return false;
		}

		return true;
	}
	function simpan_jadwal(statussetuju){
		if (validasi_add()){
			swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menyimpan Jadwal Operasi Baru ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
			}).then(function() {
				simpan(statussetuju);
			});


		}
	}
	function simpan(statussetuju){
		var id=$("#tid").val();
		var trxid=$("#trxid").val();
		var tipe=$("#ttipe").val();
		var tjenis_bedah=$("#tjenis_bedah").val();
		var idasalpendaftaran=$("#tidasalpendaftaran").val();
		var idpendaftaran=$("#tidpendaftaran").val();
		var idpasien=$("#tidpasien").val();
		var tanggaloperasi=$("#ttanggaloperasi").val();
		var waktumulaioperasi=$("#twaktu1").val();
		var waktuselesaioperasi=$("#twaktu2").val();
		var diagnosa=$("#tdiagnosa").val();
		var operasi=$("#toperasi").val();
		var jenis_operasi_id=$("#tjenis_operasi").val();
		var jenis_anestesi_id=$("#tjenis_anestesi").val();
		var kelompok_operasi_id=$("#tkelompok_operasi").val();
		var kelompok_tindakan_id=$("#tkelompok_tindakan").val();
		var kelas_tarif_id=$("#tkelas_tarif").val();
		var ruang_id=$("#truang_operasi").val();
		var kelompk_diagnosa_id=$("#tkelompok_diagnosa").val();
		var catatan=$("#tcatatan").val();
		var dpjp_id=$("#tdpjp").val();
		var tnamapasien=$("#tnamapasien").val();
		var ttarif_jenis_opr=$("#ttarif_jenis_opr").val();
		var statusdatang=$("#statusdatang").val();
		var st_ranap=$("#st_ranap").val();
		// alert(kelompk_diagnosa_id);return false;
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tkamar_bedah/simpan_add',
			type: 'POST',
			data: {
				idasalpendaftaran: idasalpendaftaran,idpendaftaran: id,
				idpasien: idpasien,tanggaloperasi: tanggaloperasi,
				waktumulaioperasi: waktumulaioperasi,waktuselesaioperasi: waktuselesaioperasi,
				diagnosa: diagnosa,operasi: operasi,
				jenis_operasi_id: jenis_operasi_id,jenis_anestesi_id: jenis_anestesi_id,
				kelompok_operasi_id: kelompok_operasi_id,kelompok_tindakan_id: kelompok_tindakan_id,
				kelas_tarif_id: kelas_tarif_id,ruang_id: ruang_id,
				kelompk_diagnosa_id: kelompk_diagnosa_id,catatan: catatan,dpjp_id: dpjp_id,namapasien: tnamapasien,
				jenis_bedah: tjenis_bedah,tarif_jenis_opr: ttarif_jenis_opr,statussetuju: statussetuju,trxid: trxid,tipe: tipe,statusdatang: statusdatang,st_ranap: st_ranap,
			},
			complete: function() {
				$("#cover-spin").hide();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Jadwal Berhasil Disimpan'});
				$('#modalTambah').modal('hide');
				if (trxid){
				$('#index_all_2').DataTable().ajax.reload( null, false );
					
				}else{
					
				$('#index_all').DataTable().ajax.reload( null, false );
				}
				// window.location.href = "{site_url}tko/index/"+idunit;
			}
		});

	}
	function load_kelompok_tindakan($id,$jenis){
		// alert($jenis);
		arr_tindakan=[];
		var kt;
			kt=$('#tkelompok_tindakan');
		kt.empty();
		// $('#Tkelompok_tindakan').empty();
		$.ajax({
			url: '{site_url}tko/kelompok_tindakan_list/'+$id,
			dataType: "json",
			success: function(data) {
				 $.each(data, function (i,item) {
					var newOption = new Option(item.text, item.id, true, true);

					kt.append(newOption);
					kt.val(null).trigger('change');
					if (item.selected){
						arr_tindakan.push(item.id);
					}
				});
				setTimeout(function() {
					kt.val(arr_tindakan).trigger('change');
				}, 200);

			}
		});

	}
	function load_kelompok_diagnosa($id,$jenis){
		arr_diagnosa=[];
		var kt;
		// if ($jenis=='edit'){
			// kt=$('#Ekelompok_diagnosa');
		// }else{
			kt=$('#tkelompok_diagnosa');
		// }
		kt.empty();
		// $('#Tkelompok_tindakan').empty();
		$.ajax({
			url: '{site_url}tko/kelompok_diagnosa_list/'+$id,
			dataType: "json",
			success: function(data) {
				 $.each(data, function (i,item) {
					var newOption = new Option(item.text, item.id, false, false);
					kt.append(newOption).trigger('change');
					if (item.selected=='selected'){
						console.log(item.text);
						arr_diagnosa.push(item.id);
					}
					kt.val(null).trigger('change');
				});
				setTimeout(function() {
					kt.val(arr_diagnosa).trigger('change');
				}, 200);

			}
		});
	}
</script>