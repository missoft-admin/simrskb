<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Registrasi</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_reg" placeholder="No Registrasi" name="no_reg" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Medrec</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_medrec" placeholder="No. Medrec" name="no_medrec" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="nama_pasien" placeholder="Nama Pasien" name="nama_pasien" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Tipe Tagihan</label>
                    <div class="col-md-8">
                        <select id="tipe_tagihan" name="tipe_tagihan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Tipe -</option>
							<option value="1">Tagihan Pegawai</option>
							<option value="2">Tagihan Dokter</option>
							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Karyawan  / Dokter</label>
                    <div class="col-md-8">
                        <select id="peg_id" name="peg_id" class="form-control" style="width: 100%;" data-placeholder="Choose one..">
							
						</select>
                    </div>
                </div>
                 <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label text-left" id="lbl_warning" for=""></label>
                    <div class="col-md-8">
						<div class="alert alert-success alert-dismissable" id="label_warning">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<p><strong>Silahkan Tentukan Filter Pencarian selanjutnya  Klik Tombol Filter</strong></p>
						</div>
					</div>
                </div>               
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tipe</label>
                    <div class="col-md-8">
						<select id="tipe" name="tipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Tipe -</option>
							<option value="1" <?=("1" == $tipe ? 'selected' : ''); ?>>Rawat Jalan</option>
							<option value="2" <?=("2" == $tipe ? 'selected' : ''); ?>>Ranap / ODS</option>
							<option value="3" <?=("4" == $tipe ? 'selected' : ''); ?>>Pembelian Obat</option>
							
						</select>
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Status</label>
                    <div class="col-md-8">
                        <select id="status" name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Status -</option>
							<option value="0" <?=("0" == $status ? 'selected' : ''); ?>>Menunggu Proses</option>
							<option value="1" <?=("1" == $status ? 'selected' : ''); ?>>Telah Diverifikasi</option>
							<option value="2" <?=("2" == $status ? 'selected' : ''); ?>>Telah Diproses</option>
							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Poliklinik</label>
                    <div class="col-md-8">
                        <select id="idpoliklinik" name="idpoliklinik" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Poliklinik -</option>
							<?foreach($list_poli as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Dokter</label>
                    <div class="col-md-8">
                        <select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Dokter -</option>
							<?foreach($list_dokter as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>							
						</select>
                    </div>
                </div>
              
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Transaksi</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tgl_trx1" name="tgl_trx1" placeholder="From" value="{tgl_trx1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tgl_trx2" name="tgl_trx2" placeholder="To" value="{tgl_trx2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Daftar</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggaldaftar1" name="tanggaldaftar1" placeholder="From" value="{tanggaldaftar1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggaldaftar2" name="tanggaldaftar2" placeholder="To" value="{tanggaldaftar2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    
					<div class="col-md-4">
                        <button class="btn btn-primary text-uppercase" type="button" name="btn_verif_page" id="btn_verif_page" style="font-size:13px;width:100%;"><i class="fa fa-check"></i> Verifikasi Per page</button>
                    </div>
					<div class="col-md-4">
                        <button class="btn btn-danger text-uppercase" type="button" name="btn_proses_page" id="btn_proses_page" style="font-size:13px;width:100%;"><i class="si si-arrow-right"></i> Proses Per Page</button>
                    </div>
                </div>
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th hidden width="5%">TIPE</th>
                    <th hidden width="2%">ID</th>
                    <th width="10%">NO</th>
                    <th width="10%">TGL KUNJUGAN</th>
                    <th width="10%">TGR TRX</th>
                    <th width="10%">JENIS</th>
                    <th width="10%">NO REG</th>
                    <th width="10%">MEDREC</th>
                    <th width="10%">PASIEN</th>
                    <th width="15%">JENIS TAGIHAN</th>
                    <th width="15%">NAMA TAGIHAN</th>
                    <th width="15%">STATUS</th>
                    <th width="15%">NOMINAL</th>
                    <th width="15%">CARA BAYAR</th>
                    <th width="15%">DESK</th>
                    <th width="15%"> TGL PENAGIHAN</th>
                    <th width="15%" class="text-center">AKSI</th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Pengalihan Tagihan</h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">
							<label class="col-md-4 control-label" for="status">Tanggal Asal</label>
							<div class="col-md-8">
							<div class="input-group">
								<input class="form-control" readonly type="text" id="tgl_asal" name="tgl_asal" value="">

								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
							</div>
						</div> 	
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Jenis Tagihan</label>
								<div class="col-md-8">
									<select id="xtipepegawai" name="xtipepegawai" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="1">Tagihan Karyawan</option>	
										<option value="2">Tagihan Dokter</option>	
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Nama Dokter / Karyawan</label>
								<div class="col-md-8">
									<select id="xidpeg_dok" name="xidpeg_dok" class="form-control" style="width: 100%;" data-placeholder="Choose one..">
										
									</select>
								</div>
							</div>				
						</div>		
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Tanggal</label>
								<div class="col-md-8">
									<select id="xtgl_pilih" name="xtgl_pilih" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										
									</select>
								</div>
							</div>				
						</div>				
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="xid" placeholder="No. PO" name="xid" value="">
								<input type="hidden" class="form-control" id="xtanggal_tagihan" placeholder="No. PO" name="xtanggal_tagihan" value="">
								<input type="hidden" class="form-control" id="xtipe" placeholder="No. PO" name="xtipe" value="">
								
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_ubah" id="btn_ubah"><i class="fa fa-save"></i> SIMPAN</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_cara_bayar" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Cara Pembayaran Tagihan </h3>
                </div>
                <div class="block-content">
					<div class="row">
						
						
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Nominal</label>
								<div class="col-md-8">
									<input class="form-control number" readonly type="text" id="xnominal" name="xnominal" value="">
								</div>
							</div>				
						</div>
						<div class="col-md-12" style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">Tanggal Tagihan</label>
							<div class="col-md-8">
							<div class="input-group">
								<input class="form-control" readonly type="text" id="tgl_start" name="tgl_start" value="">

								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
							</div>
						</div>
						
						
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Cara Bayar</label>
								<div class="col-md-8">
									<select id="xcara_bayar" name="xcara_bayar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="1">Satu Kali Pembayaran</option>	
										<option value="2">Cicilan</option>	
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Jumlah Cicilan</label>
								<div class="col-md-8">
									<input class="form-control number" type="text" id="xjml_cicilan" name="xjml_cicilan" value="">
								</div>
							</div>				
						</div>
						<div class="col-md-12 div_cara2">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Cara Penagihan</label>
								<div class="col-md-8">
									<select id="xcara_penagihan" name="xcara_penagihan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="1">Tanggal Sama disetiap Bulannya</option>	
										<option value="2">Sesuai Periode Pengaturan</option>	
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12 div_cara3">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Nominal Per Bulan</label>
								<div class="col-md-8">
									<input class="form-control number" readonly type="text" id="xperbulan" name="xperbulan" value="">
								</div>
							</div>				
						</div>
						<div class="col-md-12 div_cara3">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Dibayarkan setiap tanggal</label>
								<div class="col-md-8">
									<select disabled id="xsetiap_bulan" name="xsetiap_bulan[]" class="js-select2 form-control" multiple style="width: 100%;" data-placeholder="Choose one..">
									</select>
								</div>
							</div>				
						</div>
					</div>
					
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="xid_detail" placeholder="No. PO" name="xid_detail" value="">
								<input type="hidden" class="form-control" id="xtanggal_tagihan2" placeholder="No. PO" name="xtanggal_tagihan2" value="">
								<input type="hidden" class="form-control" id="xidpengaturan2" placeholder="No. PO" name="xidpengaturan2" value="">
								<input type="hidden" class="form-control" id="xtanggal_id2" placeholder="No. PO" name="xtanggal_id2" value="">
								<input type="hidden" class="form-control" id="xtipe2" placeholder="No. PO" name="xtipe2" value="">
								
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_save_cicilan" id="btn_save_cicilan"><i class="fa fa-save"></i> SIMPAN</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	// load_index();
  
});
 $("#peg_id").select2({
	minimumInputLength: 2,
	noResults: 'Dokter Tidak Ditemukan.',
	ajax: {
		url: '{site_url}tpiutang_verifikasi/js_pegawai_dokter/',
		dataType: 'json',
		type: "POST",
		quietMillis: 50,
	 data: function (params) {
		  var query = {
			search: params.term,tipe:$("#tipe_tagihan").val()
		  }
		  return query;
		},
		processResults: function (data) {
			return {
				results: $.map(data, function (item) {
					return {
						text: item.nama ,
						id: item.id
					}
				})
			};
		}
	}
});
$("#xidpeg_dok").select2({
	minimumInputLength: 2,
	noResults: 'Dokter Tidak Ditemukan.',
	ajax: {
		url: '{site_url}tpiutang_verifikasi/js_pegawai_dokter/',
		dataType: 'json',
		type: "POST",
		quietMillis: 50,
	 data: function (params) {
		  var query = {
			search: params.term,tipe:$("#xtipepegawai").val()
		  }
		  return query;
		},
		processResults: function (data) {
			return {
				results: $.map(data, function (item) {
					return {
						text: item.nama ,
						id: item.id
					}
				})
			};
		}
	}
});
function load_index(){
	var no_reg=$("#no_reg").val();
	var no_medrec=$("#no_medrec").val();
	var nama_pasien=$("#nama_pasien").val();
	var idkelompokpasien=$("#idkelompokpasien").val();
	var idrekanan=$("#idrekanan").val();
	var tipe=$("#tipe").val();
	var status=$("#status").val();
	var tgl_trx1=$("#tgl_trx1").val();
	var tgl_trx2=$("#tgl_trx2").val();
	var tanggaldaftar1=$("#tanggaldaftar1").val();
	var tanggaldaftar2=$("#tanggaldaftar2").val();
	var tipe_tagihan=$("#tipe_tagihan").val();
	var peg_id=$("#peg_id").val();
	var idpoliklinik=$("#idpoliklinik").val();
	var iddokter=$("#iddokter").val();
	$('#index_list').DataTable().destroy();
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0,1,17,18,19,20,21,22,23], "visible": false },
							{ "width": "3%", "targets": [2] },
							{ "width": "5%", "targets": [5,7] },
							{ "width": "5%", "targets": [3,4] },
							{ "width": "7%", "targets": [6,15,11,12,13,14] },
							{ "width": "8%", "targets": [8,,9,10] },
							{ "width": "15%", "targets": [16] },
						 {"targets": [8,16], className: "text-left" },
						 {"targets": [2,12,13,14,15], className: "text-right" },
						 {"targets": [3,4,5,6,7,9,10,11], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tpiutang_verifikasi/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						no_reg:no_reg,no_medrec:no_medrec,nama_pasien:nama_pasien,idkelompokpasien:idkelompokpasien,
						idrekanan:idrekanan,tipe:tipe,status:status,tgl_trx1:tgl_trx1,tgl_trx2:tgl_trx2,
						tanggaldaftar1:tanggaldaftar1
						,tanggaldaftar2:tanggaldaftar2
						,tipe_tagihan:tipe_tagihan
						,peg_id:peg_id
						,idpoliklinik:idpoliklinik
						,iddokter:iddokter
					   }
            }
        });
}
$("#btn_filter").click(function() {
	// alert('ID');
	// table.destroy();
	load_index();
});
$("#btn_save_cicilan").click(function() {//Simpan Cicilan
	if ($("#xcara_bayar").val()=='2' && $("#xjml_cicilan").val() < 2){
		sweetAlert("Maaf...", "Cicilan Harus lebih dari satu kali!", "error");
		return false;
	}
	// if ($("#xcara_bayar").val()=='1' && $("#xjml_cicilan").val() >1){
		// sweetAlert("Maaf...", "Cicilan Harus lebih dari satu kali!", "error");
		// return false;
	// }
	swal({
		title: "Anda Yakin ?",
		text : "Akan Mengganti Cara Pembayaran ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		save_cicilan();
	});
});
function save_cicilan(){
	var tgl=$("#tgl_start").val();
	var id_detail=$("#xid_detail").val();
	var jml=$("#xjml_cicilan").val();
	var xperbulan=$("#xperbulan").val();
	var xcara_bayar=$("#xcara_bayar").val();
	var tipe=$("#xtipe2").val();
	// alert(jml);
	var cara=$("#xcara_penagihan").val();
	var idpengaturan=$("#xidpengaturan2").val();
	var tanggal_id=$("#xtanggal_id2").val();
	var xsetiap_bulan=$("#xsetiap_bulan").val();
	// alert(xsetiap_bulan);return false;
	table = $('#index_list').DataTable()	
	
	// if (xcara_bayar=='2'){
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpiutang_verifikasi/save_cicilan/',
			dataType: "json",
			type: 'POST',
			data: {
				tgl: tgl,
				jml: jml,
				cara_penagihan: cara,
				idpengaturan: idpengaturan,
				id_detail: id_detail,
				xperbulan: xperbulan,
				tanggal_id: tanggal_id,
				cara_bayar: xcara_bayar,
				xsetiap_bulan: xsetiap_bulan,
				tipe: tipe,
				},
			success: function(data) {
				console.log(data);
				$('#modal_cara_bayar').modal('hide');
				$.toaster({priority : 'success', title : 'Succes!', message : ' Proses Pembayaran'});
				table.ajax.reload( null, false ); 
				$("#cover-spin").hide();
			}
		});
	// }else{
		// $('#modal_cara_bayar').modal('hide');
		// $.toaster({priority : 'success', title : 'Succes!', message : ' Edit Cara Pembayaran'});
	// }
}
$(document).on("click", ".verifikasi", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var tipe=table.cell(tr,0).data()
	var noreg=table.cell(tr,6).data()
	var tanggal_id=table.cell(tr,17).data()
	var idpengaturan=table.cell(tr,18).data()
	var tanggal_jt=table.cell(tr,15).data()
	// alert(tanggal_jt);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Verifikasi No. Registrasi "+noreg+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		verifikasi(id,tipe,tanggal_id,idpengaturan,tanggal_jt);
	});
});
$(document).on("click", "#btn_verif_page", function() {
	swal({
		title: "Anda Yakin ?",
		text : "Akan Verifikasi Page Ini ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		verifikasi_semua();
		// verifikasi(id,tipe,tanggal_id,idpengaturan,tanggal_jt);
	});
	
	 

});
function verifikasi_semua(){
	var table = $('#index_list').DataTable();		
	 var data = table.rows().data();
	 var arr_id=[];
	 var arr_tipe=[];
	 var arr_tanggal_id=[];
	 var arr_idpengaturan=[];
	 var arr_tanggal_jt=[];
	 data.each(function (value, index) {
			// alert('ID : '+table.cell(index,22).data());return false;
			console.log(table.cell(index,11).data());
		 if (table.cell(index,11).data()=='<span class="label label-default">MENUNGGU DIPROSES</span>' && table.cell(index,15).data() !='<span class="label label-danger">BELUM DISETTING</span>'){
			arr_id.push(table.cell(index,1).data()); 
			arr_tipe.push(table.cell(index,0).data()); 
			arr_tanggal_id.push(table.cell(index,17).data()); 
			arr_idpengaturan.push(table.cell(index,18).data()); 
			arr_tanggal_jt.push(table.cell(index,15).data()); 
		 }
		
	 });
	 // alert(arr_id);
	 if (arr_id){
		 $("#cover-spin").show();
		 // alert(arr_tanggal_jt);return false;
		$.ajax({
			url: '{site_url}tpiutang_verifikasi/verif_semua',
			type: 'POST',
			data: {
					arr_id: arr_id,
					arr_tipe: arr_tipe,
					arr_tanggal_id: arr_tanggal_id,
					arr_idpengaturan: arr_idpengaturan,
					arr_tanggal_jt: arr_tanggal_jt,
				 },
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Selesai Verifikasi'});
				$('#index_list').DataTable().ajax.reload(null, false )
				$("#cover-spin").hide();
			}
		});
	 }
}
$(document).on("click", "#btn_proses_page", function() {
	swal({
		title: "Anda Yakin ?",
		text : "Akan Generate Pembayaran Page Ini ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		generate_semua();
		// verifikasi(id,tipe,tanggal_id,idpengaturan,tanggal_jt);
	});
});
function generate_semua(){
	var table = $('#index_list').DataTable();		
	 var data = table.rows().data();
	 var arr_id=[];
	 var arr_tipe=[];
	 var arr_tanggal_id=[];
	 var arr_idpengaturan=[];
	 var arr_tanggal_jt=[];
	 data.each(function (value, index) {
		 console.log(table.cell(index,11).data());
			// alert('ID : '+table.cell(index,9).data());return false;
		 if (table.cell(index,11).data() =='<span class="label label-primary">TELAH DIVERIFIKASI</span>'){
			arr_id.push(table.cell(index,1).data()); 
			arr_tipe.push(table.cell(index,0).data()); 
			arr_tanggal_id.push(table.cell(index,17).data()); 
			arr_idpengaturan.push(table.cell(index,18).data()); 
			arr_tanggal_jt.push(table.cell(index,15).data()); 
		 }
		
	 });
	 // alert(arr_id);return false;
	 if (arr_id){
		 $("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpiutang_verifikasi/generate_semua',
			type: 'POST',
			data: {
					arr_id: arr_id,
					arr_tipe: arr_tipe,
					arr_tanggal_id: arr_tanggal_id,
					arr_idpengaturan: arr_idpengaturan,
					arr_tanggal_jt: arr_tanggal_jt,
				 },
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Selesai Generate Tagihan'});
				$('#index_list').DataTable().ajax.reload()
				$("#cover-spin").hide();
			}
		});
	 }
}
function verifikasi($id,$tipe,$tanggal_id,$idpengaturan,$tanggal_jt){
	console.log($id);
	var id=$id;		
	var tipe=$tipe;		
	var tanggal_id=$tanggal_id;		
	var idpengaturan=$idpengaturan;		
	var tanggal_jt=$tanggal_jt;		
	$("#cover-spin").show();
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tpiutang_verifikasi/verifikasi',
		type: 'POST',
		data: {id: id,tipe:tipe,tanggal_id:tanggal_id,idpengaturan:idpengaturan,tanggal_jt:tanggal_jt},
		complete: function(data) {
			$("#cover-spin").hide();
			console.log(data);
			$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi'});
			table.ajax.reload( null, false ); 
			
		}
	});
}

$(document).on("click", ".ganti", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var tipepegawai=table.cell(tr,19).data()
	var nama=table.cell(tr,10).data()
	$("#xid").val(id);
	// $("#xtanggal_id").val(table.cell(tr,17).data());
	// $("#xidpengaturan").val(table.cell(tr,18).data());
	$("#xtipe").val(table.cell(tr,0).data()).trigger('change');
	$("#xtipepegawai").val(tipepegawai).trigger('change');
		
	if (tipepegawai=='1'){
		var newOption = new Option(nama, table.cell(tr,20).data(), true, true);
	}else{
		var newOption = new Option(nama, table.cell(tr,21).data(), true, true);
		
	}
	$('#xidpeg_dok').append(newOption);
	// $("#xidrekanan").val(table.cell(tr,21).data()).trigger('change');
	$("#tgl_asal").val(table.cell(tr,15).data());
	$("#xtanggal_tagihan").val(table.cell(tr,15).data()).trigger('change');
	load_list_tanggal();
	$('#modal_edit').modal('show');
	
});
$(document).on("click", ".cara_tagihan", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var tipe=table.cell(tr,0).data()
	var nama=table.cell(tr,10).data()
	$.ajax({
		url: '{site_url}tpiutang_verifikasi/get_data_kirim/'+id+'/'+tipe,
		dataType: "json",
		success: function(data) {
			// alert(data.detail.tanggal_jatuh_tempo);
			$("#xtipe2").val(tipe);
			$("#tgl_start").val(data.detail.tanggal_jatuh_tempo);
			$("#xtanggal_tagihan2").val(data.detail.tanggal_jatuh_tempo);
			$("#xnominal").val(data.detail.nominal);
			$("#xjml_cicilan").val(data.detail.jml_cicilan);
			$("#xidpengaturan2").val(data.detail.idpengaturan);
			$("#xtanggal_id2").val(data.detail.tanggal_id);
			$("#xcara_bayar").val(data.detail.cara_bayar).trigger('change');
			$("#xcara_penagihan").val(data.detail.cara_penagihan).trigger('change');				
			$("#xid_detail").val(id);
			hitung_cicilan();
			$('#modal_cara_bayar').modal('show');
		}
	});
	
});
function load_list_tanggal(){
	var tipepegawai=$("#xtipepegawai").val();
	var idpeg_dok=$("#xidpeg_dok").val();
	// alert(idpeg_dok);
	if (tipepegawai && idpeg_dok){
		$.ajax({
			url: '{site_url}tpiutang_verifikasi/list_tanggal/'+tipepegawai+'/'+idpeg_dok,
			dataType: "json",
			success: function(data) {
				$("#xtgl_pilih").empty();
				$('#xtgl_pilih').append(data.detail);
			}
		});
	}
}
$(document).on("keyup", "#xjml_cicilan", function() {
	
	hitung_cicilan();
});
$(document).on("change", "#xcara_bayar", function() {
	if ($("#xcara_bayar").val()=='1'){
		$(".div_cara2").hide();
		$("#xjml_cicilan").val(1);
		$("#xjml_cicilan").attr('readonly',true);
		hitung_cicilan();
		simulasi();
	}else{
		$(".div_cara2").show();
		$("#xjml_cicilan").attr('readonly',false);
		hitung_cicilan();
		simulasi();
	}		
});
$(document).on("change", "#xcara_penagihan", function() {
	
		simulasi();
			
});
function hitung_cicilan(){
	var perbulan=0;
	perbulan=parseFloat($("#xnominal").val()) /parseFloat($("#xjml_cicilan").val()) ;
	$("#xperbulan").val(perbulan);
	simulasi();
}
function simulasi(){
	var tgl=$("#tgl_start").val();
	var jml=$("#xjml_cicilan").val();
	// alert(jml);
	var cara=$("#xcara_penagihan").val();
	var idpengaturan=$("#xidpengaturan2").val();
	
	$.ajax({
		url: '{site_url}tpiutang_verifikasi/simulasi/',
		dataType: "json",
		type: 'POST',
		data: {
			tgl: tgl,
			jml: jml,
			cara: cara,
			idpengaturan: idpengaturan,
			},
		success: function(data) {
			$("#xsetiap_bulan").empty();
			$('#xsetiap_bulan').append(data.detail);
		}
	});
}
$(document).on("change", "#xtipepegawai", function() {
	$("#xidpeg_dok").val("#").trigger('change');	
});
$(document).on("change", "#tipe_tagihan", function() {
	$("#peg_id").val(null).trigger('change');	
});
$(document).on("change", "#xidpeg_dok", function() {
	load_list_tanggal();
	
});
$(document).on("click", "#btn_ubah", function() {
		if ($("#xtgl_pilih").val()=='' || $("#xtgl_pilih").val()==null){
			sweetAlert("Maaf...", "Tanggal belum dipilih!", "error");
			return false;
		}
		// alert();
		if ($("#xtipepegawai").val()=='' || $("#xtipepegawai").val()==null){
			sweetAlert("Maaf...", "Tipe Tagihan Harus diisi!", "error");
			return false;
		}
		if ($("#xidpeg_dok").val()=='' || $("#xidpeg_dok").val()==null){
			sweetAlert("Maaf...", "Nama Pegawai / Dokter Harus diisi!", "error");
			return false;
		}
		
		swal({
			title: "Anda Yakin ?",
			text : "Akan Memindahkan Faktur ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			edit_tanggal_satu();
		});
		
});
function edit_tanggal_satu(){
	var piutang_id_detail=$("#xid").val();		
	var tanggal_tagihan=$("#xtgl_pilih").val();		
	var xtipepegawai=$("#xtipepegawai").val();		
	var xidpeg_dok=$("#xidpeg_dok").val();		
	var tipe=$("#xtipe").val();		
	// alert(klaim_id+' tg l'+tanggal_tagihan+' Kel '+idkelompokpasien+' rekan '+idrekanan+' tipe '+tipe);
	// return false;
	var table = $('#index_list').DataTable();
	$.ajax({
		url: '{site_url}tpiutang_verifikasi/edit_tanggal_satu',
		type: 'POST',
		data: {piutang_id_detail: piutang_id_detail,tanggal_tagihan:tanggal_tagihan,xtipepegawai:xtipepegawai,xidpeg_dok:xidpeg_dok,tipe:tipe},
		complete: function() {

			$.toaster({priority : 'success', title : 'Succes!', message : ' Memindahkan Tanggal Jatuh Tempo'});
			$("#modal_edit").modal('hide');
			$('#index_list').DataTable().ajax.reload( null, false );
			$("#cover-spin").hide();
		}
	});
}
</script>