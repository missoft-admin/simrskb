<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work-1"'); ?>
                <div class="col-md-6"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Nomor Tagihan</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{no_tagihan}" disabled>
                            <input class="form-control" type="hidden" name="disabel" id="disabel" value="{disabel}" >
                        </div> 
                    </div>
                      
                </div>
				<div class="col-md-6" style="margin-bottom: 5px;"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Tanggal Penagihan</label> 
                        <div class="col-md-4"> 
                            <input class="form-control" disabled id="tanggal_jatuh_tempo" value="<?=HumanDateShort($tanggal_jatuh_tempo)?>" disabled>
                        </div> 
                    </div>
                   
                    
                </div>
            <?php echo form_close(); ?>
        </div>
        
        <div class="form-group" style="margin-top: 20px;"></div>
    </div>
</div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">DETAIL TAGIHAN</h3>
    </div>
    <div class="block-content">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Registrasi</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_reg" placeholder="No Registrasi" name="no_reg" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Medrec</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_medrec" placeholder="No. Medrec" name="no_medrec" value="">
                    </div>
                </div>
				
                
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="nama_pasien" placeholder="Nama Pasien" name="nama_pasien" value="">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Daftar</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tgl_trx" name="tgl_trx" placeholder="From" value=""/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tgl_trx2" name="tgl_trx2" placeholder="To" value=""/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
			</div>
            <?php echo form_close(); ?>
        </div>
        <div class="form-group" style="margin-bottom: 30px;"></div>
		<div class="table-responsive">
        <table class="table table-bordered table-striped table-responsive" id="index_list">
            <thead>
                <tr>
                    <th width="0%" class="text-center"></th>
                    <th width="0%" class="text-center"></th>
                    <th width="0%" class="text-center"></th>
                    <th width="0%" class="text-center"></th>
                    <th width="10%">NO</th>
                    <th width="10%">TGL KUNJUGAN / NO. REG</th>
                    <th width="10%">TGR TRX</th>
                    <th width="10%">JENIS</th>
                    <th width="10%">MEDREC</th>
                    <th width="10%">PASIEN</th>
                    <th width="15%">KEL PASIEN</th>
                    <th width="15%">TOT TRX</th>
                    <th width="15%">TIDAK TERTAGIH</th>
                    <th width="15%">BAYAR LAINNYA</th>
                    <th width="15%">JAMINAN</th>
                    <th width="15%">STATUS</th>
                    <th width="15%">JATUH TEMPO</th>
                    <th width="15%" class="text-center">AKSI</th>
                </tr>
            </thead>
            <tbody> </tbody>
              
        </table>
    </div>
        <div class="form-group" style="margin-top: 20px;"></div>
    </div>
</div>
<div class="modal fade" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Pengalihan Jatuh Tempo </h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">						
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="status">Nominal</label>
								<div class="col-md-8">
										<input class="form-control input decimal" disabled type="text" id="xnominal2" name="xnominal2" value="">
									
									
								</div>
							</div>				
						</div>	
						<div class="col-md-12" style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">Tanggal Asal</label>
							<div class="col-md-8">
							<div class="input-group">
								<input class="form-control" readonly type="text" id="tgl_asal" name="tgl_asal" value="<?=HumanDateShort($tanggal_jatuh_tempo)?>">

								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
							</div>
						</div> 	
							
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Tanggal Edit</label>
								<div class="col-md-8">
									<div class="input-group">
										<input class="js-datepicker form-control input" type="text" id="xtgl_pilih" name="xtgl_pilih" value="<?=HumanDateShort($tanggal_jatuh_tempo)?>" data-date-format="dd-mm-yyyy">

										<span class="input-group-addon">
											<span class="fa fa-calendar"></span>
										</span>
									</div>
									
								</div>
							</div>				
						</div>	
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Alasan Ganti</label>
								<div class="col-md-8">
										<input class="form-control input" type="text" id="xalasan" name="xalasan" value="">
									
									
								</div>
							</div>				
						</div>				
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="xid" placeholder="No. PO" name="xid" value="">
								<input type="hidden" class="form-control" id="xtanggal_tagihan" placeholder="No. PO" name="xtanggal_tagihan" value="<?=HumanDateShort($tanggal_jatuh_tempo)?>">
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_ubah" id="btn_ubah"><i class="fa fa-save"></i> SIMPAN</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_fu" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:60%">
        <div class="modal-content" >
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary">
                    <h3 class="block-title">FOLLOW UP PIUTANG </h3>
                </div>
                <div class="block-content">
					<div class="row">
						
						<div class="col-md-12" style="margin-top: 15px;">
							<h4><span class="label label-primary">KETERANGAN / INFORMASI </span></h4>
						</div>
						<div class="col-md-12" style="margin-top: 15px;">
							<label class="col-md-2 control-label" for="status">Tanggal Informasi</label>
							<div class="col-md-4">
								<div class="js-datepicker input-group date" data-date-format="dd-mm-yyyy">
									<input class="form-control" type="text" id="fu_tanggal_info" name="fu_tanggal_info" placeholder="Tanggal FU" value="<?=date('d-m-Y')?>"/>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
							<label class="col-md-2 control-label" for="status">Tanggal Reminder</label>
							<div class="col-md-4">
								<div class="js-datepicker input-group date" data-date-format="dd-mm-yyyy">
									<input class="form-control" type="text" id="fu_tanggal_reminder" name="fu_tanggal_reminder" placeholder="Tanggal Reminder" value=""/>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
						</div>
						
						<div class="col-md-12" style="margin-top: 5px;">	
							<label class="col-md-2 control-label" for="status">Keterangan</label>
								<div class="col-md-10">
										<textarea class="form-control js-summernote" id="keterangan"></textarea>
								</div>				
						</div>
						<div class="col-md-12" style="margin-top: 5px;">	
							<label class="col-md-2 control-label" for="status"></label>
								<div class="col-md-10">
									<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_tambah_fu" id="btn_tambah_fu"><i class="fa fa-plus"></i> Tambah</button>
							</div>				
						</div>
						
						<div class="col-md-12" style="margin-top: 5px;">
							<div class="block block-themed block-opt-hidden" id="div_kunjungan">
								<div class="block-header bg-primary">
									<ul class="block-options">
										<li>
											<button id="button_up_kunjungan" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-down"></i></button>
										</li>
									</ul>
									<h3 class="block-title"><i class="si si-login"></i>  Riwayat Follow Up</h3>
								</div>
								<div class="block-content block-content">
									<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
										<div class="table-responsive">
										<table class="table table-bordered table-striped" id="index_informasi">
											<thead>
												<tr>
													<th hidden width="2%">ID</th>
													<th width="10%">NO</th>
													<th width="10%">TANGGAL</th>
													<th width="10%">INFORMASI</th>
													<th width="10%">USER</th>
													<th width="10%">REMINDER</th>
													<th width="10%">AKSI</th>											
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
								</div>
									</form>
								</div>
							</div>
						</div>
						<div class="col-md-12" style="margin-top: 5px;">
							<div class="block block-themed block-opt-hidden" id="div_kunjungan">
								<div class="block-header bg-primary">
									<ul class="block-options">
										<li>
											<button id="button_up_kunjungan" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-down"></i></button>
										</li>
									</ul>
									<h3 class="block-title"><i class="fa fa-file-photo-o"></i>  Document</h3>
								</div>
								<div class="block-content block-content">
									<form action="{base_url}tpiutang_tt_monitoring/upload_bukti" enctype="multipart/form-data" class="dropzone" id="dropzone_semua">
										<input type="hidden" class="form-control" id="upload_piutang_id" placeholder="" name="upload_piutang_id" value="">
										<div>
										  <h5>DOCUMENT</h5>
										</div>
									</form>
									<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
										<div class="table-responsive">
										<table class="table table-bordered table-striped" id="index_dokumen">
											<thead>
												<tr>
													<th hidden width="2%">ID</th>
													<th width="10%">NO</th>
													<th width="10%">FILE</th>
													<th width="10%">Size</th>
													<th width="10%">AKSI</th>											
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
								</div>
									</form>
								</div>
							</div>
						</div>
						
						<div class="col-md-12" style="margin-top: 5px;">	
							<div class="block-content">
								
							</div>				
						</div>
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="fu_piutang_id" placeholder="No. PO" name="fu_piutang_id" value="">
								
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_cara_bayar" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Cara Pembayaran Tagihan </h3>
                </div>
                <div class="block-content">
					<div class="row">
						
						
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Nominal</label>
								<div class="col-md-8">
									<input class="form-control decimal" readonly type="text" id="xnominal" name="xnominal" value="">
								</div>
							</div>				
						</div>
						<div class="col-md-12" style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">Tanggal Tagihan</label>
							<div class="col-md-8">
							<div class="input-group">
								<input class="form-control" readonly type="text" id="tgl_start" name="tgl_start" value="">

								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
							</div>
						</div>
						
						
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Cara Bayar</label>
								<div class="col-md-8">
									<select id="xcara_bayar" name="xcara_bayar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="1">Satu Kali Pembayaran</option>	
										<option value="2">Cicilan</option>	
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Jumlah Cicilan</label>
								<div class="col-md-8">
									<input class="form-control number" type="text" id="xjml_cicilan" name="xjml_cicilan" value="">
								</div>
							</div>				
						</div>
						
						<div class="col-md-12 div_cara3">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Nominal Per Bulan</label>
								<div class="col-md-8">
									<input class="form-control decimal" readonly type="text" id="xperbulan" name="xperbulan" value="">
								</div>
							</div>				
						</div>
						<div class="col-md-12 div_cara3">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Dibayarkan setiap tanggal</label>
								<div class="col-md-8">
									<select disabled id="xsetiap_bulan" name="xsetiap_bulan[]" class="js-select2 form-control" multiple style="width: 100%;" data-placeholder="Choose one..">
									</select>
								</div>
							</div>				
						</div>
					</div>
					
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="xid_detail" placeholder="No. PO" name="xid_detail" value="">
								<input type="hidden" class="form-control" id="xtanggal_tagihan2" placeholder="No. PO" name="xtanggal_tagihan2" value="">
								
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_save_cicilan" id="btn_save_cicilan"><i class="fa fa-save"></i> SIMPAN</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">


    var table;
	var myDropzone 
    $(document).ready(function(){
			// $('.decimal').number(true, 2);
			$(".decimal").number(true,2,'.',',');
			$(".number").number(true,0,'.',',');
			// $('.number').number(true, 2);
		Dropzone.autoDiscover = false;
		myDropzone = new Dropzone(".dropzone", { 
		   autoProcessQueue: true,
		   maxFilesize: 30,
		});
		myDropzone.on("complete", function(file) {	  
		  myDropzone.removeFile(file);
		  $('#index_dokumen').DataTable().ajax.reload( null, false );
		});
		$('.js-summernote').summernote({
		  height: 50,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }
		});
		load_rincian();
	
    });
	$(document).on("click", ".cara_tagihan", function() {
		var table = $('#index_list').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()
		var tipe=table.cell(tr,3).data()
		$.ajax({
			url: '{site_url}tpiutang_tt_monitoring/get_data_kirim/'+id+'/'+tipe,
			dataType: "json",
			success: function(data) {
				// alert(data.detail.nominal_tagihan);
				$("#xtipe2").val(tipe);
				$("#tgl_start").val(data.detail.tanggal_jatuh_tempo2);
				$("#xtanggal_tagihan2").val(data.detail.tanggal_jatuh_tempo);
				$("#xnominal").val(data.detail.nominal_tagihan);
				$("#xjml_cicilan").val(data.detail.jml_cicilan);
				$("#xcara_bayar").val(data.detail.cara_bayar).trigger('change');
				// $("#xcara_penagihan").val(data.detail.cara_penagihan).trigger('change');				
				$("#xid_detail").val(id);
				hitung_cicilan();
				$('#modal_cara_bayar').modal('show');
			}
		});
		
	});
	$(document).on("change", "#xcara_bayar", function() {
		if ($("#xcara_bayar").val()=='1'){
			$(".div_cara2").hide();
			$("#xjml_cicilan").val(1);
			$("#xjml_cicilan").attr('readonly',true);
			hitung_cicilan();
			simulasi();
		}else{
			$(".div_cara2").show();
			$("#xjml_cicilan").attr('readonly',false);
			hitung_cicilan();
			simulasi();
		}		
	});
	function hitung_cicilan(){
		var perbulan=0;
		perbulan=parseFloat($("#xnominal").val()) /parseFloat($("#xjml_cicilan").val()) ;
		$("#xperbulan").val(perbulan);
		simulasi();
	}
	$(document).on("keyup", "#xjml_cicilan", function() {
	
		hitung_cicilan();
	});
	function simulasi(){
		var tgl=$("#tgl_start").val();
		var jml=$("#xjml_cicilan").val();
		
		$.ajax({
			url: '{site_url}tpiutang_tt_monitoring/simulasi/',
			dataType: "json",
			type: 'POST',
			data: {
				tgl: tgl,
				jml: jml,
				},
			success: function(data) {
				$("#xsetiap_bulan").empty();
				$('#xsetiap_bulan').append(data.detail);
			}
		});
	}
	$("#btn_save_cicilan").click(function() {//Simpan Cicilan
		if ($("#xcara_bayar").val()=='2' && $("#xjml_cicilan").val() < 2){
			sweetAlert("Maaf...", "Cicilan Harus lebih dari satu kali!", "error");
			return false;
		}
		// if ($("#xcara_bayar").val()=='1' && $("#xjml_cicilan").val() >1){
			// sweetAlert("Maaf...", "Cicilan Harus lebih dari satu kali!", "error");
			// return false;
		// }
		swal({
			title: "Anda Yakin ?",
			text : "Akan Mengganti Cara Pembayaran ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			save_cicilan();
		});
	});
	function save_cicilan(){
		var tgl=$("#tgl_start").val();
		var id_detail=$("#xid_detail").val();
		var jml=$("#xjml_cicilan").val();
		var xperbulan=$("#xperbulan").val();
		var xcara_bayar=$("#xcara_bayar").val();
		var tipe=$("#xtipe2").val();
		// alert(jml);
		var xsetiap_bulan=$("#xsetiap_bulan").val();
		table = $('#index_list').DataTable()	
		
		if (xcara_bayar=='2'){
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tpiutang_tt_monitoring/save_cicilan/',
				dataType: "json",
				type: 'POST',
				data: {
					tgl: tgl,
					jml: jml,
					id_detail: id_detail,
					xperbulan: xperbulan,
					cara_bayar: xcara_bayar,
					xsetiap_bulan: xsetiap_bulan,
					tipe: tipe,
					},
				success: function(data) {
					console.log(data);
					$('#modal_cara_bayar').modal('hide');
					$.toaster({priority : 'success', title : 'Succes!', message : ' Proses Pembayaran'});
					table.ajax.reload( null, false ); 
					$("#cover-spin").hide();
				}
			});
		}else{
			$('#modal_cara_bayar').modal('hide');
			$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Cara Pembayaran'});
		}
	}
	$(document).on("click", ".followup", function() {
		var table = $('#index_list').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()
		$("#upload_piutang_id").val(id);
		$("#fu_piutang_id").val(id);
		// $("#fu_no_tagihan").val(table.cell(tr,3).data());
		$("#fu_tanggal_tagihan").val(table.cell(tr,9).data());
		$("#keterangan").summernote('code','')
		$("#tanggal_reminder").val();
		load_informasi();
		load_upload();
		$("#modal_fu").modal('show');
	});
	
	$(document).on("click", "#btn_tambah_fu", function() {
		if ($("#fu_tanggal_reminder").val()==''){
			sweetAlert("Maaf...", "Tanggal Reminder belum dipilih!", "error");
			return false;
		}
		if ($("#fu_tanggal_info").val()==''){
			sweetAlert("Maaf...", "Tanggal Informasi belum dipilih!", "error");
			return false;
		}
		var piutang_id=$("#fu_piutang_id").val();
		var tanggal_info=$("#fu_tanggal_info").val();
		var tanggal_reminder=$("#fu_tanggal_reminder").val();
		var keterangan=$("#keterangan").summernote('code');
		$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tpiutang_tt_monitoring/tambah_fu',
				type: 'POST',
				data: {
					piutang_id: piutang_id,
					tanggal_info: tanggal_info,
					tanggal_reminder: tanggal_reminder,
					keterangan: keterangan,
					},
				complete: function() {
					 $('#index_list').DataTable().ajax.reload( null, false );
					$.toaster({priority : 'success', title : 'Succes!', message : ' Tambah Follow Up'});
					$("#modal_fu").modal('hide');
					$("#cover-spin").hide();
				}
			});
	});
	$(document).on("click",".hapus_file",function(){	
		var table = $('#index_dokumen').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()
		$.ajax({
			url: '{site_url}tpiutang_tt_monitoring/hapus_file',
			type: 'POST',
			data: {id: id},
			success : function(data) {				
				$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
				$('#index_dokumen').DataTable().ajax.reload( null, false );			
			}
		});
	});
	$(document).on("click",".fu_hapus",function(){	
		var table = $('#index_informasi').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()
		$.ajax({
			url: '{site_url}tpiutang_tt_monitoring/hapus_informasi',
			type: 'POST',
			data: {id: id},
			success : function(data) {				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Follow up Berhasil Dihapus'});
				$('#index_informasi').DataTable().ajax.reload( null, false );			
			}
		});
	});
	function load_informasi(){
		var piutang_id=$("#fu_piutang_id").val();
		
		$('#index_informasi').DataTable().destroy();
		table = $('#index_informasi').DataTable({
				autoWidth: false,
				searching: true,
				pageLength: 10,
				serverSide: true,
				"processing": true,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [{ "targets": [0], "visible": false },
								{ "width": "5%", "targets": [1] },
								{ "width": "10%", "targets": [2,5] },
								{ "width": "15%", "targets": [3,4,6] },
							 {"targets": [3], className: "text-left" },
							 {"targets": [1], className: "text-right" },
							 {"targets": [2,4,5,6], className: "text-center" }
							 ],
				ajax: { 
					url: '{site_url}tpiutang_tt_monitoring/load_informasi', 
					type: "POST" ,
					dataType: 'json',
					data : {
							piutang_id:piutang_id
						   }
				}
			});
	}
	function load_upload(){
		var piutang_id=$("#fu_piutang_id").val();
		
		$('#index_dokumen').DataTable().destroy();
		table = $('#index_dokumen').DataTable({
				autoWidth: false,
				searching: true,
				pageLength: 10,
				serverSide: true,
				"processing": true,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [{ "targets": [0], "visible": false },
								{ "width": "5%", "targets": [1] },
								{ "width": "40%", "targets": [2] },
								{ "width": "10%", "targets": [3] },
								{ "width": "15%", "targets": [4] },
							 {"targets": [2], className: "text-left" },
							 {"targets": [1], className: "text-right" },
							 {"targets": [3,4], className: "text-center" }
							 ],
				ajax: { 
					url: '{site_url}tpiutang_tt_monitoring/load_upload', 
					type: "POST" ,
					dataType: 'json',
					data : {
							piutang_id:piutang_id
						   }
				}
			});
	}
    function load_rincian() {
		var tanggal_jatuh_tempo=$("#tanggal_jatuh_tempo").val();
		var no_reg=$("#no_reg").val();
		var no_medrec=$("#no_medrec").val();
		var nama_pasien=$("#nama_pasien").val();
		var tgl_trx=$("#tgl_trx").val();
		var tgl_trx2=$("#tgl_trx2").val();
		var disabel=$("#disabel").val();
		// alert(tgl_trx);
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 100,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tpiutang_tt_monitoring/load_rincian/',
			type: "POST",
			dataType: 'json',
			data: {
				tanggal_jatuh_tempo: tanggal_jatuh_tempo,no_reg: no_reg,
				no_medrec: no_medrec,nama_pasien: nama_pasien,
				tgl_trx:tgl_trx,tgl_trx2:tgl_trx2,disabel:disabel,
			}
		},
		columnDefs: [{ "targets": [0,1,2,3,8,15,16], "visible": false },
							{ "width": "3%", "targets": [4] },
							{ "width": "6%", "targets": [5,6,7,10,14] },
							{ "width": "7%", "targets": [11,12,13,15,16] },
							{ "width": "10%", "targets": [9] },
							// { "width": "8%", "targets": [8,,9,10] },
							{ "width": "12%", "targets": [17] },
						 {"targets": [11,12,13], className: "text-right" },
						 {"targets": [5,6,7,8,10,14,15,16,17], className: "text-center" }
						 ],
		});
	}
	
	$("#btn_filter").click(function() {
		load_rincian();
	});
	$(document).on("click", ".ganti", function() {
		var table = $('#index_list').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()
		var tanggal=table.cell(tr,16).data()
		var nominal=table.cell(tr,12).data()
		$("#xnominal2").val(nominal);
		$("#xtgl_pilih").val(tanggal);
		$("#xid").val(id);
		$("#xalasan").val('');
		$('#modal_edit').modal('show');
		
	});
	
	$(document).on("click", "#btn_ubah", function() {
		if ($("#xtgl_pilih").val()=='' || $("#xtgl_pilih").val()==null){
			sweetAlert("Maaf...", "Tanggal belum dipilih!", "error");
			return false;
		}
		// alert($("#tgl_asal").val()+' '+$("#xtgl_pilih").val());
		if ($("#tgl_asal").val()==$("#xtgl_pilih").val()){
			sweetAlert("Maaf...", "Tanggal belum dipilih!", "error");
			return false;
		}
		if ($("#xalasan").val()=='' || $("#xalasan").val()==null){
			sweetAlert("Maaf...", "Alasan harus diisi!", "error");
			return false;
		}
		// alert();
		
		swal({
			title: "Anda Yakin ?",
			text : "Akan Memindahkan Tanggal Jatuh Tempo?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			edit_tanggal_satu();
		});
		
	});
	function edit_tanggal_satu(){
		var xid=$("#xid").val();		
		var tanggal_jatuh_tempo=$("#xtgl_pilih").val();		
		var tgl_asal=$("#tgl_asal").val();		
		var xalasan=$("#xalasan").val();		
		var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}tpiutang_tt_monitoring/edit_tanggal_satu',
			type: 'POST',
			data: {id: xid,tanggal_jatuh_tempo:tanggal_jatuh_tempo,alasan:xalasan,tgl_asal:tgl_asal},
			complete: function() {

				$.toaster({priority : 'success', title : 'Succes!', message : ' Memindahkan Tanggal Jatuh Tempo'});
				// location.reload();
				// filter_form();
				$("#modal_edit").modal('hide');
				$("#cover-spin").hide();
				load_rincian();
			}
		});
	}
</script>