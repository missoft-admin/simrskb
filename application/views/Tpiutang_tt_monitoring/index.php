<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Tagihan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_tagihan" placeholder="No Tagihan" name="no_tagihan" value="">
                    </div>
                </div>
				
				          
            </div>
			<div class="col-md-6">
				
                <div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Jatuh Tempo</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
                            <input class="form-control" type="text" id="jatuh_tempo_bayar" name="jatuh_tempo_bayar" placeholder="From" value="{jatuh_tempo_bayar}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="jatuh_tempo_bayar2" name="jatuh_tempo_bayar2" placeholder="To" value="{jatuh_tempo_bayar2}"/>
                        </div>
                    </div>
                </div>
				
               
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
			
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th hidden width="2%">ID</th>
                    <th width="10%">NO</th>
                    <th width="10%">NO TAGIHAN</th>
                    <th width="10%">JUMLAH FAKTUR</th>
                    <th width="10%">TOTAL TAGIHAN</th>
                    <th width="10%">SISA TAGIHAN</th>
                    <th width="10%">JATUH TEMPO</th>
                    <th width="10%">STATUS PEMBAYARAN</th>
                    <th width="10%">ACTION</th>
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
</div>

<div class="modal fade" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Pengalihan Jatuh Tempo </h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">						
							<div class="form-group" >
								<label class="col-md-4 control-label" for="status">Nominal</label>
								<div class="col-md-8">
										<input class="form-control input decimal" disabled type="text" id="xnominal" name="xnominal" value="">
									
									
								</div>
							</div>				
						</div>	
						<div class="col-md-12" style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">Tanggal Asal</label>
							<div class="col-md-8">
							<div class="input-group">
								<input class="form-control" readonly type="text" id="tgl_asal" name="tgl_asal" value="">

								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
							</div>
						</div> 	
							
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Tanggal Edit</label>
								<div class="col-md-8">
									<div class="input-group">
										<input class="js-datepicker form-control input" type="text" id="xtgl_pilih" name="xtgl_pilih" value="" data-date-format="dd-mm-yyyy">

										<span class="input-group-addon">
											<span class="fa fa-calendar"></span>
										</span>
									</div>
									
								</div>
							</div>				
						</div>	
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Alasan Ganti</label>
								<div class="col-md-8">
										<input class="form-control input" type="text" id="xalasan" name="xalasan" value="">
									
									
								</div>
							</div>				
						</div>				
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="xid" placeholder="No. PO" name="xid" value="">
								<input type="hidden" class="form-control" id="xtanggal_tagihan" placeholder="No. PO" name="xtanggal_tagihan" value="">
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_ubah" id="btn_ubah"><i class="fa fa-save"></i> SIMPAN</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
var myDropzone 
$(document).ready(function(){
	
	// Dropzone.autoDiscover = false;
	// myDropzone = new Dropzone(".dropzone", { 
	   // autoProcessQueue: true,
	   // maxFilesize: 30,
	// });
	// myDropzone.on("complete", function(file) {	  
	  // myDropzone.removeFile(file);
	  // $('#index_dokumen').DataTable().ajax.reload( null, false );
	// });
	
	$(".number").number(true,0,'.',',');
	load_index();
   
});
function load_index(){
	var no_tagihan=$("#no_tagihan").val();
	var jatuh_tempo_bayar=$("#jatuh_tempo_bayar").val();
	var jatuh_tempo_bayar2=$("#jatuh_tempo_bayar2").val();
	$('#index_list').DataTable().destroy();
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: false,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0], "visible": false },
							{ "width": "3%", "targets": [1] },
							{ "width": "10%", "targets": [2,3,6,7] },
							{ "width": "12%", "targets": [4,5] },
							{ "width": "20%", "targets": [8] },
							// { "width": "7%", "targets": [7,8,9,10,12] },
							// { "width": "8%", "targets": [12,13,14] },
							// { "width": "10%", "targets": [15] },
						 // {"targets": [4,13,15], className: "text-left" },
						 {"targets": [4,5], className: "text-right" },
						 {"targets": [2,3,6,7,8], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tpiutang_tt_monitoring/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						no_tagihan:no_tagihan,
						jatuh_tempo_bayar2:jatuh_tempo_bayar2,jatuh_tempo_bayar:jatuh_tempo_bayar,
					   }
            }
        });
}

$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_index();
});
//lunas

$(document).on("keyup", "#nominal_bayar", function() {
	var selisih=0;
	var harus=$("#harus_dibayar").val();
	if (parseFloat($(this).val())>parseFloat(harus)){
		$(this).val(harus);
	}
	selisih=parseFloat(harus) - parseFloat($(this).val())
	$("#tidak_terbayar").val(selisih);
	
	if ($("#harus_dibayar").val() != $("#nominal_bayar").val()){
		$("#btn_ubah").attr('disabled',true);
	}else{
		$("#btn_ubah").attr('disabled',false);
	}
});

$(document).on("click", ".ganti", function() {
	// alert('data');
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	// var id=table.cell(tr,0).data()
	var tanggal=table.cell(tr,6).data()
	var nominal=table.cell(tr,5).data()
	$("#xnominal").val(nominal);
	$("#xtgl_pilih").val(tanggal);
	$("#tgl_asal").val(tanggal);
	// $("#xid").val(id);
	$("#xalasan").val('');
	$('#modal_edit').modal('show');
	
});
	
	$(document).on("click", "#btn_ubah", function() {
		if ($("#xtgl_pilih").val()=='' || $("#xtgl_pilih").val()==null){
			sweetAlert("Maaf...", "Tanggal belum dipilih!", "error");
			return false;
		}
		// alert($("#tgl_asal").val()+' '+$("#xtgl_pilih").val());
		if ($("#tgl_asal").val()==$("#xtgl_pilih").val()){
			sweetAlert("Maaf...", "Tanggal belum dipilih!", "error");
			return false;
		}
		if ($("#xalasan").val()=='' || $("#xalasan").val()==null){
			sweetAlert("Maaf...", "Alasan harus diisi!", "error");
			return false;
		}
		// alert();
		
		swal({
			title: "Anda Yakin ?",
			text : "Akan Memindahkan Tanggal Jatuh Tempo?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			edit_tanggal_all();
		});
		
	});
function edit_tanggal_all(){
		var xid=$("#xid").val();		
		var tanggal_jatuh_tempo=$("#xtgl_pilih").val();		
		var tgl_asal=$("#tgl_asal").val();		
		var xalasan=$("#xalasan").val();		
		var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}tpiutang_tt_monitoring/edit_tanggal_all',
			type: 'POST',
			data: {id: xid,tanggal_jatuh_tempo:tanggal_jatuh_tempo,alasan:xalasan,tgl_asal:tgl_asal},
			complete: function() {

				$.toaster({priority : 'success', title : 'Succes!', message : ' Memindahkan Tanggal Jatuh Tempo'});
				// location.reload();
				// filter_form();
				$("#modal_edit").modal('hide');
				$("#cover-spin").hide();
				load_index();
			}
		});
	}
</script>