<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tpenggajian" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('tpenggajian/save','class="form-horizontal push-10-t" id="form-work"') ?>
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Periode</label>
			<div class="col-md-3">
				<div class="js-datepicker input-group date">
					<input <?=($id?'disabled':'')?>  class="js-datepicker form-control" type="text" id="periode" name="periode" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?=DMYFormat($periode);?>">
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
				
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Tahun</label>
			<div class="col-md-7">
				<select name="tahun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="0" <?= ($tahun == '0')? 'selected="selected"' : ''?>>Pilih Opsi</option>
					<?php for($i=2018;$i<2031;$i++){?>
						<option <?= ($i == $tahun)? 'selected="selected"' : ''?> value="<?= $i ?>"><?= $i ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="idtipe">Bulan</label>
			<div class="col-md-7">
				<select name="bulan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option <?= ($bulan == '0')? 'selected="selected"' : ''?> value="0">Pilih Opsi</option>
					<option <?= ($bulan == '01')? 'selected="selected"' : ''?> value="01">Januari</option>
					<option <?= ($bulan == '02')? 'selected="selected"' : ''?> value="02">Februari</option>
					<option <?= ($bulan == '03')? 'selected="selected"' : ''?> value="03">Maret</option>
					<option <?= ($bulan == '04')? 'selected="selected"' : ''?> value="04">April</option>
					<option <?= ($bulan == '05')? 'selected="selected"' : ''?> value="05">Mei</option>
					<option <?= ($bulan == '06')? 'selected="selected"' : ''?> value="06">Juni</option>
					<option <?= ($bulan == '07')? 'selected="selected"' : ''?> value="07">Juli</option>
					<option <?= ($bulan == '08')? 'selected="selected"' : ''?> value="08">Agustus</option>
					<option <?= ($bulan == '09')? 'selected="selected"' : ''?> value="09">September</option>
					<option <?= ($bulan == '10')? 'selected="selected"' : ''?> value="10">Oktober</option>
					<option <?= ($bulan == '11')? 'selected="selected"' : ''?> value="11">November</option>
					<option <?= ($bulan == '12')? 'selected="selected"' : ''?> value="12">Desember</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="idsub">Nama Transaksi</label>
			<div class="col-md-7">
				<input type="text" class="form-control" id="subyek" placeholder="Nama Transaksi" name="subyek" value="{subyek}">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="idsub">Jenis</label>
			<div class="col-md-7">
				<select name="idjenis" <?=($id?'disabled':'')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="#" <?= ($idjenis == '#')? 'selected="selected"' : ''?>>Pilih Jenis</option>
					<?php foreach($list_jenis as $row){?>
						<option <?= ($row->id == $idjenis)? 'selected="selected"' : ''?> value="<?= $row->id ?>"><?= $row->nama ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>

	<div class="row" style="margin-top:-25px">
		<div class="block-content block-content-narrow">
			<div class="form-group">
				<label class="col-md-3 control-label"></label>
				<div class="col-md-7">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}tpenggajian" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
		</div>
	</div>

	<input type="hidden" id="rowindex" value="">
	<input type="hidden" id="detailValue" name="detail_value" value="">

	<br><br>
	<?php echo form_hidden('id', $id); ?>
	<?php echo form_close() ?>
</div>

<!-- start variable modal -->
<div class="modal fade in black-overlay" id="variableModal" role="dialog" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Variable Rekapan</h3>
				</div>
				<div class="block-content">
					<div class="form-horizontal">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="" style="margin-top: 5px;">Tipe</label>
							<div class="col-md-8">
								<select id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="0">-</option>
									<option value="1">Pengeluaran</option>
									<option value="2">Pengeluaran + Pendapatan</option>
								</select>
							</div>
						</div>
					</div>
					<br>
					<table class="table table-bordered table-striped table-responsive" id="variableData">
						<thead>
							<tr>
								<th></th>
								<th>Nama Variable</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button id="addVariable" class="btn btn-sm btn-success" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-check"></i> Proses</button>
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>
<!-- eof variable modal -->

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$(".number").number(true, 0, '.', ',');

		$(document).on("click", "#showVariableModal", function() {
			getVariableRekapan(0);
		});

		$(document).on("change", "#idtipe", function() {
			getVariableRekapan($(this).val());
		});

		$(document).on("click", "#addVariable", function() {
			  var html = '';
			  var getSelectedRows = $("input:checked").parents("tr");

			$('#variableData').find('tr').each(function () {
	        var row = $(this);
	        if (row.find('input[type="checkbox"]').is(':checked')) {
						html += '<tr>';
		        html += '<td style="display:none">' + row.children('td').eq(1).html() + '</td>';
		        html += '<td>' + row.children('td').eq(2).html() + '</td>';
		        html += '<td>' + row.children('td').eq(3).html() + '</td>';
		        html += '<td><a href="#" class="btn btn-danger btn-sm removeVariable" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash-o"></i></a></td>';
		        html += '</tr>';
	        }
			});

			$("#detailList tbody").append(html);

			swal({
				title: "Berhasil!",
				text: "Data telah ditambahkan.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});

		$(document).on("click", ".removeVariable", function() {
			if (confirm("Hapus data ?") == true) {
				$(this).closest('td').parent().remove();
			}
			return false;
		});

		$("#form-work").submit(function(e) {
			var form = this;

			var detailValue_tbl = $('table#detailList tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					if ($(cell).find("input").length >= 1) {
						return $(cell).find("input").val();
					} else {
						return $(cell).html();
					}
				});
			});

			$("#detailValue").val(JSON.stringify(detailValue_tbl));

			swal({
				title: "Berhasil!",
				text: "Proses penyimpanan data.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});
	});

	function getVariableRekapan(idtipe){
		$.ajax({
			url: '{site_url}tpenggajian/getVariableRekapan/' + idtipe,
			method: 'GET',
			success: function(data) {
				$("#variableData tbody").html(data);
			}
		});
	}
</script>
