<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tpenggajian" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<?php echo form_open('tpenggajian/updateReview','class="form-horizontal push-10-t" id="form-work"') ?>

	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="detailList">
			<thead>
				<tr>
					<th>Nama Variable</th>
					<th>Nominal</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($list_detail as $row){ ?>
				<?
					if ($row->id_var=='' && $row->idsub !='1'){
						$status='<span class="label label-danger">Belum Memiliki Variable Rekap</span>';
					}else{
						
						$status="";
					}
				?>
					<tr>
						<td style="display:none"><input type="text" class="form-control" placeholder="Nominal" name="iddet[]" value="<?=($row->id)?>"></td>
						<td><?=($row->idsub == 2 ? '└─' : '').' '.$row->nama.' '.$status?></td>
						<td>
							<input type="text" <?=$disabel?> class="form-control number <?= ($row->idsub != 2 ? "header_".$row->idvariable : "child child_".$row->idheader) ?>" data-header="<?= "header_".$row->idheader?>" data-child="<?= "child_".$row->idheader ?>" <?= ($row->idsub == 1 ? 'readonly' : '')?> placeholder="Nominal" name="nominal[]" value="<?=($row->nominal)?>">
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	</div>

	<div class="row" style="margin-top:-25px">
		<?if ($disabel==''){?>
		<div class="block-content block-content-narrow">
			<div class="form-group">
				<label class="col-md-3 control-label"></label>
				<div class="col-md-7">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}tpenggajian" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
		</div>
		<?}?>
	</div>

	<input type="hidden" id="rowindex" value="">
	<input type="hidden" id="detailValue" name="detail_value" value="">

	<br><br>
	<?php echo form_hidden('id', $id); ?>
	<?php echo form_close() ?>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$(".number").number(true, 0, '.', ',');

		
		$(document).on("keyup", ".child", function() {
			var var_childs = "." + $(this).data('child');
			var var_header= "." + $(this).data('header');
			var total = 0;
			$(var_childs).each(function() {
				total += parseFloat($(this).val());
			});
			console.log(var_header);
			console.log(var_childs);
			$(var_header).val(total);
		});

		$("#form-work").submit(function(e) {
			var form = this;

			var detailValue_tbl = $('table#detailList tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					if ($(cell).find("input").length >= 1) {
						return $(cell).find("input").val();
					} else {
						return $(cell).html();
					}
				});
			});

			$("#detailValue").val(JSON.stringify(detailValue_tbl));

			swal({
				title: "Berhasil!",
				text: "Proses penyimpanan data.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});
	});

	function getVariableRekapan(idtipe){
		$.ajax({
			url: '{site_url}tpenggajian/getVariableRekapan/' + idtipe,
			method: 'GET',
			success: function(data) {
				$("#variableData tbody").html(data);
			}
		});
	}


</script>
