<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>
<?php $user_id = $this->session->userdata('user_id'); ?>

<div class="block">
    <div class="block-content">
        <div class="row">
            <div class="col-md-6">
                <h4>RIWAYAT PENGIRIMAN E-MAIL HASIL PEMERIKSAAN</h4> <br>
            </div>
            <div class="col-md-6 text-right">
                <a href="{site_url}term_radiologi_xray_hasil" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
            </div>
        </div>

        <hr>
        
        <div class="row">
            <div class="col-md-12">
                <h5 class="text-primary">Pengiriman Pertama</h5><br>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="selectTujuanLab">Tanggal Pengiriman Hasil</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group date">
                            <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" disabled value="{tanggal_pengiriman_hasil}">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                            <input type="text" class="time-datepicker form-control" disabled value="{waktu_pengiriman_hasil}">
                            <span class="input-group-addon"><i class="si si-clock"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="selectTujuanLab">Tanggal Penerimaan Hasil</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group date">
                            <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" disabled value="{tanggal_penerimaan_hasil}">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                            <input type="text" class="time-datepicker form-control" disabled value="{waktu_penerimaan_hasil}">
                            <span class="input-group-addon"><i class="si si-clock"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="emailKirimHasil">E-mail Kirim Hasil</label>
                    <input type="text" class="js-tags-input form-control" id="emailKirimHasil" value="{email_tujuan}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="petugasPengirimHasil">Petugas Pengirim Hasil</label>
                    <input type="text" class="form-control" disabled value="{petugas_pengirim_hasil}">
                </div>
                <div class="form-group">
                    <label for="petugasPenerimaHasil">Petugas Penerima Hasil</label>
                    <input type="text" class="form-control" disabled value="{petugas_penerimaan_hasil}">
                </div>
                <div class="form-group">
                    <label for="penerimaBukanPetugas">Jika Penerima Bukan Petugas</label>
                    <input type="text" class="form-control" disabled value="{penerima_bukan_petugas}">
                </div>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-12">
                <h5 class="text-primary">Riwayat Pengiriman E-mail</h5><br>
            </div>
            <div class="col-md-12">
                <div class="tableFix2">
                    <table class="table table-bordered table-striped" id="table-history">
                        <thead>
                            <tr>
                                <th width="10%">No</th>
                                <th width="10%">Petugas Pengirim</th>
                                <th width="10%">Waktu Pengiriman Hasil</th>
                                <th width="10%">E-mail Tujuan</th>
                                <th width="10%">Nama File</th>
                                <th width="10%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('Term_radiologi_xray_expertise/services'); ?>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        loadDataTableRiwayatPengirimanEmail();

        setTimeout(function() {
            $('.tagsinput').css('background-color', '#eee');
            $('#emailKirimHasil_tagsinput').prop('disabled', true);
            $('#emailKirimHasil_addTag').remove();
            $('.tag a').remove();
        }, 500)
    });

    function loadDataTableRiwayatPengirimanEmail() {
        $('#table-history').DataTable().destroy();
        $('#table-history').DataTable({
            "autoWidth": false,
            "searching": true,
            "pageLength": 50,
            "serverSide": true,
            "processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
            "columnDefs": [
                {
                    "width": "10%",
                    "targets": [0, 1, 2, 3, 4, 5],
                    "className": "text-center"
                },
            ],
            ajax: {
                url: '{site_url}term_radiologi_xray_expertise/getIndexRiwayatPengirimanHasil/' + '{id}',
                type: "POST",
                dataType: 'json',
            }
        });

        $("#cover-spin").hide();
    }
</script>