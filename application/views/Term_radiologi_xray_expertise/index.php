<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>
<?php $user_id = $this->session->userdata('user_id'); ?>

<div class="block">
    <div class="block-content bg-primary" style="border-radius: 10px;">
        <div class="form-horizontal">
            <div class="row pull-10">
                <div class="col-md-2" style="width: 14.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="dokter_peminta">Dokter Peminta</label>
                        <div class="col-xs-12">
                            <select id="dokter_peminta" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="0" selected>Semua</option>
                                <?php foreach (get_all('mdokter', ['status' => 1]) as $row) { ?>
                                <option value="<?= $row->id; ?>"><?= $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="width: 14.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="asal_pasien">Asal Pasien</label>
                        <div class="col-xs-12">
                            <select id="asal_pasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="0" selected>Semua</option>
                                <option value="1">Poliklinik</option>
                                <option value="2">Instalasi Gawat Darurat (IGD)</option>
                                <option value="3">Rawat Inap</option>
                                <option value="4">One Day Surgery (ODS)</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="width: 14.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="status_cito_expertise">Status CITO Expertise</label>
                        <div class="col-xs-12">
                            <select id="status_cito_expertise" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="#" selected>Semua</option>
                                <?php foreach (list_variable_ref(249) as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="width: 18.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Tanggal</label>
                        <div class="col-xs-12">
                            <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                <input class="form-control" type="text" id="tanggal_dari" value="{tanggal}">
                                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                <input class="form-control" type="text" id="tanggal_sampai" value="{tanggal}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="width: 14.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Nama Pasien</label>
                        <div class="col-xs-12">
                            <input class="form-control" type="text" id="nama_pasien" value="">
                        </div>
                    </div>
                </div>

                <div class="col-md-2" style="width: 10.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="">&nbsp;&nbsp;</label>
                        <div class="col-xs-12">
                            <button class="btn btn-success" type="button" title="Cari" id="btn-search"><i class="fa fa-filter"></i> Cari</button>
                            <button class="btn btn-warning" type="button" title="Filter" data-toggle="modal" data-target="#modal-filter"><i class="fa fa-expand"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="block">
    <div class="block-content">
        <!-- Tabs -->
        <ul class="nav nav-pills" id="section-tabs">
            <li class="section-tab" data-tab="1">
                <a href="javascript:void(0)"><i class="si si-user"></i> My Patient</a>
            </li>
            <li class="section-tab" data-tab="2">
                <a href="javascript:void(0)"><i class="si si-user"></i> All Patient</a>
            </li>
        </ul>
        <br>
    </div>
</div>

<div class="block">
    <!-- Section Tab Content for Tab 1 -->
    <div class="section-content" data-tab="1">
        <div class="block-content">
            <ul class="nav nav-pills">
                <li id="tab-semua-my-patient" class="<?= ($status_pemeriksaan == '#' ? 'active' : ''); ?>">
                    <a href="javascript:void(0)" onclick="setTab('#')"><i class="si si-list"></i> Semua</a>
                </li>
                <li id="tab-belum-diperiksa" class="<?= ($status_pemeriksaan == '0' ? 'active' : ''); ?>">
                    <a href="javascript:void(0)"  onclick="setTab(1)"><i class="si si-pin"></i> Belum Diperiksa </a>
                </li>
                <li id="tab-telah-diperiksa" class="<?= ($status_pemeriksaan == '1' ? 'active' : ''); ?>">
                    <a href="javascript:void(0)" onclick="setTab(2)"><i class="fa fa-check-square-o"></i> Telah Diperiksa</a>
                </li>
            </ul>

            <br>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table" id="table-my-patient">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Section Tab Content for Tab 2 -->
    <div class="section-content" data-tab="2">
        <div class="block-content">
            <ul class="nav nav-pills">
                <li id="tab-semua-all-patient" class="<?= ($status_cito_expertise == '#' ? 'active' : ''); ?>">
                    <a href="javascript:void(0)" onclick="setTabStatusCITOExpertise('#')"><i class="si si-list"></i> Semua</a>
                </li>
                <li id="tab-non-cito" class="<?= ($status_cito_expertise == '1' ? 'active' : ''); ?>">
                    <a href="javascript:void(0)"  onclick="setTabStatusCITOExpertise(1)"><i class="si si-pin"></i> NON CITO </a>
                </li>
                <li id="tab-cito" class="<?= ($status_cito_expertise == '2' ? 'active' : ''); ?>">
                    <a href="javascript:void(0)" onclick="setTabStatusCITOExpertise(2)"><i class="fa fa-check-square-o"></i> CITO</a>
                </li>
            </ul>

            <br>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table" id="table-all-patient">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="status_pemeriksaan" name="status_pemeriksaan" value="{status_pemeriksaan}">
<input type="hidden" id="status_cito_expertise" name="status_cito_expertise" value="{status_cito_expertise}">
<input type="hidden" id="transaksiId" value="">

<?php $this->load->view('Term_radiologi_xray_expertise/modal/modal_filter'); ?>
<?php $this->load->view('Term_radiologi_xray_expertise/modal/modal_alasan_pemabatalan'); ?>
<?php $this->load->view('Term_radiologi_xray_expertise/modal/modal_kirim_hasil_pemeriksaan'); ?>
<?php $this->load->view('Term_radiologi_xray_expertise/modal/modal_kirim_ulang_hasil_pemeriksaan'); ?>
<?php $this->load->view('Term_radiologi_xray_expertise/modal/modal_kirim_hasil_upload'); ?>
<?php $this->load->view('Term_radiologi_xray_expertise/modal/modal_ubah_kelas_tarif'); ?>
<?php $this->load->view('Term_radiologi_xray_expertise/services'); ?>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
    var activeTab = 1;

    $(document).ready(function() {
        setSectionTab(activeTab);
        setTabStatusPemeriksaan($("#status_pemeriksaan").val());

        $(document).on("click", ".section-tab", function() {
            var tabNumber = $(this).data('tab');
            
            activeTab = tabNumber;
            setSectionTab(tabNumber);
        });

        $(document).on("click", "#btn-search", function() {
            $("#cover-spin").show();

            if (activeTab == 1) {
                loadDataTableMyPatient();
            } else if (activeTab == 2) {
                loadDataTableAllPatient();
                
                const tab = $("#status_cito_expertise option:selected").val();

                document.getElementById("tab-semua-all-patient").classList.remove("active");
                document.getElementById("tab-non-cito").classList.remove("active");
                document.getElementById("tab-cito").classList.remove("active");
                
                if (tab == '#') {
                    document.getElementById("tab-semua-all-patient").classList.add("active");
                }
                
                if (tab == '1') {
                    document.getElementById("tab-non-cito").classList.add("active");
                }
                
                if (tab == '2') {
                    document.getElementById("tab-cito").classList.add("active");
                }
            }
        });
    });

    function setSectionTab(tabNumber) {
        // Hide all section contents
        $('.section-content').hide();

        // Show the selected section content
        $(`.section-content[data-tab="${tabNumber}"]`).show();

        // Toggle 'active' class for the tabs
        $('.section-tab').removeClass('active');
        $(`.section-tab[data-tab="${tabNumber}"]`).addClass('active');

        // Store the active tab in localStorage
        localStorage.setItem('activeTab', tabNumber);

        if (tabNumber == 1) {
            loadDataTableMyPatient();
        } else if (tabNumber == 2) {
            loadDataTableAllPatient();
        }
    }

    function setTabStatusPemeriksaan(tab) {
        $("#cover-spin").show();

        document.getElementById("tab-semua-my-patient").classList.remove("active");
        document.getElementById("tab-belum-diperiksa").classList.remove("active");
        document.getElementById("tab-telah-diperiksa").classList.remove("active");
        
        if (tab == '#') {
            document.getElementById("tab-semua-my-patient").classList.add("active");
        }
        
        if (tab == '0') {
            document.getElementById("tab-belum-diperiksa").classList.add("active");
        }
        
        if (tab == '1') {
            document.getElementById("tab-telah-diperiksa").classList.add("active");
        }

        $("#status_pemeriksaan").val(tab);
        $("#selectStatusPemeriksaan").val(tab).trigger('change');

        loadDataTableMyPatient();
    }

    function setTabStatusCITOExpertise(tab) {
        $("#cover-spin").show();

        document.getElementById("tab-semua-all-patient").classList.remove("active");
        document.getElementById("tab-non-cito").classList.remove("active");
        document.getElementById("tab-cito").classList.remove("active");
        
        if (tab == '#') {
            document.getElementById("tab-semua-all-patient").classList.add("active");
        }
        
        if (tab == '1') {
            document.getElementById("tab-non-cito").classList.add("active");
        }
        
        if (tab == '2') {
            document.getElementById("tab-cito").classList.add("active");
        }

        $("#status_cito_expertise").val(tab).trigger('change');
        $("#selectStatusCitoExpertise").val(tab).trigger('change');

        loadDataTableAllPatient();
    }

    function loadDataTableMyPatient() {
        let dokter_peminta = $("#selectDokterPeminta option:selected").val();
        let asal_pasien = $("#selectAsalPasien option:selected").val();
        let status_cito_expertise = $("#selectStatusCitoExpertise option:selected").val();
        let tanggal_dari = $("#inputTanggalPermintaanDari").val();
        let tanggal_sampai = $("#inputTanggalPermintaanSampai").val();
        let nama_pasien = $("#inputNamaPasien").val();
        let nomor_medrec = $("#inputNomorMedrec").val();
        let nomor_pendaftaran = $("#inputNomorPendaftaran").val();
        let nomor_radiologi = $("#inputNomorRadiologi").val();
        let kelompok_pasien = $("#selectKelompokPasien option:selected").val();
        let nomor_permintaan = $("#inputNomorPermintaan").val();
        let status_expertise = $("#selectStatusExpertise option:selected").val();

        let requestData = {
            dokter_peminta: dokter_peminta,
            asal_pasien: asal_pasien,
            status_cito_expertise: status_cito_expertise,
            tanggal_dari: tanggal_dari,
            tanggal_sampai: tanggal_sampai,
            nama_pasien: nama_pasien,
            nomor_medrec: nomor_medrec,
            nomor_pendaftaran: nomor_pendaftaran,
            nomor_radiologi: nomor_radiologi,
            kelompok_pasien: kelompok_pasien,
            nomor_permintaan: nomor_permintaan,
            status_expertise: status_expertise,
        }

        $('#table-my-patient').DataTable().destroy();
        $('#table-my-patient').DataTable({
            "autoWidth": false,
            "searching": true,
            "pageLength": 50,
            "serverSide": true,
            "processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
            "columnDefs": [
                {
                    "width": "5%",
                    "targets": 0
                },
                {
                    "width": "30%",
                    "targets": 1
                },
                {
                    "width": "30%",
                    "targets": 2
                },
                {
                    "width": "5%",
                    "targets": 3
                },
            ],
            ajax: {
                url: '{site_url}term_radiologi_xray_expertise/getIndex/{login_pegawai_id}',
                type: "POST",
                dataType: 'json',
                data: requestData
            },
            "drawCallback": function(settings) {
                $("#table-my-patient thead").remove();
                $("#cover-spin").hide();
            }
        });

        $("#cover-spin").hide();
    }

    function loadDataTableAllPatient() {
        let dokter_peminta = $("#selectDokterPeminta option:selected").val();
        let asal_pasien = $("#selectAsalPasien option:selected").val();
        let status_cito_expertise = $("#selectStatusCitoExpertise option:selected").val();
        let tanggal_dari = $("#inputTanggalPermintaanDari").val();
        let tanggal_sampai = $("#inputTanggalPermintaanSampai").val();
        let nama_pasien = $("#inputNamaPasien").val();
        let nomor_medrec = $("#inputNomorMedrec").val();
        let nomor_pendaftaran = $("#inputNomorPendaftaran").val();
        let nomor_radiologi = $("#inputNomorRadiologi").val();
        let kelompok_pasien = $("#selectKelompokPasien option:selected").val();
        let nomor_permintaan = $("#inputNomorPermintaan").val();
        let status_expertise = $("#selectStatusExpertise option:selected").val();

        let requestData = {
            dokter_peminta: dokter_peminta,
            asal_pasien: asal_pasien,
            status_cito_expertise: status_cito_expertise,
            tanggal_dari: tanggal_dari,
            tanggal_sampai: tanggal_sampai,
            nama_pasien: nama_pasien,
            nomor_medrec: nomor_medrec,
            nomor_pendaftaran: nomor_pendaftaran,
            nomor_radiologi: nomor_radiologi,
            kelompok_pasien: kelompok_pasien,
            nomor_permintaan: nomor_permintaan,
            status_expertise: status_expertise,
        }

        $('#table-all-patient').DataTable().destroy();
        $('#table-all-patient').DataTable({
            "autoWidth": false,
            "searching": true,
            "pageLength": 50,
            "serverSide": true,
            "processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
            "columnDefs": [
                {
                    "width": "5%",
                    "targets": 0
                },
                {
                    "width": "30%",
                    "targets": 1
                },
                {
                    "width": "30%",
                    "targets": 2
                },
                {
                    "width": "5%",
                    "targets": 3
                },
            ],
            ajax: {
                url: '{site_url}term_radiologi_xray_expertise/getIndex',
                type: "POST",
                dataType: 'json',
                data: requestData
            },
            "drawCallback": function(settings) {
                $("#table-all-patient thead").remove();
                $("#cover-spin").hide();
            }
        });

        $("#cover-spin").hide();
    }
</script>
