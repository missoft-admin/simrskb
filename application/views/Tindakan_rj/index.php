<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<?
$user_id=$this->session->userdata('user_id');
?>

<div class="row">
	<div class="col-sm-12 bg-primary">
		<div class="block block-themed">
			<div class="block-header bg-primary" style="padding-bottom:0;padding-top:10px;padding-right:0px">
				<div class="block-options-simple">
					<button class="btn btn-warning btn-xs menu_besar" type="button" onclick="kecilkan()" title="Kecilkan Header"><i class="fa fa-eye-slash"></i> </button>
					<button class="btn btn-warning btn-xs menu_kecil" type="button" onclick="besarkan()" title="Munculkan Header"><i class="fa fa-expand"></i> </button>
					<button class="btn btn-danger btn-xs" type="button" title="Final Transaksi"><i class="fa fa-save push-5-r"></i>Simpan </button>
					<button class="btn btn-default btn-xs" type="button" title="Keluar"><i class="fa fa-mail-forward"></i></button>
				</div>
			</div>
			<div class="block-content bg-primary" style="padding-top:0">
				<div class="menu_besar animated fadeIn">
					<div class="col-md-4 bg-primary">
						<table class="text-left">
							<tbody>
								<tr>
									<td style="width: 15%;">
										<div class="">
											<img class="img-avatar" src="<?=site_url()?>assets/upload/avatars/<?=$def_image?>" alt="">
										</div>
									</td>
									<td class="bg-primary text-white" style="width: 85%;">
										<div class="h4 font-w700 text-white"> <?=$namapasien?></div>
										<div class="h5 text-white  push-5-t text-white">{no_medrec}</div>
										<div class="h5 text-white  push-5-t text-white">{jk}, <?=HumanDateShort($tanggal_lahir)?> | <?=$umurtahun.' Tahun '.$umurbulan.' Bulan '.$umurhari.' Hari'?></div>
										<div class="h5 text-white text-uppercase push-5-t"> 
										<div class="btn-group" role="group">
											<button class="btn btn-default btn-sm" type="button"> Tidak Ada ALergi</button>
											<button class="btn btn-danger  btn-sm" type="button"><i class="fa fa-info"></i></button>
										</div>
										
										</div>
									</td>
								</tr>
							</tbody>
						</table>			
					</div>
					<div class="col-md-5 bg-primary">
						<table class="block-table text-left">
							<tbody>
								<tr>
									<td class="bg-primary" style="width: 25%;">
										<div class="text-white text-uppercase  text-center"> Tekanan Darah</div>
										<div class="push-5-t"> <input type="text" readonly class="form-control input-sm" id="pencarian" value=""> </div>
										<div class="text-white text-uppercase  text-center push-5-t">Risiko Jatuh</div>
										<div class="text-white"><input type="text" readonly class="form-control input-sm" id="pencarian" value=""> </div>
									</td>
									<td class="bg-primary" style="width: 25%;">
										<div class="text-white text-uppercase  text-center">Suhu Tubuh</div>
										<div class="push-5-t"> <input type="text" readonly class="form-control input-sm" id="pencarian" value="">  </div>
										<div class="text-white  text-center text-uppercase push-5-t">Risiko Nyeri</div>
										<div class="text-white"><input type="text" readonly class="form-control input-sm" id="pencarian" value=""> </div>
									</td>
									<td class="bg-primary" style="width: 25%;">
										<div class="text-white text-uppercase text-center">Frek Nadi</div>
										<div class="push-5-t"> <input type="text" readonly class="form-control input-sm" id="pencarian" value="">  </div>
										<div class="text-white text-uppercase  text-center push-5-t">Berat Badan</div>
										<div class="text-white"><input type="text" readonly class="form-control input-sm" id="pencarian" value=""> </div>
									</td>
									<td class="bg-primary" style="width: 25%;">
										<div class="text-white text-uppercase  text-center">Frek Nafas</div>
										<div class="push-5-t"> <input type="text" readonly class="form-control input-sm" id="pencarian" value="">  </div>
										<div class="text-white text-uppercase  text-center push-5-t">Tinggi Badan</div>
										<div class="text-white"><input type="text" readonly class="form-control input-sm" id="pencarian" value=""> </div>
									</td>
								</tr>
								
							</tbody>
						</table>			
					</div>
					<div class="col-md-3 bg-primary">
						<table class="block-table text-left">
							<tbody>
								<tr>
									<td class="bg-primary" style="width: 100%;">
										
										<div class="h4 text-white push-5-t"> REGISTRATION INFO</div>
										<div class="h5 push-5-t"> <?=$nopendaftaran.', '.HumanDateLong($tanggaldaftar)?> </div>
										<div class="h5 text-white text-uppercase"><?=GetAsalRujukanR($idtipe).' - '.$nama_poli?></div>
										<div class="h5 text-white"><i class="fa fa-user-md"></i> <?=$nama_dokter?></div>
										<div class="h5 text-uppercase"><i class="fa fa-street-view"></i> <?=($nama_rekanan?$nama_kelompok.' - '.$nama_rekanan:$nama_kelompok)?></div>
									</td>
								</tr>
							</tbody>
						</table>			
					</div>
				</div>
				<div class="menu_kecil animated fadeIn">
					<div class="col-md-12 bg-primary push-20">
						<table class="text-left">
							<tbody>
								<tr>
									<td class="bg-primary text-white" style="width: 100%;">
										<div class="btn-group" role="group">
											<button class="btn btn-default btn-sm" type="button"> Tidak Ada ALergi</button>
											<button class="btn btn-danger  btn-sm" type="button"><i class="fa fa-info"></i></button>
											&nbsp;&nbsp;&nbsp;<span class="h5 text-left text-uppercase">
											<?=$no_medrec?> | <?=$namapasien?> | <?=$jk?>,  <?=HumanDateShort($tanggal_lahir)?> |  <?=$umurtahun.' Tahun '.$umurbulan.' Bulan '.$umurhari.' Hari'?> | <?=($nama_rekanan?$nama_kelompok.' - '.$nama_rekanan:$nama_kelompok)?></span>
										</div>
									</td>
								</tr>
							</tbody>
						</table>			
					</div>
					
					
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-12 bg-warning-light" >
		<!-- Custom files functionality is initialized in js/pages/base_pages_files.js -->
		<!-- Add the category value you want each link in .js-media-filter to filter out in its data-category attribute. Add the value 'all' to show all items -->
		<ul class="js-media-filter nav nav-pills text-uppercase" style="margin-left:-15px">
			<li class=""><a href="javascript:void(0)" data-category="erm_rj"><i class="fa fa-fw fa-folder-open-o push-5-r"></i> ERM RJ</a></li>
			<li class=""><a href="javascript:void(0)" data-category="erm_ri"><i class="fa fa-fw fa-file-movie-o push-5-r"></i> ERM RI</a></li>
			<li class=""><a href="javascript:void(0)" data-category="erm_penunjang"><i class="fa fa-fw fa-file-photo-o push-5-r"></i> PENUNJANG</a></li>
			<li class=""><a href="javascript:void(0)" data-category="e_resep"><i class="fa fa-fw fa-file-audio-o push-5-r"></i> E-RESEP</a></li>
			<li class=""><a href="javascript:void(0)" data-category="lab"><i class="fa fa-fw fa-file-text-o push-5-r text-uppercase"></i> LABORATORIUM</a></li>
			<li class=""><a href="javascript:void(0)" data-category="rad"><i class="fa fa-fw fa-file-text-o push-5-r "></i> Radiologi</a></li>
			<li class=""><a href="javascript:void(0)" data-category="transaksi"><i class="fa fa-fw fa-file-text-o push-5-r"></i> Transaksi</a></li>
			<li class=""><a href="javascript:void(0)" data-category="rencana"><i class="fa fa-fw fa-file-text-o push-5-r"></i> Perencanaan</a></li>
			<li class=""><a href="javascript:void(0)" data-category="surat"><i class="fa fa-fw fa-file-text-o push-5-r"></i> Penerbitan Surat</a></li>
			<li class=""><a href="javascript:void(0)" data-category="rujukan"><i class="fa fa-fw fa-file-text-o push-5-r"></i> Rujukan</a></li>
			<li class=""><a href="javascript:void(0)" data-category="konsul"><i class="fa fa-fw fa-file-text-o push-5-r"></i> Konsul</a></li>
		</ul>
	</div>
		<!-- Add the category value for each item in its data-category attribute (for the filter functionality to work) -->
		<div class="js-media-filter-items row bg-white">
			<div class="col-sm-2" style="display: none;">
				<!-- Music -->
				<div class="block animated fadeIn bg-gray" data-category="erm_rj">
					<div class="push-10-t bg-gray-light">
						<ul class="nav nav-pills nav-stacked push">
							<li class=""><a href="javascript:void(0)"><i class="fa fa-caret-right push-5-r"></i>ERM RJ Menu 1</a></li>
							<li class="active"><a href="javascript:void(0)"><i class="fa fa-caret-right push-5-r"></i>ERM RJ Menu 2</a></li>
							<li class=""><a href="javascript:void(0)"><i class="fa fa-caret-right push-5-r"></i>ERM RJ Menu 3</a></li>
							<li class=""><a href="javascript:void(0)"><i class="fa fa-caret-right push-5-r"></i>ERM RJ Menu 4</a></li>
							<li class=""><a href="javascript:void(0)"><i class="fa fa-caret-right push-5-r"></i>ERM RJ Menu 5</a></li>
							
						</ul>
						
					</div>
				</div>
				<!-- END Music -->
			</div>
			<div class="col-sm-10" style="display: none;">
				<!-- Music -->
				<div class="block animated fadeIn push-5-t" data-category="erm_rj">
					<div class="block-header">
						<ul class="block-options">
							<li>
								<button type="button"><i class="si si-control-play"></i></button>
							</li>
							<li>
								<button type="button"><i class="si si-star"></i></button>
							</li>
							<li>
								<button type="button"><i class="si si-pencil"></i></button>
							</li>
							<li>
								<button type="button"><i class="si si-close"></i></button>
							</li>
						</ul>
					</div>
					<div class="block-content block-content-full text-center">
						<div class="item item-2x item-circle bg-success-light text-success">
							<i class="si si-music-tone-alt"></i>
						</div>
					</div>
					<div class="block-content block-content-full text-center mheight-100">
						<h3 class="h4 font-w300 text-black push-5">Intro.mp3</h3>
						<span class="text-gray">2 min | 384 kbps</span>
					</div>
				</div>
				<!-- END Music -->
			</div>
			<div class="col-sm-2" style="display: none;">
				<!-- Music -->
				<div class="block animated fadeIn bg-gray" data-category="erm_ri">
					<div class="push-10-t bg-gray-light">
						<ul class="nav nav-pills nav-stacked push">
							<li class=""><a href="javascript:void(0)"><i class="fa fa-caret-right push-5-r"></i>ERM RI Menu 1</a></li>
							<li class="active"><a href="javascript:void(0)"><i class="fa fa-caret-right push-5-r"></i>ERM RI Menu 2</a></li>
							<li class=""><a href="javascript:void(0)"><i class="fa fa-caret-right push-5-r"></i>ERM RI Menu 3</a></li>
							<li class=""><a href="javascript:void(0)"><i class="fa fa-caret-right push-5-r"></i>ERM RI Menu 4</a></li>
							<li class=""><a href="javascript:void(0)"><i class="fa fa-caret-right push-5-r"></i>ERM RI Menu 5</a></li>
							
						</ul>
						
					</div>
				</div>
				<!-- END Music -->
			</div>
			<div class="col-sm-10" style="display: none;">
				<!-- Music -->
				<div class="block animated fadeIn push-5-t" data-category="erm_ri">
					<div class="block-header">
						<ul class="block-options">
							<li>
								<button type="button"><i class="si si-control-play"></i></button>
							</li>
							<li>
								<button type="button"><i class="si si-star"></i></button>
							</li>
							<li>
								<button type="button"><i class="si si-pencil"></i></button>
							</li>
							<li>
								<button type="button"><i class="si si-close"></i></button>
							</li>
						</ul>
					</div>
					<div class="block-content block-content-full text-center">
						<div class="item item-2x item-circle bg-success-light text-success">
							<i class="si si-music-tone-alt"></i>
						</div>
					</div>
					<div class="block-content block-content-full text-center mheight-100">
						<h3 class="h4 font-w300 text-black push-5">Intro.mp3</h3>
						<span class="text-gray">2 min | 384 kbps</span>
					</div>
				</div>
				<!-- END Music -->
			</div>
			
		</div>
</div>
<div class="row">
	
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}pages/base_pages_files.js"></script>
<script type="text/javascript">
var table;
var tab;

$(document).ready(function(){	
	besarkan();
});
function kecilkan(){
	$(".menu_kecil").show();
	$(".menu_besar").hide();
}
function besarkan(){
	$(".menu_kecil").hide();
	$(".menu_besar").show();
}
</script>