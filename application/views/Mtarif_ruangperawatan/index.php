<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('178'))){ ?>
<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('180'))){ ?>
		<ul class="block-options">
			<li>
          <a href="{base_url}mtarif_ruangperawatan/create/{idtipe}" class="btn"><i class="fa fa-plus"></i></a>
			</li>
    </ul>
		<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
	<?php if (UserAccesForm($user_acces_form,array('179'))){ ?>
		<hr style="margin-top:0px">
		<div class="row">
			<div class="col-md-6">
				<?php echo form_open('mtarif_ruangperawatan/filter','class="form-horizontal" id="form-work"') ?>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien">Tipe</label>
					<div class="col-md-8">
						<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#">Semua Tipe</option>
							<option value="1" <?=($idtipe == 1) ? "selected" : "" ?>>Ruang Perawatan</option>
							<option value="2" <?=($idtipe == 2) ? "selected" : "" ?>>HCU</option>
							<option value="3" <?=($idtipe == 3) ? "selected" : "" ?>>ICU</option>
							<option value="4" <?=($idtipe == 4) ? "selected" : "" ?>>Isolasi</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
		<hr>
	<?}?>
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"bSort": false,
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
				url: '{site_url}mtarif_ruangperawatan/getIndex/' + {idtipe},
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "10%", "targets": 0, "orderable": true },
					{ "width": "70%", "targets": 1, "orderable": true },
					{ "width": "20%", "targets": 2, "orderable": true }
				]
			});
	});
</script>
