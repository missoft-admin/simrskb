<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?
	$d_alok='';
	$st_margin='';
	$st_info='';
?>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mdata_obat_alkes_default" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mdata_obat_alkes_default/save','class="form-horizontal push-10-t"') ?>
			
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-12 control-label"><h5 align="left" class="text-primary"><b>Stok</b></h5></label>
				<div class="col-md-12">
					<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="stokreorder">Stok Reorder</label>
				<div class="col-md-2">
					<input type="text"  <?=$st_info?> class="form-control number" id="stokreorder" placeholder="Stok Reorder" name="stokreorder" value="{stokreorder}" required="" aria-required="true">
				</div>
				<label class="col-md-2 control-label" for="stokminimum">Stok Minimum</label>
				<div class="col-md-2">
					<input type="text"  <?=$st_info?> class="form-control number" id="stokminimum" placeholder="Stok Minimum" name="stokminimum" value="{stokminimum}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-12 control-label"><h5 align="left"  class="text-primary"><b>Margin (%)</b></h5></label>
				<div class="col-md-12">
					<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="marginumum">Umum</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="text"  <?=$st_margin?> class="form-control number" id="marginumum" placeholder="Umum" name="marginumum" value="{marginumum}" required="" aria-required="true">
						<span class="input-group-addon">%</span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="marginasuransi">Perusahaan Asuransi</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="text"  <?=$st_margin?> class="form-control number" id="marginasuransi" placeholder="Asuransi" name="marginasuransi" value="{marginasuransi}" required="" aria-required="true">
						<span class="input-group-addon">%</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="marginjasaraharja">Jasa Raharja</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="text" <?=$st_margin?>  class="form-control number" id="marginjasaraharja" placeholder=" Jasa Raharja" name="marginjasaraharja" value="{marginjasaraharja}" required="" aria-required="true">
						<span class="input-group-addon">%</span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="marginasuransi">BPJS Kesehatan</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="text"  <?=$st_margin?> class="form-control number" id="marginbpjskesehatan" placeholder="BPJS Kesehatan" name="marginbpjskesehatan" value="{marginbpjskesehatan}" required="" aria-required="true">
						<span class="input-group-addon">%</span>
					</div>
				</div>
			</div>
		
			<div class="form-group">
				<label class="col-md-3 control-label" for="marginbpjstenagakerja">BPJS Ketenagakerjaan</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="text"  <?=$st_margin?> class="form-control number" id="marginbpjstenagakerja" placeholder="BPJS Ketenagakerjaan" name="marginbpjstenagakerja" value="{marginbpjstenagakerja}" required="" aria-required="true">
						<span class="input-group-addon">%</span>
					</div>
				</div>
			</div>
			
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-12 control-label"><h5 align="left"  class="text-primary"><b>Alokasi</b></h5></label>
				<div class="col-md-12">
					<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<div class="col-xs-2">
								<div class="checkbox">
									<label for="alokasiumum">
									<input type="checkbox" <?=$d_alok?>  <?=($alokasiumum == 1 ? 'checked':'')?> id="alokasiumum" name="alokasiumum" value="1"> Umum
									</label>
								</div>
							</div>
							<div class="col-xs-2">
								<div class="checkbox">
									<label for="alokasiasuransi">
									<input type="checkbox" <?=$d_alok?>  <?=($alokasiasuransi == 1 ? 'checked':'')?> id="alokasiasuransi" name="alokasiasuransi" value="1"> Perusahaan Asuransi
									</label>
								</div>
							</div>
							<div class="col-xs-2">
								<div class="checkbox">
									<label for="alokasijasaraharja">
									<input type="checkbox" <?=$d_alok?>  <?=($alokasijasaraharja == 1 ? 'checked':'')?> id="alokasijasaraharja" name="alokasijasaraharja" value="1"> Jasa Raharja
									</label>
								</div>
							</div>
							<div class="col-xs-2">
								<div class="checkbox">
									<label for="alokasibpjskesehatan">
									<input type="checkbox" <?=$d_alok?>  <?=($alokasibpjskesehatan == 1 ? 'checked':'')?> id="alokasibpjskesehatan" name="alokasibpjskesehatan" value="1"> BPJS Kesehatan
									</label>
								</div>
							</div>
							<div class="col-xs-2">
								<div class="checkbox">
									<label for="alokasibpjstenagakerja">
									<input type="checkbox" <?=$d_alok?>  <?=($alokasibpjstenagakerja == 1 ? 'checked':'')?> id="alokasibpjstenagakerja" name="alokasibpjstenagakerja" value="1"> BPJS Ketenagakerjaan
									</label>
								</div>
							</div>
						</div>
						
						
						
					</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success"id="btn_simpan"  type="submit">Simpan</button>
					<a href="{base_url}mdata_obat_alkes_default" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(".number").number(true, 0, '.', ',');
	

	$("#marginumum, #marginasuransi, #marginjasaraharja, #marginbpjskesehatan, #marginbpjstenagakerja").keyup(function(){
		if($(this).val().replace(/,/g, '') > 100){
			$(this).val(100);
		}
	});
	$(document.body).on('click', '#btn_simpan' ,function(){
		$('input').removeAttr("disabled")
		$('.js-select2').removeAttr("disabled")
		// alert('Simpan');return false;
		return true;
        // alert($("#st_email").val());
        
    });
</script>
