<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<div class="row items-push">
			<div class="col-md-6">
				<div class="col-xs-12">
					<!-- Mini Stats -->
					<a class="block block-link-hover3" href="{base_url}tbooking/poliklinik">
						<table class="block-table text-center">
							<tbody>
								<tr>
									<td class="bg-primary" style="width: 50%;">
										<div class="push-30 push-30-t">
											<i class="fa fa-universal-access fa-3x text-white"></i>
										</div>
									</td>
									<td class="bg-gray-lighter" style="width: 50%;">
										<div class="h1 font-w700"><span class="h2 text-muted">+</span> <span id="jumlah_reservasi_dokter">-</span></div>
										<div class="h5 text-muted text-uppercase push-5-t">RESERVASI DOKTER</div>
									</td>
								</tr>
							</tbody>
						</table>
					</a>
					
				</div>
				<div class="col-md-6">
					<h4 class="text-primary">DAFTAR RESERVASI DOKTER</h4>
				</div>
				<div class="col-md-6">
					<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
						<input class="form-control" type="text" id="tanggal_1" name="tanggal_1" placeholder="From" value="{tanggal_1}"/>
						<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
						<input class="form-control" type="text" id="tanggal_2" name="tanggal_2" placeholder="To" value="{tanggal_2}"/>
					</div>
				</div>
				<div class="col-md-12 push-10-t">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="index_poli">
							<thead>
								<tr>
									<th width="10%">No</th>
									<th width="10%">Jenis</th>
									<th width="10%">Pasien</th>
									<th width="10%">Tujuan</th>
									<th width="10%">Tanggal </th>
									<th width="10%">Kelompok Pasien</th>
									<th width="10%">Status </th>
									<th width="10%">Tanggal Reservasi</th>
									<th width="15%">Action</th>
								   
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
            </div>
			<div class="col-md-6">
               <div class="col-xs-12">
					<!-- Mini Stats -->
					<a class="block block-link-hover3" href="{base_url}tbooking_rehab/poliklinik">
						<table class="block-table text-center">
							<tbody>
								<tr>
									<td class="bg-success" style="width: 50%;">
										<div class="push-30 push-30-t">
											<i class="si si-chemistry fa-3x text-white"></i>
										</div>
									</td>
									<td class="bg-gray-lighter" style="width: 50%;">
										<div class="h1 font-w700"><span class="h2 text-muted">+</span> <span id="jumlah_reservasi_rehab">-</span></div>
										<div class="h5 text-muted text-uppercase push-5-t ">RESERVASI REHABILITAS</div>
									</td>
								</tr>
							</tbody>
						</table>
					</a>
					
				</div>
				<div class="col-md-6">
					<h4 class="text-success">DAFTAR RESERVASI RAHABILITAS MEDIS</h4>
				</div>
				<div class="col-md-6">
					<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
						<input class="form-control" type="text" id="tanggal_r_1" name="tanggal_r_1" placeholder="From" value="{tanggal_1}"/>
						<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
						<input class="form-control" type="text" id="tanggal_r_2" name="tanggal_r_2" placeholder="To" value="{tanggal_2}"/>
					</div>
				</div>
				<div class="col-md-12 push-10-t">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="index_rehab">
							<thead>
								<tr>
									<th width="10%">No</th>
									<th width="10%">Jenis</th>
									<th width="10%">Pasien</th>
									<th width="10%">Tujuan</th>
									<th width="10%">Tanggal </th>
									<th width="10%">Kelompok Pasien</th>
									<th width="10%">Status </th>
									<th width="10%">Tanggal Reservasi</th>
									<th width="15%">Action</th>
								   
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>        
            </div>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
$(document).ready(function(){	
	// load_index_poliklinik();
	// load_index_rehab();
})
$("#tanggal_1,#tanggal_2").change(function(){
		
	load_index_poliklinik();
});
$("#tanggal_r_1,#tanggal_r_2").change(function(){
		
	load_index_rehab();
});

function load_jumlah_reservasi(){
	$("#cover-spin").show();
	let tanggal_1=$("#tanggal_1").val();
	let tanggal_2=$("#tanggal_2").val();
	$.ajax({
		url: '{site_url}tbooking_dashboard/load_jumlah_reservasi/',
		type: "POST" ,
		dataType: 'json',
		data : {
				tanggal_1:tanggal_1,
				tanggal_2:tanggal_2,
			   },
		success: function(data) {
			$("#cover-spin").hide();
			$("#jumlah_reservasi_dokter").text(data.jumlah_reservasi_dokter);
		}
	});
}
function load_jumlah_reservasi_rehab(){
	let tanggal_1=$("#tanggal_r_1").val();
	let tanggal_2=$("#tanggal_r_2").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tbooking_dashboard/load_jumlah_reservasi_rehab/',
		type: "POST" ,
		dataType: 'json',
		data : {
				tanggal_1:tanggal_1,
				tanggal_2:tanggal_2,
			   },
		success: function(data) {
			$("#jumlah_reservasi_rehab").text(data.jumlah_reservasi_rehab);
			$("#cover-spin").hide();
		}
	});
}
function load_index_poliklinik(){
	$('#index_poli').DataTable().destroy();	
	let tanggal_1=$("#tanggal_1").val();
	let tanggal_2=$("#tanggal_2").val();
	table = $('#index_poli').DataTable({
            autoWidth: false,
            searching: true,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"scrollY": "400px",
			  "scrollCollapse": true,
			  "paging": false,
			"columnDefs": [
					{ "width": "3%", "targets": 0,  className: "text-center" },
					{ "width": "10%", "targets": [1,2,3,6,7,8],  className: "text-center" },
					// { "width": "12%", "targets": [4,10],  className: "text-center" },
					// { "width": "15%", "targets": 5,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}tbooking_dashboard/load_index_poliklinik', 
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						
					   }
            }
        });
	load_jumlah_reservasi();
}
function load_index_rehab(){
	$('#index_rehab').DataTable().destroy();	
	let tanggal_1=$("#tanggal_r_1").val();
	let tanggal_2=$("#tanggal_r_2").val();
	table = $('#index_rehab').DataTable({
            autoWidth: false,
            searching: true,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"scrollY": "400px",
			  "scrollCollapse": true,
			  "paging": false,
			"columnDefs": [
					{ "width": "3%", "targets": 0,  className: "text-center" },
					{ "width": "10%", "targets": [1,2,3,6,7,8],  className: "text-center" },
					// { "width": "12%", "targets": [4,10],  className: "text-center" },
					// { "width": "15%", "targets": 5,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}tbooking_dashboard/load_index_rehabilitas', 
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						
					   }
            }
        });
	load_jumlah_reservasi_rehab();
}


</script>