<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}minterval" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('minterval/save','class="form-horizontal push-10-t"') ?>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama Frekuensi / Interval Pemberian</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="label_interval">Nama Label Frekuensi / Interval Pemberian</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="label_interval" placeholder="Label" name="label_interval" value="{label_interval}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="st_default">Default</label>
				<div class="col-md-7">
					<select id="st_default" name="st_default" class="form-control select2insidemodal" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="0" <?=($st_default==0?'selected':'')?>>TIDAK</option>
						<option value="1" <?=($st_default==1?'selected':'')?>>YA</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="jumlah_pemberian">Jumlah Pemberian</label>
				<div class="col-md-7">
					<input class="form-control" id="jumlah_pemberian" placeholder="Jumlah Pemberian" name="jumlah_pemberian" value="{jumlah_pemberian}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="jumlah_setiap_pemberian">Kuantitas Setiap Pemberian</label>
				<div class="col-md-7">
					<input type="text" class="form-control decimal" id="jumlah_setiap_pemberian" placeholder="Kuantitas Setiap Pemberian" name="jumlah_setiap_pemberian" value="{jumlah_setiap_pemberian}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="interval_waktu">Interval Waktu Pemberian</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="interval_waktu" placeholder="Interval Waktu Pemberian" name="interval_waktu" value="{interval_waktu}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}minterval" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,1,'.',',');
	
});
</script>