<?php declare(strict_types=1);
echo ErrorSuccess($this->session); ?>
<?php if ('' != $error) {
    echo ErrorMessage($error);
}?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}mtarif_laboratorium/index/<?php echo $this->uri->segment(3); ?>"
					class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mtarif_laboratorium/save', 'class="form-horizontal push-10-t" id="form-work"'); ?>
		<div class="form-group">
			<label class="col-md-3 control-label" for="idtipe">Tipe</label>
			<div class="col-md-7">
				<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;"
					data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<option value="1" <?php echo 1 == $idtipe ? 'selected' : ''; ?>>Umum
					</option>
					<option value="2" <?php echo 2 == $idtipe ? 'selected' : ''; ?>>Pathologi
						Anatomi</option>
					<option value="3" <?php echo 3 == $idtipe ? 'selected' : ''; ?>>PMI
					</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="level0">Head Parent</label>
			<div class="col-md-7">
				<select name="level0" id="level0" class="js-select2 form-control" style="width: 100%;"
					data-placeholder="Pilih Opsi">
					<?php foreach ($list_level0 as $index => $row) { ?>
					<? if ($index == 0) { ?>
						<option value="<?php echo $level0; ?>" <?php echo ($level0 === $path) ? 'selected' : ''; ?>>Root
						</option>
					<?php } ?>
					<option value="<?php echo $row->path; ?>" <?php echo ($level0 !== $path && $level0 === $row->path) ? 'selected' : ''; ?>><?php echo TreeView($row->level, $row->nama); ?>
					</option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="headerpath">Parent</label>
			<div class="col-md-7">
				<select name="headerpath" id="headerpath" class="js-select2 form-control" style="width: 100%;"
					data-placeholder="Pilih Opsi">
					<?php foreach ($list_parent as $row) { ?>
					<option value="<?php echo $row->path; ?>" <?php echo ($headerpath !== $path && $headerpath === $row->path) ? 'selected' : ''; ?>><?php echo TreeView($row->level, $row->nama); ?>
					</option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="idkelompok">Jenis</label>
			<div class="col-md-7">
				<label class="radio-inline" for="kelompok">
					<input type="radio" id="kelompok" checked name="idkelompok" onclick="toggleJenis('kelompok')"
						<?php echo 1 == $idkelompok ? 'checked' : ''; ?>
					value="1"> Kelompok
				</label>
				<label class="radio-inline" for="rincian">
					<input type="radio" id="rincian" name="idkelompok" onclick="toggleJenis('rincian')"
						<?php echo 0 == $idkelompok ? 'checked' : ''; ?>
					value="0"> Rincian
				</label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="kode">Kode Tarif</label>
			<div class="col-md-7">
				<input type="text" class="form-control" id="kode" placeholder="Kode Tarif" name="kode" value="{kode}"
					required="" aria-required="true">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Nama Tarif</label>
			<div class="col-md-7">
				<input type="text" class="form-control" id="nama" placeholder="Nama Tarif" name="nama" value="{nama}"
					required="" aria-required="true">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama_english">Nama Tarif (English Version)</label>
			<div class="col-md-7">
				<input type="text" class="form-control" id="nama_english" placeholder="Nama Tarif (English Version)" name="nama_english" value="{nama_english}"
					required="" aria-required="true">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="metode">Metode</label>
			<div class="col-md-7">
				<input type="text" class="form-control" id="metode" placeholder="Metode" name="metode" value="{metode}">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="sumber_spesimen">Sumber Spesimen Klinis</label>
			<div class="col-md-7">
				<select name="sumber_spesimen" id="sumber_spesimen" class="js-select2 form-control" style="width: 100%;"
					data-placeholder="Pilih Opsi">
					<?php foreach (list_variable_ref(90) as $row) { ?>
							<option value="<?php echo $row->id; ?>" <?php echo '' === $sumber_spesimen && '1' === $row->st_default ? 'selected' : ''; ?> <?php echo $row->id === $sumber_spesimen ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="group_test">Group Test</label>
			<div class="col-md-7">
				<select name="group_test" id="group_test" class="js-select2 form-control" style="width: 100%;"
					data-placeholder="Pilih Opsi">
					<?php foreach (get_all('merm_group_test') as $row) { ?>
					<option value="<?php echo $row->id; ?>" <?php echo ($group_test === $row->id) ? 'selected' : ''; ?>><?php echo $row->nama; ?>
					</option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group" id="form-paket">
			<label class="col-md-3 control-label" for="idpaket">Paket</label>
			<div class="col-md-7">
				<label class="radio-inline" for="paket">
					<input type="radio" id="paket" checked name="idpaket" onclick="togglePaket('paket')"
						<?php echo 1 == $idpaket ? 'checked' : ''; ?>
					value="1"> Ya
				</label>
				<label class="radio-inline" for="nonpaket">
					<input type="radio" id="nonpaket" name="idpaket" onclick="togglePaket('nonpaket')"
						<?php echo 0 == $idpaket ? 'checked' : ''; ?>
					value="0"> Tidak
				</label>
			</div>
		</div>
	</div>
	<div class="block-content" id="form-detail" style="display:none">
		<div class="table-responsive">
			<table class="table table-bordered table-striped" id="tarif-pemeriksaan">
				<thead>
					<tr>
						<th>Kelas</th>
						<th>Jasa Sarana</th>
						<th>Jasa Pelayanan</th>
						<th>BHP</th>
						<th>Biaya Perawatan</th>
						<th>Total Tarif</th>
					</tr>
				</thead>
				<tbody>
					<?php if ('create' == $this->uri->segment(2)) { ?>
						<tr style="background-color:#e0dfda">
							<td style="text-align:center">POLIKLINIK / IGD</td>
							<td style="display:none">
								<input type="hidden" class="kelaskamar" value="0">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number jasasarana" placeholder="Jasa Sarana" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="jasasarana">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_jasasarana" placeholder="Group Jasa Sarana" value="">
								<input type="text" class="form-control tarif_cito_persentase_jasasarana" placeholder="Tarif CITO Persentase - Jasa Sarana" value="">
								<input type="text" class="form-control tarif_cito_rp_jasasarana" placeholder="Tarif CITO Rupiah - Jasa Sarana" value="">
								<input type="text" class="form-control tarif_diskon_persentase_jasasarana" placeholder="Tarif Diskon Persentase - Jasa Sarana" value="">
								<input type="text" class="form-control tarif_diskon_rp_jasasarana" placeholder="Tarif Diskon Rupiah - Jasa Sarana" value="">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number jasapelayanan" placeholder="Jasa Pelayanan" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="jasapelayanan">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_jasapelayanan" placeholder="Group Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_cito_persentase_jasapelayanan" placeholder="Tarif CITO Persentase - Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_cito_rp_jasapelayanan" placeholder="Tarif CITO Rupiah - Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_diskon_persentase_jasapelayanan" placeholder="Tarif Diskon Persentase - Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_diskon_rp_jasapelayanan" placeholder="Tarif Diskon Rupiah - Jasa Pelayanan" value="">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number bhp" placeholder="BHP" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="bhp">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_bhp" placeholder="Group BHP" value="">
								<input type="text" class="form-control tarif_cito_persentase_bhp" placeholder="Tarif CITO Persentase - BHP" value="">
								<input type="text" class="form-control tarif_cito_rp_bhp" placeholder="Tarif CITO Rupiah - BHP" value="">
								<input type="text" class="form-control tarif_diskon_persentase_bhp" placeholder="Tarif Diskon Persentase - BHP" value="">
								<input type="text" class="form-control tarif_diskon_rp_bhp" placeholder="Tarif Diskon Rupiah - BHP" value="">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number biayaperawatan" placeholder="Biaya Perawatan" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="biayaperawatan">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_biayaperawatan" placeholder="Group Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_cito_persentase_biayaperawatan" placeholder="Tarif CITO Persentase - Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_cito_rp_biayaperawatan" placeholder="Tarif CITO Rupiah - Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_diskon_persentase_biayaperawatan" placeholder="Tarif Diskon Persentase - Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_diskon_rp_biayaperawatan" placeholder="Tarif Diskon Rupiah - Biaya Perawatan" value="">
							</td>
							<td>
								<input type="text" readonly class="form-control total" placeholder="Total" value="0">
							</td>
						</tr>
						<tr>
							<td style="text-align:center">I</td>
							<td style="display:none">
								<input type="hidden" class="kelaskamar" value="1">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number jasasarana" placeholder="Jasa Sarana" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="jasasarana">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_jasasarana" placeholder="Group Jasa Sarana" value="">
								<input type="text" class="form-control tarif_cito_persentase_jasasarana" placeholder="Tarif CITO Persentase - Jasa Sarana" value="">
								<input type="text" class="form-control tarif_cito_rp_jasasarana" placeholder="Tarif CITO Rupiah - Jasa Sarana" value="">
								<input type="text" class="form-control tarif_diskon_persentase_jasasarana" placeholder="Tarif Diskon Persentase - Jasa Sarana" value="">
								<input type="text" class="form-control tarif_diskon_rp_jasasarana" placeholder="Tarif Diskon Rupiah - Jasa Sarana" value="">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number jasapelayanan" placeholder="Jasa Pelayanan" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="jasapelayanan">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_jasapelayanan" placeholder="Group Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_cito_persentase_jasapelayanan" placeholder="Tarif CITO Persentase - Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_cito_rp_jasapelayanan" placeholder="Tarif CITO Rupiah - Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_diskon_persentase_jasapelayanan" placeholder="Tarif Diskon Persentase - Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_diskon_rp_jasapelayanan" placeholder="Tarif Diskon Rupiah - Jasa Pelayanan" value="">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number bhp" placeholder="BHP" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="bhp">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_bhp" placeholder="Group BHP" value="">
								<input type="text" class="form-control tarif_cito_persentase_bhp" placeholder="Tarif CITO Persentase - BHP" value="">
								<input type="text" class="form-control tarif_cito_rp_bhp" placeholder="Tarif CITO Rupiah - BHP" value="">
								<input type="text" class="form-control tarif_diskon_persentase_bhp" placeholder="Tarif Diskon Persentase - BHP" value="">
								<input type="text" class="form-control tarif_diskon_rp_bhp" placeholder="Tarif Diskon Rupiah - BHP" value="">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number biayaperawatan" placeholder="Biaya Perawatan" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="biayaperawatan">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_biayaperawatan" placeholder="Group Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_cito_persentase_biayaperawatan" placeholder="Tarif CITO Persentase - Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_cito_rp_biayaperawatan" placeholder="Tarif CITO Rupiah - Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_diskon_persentase_biayaperawatan" placeholder="Tarif Diskon Persentase - Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_diskon_rp_biayaperawatan" placeholder="Tarif Diskon Rupiah - Biaya Perawatan" value="">
							</td>
							<td>
								<input type="text" readonly class="form-control total" placeholder="Total" value="0">
							</td>
						</tr>
						<tr>
							<td style="text-align:center">II</td>
							<td style="display:none">
								<input type="hidden" class="kelaskamar" value="2">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number jasasarana" placeholder="Jasa Sarana" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="jasasarana">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_jasasarana" placeholder="Group Jasa Sarana" value="">
								<input type="text" class="form-control tarif_cito_persentase_jasasarana" placeholder="Tarif CITO Persentase - Jasa Sarana" value="">
								<input type="text" class="form-control tarif_cito_rp_jasasarana" placeholder="Tarif CITO Rupiah - Jasa Sarana" value="">
								<input type="text" class="form-control tarif_diskon_persentase_jasasarana" placeholder="Tarif Diskon Persentase - Jasa Sarana" value="">
								<input type="text" class="form-control tarif_diskon_rp_jasasarana" placeholder="Tarif Diskon Rupiah - Jasa Sarana" value="">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number jasapelayanan" placeholder="Jasa Pelayanan" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="jasapelayanan">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_jasapelayanan" placeholder="Group Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_cito_persentase_jasapelayanan" placeholder="Tarif CITO Persentase - Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_cito_rp_jasapelayanan" placeholder="Tarif CITO Rupiah - Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_diskon_persentase_jasapelayanan" placeholder="Tarif Diskon Persentase - Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_diskon_rp_jasapelayanan" placeholder="Tarif Diskon Rupiah - Jasa Pelayanan" value="">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number bhp" placeholder="BHP" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="bhp">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_bhp" placeholder="Group BHP" value="">
								<input type="text" class="form-control tarif_cito_persentase_bhp" placeholder="Tarif CITO Persentase - BHP" value="">
								<input type="text" class="form-control tarif_cito_rp_bhp" placeholder="Tarif CITO Rupiah - BHP" value="">
								<input type="text" class="form-control tarif_diskon_persentase_bhp" placeholder="Tarif Diskon Persentase - BHP" value="">
								<input type="text" class="form-control tarif_diskon_rp_bhp" placeholder="Tarif Diskon Rupiah - BHP" value="">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number biayaperawatan" placeholder="Biaya Perawatan" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="biayaperawatan">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_biayaperawatan" placeholder="Group Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_cito_persentase_biayaperawatan" placeholder="Tarif CITO Persentase - Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_cito_rp_biayaperawatan" placeholder="Tarif CITO Rupiah - Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_diskon_persentase_biayaperawatan" placeholder="Tarif Diskon Persentase - Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_diskon_rp_biayaperawatan" placeholder="Tarif Diskon Rupiah - Biaya Perawatan" value="">
							</td>
							<td>
								<input type="text" readonly class="form-control total" placeholder="Total" value="0">
							</td>
						</tr>
						<tr>
							<td style="text-align:center">III</td>
							<td style="display:none">
								<input type="hidden" class="kelaskamar" value="3">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number jasasarana" placeholder="Jasa Sarana" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="jasasarana">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_jasasarana" placeholder="Group Jasa Sarana" value="">
								<input type="text" class="form-control tarif_cito_persentase_jasasarana" placeholder="Tarif CITO Persentase - Jasa Sarana" value="">
								<input type="text" class="form-control tarif_cito_rp_jasasarana" placeholder="Tarif CITO Rupiah - Jasa Sarana" value="">
								<input type="text" class="form-control tarif_diskon_persentase_jasasarana" placeholder="Tarif Diskon Persentase - Jasa Sarana" value="">
								<input type="text" class="form-control tarif_diskon_rp_jasasarana" placeholder="Tarif Diskon Rupiah - Jasa Sarana" value="">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number jasapelayanan" placeholder="Jasa Pelayanan" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="jasapelayanan">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_jasapelayanan" placeholder="Group Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_cito_persentase_jasapelayanan" placeholder="Tarif CITO Persentase - Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_cito_rp_jasapelayanan" placeholder="Tarif CITO Rupiah - Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_diskon_persentase_jasapelayanan" placeholder="Tarif Diskon Persentase - Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_diskon_rp_jasapelayanan" placeholder="Tarif Diskon Rupiah - Jasa Pelayanan" value="">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number bhp" placeholder="BHP" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="bhp">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_bhp" placeholder="Group BHP" value="">
								<input type="text" class="form-control tarif_cito_persentase_bhp" placeholder="Tarif CITO Persentase - BHP" value="">
								<input type="text" class="form-control tarif_cito_rp_bhp" placeholder="Tarif CITO Rupiah - BHP" value="">
								<input type="text" class="form-control tarif_diskon_persentase_bhp" placeholder="Tarif Diskon Persentase - BHP" value="">
								<input type="text" class="form-control tarif_diskon_rp_bhp" placeholder="Tarif Diskon Rupiah - BHP" value="">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number biayaperawatan" placeholder="Biaya Perawatan" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="biayaperawatan">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_biayaperawatan" placeholder="Group Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_cito_persentase_biayaperawatan" placeholder="Tarif CITO Persentase - Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_cito_rp_biayaperawatan" placeholder="Tarif CITO Rupiah - Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_diskon_persentase_biayaperawatan" placeholder="Tarif Diskon Persentase - Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_diskon_rp_biayaperawatan" placeholder="Tarif Diskon Rupiah - Biaya Perawatan" value="">
							</td>
							<td>
								<input type="text" readonly class="form-control total" placeholder="Total" value="0">
							</td>
						</tr>
						<tr>
							<td style="text-align:center">UTAMA</td>
							<td style="display:none">
								<input type="hidden" class="kelaskamar" value="4">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number jasasarana" placeholder="Jasa Sarana" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="jasasarana">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_jasasarana" placeholder="Group Jasa Sarana" value="">
								<input type="text" class="form-control tarif_cito_persentase_jasasarana" placeholder="Tarif CITO Persentase - Jasa Sarana" value="">
								<input type="text" class="form-control tarif_cito_rp_jasasarana" placeholder="Tarif CITO Rupiah - Jasa Sarana" value="">
								<input type="text" class="form-control tarif_diskon_persentase_jasasarana" placeholder="Tarif Diskon Persentase - Jasa Sarana" value="">
								<input type="text" class="form-control tarif_diskon_rp_jasasarana" placeholder="Tarif Diskon Rupiah - Jasa Sarana" value="">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number jasapelayanan" placeholder="Jasa Pelayanan" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="jasapelayanan">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_jasapelayanan" placeholder="Group Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_cito_persentase_jasapelayanan" placeholder="Tarif CITO Persentase - Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_cito_rp_jasapelayanan" placeholder="Tarif CITO Rupiah - Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_diskon_persentase_jasapelayanan" placeholder="Tarif Diskon Persentase - Jasa Pelayanan" value="">
								<input type="text" class="form-control tarif_diskon_rp_jasapelayanan" placeholder="Tarif Diskon Rupiah - Jasa Pelayanan" value="">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number bhp" placeholder="BHP" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="bhp">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_bhp" placeholder="Group BHP" value="">
								<input type="text" class="form-control tarif_cito_persentase_bhp" placeholder="Tarif CITO Persentase - BHP" value="">
								<input type="text" class="form-control tarif_cito_rp_bhp" placeholder="Tarif CITO Rupiah - BHP" value="">
								<input type="text" class="form-control tarif_diskon_persentase_bhp" placeholder="Tarif Diskon Persentase - BHP" value="">
								<input type="text" class="form-control tarif_diskon_rp_bhp" placeholder="Tarif Diskon Rupiah - BHP" value="">
							</td>
							<td>
								<div class="input-group" style="display: flex;">
										<input type="text" class="form-control number biayaperawatan" placeholder="Biaya Perawatan" value="0">
										<div class="input-group-append">
												<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="biayaperawatan">
														<i class="fa fa-gear"></i>
												</button>
										</div>
								</div>
							</td>
							<td style="display:none">
								<input type="text" class="form-control group_biayaperawatan" placeholder="Group Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_cito_persentase_biayaperawatan" placeholder="Tarif CITO Persentase - Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_cito_rp_biayaperawatan" placeholder="Tarif CITO Rupiah - Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_diskon_persentase_biayaperawatan" placeholder="Tarif Diskon Persentase - Biaya Perawatan" value="">
								<input type="text" class="form-control tarif_diskon_rp_biayaperawatan" placeholder="Tarif Diskon Rupiah - Biaya Perawatan" value="">
							</td>
							<td>
								<input type="text" readonly class="form-control total" placeholder="Total" value="0">
							</td>
						</tr>
					<?php } else { ?>
						<?php foreach ($list_tarif as $row) { ?>
							<tr style="background-color:<?php echo '0' == $row->kelas ? '#e0dfda' : ''; ?>">
								<td style="text-align:center">
									<?php echo GetKelas($row->kelas); ?>
								</td>
								<td style="display:none">
									<input type="hidden" class="kelaskamar" value="<?php echo $row->kelas; ?>">
								</td>
								<td>
									<div class="input-group" style="display: flex;">
											<input type="text" class="form-control number jasasarana" placeholder="Jasa Sarana" value="<?php echo $row->jasasarana; ?>">
											<div class="input-group-append">
													<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="jasasarana">
															<i class="fa fa-gear"></i>
													</button>
											</div>
									</div>
								</td>
								<td style="display:none">
									<input type="text" class="form-control group_jasasarana" placeholder="Group Jasa Sarana" value="<?php echo $row->group_jasasarana; ?>">
									<input type="text" class="form-control tarif_cito_persentase_jasasarana" placeholder="Tarif CITO Persentase - Jasa Sarana" value="<?php echo $row->tarif_cito_persentase_jasasarana; ?>">
									<input type="text" class="form-control tarif_cito_rp_jasasarana" placeholder="Tarif CITO Rupiah - Jasa Sarana" value="<?php echo $row->tarif_cito_rp_jasasarana; ?>">
									<input type="text" class="form-control tarif_diskon_persentase_jasasarana" placeholder="Tarif Diskon Persentase - Jasa Sarana" value="<?php echo $row->tarif_diskon_persentase_jasasarana; ?>">
									<input type="text" class="form-control tarif_diskon_rp_jasasarana" placeholder="Tarif Diskon Rupiah - Jasa Sarana" value="<?php echo $row->tarif_diskon_rp_jasasarana; ?>">
								</td>
								<td>
									<div class="input-group" style="display: flex;">
											<input type="text" class="form-control number jasapelayanan" placeholder="Jasa Pelayanan" value="<?php echo $row->jasapelayanan; ?>">
											<div class="input-group-append">
													<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="jasapelayanan">
															<i class="fa fa-gear"></i>
													</button>
											</div>
									</div>
								</td>
								<td style="display:none">
									<input type="text" class="form-control group_jasapelayanan" placeholder="Group Jasa Pelayanan" value="<?php echo $row->group_jasapelayanan; ?>">
									<input type="text" class="form-control tarif_cito_persentase_jasapelayanan" placeholder="Tarif CITO Persentase - Jasa Pelayanan" value="<?php echo $row->tarif_cito_persentase_jasapelayanan; ?>">
									<input type="text" class="form-control tarif_cito_rp_jasapelayanan" placeholder="Tarif CITO Rupiah - Jasa Pelayanan" value="<?php echo $row->tarif_cito_rp_jasapelayanan; ?>">
									<input type="text" class="form-control tarif_diskon_persentase_jasapelayanan" placeholder="Tarif Diskon Persentase - Jasa Pelayanan" value="<?php echo $row->tarif_diskon_persentase_jasapelayanan; ?>">
									<input type="text" class="form-control tarif_diskon_rp_jasapelayanan" placeholder="Tarif Diskon Rupiah - Jasa Pelayanan" value="<?php echo $row->tarif_diskon_rp_jasapelayanan; ?>">
								</td>
								<td>
									<div class="input-group" style="display: flex;">
											<input type="text" class="form-control number bhp" placeholder="BHP" value="<?php echo $row->bhp; ?>">
											<div class="input-group-append">
													<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="bhp">
															<i class="fa fa-gear"></i>
													</button>
											</div>
									</div>
								</td>
								<td style="display:none">
									<input type="text" class="form-control group_bhp" placeholder="Group BHP" value="<?php echo $row->group_bhp; ?>">
									<input type="text" class="form-control tarif_cito_persentase_bhp" placeholder="Tarif CITO Persentase - BHP" value="<?php echo $row->tarif_cito_persentase_bhp; ?>">
									<input type="text" class="form-control tarif_cito_rp_bhp" placeholder="Tarif CITO Rupiah - BHP" value="<?php echo $row->tarif_cito_rp_bhp; ?>">
									<input type="text" class="form-control tarif_diskon_persentase_bhp" placeholder="Tarif Diskon Persentase - BHP" value="<?php echo $row->tarif_diskon_persentase_bhp; ?>">
									<input type="text" class="form-control tarif_diskon_rp_bhp" placeholder="Tarif Diskon Rupiah - BHP" value="<?php echo $row->tarif_diskon_rp_bhp; ?>">
								</td>
								<td>
									<div class="input-group" style="display: flex;">
											<input type="text" class="form-control number biayaperawatan" placeholder="Biaya Perawatan" value="<?php echo $row->biayaperawatan; ?>">
											<div class="input-group-append">
													<button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-target="#modalSettingTarifTambahan" data-label="biayaperawatan">
															<i class="fa fa-gear"></i>
													</button>
											</div>
									</div>
								</td>
								<td style="display:none">
									<input type="text" class="form-control group_biayaperawatan" placeholder="Group Biaya Perawatan" value="<?php echo $row->group_biayaperawatan; ?>">
									<input type="text" class="form-control tarif_cito_persentase_biayaperawatan" placeholder="Tarif CITO Persentase - Biaya Perawatan" value="<?php echo $row->tarif_cito_persentase_biayaperawatan; ?>">
									<input type="text" class="form-control tarif_cito_rp_biayaperawatan" placeholder="Tarif CITO Rupiah - Biaya Perawatan" value="<?php echo $row->tarif_cito_rp_biayaperawatan; ?>">
									<input type="text" class="form-control tarif_diskon_persentase_biayaperawatan" placeholder="Tarif Diskon Persentase - Biaya Perawatan" value="<?php echo $row->tarif_diskon_persentase_biayaperawatan; ?>">
									<input type="text" class="form-control tarif_diskon_rp_biayaperawatan" placeholder="Tarif Diskon Rupiah - Biaya Perawatan" value="<?php echo $row->tarif_diskon_rp_biayaperawatan; ?>">
								</td>
								<td>
									<input type="text" readonly class="form-control number total" placeholder="Total" value="<?php echo $row->total; ?>">
								</td>
							</tr>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>

	
	<div class="row" style="margin-top:-25px">
		<div class="block-content block-content-narrow">
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label"></label>
				<div class="col-md-7">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mtarif_laboratorium/index/<?php echo $this->uri->segment(3); ?>"
						class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
		</div>
	</div>

	<input type="hidden" id="old_headerpath" name="old_headerpath" value="{headerpath}" readonly>
	<input type="hidden" id="old_level0" name="old_level0" value="{level0}" readonly>
	<input type="hidden" id="path" name="path" value="{path}" readonly>
	<input type="hidden" id="level" name="level" value="{level}" readonly>
	<input type="hidden" id="data_tarif" name="data_tarif" />
	
	<br><br>

	<?php echo form_hidden('id', $id); ?>
	<?php echo form_close(); ?>
</div>

<!-- Modal Edit Label -->
<div class="modal fade" id="modalSettingTarifTambahan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="block-header bg-primary">
                <ul class="block-options">
                    <li>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </li>
                </ul>
                <h5 class="modal-title" id="modalSettingTarifTambahanLabel">Setting Tarif Tambahan</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12" for="">Setting Penambahan Tarif CITO Persentase</label>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input type="number" id="setting-penambahan-tarif-cito-persentase" class="form-control" name="setting-penambahan-tarif-cito-persentase" required="" aria-required="true" min="0" max="100" value="">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12" for="">Setting Penambahan Tarif CITO Rupiah</label>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input type="text" id="setting-penambahan-tarif-cito-rp" class="form-control number" name="setting-penambahan-tarif-cito-rp" required="" aria-required="true" min="0" max="100" value="">
                                    <span class="input-group-addon">Rp</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
								<br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12" for="">Setting Diskon Tarif Persentase</label>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input type="number" id="setting-diskon-tarif-persentase" class="form-control" name="setting-diskon-tarif-persentase" required="" aria-required="true" value="">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-12" for="">Setting Diskon Tarif Rupiah</label>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input type="text" id="setting-diskon-tarif-rp" class="form-control number" name="setting-diskon-tarif-rp" required="" aria-required="true" value="">
                                    <span class="input-group-addon">Rp</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btn-update-setting-tarif-tambahan" class="btn btn-sm btn-success" type="button" data-dismiss="modal">Simpan</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	let selectedLabel = "";
	let selectedRow;

	$(document).ready(function() {
		$(".number").number(true, 0, '.', ',');

		<?php if ('update' == $this->uri->segment(2)) { ?>
			var idkelompok = <?php echo $idkelompok; ?>;
			var idpaket = <?php echo $idpaket; ?>;
			var idkelompokparent = <?php echo $idkelompokparent; ?>;
			var idpaketparent = <?php echo $idpaketparent; ?>;

			<?php if (1 == $idkelompok) { ?>
				if (idkelompok == 1) {
					$("#form-paket").show();

					if (idpaket == 1) {
						$("#form-detail").show();

						$(".jasasarana").prop('readonly', false);
						$(".jasapelayanan").prop('readonly', false);
						$(".bhp").prop('readonly', false);
						$(".biayaperawatan").prop('readonly', false);
					} else {
						$("#form-detail").hide();

						$(".jasasarana").prop('readonly', true);
						$(".jasapelayanan").prop('readonly', true);
						$(".bhp").prop('readonly', true);
						$(".biayaperawatan").prop('readonly', true);
					}

				} else {
					$("#form-paket").hide();
					$("#form-detail").show();
				}
			<?php } else { ?>
				if (idkelompokparent == 1) {
					$("#form-paket").hide();

					if (idpaketparent == 1) {
						$("#form-detail").hide();

						$(".jasasarana").prop('readonly', true);
						$(".jasapelayanan").prop('readonly', true);
						$(".bhp").prop('readonly', true);
						$(".biayaperawatan").prop('readonly', true);
					} else {
						$("#form-detail").show();

						$(".jasasarana").prop('readonly', false);
						$(".jasapelayanan").prop('readonly', false);
						$(".bhp").prop('readonly', false);
						$(".biayaperawatan").prop('readonly', false);
					}

				} else {
					$("#form-paket").hide();
					$("#form-detail").show();
				}
			<?php } ?>
		<?php } ?>

		$(document).on("change", "#idtipe", function() {
			$('#level0').find('option').remove().end().append(
				'<option value="loading">Loading...</option>').val('loading');

			$.ajax({
				url: '{base_url}mtarif_laboratorium/find_manage_parent/' + $(this).val(),
				method: 'GET',
				dataType: 'json',
				success: function(data) {
					$("#level0").find("option").remove().end().append(data).val("");
				}
			});
		});

		$(document).on("change", "#level0", function() {
			$('#headerpath').find('option').remove().end().append(
				'<option value="loading">Loading...</option>').val('loading');

			// Get Subparent
			$.ajax({
				url: '{base_url}mtarif_laboratorium/find_manage_subparent/' + $(this).val(),
				method: 'GET',
				dataType: 'json',
				success: function(data) {
					$("#headerpath").find("option").remove().end().append(data).val("");
				}
			});

			// Get Path
			$.ajax({
				url: '{base_url}mtarif_laboratorium/get_child_level/' + $(this).val(),
				method: 'GET',
				dataType: 'json',
				success: function(data) {
					if ($('#level0 option:selected').text() == 'Root') {
						$("#path").val($('#level0 option:selected').val());
					} else {
						$("#path").val(data.path);
					}
					$("#level").val(data.level);

					if (data.idpaket == 1) {
						$(".jasasarana").prop('readonly', true);
						$(".jasapelayanan").prop('readonly', true);
						$(".bhp").prop('readonly', true);
						$(".biayaperawatan").prop('readonly', true);
					} else {
						$(".jasasarana").prop('readonly', false);
						$(".jasapelayanan").prop('readonly', false);
						$(".bhp").prop('readonly', false);
						$(".biayaperawatan").prop('readonly', false);
					}
				}
			});
		});

		$(document).on("change", "#headerpath", function() {
			$.ajax({
				url: '{base_url}mtarif_laboratorium/get_child_level/' + $(this).val(),
				method: 'GET',
				dataType: 'json',
				success: function(data) {
					$("#path").val(data.path);
					$("#level").val(data.level);

					if (data.idpaket == 1) {
						$(".jasasarana").prop('readonly', true);
						$(".jasapelayanan").prop('readonly', true);
						$(".bhp").prop('readonly', true);
						$(".biayaperawatan").prop('readonly', true);
					} else {
						$(".jasasarana").prop('readonly', false);
						$(".jasapelayanan").prop('readonly', false);
						$(".bhp").prop('readonly', false);
						$(".biayaperawatan").prop('readonly', false);
					}
				}
			});
		});

		$("#form-work").submit(function(e) {
			var form = this;

			$("#data_tarif").val(JSON.stringify(gatherAllDataFromTable()));

			swal({
				title: "Berhasil!",
				text: "Proses penyimpanan data.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});

		$(document).on("keyup", ".jasasarana", function() {
			var jasasarana = parseInt($(this).val().replace(/,/g, ''));
			var jasapelayanan = parseInt($(this).closest('tr').find(".jasapelayanan").val().replace(/,/g, ''));
			var bhp = parseInt($(this).closest('tr').find(".bhp").val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).closest('tr').find(".biayaperawatan").val().replace(/,/g, ''));
			var total = jasasarana + jasapelayanan + bhp + biayaperawatan;
			$(this).closest('tr').find(".total").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});

		$(document).on("keyup", ".jasapelayanan", function() {
			var jasasarana = parseInt($(this).closest('tr').find(".jasasarana").val().replace(/,/g, ''));
			var jasapelayanan = parseInt($(this).val().replace(/,/g, ''));
			var bhp = parseInt($(this).closest('tr').find(".bhp").val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).closest('tr').find(".biayaperawatan").val().replace(/,/g, ''));
			var total = jasasarana + jasapelayanan + bhp + biayaperawatan;
			$(this).closest('tr').find(".total").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});

		$(document).on("keyup", ".bhp", function() {
			var jasasarana = parseInt($(this).closest('tr').find(".jasasarana").val().replace(/,/g, ''));
			var jasapelayanan = parseInt($(this).closest('tr').find(".jasapelayanan").val().replace(/,/g, ''));
			var bhp = parseInt($(this).val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).closest('tr').find(".biayaperawatan").val().replace(/,/g, ''));

			var total = jasasarana + jasapelayanan + bhp + biayaperawatan;
			$(this).closest('tr').find(".total").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});

		$(document).on("keyup", ".biayaperawatan", function() {
			var jasasarana = parseInt($(this).closest('tr').find(".jasasarana").val().replace(/,/g, ''));
			var jasapelayanan = parseInt($(this).closest('tr').find(".jasapelayanan").val().replace(/,/g, ''));
			var bhp = parseInt($(this).closest('tr').find(".bhp").val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).val().replace(/,/g, ''));
			var total = jasasarana + jasapelayanan + bhp + biayaperawatan;

			$(this).closest('tr').find(".total").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});

		$(document).on("click", ".edit-button", function() {
			if ($(this).data('label') == 'jasasarana') {
				var label = 'Jasa Sarana';
			} else if ($(this).data('label') == 'jasapelayanan') {
				var label = 'Jasa Pelayanan';
			} else if ($(this).data('label') == 'bhp') {
				var label = 'BHP';
			} else if ($(this).data('label') == 'biayaperawatan') {
				var label = 'Biaya Perawatan';
			}

			$('#modalSettingTarifTambahanLabel').html('Setting Tarif Tambahan - ' + label);
		});
	});

	function toggleJenis(label) {
		if (label == 'kelompok') {
			$("#form-paket").show();
			$("#form-detail").hide();
		} else {
			$("#form-paket").hide();
			$("#form-detail").show();
			$("#nonpaket").prop("checked", true);
		}
	}

	function togglePaket(label) {
		if (label == 'nonpaket') {
			$("#form-detail").hide();
		} else {
			$("#form-detail").show();
		}
	}

	$(".edit-button").on("click", function () {
			selectedLabel = $(this).data("label");
			selectedRow = $(this).closest("tr");

			let citoPersentase = selectedRow.find(`.tarif_cito_persentase_${selectedLabel}`).val();
			let citoRp = selectedRow.find(`.tarif_cito_rp_${selectedLabel}`).val();
			let diskonPersentase = selectedRow.find(`.tarif_diskon_persentase_${selectedLabel}`).val();
			let diskonRp = selectedRow.find(`.tarif_diskon_rp_${selectedLabel}`).val();

			$("#setting-penambahan-tarif-cito-persentase").val(citoPersentase);
			$("#setting-penambahan-tarif-cito-rp").val(citoRp);
			$("#setting-diskon-tarif-persentase").val(diskonPersentase);
			$("#setting-diskon-tarif-rp").val(diskonRp);
	});

	$("#btn-update-setting-tarif-tambahan").on("click", function () {
			selectedRow.find(`.tarif_cito_persentase_${selectedLabel}`).val($("#setting-penambahan-tarif-cito-persentase").val());
			selectedRow.find(`.tarif_cito_rp_${selectedLabel}`).val($("#setting-penambahan-tarif-cito-rp").val());
			selectedRow.find(`.tarif_diskon_persentase_${selectedLabel}`).val($("#setting-diskon-tarif-persentase").val());
			selectedRow.find(`.tarif_diskon_rp_${selectedLabel}`).val($("#setting-diskon-tarif-rp").val());
	});

	function gatherDataFromRow(row) {
			var data = {
					'kelas': row.find('.kelaskamar').val(),
					'jasasarana': row.find('.jasasarana').val(),
					'group_jasasarana': row.find('.group_jasasarana').val(),
					'tarif_cito_persentase_jasasarana': row.find('.tarif_cito_persentase_jasasarana').val(),
					'tarif_cito_rp_jasasarana': row.find('.tarif_cito_rp_jasasarana').val(),
					'tarif_diskon_persentase_jasasarana': row.find('.tarif_diskon_persentase_jasasarana').val(),
					'tarif_diskon_rp_jasasarana': row.find('.tarif_diskon_rp_jasasarana').val(),
					'jasapelayanan': row.find('.jasapelayanan').val(),
					'group_jasapelayanan': row.find('.group_jasapelayanan').val(),
					'tarif_cito_persentase_jasapelayanan': row.find('.tarif_cito_persentase_jasapelayanan').val(),
					'tarif_cito_rp_jasapelayanan': row.find('.tarif_cito_rp_jasapelayanan').val(),
					'tarif_diskon_persentase_jasapelayanan': row.find('.tarif_diskon_persentase_jasapelayanan').val(),
					'tarif_diskon_rp_jasapelayanan': row.find('.tarif_diskon_rp_jasapelayanan').val(),
					'bhp': row.find('.bhp').val(),
					'group_bhp': row.find('.group_bhp').val(),
					'tarif_cito_persentase_bhp': row.find('.tarif_cito_persentase_bhp').val(),
					'tarif_cito_rp_bhp': row.find('.tarif_cito_rp_bhp').val(),
					'tarif_diskon_persentase_bhp': row.find('.tarif_diskon_persentase_bhp').val(),
					'tarif_diskon_rp_bhp': row.find('.tarif_diskon_rp_bhp').val(),
					'biayaperawatan': row.find('.biayaperawatan').val(),
					'group_biayaperawatan': row.find('.group_biayaperawatan').val(),
					'tarif_cito_persentase_biayaperawatan': row.find('.tarif_cito_persentase_biayaperawatan').val(),
					'tarif_cito_rp_biayaperawatan': row.find('.tarif_cito_rp_biayaperawatan').val(),
					'tarif_diskon_persentase_biayaperawatan': row.find('.tarif_diskon_persentase_biayaperawatan').val(),
					'tarif_diskon_rp_biayaperawatan': row.find('.tarif_diskon_rp_biayaperawatan').val(),
					'total': row.find('.total').val(),
			};
			return data;
	}

	function gatherAllDataFromTable() {
			var allData = [];

			$('#tarif-pemeriksaan tbody tr').each(function () {
					var rowData = gatherDataFromRow($(this));
					allData.push(rowData);
			});

			return allData;
	}
</script>