<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('153'))){ ?>
<div class="block">
    <div class="block-header">
        <?php if (UserAccesForm($user_acces_form,array('155'))){ ?>
        <ul class="block-options">
            <li>
                <a href="{base_url}mtarif_laboratorium/create/{idtipe}" class="btn"><i class="fa fa-plus"></i></a>
            </li>
        </ul>
        <?}?>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?php if (UserAccesForm($user_acces_form,array('154'))){ ?>
        <hr style="margin-top:0px">
        <?php echo form_open('mtarif_laboratorium/filter','class="form-horizontal" id="form-work"') ?>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="">Tipe</label>
                    <div class="col-md-7">
                        <select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Semua Tipe">
                            <option value="#" <?=($idtipe == '#') ? "selected" : "" ?>>Semua Tipe</option>
                            <option value="1" <?=($idtipe == '1' ? 'selected' : '')?>>Umum</option>
                            <option value="2" <?=($idtipe == '2' ? 'selected' : '')?>>Pathologi Anatomi</option>
                            <option value="3" <?=($idtipe == '3' ? 'selected' : '')?>>PMI</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="">Head Parent</label>
                    <div class="col-md-7">
                        <select name="idparent" id="idparent" class="js-select2 form-control" style="width: 100%;" data-placeholder="Semua Head Parent">
                            <option value="#" <?=($idparent == '#') ? "selected" : "" ?>>Semua Head Parent</option>
                            <?php foreach ($list_parent as $row) { ?>
                            <option value="<?=$row->path;?>" <?=($idparent === $row->path) ? "selected" : "" ?>><?=TreeView($row->level, $row->nama)?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="">Sub Parent</label>
                    <div class="col-md-7">
                        <select name="idsubparent[]" id="idsubparent" multiple class="js-select2 form-control" style="width: 100%;" data-placeholder="Semua Sub Parent">
                            <?php foreach ($list_subparent as $row) { ?>
                            <option value="<?=$row->path;?>" <?= in_array($row->path, explode("_", $idsubparent), true) ? "selected" : "" ?>><?=TreeView($row->level, $row->nama)?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="">Status</label>
                    <div class="col-md-7">
                        <select name="status_paket" id="status_paket" class="js-select2 form-control" style="width: 100%;" data-placeholder="Status">
                            <option value="9" <?=($status_paket == '9') ? "selected" : "" ?>>Semua Status</option>
                            <option value="1" <?=($status_paket == '1') ? "selected" : "" ?>>Paket</option>
                            <option value="0" <?=($status_paket == '0') ? "selected" : "" ?>>Rincian</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for=""></label>
                    <div class="col-md-7">
                        <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close() ?>
        <hr>
        <?}?>
        <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
// Initialize when page loads
jQuery(function() {
    BaseTableDatatables.init();
    $('#datatable-simrs').DataTable({
        "bSort": false,
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}mtarif_laboratorium/getIndex/{idtipe}/{idparent}/{idsubparent}/{status_paket}',
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "10%",
                "targets": 0,
                "orderable": true
            },
            {
                "width": "70%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "20%",
                "targets": 2,
                "orderable": true
            }
        ]
    });
});

$("#idtipe").change(function() {
    if ($(this).val() != '') {
        $.ajax({
            url: '{site_url}mtarif_laboratorium/find_index_parent/' + $(this).val(),
            dataType: "json",
            success: function(data) {
                $('#idparent').find('option').remove().end().append('<option value="#" selected>Semua</option>').val('#').trigger("liszt:updated");
                $('#idparent').append(data);
            }
        });
    }
});

$("#idparent").change(function() {
    if ($(this).val() != '') {
        $('#idsubparent').empty();

        $.ajax({
            url: '{site_url}mtarif_laboratorium/find_index_subparent/' + $(this).val(),
            dataType: "json",
            success: function(data) {
                $('#idsubparent').append(data);
            }
        });
    }
});
</script>
