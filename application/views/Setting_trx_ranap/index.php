<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<style>
.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
</style>
<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('2623'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" > GENERAL</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2624'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_2"> LABEL MENU</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2625'))){ ?>
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab_3" > AUTO VERIFIKASI</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2626'))){ ?>
		<li class="<?=($tab=='4'?'active':'')?>">
			<a href="#tab_4" onclick="load_batal_kwitansi()"> PEMBATALAN KWITANSI</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2627'))){ ?>
		<li class="<?=($tab=='5'?'active':'')?>">
			<a href="#tab_5" onclick="load_card()"> LOGIC CARD PASS</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2628'))){ ?>
		<li class="<?=($tab=='6'?'active':'')?>">
			<a href="#tab_6" > LABEL KWITANSI</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2629'))){ ?>
		<li class="<?=($tab=='7'?'active':'')?>">
			<a href="#tab_7"> LABEL CARD PASS</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('2623'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_info">
			<?php echo form_open('#','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label" for="st_edit_tgl_bayar">Izin Edit Pembayaran</label>
							<div class="col-md-2">
								<select id="st_edit_tgl_bayar" name="st_edit_tgl_bayar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_edit_tgl_bayar=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_edit_tgl_bayar=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							
							<label class="col-md-2 control-label div_catatan" for="st_simpan">Simpan</label>
							<div class="col-md-2 div_catatan">
								<select id="st_simpan" name="st_simpan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_simpan=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_simpan=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							
							<label class="col-md-2 control-label div_catatan" for="st_simpan_proses">Simpan & Proses Kwitansi</label>
							<div class="col-md-2 div_catatan">
								<select id="st_simpan_proses" name="st_simpan_proses" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_simpan_proses=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_simpan_proses=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label" for="st_batal">Batal</label>
							<div class="col-md-2">
								<select id="st_batal" name="st_batal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_batal=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_batal=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							
							<label class="col-md-2 control-label div_catatan" for="st_selesai">Selesai</label>
							<div class="col-md-2 div_catatan">
								<select id="st_selesai" name="st_selesai" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_selesai=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_selesai=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							
							<label class="col-md-2 control-label div_catatan" for="st_selesai_verifikasi">Selesai Verifikasi</label>
							<div class="col-md-2 div_catatan">
								<select id="st_selesai_verifikasi" name="st_selesai_verifikasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_selesai_verifikasi=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_selesai_verifikasi=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label" for="email_generate_rincian">Email Generate Rincian</label>
							<div class="col-md-10 div_catatan">
								<input class="js-tags-input form-control" type="text" id="email_generate_rincian" name="email_generate_rincian" value="<?=$email_generate_rincian?>">
							</div>
							
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label div_catatan" for="email_generate_kwitansi">Email Generate Kwitansi</label>
							<div class="col-md-6 div_catatan">
								<input class="js-tags-input form-control" type="text" id="email_generate_kwitansi" name="email_generate_kwitansi" value="<?=$email_generate_kwitansi?>">
							</div>
							<label class="col-md-2 control-label div_catatan" for="waktu_batal">Waktu Pembatalan</label>
							<div class="col-md-2 div_catatan">
								<div class="input-group">
									<input class="form-control number" type="text" id="waktu_batal" name="waktu_batal" value="{waktu_batal}" placeholder="">
									<span class="input-group-addon">Hari</span>
								</div>
							</div>
							
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label div_catatan" for="st_qrcode">QR CODE</label>
							<div class="col-md-2 div_catatan">
								<select id="st_qrcode" name="st_qrcode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_qrcode=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_qrcode=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							
							<label class="col-md-2 control-label div_catatan" for="st_verifikasi_disabled">Validasi Saat Transaksi Disabled</label>
							<div class="col-md-2 div_catatan">
								<select id="st_verifikasi_disabled" name="st_verifikasi_disabled" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_verifikasi_disabled=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_verifikasi_disabled=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							<label class="col-md-2 control-label div_catatan" for="st_edit_tgl_setoran">Tanggal proses Setoran Kas Tidak Bisa Diedit</label>
							<div class="col-md-2 div_catatan">
								<select id="st_edit_tgl_setoran" name="st_edit_tgl_setoran" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_edit_tgl_setoran=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_edit_tgl_setoran=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label div_catatan" for="st_kwitansi_all">Kwitansi All</label>
							<div class="col-md-2 div_catatan">
								<select id="st_kwitansi_all" name="st_kwitansi_all" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_kwitansi_all=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_kwitansi_all=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							
							<label class="col-md-2 control-label div_catatan" for="st_kwitansi_kontraktor">Kwitansi Asuransi</label>
							<div class="col-md-2 div_catatan">
								<select id="st_kwitansi_kontraktor" name="st_kwitansi_kontraktor" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_kwitansi_kontraktor=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_kwitansi_kontraktor=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							<label class="col-md-2 control-label div_catatan" for="st_kwitansi_exces">Kwitansi Excess</label>
							<div class="col-md-2 div_catatan">
								<select id="st_kwitansi_exces" name="st_kwitansi_exces" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_kwitansi_exces=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_kwitansi_exces=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label div_catatan" for="st_simpan_kwitansi">Simpan Kwitansi</label>
							<div class="col-md-2 div_catatan">
								<select id="st_simpan_kwitansi" name="st_simpan_kwitansi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_simpan_kwitansi=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_simpan_kwitansi=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							
							<label class="col-md-2 control-label div_catatan" for="st_simpan_generate_kwitansi">Simpan & Generate Kwitansi</label>
							<div class="col-md-2 div_catatan">
								<select id="st_simpan_generate_kwitansi" name="st_simpan_generate_kwitansi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_simpan_generate_kwitansi=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_simpan_generate_kwitansi=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							<label class="col-md-2 control-label div_catatan" for="st_edit_tgl_kwitansi">Edit Tanggal Kwitansi</label>
							<div class="col-md-2 div_catatan">
								<select id="st_edit_tgl_kwitansi" name="st_edit_tgl_kwitansi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_edit_tgl_kwitansi=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_edit_tgl_kwitansi=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label div_catatan" for="st_default_nama">Default Terima Dari Nama pasien</label>
							<div class="col-md-2 div_catatan">
								<select id="st_default_nama" name="st_default_nama" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_default_nama=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_default_nama=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							
							<label class="col-md-2 control-label div_catatan" for="label_kode_kwitansi">Kode Kwitansi</label>
							<div class="col-md-2 div_catatan">
								<input class="form-control" type="text" id="label_kode_kwitansi" style="width:100%" name="label_kode_kwitansi" value="{label_kode_kwitansi}" placeholder="">
							</div>
							<label class="col-md-2 control-label div_catatan" for="st_qrcode_kwitansi">QR Code Kwitansi</label>
							<div class="col-md-2 div_catatan">
								<select id="st_qrcode_kwitansi" name="st_qrcode_kwitansi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_qrcode_kwitansi=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_qrcode_kwitansi=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label div_catatan" for="waktu_batal_kwitansi">Waktu Pembatalan Kwitansi</label>
							<div class="col-md-2 div_catatan">
								<div class="input-group">
									<input class="form-control number" type="text" id="waktu_batal_kwitansi" name="waktu_batal_kwitansi" value="{waktu_batal_kwitansi}" placeholder="">
									<span class="input-group-addon">Hari</span>
								</div>
							</div>
							
							<label class="col-md-2 control-label div_catatan" for="waktu_maksimum_proses">Waktu Maksimum Proses Kwitansi</label>
							<div class="col-md-2 div_catatan">
								<div class="input-group">
									<input class="form-control number" type="text" id="waktu_maksimum_proses" name="waktu_maksimum_proses" value="{waktu_maksimum_proses}" placeholder="">
									<span class="input-group-addon">Hari</span>
								</div>
							</div>
							<label class="col-md-2 control-label div_catatan" for="st_edit_tgl_card">Edit Tanggal Card Pass</label>
							<div class="col-md-2 div_catatan">
								<select id="st_edit_tgl_card" name="st_edit_tgl_card" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_edit_tgl_card=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_edit_tgl_card=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
							
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label div_catatan" for="email_card_pass">Email Card Pass</label>
							<div class="col-md-10 div_catatan">
								<input class="js-tags-input form-control" type="text" id="email_card_pass" name="email_card_pass" value="<?=$email_card_pass?>">
							</div>
							
						</div>

						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label div_catatan" for="label_deskripsi_kwitansi">Setting Deskripsi Kwitansi</label>
							<div class="col-md-10 div_catatan">
								<textarea class="form-control" id="label_deskripsi_kwitansi"><?=$label_deskripsi_kwitansi?></textarea>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 15px;">
							<label class="col-md-2 control-label div_catatan" for="qr_trx_ranap">&nbsp;</label>
							<div class="col-md-2 ">
								<button class="btn btn-primary"  type="button" onclick="simpan_general()" ><i class="fa fa-save"></i> Simpan</button>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2624'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?> " id="tab_2">
			<?php echo form_open('#','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Rawat Inap Ruangan</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_ranap_ruangan_section" name="label_ranap_ruangan_section" value="{label_ranap_ruangan_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_ranap_ruangan_detail" name="label_ranap_ruangan_detail" value="{label_ranap_ruangan_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Rawat Inap FUll Care</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_ranap_fc_section" name="label_ranap_fc_section" value="{label_ranap_fc_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_ranap_fc_detail" name="label_ranap_fc_detail" value="{label_ranap_fc_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Rawat Inap ECG</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_ranap_ecg_section" name="label_ranap_ecg_section" value="{label_ranap_ecg_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_ranap_ecg_detail" name="label_ranap_ecg_detail" value="{label_ranap_ecg_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Rawat Inap Visite Dokter</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_ranap_visite_section" name="label_ranap_visite_section" value="{label_ranap_visite_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_ranap_visite_detail" name="label_ranap_visite_detail" value="{label_ranap_visite_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Rawat Inap Sewa Alat</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_ranap_alat_section" name="label_ranap_alat_section" value="{label_ranap_alat_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_ranap_alat_detail" name="label_ranap_alat_detail" value="{label_ranap_alat_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Rawat Inap Ambulance</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_ranap_ambulance_section" name="label_ranap_ambulance_section" value="{label_ranap_ambulance_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_ranap_ambulance_detail" name="label_ranap_ambulance_detail" value="{label_ranap_ambulance_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Rawat Inap Obat</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_ranap_obat_section" name="label_ranap_obat_section" value="{label_ranap_obat_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_ranap_obat_detail" name="label_ranap_obat_detail" value="{label_ranap_obat_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Rawat Inap Alat Kesehatan</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_ranap_alkes_section" name="label_ranap_alkes_section" value="{label_ranap_alkes_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_ranap_alkes_detail" name="label_ranap_alkes_detail" value="{label_ranap_alkes_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Rawat Inap Lain Lain</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_ranap_lain_section" name="label_ranap_lain_section" value="{label_ranap_lain_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_ranap_lain_detail" name="label_ranap_lain_detail" value="{label_ranap_lain_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan IGD Pelayanan</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_igd_layanan_section" name="label_igd_layanan_section" value="{label_igd_layanan_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_igd_layanan_detail" name="label_igd_layanan_detail" value="{label_igd_layanan_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan IGD Obat</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_igd_obat_section" name="label_igd_obat_section" value="{label_igd_obat_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_igd_obat_detail" name="label_igd_obat_detail" value="{label_igd_obat_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan IGD Alat Kesehatan</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_igd_alkes_section" name="label_igd_alkes_section" value="{label_igd_alkes_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_igd_alkes_detail" name="label_igd_alkes_detail" value="{label_igd_alkes_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan Labolatorium Umum</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_lab_umum_section" name="label_lab_umum_section" value="{label_lab_umum_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_lab_umum_detail" name="label_lab_umum_detail" value="{label_lab_umum_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan Labolatorium Pathologi Anatomi</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_lab_anatomi_section" name="label_lab_anatomi_section" value="{label_lab_anatomi_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_lab_anatomi_detail" name="label_lab_anatomi_detail" value="{label_lab_anatomi_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan Labolatorium PMI</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_lab_pmi_section" name="label_lab_pmi_section" value="{label_lab_pmi_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_lab_pmi_detail" name="label_lab_pmi_detail" value="{label_lab_pmi_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan Radiologi X-Ray</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_rad_xray_section" name="label_rad_xray_section" value="{label_rad_xray_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_rad_xray_detail" name="label_rad_xray_detail" value="{label_rad_xray_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan Radiologi USG</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_rad_usg_section" name="label_rad_usg_section" value="{label_rad_usg_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_rad_usg_detail" name="label_rad_usg_detail" value="{label_rad_usg_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan Radiologi CT-SCAN</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_rad_ct_section" name="label_rad_ct_section" value="{label_rad_ct_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_rad_ct_detail" name="label_rad_ct_detail" value="{label_rad_ct_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan Radiologi MRI</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_rad_mri_section" name="label_rad_mri_section" value="{label_rad_mri_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_rad_mri_detail" name="label_rad_mri_detail" value="{label_rad_mri_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan Radiologi BMD</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_rad_bmd_section" name="label_rad_bmd_section" value="{label_rad_bmd_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_rad_bmd_detail" name="label_rad_bmd_detail" value="{label_rad_bmd_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan Fisioterapi</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_fis_section" name="label_fis_section" value="{label_fis_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_fis_detail" name="label_fis_detail" value="{label_fis_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan Operasi Sewa Alat</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_ko_sewa_section" name="label_ko_sewa_section" value="{label_ko_sewa_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_ko_sewa_detail" name="label_ko_sewa_detail" value="{label_ko_sewa_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan Operasi Alat Kesehatan</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_ko_alkes_section" name="label_ko_alkes_section" value="{label_ko_alkes_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_ko_alkes_detail" name="label_ko_alkes_detail" value="{label_ko_alkes_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan Operasi  Obat</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_ko_obat_section" name="label_ko_obat_section" value="{label_ko_obat_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_ko_obat_detail" name="label_ko_obat_detail" value="{label_ko_obat_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan Operasi Narcose</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_ko_narcose_section" name="label_ko_narcose_section" value="{label_ko_narcose_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_ko_narcose_detail" name="label_ko_narcose_detail" value="{label_ko_narcose_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan Operasi Implan</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_ko_implan_section" name="label_ko_implan_section" value="{label_ko_implan_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_ko_implan_detail" name="label_ko_implan_detail" value="{label_ko_implan_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan Operasi Sewa Ruangan</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_ko_ruangan_section" name="label_ko_ruangan_section" value="{label_ko_ruangan_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_ko_ruangan_detail" name="label_ko_ruangan_detail" value="{label_ko_ruangan_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan Operasi Jasa Dokter Operator</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_ko_do_section" name="label_ko_do_section" value="{label_ko_do_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_ko_do_detail" name="label_ko_do_detail" value="{label_ko_do_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan Operasi Jasa Dokter Anestesi</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_ko_da_section" name="label_ko_da_section" value="{label_ko_da_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_ko_da_detail" name="label_ko_da_detail" value="{label_ko_da_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tindakan Operasi Jasa Assisten</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_ko_dao_section" name="label_ko_dao_section" value="{label_ko_dao_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_ko_dao_detail" name="label_ko_dao_detail" value="{label_ko_dao_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Administrasi Rawat Jalan</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_adm_rajal_section" name="label_adm_rajal_section" value="{label_adm_rajal_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_adm_rajal_detail" name="label_adm_rajal_detail" value="{label_adm_rajal_detail}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Administrasi Rawat Inap</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Section</label>
								<input type="text" class="form-control" id="label_adm_ranap_section" name="label_adm_ranap_section" value="{label_adm_ranap_section}">
							</div>
							<div class="col-md-6">
								<label>Detail</label>
								<input type="text" class="form-control" id="label_adm_ranap_detail" name="label_adm_ranap_detail" value="{label_adm_ranap_detail}">
							</div>
							
						</div>
					</div>
					
					<div class="form-group" style="margin-top: 25px;">
						<div class="col-md-12 ">
						<div class="col-md-12 ">
							<button class="btn btn-primary"  type="button" onclick="simpan_label_menu()" ><i class="fa fa-save"></i> Simpan</button>
						</div>
						</div>
						
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2625'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?> " id="tab_3">
			<?php echo form_open('#','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="text-primary">Tindakan Rawat Inap - Full Care</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_ranap_fc" name="hari_ranap_fc" value="{hari_ranap_fc}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan Rawat Inap - ECG</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_ranap_ecg" name="hari_ranap_ecg" value="{hari_ranap_ecg}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan Rawat Inap - Visite Dokter</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_ranap_visite" name="hari_ranap_visite" value="{hari_ranap_visite}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="text-primary">Tindakan Rawat Inap - Sewat Alat</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_ranap_sewa" name="hari_ranap_sewa" value="{hari_ranap_sewa}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan Rawat Inap - Ambulance</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_ranap_ambulance" name="hari_ranap_ambulance" value="{hari_ranap_ambulance}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan Rawat Inap - Lain Lain</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_ranap_lain" name="hari_ranap_lain" value="{hari_ranap_lain}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="text-primary">Tindakan Rawat Inap - Obat</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_ranap_obat" name="hari_ranap_obat" value="{hari_ranap_obat}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan Rawat Inap - Alat Kesehatan</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_ranap_alkes" name="hari_ranap_alkes" value="{hari_ranap_alkes}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan IGD - Pelayanan</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_igd_pelayanan" name="hari_igd_pelayanan" value="{hari_igd_pelayanan}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="text-primary">Tindakan IGD - Obat</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_igd_obat" name="hari_igd_obat" value="{hari_igd_obat}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan IGD - Alat Kesehatan</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_igd_alkes" name="hari_igd_alkes" value="{hari_igd_alkes}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan Laboratorium - Umum</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_lab_umum" name="hari_lab_umum" value="{hari_lab_umum}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="text-primary">Tindakan Laboratorium - patologi Anatomi</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_lab_anatomi" name="hari_lab_anatomi" value="{hari_lab_anatomi}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan Laboratorium - PMI</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_lab_pmi" name="hari_lab_pmi" value="{hari_lab_pmi}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan Radiologi - X-Ray</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_rab_xray" name="hari_rab_xray" value="{hari_rab_xray}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="text-primary">Tindakan Radiologi - USG</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_rab_usg" name="hari_rab_usg" value="{hari_rab_usg}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan Radiologi - CT-SCAN</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_rab_ct" name="hari_rab_ct" value="{hari_rab_ct}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan Radiologi - MRI</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_rab_mri" name="hari_rab_mri" value="{hari_rab_mri}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="text-primary">Tindakan Radiologi - BMD</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_rab_bmd" name="hari_rab_bmd" value="{hari_rab_bmd}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan Fisioterapi - Fisioterapi</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_fis" name="hari_fis" value="{hari_fis}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan Operasi - Sewa Alat</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_ko_sewa" name="hari_ko_sewa" value="{hari_ko_sewa}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="text-primary">Tindakan Operasi - Alat Kesehatan</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_ko_alkes" name="hari_ko_alkes" value="{hari_ko_alkes}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan Operasi - Obat</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_ko_obat" name="hari_ko_obat" value="{hari_ko_obat}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan Operasi - Narcose</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_ko_narcose" name="hari_ko_narcose" value="{hari_ko_narcose}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="text-primary">Tindakan Operasi - Implant</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_ko_implan" name="hari_ko_implan" value="{hari_ko_implan}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan Operasi - Sewa Ruangan</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_ko_ruangan" name="hari_ko_ruangan" value="{hari_ko_ruangan}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan Operasi - Jasa Dokter Operator</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_ko_do" name="hari_ko_do" value="{hari_ko_do}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="text-primary">Tindakan Operasi - Jasa Dokter Anestesi</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_ko_da" name="hari_ko_da" value="{hari_ko_da}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Tindakan Operasi - Jasa Assisten</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_ko_dao" name="hari_ko_dao" value="{hari_ko_dao}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Administasi Rawat Inap</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_adm_ranap" name="hari_adm_ranap" value="{hari_adm_ranap}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="text-primary">Administasi Rawat Jalan</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="hari_adm_rajal" name="hari_adm_rajal" value="{hari_adm_rajal}" placeholder="">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Update Auto Verifikasi Setiap</label>
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_auto_verif" name="lama_auto_verif" value="{lama_auto_verif}" placeholder="">
								<span class="input-group-addon">Jam</span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="text-primary">Next Verifikasi</label>
							<div class="input-group">
								<input class="form-control" disabled type="text" id="tanggal_auto_verif" name="tanggal_auto_verif" value="<?=HumanDateLong($tanggal_auto_verif)?>" placeholder="">
								<span class="input-group-addon">Jam</span>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group" style="margin-top: 25px;">
					<div class="col-md-12 ">
						<div class="col-md-12 ">
							<button class="btn btn-primary"  type="button" onclick="simpan_auto_verifikasi()" ><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
					
				</div>
			</div>
			
			<?php echo form_close() ?>
			</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2626'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='4'?'active in':'')?> " id="tab_4">
			<?php echo form_open_multipart('setting_trx_ranap/save_label_lembar','class="js-form1 validation form-horizontal" id="form-work"') ?>
				<div class="row">
				
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table id="tabel_batal_kwitansi" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 5%;">Step</th>								
																		
											<th style="width: 10%;">Jenis Kwitansi</th>								
											<th style="width: 10%;">Deskripsi</th>								
											<th style="width: 15%;">User </th>
											<th style="width: 10%;">Jika Setuju</th>								
											<th style="width: 10%;">Jika Tolak</th>	
											<th style="width: 10%;">Actions</th>
										</tr>
										<tr>								
											<td>
												<select name="step" style="width: 100%" id="step" class="js-select2 form-control input-sm">
													<?for($i=1;$i<10;$i++){?>
														<option value="<?=$i?>"><?=$i?></option>
													<?}?>
												</select>										
											</td>
											<td>
												<select name="jenis" style="width: 100%" id="jenis" class="js-select2 form-control input-sm">										
													<option value="#">- Pilih Jenis -</option>
													<?foreach(get_all('mjenis_kwitansi') as $r){?>
													<option value="<?=$r->id?>"><?=$r->jenis_kwitansi?></option>
													
													<?}?>
												</select>										
											</td>
											<td>
												<input type="text" style="width: 100%"  class="form-control" id="deskrpisi" placeholder="" name="deskrpisi" value="">
												<input type="hidden" class="form-control" id="id_edit_batal_kwitansi" placeholder="0" name="id_edit_batal_kwitansi" value="">
											</td>	
											<td>
												<select name="iduser" tabindex="16"  style="width: 100%" id="iduser" data-placeholder="User" class="js-select2 form-control input-sm"></select>										
											</td>
											<td>
												<select name="proses_setuju_batal_kwitansi" style="width: 100%" id="proses_setuju_batal_kwitansi" class="js-select2 form-control input-sm">										
													<option value="1">Langsung</option>
													<option value="2">Menunggu User</option>										
												</select>										
											</td>
											<td>
												<select name="proses_tolak_batal_kwitansi" style="width: 100%" id="proses_tolak_batal_kwitansi" class="js-select2 form-control input-sm">										
													<option value="1">Langsung</option>
													<option value="2">Menunggu User</option>										
												</select>										
											</td>
											<td>									
												<button type="button" class="btn btn-sm btn-primary"  id="simpan_batal_kwitansi" title="Masukan Item"><i class="fa fa-save"></i></button>
												<button type="button" class="btn btn-sm btn-warning" id="clear_edit_batal_kwitansi" title="Batalkan"><i class="fa fa-refresh"></i></button>
											</td>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
									
							</table>
							</div>					
						</div>
					</div>
				</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2627'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='5'?'active in':'')?> " id="tab_5">
			<?php echo form_open('#','class="js-form1 validation form-horizontal" id="form-work"') ?>
				<div class="row">
				
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table id="tabel_card" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
																		
											<th style="width: 25%;">Kelompok Pasien</th>								
											<th style="width: 15%;">Tipe</th>								
											<th style="width: 30%;">Rekanan / Asuransi </th>
											<th style="width: 15%;">Status</th>								
											<th style="width: 15%;">Action</th>								
											
										</tr>
										<tr>								
											
											<td>
												<input type="hidden" class="form-control" id="id_edit_card" placeholder="0" name="id_edit_batal_kwitansi" value="">
												<select name="kelompok_id" style="width: 100%" id="kelompok_id" class="js-select2 form-control input-sm">										
													<option value="#">- Kelompok Pasien -</option>
													<?foreach(get_all('mpasien_kelompok') as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>										
											</td>
											<td>
												<select name="tipe_rekanan" style="width: 100%" id="tipe_rekanan" class="js-select2 form-control input-sm">										
													<option value="0" selected>-ALL-</option>
													<option value="1">Asuransi</option>
													<option value="2">Rekanan</option>										
												</select>										
											</td>
											<td>
												<select name="idrekanan" style="width: 100%" id="idrekanan" data-placeholder="Asuransi / Rekanan" class="js-select2 form-control input-sm"></select>										
											</td>
											<td>
												<select name="status_card" style="width: 100%" id="status_card" class="js-select2 form-control input-sm">										
													<option value="1">LUNAS</option>
													<option value="2">BELUM LUNAS</option>										
												</select>										
											</td>
											<td>									
												<button type="button" class="btn btn-sm btn-primary"  id="simpan_card" title="Masukan Item"><i class="fa fa-save"></i></button>
												<button type="button" class="btn btn-sm btn-warning" id="clear_edit_card" title="Batalkan"><i class="fa fa-refresh"></i></button>
											</td>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
									
							</table>
							</div>					
						</div>
					</div>
				</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2628'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='6'?'active in':'')?> " id="tab_6">
			<?php echo form_open_multipart('setting_trx_ranap/save_kwitansi','class="js-form1 validation form-horizontal" id="form-work"') ?>
				<div class="col-md-12">
			<div class="row">
				
					<div class="form-group" style="margin-bottom:5px">
						
						<div class="col-md-12">
							<div class="col-md-6">
								<img class="img-avatar"  id="output_img" src="{upload_path}logo_setting/{logo_kwitansi}" />
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Logo Header (100x100)</label>
								<div class="box">
									<input type="file" id="file-3" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;" name="logo_kwitansi" value="{logo_kwitansi}" />
									<label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
								</div>
							</div>
						</div>
					</div>
				
					<div class="form-group" style="margin-bottom:5px">
						
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Alamat</label>
								<input type="text" class="form-control" name="alamat_kwitansi" value="{alamat_kwitansi}">
							</div>
							<div class="col-md-6">
								<label>Telepon</label>
								<input type="text" class="form-control" name="telepone_kwitansi" value="{telepone_kwitansi}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						
						<div class="col-md-12">
							<div class="col-md-12">
								<label>Web / Email</label>
								<input type="text" class="form-control" name="email_kwitansi" value="{email_kwitansi}">
							</div>
							
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Judul</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="judul_kwitansi_ina" value="{judul_kwitansi_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="judul_kwitansi_eng" value="{judul_kwitansi_eng}">
							</div>
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">No Registrasi</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="no_register_kwitansi_ina" value="{no_register_kwitansi_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="no_register_kwitansi_eng" value="{no_register_kwitansi_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">No Kwitansi</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="no_kwitansi_ina" value="{no_kwitansi_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="no_kwitansi_eng" value="{no_kwitansi_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Tanggal Deposit</label>&nbsp;&nbsp;&nbsp;&nbsp;
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="tanggal_kwitansi_ina" value="{tanggal_kwitansi_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="tanggal_kwitansi_eng" value="{tanggal_kwitansi_eng}">
							</div>
							
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Sudah Terima Dari</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="sudah_terima_kwitansi_ina" value="{sudah_terima_kwitansi_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="sudah_terima_kwitansi_eng" value="{sudah_terima_kwitansi_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Banyaknya Uang</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="banyaknya_kwitansi_ina" value="{banyaknya_kwitansi_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="banyaknya_kwitansi_eng" value="{banyaknya_kwitansi_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Untuk Pembayaran</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="untuk_kwitansi_ina" value="{untuk_kwitansi_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="untuk_kwitansi_eng" value="{untuk_kwitansi_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Jumlah</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="jumlah_kwitansi_ina" value="{jumlah_kwitansi_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="jumlah_kwitansi_eng" value="{jumlah_kwitansi_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Yang Menerima</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="yg_menerima_kwitansi_ina" value="{yg_menerima_kwitansi_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="yg_menerima_kwitansi_eng" value="{yg_menerima_kwitansi_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Footer</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="footer_kwitansi_ina" value="{footer_kwitansi_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="footer_kwitansi_eng" value="{footer_kwitansi_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Generated</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="generated_kwitansi_ina" value="{generated_kwitansi_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="generated_kwitansi_eng" value="{generated_kwitansi_eng}">
							</div>
							
						</div>
					</div>
					
					<div class="form-group" style="margin-top:25px">
						<div class="col-md-12">
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="submit" id="btn_save_label_kwitansi" name="btn_save_label"><i class="fa fa-save"></i> Simpan</button>
								</div>
						</div>
					</div>
					
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2629'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='7'?'active in':'')?> " id="tab_7">
			<?php echo form_open_multipart('setting_trx_ranap/save_label_card','class="js-form1 validation form-horizontal" id="form-work"') ?>
				<div class="col-md-12">
			<div class="row">
				
					<div class="form-group" style="margin-bottom:5px">
						
						<div class="col-md-12">
							<div class="col-md-6">
								<img class="img-avatar"  id="output_img" src="{upload_path}logo_setting/{logo_card}" />
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Logo Header (100x100)</label>
								<div class="box">
									<input type="file" id="file-3" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;" name="logo_card" value="{logo_card}" />
									<label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
								</div>
							</div>
						</div>
					</div>
				
					<div class="form-group" style="margin-bottom:5px">
						
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Alamat</label>
								<input type="text" class="form-control" name="alamat_card" value="{alamat_card}">
							</div>
							<div class="col-md-6">
								<label>Telepon</label>
								<input type="text" class="form-control" name="telepone_card" value="{telepone_card}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						
						<div class="col-md-12">
							<div class="col-md-12">
								<label>Web / Email</label>
								<input type="text" class="form-control" name="email_card" value="{email_card}">
							</div>
							
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Judul</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="judul_card_ina" value="{judul_card_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="judul_card_eng" value="{judul_card_eng}">
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Paragraph 1</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="paragraf1_card_ina" value="{paragraf1_card_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="paragraf1_card_eng" value="{paragraf1_card_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">No Register</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="no_register_card_ina" value="{no_register_card_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="no_register_card_eng" value="{no_register_card_eng}">
							</div>
							
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Nama</label>&nbsp;&nbsp;&nbsp;&nbsp;
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="nama_card_ina" value="{nama_card_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="nama_card_eng" value="{nama_card_eng}">
							</div>
							
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Deskripsi</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="deskripsi_card_ina" value="{deskripsi_card_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="deskripsi_card_eng" value="{deskripsi_card_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Paragraph 2</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="paragraf2_card_ina" value="{paragraf2_card_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="paragraf2_card_eng" value="{paragraf2_card_eng}">
							</div>
							
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Yang Menerima</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="yg_menerima_card_ina" value="{yg_menerima_card_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="yg_menerima_card_eng" value="{yg_menerima_card_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Footer</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="footer_card_ina" value="{footer_card_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="footer_card_eng" value="{footer_card_eng}">
							</div>
							
						</div>
					</div>
					<div class="form-group" style="margin-bottom:5px">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="text-primary">Generated</label>&nbsp;&nbsp;&nbsp;&nbsp;
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<label>Indonesia</label>
								<input type="text" class="form-control" name="generated_card_ina" value="{generated_card_ina}">
							</div>
							<div class="col-md-6">
								<label>English</label>
								<input type="text" class="form-control" name="generated_card_eng" value="{generated_card_eng}">
							</div>
							
						</div>
					</div>
					
					<div class="form-group" style="margin-top:25px">
						<div class="col-md-12">
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="submit" id="btn_save_label_card" name="btn_save_label"><i class="fa fa-save"></i> Simpan</button>
								</div>
						</div>
					</div>
					
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
function loadFile_img(event){
	// alert('sini');
	var output_img = document.getElementById('output_img');
	output_img.src = URL.createObjectURL(event.target.files[0]);
}
$(document).ready(function(){	
	$(".number").number(true,0,'.',',');
	var tab=$("#tab").val();
	if (tab=='1'){
		// load_kunjungan();
	}
	// if (tab=='2'){
		// load_jenis_trx_ranap();
	// }
	if (tab=='4'){
		load_batal_kwitansi();
	}
	if (tab=='5'){
		load_card();
	}
		disable_fungsi();
	
	// if (tab=='4'){
		
	// }
})	
$(document).on("change","#step,#jenis",function(){
	list_user();
});
function list_user(){
	// alert($("#idjenis").val());
	$('#iduser')
		.find('option')
		.remove()
		.end()
		.append('<option value="">- Pilih User -</option>')
		.val('').trigger("liszt:updated");
	if ($("#jenis").val()!='#' && $("#step").val()!='#'){
		// console.log($("#id").val()+'/'+$("#step").val()+'/'+$("#jenis").val()+'/'+$("#idjenis").val());
		$.ajax({
			url: '{site_url}setting_trx_ranap/list_user/'+$("#step").val()+'/'+$("#jenis").val(),
			dataType: "json",
			success: function(data) {

			$.each(data.detail, function (i,unit) {
			$('#iduser')
			.append('<option value="' + unit.id + '">' + unit.name + '</option>');
			});
			}
		});
	}else{
		$('#iduser')
		.find('option')
		.remove()
		.end()
		.append('<option value="">- Pilih User -</option>')
	}

}
	
	function load_batal_kwitansi(){
		var idlogic=$("#id").val();
		
		$('#tabel_batal_kwitansi').DataTable().destroy();
		var table = $('#tabel_batal_kwitansi').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}setting_trx_ranap/load_batal_kwitansi/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [7], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	$(document).on("click","#clear_edit_batal_kwitansi",function(){
		clear_all_batal_kwitansi();
		$("#id_edit_batal_kwitansi").val('');
		clear_input_detail_batal_kwitansi();
		$("#iduser").val(null).trigger('change');
	});
	function clear_all_batal_kwitansi(){
		// list_unit();
		// list_user_bendahara();
		$("#id_edit_batal_kwitansi").val('');
		$("#simpan_unit").attr('disabled', false);
		$(".btn_batal_kwitansi").attr('disabled', false);
	}
	function clear_input_detail_batal_kwitansi(){
		$("#step").val('1').trigger('change');
		$("#jenis").val('#').trigger('change');
		// $("#idjenis").val('#').trigger('change');
		$("#deskrpisi").val('');
		list_user();
	}
	$(document).on("click","#simpan_batal_kwitansi",function(){
		if (validate_add_detail_batal_kwitansi()==false)return false;
		var id_edit_batal_kwitansi=$("#id_edit_batal_kwitansi").val();
		var iduser=$("#iduser").val();
		var step=$("#step").val();
		var proses_setuju=$("#proses_setuju_batal_kwitansi").val();
		var proses_tolak=$("#proses_tolak_batal_kwitansi").val();
		var jenis=$("#jenis").val();
		var deskrpisi=$("#deskrpisi").val();
		$.ajax({
			url: '{site_url}setting_trx_ranap/simpan_batal_kwitansi',
			type: 'POST',
			data: {
				iduser:iduser,step:step,
				proses_setuju: proses_setuju,proses_tolak:proses_tolak,jenis:jenis,
				deskrpisi:deskrpisi,id_edit_batal_kwitansi:id_edit_batal_kwitansi
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Logic Berhasil disimpan'});
					$('#tabel_batal_kwitansi').DataTable().ajax.reload( null, false );
					clear_input_detail_batal_kwitansi();
					$("#id_edit_batal_kwitansi").val('');
				}else{
					sweetAlert("Gagal...", "Penyimpanan!", "error");
					
				}
			}
		});
		
		
	});
	
	$(document).on("click",".hapus_det_batal_kwitansi",function(){
		var table = $('#tabel_batal_kwitansi').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,7).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menghapus Logic Detail ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}setting_trx_ranap/hapus_det_kwitansi',
				type: 'POST',
				data: {
					id: id	
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Detail Berhasil dihapus'});
					$('#tabel_batal_kwitansi').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	
	$(document).on("click",".edit_batal_kwitansi",function(){
		// alert('SINI');
		table = $('#tabel_batal_kwitansi').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,7).data()
		$.ajax({
			url: '{site_url}setting_trx_ranap/get_edit_batal_kwitansi',
			type: 'POST',
			dataType: "json",
			data: {
				id: id
			},
			success: function(data) {
				// var item=data.detail;
				$("#id_edit_batal_kwitansi").val(data.id);
				$("#step").val(data.step).trigger('change');
				$("#proses_setuju_batal_kwitansi").val(data.proses_setuju).trigger('change');
				$("#proses_tolak_batal_kwitansi").val(data.proses_tolak).trigger('change');
				$("#jenis").val(data.jenis).trigger('change');
				$("#deskrpisi").val(data.deskrpisi).trigger('change');
				$("#nominal").val(data.nominal).trigger('change');
				var newOption = new Option(data.user_nama, data.iduser, true, true);
					
				$("#iduser").append(newOption);
				$("#iduser").val(data.iduser).trigger('change');
				$(".btn_batal_kwitansi").attr('disabled', true);
				console.log(data.id);
			}
		});
	});
	
	function validate_add_detail_batal_kwitansi()
	{
		if ($("#iduser").val()=='' || $("#iduser").val()==null || $("#iduser").val()=="#"){
			sweetAlert("Maaf...", "User Harus diisi", "error");
			return false;
		}
		if ($("#jenis").val()=='' || $("#jenis").val()==null || $("#jenis").val()=="#"){
			sweetAlert("Maaf...", "Tipe Refund Harus diisi", "error");
			return false;
		}
		
		if ($("#nominal").val()=='0'){
			if (($("#deskrpisi").val()!='>=' && $("#deskrpisi").val()!='>')){
			sweetAlert("Maaf...", "Nominal Harus diisi", "error");
			return false;
			}
		}
		
		return true;
	}
	
function simpan_general(){
	// alert('sini');
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_trx_ranap/simpan_general/',
		dataType: "json",
		type: "POST",
		data: {
			st_edit_tgl_bayar : $("#st_edit_tgl_bayar").val(),
			st_simpan : $("#st_simpan").val(),
			st_simpan_proses : $("#st_simpan_proses").val(),
			st_batal : $("#st_batal").val(),
			st_selesai : $("#st_selesai").val(),
			st_selesai_verifikasi : $("#st_selesai_verifikasi").val(),
			email_generate_rincian : $("#email_generate_rincian").val(),
			email_generate_kwitansi : $("#email_generate_kwitansi").val(),
			waktu_batal : $("#waktu_batal").val(),
			st_qrcode : $("#st_qrcode").val(),
			st_verifikasi_disabled : $("#st_verifikasi_disabled").val(),
			st_edit_tgl_setoran : $("#st_edit_tgl_setoran").val(),
			st_kwitansi_all : $("#st_kwitansi_all").val(),
			st_kwitansi_kontraktor : $("#st_kwitansi_kontraktor").val(),
			st_kwitansi_exces : $("#st_kwitansi_exces").val(),
			st_simpan_kwitansi : $("#st_simpan_kwitansi").val(),
			st_simpan_generate_kwitansi : $("#st_simpan_generate_kwitansi").val(),
			st_edit_tgl_kwitansi : $("#st_edit_tgl_kwitansi").val(),
			st_default_nama : $("#st_default_nama").val(),
			label_kode_kwitansi : $("#label_kode_kwitansi").val(),
			st_qrcode_kwitansi : $("#st_qrcode_kwitansi").val(),
			waktu_batal_kwitansi : $("#waktu_batal_kwitansi").val(),
			waktu_maksimum_proses : $("#waktu_maksimum_proses").val(),
			st_edit_tgl_card : $("#st_edit_tgl_card").val(),
			email_card_pass : $("#email_card_pass").val(),
			label_deskripsi_kwitansi : $("#label_deskripsi_kwitansi").val(),
		},
		success: function(data) {
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan General'});
		}
	});
}
function simpan_label_menu(){
	// alert('sini');
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_trx_ranap/simpan_label_menu/',
		dataType: "json",
		type: "POST",
		data: {
			label_ranap_ruangan_section : $("#label_ranap_ruangan_section").val(),
			label_ranap_ruangan_detail : $("#label_ranap_ruangan_detail").val(),
			label_ranap_fc_section : $("#label_ranap_fc_section").val(),
			label_ranap_fc_detail : $("#label_ranap_fc_detail").val(),
			label_ranap_ecg_section : $("#label_ranap_ecg_section").val(),
			label_ranap_ecg_detail : $("#label_ranap_ecg_detail").val(),
			label_ranap_visite_section : $("#label_ranap_visite_section").val(),
			label_ranap_visite_detail : $("#label_ranap_visite_detail").val(),
			label_ranap_alat_section : $("#label_ranap_alat_section").val(),
			label_ranap_alat_detail : $("#label_ranap_alat_detail").val(),
			label_ranap_ambulance_section : $("#label_ranap_ambulance_section").val(),
			label_ranap_ambulance_detail : $("#label_ranap_ambulance_detail").val(),
			label_ranap_obat_section : $("#label_ranap_obat_section").val(),
			label_ranap_obat_detail : $("#label_ranap_obat_detail").val(),
			label_ranap_alkes_section : $("#label_ranap_alkes_section").val(),
			label_ranap_alkes_detail : $("#label_ranap_alkes_detail").val(),
			label_ranap_lain_section : $("#label_ranap_lain_section").val(),
			label_ranap_lain_detail : $("#label_ranap_lain_detail").val(),
			label_igd_layanan_section : $("#label_igd_layanan_section").val(),
			label_igd_layanan_detail : $("#label_igd_layanan_detail").val(),
			label_igd_obat_section : $("#label_igd_obat_section").val(),
			label_igd_obat_detail : $("#label_igd_obat_detail").val(),
			label_igd_alkes_section : $("#label_igd_alkes_section").val(),
			label_igd_alkes_detail : $("#label_igd_alkes_detail").val(),
			label_lab_umum_section : $("#label_lab_umum_section").val(),
			label_lab_umum_detail : $("#label_lab_umum_detail").val(),
			label_lab_anatomi_section : $("#label_lab_anatomi_section").val(),
			label_lab_anatomi_detail : $("#label_lab_anatomi_detail").val(),
			label_lab_pmi_section : $("#label_lab_pmi_section").val(),
			label_lab_pmi_detail : $("#label_lab_pmi_detail").val(),
			label_rad_xray_section : $("#label_rad_xray_section").val(),
			label_rad_xray_detail : $("#label_rad_xray_detail").val(),
			label_rad_usg_section : $("#label_rad_usg_section").val(),
			label_rad_usg_detail : $("#label_rad_usg_detail").val(),
			label_rad_ct_section : $("#label_rad_ct_section").val(),
			label_rad_ct_detail : $("#label_rad_ct_detail").val(),
			label_rad_mri_section : $("#label_rad_mri_section").val(),
			label_rad_mri_detail : $("#label_rad_mri_detail").val(),
			label_rad_bmd_section : $("#label_rad_bmd_section").val(),
			label_rad_bmd_detail : $("#label_rad_bmd_detail").val(),
			label_fis_section : $("#label_fis_section").val(),
			label_fis_detail : $("#label_fis_detail").val(),
			label_ko_sewa_section : $("#label_ko_sewa_section").val(),
			label_ko_sewa_detail : $("#label_ko_sewa_detail").val(),
			label_ko_alkes_section : $("#label_ko_alkes_section").val(),
			label_ko_alkes_detail : $("#label_ko_alkes_detail").val(),
			label_ko_obat_section : $("#label_ko_obat_section").val(),
			label_ko_obat_detail : $("#label_ko_obat_detail").val(),
			label_ko_narcose_section : $("#label_ko_narcose_section").val(),
			label_ko_narcose_detail : $("#label_ko_narcose_detail").val(),
			label_ko_implan_section : $("#label_ko_implan_section").val(),
			label_ko_implan_detail : $("#label_ko_implan_detail").val(),
			label_ko_ruangan_section : $("#label_ko_ruangan_section").val(),
			label_ko_ruangan_detail : $("#label_ko_ruangan_detail").val(),
			label_ko_do_section : $("#label_ko_do_section").val(),
			label_ko_do_detail : $("#label_ko_do_detail").val(),
			label_ko_da_section : $("#label_ko_da_section").val(),
			label_ko_da_detail : $("#label_ko_da_detail").val(),
			label_ko_dao_section : $("#label_ko_dao_section").val(),
			label_ko_dao_detail : $("#label_ko_dao_detail").val(),
			label_adm_ranap_section : $("#label_adm_ranap_section").val(),
			label_adm_ranap_detail : $("#label_adm_ranap_detail").val(),
			label_adm_rajal_section : $("#label_adm_rajal_section").val(),
			label_adm_rajal_detail : $("#label_adm_rajal_detail").val(),
		},
		success: function(data) {
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Label Menu'});
		}
	});
}
function simpan_auto_verifikasi(){
	// alert('sini');
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_trx_ranap/simpan_auto_verifikasi/',
		dataType: "json",
		type: "POST",
		data: {
			hari_ranap_fc : $("#hari_ranap_fc").val(),
			hari_ranap_ecg : $("#hari_ranap_ecg").val(),
			hari_ranap_visite : $("#hari_ranap_visite").val(),
			hari_ranap_sewa : $("#hari_ranap_sewa").val(),
			hari_ranap_ambulance : $("#hari_ranap_ambulance").val(),
			hari_ranap_lain : $("#hari_ranap_lain").val(),
			hari_ranap_obat : $("#hari_ranap_obat").val(),
			hari_ranap_alkes : $("#hari_ranap_alkes").val(),
			hari_igd_pelayanan : $("#hari_igd_pelayanan").val(),
			hari_igd_obat : $("#hari_igd_obat").val(),
			hari_igd_alkes : $("#hari_igd_alkes").val(),
			hari_lab_umum : $("#hari_lab_umum").val(),
			hari_lab_anatomi : $("#hari_lab_anatomi").val(),
			hari_lab_pmi : $("#hari_lab_pmi").val(),
			hari_rab_xray : $("#hari_rab_xray").val(),
			hari_rab_usg : $("#hari_rab_usg").val(),
			hari_rab_ct : $("#hari_rab_ct").val(),
			hari_rab_mri : $("#hari_rab_mri").val(),
			hari_rab_bmd : $("#hari_rab_bmd").val(),
			hari_fis : $("#hari_fis").val(),
			hari_ko_sewa : $("#hari_ko_sewa").val(),
			hari_ko_alkes : $("#hari_ko_alkes").val(),
			hari_ko_obat : $("#hari_ko_obat").val(),
			hari_ko_narcose : $("#hari_ko_narcose").val(),
			hari_ko_implan : $("#hari_ko_implan").val(),
			hari_ko_ruangan : $("#hari_ko_ruangan").val(),
			hari_ko_do : $("#hari_ko_do").val(),
			hari_ko_da : $("#hari_ko_da").val(),
			hari_ko_dao : $("#hari_ko_dao").val(),
			hari_adm_ranap : $("#hari_adm_ranap").val(),
			hari_adm_rajal : $("#hari_adm_rajal").val(),
			lama_auto_verif : $("#lama_auto_verif").val(),

		},
		success: function(data) {
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Auto Verifikasi'});
		}
	});
}
$(document).on("change","#kelompok_id,#tipe_rekanan",function(){
	disable_fungsi();
	list_rekanan();
});
function disable_fungsi(){
	$("#idrekanan").attr('disabled', true);
		$("#tipe_rekanan").attr('disabled', true);
	if ($("#kelompok_id").val()!='1'){
		$("#idrekanan").attr('disabled', true);
		$("#tipe_rekanan").attr('disabled', true);
	}else{
		$("#tipe_rekanan").attr('disabled', false);
		$("#idrekanan").attr('disabled', false);
		// if ($("#tipe_rekanan").val()!='0'){
			// $("#tipe_rekanan").attr('disabled', false);
			
		// }
	}
}
function list_rekanan(){
	// alert($("#idtipe_rekanan").val());
	$('#idrekanan')
		.find('option')
		.remove()
		.end()
		.append('<option value="0">- Tidak Ditentukan -</option>')
		.val('').trigger("liszt:updated");
	if ($("#tipe_rekanan").val()!='0' && $("#kelompok_id").val()!='#'){
		// console.log($("#id").val()+'/'+$("#kelompok_id").val()+'/'+$("#tipe_rekanan").val()+'/'+$("#idtipe_rekanan").val());
		$.ajax({
			url: '{site_url}setting_trx_ranap/list_rekanan/'+$("#kelompok_id").val()+'/'+$("#tipe_rekanan").val(),
			dataType: "json",
			success: function(data) {

			$.each(data.detail, function (i,unit) {
			$('#idrekanan')
			.append('<option value="' + unit.id + '">' + unit.nama + '</option>');
			});
			}
		});
	}else{
		$('#idrekanan')
		.find('option')
		.remove()
		.end()
		.append('<option value="0">- Tidak Ditentukan -</option>')
	}

}
	
	function load_card(){
		var idlogic=$("#id").val();
		
		$('#tabel_card').DataTable().destroy();
		var table = $('#tabel_card').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}setting_trx_ranap/load_card/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [5], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	$(document).on("click","#clear_edit_card",function(){
		clear_all_card();
		$("#id_edit_card").val('');
		clear_input_detail_card();
		$("#idrekanan").val(null).trigger('change');
	});
	function clear_all_card(){
		// list_unit();
		// list_rekanan_bendahara();
		$("#id_edit_card").val('');
		$(".btn_card").attr('disabled', false);
	}
	function clear_input_detail_card(){
		$("#kelompok_id").val('1').trigger('change');
		$("#tipe_rekanan").val('#').trigger('change');
		// $("#idtipe_rekanan").val('#').trigger('change');
		$("#status_card").val('1').trigger('change');
		list_rekanan();
	}
	$(document).on("click","#simpan_card",function(){
		if (validate_add_detail_card()==false)return false;
		var id_edit_card=$("#id_edit_card").val();
		var kelompok_id=$("#kelompok_id").val();
		var tipe_rekanan=$("#tipe_rekanan").val();
		var idrekanan=$("#idrekanan").val();
		var status_card=$("#status_card").val();
		// alert(kelompok_id);
		$.ajax({
			url: '{site_url}setting_trx_ranap/simpan_card',
			type: 'POST',
			data: {
				idrekanan:idrekanan,kelompok_id:kelompok_id,
				tipe_rekanan: tipe_rekanan,idrekanan:idrekanan,
				status_card:status_card,id_edit_card:id_edit_card
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Logic Berhasil disimpan'});
					$('#tabel_card').DataTable().ajax.reload( null, false );
					clear_input_detail_card();
					$("#id_edit_card").val('');
				}else{
					sweetAlert("Gagal...", "Penyimpanan!", "error");
					
				}
			}
		});
		
		
	});
	
	$(document).on("click",".hapus_det_card",function(){
		var table = $('#tabel_card').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,5).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menghapus Logic Detail ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}setting_trx_ranap/hapus_det_card',
				type: 'POST',
				data: {
					id: id	
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Detail Berhasil dihapus'});
					$('#tabel_card').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	
	$(document).on("click",".edit_card",function(){
		// alert('SINI');
		table = $('#tabel_card').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,7).data()
		$.ajax({
			url: '{site_url}setting_trx_ranap/get_edit_card',
			type: 'POST',
			dataType: "json",
			data: {
				id: id
			},
			success: function(data) {
				// var item=data.detail;
				$("#id_edit_card").val(data.id);
				$("#kelompok_id").val(data.kelompok_id).trigger('change');
				$("#tipe_rekanan_card").val(data.tipe_rekanan).trigger('change');
				$("#proses_tolak_card").val(data.proses_tolak).trigger('change');
				$("#tipe_rekanan").val(data.tipe_rekanan).trigger('change');
				$("#status_card").val(data.status_card).trigger('change');
				$("#nominal").val(data.nominal).trigger('change');
				var newOption = new Option(data.user_nama, data.idrekanan, true, true);
					
				$("#idrekanan").append(newOption);
				$("#idrekanan").val(data.idrekanan).trigger('change');
				$(".btn_card").attr('disabled', true);
				console.log(data.id);
			}
		});
	});
	
	function validate_add_detail_card()
	{
		if ($("#kelompok_id").val()=='#'){
			sweetAlert("Maaf...", "Kelompok Pasien Harus diisi", "error");
			return false;
		}
		
		return true;
	}
</script>
