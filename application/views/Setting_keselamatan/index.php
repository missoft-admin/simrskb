<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<style>
	.bg-info {
    background-color: #cbeaff;
	}
</style>
<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('2231'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_1" onclick="load_general()"><i class="fa fa-bell"></i> General</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2233'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_2"  onclick="load_tab2()"><i class="fa fa-check-square-o"></i> Logic</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2234'))){ ?>
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab_3"  onclick="load_tab3()"><i class="fa fa-check-square-o"></i> Content</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('2231'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_1">
			
			<?php echo form_open('setting_keselamatan/save_assesmen','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_edit_catatan">Izin Edit Catatan</label>
						<div class="col-md-2">
							<select id="st_edit_catatan" name="st_edit_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_edit_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_edit_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_edit">Waktu Edit</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_edit" name="lama_edit" value="{lama_edit}" placeholder="Waktu Edit">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_edit">Edit Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_edit" name="orang_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_edit=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_edit=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_hapus_catatan">Izin Hapus Catatan</label>
						<div class="col-md-2">
							<select id="st_hapus_catatan" name="st_hapus_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_hapus_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_hapus_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_hapus">Waktu Hapus</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_hapus" name="lama_hapus" value="{lama_hapus}" placeholder="Waktu Hapus">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_hapus">Hapus Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_hapus" name="orang_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_hapus=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_hapus=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_duplikasi_catatan">Izin Duplikasi Catatan</label>
						<div class="col-md-2">
							<select id="st_duplikasi_catatan" name="st_duplikasi_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_duplikasi_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_duplikasi_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_duplikasi">Waktu Duplikasi</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_duplikasi" name="lama_duplikasi" value="{lama_duplikasi}" placeholder="Waktu Duplikasi">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_duplikasi">Duplikasi Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_duplikasi" name="orang_duplikasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_duplikasi=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_duplikasi=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_header_ina">Judul Header <span style="color:red;"> INA</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_header_ina" name="judul_header_ina" value="{judul_header_ina}" placeholder="Header">
						</div>
					</div>
				</div>
				<div class="col-md-12" hidden>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_header_eng">Judul Header <span style="color:red;"> ENG</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_header_eng" name="judul_header_eng" value="{judul_header_eng}" placeholder="Header">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_footer_ina">Judul Footer <span style="color:red;"> INA </span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_footer_ina" name="judul_footer_ina" value="{judul_footer_ina}" placeholder="Footer">
						</div>
					</div>
				</div>
				<div class="col-md-12" hidden>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_footer_eng">Judul Footer <span style="color:red;"> ENG </span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_footer_eng" name="judul_footer_eng" value="{judul_footer_eng}" placeholder="Footer">
						</div>
					</div>
				</div>
				
				<?php if (UserAccesForm($user_acces_form,array('2232'))){ ?>
				<div class="col-md-12 pull-t-50" style="margin-top:10px">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="statusstok"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_general" name="btn_save_general"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
				<?}?>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
		<?php if (UserAccesForm($user_acces_form,array('2233'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_2">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('HAK AKSES FORMULIR')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_hak_akses">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="10%">Profesi</th>
											<th width="15%">Spesialisasi</th>
											<th width="15%">PPA</th>
											<th width="15%">Melihat</th>
											<th width="10%">Input</th>
											<th width="10%">Edit</th>
											<th width="10%">Hapus</th>
											<th width="10%">Cetak</th>
											<th width="10%">Action</th>										   
										</tr>
										<tr>
											<th>#</th>
											<th>
												<select id="profesi_id" name="profesi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													
													<option value="#" <?=($profesi_id=='#'?'selected':'')?>>-Pilih Profesi-</option>
													<?foreach(list_variable_ref(21) as $r){?>
														<option value="<?=$r->id?>" <?=($profesi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
													<?}?>
													
												</select>
											</th>
											<th>
												<select id="spesialisasi_id" name="spesialisasi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" <?=($spesialisasi_id=='#'?'selected':'')?>>-SEMUA-</option>
													<?foreach(list_variable_ref(22) as $r){?>
														<option value="<?=$r->id?>" <?=($spesialisasi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
													<?}?>
													
													
												</select>
											</th>
											<th>
												<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" <?=($mppa_id=='#'?'selected':'')?>>-SEMUA-</option>
													
												</select>
											</th>
											
											<th>
												<select id="st_lihat" name="st_lihat" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1" <?=($st_lihat=='1'?'selected':'')?>>IZINKAN</option>
													<option value="0" <?=($st_lihat=='0'?'selected':'')?>>TIDAK</option>
													
												</select>
											</th>
											<th>
												<select id="st_input" name="st_input" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1" <?=($st_input=='1'?'selected':'')?>>IZINKAN</option>
													<option value="0" <?=($st_input=='0'?'selected':'')?>>TIDAK</option>
													
												</select>
											</th>
											<th>
												<select id="st_edit" name="st_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1" <?=($st_edit=='1'?'selected':'')?>>IZINKAN</option>
													<option value="0" <?=($st_edit=='0'?'selected':'')?>>TIDAK</option>
													
												</select>
											</th>
											<th>
												<select id="st_hapus" name="st_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1" <?=($st_hapus=='1'?'selected':'')?>>IZINKAN</option>
													<option value="0" <?=($st_hapus=='0'?'selected':'')?>>TIDAK</option>
													
												</select>
											</th>
											<th>
												<select id="st_cetak" name="st_cetak" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1" <?=($st_cetak=='1'?'selected':'')?>>IZINKAN</option>
													<option value="0" <?=($st_cetak=='0'?'selected':'')?>>TIDAK</option>
													
												</select>
											</th>
											<th>
												
												<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_hak_akses" name="btn_tambah_hak_akses"><i class="fa fa-plus"></i> Tambah</button>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
				
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2234'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?>" id="tab_3">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('SIGN IN')?></h5>&nbsp;
					</div>
				</div>
				</div>
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="label_sig_in_ina">SIG IN <span style="color:red;"> INA</span></label>
						<div class="col-md-10">
							<input class="form-control auto_blur" type="text" id="label_sig_in_ina" name="label_sig_in_ina" value="{label_sig_in_ina}" placeholder="INA">
						</div>
					</div>
				</div>
				</div>
				<div class="col-md-12" >
				<div class="col-md-12" >
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="label_sig_in_eng">SIG IN <span style="color:red;"> ENG</span></label>
						<div class="col-md-10">
							<input class="form-control auto_blur" type="text" id="label_sig_in_eng" name="label_sig_in_eng" value="{label_sig_in_eng}" placeholder="ENG">
						</div>
					</div>
				</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-2 control-label" for="judul_header_eng"> </label>
							<div class="col-md-10">
							<button class="btn btn-danger btn-xs" type="button" onclick="add_content(1)" ><i class="fa fa-plus"></i> TAMBAH DATA</button>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="sub_header"></label>
							<div class="col-md-10">
								<table class="table" id="index_content_1">
									<thead>
										<tr>
											<th class="text-center" style="width: 50px;">#</th>
											<th>Content</th>
											<th class="hidden-xs" style="width: 15%;">Opsi</th>
											<th class="hidden-xs" style="width: 15%;">Created</th>
											<th class="text-center" style="width: 100px;">Actions</th>
										</tr>
									</thead>
									<tbody>
										
										
									</tbody>
								</table>
							</div>
							
						</div>
						
					</div>
				</div>
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('TIME OUT')?></h5>&nbsp;
					</div>
				</div>
				</div>
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="label_sig_in_ina">TIME OUT <span style="color:red;"> INA</span></label>
						<div class="col-md-10">
							<input class="form-control auto_blur" type="text" id="label_time_out_ina" name="label_time_out_ina" value="{label_time_out_ina}" placeholder="INA">
						</div>
					</div>
				</div>
				</div>
				<div class="col-md-12" >
				<div class="col-md-12" >
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="label_time_out_eng">TIME OUT <span style="color:red;"> ENG</span></label>
						<div class="col-md-10">
							<input class="form-control auto_blur" type="text" id="label_time_out_eng" name="label_time_out_eng" value="{label_time_out_eng}" placeholder="ENG">
						</div>
					</div>
				</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-2 control-label" for="judul_header_eng"> </label>
							<div class="col-md-10">
							<button class="btn btn-danger btn-xs" type="button" onclick="add_content(2)" ><i class="fa fa-plus"></i> TAMBAH DATA</button>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="sub_header"></label>
							<div class="col-md-10">
								<table class="table" id="index_content_2">
									<thead>
										<tr>
											<th class="text-center" style="width: 50px;">#</th>
											<th>Content</th>
											<th class="hidden-xs" style="width: 15%;">Opsi</th>
											<th class="hidden-xs" style="width: 15%;">Created</th>
											<th class="text-center" style="width: 100px;">Actions</th>
										</tr>
									</thead>
									<tbody>
										
										
									</tbody>
								</table>
							</div>
							
						</div>
						
					</div>
				</div>
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('SIGN OUT')?></h5>&nbsp;
					</div>
				</div>
				</div>
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="label_sig_in_ina">SIGN OUT <span style="color:red;"> INA</span></label>
						<div class="col-md-10">
							<input class="form-control auto_blur" type="text" id="label_sign_out_ina" name="label_sign_out_ina" value="{label_sign_out_ina}" placeholder="INA">
						</div>
					</div>
				</div>
				</div>
				<div class="col-md-12" >
				<div class="col-md-12" >
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="label_sign_out_eng">SIGN OUT <span style="color:red;"> ENG</span></label>
						<div class="col-md-10">
							<input class="form-control auto_blur" type="text" id="label_sign_out_eng" name="label_sign_out_eng" value="{label_sign_out_eng}" placeholder="ENG">
						</div>
					</div>
				</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-md-2 control-label" for="judul_header_eng"> </label>
							<div class="col-md-10">
							<button class="btn btn-danger btn-xs" type="button" onclick="add_content(3)" ><i class="fa fa-plus"></i> TAMBAH DATA</button>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="sub_header"></label>
							<div class="col-md-10">
								<table class="table" id="index_content_3">
									<thead>
										<tr>
											<th class="text-center" style="width: 50px;">#</th>
											<th>Content</th>
											<th class="hidden-xs" style="width: 15%;">Opsi</th>
											<th class="hidden-xs" style="width: 15%;">Created</th>
											<th class="text-center" style="width: 100px;">Actions</th>
										</tr>
									</thead>
									<tbody>
										
										
									</tbody>
								</table>
							</div>
							
						</div>
						
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
		
		
	</div>
</div>
<div class="modal" id="modal_content" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Content</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal push-10-t">
					<div class="row">
                        <input type="hidden" readonly id="jenis_header" name="jenis_header" value="">
                        <input type="hidden" readonly id="header_id" name="header_id" value="">
                        <input type="hidden" readonly id="idcontent" name="idcontent" value="">

						<div class="col-md-12">                               
							<div class="form-group">
								<label class="col-md-2 control-label">No Urut</label>
								<div class="col-md-9"> 
									<input tabindex="0" type="text" class="form-control number" maxlength="3" id="no" placeholder="No Urut" value="">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Content</label>
								<div class="col-md-9"> 
									<textarea class="form-control js-summernote footer" name="isi" id="isi"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Jenis Jawaban</label>
								<div class="col-md-9"> 
									<select tabindex="5" id="jenis_isi"   name="reservasi_cara" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="0"  <?=($jenis_isi == "0" ? 'selected="selected"' : '')?>>Label</option>
										<option value="1"  <?=($jenis_isi == "1" ? 'selected="selected"' : '')?>>Option</option>
										<option value="2" <?=($jenis_isi == 2 ? 'selected="selected"' : '')?>>Free Text</option>
										<option value="3" <?=($jenis_isi == 3 ? 'selected="selected"' : '')?>>Tanda Tangan</option>
										
									</select>
								</div>
							</div>
							<div class="form-group div_opsi">
								<label class="col-md-2 control-label">Value</label>
								<div class="col-md-9"> 
									<select id="ref_id" name="ref_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
																		
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">                               
							<br>
						</div>
                    </div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-primary" type="submit" type="button" id="btn_simpan_content"><i class="fa fa-check"></i> Simpan</button>
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
		$(".number").number(true,0,'.',',');
	var tab=$("#tab").val();
	if (tab=='1'){
		load_general();
	}
	if (tab=='2'){
		load_tab2();
	}
	if (tab=='3'){
		load_tab3();
	}
	$("#jenis_isi").val('0').trigger('change');
	$('.js-summernote').summernote({
		  height: 80,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})

})	
$("#jenis_isi").change(function() {
	let jenis_isi=$("#jenis_isi").val();
	if (jenis_isi=='1'){
		$(".div_opsi").css("display", "block");
	}else{
		
		$(".div_opsi").css("display", "none");
	}
	
});

//add_content
$("#btn_simpan_content").click(function() {
	if ($("#no").val()==''){
		sweetAlert("Maaf...", "Tentukan No Urut", "error");
		return false;
	}
	if ($("#no").val()=='0'){
		sweetAlert("Maaf...", "Tentukan No Urut", "error");
		return false;
	}

	if ($("#isi").val()==''){
		sweetAlert("Maaf...", "Isi Content ", "error");
		return false;
	}
	var idcontent=$("#idcontent").val();
	var no=$("#no").val();
	var header_id=$("#header_id").val();
	var jenis_header=$("#jenis_header").val();
	var isi=$("#isi").val();
	var jenis_isi=$("#jenis_isi").val();
	var ref_id=$("#ref_id").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Setting_keselamatan/simpan_content/',
		dataType: "json",
		type: "POST",
		data: {
			jenis_header:jenis_header,
			idcontent:idcontent,
			header_id:header_id,
			jenis_isi:jenis_isi,
			ref_id:ref_id,
			no: no,isi:isi
		},
		success: function(data) {
			$("#modal_content").modal('hide');
			$("#cover-spin").hide();
			load_content();
		}
	});

});
$("#jenis_isi").change(function() {
	let jenis_isi=$("#jenis_isi").val();
	if (jenis_isi=='1'){
		$(".div_opsi").css("display", "block");
	}else{
		
		$(".div_opsi").css("display", "none");
	}
	
});
function add_content(jenis_header){
	$("#modal_content").modal('show');
	$("#jenis_header").val(jenis_header);
	$("#header_id").val('0');
	$("#idcontent").val('');
	var rowCount = $('#index_content tr').length;
	$("#no").val(rowCount);
	$('#isi').summernote('code','');
	var id='';
	$.ajax({
			url: '{site_url}Setting_keselamatan/load_jawaban',
			type: 'POST',
			dataType: "json",
			data: {id: id},
			success: function(data) {
				console.log(data);
				$("#ref_id").empty();
				$('#ref_id').append(data);
				$("#cover-spin").hide();
			}
	});
}
function hapus(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}Setting_keselamatan/hapus',
				type: 'POST',
				data: {id: id},
				complete: function() {
					load_content();
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function add_detail(id,jenis_header){
	$("#modal_content").modal('show');
	$("#jenis_header").val(jenis_header);
	$("#header_id").val(id);
	$("#idcontent").val('');
	var rowCount = $('#index_content tr').length;
	$("#no").val(rowCount);
	$('#isi').summernote('code','');
	var id='';
	$.ajax({
			url: '{site_url}Setting_keselamatan/load_jawaban',
			type: 'POST',
			dataType: "json",
			data: {id: id},
			success: function(data) {
				console.log(data);
				$("#ref_id").empty();
				$('#ref_id').append(data);
				$("#cover-spin").hide();
			}
	});
};
function edit_content(id,header_id,jenis_header){
	$("#jenis_header").val(jenis_header);
	$("#idcontent").val(id);
	$("#header_id").val(header_id);
	$("#modal_content").modal('show');
	$("#cover-spin").show();
		
	 $.ajax({
			url: '{site_url}Setting_keselamatan/load_jawaban',
			type: 'POST',
			dataType: "json",
			data: {id: id},
			success: function(data) {
				console.log(data);
				$("#ref_id").empty();
				$('#ref_id').append(data);
				$("#cover-spin").hide();
			}
	});
	$("#cover-spin").show();
	$.ajax({
			url: '{site_url}Setting_keselamatan/get_edit',
			type: 'POST',
			dataType: "json",
			data: {id: id},
			success: function(data) {
				console.log(data);
				$('#isi').summernote('code',data.isi);
				// $("#isi").html(data.isi);
				$("#jenis_isi").val(data.jenis_isi).trigger('change');
				$("#no").val(data.no);
				$("#cover-spin").hide();
				
			}
	});
}
function load_content(){
	$.ajax({
		url: '{site_url}Setting_keselamatan/load_content/',
		dataType: "json",
		success: function(data) {
			$("#index_content_1 tbody").empty();
			$('#index_content_1 tbody').append(data.tabel_1);
			$("#index_content_2 tbody").empty();
			$('#index_content_2 tbody').append(data.tabel_2);
			
			$("#index_content_3 tbody").empty();
			$('#index_content_3 tbody').append(data.tabel_3);
		}
	});

}
function load_general(){
	$('.js-summernote').summernote({
	  height: 50,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	});	

}
function load_tab2(){
	load_hak_akses();		
	// load_formulir();	
}
function load_tab3(){
	load_content();		
	// load_formulir();	
}

// LOGIC
function load_hak_akses(){
	$('#index_hak_akses').DataTable().destroy();	
	table = $('#index_hak_akses').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_keselamatan/load_hak_akses', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
$("#btn_tambah_hak_akses").click(function() {
	let profesi_id=$("#profesi_id").val();
	let spesialisasi_id=$("#spesialisasi_id").val();
	let mppa_id=$("#mppa_id").val();
	let st_lihat=$("#st_lihat").val();
	let st_input=$("#st_input").val();
	let st_edit=$("#st_edit").val();
	let st_hapus=$("#st_hapus").val();
	let st_cetak=$("#st_cetak").val();
	
	if (profesi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Profesi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_keselamatan/simpan_hak_akses', 
		dataType: "JSON",
		method: "POST",
		data : {
				profesi_id:profesi_id,
				spesialisasi_id:spesialisasi_id,
				mppa_id:mppa_id,
				st_lihat:st_lihat,
				st_input:st_input,
				st_edit:st_edit,
				st_hapus:st_hapus,
				st_cetak:st_cetak,
			
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				$("#profesi_id").val('#').trigger('change');
				$("#spesialisasi_id").val('#').trigger('change');
				$("#mppa_id").val('#').trigger('change');
				$("#st_cetak").val('0').trigger('change');
				$("#st_hapus").val('0').trigger('change');
				$("#st_edit").val('0').trigger('change');
				$("#st_input").val('0').trigger('change');
				$("#st_lihat").val('0').trigger('change');
				$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});

});
function hapus_hak_akses(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_keselamatan/hapus_hak_akses',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function hapus_default(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Default?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_keselamatan/hapus_default',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_tampil_default').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

$("#profesi_id").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_keselamatan/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id").empty();
				$("#mppa_id").append(data);
			}
		});
	}else{
		
				$("#mppa_id").empty();
	}

});
$(".auto_blur").blur(function(){
	$.ajax({
		url: '{site_url}setting_keselamatan/update_label/'+$(this).val(),
		dataType: "json",
		type: 'POST',
		data: {
			label_sig_in_ina : $("#label_sig_in_ina").val(),
			label_sig_in_eng : $("#label_sig_in_eng").val(),
			label_time_out_ina : $("#label_time_out_ina").val(),
			label_time_out_eng : $("#label_time_out_eng").val(),
			label_sign_out_ina : $("#label_sign_out_ina").val(),
			label_sign_out_eng : $("#label_sign_out_eng").val(),

			
			},
		success: function(data) {
			
		}
	});
	
});
</script>