<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<b><span class="label label-success" style="font-size:12px">SETTING NOMOR AKUN</span></b>
		<br><br>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_nomor_akun">
			<thead>
				<tr>
					<th width="20%">Kategori Dokter</th>
					<th width="20%">Nomor Akun Pendapatan Netto</th>
					<th width="20%">Nomor Akun Pajak</th>
					<th width="20%">Nomor Akun Piutang Dokter</th>
					<th width="20%">Aksi</th>
				</tr>
				<tr>
					<th>
						<select class="js-select2 form-control" id="nomor_akun_kategori_dokter" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach(get_all('mdokter_kategori') as $row) { ?>
								<option value="<?=$row->id?>"><?= $row->nama?></option>
							<?php } ?>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="nomor_akun_pendapatan_netto" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach(get_all('makun_nomor') as $row) { ?>
								<option value="<?=$row->id?>"><?= $row->noakun.' '.$row->namaakun?></option>
							<?php } ?>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="nomor_akun_pajak" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach(get_all('makun_nomor') as $row) { ?>
								<option value="<?=$row->id?>"><?= $row->noakun.' '.$row->namaakun?></option>
							<?php } ?>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="nomor_akun_piutang_dokter" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach(get_all('makun_nomor') as $row) { ?>
								<option value="<?=$row->id?>"><?= $row->noakun.' '.$row->namaakun?></option>
							<?php } ?>
						</select>
					</th>
					<th>
						<button class="btn btn-primary" id="submit_nomor_akun">Tambahkan</button>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($setting_akun as $row) { ?>
					<tr>
						<td hidden><?=$row->idkategori?></td>
						<td><?=$row->namakategori?></td>
						<td hidden><?=$row->idakun_pendapatan?></td>
						<td><?=$row->namaakun_pendapatan?></td>
						<td hidden><?=$row->idakun_pajak?></td>
						<td><?=$row->namaakun_pajak?></td>
						<td hidden><?=$row->idakun_piutang?></td>
						<td><?=$row->namaakun_piutang?></td>
						<td>
							<div class='btn btn-group'>
								<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		</div>
		<hr>

		<b><span class="label label-success" style="font-size:12px">SETTING PENYERAHAN ASURANSI</span></b>
		<br><br>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_penyerahan_asuransi">
			<thead>
				<tr>
					<th width="10%">Kategori Dokter</th>
					<th width="10%">Pendapatan Rajal</th>
					<th width="10%">Pendapatan Rujukan RI</th>
					<th width="10%">Pendapatan Laboratorium</th>
					<th width="10%">Pendapatan Fisioterapi</th>
					<th width="10%">Pendapatan Radiologi</th>
					<th width="10%">Pendapatan Rawat Inap</th>
					<th width="10%">Pendapatan Anesthesi</th>
					<th width="20%">Aksi</th>
				</tr>
				<tr>
					<th>
						<select class="js-select2 form-control" id="asuransi_kategori_dokter" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach(get_all('mdokter_kategori') as $row) { ?>
								<option value="<?=$row->id?>"><?= $row->nama?></option>
							<?php } ?>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="asuransi_pendapatan_rajal" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Diberikan</option>
							<option value="2">Menunggu</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="asuransi_pendapatan_rujukan_ranap" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Diberikan</option>
							<option value="2">Menunggu</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="asuransi_pendapatan_laboratorium" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Diberikan</option>
							<option value="2">Menunggu</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="asuransi_pendapatan_fisioterapi" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Diberikan</option>
							<option value="2">Menunggu</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="asuransi_pendapatan_radiologi" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Diberikan</option>
							<option value="2">Menunggu</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="asuransi_pendapatan_ranap" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Diberikan</option>
							<option value="2">Menunggu</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="asuransi_pendapatan_anaesthesi" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Diberikan</option>
							<option value="2">Menunggu</option>
						</select>
					</th>
					<th>
						<button class="btn btn-primary" id="submit_penyerahan_asuransi">Tambahkan</button>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($setting_penyerahan_asuransi as $row) { ?>
					<tr>
						<td hidden><?=$row->idkategori?></td>
						<td><?=$row->namakategori?></td>
						<td hidden><?=$row->pendapatan_rajal?></td>
						<td><?=GetStatusPenyerahanHonor($row->pendapatan_rajal)?></td>
						<td hidden><?=$row->pendapatan_rujukan_ranap?></td>
						<td><?=GetStatusPenyerahanHonor($row->pendapatan_rujukan_ranap)?></td>
						<td hidden><?=$row->pendapatan_laboratorium?></td>
						<td><?=GetStatusPenyerahanHonor($row->pendapatan_laboratorium)?></td>
						<td hidden><?=$row->pendapatan_fisioterapi?></td>
						<td><?=GetStatusPenyerahanHonor($row->pendapatan_fisioterapi)?></td>
						<td hidden><?=$row->pendapatan_radiologi?></td>
						<td><?=GetStatusPenyerahanHonor($row->pendapatan_radiologi)?></td>
						<td hidden><?=$row->pendapatan_ranap?></td>
						<td><?=GetStatusPenyerahanHonor($row->pendapatan_ranap)?></td>
						<td hidden><?=$row->pendapatan_anesthesi?></td>
						<td><?=GetStatusPenyerahanHonor($row->pendapatan_anesthesi)?></td>
						<td>
							<div class='btn btn-group'>
								<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		</div>
		<hr>

		<b><span class="label label-success" style="font-size:12px">SETTING PENYERAHAN HOLD</span></b>
		<br><br>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_penyerahan_hold">
			<thead>
				<tr>
					<th width="10%">Kategori Dokter</th>
					<th width="10%">Pendapatan Rajal</th>
					<th width="10%">Pendapatan Rujukan RI</th>
					<th width="10%">Pendapatan Laboratorium</th>
					<th width="10%">Pendapatan Fisioterapi</th>
					<th width="10%">Pendapatan Radiologi</th>
					<th width="10%">Pendapatan Rawat Inap</th>
					<th width="10%">Pendapatan Anesthesi</th>
					<th width="20%">Aksi</th>
				</tr>
				<tr>
					<th>
						<select class="js-select2 form-control" id="hold_kategori_dokter" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach(get_all('mdokter_kategori') as $row) { ?>
								<option value="<?=$row->id?>"><?= $row->nama?></option>
							<?php } ?>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="hold_pendapatan_rajal" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Diberikan</option>
							<option value="2">Menunggu</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="hold_pendapatan_rujukan_ranap" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Diberikan</option>
							<option value="2">Menunggu</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="hold_pendapatan_laboratorium" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Diberikan</option>
							<option value="2">Menunggu</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="hold_pendapatan_fisioterapi" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Diberikan</option>
							<option value="2">Menunggu</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="hold_pendapatan_radiologi" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Diberikan</option>
							<option value="2">Menunggu</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="hold_pendapatan_ranap" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Diberikan</option>
							<option value="2">Menunggu</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" id="hold_pendapatan_anaesthesi" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Diberikan</option>
							<option value="2">Menunggu</option>
						</select>
					</th>
					<th>
						<button class="btn btn-primary" id="submit_penyerahan_hold">Tambahkan</button>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($setting_penyerahan_hold as $row) { ?>
					<tr>
						<td hidden><?=$row->idkategori?></td>
						<td><?=$row->namakategori?></td>
						<td hidden><?=$row->pendapatan_rajal?></td>
						<td><?=GetStatusPenyerahanHonor($row->pendapatan_rajal)?></td>
						<td hidden><?=$row->pendapatan_rujukan_ranap?></td>
						<td><?=GetStatusPenyerahanHonor($row->pendapatan_rujukan_ranap)?></td>
						<td hidden><?=$row->pendapatan_laboratorium?></td>
						<td><?=GetStatusPenyerahanHonor($row->pendapatan_laboratorium)?></td>
						<td hidden><?=$row->pendapatan_fisioterapi?></td>
						<td><?=GetStatusPenyerahanHonor($row->pendapatan_fisioterapi)?></td>
						<td hidden><?=$row->pendapatan_radiologi?></td>
						<td><?=GetStatusPenyerahanHonor($row->pendapatan_radiologi)?></td>
						<td hidden><?=$row->pendapatan_ranap?></td>
						<td><?=GetStatusPenyerahanHonor($row->pendapatan_ranap)?></td>
						<td hidden><?=$row->pendapatan_anesthesi?></td>
						<td><?=GetStatusPenyerahanHonor($row->pendapatan_anesthesi)?></td>
						<td>
							<div class='btn btn-group'>
								<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		</div>
	</div>
</div>

<input type="hidden" class="form-control" id="rowindex">

<script type="text/javascript">
	$(document).on("click", "#submit_nomor_akun", function() {
		var rowindex;
		var duplicate = false;

		if ($("#rowindex").val() != '') {
			var content = "";
			rowindex = $("#rowindex").val();
		} else {
			var content = "<tr>";
			$('#table_nomor_akun tbody tr').filter(function() {
				var $cells = $(this).children('td');
				if ($cells.eq(0).text() === $("#nomor_akun_kategori_dokter option:selected").val()) {
					sweetAlert("Maaf...", "Kategori Dokter '" + $("#nomor_akun_kategori_dokter option:selected").text() + "' sudah ditambahkan.", "error");
					duplicate = true;
					return false;
				}
			});
		}

		if (duplicate == false) {
			content += "<td style='display: none'>" + $("#nomor_akun_kategori_dokter option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#nomor_akun_kategori_dokter option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#nomor_akun_pendapatan_netto option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#nomor_akun_pendapatan_netto option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#nomor_akun_pajak option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#nomor_akun_pajak option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#nomor_akun_piutang_dokter option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#nomor_akun_piutang_dokter option:selected").text(); + "</td>";
			content += "<td width='10%'>";
			content += "	<div class='btn btn-group'>";
			content += "		<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
			content += "	</div>";
			content += "</td>";

			if ($("#rowindex").val() != '') {
				$('#table_nomor_akun tbody tr:eq(' + rowindex + ')').html(content);
			} else {
				content += "</tr>";
				$('#table_nomor_akun tbody').append(content);
			}

			submitForm();
		}
	});

	$(document).on("click", "#submit_penyerahan_asuransi", function() {
		var rowindex;
		var duplicate = false;

		if ($("#rowindex").val() != '') {
			var content = "";
			rowindex = $("#rowindex").val();
		} else {
			var content = "<tr>";
			$('#table_penyerahan_asuransi tbody tr').filter(function() {
				var $cells = $(this).children('td');
				if ($cells.eq(0).text() === $("#asuransi_kategori_dokter option:selected").val()) {
					sweetAlert("Maaf...", "Kategori Dokter '" + $("#asuransi_kategori_dokter option:selected").text() + "' sudah ditambahkan.", "error");
					duplicate = true;
					return false;
				}
			});
		}

		if (duplicate == false) {
			content += "<td style='display: none'>" + $("#asuransi_kategori_dokter option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#asuransi_kategori_dokter option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#asuransi_pendapatan_rajal option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#asuransi_pendapatan_rajal option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#asuransi_pendapatan_rujukan_ranap option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#asuransi_pendapatan_rujukan_ranap option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#asuransi_pendapatan_laboratorium option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#asuransi_pendapatan_laboratorium option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#asuransi_pendapatan_fisioterapi option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#asuransi_pendapatan_fisioterapi option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#asuransi_pendapatan_radiologi option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#asuransi_pendapatan_radiologi option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#asuransi_pendapatan_ranap option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#asuransi_pendapatan_ranap option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#asuransi_pendapatan_anaesthesi option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#asuransi_pendapatan_anaesthesi option:selected").text(); + "</td>";
			content += "<td width='10%'>";
			content += "	<div class='btn btn-group'>";
			content += "		<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
			content += "	</div>";
			content += "</td>";

			if ($("#rowindex").val() != '') {
				$('#table_penyerahan_asuransi tbody tr:eq(' + rowindex + ')').html(content);
			} else {
				content += "</tr>";
				$('#table_penyerahan_asuransi tbody').append(content);
			}

			submitForm();
		}
	});

	$(document).on("click", "#submit_penyerahan_hold", function() {
		var rowindex;
		var duplicate = false;

		if ($("#rowindex").val() != '') {
			var content = "";
			rowindex = $("#rowindex").val();
		} else {
			var content = "<tr>";
			$('#table_penyerahan_hold tbody tr').filter(function() {
				var $cells = $(this).children('td');
				if ($cells.eq(0).text() === $("#hold_kategori_dokter option:selected").val()) {
					sweetAlert("Maaf...", "Kategori Dokter '" + $("#hold_kategori_dokter option:selected").text() + "' sudah ditambahkan.", "error");
					duplicate = true;
					return false;
				}
			});
		}

		if (duplicate == false) {
			content += "<td style='display: none'>" + $("#hold_kategori_dokter option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#hold_kategori_dokter option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#hold_pendapatan_rajal option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#hold_pendapatan_rajal option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#hold_pendapatan_rujukan_ranap option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#hold_pendapatan_rujukan_ranap option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#hold_pendapatan_laboratorium option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#hold_pendapatan_laboratorium option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#hold_pendapatan_fisioterapi option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#hold_pendapatan_fisioterapi option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#hold_pendapatan_radiologi option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#hold_pendapatan_radiologi option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#hold_pendapatan_ranap option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#hold_pendapatan_ranap option:selected").text(); + "</td>";
			content += "<td style='display: none'>" + $("#hold_pendapatan_anaesthesi option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#hold_pendapatan_anaesthesi option:selected").text(); + "</td>";
			content += "<td width='10%'>";
			content += "	<div class='btn btn-group'>";
			content += "		<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
			content += "	</div>";
			content += "</td>";

			if ($("#rowindex").val() != '') {
				$('#table_penyerahan_hold tbody tr:eq(' + rowindex + ')').html(content);
			} else {
				content += "</tr>";
				$('#table_penyerahan_hold tbody').append(content);
			}

			submitForm();
		}
	});

	$(document).on("click", ".delete", function() {
		var row = $(this).closest('td').parent();
		swal({
			title: '',
			text: "Apakah anda yakin akan menghapus data ini?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Ya!",
			cancelButtonText: "Batalkan!",
			closeOnConfirm: false,
			closeOnCancel: true
		}).then(function() {
			row.remove();

			return submitForm();
		})
		return false;
	});

	function submitForm() {
		var nomor_akun_tbl = $('table#table_nomor_akun tbody tr').get().map(function(row) {
			return $(row).find('td:not(:eq(8))').get().map(function(cell) {
				return $(cell).html();
			});
		});

		var penyerahan_asuransi_tbl = $('table#table_penyerahan_asuransi tbody tr').get().map(function(row) {
			return $(row).find('td:not(:eq(16))').get().map(function(cell) {
				return $(cell).html();
			});
		});

		var penyerahan_hold_tbl = $('table#table_penyerahan_hold tbody tr').get().map(function(row) {
			return $(row).find('td:not(:eq(16))').get().map(function(cell) {
				return $(cell).html();
			});
		});

		$.ajax({
			url: '{site_url}msetting_honor_dokter/update',
			method: 'POST',
			data: {
				nomor_akun: JSON.stringify(nomor_akun_tbl),
				penyerahan_asuransi: JSON.stringify(penyerahan_asuransi_tbl),
				penyerahan_hold: JSON.stringify(penyerahan_hold_tbl)
			},
			success: function(data) {
				swal({
					title: "Berhasil!",
					text: "Proses penyimpanan data.",
					type: "success",
					timer: 1000,
					showConfirmButton: false
				});
			}
		});
	}
</script>
