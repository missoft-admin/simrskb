<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>
<?php $user_id = $this->session->userdata('user_id'); ?>

<div class="block">
    <div class="block-content">
        <div class="row">
            <div class="col-md-3">
                <h4>RIWAYAT HASIL PEMERIKSAAN</h4> <br>
            </div>
            <div class="col-md-9 text-right">
                <a href="{site_url}term_radiologi_mri_hasil" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
            </div>
        </div>

        <hr>
        
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nomorRekamMedis">Nomor Rekam Medis & Nama Pasien</label>
                            <input type="text" class="form-control" placeholder="Nomor Rekam Medis" disabled value="{nomor_medrec} {nama_pasien}">
                        </div>

                        <div class="form-group">
                            <label for="jenisKelamin">Jenis Kelamin</label>
                            <input type="text" class="form-control" placeholder="Jenis Kelamin" disabled value="{jenis_kelamin}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nik">Nomor Induk Kependudukan</label>
                            <input type="text" class="form-control" placeholder="NIK" disabled value="{nomor_ktp}">
                        </div>

                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="email" class="form-control" placeholder="E-mail" disabled value="{email}">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <input type="text" class="form-control" placeholder="Alamat" disabled value="{alamat_pasien}">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="tglLahir">Tanggal Lahir</label>
                                    <input type="date" class="form-control" placeholder="Tanggal Lahir" disabled value="{tanggal_lahir}">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="umur">Umur</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Tahun" disabled value="{umur_tahun}">
                                                <div class="input-group-addon">
                                                    <span class="input-group-text">Tahun</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="umur">&nbsp;</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Bulan" disabled value="{umur_bulan}">
                                                <div class="input-group-addon">
                                                    <span class="input-group-text">Bulan</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="umur">&nbsp;</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Hari" disabled value="{umur_hari}">
                                                <div class="input-group-addon">
                                                    <span class="input-group-text">Hari</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kelompokPasien">Kelompok Pasien</label>
                            <input type="text" class="form-control" placeholder="Kelompok Pasien" disabled value="{kelompok_pasien}">
                        </div>

                        <div class="form-group">
                            <label for="namaAsuransi">Nama Asuransi</label>
                            <input type="text" class="form-control" placeholder="Nama Asuransi" disabled value="{nama_asuransi}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tipeKunjungan">Tipe Kunjungan</label>
                            <input type="text" class="form-control" placeholder="Tipe Kunjungan" disabled value="{tipe_kunjungan}">
                        </div>

                        <div class="form-group">
                            <label for="namaPoliklinik">Nama Poliklinik</label>
                            <input type="text" class="form-control" placeholder="Nama Poliklinik" disabled value="{nama_poliklinik}">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nomorPendaftaran">Nomor Pendaftaran</label>
                            <input type="text" class="form-control" placeholder="Nomor Pendaftaran" disabled value="{nomor_pendaftaran}">
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nomorPermintaan">Nomor Permintaan</label>
                            <input type="text" class="form-control" placeholder="Nomor Permintaan" disabled value="{nomor_permintaan}">
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nomorTransaksiRadiologi">Nomor Transaksi Radiologi</label>
                            <input type="text" class="form-control" placeholder="Nomor Transaksi Radiologi" disabled value="{nomor_transaksi_radiologi}">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="waktuPembuatan">Waktu Pembuatan</label>
                            <input type="text" class="form-control" placeholder="Waktu Pembuatan" disabled value="{waktu_pembuatan}">
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="waktuPermintaan">Waktu Permintaan</label>
                            <input type="text" class="form-control" placeholder="Nomor Permintaan" disabled value="{waktu_permintaan}">
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="rencanaPemeriksaan">Rencana Pemeriksaan</label>
                            <input type="text" class="form-control" placeholder="Rencana Pemeriksaan" disabled value="{rencana_pemeriksaan}">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="diagnosa">Diagnosa</label>
                            <input type="text" class="form-control" placeholder="Diagnosa" disabled value="{diagnosa}">
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="catatanPermintaan">Catatan Permintaan</label>
                            <input type="text" class="form-control" placeholder="Catatan Permintaan" disabled value="{catatan_permintaan}">
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nomorFoto">Nomor Foto</label>
                            <input type="text" class="form-control" placeholder="Nomor Foto" disabled value="{nomor_foto}">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-xs-12" for="">Prioritas Pemeriksaan</label>
                            <div class="col-xs-12">
                                <select class="js-select2 form-control" style="width: 100%;" disabled>
                                    <?php foreach (list_variable_ref(249) as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?php echo '' == $prioritas_pemeriksaan && '1' == $row->st_default ? 'selected' : ''; ?> <?php echo $row->id == $prioritas_pemeriksaan ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-xs-12" for="">Pasien Puasa</label>
                            <div class="col-xs-12">
                                <select class="js-select2 form-control" style="width: 100%;" disabled>
                                    <?php foreach (list_variable_ref(87) as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?php echo '' == $pasien_puasa && '1' == $row->st_default ? 'selected' : ''; ?> <?php echo $row->id == $pasien_puasa ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-xs-12" for="">Pengiriman Hasil</label>
                            <div class="col-xs-12">
                                <select class="js-select2 form-control" style="width: 100%;" disabled>
                                    <?php foreach (list_variable_ref(88) as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?php echo '' == $pengiriman_hasil && '1' == $row->st_default ? 'selected' : ''; ?> <?php echo $row->id == $pengiriman_hasil ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-xs-12" for="">Waktu Penginputan Foto</label>
                        <div class="col-xs-12">
                            <div class="row">
                            <div class="col-md-6">
                                <div class="input-group date">
                                <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" disabled value="{tanggal_penginputan_foto}">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                <input type="text" class="time-datepicker form-control" disabled value="{waktu_penginputan_foto}">
                                <span class="input-group-addon"><i class="si si-clock"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-xs-12" for="">Petugas Penginputan Foto</label>
                        <div class="col-xs-12">
                            <select class="js-select2 form-control" id="petugasPenginputanFoto" style="width: 100%;" disabled>
                            </select>
                        </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-xs-12" for="">Waktu Penginputan Ekspertise</label>
                        <div class="col-xs-12">
                            <div class="row">
                            <div class="col-md-6">
                                <div class="input-group date">
                                <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" disabled value="{tanggal_penginputan_expertise}">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                <input type="text" class="time-datepicker form-control" disabled value="{waktu_penginputan_expertise}">
                                <span class="input-group-addon"><i class="si si-clock"></i></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                        <label class="col-xs-12" for="">Petugas Penginputan Ekspertise</label>
                        <div class="col-xs-12">
                            <select class="js-select2 form-control" id="petugasPenginputanExpertise" style="width: 100%;" disabled>
                            </select>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="tableFix2">
                    <table class="table table-bordered table-striped" id="table-history">
                        <thead>
                            <tr>
                                <th width="10%">Version</th>
                                <th width="10%">Proses Transaksi Oleh</th>
                                <th width="10%">Input Foto Oleh</th>
                                <th width="10%">Input Ekspertise Oleh</th>
                                <th width="10%">Status</th>
                                <th width="10%">Dibatalkan Oleh</th>
                                <th width="10%">Jumlah Dicetak</th>
                                <th width="10%">Alasan Pembatalan</th>
                                <th width="10%">Nama File</th>
                                <th width="10%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('Term_radiologi_mri_expertise/modal/modal_riwayat_cetak_hasil_pemeriksaan'); ?>
<?php $this->load->view('Term_radiologi_mri_expertise/services'); ?>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        loadDataPetugasPenginputanFoto('{tujuan_radiologi}', '{petugas_penginputan_foto}');
        loadDataPetugasPenginputanExpertise('{tujuan_radiologi}', '{petugas_penginputan_expertise}');
        loadDataTableRiwayatCetakHasilPemeriksaan();
    });

    function loadDataTableRiwayatCetakHasilPemeriksaan() {
        $('#table-history').DataTable().destroy();
        $('#table-history').DataTable({
            "autoWidth": false,
            "searching": true,
            "pageLength": 50,
            "serverSide": true,
            "processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
            "columnDefs": [
                {
                    "width": "10%",
                    "targets": [0, 1, 2, 3, 4, 5, 6, 7, 9],
                    "className": "text-center"
                },
            ],
            ajax: {
                url: '{site_url}term_radiologi_mri_expertise/getIndexRiwayatHasil/' + '{id}',
                type: "POST",
                dataType: 'json',
            }
        });

        $("#cover-spin").hide();
    }
</script>