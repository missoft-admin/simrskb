<!-- Modal Alasan Pembatalan -->
<div class="modal fade" id="modalAlasanPembatalan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
                <div class="block-content">
                    <h5 class="text-center">Apakah anda yakin akan merubah hasil pemeriksaan ini. Jika anda membatalkan maka hasil cetak akan berubah</h5>
                    <br><br>
                    <div class="form-group">
                        <label for="alasan-pembatalan">Alasan Pembatalan</label>
                        <input type="text" class="form-control" id="alasan-pembatalan" placeholder="Isikan Alasan Pembatalan (Required)" required value="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal" id="btn-update-alasan-pembatalan">Ya</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="pembatalan-transaksi-id" value="">
    <input type="hidden" id="pembatalan-hasil-pemeriksaan-id" value="">
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#btn-update-alasan-pembatalan').click(function () {
            let transaksiId = $('#pembatalan-transaksi-id').val();
            let hasilPemeriksaanId = $('#pembatalan-hasil-pemeriksaan-id').val();
            let alasanPembatalan = $('#alasan-pembatalan').val();

            console.log(transaksiId, hasilPemeriksaanId, alasanPembatalan);

            prosesBatalHasilPemeriksaan(transaksiId, hasilPemeriksaanId, alasanPembatalan);
        });
    });

    function prosesBatalHasilPemeriksaan(transaksiId, hasilPemeriksaanId, alasanPembatalan) {
        $.ajax({
            url: '{site_url}term_radiologi_mri_expertise/batal_hasil_pemeriksaan/' + transaksiId,
            method: 'POST',
            data: {
                hasil_pemeriksaan_id: hasilPemeriksaanId,
                alasan: alasanPembatalan
            },
            success: function (result) {
                window.location = '{base_url}term_radiologi_mri_expertise/proses/' + transaksiId;
            },
            error: function (error) {
                console.error('Error deleting order:', error);
                alert('Error deleting order. Please try again.');
            }
        });
    }
</script>