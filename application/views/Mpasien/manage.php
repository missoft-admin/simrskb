<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<!-- Developer : @RendyIchtiarSaputra -->
<div class="block" style="text-transform:uppercase;">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}mpasien" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mpasien/save','class="form-horizontal push-10-t"') ?>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="no_medrec">No. Medrec<span style="color:red;">*</span></label>
			<div class="col-md-7">
				<input tabindex="4" readonly type="text" class="form-control" id="no_medrec" placeholder="No. Medrec" name="no_medrec" value="{no_medrec}" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 0px; padding-bottom:0px;">
			<label class="col-md-3 control-label" for="nama">Nama<span style="color:red;">*</span></label>
			<div class="col-md-3" style="padding-right: 10px;">
				<select tabindex="3" id="titlepasien" name="title" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
				<?php foreach(get_all('mpasien_title') as $row):?>
					<option value="<?=$row->singkatan;?>" <?=($title == $row->singkatan ? 'selected':'')?>><?=$row->singkatan;?></option>
				<?php endforeach;?>
				</select>
			</div>
			<div class="col-md-4" style="padding-left: 0px;">
				<input tabindex="4" type="text" class="form-control" id="nama" placeholder="Nama Lengkap" name="nama" value="{nama}" required>
			</div>
		</div>

		<div class="form-group" style="margin-bottom: 10px;margin-top:10px;padding-top:0px;">
			<label class="col-md-3 control-label" for="jeniskelamin">Jenis Kelamin <span style="color:red;">*</span></label>
			<div class="col-md-7">
				<select tabindex="5" id="jeniskelamin" name="jeniskelamin" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<option value="1" <?=($jenis_kelamin == 1 ? 'selected':'')?>>Laki-laki</option>
					<option value="2" <?=($jenis_kelamin == 2 ? 'selected':'')?>>Perempuan</option>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="alamat">Alamat<span style="color:red;">*</span></label>
			<div class="col-md-7">
				<textarea tabindex="6" class="form-control" id="alamat" placeholder="Alamat" name="alamat" required><?=$alamat_jalan?></textarea>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 0px;">
			<div class="col-md-3"></div>
			<label class="col-md-2 control-label" for="provinsi"><p align="left">Provinsi</p></label>
			<div class="col-md-5">
				<?php if($idpasien == NULL){ ?>
					<select tabindex="7" name="provinsi" id="provinsi" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach(get_all('mfwilayah',array('jenis' => '1')) as $row):?>
							<option value="<?=$row->id;?>" <?=($row->id == "12") ? "selected" : "";?>><?=$row->nama;?></option>
						<?php endforeach;?>
					</select>
				<?php }else{ ?>
					<select tabindex="7" name="provinsi" id="provinsi" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach(get_all('mfwilayah',array('jenis' => '1')) as $row):?>
							<option value="<?=$row->id;?>" <?=($provinsi_id == $row->id ? 'selected':'')?>><?=$row->nama;?></option>
						<?php endforeach;?>
					</select>
				<?php } ?>
			</div>
		</div>
		<div id="kabupaten" style="display: block;">
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-3"></div>
				<label class="col-md-2" for="kabupaten"><left>Kabupaten/Kota</left></label>
				<div class="col-md-5">
					<?php if($idpasien == NULL){?>
						<select tabindex="8" name="kabupaten" id="kabupaten1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</<option>
							<?php foreach(get_all('mfwilayah',array('parent_id' => '12')) as $row):?>
								<option value="<?=$row->id;?>" <?=($row->id == "213") ? "selected" : "";?>><?=$row->nama;?></<option>
							<?php endforeach;?>
						</select>
					<?php }else{ ?>
						<select tabindex="8" name="kabupaten" id="kabupaten1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</<option>
							<?php foreach(get_all('mfwilayah',array('parent_id' => $provinsi_id)) as $row):?>
								<option value="<?=$row->id;?>" <?=($kabupaten_id == $row->id ? 'selected':'')?>><?=$row->nama;?></<option>
							<?php endforeach;?>
						</select>
					<?php } ?>
				</div>
			</div>
		</div>
		<div id="kecamatan" style="display: block;">
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-3"></div>
				<label class="col-md-2 control-label" for="kecamatan" style="text-align:left;">Kecamatan</label>
				<div class="col-md-5">
					<?php if($idpasien == NULL){?>
						<select tabindex="9"="kecamatan" id="kecamatan1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</<option>
							<?php foreach(get_all('mfwilayah',array('parent_id' => '213')) as $row):?>
								<option value="<?=$row->id;?>"><?=$row->nama;?></<option>
							<?php endforeach;?>
						</select>
					<?php }else{ ?>
						<select tabindex="9" name="kecamatan" id="kecamatan1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</<option>
							<?php foreach(get_all('mfwilayah',array('parent_id' => $kabupaten_id)) as $row):?>
								<option value="<?=$row->id;?>"<?=($kecamatan_id == $row->id ? 'selected':'')?>><?=$row->nama;?></<option>
							<?php endforeach;?>
						</select>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php if($idpasien == NULL){?>
		<div id="kelurahan" style="display: none;">
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-3"></div>
				<label class="col-md-2 control-label" for="kelurahan" style="text-align:left;">Kelurahan</label>
				<div class="col-md-5">
					<select tabindex="10" name="kelurahan" id="kelurahan1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-3"></div>
				<label class="col-md-2 control-label" for="kodepos" style="text-align:left;">Kode Pos</label>
				<div class="col-md-5">
					<input tabindex="10" type="text" class="form-control" id="kodepos" placeholder="Kode Pos" name="kodepos" value="{kodepos}" readonly="true">
				</div>
			</div>
		</div>
		<?php }else{ ?>
		<div id="kelurahan" style="display: block;">
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-3"></div>
				<label class="col-md-2 control-label" for="kelurahan" style="text-align:left;">Kelurahan</label>
				<div class="col-md-5">
					<select tabindex="11" name="kelurahan" id="kelurahan1" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</<option>
						<?php foreach(get_all('mfwilayah',array('parent_id' => $kecamatan_id)) as $row):?>
						<option value="<?=$row->id;?>"<?=($kelurahan_id == $row->id ? 'selected':'')?>><?=$row->nama;?></<option>
						<?php endforeach;?>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-3"></div>
				<label class="col-md-2 control-label" for="kodepos" style="text-align:left;">Kode Pos</label>
				<div class="col-md-5">
					<input tabindex="12" type="text" class="form-control" id="kodepos" placeholder="Kode Pos" name="kodepos" value="{kodepos}" readonly="true">
				</div>
			</div>
		</div>
		<?php } ?>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="noidentitas">No Identitas</label>
			<div class="col-md-3" style="padding-right: 10px;">
				<select tabindex="13" id="jenis_id" name="jenisidentitas" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="1" <?=($jenis_id == 1 ? 'selected':'')?>>KTP</option>
					<option value="2" <?=($jenis_id == 2 ? 'selected':'')?>>SIM</option>
					<option value="3" <?=($jenis_id == 3 ? 'selected':'')?>>Pasport</option>
					<option value="4" <?=($jenis_id == 4 ? 'selected':'')?>>Kartu Pelajar/Mahasiswa</option>
				</select>
			</div>
			<div class="col-md-4" style="margin-left:0px;padding-left: 0px;">
				<input tabindex="14" type="text" class="form-control" id="noidentitas" placeholder="No Identitas" name="noidentitas" value="{ktp}">
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="nohp">No Hp</label>
			<div class="col-md-7">
				<input tabindex="15" type="text" class="form-control" id="nohp" placeholder="No HP" name="nohp" value="{hp}">
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="telprumah">Telepon Rumah</label>
			<div class="col-md-7">
				<input tabindex="16" type="text" class="form-control" id="telprumah" placeholder="Telepon Rumah" name="telprumah" value="{telepon}">
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="email">Email</label>
			<div class="col-md-7">
				<input tabindex="17" type="text" class="form-control" id="email" placeholder="Email" name="email" value="{email}">
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="tempatlahir">Tempat Lahir</label>
			<div class="col-md-7">
				<input tabindex="18" type="text" class="form-control" id="tempatLahir" placeholder="Tempat Lahir" name="tempatlahir" value="{tempat_lahir}">
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="tgllahir">Tanggal Lahir<span style="color:red;">*</span></label>
			<div class="col-md-2" style="padding-right: 10px;">
				<select tabindex="19" name="hari_lahir" id="hari" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="0">Hari</option>
					<?php for($i = 1;$i < 32;$i++){?>
						<?php $id = str_pad($i, 2, '0', STR_PAD_LEFT); ?>
						<option value="<?=$id;?>" <?=($tgl_hari == $id ? 'selected':'')?>><?=$id;?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-2" style="padding-right: 10px;padding-left:0px;">
				<select tabindex="20" name="bulan_lahir" id="bulan" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="00">Bulan</option>
					<option value="01" <?=($tgl_bulan == '01' ? 'selected':'')?>>Januari</option>
					<option value="02" <?=($tgl_bulan == '02' ? 'selected':'')?>>Februari</option>
					<option value="03" <?=($tgl_bulan == '03' ? 'selected':'')?>>Maret</option>
					<option value="04" <?=($tgl_bulan == '04' ? 'selected':'')?>>April</option>
					<option value="05" <?=($tgl_bulan == '05' ? 'selected':'')?>>Mei</option>
					<option value="06" <?=($tgl_bulan == '06' ? 'selected':'')?>>Juni</option>
					<option value="07" <?=($tgl_bulan == '07' ? 'selected':'')?>>Juli</option>
					<option value="08" <?=($tgl_bulan == '08' ? 'selected':'')?>>Agustus</option>
					<option value="09" <?=($tgl_bulan == '09' ? 'selected':'')?>>September</option>
					<option value="10" <?=($tgl_bulan == '10' ? 'selected':'')?>>Oktober</option>
					<option value="11" <?=($tgl_bulan == '11' ? 'selected':'')?>>November</option>
					<option value="12" <?=($tgl_bulan == '12' ? 'selected':'')?>>Desember</option>
				</select>
			</div>
			<div class="col-md-3" style="padding-left:0px;">
				<select tabindex="21" name="tahun_lahir" id="tahun" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="0">Tahun</option>
					<?php for($i = date("Y");$i >= 1900;$i--){?>
						<option value="<?=$i;?>" <?=($tgl_tahun == $i ? 'selected':'')?>><?=$i;?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="umur">Umur</label>
			<div class="col-md-2">
				<input tabindex="22" type="text" class="form-control" id="tahun1" name="umurtahun" value="{umur_tahun}" placeholder="Tahun" readonly="true">
			</div>
			<div class="col-md-2">
				<input tabindex="23" type="text" class="form-control" id="bulan1" name="umurbulan" value="{umur_bulan}" placeholder="Bulan" readonly="true">
			</div>
			<div class="col-md-3">
				<input tabindex="24" type="text" class="form-control" id="hari1" name="umurhari" value="{umur_hari}" placeholder="Hari" readonly="true">
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="golongandarah">Golongan Darah</label>
			<div class="col-md-7">
				<select tabindex="25" id="golongandarah" name="golongandarah" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<option value="1" <?=($golongan_darah == 1 ? 'selected':'')?>>A</option>
					<option value="2" <?=($golongan_darah == 2 ? 'selected':'')?>>B</option>
					<option value="3" <?=($golongan_darah == 3 ? 'selected':'')?>>AB</option>
					<option value="4" <?=($golongan_darah == 4 ? 'selected':'')?>>O</option>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="agama">Agama</label>
			<div class="col-md-7">
				<select tabindex="26" id="agama" name="agama" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<option value="1" <?=($agama_id == 1 ? 'selected':'')?>>Islam</option>
					<option value="2" <?=($agama_id == 2 ? 'selected':'')?>>Kristen</option>
					<option value="3" <?=($agama_id == 3 ? 'selected':'')?>>Hindu</option>
					<option value="4" <?=($agama_id == 4 ? 'selected':'')?>>Budha</option>
					<option value="5" <?=($agama_id == 5 ? 'selected':'')?>>KongHucu</option>
					<option value="6" <?=($agama_id == 6 ? 'selected':'')?>>Lain Lain</option>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="kewarganegaraan">Kewarganegaraan</label>
			<div class="col-md-7">
				<select tabindex="27" id="kewarganegaraan" name="kewarganegaraan" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<option value="1" <?=($warganegara == 1 ? 'selected':'')?>>WNI</option>
					<option value="2" <?=($warganegara == 2 ? 'selected':'')?>>WNA</option>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="suku">Suku</label>
			<div class="col-md-7">
				<input tabindex="28" type="text" class="form-control" id="suku" placeholder="Suku" name="suku" value="{suku}">
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="statuskawin">Status Kawin</label>
			<div class="col-md-7">
				<select tabindex="29" id="statuskawin" name="statuskawin" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<option value="1" <?=($status_kawin == '1' ? 'selected':'')?>>Belum Menikah</option>
					<option value="2" <?=($status_kawin == '2' ? 'selected':'')?>>Sudah Menikah</option>
					<option value="3" <?=($status_kawin == '3' ? 'selected':'')?>>Janda</option>
					<option value="4" <?=($status_kawin == '4' ? 'selected':'')?>>Duda</option>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="pendidikan">Pendidikan</label>
			<div class="col-md-7">
				<select tabindex="30" id="pendidikan" name="pendidikan" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<?php foreach(get_all('mpendidikan') as $row):?>
						<option value="<?=$row->id;?>" <?=($pendidikan_id == $row->id ? 'selected':'')?>><?=$row->nama;?></option>
					<?php endforeach;?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="pekerjaan">Pekerjaan</label>
			<div class="col-md-7">
				<select tabindex="31" id="pekerjaan" name="pekerjaan" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<?php foreach(get_all('mpekerjaan') as $row):?>
						<option value="<?=$row->id;?>" <?=($pekerjaan_id == $row->id ? 'selected':'')?>><?=$row->nama;?></option>
					<?php endforeach;?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="pekerjaan">Catatan</label>
			<div class="col-md-7">
				<textarea tabindex="46" class="form-control" id="catatan" placeholder="Catatan" name="catatan"><?= $catatan?></textarea>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="no_medrec"><span style="color:blue;">Penanggung Jawab</span></label>
			
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<div class="col-md-3"></div>
			<label class="col-md-2 control-label" for="nama_keluarga" style="text-align:left;">Nama</label>
			<div class="col-md-5">
				<input tabindex="12" type="text" class="form-control" id="nama_keluarga" placeholder="Nama Keluarga" name="nama_keluarga" value="{nama_keluarga}" >
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<div class="col-md-3"></div>
			<label class="col-md-2 control-label" for="hubungan_dengan_pasien" style="text-align:left;">Hub. Pasien</label>
			<div class="col-md-5">
				<input tabindex="12" type="text" class="form-control" id="hubungan_dengan_pasien" placeholder="Hub. Pasien" name="hubungan_dengan_pasien" value="{hubungan_dengan_pasien}" >
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<div class="col-md-3"></div>
			<label class="col-md-2 control-label" for="alamat_keluarga" style="text-align:left;">Alamat</label>
			<div class="col-md-5">
				<textarea tabindex="6" class="form-control" id="alamat_keluarga" placeholder="Alamat" name="alamat_keluarga" required><?=$alamat_keluarga?></textarea>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<div class="col-md-3"></div>
			<label class="col-md-2 control-label" for="telepon_keluarga" style="text-align:left;">Telepon</label>
			<div class="col-md-5">
				<input tabindex="12" type="text" class="form-control" id="telepon_keluarga" placeholder="No. Tlp" name="telepon_keluarga" value="{telepon_keluarga}" >
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<div class="col-md-3"></div>
			<label class="col-md-2 control-label" for="ktp_keluarga" style="text-align:left;">No Identitas</label>
			<div class="col-md-5">
				<input tabindex="12" type="text" class="form-control" id="ktp_keluarga" placeholder="No. Identitas " name="ktp_keluarga" value="{ktp_keluarga}" >
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-3 control-label" for="no_medrec"><span style="color:blue;"></span></label>
			
		</div>
		<div class="form-group">
			<div class="col-md-9 col-md-offset-3">
				<button class="btn btn-success" type="submit">Simpan</button>
				<a href="{base_url}mpasien" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
			</div>
		</div>
		<?php echo form_hidden('idpasien', $idpasien); ?>
		<?php echo form_close() ?>
	</div>
</div>

<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.select2').select2();

		// Trigger Alamat (Provinsi, Kota, Kecamatan)
		$("#provinsi").change(function() {
			$("#kabupaten").css("display", "block");
			var kodeprovinsi = $("#provinsi").val();
			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getKabupaten',
				method: "POST",
				dataType: "json",
				data: {
					"kodeprovinsi": kodeprovinsi
				},
				success: function(data) {
					$("#kabupaten1").empty();
					$("#kabupaten1").append("<option value=''>Pilih Opsi</option>");
					for (var i = 0; i < data.kabupaten.length; i++) {
						$("#kabupaten1").append("<option value='" + data.kabupaten[i].id + "'>" + data.kabupaten[i].nama + "</option>");
					}
					$("#kabupaten1").selectpicker("refresh");
				},
				error: function(data) {
					alert(data);
				}
			});
		});

		$("#kabupaten1").change(function() {
			$("#kecamatan").css("display", "block");
			var kodekab = $("#kabupaten1").val();
			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getKecamatan',
				method: "POST",
				dataType: "json",
				data: {
					"kodekab": kodekab
				},
				success: function(data) {
					$("#kecamatan1").empty();
					$("#kecamatan1").append("<option value=''>Pilih Opsi</option>");
					for (var i = 0; i < data.kecamatan.length; i++) {
						$("#kecamatan1").append("<option value='" + data.kecamatan[i].id + "'>" + data.kecamatan[i].nama + "</option>");
					}
					$("#kecamatan1").selectpicker("refresh");
				},
				error: function(data) {
					alert(data);
				}
			});
		});

		$("#kecamatan1").change(function() {
			$("#kelurahan").css("display", "block");
			var kodekec = $("#kecamatan1").val();
			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getKelurahan',
				method: "POST",
				dataType: "json",
				data: {
					"kodekec": kodekec
				},
				success: function(data) {
					$("#kelurahan1").empty();
					$("#kelurahan1").append("<option value=''>Pilih Opsi</option>");
					for (var i = 0; i < data.kelurahan.length; i++) {
						$("#kelurahan1").append("<option value='" + data.kelurahan[i].id + "'>" + data.kelurahan[i].nama + "</option>");
					}
					$("#kelurahan1").selectpicker("refresh");
				},
				error: function(data) {
					alert(data);
				}
			});
		});

		$("#kelurahan1").change(function() {
			var kodepos = $("#kelurahan1").val();
			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getKodepos',
				method: "POST",
				dataType: "json",
				data: {
					"kodepos": kodepos
				},
				success: function(data) {
					$("#kodepos").empty();
					$("#kodepos").val(data.kodepos[0].kodepos);
				}
			});
		});

		// Triger Umum Pasien
		$("#tahun").change(function() {
			var hari = $("#hari").val();
			var bulan = $("#bulan").val();
			var tahun = $("#tahun").val();
			var tanggal = tahun + "-" + bulan + "-" + hari;
			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getDate',
				method: "POST",
				dataType: "json",
				data: {
					"hari": hari,
					"bulan": bulan,
					"tahun": tahun,
					"tanggal": tanggal
				},
				success: function(data) {
					$("#tahun1").val(data.date[0].tahun);
					$("#bulan1").val(data.date[0].bulan);
					$("#hari1").val(data.date[0].hari);

					getTitlePasien(data.date[0].tahun, data.date[0].bulan, data.date[0].hari);
				}
			});
		});

		$("#bulan").change(function() {
			var hari = $("#hari").val();
			var bulan = $("#bulan").val();
			var tahun = $("#tahun").val();
			var tanggal = tahun + "-" + bulan + "-" + hari;
			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getDate',
				method: "POST",
				dataType: "json",
				data: {
					"hari": hari,
					"bulan": bulan,
					"tahun": tahun,
					"tanggal": tanggal
				},
				success: function(data) {
					if (tahun == "0") {
						$("#tahun1").val("0");
						$("#bulan1").val(data.date[0].bulan);
						$("#hari1").val(data.date[0].hari);
					} else {
						$("#tahun1").val(data.date[0].tahun);
						$("#bulan1").val(data.date[0].bulan);
						$("#hari1").val(data.date[0].hari);
					}

					getTitlePasien(data.date[0].tahun, data.date[0].bulan, data.date[0].hari);
				}
			});
		});

		$("#hari").change(function() {
			var hari = $("#hari").val();
			var bulan = $("#bulan").val();
			var tahun = $("#tahun").val();
			var tanggal = tahun + "-" + bulan + "-" + hari;
			$.ajax({
				url: '{site_url}tpoliklinik_pendaftaran/getDate',
				method: "POST",
				dataType: "json",
				data: {
					"hari": hari,
					"bulan": bulan,
					"tahun": tahun,
					"tanggal": tanggal
				},
				success: function(data) {
					if (tahun == "0" && bulan == "0") {
						$("#tahun1").val("0");
						$("#bulan1").val("0");
						$("#hari1").val(hari);
					} else {
						$("#tahun1").val(data.date[0].tahun);
						$("#bulan1").val(data.date[0].bulan);
						$("#hari1").val(data.date[0].hari);
					}

					getTitlePasien(data.date[0].tahun, data.date[0].bulan, data.date[0].hari);
				}
			});
		});

		$("#jeniskelamin").change(function() {
			getTitlePasien($("#tahun1").val(), $("#bulan").val(), $("#hari").val());
		});

		$("#statuskawin").change(function() {
			getTitlePasien($("#tahun1").val(), $("#bulan").val(), $("#hari").val());
		});
});

function getTitlePasien(tahun, bulan, hari) {
	var statuskawin = ($("#statuskawin option:selected").val() != '' ? $("#statuskawin option:selected").val() : 1);
	var jeniskelamin = ($("#jeniskelamin option:selected").val() != '' ? $("#jeniskelamin option:selected").val() : 1);

	if (tahun < 1) {
		$('#titlepasien').val("By");
	} else if (tahun >= 1 && tahun <= 15) {
		$('#titlepasien').val("An");
	} else if (tahun >= 16 && jeniskelamin == '1') {
		$('#titlepasien').val("Tn");
	} else if (tahun >= 15 && jeniskelamin == '2' && statuskawin == 1) {
		$('#titlepasien').val("Nn");
	} else if (jeniskelamin == '2' && statuskawin == 2) {
		$('#titlepasien').val("Ny");
	}
}
</script>
