<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('129'))){ ?>
<div class="block">
	<div class="block-header">
	<?php if (UserAccesForm($user_acces_form,array('130'))){ ?>
		<ul class="block-options">
       
            <a href="{base_url}mdari/create" type="button" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
       
			</ul>
	<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>Urutan</th>
					<th>Nama</th>
					<th>Deskripsi</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		</div>
	</div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ 
		// alert('sini');
		BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mdari/getIndex',
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": true },
					{ "width": "35%", "targets": 1, "orderable": true },
					{ "width": "35%", "targets": 2, "orderable": true },
					{ "width": "15%", "targets": 3, "orderable": true }
				]
			});
	});
</script>
