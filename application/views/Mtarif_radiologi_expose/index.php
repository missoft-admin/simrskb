<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('234'))){ ?>
<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('236'))){ ?>
		<ul class="block-options">
        <li>
            <a href="{base_url}mtarif_radiologi_expose/create" class="btn"><i class="fa fa-plus"></i></a>
        </li>
    </ul>
		<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"bSort": false,
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
				url: '{site_url}mtarif_radiologi_expose/getIndex/',
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "10%", "targets": 0, "orderable": true },
					{ "width": "70%", "targets": 1, "orderable": true },
					{ "width": "20%", "targets": 2, "orderable": true }
				]
			});
	});
</script>
