<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		
		<?php if (UserAccesForm($user_acces_form,array('1847'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_1"><i class="fa fa-check-square-o"></i> Label Form</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1848'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_2"><i class="fa fa-check-square-o"></i> Label Perencanaan</a>
		</li>
		<?}?>
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		
		<?php if (UserAccesForm($user_acces_form,array('1847'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_1">
			
			<?php echo form_open('setting_rm/save_rm_label','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="form-group">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">PESAN INFORMASI</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-12">
							<textarea class="form-control js-summernote" name="pesan_informasi"><?=$pesan_informasi?></textarea>
						</div>
					</div>
				</div>
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">JUDUL FORMULIR</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="judul_ina" value="{judul_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="judul_eng" value="{judul_eng}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Waktu Permintaan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="waktu_req" <?=($waktu_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="waktu_ina" value="{waktu_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="waktu_eng" value="{waktu_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tujuan </label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="tujuan_req" <?=($tujuan_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="tujuan_ina" value="{tujuan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="tujuan_eng" value="{tujuan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Dokter Peminta</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="dokter_req" <?=($dokter_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="dokter_ina" value="{dokter_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="dokter_eng" value="{dokter_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Diagnosa</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="diagnosa_req" <?=($diagnosa_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="diagnosa_ina" value="{diagnosa_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="diagnosa_eng" value="{diagnosa_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Sebanyak</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="selama_req" <?=($sebanyak_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="sebanyak_ina" value="{sebanyak_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="sebanyak_eng" value="{sebanyak_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Selama</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="selama_req" <?=($selama_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="selama_ina" value="{selama_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="selama_eng" value="{selama_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Kontrol Kembali Setelah</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="kontrol_req" <?=($kontrol_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="kontrol_ina" value="{kontrol_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="kontrol_eng" value="{kontrol_eng}">
						</div>
						
					</div>
				</div>
				
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Prioritas</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="prioritas_req" <?=($prioritas_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="prioritas_ina" value="{prioritas_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="prioritas_eng" value="{prioritas_eng}">
						</div>
						
					</div>
				</div>
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Catatan Pemeriksaan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="catatan_req" <?=($catatan_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="catatan_ina" value="{catatan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="catatan_eng" value="{catatan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Catatan Keterangan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="ket_req" <?=($ket_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="ket_ina" value="{ket_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="ket_eng" value="{ket_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Detail Tambahan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="detail_req" <?=($detail_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="detail_ina" value="{detail_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="detail_eng" value="{detail_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-top:15px">
				
					<div class="col-md-12">
						<div class="col-md-6">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_label" name="btn_save_general"><i class="fa fa-save"></i> Simpan</button>
						</div>
						
					</div>
				</div>
			</div>
			
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
		
		<?php if (UserAccesForm($user_acces_form,array('1848'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?> " id="tab_2">
			
			<?php echo form_open_multipart('setting_rm/save_rm_label_per','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="form-group">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">KODE PERENCANAAN</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-12">
							<input class="form-control" name="kode" value={kode}>
						</div>
					</div>
				</div>
				<div class="form-group">
					
					<div class="col-md-12">
						<div class="col-md-12">
							<img class="img-avatar"  id="output_img" src="{upload_path}app_setting/{logo}" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Logo Login (100x100)</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="box">
								<input type="file" id="file-3" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;" name="logo" value="{logo}" />
								<label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">JUDUL PERENCANAAN</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="judul_per_ina" value="{judul_per_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="judul_per_eng" value="{judul_per_eng}">
						</div>
					</div>
				</div>
				
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Paragraph 1</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<textarea class="form-control js-summernote" name="paragraph_1_ina"><?=$paragraph_1_ina?></textarea>
							
						</div>
						<div class="col-md-6">
							<label>English</label>
							<textarea class="form-control js-summernote" name="paragraph_1_eng"><?=$paragraph_1_eng?></textarea>
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Paragraph 2</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<textarea class="form-control js-summernote" name="paragraph_2_ina"><?=$paragraph_2_ina?></textarea>
						</div>
						<div class="col-md-6">
							<label>English</label>
							<textarea class="form-control js-summernote" name="paragraph_2_eng"><?=$paragraph_2_eng?></textarea>
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Nama Pasien & No Rekam Medis</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="nama_pasien_ina" value="{nama_pasien_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="nama_pasien_eng" value="{nama_pasien_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tanggal lahir dan umur</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="ttl_ina" value="{ttl_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="ttl_eng" value="{ttl_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Dengan Diagnosa</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="diagnosa_per_ina" value="{diagnosa_per_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="diagnosa_per_eng" value="{diagnosa_per_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Fisioterapi</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="fisio_ina" value="{fisio_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="fisio_eng" value="{fisio_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Sebanyak</label>&nbsp;&nbsp;&nbsp;&nbsp;
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="sebanyak_per_ina" value="{sebanyak_per_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="sebanyak_per_eng" value="{sebanyak_per_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Selama</label>&nbsp;&nbsp;&nbsp;&nbsp;
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="selama_per_ina" value="{selama_per_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="selama_per_eng" value="{selama_per_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Mohon Kontrol Kembali Setelah</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="kontrol_per_ina" value="{kontrol_per_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="kontrol_per_eng" value="{kontrol_per_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Paragraph 3</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<textarea class="form-control js-summernote" name="paragraph_3_ina"><?=$paragraph_3_ina?></textarea>
						</div>
						<div class="col-md-6">
							<label>English</label>
							<textarea class="form-control js-summernote" name="paragraph_3_eng"><?=$paragraph_3_eng?></textarea>
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Footer</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="footer_ina" value="{footer_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="footer_eng" value="{footer_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-top:15px">
				
					<div class="col-md-12">
						<div class="col-md-6">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_label" name="btn_save_general"><i class="fa fa-save"></i> Simpan</button>
						</div>
						
					</div>
				</div>
			</div>
			
			<?php echo form_close() ?>
			
		</div>
		<?}?>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
function loadFile_img(event){
	// alert('load');
	var output_img = document.getElementById('output_img');
	output_img.src = URL.createObjectURL(event.target.files[0]);
}
function loadFile_img_bg2(event){
	// alert('sini');
	var output_img_bg = document.getElementById('output_img_bg');
	output_img_bg.src = URL.createObjectURL(event.target.files[0]);
}
$(document).ready(function(){	
	$('.js-summernote').summernote({
		  height: 50,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})

		$(".number").number(true,0,'.',',');
	var tab=$("#tab").val();
	

})	
</script>