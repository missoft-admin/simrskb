<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>JURNAL RETUR</title>
	<style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
		@page {
            margin-top: 1,8em;
            margin-left: 2,3em;
            margin-right: 2em;
            margin-bottom: 2.5em;
        }
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }

      th {
        padding: 5px;
      }
	  td {
        padding: 5px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }

      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;

      }
	  td {
        padding: 3px;

      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
    }


  </style>
</head>

<body>
	<table class="content">
		<tr>
			<td rowspan="3" width="30%" class="text-center"><img src="<?=$logo1_rs?>" alt="" width="60" height="60"></td>
			<td rowspan="2" colspan="3" width="50%" class="text-center text-bold text-judul"><u>JURNAL RETUR</u></td>
			<td rowspan="2" width="20%"></td>
		</tr>
		<tr></tr>
		<tr>
			<td width="10%" class="text-left text-bold text-top">Dari Tanggal</td>
			<td width="20%" colspan="" class="text-top">: <?=$tanggal_trx1?></td>
			<td width="10%" colspan="" class="text-top text-bold text-top">No Jurnal </td>
			<td width="20%" colspan="" class="text-top">: <?=$nojurnal?></td>
		</tr>
		<tr>
			<td class="text-center"><?=$alamat_rs1?></td>
			<td class="text-left text-bold text-top">Sampai Tanggal</td>
			<td width="20%" colspan="" class="text-top">: <?=$tanggal_trx2?></td>
			<td width="10%" colspan="" class="text-top text-bold text-top">No. AKun</td>
			<td width="20%" colspan="" class="text-top">: <?=$idakun?> </td>
		</tr>
		
	</table>

	<br>

	<table class="content-2">
		
		<tr>
			<td class="border-full text-center" width="10%">Tanggal</td>
			<td class="border-full text-center" width="10%">Bukti</td>
			<td class="border-full text-center" width="10%">No Akun</td>
			<td class="border-full text-center" width="20%">Nama Akun</td>
			<td class="border-full text-center" width="10%">Debit</td>
			<td class="border-full text-center" width="10%">Kredit</td>
			<td class="border-full text-center" width="20%">Keterangan</td>
		</tr>
		<?php $debet = 0; ?>
		<?php $kredit = 0; ?>
		<?php foreach ($detail as $row) { ?>
		<?php 
			$debet = $debet + $row->debet;
			$kredit = $kredit + $row->kredit;

		?>
		<tr>
			<td class="border-full text-center"><?=HumanDate($row->tanggal_transaksi)?></td>
			<td class="border-full text-center"><?=$row->nojurnal?></td>
			<td class="border-full text-center"><?=$row->noakun?></td>
			<td class="border-full text-left"><?=$row->namaakun?></td>
			<td class="border-full text-right"><?=number_format($row->debet,2)?></td>
			<td class="border-full text-right"><?=number_format($row->kredit,2)?></td>
			<td class="border-full text-left"><?=$row->keterangan?></td>
		</tr>
		<?php } ?>
		<tr>
			<td class="border-full text-center" colspan="4"><strong>TOTAL</strong></td>
			<td class="border-full text-right"><strong><?=number_format($debet,2)?></strong></td>
			<td class="border-full text-right"><strong><?=number_format($kredit,2)?></strong></td>
			<td class="border-full text-left"></td>
		</tr>
	</table>
</body>

</html>
