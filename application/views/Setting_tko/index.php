<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('2285'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_1" onclick="load_general()"><i class="fa fa-bell"></i> General</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2286'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_2"  onclick="load_tab2()"><i class="fa fa-check-square-o"></i> Tarif</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<input type="hidden" id="iddet" name="iddet" value="">
		<?php if (UserAccesForm($user_acces_form,array('2285'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_1">
			
			<?php echo form_open('setting_tko/save_assesmen','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: -15px;">
						<div class="col-md-12">
							<label class="text-danger">MAIN</label>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-2">
							<label for="def_kel_opr" class="text-primary">Default Kelompok Operasi</label>
							<select id="def_kel_opr" name="def_kel_opr" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Choose one..">
								<option value="" <?=($def_kel_opr==''?'selected':'')?>>Silahkan Pilih</option>
								<?foreach(get_all('mkelompok_operasi',array('status'=>1)) as $r){?>
								
								<option value="<?=$r->id?>" <?=($def_kel_opr==$r->id?'selected':'')?>><?=$r->nama?></option>
								<?}?>
							</select>
						</div>
						<div class="col-md-2">
							<label for="def_jenis_opr" class="text-primary">Default Jenis Operasi</label>
							<select id="def_jenis_opr" name="def_jenis_opr" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($def_jenis_opr==''?'selected':'')?>>Silahkan Pilih</option>
								<?foreach(list_variable_ref(124) as $row){?>
								<option value="<?=$row->id?>" <?=($def_jenis_opr == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($def_jenis_opr == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
								<?}?>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="def_jenis_bedah" class="text-primary">Default Pembedahan</label>
							<select id="def_jenis_bedah" name="def_jenis_bedah" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($def_jenis_bedah==''?'selected':'')?>>Silahkan Pilih</option>
								<?foreach(list_variable_ref(389) as $row){?>
								<option value="<?=$row->id?>" <?=($def_jenis_bedah == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($def_jenis_bedah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
								<?}?>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="def_jenis_anes" class="text-primary">Default Jenis Anestesi</label>
							<select id="def_jenis_anes" name="def_jenis_anes" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($def_jenis_anes==''?'selected':'')?>>Silahkan Pilih</option>
								<?foreach(list_variable_ref(394) as $row){?>
								<option value="<?=$row->id?>" <?=($def_jenis_anes == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> <?=($def_jenis_anes == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
								<?}?>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="st_kelas_tarif" class="text-primary">Pilihan Kelas tarif</label>
							<select id="st_kelas_tarif" name="st_kelas_tarif" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($st_kelas_tarif==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($st_kelas_tarif=='1'?'selected':'')?>>Izinkan</option>
								<option value="0" <?=($st_kelas_tarif=='0'?'selected':'')?>>Tidak Diizinkan</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="st_pilih_tarif_jenis_opr" class="text-primary">Pilihan tarif Jenis Operasi</label>
							<select id="st_pilih_tarif_jenis_opr" name="st_pilih_tarif_jenis_opr" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($st_pilih_tarif_jenis_opr==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($st_pilih_tarif_jenis_opr=='1'?'selected':'')?>>Izinkan</option>
								<option value="0" <?=($st_pilih_tarif_jenis_opr=='0'?'selected':'')?>>Tidak Diizinkan</option>
								
							</select>
						</div>
						
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: -15px;">
						<div class="col-md-12">
							<label class="text-danger">TRANSAKSI SEWA RUANGAN</label>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-2">
							<label for="auto_input_sr" class="text-primary">Auto Input Sewa Ruangan</label>
							<select id="auto_input_sr" name="auto_input_sr" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($auto_input_sr==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($auto_input_sr=='1'?'selected':'')?>>YA</option>
								<option value="0" <?=($auto_input_sr=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="edit_jenis_opr_sr" class="text-primary">Edit Jenis Operasi Sewa Ruangan</label>
							<select id="edit_jenis_opr_sr" name="edit_jenis_opr_sr" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($edit_jenis_opr_sr==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($edit_jenis_opr_sr=='1'?'selected':'')?>>YA</option>
								<option value="0" <?=($edit_jenis_opr_sr=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="edit_kelas_tarif_sr" class="text-primary">Edit Kelas Tarif Sewa Ruangan</label>
							<select id="edit_kelas_tarif_sr" name="edit_kelas_tarif_sr" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($edit_kelas_tarif_sr==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($edit_kelas_tarif_sr=='1'?'selected':'')?>>YA</option>
								<option value="0" <?=($edit_kelas_tarif_sr=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="pilih_discount_sr" class="text-primary">Pilihan Discount Sewa Ruangan</label>
							<select id="pilih_discount_sr" name="pilih_discount_sr" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($pilih_discount_sr==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($pilih_discount_sr=='1'?'selected':'')?>>YA</option>
								<option value="0" <?=($pilih_discount_sr=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: -15px;">
						<div class="col-md-12">
							<label class="text-danger">TRANSAKSI FULL CARE OK</label>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-2">
							<label for="auto_input_fc" class="text-primary">Auto Input Full Care</label>
							<select id="auto_input_fc" name="auto_input_fc" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($auto_input_fc==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($auto_input_fc=='1'?'selected':'')?>>YA</option>
								<option value="0" <?=($auto_input_fc=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="edit_jenis_opr_fc" class="text-primary">Edit Jenis Operasi Full Care</label>
							<select id="edit_jenis_opr_fc" name="edit_jenis_opr_fc" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($edit_jenis_opr_fc==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($edit_jenis_opr_fc=='1'?'selected':'')?>>YA</option>
								<option value="0" <?=($edit_jenis_opr_fc=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="edit_kelas_tarif_fc" class="text-primary">Edit Kelas Tarif Full Care</label>
							<select id="edit_kelas_tarif_fc" name="edit_kelas_tarif_fc" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($edit_kelas_tarif_fc==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($edit_kelas_tarif_fc=='1'?'selected':'')?>>YA</option>
								<option value="0" <?=($edit_kelas_tarif_fc=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="pilih_discount_fc" class="text-primary">Pilihan Discount Full Care</label>
							<select id="pilih_discount_fc" name="pilih_discount_fc" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($pilih_discount_fc==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($pilih_discount_fc=='1'?'selected':'')?>>YA</option>
								<option value="0" <?=($pilih_discount_fc=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: -15px;">
						<div class="col-md-12">
							<label class="text-danger">TRANSAKSI PEMBEDAHAN DOKTER OPERATOR</label>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-2">
							<label for="do_jenis_opr" class="text-primary">Jenis Operasi</label>
							<select id="do_jenis_opr" name="do_jenis_opr" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($do_jenis_opr==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($do_jenis_opr=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($do_jenis_opr=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="do_kelas_tarif" class="text-primary">Pilihan Kelas Tarif</label>
							<select id="do_kelas_tarif" name="do_kelas_tarif" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($do_kelas_tarif==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($do_kelas_tarif=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($do_kelas_tarif=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="do_nama_tarif" class="text-primary">Nama Tarif Jenis Operasi</label>
							<select id="do_nama_tarif" name="do_nama_tarif" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($do_nama_tarif==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($do_nama_tarif=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($do_nama_tarif=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="do_pilih_operator" class="text-primary">Pilihan Operator Ke</label>
							<select id="do_pilih_operator" name="do_pilih_operator" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($do_pilih_operator==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($do_pilih_operator=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($do_pilih_operator=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="do_pilih_disc_operator" class="text-primary">Pilihan Discount Operator</label>
							<select id="do_pilih_disc_operator" name="do_pilih_disc_operator" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($do_pilih_disc_operator==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($do_pilih_disc_operator=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($do_pilih_disc_operator=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: -15px;">
						<div class="col-md-12">
							<label class="text-danger">TRANSAKSI PEMBEDAHAN ANESTESI</label>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-2">
							<label for="da_persen" class="text-primary">Default Anestesi</label>
							<div class="input-group">
								<input id="da_persen"  class="form-control  auto_blur"  type="text" name="da_persen" value="{da_persen}"  onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
								<span class="input-group-addon">%</span>
							</div>
						</div>
						<div class="col-md-2">
							<label for="da_edit_persen" class="text-primary">Edit Persentase Referensi</label>
							<select id="da_edit_persen" name="da_edit_persen" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($da_edit_persen==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($da_edit_persen=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($da_edit_persen=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="da_pilih_diskon" class="text-primary">Pilihan Discount Anestesi</label>
							<select id="da_pilih_diskon" name="da_pilih_diskon" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($da_pilih_diskon==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($da_pilih_diskon=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($da_pilih_diskon=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						
						
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: -15px;">
						<div class="col-md-12">
							<label class="text-danger">TRANSAKSI PEMBEDAHAN ASSISTEN OPERATOR</label>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-2">
							<label for="do_jenis_opr" class="text-primary">Default Assisten Operator</label>
							<div class="input-group">
								<input id="ao_persen"  class="form-control  auto_blur"  type="text" name="ao_persen" value="{ao_persen}"  onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
								<span class="input-group-addon">%</span>
							</div>
						</div>
						<div class="col-md-2">
							<label for="ao_edit_persen" class="text-primary">Edit Persentase Referensi</label>
							<select id="ao_edit_persen" name="ao_edit_persen" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($ao_edit_persen==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($ao_edit_persen=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($ao_edit_persen=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="ao_pilih_diskon" class="text-primary">Pilihan Discount Assisten Operator</label>
							<select id="ao_pilih_diskon" name="ao_pilih_diskon" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($ao_pilih_diskon==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($ao_pilih_diskon=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($ao_pilih_diskon=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						
						
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: -15px;">
						<div class="col-md-12">
							<label class="text-danger">TRANSAKSI PEMBEDAHAN ASSISTEN ANESTESI</label>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-2">
							<label for="aa_persen" class="text-primary">Default Assisten Anestesi</label>
							<div class="input-group">
								<input id="aa_persen"  class="form-control  auto_blur"  type="text" name="aa_persen" value="{aa_persen}"  onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
								<span class="input-group-addon">%</span>
							</div>
						</div>
						<div class="col-md-2">
							<label for="aa_edit_persen" class="text-primary">Edit Persentase Referensi</label>
							<select id="aa_edit_persen" name="aa_edit_persen" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($aa_edit_persen==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($aa_edit_persen=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($aa_edit_persen=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="aa_pilih_diskon" class="text-primary">Pilihan Discount Assisten Anestesi</label>
							<select id="aa_pilih_diskon" name="aa_pilih_diskon" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($aa_pilih_diskon==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($aa_pilih_diskon=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($aa_pilih_diskon=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: -15px;">
						<div class="col-md-12">
							<label class="text-danger">TRANSAKSI INPUTAN NARCOSE, OBAT, ALKES, IMPLANT</label>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-2">
							<label for="peggunaan_narcose" class="text-primary">Penggunaan Narcose</label>
							<select id="peggunaan_narcose" name="peggunaan_narcose" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($peggunaan_narcose==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($peggunaan_narcose=='1'?'selected':'')?>>ALKES</option>
								<option value="2" <?=($peggunaan_narcose=='2'?'selected':'')?>>IMPLANT</option>
								<option value="3" <?=($peggunaan_narcose=='3'?'selected':'')?>>OBAT</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="peggunaan_obat" class="text-primary">Penggunaan Obat</label>
							<select id="peggunaan_obat" name="peggunaan_obat" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($peggunaan_obat==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($peggunaan_obat=='1'?'selected':'')?>>ALKES</option>
								<option value="2" <?=($peggunaan_obat=='2'?'selected':'')?>>IMPLANT</option>
								<option value="3" <?=($peggunaan_obat=='3'?'selected':'')?>>OBAT</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="peggunaan_alkes" class="text-primary">Penggunaan Alkes</label>
							<select id="peggunaan_alkes" name="peggunaan_alkes" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($peggunaan_alkes==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($peggunaan_alkes=='1'?'selected':'')?>>ALKES</option>
								<option value="2" <?=($peggunaan_alkes=='2'?'selected':'')?>>IMPLANT</option>
								<option value="3" <?=($peggunaan_alkes=='3'?'selected':'')?>>OBAT</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="peggunaan_implan" class="text-primary">Penggunaan Implant</label>
							<select id="peggunaan_implan" name="peggunaan_implan" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($peggunaan_implan==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($peggunaan_implan=='1'?'selected':'')?>>ALKES</option>
								<option value="2" <?=($peggunaan_implan=='2'?'selected':'')?>>IMPLANT</option>
								<option value="3" <?=($peggunaan_implan=='3'?'selected':'')?>>OBAT</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: -15px;">
						<div class="col-md-12">
							<label class="text-danger">TRANSAKSI PEMBEDAHAN SEWA ALAT</label>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-2">
							<label for="sa_jenis_opr" class="text-primary">Jenis Operasi</label>
							<select id="sa_jenis_opr" name="sa_jenis_opr" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($sa_jenis_opr==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($sa_jenis_opr=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($sa_jenis_opr=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="sa_kelas_tarif" class="text-primary">Pilihan Kelas Tarif</label>
							<select id="sa_kelas_tarif" name="sa_kelas_tarif" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($sa_kelas_tarif==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($sa_kelas_tarif=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($sa_kelas_tarif=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<div class="col-md-2">
							<label for="sa_nama_tarif" class="text-primary">Nama Tarif Jenis Operasi</label>
							<select id="sa_nama_tarif" name="sa_nama_tarif" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($sa_nama_tarif==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($sa_nama_tarif=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($sa_nama_tarif=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						
						<div class="col-md-2">
							<label for="sa_pilih_disc_operator" class="text-primary">Pilihan Discount Sewa Alat</label>
							<select id="sa_pilih_disc_operator" name="sa_pilih_disc_operator" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" <?=($sa_pilih_disc_operator==''?'selected':'')?>>Silahkan Pilih</option>
								<option value="1" <?=($sa_pilih_disc_operator=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($sa_pilih_disc_operator=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						
					</div>
				</div>

			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
		<?php if (UserAccesForm($user_acces_form,array('2286'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_2">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('SETTING TARIF')?></h5>
					</div>
				</div>
				</div>
				
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_tarif">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="10%">Kelompok Pasien</th>
											<th width="15%">Nama Perusahaan</th>
											<th width="10%">Kelompok Operasi</th>
											<th width="10%">Jenis Operasi</th>
											<th width="10%">Jenis Pembedahan</th>
											<th width="10%">Jenis Anestesi</th>
											<th width="10%">Tarif Jenis Operasi</th>
											<th width="10%">Tarif Full Care</th>
											<th width="10%">Tarif Sewa Alat</th>
											<th width="10%">Action</th>										   
										</tr>
										<?$asal_pasien_tipe='#';?>
										<tr>
											<th>#<input type="hidden" value="" id="kunjungan_tipe_id"></th>
											
											<th>
												<select id="idkelompokpasien" name="idkelompokpasien" class="js-select2 form-control "  style="width:100%"  data-placeholder="Choose one..">
													<option value="0" selected>-Pilih</option>
													<?foreach(get_all('mpasien_kelompok',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											
											<th>
												<select id="idrekanan" name="idrekanan" class="js-select2 form-control div_asuransi"  style="width:100%"  data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(get_all('mrekanan',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="kelompok_opr" name="kelompok_opr" class="js-select2 form-control " style="width: 100%;" data-p0laceholder="-SEMUA-" >
													<option value="0" selected>Silahkan Pilih</option>
													<?foreach(get_all('mkelompok_operasi',array('status'=>1)) as $row){?>
													<option value="<?=$row->id?>" ><?=$row->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="jenis_opr" name="jenis_opr" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" >
													<option value="0" selected>-SEMUA-</option>
													<?foreach(list_variable_ref(124) as $row){?>
													<option value="<?=$row->id?>" ><?=$row->nama?></option>
													<?}?>
													
												</select>
											</th>
											<th>
												<select id="jenis_bedah" name="jenis_bedah" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" >
													<option value="0" selected>-SEMUA-</option>
													<?foreach(list_variable_ref(389) as $row){?>
													<option value="<?=$row->id?>" ><?=$row->nama?></option>
													<?}?>
													
												</select>
											</th>
											<th>
												<select id="jenis_anestesi" name="jenis_anestesi" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" >
													<option value="0" selected>-SEMUA-</option>
													<?foreach(list_variable_ref(394) as $row){?>
													<option value="<?=$row->id?>" ><?=$row->nama?></option>
													<?}?>
													
												</select>
											</th>
											<th>
												<select id="tarif_jenis_opr" name="tarif_jenis_opr" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" >
													<option value="" selected>Silahkan Pilih</option>
													<?foreach(get_all('mjenis_operasi',array('status'=>1)) as $row){?>
													
													<option value="<?=$row->id?>" ><?=$row->nama?></option>
													<?}?>
													
												</select>
											</th>
											<th>
												<select id="taif_full_care" name="taif_full_care" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" >
													<option value="" selected>Silahkan Pilih</option>
													<?foreach(get_all('mtarif_rawatinap',array('status'=>1,'idtipe'=>1,'idkelompok'=>0)) as $row){?>
														<option value="<?=$row->id?>" ><?=$row->nama?></option>
														<?}?>
														
													</select>
												</th>
												<th>
													<select id="tarif_sewa_alat" name="tarif_sewa_alat" class="js-select2 form-control " style="width: 100%;" data-placeholder="Pilih Opsi" >
														<option value="" selected>Silahkan Pilih</option>
														<?foreach(get_all('mtarif_operasi_sewaalat',array('status'=>1,'idkelompok'=>1)) as $row){?>
														<option value="<?=$row->id?>" ><?=$row->nama?></option>
														<?}?>
													
												</select>
											</th>
											<th>
												<button class="btn btn-primary btn-sm"  type="button" id="btn_tambah_tipe" onclick="add_tarif()"><i class="fa fa-plus"></i> Simpan</button>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>						
						</div>
					</div>
				</div>
				
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
		
		
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
		$(".number").number(true,0,'.',',');
	var tab=$("#tab").val();
	if (tab=='1'){
		load_general();
	}
	if (tab=='2'){
		load_tab2();
	}
	
	$('.js-summernote').summernote({
		  height: 80,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})

})	
$("#idkelompokpasien").change(function(){
	if ($(this).val()=='1'){
		$(".div_asuransi").removeAttr('disabled');
		
	}else{
		$(".div_asuransi").prop('disabled',true);
		$(".div_asuransi").val(0).trigger('change.select2');
	}
});
$(".opsi_change ").change(function(){
	simpan_assesmen();
});

$(".auto_blur").blur(function(){
	simpan_assesmen();
});
function simpan_assesmen(){
	$.ajax({
		url: '{site_url}setting_tko/save_assesmen', 
		dataType: "JSON",
		method: "POST",
		data : {
				def_kel_opr : $("#def_kel_opr").val(),
				def_jenis_opr : $("#def_jenis_opr").val(),
				def_jenis_bedah : $("#def_jenis_bedah").val(),
				def_jenis_anes : $("#def_jenis_anes").val(),
				st_kelas_tarif : $("#st_kelas_tarif").val(),
				st_pilih_tarif_jenis_opr : $("#st_pilih_tarif_jenis_opr").val(),
				auto_input_sr : $("#auto_input_sr").val(),
				edit_jenis_opr_sr : $("#edit_jenis_opr_sr").val(),
				edit_kelas_tarif_sr : $("#edit_kelas_tarif_sr").val(),
				pilih_discount_sr : $("#pilih_discount_sr").val(),
				auto_input_fc : $("#auto_input_fc").val(),
				edit_jenis_opr_fc : $("#edit_jenis_opr_fc").val(),
				edit_kelas_tarif_fc : $("#edit_kelas_tarif_fc").val(),
				pilih_discount_fc : $("#pilih_discount_fc").val(),
				do_jenis_opr : $("#do_jenis_opr").val(),
				do_kelas_tarif : $("#do_kelas_tarif").val(),
				do_nama_tarif : $("#do_nama_tarif").val(),
				do_pilih_operator : $("#do_pilih_operator").val(),
				do_pilih_disc_operator : $("#do_pilih_disc_operator").val(),
				da_persen : $("#da_persen").val(),
				da_edit_persen : $("#da_edit_persen").val(),
				da_pilih_diskon : $("#da_pilih_diskon").val(),
				ao_persen : $("#ao_persen").val(),
				ao_edit_persen : $("#ao_edit_persen").val(),
				ao_pilih_diskon : $("#ao_pilih_diskon").val(),
				aa_persen : $("#aa_persen").val(),
				aa_edit_persen : $("#aa_edit_persen").val(),
				aa_pilih_diskon : $("#aa_pilih_diskon").val(),
				peggunaan_narcose : $("#peggunaan_narcose").val(),
				peggunaan_obat : $("#peggunaan_obat").val(),
				peggunaan_alkes : $("#peggunaan_alkes").val(),
				peggunaan_implan : $("#peggunaan_implan").val(),
				sa_jenis_opr : $("#sa_jenis_opr").val(),
				sa_kelas_tarif : $("#sa_kelas_tarif").val(),
				sa_nama_tarif : $("#sa_nama_tarif").val(),
				sa_pilih_operator : $("#sa_pilih_operator").val(),
				sa_pilih_disc_operator : $("#sa_pilih_disc_operator").val(),

			},
		success: function(data) {
					console.log(data);
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan Assesmen.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
				$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save'});
			}
		}
	});
		
}
//Nilai

function load_general(){
	$('.js-summernote').summernote({
	  height: 50,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	});	

}
function load_tab2(){
	load_tarif();		
}

// LOGIC
function load_tarif(){
	$('#index_tarif').DataTable().destroy();	
	table = $('#index_tarif').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_tko/load_tarif', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function clear_inputan(){
	$("#idrekanan").val('0').trigger('change');
	$("#kelompok_opr").val('0').trigger('change');
	$("#jenis_opr").val('0').trigger('change');
	$("#jenis_bedah").val('0').trigger('change');
	$("#jenis_anestesi").val('0').trigger('change');
	$("#tarif_jenis_opr").val('').trigger('change');
	$("#taif_full_care").val('').trigger('change');
	$("#tarif_sewa_alat").val('').trigger('change');
	$("#iddet").val('');

}
function add_tarif(){
	let idkelompokpasien=$("#idkelompokpasien").val();
	let iddet=$("#iddet").val();
	
	if (idkelompokpasien=='0'){
		sweetAlert("Maaf...", "Tentukan Kelompok Pasien", "error");
		return false;
	}
	if ($("#tarif_jenis_opr").val()==''){
		sweetAlert("Maaf...", "Tentukan Tarif Jenis Operasi", "error");
		return false;
	}
	if ($("#taif_full_care").val()==''){
		sweetAlert("Maaf...", "Tentukan Tarif Full Care", "error");
		return false;
	}
	if ($("#tarif_sewa_alat").val()==''){
		sweetAlert("Maaf...", "Tentukan Tarif Sewa Alat", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_tko/simpan_tarif', 
		dataType: "JSON",
		method: "POST",
		data : {
				idkelompokpasien : $("#idkelompokpasien").val(),
				tipe_rekanan : $("#tipe_rekanan").val(),
				idrekanan : $("#idrekanan").val(),
				kelompok_opr : $("#kelompok_opr").val(),
				jenis_opr : $("#jenis_opr").val(),
				jenis_bedah : $("#jenis_bedah").val(),
				jenis_anestesi : $("#jenis_anestesi").val(),
				tarif_jenis_opr : $("#tarif_jenis_opr").val(),
				taif_full_care : $("#taif_full_care").val(),
				tarif_sewa_alat : $("#tarif_sewa_alat").val(),
				iddet : $("#iddet").val(),
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				load_tarif();
				clear_inputan();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});
}
function edit_tarif(id){
	$.ajax({
		url: '{site_url}setting_tko/edit_tarif',
		dataType: "JSON",
		method: "POST",
		data: {id: id},
		success: function(data) {
			$("#iddet").val(data.id);
			// alert(data.idkelompokpasien);
			$("#idkelompokpasien").val(data.idkelompokpasien).trigger('change');
			$("#idrekanan").val(data.idrekanan).trigger('change');
			$("#kelompok_opr").val(data.kelompok_opr).trigger('change');
			$("#jenis_opr").val(data.jenis_opr).trigger('change');
			$("#jenis_bedah").val(data.jenis_bedah).trigger('change');
			$("#jenis_anestesi").val(data.jenis_anestesi).trigger('change');
			$("#tarif_jenis_opr").val(data.tarif_jenis_opr).trigger('change');
			$("#taif_full_care").val(data.taif_full_care).trigger('change');
			$("#tarif_sewa_alat").val(data.tarif_sewa_alat).trigger('change');

			
		}
	});
}
function hapus_tarif(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_tko/hapus_tarif',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_tarif').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

</script>