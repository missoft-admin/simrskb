<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mreferensi_prioritas_pemeriksaan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mreferensi_prioritas_pemeriksaan/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="noid">ID</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="noid" placeholder="ID" name="noid" value="{noid}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Referensi</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Referensi" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="telepon">Set Default</label>
				<div class="col-md-7">
					<select name="status_default" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="0" <?=($status_default == 0 ? 'selected="selected"':'')?>>Tidak</option>
						<option value="1" <?=($status_default == 1 ? 'selected="selected"':'')?>>Ya</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mreferensi_prioritas_pemeriksaan" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>
