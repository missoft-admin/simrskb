<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}tpengelolaan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('tpengelolaan/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>
					<input type="hidden" class="form-control" id="tpengelolaan_id" placeholder="ID" name="tpengelolaan_id" value="{id}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="disabel" placeholder="ID" name="disabel" value="{disabel}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="st_harga_jual" placeholder="ID" name="st_harga_jual" value="{st_harga_jual}" required="" aria-required="true">
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Nama Pengelolaan</label>
				<div class="col-md-10">
					<input type="text" class="form-control" id="nama_pengelolaan" <?=$disabel?> placeholder="Nama" name="nama_pengelolaan" value="{nama_pengelolaan}" required="" aria-required="true">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="deskripsi">Tanggal Hitung</label>
				<div class="col-md-3">
					<div class="input-group date">
							<input class="js-datepicker form-control input" disabled  type="text" id="tanggal_hitung" name="tanggal_hitung" value="{tanggal_hitung}" data-date-format="dd-mm-yyyy"/>
							<span class="input-group-addon">
								<span class="fa fa-calendar"></span>
							</span>
					</div>

				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Deskrpisi</label>
				<div class="col-md-10">
					<input type="text" class="form-control" id="deskripsi_pengelolaan" <?=$disabel?> placeholder="Nama" name="deskripsi_pengelolaan" value="{deskripsi_pengelolaan}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Total Hutang</label>
				<div class="col-md-4">
					<input type="text" class="form-control number" readonly id="total_hutang" <?=$disabel?> placeholder="Hutang" name="total_hutang" value="{total_hutang}" required="" aria-required="true">
				</div>
				<label class="col-md-2 control-label" for="nama">Total Bayar</label>
				<div class="col-md-4">
					<input type="text" class="form-control number" readonly id="total_bayar" <?=$disabel?> placeholder="Total Bayar" name="total_bayar" value="{total_bayar}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Total Piutang</label>
				<div class="col-md-4">
					<input type="text" class="form-control number" readonly id="total_piutang" <?=$disabel?> placeholder="Piutang" name="total_piutang" value="{total_piutang}" required="" aria-required="true">
				</div>
				<label class="col-md-2 control-label" for="nama">Total Terima</label>
				<div class="col-md-4">
					<input type="text" class="form-control number" readonly id="total_terima" <?=$disabel?> placeholder="Total Terima" name="total_terima" value="{total_terima}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Keterangan</label>
				<div class="col-md-10">
					<input type="text" class="form-control" readonly id="keterangan_all" <?=$disabel?> placeholder="Keterangan" name="keterangan_all" value="{keterangan}" required="" aria-required="true">
				</div>
				
			</div>
			
			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button class="btn btn-primary" <?=$disabel?>  type="submit" name="btn_simpan_all" value="1">Simpan Draft</button>
					<button class="btn btn-success" <?=$disabel?>  type="submit" name="btn_simpan_all" value="2">Proses</button>
					<a href="{base_url}tpengelolaan" class="btn btn-default" type="reset"><i class="pg-close"></i> Kembali</a>
				</div>
			</div>
			
			<?php echo form_close() ?>
	</div>
</div>
<?if ($id){?>

	
	<div class="block">
		<div class="block-header">		
			<h4 ><?=text_success('Transaksi')?>  <?=text_default('Hutang')?></h4>
		</div>
		<div class="block-content">
			<input type="hidden" class="form-control" id="tedit_hutang" placeholder="No. Rek" value="">
			<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
			<table id="tabel_hutang" class="table table-striped table-bordered" style="margin-bottom: 0;">
				<thead>
					<tr>
						<th></th>
						<th>No</th>
						<th>Nama Transaksi</th>
						<th>Nominal Default</th>
						<th>QTY</th>
						<th>Total</th>
						<th>Keterangan</th>
						<th>Aksi</th>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							<select tabindex="1" id="mdata_hutang_id" <?=$disabel?> name="mdata_hutang_id" class="js-select2 form-control" style="width: 100%;" >
								<option value="#" selected>-Pilih-</option>
								<?foreach($list_hutang as $row){?>
									<option value="<?=$row->id?>" ><?=$row->nama?></option>
								<?}?>
							</select>
						</td>
						<td><input type="text" class="form-control number" <?=$disabel?> id="nominal_hutang" placeholder="Nominal" value="" style="width:100%"></td>
						<td><input type="text" class="form-control number" <?=$disabel?> id="kuantitas_hutang" placeholder="Qty" value="" style="width:100%"></td>
						<td><input type="text" readonly class="form-control number" <?=$disabel?> id="totalkeseluruhan_hutang" placeholder="Total" value="" style="width:100%"></td>						
						<td><input type="text" class="form-control" id="keterangan_hutang" <?=$disabel?> placeholder="Keterangan" value="" style="width:100%"></td>						
						<td>
							<button title="Simpan" id="btn_simpan_hutang" type="button" class="btn btn-primary btn-sm" <?=$disabel?>><i class="fa fa-save"></i></button>
							<button title="Simpan" id="btn_clear" type="button" class="btn btn-warning btn-sm" <?=$disabel?>><i class="fa fa-refresh"></i></button>
						</td>
						
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
	<div class="block">
		<div class="block-header">		
			<h4 ><?=text_primary('Transaksi Dari Pendapatan Lain-Lain')?></h4>
		</div>
		<div class="block-content">
			<input type="hidden" class="form-control" id="tedit_pendapatan" placeholder="No. Rek" value="">
			<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
			<table id="tabel_pendapatan" class="table table-striped table-bordered" style="margin-bottom: 0;">
				<thead>
					<tr>
						<th></th>
						<th>No</th>
						<th width="40%">Nama Transaksi</th>
						<th>Nominal</th>
						<th>Dari</th>
						<th>Keterangan</th>
						<th>Aksi</th>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							<div class="input-group" style="width: 110%;">
								<input class="form-control" readonly type="text" id="nama_transaksi_pendapatan" name="nama_transaksi_pendapatan">
								<span class="input-group-btn">
									<button class="btn btn-success" <?=$disabel?> id="btn_show_modal_pendapatan" type="button"><i class="fa fa-search-plus"></i></button>
								</span>
							</div>

						</td>
						<td><input type="text" class="form-control number" readonly <?=$disabel?> id="nominal_pendapatan" placeholder="Nominal" value="" style="width:100%"></td>
						<td><input type="text" class="form-control" readonly <?=$disabel?> id="dari_pendapatan" placeholder="Dari" value="" style="width:100%"></td>
						<td><input type="text" class="form-control" id="keterangan_pendapatan" <?=$disabel?> placeholder="Keterangan" value="" style="width:100%"></td>						
						<td>
							<input type="hidden" class="form-control" id="tbendahara_pendapatan_id" <?=$disabel?> placeholder="tbendahara_pendapatan_id" value="" style="width:100%">					
							<button title="Simpan" id="btn_simpan_pendapatan" type="button" class="btn btn-primary btn-sm" <?=$disabel?>><i class="fa fa-save"></i></button>
							<button title="Simpan" id="btn_clear_pendapatan" type="button" class="btn btn-warning btn-sm" <?=$disabel?>><i class="fa fa-refresh"></i></button>
						</td>
						
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
	<div class="block">
		<div class="block-header">		
			<h4 ><?=text_success('Transaksi')?>  <?=text_default('Piutang')?></h4>
		</div>
		<div class="block-content">
			<input type="hidden" class="form-control" id="tedit_piutang" placeholder="No. Rek" value="">
			<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
			<table id="tabel_piutang" class="table table-striped table-bordered" style="margin-bottom: 0;">
				<thead>
					<tr>
						<th></th>
						<th>No</th>
						<th>Nama Transaksi</th>
						<th>Nominal Default</th>
						<th>QTY</th>
						<th>Total</th>
						<th>Keterangan</th>
						<th>Aksi</th>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							<select tabindex="1" id="mdata_piutang_id" <?=$disabel?> name="mdata_piutang_id" class="js-select2 form-control" style="width: 100%;" >
								<option value="#" selected>-Pilih-</option>
								<?foreach($list_piutang as $row){?>
									<option value="<?=$row->id?>" ><?=$row->nama?></option>
								<?}?>
							</select>
						</td>
						<td><input type="text" class="form-control number" <?=$disabel?> id="nominal_piutang" placeholder="Nominal" value="" style="width:100%"></td>
						<td><input type="text" class="form-control number" <?=$disabel?> id="kuantitas_piutang" placeholder="Qty" value="" style="width:100%"></td>
						<td><input type="text" readonly class="form-control number" <?=$disabel?> id="totalkeseluruhan_piutang" placeholder="Total" value="" style="width:100%"></td>						
						<td><input type="text" class="form-control" id="keterangan_piutang" <?=$disabel?> placeholder="Keterangan" value="" style="width:100%"></td>						
						<td>
							<button title="Simpan" id="btn_simpan_piutang" type="button" class="btn btn-primary btn-sm" <?=$disabel?>><i class="fa fa-save"></i></button>
							<button title="Simpan" id="btn_clear" type="button" class="btn btn-warning btn-sm" <?=$disabel?>><i class="fa fa-refresh"></i></button>
						</td>
						
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
	<div class="block">
		<div class="block-header">		
			<h4 ><?=text_primary('Transaksi Dari Permintaan Barang')?></h4>
		</div>
		<div class="block-content">
			<input type="hidden" class="form-control" id="tedit_permintaan" placeholder="No. Rek" value="">
			<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
			<table id="tabel_permintaan" class="table table-striped table-bordered" style="margin-bottom: 0;">
				<thead>
					<tr>
						<th></th>
						<th>No</th>
						<th width="40%">Nama Transaksi</th>
						<th>Nominal</th>
						<th>Dari</th>
						<th>Keterangan</th>
						<th>Aksi</th>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							<div class="input-group" style="width: 110%;">
								<input class="form-control" readonly type="text" id="nama_transaksi_permintaan" name="nama_transaksi_permintaan">
								<span class="input-group-btn">
									<button class="btn btn-success" <?=$disabel?> id="btn_show_modal_permintaan" type="button"><i class="fa fa-search-plus"></i></button>
								</span>
							</div>

						</td>
						<td><input type="text" class="form-control number" readonly <?=$disabel?> id="nominal_permintaan" placeholder="Nominal" value="" style="width:100%"></td>
						<td><input type="text" class="form-control" readonly <?=$disabel?> id="dari_permintaan" placeholder="Dari" value="" style="width:100%"></td>
						<td><input type="text" class="form-control" id="keterangan_permintaan" <?=$disabel?> placeholder="Keterangan" value="" style="width:100%"></td>						
						<td>
							<input type="hidden" class="form-control" id="tpermintaan_id" <?=$disabel?> placeholder="tpermintaan_id" value="" style="width:100%">					
							<button title="Simpan" id="btn_simpan_permintaan" type="button" class="btn btn-primary btn-sm" <?=$disabel?>><i class="fa fa-save"></i></button>
							<button title="Simpan" id="btn_clear_permintaan" type="button" class="btn btn-warning btn-sm" <?=$disabel?>><i class="fa fa-refresh"></i></button>
						</td>
						
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
<?}?>
<?$this->load->view('Tpengelolaan/mod_cari_pendapatan')?>
<?$this->load->view('Tpengelolaan/mod_cari_permintaan')?>
<div id="cover-spin"></div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".number").number(true,0,'.',',');
		// $("#modal_pendapatan").modal();
		load_hutang();	
		// clear_hutang();
		load_piutang();	
		// clear_piutang();		
		// clear_pendapatan();		
		load_pendapatan();	
		load_permintaan();	
		get_info();
	})	
	function get_info(){
		$id=$("#tpengelolaan_id").val();
		$.ajax({
			url: '{site_url}tpengelolaan/get_info/'+$id,
			dataType: "json",
			success: function(data) {
				console.log(data.total_hutang);
				$("#total_hutang").val(data.total_hutang);
				$("#total_piutang").val(data.total_piutang);
				$("#total_bayar").val(data.total_bayar);
				$("#total_terima").val(data.total_terima);
				$("#keterangan_all").val(data.keterangan);
					// $("#tbendahara_pendapatan_id").val(data.detail.tbendahara_pendapatan_id);
					// $("#keterangan_pendapatan").val(data.detail.ket);
					// $("#nominal_pendapatan").val(data.detail.nominal);
					// $("#dari_pendapatan").val(data.detail.terimadari_nama);
			}
		});
	}	
	
	function validate_final()
	{
		if ($("#nama").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Rekapan", "error");
			return false;
		}
		if ($("#st_harga_jual").val()=='#'){
			sweetAlert("Maaf...", "Tentukan Jenis Pendapatan", "error");
			return false;
		}
		
		
		$("*[disabled]").not(true).removeAttr("disabled");
		$("#simpan_tko").hide();
		$("#cover-spin").show();
		return true;
	}
	
	
	
	//Hutang
	function load_hutang() {
		// alert('sini');
		var tpengelolaan_id=$("#tpengelolaan_id").val();		
		var disabel=$("#disabel").val();		
		$('#tabel_hutang').DataTable().destroy();
		var table = $('#tabel_hutang').DataTable({
		"pageLength": 50,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tpengelolaan/load_hutang/',
			type: "POST",
			dataType: 'json',
			data: {
				tpengelolaan_id: tpengelolaan_id,				
				disabel: disabel,				
			}
		},
		columnDefs: [
						{  "visible":false, targets:[0] },
						{  className: "text-right", targets:[3,4,5] },
						{  className: "text-center", targets:[7] },
						{ "width": "5%", "targets": [1] },
						{ "width": "9%", "targets": [4] },
						{ "width": "12%", "targets": [3,5,7] },
						{ "width": "18%", "targets": [2,6] },
				],
		});
	}
	
	$(document).on("click",".edit_hutang",function(){	
		var table = $('#tabel_hutang').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		// var id=table.cell(tr,0).data();
		var tedit_hutang=table.cell(tr,0).data();
		$("#tedit_hutang").val(tedit_hutang);
			get_data_hutang(tedit_hutang);					
	});
	$("#mdata_hutang_id").change(function(){	
		if ($("#tedit_hutang").val()=='' && $(this).val()!='#'){
			// alert('LOAD DEFAULT');
			get_default_hutang($(this).val());					
		}
	});
	function get_default_hutang($id){
		$.ajax({
			url: '{site_url}mdata_pengelolaan/get_data_hutang/'+$id,
			dataType: "json",
			success: function(data) {
				$("#nominal_hutang").val(data.detail.nominal_default);
				$("#kuantitas_hutang").val(1);
				$("#totalkeseluruhan_hutang").val(data.detail.nominal_default);
			}
		});

	}
	function get_data_hutang($id){
		$.ajax({
			url: '{site_url}tpengelolaan/get_data_hutang/'+$id,
			dataType: "json",
			success: function(data) {
				$("#mdata_hutang_id").val(data.detail.mdata_hutang_id).trigger('change');
				$("#nominal_hutang").val(data.detail.nominal);
				$("#kuantitas_hutang").val(data.detail.kuantitas);
				$("#totalkeseluruhan_hutang").val(data.detail.totalkeseluruhan);
				$("#keterangan_hutang").val(data.detail.keterangan);
			}
		});
	}	
	$(document).on("click","#btn_clear",function(){	
		clear_hutang();
	});
	
	$(document).on("click",".hapus_hutang",function(){	
		var table = $('#tabel_hutang').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var id=table.cell(tr,0).data();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Hapus Record ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			
				$.ajax({
					url: '{site_url}tpengelolaan/hapus_hutang',
					type: 'POST',
					data: {id: id},
					complete: function() {					
						swal({
							title: "Berhasil!",
							text: "Hutang Berhasil Dihapus.",
							type: "success",
							timer: 500,
							showConfirmButton: false
						});
						get_info();
						$('#tabel_hutang').DataTable().ajax.reload(null,false)
					}
				});
		});

	
	});
	$(document).on("click","#btn_simpan_hutang",function(){	
		simpan_hutang();	
	});
	function simpan_hutang(){
		if (validate_hutang()==false){return false}
			$("#cover-spin").show();
			var tpengelolaan_id=$("#tpengelolaan_id").val();
			var tedit_hutang=$("#tedit_hutang").val();
			var mdata_hutang_id=$("#mdata_hutang_id").val();
			var nominal=$("#nominal_hutang").val();
			var kuantitas=$("#kuantitas_hutang").val();
			var totalkeseluruhan=$("#totalkeseluruhan_hutang").val();
			var keterangan=$("#keterangan_hutang").val();
			var nama_transaksi=$("#mdata_hutang_id option:selected").text();	
			// alert(tedit_hutang);
			$.ajax({
				url: '{site_url}tpengelolaan/simpan_hutang',
				type: 'POST',
				data: {
					tpengelolaan_id: tpengelolaan_id,
					tedit_hutang: tedit_hutang,
					mdata_hutang_id: mdata_hutang_id,
					nama_transaksi: nama_transaksi,
					nominal: nominal,				
					kuantitas: kuantitas,				
					totalkeseluruhan: totalkeseluruhan,				
					keterangan: keterangan,				
				},
				complete: function() {					
					swal({
						title: "Berhasil!",
						text: "Hutang Berhasil ditambah.",
						type: "success",
						timer: 500,
						showConfirmButton: false
					});
					$("#cover-spin").hide();
					clear_hutang();
					$('#tabel_hutang').DataTable().ajax.reload(null,false)
				}
			});
		
	}
	function clear_hutang()
	{
		$("#tedit_hutang").val('');
		$("#mdata_hutang_id").val('#').trigger('change');
		$("#nominal_hutang").val('0');
		$("#kuantitas_hutang").val('0');
		$("#totalkeseluruhan_hutang").val('0');
		$("#keterangan_hutang").val('');
		get_info()
	}
	function validate_hutang()
	{
		if ($("#mdata_hutang_id").val()=='#'){
			sweetAlert("Maaf...", "Nama Transaksi Harus diisi", "error");
			return false;
		}		
		if ($("#totalkeseluruhan_hutang").val()=='0' || $("#totalkeseluruhan_hutang").val()==''){
			sweetAlert("Maaf...", "Nominal Default Harus diisi", "error");
			return false;
		}	
		if ($("#kuantitas_hutang").val()=='0' || $(kuantitas_hutang).val()==''){
			sweetAlert("Maaf...", "Nominal Default Harus diisi", "error");
			return false;
		}	
		return true;
	}
	$("#kuantitas_hutang,#nominal_hutang").keyup(function(){	
		if ($(this).val()=='0' || $(this).val()==''){
			$(this).val(1);
		}
		var total=parseFloat($("#kuantitas_hutang").val()) * parseFloat($("#nominal_hutang").val());
		$("#totalkeseluruhan_hutang").val(total)
	});
	
	//Piutang
	function load_piutang() {
		// alert('sini');
		var tpengelolaan_id=$("#tpengelolaan_id").val();		
		var disabel=$("#disabel").val();		
		$('#tabel_piutang').DataTable().destroy();
		var table = $('#tabel_piutang').DataTable({
		"pageLength": 50,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tpengelolaan/load_piutang/',
			type: "POST",
			dataType: 'json',
			data: {
				tpengelolaan_id: tpengelolaan_id,				
				disabel: disabel,				
			}
		},
		columnDefs: [
						{  "visible":false, targets:[0] },
						{  className: "text-right", targets:[3,4,5] },
						{  className: "text-center", targets:[7] },
						{ "width": "5%", "targets": [1] },
						{ "width": "9%", "targets": [4] },
						{ "width": "12%", "targets": [3,5,7] },
						{ "width": "18%", "targets": [2,6] },
				],
		});
	}
	
	$(document).on("click",".edit_piutang",function(){	
		var table = $('#tabel_piutang').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		// var id=table.cell(tr,0).data();
		var tedit_piutang=table.cell(tr,0).data();
		$("#tedit_piutang").val(tedit_piutang);
			get_data_piutang(tedit_piutang);					
	});
	$("#mdata_piutang_id").change(function(){	
		if ($("#tedit_piutang").val()=='' && $(this).val()!='#'){
			get_default_piutang($(this).val());					
		}
	});
	function get_default_piutang($id){
		$.ajax({
			url: '{site_url}mdata_pengelolaan/get_data_hutang/'+$id,
			dataType: "json",
			success: function(data) {
				$("#nominal_piutang").val(data.detail.nominal_default);
				$("#kuantitas_piutang").val(1);
				$("#totalkeseluruhan_piutang").val(data.detail.nominal_default);
			}
		});

	}
	function get_data_piutang($id){
		$.ajax({
			url: '{site_url}tpengelolaan/get_data_piutang/'+$id,
			dataType: "json",
			success: function(data) {
				// alert(data.detail.mdata_piutang_id);
				$("#mdata_piutang_id").val(data.detail.mdata_piutang_id).trigger('change');
				$("#nominal_piutang").val(data.detail.nominal);
				$("#kuantitas_piutang").val(data.detail.kuantitas);
				$("#totalkeseluruhan_piutang").val(data.detail.totalkeseluruhan);
				$("#keterangan_piutang").val(data.detail.keterangan);
			}
		});
	}	
	$(document).on("click","#btn_clear",function(){	
		clear_piutang();
	});
	
	$(document).on("click",".hapus_piutang",function(){	
		var table = $('#tabel_piutang').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var id=table.cell(tr,0).data();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Hapus Record ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			
				$.ajax({
					url: '{site_url}tpengelolaan/hapus_piutang',
					type: 'POST',
					data: {id: id},
					complete: function() {					
						swal({
							title: "Berhasil!",
							text: "Hutang Berhasil Dihapus.",
							type: "success",
							timer: 500,
							showConfirmButton: false
						});
						get_info();
						$('#tabel_piutang').DataTable().ajax.reload(null,false)
					}
				});
		});

	
	});
	$(document).on("click","#btn_simpan_piutang",function(){	
		simpan_piutang();	
	});
	function simpan_piutang(){
		if (validate_piutang()==false){return false}
			$("#cover-spin").show();
			var tpengelolaan_id=$("#tpengelolaan_id").val();
			var tedit_piutang=$("#tedit_piutang").val();
			var mdata_piutang_id=$("#mdata_piutang_id").val();
			var nominal=$("#nominal_piutang").val();
			var kuantitas=$("#kuantitas_piutang").val();
			var totalkeseluruhan=$("#totalkeseluruhan_piutang").val();
			var keterangan=$("#keterangan_piutang").val();
			var nama_transaksi=$("#mdata_piutang_id option:selected").text();	
			// alert(tedit_piutang);
			$.ajax({
				url: '{site_url}tpengelolaan/simpan_piutang',
				type: 'POST',
				data: {
					tpengelolaan_id: tpengelolaan_id,
					tedit_piutang: tedit_piutang,
					mdata_piutang_id: mdata_piutang_id,
					nama_transaksi: nama_transaksi,
					nominal: nominal,				
					kuantitas: kuantitas,				
					totalkeseluruhan: totalkeseluruhan,				
					keterangan: keterangan,				
				},
				complete: function() {					
					swal({
						title: "Berhasil!",
						text: "Piuttang Berhasil ditambah.",
						type: "success",
						timer: 500,
						showConfirmButton: false
					});
					$("#cover-spin").hide();
					clear_piutang();
					$('#tabel_piutang').DataTable().ajax.reload(null,false)
				}
			});
		
	}
	function clear_piutang()
	{
		$("#tedit_piutang").val('');
		$("#mdata_piutang_id").val('#').trigger('change');
		$("#nominal_piutang").val('0');
		$("#kuantitas_piutang").val('0');
		$("#totalkeseluruhan_piutang").val('0');
		$("#keterangan_piutang").val('');
		get_info()
	}
	function validate_piutang()
	{
		if ($("#mdata_piutang_id").val()=='#'){
			sweetAlert("Maaf...", "Nama Transaksi Harus diisi", "error");
			return false;
		}		
		if ($("#totalkeseluruhan_piutang").val()=='0' || $("#totalkeseluruhan_piutang").val()==''){
			sweetAlert("Maaf...", "Nominal Default Harus diisi", "error");
			return false;
		}	
		if ($("#kuantitas_piutang").val()=='0' || $(kuantitas_piutang).val()==''){
			sweetAlert("Maaf...", "Nominal Default Harus diisi", "error");
			return false;
		}	
		return true;
	}
	$("#kuantitas_piutang,#nominal_piutang").keyup(function(){	
		if ($(this).val()=='0' || $(this).val()==''){
			$(this).val(1);
		}
		var total=parseFloat($("#kuantitas_piutang").val()) * parseFloat($("#nominal_piutang").val());
		$("#totalkeseluruhan_piutang").val(total)
	});
	
	
	$("#btn_show_modal_pendapatan").click(function(){	
		load_cari_pendapatan();
		$("#modal_pendapatan").modal('show');
	});
	$("#btn_simpan_pendapatan").click(function(){	
		simpan_pendapatan();
	});
	$("#btn_clear_pendapatan").click(function(){	
		clear_pendapatan();
	});
	function simpan_pendapatan(){
		if (validate_pendapatan()==false){return false}
			$("#cover-spin").show();
			var tpengelolaan_id=$("#tpengelolaan_id").val();
			var tedit_pendapatan=$("#tedit_pendapatan").val();
			var keterangan=$("#keterangan_pendapatan").val();
			var nama_transaksi=$("#nama_transaksi_pendapatan").val();	
			var tbendahara_pendapatan_id=$("#tbendahara_pendapatan_id").val();	
			// alert(tedit_piutang);
			$.ajax({
				url: '{site_url}tpengelolaan/simpan_pendapatan',
				type: 'POST',
				data: {
					tpengelolaan_id: tpengelolaan_id,
					tbendahara_pendapatan_id: tbendahara_pendapatan_id,
					tedit_pendapatan: tedit_pendapatan,
					nama_transaksi: nama_transaksi,
					keterangan: keterangan,				
				},
				complete: function() {					
					swal({
						title: "Berhasil!",
						text: "Variable Berhasil ditambah.",
						type: "success",
						timer: 500,
						showConfirmButton: false
					});
					$("#cover-spin").hide();
					clear_pendapatan();
					$('#tabel_pendapatan').DataTable().ajax.reload(null,false)
				}
			});
		
	}
	function validate_pendapatan()
	{
		if ($("#nama_transaksi_pendapatan").val()==''){
			sweetAlert("Maaf...", "Transaksi Harus diisi", "error");
			return false;
		}		
		
		return true;
	}
	function clear_pendapatan()
	{
		$("#tedit_pendapatan").val('');
		$("#tbendahara_pendapatan_id").val('');
		$("#nama_transaksi_pendapatan").val('');
		$("#keterangan_pendapatan").val('');
		$("#nominal_pendapatan").val('');
		$("#dari_pendapatan").val('');
		if ($("#disabel").val()==''){
			$('#btn_show_modal_pendapatan').attr('disabled',false);
			
		}
		get_info()
		
	}
	//Pendapatan
	function load_pendapatan() {
		// alert('sini');
		var tpengelolaan_id=$("#tpengelolaan_id").val();		
		var disabel=$("#disabel").val();		
		$('#tabel_pendapatan').DataTable().destroy();
		var table = $('#tabel_pendapatan').DataTable({
		"pageLength": 50,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tpengelolaan/load_pendapatan/',
			type: "POST",
			dataType: 'json',
			data: {
				tpengelolaan_id: tpengelolaan_id,				
				disabel: disabel,				
			}
		},
		columnDefs: [
						{  "visible":false, targets:[0] },
						{  className: "text-right", targets:[1,3] },
						{  className: "text-center", targets:[4,5,6] },
						{ "width": "5%", "targets": [1] },
						{ "width": "10%", "targets": [3,6] },
						{ "width": "40%", "targets": [2] },
						{ "width": "15%", "targets": [4,5] },
				],
		});
	}
	$(document).on("click",".hapus_pendapatan",function(){	
		var table = $('#tabel_pendapatan').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var id=table.cell(tr,0).data();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Hapus Record ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			
				$.ajax({
					url: '{site_url}tpengelolaan/hapus_pendapatan',
					type: 'POST',
					data: {id: id},
					complete: function() {					
						swal({
							title: "Berhasil!",
							text: "Hutang Berhasil Dihapus.",
							type: "success",
							timer: 500,
							showConfirmButton: false
						});
						get_info();
						$('#tabel_pendapatan').DataTable().ajax.reload(null,false)
					}
				});
		});

	
	});
	$(document).on("click",".edit_pendapatan",function(){	
		var table = $('#tabel_pendapatan').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		// var id=table.cell(tr,0).data();
		var tedit_pendapatan=table.cell(tr,0).data();
		$("#tedit_pendapatan").val(tedit_pendapatan);
			get_data_pendapatan(tedit_pendapatan);		
		$('#btn_show_modal_pendapatan').attr('disabled',true);
	});
	function get_data_pendapatan($id){
		$.ajax({
			url: '{site_url}tpengelolaan/get_edit_pendapatan_lain/'+$id,
			dataType: "json",
			success: function(data) {
				$("#nama_transaksi_pendapatan").val(data.detail.nama_transaksi);
					$("#tbendahara_pendapatan_id").val(data.detail.tbendahara_pendapatan_id);
					$("#keterangan_pendapatan").val(data.detail.ket);
					$("#nominal_pendapatan").val(data.detail.nominal);
					$("#dari_pendapatan").val(data.detail.terimadari_nama);
			}
		});
	}	
	
	//PEERMINTAAN
	
	$("#btn_show_modal_permintaan").click(function(){	
		// load_cari_permintaan();
		$("#modal_permintaan").modal('show');
	});
	$("#btn_simpan_permintaan").click(function(){	
		simpan_permintaan();
	});
	$("#btn_clear_permintaan").click(function(){	
		clear_permintaan();
	});
	function simpan_permintaan(){
		if (validate_permintaan()==false){return false}
			$("#cover-spin").show();
			var st_harga_jual=$("#st_harga_jual").val();
			var tpengelolaan_id=$("#tpengelolaan_id").val();
			var tedit_permintaan=$("#tedit_permintaan").val();
			var keterangan=$("#keterangan_permintaan").val();
			var nama_transaksi=$("#nama_transaksi_permintaan").val();	
			var tpermintaan_id=$("#tpermintaan_id").val();	
			// alert(tedit_piutang);
			$.ajax({
				url: '{site_url}tpengelolaan/simpan_permintaan',
				type: 'POST',
				data: {
					tpengelolaan_id: tpengelolaan_id,
					tpermintaan_id: tpermintaan_id,
					tedit_permintaan: tedit_permintaan,
					nama_transaksi: nama_transaksi,
					keterangan: keterangan,				
					st_harga_jual: st_harga_jual,				
				},
				complete: function() {					
					swal({
						title: "Berhasil!",
						text: "Variable Berhasil ditambah.",
						type: "success",
						timer: 500,
						showConfirmButton: false
					});
					$("#cover-spin").hide();
					clear_permintaan();
					$('#tabel_permintaan').DataTable().ajax.reload(null,false)
				}
			});
		
	}
	function validate_permintaan()
	{
		if ($("#nama_transaksi_permintaan").val()==''){
			sweetAlert("Maaf...", "Transaksi Harus diisi", "error");
			return false;
		}		
		
		return true;
	}
	function clear_permintaan()
	{
		$("#tedit_permintaan").val('');
		$("#tpermintaan_id").val('');
		$("#nama_transaksi_permintaan").val('');
		$("#keterangan_permintaan").val('');
		$("#nominal_permintaan").val('');
		$("#dari_permintaan").val('');
		if ($("#disabel").val()==''){
		$('#btn_show_modal_permintaan').attr('disabled',false);
		get_info()
		}
		
	}
	//Pendapatan
	function load_permintaan() {
		// alert('sini');
		var tpengelolaan_id=$("#tpengelolaan_id").val();		
		var disabel=$("#disabel").val();		
		$('#tabel_permintaan').DataTable().destroy();
		var table = $('#tabel_permintaan').DataTable({
		"pageLength": 50,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tpengelolaan/load_permintaan/',
			type: "POST",
			dataType: 'json',
			data: {
				tpengelolaan_id: tpengelolaan_id,				
				disabel: disabel,				
			}
		},
		columnDefs: [
						{  "visible":false, targets:[0] },
						{  className: "text-right", targets:[1,3] },
						{  className: "text-center", targets:[4,5,6] },
						{ "width": "5%", "targets": [1] },
						{ "width": "10%", "targets": [3,6] },
						{ "width": "40%", "targets": [2] },
						{ "width": "15%", "targets": [4,5] },
				],
		});
	}
	$(document).on("click",".hapus_permintaan",function(){	
		var table = $('#tabel_permintaan').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var id=table.cell(tr,0).data();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Hapus Record ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			
				$.ajax({
					url: '{site_url}tpengelolaan/hapus_permintaan',
					type: 'POST',
					data: {id: id},
					complete: function() {					
						swal({
							title: "Berhasil!",
							text: "Hutang Berhasil Dihapus.",
							type: "success",
							timer: 500,
							showConfirmButton: false
						});
						get_info();
						$('#tabel_permintaan').DataTable().ajax.reload(null,false)
					}
				});
		});

	
	});
	$(document).on("click",".edit_permintaan",function(){	
		var table = $('#tabel_permintaan').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		// var id=table.cell(tr,0).data();
		var tedit_permintaan=table.cell(tr,0).data();
		$("#tedit_permintaan").val(tedit_permintaan);
			get_data_permintaan(tedit_permintaan);		
		$('#btn_show_modal_permintaan').attr('disabled',true);
	});
	function get_data_permintaan($id){
		$.ajax({
			url: '{site_url}tpengelolaan/get_edit_permintaan/'+$id,
			dataType: "json",
			success: function(data) {
				$("#nama_transaksi_permintaan").val(data.detail.nama_transaksi);
					$("#tpermintaan_id").val(data.detail.tpermintaan_id);
					$("#keterangan_permintaan").val(data.detail.ket);
					$("#nominal_permintaan").val(data.detail.nominal);
					$("#dari_permintaan").val(data.detail.dari_nama);
			}
		});
	}
</script>