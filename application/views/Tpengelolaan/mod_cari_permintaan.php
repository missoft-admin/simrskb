<div class="modal fade in black-overlay" id="modal_permintaan" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 65%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Piutang dari Transaksi Permintaan Unit</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm">No Transaksi</label>
									<div class="col-md-9">
										<input type="text" style="width: 100%"  class="form-control" id="xnopermintaan" placeholder="No Transaksi" name="xnopermintaan" value="">									
									</select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm">Dari Unit</label>
									<div class="col-md-9">
										<select name="xdari_id" style="width: 100%" id="xdari_id" class="js-select2 form-control input-sm">										
											<option value="#" selected>- ALL -</option>		
											<?foreach(get_all('munitpelayanan') as $row) {?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>		
											
											<?}?>
										</select>
									</div>
								</div>
								
								
								
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 10px;">
									<label class="col-md-3 control-label" for="xke_id">Ke Unit</label>
									<div class="col-md-9">
										<select name="xke_id" style="width: 100%" id="xke_id" class="js-select2 form-control input-sm">										
											<option value="#" selected>- ALL -</option>		
											<?foreach(get_all('munitpelayanan') as $row) {?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>		
											
											<?}?>
										</select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 10px;">
									<label class="col-md-3 control-label" for="xke_id">Tanggal</label>
									<div class="col-md-9">
										<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="form-control" type="text" id="xtgl1_permintaan" name="xtgl1" placeholder="From" value="{xtgl1}"/>
											<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
											<input class="form-control" type="text" id="xtgl2_permintaan" name="xtgl2" placeholder="To" value="{xtgl2}"/>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="btn_cari_permintaan"></label>
									<div class="col-md-9">
										<button class="btn btn-sm btn-success" id="btn_cari_permintaan" type="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-search"></i> Cari</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<table id="tabel_cari_permintaan" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 25%;" hidden></th>
								<th style="width: 5%;">X</th>
								<th style="width: 10%;">No Transaksi</th>
								<th style="width: 20%;">Tanggal</th>
								<th style="width: 10%;">Dari Unit</th>
								<th style="width: 10%;">Ke Unit</th>
								<th style="width: 10%;">Nominal</th>
								<th style="width: 20%;">ACTION</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">				
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Keluar</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
var table_permintaan;
	$(document).ready(function() {
		// $('#modal_permintaan').modal('show');
		// load_cari_permintaan();
	});
	$(document).on('click', '#btn_cari_permintaan', function() {
		load_cari_permintaan();
	});
	// $(document).on('click', '.pilih_permintaan', function() {
		
	// });
	function pilih_permintaan($id){
		// alert($id);
		var st_harga_jual=$("#st_harga_jual").val();
		$.ajax({
			url: '{site_url}tpengelolaan/get_data_permintaan/'+$id,
				dataType: "json",
				success: function(data) {
					$("#nama_transaksi_permintaan").val(data.detail.nopermintaan+'-'+data.detail.dari_nama);
					$("#tpermintaan_id").val(data.detail.id);
					$("#keterangan_permintaan").val('');
					if (st_harga_jual=='1'){
						$("#nominal_permintaan").val(data.detail.total_dasar);						
					}else{
						$("#nominal_permintaan").val(data.detail.total_margin);						
					}
					$("#dari_permintaan").val(data.detail.dari_nama);
					$("#tedit_permintaan").val('');
				}
			});
		$("#modal_permintaan").modal('hide');
	}
	function load_cari_permintaan(){
		var st_harga_jual=$("#st_harga_jual").val();
		var xnopermintaan=$("#xnopermintaan").val();
		var xdari_id=$("#xdari_id").val();
		var xke_id=$("#xke_id").val();
		var xtgl1=$("#xtgl1_permintaan").val();
		// alert(xtgl1);
		var xtgl2=$("#xtgl2_permintaan").val();
		$('#tabel_cari_permintaan').DataTable().destroy();
		table_permintaan = $('#tabel_cari_permintaan').DataTable({
            autoWidth: false,
            searching: false,
            "lengthChange": false,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			
			columnDefs: [
							{ "targets": [0], "visible": false },
							{ "width": "3%", "targets": [1] },
							{ "width": "8%", "targets": [3,6] },
							{ "width": "10%", "targets": [2,,4,5] },
							{ "width": "12%", "targets": [7] },
							{"targets": [6], className: "text-right" },
							{"targets": [2,3,4,5,7], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tpengelolaan/load_cari_permintaan', 
                type: "POST" ,
                dataType: 'json',
				data : {
						st_harga_jual:st_harga_jual,
						xnopermintaan:xnopermintaan
						,xdari_id:xdari_id
						,xke_id:xke_id
						,xtgl1:xtgl1
						,xtgl2:xtgl2
					   }
            }
        });
	}
	
	
	

</script>
