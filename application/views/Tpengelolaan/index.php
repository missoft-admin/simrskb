<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<?php if (UserAccesForm($user_acces_form,array('249'))): ?>
		<hr style="margin-top:0px">
		 <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
		<div class="row">
			<div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien">Nama</label>
					<div class="col-md-8">
						<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="asalpasien">Tanggal Hitung </label>
					<div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy" style="width: 100%;">
                            <input class="form-control" type="text" id="tanggal_trx1" name="tanggal_trx1" placeholder="From" value="{tanggal_trx1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_trx2" name="tanggal_trx2" placeholder="To" value="{tanggal_trx2}"/>
                        </div>
					</div>
				</div>
				
			</div>
			<div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="asalpasien">Status </label>
					<div class="col-md-8">
						<select name="status" id="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="1" <?=($status == 1 ? 'selected="selected"':'')?>>Aktif</option>
							<option value="0" <?=($status == 0 ? 'selected="selected"':'')?>>Tidak Aktif</option>
							<option value="#" <?=($status =='#' ? 'selected="selected"':'')?>>- All -</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien"></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="button" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				
			</div>
			
		</div>
		<?php echo form_close() ?>
		<hr>
		<?php endif ?>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="index_list">
			<thead>
				<tr>
					<th>#</th>
					<th>No</th>
					<th>No Pengelolaan</th>
					<th>Nama Pengelolaan</th>
					<th>Tanggal Hitung</th>
					<th>Deskripsi</th>
					<th>Total Nominal</th>
					<th>Status	</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_approval" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<div class="form-horizontal">
						<div class="row">
							<input type="hidden" class="form-control" id="tpengelolaan_id" placeholder="" name="tpengelolaan_id" value="">
							<div class="col-md-12">
								<table width="100%" id="tabel_user" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Step</th>
											<th>User</th>
											<th>Jika Setuju</th>
											<th>Jika Menolak</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-primary" id="btn_simpan_approval" type="button" data-dismiss="modal"><i
						class="fa fa-check"></i> Proses Persetujuan</button>
			</div>

		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
$(document).ready(function(){
	// 	
	load_index();
   $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
	  table.destroy();
		load_index();
    }
  });
});
function kirim($id) {
	var id = $id;
	$("#modal_approval").modal('show');
	$('#btn_simpan_approval').attr('disabled', true);
	load_user_approval(id);
}

function load_user_approval($id) {
	var id = $id;
	$("#tpengelolaan_id").val(id);
	$('#tabel_user').DataTable().destroy();
	table = $('#tabel_user').DataTable({
		"autoWidth": false,
		"pageLength": 10,
		"searching": false,
		"lengthChange": false,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"order": [],
		"ajax": {
			url: '{site_url}tpengelolaan/load_user_approval',
			type: "POST",
			dataType: 'json',
			data: {
				id: id,
			}
		},
		"columnDefs": [{
				className: "text-right",
				targets: [0]
			},
			{
				className: "text-center",
				targets: [1, 2, 3]
			},
			{
				"width": "10%",
				"targets": [0]
			},
			{
				"width": "40%",
				"targets": [1]
			},
			{
				"width": "25%",
				"targets": [2, 3]
			},

		],
		"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			if (aData[1]) {
				$('#btn_simpan_approval').attr('disabled', false);
			}
		},

	});
}
$(document).on("click", "#btn_simpan_approval", function () {
		var id = $("#tpengelolaan_id").val();
		swal({
			title: "Anda Yakin ?",
			text: "Akan Kirim Ke User Persetujuan?",
			type: "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function () {
			table = $('#index_list').DataTable()
			$.ajax({
				url: '{site_url}tpengelolaan/simpan_proses_peretujuan/' + id,
				complete: function (data) {
					$.toaster({
						priority: 'success',
						title: 'Succes!',
						message: ' Kirim Data'
					});
					table.ajax.reload(null, false);
				}
			});
		});
	});
$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_index();
});

function load_index(){
	
	
	var status=$("#status").val();
	var nama=$("#nama").val();
	
	
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [
						{  "visible":false, targets:[0] },
						{  className: "text-right", targets:[1,6] },
						{  className: "text-right", targets:[1,6] },
						{  className: "text-center", targets:[2,3,4,5,7,8] },
						{ "width": "5%", "targets": [1] },
						{ "width": "10%", "targets": [2,4,6,7] },
						{ "width": "15%", "targets": [3,5,8] },
						// { "width": "20%", "targets": [4] }
				],
            ajax: { 
                url: '{site_url}tpengelolaan/getIndex', 
                type: "POST" ,
                dataType: 'json',
				data : {
						status:status,
						nama:nama
					   }
            }
        });
}

$(document).on("click", ".removeData", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,0).data()
	
	swal({
		title: "Anda Yakin ?",
		text : "Akan Menghapus ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		removeData(id);
	});
});
function removeData($id){
	console.log($id);
	var id=$id;		

	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tpengelolaan/delete/'+id,
		complete: function(data) {
			$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
			table.ajax.reload( null, false ); 
		}
	});
}
$(document).on("click", ".aktifData", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,0).data()
	
	swal({
		title: "Anda Yakin ?",
		text : "Akan Mengaktifkan Kembali ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		aktifkan(id);
	});
});
function aktifkan($id){
	console.log($id);
	var id=$id;		

	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tpengelolaan/aktifkan/'+id,
		complete: function(data) {
			$.toaster({priority : 'success', title : 'Succes!', message : ' Aktif Kembali'});
			table.ajax.reload( null, false ); 
		}
	});
}
function verifikasi($id){
	
	var id=$id;		

	table = $('#index_list').DataTable()	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Verifikasi ini ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}tpengelolaan/verifikasi/'+id,
			complete: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi Berhasil'});
				table.ajax.reload( null, false ); 
			}
		});
	});
	

	
}
</script>
