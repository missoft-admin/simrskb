<div class="modal fade in black-overlay" id="modal_pendapatan" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 65%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Hutang dari Transaksi Pendapatan Lain-Lain</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm">No Transaksi</label>
									<div class="col-md-9">
										<input type="text" style="width: 100%"  class="form-control" id="xnotransaksi" placeholder="No Transaksi" name="xnotransaksi" value="">									
									</select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm">Tipe Pendapatan</label>
									<div class="col-md-9">
										<select name="xidpendapatan" style="width: 100%" id="xidpendapatan" class="js-select2 form-control input-sm">										
											<option value="#" selected>- ALL -</option>		
											<?foreach(get_all('mpendapatan',array('status'=>1)) as $row) {?>
											<option value="<?=$row->id?>"><?=$row->keterangan?></option>		
											
											<?}?>
										</select>
									</div>
								</div>
								
								
								
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 10px;">
									<label class="col-md-3 control-label" for="xterima_dari">Terima Dari</label>
									<div class="col-md-9">
										<select name="xterima_dari" style="width: 100%" id="xterima_dari" class="js-select2 form-control input-sm">										
											<option value="#" selected>- ALL -</option>		
											<?foreach(get_all('mdari') as $row) {?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>		
											
											<?}?>
										</select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 10px;">
									<label class="col-md-3 control-label" for="xterima_dari">Tanggal</label>
									<div class="col-md-9">
										<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="form-control" type="text" id="xtgl1" name="xtgl1" placeholder="From" value="{xtgl1}"/>
											<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
											<input class="form-control" type="text" id="xtgl2" name="xtgl2" placeholder="To" value="{xtgl2}"/>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="btn_cari_pendapatan"></label>
									<div class="col-md-9">
										<button class="btn btn-sm btn-success" id="btn_cari_pendapatan" type="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-search"></i> Cari</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<table id="tabel_cari_pendapatan" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 25%;" hidden></th>
								<th style="width: 5%;">X</th>
								<th style="width: 10%;">No Transaksi</th>
								<th style="width: 20%;">Tanggal</th>
								<th style="width: 10%;">Tipe Pendapatan</th>
								<th style="width: 10%;">Deskripsi</th>
								<th style="width: 10%;">Terima Dari</th>
								<th style="width: 10%;">Nominal</th>
								<th style="width: 10%;">Status</th>
								<th style="width: 20%;">ACTION</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">				
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Keluar</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
var table_pendapatan;
	$(document).ready(function() {
		// load_cari_pendapatan();
	});
	$(document).on('click', '#btn_cari_pendapatan', function() {
		load_cari_pendapatan();
	});
	// $(document).on('click', '.pilih_pendapatan', function() {
		
	// });
	function pilih_pendapatan($id){
		// alert($id);
		$.ajax({
			url: '{site_url}tpengelolaan/get_data_pendapatan_lain/'+$id,
				dataType: "json",
				success: function(data) {
					$("#nama_transaksi_pendapatan").val(data.detail.notransaksi+'-'+data.detail.tipe);
					$("#tbendahara_pendapatan_id").val(data.detail.id);
					$("#keterangan_pendapatan").val(data.detail.ket);
					$("#nominal_pendapatan").val(data.detail.nominal);
					$("#dari_pendapatan").val(data.detail.terimadari_nama);
					$("#tedit_pendapatan").val('');
				}
			});
		$("#modal_pendapatan").modal('hide');
	}
	function load_cari_pendapatan(){
		var xnotransaksi=$("#xnotransaksi").val();
		var xidpendapatan=$("#xidpendapatan").val();
		var xterima_dari=$("#xterima_dari").val();
		var xtgl1=$("#xtgl1").val();
		var xtgl2=$("#xtgl2").val();
		$('#tabel_cari_pendapatan').DataTable().destroy();
		table_pendapatan = $('#tabel_cari_pendapatan').DataTable({
            autoWidth: false,
            searching: false,
            "lengthChange": false,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			
			columnDefs: [
							{ "targets": [0,8], "visible": false },
							{ "width": "3%", "targets": [1] },
							{ "width": "8%", "targets": [2,3,7,8,9] },
							{ "width": "10%", "targets": [4,6] },
							{ "width": "12%", "targets": [5] },
							{"targets": [7], className: "text-right" },
							{"targets": [2,3,4,5,6,8,9], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tpengelolaan/load_cari_pendapatan', 
                type: "POST" ,
                dataType: 'json',
				data : {
						xnotransaksi:xnotransaksi
						,xidpendapatan:xidpendapatan
						,xterima_dari:xterima_dari
						,xtgl1:xtgl1
						,xtgl2:xtgl2
					   }
            }
        });
	}
	
	
	

</script>
