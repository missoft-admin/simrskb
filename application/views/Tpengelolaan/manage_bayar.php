<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}tpengelolaan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('tpengelolaan/save_bayar','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Nama Pengelolaan</label>
				<div class="col-md-10">
					<input type="text" class="form-control" id="nama_pengelolaan" readonly disabled placeholder="Nama" name="nama_pengelolaan" value="{nama_pengelolaan}" required="" aria-required="true">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="deskripsi">Tanggal Hitung</label>
				<div class="col-md-3">
					<div class="input-group date">
							<input class="js-datepicker form-control input" disabled readonly  type="text" id="tanggal_hitung" name="tanggal_hitung" value="{tanggal_hitung}" data-date-format="dd-mm-yyyy"/>
							<span class="input-group-addon">
								<span class="fa fa-calendar"></span>
							</span>
					</div>

				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Deskrpisi</label>
				<div class="col-md-10">
					<input type="text" class="form-control" id="deskripsi_pengelolaan" readonly disabled placeholder="Nama" name="deskripsi_pengelolaan" value="{deskripsi_pengelolaan}" required="" aria-required="true">
				</div>
			</div>
			
			
			<?php echo form_close() ?>
	</div>
</div>

<div class="block">
	
	<div class="block-content">
		<h4 ><?=text_success('Transaksi')?>  <?=text_default('Hutang')?></h4><hr>
		<input type="hidden" class="form-control" id="tedit_hutang" placeholder="No. Rek" value="">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<table id="tabel_hutang" class="table table-striped table-bordered" style="margin-bottom: 0;">
			<thead>
				<tr>
					<th></th>
					<th>No</th>
					<th>Nama Transaksi</th>
					<th>Nominal Default</th>
					<th>QTY</th>
					<th>Total</th>
					<th>Keterangan</th>
					<th>Aksi</th>
				</tr>
				
			</thead>
			<tbody>
			</tbody>
		</table>
		<h4 ><?=text_primary('Transaksi Dari Pendapatan Lain-Lain')?></h4><hr>
		<input type="hidden" class="form-control" id="tedit_pendapatan" placeholder="No. Rek" value="">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<table id="tabel_pendapatan" class="table table-striped table-bordered" style="margin-bottom: 0;">
			<thead>
				<tr>
					<th></th>
					<th>No</th>
					<th width="40%">Nama Transaksi</th>
					<th>Nominal</th>
					<th>Dari</th>
					<th>Keterangan</th>
					<th>Aksi</th>
				</tr>
				
			</thead>
			<tbody>
			</tbody>
		</table>
		<h4 ><?=text_success('Transaksi')?>  <?=text_default('Piutang')?></h4><hr>
		<input type="hidden" class="form-control" id="tedit_piutang" placeholder="No. Rek" value="">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<table id="tabel_piutang" class="table table-striped table-bordered" style="margin-bottom: 0;">
			<thead>
				<tr>
					<th></th>
					<th>No</th>
					<th>Nama Transaksi</th>
					<th>Nominal Default</th>
					<th>QTY</th>
					<th>Total</th>
					<th>Keterangan</th>
					<th>Aksi</th>
				</tr>
				
			</thead>
			<tbody>
			</tbody>
		</table>
		<h4 ><?=text_primary('Transaksi Dari Permintaan Barang')?></h4><hr>
		<input type="hidden" class="form-control" id="tedit_permintaan" placeholder="No. Rek" value="">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<table id="tabel_permintaan" class="table table-striped table-bordered" style="margin-bottom: 0;">
			<thead>
				<tr>
					<th></th>
					<th>No</th>
					<th width="40%">Nama Transaksi</th>
					<th>Nominal</th>
					<th>Dari</th>
					<th>Keterangan</th>
					<th>Aksi</th>
				</tr>
				
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	
</div>
<div class="block">
	<div class="block-header">
		
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('tpengelolaan/save_bayar','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>
					<input type="hidden" class="form-control" id="tpengelolaan_id" placeholder="ID" name="tpengelolaan_id" value="{id}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="disabel" placeholder="ID" name="disabel" value="{disabel}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="st_harga_jual" placeholder="ID" name="st_harga_jual" value="{st_harga_jual}" required="" aria-required="true">
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Total Hutang</label>
				<div class="col-md-4">
					<input type="text" class="form-control number" readonly id="total_hutang" disabled placeholder="Hutang" name="total_hutang" value="{total_hutang}" required="" aria-required="true">
				</div>
				<label class="col-md-2 control-label" for="nama">Total Bayar</label>
				<div class="col-md-4">
					<input type="text" class="form-control number" readonly disabled placeholder="Total Bayar" name="total_bayar" value="{total_bayar}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Total Piutang</label>
				<div class="col-md-4">
					<input type="text" class="form-control number" readonly id="total_piutang" disabled placeholder="Piutang" name="total_piutang" value="{total_piutang}" required="" aria-required="true">
				</div>
				<label class="col-md-2 control-label" for="nama">Total Terima</label>
				<div class="col-md-4">
					<input type="text" class="form-control number" readonly id="total_terima" disabled placeholder="Total Terima" name="total_terima" value="{total_terima}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Keterangan</label>
				<div class="col-md-10">
					<input type="text" class="form-control" readonly id="keterangan_all" disabled placeholder="Keterangan" name="keterangan_all" value="{keterangan}" required="" aria-required="true">
				</div>
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nominal">Nominal</label>
				<div class="col-md-4">
					<input type="text" disabled class="form-control number" id="nominal" placeholder="Nominal" name="nominal" value="{nominal}" required="" aria-required="true">
				</div>
				<div class="col-md-2">
					<button  type="button" <?=$disabel?> class="btn btn-danger btn_hide" type="button" id="btn_add_pembayaran" title="Add"><i class="fa fa-plus"></i> Pembayaran</button>
				</div>
			</div>
			
			<div class="form-group" style="margin-bottom: 5px;">
				
			</div>		
					
			<div class="form-group">
				<input type="hidden" class="form-control" id="rowindex">
				<input type="hidden" class="form-control" id="iddet">
				 <table class="table table-bordered" id="list_pembayaran">
					<thead>
						<tr>
							<th width="5%" class="text-center" hidden>NO</th>
							<th width="10%" class="text-center">Jenis Kas</th>
							<th width="15%" class="text-center">Sumber Kas</th>
							<th width="10%" class="text-center">Metode</th>
							<th width="10%" class="text-center">Tgl Pencairan</th>
							<th width="15%" class="text-center">Keterangan</th>
							<th width="10%" class="text-center">Nominal</th>
							<th width="15%" class="text-center">Actions</th>
							
						</tr>
					</thead>
					</thead>
					<tbody>	
						<? 
							$no=1;
							if ($list_pembayaran){
								foreach($list_pembayaran as $row){ ?>
									<tr>
									<td hidden><?=$no?></td>
									<td><?=$row->jenis_kas?></td>
									<td><?=$row->sumber_kas?></td>
									<td><?=metodePembayaran_bendahara($row->idmetode)?></td>
									<td><?=HumanDateShort($row->tanggal_pencairan)?></td>
									<td><?=$row->ket_pembayaran?></td>
									<td align="right"><?=number_format($row->nominal_bayar,0)?></td>
									<td><button type='button'  <?=$disabel?>  class='btn btn-info btn-xs pembayaran_edit' data-toggle='modal' data-target='#modal-pembayaran' title='Edit'><i class='fa fa-pencil'></i></button>&nbsp;&nbsp;<button type='button'  <?=$disabel?>  class='btn btn-danger btn-xs pembayaran_remove' title='Remove'><i class='fa fa-trash-o'></i></button></td>
									<td style='display:none'><input readonly type='text' class='form-control' name='xiddet[]' value='<?=$row->id?>'></td>
									<td style='display:none'><input readonly type='text' class='form-control' name='xjenis_kas_id[]' value='<?=$row->jenis_kas_id?>'></td>
									<td style='display:none'><input readonly type='text' class='form-control' name='xsumber_kas_id[]' value='<?=$row->sumber_kas_id?>'></td>
									<td style='display:none'><input readonly type='text' class='form-control' name='xidmetode[]' value='<?=$row->idmetode?>'></td>
									<td style='display:none'><input readonly type='text' class='form-control' name='ket[]' value='<?=$row->ket_pembayaran?>'></td>
									<td style='display:none'><input readonly type='text' class='form-control' name='xnominal_bayar[]' value='<?=$row->nominal_bayar?>'></td>
									<td style='display:none'><input readonly type='text' class='form-control' name='xstatus[]' value='1'></td>
									<td style='display:none'><input readonly type='text' class='form-control' name='xtanggal_pencairan[]' value='<?=$row->tanggal_pencairan?>'></td>
									</tr>
								<?
								$no=$no+1;
								}
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="4" class="text-right"> <strong>Total Bayar </strong></td>
							<td  colspan="2"> <input class="form-control input number" readonly type="text" id="total_bayar" name="total_bayar" value="0"/></td>
						</tr>
						<tr>
							<td colspan="4" class="text-right"> <strong>Sisa</strong></td>
							<td  colspan="2"><input class="form-control input number" readonly type="text" id="total_sisa" name="total_sisa" value="0"/> </td>
						</tr>
					</tfoot>
				</table>
			</div>
			<? if ($disabel==''){ ?>
			<div class="form-group hiden btn_hide">
				<div class="text-right bg-light lter">
					<button class="btn btn-success" type="submit"  name="btn_simpan" value="2">Simpan & Verifikasi</button>
					<button class="btn btn-primary" type="submit"  name="btn_simpan" value="1">Simpan</button>
					<a href="{base_url}tpengelolaan" class="btn btn-default" type="button">Batal</a>
				</div>
			</div>
			<?}?>
					
			
			<?php echo form_close() ?>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_pembayaran" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Pembayaran</h3>
				</div>
				
				<div class="block-content">
					<form class="form-horizontal">
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Sisa Rp. </label>
							<div class="col-md-7">
								<input readonly type="text" class="form-control number" id="sisa_modal" placeholder="Sisa" name="sisa_modal" required="" aria-required="true">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="jenis_kas_id">Jenis Kas</label>
							<div class="col-md-7">
								<select name="jenis_kas_id" id="jenis_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="#">- Pilih Opsi -</option>
									<? foreach($list_jenis_kas as $row){ ?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>
									<?}?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="sumber_kas_id">Sumber Kas</label>
							<div class="col-md-7">
								<select name="sumber_kas_id" id="sumber_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">							
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="idmetode">Metode</label>
							<div class="col-md-7">
								<select name="idmetode" id="idmetode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="#">Pilih Opsi</option>							
									<option value="1">Cheq</option>	
									<option value="2">Tunai</option>	
									<option value="4">Transfer Antar Bank</option>
									<option value="3">Transfer Beda Bank</option>							
								</select>
							</div>
						</div>
						
						
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Nominal Rp. </label>
							<div class="col-md-7">
								<input  type="hidden" class="form-control" id="sumber_kas_id_tmp" placeholder="sumber_kas_id_tmp" name="sumber_kas_id_tmp" >
								<input  type="hidden" class="form-control" id="idpembayaran" placeholder="idpembayaran" name="idpembayaran" >
								<input  type="text" class="form-control number" id="nominal_bayar" placeholder="Nominal" name="nominal_bayar" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Tanggal Pencairan</label>
							<div class="col-md-7">
								<div class="input-group date">
								<input class="js-datepicker form-control input"  type="text" id="tanggal_pencairan" name="tanggal_pencairan" value="" data-date-format="dd-mm-yyyy"/>
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Keterangan </label>
							<div class="col-md-7">
								<textarea class="form-control" name="ket_pembayaran" id="ket_pembayaran"></textarea>
							</div>
						</div>

						<div class="modal-footer">
							<button class="btn btn-sm btn-primary" id="btn_add_bayar" type="button"><i class="fa fa-check"></i> Add</button>
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Exit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="cover-spin"></div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".number").number(true,0,'.',',');
		load_hutang();	
		load_piutang();	
		load_pendapatan();	
		load_permintaan();	
		checkTotal();
	})	
	
	$(document).on("click","#btn_add_pembayaran",function(){
		// if ($("#nominal").val()=='' || $("#nominal").val()=='0'){
			// return false;
		// }
		$("#modal_pembayaran").modal('show');
		$("#total_sisa").val(parseFloat($("#nominal").val()) - parseFloat($("#total_bayar").val()));
		$("#sisa_modal").val($("#total_sisa").val());
		$("#nominal_bayar").val($("#total_sisa").val());
		
	});
	$(document).on("change","#jenis_kas_id",function(){
		// alert('soo');
		var jenis_kas_id=$("#jenis_kas_id").val();
		var sumber_kas_id=$("#sumber_kas_id_tmp").val();
		$.ajax({
			url: '{site_url}mrka_bendahara/list_sumber_kas',
			type: 'POST',
			dataType: "json",
			data: {jenis_kas_id: jenis_kas_id,sumber_kas_id: sumber_kas_id},
			success : function(data) {				
				$("#sumber_kas_id").empty();	
				console.log(data.detail);
				// $("#sumber_kas_id").append(data.detail);	
				$('#sumber_kas_id').append(data.detail);
			}
		});
	});
	$(document).on("click","#btn_add_bayar",function(){
		add_pembayaran();
	});
	function add_pembayaran(){
			
		if ($("#jenis_kas_id").val() == "#") {
			sweetAlert("Maaf...", "Jenis Kas Harus Diisi!", "error");
			$("#jenis_kas_id").focus();
			return false;
		}
		if ($("#sumber_kas_id").val() == "#") {
			sweetAlert("Maaf...", "Sumber Kas Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		if ($("#idmetode").val() == "#" || $("#idmetode").val() == "") {
			sweetAlert("Maaf...", "Metode  Harus Diisi!", "error");
			$("#idmetode").focus();
			return false;
		}
		if ($("#nominal_bayar").val() == "" || $("#nominal_bayar").val() == "0") {
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		if ($("#tanggal_pencairan").val() == "") {
			sweetAlert("Maaf...", "Tanggal Pencairan Harus Diisi!", "error");
			$("#tanggal_pencairan").focus();
			return false;
		}
		var content = "";

		var row_index;
		var duplicate = false;

		if($("#rowindex").val() != ''){
			var number = $("#number").val();
			var content = "";
			row_index = $("#rowindex").val();
		}else{
			var number = $('#list_pembayaran tr').length;
			var content = "<tr>";
			// alert($("#idmetode option:selected").text());
			$('#list_pembayaran tbody tr').filter(function (){
				var $cells = $(this).children('td');
				console.log($cells.eq(1).text());
				if($cells.eq(1).text() === $("#idmetode option:selected").text()){
					sweetAlert("Maaf...", $("#pembayaran_jenis option:selected").text() + " sudah ditambahkan.", "error");
					duplicate = true;

					pembayaran_clear();
					return false;
				}
			});
		}

		if(duplicate == false){
			var ket=$('#ket_pembayaran').summernote('code');
			content += "<td style='display:none'>" + number + "</td>";
			content += "<td>" + $("#jenis_kas_id option:selected").text() + "</td>";
			content += "<td>" + $("#sumber_kas_id option:selected").text() + "</td>";
			content += "<td>" + $("#idmetode option:selected").text() + "</td>";
			content += "<td>" + $("#tanggal_pencairan").val() + "</td>";
			content += "<td>" + ket + "</td>";
			content += "<td align='right'>" + formatNumber($("#nominal_bayar").val()) + "</td>";
			content += "<td><button type='button' class='btn btn-info btn-xs pembayaran_edit' data-toggle='modal' data-target='#modal-pembayaran' title='Edit'><i class='fa fa-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-danger btn-xs pembayaran_remove' title='Remove'><i class='fa fa-trash-o'></i></button></td>";
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xiddet[]' value='"+$("#iddet").val()+"'></td>";//8
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xjenis_kas_id[]' value='"+$("#jenis_kas_id").val()+"'></td>";//9
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xsumber_kas_id[]' value='"+$("#sumber_kas_id").val()+"'></td>";//10
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xidmetode[]' value='"+$("#idmetode").val()+"'></td>";//11
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='ket[]' value='"+ket+"'></td>";//12
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xnominal_bayar[]' value='"+$("#nominal_bayar").val()+"'></td>";//13
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xstatus[]' value='1'></td>";//14
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xtanggal_pencairan[]' value='"+$("#tanggal_pencairan").val()+"'></td>";//15
			
			if($("#rowindex").val() != ''){
				$('#list_pembayaran tbody tr:eq(' + row_index + ')').html(content);
			}else{
				content += "</tr>";
				$('#list_pembayaran tbody').append(content);
			}

			pembayaran_clear();
			checkTotal();
		}
		
		
	}
	$(document).on("click",".pembayaran_edit",function(){
		$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
		$("#modal_pembayaran").modal('show');
		$("#iddet").val($(this).closest('tr').find("td:eq(8) input").val());
		$("#sumber_kas_id_tmp").val($(this).closest('tr').find("td:eq(10) input").val());
		$("#jenis_kas_id").val($(this).closest('tr').find("td:eq(9) input").val()).trigger('change');
		$("#nominal_bayar").val($(this).closest('tr').find("td:eq(13) input").val());
		$("#ket_pembayaran").summernote('code',$(this).closest('tr').find("td:eq(12) input").val());
		$("#idmetode").val($(this).closest('tr').find("td:eq(11) input").val()).trigger('change');
		$("#tanggal_pencairan").val($(this).closest('tr').find("td:eq(4)").text());
		$("#sisa_modal").val(parseFloat($("#total_sisa").val()) + parseFloat($("#nominal_bayar").val()));
		// sisa_modal($();
		// $("#pembayaran_id").val($(this).closest('tr').find("td:eq(0)").val());

		
		return false;
	});
	$(document).on("click", ".pembayaran_remove", function() {
		var tr=$(this);
			swal({
				title: "Apakah Anda Yakin ?",
				text : "Akan Menghapus Pembayaran ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				var iddet=tr.closest('tr').find("td:eq(8) input").val()
				if (iddet==''){
					tr.closest('td').parent().remove();
						
				}else{
					tr.closest('tr').find("td:eq(14) input").val(0)
					tr.closest('tr').find("td:eq(7)").html('<span class="label label-danger">DELETED</span>')
				}
				checkTotal();
			});

			// if (confirm("Hapus data ?") == true) {
				
			// }
		// return false;
	});
	function formatNumber (num) {
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}
	function checkTotal(){
		var total_pembayaran = 0;
		$('#list_pembayaran tbody tr').each(function() {
			if ($(this).closest('tr').find("td:eq(14) input").val()=='1'){
			console.log($(this).closest('tr').find("td:eq(13) input").val());
			total_pembayaran += parseFloat($(this).find("td:eq(13) input").val());
			}
		});
		$("#total_bayar").val(total_pembayaran);
		$("#total_sisa").val(parseFloat($("#nominal").val()) - parseFloat(total_pembayaran));
	}
	function pembayaran_clear(){
		$("#sumber_kas_id_tmp").val('');
		$("#number").val('');
		$("#rowindex").val('');
		$("#modal_pembayaran").modal('hide');
		clear_input_pembayaran();
	}
	function clear_input_pembayaran(){
		$("#jenis_kas_id").val("#").trigger('change');
		$("#sumber_kas_id").val("#").trigger('change');
		$("#nominal_bayar").val(0);
		$("#sisa_modal").val(0);
		$("#idpembayaran").val('');
		$("#idmetode").val("#").trigger('change');
		$("#ket_pembayaran").summernote('code','');
		$("#iddet").val('');
	}
	function validate_final(){
		// if ($("#idpendapatan").val() == "#") {
			// sweetAlert("Maaf...", "Pendapatan tidak boleh kosong!", "error");
			// return false;
		// }
		// if ($("#terimadari").val() == "#") {
			// sweetAlert("Maaf...", "Sumber Pendapatan tidak boleh kosong!", "error");
			// return false;
		// }
		// if ($("#nominal").val() == "" || $("#nominal").val() == "0") {
			// sweetAlert("Maaf...", "Nominal tidak boleh kosong!", "error");
			// return false;
		// }
		if ($("#total_bayar").val() == "0") {
			sweetAlert("Maaf...", "Pembayaran Belum Dipilih!", "error");
			return false;
		}
		if ($("#total_sisa").val() != "0") {
			sweetAlert("Maaf...", "Masih Ada Sisa!", "error");
			return false;
		}
	}
	
	
	//Hutang
	function load_hutang() {
		// alert('sini');
		var tpengelolaan_id=$("#tpengelolaan_id").val();		
		var disabel='disabled';		
		$('#tabel_hutang').DataTable().destroy();
		var table = $('#tabel_hutang').DataTable({
		"pageLength": 50,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tpengelolaan/load_hutang/',
			type: "POST",
			dataType: 'json',
			data: {
				tpengelolaan_id: tpengelolaan_id,				
				disabel: disabel,				
			}
		},
		columnDefs: [
						{  "visible":false, targets:[0,7] },
						{  className: "text-right", targets:[3,4,5] },
						{  className: "text-center", targets:[7] },
						{ "width": "5%", "targets": [1] },
						{ "width": "9%", "targets": [4] },
						{ "width": "12%", "targets": [3,5,7] },
						{ "width": "18%", "targets": [2,6] },
				],
		});
	}
	
	$(document).on("click",".edit_hutang",function(){	
		var table = $('#tabel_hutang').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		// var id=table.cell(tr,0).data();
		var tedit_hutang=table.cell(tr,0).data();
		$("#tedit_hutang").val(tedit_hutang);
			get_data_hutang(tedit_hutang);					
	});
	
	//Piutang
	function load_piutang() {
		// alert('sini');
		var tpengelolaan_id=$("#tpengelolaan_id").val();		
		var disabel='disabled';		
		$('#tabel_piutang').DataTable().destroy();
		var table = $('#tabel_piutang').DataTable({
		"pageLength": 50,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tpengelolaan/load_piutang/',
			type: "POST",
			dataType: 'json',
			data: {
				tpengelolaan_id: tpengelolaan_id,				
				disabel: disabel,				
			}
		},
		columnDefs: [
						{  "visible":false, targets:[0,7] },
						{  className: "text-right", targets:[3,4,5] },
						{  className: "text-center", targets:[7] },
						{ "width": "5%", "targets": [1] },
						{ "width": "9%", "targets": [4] },
						{ "width": "12%", "targets": [3,5,7] },
						{ "width": "18%", "targets": [2,6] },
				],
		});
	}
	
	function load_pendapatan() {
		// alert('sini');
		var tpengelolaan_id=$("#tpengelolaan_id").val();		
		var disabel='disabled';		
		$('#tabel_pendapatan').DataTable().destroy();
		var table = $('#tabel_pendapatan').DataTable({
		"pageLength": 50,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tpengelolaan/load_pendapatan/',
			type: "POST",
			dataType: 'json',
			data: {
				tpengelolaan_id: tpengelolaan_id,				
				disabel: disabel,				
			}
		},
		columnDefs: [
						{  "visible":false, targets:[0,6] },
						{  className: "text-right", targets:[1,3] },
						{  className: "text-center", targets:[4,5,6] },
						{ "width": "5%", "targets": [1] },
						{ "width": "10%", "targets": [3,6] },
						{ "width": "40%", "targets": [2] },
						{ "width": "15%", "targets": [4,5] },
				],
		});
	}
	
	//Pendapatan
	function load_permintaan() {
		// alert('sini');
		var tpengelolaan_id=$("#tpengelolaan_id").val();		
		var disabel='disabled';		
		$('#tabel_permintaan').DataTable().destroy();
		var table = $('#tabel_permintaan').DataTable({
		"pageLength": 50,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tpengelolaan/load_permintaan/',
			type: "POST",
			dataType: 'json',
			data: {
				tpengelolaan_id: tpengelolaan_id,				
				disabel: disabel,				
			}
		},
		columnDefs: [
						{  "visible":false, targets:[0,6] },
						{  className: "text-right", targets:[1,3] },
						{  className: "text-center", targets:[4,5,6] },
						{ "width": "5%", "targets": [1] },
						{ "width": "10%", "targets": [3,6] },
						{ "width": "40%", "targets": [2] },
						{ "width": "15%", "targets": [4,5] },
				],
		});
	}
	
</script>