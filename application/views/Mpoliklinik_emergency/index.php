<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1784'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_poli()"><i class="fa fa-bar-chart-o"></i> {title}</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('1784'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?>" id="tab_info">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
					
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_poli">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="30%">Tipe</th>
											<th width="50%">Poliklinik</th>
											<th width="15%">Action</th>										   
										</tr>
										<tr>
											<th>#</th>
											
											<th>
												<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													
													<option value="#" <?=($idtipe=='#'?'selected':'')?>>-Tipe-</option>
													<option value="1" <?=($idtipe=='1'?'selected':'')?>>POLIKINIK</option>
													<option value="2" <?=($idtipe=='2'?'selected':'')?>>IGD</option>
													
												</select>
											</th>
											<th>
												<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" <?=($idpoli=='#'?'selected':'')?>>-Pilih Poli-</option>
													<?foreach(get_all('mpoliklinik',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>" <?=($idpoli==$r->id?'selected':'')?>><?=$r->nama?></option>
													<?}?>
													
													
												</select>
											</th>
											
											<th>
												<div class="btn-group">
												<?php if (UserAccesForm($user_acces_form,array('1785'))){ ?>
												<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_poli" name="btn_tambah_poli"><i class="fa fa-save"></i> Simpan</button>
												<?}?>
												<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_poli()"><i class="fa fa-refresh"></i> Clear</button>
												</div>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			
			
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
	load_poli();		
	
})	
$("#idtipe").change(function(){
		$.ajax({
			url: '{site_url}Mpoliklinik_emergency/find_poli/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#idpoli").empty();
				$("#idpoli").append(data);
			}
		});

});
function clear_poli(){
	$("#idtipe").val('#').trigger('change');
	$("#idpoli").val('#').trigger('change');
	
	$("#btn_tambah_poli").html('<i class="fa fa-save"></i> Save');
	
}
function load_poli(){
	$('#index_poli').DataTable().destroy();	
	table = $('#index_poli').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}Mpoliklinik_emergency/load_poli', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}

$("#btn_tambah_poli").click(function() {
	let idpoli=$("#idpoli").val();
	let idtipe=$("#idtipe").val();
	
	if (idtipe=='#'){
		sweetAlert("Maaf...", "Tentukan Tipe", "error");
		return false;
	}
	if (idpoli=='#'){
		sweetAlert("Maaf...", "Tentukan Poliklinik", "error");
		return false;
	}
	
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Mpoliklinik_emergency/simpan_poli', 
		dataType: "JSON",
		method: "POST",
		data : {
				idpoli:idpoli,
				
			},
		complete: function(data) {
			clear_poli();
			$('#index_poli').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});

function hapus_poli(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Poliklinik?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}Mpoliklinik_emergency/hapus_poli',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_poli').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

</script>