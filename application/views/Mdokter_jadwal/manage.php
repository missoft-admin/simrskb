<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mdokter_jadwal" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mdokter_jadwal/save','class="form-horizontal" id="form-work"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Periode</label>
				<div class="col-md-7">
					<input type="text" class="form-control" readonly value="<?=GetDayIndonesia($periode)?>">
					<input type="hidden" class="form-control" id="periode" placeholder="Periode" name="periode" value="{periode}" readonly required="" aria-required="true">
				</div>
			</div>
	</div>
	<div class="block-content">
			<input type="hidden" class="form-control" id="rowindex"/>
			<input type="hidden" class="form-control" id="tanggal"/>
			<input type="hidden" class="form-control" id="status_verifikasi"/>
			<div class="table-responsive">
			<table class="table table-bordered table-striped" id="detail_list">
				<thead>
					<tr>
						<th style="width:25%">Tanggal</th>
						<th style="width:25%">Jadwal Pagi</th>
						<th style="width:25%">Jadwal Siang</th>
						<th style="width:25%">Jadwal Malam</th>
						<th style="width:25%">Aksi</th>
					</tr>
					<tr>
						<th>
							<input type="text" class="form-control" id="hari" readonly placeholder="Hari" value="">
						</th>
						<th>
							<select class="js-select2 form-control" id="dokterIdPagi" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="">Pilih Opsi</option>
								<?php foreach ($list_dokter as $row){ ?>
									<option value="<? echo $row->id ?>"><?php echo $row->nama; ?></option>
								<?php } ?>
							</select>
						</th>
						<th>
							<select class="js-select2 form-control" id="dokterIdSiang" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="">Pilih Opsi</option>
								<?php foreach ($list_dokter as $row){ ?>
									<option value="<? echo $row->id ?>"><?php echo $row->nama; ?></option>
								<?php } ?>
							</select>
						</th>
						<th>
							<select class="js-select2 form-control" id="dokterIdMalam" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="">Pilih Opsi</option>
								<?php foreach ($list_dokter as $row){ ?>
									<option value="<? echo $row->id ?>"><?php echo $row->nama; ?></option>
								<?php } ?>
							</select>
						</th>
						<th>
							<?php if (UserAccesForm($user_acces_form,array('18'))){ ?>
							<a id="detail_add" class="btn btn-success">Ubah</a>
							<?}?>
						</th>
					</tr>
				</thead>
				<tbody>
					<?php if ($this->uri->segment(2) == 'update') { ?>
						<?php foreach  ($list_jadwal as $row) { ?>
						<tr>
							<td style="display:none;"><?=$row->tanggal;?></td>
							<td><?=GetDayIndonesia($row->periode.'-'.$row->tanggal, true);?></td>
							<td style="display:none;"><?=$row->iddokter_pagi;?></td>
							<td><?=$row->nama_dokter_pagi;?></td>
							<td style="display:none;"><?=$row->iddokter_siang;?></td>
							<td><?=$row->nama_dokter_siang;?></td>
							<td style="display:none;"><?=$row->iddokter_malam;?></td>
							<td><?=$row->nama_dokter_malam;?></td>
							<td>
								<?php if($row->status_verifikasi == 0){ ?>
								<a href="#" class="btn btn-primary detail_edit" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a>
							<?php }else{ ?>
								<a href="#" data-toggle="tooltip" title="Terkunci" class="btn btn-danger btn-sm"><i class="fa fa-lock"></i></a>
							<?php } ?>
							</td>
							<td style="display:none;"><?=$row->status_verifikasi;?></td>
						</tr>
						<?php } ?>
					<?php }else{ ?>
						<?php $totaldays = cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y")); ?>
						<?php for ($i=1; $i <= $totaldays; $i++) { ?>
							<tr>
								<td style="display:none;"><?=$i;?></td>
								<td><?=GetDayIndonesia(date("Y-m-$i"), true)?></td>
								<td style="display:none;"></td>
								<td></td>
								<td style="display:none;"></td>
								<td></td>
								<td style="display:none;"></td>
								<td></td>
								<td><a href="#" class="btn btn-primary detail_edit" data-toggle="tooltip" title="Ubah"><i class="fa fa-pencil"></i></a></td>
								<td style="display:none;">0</td>
							</tr>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>
				</div>
			<div class="row">
				<div class="col-sm-3">
				</div>
				<div class="col-sm-9">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mdokter_jadwal" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<input type="hidden" id="detail_value" name="detail_value"/>
			<br><br>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
		$("#detail_add").click(function() {
      var valid = validateDetail();
			if(!valid) return false;

			var row_index;
			var tanggal;
			var duplicate = false;

			if($("#tanggal").val() != ''){
				var content = "";
				tanggal = $("#tanggal").val();
				row_index = $("#rowindex").val();
			}else{
				var content = "<tr>";
				tanggal = $('#tanggal').val();
				$('#detail_list tbody tr').filter(function (){
					var $cells = $(this).children('td');
					if($cells.eq(1).text() === $("#tanggal").val()){
						sweetAlert("Maaf...", "Jadwal untuk tanggal " + $("#tanggal").val() + " sudah ditambahkan.", "error");
						duplicate = true;
						return false;
					}else{
						no = $("#tanggal").val();
					}
				});
			}

			if(duplicate == false){
				content += "<td style='display:none;'>" + $("#tanggal").val() + "</td>";
				content += "<td>" + $("#hari").val() + "</td>";
				content += "<td style='display:none;'>" + $("#dokterIdPagi option:selected").val(); + "</td>";
				content += "<td>" + $("#dokterIdPagi option:selected").text(); + "</td>";
				content += "<td style='display:none;'>" + $("#dokterIdSiang option:selected").val(); + "</td>";
				content += "<td>" + $("#dokterIdSiang option:selected").text(); + "</td>";
				content += "<td style='display:none;'>" + $("#dokterIdMalam option:selected").val(); + "</td>";
				content += "<td>" + $("#dokterIdMalam option:selected").text(); + "</td>";
				content += "<td><a href='#' class='btn btn-primary detail_edit' data-toggle='tooltip' title='Ubah'><i class='fa fa-pencil'></i></a></td>";
				content += "<td>" + $("#status_verifikasi").val(); + "</td>";

				if($("#rowindex").val() != ''){
					$('#detail_list tbody tr:eq(' + row_index + ')').html(content);
				}else{
					content += "</tr>";
					$('#detail_list tbody').append(content);
				}

				detailClear();
			}
		});

		$(document).on("click", ".detail_edit", function() {
			$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
			$("#tanggal").val($(this).closest('tr').find("td:eq(0)").html());

			$("#hari").val($(this).closest('tr').find("td:eq(1)").html());
			$("#dokterIdPagi").select2('val', $(this).closest('tr').find("td:eq(2)").html());
			$("#dokterIdSiang").select2('val', $(this).closest('tr').find("td:eq(4)").html());
			$("#dokterIdMalam").select2('val', $(this).closest('tr').find("td:eq(6)").html());

			$("#dokterIdPagi").select2('open');

			$("#status_verifikasi").val($(this).closest('tr').find("td:eq(9)").html());

			return false;
		});

		$(document).on("click", ".detail_remove", function() {
			if (confirm("Hapus data ?") == true) {
				$(this).closest('td').parent().remove();
			}
			return false;
		});

		$("#form-work").submit(function(e) {
			var form = this;

			if ($("#periode").val() == "") {
				e.preventDefault();
				sweetAlert("Maaf...", "Periode jadwal tidak boleh kosong!", "error");
				return false;
			}

			var detail_tbl = $('table#detail_list tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					return $(cell).html();
				});
			});

			$("#detail_value").val(JSON.stringify(detail_tbl));

			swal({
				title: "Berhasil!",
				text: "Proses penyimpanan data.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});

		$("#dokterIdPagi").change(function() {
			$("#dokterIdSiang").select2('open');
		});

		$("#dokterIdSiang").change(function() {
			$("#dokterIdMalam").select2('open');
		});
	});

	function detailClear() {
		$("#rowindex").val('');
		$("#tanggal").val('');
		$("#hari").val('');
		$(".detail").val('');

		$("#dokterIdPagi").select2("trigger", "select", {data : {id: '',text: ''}});
		$("#dokterIdSiang").select2("trigger", "select", {data : {id: '',text: ''}});
		$("#dokterIdMalam").select2("trigger", "select", {data : {id: '',text: ''}});
	}

	function validateDetail() {
		if ($("#hari").val() == "") {
			sweetAlert("Maaf...", "Tanggal / Hari belum dipilih!", "error");
			return false;
		}
		if ($("#dokterIdPagi").val() == "") {
			sweetAlert("Maaf...", "Dokter Jadwal Pagi Belum Dipilih!", "error");
			return false;
		}

		if ($("#dokterIdSiang").val() == "") {
			sweetAlert("Maaf...", "Dokter Jadwal Siang Belum Dipilih!", "error");
			return false;
		}

		if ($("#dokterIdMalam").val() == "") {
			sweetAlert("Maaf...", "Dokter Jadwal Malam Belum Dipilih!", "error");
			return false;
		}

		return true;
	}
</script>
