<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mdistributor" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mdistributor/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="kode">Kode</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="kode" placeholder="Kode" name="kode" value="{kode}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="alamat">Alamat</label>
				<div class="col-md-7">
					<textarea class="form-control" id="alamat" placeholder="Alamat" name="alamat" required="" aria-required="true">{alamat}</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="telepon">Telepon</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="telepon" placeholder="Telepon" name="telepon" value="{telepon}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mdistributor" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>
