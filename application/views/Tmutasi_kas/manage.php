<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">

		<h3 class="block-title">TRANSAKSI MUTASI KAS</h3>
	</div>
	<div class="block-content" style="margin-top: -20px;font-size:">
		<?php echo form_open('tmutasi_kas/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label" for="nama">Tanggal Transaksi</label>
					<div class="col-md-4">
						<div class="input-group date">
						<input class="js-datepicker form-control input" <?=$disabel?> <?=($st_verifikasi?'disabled':'')?> type="text" id="tanggal_trx" name="tanggal_trx" value="<?=$tanggal_trx?>" data-date-format="dd-mm-yyyy"/>
						<span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2  control-label" for="dari">Dari Kas</label>
					<div class="col-md-4">
						<select <?=$disabel?> name="dari" id="dari" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">	
							<option value="#">- Pilih Sumber Kas -</option>
							<? foreach($list_sumber_kas as $row){?>
								<option value="<?=$row->id?>" <?=($dari==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
					</div>
					<? if ($id==''){?>
					<label class="col-md-2  control-label" for="ke">Saldo Kas Dari-</label>
					<div class="col-md-4">
						<input  type="text" readonly class="form-control number" id="saldo_dari" placeholder="Saldo" name="saldo_dari"  value="0">
					</div>
					<?}?>
				</div>
				<div class="form-group">
					
					<label class="col-md-2  control-label" for="ke">Ke Kas</label>
					<div class="col-md-4">
						<select <?=$disabel?> name="ke" id="ke" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">	
							<option value="#" selected>- Pilih Ke Kas -</option>
							
							<? 
							  if ($id!=''){
							  foreach($list_sumber_kas as $row){?>
								<option value="<?=$row->id?>" <?=($ke==$row->id?'selected':'')?>><?=$row->nama?></option>
							  <?}}?>
						</select>
					</div>
					<? if ($id==''){?>
					<label class="col-md-2  control-label" for="ke">Saldo Kas Ke-</label>
					<div class="col-md-4">
						<input  type="text" readonly class="form-control number" id="saldo_ke" placeholder="Saldo" name="saldo_ke" value="0">
					</div>
					<?}?>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="idmetode">Metode</label>
					<div class="col-md-4">
						<select <?=$disabel?> name="idmetode" id="idmetode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#">Pilih Opsi</option>							
							<option value="1" <?=($idmetode=='1'?'selected':'')?>>Cheq</option>	
							<option value="2" <?=($idmetode=='2'?'selected':'')?>>Tunai</option>	
							<option value="4" <?=($idmetode=='4'?'selected':'')?>>Transfer Antar Bank</option>
							<option value="3" <?=($idmetode=='3'?'selected':'')?>>Transfer Beda Bank</option>							
						</select>
					</div>
					<label class="col-md-2 control-label" for="nama">Nominal Rp. </label>
					<div class="col-md-4">
						<input <?=$disabel?>  type="hidden" class="form-control" id="sumber_kas_id_tmp" placeholder="sumber_kas_id_tmp" name="sumber_kas_id_tmp" >
						
						<input <?=$disabel?>  type="text" class="form-control number" id="nominal_bayar" placeholder="Nominal" name="nominal_bayar" value="{nominal}">
						<input   type="hidden" class="form-control" id="nama_pegawai" placeholder="Nama Pegawai" name="nama_pegawai" value="{nama_pegawai}">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-2 control-label" for="nama">Pegawai Yang Mengambil </label>
					<div class="col-md-10">
						<select <?=$disabel?> name="idpegawai" id="idpegawai" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">	
							<option value="#" <?=($idpegawai=='#'?'selected':'')?> >- Pilih Pegawai -</option>							
							<? 
							  
							  foreach(get_all('mpegawai') as $row){?>
								<option value="<?=$row->id?>" <?=($idpegawai==$row->id?'selected':'')?>><?=$row->nama?></option>
							  <?}?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="nama">Keterangan </label>
					<div class="col-md-10">
						<textarea class="form-control js-summernote" name="deskripsi" id="deskripsi"><?=$deskripsi?></textarea>
					</div>
				</div>
				<div class="form-group hiden btn_hide">
					<div class="text-right bg-light lter">
						<? if ($disabel==''){?>
						<button class="btn btn-success" type="submit" id="btn_simpan" name="btn_simpan" value="2">Simpan & Verifikasi</button>
						<button class="btn btn-primary" type="submit" id="btn_simpan" name="btn_simpan" value="1">Simpan</button>
							  <?}?>
						<a href="{base_url}tmutasi_kas" class="btn btn-default" type="button">Kembali</a>
					</div>
				</div>
			</div>
		</div>
		<input  type="hidden" readonly class="form-control" id="id" placeholder="id" name="id" value="<?=$id?>">
		
		<?php echo form_close() ?>
		
		
</div>
</div>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	
	$(document).ready(function(){
		
		$(".decimal").number(true,2,'.',',');
		$('.number').number(true, 0, '.', ',');
		$('#deskripsi').summernote({
		  height: 70,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }
		});

	})
	function refresh_koneksi(){
		var dari=$("#dari").val();
		$.ajax({
			url: '{site_url}tmutasi_kas/refresh_koneksi/'+dari,
			dataType: "json",
			success: function(data) {
				$("#ke").empty();
				$('#ke').append('<option value="#">- Pilih Ke -</option>');
				$('#ke').append(data.detail);
				$('#saldo_dari').val(data.saldo_dari);
			}
		});
	}
	
	$("#idpegawai").change(function(){
		$("#nama_pegawai").val($("#idpegawai option:selected").text());
	});
	$("#dari").change(function(){
		refresh_koneksi();
	});
	$("#ke").change(function(){
		var ke=$("#ke").val();
		$.ajax({
			url: '{site_url}tmutasi_kas/get_saldo_ke/'+ke,
			dataType: "json",
			success: function(data) {				
				$('#saldo_ke').val(data.saldo_ke);
			}
		});
	});
	$("#nominal_bayar").keyup(function(){
		// if (parseFloat($("#nominal_bayar").val()) > parseFloat($("#saldo_dari").val())){
			// sweetAlert("Maaf...", "Saldo Sumber tidak mencukupi!", "error");
			// // alert($("#saldo_dari").val());
			// $("#nominal_bayar").val($("#saldo_dari").val());
			// return false;
		// }
	});
	function stripHTML(text){
	   var regex = /(<([^>]+)>)/ig;
	   return text.replace(regex, "");
	}
	function validate_final(){
		var des=stripHTML($('#deskripsi').summernote('code'));
		// alert();
		if ($("#dari").val() == "#") {
			sweetAlert("Maaf...", "Dari Kas harus diisi!", "error");
			$("#dari").focus();
			return false;
		}
		if ($("#ke").val() == "#") {
			sweetAlert("Maaf...", "Ke Kas harus diisi!", "error");
			$("#ke").focus();
			return false;
		}
		if ($("#nominal_bayar").val() == "0" || $("#nominal_bayar").val() == "") {
			sweetAlert("Maaf...", "Nominal harus diisi!", "error");
			$("#nominal_bayar").focus();
			return false;
		}
		if ($("#idmetode").val() == "#") {
			sweetAlert("Maaf...", "Metode harus diisi!", "error");
			$("#idmetode").focus();
			return false;
		}
		if (des== "") {
			sweetAlert("Maaf...", "Deskripsi harus diisi!", "error");
			$("#deskripsi").focus();
			return false;
		}
		if ($("#idpegawai").val() == "#") {
			sweetAlert("Maaf...", "Pegawai  harus diisi!", "error");
			$("#deskripsi").focus();
			return false;
		}
	}
	function goBack() {
	  window.history.back();
	}
	
</script>