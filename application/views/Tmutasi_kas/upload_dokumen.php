<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tmutasi_kas/index" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title} </h3>
	</div>
	<div class="block-content">
		<div class="row">
			<div class="col-md-6">
				<div class="input-group" style="width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">No. Transaksi</span>
					<input type="text" class="form-control" value="{notransaksi}" readonly="true">
					<input type="hidden" class="form-control" id="id" value="{id}" readonly="true">
				</div>
				<div class="input-group" style="margin-top:5px; width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">Tujuan</span>
					<input type="text" class="form-control" value="{ke_nama}" readonly="true">
				</div>
			</div>
			<div class="col-md-6">
				<div class="input-group" style="width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">Dari Kas</span>
					<input type="text" class="form-control" value="<?=($dari_nama)?>" readonly="true">
				</div>
				<div class="input-group" style="margin-top:5px; width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">Nominal</span>
					<input type="text" class="form-control" value="<?=number_format($nominal)?>" readonly="true">
				</div>
			</div>
		</div>

    <br>

	<div class="">
      <div class="row">
        <div class="col-md-12">
          <label class="col-md-3 control-label" for="nama">Keterangan</label>
        </div>
		<div class="col-md-12">
          <textarea class="form-control js-summernote" name="keterangan_tmp" id="keterangan_tmp"></textarea>
        </div>
      </div>
	  <div class="row">
        <div class="col-md-12">
          <form class="dropzone" action="{base_url}tmutasi_kas/upload_files" method="post" enctype="multipart/form-data">
			<textarea id="keterangan" name="keterangan" class="form-control" style="display:none;"></textarea>
            <input name="idtransaksi" type="hidden" value="{id}">
          </form>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <br>
          <button class="btn btn-success" id="btn_refresh"><i class="fa fa-refresh"></i> Reload List</button>
          <br><br>
        </div>
        <div class="col-lg-12">
		<div class="table-responsive">
          <table class="table table-bordered table-striped table-responsive" id="listFile">
      			<thead>
      				<tr>
      					<th width="1%">No</th>
      					<th width="20%">File</th>
      					<th width="20%">Keterangan</th>
      					<th width="15%">Upload By</th>
      					<th width="5%">Size</th>
      					<th width="5%">Aksi</th>
      				</tr>
      			</thead>
      			<tbody>
              <?php foreach ($listFiles as $index => $row) { ?>
                <tr>
                  <td><?=$index+1?></td>
                  <td><a href="{base_url}assets/upload/mutasi_kas/<?=$row->filename?>" target="_blank" data-toggle="tooltip" title="Preview"> <?=$row->filename?></a></td>
                  <td><?=$row->keterangan?></td>
                  <td><?=$row->upload_by_nama?> <?=HumanDateLong($row->upload_date)?></td>
                  <td><?=$row->size?></td>
                  <td>
                    <a href="{base_url}assets/upload/mutasi_kas/<?=$row->filename?>" target="_blank" data-toggle="tooltip" title="Preview" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                    <a href="#" data-urlindex="{base_url}tmutasi_kas/upload_document/{id}" data-urlremove="{base_url}Tmutasi_kas/delete_file/<?=$row->id?>" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>
                  </td>
                </tr>
              <? } ?>
      			</tbody>
      		</table>
        </div>
        </div>
      </div>
    </div>
	</div>
</div>

<script type="text/javascript">
var myDropzone 
	$(document).ready(function(){
		$('.js-summernote').summernote({
		  height: 100,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})	
		Dropzone.autoDiscover = false;
		myDropzone = new Dropzone(".dropzone", { 
		   autoProcessQueue: true,
		   maxFilesize: 30,
		});
		myDropzone.on("complete", function(file) {
		  
		  refresh_image();
		  myDropzone.removeFile(file);
		  
		});
	
	});
	$("#keterangan_tmp").on("summernote.change", function (e) {   // callback as jquery custom event 
		console.log('it is changed');
		$("#keterangan").val($('#keterangan_tmp').summernote('code'));
	});
	
	$(document).on("click","#btn_refresh",function(){
		refresh_image();
	});

	function refresh_image(){
		var id=$("#id").val();
		
		$.ajax({
			url: '{site_url}Tmutasi_kas/refresh_image/'+id,
			dataType: "json",
			success: function(data) {
				// if (data.detail!=''){
					$('#listFile tbody').empty();
					$("#box_file2").attr("hidden",false);
					$("#listFile tbody").append(data.detail);
				// }
				// console.log();
				
			}
		});
	}
  $('#listFile tbody').on('click','a.removeData', function(){
      urlIndex = $(this).data('urlindex');
      urlRemove = $(this).data('urlremove');
      warningText = ($(this).data('messagewarning') ? $(this).data('messagewarning') : 'Hapus record tersebut?');
      successText = ($(this).data('messagesuccess') ? $(this).data('messagesuccess') : 'Data telah dihapus.');
      setTimeout(function() {
          swal({
              text: warningText,
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#d26a5c',
              confirmButtonText: 'Ya',
              cancelButtonText: 'Batal',
              html: false,
              preConfirm: function() {
                  return new Promise(function (resolve) {
                      setTimeout(function() {
                        resolve();
                      }, 100);
                  })
              }
          }).then(
              function (result) {
                if(successText == 'Data telah dihapus.'){
                  $.ajax({
                    url: urlRemove
                  });
                  swal('Berhasil!', successText, 'success');
                  setTimeout(function() {
                    window.location = urlIndex;
                  }, 100);
                }else{
                  window.location = urlRemove;
                }
              }
          )
      }, 100);
  });
</script>
