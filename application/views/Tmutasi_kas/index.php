<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('1431'))){ ?>
<div class="block">
	<div class="block-header">
	<?php if (UserAccesForm($user_acces_form,array('1433'))){ ?>
		<ul class="block-options">       
            <a href="{base_url}tmutasi_kas/create" type="button" class="btn btn-success"><i class="fa fa-plus"></i> TAMBAH MUTASI KAS</a>       
		</ul>
	<?}?>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:20px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Transaksi</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="notransaksi" placeholder="No Transaksi" name="notransaksi" value="">
                    </div>
                </div>
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Dari</label>
                    <div class="col-md-8">
                       <select name="dari" id="dari" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">	
							<option value="#">- Pilih Sumber Kas -</option>
							<? foreach($list_sumber_kas as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>
							<?}?>
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Ke</label>
                    <div class="col-md-8">
                       <select name="ke" id="ke" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">	
							<option value="#">- Pilih Ke Kas -</option>
							<? foreach($list_sumber_kas as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>
							<?}?>
						</select>
                    </div>
                </div>
				
				
            </div>
			<div class="col-md-6">
				
				 <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Mutasi</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggal_trx1" name="tanggal_trx1" placeholder="From" value="{tanggal_trx1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_trx2" name="tanggal_trx2" placeholder="To" value="{tanggal_trx2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-4 control-label" for="status">Status</label>
                    <div class="col-md-8">
                       <select id="status" name="status" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua -</option>
							<option value="1" >SUDAH VERIFIKASI</option>
							<option value="2" >BELUM DIVERIFIKASI</option>							
							<option value="3" >DIHAPUS</option>							
						</select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
                
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_index">
			<thead>
				<tr>
					<th>#</th>
					<th>No</th>					
					<th>No Mutasi</th>
					<th>Tanggal </th>
					<th>Dari Kas</th>
					<th>Ke Kas</th>
					<th>Deskripsi</th>
					<th>Nominal</th>
					<th>User</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<?}?>
<div class="modal fade in black-overlay" id="modal_approval" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<input type="hidden" class="form-control" id="idtrx" placeholder="" name="idtrx" value="">
						<div class="col-md-12">
							<table width="100%" id="tabel_user" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Step</th>
										<th>User</th>
										<th>Jika Setuju</th>
										<th>Jika Menolak</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

					</div>
					<?php echo form_close() ?>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-primary" id="btn_simpan_approval" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Proses Persetujuan</button>
			</div>

		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_user" role="dialog" aria-hidden="true">
	<div class="modal-dialog  modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<input type="hidden" class="form-control" id="idrka" placeholder="" name="idrka" value="">
						<div class="col-md-12">
							<table width="100%" id="tabel_user_proses" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Step</th>
										<th>User</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

					</div>
					<?php echo form_close() ?>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		// alert('sini');
		load_index();
		
	})	
	$("#btn_filter").click(function() {
		// alert('ID');
		table.destroy();
		load_index();
	});
	
	$(document).on("click",".verif",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		// alert(id); return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Verifikasi Mutasi Kas?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}tmutasi_kas/verif/'+id,
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Mutas diselesaikan diverifikasi'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	$(document).on("click",".hapus",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		// alert(id); return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Hapus Mutas?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}tmutasi_kas/hapus/'+id,
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Mutas diselesaikan diverifikasi'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	function load_index(){
		var notransaksi=$("#notransaksi").val();
		var status=$("#status").val();		
		var tanggal_trx1=$("#tanggal_trx1").val();
		var tanggal_trx2=$("#tanggal_trx2").val();
		var dari=$("#dari").val();
		var ke=$("#ke").val();
		// alert(tanggal_setoran1);
		table=$('#table_index').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tmutasi_kas/getIndex',
					type: "POST",
					dataType: 'json',
					data : {
						notransaksi:notransaksi,
						status:status,
						tanggal_trx1:tanggal_trx1,
						tanggal_trx2:tanggal_trx2,
						dari:dari,
						ke:ke,
						
					   }
				},
				"columnDefs": [
					{ "width": "0%", "targets": 0, "visible": false },
					{ "width": "3%", "targets": 1, "visible": true },
					{ "width": "8%", "targets": 2, "visible": true,"class":"text-center" },
					{ "width": "10%", "targets": 3, "visible": true,"class":"text-center" },
					{ "width": "10%", "targets": 4, "visible": true },
					{ "width": "10%", "targets": 5, "visible": true },
					{ "width": "10%", "targets": 6, "visible": true },
					{ "width": "8%", "targets": 7, "visible": true,"class":"text-right"  },
					{ "width": "10%", "targets": 8, "visible": true,"class":"text-center" },
					{ "width": "10%", "targets": 9, "visible": true,"class":"text-center"  },
					{ "width": "15%", "targets": 10, "visible": true },
				]
			});
	}
	function load_user_approval(id,dari,ke,nominal){
		$("#idtrx").val(id)
		$("#modal_approval").modal('show');
		$("#btn_simpan_approval").attr('disabled', true);
		$('#tabel_user').DataTable().destroy();
		table=$('#tabel_user').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tmutasi_kas/load_user_approval',
					type: "POST",
					dataType: 'json',
					data : {
						dari:dari,ke:ke,
						nominal:nominal,

					   }
				},
				"columnDefs": [
					 {  className: "text-right", targets:[0] },
					 {  className: "text-center", targets:[1,2,3] },
					 { "width": "10%", "targets": [0] },
					 { "width": "40%", "targets": [1] },
					 { "width": "25%", "targets": [2,3] },

				],
				"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					if (aData[1]){
						$("#btn_simpan_approval").attr('disabled', false);
					}
				},
			});
	}
	$(document).on("click","#btn_simpan_approval",function(){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Melanjutkan Step Persetujuan?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tmutasi_kas/simpan_proses_peretujuan/'+$("#idtrx").val(),
				type: 'POST',
				complete: function() {
					$("#cover-spin").hide();
					swal({
						title: "Berhasil!",
						text: "Proses Approval Berhasil.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});

		return false;
	});
	function lihat_user(idtrx){

		$("#modal_user").modal('show');

		// alert(idrka);
		$.ajax({
			url: '{site_url}tmutasi_kas/list_user/'+idtrx,
			dataType: "json",
			success: function(data) {
				$("#tabel_user_proses tbody").empty();
				$("#tabel_user_proses tbody").append(data.detail);
			}
		});
	}
</script>
