<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Kwitansi Pendapatan</title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 9px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
        font-size: 18px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
	  .header {
		  font-size: 20px !important;
        font-weight: bold;
        text-align: center;
      }
      .content td {
		  border: 0px solid #000 !important;
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <?php for ($i=0; $i < 2; $i++) { ?>
   
	<table class="content">
      <tr  height="300px">
        <td rowspan="4" width="20%" class="text-center"><img src="assets/upload/logo/logo.png" alt="" width="100" height="100"></td>
        <td width="60%" colspan="2" class="text-right header"><b>BUKTI MUTASI KAS</b> &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; </td>
		<td width="10%"></td>
      </tr>
	 
	  <tr>
			<td width="20%" class="text-left">TANGGAL</td>
			<td width="90%" colspan="2" >:  <?=HumanDateLong($created_at)?></td>
	  </tr>
	  <tr>
			<td class="text-left">NO TRANSAKSI</td>
			<td colspan="2">:  <?=$notransaksi;?></td>
	  </tr>
	   <tr>
			<td class="text-left">USER</td>
			<td colspan="2">: <?=$user_buat?></td>
	  </tr>
	  
  </table>
  <br>
    <table class="content-2">
       <tr>
        <td class="text-italic text-bold">Nominal Mutasi</td>
        <td class="text-center" style="width:5%">:</td>
        <td class="text-italic" style="background: #e2e2e2;">Rp, <?=ucwords(number_format($nominal));?>,-</td>
      </tr>
      <tr>
        <td class="text-italic text-bold" style="width:20%">Diambil Dari</td>
        <td class="text-center" style="width:5%">:</td>
        <td class="text-italic"><?=strtoupper($dari_nama);?></td>
      </tr>
	  <tr>
        <td class="text-italic text-bold" style="width:20%">Tujuan</td>
        <td class="text-center" style="width:5%">:</td>
        <td class="text-italic"><?=strtoupper($ke_nama);?></td>
      </tr>
     
      <tr>
        <td class="text-italic text-bold">Deskripsi</td>
        <td class="text-center" style="width:5%">:</td>
        <td class="border-dotted text-italic"><?=$deskripsi;?></td>
      </tr>
     
    </table>
	<br>
    <table class="content">
      <tr>
        <td style="width:40%">&nbsp;</td>
        <td style="width:10%">&nbsp;</td>
        <td style="width:10%">&nbsp;</td>
        <td class="text-italic" style="width:40%">Bandung, <?=HumanDate($created_at)?></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width:30%">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
	  <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width:30%">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
	  <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width:30%">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="border-thick-top border-thick-bottom text-italic text-bold text-center"></td>
        <td class="border-thick-top border-thick-bottom text-italic text-bold text-right"></td>
        <td style="width:30%">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="text-italic text-center"><u>( <?=$nama_pegawai;?> )</u></td>
        <td>&nbsp;</td>
        <td style="width:60%">&nbsp;</td>
        <td class="text-italic text-left"><u>( <?=$this->session->userdata('user_name')?> )</u><br>Tanggal Cetak : <?=date('d-m-Y H:i:s')?></td>
      </tr>
    </table>
    <br>
    <div class="border-dotted"></div>
    <br>
    <br>
    <?php } ?>
  </body>
</html>
