<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}antrian_asset_sound" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('antrian_asset_sound/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="row">
				<audio id="myAudio">
				  <source src="{upload_path}sound_antrian/<?=$file_sound?>" type="audio/ogg">
				</audio>
				<div class="form-group">
					<label class="col-md-2 control-label" for="nama_asset">Nama Asset</label>
					<div class="col-md-8">
						<input type="text" class="form-control" id="nama_asset" placeholder="Nama Asset" name="nama_asset" value="{nama_asset}">
					</div>
				</div>
				
				<div class="form-group ">
					<label class="col-md-2 control-label" for="example-hf-email">File Sound (.mp3 / .wav)</label>
					<div class="col-md-7">
						<div class="box">
							<input type="file" id="file-3" accept=".mp3,.wav" class="inputfile" style="display:none;" name="file_sound" value="{file_sound}" />
							<label for="file-3" class="text-danger"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span>
							
							
							</label>
						</div>
					</div>
				</div>
				<?if($file_sound){?>
				<div class="form-group push-5-t">
					<label class="col-md-2 control-label" for="nama_asset"></label>
					<div class="col-md-8">
						<button type="button" onclick="play_sound()" class="btn btn-xs"><i class="fa fa-play"> </i> Play <?=$file_sound?></button>
					</div>
				</div>
				<?}?>
				
				
				<div class="form-group">
					<div class="col-md-2 col-md-offset-2">
						<button class="btn btn-success" type="submit">Simpan</button>
						<a href="{base_url}antrian_layanan" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
					</div>
				</div>
			</div>

			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>

</div>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var x = document.getElementById("myAudio");

function play_sound() {
  x.play();
}

function pauseAudio() {
  x.pause();
}
	$(document).ready(function(){
		
	})	
	$(".chk_status").on("click", function(){
		let id_det;
		 check = $(this).is(":checked");
			if(check) {
				$(this).closest('tr').find(".isiField").removeAttr("disabled");
			} else {
				$(this).closest('tr').find(".isiField").attr('disabled', 'disabled');
			}
	}); 
	
	function validate_final(){
		
		
		if ($("#antrian_asset_sound").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Counter", "error");
			return false;
		}
		
		$("*[disabled]").not(true).removeAttr("disabled");
		$("#cover-spin").show();
		return true;

		
	}
</script>