<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-rounded block-bordered">
	<div class="block-header">
		<ul class="block-options">
			<li><a href="{site_url}tgudang_pengembalian" class="btn"><i class="fa fa-reply"></i></a></li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<?php echo form_open('tgudang_pengembalian/save','class="form-horizontal" id="form-work"') ?>
	<div class="block-content">
		<div class="row">
			<div class="col-sm-12">
				<div class="control-group">
					<div class="col-md-12">
						<table class="table table-striped table-bordered" id="detail_list" style="margin-bottom:0px">
							<thead>
								<tr>
									<th>Tipe</th>
									<th>Barang</th>
									<th>Distributor</th>
									<th>No Batch</th>
									<th>Harga Beli</th>
									<th>Kuantitas</th>
									<th>Jenis</th>
									<th>Alasan</th>
									<th>Jenis Retur</th>
									<th>Nominal Retur</th>
									<th>Actions</th>
									<th hidden>idtipe</th>
									<th hidden>nopenerimaan</th>
									<th hidden>kodebarang</th>
									<th hidden>iddistributor</th>
									<th hidden>idpenerimaan</th>
									<th hidden>kuantitas_pesan</th>
									<th hidden>jenis</th>
									<th hidden>jenis_retur</th>
									<th hidden>tgl_terima</th>
									<th hidden>pengganti</th>
                                    <th hidden>idpenerimaandetail</th>
                                    <th hidden>expireddate</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
						<table class="table table-striped table-bordered">
							<tbody>
								<tr>
									<td colspan="2" class="hidden-phone">
										
										<button class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal_detail_pengembalian" data-backdrop="static" id="detail_pengembalian_add" type="button">Tambah Pengembalian</button>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="text-right bg-light lter">
							<button class="btn btn-info" type="submit">Simpan</button>
							<a href="{site_url}tgudang_pengembalian" class="btn btn-default">Batal</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" id="detail_value" name="detail_value" value="[]">
		<input type="hidden" id="pengganti" name="pengganti"/>
		<br><br>
		<?php echo form_hidden('id', $id); ?>
		<?php echo form_close() ?>
	</div>
</div>
<button hidden id="success_msg" class="js-notify" data-notify-type="success" data-notify-message="Success!">Success</button>
<button hidden id="error_msg" class="js-notify" data-notify-type="danger" data-notify-message="Error!">Danger</button>


<div class="modal in" id="modal_detail_pengembalian"  role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Tambah Detail Pengembalian Gudang</h3>
                </div>
                <div class="block-content">
                	<form class="form-horizontal" id="form_detail">
                		<input type="hidden" id="idbarang">
                        <input type="hidden" id="idtipe"/>
                        <input type="hidden" id="nopenerimaan"/>
                        <input type="hidden" id="idpenerimaan"/>
                        <input type="hidden" id="iddistributor"/>
                        <input type="hidden" id="namadistributor"/>
                        <input type="hidden" id="rowindex"/>
                        <input type="hidden" id="tgl_terima"/>
                        <input type="hidden" id="idpenerimaandetail">
                        <input type="hidden" id="expireddate">
                        <input type="hidden" id="harga_master">
                        <input type="hidden" id="harga_asal">
                        <input type="hidden" id="jumlah_besar">
                        <input type="hidden" id="opsisatuan_asal">
                        <input type="hidden" id="xnobatch">
                        <input type="hidden" id="satuan_besar">
                        <input type="hidden" id="satuan_kecil">
                        <div class="form-group c" id="f_nobatch">
                			<label class="col-md-3 control-label">No Batch</label>
                			<div class="col-md-6">
                				<select id="nobatch" class="form-control" style="width: 100%;" data-placeholder="Choose one.."></select>
                			</div>
                		</div>
                        <div class="form-group c" id="f_nobatch">
                			<label class="col-md-3 control-label">No Penerimaan</label>
                			<div class="col-md-6">
                				<select id="noterima" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.."></select>
                			</div>
                		</div>
                		
						<div class="form-group c" id="f_namabarang">
							<label class="col-md-3 control-label" for="namabarang">Tipe</label>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input readonly type="text" class="form-control" id="tipe" placeholder="Tipe" name="tipe" value="" required="" aria-required="true">
								</div>
							</div>
						</div>
                        
                        <div class="form-group c" id="f_namabarang">
							<label class="col-md-3 control-label" for="namabarang">Nama Barang</label>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input readonly type="text" class="form-control" id="namabarang" placeholder="Nama Barang" name="namabarang" value="" required="" aria-required="true">
								</div>
							</div>
						</div>
						<div class="form-group c" id="f_hargabeli">
							<label class="col-md-3 control-label" for="hargabeli">Harga Beli</label>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input readonly type="text" class="form-control decimal" id="hargabeli" placeholder="Harga Beli" name="hargabeli" value="" required="" aria-required="true">
								</div>
							</div>
						</div>
                        <div class="form-group c" id="f_hargabeli">
							<label class="col-md-3 control-label" for="kuantitas_pesan">Kuantitas</label>
							<div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input readonly type="text" class="form-control number" id="kuantitas_pesan" placeholder="Kuantitas" name="kuantitas_pesan" value="" required="" aria-required="true">
								</div>
							</div>
							
							<div class="col-md-3">
									<input readonly type="text" class="form-control" id="satuan" placeholder="Satuan" name="satuan" value="" required="" aria-required="true">
							</div>
						</div>
						
						<div class="form-group c" id="f_kuantitas">
							<label class="col-md-3 control-label" for="kuantitas">Kuantitas Retur</label>
							<div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-edit"></i></span>
									<input type="text" class="form-control number" id="kuantitas" placeholder="Kuantitas Retur" name="kuantitas" value="" required="" aria-required="true">
								</div>
							</div>
                            <div class="col-md-3">
                                <select id="opsisatuan" name="opsisatuan" class="form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1">KECIL</option>
									<option value="2">BESAR</option>
								</select>
                            </div>
						</div>
						<div class="form-group c" id="f_hargabeli">
							<label class="col-md-3 control-label" for="kuantitas_pesan">Nominal Retur</label>
							<div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input readonly type="text" class="form-control decimal" id="nominal_retur" placeholder="Nominal" name="nominal_retur" value="" required="" aria-required="true">
								</div>
							</div>
							
						</div>
                        <div class="form-group c" id="f_nobatch">
                			<label class="col-md-3 control-label">Jenis Pengembalian</label>
                			<div class="col-md-6">
                				<select id="jenis_pengembalian" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                    <option value="0">Choose one..</option>
									<?php if (UserAccesForm($user_acces_form,array('1146'))){ ?>
                                    <option value="1">Retur</option>
									<?}?>
									<?php if (UserAccesForm($user_acces_form,array('1147'))){ ?>
                                    <option value="2">Pemusnahan</option>
									<?}?>
                                </select>
                			</div>
                		</div>
                       
                        <div class="form-group c" id="f_nobatch">
                			<label class="col-md-3 control-label">Pilihan Retur</label>
                			<div class="col-md-6">
                				<select id="jenis_retur" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                    <option value="0">Choose one..</option>
									<?php if (UserAccesForm($user_acces_form,array('1148'))){ ?>
                                    <option value="1">Ganti Uang</option>
									<?}?>
									<?php if (UserAccesForm($user_acces_form,array('1149'))){ ?>
                                    <option value="2">Ganti Barang</option>
									<?}?>
									<?php if (UserAccesForm($user_acces_form,array('1150'))){ ?>
                                    <option value="3">Ganti Barang Lain</option>
									<?}?>
                                </select>
                			</div>
                		</div>
						 <div class="form-group c" id="f_kuantitas">
							<label class="col-md-3 control-label" for="kuantitas">Alasan Retur</label>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-edit"></i></span>
									<input type="text" class="form-control" id="alasan" placeholder="Alasan Retur" name="alasan" value="" required="" aria-required="true">
								</div>
							</div>
						</div>
                	</form> 
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                <button class="btn btn-sm btn-primary" id="submit-form_detail" type="button"><i class="fa fa-check"></i> Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modalAdd" role="dialog" aria-hidden="true" aria-labelledby="modalAdd">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Pilih barang pengganti Retur</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal" id="form_detail">
                        <input type="hidden" id="rowIndex">
                        <input type="hidden" id="idbarang">
                        <input type="hidden" id="t_harga_beli">
                        <input type="hidden" id="t_harga_beli_besar">

                        <div class="form-group c" id="f_idtipe">
                            <label class="col-md-3 control-label">Nama Barang</label>
                            <div class="col-md-6">
                                <select id="ganti_idbarang" class="form-control" style="width: 100%;" data-placeholder="Choose one.."></select>
                            </div>
                        </div>

                        <div class="form-group c" id="f_namabarang">
                            <label class="col-md-3 control-label" for="ganti_tipe">Tipe</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                    <input readonly type="text" class="form-control" id="ganti_tipe" placeholder="Tipe" name="namabarang" value="" required="" aria-required="true">
                                    <input type="hidden" id="ganti_idtipe" name="ganti_idtipe" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group c" id="f_hargabeli">
                            <label class="col-md-3 control-label" for="ganti_hargabeli">Harga Beli</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                    <input type="text" class="form-control decimal" id="ganti_hargabeli" placeholder="Harga Beli" name="ganti_hargabeli" value="" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
						<div class="form-group c" id="f_kuantitas">
                            <label class="col-md-3 control-label" for="opsisatuan2">Satuan</label>
                            <div class="col-md-6">
                                <select id="opsisatuan2" name="opsisatuan2" class="form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="2">BESAR</option>
									<option value="1">KECIL</option>
								</select>
                            </div>
                        </div>
                        <div class="form-group c" id="f_kuantitas">
                            <label class="col-md-3 control-label" for="ganti_kuantitas">Kuantitas</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                    <input type="text" class="form-control number" id="ganti_kuantitas" placeholder="Kuantitas" name="ganti_kuantitas" value="" required="" aria-required="true" autocomplete="off">
                                    <input type="hidden" id="ganti_rowIndex" name="ganti_rowIndex" value="">
                                    
                                </div>
                            </div>
                        </div>
						<div class="form-group c" id="f_hargabeli">
                            <label class="col-md-3 control-label" for="ganti_tot">Total</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="text" readonly class="form-control decimal" id="ganti_tot" placeholder="Harga Beli" name="ganti_tot" value="" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="kuantitas">&nbsp;</label>
                            <div class="col-md-6">
                                <button class="btn btn-sm btn-success pull-right" type="button" id="tambahBarang">Tambahkan</button>
                            </div>
                        </div>

                        <table class="table table-bordered" id="tempPemesanan">
                            <thead>
                                <tr>
                                    <th>Tipe</th>
                                    <th>Barang</th>
                                    <th>Harga</th>
                                    <th>Kuantitas</th>
                                    <th>Satuan</th>
                                    <th>Total</th>
                                    <th>Total</th>
                                    <th>idtipe</th>
                                    <th>idbarang</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" id="tutup" data-dismiss="modal">Tutup</button>
                <button class="btn btn-sm btn-success" type="button" id="ganti_simpan">Simpan</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="modalView" role="dialog" aria-hidden="true" aria-labelledby="modalView">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Barang Pengganti Retur</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal" id="form_detail">
                        

                        <table class="table table-bordered" id="tempView">
                            <thead>
                                <tr>
                                    <th>Tipe</th>
                                    <th>Barang</th>
                                    <th>Harga</th>
                                    <th>Kuantitas</th>
                                    <th>Satuan</th>
                                    <th>Total</th>
                                    
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" id="tutup" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
var gettipe = ["Tipe Barang","Alkes", "Implan", "Obat", "Logistik"];
var getpengembalian = ["Jenis Pengembalian","Retur", "Pemusnahan"];
var getretur = ["","Ganti Uang", "Ganti Barang", "Ganti Barang Lain"];
var table;
var t;
var v;
$(".number").number(true,0,'.',',');
$(".decimal").number(true,2,'.',',');
$("#opsisatuan").select2();
$("#opsisatuan2").select2();
t = $('#tempPemesanan').DataTable({
        paging: false,
        info: false,
        sort: false,
        searching: false,
        destroy: true,
        autoWidth: false,
        columnDefs: [{ "targets": [6, 7,8 ], "visible": false },
		{
            "render": function (data, type, row) {
                 return formatNumber(data);
            },
            "targets": [2,3,5]
        }]
    })
	v = $('#tempView').DataTable({
        paging: false,
        info: false,
        sort: false,
        searching: false,
        destroy: true,
        autoWidth: false,
        columnDefs: [
		{
            "render": function (data, type, row) {
                 return formatNumber(data);
            },
            "targets": [2,3,5]
        }]
    })

$(document).on('click','#detail_pengembalian_add', function(){
   
    clear_form();
    $("#jenis_retur").select2({disabled:true});
    $("#alasan").prop({disabled:true});
})

$(document).on('submit','#form-work', function(){
   
    var detail_tbl = $('table#detail_list tbody tr').get().map(function(row) {
        return $(row).find('td').get().map(function(cell) {
            if($(cell).hasClass('json')){
                if($(cell).html() != ''){
                    return JSON.parse($(cell).html());
                }else{
                    return '';
                }
            }else{
                return $(cell).html();
            }
        });
    });
    // console.log(detail_tbl);
    // return false;
    $("#detail_value").val(JSON.stringify(detail_tbl))
    
})

$(document).on( 'click', '#submit-form_detail', function(){
    
    
    if($("#nobatch").val() == '' || $("#nobatch").val() == null){
        $.toaster({priority : 'danger', title : 'Error!', message : 'No Batch Belum diisi.. '});
        return false;
    }
    if($("#kuantitas").val() == '' || $("#kuantitas").val() == '0'){
        $.toaster({priority : 'danger', title : 'Error!', message : 'Kuantitas Belum diisi.. '});
        return false;
    }
    if($("#jenis_pengembalian").val() == '' || $("#jenis_pengembalian").val() == null){
        $.toaster({priority : 'danger', title : 'Error!', message : 'Jenis Pengembalian Belum diisi.. '});
        return false;
    }
    if($("#noterima").val() == '' || $("#noterima").val() == null){
        $.toaster({priority : 'danger', title : 'Error!', message : 'No Penerimaan Belum diisi.. '});
        return false;
    }
    
	if ($("#jenis_pengembalian").val()=='1'){
		if($("#jenis_retur").val() == '' || $("#jenis_retur").val() == null){
			$.toaster({priority : 'danger', title : 'Error!', message : 'Pilih Retur Belum diisi.. '});
			return false;
		}
		if($("#alasan").val() == '' || $("#alasan").val() == null){
			$.toaster({priority : 'danger', title : 'Error!', message : 'Alasan Retur Belum diisi.. '});
			return false;
		}
	}
    
    if($("#jenis_retur").val() == '3'){
        $('#modalAdd .block-title').text('Pilih barang pengganti Retur');
        $('#modalAdd').modal('toggle');
    }else{
        add_data_detail();
        clear_form();
        $.toaster({priority : 'success', title : 'Sukses!', message : 'Data berhasil ditambahkan ... '});
    }
    $('#modal_detail_pengembalian').modal('toggle');
})
$(document).on( 'click', '.detail', function(){
   $row = $(this).closest('tr');
   v.clear().draw(false);
   var pengganti_list = JSON.parse($row.find("td:eq(19)").html());
    pengganti_list.forEach(function(row){
        v.row.add([
                row[0],
                row[1],
                row[2],
                row[3],
                row[4],
                row[5],
                "<a href='#' class='on-default detailEdit' title='Edit'><i class='fa fa-pencil'></i></a>&nbsp;"+
                "<a href='#' class='on-default detailRemove' title='Hapus'><i class='fa fa-trash-o'></i></a>",

            ]).draw(false)    
        
    });
    $('#modalView').modal('toggle');
});
$(document).on("click",".detail_edit",function(){
    $row = $(this).closest('tr');
    $("#rowindex").val($row[0].sectionRowIndex);
	var idbarang=$row.find("td:eq(14)").html();
	var idtipe=$row.find("td:eq(10)").html();
	var nobatch=$row.find("td:eq(3)").html();
	$.ajax({
		url: '{site_url}tgudang_pengembalian/getPenerimaan/',
		method: 'POST',
		data: { idbarang: idbarang,idtipe:idtipe,nobatch:nobatch},
		dataType: 'json',
		success: function(data) {
			dataoption = '<option value"">Pilih No Penerimaan..<option>';
			$.each(data, function(i,item){
				dataoption += '<option value="'+item.id+'">No. '+ item.nopenerimaan + '|' + item.tanggalpenerimaan + ' | ' + item.nama_distributor  +'</option>';
			})    
			
			$('#noterima').empty().append(this,dataoption).selectpicker('refresh');
			$('#noterima').val($row.find("td:eq(21)").html()).trigger('change');
		}
	}) 
	
    $('#nobatch').empty().append('<option selected value="'+ $row.find("td:eq(21)").html() +'">'+ $row.find("td:eq(1)").html() +' - '+$row.find("td:eq(3)").html()+'</option>').selectpicker('refresh')
    
    $('#kuantitas').val($row.find("td:eq(24)").html());
    $('#jenis_pengembalian').select2('val',$row.find("td:eq(16)").html());
    $('#alasan').val($row.find("td:eq(7)").html());
    $('#jenis_retur').select2('val',$row.find("td:eq(17)").html());
    $('#opsisatuan').val($row.find("td:eq(23)").html()).trigger('change');
	
    $('#idpenerimaandetail').val($row.find("td:eq(21)").html());
    // $('#expireddate').val($row.find("td:eq(21)").html());
	var json=$row.find("td:eq(19)").html();
	// alert($row.find("td:eq(19)").html());
	if (json){
		t.clear().draw(false);
		var pengganti_list = JSON.parse(json);
		pengganti_list.forEach(function(row){
			t.row.add([
					row[0],
					row[1],
					row[2],
					row[3],
					row[4],
					row[5],
					row[6],
					row[7],
					row[8],
					"<a href='#' class='on-default detailEdit' title='Edit'><i class='fa fa-pencil'></i></a>&nbsp;"+
					"<a href='#' class='on-default detailRemove' title='Hapus'><i class='fa fa-trash-o'></i></a>",

				]).draw(false)    
			
		});
	}
    $('#modal_detail_pengembalian').modal('toggle');
});
function add_data_detail()
{
   var table = "";
   var satuan;
   if ($("#opsisatuan").val()=='1'){
	   satuan=$("#satuan_kecil").val();
   }else{	   
	   satuan=$("#satuan_besar").val();
   }
    table += "<td>"+$("#tipe").val()+"</td>"; //0
    table += "<td>"+$("#namabarang").val()+"</td>"; //1
    table += "<td>"+$("#namadistributor").val()+"</td>"; //2
    table += "<td>"+$('#xnobatch').val()+"</td>"; //3
    table += "<td>"+formatNumber($("#hargabeli").val())+"</td>"; //4
    table += "<td>"+$("#kuantitas").val()+'  ('+satuan+' | '+$("#opsisatuan option:selected").text()+')'+"</td>"; //5
    table += "<td>"+getpengembalian[$("#jenis_pengembalian").val()]+"</td>"; //6
    table += "<td>"+$("#alasan").val()+"</td>"; //7
	if ($("#jenis_retur").val()=='3'){		
    table += '<td><button class="btn btn-info btn-xs detail" type="button"><i class="fa fa-search"></i> '+getretur[$("#jenis_retur").val()]+'</button></td>'; //8
	}else{		
    table += "<td>"+getretur[$("#jenis_retur").val()]+"</td>"; //8
	}
    table += "<td>"+formatNumber($("#nominal_retur").val())+"</td>"; //9
    table += "<td hidden>"+ $("#idtipe").val() +"</td>"; //10
    table += "<td hidden>"+ $("#nopenerimaan").val() +"</td>"; //11
    table += "<td hidden>"+ $("#iddistributor").val() +"</td>"; //12
    table += "<td hidden>"+ $("#noterima").val() +"</td>"; //13
    table += "<td hidden>"+ $("#idbarang").val() +"</td>"; //14
    table += "<td hidden>"+ $("#kuantitas_pesan").val() +"</td>"; //15
    table += "<td hidden>"+ $("#jenis_pengembalian").val() +"</td>"; //16
    table += "<td hidden>"+ $("#jenis_retur").val() +"</td>"; //17
    table += "<td hidden>"+ $("#tgl_terima").val() +"</td>"; //18
    table += "<td hidden class=\"json\">"+ $("#pengganti").val() +"</td>"; //19
    table += "<td><a href='#' class='on-default detail_edit' title='Edit'><i class='fa fa-edit'></i></a>&nbsp;&nbsp;<a href='#' class='on-default detail_remove' title='Remove'><i class='fa fa-trash-o'></i></a></td>"; //19
    table += "<td hidden>"+$("#idpenerimaandetail").val()+"</td>"; //21
    table += "<td hidden>"+$("#expireddate").val()+"</td>"; //22
    table += "<td hidden>"+$("#opsisatuan").val()+"</td>"; //23
    table += "<td hidden>"+$("#kuantitas").val()+"</td>"; //24
    table += "<td hidden>"+$("#idpenerimaan").val()+"</td>"; //25
    if($("#rowindex").val() != ''){
        $('#detail_list tbody tr:eq(' + $("#rowindex").val() + ')').html(table);
    }else{
        table = "<tr>" + table + "</tr>";
        $('#detail_list tbody').append(table);
    } 
    
}
function formatNumber (num) {
return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

$(document).on( 'click', '#tambahBarang', function(){    

    if(validate()) {
        if($('#tambahBarang').text() == 'Tambahkan') {
            t.row.add([
                $('#ganti_tipe').val(),//0
                $('#ganti_idbarang :selected').text(),//1
                $('#ganti_hargabeli').val(),//2
                $('#ganti_kuantitas').val(),//3
                $('#opsisatuan2 option:selected').text(),//4
				$('#ganti_tot').val(),//5
                $('#ganti_idtipe').val(),//6
                $('#ganti_idbarang').val(),//7
                $('#opsisatuan2').val(),//8
                "<a href='#' class='on-default detailEdit' title='Edit'><i class='fa fa-pencil'></i></a>&nbsp;"+
                "<a href='#' class='on-default detailRemove' title='Hapus'><i class='fa fa-trash-o'></i></a>",
            ]).draw(false)
        } else {
            t.row( $('#ganti_rowIndex').val() ).data([
                $('#ganti_tipe').val(),
                $('#ganti_idbarang :selected').text(),
                $('#ganti_hargabeli').val(),
                $('#ganti_kuantitas').val(),
				$('#opsisatuan2 option:selected').text(),
                $('#ganti_tot').val(),
                $('#ganti_idtipe').val(),
                $('#ganti_idbarang').val(),
				$('#opsisatuan2').val(),
                "<a href='#' class='on-default detailEdit' title='Edit'><i class='fa fa-pencil'></i></a>&nbsp;"+
                "<a href='#' class='on-default detailRemove' title='Hapus'><i class='fa fa-trash-o'></i></a>",
            ]).draw(false)
        }
        $('#tambahBarang').text('Tambahkan').removeClass('btn-warning').addClass('btn-success')
        $('#ganti_tipe,#ganti_hargabeli,#ganti_kuantitas,#t_harga_beli,#t_harga_beli_besar,#ganti_tot').val('');
        $('#opsisatuan2').val('2').trigger('change');
        $('#ganti_idbarang').val('').trigger('change');
        return true;
    } else {
        return false;
    }

})

$(document).on("click",".detail_remove", function() {
    $(this).closest('tr').remove();
    // detail_value()

    $.toaster({priority : 'success', title : 'Sukses!', message : 'Data berhasil dihapus'});
})




$(document).on( 'change', '#jenis_pengembalian', function(){
    if($(this).val() == 2){
        $("#jenis_retur").select2({disabled:true});
        $("#alasan").prop({disabled:true});
        
    }else if($(this).val() == 1){
        $("#jenis_retur").select2({disabled:false});
        $("#alasan").prop({disabled:false});
    }
    
});
$(document).on( 'change', '#opsisatuan', function(){
   get_nominal();
    
});
$(document).on( 'change', '#opsisatuan2', function(){
   get_nominal2();
    
});

$(document).on( 'keyup', '#kuantitas', function(){
    get_nominal();
    
});
$(document).on( 'keyup', '#ganti_kuantitas', function(){
    get_nominal2();
    
});
function get_nominal(){
	var nominal=0;
	var harga_kecil=0;
	var harga_besar=0;
	if ($("#opsisatuan_asal").val()=='1'){
		harga_kecil=$("#harga_asal").val();
		harga_besar=parseFloat($("#harga_asal").val()*parseFloat($("#jumlah_besar").val()));
	}else{
		harga_kecil=parseFloat($("#harga_asal").val()) / parseFloat($("#jumlah_besar").val());
		harga_besar=parseFloat($("#harga_asal").val());
	}
	if ($("#opsisatuan").val()=='1'){
		$("#nominal_retur").val(harga_kecil * parseFloat($("#kuantitas").val()));
	}else{
		$("#nominal_retur").val(harga_besar * parseFloat($("#kuantitas").val()));
	}
}
function get_nominal2(){
	var nominal=0;
	var harga_kecil=$("#t_harga_beli").val();
	var harga_besar=$("#t_harga_beli_besar").val();
	
	if ($("#opsisatuan2").val()=='1'){
		$("#ganti_tot").val(harga_kecil * parseFloat($("#ganti_kuantitas").val()));
		$("#ganti_hargabeli").val(harga_kecil);
		
	}else{
		$("#ganti_tot").val(harga_besar * parseFloat($("#ganti_kuantitas").val()));
		$("#ganti_hargabeli").val(harga_besar);
	}
}
$(document).on( 'change', '#nobatch', function(){
	var idbarang;
	var idtipe;
	var nobatch;
	if ($("#rowindex").val()==''){	
	// alert($(this).val());
		
		if ($(this).val()){
			$.ajax({
				url: '{site_url}tgudang_pengembalian/get_data_barang/'+$(this).val(),
				method: 'GET',
				dataType: 'json',
				success: function(data) {
					idbarang=data.idbarang;
					idtipe=data.idtipe;
					nobatch=data.nobatch;
					$.ajax({
						url: '{site_url}tgudang_pengembalian/getPenerimaan/',
						method: 'POST',
						data: { idbarang: idbarang,idtipe:idtipe,nobatch:nobatch},
						dataType: 'json',
						success: function(data) {
							dataoption = '<option value"">Pilih No Penerimaan..<option>';
							$.each(data, function(i,item){
								dataoption += '<option value="'+item.id+'">No. '+ item.nopenerimaan + '|' + item.tanggalpenerimaan + ' | ' + item.nama_distributor  +'</option>';
							})    
							
							$('#noterima').empty().append(this,dataoption).selectpicker('refresh')
						}
					}) 
					
				}
			})    
			
		}	
	}
	$("#tipe").val('');
	$("#namabarang").val('');
	$("#hargabeli").val('');
	$("#kuantitas_pesan").val('');
	$("#satuan").val('');
})

$(document).on( 'change', '#noterima', function(){    
    $.ajax({
        url: '{site_url}tgudang_pengembalian/getPenerimaanDetail/',
        method: 'POST',
        data: { id: $(this).val()},
        dataType: 'json',
        success: function(data) {
            $('#idpenerimaan').val(data.id);
            $('#nopenerimaan').val(data.nopenerimaan);
            $('#iddistributor').val(data.iddistributor);
            $('#namadistributor').val(data.nama_distributor);
            $('#tipe').val(gettipe[data.idtipe]);
            $('#idtipe').val(data.idtipe);
            $('#namabarang').val(data.nama);
            $('#hargabeli').val(data.harga);
            $('#kuantitas_pesan').val(data.kuantitas);
            $('#tgl_terima').val(data.tanggalpenerimaan);
            $('#idbarang').val(data.idbarang);
            $("#idpenerimaandetail").val(data.detail_id);
            $("#expireddate").val(data.expired_date);
            $("#satuan").val(data.satuan+' | '+data.jenis_satuan);
            $("#harga_master").val(data.harga_master);
            $("#harga_asal").val(data.harga);
            $("#jumlah_besar").val(data.jumlahsatuanbesar);
            $("#satuan_besar").val(data.satuan_besar);
            $("#satuan_kecil").val(data.satuan_kecil);
			$('#kuantitas').focus();
			$("#opsisatuan_asal").val(data.opsisatuan);
			if ($("#rowindex").val()==''){
				// alert('ROW INDEX'+$("#rowindex").val());
				
				$("#opsisatuan").val(data.opsisatuan).trigger('change');
				if (data.opsisatuan=='1'){
					 $("#opsisatuan").select2({disabled:true});
				}else{
					 $("#opsisatuan").select2({disabled:false});
					
				}
			}
            $("#xnobatch").val(data.nobatch);
        }
    })    
});

$(document).on('keyup','#kuantitas', function(){
	var kuantitas_max_kecil;
	var kuantitas_max_besar;
	var kuantitas=$(this).val();
	if ($("#opsisatuan_asal").val()=='1'){//KECIL
		kuantitas_max_kecil=$("#kuantitas_pesan").val();
		kuantitas_max_besar=$("#kuantitas_pesan").val();
	}else{
		kuantitas_max_kecil=$("#kuantitas_pesan").val()*$('#kuantitas_pesan').val();
		kuantitas_max_besar=$("#kuantitas_pesan").val();
	}
	// console.log('Max kecil :'+kuantitas_max_kecil + ' Maks Besar :'+kuantitas_max_besar);
	if ($("#opsisatuan").val()=='1'){//KECIL
		if (parseFloat(kuantitas)>parseFloat(kuantitas_max_kecil)){
			sweetAlert("Maaf...", "Kuantitas Retur  Lebih Besar Dari Terima!", "error");
			$("#kuantitas").val(kuantitas_max_kecil);
            return false;
		}
	}else{
		if (parseFloat(kuantitas)>parseFloat(kuantitas_max_besar)){
			sweetAlert("Maaf...", "Kuantitas Retur BESAR Lebih Besar Dari Terima!", "error");
			$("#kuantitas").val(kuantitas_max_besar);
            return false;
		}
	}
})
$(document).on( 'click', '#ganti_simpan', function(){
    if(!t.data().count()) {
        $.toaster({priority : 'danger', title : 'Error!', message : 'Barang pengganti tidak boleh kosong'});
        return false;
    }
    
    var ganti_tbl = t.rows().data().toArray();
    
    var ganti_tbl = ganti_tbl.map(function(val){
        return val.slice(0, -1);
    });
    $("#pengganti").val(JSON.stringify(ganti_tbl));
    add_data_detail();
    t.clear().draw();
    $('#modalAdd').modal('toggle');
});

$(document).on( 'change', '#ganti_idbarang', function(){
	if ($(this).val()){
    data=$(this).select2('data')[0];
	// alert(data);
    $('#ganti_tipe').val(gettipe[data.idtipe]);
    $('#ganti_idtipe').val(data.idtipe);
    $('#t_harga_beli').val(data.hargabeli);
    $('#t_harga_beli_besar').val(data.hargabelibesar);
	if ($("#opsisatuan2").val()=='1') {
		$('#ganti_hargabeli').val(data.hargabeli);
	}else{
		$('#ganti_hargabeli').val(data.hargabelibesar);
	}
    // $('#ganti_hargabeli').val(data.hargabeli);
    // $('#opsi').val(data.hargabeli);
    setTimeout(function(){
    $('#ganti_kuantitas').focus();
    });
	}

});

$('#tempPemesanan tbody').on( 'click', 'a.detailEdit', function () {
    $('#tambahBarang').text('Sunting').removeClass('btn-success').addClass('btn-warning')

    var rowIndex = t.row($(this).parents('tr')).index()
                    
    $('#ganti_rowIndex').val( rowIndex )
    $('#ganti_tipe').val( t.cell(rowIndex,0).data() )
    // $('#ganti_idbarang').empty().append('<option selected value="'+ t.cell(rowIndex,5).data() +'">'+ t.cell(rowIndex,1).data() +'</option>').selectpicker('refresh')
	$('#ganti_idbarang').val(t.cell(rowIndex,7).data()).trigger('change');
   $('#ganti_hargabeli').val( t.cell(rowIndex,2).data() )
    $('#ganti_kuantitas').val( t.cell(rowIndex,3).data() )
    $('#ganti_tot').val( t.cell(rowIndex,5).data() )
    $('#ganti_idtipe').val( t.cell(rowIndex,4).data() )
})

$('#tempPemesanan tbody').on( 'click', '.detailRemove', function () {
    t.row($(this).parents('tr')).remove().draw()

    $('#tambahBarang').text('Tambahkan').removeClass('btn-warning').addClass('btn-success');
    $('#ganti_tipe,#ganti_hargabeli,#ganti_kuantitas').val('');
    $('#ganti_idbarang').val('').trigger('change');
})

$(document).on('click','#btnback', function(){
    clear_form();
})

function clear_form()
{
    $('#rowindex').val('');
    $('#nopenerimaan').val('');
    $('#iddistributor').val('');
    $('#namadistributor').val('');
    $('#tipe').val('');
    $('#idtipe').val('');
    $('#namabarang').val('');
    $('#hargabeli').val('');
    $('#kuantitas_pesan').val('');
    $('#alasan').val('');
    $('#kuantitas').val('');
    $('#noterima').empty();
    $('#nobatch').val('').trigger('change');
    $('#jenis_pengembalian').val('').trigger('change');
    $('#jenis_retur').val('').trigger('change');
    $('#pengganti').val('');
    $('#idpenerimaandetail').val('');
    $('#expireddate').val('');
    t.clear().draw();
}

function detail_value() {
    var detail_tbl = $('table#detail_list tbody tr').get().map(function(row) {
        return $(row).find('td').get().map(function(cell) {
            return $(cell).html();
        });
    });

    $("#detail_value").val(JSON.stringify(detail_tbl))
}

function validate() {
	// alert($('#ganti_idbarang').val());
    if($('#ganti_idbarang').val() == '' || $('#ganti_idbarang').val() == null) {
		// alert('NULL');
        $.toaster({priority : 'danger', title : 'Error!', message : 'Barang tidak boleh kosong'});
        return false;
    }
    if($('#ganti_hargabeli').val() == '' || $('#ganti_hargabeli').val() == '0') {
        $.toaster({priority : 'danger', title : 'Error!', message : 'Harga tidak boleh kosong'});
        return false;
    }
    if($('#ganti_kuantitas').val() == '' || $('#ganti_kuantitas').val() == '0') {
        $.toaster({priority : 'danger', title : 'Error!', message : 'Kuantitas tidak boleh kosong'});
        return false;
    }

    return true;
}


$(document).ready(function() {
    $("#jenis_retur").select2({disabled:true});
    $("#alasan").prop({disabled:true});
    $("#nobatch").select2({
        minimumInputLength: 2,
        ajax: {
            url: '{site_url}tgudang_pengembalian/getNoBatchProduk/',
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            data: function (params) {
              var query = {
                search: params.term,                 
              }
              return query;
            }, 
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
							console.log(item.id);
                        return {
                            text: item.nama + " - "+ item.nobatch,
                            id: item.id
                        }
                    })
                };
            }
        }
    });
    
    $("#ganti_idbarang").select2({
        minimumInputLength: 2,
        ajax: {
            url: '{site_url}tgudang_pengembalian/getProductList/',
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            data: function (params) {
              var query = {
                search: params.term,                 
              }
              return query;
            }, 
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nama,
                            id: item.id,
                            idtipe: item.idtipe,
                            hargabeli: item.hargabeli,
                            hargabelibesar: item.hargabeli_besar
                        }
                    })
                };
            }
        }
    });
    // $.toaster({priority : 'success', title : 'Sukses!', message : 'Data berhasil dihapus'});
   
});
	

</script>
