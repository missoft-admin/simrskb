<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('1144'))){ ?>
<div class="block">
	<div class="block-header">
        <?php if (UserAccesForm($user_acces_form,array('1145'))){ ?>
    		<ul class="block-options">
                <li><a href="{site_url}tgudang_pengembalian/create" class="btn btn-default"><i class="fa fa-plus"></i> Tambah</a></li>
            </ul>
        <?php } ?>
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:25px">
        <div class="row">
            <?php echo form_open('tgudang_pengembalian/filter', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">No. Pengembalian</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_trx_pengembalian" placeholder="No. Pengembalian" name="no_trx_pengembalian" value="{no_trx_pengembalian}">
                    </div>
                </div>
				
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Distributor</label>
                    <div class="col-md-8">
                        <select id="iddistributor" name="iddistributor" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#">Semua Distributor</option>
                            <?php foreach ($list_distributor as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?=($iddistributor == $row->id ? 'selected' : ''); ?>><?php echo $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Status <?=$statuspenerimaan?></label>
                    <div class="col-md-8">
                        <select id="status" name="statuspenerimaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#" <?=('#' == $statuspenerimaan ? 'selected' : ''); ?>>Semua Status</option>
                            <option value="1" <?=(1 == $statuspenerimaan ? 'selected' : ''); ?>>PENDING</option>
                            <option value="2" <?=(2 == $statuspenerimaan ? 'selected' : ''); ?>>SUDAH DIPROSES</option>
                            <option value="3" <?=(3 == $statuspenerimaan ? 'selected' : ''); ?>>SUDAH SELESAI</option>
                            <option value="0" <?=('0' == $statuspenerimaan ? 'selected' : ''); ?>>DIBATALKAN</option>
                        </select>
                    </div>
                </div>
				
                
            </div>
			<div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="jenis">Jenis Pengembalian</label>
                    <div class="col-md-8">
                        <select id="jenis" name="jenis" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#">Semua </option>
                            <option value="1" <?=(1 == $jenis ? 'selected' : ''); ?>>RETUR</option>
                            <option value="2" <?=(2 == $jenis ? 'selected' : ''); ?>>PEMUSNAHAN</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="jenis_retur">Jenis Retur</label>
                    <div class="col-md-8">
                        <select id="jenis_retur" name="jenis_retur" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#">Semua </option>
                            <option value="1" <?=(1 == $jenis_retur ? 'selected' : ''); ?>>GANTI UANG</option>
                            <option value="2" <?=(2 == $jenis_retur ? 'selected' : ''); ?>>GANTI BARANG SAMA</option>
                            <option value="3" <?=(3 == $jenis_retur ? 'selected' : ''); ?>>GANTI BARANG LAINNYA</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="yyyy-mm-dd">
                            <input class="form-control" type="text" name="tanggaldari" placeholder="From" value="{tanggaldari}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" name="tanggalsampai" placeholder="To" value="{tanggalsampai}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
				<tr>
					<th>ID</th>
					<th width="5%">#</th>
					<th width="8%">No Pengembalian</th>
					<th width="10%">Tanggal</th>
					<th width="15%">Distributor</th>
					<th width="8%">Pengembalian</th>
                    <th width="10%">Jenis Retur</th>
					<th width="10%">Alasan</th>
					<th width="8%">Nominal Retur</th>
					<th width="8%">User </th>
					<th width="8%">Status</th>
					<th width="10%">Option</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
</div>
<?}?>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;

$(document).ready(function(){
    // table = $('#index_list').DataTable({
        // "pageLength": 50,  
        // "serverSide": true, 
        // "order": [],
        // "ajax": { "url": '{site_url}tgudang_pengembalian/ajax_list', "type": "POST" },
        // "columns": [
            // {"data": "id", visible: false},
            // {"data": "nopengembalian"},
            // {"data": "nama"},
            // {"data": "totalbarang", "render": $.fn.DataTable.render.number('.',',') },
            // {"data": "totalharga", "render": $.fn.DataTable.render.number('.',',') },
            // {"data": "alasan"},
            // {"data": "status"},
            // {"data": "option"},
        // ],
        // "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

            // var action = '<div class="btn-group" data-toggle="buttons">';
            // action += link_action('info','view','tgudang_pengembalian/view/'+aData['id'],'glyphicon glyphicon-search','View');
            // <?php if (button_roles('tgudang_pengembalian/print_pengembalin')): ?>
                // action += link_action('default','print','tgudang_pengembalian/print_pengembalin/'+aData['id'],'fa fa-print','Print'); 
            // <?php endif ?>
            // // if(aData['status'] == 1) 
                // // action += link_action('success','acc','tgudang_pengembalian/acc/'+aData['id'],'glyphicon glyphicon-check','ACC');
            // if(aData['status'] == 1 || aData['status'] == 2)
                // <?php if (button_roles('tgudang_pengembalian/print_pengembalin')): ?>
                    // action += link_action('danger','cancel','tgudang_pengembalian/cancel/'+aData['id'],'fa fa-times','Cancel');
                // <?php endif ?>
            
            // action += '</div>';

            // if(aData['status'] == 1){
                // $("td:eq(5)", nRow).html('<span class="label label-default">Pending</span>');
                // $("td:eq(6)", nRow).html(action);
            // }else if(aData['status'] == 2) {
                // $("td:eq(5)", nRow).html('<span class="label label-info">Sudah Dipesan</span>');
                // $("td:eq(6)", nRow).html(action);
            // } else if(aData['status'] == 3) {
                // $("td:eq(5)", nRow).html('<span class="label label-warning">Penerimaan</span>');                
                // $("td:eq(6)", nRow).html(action);
            // } else if(aData['status'] == 4) {
                // $("td:eq(5)", nRow).html('<span class="label label-success">Selesai</span>');                
                // $("td:eq(6)", nRow).html(action);
            // } else {
                // $("td:eq(5)", nRow).html('<span class="label label-danger">Dibatalkan</span>');                
                // $("td:eq(6)", nRow).html(action);
            // }
            // return nRow;            
        // }        
    // })
	load_index();
})
function load_index(){
	// alert('SINI');
	table = $('#index_list').DataTable({
		autoWidth: false,
		searching: false,
		pageLength: 50,
		serverSide: true,
		"order": [],
		"pageLength": 10,
		"ordering": false,		
		columnDefs: [{ "targets": [0], "visible": false },
						 {"targets": [1], className: "text-center" },
						 {"targets": [2], className: "text-center" },
						 {"targets": [3], className: "text-left" },
						 {"targets": [4], className: "text-left" },
						 {"targets": [5], className: "text-center" },
						 {"targets": [7], className: "text-left" },
						 {"targets": [8], className: "text-right" },
						 {"targets": [9], className: "text-center" },
						 {"targets": [10], className: "text-right" }
						 ],
		ajax: { 
			url: '{site_url}tgudang_pengembalian/get_index/', 
			type: "POST" ,
			dataType: 'json'
		}
	});
}

$(document).on('click','.view,.print', function(){
	var url = $(this).attr('url')
	window.location = url
})

$(document).on('click','.acc', function(){
    // var url = $(this).attr('url')
    // $.ajax({
        // url: url,
        // success: function(data) {
        	// table = $('#index_list').DataTable().ajax.reload()
            // sweetAlert("Sukses!", 'Data berhasil diacc', "success");
        // }
    // })
})

$(document).on('click','.cancel', function(){
	var href = $(this).attr("href");
                // alert($(this).attr("href"));
	// return false;
    if(confirm('Cancel Retur ?')){
        var url = href
        $.ajax({
            url: url,
            success: function(data) {
				// alert(data);
                table = $('#index_list').DataTable().ajax.reload()
                sweetAlert("Sukses!", 'Data berhasil dibatalkan', "success");
            }
        })
    }
})


function link_action(btn,id,url,icon,tooltip='') {
    $html = '<button class="btn btn-'+btn+' btn-xs '+id+'" data-toggle="tooltip" data-placement="top" title="'+tooltip+'" url="./'+url+'"><i class="'+icon+'"></i></button>';
    return $html;
}	
</script>
