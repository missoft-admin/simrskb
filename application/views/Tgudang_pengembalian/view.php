<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>

    <?php echo form_open('tgudang_pengembalian/verifikasi/'.$id, 'class="form-horizontal push-10-t" id="form-work"') ?>
    <div class="block-content block-content-narrow">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-3 control-label">No Pengembalian</label>
                    <div class="col-md-9">
                        <input type="hidden" class="form-control" name="idpenerimaan" id="idpenerimaan" value="{idpenerimaan}" readonly>
                        <input type="hidden" class="form-control" name="id" id="id" value="{id}" readonly>
                        <input type="text" class="form-control" name="nopengembalian" id="nopengembalian" value="{nopengembalian}" readonly>
                        <input type="hidden" class="form-control" name="xjenis" id="xjenis" value="{jenis}" readonly>
                        <input type="hidden" class="form-control" name="xjenis_retur" id="xjenis_retur" value="{jenis_retur}" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Distributor</label>
                    <div class="col-md-9">
                        <input class="form-control" readonly type="text" name="nama_distributor" id="nama_distributor" value="{nama_distributor}">
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-md-3 control-label">Tanggal</label>
                    <div class="col-md-9">
                        <input class="form-control" readonly type="text" name="tanggal" id="tanggal" value="<?=HumanDateLong($tanggal)?>">
                        <input class="form-control" type="hidden" name="iddistributor" id="iddistributor" value="{iddistributor}">
                    </div>
                </div>
				
                <div class="form-group">
                    <label class="col-md-3 control-label">Alasan</label>
                    <div class="col-md-9">
                        <textarea readonly class="form-control" name="alasan" id="alasan"><?=$alasan?></textarea>
                    </div>
                </div>
                
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-3 control-label">Nominal Retur</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control diskon" name="totalbarang" id="totalbarang" value="{totalharga}" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Nominal Pengganti</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control diskon" name="totalharga" id="totalharga" value="{totalpengganti}" readonly>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-3 control-label">Jenis Pengembalian</label>
                    <div class="col-md-9">
                        <select disabled name="jenis" id="jenis" class="form-control">
                            <option value="1" <?=($jenis=='1'?'selected':'')?>>Retur</option>
                            <option value="2" <?=($jenis=='2'?'selected':'')?>>Pemusnahan</option>
                        </select>
                    </div>
                </div>
				<?if ($jenis=='1'){?>
				<div class="form-group">
                    <label class="col-md-3 control-label">Jenis Retur</label>
                    <div class="col-md-9">
                        <select disabled name="jenis_retur" id="jenis_retur" class="form-control">
                            <option value="1" <?=(1 == $jenis_retur ? 'selected' : ''); ?>>GANTI UANG</option>
                            <option value="2" <?=(2 == $jenis_retur ? 'selected' : ''); ?>>GANTI BARANG SAMA</option>
                            <option value="3" <?=(3 == $jenis_retur ? 'selected' : ''); ?>>GANTI BARANG LAINNYA</option>
                        </select>
                    </div>
                </div>
				<?}?>
            </div>
            <div class="clearfix"></div>
            <hr />
			<div class="col-md-12" >
				
				<table class="table table-striped table-borderless table-header-bg" id="data-barang">
					<thead>
						<tr>
							<th width="5%" class="text-center">Tipe</th>
							<th width="20%" class="text-center">Barang</th>
							<th width="8%" class="text-center">Jumlah Retur</th>
							<th width="8%" class="text-center">Satuan</th>
							<th width="8%" class="text-center">Harga</th>
							<th width="10%" class="text-center">Total</th>
							<th width="10%" class="text-center">Expired</th>
							<th width="10%" class="text-center">No.Batch</th>
							<th width="10%" class="text-center">Jenis</th>
							
						</tr>
					</thead>
					<tbody>
						<?foreach($detail as $row){?>
							<tr>
								<td class="text-center"><?=$row->namatipe?></td><!-- 0 -->
								<td class="text-left"><?=$row->nama_barang?></td><!-- 1 -->
								<td class="text-center"><?=$row->kuantitas.' '.$row->namasatuan?></td><!-- 2 -->
								<td class="text-right"><?=$row->jenissatuan?></td><!-- 3 -->
								<td class="text-right"><?=number_format($row->harga,2)?></td><!-- 4 -->
								<td class="text-right"><?=number_format($row->tot_harga,2)?></td><!-- 5 -->								
								<td class="text-center"><?=$row->expired_date?></td><!-- 6 -->
								<td class="text-center"><?=$row->nobatch?></td><!-- 7 -->
								<td class="text-center">
									<?if ($jenis=='1' && $jenis_retur=='3'){?>
									<button class="btn btn-info btn-xs detail" type="button"><i class="fa fa-search"></i> Ganti Barang lain</button>
									<?}else{?>
										<span class="label label-success"><?=$retur_nama?></span>
									<?}?>
								</td><!-- 8 -->
								
								<td hidden><?=$row->idtipe?></td><!-- 10 -->
								<td hidden><?=$row->idbarang?></td><!-- 11 -->
								<td hidden><?=$row->opsisatuan?></td><!-- 12 -->
								<td hidden><?=$row->kuantitas?></td><!-- 13 -->
								<td hidden><?=$row->namasatuan?></td><!-- 14 -->
								<td hidden><?=$row->satuan_kecil?></td><!-- 15 -->
								<td hidden><?=$row->satuan_besar?></td><!-- 16 -->
								<td hidden><?=$row->hargabeli?></td><!-- 17 -->
								<td hidden><?=$row->hargabeli_besar?></td><!-- 18 -->
								<td hidden><?=$row->idpengembalian?></td><!-- 19 -->
								<td hidden><?=$row->id_penerimaan_detail?></td><!-- 20 -->
								<td hidden><?=$row->harga?></td><!-- 21 -->
								<td hidden><?=$row->tot_harga?></td><!-- 22 -->
								<td class="json"  hidden><?=get_detail_pengganti($row->id)?></td><!-- 23 -->
								<td hidden>0</td><!-- 24 Edited -->
								<td hidden><?=$row->id?></td><!--  25 -->
							</tr>
						
						<?					
						}?>
					</tbody>
				</table>
			</div>
			<br>
			<br>
			<br>
			<hr />
			<div class="col-md-12" id="div_hapus" hidden>
				<h3 class="block-title">DATA HAPUS & ALIHKAN DISTRIBUTOR</h3>
				<table class="table table-striped table-borderless table-header-bg" id="data_hapus">
					<thead>
						<tr>
							<th width="5%" class="text-center">Tipe</th>
							<th width="15%" class="text-center">Barang</th>
							<th width="5%" class="text-center">Jumlah</th>
							<th width="15%" class="text-center">Keterangan</th>
							<th width="15%" class="text-center">Distributor</th>
							<th width="20%" class="text-center">Alasan</th>
							
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
        </div>
		<input type="hidden"   id="pengganti" name="pengganti"/>
		<input type="hidden" id="detail_value" name="detail_value" value="[]">
        <div style="margin-top:50px;margin-bottom:20px;" class="text-right">
            <a href="{site_url}tgudang_pengembalian" class="btn btn-default btn-md">Kembali</a>
        </div>
		<div class="row">
		<div class="col-md-6">
			<h4 class="block-title">Info User</h4>
			<table class="table table-bordered" id="data_user">
				<thead>
					<tr>
						<th width="5%" class="text-left">Jenis</th>
						<th width="15%" class="text-left">Nama User</th>
						<th width="5%" class="text-left">Tanggal</th>
						
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class='text-left'>USER PEMBUAT</td>
						<td  class='text-left'><?=$nama_user_created?></td>
						<td  class='text-left'><?=($tgl_created?HumanDateLong($tgl_created):'')?></td>
					<tr>
					<tr>
						<td class='text-left'>USER VERIFIKASI</td>
						<td  class='text-left'><?=$nama_user_verifikasi?></td>
						<td  class='text-left'><?=($tgl_verifikasi?HumanDateLong($tgl_verifikasi):'')?></td>
					<tr>
					<tr>
						<td class='text-left'>USER EDIT</td>
						<td  class='text-left'><?=$nama_user_edit?></td>
						<td  class='text-left'><?=($tgl_edit?HumanDateLong($tgl_edit):'')?></td>
					<tr>
					<tr>
						<td class='text-left'>USER DELETE</td>
						<td  class='text-left'><?=$nama_user_hapus?></td>
						<td  class='text-left'><?=($tgl_hapus?HumanDateLong($tgl_hapus):'')?></td>
					<tr>
				</tbody>
			</table>
		</div>
		</div>
    </div>
    <?php echo form_close() ?>
</div>
<div class="modal" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title" id='title_barang'>Verifikasi Pengembalian BARANG</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal">
                        <input type="hidden" id="rowIndex">
                        <div class="row">
							<input type="hidden" id="idbarang">
							<input type="hidden" id="idtipe"/>
							<input type="hidden" id="nopenerimaan"/>
							<input type="hidden" id="idpenerimaan"/>
							<input type="hidden" id="iddistributor"/>
							<input type="hidden" id="namadistributor"/>
							<input type="hidden" id="rowindex"/>
							<input type="hidden" id="tgl_terima"/>
							<input type="hidden" id="idpenerimaandetail">
							<input type="hidden" id="expireddate">
							<input type="hidden" id="harga_master">
							<input type="hidden" id="harga_asal">
							<input type="hidden" id="jumlah_besar">
							<input type="hidden" id="opsisatuan_asal">
							<input type="hidden" id="xnobatch">
							<input type="hidden" id="satuan_besar">
							<input type="hidden" id="satuan_kecil">
							<input type="hidden" id="harga_besar">
							<input type="hidden" id="harga_kecil">
							<input type="hidden" id="xid">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tipe Barang</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" readonly id="xnamatipe">
                                    </div>
									
                                </div>
								<div class="form-group">
									 <label class="control-label col-md-3">Nama Barang</label>
									<div class="col-md-6">
										<div class="input-group">
											<select name="noterima" id="noterima" data-placeholder="Cari Barang" class="form-control" style="width:100%"></select>
											<div class="input-group-btn">
												<button class='btn btn-info modal_list_barang' data-toggle="modal" data-target="#modal_list_barang" type='button' id='btn_cari'>
													<i class="fa fa-search"></i>
												</button>
											</div>
										</div>
									</div>
								</div>
                                <div class="form-group c" id="f_hargabeli">
							<label class="col-md-3 control-label" for="hargabeli">Harga Beli</label>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-edit"></i></span>
									<input type="text" class="form-control number" id="hargabeli" placeholder="Harga Beli" name="hargabeli" value="" required="" aria-required="true">
								</div>
							</div>
						</div>
                        <div class="form-group c" id="f_hargabeli">
							<label class="col-md-3 control-label" for="kuantitas_pesan">Kuantitas</label>
							<div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input readonly type="text" class="form-control number" id="kuantitas_pesan" placeholder="Kuantitas" name="kuantitas_pesan" value="" required="" aria-required="true">
								</div>
							</div>
							
							<div class="col-md-3">
									<input readonly type="text" class="form-control" id="satuan" placeholder="Satuan" name="satuan" value="" required="" aria-required="true">
							</div>
						</div>
						
						<div class="form-group c" id="f_kuantitas">
							<label class="col-md-3 control-label" for="kuantitas">Kuantitas Retur</label>
							<div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-edit"></i></span>
									<input type="text" class="form-control number" id="kuantitas" placeholder="Kuantitas Retur" name="kuantitas" value="" required="" aria-required="true">
								</div>
							</div>
                            <div class="col-md-3">
                                <select id="opsisatuan" name="opsisatuan" class="form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1">KECIL</option>
									<option value="2">BESAR</option>
								</select>
                            </div>
						</div>
						<div class="form-group c" id="f_hargabeli">
							<label class="col-md-3 control-label" for="kuantitas_pesan">Nominal Retur</label>
							<div class="col-md-3">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input readonly type="text" class="form-control number" id="nominal_retur" placeholder="Nominal" name="nominal_retur" value="" required="" aria-required="true">
								</div>
							</div>
							
						</div>
						<?if ($jenis_retur=='3'){?>
						<div class="form-group c" id="f_hargabeli">
							<label class="col-md-3 control-label" for="kuantitas_pesan">Pengganti</label>
							<div class="col-md-3">
								<div class="input-group-btn">
									<button class='btn btn-sm btn-info modalAdd' data-toggle="modalAdd" data-target="#modalAdd" type='button' id='btn_add_pengganti'>
										<i class="fa fa-plus"></i> Tambahkan Barang Pengganti
									</button>
								</div>
							</div>
							
						</div>
						<?}?>
                        </div>
                            
                                
                       </div>
                    </form>
                </div>
				 <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-sm btn-success" id="btn_update" type="submit">Update</button>
                </div>
                </div>
            </div>
        </div>
    </div>
	<div class="modal" id="modalView" role="dialog" aria-hidden="true" aria-labelledby="modalView">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="block block-themed block-transparent remove-margin-b">
					<div class="block-header bg-success">
						<h3 class="block-title">Barang Pengganti Retur</h3>
					</div>
					<div class="block-content">
						<form class="form-horizontal" id="form_detail">                        

							<table class="table table-bordered" id="tempView">
								<thead>
									<tr>
										<th>Tipe</th>
										<th>Barang</th>
										<th>Harga</th>
										<th>Kuantitas</th>
										<th>Satuan</th>
										<th>Total</th>
										
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</form>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" id="tutup" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal" id="modalAdd" role="dialog" aria-hidden="true" aria-labelledby="modalAdd">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="block block-themed block-transparent remove-margin-b">
					<div class="block-header bg-success">
						<h3 class="block-title">Pilih barang pengganti Retur</h3>
					</div>
					<div class="block-content">
						<form class="form-horizontal" id="form_detail">
							<input type="hidden" id="ganti_rowIndex">
							<input type="hidden" id="idbarang">
							<input type="hidden" id="t_harga_beli">
							<input type="hidden" id="t_harga_beli_besar">
							<input type="hidden" id="g_idpengembalian">
							<input type="hidden" id="g_idpengembalian_det">
							<input type="hidden" id="g_id">

							<div class="form-group c" id="f_idtipe">
								<label class="col-md-3 control-label">Nama Barang</label>
								<div class="col-md-6">
									<select id="ganti_idbarang" class="form-control" style="width: 100%;" data-placeholder="Choose one.."></select>
								</div>
							</div>

							<div class="form-group c" id="f_namabarang">
								<label class="col-md-3 control-label" for="ganti_tipe">Tipe</label>
								<div class="col-md-6">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-edit"></i></span>
										<input readonly type="text" class="form-control" id="ganti_tipe" placeholder="Tipe" name="namabarang" value="" required="" aria-required="true">
										<input type="hidden" id="ganti_idtipe" name="ganti_idtipe" value="">
									</div>
								</div>
							</div>
							<div class="form-group c" id="f_hargabeli">
								<label class="col-md-3 control-label" for="ganti_hargabeli">Harga Beli</label>
								<div class="col-md-6">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-edit"></i></span>
										<input type="text" class="form-control number" id="ganti_hargabeli" placeholder="Harga Beli" name="ganti_hargabeli" value="" required="" aria-required="true">
									</div>
								</div>
							</div>
							<div class="form-group c" id="f_kuantitas">
								<label class="col-md-3 control-label" for="opsisatuan2">Satuan</label>
								<div class="col-md-6">
									<select id="opsisatuan2" name="opsisatuan2" class="form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="2">BESAR</option>
										<option value="1">KECIL</option>
									</select>
								</div>
							</div>
							<div class="form-group c" id="f_kuantitas">
								<label class="col-md-3 control-label" for="ganti_kuantitas">Kuantitas</label>
								<div class="col-md-6">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-edit"></i></span>
										<input type="text" class="form-control number" id="ganti_kuantitas" placeholder="Kuantitas" name="ganti_kuantitas" value="" required="" aria-required="true" autocomplete="off">
										
									</div>
								</div>
							</div>
							<div class="form-group c" id="f_hargabeli">
								<label class="col-md-3 control-label" for="ganti_tot">Total</label>
								<div class="col-md-6">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-lock"></i></span>
										<input type="text" readonly class="form-control number" id="ganti_tot" placeholder="Harga Beli" name="ganti_tot" value="" required="" aria-required="true">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="kuantitas">&nbsp;</label>
								<div class="col-md-6">
									<button class="btn btn-sm btn-success pull-right" type="button" id="tambahBarang">Tambahkan</button>
								</div>
							</div>

							<table class="table table-bordered" id="tempPemesanan">
								<thead>
									<tr>
										<th>Tipe</th>
										<th>Barang</th>
										<th>Harga</th>
										<th>Kuantitas</th>
										<th>Satuan</th>
										<th>Total</th>
										<th>Total</th>
										<th>idtipe</th>
										<th>idbarang</th>
										<th>idbarang</th>
										<th>idbarang</th>
										<th>idbarang</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</form>
					</div>
				</div>
				
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" id="tutup" data-dismiss="modal">Tutup</button>
					<button class="btn btn-sm btn-success" type="button" id="ganti_simpan">Simpan</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
    var table, t, tanggal;
	var v,t;
	
	var gettipe = ["Tipe Barang","Alkes", "Implan", "Obat", "Logistik"];
	var getretur = ["","Ganti Uang", "Ganti Barang", "Ganti Barang Lain"];
	$(".number").number(true,0,'.',',');
	$(".diskon").number(true,2,'.',',');
	// $("#pembayaran").select2();
	$('#jenis_retur').select2();
	$('#jenis').select2();
	$('#opsisatuan').select2();
	t = $('#tempPemesanan').DataTable({
        paging: false,
        info: false,
        sort: false,
        searching: false,
        destroy: true,
        autoWidth: false,
        columnDefs: [{ "targets": [6, 7,8,9,10,11], "visible": false },
		{
            "render": function (data, type, row) {
                 return formatNumber(data);
            },
            "targets": [2,3,5]
        }]
    })
	v = $('#tempView').DataTable({
        paging: false,
        info: false,
        sort: false,
        searching: false,
        destroy: true,
        autoWidth: false,
        columnDefs: [
		{
            "render": function (data, type, row) {
                 return formatNumber(data);
            },
            "targets": [2,3,5]
        }]
    })
    $(document).ready(function() {
			// alert();
			$("#noterima").select2({
				ajax: {
					url: '{site_url}tgudang_pengembalian/Get_barang_penerimaan_detail/'+$("#idpenerimaan").val(),
					dataType: 'json',
					type: "GET",
					quietMillis: 50,				
					processResults: function (data) {
						return {
							results: $.map(data, function (item) {
									console.log(item.id);
								return {
									text: item.nama + " - "+ item.nobatch,
									id: item.id
								}
							})
						};
					}
				}
			});
			$("#ganti_idbarang").select2({
				minimumInputLength: 2,
				ajax: {
					url: '{site_url}tgudang_pengembalian/getProductList/',
					dataType: 'json',
					type: "GET",
					quietMillis: 50,
					data: function (params) {
					  var query = {
						search: params.term,                 
					  }
					  return query;
					}, 
					processResults: function (data) {
						return {
							results: $.map(data, function (item) {
								return {
									text: item.nama,
									id: item.id,
									idtipe: item.idtipe,
									hargabeli: item.hargabeli,
									hargabelibesar: item.hargabeli_besar
								}
							})
						};
					}
				}
			});
    });
	$(document).on('submit','#form-work', function(){
   
		var detail_tbl = $('table#data-barang tbody tr').get().map(function(row) {
			return $(row).find('td').get().map(function(cell) {
				if($(cell).hasClass('json')){
					if($(cell).html() != ''){
						return JSON.parse($(cell).html());
					}else{
						return '';
					}
				}else{
					return $(cell).html();
				}
			});
		});
		// console.log(detail_tbl);
		// return false;
		$("#detail_value").val(JSON.stringify(detail_tbl))
		
	})
	// $(document).on( 'change', '#ganti_idbarang', function(){
		// if ($(this).val()){
		// data=$(this).select2('data')[0];
		// alert(data.idtipe);
		// $('#ganti_tipe').val(gettipe[data.idtipe]);
		// $('#ganti_idtipe').val(data.idtipe);
		// $('#t_harga_beli').val(data.hargabeli);
		// $('#t_harga_beli_besar').val(data.hargabelibesar);
		// if ($("#opsisatuan2").val()=='1') {
			// $('#ganti_hargabeli').val(data.hargabeli);
		// }else{
			// $('#ganti_hargabeli').val(data.hargabelibesar);
		// }
		// // $('#ganti_hargabeli').val(data.hargabeli);
		// // $('#opsi').val(data.hargabeli);
		// setTimeout(function(){
		// $('#ganti_kuantitas').focus();
		// });
		// }

	// });
	$(document).on( 'click', '#ganti_simpan', function(){
		if(!t.data().count()) {
			$.toaster({priority : 'danger', title : 'Error!', message : 'Barang pengganti tidak boleh kosong'});
			return false;
		}
		
		var ganti_tbl = t.rows().data().toArray();
		
		var ganti_tbl = ganti_tbl.map(function(val){
			return val.slice(0, -1);
		});
		$("#pengganti").val(JSON.stringify(ganti_tbl));
		add_data_detail();
		t.clear().draw();
		$('#modalAdd').modal('toggle');
	});
	function detail_value() {
		var detail_tbl = $('table#data-barang tbody tr').get().map(function(row) {
			return $(row).find('td').get().map(function(cell) {
				return $(cell).html();
			});
		});

		$("#detail_value").val(JSON.stringify(detail_tbl))
	}
	function get_nominal2(){
		var nominal=0;
		var harga_kecil=$("#t_harga_beli").val();
		var harga_besar=$("#t_harga_beli_besar").val();
		
		if ($("#opsisatuan2").val()=='1'){
			$("#ganti_tot").val(harga_kecil * parseFloat($("#ganti_kuantitas").val()));
			$("#ganti_hargabeli").val(harga_kecil);
			
		}else{
			$("#ganti_tot").val(harga_besar * parseFloat($("#ganti_kuantitas").val()));
			$("#ganti_hargabeli").val(harga_besar);
		}
	}
	$(document).on( 'keyup', '#ganti_hargabeli', function(){
		// if ($("#opsisatuan2").val()=='1'){
			// $("#ganti_tot").val(harga_kecil * parseFloat($("#ganti_kuantitas").val()));
			// $("#ganti_hargabeli").val(harga_kecil);
			
		// }else{
			// $("#ganti_tot").val(harga_besar * parseFloat($("#ganti_kuantitas").val()));
			// $("#ganti_hargabeli").val(harga_besar);
		// }
		
		$("#ganti_tot").val(parseFloat($("#ganti_hargabeli").val()) * parseFloat($("#ganti_kuantitas").val()));
	});
	$(document).on( 'keyup', '#ganti_kuantitas', function(){
		get_nominal2();
		
	});
	$(document).on( 'change', '#opsisatuan2', function(){
	   get_nominal2();
		
	});
	$(document).on( 'click', '.detail', function(){
	   $row = $(this).closest('tr');
		v.clear().draw(false);
	   var pengganti_list = JSON.parse($row.find("td:eq(23)").html());
		pengganti_list.forEach(function(row){
			v.row.add([
					row[0],
					row[1],
					row[2],
					row[3],
					row[4],
					row[5]

				]).draw(false)    
			
		});
		$('#modalView').modal('toggle');
	});
	$(document).on( 'click', '#btn_add_pengganti', function(){
	   
		$('#modalAdd').modal('toggle');
	});
	$(document).on( 'change', '#noterima', function(){    
	// alert($(this).val());
	if ($(this).val()){
    $.ajax({
        url: '{site_url}tgudang_pengembalian/getPenerimaanDetail/',
        method: 'POST',
        data: { id: $(this).val()},
        dataType: 'json',
        success: function(data) {
            // $('#idbarang').val(data.idbarang);
            $('#idtipe').val(data.idtipe);
            $('#nopenerimaan').val(data.nopenerimaan);
            $('#iddistributor').val(data.iddistributor);
            $('#namadistributor').val(data.nama_distributor);
            $('#tipe').val(gettipe[data.idtipe]);
            $('#namabarang').val(data.nama);
            $('#hargabeli').val(data.harga);
            $('#kuantitas_pesan').val(data.kuantitas);
            $('#tgl_terima').val(data.tanggalpenerimaan);
            $('#idbarang').val(data.idbarang);
            $("#idpenerimaandetail").val(data.detail_id);
            $("#expireddate").val(data.expired_date);
            $("#satuan").val(data.satuan+' | '+data.jenis_satuan);
            $("#harga_master").val(data.harga_master);
            $("#harga_asal").val(data.harga);
            $("#jumlah_besar").val(data.jumlahsatuanbesar);
            $("#satuan_besar").val(data.satuan_besar);
            $("#satuan_kecil").val(data.satuan_kecil);
            $("#harga_besar").val(data.harga_besar);
            $("#harga_kecil").val(data.harga_kecil);
			$('#kuantitas').focus();
			$("#opsisatuan_asal").val(data.opsisatuan);
			if ($("#rowindex").val()==''){
				// alert('ROW INDEX'+$("#rowindex").val());
				$("#opsisatuan").val(data.opsisatuan).trigger('change');
			}
            $("#xnobatch").val(data.nobatch);
			hitung_total();
        }
    });
	
	}	
});
	function add_data_detail()
	{
		// alert('Sni');
	   var table = "";
	   var satuan;
	   if ($("#opsisatuan").val()=='1'){
		   satuan=$("#satuan_kecil").val();
	   }else{	   
		   satuan=$("#satuan_besar").val();
	   }
		table += "<td>"+$("#xnamatipe").val()+"</td>"; //0
		table += "<td>"+$("#noterima  option:selected").text()+"</td>"; //1
		table += "<td>"+$("#kuantitas").val()+' '+ satuan +"</td>"; //2
		table += "<td>"+$("#opsisatuan option:selected").text()+"</td>"; //3
		table += "<td>"+formatNumber($("#hargabeli").val())+"</td>"; //4
		table += "<td>"+formatNumber($("#nominal_retur").val())+"</td>"; //5
		table += "<td>"+$('#expireddate').val()+"</td>"; //6
		table += "<td>"+$('#xnobatch').val()+"</td>"; //7
		if ($("#jenis_retur").val()=='3'){		//8
		table += '<td><button class="btn btn-info btn-xs detail" type="button"><i class="fa fa-search"></i> '+getretur[$("#jenis_retur").val()]+'</button></td>'; //8
		}else{		
		table += "<td>"+getretur[$("#jenis_retur").val()]+"</td>"; //8
		}
		table += '<td class="text-center">';//9
		table += '<button class="btn btn-xs btn-success edit" type="button" title="Edit Barang"><i class="fa fa-pencil"></i></button></td></td>';								
		
		table += "<td hidden>"+ $("#idtipe").val() +"</td>"; //10
		table += "<td hidden>"+ $("#idbarang").val() +"</td>"; //11
		table += "<td hidden>"+ $("#opsisatuan").val() +"</td>"; //12
		table += "<td hidden>"+ $("#kuantitas").val() +"</td>"; //13
		table += "<td hidden>"+ satuan +"</td>"; //14
		table += "<td hidden>"+ $("#satuan_kecil").val() +"</td>"; //15
		table += "<td hidden>"+ $("#satuan_besar").val() +"</td>"; //16
		table += "<td hidden>"+ $("#harga_kecil").val() +"</td>"; //17
		table += "<td hidden>"+ $("#harga_besar").val() +"</td>"; //18
		table += "<td hidden>"+ $("#id").val() +"</td>"; //19
		table += "<td hidden>"+ $("#idpenerimaandetail").val() +"</td>"; //20
		table += "<td hidden>"+$("#hargabeli").val()+"</td>"; //21
		table += "<td hidden>"+$("#nominal_retur").val()+"</td>"; //22
		table += "<td hidden class=\"json\">"+ $("#pengganti").val() +"</td>"; //23
		table += "<td hidden>1</td>"; //24
		table += "<td hidden>"+$("#xid").val()+"</td>"; //25
		if($("#rowindex").val() != ''){
			$('#data-barang tbody tr:eq(' + $("#rowindex").val() + ')').html(table);
		}else{
			table = "<tr>" + table + "</tr>";
			$('#data-barang tbody').append(table);
		} 
		 $('#modal_edit').modal('toggle');
	}
	$("#btn_update").click(function() {
			add_data_detail();
        
    });
    
	var table, tr;
	$(document).on("click",".edit",function(){
		 var tr = $(this).closest('tr');
		 var noterima=tr.find('td:eq(20)').text();
		 // alert('NO :' + noterima);
		$('#xnamatipe').val(tr.find('td:eq(0)').text());
		$('#kuantitas').val(tr.find('td:eq(13)').text());
		$('#opsisatuan').val(tr.find('td:eq(12)').text()).trigger('change');
		$('#nominal_retur').val(tr.find('td:eq(22)').text());
		$('#xid').val(tr.find('td:eq(25)').text());
		var newOption = new Option(tr.find('td:eq(1)').text(), noterima, false, false);
		$('#noterima').append(newOption).trigger('change');
		$('#noterima').val(noterima).trigger('change');
		
		$('#modal_edit').modal('show');
        $("#rowindex").val(tr[0].sectionRowIndex);
		
		
		
	var json=tr.find("td:eq(23)").html();
	// alert($row.find("td:eq(19)").html());
		// alert(json);
	if (json){
		t.clear().draw(false);
		var pengganti_list = JSON.parse(json);
		pengganti_list.forEach(function(row){
			t.row.add([
					row[0],
					row[1],
					row[2],
					row[3],
					row[4],
					row[5],
					row[6],
					row[7],
					row[8],
					row[9],
					row[10],
					row[11],
					"<a href='#' class='on-default detailEdit' title='Edit'><i class='fa fa-pencil'></i></a>&nbsp;"+
					"<a href='#' class='on-default detailRemove' title='Hapus'><i class='fa fa-trash-o'></i></a>",

				]).draw(false)    
			
		});
		
		var ganti_tbl = t.rows().data().toArray();
		
		var ganti_tbl = ganti_tbl.map(function(val){
			return val.slice(0, -1);
		});
		$("#pengganti").val(JSON.stringify(ganti_tbl));
	}
		// gen_harga();
		// cek_harga();
	});
	
	$('#tempPemesanan tbody').on( 'click', '.detailRemove', function () {
		t.row($(this).parents('tr')).remove().draw()

		$('#tambahBarang').text('Tambahkan').removeClass('btn-warning').addClass('btn-success');
		$('#ganti_tipe,#ganti_hargabeli,#ganti_kuantitas').val('');
		$('#ganti_idbarang').val('').trigger('change');
	})
	$('#tempPemesanan tbody').on( 'click', 'a.detailEdit', function () {
		$('#tambahBarang').text('Sunting').removeClass('btn-success').addClass('btn-warning')

		var rowIndex = t.row($(this).parents('tr')).index()
						
		$('#ganti_rowIndex').val(rowIndex )
		$('#ganti_idtipe').val(t.cell(rowIndex,6).data() )
		$('#ganti_tipe').val(t.cell(rowIndex,0).data() )
		// alert(t.cell(rowIndex,7).data());
		$('#ganti_idbarang').empty().append('<option selected value="'+ t.cell(rowIndex,7).data() +'">'+ t.cell(rowIndex,1).data() +'</option>').selectpicker('refresh')
		$('#ganti_idbarang').val(t.cell(rowIndex,7).data()).trigger('change');
		$('#ganti_hargabeli').val( t.cell(rowIndex,2).data() )
		$('#ganti_kuantitas').val( t.cell(rowIndex,3).data() )
		$('#ganti_tot').val( t.cell(rowIndex,5).data() )
		$('#g_idpengembalian').val( t.cell(rowIndex,9).data() )
		$('#g_idpengembalian_det').val( t.cell(rowIndex,10).data() )
		$('#g_id').val( t.cell(rowIndex,11).data() )
		// $('#ganti_tipe').val(gettipe(t.cell(rowIndex,4).data()))
	})
	$(document).on( 'change', '#ganti_idbarang', function(){
		
		if ($("#ganti_rowIndex").val()!=''){
			// alert('EDIT');
			if ($(this).val()){
				$.ajax({
					url: '{site_url}tgudang_pengembalian/get_mbarang/'+$(this).val()+'/'+$('#ganti_idtipe').val(),
					method: 'GET',
					dataType: 'json',
					success: function(data) {
						$("#t_harga_beli").val(data.hargabeli);
						$("#t_harga_beli_besar").val(data.hargabeli_besar);
						$("#ganti_idtipe").val(data.idtipe);
						$("#ganti_tipe").val(data.namatipe);
					}
				}
				); 			
			}
		}else{
			// alert('NEW');
			if ($(this).val()){
				data=$(this).select2('data')[0];
				// alert(data.idtipe);
				$('#ganti_tipe').val(gettipe[data.idtipe]);
				$('#ganti_idtipe').val(data.idtipe);
				$('#t_harga_beli').val(data.hargabeli);
				$('#t_harga_beli_besar').val(data.hargabelibesar);
				if ($("#opsisatuan2").val()=='1') {
					$('#ganti_hargabeli').val(data.hargabeli);
				}else{
					$('#ganti_hargabeli').val(data.hargabelibesar);
				}
				// $('#ganti_hargabeli').val(data.hargabeli);
				// $('#opsi').val(data.hargabeli);
				setTimeout(function(){
				$('#ganti_kuantitas').focus();
				});
				}
		}
	});
	$(document).on("click",".hapus",function(){
		$("#st_hapus_alih").val('1');
		$("#div_hapus").show();
		 var tr = $(this).closest('tr');
		 var content = "<tr>";
		 content += "<td class='text-center'>" + tr.find('td:eq(0)').text() + "</td>"; //0 Nomor
		 content += "<td  class='text-left'>" + tr.find('td:eq(1)').text() + "</td>"; //1 
		 content += "<td  class='text-center'>" + tr.find('td:eq(28) input').val()+' '+tr.find('td:eq(32) input').val()+ "</td>"; //2 
		 if (tr.find('td:eq(27) input').val() == tr.find('td:eq(28) input').val()){
			content += '<td  class="text-center"><span class="label label-danger">DIBATALKAN SEMUA</span></td>'; //2 
		 }else{
			content += '<td  class="text-center"><span class="label label-danger">DIBATALKAN SEBAGIAN</span></td>'; //2 
		 }
		 content += '<td ><select class="form-control e_dist" name="d_iddistributor[]" style="width:100%"><option value="#" selected>TIDAK PERLU DIISI</option></select></td>'; //3
		 content += '<td ><input type="text" class="form-control" name="d_alasan[]" value=""></td>'; //4
		 content += '<td hidden><input type="text" class="form-control" name="d_iddet[]" value="'+tr.find('td:eq(15) input').val()+'"></td>'; //5
		 
		 if (tr.find('td:eq(27) input').val() == tr.find('td:eq(28) input').val()){
			content += '<td hidden><input type="text" class="form-control" name="d_status[]" value="1"></td>'; //ALL
		 }else{
			 content += '<td hidden><input type="text" class="form-control" name="d_status[]" value="2"></td>'; //SEBAGIAN
		 }
		 content += '<td hidden><input type="text" class="form-control" name="d_kuantitas[]" value="'+ tr.find('td:eq(28) input').val()+'"></td>'; //5
		 content += "</tr>";
		 tr.remove();
		$('#data_hapus tbody').append(content);
		get_distributor();
		
	});
	$(document).on( 'click', '#tambahBarang', function(){    

		if(validate()) {
			if($('#tambahBarang').text() == 'Tambahkan') {
				t.row.add([
					$('#ganti_tipe').val(),//0
					$('#ganti_idbarang :selected').text(),//1
					$('#ganti_hargabeli').val(),//2
					$('#ganti_kuantitas').val(),//3
					$('#opsisatuan2 option:selected').text(),//4
					$('#ganti_tot').val(),//5
					$('#ganti_idtipe').val(),//6
					$('#ganti_idbarang').val(),//7
					$('#opsisatuan2').val(),//8
					$('#g_idpengembalian').val(),//9
					$('#g_idpengembalian_det').val(),//10
					$('#g_id').val(),//11
					"<a href='#' class='on-default detailEdit' title='Edit'><i class='fa fa-pencil'></i></a>&nbsp;"+
					"<a href='#' class='on-default detailRemove' title='Hapus'><i class='fa fa-trash-o'></i></a>",
				]).draw(false)
			} else {
				t.row( $('#ganti_rowIndex').val() ).data([
					$('#ganti_tipe').val(),
					$('#ganti_idbarang :selected').text(),
					$('#ganti_hargabeli').val(),
					$('#ganti_kuantitas').val(),
					$('#opsisatuan2 option:selected').text(),
					$('#ganti_tot').val(),
					$('#ganti_idtipe').val(),
					$('#ganti_idbarang').val(),
					$('#opsisatuan2').val(),
					$('#g_idpengembalian').val(),//9
					$('#g_idpengembalian_det').val(),//10
					$('#g_id').val(),//11
					"<a href='#' class='on-default detailEdit' title='Edit'><i class='fa fa-pencil'></i></a>&nbsp;"+
					"<a href='#' class='on-default detailRemove' title='Hapus'><i class='fa fa-trash-o'></i></a>",
				]).draw(false)
			}
			$('#ganti_rowIndex').val('');
			$('#tambahBarang').text('Tambahkan').removeClass('btn-warning').addClass('btn-success')
			$('#ganti_tipe,#ganti_hargabeli,#ganti_kuantitas,#t_harga_beli,#t_harga_beli_besar,#ganti_tot').val('');
			$('#opsisatuan2').val('2').trigger('change');
			$('#ganti_idbarang').val('').trigger('change');
			return true;
		} else {
			return false;
		}

	})
	$(document).on("change","#opsisatuan",function(){		
		hitung_total();
	});
	$(document).on("keyup","#kuantitas",function(){		
		hitung_total();
	});
	function hitung_total(){
		if ($("#opsisatuan").val()=='1'){
			$("#hargabeli").val($("#harga_kecil").val());
		}else{
			$("#hargabeli").val($("#harga_besar").val());
		}
		$("#nominal_retur").val(parseFloat($("#hargabeli").val()) * parseFloat($("#kuantitas").val()));
	}
	function formatNumber (num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}
    

    function validateHead() {
        if ($('#nopemesanan').val() == '') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'No Pemesanan tidak boleh kosong'
            });
            return false;
        }
        if ($('#tanggalpenerimaan').val() == '') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Tanggal Penerimaan tidak boleh kosong'
            });
            return false;
        }
        if ($('#tanggaljatuhtempo').val() == '') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Tanggal Jatuh Tempo tidak boleh kosong'
            });
            return false;
        }
        if ($('#totalharga').val() == '' || $('#totalharga').val() <= 0) {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Data belum lengkap. Total terima barang masih 0'
            });
            return false;
        }
        return true;
    }

    function validate() {
        if ($('#xharga_total_fix').val() == 0) {
            sweetAlert("Maaf...", "Harga Dan kuantitas harus diisi!", "error");
            return false;
        }
        
        if ($('#tanggalkadaluarsa').val() == '') {
            sweetAlert("Maaf...", "Tanggal Kadaluarsa harus diisi!", "error");
            return false;
        }
        if ($('#nobatch').val() == '' || $('#nobatch').val() == '-') {
            sweetAlert("Maaf...", "No Batch Harus diisi!", "error");
            return false;
        }
        return true;
    }

    
	
    function validate_header() {
		// alert('Validasi');
		var rowCount = $('#data-barang tr').length;
		var rowCountHapus = $('#data_hapus tr').length;
		// alert(rowCount);
        if (parseFloat($('#totalbarang').val()) == 0 && rowCount > 1) {
			if (rowCountHapus <= 1){				
				sweetAlert("Maaf...", "Silhakan Pilih barang Yang mau diverifikasi!", "error");
				return false;
			}
        }
        
        if ($('#nofakturexternal').val() == '') {
            sweetAlert("Maaf...", "No Faktur harus diisi!", "error");
            return false;
        }
		if ($('#tanggalpenerimaan').val() == '') {
            sweetAlert("Maaf...", "Tanggal Harus di isi!", "error");
            return false;
        }
        if ($('#tanggaljatuhtempo').val() == '' && $("#pembayaran").val()=='2') {
            sweetAlert("Maaf...", "Tanggal Jatuh Tempo Harus di isi!", "error");
            return false;
        }
		var ada_kosong_dist='1';
		var ada_kosong_dist_alasan='1';
		var ada_kosong_hapus='1';
		if (rowCountHapus>1){
			$('#data_hapus tbody tr').each(function() {
				var tr = $(this).closest('tr');	
				 // alert(tr.find("td:eq(13) input").val());
				 console.log('DIST'+tr.find("td:eq(4) select").val());
				if (tr.find("td:eq(7) input").val()=='3' && tr.find("td:eq(4) select").val()=='#'){
					ada_kosong_dist='0';
				}
				if (tr.find("td:eq(7) input").val()=='3' && tr.find("td:eq(5) input").val()==''){
					ada_kosong_dist_alasan='0';
				}
				if (tr.find("td:eq(7) input").val()=='1' && tr.find("td:eq(5) input").val()==''){
					ada_kosong_hapus='0';					
				}
				if (tr.find("td:eq(7) input").val()=='2' && tr.find("td:eq(5) input").val()==''){
					ada_kosong_hapus='0';					
				}
				console.log(tr.find("td:eq(7) input").val()+'-'+tr.find("td:eq(5) input").val());
				
			});
			console.log(ada_kosong_dist+';'+ada_kosong_dist_alasan+';'+ada_kosong_hapus);
			if (ada_kosong_dist=='0'){
				sweetAlert("Maaf...", "Harus diisi Distributor!", "error");
				return false;
			}
			if (ada_kosong_dist_alasan=='0'){
				sweetAlert("Maaf...", "Harus diisi Alasan Ganti Distributor!", "error");
				return false;
			}
			if (ada_kosong_hapus=='0'){
				sweetAlert("Maaf...", "Harus diisi Alasan Hapus!", "error");
				return false;
			}
		}
        return true;
    }
	$("#simpan-penerimaan").click(function() {		
		event.preventDefault();
		swal({
				title: "Apa anda yakin Lanjut Verifikasi?",
				text: "Anda tidak akan dapat Edit dan membatalkan transaksi ini!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya , Verifikasi !",
				cancelButtonText: "Tidak !",
				closeOnConfirm: false,
				closeOnCancel: false
			}).then(function() {
					swal({
						title: 'Success!',
						text: 'Succes! Verifikasi Berhasil.',
						type: 'success',
						timer: 2000,
					  showCancelButton: false,
					  showConfirmButton: false
					});
					$("#form-work").submit();
				},function(dismiss) {
					if(dismiss == 'cancel') {
						// swal("Cancelled", "Pembatalan dibatalkan!", "error");
						swal({
							title: 'Dibatalkan!',
							text: 'Dibatalkan! Verifikasi dibatalkan!.',
							type: 'error',
							timer: 2000,
						  showCancelButton: false,
						  showConfirmButton: false
						});
					}
				});

		// alert('Simpan');
		 // if (validate_header()){
			// $("#form-work").submit();
		// }
		
		return false;
	});
	
</script>