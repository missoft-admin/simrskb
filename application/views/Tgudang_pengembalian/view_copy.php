<div class="block">
	<div class="block-header">
		<ul class="block-options">
            <li><a href="{site_url}tgudang_pengembalian" class="btn"><i class="fa fa-reply"></i></a></li>
        </ul>
		<h3 class="block-title">{title}</h3>
	</div>	
	<div class="block-content">
		<form class="form-horizontal">
			<div class="form-group c" id="f_namabarang">
				<label class="col-md-3 control-label" for="namabarang">No Pengembalian</label>
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input disabled type="text" class="form-control" aria-required="true" value="{nopengembalian}">
					</div>
				</div>
			</div>
			<div class="form-group c" id="f_namabarang">
				<label class="col-md-3 control-label" for="namabarang">Distributor</label>
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input disabled type="text" class="form-control" aria-required="true" value="{distributor}">
					</div>
				</div>
			</div>
            <div class="form-group c" id="f_namabarang">
				<label class="col-md-3 control-label" for="namabarang">Jenis Pengembalian</label>
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input disabled type="text" class="form-control" aria-required="true" value="<?= jenis_pengembalian($jenis) ?>">
					</div>
				</div>
			</div>
            <div class="form-group c" id="f_namabarang">
				<label class="col-md-3 control-label" for="namabarang">Alasan</label>
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input disabled type="text" class="form-control" aria-required="true" value="{alasan}">
					</div>
				</div>
			</div>
            <div class="form-group c" id="f_namabarang">
				<label class="col-md-3 control-label" for="namabarang">Jenis Retur</label>
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input disabled type="text" class="form-control" aria-required="true" value="<?= jenis_retur($jenis_retur) ?>">
					</div>
				</div>
			</div>
			<div class="form-group c" id="f_namabarang">
				<label class="col-md-3 control-label" for="namabarang">Total Barang</label>
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input disabled type="text" class="form-control" aria-required="true" value="{totalbarang}">
					</div>
				</div>
			</div>
			<div class="form-group c" id="f_namabarang">
				<label class="col-md-3 control-label" for="namabarang">Total Harga</label>
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input disabled type="text" class="form-control" aria-required="true" value="{totalharga}">
					</div>
				</div>
			</div>
		</form>
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
				<tr>
					<th>Nama Barang</th>
					<th>No. Batch</th>
					<th>Kuantitas</th>
					<th>Harga</th>
                    <th>T. Harga</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($this->tgudang_pengembalian->viewDetail($id) as $r): ?>
					<tr>
						<td><?= $this->tgudang_pengembalian->getNamaBarang($r->idtipe,$r->idbarang) ?></td>
						<td><?= $r->nobatch ?></td>
						<td><?= $r->kuantitas ?></td>
						<td><?= $r->harga ?></td>
						<td><?= $r->harga*$r->kuantitas ?></td>
					</tr>
				<?php endforeach  ?>
			</tbody>
		</table>
	</div>
	<!-- end content -->

</div>