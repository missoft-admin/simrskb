<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Kelompok Pasien</label>
                    <div class="col-md-8">
                        <select id="idkelompokpasien" name="idkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Kelompok Pasien -</option>
							<?foreach($list_kelompok_pasien as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Perusahaan</label>
                    <div class="col-md-8">
                        <select id="idrekanan" name="idrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Perusahaan -</option>
							<?foreach($list_rekanan as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
                                
            </div>
			<div class="col-md-6">
				
                <div class="form-group" style="margin-bottom: 5px;">
                    <div class="col-md-3">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th width="10%">NO</th>
                    <th width="10%">REKANAN</th>
                    <th width="10%">SISA PIUTANG</th>
                    <th width="10%">1 BULAN</th>
                    <th width="10%">2 BULAN</th>
                    <th width="10%">3 BULAN</th>
                    <th width="10%">4 BULAN</th>
                    <th width="10%">5 BULAN</th>
                    <th width="10%">6 BULAN</th>
                    <th width="10%">1 TAHUN</th>
                    <th width="10%"> < 2 TAHUN</th>
                    <th width="10%"> > 2 TAHUN</th>                    
                    <th width="10%">TOTAL TRANSAKSI</th>                    
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
</div>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	load_index();
   
});
function load_index(){
	
	var idkelompokpasien=$("#idkelompokpasien").val();
	var idrekanan=$("#idrekanan").val();
	
	
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: false,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [
							// { "targets": [0,1,13,14,17,18,19,20,21], "visible": false },
							{ "width": "3%", "targets": [0] },
							{ "width": "15%", "targets": [1] },
							{ "width": "8%", "targets": [2,3,4,5,6,7,8,9,10,11,12] },
							
						 {"targets": [1], className: "text-left" },
						 {"targets": [0,2,3,4,5,6,7,8,9,10,11,12], className: "text-right" },
						 ],
            ajax: { 
                url: '{site_url}tklaim_umur/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						idkelompokpasien:idkelompokpasien,
						idrekanan:idrekanan
						
					   }
            }
        });
}
$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_index();
});
$(document).on("change", "#idkelompokpasien", function() {
	if ($('#idkelompokpasien').val()!='1'){
		// alert('sini');
		$('#idrekanan').val("#").trigger('change');
	}
	
});


</script>