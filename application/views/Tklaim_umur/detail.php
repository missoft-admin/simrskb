<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work-1"'); ?>
                <div class="col-md-6"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Nama Asuransi</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{nama_asuransi}" disabled>
                            <input class="form-control" type="hidden" name="jenis_rekanan" id="jenis_rekanan" value="{jenis_rekanan}" >
							<input class="form-control" type="hidden" name="idkelompokpasien" id="idkelompokpasien" value="{idkelompokpasien}" >
                            <input class="form-control" type="hidden" name="idrekanan" id="idrekanan" value="{idrekanan}" >
                            <input class="form-control" type="hidden" name="bulan" id="bulan" value="{bulan}" >
                            <input class="form-control" type="hidden" name="disabel" id="disabel" value="{disabel}" >
                        </div> 
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Umur Piutang</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{umur}" disabled>
                        </div> 
                    </div>
					<div class="form-group" style="margin-bottom: 15px;"> 
                        <label class="col-md-3 control-label">Nominal Umur Piutang</label> 
                        <div class="col-md-9"> 
                            <input class="form-control number" value="{total}" disabled>
                        </div> 
                    </div>
                                      
                </div>
				
            <?php echo form_close(); ?>
        </div>
        
        <div class="form-group" style="margin-top: 20px;"></div>
    </div>
</div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">DETAIL TAGIHAN</h3>
    </div>
    <div class="block-content">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Registrasi</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_reg" placeholder="No Registrasi" name="no_reg" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Medrec</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_medrec" placeholder="No. Medrec" name="no_medrec" value="">
                    </div>
                </div>
				
                
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="nama_pasien" placeholder="Nama Pasien" name="nama_pasien" value="">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Daftar</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tgl_trx" name="tgl_trx" placeholder="From" value=""/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tgl_trx2" name="tgl_trx2" placeholder="To" value=""/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
			</div>
            <?php echo form_close(); ?>
        </div>
        <div class="form-group" style="margin-bottom: 30px;"></div>
		<div class="table-responsive">
		
        <table class="table table-bordered table-striped table-responsive" id="index_list">
            <thead>
                <tr>
                    <th>#</th>
                    <th>TANGGAL KUNJUNGAN</th>
                    <th>TANGGAL TRANSAKSI</th>
                    <th>JENIS</th>
                    <th>NO. REG</th>
                    <th>MEDREC</th>
                    <th>PASIEN</th>
                    <th>TOTAL TRX</th>
                    <th>DIBAYAR ASURANSI</th>
                    <th>DIBAYAR METODE LAIN</th>
                    <th>TGL TAGIHAN</th>
                    <th>NO. PENAGIHAN</th>
                    <th>AKSI</th>
                </tr>
            </thead>
            <tbody> </tbody>
              
        </table>
    </div>
        <div class="form-group" style="margin-top: 20px;"></div>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">


    var table;

    $(document).ready(function(){
		$(".number").number(true,0,'.',',');
		load_index();
	
    });
	
    function load_index() {
		var jenis_rekanan=$("#jenis_rekanan").val();
		var idkelompokpasien=$("#idkelompokpasien").val();
		var idrekanan=$("#idrekanan").val();
		var bulan=$("#bulan").val();
		var no_reg=$("#no_reg").val();
		var no_medrec=$("#no_medrec").val();
		var nama_pasien=$("#nama_pasien").val();
		var tgl_trx=$("#tgl_trx").val();
		var tgl_trx2=$("#tgl_trx2").val();
		var disabel=$("#disabel").val();
		// alert(tgl_trx);
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 100,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tklaim_umur/get_detail/',
			type: "POST",
			dataType: 'json',
			data: {
				jenis_rekanan: jenis_rekanan,
				idkelompokpasien: idkelompokpasien,
				idrekanan: idrekanan,
				bulan: bulan,
				no_reg: no_reg,
				no_medrec: no_medrec,nama_pasien: nama_pasien,
				tgl_trx:tgl_trx,tgl_trx2:tgl_trx2,disabel:disabel,
			}
		},
		columnDefs: [
					// {"targets": [0,1], "visible": false },
					 {  className: "text-right", targets:[0,7,8,9] },
					 {  className: "text-center", targets:[1,2,3,4,5,10,11] },
					 {  className: "text-left", targets:[6,12] },
					 { "width": "3%", "targets": [0,3] },
					 // { "width": "5%", "targets": [4] },
					 { "width": "8%", "targets": [1,2,4,5,10,11] },
					 { "width": "8%", "targets": [6,7,8,9] },
					 { "width": "15%", "targets": [12] },
					 // { "width": "15%", "targets": [8] }

					]
		});
	}
	
	$("#btn_filter").click(function() {
		load_index();
	});
	
	
</script>