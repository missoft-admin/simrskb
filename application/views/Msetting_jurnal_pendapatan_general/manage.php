<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content" style="margin-top: -20px;font-size:">
		<?php echo form_open('msetting_jurnal_pendapatan_general/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>	
			<div class="row">
				<div class="col-md-12">
					<div class="block block-themed block-opt-hidden">
						<div class="block-header bg-success">
							<ul class="block-options">

								<li>
									<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title">RAWAT JALAN & OBAT BEBAS</h3>
						</div>
						<div class="block-content">
							<div class="form-group">
								<label class="col-md-2 control-label" ><?=('Proses Approval')?></label>
								<input type="hidden" class="form-control" id="tmp_idakun_tunai_rajal" placeholder="0" name="tmp_idakun_tunai_rajal" value="{idakun_tunai_rajal}">
								<div class="col-md-4">
									<select id="st_auto_posting_rajal" name="st_auto_posting_rajal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="1" <?=(1 == $st_auto_posting_rajal ? 'selected' : ''); ?>>AUTO POSTING</option>
											<option value="2" <?=(2 == $st_auto_posting_rajal ? 'selected' : ''); ?>>MANUAL</option>
										</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" ><?=('Akun Pembayaran Tunai')?> </label>
								
								<div class="col-md-8">
									<select id="idakun_tunai_rajal" name="idakun_tunai_rajal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<?foreach($list_akun as $r){?>
										<option value="<?=$r->id?>" <?=($r->id == $idakun_tunai_rajal? 'selected' : ''); ?>><?=$r->noakun.' - '.$r->namaakun.' ('.$r->id.')'?></option>
									<?}?>
									</select>
								</div>
								
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" ><?=('Group Akun Pembulatan')?> </label>
								
								<div class="col-md-8">
									<select id="group_round_rajal" name="group_round_rajal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<?foreach($list_group as $r){?>
										<option value="<?=$r->id?>" <?=($r->id == $group_round_rajal? 'selected' : ''); ?>><?=$r->nama?></option>
									<?}?>
									</select>
								</div>
								
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" ><?=('Group Akun Diskon ')?> </label>
								
								<div class="col-md-8">
									<select id="group_diskon_rajal" name="group_diskon_rajal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<?foreach($list_group as $r){?>
										<option value="<?=$r->id?>" <?=($r->id == $group_diskon_rajal? 'selected' : ''); ?>><?=$r->nama?></option>
									<?}?>
									</select>
								</div>
								
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" ><?=('Group Akun Tuslah Racikan')?> </label>
								
								<div class="col-md-8">
									<select id="group_tuslah_racikan" name="group_tuslah_racikan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<?foreach($list_group as $r){?>
										<option value="<?=$r->id?>" <?=($r->id == $group_tuslah_racikan? 'selected' : ''); ?>><?=$r->nama?></option>
									<?}?>
									</select>
								</div>
								
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" ><?=('Group Akun Tidak Tertagih')?> </label>
								
								<div class="col-md-8">
									<select id="group_tidak_tertagih_rajal" name="group_tidak_tertagih_rajal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<?foreach($list_group as $r){?>
										<option value="<?=$r->id?>" <?=($r->id == $group_tidak_tertagih_rajal? 'selected' : ''); ?>><?=$r->nama?></option>
									<?}?>
									</select>
								</div>
								
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" ><?=('Group Akun Pegawai / Dokter')?> </label>
								
								<div class="col-md-8">
									<select id="group_karyawan_rajal" name="group_karyawan_rajal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<?foreach($list_group as $r){?>
										<option value="<?=$r->id?>" <?=($r->id == $group_karyawan_rajal? 'selected' : ''); ?>><?=$r->nama?></option>
									<?}?>
									</select>
								</div>
								
							</div>
							
							<div class="form-group">
								<label class="col-md-2 control-label" ><?=('Batas Waktu Batal')?></label>
								
								<div class="col-md-4">
									<input type="text" style="width: 100%"  class="form-control number" id="batas_batal_rajal" placeholder="0" name="batas_batal_rajal" value="<?=$batas_batal_rajal?>">
								</div>
								<div class="col-md-2">
									<button type="button" class="btn btn-sm btn-success" tabindex="8" id="btn_update_rajal" title="Update"><i class="fa fa-save"></i> UPDATE</button>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-1 control-label"><h4><?=text_success('SETTING PENDAPATAN GABUNGAN RAWAT JALAN')?></h4></label>
								
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<table id="tabel_rajal" class="table table-striped table-bordered" style="margin-bottom: 0;">
										<thead>
											<tr>
												<th style="width: 0%;">#</th>															
												<th style="width: 15%;">Kelompok Pasien</th>															
												<th style="width: 25%;">Rekanan </th>															
												<th style="width: 50%;">Group Pembayaran </th>
												<th style="width: 10%;">Actions</th>
											</tr>
											<tr>								
												<td></td>
												<td>
													<select name="idkelompokpasien_rajal" id="idkelompokpasien_rajal" style="width: 100%" id="step" class="js-select2 form-control input-sm idkelompokpasien">										
																																
														
													</select>										
												</td>
												<td>
													<select name="idrekanan_rajal" id="idrekanan_rajal" style="width: 100%" id="step" class="js-select2 form-control input-sm rekanan">										
														<option value="#" selected>- All Rekanan -</option>	
													</select>										
												</td>								
												
												<td>
													<select name="idakun_rajal" id="idakun_rajal" style="width: 100%" class="js-select2 form-control input-sm idakun">										
														
													</select>
													<input type="hidden" class="form-control" id="id_edit_rajal" placeholder="0" name="id_edit_rajal" value="">
												</td>	
												
												<td>									
													<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_rajal" title="Masukan Item"><i class="fa fa-save"></i></button>
													<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_rajal" title="Batalkan"><i class="fa fa-refresh"></i></button>
												</td>
											</tr>
										</thead>
										<tbody>
											
										</tbody>
										
								</table>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-1 control-label"><h4><?=text_danger('SETTING PIUTANG ASURANSI')?></h4></label>
								
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<table id="tabel_rajal_piutang" class="table table-striped table-bordered" style="margin-bottom: 0;">
										<thead>
											<tr>
												<th style="width: 0%;">#</th>															
												<th style="width: 15%;">Kelompok Pasien</th>															
												<th style="width: 25%;">Rekanan </th>															
												<th style="width: 50%;">Group Pembayaran </th>
												<th style="width: 10%;">Actions</th>
											</tr>
											<tr>								
												<td></td>
												<td>
													<select name="idkelompokpasien_rajal_piutang" id="idkelompokpasien_rajal_piutang" style="width: 100%" id="step" class="js-select2 form-control input-sm idkelompokpasien">	
													</select>										
												</td>
												<td>
													<select name="idrekanan_rajal_piutang" id="idrekanan_rajal_piutang" style="width: 100%" id="step" class="js-select2 form-control input-sm rekanan">										
														<option value="#" selected>- All Rekanan -</option>	
													</select>										
												</td>								
												
												<td>
													<select name="idakun_rajal_piutang" id="idakun_rajal_piutang" style="width: 100%" class="js-select2 form-control input-sm idakun">										
														
													</select>
													<input type="hidden" class="form-control" id="id_edit_rajal" placeholder="0" name="id_edit_rajal" value="">
												</td>	
												
												<td>									
													<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_rajal_piutang" title="Masukan Item"><i class="fa fa-save"></i></button>
													<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_rajal_piutang" title="Batalkan"><i class="fa fa-refresh"></i></button>
												</td>
											</tr>
										</thead>
										<tbody>
											
										</tbody>
										
								</table>
								</div>
							</div>
							
							
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="block block-themed ">
						<div class="block-header bg-primary">
							<ul class="block-options">

								<li>
									<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title">RAWAT INAP & ODS</h3>
						</div>
						<div class="block-content">
							<div class="form-group">
								<label class="col-md-2 control-label" ><?=('Proses Approval')?></label>
								<input type="hidden" class="form-control" id="tmp_idakun_tunai_ranap" placeholder="0" name="tmp_idakun_tunai_ranap" value="{idakun_tunai_ranap}">
								<div class="col-md-4">
									<select id="st_auto_posting_ranap" name="st_auto_posting_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="1" <?=(1 == $st_auto_posting_ranap ? 'selected' : ''); ?>>AUTO POSTING</option>
											<option value="2" <?=(2 == $st_auto_posting_ranap ? 'selected' : ''); ?>>MANUAL</option>
										</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" ><?=('Akun Pembayaran Tunai')?> </label>
								
								<div class="col-md-8">
									<select id="idakun_tunai_ranap" name="idakun_tunai_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<?foreach($list_akun as $r){?>
										<option value="<?=$r->id?>" <?=($r->id == $idakun_tunai_ranap? 'selected' : ''); ?>><?=$r->noakun.' - '.$r->namaakun.' ('.$r->id.')'?></option>
									<?}?>
									</select>
								</div>
								
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" ><?=('Group Akun Pembulatan')?> </label>
								
								<div class="col-md-8">
									<select id="group_round_ranap" name="group_round_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<?foreach($list_group as $r){?>
										<option value="<?=$r->id?>" <?=($r->id == $group_round_ranap? 'selected' : ''); ?>><?=$r->nama?></option>
									<?}?>
									</select>
								</div>
								
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" ><?=('Group Akun Diskon ')?> </label>
								
								<div class="col-md-8">
									<select id="group_diskon_ranap" name="group_diskon_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<?foreach($list_group as $r){?>
										<option value="<?=$r->id?>" <?=($r->id == $group_diskon_ranap? 'selected' : ''); ?>><?=$r->nama?></option>
									<?}?>
									</select>
								</div>
								
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" ><?=('Group Akun Tidak Tertagih')?> </label>
								
								<div class="col-md-8">
									<select id="group_tidak_tertagih_ranap" name="group_tidak_tertagih_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<?foreach($list_group as $r){?>
										<option value="<?=$r->id?>" <?=($r->id == $group_tidak_tertagih_ranap? 'selected' : ''); ?>><?=$r->nama?></option>
									<?}?>
									</select>
								</div>
								
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" ><?=('Group Akun Pegawai / Dokter')?> </label>
								
								<div class="col-md-8">
									<select id="group_karyawan_ranap" name="group_karyawan_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<?foreach($list_group as $r){?>
										<option value="<?=$r->id?>" <?=($r->id == $group_karyawan_ranap? 'selected' : ''); ?>><?=$r->nama?></option>
									<?}?>
									</select>
								</div>
								
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" ><?=('Batas Waktu Batal')?></label>
								
								<div class="col-md-4">
									<input type="text" style="width: 100%"  class="form-control number" id="batas_batal_ranap" placeholder="0" name="batas_batal_ranap" value="<?=$batas_batal_ranap?>">
								</div>
								<div class="col-md-2">
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="btn_update_ranap" title="Update"><i class="fa fa-save"></i> UPDATE</button>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-1 control-label"><h4><?=text_primary('SETTING PENDAPATAN GABUNGAN RAWAT INAP & ODS')?></h4></label>
								
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<table id="tabel_ranap" class="table table-striped table-bordered" style="margin-bottom: 0;">
										<thead>
											<tr>
												<th style="width: 0%;">#</th>															
												<th style="width: 15%;">Kelompok Pasien</th>															
												<th style="width: 25%;">Rekanan </th>															
												<th style="width: 50%;">Group Pembayaran </th>
												<th style="width: 10%;">Actions</th>
											</tr>
											<tr>								
												<td></td>
												<td>
													<select name="idkelompokpasien_ranap" id="idkelompokpasien_ranap" style="width: 100%" id="step" class="js-select2 form-control input-sm idkelompokpasien">										
																																
														
													</select>										
												</td>
												<td>
													<select name="idrekanan_ranap" id="idrekanan_ranap" style="width: 100%" id="step" class="js-select2 form-control input-sm rekanan">										
														<option value="#" selected>- All Rekanan -</option>	
													</select>										
												</td>								
												
												<td>
													<select name="idakun_ranap" id="idakun_ranap" style="width: 100%" class="js-select2 form-control input-sm idakun">										
														
													</select>
													<input type="hidden" class="form-control" id="id_edit_ranap" placeholder="0" name="id_edit_ranap" value="">
												</td>	
												
												<td>									
													<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_ranap" title="Masukan Item"><i class="fa fa-save"></i></button>
													<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_ranap" title="Batalkan"><i class="fa fa-refresh"></i></button>
												</td>
											</tr>
										</thead>
										<tbody>
											
										</tbody>
										
								</table>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-1 control-label"><h4><?=text_danger('SETTING PIUTANG ASURANSI')?></h4></label>
								
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<table id="tabel_ranap_piutang" class="table table-striped table-bordered" style="margin-bottom: 0;">
										<thead>
											<tr>
												<th style="width: 0%;">#</th>															
												<th style="width: 15%;">Kelompok Pasien</th>															
												<th style="width: 25%;">Rekanan </th>															
												<th style="width: 50%;">Group Pembayaran </th>
												<th style="width: 10%;">Actions</th>
											</tr>
											<tr>								
												<td></td>
												<td>
													<select name="idkelompokpasien_ranap_piutang" id="idkelompokpasien_ranap_piutang" style="width: 100%" id="step" class="js-select2 form-control input-sm idkelompokpasien">										
																																
														
													</select>										
												</td>
												<td>
													<select name="idrekanan_ranap_piutang" id="idrekanan_ranap_piutang" style="width: 100%" id="step" class="js-select2 form-control input-sm rekanan">										
														<option value="#" selected>- All Rekanan -</option>	
													</select>										
												</td>								
												
												<td>
													<select name="idakun_ranap_piutang" id="idakun_ranap_piutang" style="width: 100%" class="js-select2 form-control input-sm idakun">										
														
													</select>
													<input type="hidden" class="form-control" id="id_edit_ranap_piutang" placeholder="0" name="id_edit_ranap_piutang" value="">
												</td>	
												
												<td>									
													<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_ranap_piutang" title="Masukan Item"><i class="fa fa-save"></i></button>
													<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_ranap_piutang" title="Batalkan"><i class="fa fa-refresh"></i></button>
												</td>
											</tr>
										</thead>
										<tbody>
											
										</tbody>
										
								</table>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-1 control-label"><h4><?=text_success('SETTING TARIF ADMINISTRASI')?></h4></label>
								
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<table id="tabel_ranap_adm" class="table table-striped table-bordered" style="margin-bottom: 0;">
										<thead>
											<tr>
												<th style="width: 0%;">#</th>															
												<th style="width: 15%;">Kelompok Pasien</th>															
												<th style="width: 15%;">Rekanan </th>															
												<th style="width: 30%;">Group Tarif </th>
												<th style="width: 30%;">Group Diskon </th>
												<th style="width: 5%;">Actions</th>
											</tr>
											<tr>								
												<td></td>
												<td>
													<select name="idkelompokpasien_ranap_adm" id="idkelompokpasien_ranap_adm" style="width: 100%" id="step" class="js-select2 form-control input-sm idkelompokpasien">										
																																
														
													</select>										
												</td>
												<td>
													<select name="idrekanan_ranap_adm" id="idrekanan_ranap_adm" style="width: 100%" id="step" class="js-select2 form-control input-sm rekanan">										
														<option value="#" selected>- All Rekanan -</option>	
													</select>										
												</td>								
												
												<td>
													<select name="idakun_ranap_adm" id="idakun_ranap_adm" style="width: 100%" class="js-select2 form-control input-sm idakun">										
														
													</select>
													<input type="hidden" class="form-control" id="id_edit_ranap_adm" placeholder="0" name="id_edit_ranap_adm" value="">
												</td>	
												<td>
													<select name="idakun_ranap_diskon_adm" id="idakun_ranap_diskon_adm" style="width: 100%" class="js-select2 form-control input-sm idakun">										
														
													</select>
												</td>	
												
												<td>									
													<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_ranap_adm" title="Masukan Item"><i class="fa fa-save"></i></button>
													<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_ranap_adm" title="Batalkan"><i class="fa fa-refresh"></i></button>
												</td>
											</tr>
										</thead>
										<tbody>
											
										</tbody>
										
								</table>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			
			
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		list_akun();
		list_kelompok();
		
		load_rajal();
		load_rajal_piutang();
		load_ranap();
		load_ranap_piutang();
		load_ranap_adm();
		clear_input_rajal();
		clear_input_rajal_piutang();
		clear_input_ranap();
		clear_input_ranap_piutang();
		clear_input_ranap_adm();

	});	
	function list_rekanan($idkelompokpasien){
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_general/list_rekanan/'+$idkelompokpasien,
			dataType: "json",
			success: function(data) {
				$(".rekanan").empty();
				$('.rekanan').append('<option value="#" selected>- All Rekanan -</option>');
				$('.rekanan').append(data.detail);
				
			}
		});		
	}
	function list_akun(){
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_general/list_akun_group',
			dataType: "json",
			success: function(data) {
				$(".idakun").empty();
				$('.idakun').append('<option value="#" selected>- Pilih Group Akun -</option>');
				$('.idakun').append(data.detail);
				
			}
		});		
	}
	function list_kelompok(){
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_general/list_kelompok',
			dataType: "json",
			success: function(data) {
				$(".idkelompokpasien").empty();
				$('.idkelompokpasien').append('<option value="#" selected>- Pilih Kelompok Pasien -</option>');
				$('.idkelompokpasien').append(data.detail);
				
			}
		});		
	}
	$(".idkelompokpasien").change(function(){
		list_rekanan($(this).val());
	});

	
	
	//ULAINGI
	$(document).on("click","#btn_update_rajal",function(){
		if (validate_update_rajal()==false)return false;
		var st_auto_posting_rajal=$("#st_auto_posting_rajal").val();
		var idakun_tunai_rajal=$("#idakun_tunai_rajal").val();
		var batas_batal_rajal=$("#batas_batal_rajal").val();
		var group_tuslah_racikan=$("#group_tuslah_racikan").val();
		var group_diskon_rajal=$("#group_diskon_rajal").val();
		var group_tidak_tertagih_rajal=$("#group_tidak_tertagih_rajal").val();
		var group_karyawan_rajal=$("#group_karyawan_rajal").val();
		var group_diskon_rajal=$("#group_diskon_rajal").val();
		var group_round_rajal=$("#group_round_rajal").val();
		$("#tmp_idakun_tunai_rajal").val(idakun_tunai_rajal)
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_general/update_rajal',
			type: 'POST',
			data: {
				st_auto_posting_rajal:st_auto_posting_rajal,idakun_tunai_rajal:idakun_tunai_rajal,batas_batal_rajal:batas_batal_rajal,group_tuslah_racikan:group_tuslah_racikan,
				group_diskon_rajal:group_diskon_rajal,
				group_round_rajal:group_round_rajal,
				group_tidak_tertagih_rajal:group_tidak_tertagih_rajal,
				group_karyawan_rajal:group_karyawan_rajal,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$("#idakun_tunai_rajal").val($("#tmp_idakun_tunai_rajal").val()).trigger('change');
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function validate_update_rajal()
	{
		
		if ($("#st_auto_posting_rajal").val()=='#'){
			sweetAlert("Maaf...", "Setting Harus diisi", "error");
			return false;
		}
		if ($("#batas_batal_rajal").val()==''){
			sweetAlert("Maaf...", "Batas Waktu Batal Harus diisi", "error");
			return false;
		}
		if ($("#idakun_tunai_rajal").val()=='#'){
			sweetAlert("Maaf...", "No AKun Pembayaran Kredit Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	
	function validate_add_rajal()
	{
		
		
		if ($("#idkelompokpasien_rajal").val()=='#'){
			sweetAlert("Maaf...", "Kelompok Pasien Harus diisi", "error");
			return false;
		}
		if ($("#idakun_rajal").val()=='#'){
			sweetAlert("Maaf...", "Group AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function validate_add_rajal_piutang()
	{
		
		
		if ($("#idkelompokpasien_rajal_piutang").val()=='#'){
			sweetAlert("Maaf...", "Kelompok Pasien Harus diisi", "error");
			return false;
		}
		if ($("#idakun_rajal_piutang").val()=='#'){
			sweetAlert("Maaf...", "Group AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_rajal(){
		$("#idkelompokpasien_rajal").val('#').trigger('change');	
		$("#idrekanan_rajal").val('#').trigger('change');	
		$("#id_edit_rajal").val('');		
		$("#idakun_rajal").val('#').trigger('change');
		// alert($("#idakun").val());
	}
	function clear_input_rajal_piutang(){
		$("#idkelompokpasien_rajal_piutang").val('#').trigger('change');	
		$("#idrekanan_rajal_piutang").val('#').trigger('change');	
		$("#id_edit_rajal_piutang").val('');		
		$("#idakun_rajal_piutang").val('#').trigger('change');
		// alert($("#idakun").val());
	}
	$(document).on("click","#simpan_rajal",function(){
		if (validate_add_rajal()==false)return false;
		var id_edit=$("#id_edit_rajal").val();
		var idkelompokpasien=$("#idkelompokpasien_rajal").val();
		var idrekanan=$("#idrekanan_rajal").val();
		var idakun=$("#idakun_rajal").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_general/simpan_rajal',
			type: 'POST',
			data: {
				id_edit:id_edit,idrekanan:idrekanan,
				idakun:idakun,idkelompokpasien:idkelompokpasien,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_rajal').DataTable().ajax.reload( null, false );
					clear_input_rajal();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	$(document).on("click","#simpan_rajal_piutang",function(){
		if (validate_add_rajal_piutang()==false)return false;
		var id_edit=$("#id_edit_rajal_piutang").val();
		var idkelompokpasien=$("#idkelompokpasien_rajal_piutang").val();
		var idrekanan=$("#idrekanan_rajal_piutang").val();
		var idakun=$("#idakun_rajal_piutang").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_general/simpan_rajal_piutang',
			type: 'POST',
			data: {
				id_edit:id_edit,idrekanan:idrekanan,
				idakun:idakun,idkelompokpasien:idkelompokpasien,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_rajal_piutang').DataTable().ajax.reload( null, false );
					clear_input_rajal_piutang();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	function hapus_rajal($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_pendapatan_general/hapus_rajal/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_rajal').DataTable().ajax.reload( null, false );
						clear_input_rajal();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	function hapus_rajal_piutang($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_pendapatan_general/hapus_rajal_piutang/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_rajal_piutang').DataTable().ajax.reload( null, false );
						clear_input_rajal_piutang();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_rajal",function(){
		
		clear_input_rajal();
	});
	$(document).on("click","#clear_rajal_piutang",function(){
		
		clear_input_rajal_piutang();
	});
	function load_rajal(){
		var idlogic=$("#id").val();
		
		$('#tabel_rajal').DataTable().destroy();
		var table = $('#tabel_rajal').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pendapatan_general/load_rajal/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	function load_rajal_piutang(){
		var idlogic=$("#id").val();
		// alert('sini');
		$('#tabel_rajal_piutang').DataTable().destroy();
		var table = $('#tabel_rajal_piutang').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pendapatan_general/load_rajal_piutang/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	//ULAINGI
	$(document).on("click","#btn_update_ranap",function(){
		if (validate_update_ranap()==false)return false;
		var st_auto_posting_ranap=$("#st_auto_posting_ranap").val();
		var idakun_tunai_ranap=$("#idakun_tunai_ranap").val();
		var batas_batal_ranap=$("#batas_batal_ranap").val();
		var group_diskon_ranap=$("#group_diskon_ranap").val();
		var group_tidak_tertagih_ranap=$("#group_tidak_tertagih_ranap").val();
		var group_karyawan_ranap=$("#group_karyawan_ranap").val();
		var group_round_ranap=$("#group_round_ranap").val();
		$("#tmp_idakun_tunai_ranap").val(idakun_tunai_ranap)
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_general/update_ranap',
			type: 'POST',
			data: {
				st_auto_posting_ranap:st_auto_posting_ranap,idakun_tunai_ranap:idakun_tunai_ranap,batas_batal_ranap:batas_batal_ranap,
				group_diskon_ranap:group_diskon_ranap,
				group_tidak_tertagih_ranap:group_tidak_tertagih_ranap,
				group_karyawan_ranap:group_karyawan_ranap,
				group_round_ranap:group_round_ranap,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$("#idakun_tunai_ranap").val($("#tmp_idakun_tunai_ranap").val()).trigger('change');
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function validate_update_ranap()
	{
		
		if ($("#st_auto_posting_ranap").val()=='#'){
			sweetAlert("Maaf...", "Setting Harus diisi", "error");
			return false;
		}
		if ($("#batas_batal_ranap").val()==''){
			sweetAlert("Maaf...", "Batas Waktu Batal Harus diisi", "error");
			return false;
		}
		if ($("#idakun_tunai_ranap").val()=='#'){
			sweetAlert("Maaf...", "No AKun Pembayaran Kredit Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	
	function validate_add_ranap()
	{
		
		
		if ($("#idkelompokpasien_ranap").val()=='#'){
			sweetAlert("Maaf...", "Kelompok Pasien Harus diisi", "error");
			return false;
		}
		if ($("#idakun_ranap").val()=='#'){
			sweetAlert("Maaf...", "Group AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_ranap(){
		$("#idkelompokpasien_ranap").val('#').trigger('change');	
		$("#idrekanan_ranap").val('#').trigger('change');	
		$("#id_edit_ranap").val('');		
		$("#idakun_ranap").val('#').trigger('change');
		// alert($("#idakun").val());
	}
	$(document).on("click","#simpan_ranap",function(){
		if (validate_add_ranap()==false)return false;
		var id_edit=$("#id_edit_ranap").val();
		var idkelompokpasien=$("#idkelompokpasien_ranap").val();
		var idrekanan=$("#idrekanan_ranap").val();
		var idakun=$("#idakun_ranap").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_general/simpan_ranap',
			type: 'POST',
			data: {
				id_edit:id_edit,idrekanan:idrekanan,
				idakun:idakun,idkelompokpasien:idkelompokpasien,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_ranap').DataTable().ajax.reload( null, false );
					clear_input_ranap();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	function hapus_ranap($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_pendapatan_general/hapus_ranap/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_ranap').DataTable().ajax.reload( null, false );
						clear_input_ranap();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_ranap",function(){
		
		clear_input_ranap();
	});
	function load_ranap(){
		var idlogic=$("#id").val();
		
		$('#tabel_ranap').DataTable().destroy();
		var table = $('#tabel_ranap').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pendapatan_general/load_ranap/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	//ULAINGI
	$(document).on("click","#btn_update_ranap_piutang",function(){
		if (validate_update_ranap_piutang()==false)return false;
		var st_auto_posting_ranap_piutang=$("#st_auto_posting_ranap_piutang").val();
		var idakun_tunai_ranap_piutang=$("#idakun_tunai_ranap_piutang").val();
		var batas_batal_ranap_piutang=$("#batas_batal_ranap_piutang").val();
		var group_diskon_ranap_piutang=$("#group_diskon_ranap_piutang").val();
		$("#tmp_idakun_tunai_ranap_piutang").val(idakun_tunai_ranap_piutang)
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_general/update_ranap_piutang',
			type: 'POST',
			data: {
				st_auto_posting_ranap_piutang:st_auto_posting_ranap_piutang,idakun_tunai_ranap_piutang:idakun_tunai_ranap_piutang,batas_batal_ranap_piutang:batas_batal_ranap_piutang,group_diskon_ranap_piutang:group_diskon_ranap_piutang,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$("#idakun_tunai_ranap_piutang").val($("#tmp_idakun_tunai_ranap_piutang").val()).trigger('change');
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function validate_update_ranap_piutang()
	{
		
		if ($("#st_auto_posting_ranap_piutang").val()=='#'){
			sweetAlert("Maaf...", "Setting Harus diisi", "error");
			return false;
		}
		if ($("#batas_batal_ranap_piutang").val()==''){
			sweetAlert("Maaf...", "Batas Waktu Batal Harus diisi", "error");
			return false;
		}
		if ($("#idakun_tunai_ranap_piutang").val()=='#'){
			sweetAlert("Maaf...", "No AKun Pembayaran Kredit Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	
	function validate_add_ranap_piutang()
	{
		
		
		if ($("#idkelompokpasien_ranap_piutang").val()=='#'){
			sweetAlert("Maaf...", "Kelompok Pasien Harus diisi", "error");
			return false;
		}
		if ($("#idakun_ranap_piutang").val()=='#'){
			sweetAlert("Maaf...", "Group AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_ranap_piutang(){
		$("#idkelompokpasien_ranap_piutang").val('#').trigger('change');	
		$("#idrekanan_ranap_piutang").val('#').trigger('change');	
		$("#id_edit_ranap_piutang").val('');		
		$("#idakun_ranap_piutang").val('#').trigger('change');
		// alert($("#idakun").val());
	}
	$(document).on("click","#simpan_ranap_piutang",function(){
		if (validate_add_ranap_piutang()==false)return false;
		var id_edit=$("#id_edit_ranap_piutang").val();
		var idkelompokpasien=$("#idkelompokpasien_ranap_piutang").val();
		var idrekanan=$("#idrekanan_ranap_piutang").val();
		var idakun=$("#idakun_ranap_piutang").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_general/simpan_ranap_piutang',
			type: 'POST',
			data: {
				id_edit:id_edit,idrekanan:idrekanan,
				idakun:idakun,idkelompokpasien:idkelompokpasien,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_ranap_piutang').DataTable().ajax.reload( null, false );
					clear_input_ranap_piutang();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	function hapus_ranap_piutang($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_pendapatan_general/hapus_ranap_piutang/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_ranap_piutang').DataTable().ajax.reload( null, false );
						clear_input_ranap_piutang();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_ranap_piutang",function(){
		
		clear_input_ranap_piutang();
	});
	function load_ranap_piutang(){
		var idlogic=$("#id").val();
		
		$('#tabel_ranap_piutang').DataTable().destroy();
		var table = $('#tabel_ranap_piutang').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pendapatan_general/load_ranap_piutang/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	$(document).on("click","#btn_update_ranap_adm",function(){
		if (validate_update_ranap_adm()==false)return false;
		var st_auto_posting_ranap_adm=$("#st_auto_posting_ranap_adm").val();
		var idakun_tunai_ranap_adm=$("#idakun_tunai_ranap_adm").val();
		var batas_batal_ranap_adm=$("#batas_batal_ranap_adm").val();
		var group_diskon_ranap_adm=$("#group_diskon_ranap_adm").val();
		$("#tmp_idakun_tunai_ranap_adm").val(idakun_tunai_ranap_adm)
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_general/update_ranap_adm',
			type: 'POST',
			data: {
				st_auto_posting_ranap_adm:st_auto_posting_ranap_adm,idakun_tunai_ranap_adm:idakun_tunai_ranap_adm,batas_batal_ranap_adm:batas_batal_ranap_adm,group_diskon_ranap_adm:group_diskon_ranap_adm,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$("#idakun_tunai_ranap_adm").val($("#tmp_idakun_tunai_ranap_adm").val()).trigger('change');
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function validate_update_ranap_adm()
	{
		
		if ($("#st_auto_posting_ranap_adm").val()=='#'){
			sweetAlert("Maaf...", "Setting Harus diisi", "error");
			return false;
		}
		if ($("#batas_batal_ranap_adm").val()==''){
			sweetAlert("Maaf...", "Batas Waktu Batal Harus diisi", "error");
			return false;
		}
		if ($("#idakun_tunai_ranap_adm").val()=='#'){
			sweetAlert("Maaf...", "No AKun Pembayaran Kredit Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	
	function validate_add_ranap_adm()
	{
		
		
		if ($("#idkelompokpasien_ranap_adm").val()=='#'){
			sweetAlert("Maaf...", "Kelompok Pasien Harus diisi", "error");
			return false;
		}
		if ($("#idakun_ranap_adm").val()=='#'){
			sweetAlert("Maaf...", "Group AKun Harus diisi", "error");
			return false;
		}
		if ($("#idakun_ranap_diskon_adm").val()=='#'){
			sweetAlert("Maaf...", "Group AKun Diskon Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_ranap_adm(){
		$("#idkelompokpasien_ranap_adm").val('#').trigger('change');	
		$("#idrekanan_ranap_adm").val('#').trigger('change');	
		$("#id_edit_ranap_adm").val('');		
		$("#idakun_ranap_adm").val('#').trigger('change');
		// alert($("#idakun").val());
	}
	$(document).on("click","#simpan_ranap_adm",function(){
		if (validate_add_ranap_adm()==false)return false;
		var id_edit=$("#id_edit_ranap_adm").val();
		var idkelompokpasien=$("#idkelompokpasien_ranap_adm").val();
		var idrekanan=$("#idrekanan_ranap_adm").val();
		var idakun=$("#idakun_ranap_adm").val();
		var idakun_diskon=$("#idakun_ranap_diskon_adm").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_general/simpan_ranap_adm',
			type: 'POST',
			data: {
				id_edit:id_edit,idrekanan:idrekanan,
				idakun:idakun,idkelompokpasien:idkelompokpasien,idakun_diskon:idakun_diskon,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_ranap_adm').DataTable().ajax.reload( null, false );
					clear_input_ranap_adm();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	function hapus_ranap_adm($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_pendapatan_general/hapus_ranap_adm/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_ranap_adm').DataTable().ajax.reload( null, false );
						clear_input_ranap_adm();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_ranap_adm",function(){
		
		clear_input_ranap_adm();
	});
	function load_ranap_adm(){
		var idlogic=$("#id").val();
		
		$('#tabel_ranap_adm').DataTable().destroy();
		var table = $('#tabel_ranap_adm').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pendapatan_general/load_ranap_adm/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
</script>
