<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content" style="margin-top: -20px;font-size:">
		<?php echo form_open('msetting_jurnal_pendapatan_jual_ko/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>	
		<?
		$q="SELECT *FROM mdata_tipebarang WHERE id IN(1,3)";
		$list_tipe=$this->db->query($q)->result();
		
		
		$q="SELECT *FROM mkelas WHERE status='1'";
		$list_kelas=$this->db->query($q)->result();
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="block block-themed block-opt-hidden">
					<div class="block-header bg-primary">
						<ul class="block-options">

							<li>
								<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">SETTING AKUN PENJUALAN NARCOSE KAMAR OPERASI</h3>
					</div>
					
					<div class="block-content">						
						<input type="hidden" class="form-control" id="id_edit_narcose" placeholder="0" name="id_edit_narcose" value="">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Tipe Operasi</label>							
							<div class="col-md-4">
								<select name="idtipe_narcose" id="idtipe_narcose" style="width: 100%" class="js-select2 form-control input-sm idbarang">
									<option value="#" selected>- Semua Tipe -</option>		
									<option value="1">Rawat Inap</option>		
									<option value="2">ODS</option>		
									
								</select>	
							</div>
							<label class="col-md-2 control-label" >Kelas</label>
							<div class="col-md-4">
								<select name="idkelas_narcose" id="idkelas_narcose" style="width: 100%" class="js-select2 form-control input-sm">
									<option value="0" selected>- Semua Kelas -</option>	
									<?foreach($list_kelas as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>																				
									<?}?>	
								</select>
							</div>
						</div>
						
						
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Kategori</label>
							
							<div class="col-md-4">
								<select name="idkategori_narcose" id="idkategori_narcose" style="width: 100%" class="js-select2 form-control input-sm idkategori"></select>
							</div>
							<label class="col-md-2 control-label" >Group Penjualan</label>
							<div class="col-md-4">
								<select name="idgroup_penjualan_narcose" id="idgroup_penjualan_narcose" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" >Barang</label>
							
							<div class="col-md-4">
								<select name="idbarang_narcose" id="idbarang_narcose" style="width: 100%" class="js-select2 form-control input-sm"></select>
							</div>
							<label class="col-md-2 control-label" >Group Diskon</label>
							<div class="col-md-4">
								<select name="idgroup_diskon_narcose" id="idgroup_diskon_narcose" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" ></label>
							
							<div class="col-md-4">
								<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_narcose" title="Masukan Item"><i class="fa fa-save"></i> Simpan</button>
								<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_narcose" title="Batalkan"><i class="fa fa-refresh"></i> Clear Input</button>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<table id="tabel_narcose" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 0%;">#</th>															
											<th style="width: 8%;">Tipe Operasi</th>															
											<th style="width: 8%;">Kelas</th>															
											<th style="width: 9%;">Kategori</th>															
											<th style="width: 11%;">Barang</th>															
											<th style="width: 12%;">Group Pembelian</th>															
											<th style="width: 12%;">Group Diskon</th>															
											<th style="width: 5%;">Actions</th>
										</tr>
										
									</thead>
									<tbody>
										
									</tbody>
									
							</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-md-12">
				<div class="block block-themed block-opt-hidden">
					<div class="block-header bg-success">
						<ul class="block-options">

							<li>
								<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">SETTING AKUN PENJUALAN OBAT KAMAR OPERASI</h3>
					</div>
					
					<div class="block-content">						
						<input type="hidden" class="form-control" id="id_edit_obat" placeholder="0" name="id_edit_obat" value="">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Tipe Operasi</label>							
							<div class="col-md-4">
								<select name="idtipe_obat" id="idtipe_obat" style="width: 100%" class="js-select2 form-control input-sm idbarang">
									<option value="#" selected>- Semua Tipe -</option>		
									<option value="1">Rawat Inap</option>		
									<option value="2">ODS</option>		
									
								</select>	
							</div>
							<label class="col-md-2 control-label" >Kelas</label>
							<div class="col-md-4">
								<select name="idkelas_obat" id="idkelas_obat" style="width: 100%" class="js-select2 form-control input-sm">
									<option value="0" selected>- Semua Kelas -</option>	
									<?foreach($list_kelas as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>																				
									<?}?>	
								</select>
							</div>
						</div>
						
						
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Kategori</label>
							
							<div class="col-md-4">
								<select name="idkategori_obat" id="idkategori_obat" style="width: 100%" class="js-select2 form-control input-sm idkategori"></select>
							</div>
							<label class="col-md-2 control-label" >Group Penjualan</label>
							<div class="col-md-4">
								<select name="idgroup_penjualan_obat" id="idgroup_penjualan_obat" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" >Barang</label>
							
							<div class="col-md-4">
								<select name="idbarang_obat" id="idbarang_obat" style="width: 100%" class="js-select2 form-control input-sm"></select>
							</div>
							<label class="col-md-2 control-label" >Group Diskon</label>
							<div class="col-md-4">
								<select name="idgroup_diskon_obat" id="idgroup_diskon_obat" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" ></label>
							
							<div class="col-md-4">
								<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_obat" title="Masukan Item"><i class="fa fa-save"></i> Simpan</button>
								<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_obat" title="Batalkan"><i class="fa fa-refresh"></i> Clear Input</button>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<table id="tabel_obat" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 0%;">#</th>															
											<th style="width: 8%;">Tipe Operasi</th>															
											<th style="width: 8%;">Kelas</th>															
											<th style="width: 9%;">Kategori</th>															
											<th style="width: 11%;">Barang</th>															
											<th style="width: 12%;">Group Pembelian</th>															
											<th style="width: 12%;">Group Diskon</th>															
											<th style="width: 5%;">Actions</th>
										</tr>
										
									</thead>
									<tbody>
										
									</tbody>
									
							</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		
		<div class="row">
			<div class="col-md-12">
				<div class="block block-themed  block-opt-hidden">
					<div class="block-header bg-default">
						<ul class="block-options">

							<li>
								<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">SETTING AKUN PENJUALAN ALKES KAMAR OPERASI</h3>
					</div>
					
					<div class="block-content">						
						<input type="hidden" class="form-control" id="id_edit_alkes" placeholder="0" name="id_edit_alkes" value="">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Tipe Operasi</label>							
							<div class="col-md-4">
								<select name="idtipe_alkes" id="idtipe_alkes" style="width: 100%" class="js-select2 form-control input-sm idbarang">
									<option value="#" selected>- Semua Tipe -</option>		
									<option value="1">Rawat Inap</option>		
									<option value="2">ODS</option>		
									
								</select>	
							</div>
							<label class="col-md-2 control-label" >Kelas</label>
							<div class="col-md-4">
								<select name="idkelas_alkes" id="idkelas_alkes" style="width: 100%" class="js-select2 form-control input-sm">
									<option value="0" selected>- Semua Kelas -</option>	
									<?foreach($list_kelas as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>																				
									<?}?>	
								</select>
							</div>
						</div>
						
						
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Kategori</label>
							
							<div class="col-md-4">
								<select name="idkategori_alkes" id="idkategori_alkes" style="width: 100%" class="js-select2 form-control input-sm idkategori"></select>
							</div>
							<label class="col-md-2 control-label" >Group Penjualan</label>
							<div class="col-md-4">
								<select name="idgroup_penjualan_alkes" id="idgroup_penjualan_alkes" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" >Barang</label>
							
							<div class="col-md-4">
								<select name="idbarang_alkes" id="idbarang_alkes" style="width: 100%" class="js-select2 form-control input-sm"></select>
							</div>
							<label class="col-md-2 control-label" >Group Diskon</label>
							<div class="col-md-4">
								<select name="idgroup_diskon_alkes" id="idgroup_diskon_alkes" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" ></label>
							
							<div class="col-md-4">
								<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_alkes" title="Masukan Item"><i class="fa fa-save"></i> Simpan</button>
								<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_alkes" title="Batalkan"><i class="fa fa-refresh"></i> Clear Input</button>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<table id="tabel_alkes" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 0%;">#</th>															
											<th style="width: 8%;">Tipe Operasi</th>															
											<th style="width: 8%;">Kelas</th>															
											<th style="width: 9%;">Kategori</th>															
											<th style="width: 11%;">Barang</th>															
											<th style="width: 12%;">Group Pembelian</th>															
											<th style="width: 12%;">Group Diskon</th>															
											<th style="width: 5%;">Actions</th>
										</tr>
										
									</thead>
									<tbody>
										
									</tbody>
									
							</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-md-12">
				<div class="block block-themed  block-opt-hidden">
					<div class="block-header bg-danger">
						<ul class="block-options">

							<li>
								<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">SETTING AKUN PENJUALAN IMPLAN KAMAR OPERASI</h3>
					</div>
					
					<div class="block-content">						
						<input type="hidden" class="form-control" id="id_edit_implan" placeholder="0" name="id_edit_implan" value="">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Tipe Operasi</label>							
							<div class="col-md-4">
								<select name="idtipe_implan" id="idtipe_implan" style="width: 100%" class="js-select2 form-control input-sm idbarang">
									<option value="#" selected>- Semua Tipe -</option>		
									<option value="1">Rawat Inap</option>		
									<option value="2">ODS</option>		
									
								</select>	
							</div>
							<label class="col-md-2 control-label" >Kelas</label>
							<div class="col-md-4">
								<select name="idkelas_implan" id="idkelas_implan" style="width: 100%" class="js-select2 form-control input-sm">
									<option value="0" selected>- Semua Kelas -</option>	
									<?foreach($list_kelas as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>																				
									<?}?>	
								</select>
							</div>
						</div>
						
						
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Kategori</label>
							
							<div class="col-md-4">
								<select name="idkategori_implan" id="idkategori_implan" style="width: 100%" class="js-select2 form-control input-sm idkategori"></select>
							</div>
							<label class="col-md-2 control-label" >Group Penjualan</label>
							<div class="col-md-4">
								<select name="idgroup_penjualan_implan" id="idgroup_penjualan_implan" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" >Barang</label>
							
							<div class="col-md-4">
								<select name="idbarang_implan" id="idbarang_implan" style="width: 100%" class="js-select2 form-control input-sm"></select>
							</div>
							<label class="col-md-2 control-label" >Group Diskon</label>
							<div class="col-md-4">
								<select name="idgroup_diskon_implan" id="idgroup_diskon_implan" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" ></label>
							
							<div class="col-md-4">
								<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_implan" title="Masukan Item"><i class="fa fa-save"></i> Simpan</button>
								<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_implan" title="Batalkan"><i class="fa fa-refresh"></i> Clear Input</button>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<table id="tabel_implan" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 0%;">#</th>															
											<th style="width: 8%;">Tipe Operasi</th>															
											<th style="width: 8%;">Kelas</th>															
											<th style="width: 9%;">Kategori</th>															
											<th style="width: 11%;">Barang</th>															
											<th style="width: 12%;">Group Pembelian</th>															
											<th style="width: 12%;">Group Diskon</th>															
											<th style="width: 5%;">Actions</th>
										</tr>
										
									</thead>
									<tbody>
										
									</tbody>
									
							</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		list_group();
		list_kategori_narcose(3);
		list_barang_narcose();
		load_narcose();
		
		list_kategori_obat(3);
		list_barang_obat();
		load_obat();
		
		list_kategori_alkes(1);
		list_barang_alkes();
		load_alkes();
		
		list_kategori_implan(2);
		list_barang_implan();
		load_implan();
		
	});	
	function list_group(){
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_general/list_akun_group',
			dataType: "json",
			success: function(data) {
				$(".idgroup").empty();
				$('.idgroup').append('<option value="#" selected>- Pilih Group -</option>');
				$('.idgroup').append(data.detail);
				
			}
		});		
	}
	function hapus($id,$jenis){
		var jenis=$jenis;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_pendapatan_jual_ko/hapus/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						if (jenis=='1'){
							$('#tabel_narcose').DataTable().ajax.reload( null, false );							
						}
						if (jenis=='2'){
							$('#tabel_obat').DataTable().ajax.reload( null, false );							
						}
						if (jenis=='3'){
							$('#tabel_alkes').DataTable().ajax.reload( null, false );							
						}
						if (jenis=='4'){
							$('#tabel_implan').DataTable().ajax.reload( null, false );							
						}
						clear_input_narcose();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	//NARCOSE
	$(document).on("change","#idkategori_narcose",function(){
		list_barang_narcose();
	});
	function list_kategori_narcose($id){
		if ($id!='#'){
			$.ajax({
				url: '{site_url}msetting_jurnal_pembelian/list_kategori/'+$id,
				dataType: "json",
				success: function(data) {
					$("#idkategori_narcose").empty();
					$('#idkategori_narcose').append('<option value="0" selected>- Semua Kategori -</option>');
					$('#idkategori_narcose').append(data.detail);
					$('#idkategori_narcose').val(0).trigger('change');
				}
			});
		}else{
			$("#idkategori_narcose").empty();
			$('#idkategori_narcose').append('<option value="0" selected>- Semua Kategori -</option>');
			$('#idkategori_narcose').val(0).trigger('change');
		}		
	}
	function list_barang_narcose(){
		$('#idbarang_narcose').val(null).trigger('change');
		var newOption = new Option('- Semua Barang -', '0', false, false);
		$("#idbarang_narcose").append(newOption);
		$("#idbarang_narcose").val("0").trigger('change');
		
		var idtipe='3';
		var idkategori=$("#idkategori_narcose").val();
		
		$("#idbarang_narcose").select2({
			minimumInputLength: 0,
			noResults: 'Obat Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}msetting_jurnal_pembelian/list_barang/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,idtipe: idtipe,idkategori:idkategori,
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.kode+' - '+item.nama,
								id: item.id,
								idtipe : item.idtipe
							}
						})
					};
				}
			}
		});

	}
	
	function validate_add_narcose()
	{
			var idtipe_narcose=$("#idtipe_narcose").val();
			var idgroup_penjualan_narcose=$("#idgroup_penjualan_narcose").val();
			var idgroup_diskon_narcose=$("#idgroup_diskon_narcose").val();
			var idgroup_tuslah_narcose=$("#idgroup_tuslah_narcose").val();
		
					
			if (idgroup_penjualan_narcose=='#'){
				sweetAlert("Maaf...", "Group Penjualan Harus diisi", "error");
				return false;
			}
			if (idgroup_diskon_narcose=='#'){
				sweetAlert("Maaf...", "Group Diskon Harus diisi", "error");
				return false;
			}
			
			
		return true;
	}
	function clear_input_narcose(){
		$("#idtipe_narcose").val('#').trigger('change');	
		$("#idgroup_penjualan_narcose").val('#').trigger('change');
		$("#idgroup_diskon_narcose").val('#').trigger('change');
		
		$("#idkategori_narcose").val('0').trigger('change');
		$("#idkelas_narcose").val('0').trigger('change');
		$("#idbarang_narcose").val('0').trigger('change');
	}
	$(document).on("click","#simpan_narcose",function(){
		if (validate_add_narcose()==false)return false;
		var tipe_barang=3;
		var idkelas=$("#idkelas_narcose").val();
		var idtipe=$("#idtipe_narcose").val();
		var idkategori=$("#idkategori_narcose").val();
		var idbarang=$("#idbarang_narcose").val();
		var idgroup_penjualan=$("#idgroup_penjualan_narcose").val();
		var idgroup_diskon=$("#idgroup_diskon_narcose").val();
	
		
		// alert('SINI');
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_jual_ko/simpan_narcose',
			type: 'POST',
			data: {
				idkelas:idkelas
				,idtipe:idtipe
				,tipe_barang:tipe_barang
				,idkategori:idkategori
				,idbarang:idbarang
				,idgroup_penjualan:idgroup_penjualan
				,idgroup_diskon:idgroup_diskon
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_narcose').DataTable().ajax.reload( null, false );
					clear_input_narcose();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	
	$(document).on("click","#clear_narcose",function(){		
		clear_input_narcose();
	});
	function load_narcose(){
		var jenis=1;
		var idlogic=$("#id").val();
		
		$('#tabel_narcose').DataTable().destroy();
		var table = $('#tabel_narcose').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pendapatan_jual_ko/load_setting/',
			type: "POST",
			dataType: 'json',
			data: {
				jenis: jenis
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
					]
		});
	}
	
	//OBAT
	// $(document).on("change","#idkategori_obat",function(){
		// list_barang_obat();
	// });
	function list_kategori_obat($id){
		if ($id!='#'){
			$.ajax({
				url: '{site_url}msetting_jurnal_pembelian/list_kategori/'+$id,
				dataType: "json",
				success: function(data) {
					$("#idkategori_obat").empty();
					$('#idkategori_obat').append('<option value="0" selected>- Semua Kategori -</option>');
					$('#idkategori_obat').append(data.detail);
					$('#idkategori_obat').val(0).trigger('change');
				}
			});
		}else{
			$("#idkategori_obat").empty();
			$('#idkategori_obat').append('<option value="0" selected>- Semua Kategori -</option>');
			$('#idkategori_obat').val(0).trigger('change');
		}		
	}
	function list_barang_obat(){
		$('#idbarang_obat').val(null).trigger('change');
		var newOption = new Option('- Semua Barang -', '0', false, false);
		$("#idbarang_obat").append(newOption);
		$("#idbarang_obat").val("0").trigger('change');
		
		var idtipe='3';
		var idkategori=$("#idkategori_obat").val();
		
		$("#idbarang_obat").select2({
			minimumInputLength: 0,
			noResults: 'Obat Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}msetting_jurnal_pembelian/list_barang/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,idtipe: idtipe,idkategori:idkategori,
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.kode+' - '+item.nama,
								id: item.id,
								idtipe : item.idtipe
							}
						})
					};
				}
			}
		});

	}
	
	function validate_add_obat()
	{
			var idtipe_obat=$("#idtipe_obat").val();
			var idgroup_penjualan_obat=$("#idgroup_penjualan_obat").val();
			var idgroup_diskon_obat=$("#idgroup_diskon_obat").val();
			var idgroup_tuslah_obat=$("#idgroup_tuslah_obat").val();
		
					
			if (idgroup_penjualan_obat=='#'){
				sweetAlert("Maaf...", "Group Penjualan Harus diisi", "error");
				return false;
			}
			if (idgroup_diskon_obat=='#'){
				sweetAlert("Maaf...", "Group Diskon Harus diisi", "error");
				return false;
			}
			
			
		return true;
	}
	function clear_input_obat(){
		$("#idtipe_obat").val('#').trigger('change');	
		$("#idgroup_penjualan_obat").val('#').trigger('change');
		$("#idgroup_diskon_obat").val('#').trigger('change');
		
		$("#idkategori_obat").val('0').trigger('change');
		$("#idkelas_obat").val('0').trigger('change');
		$("#idbarang_obat").val('0').trigger('change');
	}
	$(document).on("click","#simpan_obat",function(){
		if (validate_add_obat()==false)return false;
		var tipe_barang=3;
		var idkelas=$("#idkelas_obat").val();
		var idtipe=$("#idtipe_obat").val();
		var idkategori=$("#idkategori_obat").val();
		var idbarang=$("#idbarang_obat").val();
		var idgroup_penjualan=$("#idgroup_penjualan_obat").val();
		var idgroup_diskon=$("#idgroup_diskon_obat").val();
	
		
		// alert('SINI');
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_jual_ko/simpan_obat',
			type: 'POST',
			data: {
				idkelas:idkelas
				,idtipe:idtipe
				,tipe_barang:tipe_barang
				,idkategori:idkategori
				,idbarang:idbarang
				,idgroup_penjualan:idgroup_penjualan
				,idgroup_diskon:idgroup_diskon
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_obat').DataTable().ajax.reload( null, false );
					clear_input_obat();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	
	$(document).on("click","#clear_obat",function(){		
		clear_input_obat();
	});
	function load_obat(){
		var jenis=2;
		var idlogic=$("#id").val();
		
		$('#tabel_obat').DataTable().destroy();
		var table = $('#tabel_obat').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pendapatan_jual_ko/load_setting/',
			type: "POST",
			dataType: 'json',
			data: {
				jenis: jenis
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
					]
		});
	}
	
	//ALKES
	$(document).on("change","#idkategori_alkes",function(){
		list_barang_alkes();
	});
	function list_kategori_alkes($id){
		if ($id!='#'){
			$.ajax({
				url: '{site_url}msetting_jurnal_pembelian/list_kategori/'+$id,
				dataType: "json",
				success: function(data) {
					$("#idkategori_alkes").empty();
					$('#idkategori_alkes').append('<option value="0" selected>- Semua Kategori -</option>');
					$('#idkategori_alkes').append(data.detail);
					$('#idkategori_alkes').val(0).trigger('change');
				}
			});
		}else{
			$("#idkategori_alkes").empty();
			$('#idkategori_alkes').append('<option value="0" selected>- Semua Kategori -</option>');
			$('#idkategori_alkes').val(0).trigger('change');
		}		
	}
	function list_barang_alkes(){
		$('#idbarang_alkes').val(null).trigger('change');
		var newOption = new Option('- Semua Barang -', '0', false, false);
		$("#idbarang_alkes").append(newOption);
		$("#idbarang_alkes").val("0").trigger('change');
		
		var idtipe='1';
		var idkategori=$("#idkategori_alkes").val();
		
		$("#idbarang_alkes").select2({
			minimumInputLength: 0,
			noResults: 'Obat Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}msetting_jurnal_pembelian/list_barang/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,idtipe: idtipe,idkategori:idkategori,
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.kode+' - '+item.nama,
								id: item.id,
								idtipe : item.idtipe
							}
						})
					};
				}
			}
		});

	}
	
	function validate_add_alkes()
	{
			var idtipe_alkes=$("#idtipe_alkes").val();
			var idgroup_penjualan_alkes=$("#idgroup_penjualan_alkes").val();
			var idgroup_diskon_alkes=$("#idgroup_diskon_alkes").val();
			var idgroup_tuslah_alkes=$("#idgroup_tuslah_alkes").val();
		
					
			if (idgroup_penjualan_alkes=='#'){
				sweetAlert("Maaf...", "Group Penjualan Harus diisi", "error");
				return false;
			}
			if (idgroup_diskon_alkes=='#'){
				sweetAlert("Maaf...", "Group Diskon Harus diisi", "error");
				return false;
			}
			
			
		return true;
	}
	function clear_input_alkes(){
		$("#idtipe_alkes").val('#').trigger('change');	
		$("#idgroup_penjualan_alkes").val('#').trigger('change');
		$("#idgroup_diskon_alkes").val('#').trigger('change');
		
		$("#idkategori_alkes").val('0').trigger('change');
		$("#idkelas_alkes").val('0').trigger('change');
		$("#idbarang_alkes").val('0').trigger('change');
	}
	$(document).on("click","#simpan_alkes",function(){
		if (validate_add_alkes()==false)return false;
		var tipe_barang=1;
		var idkelas=$("#idkelas_alkes").val();
		var idtipe=$("#idtipe_alkes").val();
		var idkategori=$("#idkategori_alkes").val();
		var idbarang=$("#idbarang_alkes").val();
		var idgroup_penjualan=$("#idgroup_penjualan_alkes").val();
		var idgroup_diskon=$("#idgroup_diskon_alkes").val();
	
		
		// alert(idkelas);
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_jual_ko/simpan_alkes',
			type: 'POST',
			data: {
				idkelas:idkelas
				,idtipe:idtipe
				,tipe_barang:tipe_barang
				,idkategori:idkategori
				,idbarang:idbarang
				,idgroup_penjualan:idgroup_penjualan
				,idgroup_diskon:idgroup_diskon
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_alkes').DataTable().ajax.reload( null, false );
					clear_input_alkes();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	
	$(document).on("click","#clear_alkes",function(){		
		clear_input_alkes();
	});
	function load_alkes(){
		var jenis=3;
		var idlogic=$("#id").val();
		
		$('#tabel_alkes').DataTable().destroy();
		var table = $('#tabel_alkes').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pendapatan_jual_ko/load_setting/',
			type: "POST",
			dataType: 'json',
			data: {
				jenis: jenis
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
					]
		});
	}
	//ALKES
	$(document).on("change","#idkategori_implan",function(){
		list_barang_implan();
	});
	function list_kategori_implan($id){
		if ($id!='#'){
			$.ajax({
				url: '{site_url}msetting_jurnal_pembelian/list_kategori/'+$id,
				dataType: "json",
				success: function(data) {
					$("#idkategori_implan").empty();
					$('#idkategori_implan').append('<option value="0" selected>- Semua Kategori -</option>');
					$('#idkategori_implan').append(data.detail);
					$('#idkategori_implan').val(0).trigger('change');
				}
			});
		}else{
			$("#idkategori_implan").empty();
			$('#idkategori_implan').append('<option value="0" selected>- Semua Kategori -</option>');
			$('#idkategori_implan').val(0).trigger('change');
		}		
	}
	function list_barang_implan(){
		$('#idbarang_implan').val(null).trigger('change');
		var newOption = new Option('- Semua Barang -', '0', false, false);
		$("#idbarang_implan").append(newOption);
		$("#idbarang_implan").val("0").trigger('change');
		
		var idtipe='2';
		var idkategori=$("#idkategori_implan").val();
		
		$("#idbarang_implan").select2({
			minimumInputLength: 0,
			noResults: 'Obat Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}msetting_jurnal_pembelian/list_barang/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,idtipe: idtipe,idkategori:idkategori,
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.kode+' - '+item.nama,
								id: item.id,
								idtipe : item.idtipe
							}
						})
					};
				}
			}
		});

	}
	
	function validate_add_implan()
	{
			var idtipe_implan=$("#idtipe_implan").val();
			var idgroup_penjualan_implan=$("#idgroup_penjualan_implan").val();
			var idgroup_diskon_implan=$("#idgroup_diskon_implan").val();
			var idgroup_tuslah_implan=$("#idgroup_tuslah_implan").val();
		
					
			if (idgroup_penjualan_implan=='#'){
				sweetAlert("Maaf...", "Group Penjualan Harus diisi", "error");
				return false;
			}
			if (idgroup_diskon_implan=='#'){
				sweetAlert("Maaf...", "Group Diskon Harus diisi", "error");
				return false;
			}
			
			
		return true;
	}
	function clear_input_implan(){
		$("#idtipe_implan").val('#').trigger('change');	
		$("#idgroup_penjualan_implan").val('#').trigger('change');
		$("#idgroup_diskon_implan").val('#').trigger('change');
		
		$("#idkategori_implan").val('0').trigger('change');
		$("#idkelas_implan").val('0').trigger('change');
		$("#idbarang_implan").val('0').trigger('change');
	}
	$(document).on("click","#simpan_implan",function(){
		if (validate_add_implan()==false)return false;
		var tipe_barang=2;
		var idkelas=$("#idkelas_implan").val();
		var idtipe=$("#idtipe_implan").val();
		var idkategori=$("#idkategori_implan").val();
		var idbarang=$("#idbarang_implan").val();
		var idgroup_penjualan=$("#idgroup_penjualan_implan").val();
		var idgroup_diskon=$("#idgroup_diskon_implan").val();
	
		
		// alert(idkelas);
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_jual_ko/simpan_implan',
			type: 'POST',
			data: {
				idkelas:idkelas
				,idtipe:idtipe
				,tipe_barang:tipe_barang
				,idkategori:idkategori
				,idbarang:idbarang
				,idgroup_penjualan:idgroup_penjualan
				,idgroup_diskon:idgroup_diskon
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_implan').DataTable().ajax.reload( null, false );
					clear_input_implan();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	
	$(document).on("click","#clear_implan",function(){		
		clear_input_implan();
	});
	function load_implan(){
		var jenis=4;
		var idlogic=$("#id").val();
		
		$('#tabel_implan').DataTable().destroy();
		var table = $('#tabel_implan').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pendapatan_jual_ko/load_setting/',
			type: "POST",
			dataType: 'json',
			data: {
				jenis: jenis
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
					]
		});
	}
</script>
