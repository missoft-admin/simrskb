<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('129'))){ ?>
<div class="block">
	<div class="block-header">
	<?php if (UserAccesForm($user_acces_form,array('130'))){ ?>
		<ul class="block-options">
       
            <a href="{base_url}mdata_pengelolaan/create" type="button" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
       
			</ul>
	<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<div class="row">
			 <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
			<div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien">Nama Pengelolaan</label>
					<div class="col-md-8">
						<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="asalpasien">Status </label>
					<div class="col-md-8">
						<select name="status" id="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="1" <?=($status == 1 ? 'selected="selected"':'')?>>Aktif</option>
							<option value="0" <?=($status == 0 ? 'selected="selected"':'')?>>Tidak Aktif</option>
							<option value="#" <?=($status =='#' ? 'selected="selected"':'')?>>- All -</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien"></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="button" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				
			</div>
			<?php echo form_close() ?>
		</div>
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="index_list">
			<thead>
				<tr>
					<th>Urutan</th>
					<th>Nama</th>
					<th>Harga Jual</th>
					<th>Tanggal</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		</div>
	</div>
</div>
<?}?>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		// 	
		load_index();
	  
	});
	$("#btn_filter").click(function() {
		// alert('ID');
		table.destroy();
		load_index();
	});
	function load_index(){
		var status=$("#status").val();
		var nama=$("#nama").val();
		
		
		table = $('#index_list').DataTable({
				autoWidth: false,
				searching: true,
				pageLength: 50,
				serverSide: true,
				"processing": true,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				"columnDefs": [
						{ "width": "5%", "targets": 0, "orderable": true },
						{ "width": "25%", "targets": 1, "orderable": true },
						{ "width": "15%%", "targets": 2, "orderable": true },
						{ "width": "15%", "targets": 3, "orderable": true },
						{ "width": "10%", "targets": 4, "orderable": true },
						{ "width": "15%", "targets": 5, "orderable": true },
					],
				ajax: { 
					url: '{site_url}mdata_pengelolaan/getIndex',
					type: "POST" ,
					dataType: 'json',
					data : {
							status:status,
							nama:nama
						   }
				}
			});
	}

	function softDelete($id){
		var id=$id;		
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			table = $('#index_list').DataTable()	
			$.ajax({
				url: '{site_url}mdata_pengelolaan/softDelete/'+id,
				complete: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Dihapus'});
					table.ajax.reload( null, false ); 
				}
			});
		});
		

		
	}
	function aktifkan($id){
		var id=$id;		
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Mengaktifkan Kembali?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			table = $('#index_list').DataTable()	
			$.ajax({
				url: '{site_url}mdata_pengelolaan/aktifkan/'+id,
				complete: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Aktif Kembali'});
					table.ajax.reload( null, false ); 
				}
			});
		});
		

		
	}
</script>
