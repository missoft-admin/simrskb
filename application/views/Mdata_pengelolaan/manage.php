<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mdata_pengelolaan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mdata_pengelolaan/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama Rekapan</label>
				<div class="col-md-7">
					<input type="hidden" class="form-control" id="idpengelolaan" placeholder="ID" name="idpengelolaan" value="{id}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="id" placeholder="ID" name="id" value="{id}" required="" aria-required="true">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="deskripsi">Harga Jual Barang</label>
				<div class="col-md-7">
					<select tabindex="1" id="st_harga_jual" name="st_harga_jual" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
						<option value="#" selected>-Pilih-</option>
							<option value="1" <?=($st_harga_jual == 1 ? 'selected' : '')?>>Sebelum Margin</option>
							<option value="2" <?=($st_harga_jual == 2 ? 'selected' : '')?>>Setelah Margin</option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mdata_pengelolaan" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			
			<?php echo form_close() ?>
	</div>
</div>
<?if ($id){?>

	<div class="block">
		<div class="block-header">		
			<h4 ><?=text_success('Tanggal')?>  <?=text_default('Perhitungan')?></h4>
		</div>
		<div class="block-content">
			<input type="hidden" class="form-control" id="tedit_tanggal" placeholder="No. Rek" value="">
			<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
			<table id="tabel_tanggal" class="table table-striped table-bordered" style="margin-bottom: 0;">
				<thead>
					<tr>
						<th></th>
						<th>X</th>
						<th>Tanggal</th>
						<th>Deskrpisi</th>
						<th>User</th>
						<th>Aksi</th>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							<select tabindex="1" id="tanggal" name="tanggal" class="js-select2 form-control" style="width: 100%;" >
								<option value="#" selected>-Pilih-</option>
								<? for ($x = 1; $x <= 31; $x+=1) { ?>	
								<option value="<?=$x?>" ><?=$x?></option>
								<?}?>
							</select>
						</td>
						<td><input type="text" class="form-control" id="deskripsi" placeholder="Deskrpisi" value="" style="width:100%"></td>
						<td>
							<input type="text" class="form-control" readonly value="<?=$this->session->userdata('user_name')?>" style="width:100%">
						</td>
						<td>
							<button title="Simpan" id="btn_simpan_tanggal" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i></button>
							<button title="Simpan" id="btn_clear" type="button" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i></button>
						</td>
						
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
	<div class="block">
		<div class="block-header">		
			<h4 ><?=text_success('Info')?>  <?=text_default('Rekening')?></h4>
		</div>
		<div class="block-content">
			<input type="hidden" class="form-control" id="tedit_rekening" placeholder="No. Rek" value="">
			<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
			<table id="tabel_rekening" class="table table-striped table-bordered" style="margin-bottom: 0;">
				<thead>
					<tr>
						<th></th>
						<th>X</th>
						<th>Bank</th>
						<th>No Rekening</th>
						<th>Atas Nama</th>
						<th>Status</th>
						<th>Keterangan</th>
						<th>Aksi</th>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							<select tabindex="1" id="idbank" name="idbank" class="js-select2 form-control" style="width: 100%;" >
								<option value="#" selected>-Pilih-</option>
								<?foreach($list_bank as $row){?>
									<option value="<?=$row->id?>" ><?=$row->bank?></option>
								<?}?>
							</select>
						</td>
						<td><input type="text" class="form-control" id="norekening" placeholder="No Rekening" value="" style="width:100%"></td>
						<td><input type="text" class="form-control" id="atasnama" placeholder="Atas Nama" value="" style="width:100%"></td>
						<td>
							<select tabindex="1" id="st_primary" name="st_primary" class="js-select2 form-control" style="width: 100%;" >
								<option value="#" selected>-Pilih-</option>
								<option value="0">Secondary</option>
								<option value="1">Primary</option>
								
							</select>
						</td>
						<td>
							<input type="text" class="form-control" id="keterangan"  value="" style="width:100%">
						</td>
						<td>
							<button title="Simpan" id="btn_simpan_rekening" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i></button>
							<button title="Simpan" id="btn_clear" type="button" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i></button>
						</td>
						
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
	<div class="block">
		<div class="block-header">		
			<h4 ><?=text_success('Pemilihan')?>  <?=text_default('Transaksi Hutang')?></h4>
		</div>
		<div class="block-content">
			<input type="hidden" class="form-control" id="tedit_hutang" placeholder="No. Rek" value="">
			<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
			<table id="tabel_hutang" class="table table-striped table-bordered" style="margin-bottom: 0;">
				<thead>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th>Nama Transaksi</th>
						<th>Status</th>
						<th>Nominal Default</th>
						<th>No Akun</th>
						<th>Aksi</th>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>
							<select tabindex="1" id="mdata_hutang_id" name="mdata_hutang_id" class="js-select2 form-control" style="width: 100%;" >
								<option value="#" selected>-Pilih-</option>
								<?foreach($list_hutang as $row){?>
									<option value="<?=$row->id?>" ><?=$row->nama?></option>
								<?}?>
							</select>
						</td>
						<td>
							<select tabindex="1" id="st_rutin_hutang" name="st_rutin_hutang" class="js-select2 form-control" style="width: 100%;" >
								<option value="#" selected>-Pilih-</option>
								<option value="1">Rutin</option>
								<option value="0">Tidak Rutin</option>
								
							</select>
						</td>
						<td><input type="text" class="form-control number" id="nominal_default_hutang" placeholder="Nominal Default" value="" style="width:100%"></td>
						<td>
							<select tabindex="1" id="idakun_hutang" name="idakun_hutang" class="js-select2 form-control" style="width: 100%;" >
								<option value="#" selected>-Pilih-</option>
								<?foreach($list_akun as $row){?>
									<option value="<?=$row->id?>" ><?=$row->noakun.' - '.$row->namaakun?></option>
								<?}?>
							</select>
						</td>
						<td>
							<button title="Simpan" id="btn_simpan_hutang" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i></button>
							<button title="Simpan" id="btn_clear" type="button" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i></button>
						</td>
						
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
	<div class="block">
		<div class="block-header">		
			<h4 ><?=text_success('Pemilihan')?>  <?=text_default('Transaksi Piutang')?></h4>
		</div>
		<div class="block-content">
			<input type="hidden" class="form-control" id="tedit_piutang" placeholder="No. Rek" value="">
			<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
			<table id="tabel_piutang" class="table table-striped table-bordered" style="margin-bottom: 0;">
				<thead>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th>Nama Transaksi</th>
						<th>Status</th>
						<th>Nominal Default</th>
						<th>No Akun</th>
						<th>Aksi</th>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>
							<select tabindex="1" id="mdata_piutang_id" name="mdata_piutang_id" class="js-select2 form-control" style="width: 100%;" >
								<option value="#" selected>-Pilih-</option>
								<?foreach($list_piutang as $row){?>
									<option value="<?=$row->id?>" ><?=$row->nama?></option>
								<?}?>
							</select>
						</td>
						<td>
							<select tabindex="1" id="st_rutin_piutang" name="st_rutin_piutang" class="js-select2 form-control" style="width: 100%;" >
								<option value="#" selected>-Pilih-</option>
								<option value="1">Rutin</option>
								<option value="0">Tidak Rutin</option>
								
							</select>
						</td>
						<td><input type="text" class="form-control number" id="nominal_default_piutang" placeholder="Nominal Default" value="" style="width:100%"></td>
						<td>
							<select tabindex="1" id="idakun_piutang" name="idakun_piutang" class="js-select2 form-control" style="width: 100%;" >
								<option value="#" selected>-Pilih-</option>
								<?foreach($list_akun as $row){?>
									<option value="<?=$row->id?>" ><?=$row->noakun.' - '.$row->namaakun?></option>
								<?}?>
							</select>
						</td>
						<td>
							<button title="Simpan" id="btn_simpan_piutang" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i></button>
							<button title="Simpan" id="btn_clear" type="button" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i></button>
						</td>
						
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
<?}?>

<div id="cover-spin"></div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".number").number(true,0,'.',',');
		load_tanggal();	
		load_rekening();	
		load_hutang();	
		load_piutang();	
	})	
	
	function validate_final()
	{
		if ($("#nama").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Rekapan", "error");
			return false;
		}
		if ($("#st_harga_jual").val()=='#'){
			sweetAlert("Maaf...", "Tentukan Jenis Pendapatan", "error");
			return false;
		}
		
		
		$("*[disabled]").not(true).removeAttr("disabled");
		$("#simpan_tko").hide();
		$("#cover-spin").show();
		return true;
	}
	
	function load_tanggal() {
		var idpengelolaan=$("#idpengelolaan").val();
		
		$('#tabel_tanggal').DataTable().destroy();
		var table = $('#tabel_tanggal').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}mdata_pengelolaan/load_tanggal/',
			type: "POST",
			dataType: 'json',
			data: {
				idpengelolaan: idpengelolaan,				
			}
		},
		columnDefs: [{"targets": [0], "visible": false },
					 { "width": "3%", "targets": [1],className: "text-left" },
					 { "width": "20%", "targets": [2],className: "text-left" },
					 { "width": "50%", "targets": [3],className: "text-left" },
					 { "width": "15%", "targets": [4],className: "text-left" },
					 { "width": "13%", "targets": [5],className: "text-left" },
					]
		});
	}
	
	$(document).on("click",".edit_tanggal",function(){	
		var table = $('#tabel_tanggal').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		// var id=table.cell(tr,0).data();
		var tedit_tanggal=table.cell(tr,0).data();
		var tanggal=table.cell(tr,2).data();
		var deskripsi=table.cell(tr,3).data();
		var user=table.cell(tr,4).data();
		$("#tedit_tanggal").val(tedit_tanggal);
		$("#tanggal").val(tanggal).trigger('change');
		$("#deskripsi").val(deskripsi);
		$("#user").val(user);	
	});
	$(document).on("click","#btn_clear",function(){	
		clear_tanggal();
	});
	
	$(document).on("click",".hapus_tanggal",function(){	
		var table = $('#tabel_tanggal').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var id=table.cell(tr,0).data();
		$.ajax({
			url: '{site_url}mdata_pengelolaan/hapus_tanggal',
			type: 'POST',
			data: {id: id},
			complete: function() {					
				swal({
					title: "Berhasil!",
					text: "Rekening Berhasil Dihapus.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});
				
				$('#tabel_tanggal').DataTable().ajax.reload(null,false)
			}
		});
	
	});
	$(document).on("click","#btn_simpan_tanggal",function(){	
		simpan_tanggal();	
	});
	function simpan_tanggal(){
		if (validate_tanggal()==false){return false}
			$("#cover-spin").show();
			var idpengelolaan=$("#idpengelolaan").val();
			var tedit_tanggal=$("#tedit_tanggal").val();
			var tanggal=$("#tanggal").val();
			var deskripsi=$("#deskripsi").val();
			// alert(tedit_tanggal);
			$.ajax({
				url: '{site_url}mdata_pengelolaan/simpan_tanggal',
				type: 'POST',
				data: {
					idpengelolaan: idpengelolaan,
					tedit_tanggal: tedit_tanggal,
					tanggal: tanggal,
					deskripsi: deskripsi,				
				},
				complete: function() {					
					swal({
						title: "Berhasil!",
						text: "Variable Berhasil ditambah.",
						type: "success",
						timer: 500,
						showConfirmButton: false
					});
					$("#cover-spin").hide();
					clear_tanggal();
					$('#tabel_tanggal').DataTable().ajax.reload(null,false)
				}
			});
		
	}
	function clear_tanggal()
	{
		$("#tedit_tanggal").val('');
		$("#deskripsi").val('');
		$("#tanggal").val('#').trigger('change');
	}
	function validate_tanggal()
	{
		if ($("#tanggal").val()=='#'){
			sweetAlert("Maaf...", "Tanggal Harus diisi", "error");
			return false;
		}
		if ($("#deskripsi").val()==''){
			sweetAlert("Maaf...", "Deskrpisi Harus diisi", "error");
			return false;
		}
		
		// $("*[disabled]").not(true).removeAttr("disabled");
		// $("#simpan_tko").hide();
		// $("#cover-spin").show();
		return true;
	}
	//Rekening
	function load_rekening() {
		var idpengelolaan=$("#idpengelolaan").val();
		
		$('#tabel_rekening').DataTable().destroy();
		var table = $('#tabel_rekening').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}mdata_pengelolaan/load_rekening/',
			type: "POST",
			dataType: 'json',
			data: {
				idpengelolaan: idpengelolaan,				
			}
		},
		columnDefs: [{"targets": [0,1,8], "visible": false },
					 { "width": "3%", "targets": [1],className: "text-left" },
					 { "width": "15%", "targets": [2],className: "text-left" },
					 { "width": "20%", "targets": [3],className: "text-left" },
					 { "width": "20%", "targets": [4],className: "text-left" },
					 { "width": "20%", "targets": [5],className: "text-left" },
					 { "width": "13%", "targets": [6],className: "text-left" },
					 // { "width": "13%", "targets": [7],className: "text-left" },
					]
		});
	}
	
	$(document).on("click",".edit_rekening",function(){	
		var table = $('#tabel_rekening').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		// var id=table.cell(tr,0).data();
		var tedit_rekening=table.cell(tr,0).data();
		var idbank=table.cell(tr,1).data();
		var norekening=table.cell(tr,3).data();
		var atasnama=table.cell(tr,4).data();
		var keterangan=table.cell(tr,6).data();
		var st_primary=table.cell(tr,8).data();
		$("#tedit_rekening").val(tedit_rekening);
		$("#st_primary").val(st_primary).trigger('change');
		$("#idbank").val(idbank).trigger('change');
		$("#norekening").val(norekening);
		$("#atasnama").val(atasnama);
		$("#keterangan").val(keterangan);
		$("#user").val(user);	
	});
	$(document).on("click","#btn_clear",function(){	
		clear_rekening();
	});
	
	$(document).on("click",".hapus_rekening",function(){	
		var table = $('#tabel_rekening').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var id=table.cell(tr,0).data();
		$.ajax({
			url: '{site_url}mdata_pengelolaan/hapus_rekening',
			type: 'POST',
			data: {id: id},
			complete: function() {					
				swal({
					title: "Berhasil!",
					text: "Rekening Berhasil Dihapus.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});
				
				$('#tabel_rekening').DataTable().ajax.reload(null,false)
			}
		});
	
	});
	$(document).on("click","#btn_simpan_rekening",function(){	
		simpan_rekening();	
	});
	function simpan_rekening(){
		if (validate_rekening()==false){return false}
			$("#cover-spin").show();
			var idpengelolaan=$("#idpengelolaan").val();
			var tedit_rekening=$("#tedit_rekening").val();
			var idbank=$("#idbank").val();
			var norekening=$("#norekening").val();
			var atasnama=$("#atasnama").val();
			var st_primary=$("#st_primary").val();
			var keterangan=$("#keterangan").val();
			// alert(tedit_rekening);
			$.ajax({
				url: '{site_url}mdata_pengelolaan/simpan_rekening',
				type: 'POST',
				data: {
					idpengelolaan: idpengelolaan,
					tedit_rekening: tedit_rekening,
					idbank: idbank,
					norekening: norekening,				
					atasnama: atasnama,				
					st_primary: st_primary,				
					keterangan: keterangan,				
				},
				complete: function() {					
					swal({
						title: "Berhasil!",
						text: "Variable Berhasil ditambah.",
						type: "success",
						timer: 500,
						showConfirmButton: false
					});
					$("#cover-spin").hide();
					clear_rekening();
					$('#tabel_rekening').DataTable().ajax.reload(null,false)
				}
			});
		
	}
	function clear_rekening()
	{
		$("#tedit_rekening").val('');
		$("#idbank").val('#').trigger('change');
		$("#st_primary").val('#').trigger('change');
		$("#norekening").val('');
		$("#atasnama").val('');
		$("#keterangan").val('');
	}
	function validate_rekening()
	{
		if ($("#idbank").val()=='#'){
			sweetAlert("Maaf...", "Bank Harus diisi", "error");
			return false;
		}
		if ($("#norekening").val()==''){
			sweetAlert("Maaf...", "No rekening Harus diisi", "error");
			return false;
		}
		if ($("#atasnama").val()==''){
			sweetAlert("Maaf...", "Atas Nama Harus diisi", "error");
			return false;
		}
		if ($("#st_primary").val()=='#'){
			sweetAlert("Maaf...", "Status Primary Harus diisi", "error");
			return false;
		}
		
		// $("*[disabled]").not(true).removeAttr("disabled");
		// $("#simpan_tko").hide();
		// $("#cover-spin").show();
		return true;
	}
	
	//Hutang
	function load_hutang() {
		// alert('sini');
		var idpengelolaan=$("#idpengelolaan").val();		
		$('#tabel_hutang').DataTable().destroy();
		var table = $('#tabel_hutang').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}mdata_pengelolaan/load_hutang/',
			type: "POST",
			dataType: 'json',
			data: {
				idpengelolaan: idpengelolaan,				
			}
		},
		columnDefs: [{"targets": [0,1,2,3], "visible": false },
					 { "width": "20%", "targets": [4],className: "text-left" },
					 { "width": "15%", "targets": [5],className: "text-left" },
					 { "width": "10%", "targets": [6],className: "text-left" },
					 { "width": "45%", "targets": [7],className: "text-left" },
					 { "width": "15%", "targets": [8],className: "text-left" },
					]
		});
	}
	
	$(document).on("click",".edit_hutang",function(){	
		var table = $('#tabel_hutang').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		// var id=table.cell(tr,0).data();
		var tedit_hutang=table.cell(tr,0).data();
		var mdata_hutang_id=table.cell(tr,1).data();
		var idakun=table.cell(tr,2).data();
		var st_rutin=table.cell(tr,3).data();
		var nominal_default=table.cell(tr,6).data();
		$("#tedit_hutang").val(tedit_hutang);
		$("#st_rutin_hutang").val(st_rutin).trigger('change');
		$("#mdata_hutang_id").val(mdata_hutang_id).trigger('change');
		$("#idakun_hutang").val(idakun).trigger('change');
		$("#nominal_default_hutang").val(nominal_default);
		$("#user").val(user);	
	});
	$(document).on("click","#btn_clear",function(){	
		clear_hutang();
	});
	
	$(document).on("click",".hapus_hutang",function(){	
		var table = $('#tabel_hutang').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var id=table.cell(tr,0).data();
		$.ajax({
			url: '{site_url}mdata_pengelolaan/hapus_hutang',
			type: 'POST',
			data: {id: id},
			complete: function() {					
				swal({
					title: "Berhasil!",
					text: "Rekening Berhasil Dihapus.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});
				
				$('#tabel_hutang').DataTable().ajax.reload(null,false)
			}
		});
	
	});
	$(document).on("click","#btn_simpan_hutang",function(){	
		simpan_hutang();	
	});
	function simpan_hutang(){
		if (validate_hutang()==false){return false}
			$("#cover-spin").show();
			var idpengelolaan=$("#idpengelolaan").val();
			var tedit_hutang=$("#tedit_hutang").val();
			var mdata_hutang_id=$("#mdata_hutang_id").val();
			var idakun=$("#idakun_hutang").val();
			var nominal_default=$("#nominal_default_hutang").val();
			var st_rutin=$("#st_rutin_hutang").val();
			// alert(tedit_hutang);
			$.ajax({
				url: '{site_url}mdata_pengelolaan/simpan_hutang',
				type: 'POST',
				data: {
					idpengelolaan: idpengelolaan,
					tedit_hutang: tedit_hutang,
					mdata_hutang_id: mdata_hutang_id,
					idakun: idakun,				
					nominal_default: nominal_default,				
					st_rutin: st_rutin,				
				},
				complete: function() {					
					swal({
						title: "Berhasil!",
						text: "Variable Berhasil ditambah.",
						type: "success",
						timer: 500,
						showConfirmButton: false
					});
					$("#cover-spin").hide();
					clear_hutang();
					$('#tabel_hutang').DataTable().ajax.reload(null,false)
				}
			});
		
	}
	function clear_hutang()
	{
		$("#tedit_hutang").val('');
		$("#mdata_hutang_id").val('#').trigger('change');
		$("#st_rutin_hutang").val('#').trigger('change');
		$("#idakun_hutang").val('#').trigger('change');
		$("#nominal_default_hutang").val('0	');
	}
	function validate_hutang()
	{
		if ($("#mdata_hutang_id").val()=='#'){
			sweetAlert("Maaf...", "Nama Transaksi Harus diisi", "error");
			return false;
		}
		if ($("#idakun_hutang").val()=='#'){
			sweetAlert("Maaf...", "No Akun Harus diisi", "error");
			return false;
		}
		if ($("#nominal_default_hutang").val()=='0' || $("#nominal_default_hutang").val()==''){
			sweetAlert("Maaf...", "Nominal Default Harus diisi", "error");
			return false;
		}
		if ($("#st_rutin_hutang").val()=='#'){
			sweetAlert("Maaf...", "Status Harus diisi", "error");
			return false;
		}
		
		// $("*[disabled]").not(true).removeAttr("disabled");
		// $("#simpan_tko").hide();
		// $("#cover-spin").show();
		return true;
	}
	$("#mdata_hutang_id").change(function(){	
		if ($("#tedit_hutang").val()==''){
			get_data_hutang($(this).val());					
		}
	});
	function get_data_hutang($id){
		$.ajax({
			url: '{site_url}mdata_pengelolaan/get_data_hutang/'+$id,
			dataType: "json",
			success: function(data) {
				$("#nominal_default_hutang").val(data.detail.nominal_default);
				$("#idakun_hutang").val(data.detail.idakun).trigger('change');
			}
		});

	}
	
	//Hutang
	function load_piutang() {
		// alert('sini');
		var idpengelolaan=$("#idpengelolaan").val();		
		$('#tabel_piutang').DataTable().destroy();
		var table = $('#tabel_piutang').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}mdata_pengelolaan/load_piutang/',
			type: "POST",
			dataType: 'json',
			data: {
				idpengelolaan: idpengelolaan,				
			}
		},
		columnDefs: [{"targets": [0,1,2,3], "visible": false },
					 { "width": "20%", "targets": [4],className: "text-left" },
					 { "width": "15%", "targets": [5],className: "text-left" },
					 { "width": "10%", "targets": [6],className: "text-left" },
					 { "width": "45%", "targets": [7],className: "text-left" },
					 { "width": "15%", "targets": [8],className: "text-left" },
					]
		});
	}
	
	$(document).on("click",".edit_piutang",function(){	
		var table = $('#tabel_piutang').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		// var id=table.cell(tr,0).data();
		var tedit_piutang=table.cell(tr,0).data();
		var mdata_piutang_id=table.cell(tr,1).data();
		var idakun=table.cell(tr,2).data();
		var st_rutin=table.cell(tr,3).data();
		var nominal_default=table.cell(tr,6).data();
		$("#tedit_piutang").val(tedit_piutang);
		$("#st_rutin_piutang").val(st_rutin).trigger('change');
		$("#mdata_piutang_id").val(mdata_piutang_id).trigger('change');
		$("#idakun_piutang").val(idakun).trigger('change');
		$("#nominal_default_piutang").val(nominal_default);
		$("#user").val(user);	
	});
	$(document).on("click","#btn_clear",function(){	
		clear_piutang();
	});
	
	$(document).on("click",".hapus_piutang",function(){	
		var table = $('#tabel_piutang').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var id=table.cell(tr,0).data();
		$.ajax({
			url: '{site_url}mdata_pengelolaan/hapus_piutang',
			type: 'POST',
			data: {id: id},
			complete: function() {					
				swal({
					title: "Berhasil!",
					text: "Rekening Berhasil Dihapus.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});
				
				$('#tabel_piutang').DataTable().ajax.reload(null,false)
			}
		});
	
	});
	$(document).on("click","#btn_simpan_piutang",function(){	
		simpan_piutang();	
	});
	function simpan_piutang(){
		if (validate_piutang()==false){return false}
			$("#cover-spin").show();
			var idpengelolaan=$("#idpengelolaan").val();
			var tedit_piutang=$("#tedit_piutang").val();
			var mdata_piutang_id=$("#mdata_piutang_id").val();
			var idakun=$("#idakun_piutang").val();
			var nominal_default=$("#nominal_default_piutang").val();
			var st_rutin=$("#st_rutin_piutang").val();
			// alert(tedit_piutang);
			$.ajax({
				url: '{site_url}mdata_pengelolaan/simpan_piutang',
				type: 'POST',
				data: {
					idpengelolaan: idpengelolaan,
					tedit_piutang: tedit_piutang,
					mdata_piutang_id: mdata_piutang_id,
					idakun: idakun,				
					nominal_default: nominal_default,				
					st_rutin: st_rutin,				
				},
				complete: function() {					
					swal({
						title: "Berhasil!",
						text: "Variable Berhasil ditambah.",
						type: "success",
						timer: 500,
						showConfirmButton: false
					});
					$("#cover-spin").hide();
					clear_piutang();
					$('#tabel_piutang').DataTable().ajax.reload(null,false)
				}
			});
		
	}
	function clear_piutang()
	{
		$("#tedit_piutang").val('');
		$("#mdata_piutang_id").val('#').trigger('change');
		$("#st_rutin_piutang").val('#').trigger('change');
		$("#idakun_piutang").val('#').trigger('change');
		$("#nominal_default_piutang").val('0	');
	}
	function validate_piutang()
	{
		if ($("#mdata_piutang_id").val()=='#'){
			sweetAlert("Maaf...", "Nama Transaksi Harus diisi", "error");
			return false;
		}
		if ($("#idakun_piutang").val()=='#'){
			sweetAlert("Maaf...", "No Akun Harus diisi", "error");
			return false;
		}
		if ($("#nominal_default_piutang").val()=='0' || $("#nominal_default_piutang").val()==''){
			sweetAlert("Maaf...", "Nominal Default Harus diisi", "error");
			return false;
		}
		if ($("#st_rutin_piutang").val()=='#'){
			sweetAlert("Maaf...", "Status Harus diisi", "error");
			return false;
		}
		
		// $("*[disabled]").not(true).removeAttr("disabled");
		// $("#simpan_tko").hide();
		// $("#cover-spin").show();
		return true;
	}
	$("#mdata_piutang_id").change(function(){	
		if ($("#tedit_piutang").val()==''){
			get_data_piutang($(this).val());					
		}
	});
	function get_data_piutang($id){
		$.ajax({
			url: '{site_url}mdata_pengelolaan/get_data_piutang/'+$id,
			dataType: "json",
			success: function(data) {
				$("#nominal_default_piutang").val(data.detail.nominal_default);
				$("#idakun_piutang").val(data.detail.idakun).trigger('change');
			}
		});

	}
</script>