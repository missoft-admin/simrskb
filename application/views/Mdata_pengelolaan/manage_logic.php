<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mdata_pengelolaan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mdata_pengelolaan/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Nama Rekapan</label>
				<div class="col-md-10">
					<input type="hidden" class="form-control" id="idpengelolaan" placeholder="ID" name="idpengelolaan" value="{id}" required="" aria-required="true">
					<input type="text" class="form-control" disabled id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="deskripsi">Harga Jual Barang</label>
				<div class="col-md-10">
					<select tabindex="1" id="st_harga_jual" disabled name="st_harga_jual" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
						<option value="#" selected>-Pilih-</option>
							<option value="1" <?=($st_harga_jual == 1 ? 'selected' : '')?>>Sebelum Margin</option>
							<option value="2" <?=($st_harga_jual == 2 ? 'selected' : '')?>>Setelah Margin</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="Lokasi Tubuh"><?=text_primary('Proses Approval')?></label>
				
				<div class="col-md-10">
					<table id="tabel_detail" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 5%;">id</th>															
								<th style="width: 5%;">Step</th>															
								<th style="width: 10%;">Nominal Transaksi</th>								
								<th style="width: 15%;">User </th>
								<th style="width: 10%;">Jika Setuju</th>								
								<th style="width: 10%;">Jika Tolak</th>	
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td></td>
								<td>
									<select name="step" style="width: 100%" id="step" class="js-select2 form-control input-sm">
										<option value="#" selected>- Step -</option>
										<?for($i=1;$i<10;$i++){?>
											<option value="<?=$i?>"><?=$i?></option>
										<?}?>
									</select>										
								</td>
								
								
								
								<td>
									<select name="operand" style="width: 100%" id="operand" class="js-select2 form-control input-sm">					
										
										<option value=">">></option>										
										<option value=">=">>=</option>	
										<option value="<"><</option>
										<option value="<="><=</option>
										<option value="=">=</option>										
									</select>
									<input type="text" style="width: 100%"  class="form-control number" id="nominal" placeholder="0" name="nominal" value="0">
									<input type="hidden" class="form-control" id="id_edit" placeholder="0" name="id_edit" value="">
								</td>	
								<td>
									<select name="iduser" tabindex="16"  style="width: 100%" id="iduser" data-placeholder="User" class="js-select2 form-control input-sm"></select>										
								</td>
								<td>
									<select name="proses_setuju" style="width: 100%" id="proses_setuju" class="js-select2 form-control input-sm">										
										<option value="1">Langsung</option>
										<option value="2">Menunggu User</option>										
									</select>										
								</td>
								<td>
									<select name="proses_tolak" style="width: 100%" id="proses_tolak" class="js-select2 form-control input-sm">										
										<option value="1">Langsung</option>
										<option value="2">Menunggu User</option>										
									</select>										
								</td>
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_detail" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_edit" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						
				</table>
				</div>
			</div>
			
			
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		// load_unit();
		clear_all();
		// list_user();
		load_detail();
		// load_user();
	});
	
	$(document).on("change","#step",function(){
		if ($("#id_edit").val()==''){
		// alert($("#id_edit").val());
			list_user();
			
		}
	});
	function list_user(){
		// alert($("#idpengelolaan").val());
		$('#iduser')
			.find('option')
			.remove()
			.end()
			.append('<option value="">- Pilih User -</option>')
			.val('').trigger("liszt:updated");
		if ($("#step").val()!='#'){
			// console.log($("#id").val()+'/'+$("#step").val()+'/'+$("#idtipe").val()+'/'+$("#idjenis").val());
			$.ajax({
				url: '{site_url}mdata_pengelolaan/list_user_approval/'+$("#step").val()+'/'+$("#idpengelolaan").val(),
				dataType: "json",
				success: function(data) {

				$.each(data.detail, function (i,unit) {
				$('#iduser')
				.append('<option value="' + unit.id + '">' + unit.name + '</option>');
				});
				}
			});
		}else{
			$('#iduser')
			.find('option')
			.remove()
			.end()
			.append('<option value="">- Pilih User -</option>')
		}

	}
	
	function load_detail(){
		var idpengelolaan=$("#idpengelolaan").val();
		
		$('#tabel_detail').DataTable().destroy();
		var table = $('#tabel_detail').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}mdata_pengelolaan/load_detail/',
			type: "POST",
			dataType: 'json',
			data: {
				idpengelolaan: idpengelolaan
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	$(document).on("click","#clear_edit",function(){
		clear_all();
		$("#id_edit").val('');
		clear_input_detail();
		$("#iduser").val(null).trigger('change');
	});
	function clear_all(){
		// list_unit();
		// list_user_bendahara();
		$("#id_edit").val('');
		$("#simpan_unit").attr('disabled', false);
	}
	function clear_input_detail(){
		$("#step").val('1').trigger('change');
		$("#idtipe").val('#').trigger('change');
		$("#tipepemesanan").val('#').trigger('change');
		// $("#idjenis").val('#').trigger('change');
		$("#operand").val('>=').trigger('change');
		list_user();
		$("#nominal").val(0);
	}
	$(document).on("click","#simpan_detail",function(){
		if (validate_add_detail()==false)return false;
		var id_edit=$("#id_edit").val();
		var iduser=$("#iduser").val();
		var step=$("#step").val();
		var proses_setuju=$("#proses_setuju").val();
		var proses_tolak=$("#proses_tolak").val();
		var operand=$("#operand").val();
		var nominal=$("#nominal").val();
		var idpengelolaan=$("#idpengelolaan").val();
		$.ajax({
			url: '{site_url}mdata_pengelolaan/simpan_detail',
			type: 'POST',
			data: {
				iduser:iduser,step:step,
				proses_setuju: proses_setuju,proses_tolak:proses_tolak,
				operand:operand,nominal:nominal,id_edit:id_edit,idpengelolaan:idpengelolaan
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Logic Berhasil disimpan'});
					$('#tabel_detail').DataTable().ajax.reload( null, false );
					clear_input_detail();
					$("#id_edit").val('');
				}else{
					sweetAlert("Gagal...", "Penyimpanan!", "error");
					
				}
			}
		});
		
		
	});
	
	$(document).on("click",".hapus_det",function(){
		var table = $('#tabel_detail').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menghapus Approval ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mdata_pengelolaan/hapus_det',
				type: 'POST',
				data: {
					id: id	
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Detail Berhasil dihapus'});
					$('#tabel_detail').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	
	$(document).on("click",".edit",function(){
		// alert('SINI');
		table = $('#tabel_detail').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		$.ajax({
			url: '{site_url}mdata_pengelolaan/get_edit',
			type: 'POST',
			dataType: "json",
			data: {
				id: id
			},
			success: function(data) {
				// var item=data.detail;
				$("#id_edit").val(data.id);
				$("#step").val(data.step).trigger('change');
				$("#proses_setuju").val(data.proses_setuju).trigger('change');
				$("#proses_tolak").val(data.proses_tolak).trigger('change');
				$("#idtipe").val(data.idtipe).trigger('change');
				$("#operand").val(data.operand).trigger('change');
				$("#nominal").val(data.nominal).trigger('change');
				$("#tipepemesanan").val(data.tipepemesanan).trigger('change');
				var newOption = new Option(data.user_nama, data.iduser, true, true);
					
				$("#iduser").append(newOption);
				$("#iduser").val(data.iduser).trigger('change');

				console.log(data.id);
			}
		});
	});
	
	
	
	function validate_add_detail()
	{
		if ($("#iduser").val()=='' || $("#iduser").val()==null || $("#iduser").val()=="#"){
			sweetAlert("Maaf...", "User Harus diisi", "error");
			return false;
		}
		
		if ($("#nominal").val()=='0'){
			if (($("#operand").val()!='>=' && $("#operand").val()!='>')){
			sweetAlert("Maaf...", "Nominal Harus diisi", "error");
			return false;
			}
		}
		
		return true;
	}
</script>
