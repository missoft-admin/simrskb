<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('2039'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_unit()"><i class="fa fa-bar-chart-o"></i> {title}</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('2039'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?>" id="tab_info">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
					
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_unit">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="50%">Unit Pelayanan</th>
											<th width="15%">Action</th>										   
										</tr>
										<tr>
											<th>#</th>
											
											<th>
												<select id="idunit" name="idunit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													
												</select>
											</th>
											
											<th>
												<div class="btn-group">
												<?php if (UserAccesForm($user_acces_form,array('2040'))){ ?>
												<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_unit" name="btn_tambah_unit"><i class="fa fa-save"></i> Simpan</button>
												<?}?>
												<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_unit()"><i class="fa fa-refresh"></i> Clear</button>
												</div>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			
			
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
	load_unit();		
	find_unit();
})	

function clear_unit(){
	$("#idunit").val('#').trigger('change');
	find_unit();
	$("#btn_tambah_unit").html('<i class="fa fa-save"></i> Save');
	
}
function load_unit(){
	$('#index_unit').DataTable().destroy();	
	table = $('#index_unit').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}munitpelayanan_tujuan/load_unit', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}

$("#btn_tambah_unit").click(function() {
	let idunit=$("#idunit").val();
	
	if (idunit=='#'){
		sweetAlert("Maaf...", "Tentukan Unit", "error");
		return false;
	}
	
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}munitpelayanan_tujuan/simpan_unit', 
		dataType: "JSON",
		method: "POST",
		data : {
				idunit:idunit,
				
			},
		complete: function(data) {
			clear_unit();
			$('#index_unit').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
function find_unit(){
	$.ajax({
		url: '{site_url}munitpelayanan_tujuan/find_unit/',
		dataType: "json",
		success: function(data) {
			// alert(data);
			$("#idunit").empty();
			$("#idunit").append(data);
		}
	});
}
function hapus_unit(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Unit?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}munitpelayanan_tujuan/hapus_unit',
				type: 'POST',
				data: {id: id},
				complete: function() {
					find_unit();
					$('#index_unit').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

</script>