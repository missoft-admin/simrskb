<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1617'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_td()"><i class="fa fa-bar-chart-o"></i> {title}</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('1617'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?>" id="tab_info">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-xs-12" for="satuan_td">Satuan Tekanan Darah</label>
							<div class="col-xs-4">
								<div class="input-group">
									<input class="form-control" type="text" id="satuan_td" name="satuan_td" value="{satuan_td}" placeholder="Satuan Tekanan Darah">
									<span class="input-group-btn">
										<button class="btn btn-success" onclick="update_satuan()" type="button"><i class="fa fa-save"></i>  Update</button>
									</span>
								</div>
								
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="col-xs-12 h4" for="satuan_td"><?=text_primary('SISTOLE')?></label>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="form-group" style="margin-bottom: 15px;">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_td">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="40%">Range Tekanan Darah</th>
												<th width="25%">Kategori</th>
												<th width="15%">Warna</th>
												<th width="15%">Action</th>										   
											</tr>
											<tr>
												<th>#<input class="form-control" type="hidden" id="td_id" name="td_id" value="" placeholder="id"></th>
												<th>
													<div class="input-group" style="width:100%">
														<input class="form-control decimal" type="text" id="td_1" name="td_1" placeholder="<?=$satuan_td?>" value="" >
														<span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
														
														<input class="form-control decimal" type="text" id="td_2" name="td_2" placeholder="<?=$satuan_td?>" value="" >
														
														
													</div>
												</th>
												<th>
													<input class="form-control" type="text" style="width:100%" id="kategori_td" name="kategori_td" placeholder="Kategori" value="" >
												</th>
												<th>
													
													<div class="js-colorpicker input-group colorpicker-element">
														<input class="form-control" type="text" id="warna" name="warna" value="#0000">
														<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
													</div>
												
												</th>
												<th>
													<div class="btn-group">
													<?php if (UserAccesForm($user_acces_form,array('1618'))){ ?>
													<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_td" name="btn_tambah_td"><i class="fa fa-save"></i> Simpan</button>
													<?}?>
													<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_td()"><i class="fa fa-refresh"></i> Clear</button>
													</div>
												</th>										   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>					
							</div>
						</div>
					</div>
					<div class="col-md-12">
					<div class="form-group">
						<label class="col-xs-12 h4"><?=text_success('DIASTOLE')?></label>
					</div>
					</div>

					<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_td_diastole">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="40%">Range Tekanan Darah</th>
											<th width="25%">Kategori</th>
											<th width="15%">Warna</th>
											<th width="15%">Action</th>										   
										</tr>
										<tr>
											<th>#<input class="form-control" type="hidden" id="td_diastole_id" name="td_diastole_id" value="" placeholder="id"></th>
											<th>
												<div class="input-group" style="width:100%">
													<input class="form-control decimal" type="text" id="td_diastole_1" name="td_diastole_1" placeholder="<?=$satuan_td?>" value="" >
													<span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
													
													<input class="form-control decimal" type="text" id="td_diastole_2" name="td_diastole_2" placeholder="<?=$satuan_td?>" value="" >
													
													
												</div>
											</th>
											<th>
												<input class="form-control" type="text" style="width:100%" id="kategori_td_diastole" name="kategori_td_diastole" placeholder="Kategori" value="" >
											</th>
											<th>
												
												<div class="js-colorpicker input-group colorpicker-element">
													<input class="form-control" type="text" id="warna_diastole" name="warna_diastole" value="#0000">
													<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
												</div>
											
											</th>
											<th>
												<div class="btn-group">
												<?php if (UserAccesForm($user_acces_form,array('1618'))){ ?>
												<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_td_diastole" name="btn_tambah_td_diastole"><i class="fa fa-save"></i> Simpan</button>
												<?}?>
												<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_td_diastole()"><i class="fa fa-refresh"></i> Clear</button>
												</div>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
					</div>
					
				</div>
			
			
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
	$(".decimal").number(true,1,'.',',');	
	load_td();		
	load_td_diastole();		
	
})	
function clear_td(){
	$("#td_id").val('');
	$("#td_1").val('');
	$("#td_2").val('');
	$("#kategori_td").val('');
	$("#warna").val('#000');
	$("#btn_tambah_td").html('<i class="fa fa-save"></i> Save');
	
}
function load_td(){
	$('#index_td').DataTable().destroy();	
	table = $('#index_td').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mtd/load_td', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function update_satuan(){
	let satuan_td=$("#satuan_td").val();
	
	if (satuan_td==''){
		sweetAlert("Maaf...", "Tentukan Ssatuan", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mtd/update_satuan', 
		dataType: "JSON",
		method: "POST",
		data : {
				satuan_td:satuan_td,
				
				
			},
		complete: function(data) {
			location.reload();
		}
	});
}
$("#btn_tambah_td").click(function() {
	let td_id=$("#td_id").val();
	let td_1=$("#td_1").val();
	let td_2=$("#td_2").val();
	let kategori_td=$("#kategori_td").val();
	let warna=$("#warna").val();
	
	if (td_1==''){
		sweetAlert("Maaf...", "Tentukan Range Tekanan Darah", "error");
		return false;
	}
	if (td_2==''){
		sweetAlert("Maaf...", "Tentukan Range Tekanan Darah", "error");
		return false;
	}
	if (kategori_td==''){
		sweetAlert("Maaf...", "Tentukan Kategori", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mtd/simpan_td', 
		dataType: "JSON",
		method: "POST",
		data : {
				td_id:td_id,
				td_1:td_1,
				td_2:td_2,
				kategori_td:kategori_td,
				warna:warna,
				
			},
		complete: function(data) {
			clear_td();
			$('#index_td').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
function edit_td(td_id){
	$("#td_id").val(td_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mtd/find_td',
		type: 'POST',
		dataType: "JSON",
		data: {id: td_id},
		success: function(data) {
			$("#btn_tambah_td").html('<i class="fa fa-save"></i> Edit');
			$("#td_id").val(data.id);
			$("#td_1").val(data.td_1);
			$("#td_2").val(data.td_2);
			$("#warna").val(data.warna).trigger('change');
			$("#kategori_td").val(data.kategori_td);
			
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_td(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Tekanan Darah?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mtd/hapus_td',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_td').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

//DIASTOLE
function clear_td_diastole(){
	$("#td_diastole_id").val('');
	$("#td_diastole_1").val('');
	$("#td_diastole_2").val('');
	$("#kategori_td_diastole").val('');
	$("#warna").val('#000');
	$("#btn_tambah_td_diastole").html('<i class="fa fa-save"></i> Save');
	
}
function load_td_diastole(){
	$('#index_td_diastole').DataTable().destroy();	
	table = $('#index_td_diastole').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mtd/load_td_diastole', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}

$("#btn_tambah_td_diastole").click(function() {
	let td_diastole_id=$("#td_diastole_id").val();
	let td_diastole_1=$("#td_diastole_1").val();
	let td_diastole_2=$("#td_diastole_2").val();
	let kategori_td_diastole=$("#kategori_td_diastole").val();
	let warna=$("#warna_diastole").val();
	
	if (td_diastole_1==''){
		sweetAlert("Maaf...", "Tentukan Range Tekanan Darah", "error");
		return false;
	}
	if (td_diastole_2==''){
		sweetAlert("Maaf...", "Tentukan Range Tekanan Darah", "error");
		return false;
	}
	if (kategori_td_diastole==''){
		sweetAlert("Maaf...", "Tentukan Kategori", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mtd/simpan_td_diastole', 
		dataType: "JSON",
		method: "POST",
		data : {
				td_diastole_id:td_diastole_id,
				td_diastole_1:td_diastole_1,
				td_diastole_2:td_diastole_2,
				kategori_td_diastole:kategori_td_diastole,
				warna:warna,
				
			},
		complete: function(data) {
			clear_td_diastole();
			$('#index_td_diastole').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
function edit_td_diastole(td_diastole_id){
	$("#td_diastole_id").val(td_diastole_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mtd/find_td_diastole',
		type: 'POST',
		dataType: "JSON",
		data: {id: td_diastole_id},
		success: function(data) {
			$("#btn_tambah_td_diastole").html('<i class="fa fa-save"></i> Edit');
			$("#td_diastole_id").val(data.id);
			$("#td_diastole_1").val(data.td_1);
			$("#td_diastole_2").val(data.td_2);
			$("#warna_diastole").val(data.warna).trigger('change');
			$("#kategori_td_diastole").val(data.kategori_td);
			
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_td_diastole(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Tekanan Darah?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mtd/hapus_td_diastole',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_td_diastole').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

</script>