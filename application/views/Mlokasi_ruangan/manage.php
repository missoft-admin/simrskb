<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mlokasi_ruangan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mlokasi_ruangan/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="kode">Kode</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="kode" placeholder="Kode" name="kode" value="{kode}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="deskripsi">Deskripsi</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="deskripsi" placeholder="Deskripsi" name="deskripsi" value="{deskripsi}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="ruangan_id">Ruangan</label>
				<div class="col-md-7">
					<select name="ruangan_id" class="select2 form-control" style="width: 100%;" data-placeholder="Select Option">
							<option value="">Select Option</option>
							<?php foreach(get_all('mruangan', array('status' => '1')) as $row) {?>
									<option value="<?= $row->id; ?>" <?= $ruangan_id === $row->id ? 'selected' : ''; ?>><?= $row->nama; ?></option>
							<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="poliklinik_id">Poliklinik</label>
				<div class="col-md-7">
					<select name="poliklinik_id" class="select2 form-control" style="width: 100%;" data-placeholder="Select Option">
							<option value="">Select Option</option>
							<?php foreach(get_all('mpoliklinik', array('status' => '1')) as $row) {?>
									<option value="<?= $row->id; ?>" <?= $poliklinik_id === $row->id ? 'selected' : ''; ?>><?= $row->nama; ?></option>
							<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="gedung_id">Gedung</label>
				<div class="col-md-7">
					<select name="gedung_id" class="select2 form-control" style="width: 100%;" data-placeholder="Select Option">
							<option value="">Select Option</option>
							<?php foreach (list_variable_ref(296) as $row) { ?>
									<option value="<?php echo $row->id; ?>" <?php echo $row->id == $gedung_id ? 'selected' : ''; ?> <?php echo $row->id == $gedung_id ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
							<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="lantai_id">Lantai</label>
				<div class="col-md-7">
					<select name="lantai_id" class="select2 form-control" style="width: 100%;" data-placeholder="Select Option">
							<option value="">Select Option</option>
							<?php foreach (list_variable_ref(297) as $row) { ?>
									<option value="<?php echo $row->id; ?>" <?php echo $row->id == $lantai_id ? 'selected' : ''; ?> <?php echo $row->id == $lantai_id ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
							<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mlokasi_ruangan" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>


<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
			$('.select2').select2();
		});
</script>