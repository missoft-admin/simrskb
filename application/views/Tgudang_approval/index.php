<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">
		
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:25px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Distributor</label>
                    <div class="col-md-8">
                        <select id="filter_distributor" name="filter_distributor" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#">- Semua Distributor -</option>
                            <?php foreach ($list_distributor as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?=($filter_distributor == $row->id ? 'selected' : ''); ?>><?php echo $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Status</label>
                    <div class="col-md-8">
                        <select id="status" name="statuspesan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#">- Semua Status -</option>                            
                            <option value="2" <?=("2" == $statuspesan ? 'selected' : ''); ?>>PERSETUJUAN KEUANGAN</option>                           
                        </select>
                    </div>
                </div>
                
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" name="tanggaldari" id="tanggaldari" placeholder="From" value="{tanggaldari}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" name="tanggalsampai" id="tanggalsampai" placeholder="To" value="{tanggalsampai}"/>
                        </div>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">No. PO</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_trx" placeholder="No. PO" name="no_trx" value="{no_trx}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
					<?php if (UserAccesForm($user_acces_form,array('1118'))){ ?>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button"  id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
					<?}?>
                </div>
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th width="5%">ID</th>
                    <th width="2%">ID</th>
                    <th width="10%">NO. PO</th>
                    <th width="10%">TANGGAL</th>
                    <th width="10%">TIPE</th>
                    <th width="15%">DISTRIBUTOR</th>
                    <th width="5%">ITEM</th>
                    <th width="15%" class="text-center">Status</th>
                    <th width="15%" class="text-center">Respon</th>
                    <th width="15%" class="text-center">AKSI</th>
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_user" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<input type="hidden" class="form-control" id="idrka" placeholder="" name="idrka" value="">
						<div class="col-md-12">
							<table width="100%" id="tabel_user_proses" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Step</th>
										<th>User</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						
					</div>
					<?php echo form_close() ?>
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
			
		</div>
	</div>
</div>
<div class="modal fade" id="modal_tolak" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Alasan Tolak</h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Alasan</label>
								<div class="col-md-8">
									<textarea class="form-control" rows="2" name="alasan_tolak" id="alasan_tolak"></textarea>
									<input type="hidden" readonly class="form-control" id="id_approval"  placeholder="id_approval" name="id_approval" value="">
								</div>
							</div>
						</div>							
					</div>
					
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-danger text-uppercase" type="button" name="btn_tolak" id="btn_tolak"><i class="fa fa-save"></i> SIMPAN TOLAK</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	load_index();
   
});
function load_index(){
	var notransaksi=$("#no_trx").val();
	var status=$("#status").val();
	var filter_distributor=$("#filter_distributor").val();
	var tanggal_tagihan1=$("#tanggaldari").val();
	var tanggal_tagihan2=$("#tanggalsampai").val();
	$('#index_list').DataTable().destroy();
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0], "visible": false },
							{ "width": "3%", "targets": [1,6] },
							{ "width": "8%", "targets": [2,3,4] },
							{ "width": "10%", "targets": [6] },
							{ "width": "12%", "targets": [7,8] },
							{ "width": "20%", "targets": [9] },
						 // {"targets": [8,3,4], className: "text-left" },
						 {"targets": [1,6], className: "text-right" },
						 {"targets": [2,3,4,7,8], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tgudang_approval/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						notransaksi:notransaksi,status:status,
						filter_distributor:filter_distributor,tanggal_tagihan1:tanggal_tagihan1,
						tanggal_tagihan2:tanggal_tagihan2
					   }
            }
        });
}
$("#btn_filter").click(function() {
	// alert('ID');
	// table.destroy();
	load_index();
});
function load_user($id){
	var id=$id;
	$("#modal_user").modal('show');
	
	$.ajax({
		url: '{site_url}tgudang_approval/list_user/'+id,
		dataType: "json",
		success: function(data) {
			$("#tabel_user_proses tbody").empty();
			$("#tabel_user_proses tbody").append(data.detail);
		}
	});
}
function setuju($id_approval,$id){
	var table = $('#index_list').DataTable()
		// var tr = $(this).parents('tr')
		var id = $id;
		var id_approval=$id_approval;
		// alert(id_approval);return false;
		swal({
			title: "Apakah Anda Yakin  ?",
			text : "Menyetujui Pengajuan ini?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}tgudang_approval/setuju_batal/'+id_approval+'/1'+'/'+id,
				type: 'POST',
				success: function(data) {
					// console.log(data);
					// alert(data);
					$.toaster({priority : 'success', title : 'Succes!', message : ' Bagi Hasil berhasil disetujui'});
					$('#index_list').DataTable().ajax.reload( null, false );
					// load_index();
					// if (data=='"1"'){
					// window.location.href = "{site_url}mrka_approval/next_bendahara/"+id;
					// }
				}
			});
		});
}
function batal($id_approval,$id){
	var table = $('#index_list').DataTable()
		// var tr = $(this).parents('tr')
		var id = $id;
		var id_approval=$id_approval;
		// alert(id_approval);return false;
		swal({
			title: "Apakah Anda Yakin  ?",
			text : "Mebatalkan Psersetujuan?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}tgudang_approval/setuju_batal/'+id_approval+'/0'+'/'+id,
				type: 'POST',
				success: function(data) {
					// console.log(data);
					// alert(data);
					$.toaster({priority : 'success', title : 'Succes!', message : ' Mebatalkan Pesetujuan'});
					$('#index_list').DataTable().ajax.reload( null, false );
					// load_index();
					// if (data=='"1"'){
					// window.location.href = "{site_url}mrka_approval/next_bendahara/"+id;
					// }
				}
			});
		});
}
function tolak($id_approval,$id){
	$("#id_approval").val($id_approval)
	$('#modal_tolak').modal('show');
}
$(document).on("click","#btn_tolak",function(){
		
		var id_approval=$("#id_approval").val();
		var alasan_tolak=$("#alasan_tolak").val();
		swal({
			title: "Apakah Anda Yakin  ?",
			text : "Menolak Pengajuan ini?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}tgudang_approval/tolak/',
				type: 'POST',
				data: {
					id_approval:id_approval,
					alasan_tolak:alasan_tolak,
				},
				complete: function() {
					$('#modal_tolak').modal('hide');
					$.toaster({priority : 'success', title : 'Succes!', message : ' Pengajuan berhasil ditolak'});
					$('#index_list').DataTable().ajax.reload( null, false );
					
				}
			});
		});
		
		return false;
		
	});
	function kirim($id){
		var id=$id;
		$("#modal_approval").modal('show');
		
		load_user_approval(id);
		
	}
	function load_user_approval($id){
		var id=$id;
		// alert(id)
		$("#id_setuju").val(id);
		$('#tabel_user').DataTable().destroy();
		table=$('#tabel_user').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"searching": false,
				"lengthChange": false,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tgudang_approval/load_user_approval',
					type: "POST",
					dataType: 'json',
					data : {
						id:id,
												
					   }
				},
				"columnDefs": [
					 {  className: "text-right", targets:[0] },
					 {  className: "text-center", targets:[1,2,3] },
					 { "width": "10%", "targets": [0] },
					 { "width": "40%", "targets": [1] },
					 { "width": "25%", "targets": [2,3] },

				]
			});
	}
</script>