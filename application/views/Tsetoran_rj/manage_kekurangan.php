<style>
	.form_sm {
		border-radius: 3px;
		background-color:#fdffe2;
		height:28px;
	}
</style>

<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tsetoran_rj/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tsetoran_rj" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">TRANSAKSI SETORAN KASIR</h3>
	</div>
	<?
		$disabel=($st_verifikasi=='1'?'disabled':'');
	?>
	<div class="block-content" style="margin-top: -20px;font-size:">
		<div class="row">
			<div class="col-md-2">
				<a class="block block-link-hover2 text-center" href="javascript:void(0)">
					<div class="block-content block-content-full bg-modern">
						<i class="fa fa-calendar fa-2x text-white"></i>
						<div class="font-w600 text-white-op push-15-t">TANGGAL TRANSAKSI</div>
					</div>
					<div class="block-content block-content-full bg-gray-light block-content-mini">
						<strong><?=tanggal_indo_DMY($tanggal_trx)?></strong>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a class="block block-link-hover2 text-center" href="javascript:void(0)">
					<div class="block-content block-content-full bg-success">
						<i class="fa fa-user-o fa-2x text-white"></i>
						<div class="font-w600 text-white-op push-15-t">DIBUAT OLEH</div>
					</div>
					<div class="block-content block-content-full bg-gray-light block-content-mini">
						<?=($user_buat?$user_buat:'AUTOMATIC SYSTEM')?> - <?=($user_buat?HumanDateLong($created_at):'')?>
					</div>
				</a>
			</div>
			<div class="col-md-3">
				<a class="block block-link-hover2 text-center" onclick="show_modal_last(<?=$id?>)" href="javascript:void(0)">
					<div class="block-content block-content-full bg-warning">
						<i class="fa fa-pencil fa-2x text-white"></i>
						<div class="font-w600 text-white-op push-15-t">LAST UPDATE</div>
						
					</div>
					<div class="block-content block-content-full bg-gray-light block-content-mini">
						<span id="user_update"><?=($user_update)?></span>
					</div>
				</a>
			</div>
			<div class="col-md-4">
				<a class="block block-link-hover2 text-center" href="javascript:void(0)">
					<div class="block-content block-content-full bg-default">
						<i class="fa fa-star-o fa-2x text-white"></i>
						<div class="font-w600 text-white-op push-15-t">HASIL</div>
					</div>
					<div class="block-content block-content-full bg-gray-light block-content-mini">
						<span id="hasil"><?=$hasil_pemeriksaan_label?></span>
					</div>
				</a>
			</div>
			
			<input type="hidden" class="form-control" id="idtransaksi" name="idtransaksi" placeholder="ID" value="{id}" required="" aria-required="true">
			<input type="hidden" readonly class="form-control" id="status_setoran" value="{status}" required="" aria-required="true">
			
			<div class="col-md-12">
				<!-- Static Labels block-opt-hidden -->
				<div class="block block-themed" >
					<div class="block-header bg-success">
						<ul class="block-options">
							<li>
								<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title"><i class="fa fa-money"></i>  PEMBAYARAN</h3>
					</div>
					<div class="block-content block-content">
							<div class="form-group" style="margin-bottom: 15px;">
								<label class="col-md-2 control-label" for="nama">Noninal</label>
								<div class="col-md-2">
									<input type="text" readonly class="form-control number" id="nominal" placeholder="Selisih" name="nominal" value="(<?=($selisih<0?$selisih*-1:$selisih)?>)">
								</div>
								<label class="col-md-2 control-label" for="nama">Tanggal Setoran</label>
								<div class="col-md-2">
									<div class="input-group date">
									<input class="js-datepicker form-control input" type="text" id="tanggal_setoran" name="tanggal_setoran" value="<?=($tanggal_setoran?HumanDateShort($tanggal_setoran) : HumanDateShort($tanggal_trx))?>" data-date-format="dd-mm-yyyy"/>
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 15px;">
								<label class="col-md-2 control-label" for="nama">Jenis</label>
								<div class="col-md-8">
									 <select id="jenis_id" name="jenis_id" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($jenis_id=='0'?'selected':'')?>>-Silahkan Pilih-</option>
										<?foreach($list_jenis as $r){?>
										<option value="<?=$r->id?>" <?=($jenis_id==$r->id?'selected':'')?>><?=$r->nama_jenis?></option>
										<?}?>
													
									</select>
								</div>
								
							</div>
							<div class="form-group" style="margin-bottom: 15px;">
								<label class="col-md-2 control-label" for="nama">Keterangan</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="keterangan" placeholder="Description" name="keterangan" value="{keterangan}">
								</div>
								<? if ($st_verifikasi=='0'){ ?>
								<div class="col-md-2">
									<button  type="button" class="btn btn-danger btn_hide"  <?=$disabel?>  type="button" id="btn_add_pembayaran" title="Add"><i class="fa fa-plus"></i> Pembayaran</button>
								</div>
								<?}?>
								
							</div>
							<input type="hidden" class="form-control" id="rowindex">
							<input type="hidden" class="form-control" id="iddet">
							<div class="form-group">
								 <table class="table table-bordered" id="list_pembayaran">
									<thead>
										<tr>
											<th width="5%" class="text-center" hidden>NO</th>
											<th width="10%" class="text-center">Jenis Kas</th>
											<th width="15%" class="text-center">Sumber Kas</th>
											<th width="10%" class="text-center">Metode</th>
											<th width="10%" class="text-center" hidden>Tgl Pencairan</th>
											<th width="15%" class="text-center">Keterangan</th>
											<th width="10%" class="text-center">Nominal</th>
											<th width="15%" class="text-center">Actions</th>
											
										</tr>
									</thead>
									</thead>
									<tbody>	
										<? 
											$disabel=($st_verifikasi=='1'?'disabled':'');
											$no=1;
											if ($list_pembayaran){
												foreach($list_pembayaran as $row){ ?>
													<tr>
													<td hidden><?=$no?></td>
													<td><?=$row->jenis_kas?></td>
													<td><?=$row->sumber_kas?></td>
													<td><?=metodePembayaran_bendahara($row->idmetode)?></td>
													<td hidden><?=$row->idmetode?></td>
													<td><?=$row->ket_pembayaran?></td>
													<td align="right"><?=number_format($row->nominal_bayar,0)?></td>
													<td><button type='button'  <?=$disabel?>  class='btn btn-info btn-xs pembayaran_edit' data-toggle='modal' data-target='#modal-pembayaran' title='Edit'><i class='fa fa-pencil'></i></button>&nbsp;&nbsp;<button type='button'  <?=$disabel?>  class='btn btn-danger btn-xs pembayaran_remove' title='Remove'><i class='fa fa-trash-o'></i></button></td>
													<td style='display:none'><input readonly type='text' class='form-control' name='xiddet[]' value='<?=$row->id?>'></td>
													<td style='display:none'><input readonly type='text' class='form-control' name='xjenis_kas_id[]' value='<?=$row->jenis_kas_id?>'></td>
													<td style='display:none'><input readonly type='text' class='form-control' name='xsumber_kas_id[]' value='<?=$row->sumber_kas_id?>'></td>
													<td style='display:none'><input readonly type='text' class='form-control' name='xidmetode[]' value='<?=$row->idmetode?>'></td>
													<td style='display:none'><input readonly type='text' class='form-control' name='ket[]' value='<?=$row->ket_pembayaran?>'></td>
													<td style='display:none'><input readonly type='text' class='form-control' name='xnominal_bayar[]' value='<?=$row->nominal_bayar?>'></td>
													<td style='display:none'><input readonly type='text' class='form-control' name='xstatus[]' value='1'></td>
													</tr>
												<?
												$no=$no+1;
												}
											}
										?>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="4" class="text-right"> <strong>Total Bayar </strong></td>
											<td  colspan="2"> <input class="form-control input number" readonly type="text" id="total_bayar" name="total_bayar" value="{nominal}"/></td>
										</tr>
										<tr>
											<td colspan="4" class="text-right"> <strong>Sisa</strong></td>
											<td  colspan="2"><input class="form-control input number" readonly type="text" id="total_sisa" name="total_sisa" value="0"/> </td>
										</tr>
									</tfoot>
								</table>
							</div>
							<? if ($st_verifikasi=='0'){ ?>
							<div class="form-group hiden btn_hide">
								<div class="text-right bg-light lter">
									<button class="btn btn-primary" type="submit" id="btn_simpan" name="btn_simpan">Simpan</button>
									<a href="{base_url}tsetoran_kas" class="btn btn-default" type="button">Batal</a>
								</div>
							</div>
							<?}?>
							
						</div> 
				</div>
				<!-- END Static Labels -->
			</div>
		
	</div>
	<?php echo form_close() ?>
</div>
<div class="modal fade in black-overlay" id="modal_pembayaran" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Pembayaran</h3>
				</div>
				<div class="block-content">
					
						<div class="form-group">
							<label class="col-md-2 control-label" for="nama">Sisa Rp. </label>
							<div class="col-md-9">
								<input readonly type="text" class="form-control number" id="sisa_modal" placeholder="Sisa" name="sisa_modal" required="" aria-required="true">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="jenis_kas_id">Jenis Kas</label>
							<div class="col-md-9">
								<select name="jenis_kas_id" id="jenis_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="#">- Pilih Opsi -</option>
									<? foreach($list_jenis_kas as $row){ ?>
										<option value="<?=$row->id?>" <?=$row->st_default=='1'?'selected':''?>><?=$row->nama?></option>
									<?}?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="sumber_kas_id">Sumber Kas</label>
							<div class="col-md-9">
								<select name="sumber_kas_id" id="sumber_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">							
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="idmetode">Metode</label>
							<div class="col-md-9">
								<select name="idmetode" id="idmetode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="#">Pilih Opsi</option>							
									<option value="1">Cheq</option>	
									<option value="2">Tunai</option>	
									<option value="4">Transfer Antar Bank</option>
									<option value="3">Transfer Beda Bank</option>							
								</select>
							</div>
						</div>
						
						
						<div class="form-group">
							<label class="col-md-2 control-label" for="nama">Nominal Rp. </label>
							<div class="col-md-9">
								<input  type="hidden" class="form-control" id="sumber_kas_id_tmp" placeholder="sumber_kas_id_tmp" name="sumber_kas_id_tmp" >
								<input  type="hidden" class="form-control" id="idpembayaran" placeholder="idpembayaran" name="idpembayaran" >
								<input  type="text" class="form-control number" id="nominal_bayar" placeholder="Nominal" name="nominal_bayar" >
							</div>
						</div>
						<div class="form-group" hidden>
							<label class="col-md-2 control-label" for="nama">Tanggal Pencairan</label>
							<div class="col-md-9">
								<div class="input-group date">
								<input class="js-datepicker form-control input"  type="text" id="tanggal_pencairan" name="tanggal_pencairan" value="" data-date-format="dd-mm-yyyy"/>
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-2 control-label" for="nama">Keterangan </label>
							<div class="col-md-9">
								<textarea class="form-control js-summernote" name="ket_pembayaran" id="ket_pembayaran"></textarea>
							</div>
						</div>

						<div class="modal-footer">
							<button class="btn btn-sm btn-primary" id="btn_add_bayar" type="button"><i class="fa fa-check"></i> Add</button>
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Exit</button>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
<? $this->load->view('Tsetoran_rj/modal_user')?>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		// st_show_rajal_tunai=0;
		$(".decimal").number(true,2,'.',',');
		$('.number').number(true, 0, '.', ',');
		$('#ket_pembayaran').summernote({
		  height: 100,   //set editable area's height
			dialogsInBody: true,
					  codemirror: { // codemirror options
						theme: 'monokai'
					  }	
		})	


	})
	$(document).on("click","#btn_add_bayar",function(){
		add_pembayaran();
	});
	function add_pembayaran(){
			
		if ($("#jenis_kas_id").val() == "#") {
			sweetAlert("Maaf...", "Jenis Kas Harus Diisi!", "error");
			$("#jenis_kas_id").focus();
			return false;
		}
		if ($("#sumber_kas_id").val() == "#") {
			sweetAlert("Maaf...", "Sumber Kas Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		if ($("#idmetode").val() == "#" || $("#idmetode").val() == "") {
			sweetAlert("Maaf...", "Metode  Harus Diisi!", "error");
			$("#idmetode").focus();
			return false;
		}
		if ($("#nominal_bayar").val() == "" || $("#nominal_bayar").val() == "0") {
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		var content = "";

		var row_index;
		var duplicate = false;

		if($("#rowindex").val() != ''){
			var number = $("#number").val();
			var content = "";
			row_index = $("#rowindex").val();
		}else{
			var number = $('#list_pembayaran tr').length;
			var content = "<tr>";
			// alert($("#idmetode option:selected").text());
			$('#list_pembayaran tbody tr').filter(function (){
				var $cells = $(this).children('td');
				// console.log($cells.eq(1).text());
				if($cells.eq(1).text() === $("#idmetode option:selected").text()){
					sweetAlert("Maaf...", $("#pembayaran_jenis option:selected").text() + " sudah ditambahkan.", "error");
					duplicate = true;

					pembayaran_clear();
					return false;
				}
			});
		}

		if(duplicate == false){
			var ket=$('#ket_pembayaran').summernote('code');
			content += "<td style='display:none'>" + number + "</td>";
			content += "<td>" + $("#jenis_kas_id option:selected").text() + "</td>";
			content += "<td>" + $("#sumber_kas_id option:selected").text() + "</td>";
			content += "<td>" + $("#idmetode option:selected").text() + "</td>";
			content += "<td style='display:none'>-</td>";
			content += "<td>" + ket + "</td>";
			content += "<td align='right'>" + formatNumber($("#nominal_bayar").val()) + "</td>";
			content += "<td><button type='button' class='btn btn-info btn-xs pembayaran_edit' data-toggle='modal' data-target='#modal-pembayaran' title='Edit'><i class='fa fa-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-danger btn-xs pembayaran_remove' title='Remove'><i class='fa fa-trash-o'></i></button></td>";
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xiddet[]' value='"+$("#iddet").val()+"'></td>";//8
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xjenis_kas_id[]' value='"+$("#jenis_kas_id").val()+"'></td>";//9
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xsumber_kas_id[]' value='"+$("#sumber_kas_id").val()+"'></td>";//10
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xidmetode[]' value='"+$("#idmetode").val()+"'></td>";//11
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='ket[]' value='"+ket+"'></td>";//12
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xnominal_bayar[]' value='"+$("#nominal_bayar").val()+"'></td>";//13
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xstatus[]' value='1'></td>";//14
			if($("#rowindex").val() != ''){
				$('#list_pembayaran tbody tr:eq(' + row_index + ')').html(content);
			}else{
				content += "</tr>";
				$('#list_pembayaran tbody').append(content);
			}

			pembayaran_clear();
			checkTotal();
		}
		
		
	}
	function formatNumber (num) {
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}
	function pembayaran_clear(){
		$("#sumber_kas_id_tmp").val('');
		$("#number").val('');
		$("#rowindex").val('');
		$("#modal_pembayaran").modal('hide');
		clear_input_pembayaran();
	}
	function checkTotal(){
		var total_pembayaran = 0;
		$('#list_pembayaran tbody tr').each(function() {
			if ($(this).closest('tr').find("td:eq(14) input").val()=='1'){
			total_pembayaran += parseFloat($(this).find('td:eq(6)').text().replace(/\,/g, ""));
			}
		});
		$("#total_bayar").val(total_pembayaran);
		$("#total_sisa").val(parseFloat($("#nominal").val()) - parseFloat(total_pembayaran));
	}
	$(document).on("click",".pembayaran_edit",function(){
		$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
		$("#modal_pembayaran").modal('show');
		$("#iddet").val($(this).closest('tr').find("td:eq(8) input").val());
		$("#sumber_kas_id_tmp").val($(this).closest('tr').find("td:eq(10) input").val());
		$("#jenis_kas_id").val($(this).closest('tr').find("td:eq(9) input").val()).trigger('change');
		$("#nominal_bayar").val($(this).closest('tr').find("td:eq(13) input").val());
		$("#ket_pembayaran").summernote('code',$(this).closest('tr').find("td:eq(12) input").val());
		$("#idmetode").val($(this).closest('tr').find("td:eq(11) input").val()).trigger('change');
		$("#sisa_modal").val(parseFloat($("#total_sisa").val()) + parseFloat($("#nominal_bayar").val()));
		// sisa_modal($();
		// $("#pembayaran_id").val($(this).closest('tr').find("td:eq(0)").val());

		
		return false;
	});
	$(document).on("click", ".pembayaran_remove", function() {
		var tr=$(this);
			swal({
				title: "Apakah Anda Yakin ?",
				text : "Akan Menghapus Pendapatan ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				if ($("#id").val()==''){
					tr.closest('td').parent().remove();
						
				}else{
					tr.closest('tr').find("td:eq(14) input").val(0)
					tr.closest('tr').find("td:eq(7)").html('<span class="label label-danger">DELETED</span>')
				}
				checkTotal();
			});

			// if (confirm("Hapus data ?") == true) {
				
			// }
		// return false;
	});
	function clear_input_pembayaran(){
		$("#jenis_kas_id").val("#").trigger('change');
		$("#sumber_kas_id").val("#").trigger('change');
		$("#nominal_bayar").val(0);
		$("#sisa_modal").val(0);
		$("#idpembayaran").val('');
		$("#idmetode").val("#").trigger('change');
		$("#ket_pembayaran").summernote('code','');
		$("#iddet").val('');
	}
	$(document).on("change","#jenis_kas_id",function(){
		// alert('soo');
		var jenis_kas_id=$("#jenis_kas_id").val();
		var sumber_kas_id=$("#sumber_kas_id_tmp").val();
		$.ajax({
			url: '{site_url}mrka_bendahara/list_sumber_kas',
			type: 'POST',
			dataType: "json",
			data: {jenis_kas_id: jenis_kas_id,sumber_kas_id: sumber_kas_id},
			success : function(data) {				
				$("#sumber_kas_id").empty();	
				// console.log(data.detail);
				// $("#sumber_kas_id").append(data.detail);	
				$('#sumber_kas_id').append(data.detail);
			}
		});
	});
	function validate_final(){
		
		if ($("#jenis_id").val() == "0") {
			sweetAlert("Maaf...", "Jenis Belum Dipilih!", "error");
			return false;
		}
		if ($("#keterangan").val() == "") {
			sweetAlert("Maaf...", "Keterangan Belum Dipilih!", "error");
			return false;
		}
		if ($("#total_bayar").val() == "0") {
			sweetAlert("Maaf...", "Pembayaran Belum Dipilih!", "error");
			return false;
		}
		if ($("#total_sisa").val() != "0") {
			sweetAlert("Maaf...", "Masih Ada Sisa!", "error");
			return false;
		}
	}
	
	$(document).on("click","#btn_add_pembayaran",function(){
		// if ($("#nominal").val()=='' || $("#nominal").val()=='0'){
			// return false;
		// }
		$("#modal_pembayaran").modal('show');
		$("#jenis_kas_id").val($("#jenis_kas_id").val()).trigger('change');
		$("#idmetode").val(4).trigger('change');
		$("#total_sisa").val(parseFloat($("#nominal").val()) - parseFloat($("#total_bayar").val()));
		$("#sisa_modal").val($("#total_sisa").val());
		$("#nominal_bayar").val($("#total_sisa").val());
		
	});
</script>