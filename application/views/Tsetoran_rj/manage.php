<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tsetoran_rj/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tsetoran_rj" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">TRANSAKSI SETORAN KASIR</h3>
	</div>
	<div class="block-content" style="margin-top: -20px;font-size:">
		<div class="row">
			<div class="col-md-12">
				
			<input type="hidden" class="form-control" id="id" placeholder="ID" name="id" value="{id}" required="" aria-required="true">
			
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Tanggal Transaksi</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input" <?=$disabel?> <?=($id?'disabled':'')?> type="text" id="tanggal_trx" name="tanggal_trx" value="<?=$tanggal_trx?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" type="button" id="btn_create" onclick="create_setoran()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-arrow-circle-right"></i> PROSES PENDAPATAN</button>
				</div>
				
			</div>
			<?if ($id==''){?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama"></label>
				<div class="col-md-2">
				
				</div>
			</div>
			<?}?>
			
			</div>
		</div>
		<div class="row hiden" hidden>
			<div class="col-md-12">
				<div class="block block-themed ">
					<div class="block-header bg-primary">
						<ul class="block-options">

							<li>
								<button type="button" id="btn_rajal" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">PENDAPATAN RAWAT JALAN</h3>
					</div>
					<div class="block-content">
						<h4><span class="label label-success">SETORAN RAWAT JALAN</span></h4><br>
						<table id="tabel_rajal" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 25%;">DESKRIPSI</th>
										<th style="width: 15%;">NOMINAL SISTEM</th>
										<th style="width: 15%;">NOMINAL VERIFIKASI</th>
										<th style="width: 20%;">REAL CASH</th>
										<th style="width: 25%;">ACTION</th>
									</tr>
									
								</thead>
								<tbody>
									<tr>
										<td>PENDAPATAN TUNAI RAJAL & FARMASI</td>
										<td><input type="text" readonly name="rj_tunai_sistem" id="rj_tunai_sistem" class="form-control number sm" value="0" ></td>
										<td><input type="text" readonly name="rj_tunai_verif" id="rj_tunai_verif" class="form-control sm number" value="0" ></td>
										<td><input type="text" name="rj_tunai_real" id="rj_tunai_real" class="form-control sm number" <?=$disabel?> value="{rj_tunai_real}" ></td>
										<td>
											<div class="btn-group">
											  <button class="btn btn-primary btn-sm detail" type="button"><i class="glyphicon glyphicon-menu-down"></i></button>
											  <button class="btn btn-info btn-sm detail_pasien_tunai_rj"  type="button"><i class="fa fa-list-ul"></i></button>
											  <button class="btn btn-success btn-sm "  type="button"><i class="fa fa-pencil"></i></button>
											  <button class="btn btn-success btn-sm "  type="button"><i class="fa fa-print"></i></button>
											</div>
										</td>
									</tr>
								</tbody>
								
								
						</table>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row notif" hidden>
			<div class="col-md-12">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h3 class="font-w300 push-15">Informasi</h3>
					<p>Opps, Tanggal Transaksi Sudah <a class="alert-link" href="{base_url}tsetoran_rj"> Pernah diinput</a>!</p>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close() ?>
</div>

<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	var st_show_rajal_tunai;
	var st_show_ranap_tunai;
	$(document).ready(function(){
		// st_show_rajal_tunai=0;
		$(".decimal").number(true,2,'.',',');
		$('.number').number(true, 0, '.', ',');
		cek_sudah_ada();
		
	})
	function create_setoran(){
		var tanggal_trx=$("#tanggal_trx").val();
		$.ajax({
			url: '{site_url}tsetoran_rj/create_setoran/',
			dataType: "json",
			type: "POST",
			data : {
					tanggal_trx:tanggal_trx,
				   },
			success: function(data) {
				$("#cover-spin").show();
				window.location.href = "<?php echo site_url('tsetoran_rj'); ?>";
			}
		});
	}
	$(document).on("change","#tanggal_trx",function(){
		
		if ($("#id").val()==''){
			cek_sudah_ada();
			
		}
	});
	function cek_sudah_ada(){
		var tanggal_trx=$("#tanggal_trx").val();
		$.ajax({
			url: '{site_url}tsetoran_rj/cek_sudah_ada/',
			dataType: "json",
			type: "POST",
			data : {
					tanggal_trx:tanggal_trx,
				   },
			success: function(data) {
				// console.log(data);
				if (data!='0'){					
					$(".hiden").hide();
					$(".notif").show();
					$("#btn_create").hide();
				}else{
					$("#btn_create").show();
					$(".hiden").hide();
					$(".notif").hide();
				}
				$(".btn_hide").hide();
			}
		});
	}
	
</script>