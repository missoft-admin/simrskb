<div class="modal in" id="Modal_Rincian_Last_Update" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title titleRincian">Last User Update</h3>
				</div>
				<div class="block-content" style="margin-bottom: 10px;padding-top: 10px;">
					<table id="index_user" class="table table-bordered table-striped" style="margin-top: 10px;">
						<thead>
							<tr>
								<th style="width:20%" class="text-center">Tanggal</th>
								<th style="width:30%" class="text-center">User</th>
								<th  style="width:45%" class="text-center">Keterangan</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						
					</table>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function show_modal_last(id){
		let table;
		$("#Modal_Rincian_Last_Update").modal('show');
		$('#index_user').DataTable().destroy();
		table=$('#index_user').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tsetoran_rj/getIndex_user',
					type: "POST",
					dataType: 'json',
					data : {
						id:id,
					   }
				},
				"columnDefs": [
					
					{  className: "text-center", targets:[0,1] },
				]
			});
		
	}
</script>