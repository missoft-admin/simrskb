<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title><?=$title?></title>
	<style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
	@page {
		margin-top: 1em;
		margin-left: 1.6em;
		margin-right: 1.5 em;
		margin-bottom: 1em;
	}
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }

      th {
        padding: 5px;
      }
	  td {
        padding: 0px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }

      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		
      }
      th {
        padding: 5px;

      }
	  td {
        padding: 0px;

      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
		vertical-align:middle;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .text-notes{
		font-size: 10px !important;
      }
	  .text-normal{
		font-size: 12px !important;
      }
	  .text-upnormal{
		font-size: 14px !important;
      }
	  .text-header{
		font-size: 16px !important;
		font-weight:bold;
      }
	  .text-judul{
        font-size: 18px  !important;
        
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 20px 0;
		}
		fieldset {
			border:1px solid #000;
			border-radius:4px;
			box-shadow:0 0 0px #000;
			margin-top: -4px !important;
		}
		legend {
			background:#fff;
		}
    }

	.text-muted {
	  color: #000;
	  font-weight: normal;
	  font-style: italic;
	  
	}
	.center {
	  display: block;
	  margin-left: auto;
	  margin-right: auto;
	  width: 50%;
	}
	ul {
	  margin:0;
	  padding-left: 12px;
	}
  </style>
</head>

<body>
	<header>
		
	</header>
	
	<main>
	<table class="content">
		<tr>
			<td width="100%" class="text-center"><img src="<?=base_url().'/assets/upload/app_setting/'.$logo?>" alt="" width="100" height="100"></td>
		</tr>
		<tr>
			<td width="100%" class="text-center text-normal"><?=$alamat_rs1?><br><?=$telepon_rs?> <?=$fax_rs?><br><?=$web_rs?> Email : <?=$email_rs?></td>
		</tr>
	</table>
	
	<table class="content">
		<tr>
			<td width="100%" class="text-center text-judul"><strong><?=$judul_header_ina?></strong><br><i><?=$judul_header_eng?></i></td>
		</tr>
		<tr>
			<td width="100%" class="text-left text-upnormal"><strong><?=$label_identitas_ina?></strong><br><i><?=$label_identitas_eng?></i></td>
		</tr>
	</table>
	<table class="content">
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$no_reg_ina?></strong><br><i><?=$no_reg_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?=$no_medrec?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$no_rm_ina?></strong><br><i><?=$no_rm_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=$no_medrec?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$nama_pasien_ina?></strong><br><i><?=$nama_pasien_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=$namapasien?></td>
		</tr>
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$ttl_ina?></strong><br><i><?=$ttl_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?= HumanDateShort($tanggal_lahir)?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$umur_ina?></strong><br><i><?=$umur_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=$umurtahun?> Tahun</td>
			<td width="12%" class="text-left text-normal"><strong><?=$jk_ina?></strong><br><i><?=$jk_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=$jk?></td>
		</tr>
		
		
	</table>
	<table class="content">
		<tr>
			<td width="2%" class="text-left text-normal">&nbsp;</td>
			<td width="98%" class="text-left text-normal"><strong><?= ($paragraf_1_ina)?></strong> <br><i><?=($paragraf_1_eng)?></i></td>
		</tr>
		
	</table>
	<table class="content">
		<tr>
			<td width="2%" class="text-left text-normal">&nbsp;</td>
			<td width="18%" class="text-left text-normal"><strong><?= ($dpjp_ina)?></strong> <br><i><?=($dpjp_eng)?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="79%" class="text-left text-normal"><strong><?= get_nama_ppa($dpjp_ppa)?></strong></td>
		</tr>
		
	</table>
	<table class="content">
		<tr>
			<td width="2%" class="text-left text-normal">&nbsp;</td>
			<td width="18%" class="text-left text-normal"><strong><?= ($tanggal_tindakan_ina)?></strong> <br><i><?=($tanggal_tindakan_ina)?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="19%" class="text-left text-normal"><strong><?= ($nama_hari).', '.($tanggal_opr)?></strong></td>
			<td width="15%" class="text-left text-normal"><strong><?= ($waktu_tindakan_ina)?></strong> <br><i><?=($waktu_tindakan_eng)?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="44%" class="text-left text-normal"><strong><?= ($mulai)?></strong></td>
		</tr>
		
	</table>
	<table class="content">
		<tr>
			<td width="2%" class="text-left text-normal">&nbsp;</td>
			<td width="18%" class="text-left text-normal"><strong><?= ($jenis_bius_ina)?></strong> <br><i><?=($jenis_bius_eng)?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="19%" class="text-left text-normal"><strong><?= get_nama_ref($jenis_bius,348)?></strong></td>
			<td width="15%" class="text-left text-normal"><strong><?= ($minum_ina)?></strong> <br><i><?=($minum_eng)?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="44%" class="text-left text-normal"><strong><?= get_nama_ref($pengencer_darah,431)?></strong></td>
			
		</tr>
		
	</table>
	<?if ($pengencer_darah){?>
	<table class="content">
		<tr>
			<td width="2%" class="text-left text-normal">&nbsp;</td>
			<td width="18%" class="text-left text-normal"><strong><?= ($jika_ya_ina)?></strong> <br><i><?=($jika_ya_eng)?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="79%" class="text-left text-normal"><strong><?= ($detail_pengencer)?></strong></td>
		</tr>
		
	</table>
	<?}?>
	<table class="content">
		<tr>
			<td width="2%" class="text-left text-normal">&nbsp;</td>
			<td width="98%" class="text-left text-normal"><strong><?= ($info_ina)?></strong> <br><i><?=($info_eng)?></i></td>
			
		</tr>
		<tr>
			<td width="2%" class="text-left text-normal">&nbsp;</td>
			<td width="98%" class="text-left text-normal"><strong><?= ($paragraf_2_ina)?></strong> <br><i><?=($paragraf_2_eng)?></i></td>
			
		</tr>
		<tr>
			<td width="2%" class="text-left text-normal">&nbsp;</td>
			<td width="98%" class="text-left text-normal"><strong><?= ($paragraf_3_ina)?></strong> <br><i><?=($paragraf_3_eng)?></i></td>
			
		</tr>
		<tr>
			<td width="2%" class="text-left text-normal">&nbsp;</td>
			<td width="98%" class="text-left text-normal"><strong><?= ($paragraf_4_ina)?></strong> <br><i><?=($paragraf_4_eng)?></i></td>
			
		</tr>
		
	</table>
	<table class="content">
		<tr>
			<td width="2%" class="text-left text-normal">&nbsp;</td>
			<td width="10%" class="text-left text-normal"><strong><?= ($label_pasien_ina)?></strong> <br><i><?=($label_pasien_eng)?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="10%" class="text-left text-normal"><strong><?= get_nama_ref($jenis_pasien,428)?></strong></td>
			<td width="10%" class="text-left text-normal"><strong><?= ($label_info_ina)?></strong> <br><i><?=($label_info_eng)?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="25%" class="text-left text-normal"><strong><?= ($informasi)?></strong></td>
			<td width="10%" class="text-left text-normal"><strong><?= ($label_puasa_ina)?></strong> <br><i><?=($label_puasa_eng)?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><strong><?= ($puasa_mulai)?></strong></td>
		</tr>
		
	</table>
	<table class="content">
		<tr>
			<td width="2%" class="text-left text-normal">&nbsp;</td>
			<td width="98%" class="text-left text-normal"><strong><?= ($paragraf_5_ina)?></strong> <br><i><?=($paragraf_5_eng)?></i></td>
			
		</tr>
		
	</table>
	<table class="content">
		<thead>
			<tr>
				<th width="2%" class="text-center ">&nbsp;</th>
				<th width="5%" class="text-center border-full">No</th>
				<th width="30%" class="text-center border-full"><strong><?=$paragraf_dok_ina?></strong><br><i class="text-muted"><?=$paragraf_dok_eng?></i></th>
				<th width="15%" class="text-center border-full"><strong><?=$label_status_ina?></strong><br><i class="text-muted"><?=$label_status_eng?></i></th>
				<th width="48%" class="text-center border-full"><strong><?=$label_ket_ina?></strong><br><i class="text-muted"><?=$label_ket_eng?></i></th>
				
			</tr>
			<?
				$q="SELECT H.* 
						,J.ref as nama_dok,S.ref as st_dok
						FROM tranap_info_ods_dok H
						LEFT JOIN merm_referensi J ON J.nilai=H.nama_dok_id AND J.ref_head_id='430'
						LEFT JOIN merm_referensi S ON S.nilai=H.status_dok AND S.ref_head_id='429'
						WHERE H.assesmen_id='$assesmen_id' AND H.st_aktif='1'";
				$list_dok=$this->db->query($q)->result();
				$no=1;
			?>
			
		</thead>
		<tbody>
			<?foreach($list_dok as $r){?>
			<tr>
				<th class="text-center ">&nbsp;</th>
				<td class="text-center border-full"><?=$no?></td>
				<td class="text-center border-full"><?=$r->nama_dok?></td>
				<td class="text-center border-full"><?=strtoupper($r->st_dok)?></td>
				<td class="text-center border-full"><?=$r->ket_dok?></td>
				
			</tr>
			<?
			$no++;
			}?>
		</tbody>
		
	</table>
	<table class="content">
		<tr>
			<td width="2%" class="text-left text-normal">&nbsp;</td>
			<td width="98%" class="text-left text-normal"><strong><?= ($paragraf_6_ina)?></strong> <br><i><?=($paragraf_6_eng)?></i></td>
		</tr>
		<tr>
			<td width="2%" class="text-left text-normal">&nbsp;</td>
			<td width="98%" class="text-left text-normal"><strong><?= ($paragraf_7_ina)?></strong> <br><i><?=($paragraf_7_eng)?></i></td>
		</tr>
		<tr>
			<td width="2%" class="text-left text-normal">&nbsp;</td>
			<td width="98%" class="text-left text-normal"><strong><?= ($label_catatan_ina)?></strong> <br><i><?=($label_catatan_eng)?></i></td>
		</tr>
		
	</table>
	<table class="content">
		<tr>
			<td width="3%" class="text-left text-normal">&nbsp;</td>
			<td width="97%" class="text-left text-normal"><?= ($content_catatan_ina)?></i></td>
		</tr>
		
		
	</table>
	<table class="content">
		<?
		$arr_mengetahui=get_nama_ppa_array($petugas_id);
		?>
		<tr>
			<td style="width:30%" class="text-center"><strong><?=strip_tags($label_keluarga_ina)?></strong><br><i><?=strip_tags($label_keluarga_eng)?></i></td>
			<td style="width:30%" class="text-center"></td>
			<td style="width:30%" class="text-center"><strong><?=strip_tags($label_petugas_ina)?></strong><br><i><?=strip_tags($label_petugas_eng)?></i></td>
		</tr>
		<tr>
			<td style="width:30%" class="text-center">
				<img class="" style="width:100px;height:90px; text-align: center;" src="<?=$ttd_pasien?>" alt="" title="">
				
			</td>
			
			<td style="width:30%" class="text-center"></td>
			<td style="width:30%" class="text-center">
				
				<img class="" style="width:100px;height:90px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($petugas_id?$petugas_id:$login_ppa_id)?>" alt="" title="">
			</td>
		</tr>
		<tr>
			<td style="width:30%" class="text-center"><strong><?=($nama_pasien)?></strong></td>
			<td style="width:30%" class="text-center"></td>
			<td style="width:30%" class="text-center">
			<strong><?=$arr_mengetahui['nama']?></strong><br><i>(<?=$arr_mengetahui['nik']?>)</i>
			</td>
		</tr>
		
	</table>
	<br>
	<br>
	<br>
	<br>
	<br>
	<table class="content">
		<tr>
			<td class="text-left text-normal"><strong><?=($judul_footer_ina)?></strong><br><i><?=($judul_footer_eng)?></i></td>
		</tr>
	</table>
	</main>
</body>

</html>
