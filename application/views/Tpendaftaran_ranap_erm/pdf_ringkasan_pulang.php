<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title><?=$title?></title>
	<style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
	@page {
		margin-top: 1em;
		margin-left: 1.6em;
		margin-right: 1.5 em;
		margin-bottom: 1em;
	}
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }

      th {
        padding: 5px;
      }
	  td {
        padding: 0px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }

      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		
      }
      th {
        padding: 5px;

      }
	  td {
        padding: 0px;

      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
		vertical-align:middle;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .text-notes{
		font-size: 10px !important;
      }
	  .text-normal{
		font-size: 12px !important;
      }
	  .text-upnormal{
		font-size: 14px !important;
      }
	  .text-header{
		font-size: 16px !important;
		font-weight:bold;
      }
	  .text-judul{
        font-size: 18px  !important;
        
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 20px 0;
		}
		fieldset {
			border:1px solid #000;
			border-radius:4px;
			box-shadow:0 0 0px #000;
			margin-top: -4px !important;
		}
		legend {
			background:#fff;
		}
    }

	.text-muted {
	  color: #000;
	  font-weight: normal;
	  font-style: italic;
	  
	}
	.center {
	  display: block;
	  margin-left: auto;
	  margin-right: auto;
	  width: 50%;
	}
	ul {
	  margin:0;
	  padding-left: 12px;
	}
  </style>
</head>

<body>
	<header>
		
	</header>
	
	<main>
	<table class="content">
		<tr>
			<td width="100%" class="text-center"><img src="<?=base_url().'/assets/upload/app_setting/'.$logo?>" alt="" width="100" height="100"></td>
		</tr>
		<tr>
			<td width="100%" class="text-center text-normal"><?=$alamat_rs1?><br><?=$telepon_rs?> <?=$fax_rs?><br><?=$web_rs?> Email : <?=$email_rs?></td>
		</tr>
	</table>
	
	<table class="content">
		<tr>
			<td width="100%" class="text-center text-judul"><strong><?=$judul_header_ina?></strong><br><i><?=$judul_header_eng?></i></td>
		</tr>
		<tr>
			<td width="100%" class="text-left text-upnormal"><strong><?=$label_identitas_ina?></strong><br><i><?=$label_identitas_eng?></i></td>
		</tr>
	</table>
	<table class="content">
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$no_reg_ina?></strong><br><i><?=$no_reg_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?=$no_medrec?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$no_rm_ina?></strong><br><i><?=$no_rm_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=$no_medrec?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$nama_pasien_ina?></strong><br><i><?=$nama_pasien_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=$namapasien?></td>
		</tr>
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$ttl_ina?></strong><br><i><?=$ttl_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?= HumanDateShort($tanggal_lahir)?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$umur_ina?></strong><br><i><?=$umur_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=$umur?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$jk_ina?></strong><br><i><?=$jk_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=$jenis_kelamin?></td>
		</tr>
		
		
	</table>
	<table class="content">
		<tr>
			<td width="100%" class="text-left text-normal"><strong><?= ($indikasi_rawat_ina)?></strong> / <i><?=($indikasi_rawat_eng)?></i></td>
		</tr>
		<tr>
			<td width="100%" class="text-left text-normal">
			<fieldset><legend style="font-weight:bold;"></legend><?=(trim($indikasi_rawat)?$indikasi_rawat:'&nbsp')?></fieldset>
		</tr>
		<tr>
			<td width="100%" class="text-left text-normal"><strong><?= ($ringkasan_ina)?></strong> / <i><?=($ringkasan_eng)?></i></td>
		</tr>
		<tr>
			<td width="100%" class="text-left text-normal">
			<fieldset><legend style="font-weight:bold;"></legend><?=(trim($ringkasan)?($ringkasan):'&nbsp;')?></fieldset>
		</tr>
		<tr>
			<td width="100%" class="text-left text-normal"><strong><?= ($pemeriksaan_ina)?></strong> / <i><?=($pemeriksaan_eng)?></i></td>
		</tr>
		<tr>
			<td width="100%" class="text-left text-normal">
			<fieldset><legend style="font-weight:bold;"></legend><?=(trim($pemeriksaan)?($pemeriksaan):'&nbsp;')?></fieldset>
		</tr>
		<tr>
			<td width="100%" class="text-left text-normal"><strong><?= ($penunjang_ina)?></strong> / <i><?=($penunjang_eng)?></i></td>
		</tr>
		<tr>
			<td width="100%" class="text-left text-normal">
			<fieldset><legend style="font-weight:bold;"></legend><?=(trim($penunjang)?($penunjang):'&nbsp;')?></fieldset>
		</tr>
		<tr>
			<td width="100%" class="text-left text-normal"><strong><?= ($konsultasi_ina)?></strong> / <i><?=($konsultasi_eng)?></i></td>
		</tr>
		<tr>
			<td width="100%" class="text-left text-normal">
			<fieldset><legend style="font-weight:bold;"></legend><?=(trim($konsultasi)?($konsultasi):'&nbsp;')?></fieldset>
		</tr>
		<tr>
			<td width="100%" class="text-left text-normal"><strong><?= ($therapi_ina)?></strong> / <i><?=($therapi_eng)?></i></td>
		</tr>
		<tr>
			<td width="100%" class="text-left text-normal">
			<fieldset><legend style="font-weight:bold;"></legend><?=(trim($therapi)?($therapi):'&nbsp;')?></fieldset>
		</tr>
		<tr>
			<td width="100%" class="text-left text-normal"><strong><?= ($therapi_pulang_ina)?></strong> / <i><?=($therapi_pulang_eng)?></i></td>
		</tr>
		<tr>
			<td width="100%" class="text-left text-normal">
			<fieldset><legend style="font-weight:bold;"></legend><?=(trim($therapi_pulang)?($therapi_pulang):'&nbsp;')?></fieldset>
		</tr>
		<tr>
			<td width="100%" class="text-left text-normal"><strong><?= ($komplikasi_ina)?></strong> / <i><?=($komplikasi_eng)?></i></td>
		</tr>
		<tr>
			<td width="100%" class="text-left text-normal">
			<fieldset><legend style="font-weight:bold;"></legend><?=(trim($komplikasi)?($komplikasi):'&nbsp;')?></fieldset>
		</tr>
		
	</table>
	
	<table class="content">
		<tr>
			<td style="width:20%;" class="text-normal"><strong><?=$prognis_ina?></strong> / <i><?=$prognis_eng?></i></td>
			<td style="width:25%;" class="text-normal"><strong><?=$cara_pulang_ina?></strong> / <i><?=$cara_pulang_eng?></i></td>
			<td style="width:30%;" class="text-normal" colspan="2"><strong><?=$intuksi_fu_ina?></strong> / <i><?=$intuksi_fu_eng?></i></td>
			<td style="width:25%;" class="text-normal"><strong><?=$keterangan_ina?></strong> / <i><?=$keterangan_eng?></i></td>
		</tr>
		<tr>
			<td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($prognis?get_nama_ref($prognis,415):'&nbsp;')?></fieldset>
			</td>
			<td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($cara_pulang?get_nama_ref($cara_pulang,416):'&nbsp;')?></fieldset>
			</td>
			<td class="text-normal" style="width:20%;">
				<fieldset><legend style="font-weight:bold;"></legend><?=(get_nama_ref($intuksi_fu,43))?></fieldset>
			</td>
			<td class="text-normal" style="width:10%;">
				<fieldset><legend style="font-weight:bold;"></legend><?=($tanggal_kontrol?(HumanDateShort($tanggal_kontrol)):'&nbsp;')?></fieldset>
			</td>
			<td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($keterangan?($keterangan):'&nbsp;')?></fieldset>
			</td>
		</tr>
	</table>
	<table class="content">
		<tr>
			<td style="width:25%;" class="text-normal"><strong><?=$diagnosa_utama_ina?></strong><br><i><?=$diagnosa_utama_eng?></i></td>
			<td style="width:2%;" class="text-normal">:</td>
			<td style="width:48%;" class="text-normal border-bottom"><?=$diagnosa_utama?></td>
			<td style="width:10%;" class="text-normal text-center"><strong><?=$icd_10_ina?></strong></td>
			<td style="width:15%;" class="text-normal border-bottom"></td>
		</tr>
		<?
			$q="SELECT M.nama, H.* FROM tranap_ringkasan_pulang_diagnosa H
				LEFT JOIN mdiagnosa_medis M ON M.id=H.mdiagnosa_id
				WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'";
			$list_data=$this->db->query($q)->result();
			$no=1;
		?>
		<?if ($list_data){?>
			<?foreach($list_data as $row){?>
			<tr>
				<td style="width:25%;" class="text-normal"><strong><?=($no<2?$diagnosa_tambahan_ina:'&nbsp;')?></strong><br><i><?=($no<2?$diagnosa_tambahan_eng:'&nbsp;')?></i></td>
				<td style="width:2%;" class="text-normal">:</td>
				<td style="width:48%;" class="text-normal border-bottom"><?=$no?>. <?=$row->nama?></td>
				<td style="width:10%;" class="text-normal text-center"><strong><?=($no<2?$icd_10_ina:'&nbsp;')?></strong></td>
				<td style="width:15%;" class="text-normal border-bottom"></td>
			</tr>
			<?
			$no++;
			}?>
		<?}else{?>
			<tr>
				<td style="width:25%;" class="text-normal"><strong><?=$diagnosa_tambahan_ina?></strong><br><i><?=$diagnosa_tambahan_eng?></i></td>
				<td style="width:2%;" class="text-normal">:</td>
				<td style="width:48%;" class="text-normal border-bottom"></td>
				<td style="width:10%;" class="text-normal text-center"><strong><?=$icd_10_ina?></strong></td>
				<td style="width:15%;" class="text-normal border-bottom"></td>
			</tr>
		<?}?>
	</table>
	<table class="content">
		
		<?
			$q="SELECT M.nama, H.* FROM tranap_ringkasan_pulang_tindakan H
				LEFT JOIN mtindakan_medis M ON M.id=H.mtindakan_id
				WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'";
			$list_data=$this->db->query($q)->result();
			$no=1;
		?>
		<?if ($list_data){?>
			<?foreach($list_data as $row){?>
			<tr>
				<td style="width:25%;" class="text-normal"><strong><?=($no<2?$prosedure_ina:'&nbsp;')?></strong><br><i><?=($no<2?$prosedure_eng:'&nbsp;')?></i></td>
				<td style="width:2%;" class="text-normal">:</td>
				<td style="width:48%;" class="text-normal border-bottom"><?=$no?>. <?=$row->nama?></td>
				<td style="width:10%;" class="text-normal text-center"><strong><?=($no<2?$icd_9_ina:'&nbsp;')?></strong></td>
				<td style="width:15%;" class="text-normal border-bottom"></td>
			</tr>
			<?
			$no++;
			}?>
		<?}else{?>
			<tr>
				<td style="width:25%;" class="text-normal"><strong><?=$prosedure_ina?></strong><br><i><?=$prosedure_eng?></i></td>
				<td style="width:2%;" class="text-normal">:</td>
				<td style="width:48%;" class="text-normal border-bottom"></td>
				<td style="width:10%;" class="text-normal text-center"><strong><?=$icd_9_ina?></strong></td>
				<td style="width:15%;" class="text-normal border-bottom"></td>
			</tr>
		<?}?>
	</table>
	
	<br>
	<br>
	<br>
	<br>
	<br>
	<table class="content">
	   <tr>
		<td width="100%" colspan="3">&nbsp;</td>
	  </tr> 
	   <tr>
		<td width="100%" colspan="3">&nbsp;</td>
	  </tr> 
	  <tr>
		<td width="40" class="text-center "><strong><?=$dpjp_ina?></strong><?=($dpjp_eng?'<br><i>'.$dpjp_eng.'</i>':'')?></td>
		<td width="20" class="text-center ">&nbsp;&nbsp;</td>
		<td width="40" class="text-center "><strong><?=$keluarga_ina?></strong><?=($keluarga_eng?'<br><i>'.$keluarga_eng.'</i>':'')?></td>
	  </tr>
	  <tr>
		<td class="text-center">
		  <img src="<?= base_url(); ?>qrcode/qr_code_ttd_dokter/<?= $dpjp; ?>" style="width:100px;height:100px">
		</td>
		<td class="text-center"></td>
		<td class="text-center">
			 <img src="<?= $pasien_ttd; ?>" />
		</td>
	  </tr>
	  <tr>
		<td class="text-center">
		<strong><?=get_nama_dokter_ttd($dpjp)?></strong> 
		
		</td>
		<td class="text-center"></td>
		<td class="text-center">
		  <strong>
			<?if ($pasien_nama){?>
			  (<?=($pasien_nama)?>)
			<?}?>
		  </strong>
		</td>
	  </tr>
	  <tr>
		<td width="100%" colspan="3">&nbsp;</td>
	  </tr> 
	  <tr>
		<td width="100%" colspan="3" class="text-notes"><br><br><i><?=$notes_ina?></i><?=($notes_eng?'<br><i>'.$notes_eng.'</i>':'')?></td>
	  </tr>
	</table>
	</td>
	</tr>
	</table>
	<br>
	</main>
</body>

</html>
