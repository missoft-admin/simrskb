<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title><?=$title?></title>
	<style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
	@page {
		margin-top: 1em;
		margin-left: 1.6em;
		margin-right: 1.5 em;
		margin-bottom: 1em;
	}
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }

      th {
        padding: 5px;
      }
	  td {
        padding: 0px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }

      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		
      }
      th {
        padding: 5px;

      }
	  td {
        padding: 0px;

      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
		vertical-align:middle;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .text-notes{
		font-size: 10px !important;
      }
	  .text-normal{
		font-size: 12px !important;
      }
	  .text-upnormal{
		font-size: 14px !important;
      }
	  .text-header{
		font-size: 16px !important;
		font-weight:bold;
      }
	  .text-judul{
        font-size: 18px  !important;
        
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 20px 0;
		}
		fieldset {
			border:1px solid #000;
			border-radius:4px;
			box-shadow:0 0 0px #000;
			margin-top: -4px !important;
		}
		legend {
			background:#fff;
		}
    }

	.text-muted {
	  color: #000;
	  font-weight: normal;
	  font-style: italic;
	  
	}
	.center {
	  display: block;
	  margin-left: auto;
	  margin-right: auto;
	  width: 50%;
	}
	ul {
	  margin:0;
	  padding-left: 12px;
	}
  </style>
</head>

<body>
	<header>
		
	</header>
	
	<main>
	<table class="content">
		<tr>
			<td width="100%" class="text-center"><img src="<?=base_url().'/assets/upload/app_setting/'.$logo?>" alt="" width="100" height="100"></td>
		</tr>
		<tr>
			<td width="100%" class="text-center text-normal"><?=$alamat_rs1?><br><?=$telepon_rs?> <?=$fax_rs?><br><?=$web_rs?> Email : <?=$email_rs?></td>
		</tr>
	</table>
	
	<table class="content">
		<tr>
			<td width="100%" class="text-center text-judul"><strong><?=$judul_header_ina?></strong><br><i><?=$judul_header_eng?></i></td>
		</tr>
		<tr>
			<td width="100%" class="text-left text-upnormal"><strong><?=$identitas_pasien_ina?></strong><br><i><?=$identitas_pasien_eng?></i></td>
		</tr>
	</table>
	
	<table class="content">
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$no_reg_ina?></strong><br><i><?=$no_reg_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?=$nopendaftaran?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$norek_ina?></strong><br><i><?=$norek_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=$no_medrec?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$nama_pasien_ina?></strong><br><i><?=$nama_pasien_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=$namapasien?></td>
		</tr>
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$tanggal_lahir_ina?></strong><br><i><?=$tanggal_lahir_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?= HumanDateShort($tanggal_lahir)?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$umur_ina?></strong><br><i><?=$umur_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=$umurtahun?> Tahun</td>
			<td width="12%" class="text-left text-normal"><strong><?=$jenis_kelamin_ina?></strong><br><i><?=$jenis_kelamin_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=$jk?></td>
		</tr>
		
		
	</table>
	<br>
	<br>
	<br>
	<table class="content">
		<tr>
			<td width="100%" class="text-center text-judul"><strong><?=$daftar_persetujuan_ina?></strong><br><i><?=$daftar_persetujuan_eng?></i></td>
		</tr>
	</table>
	<br>
	<table class="content">
		<thead>
			<tr>
				<td width="12%" class="text-center border-full"><strong><?=$tanggal_jam_ina?></strong><br><i><?=$tanggal_jam_eng?></i></td>
				<td width="24%" class="text-center border-full">
					<strong><?=$nama_tindakan_ina?></strong><br><i><?=$nama_tindakan_eng?></i>
				</td>
				<td width="24%" class="text-center border-full"><strong><?=$ket_ina?></strong><br><i><?=$ket_eng?></i></td>
				<td width="15%" class="text-center border-full"><strong><?=$ttd_pasien_ina?></strong><br><i><?=$ttd_pasien_eng?></i></td>
				<td width="10%" class="text-center border-full"><strong><?=$persetujuan_ina?></strong><br><i><?=$persetujuan_eng?></i></td>
				<td width="15%" class="text-center border-full"><strong><?=$ttd_petugas_ina?></strong><br><i><?=$ttd_petugas_eng?></i></td>
			</tr>
		
		</thead>
		
		?>
		<tbody>
			<?
				$q="select H.*,M.nama as nama_setuju FROM tranap_invasif_materi H
				LEFT JOIN mjenis_persetujuan_invasif M ON M.id=H.hereby
				WHERE H.assesmen_id='$assesmen_id' AND H.staktif='1'";
				
				$list_info=$this->db->query($q)->result();
				  $tabel='';
				  foreach ($list_info as $r){
					  // style="text-align: center;"
					  $btn_ttd_petugas='';
					  if ($r->st_otomatis=='1'){
							$btn_ttd_petugas='<img class="" style="width:70px;height:70px; text-align: center;"  src="'.base_url().'qrcode/qr_code_ppa/'.$r->petugas_id.'" alt="">'.'<br>'.$r->ttd_petugas_nama;
						  
					  }else{
							if ($r->ttd_petugas){
								$img='<img class="" style="width:100px;height:90px; text-align: center;"  src="'.$r->ttd_petugas.'" alt="">'.'<br>'.$r->ttd_petugas_nama;
								$btn_ttd_petugas=$img;
							}else{
							}

					  }
					  $btn_ttd_sasaran='';
					  if ($r->ttd_sasaran){
						  $btn_ttd_sasaran='<img class="" style="width:100px;height:90px; text-align: center;"  src="'.$r->ttd_sasaran.'" alt="">'.'<br>'.$r->nama_pernyataan;
					  
					  }else{
					  }
					
					  $tabel .='<tr>';
						$tabel .='<td class="text-center border-full"><input type="hidden" class="invasif_detail_id" value="'.$r->id.'">'.HumanDateLong($r->input_tanggal).'</td>';
						$tabel .='<td class="border-full">'.$r->materi_invasif.'</td>';
						$tabel .='<td class="text-center border-full">'.$r->ket_invasif.'</td>';
						$tabel .='<td class="text-center border-full">'.$btn_ttd_sasaran.'</td>';
						$tabel .='<td class="text-center border-full">'.($r->hereby==1?text_primary($r->nama_setuju):text_danger($r->nama_setuju)).'</td>';
						$tabel .='<td class="text-center border-full">'.$btn_ttd_petugas.'</td>';
					  $tabel .='</tr>';
				  } 
			?>
			<? echo $tabel;?>
		</tbody>
	</table>
	
	<br>
	<br>
	<br>
	<br>
	<br>
	<table class="content">
		<tr>
			<td class="text-left text-normal"><strong><?=($judul_footer_ina)?></strong><br><i><?=($judul_footer_eng)?></i></td>
		</tr>
	</table>
	
	</main>
</body>

</html>
