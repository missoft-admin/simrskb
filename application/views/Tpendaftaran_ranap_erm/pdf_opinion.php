<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title><?=$title?></title>
	<style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
	@page {
		margin-top: 1em;
		margin-left: 1.6em;
		margin-right: 1.5 em;
		margin-bottom: 1em;
	}
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collrohanie: collrohanie !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }

      th {
        padding: 5px;
      }
	  td {
        padding: 0px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }

      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collrohanie: collrohanie !important;
        width: 100% !important;
		
      }
      th {
        padding: 5px;

      }
	  td {
        padding: 0px;

      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
		vertical-align:middle;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .text-notes{
		font-size: 10px !important;
      }
	  .text-normal{
		font-size: 12px !important;
      }
	  .text-upnormal{
		font-size: 14px !important;
      }
	  .text-header{
		font-size: 16px !important;
		font-weight:bold;
      }
	  .text-judul{
        font-size: 18px  !important;
        
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 20px 0;
		}
		fieldset {
			border:1px solid #000;
			border-radius:4px;
			box-shadow:0 0 0px #000;
			margin-top: -4px !important;
		}
		legend {
			background:#fff;
		}
    }

	.text-muted {
	  color: #000;
	  font-weight: normal;
	  font-style: italic;
	  
	}
	.center {
	  display: block;
	  margin-left: auto;
	  margin-right: auto;
	  width: 50%;
	}
	ul {
	  margin:0;
	  padding-left: 12px;
	}
  </style>
</head>

<body>
	<? $this->load->view('Tsurat/pdf_header')?>
	<table class="content">
		<tr>
			<td width="100%" class="text-center text-judul"><strong><?=$judul_header?></strong><br><i><?=$judul_header_eng?></i></td>
		</tr>
		
	</table>
	<br>
	<br>
	<table class="content">
		<tr>
			<td class="text-left text-normal"><strong><?=strip_tags($paragraf_1_ina)?></strong><br><i><?=strip_tags($paragraf_1_eng)?></i></td>
		</tr>
	</table>
	<br>
	<br>
	<br>
	<table class="content">
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$nama_ttd_ina?></strong><br><i><?=$nama_ttd_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?=$nama_ttd?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$ttl_ttd_ina?></strong><br><i><?=$ttl_ttd_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=$ttl_ttd?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$umur_ttd_ina?></strong><br><i><?=$umur_ttd_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=$umur_ttd?></td>
		</tr>
	</table>	
	<table>
		<tr>
			<td style="width:12%" class="text-bold"><?=$alamat_ttd_ina?><br><i><?=$alamat_ttd_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td style="width:50%" class="text-left text-normal">
				<?=$alamat_ttd?>
			</td>
			<td style="width:12%" class="text-bold"><?=$hubungan_ina?><br><i><?=$hubungan_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td style="width:23%" class="">
				<?=get_nama_ref($hubungan_ttd,9)?>
			</td>
		</tr>
	</table>	
	
	<br>
	<table class="content">
		<tr>
			<td class="text-left text-normal"><strong><?=strip_tags($paragraf_2_ina)?></strong><br><i><?=strip_tags($paragraf_2_eng)?></i></td>
		</tr>
		<tr>
			<td class="text-left text-normal"><strong><?=strtoupper(get_nama_ref($hereby,436))?></strong></td>
		</tr>
		
	</table>
	<table class="content">
		<tr>
			<td class="text-left text-normal"><strong><?=strip_tags($terhadap_ina)?></strong><br><i><?=strip_tags($terhadap_eng)?></i></td>
		</tr>
	</table>
	<table class="content">
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$noreg_ina?></strong><br><i><?=$noreg_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?=$nopendaftaran?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$nomedrec_ina?></strong><br><i><?=$nomedrec_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=$no_medrec?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$nama_pasien_ina?></strong><br><i><?=$nama_pasien_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=$namapasien?></td>
		</tr>
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$ttl_pasien_ina?></strong><br><i><?=$ttl_pasien_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?= HumanDateShort($tanggal_lahir)?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$umur_pasien_ina?></strong><br><i><?=$umur_pasien_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=$umurtahun.' Tahun '.$umurbulan.' Bulan '.$umurhari.' Hari'?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$jk_ina?></strong><br><i><?=$jk_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=$jk?></td>
		</tr>
		
		
	</table>
	<br>
	<table class="content">
		<tr>
			<td class="text-left text-normal"><strong><?=strip_tags($paragraf_3_ina)?></strong><br><i><?=strip_tags($paragraf_3_eng)?></i></td>
		</tr>
		
	</table>
	<table class="content">
		
		<tr>
			<td style="width:2%">&nbsp;</td>
			<td style="width:98%" class="text-left text-normal"><?=($paragraf_4_ina)?></td>
		</tr>
		
	</table>
	<table class="content">
		<tr>
			<td class="text-left text-normal">
				<table class="table table-bordered" id="tabel_nama" >
					<thead>
						<tr>
							<th width="5%" class="text-center border-full">No</th>
							<th width="45%" class="text-center border-full"><?=$hasil_ina?> / <i class="text-muted"><?=$hasil_eng?></i></th>
							<th width="50%" class="text-center border-full"><?=$ket_tabel_ina?> / <i class="text-muted"><?=$ket_tabel_eng?></i></th>
						</tr>
						
					</thead>
					<?
							
					  $q="select H.* FROM tranap_opinion_nama H
							WHERE H.assesmen_id='$assesmen_id'";
					  $list_info=$this->db->query($q)->result();
					  $tabel='';
					  $no=1;
					  foreach ($list_info as $r){
						 
						  $tabel .='<tr>';
							$tabel .='<td class="text-center border-full">'.($no).'</td>';
							$tabel .='<td class="text-left border-full">'.$r->nama.'</td>';
							$tabel .='<td class="text-left border-full">'.$r->keterangan.'</td>';
						  $tabel .='</tr>';
						  $no++;
					  } 
					?>
					<tbody>
						<?=$tabel;?>
					</tbody>
				</table>
			
			</td>
		</tr>
	</table>
	
		<br>
		<br>
	<table class="content">
		<tr>
			<td class="text-left text-normal"><strong><?=strip_tags($paragraf_5_ina)?></strong><br><i><?=strip_tags($paragraf_5_eng)?></i></td>
		</tr>
		
	</table>
	<br>
		<br>
	<table class="content">
		<?
		$arr_mengetahui=get_nama_ppa_array($petugas_id);
		$arr_menerima=get_nama_ppa_array($yg_menerima);
		?>
		<tr>
			<td style="width:25%" class="text-center"><strong><?=strip_tags($yg_pernyataan_ina)?></strong><br><i><?=strip_tags($yg_pernyataan_eng)?></i></td>
			<td style="width:25%" class="text-center"><strong><?=strip_tags($yg_menerima_ina)?></strong><br><i><?=strip_tags($yg_menerima_eng)?></i></td>
			<td style="width:25%" class="text-center"><strong><?=strip_tags($saksi_kel_ina)?></strong><br><i><?=strip_tags($saksi_kel_eng)?></i></td>
			<td style="width:25%" class="text-center"><strong><?=strip_tags($saksi_rs_ina)?></strong><br><i><?=strip_tags($saksi_rs_eng)?></i></td>
		</tr>
		<tr>
			<td style="width:25%" class="text-center">
				<img class="" style="width:100px;height:90px; text-align: center;" src="<?=$ttd_pernyataan?>" alt="" title="">
				
			</td>
			<td style="width:25%" class="text-center">
				
				<img class="" style="width:100px;height:90px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($yg_menerima?$yg_menerima:$login_ppa_id)?>" alt="" title="">
			</td>
			<td style="width:25%" class="text-center">
				
				<img class="" style="width:100px;height:90px; text-align: center;" src="<?=$saksi_kel_ttd?>" alt="" title="">
			</td>
			<td style="width:25%" class="text-center">
				
				<img class="" style="width:100px;height:90px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($petugas_id?$petugas_id:$login_ppa_id)?>" alt="" title="">
			</td>
		</tr>
		<tr>
			<td style="width:25%" class="text-center"><strong><?=($nama_ttd)?></strong></td>
			<td style="width:25%" class="text-center">
			<strong><?=$arr_menerima['nama']?></strong><br><i>(<?=$arr_menerima['nik']?>)</i>
			</td>
			<td style="width:25%" class="text-center"><strong><?=($saksi_kel)?></strong></td>
			<td style="width:25%" class="text-center">
			<strong><?=$arr_mengetahui['nama']?></strong><br><i>(<?=$arr_mengetahui['nik']?>)</i>
			</td>
		</tr>
		
	</table>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<table class="content">
		<tr>
			<td class="text-left text-normal"><strong><?=($footer_ina)?></strong><br><i><?=($footer_eng)?></i></td>
		</tr>
	</table>
	</main>
</body>

</html>
