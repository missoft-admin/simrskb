<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title><?=$title?></title>
	<style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
	
	@page {size: 210mm 330mm; margin:7mm!important; padding:2mm!important}
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }

      th {
        padding: 5px;
      }
	  td {
        padding: 0px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }

      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		
      }
      th {
        padding: 5px;

      }
	  td {
        padding: 0px;

      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
		vertical-align:middle;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .text-notes{
		font-size: 10px !important;
      }
	  .text-normal{
		font-size: 12px !important;
      }
	  .text-upnormal{
		font-size: 14px !important;
      }
	  .text-header{
		font-size: 16px !important;
		font-weight:bold;
      }
	  .text-judul{
        font-size: 18px  !important;
        
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 20px 0;
		}
		fieldset {
			border:1px solid #000;
			border-radius:4px;
			box-shadow:0 0 0px #000;
			margin-top: -4px !important;
		}
		legend {
			background:#fff;
		}
    }

	.text-muted {
	  color: #000;
	  font-weight: normal;
	  font-style: italic;
	  
	}
	.center {
	  display: block;
	  margin-left: auto;
	  margin-right: auto;
	  width: 50%;
	}
	ul {
	  margin:0;
	  padding-left: 12px;
	}
  </style>
</head>

<body>
	<header>
		
	</header>
	
	<main>
	<? $this->load->view('Tsurat/pdf_header')?>
	<?
	$br_spasi='&nbsp;<br>&nbsp;<br>&nbsp;<br>';
	$br_spasi_short='&nbsp;<br>&nbsp;<br>';
	?>
	<table class="content">
		<tr>
			<td width="100%" class="text-center text-judul"><strong><?=$judul_header_ina?></strong><br><i><?=$judul_header_eng?></i></td>
		</tr>
		<tr>
			<td width="100%" class="text-left text-upnormal"><strong><?=$label_identitas_ina?></strong><br><i><?=$label_identitas_eng?></i></td>
		</tr>
	</table>
	<table class="content">
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$no_reg_ina?></strong><br><i><?=$no_reg_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?=$nopendaftaran?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$norek_ina?></strong><br><i><?=$norek_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=$no_medrec?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$nama_pasien_ina?></strong><br><i><?=$nama_pasien_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=$namapasien?></td>
		</tr>
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$tanggal_lahir_ina?></strong><br><i><?=$tanggal_lahir_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?= HumanDateShort($tanggal_lahir)?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$usia_ina?></strong><br><i><?=$usia_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=$umurtahun?> Tahun</td>
			<td width="12%" class="text-left text-normal"><strong><?=$jenis_kelamin_ina?></strong><br><i><?=$jenis_kelamin_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=$jk?></td>
		</tr>
		
		
	</table>
	<table class="content">
		<tr>
			<td width="100%" class="text-left text-upnormal"><strong><?=$label_pemberi_info_ina?></strong><br><i><?=$label_pemberi_info_eng?></i></td>
		</tr>
	</table>
	<table class="content">
		<tr>
			<td width="1%" class="text-left text-normal"><strong>&nbsp;</strong></td>
			<td width="15%" class="text-left text-normal"><strong><?=$dokter_pelaksan_ina?></strong><br><i><?=$dokter_pelaksan_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="33%" class="text-left text-normal"><?=get_nama_ppa($dokter_pelaksana)?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$tanggal_tindakan_ina?></strong><br><i><?=$tanggal_tindakan_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="12%" class="text-left text-normal"><?= HumanDateShort($tanggal_informasi)?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$waktu_tindakan_ina?></strong><br><i><?=$waktu_tindakan_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="12%" class="text-left text-normal"><?=HumanTime($tanggal_informasi)?></td>
			
		</tr>
		
	</table>
	<table class="content">
		<tr>
			<td width="1%" class="text-left text-normal"><strong>&nbsp;</strong></td>
			<td width="15%" class="text-left text-normal"><strong><?=$nama_pemberi_info_ina?></strong><br><i><?=$nama_pemberi_info_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="33%" class="text-left text-normal"><?=get_nama_ppa($pemberi_info)?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$nama_petugas_pendamping_ina?></strong><br><i><?=$nama_petugas_pendamping_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="37%" class="text-left text-normal"><?= get_nama_ppa($pendamping)?></td>
			
		</tr>
		<tr>
			<td width="1%" class="text-left text-normal"><strong>&nbsp;</strong></td>
			<td width="15%" class="text-left text-normal"><strong><?=$nama_penerima_info_ina?></strong><br><i><?=$nama_penerima_info_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="33%" class="text-left text-normal"><?=($penerima_info)?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$hubungan_ina?></strong><br><i><?=$hubungan_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="37%" class="text-left text-normal"><?= get_nama_ref($hubungan_id,80)?></td>
			
		</tr>
		
	</table>
	<br>
	<table class="content">
		<tr>
			<td width="1%" class="text-left text-normal"><strong>&nbsp;</strong></td>
			<td width="99%" class="text-left text-normal">
				<table class="table table-bordered" id="tabel_informasi">
					<thead>
						<tr>
							<td width="5%" class="text-center border-full"><strong>No</strong></td>
							<td width="25%" class="text-center border-full"><strong><?=$jenis_info_ina?></strong><br><i><?=$jenis_info_eng?></i></td>
							<td width="50%" class="text-center border-full"><strong><?=$isi_info_ina?></strong><br><i><?=$isi_info_eng?></i></td>
							<td width="20%" class="text-center border-full"><strong><?=$paraf_ina?></strong><br><i><?=$paraf_eng?></i></td>
						</tr>
					</thead>
					<?
					$btn_ttd_dokter='';
					$btn_ttd_kel='';
						$q="select *FROM tpoliklinik_ic_informasi WHERE assesmen_id='$assesmen_id'";
						$list_info=$this->db->query($q)->result();
						$tabel='';
						foreach ($list_info as $r){
						  // style="text-align: center;"
						  $btn_paraf='';
						  if ($r->paraf){
						  $btn_paraf='
								<img style="width:80px;height:60px; text-align: center;"src="'.$r->paraf.'" alt="">
						  ';
						  }
						  $tabel .='<tr>';
							$tabel .='<td class="text-center border-full"><input type="hidden" class="informasi_id" value="'.$r->id.'">'.$r->nourut.'</td>';
							$tabel .='<td class="text-center border-full"><strong>'.$r->nama.'</strong><br><i>'.$r->nama_english.'</i></td>';
							$tabel .='<td class="border-full">'.($r->isi_informasi).'</td>';
							$tabel .='<td class="text-center border-full">'.$btn_paraf.'</td>';
						  $tabel .='</tr>';
					  } 
					 $btn_ttd_dokter='<img style="width:80px;height:60px; text-align: center;"src="'.$ttd_dokter_pelaksana.'" alt="">';
					 $btn_ttd_kel='<img style="width:80px;height:60px; text-align: center;"src="'.$ttd_penerima_info.'" alt="">';
					  $tabel .='<tr>';
					  $tabel .='<td colspan="3" class="border-full"><strong>'.$ket_ttd_dokter_ina.'</strong><br><i>'.$ket_ttd_dokter_eng.'</i></td>';
					  $tabel .='<td  class="border-full text-center">
								<div class=" text-center"><strong>'.$ttd_dokter_ina.'</strong> / <i>'.$ttd_dokter_eng.'</i></div>
								<div class=" class="border-full  text-muted text-uppercase push-5-t text-center">'.($ttd_dokter_pelaksana?$btn_ttd_dokter:$br_spasi_short).'</div>
								<div class=" class="border-full  text-muted text-uppercase push-5-t text-center">'.get_nama_ppa($dokter_pelaksana).'</div>
								</td>';
					  $tabel .='</tr>';
					  $tabel .='<tr>';
					  $tabel .='<td colspan="3" class="border-full"><strong>'.$ket_ttd_pasien_ina.'</strong><br><i>'.$ket_ttd_pasien_eng.'</i></td>';
					   $tabel .='<td  class="border-full text-center">
								<div class=" text-center"><strong>'.$ttd_pasien_ina.'</strong>/<i>'.$ttd_pasien_eng.'</i></div>
								<div class=" class="border-full text-muted text-uppercase push-5-t text-center">'.($ttd_penerima_info?$btn_ttd_kel:$br_spasi_short).'</div>
								<div class=" class="border-full text-muted text-uppercase push-5-t text-center">'.($penerima_info).'</div>
								</td>';
					  $tabel .='</tr>';
					?>
					<tbody>
						<?=$tabel;?>
					</tbody>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<table class="content">
		<tr>
			<td width="100%" class="text-left text-upnormal"><strong><?=$label_pernyataan_ina?></strong><br><i><?=$label_pernyataan_eng?></i></td>
		</tr>
	</table>
	<table class="content">
		<tr>
			<td width="1%" class="text-left text-upnormal">&nbsp;</td>
			<td width="99%" class="text-left text-upnormal"><strong><?=$info_pernyataan_ina?></strong><br><i><?=$info_pernyataan_eng?></i></td>
		</tr>
	</table>
	<table class="content">
		<tr>
			<td width="1%" class="text-left text-normal"><strong>&nbsp;</strong></td>
			<td width="15%" class="text-left text-normal"><strong><?=$nama_pembuat_pernyataan_ina?></strong><br><i><?=$nama_pembuat_pernyataan_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="33%" class="text-left text-normal"><?=($nama)?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$umur_pembuat_pernyataan_ina?></strong><br><i><?=$umur_pembuat_pernyataan_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="12%" class="text-left text-normal"><?= ($umur)?> Tahun</td>
			<td width="12%" class="text-left text-normal"><strong><?=$jk_pembuat_pernyataan_ina?></strong><br><i><?=$jk_pembuat_pernyataan_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="12%" class="text-left text-normal"><?=get_nama_ref($jenis_kelamin,77)?></td>
			
		</tr>
		
	</table>
	<br>
	<?
		$q="SELECT *FROM mjenis_persetujuan_ic H WHERE H.id='$hereby'";
		$data_setuju=$this->db->query($q)->row();
	?>
	<table class="content">
		<tr>
			<td width="1%" class="text-left text-normal"><strong>&nbsp;</strong></td>
			<td width="15%" class="text-left text-normal"><strong><?=$alamat_pembuat_pernyataan_ina?></strong><br><i><?=$alamat_pembuat_pernyataan_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="33%" class="text-left text-normal"><?=($alamat)?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$nik_pembuat_pernyataan_eng?></strong><br><i><?=$nik_pembuat_pernyataan_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="37%" class="text-left text-normal"><?= ($no_ktp)?></td>
			
		</tr>
		<tr>
			<td width="1%" class="text-left text-normal"><strong>&nbsp;</strong></td>
			<td width="15%" class="text-left text-normal"><strong><?=$dengan_ini_memberikan_ina?></strong><br><i><?=$dengan_ini_memberikan_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="33%" class="text-left text-normal"><strong><i><?=($data_setuju->nama)?></i></strong></td>
			<td width="12%" class="text-left text-normal"><strong><?=$untuk_tindakan_ina?></strong><br><i><?=$untuk_tindakan_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="37%" class="text-left text-normal"><?= ($untuk_tindakan)?></td>
			
		</tr>
		
	</table>
	<table class="content">
		<tr>
			<td width="1%" class="text-left text-normal"><strong>&nbsp;</strong></td>
			<td width="15%" class="text-left text-normal"><strong><?=$terhadap_ina?></strong><br><i><?=$terhadap_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="8%" class="text-left text-normal"><?= get_nama_ref($terhadap,79)?></td>
			<td width="10%" class="text-right text-normal"><strong><?=$nama_pasien_ina?></strong><br><i><?=$nama_pasien_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="14%" class="text-left text-normal"><?= ($nama_pasien)?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$tanggal_lahir_ina?></strong><br><i><?=$tanggal_lahir_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="12%" class="text-left text-normal"><?=HumanDateShort($tanggal_lahir_pasien)?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$usia_ina?></strong><br><i><?=$usia_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="12%" class="text-left text-normal"><?=($umur_pasien)?> Tahun</td>
			
		</tr>
		
		<tr>
			<td width="1%" class="text-left text-normal"><strong>&nbsp;</strong></td>
			<td width="15%" class="text-left text-normal"><strong><?=$jenis_kelamin_ina?></strong><br><i><?=$jenis_kelamin_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="8%" class="text-left text-normal"><?= ($jenis_kelamin_pasien)?></td>
			<td width="10%" class="text-right text-normal"><strong><?=$norek_ina?></strong><br><i><?=$norek_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="14%" class="text-left text-normal"><?= ($no_medrec)?></td>
			<td width="12%" class="text-left text-normal">&nbsp;</td>
			<td width="1%" class="text-left text-normal">&nbsp;</td>
			<td width="12%" class="text-left text-normal">&nbsp;</td>
			<td width="12%" class="text-left text-normal">&nbsp;</td>
			<td width="1%" class="text-left text-normal">&nbsp;</td>
			<td width="12%" class="text-left text-normal">&nbsp;</td>
			
		</tr>
		
		
	</table>
	<br><br>
	<table class="content">
		<tr>
			<td width="2%" class="text-left text-upnormal">&nbsp;</td>
			<td width="98%" class="text-left text-upnormal"><strong><?=$data_setuju->label_ina?></strong><br><i><?=$data_setuju->label_eng?></i></td>
		</tr>
	</table>
	<br>
	<br>
	<table class="content">
		<tr>
			<td width="1%" class="text-left text-upnormal">&nbsp;</td>
			<td width="99%" class="text-left text-upnormal"><strong><?=$waktu_pernyataan_ina?></strong> / <i><?=$waktu_pernyataan_eng?></i></td>
		</tr>
		<tr>
			<td width="1%" class="text-left text-upnormal">&nbsp;</td>
			<td width="99%" class="text-left text-upnormal"><strong><?=$lokasi_pernyataan_ina.',  '.HumanDateLong($tanggal_pernyataan)?></strong></i></td>
		</tr>
	</table>
	<br>
	<br>
	<br>
	<table class="content">
		<tr>
			<td style="width:30%" class="text-center"><strong><?=$ket_gambar_paraf_menyatakan_ina?></strong><br><i><?=$ket_gambar_paraf_menyatakan_eng?></i></td>
			<td style="width:30%" class="text-center"><strong><?=$ket_gambar_paraf_saksi_ina?></strong><br><i><?=$ket_gambar_paraf_saksi_eng?></i></td>
			<td style="width:30%" class="text-center"><strong><?=$ket_gambar_paraf_saksi_rs_ina?></strong><br><i><?=$ket_gambar_paraf_saksi_rs_eng?></i></td>
		</tr>
		<tr>
			<td style="width:30%" class="text-center">
				<?if ($ttd_menyatakan){?>
				<img style="width:100px;height:100px; text-align: center;" src="<?=$ttd_menyatakan?>" alt="">
				<?}else{?>
					<?=$br_spasi.$br_spasi?>
				<?}?>
			</td>
			<td style="width:30%" class="text-center">
				<?if ($ttd_saksi){?>
				<img style="width:100px;height:100px; text-align: center;" src="<?=$ttd_saksi?>" alt="">
				<?}else{?>
					<?=$br_spasi.$br_spasi?>
				<?}?>
			</td>
			<td style="width:30%" class="text-center">
				<?if ($ttd_saksi_rs){?>
				<img style="width:100px;height:100px; text-align: center;" src="<?=$ttd_saksi_rs?>" alt="">
				<?}else{?>
					<?=$br_spasi.$br_spasi?>
				<?}?>
			</td>
		</tr>
		<tr>
			<td style="width:30%" class="text-center"><label id="nama_pemberi_pernyataan"><?=$yang_menyatakan?></label></td>
			<td style="width:30%" class="text-center"><label id="nama_saksi_keluarga"><?=$saksi_keluarga?></label></td>
			<td style="width:30%" class="text-center"><label id="nama_saksi_rs"><?=$saksi_rs?></td>
		</tr>
	</table>
	
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<table class="content">
		<tr>
			<td class="text-left text-normal"><strong><?=($judul_footer_ina)?></strong><br><i><?=($judul_footer_eng)?></i></td>
		</tr>
	</table>
	</main>
</body>

</html>
