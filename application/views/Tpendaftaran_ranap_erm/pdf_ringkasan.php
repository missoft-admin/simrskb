<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title><?=$title?></title>
	<style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
	@page {
		margin-top: 1em;
		margin-left: 1,3em;
		margin-right: 1,5 em;
		margin-bottom: 1em;
	}
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }

      th {
        padding: 5px;
      }
	  td {
        padding: 5px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }

      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		
      }
      th {
        padding: 5px;

      }
	  td {
        padding: 3px;

      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
		vertical-align:top;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .text-normal{
		font-size: 12px !important;
      }
	  .text-upnormal{
		font-size: 14px !important;
      }
	  .text-header{
		font-size: 16px !important;
		font-weight:bold;
      }
	  .text-judul{
        font-size: 18px  !important;
        
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 20px 0;
		}
		fieldset {
			border:1px solid #000;
			border-radius:4px;
			box-shadow:0 0 0px #000;
			margin-top: -4px !important;
		}
		legend {
			background:#fff;
		}
    }

	.text-muted {
	  color: #000;
	  font-weight: normal;
	  font-style: italic;
	  
	}
	.center {
	  display: block;
	  margin-left: auto;
	  margin-right: auto;
	  width: 50%;
	}
  </style>
</head>

<body>
	<header>
		
	</header>
	
	<main>
	<table class="content">
		<tr>
			<td width="100%" class="text-center"><img src="<?=base_url().'/assets/upload/app_setting/'.$logo?>" alt="" width="100" height="100"></td>
		</tr>
		<tr>
			<td width="100%" class="text-center text-normal"><?=$alamat_rs1?><br><?=$telepon_rs?> <?=$fax_rs?><br><?=$web_rs?> Email : <?=$email_rs?></td>
		</tr>
	</table>
	
	<table class="content">
		<tr>
			<td width="100%" class="text-center text-judul"><strong><?=$judul_header_ina?></strong><br><i><?=$judul_header_eng?></i></td>
		</tr>
		<tr>
			<td width="100%" class="text-left text-upnormal"><strong>IDENTITAS PASIEN</strong></td>
		</tr>
	</table>
	<table class="content">
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$no_reg_ina?></strong><br><i><?=$no_reg_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?=$no_medrec?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$no_rm_ina?></strong><br><i><?=$no_rm_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=$no_medrec?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$nama_pasien_ina?></strong><br><i><?=$nama_pasien_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=$namapasien?></td>
		</tr>
		<tr>
			<td width="12%" class="text-left text-normal"><strong><?=$ttl_ina?></strong><br><i><?=$ttl_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="15%" class="text-left text-normal"><?= HumanDateShort($tanggal_lahir)?></td>
			<td width="14%" class="text-left text-normal"><strong><?=$umur_ina?></strong><br><i><?=$umur_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="20%" class="text-left text-normal"><?=$umur?></td>
			<td width="12%" class="text-left text-normal"><strong><?=$jk_ina?></strong><br><i><?=$jk_eng?></i></td>
			<td width="1%" class="text-left text-normal"><strong>:</strong></td>
			<td width="23%" class="text-left text-normal"><?=$jenis_kelamin?></td>
		</tr>
		
		
	</table>
	<table class="content">
		<tr>
			<td width="100%" class="text-left text-normal"><strong><?= strtoupper($kondisi_ina)?></strong><?=strtoupper($kondisi_eng)?></td>
		</tr>
	</table>
  <table class="content">
		<tr>
			<td style="width:16%;" class="text-normal"><?=$alasan_ina?><br><i><?=$alasan_eng?></i></td>
			<td style="width:18%;" class="text-normal"><?=$mobilisasi_ina?><br><i><?=$mobilisasi_eng?></i></td>
			<td style="width:16%;" class="text-normal"><?=$mobilisasi_lain_ina?><br><i><?=$mobilisasi_lain_eng?></i></td>
			<td style="width:16%;" class="text-normal"><?=$alkes_ina?><br><i><?=$alkes_eng?></i></td>
			<td style="width:16%;" class="text-normal"><?=$perawatan_ina?><br><i><?=$perawatan_eng?></i></td>
			<td style="width:18%;" class="text-normal"><?=$perawatan_lain_ina?><br><i><?=$perawatan_lain_eng?></i></td>
		</tr>
    <tr>
			<td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($alasan?get_nama_ref($alasan,317):'&nbsp;')?></fieldset>
			</td>
			<td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($mobilisasi?get_nama_ref($mobilisasi,318):'&nbsp;')?></fieldset>
			</td>
			<td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($mobilisasi_lain?($mobilisasi_lain):'&nbsp;')?></fieldset>
			</td>
			<td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($alkes?get_nama_ref($alkes,319):'&nbsp;')?></fieldset>
			</td>
			<td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($perawatan?get_nama_ref($perawatan,320):'&nbsp;')?></fieldset>
			</td>
			<td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($perawatan_lain?($perawatan_lain):'&nbsp;')?></fieldset>
			</td>
			
		</tr>
	</table>
  <table class="content">
		<tr>
			<td width="100%" class="text-left text-normal"><strong><?=strtoupper($keadaan_pulang_ina)?></strong> / <?=strtoupper($keadaan_pulang_eng)?></td>
		</tr>
	</table>
  <table class="content">
		<tr>
			<td style="width:20%;" class="text-normal"><?=$tv_ina?><br><i><?=$tv_eng?></i></td>
			<td style="width:40%;" class="text-normal"><?=$tingkat_kesadaran_ina?><br><i><?=$tingkat_kesadaran_eng?></i></td>
			<td style="width:20%;" class="text-normal"><?=$tekanan_darah_ina?><br><i><?=$tekanan_darah_eng?></i></td>
			<td style="width:20%;" class="text-normal"><?=$suhu_ina?><br><i><?=$suhu_eng?></i></td>
		</tr>
    <tr>
			<td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($jam_tv?($jam_tv):'&nbsp;')?></fieldset>
			</td>
			<td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($tingkat_kesadaran?get_nama_ref($tingkat_kesadaran,23):'&nbsp;')?></fieldset>
			</td>
			<td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($td_sistole.' '.$satuan_td.' / '.$td_diastole.' '.$satuan_td)?></fieldset>
			</td>
			<td class="text-normal">
        <fieldset><legend style="font-weight:bold;"></legend><?=($suhu.' ')?>&deg; C</fieldset>
			</td>
			
		</tr>
	</table>
  <table class="content">
		<tr>
			<td style="width:20%;" class="text-normal"><?=$nadi_ina?><br><i><?=$nadi_eng?></i></td>
			<td style="width:20%;" class="text-normal"><?=$nafas_ina?><br><i><?=$nafas_eng?></i></td>
			<td style="width:20%;" class="text-normal"><?=$spo2_ina?><br><i><?=$spo2_eng?></i></td>
			<td style="width:20%;" class="text-normal"><?=$tb_ina?><br><i><?=$tb_eng?></i></td>
			<td style="width:20%;" class="text-normal"><?=$bb_ina?><br><i><?=$bb_eng?></i></td>
		</tr>
    <tr>
			<td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($nadi.' '.$satuan_nadi)?></fieldset>
			</td>
      <td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($nafas.' '.$satuan_nafas)?></fieldset>
			</td>
			<td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($spo2.' '.$satuan_spo2)?></fieldset>
			</td>
      <td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($tinggi_badan.' cm')?></fieldset>
			</td>
      <td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($berat_badan.' Kg')?></fieldset>
			</td>
			
		</tr>
	</table>
  <table class="content">
		<tr>
			<td width="50%" class="text-left text-normal"><strong><?=strtoupper($diagnosa_kep_ina)?></strong> / <i><?=strtoupper($diagnosa_kep_eng)?></i></td>
			<td width="50%" class="text-left text-normal"><strong><?=strtoupper($tindakan_kep_ina)?></strong> / <i><?=strtoupper($tindakan_kep_eng)?></i></td>
		</tr>
	</table>
  <table class="content">
		<tr>
			<td style="width:50%;" class="text-normal">
          <table class="content">
              <tr>
                <td width="10%" class=" text-upnormal border-full text-center" style="background-color: #f2f2f2;"><strong>No</strong></td>
                <td width="90%" class=" text-upnormal border-full text-center" style="background-color: #f2f2f2;"><strong><?=$diagnosa_kep_ina?></strong><br><i><?=$diagnosa_kep_ina?></i></td>
              </tr>
              <?
                $q="SELECT MP.nama as nama_user,CONCAT(MD.nama,' (',MD.kode_diagnosa,')') as diagnosa, H.* FROM tranap_assesmen_pulang_diagnosa H
						LEFT JOIN mppa MP ON MP.id=H.created_by
						LEFT JOIN mdiagnosa MD ON MD.id=H.mdiagnosa_id 
						WHERE H.staktif='1' AND H.assesmen_id='".$assesmen_id."'
						ORDER BY H.prioritas ASC";
            $no=1;
            $list_diagnosa=$this->db->query($q)->result();
            foreach($list_diagnosa as $row){
              ?>
              <tr>
                <td width="10%" class=" text-normal border-full text-center"><?=$no?></td>
                <td width="90%" class=" text-normal border-full text-left"><?=$row->diagnosa?></td>
              </tr>
              <?
              $no++;
            }?>
          </table>
      </td>
      <td style="width:50%;" class="text-normal">
          <table class="content">
              <tr>
                <td width="10%" class=" text-upnormal border-full text-center" style="background-color: #f2f2f2;"><strong>No</strong><br></td>
                <td width="60%" class=" text-upnormal border-full text-center" style="background-color: #f2f2f2;"><strong><?=$tindakan_kep_ina?></strong><br><i><?=$tindakan_kep_eng?></i></td>
                <td width="30%" class=" text-upnormal border-full text-center" style="background-color: #f2f2f2;"><strong>Tanggal</strong><br><i>Date</i></td>
              </tr>
               <?
                $q="SELECT H.*
                    FROM tranap_assesmen_pulang_tindakan H
                    WHERE H.assesmen_id='".$assesmen_id."'
                    AND H.staktif='1'	
                    ORDER BY H.id ASC";
                  $no=1;
                  $list_diagnosa=$this->db->query($q)->result();
                  foreach($list_diagnosa as $row){
                    ?>
                    <tr>
                      <td  class=" text-normal border-full text-center"><?=$no?></td>
                      <td  class=" text-normal border-full text-left"><?=$row->nama?></td>
                      <td  class=" text-normal border-full text-center"><?=HumanDateShort_exp($row->tanggal)?></td>
                    </tr>
                    <?
                    $no++;
                  }?>
          </table>
      </td>
			
		</tr>
    
	</table>
  <table class="content">
		<tr>
			<td width="100%" class="text-left text-normal"><?=$evaluasi_ina?> / <i><?=$evaluasi_eng?></i></td>
		</tr>
    <tr>
			<td width="100%" class="text-left text-normal"><fieldset><legend style="font-weight:bold;"></legend><?=($evaluasi?$evaluasi:'&nbsp;')?></fieldset></td>
		</tr>
	</table>
  <table class="content">
		<tr>
			<td style="width:25%;" class="text-normal"><?=$obat_nyeri_ina?><br><i><?=$obat_nyeri_eng?></i></td>
			<td style="width:25%;" class="text-normal"><?=$efek_ina?><br><i><?=$efek_eng?></i></td>
			<td style="width:35%;" class="text-normal"><?=$bila_nyeri_ina?><br><i><?=$bila_nyeri_eng?></i></td>
			<td style="width:15%;" class="text-normal"><?=$batasan_ina?><br><i><?=$batasan_eng?></i></td>
		</tr>
    <tr>
      <td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($obat?($obat):'&nbsp;')?></fieldset>
			</td>
      <td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($efek?($efek):'&nbsp;')?></fieldset>
			</td>
      <td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($nyeri?($nyeri):'&nbsp;')?></fieldset>
			</td>
			<td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($batasan_cairan?get_nama_ref($batasan_cairan,321):'&nbsp;')?></fieldset>
			</td>
			
		</tr>
	</table>
  <table class="content">
		<tr>
			<td width="100%" class="text-left text-upnormal"><strong><?=$edukasi_ina?></strong> / <i><?=$edukasi_eng?></i></td>
		</tr>
	</table>
  <table class="content">
    <tr>
      <td width="100%">
        <table class="content">
          
            <tr>
              <td width="5%" class="text-normal border-full text-center"><strong>NO</strong></td>
              <td width="45%" class="text-normal border-full text-center"><strong>JENIS EDUKASI</strong></td>
              <?
              $q="SELECT H.jenis as id,M.ref as nama 
              FROM `tranap_assesmen_pulang_pengkajian` H 
              INNER JOIN merm_referensi M ON M.nilai=H.jenis AND M.ref_head_id='325'
              WHERE H.assesmen_id='".$assesmen_id."'
              GROUP BY H.jenis
              ORDER BY H.jenis";
              $list_header=$this->db->query($q)->result();
              $jml_kolom=count($list_header);
              $lebar_kolom=50 / $jml_kolom;
              foreach($list_header as $r){
              ?>
              <td width="<?=$lebar_kolom?>%" class="text-normal border-full text-center"><strong><?=$r->nama?></strong></td>
              
              <?}?>
            </tr>
            <?
            $q="SELECT H.jenis as id,M.ref as nama 
                FROM `tranap_assesmen_pulang_pengkajian` H 
                INNER JOIN merm_referensi M ON M.nilai=H.jenis AND M.ref_head_id='325'
                WHERE H.assesmen_id='".$assesmen_id."'
                GROUP BY H.jenis
                ORDER BY H.jenis";
              $list_header=$this->db->query($q)->result();
              $q="SELECT * 
              FROM `tranap_assesmen_pulang_pengkajian` H 
              WHERE H.assesmen_id='".$assesmen_id."'
              ";
              $isi=$this->db->query($q)->result_array();
              
              $q="SELECT * 
              FROM `tranap_assesmen_pulang_pengkajian` H 
              WHERE H.assesmen_id='".$assesmen_id."'
              GROUP BY H.param_id";
              $hasil=$this->db->query($q)->result();
              $opsi='';
              $no=0;
              $total=0;
              $nilai_max=0;
              $nilai_max_nama='';
              foreach($hasil as $r){
                $param_id=$r->param_id;
                  $trx_id=$r->id;
                $no=$no+1;
                $kolom_baru='';
                foreach ($list_header as $row){
                  $jenis_id=$row->id;
                  $kolom_baru .='
                  <td class="text-normal border-full text-center">
                    '.opsi_nilai_pulang_pdf($isi,$param_id,$jenis_id).'
                  </td>
                  ';
                }
              $opsi.='
                <tr>
                  <td class="text-normal border-full text-center">'.$no.'</td>
                  <td class="text-normal border-full text-left">'.$r->parameter_nama.'</td>
                  '.$kolom_baru.'
                  
                </tr>';

              }
            
            
            ?>
            <?=$opsi?>
        </table>
      </td></tr>
  </table>
  <table class="content">
		<tr>
			<td width="50%" class="text-left text-normal"><strong><?=strtoupper($hasil_ina)?></strong> / <i><?=strtoupper($hasil_eng)?></i></td>
			<td width="50%" class="text-left text-normal"><strong><?=strtoupper($rencana_ina)?></strong> / <i><?=strtoupper($rencana_eng)?></i></td>
		</tr>
	</table>
  <table class="content">
		<tr>
			<td style="width:50%;" class="text-normal">
          <table class="content">
              <tr>
                <td width="5%" class=" text-upnormal border-full text-center" style="background-color: #f2f2f2;">NO</td>
                <td width="15%" class=" text-upnormal border-full text-center" style="background-color: #f2f2f2;">JENIS PEMERIKSAAN</td>
                <td width="35%" class=" text-upnormal border-full text-center" style="background-color: #f2f2f2;">HASIL PEMERIKSAAN</td>
                <td width="20%" class=" text-upnormal border-full text-center" style="background-color: #f2f2f2;">TANGGAL PEMERIKSAAN</td>
                <td width="15%" class=" text-upnormal border-full text-center" style="background-color: #f2f2f2;">JUMLAH LEMBAR</td>
              </tr>
              <?
              $q="
                SELECT H.*,M.ref as jenis_nama
				FROM tranap_assesmen_pulang_hasil H
				LEFT JOIN merm_referensi M ON M.nilai=H.jenis_pemeriksaan AND M.ref_head_id='322'
				WHERE H.assesmen_id='".$assesmen_id."'
				AND H.staktif='1'	
				ORDER BY H.id ASC
              ";
               
            $no=1;
            $list_diagnosa=$this->db->query($q)->result();
            foreach($list_diagnosa as $row){
              ?>
              <tr>
                <td class=" text-normal border-full text-center"><?=$no?></td>
                <td class=" text-normal border-full text-left"><?=$row->jenis_nama?></td>
                <td class=" text-normal border-full text-left"><?=$row->hasil?></td>
                <td class=" text-normal border-full text-left"><?=HumanDateShort_exp($row->tanggal)?></td>
                <td class=" text-normal border-full text-left"><?=$row->jumlah_lembar?></td>
              </tr>
              <?
              $no++;
            }?>
          </table>
      </td>
      <td style="width:50%;" class="text-normal">
          <table class="content">
              <tr>
                <td width="5%" class=" text-upnormal border-full text-center" style="background-color: #f2f2f2;">No</td>
                <td width="20%" class=" text-upnormal border-full text-center" style="background-color: #f2f2f2;">TANGGAL</td>
                <td width="30%" class=" text-upnormal border-full text-center" style="background-color: #f2f2f2;">DOKTER</td>
                <td width="20%" class=" text-upnormal border-full text-center" style="background-color: #f2f2f2;">JAM PRAKTEK</td>
                <td width="25%" class=" text-upnormal border-full text-center" style="background-color: #f2f2f2;">KETERANGAN</td>
              </tr>
               <?
                $q="SELECT H.*,MP.nama as nama_poli,MD.nama as nama_dokter,DR.hari,HR.namahari 
				FROM tranap_assesmen_pulang_kontrol H
				LEFT JOIN mpoliklinik MP ON MP.id=H.idpoli
				LEFT JOIN mdokter MD ON MD.id=H.iddokter
				INNER JOIN date_row DR ON DR.tanggal=H.tanggal
				LEFT JOIN merm_hari HR ON HR.kodehari=DR.hari
				WHERE H.assesmen_id='".$assesmen_id."'
				AND H.staktif='1'	
				ORDER BY H.id ASC";
                  $no=1;
                  $list_diagnosa=$this->db->query($q)->result();
                  foreach($list_diagnosa as $row){
                    ?>
                    <tr>
                      <td  class=" text-normal border-full text-center"><?=$no?></td>
                      <td  class=" text-normal border-full text-left"><?=$row->namahari.', '.HumanDateShort_exp($row->tanggal)?></td>
                      <td  class=" text-normal border-full text-left"><?=($row->nama_poli.'<br>'.$row->nama_dokter)?></td>
                      <td  class=" text-normal border-full text-center"><?=($row->jam)?></td>
                      <td  class=" text-normal border-full text-center"><?=($row->keterangan)?></td>
                    </tr>
                    <?
                    $no++;
                  }?>
          </table>
      </td>
			
		</tr>
    
	</table>
  <table class="content">
		<tr>
			<td style="width:25%;" class="text-normal"><?=$skr_ina?> / <i><?=$skr_eng?></i></td>
			<td style="width:25%;" class="text-normal"><?=$asuransi_ina?> / <i><?=$asuransi_eng?></i></td>
			<td style="width:35%;" class="text-normal">&nbsp;</td>
			<td style="width:15%;" class="text-normal">&nbsp;</td>
		</tr>
    <tr>
			<td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($skr?get_nama_ref($skr,323):'&nbsp;')?></fieldset>
			</td>
			<td class="text-normal">
				<fieldset><legend style="font-weight:bold;"></legend><?=($asuransi?get_nama_ref($asuransi,324):'&nbsp;')?></fieldset>
			</td>
      <td class="text-normal">
			</td>
      <td class="text-normal">
			</td>
      
			
		</tr>
	</table>
	<table class="content">
	  <tr>
	  <td>
	<table class="content">
	  <tr>
		<td width="25" class="text-center "><strong>DISERAHKAN OLEH<br>PERAWAT PENANGGUNG JAWAB PASIEN</strong></td>
		<td width="40" class="text-center ">&nbsp;&nbsp;</td>
		<td width="35" class="text-center "><strong>DITERIMA OLEH<br>PASIEN / KELUARGA</strong></td>
	  </tr>
	  <tr>
		<td class="text-center">
		  <img src="<?= base_url(); ?>qrcode/qr_code_ttd/<?= $diserahkan; ?>" width="80px">
		</td>
		<td class="text-center"><?=$notes_ina?></td>
		<td class="text-center">
			 <img style="width:150px;height:90px" src="<?= $diterima_pasien_ttd; ?>" />
		</td>
	  </tr>
	  <tr>
		<td class="text-center">
		<strong>( <?=get_nama_ppa($diserahkan)?> )</strong> 
		
		</td>
		<td class="text-center"></td>
		<td class="text-center">
		  <strong>
			<?if ($diterima_pasien_nama){?>
			  (<?=($diterima_pasien_nama)?>)
			<?}?>
		  </strong>
		</td>
	  </tr>
	</table>
	</td>
	</tr>
	</table>
	<br>
	</main>
</body>

</html>
