
<div class="modal fade" id="modal_list_bayar" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:60%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">HISTORY PEMBAYARAN </h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12" style="margin-top: 5px;">
							<div class="table-responsive">
							 <table class="table table-bordered table-striped table-responsive" id="index_list_pembayaran">
								<thead>
									<tr>
										<th>#</th>
										<th>#</th>
										<th>TANGGAL BAYAR</th>
										<th>SISA AWAL</th>
										<th>BAYAR</th>
										<th>SISA</th>
										<th>METODE</th>                    
										<th>USER</th>
										<th>AKSI</th>
									</tr>
								</thead>
								<tbody> </tbody>
								  
							</table>
						</div>
						</div>
						
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
				</div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){
		
    });
	function get_pembayaran($id){
		var id=$id;
		var disabel=$("#disabel").val();
		// alert(tgl_trx);
		$('#index_list_pembayaran').DataTable().destroy();
		var table = $('#index_list_pembayaran').DataTable({
		"pageLength": 100,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tklaim_monitoring/get_pembayaran/',
			type: "POST",
			dataType: 'json',
			data: {
				id: id,disabel: disabel,
				
			}
		},
		columnDefs: [
					{"targets": [0], "visible": false },
					 {  className: "text-right", targets:[1,3,4,5] },
					 {  className: "text-center", targets:[2,8] },
					 { "width": "8%", "targets": [1] },
					 { "width": "10%", "targets": [2,3,4,5] },
					 { "width": "15%", "targets": [6,8] },
					 // { "width": "15%", "targets": [8] }

					]
		});
	}
	$(document).on("click", ".hapus_bayar", function() {
		var table = $('#index_list_pembayaran').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()
		swal({
			title: "Anda Yakin ?",
			text : "Akan Melakukan Penghapusan Pembayaran ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			hapus_bayar(id);
		});
		
		
	});
	function hapus_bayar($id){
		$("#cover-spin").show();
		var id=$id;
		$.ajax({
			url: '{site_url}tklaim_monitoring/hapus_bayar',
			type: 'POST',
			data: {
				id: id
				
				},
			complete: function() {
				$('#index_list_pembayaran').DataTable().ajax.reload( null, false );
				$('#index_list').DataTable().ajax.reload( null, false );
				$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Pembayaran'});
				$("#cover-spin").hide();
			}
		});
	}
</script>