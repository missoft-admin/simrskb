<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Tagihan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_klaim" placeholder="No Tagihan" name="no_klaim" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Kelompok Pasien</label>
                    <div class="col-md-8">
                        <select id="idkelompokpasien" name="idkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Kelompok Pasien -</option>
							<?foreach($list_kelompok_pasien as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Rekanan</label>
                    <div class="col-md-8">
                        <select id="idrekanan" name="idrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Perusahaan -</option>
							<?foreach($list_rekanan as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Status</label>
                    <div class="col-md-8">
						<select id="status_lunas" name="status_lunas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua Status -</option>
							<option value="0">Belum Lunas</option>
							<option value="1">Lunas All</option>
							<option value="2">Lunas Other Loss</option>							
							<option value="3">Perlu Follow Up Ulang</option>							
						</select>
					</div>
                </div>
				          
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Jenis Layanan</label>
                    <div class="col-md-8">
						<select id="tipe" name="tipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Layanan -</option>
							<option value="1" <?=("1" == $tipe ? 'selected' : ''); ?>>Rawat Jalan</option>
							<option value="2" <?=("2" == $tipe ? 'selected' : ''); ?>>Ranap / ODS</option>
							
						</select>
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Umur (bulan) <=</label>
                    <div class="col-md-8">
						<input type="text" class="form-control number" id="umur" placeholder="Umur" name="umur" value="">
					</div>
                </div>
				
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Jatuh Tempo</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
                            <input class="form-control" type="text" id="jatuh_tempo_bayar" name="jatuh_tempo_bayar" placeholder="From" value="{jatuh_tempo_bayar}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="jatuh_tempo_bayar2" name="jatuh_tempo_bayar2" placeholder="To" value="{jatuh_tempo_bayar2}"/>
                        </div>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Kirim</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
                            <input class="form-control" type="text" id="tgl_kirim" name="tgl_kirim" placeholder="From" value="{tgl_kirim}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tgl_kirim2" name="tgl_kirim2" placeholder="To" value="{tgl_kirim2}"/>
                        </div>
                    </div>
                </div>
               
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-4" hidden>
                        <button class="btn btn-danger text-uppercase" type="button" name="btn_verif_filter" id="btn_verif_filter" style="font-size:13px;width:100%;"><i class="fa fa-check-square-o"></i> Verifikasi By Filter</button>
                    </div>
					<div class="col-md-8" hidden>
                        <button disabled class="btn btn-primary text-uppercase" type="button" name="btn_verif_page" id="btn_verif_page" style="font-size:13px;width:100%;"><i class="fa fa-check"></i> Verifikasi Per page</button>
                    </div>
                </div>
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th hidden width="5%">TIPE</th>
                    <th hidden width="2%">ID</th>
                    <th width="10%">NO</th>
                    <th width="10%">NO TAGIHAN</th>
                    <th width="10%"> NAMA REKANAN</th>
                    <th width="10%">JENIS LAYANAN</th>
                    <th width="10%">JML FAKTUR</th>
                    <th width="10%">TOT TAGIHAN</th>
                    <th width="10%">SISA TAGIHAN</th>
                    <th width="10%">TANGGAL KIRIM</th>
                    <th width="10%">JATUH TEMPO PEMBAYARAN</th>
                    <th width="15%">UMUR / BULAN</th>
                    <th width="15%">STATUS</th>
                    <th width="15%">LAST FOLLOWUP</th>
                    <th width="15%">LAST KOREKSI</th>
                    <th width="15%" class="text-center">AKSI</th>
                    <th width="15%" class="text-center"></th>
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
</div>
<div class="modal fade" id="modal_fu" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:60%">
        <div class="modal-content" >
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary">
                    <h3 class="block-title">FOLLOW UP PIUTANG </h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">
							<label class="col-md-2 control-label" for="status">Nomor Tagihan</label>
							<div class="col-md-4">
								<input class="form-control" readonly type="text" id="fu_no_klaim" name="fu_no_klaim">
							</div>
							<label class="col-md-2 control-label" for="status">Tanggal Penagihan </label>
							<div class="col-md-4">
								<input class="form-control" readonly type="text" id="fu_tanggal_tagihan" name="fu_tanggal_tagihan">
							</div>
						</div> 
						<div class="col-md-12" style="margin-top: 5px;">
							<label class="col-md-2 control-label" for="status">Nama Rekanan</label>
							<div class="col-md-4">
								<input class="form-control" readonly type="text" id="fu_nama_rekanan" name="fu_nama_rekanan">
							</div>
							<label class="col-md-2 control-label" for="status">Tanggal Jatuh Tempo</label>
							<div class="col-md-4">
								<input class="form-control" readonly type="text" id="fu_tanggal_jatuh_tempo" name="fu_tanggal_jatuh_tempo">
							</div>
						</div>
						<div class="col-md-12" style="margin-top: 15px;">
							<h4><span class="label label-primary">KETERANGAN / INFORMASI </span></h4>
						</div>
						<div class="col-md-12" style="margin-top: 15px;">
							<label class="col-md-2 control-label" for="status">Tanggal Informasi</label>
							<div class="col-md-4">
								<div class="js-datepicker input-group date" data-date-format="dd-mm-yyyy">
									<input class="form-control" type="text" id="fu_tanggal_info" name="fu_tanggal_info" placeholder="Tanggal FU" value="<?=date('d-m-Y')?>"/>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
							<label class="col-md-2 control-label" for="status">Tanggal Reminder</label>
							<div class="col-md-4">
								<div class="js-datepicker input-group date" data-date-format="dd-mm-yyyy">
									<input class="form-control" type="text" id="fu_tanggal_reminder" name="fu_tanggal_reminder" placeholder="Tanggal Reminder" value=""/>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
						</div>
						
						<div class="col-md-12" style="margin-top: 5px;">	
							<label class="col-md-2 control-label" for="status">Keterangan</label>
								<div class="col-md-10">
										<textarea class="form-control js-summernote" id="keterangan"></textarea>
								</div>				
						</div>
						<div class="col-md-12" style="margin-top: 5px;">	
							<label class="col-md-2 control-label" for="status"></label>
								<div class="col-md-10">
									<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_tambah_fu" id="btn_tambah_fu"><i class="fa fa-plus"></i> Tambah</button>
							</div>				
						</div>
						
						<div class="col-md-12" style="margin-top: 5px;">
							<div class="block block-themed block-opt-hidden" id="div_kunjungan">
								<div class="block-header bg-primary">
									<ul class="block-options">
										<li>
											<button id="button_up_kunjungan" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-down"></i></button>
										</li>
									</ul>
									<h3 class="block-title"><i class="si si-login"></i>  Riwayat Follow Up</h3>
								</div>
								<div class="block-content block-content">
									<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
										<div class="table-responsive">
										<table class="table table-bordered table-striped" id="index_informasi">
											<thead>
												<tr>
													<th hidden width="2%">ID</th>
													<th width="10%">NO</th>
													<th width="10%">TANGGAL</th>
													<th width="10%">INFORMASI</th>
													<th width="10%">USER</th>
													<th width="10%">REMINDER</th>
													<th width="10%">AKSI</th>											
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
								</div>
									</form>
								</div>
							</div>
						</div>
						<div class="col-md-12" style="margin-top: 5px;">
							<div class="block block-themed block-opt-hidden" id="div_kunjungan">
								<div class="block-header bg-primary">
									<ul class="block-options">
										<li>
											<button id="button_up_kunjungan" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-down"></i></button>
										</li>
									</ul>
									<h3 class="block-title"><i class="fa fa-file-photo-o"></i>  Document</h3>
								</div>
								<div class="block-content block-content">
									<form action="{base_url}tklaim_monitoring/upload_bukti" enctype="multipart/form-data" class="dropzone" id="dropzone_semua">
										<input type="hidden" class="form-control" id="upload_klaim_id" placeholder="" name="upload_klaim_id" value="">
										<div>
										  <h5>DOCUMENT</h5>
										</div>
									</form>
									<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
										<div class="table-responsive">
										<table class="table table-bordered table-striped" id="index_dokumen">
											<thead>
												<tr>
													<th hidden width="2%">ID</th>
													<th width="10%">NO</th>
													<th width="10%">FILE</th>
													<th width="10%">Size</th>
													<th width="10%">AKSI</th>											
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
								</div>
									</form>
								</div>
							</div>
						</div>
						
						<div class="col-md-12" style="margin-top: 5px;">	
							<div class="block-content">
								
							</div>				
						</div>
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="fu_klaim_id" placeholder="No. PO" name="fu_klaim_id" value="">
								
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_bayar" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">PELUNASAN PIUTANG ALL</h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12" style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">NO TAGIHAN.</label>
							<div class="col-md-8">
								<input class="form-control" readonly type="text" id="xno_klaim" name="xno_klaim" value="">
							</div>
						</div>
						
						<div class="col-md-12" style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">Nama Rekanan</label>
							<div class="col-md-8">
								<input class="form-control" readonly type="text" id="xnama_rekanan" name="xnama_rekanan" value="">
							</div>
						</div>
						
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Tanggal Pembayaran</label>
								<div class="col-md-8">
									<div class="js-datepicker input-group date" data-date-format="dd-mm-yyyy">
									<input class="form-control" type="text" id="xtanggal_pembayaran" name="xtanggal_pembayaran" placeholder="Tanggal Pembayaran" value="<?=date('d-m-Y')?>"/>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Metode Pembayaran</label>
								<div class="col-md-8">
									<select id="metode_bayar" name="metode_bayar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#">- Pilih Pembayaran -</option>
										<option value="1" selected>TRANSFER<option>
										<option value="2">CASH<option>										
									</select>
								</div>
							</div>				
						</div>
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Bank</label>
								<div class="col-md-8">
									<select id="bankid" name="bankid" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#">- Pilih Bank -</option>
										<?foreach($list_bank as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama.' - '.$r->norekening?><option>
										<?}?>
									</select>
								</div>
							</div>				
						</div>
						<div class="col-md-12"  style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">Total Harus dibayar</label>
							<div class="col-md-8">
								<input class="form-control number" readonly type="text" id="harus_dibayar" name="harus_dibayar" value="">
							</div>
						</div>
						
						<div class="col-md-12"  style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">Total Bayar</label>
							<div class="col-md-8">
								<input class="form-control number" type="text" id="nominal_bayar" name="nominal_bayar" value="">
							</div>
						</div> 	
						<div class="col-md-12"  style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">Selisih</label>
							<div class="col-md-8">
								<input class="form-control number" readonly type="text" id="tidak_terbayar" name="tidak_terbayar" value="">
							</div>
						</div>
						<div class="col-md-12"  style="margin-top: 10px;">
								<textarea class="form-control js-summernote" id="keterangan"></textarea>
						</div>		
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="xid" placeholder="No. PO" name="xid" value="">
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_ubah" id="btn_ubah"><i class="fa fa-save"></i> SIMPAN</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
var myDropzone 
$(document).ready(function(){
	$('.js-summernote').summernote({
	  height: 50,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }
	});
	Dropzone.autoDiscover = false;
	myDropzone = new Dropzone(".dropzone", { 
	   autoProcessQueue: true,
	   maxFilesize: 30,
	});
	myDropzone.on("complete", function(file) {	  
	  myDropzone.removeFile(file);
	  $('#index_dokumen').DataTable().ajax.reload( null, false );
	});
	
	$(".number").number(true,0,'.',',');
	load_index();
   
});
function load_index(){
	var no_klaim=$("#no_klaim").val();
	var idkelompokpasien=$("#idkelompokpasien").val();
	var idrekanan=$("#idrekanan").val();
	var status_lunas=$("#status_lunas").val();
	var umur=$("#umur").val();
	// alert(status_lunas);
	var tipe=$("#tipe").val();
	var jatuh_tempo_bayar=$("#jatuh_tempo_bayar").val();
	var jatuh_tempo_bayar2=$("#jatuh_tempo_bayar2").val();
	var tgl_kirim=$("#tgl_kirim").val();
	var tgl_kirim2=$("#tgl_kirim2").val();
	var status_pengiriman=$("#status_pengiriman").val();
	$('#index_list').DataTable().destroy();
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: false,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0,1,16], "visible": false },
							{ "width": "3%", "targets": [2] },
							{ "width": "5%", "targets": [5,6,11] },
							{ "width": "7%", "targets": [7,8,9,10,12] },
							{ "width": "8%", "targets": [12,13,14] },
							{ "width": "10%", "targets": [15] },
						 {"targets": [4,13,15], className: "text-left" },
						 {"targets": [2,7,8], className: "text-right" },
						 {"targets": [3,5,6,9,10,11,12,13,14], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tklaim_monitoring/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						no_klaim:no_klaim,idkelompokpasien:idkelompokpasien,
						idrekanan:idrekanan,status:status,
						tipe:tipe,status_pengiriman:status_pengiriman,
						jatuh_tempo_bayar2:jatuh_tempo_bayar2,jatuh_tempo_bayar:jatuh_tempo_bayar,
						tgl_kirim:tgl_kirim,tgl_kirim2:tgl_kirim2,
						status_lunas:status_lunas,
						umur:umur,
					   }
            }
        });
}
function load_informasi(){
	var klaim_id=$("#fu_klaim_id").val();
	
	$('#index_informasi').DataTable().destroy();
	table = $('#index_informasi').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 10,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0], "visible": false },
							{ "width": "5%", "targets": [1] },
							{ "width": "10%", "targets": [2,5] },
							{ "width": "15%", "targets": [3,4,6] },
						 {"targets": [3], className: "text-left" },
						 {"targets": [1], className: "text-right" },
						 {"targets": [2,4,5,6], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tklaim_monitoring/load_informasi', 
                type: "POST" ,
                dataType: 'json',
				data : {
						klaim_id:klaim_id
					   }
            }
        });
}
function load_upload(){
	var klaim_id=$("#fu_klaim_id").val();
	
	$('#index_dokumen').DataTable().destroy();
	table = $('#index_dokumen').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 10,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0], "visible": false },
							{ "width": "5%", "targets": [1] },
							{ "width": "40%", "targets": [2] },
							{ "width": "10%", "targets": [3] },
							{ "width": "15%", "targets": [4] },
						 {"targets": [2], className: "text-left" },
						 {"targets": [1], className: "text-right" },
						 {"targets": [3,4], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tklaim_monitoring/load_upload', 
                type: "POST" ,
                dataType: 'json',
				data : {
						klaim_id:klaim_id
					   }
            }
        });
}
$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_index();
});
//lunas
$(document).on("click", ".followup", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	$("#upload_klaim_id").val(id);
	$("#fu_klaim_id").val(id);
	$("#fu_no_klaim").val(table.cell(tr,3).data());
	$("#fu_tanggal_tagihan").val(table.cell(tr,9).data());
	$("#fu_tanggal_jatuh_tempo").val(table.cell(tr,10).data());
	$("#fu_nama_rekanan").val(table.cell(tr,4).data());
	$("#keterangan").summernote('code','')
	$("#tanggal_reminder").val();
	load_informasi();
	load_upload();
	$("#modal_fu").modal('show');
});
$(document).on("click", "#btn_tambah_fu", function() {
	if ($("#fu_tanggal_reminder").val()==''){
		sweetAlert("Maaf...", "Tanggal Reminder belum dipilih!", "error");
		return false;
	}
	if ($("#fu_tanggal_info").val()==''){
		sweetAlert("Maaf...", "Tanggal Informasi belum dipilih!", "error");
		return false;
	}
	var klaim_id=$("#fu_klaim_id").val();
	var tanggal_info=$("#fu_tanggal_info").val();
	var tanggal_reminder=$("#fu_tanggal_reminder").val();
	var keterangan=$("#keterangan").summernote('code');
	$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tklaim_monitoring/tambah_fu',
			type: 'POST',
			data: {
				klaim_id: klaim_id,
				tanggal_info: tanggal_info,
				tanggal_reminder: tanggal_reminder,
				keterangan: keterangan,
				},
			complete: function() {
				 $('#index_list').DataTable().ajax.reload( null, false );
				$.toaster({priority : 'success', title : 'Succes!', message : ' Tambah Follow Up'});
				$("#modal_fu").modal('hide');
				$("#cover-spin").hide();
			}
		});
});
$(document).on("click",".hapus_file",function(){	
	var table = $('#index_dokumen').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,0).data()
	$.ajax({
		url: '{site_url}tklaim_monitoring/hapus_file',
		type: 'POST',
		data: {id: id},
		success : function(data) {				
			$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
			$('#index_dokumen').DataTable().ajax.reload( null, false );			
		}
	});
});
$(document).on("click",".fu_hapus",function(){	
	var table = $('#index_informasi').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,0).data()
	$.ajax({
		url: '{site_url}tklaim_monitoring/hapus_informasi',
		type: 'POST',
		data: {id: id},
		success : function(data) {				
			$.toaster({priority : 'success', title : 'Succes!', message : ' Follow up Berhasil Dihapus'});
			$('#index_informasi').DataTable().ajax.reload( null, false );			
		}
	});
});
$(document).on("keyup", "#nominal_bayar", function() {
		var selisih=0;
		var harus=$("#harus_dibayar").val();
		if (parseFloat($(this).val())>parseFloat(harus)){
			$(this).val(harus);
		}
		selisih=parseFloat(harus) - parseFloat($(this).val())
		$("#tidak_terbayar").val(selisih);
		
		if ($("#harus_dibayar").val() != $("#nominal_bayar").val()){
			$("#btn_ubah").attr('disabled',true);
		}else{
			$("#btn_ubah").attr('disabled',false);
		}
	});
$(document).on("click", ".lunas", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	$("#xid").val(id);
	$("#xno_klaim").val(table.cell(tr,3).data());
	$("#xnama_rekanan").val(table.cell(tr,4).data());
	$("#harus_dibayar").val(table.cell(tr,16).data());
	$("#tidak_terbayar").val(table.cell(tr,16).data());
	$("#modal_bayar").modal('show');
	$("#nominal_bayar").val(0);
	$("#bankid").val("#").trigger('change')
	// btn_ubah
	$("#btn_ubah").attr('disabled',true);
});
$(document).on("click", "#btn_ubah", function() {
	if ($("#xtanggal_pembayaran").val()=='' || $("#xtanggal_pembayaran").val()==null){
		sweetAlert("Maaf...", "Tanggal pembayaran belum dipilih!", "error");
		return false;
	}
	if ($("#harus_dibayar").val() != $("#nominal_bayar").val()){
		sweetAlert("Maaf...", "Nominal bayar Harus sama!", "error");
		return false;
	}
	// alert();
	if ($("#metode_bayar").val()=='#'){
		sweetAlert("Maaf...", "Motede Pembayaran Harus diisi!", "error");
		return false;
	}
	if (parseFloat($("#nominal_bayar").val())==0){
		sweetAlert("Maaf...", "Nominal Harus di isi!", "error");
		return false;
	}
	if ($("#bankid").val()=="#" && $("#metode_bayar").val()=="1"){
		sweetAlert("Maaf...", "Bank harus diisi!", "error");
		return false;
	}
	
	swal({
		title: "Anda Yakin ?",
		text : "Akan Melakukan Pembayaran ALL?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		update_pembayaran_all();
	});
	
});
function update_pembayaran_all(){
	$("#cover-spin").show();
	var id=$("#xid").val();
	var klaim_id=$("#klaim_id").val();
	var tanggal_pembayaran=$("#xtanggal_pembayaran").val();
	var metode_bayar=$("#metode_bayar").val();
	var bankid=$("#bankid").val();
	var nominal_bayar=$("#nominal_bayar").val();
	var tidak_terbayar=$("#tidak_terbayar").val();
	var keterangan=$("#keterangan").summernote('code');
	$.ajax({
		url: '{site_url}tklaim_monitoring/update_pembayaran_all',
		type: 'POST',
		data: {
			id: id,
			tanggal_pembayaran: tanggal_pembayaran,
			metode_bayar: metode_bayar,
			bankid: bankid,
			nominal_bayar: nominal_bayar,
			tidak_terbayar: tidak_terbayar,
			keterangan: keterangan,
			klaim_id: klaim_id,
			},
		complete: function() {
			$('#index_list').DataTable().ajax.reload( null, false );
			$.toaster({priority : 'success', title : 'Succes!', message : ' Pembayaran'});
			$("#modal_bayar").modal('hide');
			$("#cover-spin").hide();
		}
	});
}

</script>