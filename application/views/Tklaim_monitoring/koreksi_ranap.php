<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work-1"'); ?>
                <div class="col-md-6"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Nomor Tagihan</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{no_klaim}" disabled>
                            <input class="form-control" type="hidden" name="klaim_id" id="klaim_id" value="{id}" >
							<input class="form-control" type="hidden" name="idkelompokpasien" id="idkelompokpasien" value="{idkelompokpasien}" >
                            <input class="form-control" type="hidden" name="idrekanan" id="idrekanan" value="{idrekanan}" >
                            <input class="form-control" type="hidden" name="disabel" id="disabel" value="{disabel}" >
                        </div> 
                    </div>
                    <div class="form-group" style="margin-bottom: 15px;"> 
                        <label class="col-md-3 control-label">Nama Rekanan</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{rekanan_nama}" disabled>
                        </div> 
                    </div>
                                      
                </div>
				<div class="col-md-6" style="margin-bottom: 5px;"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Tanggal Penagihan</label> 
                        <div class="col-md-4"> 
                            <input class="form-control" value="<?=HumanDateShort($tanggal_tagihan)?>" disabled>
                        </div> 
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Tanggal Jatuh Tempo</label> 
                        <div class="col-md-4"> 
                            <input class="form-control" value="<?=HumanDateShort($jatuh_tempo_bayar)?>" disabled>
                        </div> 
                    </div>
                    
                    
                </div>
            <?php echo form_close(); ?>
        </div>
        
        <div class="form-group" style="margin-top: 20px;"></div>
    </div>
</div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">DETAIL TAGIHAN</h3>
    </div>
    <div class="block-content">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Registrasi</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_reg" placeholder="No Registrasi" name="no_reg" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Medrec</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_medrec" placeholder="No. Medrec" name="no_medrec" value="">
                    </div>
                </div>
				
                
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="nama_pasien" placeholder="Nama Pasien" name="nama_pasien" value="">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Daftar</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tgl_trx" name="tgl_trx" placeholder="From" value=""/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tgl_trx2" name="tgl_trx2" placeholder="To" value=""/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
			</div>
            <?php echo form_close(); ?>
        </div>
        <div class="form-group" style="margin-bottom: 30px;"></div>
		<div class="table-responsive">
        <table class="table table-bordered table-striped table-responsive" id="index_list">
            <thead>
                <tr>
                    <th>#</th>
                    <th>#</th>
                    <th>#</th>
                    <th>TANGGAL</th>
                    <th>NO. REG</th>
                    <th>MEDREC</th>
                    <th>PASIEN</th>
                    <th>KELAS</th>
                    <th>RAWAT INAP</th>                    
                    <th>TAGIHAN</th>
                    <th>PEMBAYARAN</th>
                    <th>STATUS</th>
                    <th>AKSI</th>
                    <th></th>
                </tr>
            </thead>
            <tbody> </tbody>
              
        </table>
    </div>
        <div class="form-group" style="margin-top: 20px;"></div>
    </div>
</div>
<div class="modal fade" id="modal_bayar" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">PELUNASAN PIUTANG </h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12" style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">NO REG.</label>
							<div class="col-md-8">
								<input class="form-control" readonly type="text" id="xno_klaim" name="xno_klaim" value="<?=$no_klaim?>">
							</div>
						</div>
						<div class="col-md-12" style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">No. Medrec</label>
							<div class="col-md-8">
								<input class="form-control" readonly type="text" id="xno_medrec" name="xno_medrec" value="">
							</div>
						</div>
						<div class="col-md-12" style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">Pasien</label>
							<div class="col-md-8">
								<input class="form-control" readonly type="text" id="xnama_pasien" name="xnama_pasien" value="">
							</div>
						</div>
						<div class="col-md-12" style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">Nama Rekanan</label>
							<div class="col-md-8">
								<input class="form-control" readonly type="text" id="xnama_rekanan" name="xnama_rekanan" value="{rekanan_nama}">
							</div>
						</div>
						
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Tanggal Pembayaran</label>
								<div class="col-md-8">
									<div class="js-datepicker input-group date" data-date-format="dd-mm-yyyy">
									<input class="form-control" type="text" id="xtanggal_pembayaran" name="xtanggal_pembayaran" placeholder="Tanggal Pembayaran" value="<?=date('d-m-Y')?>"/>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Metode Pembayaran</label>
								<div class="col-md-8">
									<select id="metode_bayar" name="metode_bayar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#">- Pilih Pembayaran -</option>
										<option value="1" selected>TRANSFER<option>
										<option value="2">CASH<option>										
									</select>
								</div>
							</div>				
						</div>
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Bank</label>
								<div class="col-md-8">
									<select id="bankid" name="bankid" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#">- Pilih Bank -</option>
										<?foreach($list_bank as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama.' - '.$r->norekening?><option>
										<?}?>
									</select>
								</div>
							</div>				
						</div>
						<div class="col-md-12"  style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">Sisa Total Harus dibayar</label>
							<div class="col-md-8">
								<input class="form-control decimal" readonly type="text" id="harus_dibayar" name="harus_dibayar" value="">
							</div>
						</div>
						
						<div class="col-md-12"  style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">Total Bayar</label>
							<div class="col-md-8">
								<input class="form-control decimal" type="text" id="nominal_bayar" name="nominal_bayar" value="">
							</div>
						</div> 	
						<div class="col-md-12"  style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">Selisih</label>
							<div class="col-md-8">
								<input class="form-control decimal" readonly type="text" id="tidak_terbayar" name="tidak_terbayar" value="">
							</div>
						</div>
						<div class="col-md-12"  style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">Other Income</label>
							<div class="col-md-8">
								<input class="form-control decimal" readonly type="text" id="other_income" name="other_income" value="">
							</div>
						</div>
						<div class="col-md-12"  style="margin-top: 10px;">
								<textarea class="form-control js-summernote" id="keterangan"></textarea>
						</div>		
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="xid" placeholder="No. PO" name="xid" value="">
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_ubah" id="btn_ubah"><i class="fa fa-save"></i> SIMPAN</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<?
	$this->load->view('Tklaim_monitoring/modal_list');
?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">


    var table;

    $(document).ready(function(){
		$(".number").number(true,0,'.',',');
		$(".decimal").number(true,2,'.',',');
		$('.js-summernote').summernote({
		  height: 50,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }
		});
		load_rajal();
	
    });
	
    function load_rajal() {
		var klaim_id=$("#klaim_id").val();
		var no_reg=$("#no_reg").val();
		var no_medrec=$("#no_medrec").val();
		var nama_pasien=$("#nama_pasien").val();
		var tgl_trx=$("#tgl_trx").val();
		var tgl_trx2=$("#tgl_trx2").val();
		var disabel=$("#disabel").val();
		// alert(tgl_trx);
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 100,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tklaim_monitoring/load_koreksi_ranap/',
			type: "POST",
			dataType: 'json',
			data: {
				klaim_id: klaim_id,no_reg: no_reg,
				no_medrec: no_medrec,nama_pasien: nama_pasien,
				tgl_trx:tgl_trx,tgl_trx2:tgl_trx2,disabel:disabel,
			}
		},
		columnDefs: [
					{"targets": [0,1,13], "visible": false },
					 {  className: "text-right", targets:[2,9,10] },
					 {  className: "text-center", targets:[4,5,11] },
					 {  className: "text-left", targets:[6,7] },
					 { "width": "3%", "targets": [2] },
					 { "width": "8%", "targets": [3,4,5,9,10] },
					 { "width": "15%", "targets": [6,12] },
					 // { "width": "15%", "targets": [8] }

					]
		});
	}
	
	$("#btn_filter").click(function() {
		load_rajal();
	});
	$(document).on("click", ".lunas", function() {
		var table = $('#index_list').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()
		$("#xid").val(id);
		$("#xno_medrec").val(table.cell(tr,5).data());
		$("#xnama_pasien").val(table.cell(tr,6).data());
		$("#harus_dibayar").val(table.cell(tr,13).data());
		$("#nominal_bayar").val(table.cell(tr,13).data());
		$("#tidak_terbayar").val(0);
		$("#bankid").val("#").trigger('change')
		$("#keterangan").summernote('code','');
		$('#modal_bayar').modal('show');
		
	});
	$(document).on("click", ".list_bayar", function() {
		$("#modal_list_bayar").modal('show');
		var table = $('#index_list').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()
		get_pembayaran(id);
	});
	$(document).on("click", ".selesai", function() {
		var table = $('#index_list').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()
		swal({
			title: "Anda Yakin ?",
			text : "Akan Melakukan Set Other Loss ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			update_other_loss(id);
		});
		
		
	});
	// $(document).on("keyup", "#nominal_bayar", function() {
		// var selisih=0;
		// var harus=$("#harus_dibayar").val();
		// if (parseFloat($(this).val()) > parseFloat(harus)){
			// $(this).val(harus);
		// }
		// selisih=parseFloat(harus) - parseFloat($(this).val())
		// $("#tidak_terbayar").val(selisih);
		
		
	// });
	$(document).on("keyup", "#nominal_bayar", function() {
		var selisih=0;
		var harus=$("#harus_dibayar").val();
		if (parseFloat($(this).val()) > parseFloat(harus)){
			// $(this).val(harus);
		}
		selisih=parseFloat(harus) - parseFloat($(this).val())
		if (selisih < 0){
			$("#tidak_terbayar").val(0);
			$("#other_income").val(selisih*(-1));
			
		}else{
			$("#tidak_terbayar").val(selisih);			
			$("#other_income").val(0);
		}
		// $("#other_income").val(selisih);
		
		
	});
	
	$(document).on("click", "#btn_ubah", function() {
		if ($("#xtanggal_pembayaran").val()=='' || $("#xtanggal_pembayaran").val()==null){
			sweetAlert("Maaf...", "Tanggal pembayaran belum dipilih!", "error");
			return false;
		}
		// alert();
		if ($("#metode_bayar").val()=='#'){
			sweetAlert("Maaf...", "Motede Pembayaran Harus diisi!", "error");
			return false;
		}
		if (parseFloat($("#nominal_bayar").val())==0){
			sweetAlert("Maaf...", "Nominal Harus di isi!", "error");
			return false;
		}
		if ($("#bankid").val()=="#" && $("#metode_bayar").val()=="1"){
			sweetAlert("Maaf...", "Bank harus diisi!", "error");
			return false;
		}
		
		swal({
			title: "Anda Yakin ?",
			text : "Akan Melakukan Pembayaran ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			update_pembayaran();
		});
		
	});
	function update_pembayaran(){
		$("#cover-spin").show();
		var id=$("#xid").val();
		var klaim_id=$("#klaim_id").val();
		var tanggal_pembayaran=$("#xtanggal_pembayaran").val();
		var metode_bayar=$("#metode_bayar").val();
		var bankid=$("#bankid").val();
		var nominal_bayar=$("#nominal_bayar").val();
		var tidak_terbayar=$("#tidak_terbayar").val();
		var harus_dibayar=$("#harus_dibayar").val();
		var other_income=$("#other_income").val();
		var keterangan=$("#keterangan").summernote('code');
		alert(other_income);
		$.ajax({
			url: '{site_url}tklaim_monitoring/update_pembayaran',
			type: 'POST',
			data: {
				id: id,
				tanggal_pembayaran: tanggal_pembayaran,
				metode_bayar: metode_bayar,
				bankid: bankid,
				harus_dibayar: harus_dibayar,
				other_income: other_income,
				nominal_bayar: nominal_bayar,
				tidak_terbayar: tidak_terbayar,
				keterangan: keterangan,
				klaim_id: klaim_id,
				},
			complete: function() {
				$('#index_list').DataTable().ajax.reload( null, false );
				$.toaster({priority : 'success', title : 'Succes!', message : ' Pembayaran'});
				$("#modal_bayar").modal('hide');
				$("#cover-spin").hide();
			}
		});
	}
	function update_other_loss($id){
		$("#cover-spin").show();
		var id=$id;
		var klaim_id=$("#klaim_id").val();
		$.ajax({
			url: '{site_url}tklaim_monitoring/update_other_loss',
			type: 'POST',
			data: {
				id: id,klaim_id: klaim_id,
				
				},
			complete: function() {
				$('#index_list').DataTable().ajax.reload( null, false );
				$.toaster({priority : 'success', title : 'Succes!', message : ' Set Ogher Loss'});
				$("#cover-spin").hide();
			}
		});
	}
	
</script>