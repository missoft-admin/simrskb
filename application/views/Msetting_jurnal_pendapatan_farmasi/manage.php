<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content" style="margin-top: -20px;font-size:">
		<?php echo form_open('msetting_jurnal_pendapatan_farmasi/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>	
		<?
		$q="SELECT *FROM mdata_tipebarang WHERE id IN(1,2,3)";
		$list_tipe=$this->db->query($q)->result();
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="block block-themed  block-opt-hidden">
					<div class="block-header bg-primary">
						<ul class="block-options">

							<li>
								<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">SETTING AKUN PENJUALAN FARMASI NON RACIKAN</h3>
					</div>
					<div class="block-content">						
						<input type="hidden" class="form-control" id="id_edit_nr" placeholder="0" name="id_edit_nr" value="">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Jenis Kunjungan</label>
							
							<div class="col-md-4">
								<select name="asalrujukan_nr" id="asalrujukan_nr" style="width: 100%" id="step" class="js-select2 form-control input-sm">										
									<option value="#" selected>- Pilih Jenis Kunjungan -</option>																			
									<option value="0">Non Rujukan</option>																			
									<option value="1">Poliklinik</option>																			
									<option value="2">IGD</option>																			
									<option value="3">Rawat Inap</option>
								</select>			
							</div>
							<label class="col-md-2 control-label" >Kelas / Poli / Tipe</label>
							<div class="col-md-4">
								<select name="idpoli_kelas_nr" id="idpoli_kelas_nr" style="width: 100%" class="js-select2 form-control input-sm"></select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Tipe Barang</label>							
							<div class="col-md-4">
								<select name="idtipe_nr" id="idtipe_nr" style="width: 100%" class="js-select2 form-control input-sm idbarang">
									<option value="#" selected>- Pilih Tipe -</option>		
									<?foreach($list_tipe as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama_tipe?></option>																				
									<?}?>
								</select>	
							</div>
							<label class="col-md-2 control-label" >Group Penjualan</label>
							<div class="col-md-4">
								<select name="idgroup_penjualan_nr" id="idgroup_penjualan_nr" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Kategori</label>
							
							<div class="col-md-4">
								<select name="idkategori_nr" id="idkategori_nr" style="width: 100%" class="js-select2 form-control input-sm idkategori"></select>
							</div>
							<label class="col-md-2 control-label" >Group Diskon</label>
							<div class="col-md-4">
								<select name="idgroup_diskon_nr" id="idgroup_diskon_nr" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" >Barang</label>
							
							<div class="col-md-4">
								<select name="idbarang_nr" id="idbarang_nr" style="width: 100%" class="js-select2 form-control input-sm"></select>
							</div>
							<label class="col-md-2 control-label" >Group Tuslah</label>
							<div class="col-md-4">
								<select name="idgroup_tuslah_nr" id="idgroup_tuslah_nr" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" ></label>
							
							<div class="col-md-4">
								<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_nr" title="Masukan Item"><i class="fa fa-save"></i> Simpan</button>
								<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_nr" title="Batalkan"><i class="fa fa-refresh"></i> Clear Input</button>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<table id="tabel_nr" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 0%;">#</th>															
											<th style="width: 8%;">Jenis Kunjungan</th>															
											<th style="width: 8%;">Kelas / Poli</th>															
											<th style="width: 8%;">Tipe</th>															
											<th style="width: 9%;">Kategori</th>															
											<th style="width: 11%;">Barang</th>															
											<th style="width: 12%;">Group Pembelian</th>															
											<th style="width: 12%;">Group Diskon</th>															
											<th style="width: 12%;">Group Tuslah</th>															
											<th style="width: 5%;">Actions</th>
										</tr>
										
									</thead>
									<tbody>
										
									</tbody>
									
							</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-md-12">
				<div class="block block-themed  block-opt-hidden">
					<div class="block-header bg-success">
						<ul class="block-options">

							<li>
								<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">SETTING AKUN PENJUALAN FARMASI RACIKAN</h3>
					</div>
					<div class="block-content">						
						<input type="hidden" class="form-control" id="id_edit_r" placeholder="0" name="id_edit_r" value="">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Jenis Kunjungan</label>
							
							<div class="col-md-4">
								<select name="asalrujukan_r" id="asalrujukan_r" style="width: 100%" id="step" class="js-select2 form-control input-sm">										
									<option value="#" selected>- Pilih Jenis Kunjungan -</option>																			
									<option value="0">Non Rujukan</option>																			
									<option value="1">Poliklinik</option>																			
									<option value="2">IGD</option>																			
									<option value="3">Rawat Inap</option>
								</select>			
							</div>
							<label class="col-md-2 control-label" >Kelas / Poli / Tipe</label>
							<div class="col-md-4">
								<select name="idpoli_kelas_r" id="idpoli_kelas_r" style="width: 100%" class="js-select2 form-control input-sm"></select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Tipe Barang</label>							
							<div class="col-md-4">
								<select name="idtipe_r" id="idtipe_r" style="width: 100%" class="js-select2 form-control input-sm idbarang">
									<option value="#" selected>- Pilih Tipe -</option>		
									<?foreach($list_tipe as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama_tipe?></option>																				
									<?}?>
								</select>	
							</div>
							<label class="col-md-2 control-label" >Group Penjualan</label>
							<div class="col-md-4">
								<select name="idgroup_penjualan_r" id="idgroup_penjualan_r" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Kategori</label>
							
							<div class="col-md-4">
								<select name="idkategori_r" id="idkategori_r" style="width: 100%" class="js-select2 form-control input-sm idkategori"></select>
							</div>
							<label class="col-md-2 control-label" >Group Diskon</label>
							<div class="col-md-4">
								<select name="idgroup_diskon_r" id="idgroup_diskon_r" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" >Barang</label>
							
							<div class="col-md-4">
								<select name="idbarang_r" id="idbarang_r" style="width: 100%" class="js-select2 form-control input-sm"></select>
							</div>
							<label class="col-md-2 control-label" ></label>
							<div class="col-md-4" hidden>
								<select name="idgroup_tuslah_r" id="idgroup_tuslah_r" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" ></label>
							
							<div class="col-md-4">
								<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_r" title="Masukan Item"><i class="fa fa-save"></i> Simpan</button>
								<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_r" title="Batalkan"><i class="fa fa-refresh"></i> Clear Input</button>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<table id="tabel_r" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 0%;">#</th>															
											<th style="width: 8%;">Jenis Kunjungan</th>															
											<th style="width: 8%;">Kelas / Poli</th>															
											<th style="width: 8%;">Tipe</th>															
											<th style="width: 9%;">Kategori</th>															
											<th style="width: 11%;">Barang</th>															
											<th style="width: 12%;">Group Pembelian</th>															
											<th style="width: 12%;">Group Diskon</th>															
											<th style="width: 5%;">Actions</th>
										</tr>
										
									</thead>
									<tbody>
										
									</tbody>
									
							</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
			
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		list_group();
		list_poli_kelas('#');
		list_kategori_nr('#');
		clear_input_nr();
		list_barang_nr();
		load_nr();
		
		list_kategori_r('#');
		clear_input_r();
		list_barang_r();
		load_r();
	});	
	function list_group(){
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_general/list_akun_group',
			dataType: "json",
			success: function(data) {
				$(".idgroup").empty();
				$('.idgroup').append('<option value="#" selected>- Pilih Group -</option>');
				$('.idgroup').append(data.detail);
				
			}
		});		
	}
	
	//Non Racikan
	$(document).on("change","#asalrujukan_nr",function(){
		list_poli_kelas($(this).val());
	});
	function list_poli_kelas($id){
		if ($id !='#'){			
			$.ajax({
				url: '{site_url}msetting_jurnal_pendapatan_farmasi/list_poli_kelas/'+$id,
				dataType: "json",
				success: function(data) {
					$("#idpoli_kelas_nr").empty();
					$('#idpoli_kelas_nr').append('<option value="0" selected>- Semua -</option>');
					$('#idpoli_kelas_nr').append(data.detail);
				}
			});
		}else{
			$("#idpoli_kelas_nr").empty();
			$('#idpoli_kelas_nr').append('<option value="0" selected>- Semua -</option>');
		}
		
	}
	
	$(document).on("change","#idtipe_nr",function(){
		list_kategori_nr($(this).val());
		list_barang_nr();
	});
	$(document).on("change","#idkategori_nr",function(){
		list_barang_nr();
	});
	function list_kategori_nr($id){
		if ($id!='#'){
			$.ajax({
				url: '{site_url}msetting_jurnal_pembelian/list_kategori/'+$id,
				dataType: "json",
				success: function(data) {
					$("#idkategori_nr").empty();
					$('#idkategori_nr').append('<option value="0" selected>- Semua Kategori -</option>');
					$('#idkategori_nr').append(data.detail);
				}
			});
		}else{
			$("#idkategori_nr").empty();
			$('#idkategori_nr').append('<option value="0" selected>- Semua Kategori -</option>');
		}		
	}
	function list_barang_nr(){
		var newOption = new Option('- Semua Barang -', '0', true, true);					
		$("#idbarang_nr").append(newOption);
		$("#idbarang_nr").val("0").trigger('change');
		
		var idtipe=$("#idtipe_nr").val();
		var idkategori=$("#idkategori_nr").val();
		$("#idbarang_nr").select2({
			minimumInputLength: 0,
			noResults: 'Obat Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}msetting_jurnal_pembelian/list_barang/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,idtipe: idtipe,idkategori:idkategori,
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.kode+' - '+item.nama,
								id: item.id,
								idtipe : item.idtipe
							}
						})
					};
				}
			}
		});

	}
	
	function validate_add_nr()
	{
			var idtipe_nr=$("#idtipe_nr").val();
			var asalrujukan_nr=$("#asalrujukan_nr").val();
			var idgroup_penjualan_nr=$("#idgroup_penjualan_nr").val();
			var idgroup_diskon_nr=$("#idgroup_diskon_nr").val();
			var idgroup_tuslah_nr=$("#idgroup_tuslah_nr").val();
		
			if (asalrujukan_nr=='#'){
				sweetAlert("Maaf...", "Jenis Kunjungan Harus diisi", "error");
				return false;
			}
			if (idtipe_nr=='#'){
				sweetAlert("Maaf...", "Tipe Barang Harus diisi", "error");
				return false;
			}
			
			
			if (idgroup_penjualan_nr=='#'){
				sweetAlert("Maaf...", "Group Penjualan Harus diisi", "error");
				return false;
			}
			if (idgroup_diskon_nr=='#'){
				sweetAlert("Maaf...", "Group Diskon Harus diisi", "error");
				return false;
			}
			if (idgroup_tuslah_nr=='#'){
				sweetAlert("Maaf...", "Group Tuslah Harus diisi", "error");
				return false;
			}
			
		return true;
	}
	function clear_input_nr(){
		$("#idtipe_nr").val('#').trigger('change');	
		$("#asalrujukan_nr").val('#').trigger('change');	
		
		$("#idgroup_penjualan_nr").val('#').trigger('change');
		$("#idgroup_diskon_nr").val('#').trigger('change');
		$("#idgroup_tuslah_nr").val('#').trigger('change');
		
		$("#idkategori_nr").val('0').trigger('change');
		$("#idpoli_kelas_nr").val('0').trigger('change');
		$("#idbarang_nr").val('0').trigger('change');
	}
	$(document).on("click","#simpan_nr",function(){
		if (validate_add_nr()==false)return false;
		var asalrujukan=$("#asalrujukan_nr").val();
		var idpoli_kelas=$("#idpoli_kelas_nr").val();
		var idtipe=$("#idtipe_nr").val();
		var idkategori=$("#idkategori_nr").val();
		var idbarang=$("#idbarang_nr").val();
		var idgroup_penjualan=$("#idgroup_penjualan_nr").val();
		var idgroup_diskon=$("#idgroup_diskon_nr").val();
		var idgroup_tuslah=$("#idgroup_tuslah_nr").val();
	
		
		// alert('SINI');
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_farmasi/simpan_nr',
			type: 'POST',
			data: {
				asalrujukan:asalrujukan
				,idpoli_kelas:idpoli_kelas
				,idtipe:idtipe
				,idkategori:idkategori
				,idbarang:idbarang
				,idgroup_penjualan:idgroup_penjualan
				,idgroup_diskon:idgroup_diskon
				,idgroup_tuslah:idgroup_tuslah
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_nr').DataTable().ajax.reload( null, false );
					clear_input_nr();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	function hapus_nr($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_pendapatan_farmasi/hapus_nr/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_nr').DataTable().ajax.reload( null, false );
						clear_input_nr();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_nr",function(){		
		clear_input_nr();
	});
	function load_nr(){
		var idlogic=$("#id").val();
		
		$('#tabel_nr').DataTable().destroy();
		var table = $('#tabel_nr').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pendapatan_farmasi/load_nr/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
					]
		});
	}
	
	// Racikan
	$(document).on("change","#asalrujukan_r",function(){
		list_poli_kelas_r($(this).val());
	});
	function list_poli_kelas_r($id){
		if ($id !='#'){			
			$.ajax({
				url: '{site_url}msetting_jurnal_pendapatan_farmasi/list_poli_kelas/'+$id,
				dataType: "json",
				success: function(data) {
					$("#idpoli_kelas_r").empty();
					$('#idpoli_kelas_r').append('<option value="0" selected>- Semua -</option>');
					$('#idpoli_kelas_r').append(data.detail);
				}
			});
		}else{
			$("#idpoli_kelas_r").empty();
			$('#idpoli_kelas_r').append('<option value="0" selected>- Semua -</option>');
		}
		
	}
	
	$(document).on("change","#idtipe_r",function(){
		list_kategori_r($(this).val());
		list_barang_r();
	});
	$(document).on("change","#idkategori_r",function(){
		list_barang_r();
	});
	function list_kategori_r($id){
		if ($id!='#'){
			$.ajax({
				url: '{site_url}msetting_jurnal_pembelian/list_kategori/'+$id,
				dataType: "json",
				success: function(data) {
					$("#idkategori_r").empty();
					$('#idkategori_r').append('<option value="0" selected>- Semua Kategori -</option>');
					$('#idkategori_r').append(data.detail);
				}
			});
		}else{
			$("#idkategori_r").empty();
			$('#idkategori_r').append('<option value="0" selected>- Semua Kategori -</option>');
		}		
	}
	function list_barang_r(){
		var newOption = new Option('- Semua Barang -', '0', true, true);					
		$("#idbarang_r").append(newOption);
		$("#idbarang_r").val("0").trigger('change');
		
		var idtipe=$("#idtipe_r").val();
		var idkategori=$("#idkategori_r").val();
		$("#idbarang_r").select2({
			minimumInputLength: 0,
			noResults: 'Obat Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}msetting_jurnal_pembelian/list_barang/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,idtipe: idtipe,idkategori:idkategori,
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.kode+' - '+item.nama,
								id: item.id,
								idtipe : item.idtipe
							}
						})
					};
				}
			}
		});

	}
	
	function validate_add_r()
	{
			var idtipe_r=$("#idtipe_r").val();
			var asalrujukan_r=$("#asalrujukan_r").val();
			var idgroup_penjualan_r=$("#idgroup_penjualan_r").val();
			var idgroup_diskon_r=$("#idgroup_diskon_r").val();
			var idgroup_tuslah_r=$("#idgroup_tuslah_r").val();
		
			if (asalrujukan_r=='#'){
				sweetAlert("Maaf...", "Jenis Kunjungan Harus diisi", "error");
				return false;
			}
			if (idtipe_r=='#'){
				sweetAlert("Maaf...", "Tipe Barang Harus diisi", "error");
				return false;
			}
			
			
			if (idgroup_penjualan_r=='#'){
				sweetAlert("Maaf...", "Group Penjualan Harus diisi", "error");
				return false;
			}
			if (idgroup_diskon_r=='#'){
				sweetAlert("Maaf...", "Group Diskon Harus diisi", "error");
				return false;
			}
			// if (idgroup_tuslah_r=='#'){
				// sweetAlert("Maaf...", "Group Tuslah Harus diisi", "error");
				// return false;
			// }
			
		return true;
	}
	function clear_input_r(){
		$("#idtipe_r").val('#').trigger('change');	
		$("#asalrujukan_r").val('#').trigger('change');	
		
		$("#idgroup_penjualan_r").val('#').trigger('change');
		$("#idgroup_diskon_r").val('#').trigger('change');
		// $("#idgroup_tuslah_r").val('#').trigger('change');
		
		$("#idkategori_r").val('0').trigger('change');
		$("#idpoli_kelas_r").val('0').trigger('change');
		$("#idbarang_r").val('0').trigger('change');
	}
	$(document).on("click","#simpan_r",function(){
		if (validate_add_r()==false)return false;
		var asalrujukan=$("#asalrujukan_r").val();
		var idpoli_kelas=$("#idpoli_kelas_r").val();
		var idtipe=$("#idtipe_r").val();
		var idkategori=$("#idkategori_r").val();
		var idbarang=$("#idbarang_r").val();
		var idgroup_penjualan=$("#idgroup_penjualan_r").val();
		var idgroup_diskon=$("#idgroup_diskon_r").val();
		// var idgroup_tuslah=$("#idgroup_tuslah_r").val();
	
		
		// alert('SINI');
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_farmasi/simpan_r',
			type: 'POST',
			data: {
				asalrujukan:asalrujukan
				,idpoli_kelas:idpoli_kelas
				,idtipe:idtipe
				,idkategori:idkategori
				,idbarang:idbarang
				,idgroup_penjualan:idgroup_penjualan
				,idgroup_diskon:idgroup_diskon
				// ,idgroup_tuslah:idgroup_tuslah
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_r').DataTable().ajax.reload( null, false );
					clear_input_r();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	function hapus_r($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_pendapatan_farmasi/hapus_r/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_r').DataTable().ajax.reload( null, false );
						clear_input_r();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_r",function(){		
		clear_input_r();
	});
	function load_r(){
		var idlogic=$("#id").val();
		
		$('#tabel_r').DataTable().destroy();
		var table = $('#tabel_r').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pendapatan_farmasi/load_r/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
					]
		});
	}
	
	
</script>
