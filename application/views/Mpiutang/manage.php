<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mpiutang" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mpiutang/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama Pengaturan</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="{id}" required="">
				</div>
			</div>
			
			
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mpiutang" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<hr>
			<?if ($id !=''){?>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="Lokasi Tubuh"><?=text_primary('Tanggal')?></label>
				<div class="col-md-9">
					<table id="tabel_tanggal" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<input type="hidden" class="form-control" id="tedit" placeholder="tedit" name="tedit" value="0" required="">
							<tr>
								<th style="width: 40%;">Deskripsi</th>
								<th style="width: 30%;">Tanggal</th>								
								<th style="width: 20%;">Actions</th>
								<th style="width: 20%;">id</th>
							</tr>
							<tr>
								
								<td>
									<input type="text" class="form-control" id="deskripsi" placeholder="Deskripsi" name="deskripsi" value=""  style="width: 100%">								
								</td>
								<td>
									<select name="tanggal_hari" tabindex="16"  style="width: 100%" id="tanggal_hari" data-placeholder="Cari Tanggal" class="form-control js-select2">
										<option value="#" selected>-Pilih Tanggal-</option>
										<? for($i=1;$i<29;$i++) {?>
											<option value="<?=sprintf("%02s", $i)?>"><?=sprintf("%02s", $i)?></option>
										<? }?>
									</select>										
								</td>
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_tanggal" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_tanggal" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						
				</table>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="Lokasi Tubuh"><?=text_primary('Karyawan / Dokter')?></label>
				
				<div class="col-md-9">
					<table id="tabel_karyawan" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 30%;">Tipe </th>
								<th style="width: 60%;">Nama Pegawai / Dokter</th>								
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td>
									<select name="tipe" tabindex="16"  style="width: 100%" id="tipe" data-placeholder="Cari Tipe Tarif" class="js-select2 form-control input-sm">
										<option value="1" selected>Pegawai</option>
										<option value="2" >Dokter</option>											
									</select>										
								</td>
								<td>									
									<select name="peg_id" tabindex="16"  style="width: 100%" id="peg_id" data-placeholder="" class="form-control input-sm">																			
									</select>
								</td>								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_nama" title="Masukan Item"><i class="fa fa-save"></i></button>&nbsp;&nbsp;
								</td>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						
				</table>
				</div>
			</div>
			
			<?}?>
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		load_tanggal();
		load_peg_dok();
		clear_all();
	});
	
	// Initialize when page loads
	$("#peg_id").select2({
			minimumInputLength: 2,
			noResults: 'Dokter Tidak Ditemukan.',
		ajax: {
			url: '{site_url}mpiutang/jd_pegawai_dokter/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,
		 data: function (params) {
			  var query = {
				search: params.term,tipe:$("#tipe").val()
			  }
			  return query;
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							text: item.nama ,
							id: item.id
						}
					})
				};
			}
		}
	});
	$(document).on("change","#tipe",function(){
		$("#peg_id").val(null).trigger('change');
	});
	function load_tanggal(){
		var id=$("#id").val();
		
		$('#tabel_tanggal').DataTable().destroy();
		var table = $('#tabel_tanggal').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}mpiutang/load_tanggal/',
			type: "POST",
			dataType: 'json',
			data: {
				id: id
			}
		},
		columnDefs: [
		{"targets": [3], "visible": false },
		{ "width": "60%", "targets": [0] },
		 { "width": "30%", "targets": [1] },
		 { "width": "10%", "targets": [3] },
					]
		});
	}
	function load_peg_dok(){
		var id=$("#id").val();
		
		$('#tabel_karyawan').DataTable().destroy();
		var table = $('#tabel_karyawan').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}mpiutang/load_peg_dok/',
			type: "POST",
			dataType: 'json',
			data: {
				id: id
			}
		},
		columnDefs: [
		{"targets": [3,4,5], "visible": false },
		{ "width": "30%", "targets": [0] },
		 { "width": "60%", "targets": [1] },
		 { "width": "10%", "targets": [3] },
					]
		});
	}
	$(document).on("click","#simpan_tanggal",function(){
		
		if (validate_add()==false)return false;
		var duplicate=false;
			var table = $('#tabel_tanggal').DataTable();		
			table.rows().eq(0).each( function ( index ) {
				var row = table.row( index );		 
				var data = row.data();
				if ($("#tedit").val()==''){
					if (data[1]==$("#tanggal_hari").val()){
						duplicate = true;
						sweetAlert("Duplicate...", "Tanggal Duplicate!", "error");
					}
				}else{
					if (data[1]==$("#tanggal_hari").val() && data[3]!=$("#tedit").val()){
						duplicate = true;
						sweetAlert("Duplicate...", "Tanggal Duplicate!", "error");
					}
				}
				
			} );
		// return false;
		if(duplicate) return false;
		if ($("#tedit").val()==''){
			simpan();
		}else{
			simpan_edit();
		}
		
	});
	$(document).on("click","#clear_tanggal",function(){
		
		clear_all();
		
	});
	function clear_all(){
		$("#clear_tanggal").hide();
		$("#tedit").val('');
		$("#deskripsi").val('');
		$("#tanggal_hari").val('#').trigger('change');
		$("#simpan_tanggal").attr('disabled', false);	
	}
	$(document).on("click","#simpan_nama",function(){
		if (validate_add_peg()==false)return false;
		var tipe=$("#tipe").val();
		var peg_id=$("#peg_id").val();
		var id=$("#id").val();		
		// alert(tipe);
		$.ajax({
			url: '{site_url}mpiutang/cek_duplicate',
			type: 'POST',
			data: {
				tipe: tipe,	peg_id:peg_id,pengaturan_id:id
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Tarif Berhasil disimpan'});
					$('#tabel_karyawan').DataTable().ajax.reload( null, false );
					$("#peg_id").val(null).trigger('change')
				}else{
					sweetAlert("Gagal...", "Penyimpanan!", "error");
					
				}
			}
		});
		// if(duplicate) return false;
		
		// simpan();
		
	});
	$(document).on("click",".edit",function(){
		 table = $('#tabel_tanggal').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,3).data()
		var des = table.cell(tr,0).data()
		var tgl = table.cell(tr,1).data()
		
		$('#tanggal_hari').val(tgl).trigger('change');
		$('#tedit').val(id);
		$('#deskripsi').val(des);
		$("#clear_tanggal").show();
		// $("#Eid").val(id);
		// alert('SINI');
		return false;
	});
	$(document).on("click",".hapus",function(){
		table = $('#tabel_tanggal').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,3).data()
		// var idunit = table.cell(tr,7).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menghapus Tanggal ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mpiutang/hapus_tanggal',
				type: 'POST',
				data: {
					tedit: id	
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Tanggal Berhasil dihapus'});
					$('#tabel_tanggal').DataTable().ajax.reload( null, false );
								
					// window.location.href = "{site_url}tko/index/"+idunit;
				}
			});
		});
		
		return false;
	});
	$(document).on("click",".hapus_peg",function(){
		// alert('SINI');
		table = $('#tabel_karyawan').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,3).data()
		var tipe = table.cell(tr,4).data()
		// var idunit = table.cell(tr,7).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menghapus Data Pegawai / Dokter?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mpiutang/hapus_peg',
				type: 'POST',
				data: {
					id: id,tipe:tipe	
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Tarif Berhasil dihapus'});
					$('#tabel_karyawan').DataTable().ajax.reload( null, false );
								
					// window.location.href = "{site_url}tko/index/"+idunit;
				}
			});
		});
		
		return false;
	});
	
	function simpan(){
		$("#simpan_tanggal").attr('disabled', true);
		var id=$("#id").val();		
		var tanggal_hari=$("#tanggal_hari").val();
		var deskripsi=$("#deskripsi").val();

			$.ajax({
				url: '{site_url}mpiutang/simpan_add',
				type: 'POST',
				data: {
					id: id,tanggal_hari: tanggal_hari,deskripsi: deskripsi,
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Tanggal Berhasil Disimpan'});
					$('#tabel_tanggal').DataTable().ajax.reload( null, false );
					// $('#deskripsi').val('').trigger('change');
					// $('#tanggal_hari').val('#').trigger('change');
					// $("#simpan_tanggal").attr('disabled', false);	
					// window.location.href = "{site_url}tko/index/"+idunit;
					clear_all();
				}
			});
		
	}
	function simpan_edit(){
		$("#simpan_tanggal").attr('disabled', true);
		var id=$("#id").val();		
		var tanggal_hari=$("#tanggal_hari").val();
		var deskripsi=$("#deskripsi").val();
		var tedit=$("#tedit").val();

			$.ajax({
				url: '{site_url}mpiutang/simpan_edit',
				type: 'POST',
				data: {
					id: id,tanggal_hari: tanggal_hari,deskripsi: deskripsi,tedit: tedit,
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Tanggal Berhasil Disimpan'});
					$('#tabel_tanggal').DataTable().ajax.reload( null, false );
					clear_all();
					// $('#deskripsi').val('').trigger('change');
					// $('#tanggal_hari').val('#').trigger('change');
					// $("#simpan_tanggal").attr('disabled', false);	
					// window.location.href = "{site_url}tko/index/"+idunit;
				}
			});
		
	}
	function validate_final()
	{
		if ($("#nama").val()==''){
			sweetAlert("Maaf...", "Nama Diagnosa Harus diisi", "error");
			return false;
		}
		
		if ($("#kelompok_operasi_id").val()=='#'){
			sweetAlert("Maaf...", "Kelompok Operasi Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function validate_add()
	{
		if ($("#tanggal_hari").val()=='' || $("#tanggal_hari").val()==null || $("#tanggal_hari").val()=="#"){
			sweetAlert("Maaf...", "Tanggal Harus diisi", "error");
			return false;
		}
		if ($("#deskripsi").val()=='' || $("#deskripsi").val()==null || $("#deskripsi").val()=="#"){
			sweetAlert("Maaf...", "Deskripsi Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function validate_add_peg()
	{
		if ($("#tipe").val()=='' || $("#tipe").val()==null || $("#tipe").val()=="#"){
			sweetAlert("Maaf...", "Tipe Harus diisi", "error");
			return false;
		}
		if ($("#peg_id").val()=='' || $("#peg_id").val()==null || $("#peg_id").val()=="#"){
			sweetAlert("Maaf...", "Nama Harus diisi", "error");
			return false;
		}
		
		return true;
	}
</script>
