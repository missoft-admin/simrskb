<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('229'))){ ?>
<div class="block">
	<div class="block-header">
	<?php if (UserAccesForm($user_acces_form,array('231'))){ ?>
		<ul class="block-options">
			<li>
          <a href="{base_url}mtarif_rawatinap/create/{idtipe}/{idruangan}" class="btn"><i class="fa fa-plus"></i></a>
			</li>
    </ul>
	<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
	<?php if (UserAccesForm($user_acces_form,array('230'))){ ?>
		<hr style="margin-top:0px">
		<?php echo form_open('mtarif_rawatinap/filter','class="form-horizontal" id="form-work"') ?>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien">Tipe</label>
					<div class="col-md-8">
						<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="" selected>Pilih Opsi</option>
							<option value="1" <?=($idtipe == 1) ? "selected" : "" ?>>Full Care</option>
							<option value="2" <?=($idtipe == 2) ? "selected" : "" ?>>ECG</option>
							<option value="4" <?=($idtipe == 4) ? "selected" : "" ?>>Sewa Alat</option>
							<option value="5" <?=($idtipe == 5) ? "selected" : "" ?>>Ambulance</option>
							<option value="6" <?=($idtipe == 6) ? "selected" : "" ?>>Lain-lain</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien">Ruangan</label>
					<div class="col-md-8">
						<select name="idruangan" id="idruangan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="">Pilih Opsi</option>
							<?php foreach  ($list_ruangan as $row) { ?>
								<option value="<?=$row->id;?>" <?=($idruangan == $row->id) ? "selected" : "" ?>><?=$row->nama?></option>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-12">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close() ?>
		<hr>
	<?}?>
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		</div>
	</div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"bSort": false,
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
				url: '{site_url}mtarif_rawatinap/getIndex/' + {idtipe} + '/' + {idruangan},
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "10%", "targets": 0, "orderable": true },
					{ "width": "70%", "targets": 1, "orderable": true },
					{ "width": "20%", "targets": 2, "orderable": true }
				]
			});
	});
</script>
