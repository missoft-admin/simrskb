<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mlogic" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mlogic/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Nama Logic</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="{id}" required="">
				</div>
			</div>
			
			
			<div class="form-group">
				<div class="col-md-10 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mlogic" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<hr>
			<?if ($id !=''){?>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="Lokasi Tubuh"><?=text_primary('Unit Pelayanan')?></label>
				<div class="col-md-10">
					<table id="tabel_unit" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<input type="hidden" class="form-control" id="tedit" placeholder="tedit" name="tedit" value="0" required="">
							<tr>
								<th style="width: 40%;">Unit</th>
								<th style="width: 20%;">Actions</th>
								<th style="width: 20%;">id</th>
							</tr>
							<tr>
								
								<td>
									<select id="idunit" name="idunit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#">- Pilih Unit -</option>
										
									</select>								
								</td>							
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_unit" title="Masukan Item"><i class="fa fa-save"></i></button>
									
								</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						
					</table>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-2 control-label" for="Lokasi Tubuh"><?=text_primary('Proses Approval')?></label>
				
				<div class="col-md-10">
					<table id="tabel_detail" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 5%;">Step</th>								
															
								<th style="width: 10%;">Tipe RKA</th>								
								<th style="width: 10%;">Jenis Pengajuan</th>								
								<th style="width: 10%;">Nominal Transaksi</th>								
								<th style="width: 15%;">User </th>
								<th style="width: 10%;">Jika Setuju</th>								
								<th style="width: 10%;">Jika Tolak</th>	
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								
								<td>
									<select name="step" style="width: 100%" id="step" class="js-select2 form-control input-sm">
										<?for($i=1;$i<10;$i++){?>
											<option value="<?=$i?>"><?=$i?></option>
										<?}?>
									</select>										
								</td>
								
								<td>
									<select name="tipe_rka" style="width: 100%" id="tipe_rka" class="js-select2 form-control input-sm">										
										<option value="#">- Pilih Tipe RKA -</option>
										<option value="1">RKA</option>
										<option value="2">NON RKA</option>										
									</select>										
								</td>
								<td>
									<select name="idjenis" style="width: 100%" id="idjenis" class="js-select2 form-control input-sm">										
										<option value="#">- Pilih Jenis -</option>
										<?foreach(get_all('mjenis_pengajuan_rka',array('status'=>1),'nama') as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>									
										<?}?>										
									</select>										
								</td>
								<td>
									<select name="operand" style="width: 100%" id="operand" class="js-select2 form-control input-sm">					
										
										<option value=">">></option>										
										<option value=">=">>=</option>	
										<option value="<"><</option>
										<option value="<="><=</option>
										<option value="=">=</option>										
									</select>
									<input type="text" style="width: 100%"  class="form-control number" id="nominal" placeholder="0" name="nominal" value="0">
									<input type="hidden" class="form-control" id="id_edit" placeholder="0" name="id_edit" value="">
								</td>	
								<td>
									<select name="iduser" tabindex="16"  style="width: 100%" id="iduser" data-placeholder="User" class="js-select2 form-control input-sm"></select>										
								</td>
								<td>
									<select name="proses_setuju" style="width: 100%" id="proses_setuju" class="js-select2 form-control input-sm">										
										<option value="1">Langsung</option>
										<option value="2">Menunggu User</option>										
									</select>										
								</td>
								<td>
									<select name="proses_tolak" style="width: 100%" id="proses_tolak" class="js-select2 form-control input-sm">										
										<option value="1">Langsung</option>
										<option value="2">Menunggu User</option>										
									</select>										
								</td>
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_detail" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_edit" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						
				</table>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-2 control-label" for="Lokasi Tubuh"><?=text_primary('User Bendahara')?></label>
				<div class="col-md-10">
					<table id="tabel_user" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 25%;">Tipe RKA</th>
								<th style="width: 25%;">Jenis</th>
								<th style="width: 25%;">User Bendahara</th>
								<th style="width: 10%;">Actions</th>
								<th style="width: 20%;">id</th>
							</tr>
							<tr>
								<td>
									<select name="tipe_rka_user" style="width: 100%" id="tipe_rka_user" class="js-select2 form-control input-sm">										
										<option value="#">- Pilih Tipe RKA -</option>
										<option value="1">RKA</option>
										<option value="2">NON RKA</option>										
									</select>										
								</td>
								<td>
									<select name="idjenis_user" style="width: 100%" id="idjenis_user" class="js-select2 form-control input-sm">										
										<option value="#">- Pilih Jenis -</option>
										<?foreach(get_all('mjenis_pengajuan_rka',array('status'=>1),'nama') as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>									
										<?}?>										
									</select>										
								</td>
								<td>
									<select id="iduser_bendahara" name="iduser_bendahara" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										
									</select>								
								</td>							
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_user" title="Masukan Item"><i class="fa fa-save"></i></button>
									
								</td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						
					</table>
				</div>
			</div>
			
			<?}?>
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		load_unit();
		clear_all();
		list_user();
		load_detail();
		load_user();
	});
	function list_unit(){
		$('#idunit')
			.find('option')
			.remove()
			.end()
			.append('<option value="">- Pilih Unit -</option>')
			.val('').trigger("liszt:updated");

		$.ajax({
			url: '{site_url}mlogic/list_unit/'+$("#id").val(),
			dataType: "json",
			success: function(data) {

			$.each(data.detail, function (i,unit) {
			$('#idunit')
			.append('<option value="' + unit.id + '">' + unit.nama + '</option>');
			});
			}
		});

	}
	$(document).on("change","#step,#tipe_rka,#idjenis",function(){
		list_user();
	});
	$(document).on("change","#tipe_rka_user,#idjenis_user",function(){
		list_user_bendahara();
	});
	function list_user(){
		// alert($("#idjenis").val());
		$('#iduser')
			.find('option')
			.remove()
			.end()
			.append('<option value="">- Pilih User -</option>')
			.val('').trigger("liszt:updated");
		if ($("#tipe_rka").val()!='#' && $("#idjenis").val()!='#'){
			// console.log($("#id").val()+'/'+$("#step").val()+'/'+$("#tipe_rka").val()+'/'+$("#idjenis").val());
			$.ajax({
				url: '{site_url}mlogic/list_user/'+$("#id").val()+'/'+$("#step").val()+'/'+$("#tipe_rka").val()+'/'+$("#idjenis").val(),
				dataType: "json",
				success: function(data) {

				$.each(data.detail, function (i,unit) {
				$('#iduser')
				.append('<option value="' + unit.id + '">' + unit.name + '</option>');
				});
				}
			});
		}else{
			$('#iduser')
			.find('option')
			.remove()
			.end()
			.append('<option value="">- Pilih User -</option>')
		}

	}
	function list_user_bendahara(){
		$('#iduser_bendahara')
			.find('option')
			.remove()
			.end()
			.append('<option value="">- Pilih user -</option>')
			.val('').trigger("liszt:updated");
		if ($("#tipe_rka_user").val()!='#' && $("#idjenis_user").val()!='#'){
		$.ajax({
			url: '{site_url}mlogic/list_user_bendahara/'+$("#id").val()+'/'+$("#tipe_rka_user").val()+'/'+$("#idjenis_user").val(),
			dataType: "json",
			success: function(data) {

			$.each(data.detail, function (i,unit) {
			$('#iduser_bendahara')
			.append('<option value="' + unit.id + '">' + unit.name + '</option>');
			});
			}
		});
		}else{
			$('#iduser_bendahara')
			.find('option')
			.remove()
			.end()
			.append('<option value="">- Pilih User -</option>')
		}

	}
	
	function load_unit(){
		var id=$("#id").val();
		
		$('#tabel_unit').DataTable().destroy();
		var table = $('#tabel_unit').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}mlogic/load_unit/',
			type: "POST",
			dataType: 'json',
			data: {
				id: id
			}
		},
		columnDefs: [
		{"targets": [2], "visible": false },
		{ "width": "60%", "targets": [0] },
		 { "width": "30%", "targets": [1] },
		 { "width": "10%", "targets": [2] },
					]
		});
	}
	function load_user(){
		var id=$("#id").val();
		
		$('#tabel_user').DataTable().destroy();
		var table = $('#tabel_user').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}mlogic/load_user/',
			type: "POST",
			dataType: 'json',
			data: {
				id: id
			}
		},
		columnDefs: [
		{"targets": [4], "visible": false },
		{ "width": "15%", "targets": [0] },
		 { "width": "20%", "targets": [1] },
		 { "width": "20%", "targets": [2] },
		 { "width": "20%", "targets": [3] },
					]
		});
	}
	function load_detail(){
		var idlogic=$("#id").val();
		var tipe_rka=$("#tipe_rka_user").val();
		var idjenis=$("#idjenis_user").val();
		
		$('#tabel_detail').DataTable().destroy();
		var table = $('#tabel_detail').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}mlogic/load_detail/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic,
				
			}
		},
		columnDefs: [
		{"targets": [8], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	$(document).on("click","#simpan_unit",function(){
		
		if (validate_add()==false)return false;
		
		simpan();
		
	});
	$(document).on("click","#simpan_user",function(){
		
		if (validate_add_user()==false)return false;
		
		simpan_user();
		
	});
	$(document).on("click","#clear_edit",function(){
		clear_all();
		$("#id_edit").val('');
		clear_input_detail();
		$("#iduser").val(null).trigger('change');
	});
	function clear_all(){
		list_unit();
		list_user_bendahara();
		$("#id_edit").val('');
		$("#simpan_unit").attr('disabled', false);
	}
	function clear_input_detail(){
		$("#step").val('1').trigger('change');
		$("#tipe_rka").val('#').trigger('change');
		$("#idjenis").val('#').trigger('change');
		$("#operand").val('>=').trigger('change');
		list_user();
		$("#nominal").val(0);
	}
	$(document).on("click","#simpan_detail",function(){
		if (validate_add_detail()==false)return false;
		var id_edit=$("#id_edit").val();
		var idlogic=$("#id").val();
		var iduser=$("#iduser").val();
		var step=$("#step").val();
		var proses_setuju=$("#proses_setuju").val();
		var proses_tolak=$("#proses_tolak").val();
		var tipe_rka=$("#tipe_rka").val();
		var idjenis=$("#idjenis").val();
		var operand=$("#operand").val();
		var nominal=$("#nominal").val();
		$.ajax({
			url: '{site_url}mlogic/simpan_detail',
			type: 'POST',
			data: {
				idlogic: idlogic,iduser:iduser,step:step,
				proses_setuju: proses_setuju,proses_tolak:proses_tolak,tipe_rka:tipe_rka,
				idjenis: idjenis,operand:operand,nominal:nominal,id_edit:id_edit
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Logic Berhasil disimpan'});
					$('#tabel_detail').DataTable().ajax.reload( null, false );
					clear_input_detail();
					$("#id_edit").val('');
				}else{
					sweetAlert("Gagal...", "Penyimpanan!", "error");
					
				}
			}
		});
		
		
	});
	
	$(document).on("click",".hapus_det",function(){
		var table = $('#tabel_detail').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,8).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menghapus Logic Detail ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mlogic/hapus_det',
				type: 'POST',
				data: {
					id: id	
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Detail Berhasil dihapus'});
					$('#tabel_detail').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	$(document).on("click",".hapus",function(){
		var table = $('#tabel_unit').DataTable()
		var tr = $(this).parents('tr')
		var idunit = table.cell(tr,2).data()
		var idlogic = $("#id").val()
		// alert(id);
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menghapus Unit?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mlogic/hapus_unit',
				type: 'POST',
				data: {
					idlogic: idlogic,idunit:idunit	
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Unit Berhasil dihapus'});
					$('#tabel_unit').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	$(document).on("click",".hapus_user",function(){
		var table = $('#tabel_user').DataTable()
		var tr = $(this).parents('tr')
		var iduser = table.cell(tr,4).data()
		var idlogic = $("#id").val()
		// alert(id);
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menghapus User Bendahara?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mlogic/hapus_user',
				type: 'POST',
				data: {
					idlogic: idlogic,iduser:iduser	
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' User Berhasil dihapus'});
					$('#tabel_user').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	
	$(document).on("click",".edit",function(){
		// alert('SINI');
		table = $('#tabel_detail').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,8).data()
		$.ajax({
			url: '{site_url}mlogic/get_edit',
			type: 'POST',
			dataType: "json",
			data: {
				id: id
			},
			success: function(data) {
				// var item=data.detail;
				$("#id_edit").val(data.id);
				$("#step").val(data.step).trigger('change');
				$("#proses_setuju").val(data.proses_setuju).trigger('change');
				$("#proses_tolak").val(data.proses_tolak).trigger('change');
				$("#tipe_rka").val(data.tipe_rka).trigger('change');
				$("#idjenis").val(data.idjenis).trigger('change');
				$("#operand").val(data.operand).trigger('change');
				$("#nominal").val(data.nominal).trigger('change');
				var newOption = new Option(data.user_nama, data.iduser, true, true);
					
				$("#iduser").append(newOption);
				$("#iduser").val(data.iduser).trigger('change');

				console.log(data.id);
			}
		});
	});
	
	function simpan(){
		$("#simpan_unit").attr('disabled', true);
		var id=$("#id").val();		
		var idunit=$("#idunit").val();

			$.ajax({
				url: '{site_url}mlogic/simpan_add',
				type: 'POST',
				data: {
					id: id,idunit: idunit
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Tanggal Berhasil Disimpan'});
					$('#tabel_unit').DataTable().ajax.reload( null, false );					
					clear_all();
				}
			});
		
	}
	function simpan_user(){
		var id=$("#id").val();		
		var iduser_bendahara=$("#iduser_bendahara").val();
		var tipe_rka=$("#tipe_rka_user").val();
		var idjenis=$("#idjenis_user").val();

			$.ajax({
				url: '{site_url}mlogic/simpan_user',
				type: 'POST',
				data: {
					id: id,iduser: iduser_bendahara,tipe_rka: tipe_rka,idjenis: idjenis
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Tanggal Berhasil Disimpan'});
					$('#tabel_user').DataTable().ajax.reload( null, false );					
					clear_all();
				}
			});
		
	}
	function simpan_edit(){
		$("#simpan_unit").attr('disabled', true);
		var id=$("#id").val();		
		var tanggal_hari=$("#tanggal_hari").val();
		var deskripsi=$("#deskripsi").val();
		var tedit=$("#tedit").val();

			$.ajax({
				url: '{site_url}mlogic/simpan_edit',
				type: 'POST',
				data: {
					id: id,tanggal_hari: tanggal_hari,deskripsi: deskripsi,tedit: tedit,
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Tanggal Berhasil Disimpan'});
					$('#tabel_unit').DataTable().ajax.reload( null, false );
					clear_all();
					// $('#deskripsi').val('').trigger('change');
					// $('#tanggal_hari').val('#').trigger('change');
					// $("#simpan_unit").attr('disabled', false);	
					// window.location.href = "{site_url}tko/index/"+idunit;
				}
			});
		
	}
	function validate_final()
	{
		if ($("#nama").val()==''){
			sweetAlert("Maaf...", "Nama Diagnosa Harus diisi", "error");
			return false;
		}
		
		if ($("#kelompok_operasi_id").val()=='#'){
			sweetAlert("Maaf...", "Kelompok Operasi Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function validate_add()
	{
		if ($("#idunit").val()=='' || $("#idunit").val()==null || $("#idunit").val()=="#"){
			sweetAlert("Maaf...", "Unit Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function validate_add_user()
	{
		if ($("#tipe_rka_user").val()=='' || $("#tipe_rka_user").val()==null || $("#tipe_rka_user").val()=="#"){
			sweetAlert("Maaf...", "Tipe RKA Harus diisi", "error");
			return false;
		}
		if ($("#idjenis_user").val()=='' || $("#idjenis_user").val()==null || $("#idjenis_user").val()=="#"){
			sweetAlert("Maaf...", "Tipe RKA Harus diisi", "error");
			return false;
		}
		if ($("#iduser_bendahara").val()=='' || $("#iduser_bendahara").val()==null || $("#iduser_bendahara").val()=="#"){
			sweetAlert("Maaf...", "User Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function validate_add_detail()
	{
		if ($("#iduser").val()=='' || $("#iduser").val()==null || $("#iduser").val()=="#"){
			sweetAlert("Maaf...", "User Harus diisi", "error");
			return false;
		}
		if ($("#idjenis").val()=='' || $("#idjenis").val()==null || $("#idjenis").val()=="#"){
			sweetAlert("Maaf...", "Jenis Pengajuan Harus diisi", "error");
			return false;
		}
		if ($("#idjenis").val()=='' || $("#idjenis").val()==null || $("#idjenis").val()=="#"){
			sweetAlert("Maaf...", "Jenis Pengajuan Harus diisi", "error");
			return false;
		}
		if ($("#nominal").val()=='0'){
			if (($("#operand").val()!='>=' && $("#operand").val()!='>')){
			sweetAlert("Maaf...", "Nominal Harus diisi", "error");
			return false;
			}
		}
		
		return true;
	}
</script>
