<!-- Modal Cari Pasien -->
<div class="modal in" id="cari-pasien-modal" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:70%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Data Pasien</h3>
				</div>
				<div class="block-content">
					<hr>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snomedrec">No. Medrec</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snomedrec" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snamapasien">Nama Pasien</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snamapasien" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snama_keluarga">Penanggung Jawab</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snama_keluarga" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;display:none">
									<label class="col-md-4 control-label" for="sjeniskelamin">Jenis Kelamin</label>
									<div class="col-md-8">
										<label class="css-input css-radio css-radio-sm css-radio-default push-10-r">
											<input type="radio" class="sjeniskelamin" name="sjeniskelamin" value="1"><span></span> Laki-laki
										</label>
										<label class="css-input css-radio css-radio-sm css-radio-default">
											<input type="radio" class="sjeniskelamin" name="sjeniskelamin" value="2"><span></span> Perempuan
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="salamat">Alamat</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="salamat" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="stanggallahir">Tanggal Lahir</label>
									<div class="col-md-8">
										<input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="stanggallahir" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snotelepon">Telepon</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snotelepon" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snotelepon"></label>
									<div class="col-md-8">
										<button class="btn btn-primary" type="button" id="btn-cari-pasien" style="width:100%"><i class="fa fa-search"></i> Cari</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>

					<table width="100%" class="table table-bordered table-striped" id="datatable-pasien">
						<thead>
							<tr>
								<th>No. Medrec</th>
								<th>Nama</th>
								<th>Jenis Kelamin</th>
								<th>Tanggal Lahir</th>
								<th>Alamat</th>
								<th>Telepon</th>
								<th>Keluarga</th>
								<th>Tlp. Keluarga</th>
							</tr>
						</thead>
						<tbody id="pasienmodaltabel">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$("#btn-cari-pasien").click(function(){
	
	loadDataPasien();
});
$("#btn_cari_pasien").click(function(){
	
	loadDataPasien();
});

function loadDataPasien() {
	let snomedrec=$("#snomedrec").val();
	let snamapasien=$("#snamapasien").val();
	let snama_keluarga=$("#snama_keluarga").val();
	let salamat=$("#salamat").val();
	let stanggallahir=$("#stanggallahir").val();
	let snotelepon=$("#snotelepon").val();
	$('#datatable-pasien').DataTable().destroy();
		var tablePasien = $('#datatable-pasien').DataTable({
			"pageLength": 8,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}tpoliklinik_pendaftaran/getPasien',
				type: "POST",
				dataType: 'json',
				data: {
					snomedrec: snomedrec,
					snamapasien: snamapasien,
					snama_keluarga: snama_keluarga,
					salamat: salamat,
					stanggallahir: stanggallahir,
					snotelepon: snotelepon,
				}
			},
			// "columnDefs": [{
				// "targets": [0],
				// "orderable": true
			// }]
		});
		
	}
</script>
<!-- End Modal Cari Pasien -->