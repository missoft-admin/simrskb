<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<?
$user_id=$this->session->userdata('user_id');
?>
<div class="block push-10">
	<div class="block-content bg-primary">
			<input type="hidden" id="tab" name="tab" value="{tab}">
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<input type="hidden" id="st_login" name="st_login" value="{st_login}">
			<div class="row pull-10">
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">Dokter</label>
						<div class="col-xs-12">
							<select id="iddokter" name="iddokter" disabled  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" <?=($iddokter=='#'?'selected':'')?>>- Semua Dokter -</option>
								<?foreach(get_all('mdokter',array('status'=>1)) as $r){?>
								<option value="<?=$r->id?>" <?=($iddokter==$r->id?'selected':'')?>><?=$r->nama?></option>
								<?}?>
								
								
							</select>
						</div>
					</div>
				</div>
				
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">Tanggal</label>
						<div class="col-xs-12">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                <input class="form-control" type="text" id="tanggal_1" placeholder="From" value="{tanggal_1}">
                                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                <input class="form-control" type="text" id="tanggal_2" placeholder="To" value="{tanggal_2}">
                            </div>
						</div>
					</div>
				</div>
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">Search</label>
						<div class="col-xs-12">
							<input type="text" class="form-control" id="pencarian" value="">
						</div>
					</div>
				</div>
				
				<div class="col-md-1 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">&nbsp;&nbsp;</label>
						<div class="col-xs-12">
							<span class="input-group-btn ">
								<button class="btn btn-warning btn-block " id="btn_cari" type="button" title="Login"><i class="fa fa-search pull-left"></i> Cari</button>&nbsp;
							</span>
						</div>
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
	</div>
</div>

<div class="block">
	<ul class="nav nav-pills">
		<li id="div_1" class="<?=($tab=='1'?'active':'')?>">
			<a href="javascript:void(0)" onclick="set_tab(1)"><i class="si si-list"></i> Semua</a>
		</li>
		<li  id="div_2" class="<?=($tab=='2'?'active':'')?>">
			<a href="javascript:void(0)"  onclick="set_tab(2)"><i class="si si-pin"></i> Belum Dijawab </a>
		</li>
		<li  id="div_3" class="<?=($tab=='3'?'active':'')?>">
			<a href="javascript:void(0)" onclick="set_tab(3)"><i class="fa fa-check-square-o"></i> Telah Dijawab</a>
		</li>
		
	</ul>
	<div class="block-content">
			<input type="hidden" id="tab" name="tab" value="{tab}">
			<div class="row   ">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table" id="index_all">
							<thead>
								<tr>
									<th width="10%"></th>
									<th width="10%"></th>
									<th width="10%"></th>
									<th width="10%"></th>
									<th width="10%"></th>
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		
	
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<? $this->load->view('Tpendaftaran_poli_ttv/modal_alergi')?>
<script type="text/javascript">
var table;
var tab;
var st_login;

$(document).ready(function(){	
	load_index_all();
	
});


$(document).on("click","#btn_cari",function(){
	load_index_all();
});
function set_tab($tab){
	tab=$tab;
	// alert(tab);
	// $("#cover-spin").show();
	document.getElementById("div_1").classList.remove("active");
	document.getElementById("div_2").classList.remove("active");
	document.getElementById("div_3").classList.remove("active");
	if (tab=='1'){
		document.getElementById("div_1").classList.add("active");
	}
	if (tab=='2'){
		document.getElementById("div_2").classList.add("active");
	}
	if (tab=='3'){
		document.getElementById("div_3").classList.add("active");
	}
	
	load_index_all();
}
function setujui_all($assesmen_id){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan setuju Semua ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}trekon_obat/setujui_all', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$assesmen_id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Proses.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$('#index_all').DataTable().ajax.reload( null, false );
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
				}
			}
		});
	});

}
function tolak_pindah($assesmen_id){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menolak Pindah DPJP ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}trekon_obat/tolak_pindah', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$assesmen_id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Hapus TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$('#index_all').DataTable().ajax.reload( null, false );
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
				}
			}
		});
	});

}
function setuju_terima($assesmen_id){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menerima DPJP Baru?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}trekon_obat/setuju_terima', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$assesmen_id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Hapus TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$('#index_all').DataTable().ajax.reload( null, false );
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
				}
			}
		});
	});

}
function tolak_terima($assesmen_id){
	
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menerima DPJP Baru?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}trekon_obat/tolak_terima', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:$assesmen_id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Hapus TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$('#index_all').DataTable().ajax.reload( null, false );
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
				}
			}
		});
	});

}

function load_index_all(){
	$('#index_all').DataTable().destroy();	
	let idpoli=$("#idpoli").val();
	let iddokter=$("#iddokter").val();
	let tanggal_1=$("#tanggal_1").val();
	let tanggal_2=$("#tanggal_2").val();
	let pencarian=$("#pencarian").val();
	
	// alert(ruangan_id);
	table = $('#index_all').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			
			"processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "10%", "targets": 0},
					{ "width": "50%", "targets": 1},
					{ "width": "30%", "targets": 2},
					{ "width": "10%", "targets": 3},
					
				],
            ajax: { 
                url: '{site_url}trekon_obat/getIndex_all', 
                type: "POST" ,
                dataType: 'json',
				data : {
						iddokter:iddokter,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						pencarian:pencarian,
						tab:tab,
						
					   }
            },
			"drawCallback": function( settings ) {
				 $("#index_all thead").remove();
			 }  
        });
	$("#cover-spin").hide();
}

</script>