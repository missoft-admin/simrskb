<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>

<div class="block">
    <div class="block-content">
        <h4>PLAN ORDER</h4> <br>

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nomorRekamMedis">Nomor Rekam Medis & Nama Pasien</label>
                            <input type="text" class="form-control" id="nomorRekamMedis" placeholder="Nomor Rekam Medis" disabled value="{nomor_medrec} {nama_pasien}">
                        </div>

                        <div class="form-group">
                            <label for="jenisKelamin">Jenis Kelamin</label>
                            <input type="text" class="form-control" id="jenisKelamin" placeholder="Jenis Kelamin" disabled value="{jenis_kelamin}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nik">Nomor Induk Kependudukan</label>
                            <input type="text" class="form-control" id="nik" placeholder="NIK" disabled value="{nomor_ktp}">
                        </div>

                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="email" class="form-control" id="email" placeholder="E-mail" disabled value="{email}">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" class="form-control" id="alamat" placeholder="Alamat" disabled value="{alamat_pasien}">
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="tglLahir">Tanggal Lahir</label>
                            <input type="text" class="form-control" id="tglLahir" placeholder="Tanggal Lahir" disabled value="{tanggal_lahir}">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="umur">Umur</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="tahun" placeholder="Tahun" disabled value="{umur_tahun}">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">Tahun</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="umur">&nbsp;</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="bulan" placeholder="Bulan" disabled value="{umur_bulan}">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">Bulan</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="umur">&nbsp;</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="hari" placeholder="Hari" disabled value="{umur_hari}">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">Hari</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="tglPermintaanDari">Tanggal Permintaan</label>
                    <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                        <input class="form-control" type="text" autocomplete="off" id="tglPermintaanDari" placeholder="From" value="{tanggal}">
                        <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                        <input class="form-control" type="text" autocomplete="off" id="tglPermintaanSampai" placeholder="To" value="{tanggal}">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="tglPendaftaranDari">Tanggal Pendaftaran</label>
                    <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                        <input class="form-control" type="text" autocomplete="off" id="tglPendaftaranDari" placeholder="From" value="">
                        <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                        <input class="form-control" type="text" autocomplete="off" id="tglPendaftaranSampai" placeholder="To" value="">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="nomorRegistrasi">Nomor Registrasi</label>
                    <input type="text" class="form-control" id="nomorRegistrasi">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="tujuanKlinik">Tujuan Klinik</label>
                    <select class="js-select2 form-control" id="tujuanKlinik">
                        <option value="0">Semua</option>
                        <?php foreach (get_all('mpoliklinik', array('status' => '1')) as $row) { ?>
                            <option value="<?= $row->id; ?>"><?= $row->nama; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="tujuanDokter">Tujuan Dokter</label>
                    <select class="js-select2 form-control" id="tujuanDokter">
                        <option value="0">Semua</option>
                        <?php foreach (get_all('mdokter', array('status' => '1')) as $row) { ?>
                            <option value="<?= $row->id; ?>"><?= $row->nama; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="dokterPeminta">Dokter Peminta</label>
                    <select class="js-select2 form-control" id="dokterPeminta">
                        <option value="0">Semua</option>
                        <?php foreach (get_all('mdokter', array('status' => '1')) as $row) { ?>
                            <option value="<?= $row->id; ?>"><?= $row->nama; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="dokterPeminta">&nbsp;</label>
                    <button class="btn btn-success text-uppercase" type="button" id="btn-filter" style="width:100%;"><i class="fa fa-filter"></i> Filter</button>
                </div>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="table_plan_order">
                        <thead>
                            <tr>
                                <th width="10%">Aksi</th>
                                <th width="10%">Waktu Permintaan</th>
                                <th width="10%">Nomor Permintaan</th>
                                <th width="10%">Nomor Pendaftaran</th>
                                <th width="10%">Tujuan Dokter & Klinik</th>
                                <th width="10%">Dokter Peminta</th>
                                <th width="10%">Diagnosa</th>
                                <th width="10%">Tujuan Radiologi</th>
                                <th width="10%">Prioritas</th>
                                <th width="10%">Dibuat Oleh</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        loadDataPlanOrder();

        $(document).on("click", "#btn-filter", function() {
            $("#cover-spin").show();

            loadDataPlanOrder();
        });
    });
    
    function loadDataPlanOrder() {
        let tanggal_permintaan_dari = $("#tglPermintaanDari").val();
        let tanggal_permintaan_sampai = $("#tglPermintaanSampai").val();
        let tanggal_pendaftaran_dari = $("#tglPendaftaranDari").val();
        let tanggal_pendaftaran_sampai = $("#tglPendaftaranSampai").val();
        let nomor_registrasi = $("#nomorRegistrasi").val();
        let tujuan_klinik = $("#tujuanKlinik option:selected").val();
        let tujuan_dokter = $("#tujuanDokter option:selected").val();
        let dokter_peminta = $("#dokterPeminta option:selected").val();

        $('#table_plan_order').DataTable().destroy();
        $('#table_plan_order').DataTable({
            "autoWidth": false,
            "searching": true,
            "pageLength": 50,
            "serverSide": true,
            "processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
            "columnDefs": [
                {
                    "width": "5%",
                    "targets": [0, 1, 2, 3, 4, 5, 6, 7, 9],
                    "className": "text-center"
                },
                {
                    "width": "5%",
                    "targets": [8],
                    "className": "text-left"
                },
            ],
            ajax: {
                url: '{site_url}term_radiologi_usg/getIndexPlanOrder/' + "{pasien_id}/{pendaftaran_id}/{asal_rujukan}",
                type: "POST",
                dataType: 'json',
                data: {
                    tanggal_permintaan_dari: tanggal_permintaan_dari,
                    tanggal_permintaan_sampai: tanggal_permintaan_sampai,
                    tanggal_pendaftaran_dari: tanggal_pendaftaran_dari,
                    tanggal_pendaftaran_sampai: tanggal_pendaftaran_sampai,
                    nomor_registrasi: nomor_registrasi,
                    tujuan_klinik: tujuan_klinik,
                    tujuan_dokter: tujuan_dokter,
                    dokter_peminta: dokter_peminta,
                }
            }
        });

        $("#cover-spin").hide();
    }

    function prosesPermintaan(asalRujukan, pendaftaranId, transaksiId, refPendaftaranId, refAsalRujukan) {
        window.open('{base_url}term_radiologi_usg/tindakan/' + asalRujukan + '/' + pendaftaranId + '/erm_rad/usg_permintaan/' + transaksiId + '/input_permintaan?ref=planorder&ref_pendaftaran_id=' + refPendaftaranId + '&ref_asal_rujukan=' + refAsalRujukan, '_blank');
    }

    function splitPermintaan(asalRujukan, pendaftaranId, transaksiId) {
        window.open('{base_url}term_radiologi_usg/tindakan/' + asalRujukan + '/' + pendaftaranId + '/erm_rad/usg_split/' + transaksiId, '_blank');
    }

    function cetakPermintaan(transaksiId) {
        window.open('{base_url}term_radiologi_usg/cetak_pemeriksaan/' + transaksiId, '_blank');
    }
</script>