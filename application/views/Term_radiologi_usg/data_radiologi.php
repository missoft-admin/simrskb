<?php declare(strict_types=1);
echo ErrorSuccess($this->session); ?>
<?php if ('' !== $error) {
    echo ErrorMessage($error);
} ?>

<div class="block">
    <div class="block-content">
        <h4 class="text-center">DATA RADIOLOGI</h4> <br>

        <div class="row" id="form-work">
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 15px">
                    <label for="nomorRekamMedis">Nomor Rekam Medis & Nama Pasien</label>
                    <input type="text" class="form-control" id="nomorRekamMedis" placeholder="Nomor Rekam Medis" disabled value="{nomor_medrec} {nama_pasien}">
                </div>

                <div class="form-group" style="margin-bottom: 15px">
                    <label for="nik">Nomor Induk Kependudukan</label>
                    <input type="text" class="form-control" id="nik" placeholder="NIK" disabled value="{nomor_ktp}">
                </div>

                <div class="form-group" style="margin-bottom: 15px">
                    <label for="jenisKelamin">Jenis Kelamin</label>
                    <input type="text" class="form-control" id="jenisKelamin" placeholder="Jenis Kelamin" disabled value="{jenis_kelamin}">
                </div>

                <div class="form-group" style="margin-bottom: 15px">
                    <label for="alamat">Alamat</label>
                    <input type="text" class="form-control" id="alamat" placeholder="Alamat" disabled value="{alamat_pasien}">
                </div>

                <div class="row" style="margin-bottom: 15px">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="tglLahir">Tanggal Lahir</label>
                            <input type="date" class="form-control" id="tglLahir" placeholder="Tanggal Lahir" disabled value="{tanggal_lahir}">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="umur">Umur</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="tahun" placeholder="Tahun" disabled value="{umur_tahun}">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">Tahun</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="umur">&nbsp;</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="bulan" placeholder="Bulan" disabled value="{umur_bulan}">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">Bulan</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="umur">&nbsp;</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="hari" placeholder="Hari" disabled value="{umur_hari}">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">Hari</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kelompok_pasien">Kelompok Pasien</label>
                            <input type="text" class="form-control" id="kelompok_pasien" placeholder="Kelompok Pasien" disabled value="{kelompok_pasien}">
                        </div>

                        <div class="form-group">
                            <label for="tipe_kunjungan">Tipe Kunjungan</label>
                            <input type="text" class="form-control" id="tipe_kunjungan" placeholder="Tipe Kunjungan" disabled value="{tipe_kunjungan}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama_asuransi">Nama Asuransi</label>
                            <input type="text" class="form-control" id="nama_asuransi" placeholder="Nama Asuransi" disabled value="{nama_asuransi}">
                        </div>

                        <div class="form-group">
                            <label for="nama_poliklinik">Nama Poliklinik</label>
                            <input type="text" class="form-control" id="nama_poliklinik" placeholder="Nama Poliklinik" disabled value="{nama_poliklinik}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dpjp">DPJP</label>
                            <input type="text" class="form-control" id="dpjp" placeholder="Tipe Kunjungan" disabled value="{dpjp}">
                        </div>
                    </div>
                        
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dokter_peminta_id">Dokter Peminta</label>
                            <select name="dokter_peminta_id" id="dokter_peminta_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <?php foreach (get_all('mdokter', ['status' => '1']) as $r) { ?>
                                <option value="<?php echo $r->id; ?>" <?php echo $r->id == $dokter_peminta_id ? 'selected' : ''; ?>><?php echo $r->nama; ?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama_dokter_luar_rs">Nama Dokter</label>
                            <input type="text" class="form-control" id="nama_dokter_luar_rs" placeholder="* Diisi Jika Permintaan Dari Luar RS" value="{nama_dokter_luar_rs}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="notelp_dokter_pengirim_luar_rs">No. Telepon Dokter Pengirim</label>
                            <input type="text" class="form-control" id="notelp_dokter_pengirim_luar_rs" placeholder="* Diisi Jika Permintaan Dari Luar RS" value="{notelp_dokter_pengirim_luar_rs}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama_fasilitas_pelayanana_kesehatan_luar_rs">Nama Fasilitas Pelayanan Kesehatan</label>
                            <input type="text" class="form-control" id="nama_fasilitas_pelayanana_kesehatan_luar_rs" placeholder="* Diisi Jika Permintaan Dari Luar RS" value="{nama_fasilitas_pelayanana_kesehatan_luar_rs}">
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <!-- First row -->
                <div class="row">
                    <!-- First set of col-md-4 -->
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="nomor_pendaftaran">Nomor Pendaftaran</label>
                            <input type="text" class="form-control" id="nomor_pendaftaran" placeholder="Nomor Pendaftaran" disabled value="{nomor_pendaftaran}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="nomor_permintaan">Nomor Permintaan</label>
                            <input type="text" class="form-control" id="nomor_permintaan" placeholder="Nomor Permintaan" disabled value="{nomor_permintaan}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="nomor_transaksi_radiologi">Nomor Transaksi Radiologi</label>
                            <input type="text" class="form-control" id="nomor_transaksi_radiologi" placeholder="Nomor Transaksi Radiologi" disabled value="{nomor_transaksi_radiologi}">
                        </div>
                    </div>
                </div>

                <!-- Second row -->
                <div class="row">
                    <!-- Second set of col-md-4 -->
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="waktu_pembuatan">Waktu Pembuatan</label>
                            <input type="text" class="form-control" id="waktu_pembuatan" placeholder="Waktu Pembuatan" disabled value="{waktu_pembuatan}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="waktu_permintaan">Waktu Permintaan</label>
                            <input type="text" class="form-control" id="waktu_permintaan" placeholder="Waktu Permintaan" disabled value="{waktu_permintaan}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="rencana_pemeriksaan">Rencana Pemeriksaan</label>
                            <input type="text" class="form-control" id="rencana_pemeriksaan" placeholder="Rencana Pemeriksaan" disabled value="{rencana_pemeriksaan}">
                        </div>
                    </div>
                </div>

                <!-- Third row -->
                <div class="row">
                    <!-- Third set of col-md-4 -->
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="diagnosa">Diagnosa</label>
                            <input type="text" class="form-control" id="diagnosa" placeholder="Diagnosa" disabled value="{diagnosa}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="catatan_permintaan">Catatan Permintaan</label>
                            <input type="text" class="form-control" id="catatan_permintaan" placeholder="Catatan Permintaan" disabled value="{catatan_permintaan}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="nomor_foto">No Foto</label>
                            <input type="text" class="form-control" id="nomor_foto" placeholder="No Foto" value="{nomor_foto}">
                        </div>
                    </div>
                </div>

                <!-- Fourth row -->
                <div class="row">
                    <!-- First set of col-md-4 with select elements -->
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="prioritas">Prioritas Pemeriksaan</label>
                            <select name="prioritas" id="prioritas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php foreach (list_variable_ref(249) as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?php echo $row->id == $prioritas ? 'selected' : ''; ?> <?php echo $row->id == $prioritas ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="pasien_puasa">Pasien Puasa</label>
                            <select name="pasien_puasa" id="pasien_puasa" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php foreach (list_variable_ref(87) as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?php echo $row->id == $pasien_puasa ? 'selected' : ''; ?> <?php echo $row->id == $pasien_puasa ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="pengiriman_hasil">Pengiriman Hasil</label>
                            <select name="pengiriman_hasil" id="pengiriman_hasil" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php foreach (list_variable_ref(88) as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?php echo $row->id == $pengiriman_hasil ? 'selected' : ''; ?> <?php echo $row->id == $pengiriman_hasil ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="pengiriman_hasil">Alergi Bahan Kontras</label>
                            <select name="alergi_bahan_kontras" id="alergi_bahan_kontras" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" <?= 'lihat_permintaan' == $status_form ? 'disabled' : ''; ?>>
                                <?php foreach (list_variable_ref(252) as $row) { ?>
                                    <option value="<?= $row->id; ?>" <?= '' == $alergi_bahan_kontras && '1' == $row->st_default ? 'selected' : ''; ?> <?= $row->id == $alergi_bahan_kontras ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="pengiriman_hasil">Pasien Hamil</label>
                            <select name="pasien_hamil" id="pasien_hamil" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" <?= 'lihat_permintaan' == $status_form ? 'disabled' : ''; ?>>
                                <?php foreach (list_variable_ref(253) as $row) { ?>
                                    <option value="<?= $row->id; ?>" <?= '' == $pasien_hamil && '1' == $row->st_default ? 'selected' : ''; ?> <?= $row->id == $pasien_hamil ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                        <label class="col-xs-12" for="">Dokter Radiologi</label>
                        <div class="col-xs-12">
                            <select name="dokter_radiologi" class="js-select2 form-control" id="dokter_radiologi" style="width: 100%;" data-placeholder="Pilih Opsi">
                            </select>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                        <label class="col-xs-12" for="">Jumlah Expose</label>
                        <div class="col-xs-12">
                            <input type="text" name="jumlah_expose" id="jumlah_expose" class="form-control" placeholder="Jumlah Expose" value="{jumlah_expose}">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                        <label class="col-xs-12" for="">Jumlah Film</label>
                        <div class="col-xs-12">
                            <input type="text" name="jumlah_film" id="jumlah_film" class="form-control" placeholder="Jumlah Film" value="{jumlah_film}">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                        <label class="col-xs-12" for="">QP</label>
                        <div class="col-xs-12">
                            <input type="text" name="qp" id="qp" class="form-control" placeholder="QP" value="{qp}">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                        <label class="col-xs-12" for="">MAS</label>
                        <div class="col-xs-12">
                            <input type="text" name="mas" id="mas" class="form-control" placeholder="MAS" value="{mas}">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                        <label class="col-xs-12" for="">Posisi</label>
                        <div class="col-xs-12">
                            <input type="text" name="posisi" id="posisi" class="form-control" placeholder="Posisi" value="{posisi}">
                        </div>
                        </div>
                    </div>
                </div>

                <!-- Sixth row -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-xs-12" for="">Waktu Pemeriksaan</label>
                            <div class="col-xs-12">
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group date">
                                    <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_pemeriksaan" id="tanggal_pemeriksaan" placeholder="HH/BB/TTTT" value="{tanggal_pemeriksaan}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                    <input type="text" class="time-datepicker form-control" name="waktu_pemeriksaan" id="waktu_pemeriksaan" value="{waktu_pemeriksaan}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="petugas_pemeriksaan">Petugas Pemeriksaan</label>
                            <select name="petugas_pemeriksaan" class="js-select2 form-control" id="petugas_pemeriksaan" style="width: 100%;" data-placeholder="Pilih Opsi">
                            </select>
                        </div>
                    </div>
                </div>

                <!-- Fifth row -->
                <div class="row">
                    <!-- First set of col-md-6 with input date and select elements -->
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-xs-12" for="">Waktu Penginputan Foto</label>
                            <div class="col-xs-12">
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group date">
                                    <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_penginputan_foto" id="tanggal_penginputan_foto" placeholder="HH/BB/TTTT" value="{tanggal_penginputan_foto}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                    <input type="text" class="time-datepicker form-control" name="waktu_penginputan_foto" id="waktu_penginputan_foto" value="{waktu_penginputan_foto}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="petugas_penginputan_foto">Petugas Penginputan Foto</label>
                            <select name="petugas_penginputan_foto" class="js-select2 form-control" id="petugas_penginputan_foto" style="width: 100%;" data-placeholder="Pilih Opsi">
                            </select>
                        </div>
                    </div>
                </div>

                <!-- Sixth row -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-xs-12" for="">Waktu Penginputan Expertise</label>
                            <div class="col-xs-12">
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group date">
                                    <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_penginputan_expertise" id="tanggal_penginputan_expertise" placeholder="HH/BB/TTTT" value="{tanggal_penginputan_expertise}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                    <input type="text" class="time-datepicker form-control" name="waktu_penginputan_expertise" id="waktu_penginputan_expertise" value="{waktu_penginputan_expertise}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="petugas_penginputan_expertise">Petugas Penginputan Expertise</label>
                            <select name="petugas_penginputan_expertise" class="js-select2 form-control" id="petugas_penginputan_expertise" style="width: 100%;" data-placeholder="Pilih Opsi">
                            </select>
                        </div>
                    </div>
                </div>

                <!-- Ninth row -->
                <div class="row">
                    <!-- Fifth set of col-md-6 with input date and select elements -->
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-xs-12" for="">Waktu Pengiriman Hasil</label>
                            <div class="col-xs-12">
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group date">
                                    <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_pengiriman_hasil" id="tanggal_pengiriman_hasil" placeholder="HH/BB/TTTT" value="{tanggal_pengiriman_hasil}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                    <input type="text" class="time-datepicker form-control" name="waktu_pengiriman_hasil" id="waktu_pengiriman_hasil" value="{waktu_pengiriman_hasil}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="petugas_pengiriman_hasil">Petugas Yang Mengirim Hasil</label>
                            <select name="petugas_pengiriman_hasil" class="js-select2 form-control" id="petugas_pengiriman_hasil" style="width: 100%;" data-placeholder="Pilih Opsi">
                            </select>
                        </div>
                    </div>
                </div>

                <!-- Tenth row -->
                <div class="row">
                    <!-- Sixth set of col-md-6 with input date and select elements -->
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-xs-12" for="">Waktu Permintaan Hasil oleh Unit</label>
                            <div class="col-xs-12">
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group date">
                                    <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_penerimaan_hasil" id="tanggal_penerimaan_hasil" placeholder="HH/BB/TTTT" value="{tanggal_penerimaan_hasil}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                    <input type="text" class="time-datepicker form-control" name="waktu_penerimaan_hasil" id="waktu_penerimaan_hasil" value="{waktu_penerimaan_hasil}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="petugas_penerimaan_hasil">Petugas Yang Menerima Hasil</label>
                            <select name="petugas_penerimaan_hasil" class="js-select2 form-control" id="petugas_penerimaan_hasil" style="width: 100%;" data-placeholder="Pilih Opsi">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        loadDataDokterRadiologi('{tujuan_radiologi}', '{dokter_radiologi}');
        loadDataPetugasProsesPemeriksaan('{tujuan_radiologi}', '{petugas_pemeriksaan}');
        loadDataPetugasPenginputanFoto('{tujuan_radiologi}', '{petugas_penginputan_foto}');
        loadDataPetugasPenginputanExpertise('{tujuan_radiologi}', '{petugas_penginputan_expertise}');
        loadDataPetugasPengirimanHasil('{tujuan_radiologi}', '{petugas_pengiriman_hasil}');
        loadDataPetugasPenerimaHasil('{tujuan_radiologi}', '{petugas_penerimaan_hasil}');

        $("#form-work input").on('blur', function() {
            updateDataRadiologi();
        });

        $("#form-work select").on('change', function() {
            updateDataRadiologi();
        });
    });

    function loadDataDokterRadiologi(value, selected) {
        $.ajax({
            url: '{site_url}term_radiologi_usg/dokter_radiologi/' + value,
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                $("#dokter_radiologi").select2('destroy');
                $("#dokter_radiologi").empty();

                response.dokter.map(function(dokter) {
                    var status = dokter.iddokter == selected ? 'selected' : '';
                    $("#dokter_radiologi").append('<option value="' + dokter.iddokter + '" ' + status + '>' + dokter.nama_dokter + '</option>');
                });

                $("#dokter_radiologi").select2();
            },
            error: function(error) {
                console.error('Error loading data:', error);
            }
        });
    }

    function loadDataPetugasProsesPemeriksaan(value, selected) {
        $.ajax({
            url: '{site_url}term_radiologi_usg/petugas_proses_pemeriksaan/' + value,
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                $("#petugas_pemeriksaan").select2('destroy');
                $("#petugas_pemeriksaan").empty();
                
                $("#petugas_pemeriksaan").append('<option value="" ' + (selected == '' ? "selected" : "") + '>Pilih Opsi</option>');
                
                response.petugas.map(function(petugas) {
                $("#petugas_pemeriksaan").append('<option value="' + petugas.iduser + '" ' + (petugas.iduser == selected ? "selected" : "") + '>' + petugas.nama_petugas + '</option>');
                });

                $("#petugas_pemeriksaan").select2();
            },
            error: function(error) {
                console.error('Error loading data:', error);
            }
        });
    }

    function loadDataPetugasPenginputanFoto(value, selected) {
        $.ajax({
            url: '{site_url}term_radiologi_usg/petugas_penginputan_foto/' + value,
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                $("#petugas_penginputan_foto").select2('destroy');
                $("#petugas_penginputan_foto").empty();
                
                $("#petugas_penginputan_foto").append('<option value="" ' + (selected == '' ? "selected" : "") + '>Pilih Opsi</option>');
                
                response.petugas.map(function(petugas) {
                $("#petugas_penginputan_foto").append('<option value="' + petugas.iduser + '" ' + (petugas.iduser == selected ? "selected" : "") + '>' + petugas.nama_petugas + '</option>');
                });

                $("#petugas_penginputan_foto").select2();
            },
            error: function(error) {
                console.error('Error loading data:', error);
            }
        });
    }

    function loadDataPetugasPenginputanExpertise(value, selected) {
        $.ajax({
            url: '{site_url}term_radiologi_usg/petugas_penginputan_expertise/' + value,
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                $("#petugas_penginputan_expertise").select2('destroy');
                $("#petugas_penginputan_expertise").empty();
                
                $("#petugas_penginputan_expertise").append('<option value="" ' + (selected == '' ? "selected" : "") + '>Pilih Opsi</option>');

                response.petugas.map(function(petugas) {
                $("#petugas_penginputan_expertise").append('<option value="' + petugas.iduser + '" ' + (petugas.iduser == selected ? "selected" : "") + '>' + petugas.nama_petugas + '</option>');
                });

                $("#petugas_penginputan_expertise").select2();
            },
            error: function(error) {
                console.error('Error loading data:', error);
            }
        });
    }

    function loadDataPetugasPengirimanHasil(value, selected) {
        $.ajax({
            url: '{site_url}term_radiologi_usg/petugas_pengiriman_hasil/' + value,
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                $("#petugas_pengiriman_hasil").select2('destroy');
                $("#petugas_pengiriman_hasil").empty();
                
                $("#petugas_pengiriman_hasil").append('<option value="" ' + (selected == '' ? "selected" : "") + '>Pilih Opsi</option>');

                response.petugas.map(function(petugas) {
                $("#petugas_pengiriman_hasil").append('<option value="' + petugas.iduser + '" ' + (petugas.iduser == selected ? "selected" : "") + '>' + petugas.nama_petugas + '</option>');
                });

                $("#petugas_pengiriman_hasil").select2();
            },
            error: function(error) {
                console.error('Error loading data:', error);
            }
        });
    }

    function loadDataPetugasPenerimaHasil(value, selected) {
        $.ajax({
            url: '{site_url}term_radiologi_usg/petugas_penerimaan_hasil/' + value,
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                $("#petugas_penerimaan_hasil").select2('destroy');
                $("#petugas_penerimaan_hasil").empty();
                
                $("#petugas_penerimaan_hasil").append('<option value="" ' + (selected == '' ? "selected" : "") + '>Pilih Opsi</option>');

                response.petugas.map(function(petugas) {
                $("#petugas_penerimaan_hasil").append('<option value="' + petugas.iduser + '" ' + (petugas.iduser == selected ? "selected" : "") + '>' + petugas.nama_petugas + '</option>');
                });

                $("#petugas_penerimaan_hasil").select2();
            },
            error: function(error) {
                console.error('Error loading data:', error);
            }
        });
    }

    function updateDataRadiologi() {
        var payload = {
            transaksi_id: '{transaksi_id}',
            nama_dokter_luar_rs: $("#nama_dokter_luar_rs").val(),
            notelp_dokter_pengirim_luar_rs: $("#notelp_dokter_pengirim_luar_rs").val(),
            nama_fasilitas_pelayanana_kesehatan_luar_rs: $("#nama_fasilitas_pelayanana_kesehatan_luar_rs").val(),
            nomor_foto: $("#nomor_foto").val(),
            prioritas: $("#prioritas option:selected").val(),
            pasien_puasa: $("#pasien_puasa option:selected").val(),
            pengiriman_hasil: $("#pengiriman_hasil option:selected").val(),
            alergi_bahan_kontras: $("#alergi_bahan_kontras option:selected").val(),
            pasien_hamil: $("#pasien_hamil option:selected").val(),
            dokter_radiologi: $("#dokter_radiologi option:selected").val(),
            jumlah_expose: $("#jumlah_expose").val(),
            jumlah_film: $("#jumlah_film").val(),
            qp: $("#qp").val(),
            mas: $("#mas").val(),
            posisi: $("#posisi").val(),
            tanggal_pemeriksaan: $("#tanggal_pemeriksaan").val(),
            waktu_pemeriksaan: $("#waktu_pemeriksaan").val(),
            petugas_pemeriksaan: $("#petugas_pemeriksaan option:selected").val(),
            tanggal_penginputan_foto: $("#tanggal_penginputan_foto").val(),
            waktu_penginputan_foto: $("#waktu_penginputan_foto").val(),
            petugas_penginputan_foto: $("#petugas_penginputan_foto option:selected").val(),
            tanggal_penginputan_expertise: $("#tanggal_penginputan_expertise").val(),
            waktu_penginputan_expertise: $("#waktu_penginputan_expertise").val(),
            petugas_penginputan_expertise: $("#petugas_penginputan_expertise option:selected").val(),
            tanggal_pengiriman_hasilpertise: $("#tanggal_pengiriman_hasilpertise").val(),
            waktu_pengiriman_hasil: $("#waktu_pengiriman_hasil").val(),
            petugas_pengiriman_hasil: $("#petugas_pengiriman_hasil option:selected").val(),
            tanggal_penerimaan_hasil: $("#tanggal_penerimaan_hasil").val(),
            waktu_penerimaan_hasil: $("#waktu_penerimaan_hasil").val(),
            petugas_penerimaan_hasil: $("#petugas_penerimaan_hasil option:selected").val(),
        };

        $.ajax({
            url: '{site_url}term_radiologi_usg/update_data_radiologi', 
            type: 'POST',
            data: payload,
            success: function (result) {
                $.toaster({ priority: 'success', title: 'Success!', message: 'Perubahan Data berhasil disimpan' });
            },
            error: function (error) {
                // Handle error if deletion fails
                console.error('Error cloning order:', error);
                alert('Error cloning order. Please try again.');
            }
        });
    }
</script>