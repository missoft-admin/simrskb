<?php declare(strict_types=1);
echo ErrorSuccess($this->session); ?>
<?php if ('' !== $error) {
    echo ErrorMessage($error);
} ?>
<?php $user_id = $this->session->userdata('user_id'); ?>

<div class="block">
    <div class="block-content">
        <h4>RIWAYAT RADIOLOGI</h4> <br>

        <form>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="tglPermintaanDari">Tanggal Permintaan</label>
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" autocomplete="off" id="tglPermintaanDari" placeholder="From" value="{tanggal}">
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" autocomplete="off" id="tglPermintaanSampai" placeholder="To" value="{tanggal}">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="tglPendaftaranDari">Tanggal Pendaftaran</label>
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" autocomplete="off" id="tglPendaftaranDari" placeholder="From" value="">
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" autocomplete="off" id="tglPendaftaranSampai" placeholder="To" value="">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="nomorRegistrasi">Nomor Registrasi</label>
                        <input type="text" class="form-control" id="nomorRegistrasi">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="tujuanKlinik">Tujuan Klinik</label>
                        <select class="js-select2 form-control" id="tujuanKlinik">
                            <option value="0">Semua</option>
                            <?php foreach (get_all('mpoliklinik', ['status' => '1']) as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="tujuanDokter">Tujuan Dokter</label>
                        <select class="js-select2 form-control" id="tujuanDokter">
                            <option value="0">Semua</option>
                            <?php foreach (get_all('mdokter', ['status' => '1']) as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="tujuanRadiologi">Tujuan Radiologi</label>
                        <select class="js-select2 form-control" id="tujuanRadiologi">
                            <option value="0">Semua</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="">&nbsp;</label>
                        <button class="btn btn-success text-uppercase" type="button" id="btn-filter" style="width:100%;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
        </form>

        <!-- Horizontal Line -->
        <hr>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="table_riwayat_order">
                        <thead>
                            <tr>
                                <th width="10%">Aksi</th>
                                <th width="10%">Tipe</th>
                                <th width="10%">Nomor Registrasi</th>
                                <th width="10%">Waktu Peminta</th>
                                <th width="10%">Nomor Permintaan</th>
                                <th width="10%">Tujuan Dokter & Klinik</th>
                                <th width="10%">Dokter Peminta</th>
                                <th width="10%">Diagnosa</th>
                                <th width="10%">Tujuan Radiologi</th>
                                <th width="10%">Prioritas</th>
                                <th width="10%">Status Pemeriksaan</th>
                                <th width="10%">Dibuat Oleh</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        loadDataRiwayatRadiologi();

        $(document).on("click", "#btn-filter", function() {
            $("#cover-spin").show();

            loadDataRiwayatRadiologi();
        });
    });

    function loadDataRiwayatRadiologi() {
        let tanggal_permintaan_dari = $("#tglPermintaanDari").val();
        let tanggal_permintaan_sampai = $("#tglPermintaanSampai").val();
        let tanggal_pendaftaran_dari = $("#tglPendaftaranDari").val();
        let tanggal_pendaftaran_sampai = $("#tglPendaftaranSampai").val();
        let nomor_registrasi = $("#nomorRegistrasi").val();
        let tujuan_klinik = $("#tujuanKlinik option:selected").val();
        let tujuan_dokter = $("#tujuanDokter option:selected").val();
        let tujuan_radiologi = $("#tujuanRadiologi option:selected").val();

        $('#table_riwayat_order').DataTable().destroy();
        $('#table_riwayat_order').DataTable({
            "autoWidth": false,
            "searching": true,
            "pageLength": 50,
            "serverSide": true,
            "processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
            "columnDefs": [
                {
                    "width": "5%",
                    "targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                    "className": "text-center"
                },
            ],
            ajax: {
                url: '{site_url}term_radiologi_usg/getIndexRiwayatRadiologi/{pasien_id}',
                type: "POST",
                dataType: 'json',
                data: {
                    tanggal_permintaan_dari: tanggal_permintaan_dari,
                    tanggal_permintaan_sampai: tanggal_permintaan_sampai,
                    tanggal_pendaftaran_dari: tanggal_pendaftaran_dari,
                    tanggal_pendaftaran_sampai: tanggal_pendaftaran_sampai,
                    nomor_registrasi: nomor_registrasi,
                    tujuan_klinik: tujuan_klinik,
                    tujuan_dokter: tujuan_dokter,
                    tujuan_radiologi: tujuan_radiologi,
                }
            }
        });

        $("#cover-spin").hide();
    }

    function clonePermintaan(asalRujukan, transaksiId) {
        swal({
            title: "Konfirmasi Duplikasi Permintaan",
            text: "Apakah Anda yakin ingin membuat transaksi permintaan dari duplikasi?",
            type: "info",
            showCancelButton: true,
            confirmButtonText: "Ya",
            confirmButtonColor: "#34a263",
            cancelButtonText: "Batalkan",
        }).then((willSubmit) => {
            if (willSubmit) {
                $("#cover-spin").show();

                $.ajax({
                    url: '{site_url}term_radiologi_usg/clone_permintaan/' + transaksiId, 
                    dataType: "JSON",
                    success: function (result) {
                        $.toaster({priority: 'success', title: 'Berhasil!', message: 'Data berhasil diduplikasi.'});
                        window.location = '{base_url}term_radiologi_usg/tindakan/' + asalRujukan + '/' + '{pendaftaran_id}' + '/erm_rad/usg_permintaan';
                    },
                    error: function (error) {
                        // Handle error if deletion fails
                        console.error('Error cloning order:', error);
                        alert('Error cloning order. Please try again.');
                    }
                });
            }
        });
    }
    
    function cetakPermintaan(transaksiId) {
        window.open('{base_url}term_radiologi_usg/cetak_pemeriksaan/' + transaksiId, '_blank');
    }
    
    function lihatPemeriksaan(transaksiId) {
    
    }
</script>