<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?php echo form_open('tkas/save','class="form-horizontal" id="form-work"') ?>
		<div class="form-group">
			<label class="col-md-3 control-label" for="">No. Transaksi</label>
			<div class="col-md-7">
				<input class="form-control" type="text" readonly value="{notransaksi}">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="">Nama Transaksi</label>
			<div class="col-md-7">
				<input class="form-control" type="text" readonly value="{namatransaksi}">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="">Nominal Transaksi</label>
			<div class="col-md-7">
				<input class="form-control" type="text" readonly value="<?=number_format($nominaltransaksi)?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label" for="example">Tanggal Transaksi</label>
			<div class="col-md-7">
				<input class="js-datepicker form-control" type="text" name="tanggal" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" value="{tanggaltransaksi}">
			</div>
		</div>

		<hr class="m-b-xs m-t-xs">

		<div class="row">
			<div class="col-sm-12">
				<button style="float:right;" type="button" class="btn btn-primary debit_modal" data-toggle="modal" data-target="#modalDebit" title="Edit"><i class="fa fa-plus"></i> Debit (Biaya)</button>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<input type="hidden" class="form-control" id="debitRowIndex">
				<input type="hidden" class="form-control" id="debitNumber">
				<div class="table-responsive">
				<table id="listDebitTable" class="table table-bordered table-striped table-responsive" style="margin-top:10px">
					<thead>
						<tr>
							<th width="20%">Nama Biaya</th>
							<th width="20%">Noakun</th>
							<th width="20%">Keterangan</th>
							<th width="20%">Nominal</th>
							<th width="20%">Aksi</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="3" style="font-weight: bold;text-align: center;">TOTAL</td>
							<td colspan="2" style="font-weight: bold;" id="totalDebit">0</td>
						</tr>
					</tfoot>
				</table>
			</div>
			</div>
		</div>

		<hr class="m-b-xs m-t-xs">

		<div class="row">
			<div class="col-sm-12">
				<button style="float:right;" type="button" class="btn btn-primary kredit_modal" data-toggle="modal" data-target="#modalKredit" title="Edit"><i class="fa fa-plus"></i> Kredit (Pembayaran)</button>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<input type="hidden" class="form-control" id="kreditRowIndex">
				<input type="hidden" class="form-control" id="kreditNumber">
				<div class="table-responsive">
				<table id="listKreditTable" class="table table-bordered table-striped table-responsive" style="margin-top:10px">
					<thead>
						<tr>
							<th width="10%">Tipe Transaksi</th>
							<th width="10%">Sumber Dana</th>
							<th width="10%">No. Akun</th>
							<th width="10%">Metode</th>
							<th width="10%">Keterangan</th>
							<th width="10%">Nominal</th>
							<th width="20%">Action</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="5" style="font-weight: bold;text-align: center;">TOTAL</td>
							<td colspan="2" style="font-weight: bold;" id="totalKredit">0</td>
						</tr>
					</tfoot>
				</table>
			</div>
			</div>
		</div>
	</div>
	<footer class="panel-footer text-right bg-light lter">
		<button class="btn btn-info" type="submit">Simpan</button>
		<a href="{base_url}tkas" class="btn btn-default" type="button">Batalkan</a>
	</footer>

	<input type="hidden" name="iddetailpencairan" value="{iddetailpencairan}">
	<input type="hidden" id="debitvalue" name="debitvalue">
	<input type="hidden" id="kreditvalue" name="kreditvalue">

	<?php echo form_close() ?>
</div>

<div class="modal fade" id="modalDebit" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-popin">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Biaya</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal">
						<div class="form-group">
							<label class="col-md-3 control-label" for="debitBiaya">Biaya</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="debitBiaya" placeholder="Biaya" value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="debitNoAkun">No. Akun</label>
							<div class="col-md-7">
								<select class="select2 form-control" id="debitNoAkun" style="width: 100%;" data-placeholder="No. Akun">
									<option value="#">Pilih Opsi</option>
									<?php foreach($list_akundebit as $row){ ?>
										<option value="<? echo $row->noakun ?>"><?php echo $row->noakun.' '.$row->namaakun; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="debitKeterangan">Keterangan</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="debitKeterangan" placeholder="Keterangan" value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="nominal">Nominal</label>
							<div class="col-md-7">
								<input type="text" class="form-control number" id="debitNominal" placeholder="Nominal" value="">
							</div>
						</div>
						<input type="hidden" id="debitId" readonly>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batalkan</button>
				<button class="btn btn-sm btn-primary" type="button" id="debitAdd" data-dismiss="modal">Tambahkan</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalKredit" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-popin">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Pembayaran</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal">
						<div class="form-group">
							<label class="col-md-4 control-label">Tipe Transaksi</label>
							<div class="col-md-6">
								<select class="form-control" id="kreditIdTipe" style="width: 100%;" data-placeholder="Pilih Tipe Transaksi">
									<option value="#">Pilih Opsi</option>
									<option value="1">Kas Kecil</option>
									<option value="2">Kas Besar</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Sumber Dana</label>
							<div class="col-md-6">
								<select class="select2 form-control" id="kreditSumberDana" style="width: 100%;" data-placeholder="Pilih Sumber Dana">
									<option value="#">Pilih Opsi</option>
									<?php foreach($list_akunkredit as $row){ ?>
										<option value="<? echo $row->noakun ?>"><?php echo $row->namaakun; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">No. Akun</label>
							<div class="col-md-6">
								<input class="form-control" type="text" id="kreditNoAkun" readonly placeholder="No. Akun">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Metode Pembayaran</label>
							<div class="col-md-6">
								<select class="form-control" id="kreditMetodePembayaran" style="width: 100%;" data-placeholder="Pilih Metode Pembayaran">
									<option value="#">Pilih Opsi</option>
									<option value="1">Tunai</option>
									<option value="2">Cheq</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Keterangan</label>
							<div class="col-md-6">
								<input class="form-control" type="text" id="kreditKeterangan" placeholder="Keterangan">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Nominal</label>
							<div class="col-md-6">
								<input class="form-control number" type="text" id="kreditNominal" placeholder="Nominal">
							</div>
						</div>
						<input type="hidden" id="kreditId" readonly>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batalkan</button>
				<button class="btn btn-sm btn-primary" type="button" id="kreditAdd" data-dismiss="modal">Tambahkan</button>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.number').number(true, 0, '.', ',');

		// Debit
		$("#debitAdd").click(function() {
			var rowindex;
			var duplicate = false;

			if ($("#debitRowIndex").val() != '') {
				var content = "";
				var number = $("#debitNumber").val();
				rowindex = $("#debitRowIndex").val();
			} else {
				var number = $('#listDebitTable tr').length;
				var content = "<tr>";
			}

			content += "<td style='display:none'>" + number + "</td>";
			content += "<td>" + $("#debitBiaya").val() + "</td>";
			content += "<td>" + $("#debitNoAkun option:selected").val() + "</td>";
			content += "<td>" + $("#debitKeterangan").val() + "</td>";
			content += "<td>" + $.number($("#debitNominal").val()); + "</td>";
			content += "<td><button type='button' class='btn btn-primary debitEdit' data-toggle='modal' data-target='#modalDebit' title='Ubah'><i class='fa fa-pencil'></i> Ubah</button> <button type='button' class='btn btn-danger debitRemove' title='hapus'><i class='fa fa-trash-o'></i> Hapus</button></td>";

			if ($("#debitRowIndex").val() != '') {
				$('#listDebitTable tbody tr:eq(' + rowindex + ')').html(content);
			} else {
				content += "</tr>";
				$('#listDebitTable tbody').append(content);
			}

			clearFormDebit();
			checkTotalDebit();
		});

		$(document).on("click", ".debitEdit", function() {
			$("#debitRowIndex").val($(this).closest('tr')[0].sectionRowIndex);
			$("#debitId").val($(this).closest('tr').find("td:eq(0)").html());
			$("#debitBiaya").val($(this).closest('tr').find("td:eq(1)").html());
			$("#debitNoAkun").val($(this).closest('tr').find("td:eq(2)").html());
			$("#debitKeterangan").val($(this).closest('tr').find("td:eq(3)").html());
			$("#debitNominal").val($(this).closest('tr').find("td:eq(4)").html());

			return false;
		});

		$(document).on("click", ".debitRemove", function() {
			if (confirm("Hapus data ?") == true) {
				$(this).closest('td').parent().remove();
				checkTotalDebit();
			}
			return false;
		});

		$("#kreditSumberDana").change(function() {
			$("#kreditNoAkun").val($(this).val());
		});

		$("#kreditAdd").click(function() {
			if ($("#kreditIdTipe option:selected").val() == '#') {
				sweetAlert("Maaf...", "Tipe Transaksi Belum Dipilih!", "error");
				return false;
			}

			var rowindex;
			var duplicate = false;

			if ($("#kreditRowIndex").val() != '') {
				var content = "";
				var number = $("#kreditNumber").val();
				rowindex = $("#kreditRowIndex").val();
			} else {
				var number = $('#listKreditTable tr').length;
				var content = "<tr>";
			}

			content += "<td style='display:none'>" + number + "</td>";
			content += "<td style='display:none'>" + $("#kreditIdTipe option:selected").val() + "</td>";
			content += "<td>" + $("#kreditIdTipe option:selected").text() + "</td>";
			content += "<td style='display:none'>" + $("#kreditSumberDana option:selected").val() + "</td>";
			content += "<td>" + $("#kreditSumberDana option:selected").text() + "</td>";
			content += "<td>" + $("#kreditNoAkun").val() + "</td>";
			content += "<td style='display:none'>" + $("#kreditMetodePembayaran option:selected").val() + "</td>";
			content += "<td>" + $("#kreditMetodePembayaran option:selected").text() + "</td>";
			content += "<td>" + $("#kreditKeterangan").val(); + "</td>";
			content += "<td>" + $("#kreditNominal").val(); + "</td>";
			content += "<td><button type='button' class='btn btn-primary kreditEdit' data-toggle='modal' data-target='#modalKredit' title='Ubah'><i class='fa fa-pencil'></i> Ubah</button> <button type='button' class='btn btn-danger kreditRemove' title='hapus'><i class='fa fa-trash-o'></i> Hapus</button></td>";

			if ($("#kreditRowIndex").val() != '') {
				$('#listKreditTable tbody tr:eq(' + rowindex + ')').html(content);
			} else {
				content += "</tr>";
				$('#listKreditTable tbody').append(content);
			}

			clearFormKredit();
			checkTotalKredit();
		});

		$(document).on("click", ".kreditEdit", function() {
			$("#kreditRowIndex").val($(this).closest('tr')[0].sectionRowIndex);
			$("#kreditId").val($(this).closest('tr').find("td:eq(0)").html());

			$("#kreditIdTipe").val($(this).closest('tr').find("td:eq(1)").html());
			$("#kreditSumberDana").val($(this).closest('tr').find("td:eq(3)").html());
			$("#kreditNoAkun").val($(this).closest('tr').find("td:eq(5)").html());
			$("#kreditMetodePembayaran").val($(this).closest('tr').find("td:eq(6)").html());
			$("#kreditKeterangan").val($(this).closest('tr').find("td:eq(8)").html());
			$("#kreditNominal").val($(this).closest('tr').find("td:eq(9)").html());

			return false;
		});

		$(document).on("click", ".kreditRemove", function() {
			if (confirm("Hapus data ?") == true) {
				$(this).closest('td').parent().remove();
				checkTotalKredit();
			}
			return false;
		});

		// Submit Form
		$("#form-work").submit(function(e) {
			var form = this;

			if ($("#tanggal").val() == "") {
				e.preventDefault();
				sweetAlert("Maaf...", "Tanggal tidak boleh kosong!", "error");
				return false;
			}

			var debitTbl = $('table#listDebitTable tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					return $(cell).html();
				});
			});

			var kreditTbl = $('table#listKreditTable tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					return $(cell).html();
				});
			});

			$("#debitvalue").val(JSON.stringify(debitTbl));
			$("#kreditvalue").val(JSON.stringify(kreditTbl));

			swal({
				title: "Berhasil!",
				text: "Proses penyimpanan data.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});
	});

	function clearFormDebit() {
		$("#debitNumber").val('');
		$("#debitRowIndex").val('');
		$("#debitBiaya").val('');
		$("#debitNoAkun").val('#');
		$("#debitKeterangan").val('');
		$("#debitNominal").val('');
	}

	function clearFormKredit() {
		$("#kreditNumber").val('');
		$("#kreditRowIndex").val('');
		$("#kreditIdTipe").val('#');
		$("#kreditSumberDana").val('#');
		$("#kreditMetodePembayaran").val('#');
		$("#kreditNoAkun").val('');
		$("#kreditNominal").val('');
		$("#kreditKeterangan").val('');
	}

	function checkTotalDebit() {
		var totalDebit = 0;
		$('#listDebitTable tbody tr').each(function() {
			totalDebit += parseFloat($(this).find('td:eq(4)').text().replace(/\,/g, ""));
		});
		$("#totalDebit").html($.number(totalDebit));
	}

	function checkTotalKredit() {
		var totalKredit = 0;
		$('#listKreditTable tbody tr').each(function() {
			totalKredit += parseFloat($(this).find('td:eq(9)').text().replace(/\,/g, ""));
		});
		$("#totalKredit").html($.number(totalKredit));
	}
</script>
