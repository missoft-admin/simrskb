<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<?
$user_id=$this->session->userdata('user_id');
?>
<div class="block push-10">
	<div class="block-content bg-primary">
			<input type="hidden" id="tab" name="tab" value="{tab}">
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<input type="hidden" id="st_login" name="st_login" value="{st_login}">
			<div class="row pull-10">
				
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">Tanggal</label>
						<div class="col-xs-12">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                <input class="form-control" type="text" id="tanggal_1" placeholder="From" value="{tanggal_1}">
                                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                <input class="form-control" type="text" id="tanggal_2" placeholder="To" value="{tanggal_2}">
                            </div>
						</div>
					</div>
				</div>
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">No Medrec</label>
						<div class="col-xs-12">
							<input type="text" class="form-control" id="no_medrec" value="">
						</div>
					</div>
				</div>
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">Nama Pasien</label>
						<div class="col-xs-12">
							<input type="text" class="form-control" id="nama_pasien" value="">
						</div>
					</div>
				</div>
				
				<div class="col-md-1 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">&nbsp;&nbsp;</label>
						<div class="col-xs-12">
							<span class="input-group-btn ">
								<button class="btn btn-warning btn-block " id="btn_cari" type="button" title="Login"><i class="fa fa-search pull-left"></i> Cari</button>&nbsp;
							</span>
						</div>
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
	</div>
</div>

<div class="block">
	<ul class="nav nav-pills">
		<li id="div_1" class="<?=($tab=='1'?'active':'')?>">
			<a href="javascript:void(0)" onclick="set_tab(1)"><i class="si si-list"></i> Menunggu Persetujuan</a>
		</li>
		<li  id="div_2" class="<?=($tab=='2'?'active':'')?>">
			<a href="javascript:void(0)"  onclick="set_tab(2)"><i class="si si-pin"></i> Selesai Persetujuan </a>
		</li>
		<li  id="div_3" class="<?=($tab=='3'?'active':'')?>">
			<a href="javascript:void(0)" onclick="set_tab(3)"><i class="fa fa-check-square-o"></i> Setuju</a>
		</li>
		<li  id="div_4" class="<?=($tab=='4'?'active':'')?>">
			<a href="javascript:void(0)" onclick="set_tab(4)"><i class="fa fa-times"></i> Tolak</a>
		</li>
		<li  id="div_5" class="<?=($tab=='5'?'active':'')?>">
			<a href="javascript:void(0)" onclick="set_tab(5)"><i class="fa fa-list-ul"></i> Semua</a>
		</li>
		
	</ul>
	<div class="block-content">
			<input type="hidden" id="tab" name="tab" value="{tab}">
			<div class="row   ">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table" id="index_all">
							<thead>
								<tr>
									<th width="10%">Action</th>
									<th width="10%">Step</th>
									<th width="10%">Alasan Batal</th>
									<th width="10%">Yang Mengajukan</th>
									<th width="10%">Tipe Deposit</th>
									<th width="10%">Pasien</th>
									<th width="10%">No Transaksi</th>
									<th width="8%">Jenis Deposit</th>
									<th width="8%">Metode</th>
									<th width="8%">Nominal</th>
									<th width="8%">Bukti</th>
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		
	
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_user" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12">
							<table width="100%" id="tabel_user_proses" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th style="width:25%">Step</th>
										<th style="width:25%">Deskripsi</th>
										<th style="width:25%">User</th>
										<th style="width:25%">Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

					</div>
					<?php echo form_close() ?>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>
<div class="modal fade" id="modal_tolak" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Alasan Tolak</h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Alasan</label>
								<div class="col-md-8">
									<textarea class="form-control" rows="2" name="alasan_tolak" id="alasan_tolak"></textarea>
									<input type="hidden" readonly class="form-control" id="id_approval"  placeholder="id_approval" name="id_approval" value="">
								</div>
							</div>
						</div>
					</div>

				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-danger text-uppercase" type="button" name="btn_tolak" id="btn_tolak"><i class="fa fa-save"></i> SIMPAN TOLAK</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<? $this->load->view('Tverifikasi_transaksi/modal/modal_deposit_header')?>
<script type="text/javascript">
var table;
var tab=<?=$tab?>;
var st_login;

$(document).ready(function(){	
	load_index_all();
	
});

function list_user(idtrx){
	$("#modal_user").modal('show');

	// alert(idrka);
	$.ajax({
		url: '{site_url}ttindakan_ranap_deposit/list_user/'+idtrx,
		dataType: "json",
		success: function(data) {
			$("#tabel_user_proses tbody").empty();
			$("#tabel_user_proses tbody").append(data.detail);
		}
	});
}
$(document).on("click",".setuju",function(){
	var table = $('#index_all').DataTable()
	var tr = $(this).parents('tr')
	// var id = table.cell(tr,0).data()
	var id_approval=$(this).data('id');
	swal({
		title: "Apakah Anda Yakin  ?",
		text : "Menyetujui Pembatalan Deposit ini?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#d26a5c",
		cancelButtonText: "Tidak Jadi",
	}).then(function() {
		$.ajax({
			url: '{site_url}tdeposit_approval/setuju_batal/'+id_approval+'/1',
			type: 'POST',
			success: function(data) {
				// console.log(data);
				// alert(data);
				// $.toaster({priority : 'success', title : 'Succes!', message : ' Deposit berhasil disetujui'});
				$('#index_all').DataTable().ajax.reload( null, false );
				// if (data=='"1"'){
				// window.location.href = "{site_url}mrka_approval/next_bendahara/"+id;
				// }
			}
		});
	});

	return false;

});
$(document).on("click","#btn_tolak",function(){

	var id_approval=$("#id_approval").val();
	var alasan_tolak=$("#alasan_tolak").val();
	swal({
		title: "Apakah Anda Yakin  ?",
		text : "Menolak Pembatalan Deposit ini?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#d26a5c",
		cancelButtonText: "Tidak Jadi",
	}).then(function() {
		$.ajax({
			url: '{site_url}tdeposit_approval/tolak/',
			type: 'POST',
			data: {
				id_approval:id_approval,
				alasan_tolak:alasan_tolak,
			},
			complete: function() {
				$('#modal_tolak').modal('hide');
				$.toaster({priority : 'success', title : 'Succes!', message : ' Deposit berhasil ditolak'});
				$('#index_all').DataTable().ajax.reload( null, false );

			}
		});
	});

	return false;

});
$(document).on("click",".tolak",function(){
	var table = $('#index_all').DataTable()
	var tr = $(this).parents('tr')
	var id_approval=$(this).data('id');
	$("#id_approval").val(id_approval)
	$('#modal_tolak').modal('show');
	// modal_tolak

});
$(document).on("click","#btn_cari",function(){
	load_index_all();
});
function set_tab($tab){
	tab=$tab;
	// alert(tab);
	// $("#cover-spin").show();
	document.getElementById("div_1").classList.remove("active");
	document.getElementById("div_2").classList.remove("active");
	document.getElementById("div_3").classList.remove("active");
	document.getElementById("div_4").classList.remove("active");
	document.getElementById("div_5").classList.remove("active");
	if (tab=='1'){
		document.getElementById("div_1").classList.add("active");
	}
	if (tab=='2'){
		document.getElementById("div_2").classList.add("active");
	}
	if (tab=='3'){
		document.getElementById("div_3").classList.add("active");
	}
	if (tab=='4'){
		document.getElementById("div_4").classList.add("active");
	}
	if (tab=='5'){
		document.getElementById("div_5").classList.add("active");
	}
	
	load_index_all();
}

function load_index_all(){
	$('#index_all').DataTable().destroy();	
	$("#cover-spin").show();
	let tanggal_1=$("#tanggal_1").val();
	let tanggal_2=$("#tanggal_2").val();
	let no_medrec=$("#no_medrec").val();
	let nama_pasien=$("#nama_pasien").val();
	
	// alert(ruangan_id);
	table = $('#index_all').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			
			"processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					// { "width": "10%", "targets": 0},
					// { "width": "50%", "targets": 1},
					// { "width": "30%", "targets": 2},
					// { "width": "10%", "targets": 3},
					
				],
            ajax: { 
                url: '{site_url}tdeposit_approval/getIndex_all', 
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						no_medrec:no_medrec,
						nama_pasien:nama_pasien,
						tab:tab,
						
					   }
            },
			"drawCallback": function( settings ) {
				$("#cover-spin").hide();
			 }  
        });
}

</script>