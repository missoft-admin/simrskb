<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tpengajuan" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
        <hr style="margin-bottom:0px;">
	</div>
	<?php echo form_open_multipart('tpengajuan/savePersetujuan','class="form-horizontal push-10-t" id="form-work"') ?>
	<div class="block-content  block-content-narrow">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label text-uppercase">No. Pengajuan</label>
					<div class="col-md-10">
						<input type="hidden" name="id" value="{id}">
						<input type="text" class="form-control" readonly placeholder="No. Pengajuan" value="{nopengajuan}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label text-uppercase">Subjek</label>
					<div class="col-md-10">
						<input type="text" class="form-control" readonly placeholder="Subjek" value="{subjek}">
					</div>
				</div>
			</div>
		</div>
	</div>

	<input type="hidden" class="form-control" id="rowindex" />

	<div class="block-content">
		<span class="badge badge-primary text-uppercase" style="font-size: 12px;">Meminta Persetujuan</span>
		<br><br>
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="assignmentList">
			<thead>
				<tr>
					<th style="width:10%">Nama Pegawai</th>
					<th style="width:10%">Aksi</th>
				</tr>
				<tr>
					<th>
						<select id="idpegawai" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="" disabled selected>Pilih Opsi</option>
							<?php foreach  ($datapegawai as $row) { ?>
							<option value="<?=$row->id;?>">
								<?=$row->name?>
							</option>
							<?php } ?>
						</select>
					</th>
					<th>
                        <a id="assignmentAdd" class="btn btn-default"><span style="font-size: 12px;">Tambahkan</span></a>
					</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>

	<div class="block-content">
		<span class="badge badge-primary text-uppercase" style="font-size: 10px;">Memo</span><br><br>
		<textarea class="form-control js-summernote" placeholder="Memo" name="memo"></textarea>
	</div>

	<div class="block-content">
		<div class="row">
			<div class="col-sm-12">
                <hr style="margin-top:0px;">
                <div style="float: right;">
                    <button class="btn btn-success text-uppercase" type="submit" style="width: 100px;font-size: 12px;">Simpan</button>
                    <button class="btn btn-danger text-uppercase" type="reset" style="width: 100px;font-size: 12px;"><i class="fa fa-undo"></i>&nbsp;Batal</button>
                </div>
			</div>
		</div>

		<input type="hidden" name="steppersetujuan" value="{stepPersetujuan}">
		<input type="hidden" id="assignmentValue" name="assignmentValue">
		<input type="hidden" id="countAssignment">
        <input type="hidden" name="kategori" value="{kategori}">
		<br><br>
	</div>

    <!-- this is a value of idpengajuan ! -->
    <input type="hidden" name="idpengajuan" value="{id}">
	<?php echo form_close() ?>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$(".number").number(true, 0, '.', ',');

		$("#assignmentAdd").click(function () {

			countAssignment = $('table#assignmentList tbody tr').length + 1;
			$('#countAssignment').val(countAssignment);

			var valid = validateAssigment();
			if (!valid) return false;

			var rowIndex;
			var duplicate = false;

			if ($("#rowindex").val() != '') {
				var content = "";
				rowIndex = $("#rowindex").val();
			} else {
				var content = "<tr>";
				$('#assignmentList tbody tr').filter(function () {
					var $cells = $(this).children('td');
					if ($cells.eq(0).text() === $("#idpegawai").val()) {
						sweetAlert("Maaf...", "Pegawai " + $("#idpegawai option:selected").text() + " sudah ditambahkan.", "error");
						duplicate = true;
						return false;
					}
				});
			}

			if (duplicate == false) {
				content += "<td style='display:none'>" + $("#idpegawai option:selected").val() + "</td>";
				content += "<td>" + $("#idpegawai option:selected").text() + "</td>";
				content +=
					"<td><a href='#' class='btn btn-danger assignmentRemove' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash'></i></a></td>";

				if ($("#rowindex").val() != '') {
					$('#assignmentList tbody tr:eq(' + rowIndex + ')').html(content);
				} else {
					content += "</tr>";
					$('#assignmentList tbody').append(content);
				}

				detailClear();
			}
		});

		$(document).on("click", ".assignmentRemove", function () {
			if (confirm("Hapus data ?") == true) {
				$(this).closest('td').parent().remove();
			}
			return false;
		});

		$("#form-work").submit(function (e) {
			var countAssignment = $('#countAssignment').val();
			if (countAssignment) {
				var form = this;

				var assignmentTbl = $('table#assignmentList tbody tr').get().map(function (row) {
					return $(row).find('td').get().map(function (cell) {
						return $(cell).html();
					});
				});

				$("#assignmentValue").val(JSON.stringify(assignmentTbl));

				swal({
					title: "Berhasil!",
					text: "Proses penyimpanan data.",
					type: "success",
					timer: 1500,
					showConfirmButton: false
				});
			} else {
				sweetAlert("Maaf...", "Pegawai tidak boleh kosong!", "error");
				return false;
			}
		});
	});

	function detailClear() {
		$("#rowindex").val('');
		$(".detail").val('');

		$("#idpegawai").select2("trigger", "select", {
			data: {
				id: '',
				text: ''
			}
		});
	}

	function validateAssigment() {
		if ($("#idpegawai").val() == "") {
			sweetAlert("Maaf...", "Pegawai belum dipilih!", "error");
			return false;
		}

		return true;
	}

</script>
