<!-- @tab navigation &start -->
<div class="content-grid">
	<div class="row">

        <?php if($this->session->userdata('user_idpermission') != 5) {?>
            <div class="col-xs-6 col-sm-4 col-lg-2">
                <a class="block block-link-hover2 text-center" href="{base_url}tpengajuan">
                    <div class="block-content block-content-full bg-primary-dark">
                        <i class="si si-user fa-4x text-white"></i>
                        <div class="font-w600 text-white-op push-15-t text-uppercase">Unit</div>
                    </div>
                </a>
            </div>

            <?php if($this->session->userdata('user_idpermission') == 4) {?>
                <div class="col-xs-6 col-sm-4 col-lg-2">
                    <a class="block block-link-hover2 text-center" href="{base_url}tpengajuan_bendahara">
                        <div class="block-content block-content-full bg-modern">
                            <i class="si si-user fa-4x text-white"></i>
                            <div class="font-w600 text-white-op push-15-t text-uppercase">Bendahara</div>
                        </div>
                    </a>
                </div>
            <?php } ?>

            <div class="col-xs-6 col-sm-4 col-lg-2">
                <a class="block block-link-hover2 text-center" href="{base_url}tpengajuan_unitpemroses">
                    <div class="block-content block-content-full bg-modern">
                        <i class="si si-user fa-4x text-white"></i>
                        <div class="font-w600 text-white-op push-15-t text-uppercase">Unit Proses</div>
                    </div>
                </a>
            </div>
        <?php } ?>

        <?php if ($this->session->userdata('user_idpermission') == 5) { ?>
            <div class="col-xs-6 col-sm-4 col-lg-2">
                <a class="block block-link-hover2 text-center" href="#">
                    <div class="block-content block-content-full bg-modern">
                        <i class="si si-user fa-4x text-white"></i>
                        <div class="font-w600 text-white-op push-15-t text-uppercase">Wadir</div>
                    </div>
                </a>
            </div>
        <?php } ?>

        <!-- 1 unit, 2 unit, 3 bendahara & 4 unit proses -->
        <!-- #comment : (1& 4 biasa) (1, 2, 4 & bendahara) (2 wadir) -->

        <!-- 5 wadir & 4 bendahara -->
        <!---->
	</div>
</div>
<!-- @tab navigation &end -->

<?php echo ErrorSuccess($this->session)?>
<?php //if($error != '') {echo ErrorMessage($error)}?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tpengajuan/create" class="btn"><i class="fa fa-plus"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
        <hr style="margin-bottom: 0;">
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>No. Pengajuan</th>
					<th>Tgl Pengajuan</th>
					<th>Tgl Dibutuhkan</th>
					<th>Pemohon</th>
					<th>Subjek</th>
					<th>Unit</th>
					<th>Diperlukan Untuk</th>
					<th>Nominal</th>
					<th>Persetujuan</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
    $(document).ready(function() {
        BaseTableDatatables.init();
        var datablesimrs = $('#datatable-simrs').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "scrollX": true,
            "order": [],
            "ajax": {
                url: '{site_url}tpengajuan/getIndex',
                type: "POST",
                dataType: 'json'
            }
        });

        $(document).on('click', '.delete-pengajuan', function() {
            let id = $(this).attr('data-value');

            swal({
                title: 'Apakah anda yakin akan membatalkan <span class="text-uppercase" style="font-weight:bold;">pengajuan</span> ini ?',
                text : "",
                type : "success",
                showCancelButton: true,
                confirmButtonText: "Ya",
                confirmButtonColor: "#34a263",
                cancelButtonText: "Batalkan",
            }).then(function() {
                $.ajax({
                    url: '{site_url}Tpengajuan/delete/'+id,
                    type: 'POST',
                    dataType: 'json',
                    complete: function() {
                        swal("Berhasil!", "pengajuan yang di ajukan dibatalkan", "success").then(function() {
                            window.location.assign('{site_url}Tpengajuan');
                        });
                    },
                    error: function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    }
                });
            });
        });

    });
</script>
