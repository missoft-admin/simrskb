<?php echo ErrorSuccess($this->session) ?>
<?php if ($error != '') echo ErrorMessage($error) ?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}tpengajuan" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
        <hr style="margin-bottom: 0px;">
    </div>

    <!-- @ulasan form &start -->
    <?php echo form_open_multipart('tpengajuan/saveUlasan/', 'class="form-horizontal push-10-t" id="form-work"') ?>
    <div class="block-content  block-content-narrow">
        <div class="row">
            <div class="col-md-6">

                <!-- @pemohon &start -->
                <div class="form-group">
                    <label class="col-md-5 control-label text-uppercase" for="iduserlogin">Pemohon</label>
                    <div class="col-md-7">
                        <input type="text" class="form-control" disabled placeholder="Pemohon" value="{namapemohon}">
                    </div>
                </div>
                <!-- @pemohon &end -->

                <!-- @tgl_pengajuan &start -->
                <div class="form-group">
                    <label class="col-md-5 control-label text-uppercase" for="idtipe">Tanggal Pengajuan</label>
                    <div class="col-md-7">
                        <input type="text" class="js-datepicker form-control" disabled data-date-format="dd/mm/yyyy"
                               placeholder="dd/mm/yyyy"
                               value="{tanggal}">
                    </div>
                </div>
                <!-- @tgl_pengajuan &end -->

                <!-- @tgl_dibutuhkan &start -->
                <div class="form-group">
                    <label class="col-md-5 control-label text-uppercase" for="tanggaldibutuhkan">Tanggal
                        Dibutuhkan</label>
                    <div class="col-md-7">
                        <input type="text" class="js-datepicker form-control" disabled name="tanggaldibutuhkan"
                               data-date-format="dd/mm/yyyy"
                               placeholder="dd/mm/yyyy" value="{tanggal}">
                    </div>
                </div>
                <!-- @tgl_dibutuhkan &end -->

                <!-- @subjek &start -->
                <div class="form-group">
                    <label class="col-md-5 control-label text-uppercase" for="subjek">Subjek</label>
                    <div class="col-md-7">
                        <input type="text" class="form-control" disabled placeholder="Subjek (Judul Pengajuan)"
                               name="subjek" value="{subjek}">
                    </div>
                </div>
                <!-- @subjek &end -->

                <!-- @bagian &start -->
                <div class="form-group">
                    <label class="col-md-5 control-label text-uppercase" for="untukbagian">Untuk Bagian</label>
                    <div class="col-md-7">
                        <select name="untukbagian" class="js-select2 form-control" disabled style="width: 100%;"
                                data-placeholder="Pilih Opsi">
                            <option value="" disabled selected>Pilih Opsi</option>
                            <?php foreach ($list_unitpelayanan as $row) { ?>
                                <option value="<?= $row->id; ?>" <?= ($untukbagian == $row->id) ? "selected" : "" ?>>
                                    <?= $row->nama ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <!-- @bagian &end -->

                <!-- @diperlukan &start -->
                <div class="form-group">
                    <label class="col-md-5 control-label text-uppercase" for="diperlukanuntuk">Diperlukan Untuk</label>
                    <div class="col-md-7">
                        <select name="diperlukanuntuk" class="js-select2 form-control" disabled style="width: 100%;"
                                data-placeholder="Pilih Opsi">
                            <option value="" disabled selected>Pilih Opsi</option>
                            <?php foreach ($list_keperluan as $row) { ?>
                                <option value="<?= $row->id; ?>" <?= ($diperlukanuntuk == $row->id) ? "selected" : "" ?>>
                                    <?= $row->nama ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <!-- @diperlukan &end -->
            </div>
            <div class="col-md-6">
                <!-- @vendor &start -->
                <div class="form-group">
                    <label class="col-md-5 control-label text-uppercase" for="idvendor">Vendor</label>
                    <div class="col-md-7">
                        <select name="idvendor" class="js-select2 form-control" disabled style="width: 100%;"
                                data-placeholder="Pilih Opsi">
                            <option value="" disabled selected>Pilih Opsi</option>
                            <?php foreach ($list_vendor as $row) { ?>
                                <option value="<?= $row->id; ?>" <?= ($idvendor == $row->id) ? "selected" : "" ?>
                                        data-norekening="
								<?= $row->norekening ?>" data-atasnama="
								<?= $row->atasnama ?>" data-bank="
								<?= $row->bank ?>">
                                    <?= $row->nama ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <!-- @vendor &end -->

                <!-- @pembayaran &start -->
                <div class="form-group">
                    <label class="col-md-5 control-label text-uppercase" for="jenispembayaran">Jenis Pembayaran</label>
                    <div class="col-md-7">
                        <select name="jenispembayaran" class="js-select2 form-control" disabled style="width: 100%;"
                                data-placeholder="Pilih Opsi">
                            <option value="1" <?= ($jenispembayaran == 1 ? 'selected' : '') ?>>Tunai</option>
                            <option value="2" <?= ($jenispembayaran == 2 ? 'selected' : '') ?>>Kontrabon</option>
                            <option value="3" <?= ($jenispembayaran == 3 ? 'selected' : '') ?>>Transfer</option>
                            <option value="4" <?= ($jenispembayaran == 4 ? 'selected' : '') ?>>Termin By Progress
                            </option>
                            <option value="5" <?= ($jenispembayaran == 5 ? 'selected' : '') ?>>Termin Fix (Cicilan)
                            </option>
                        </select>
                    </div>
                </div>
                <!-- @pembayaran &end -->


                <div id="pembayaranKontrabon" <?= ($jenispembayaran == 2 ? '' : 'hidden') ?>>
                    <div class="form-group">
                        <label class="col-md-5 control-label text-uppercase" for="tanggalkontrabon">Tanggal
                            Kontrabon</label>
                        <div class="col-md-7">
                            <input type="text" class="js-datepicker form-control" disabled name="tanggalkontrabon"
                                   data-date-format="dd/mm/yyyy"
                                   placeholder="dd/mm/yyyy" value="{tanggalkontrabon}">
                        </div>
                    </div>
                </div>

                <div <?= ($jenispembayaran == 3 ? '' : 'hidden') ?>>
                    <div class="form-group">
                        <label class="col-md-5 control-label text-uppercase" for="norekening">No. Rekening</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control" disabled placeholder="No. Rekening"
                                   name="norekening" value="{norekening}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label text-uppercase" for="bank">Bank</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control" disabled placeholder="Bank" name="bank"
                                   value="{bank}">
                        </div>
                    </div>
                </div>

                <div <?= ($jenispembayaran == 5 ? '' : 'hidden') ?>>
                    <div class="form-group">
                        <label class="col-md-5 control-label text-uppercase" for="downpayment">DP (DownPayment)</label>
                        <div class="col-md-7">
                            <label class="css-input switch switch-sm switch-default">
                                <input type="checkbox" disabled <?= ($downpayment ? 'checked' : '') ?>><span></span>
                            </label>

                            <input type="text" <?= ($downpayment == 1 ? '' : 'hidden') ?>
                                   class="js-datepicker form-control" disabled
                                   name="tanggaldownpayment" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy"
                                   value="{tanggaldownpayment}">
                            <br>
                            <input type="text" <?= ($downpayment == 1 ? '' : 'hidden') ?> class="form-control number"
                                   disabled
                                   placeholder="Nominal DP (DownPayment)" name="downpayment" value="{downpayment}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label text-uppercase" for="cicilan">Cicilan</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control number" disabled placeholder="Cicilan" name="cicilan"
                                   value="{cicilan}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label text-uppercase" for="nominalcicilan">Nominal
                            Cicilan</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control number" disabled readonly
                                   placeholder="Nominal Cicilan" name="nominalcicilan"
                                   value="{nominalcicilan}">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-5 control-label text-uppercase" for="fakturpajak">Faktur Pajak</label>
                    <div class="col-md-7">
                        <label class="css-input css-checkbox css-checkbox-primary">
                            <input type="checkbox" <?= ($fakturpajak ? 'checked' : '') ?> disabled
                                   name="fakturpajak"><span></span> Ya
                        </label>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- @ulasan form &end -->

    <div class="block-content" <?= ($jenispembayaran == 4 ? '' : 'hidden') ?>>
        <span class="badge badge-primary text-uppercase">Termin</span>
        <br><br>
		<div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th style="width:10%">Judul Termin</th>
                <th style="width:10%">Deskripsi</th>
                <th style="width:10%">Nominal</th>
                <th style="width:10%">Tanggal</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($list_termin_progress as $row) { ?>
                <tr>
                    <td>
                        <?= $row->subjek; ?>
                    </td>
                    <td>
                        <?= $row->deskripsi; ?>
                    </td>
                    <td>
                        <?= number_format($row->nominal); ?>
                    </td>
                    <td>
                        <?= date_format(date_create($row->tanggal), "d/m/Y"); ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    </div>

    <div class="block-content">
        <span class="badge badge-primary text-uppercase">Barang</span>
        <br><br>
		<div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th style="width:10%">Nama Barang</th>
                <th style="width:10%">Merk</th>
                <th style="width:10%">Kuantitas</th>
                <th style="width:10%">Satuan</th>
                <th style="width:10%">Harga</th>
                <th style="width:10%">Jumlah</th>
                <th style="width:10%">Keterangan</th>
            </tr>
            </thead>
            <tbody>
            <?php $totalnominal = 0; ?>
            <?php foreach ($list_detail as $row) { ?>
                <?php $totalnominal += $row->harga; ?>
                <tr>
                    <td>
                        <?= $row->namabarang; ?>
                    </td>
                    <td>
                        <?= $row->merk; ?>
                    </td>
                    <td>
                        <?= number_format($row->kuantitas); ?>
                    </td>
                    <td style="display:none;">
                        <?= $row->idsatuan; ?>
                    </td>
                    <td>
                        <?= $row->namasatuan; ?>
                    </td>
                    <td>
                        <?= number_format($row->harga); ?>
                    </td>
                    <td>
                        <?= number_format($row->jumlah); ?>
                    </td>
                    <td>
                        <?= $row->keterangan; ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="5" style="text-align:right; font-weight:bold;">TOTAL</td>
                <td colspan="2">
                    <?= number_format($totalnominal) ?>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    </div>

    <input type="hidden" class="form-control" name="idpengajuan" value="{id}">
    <input type="hidden" class="form-control" name="idpersetujuan" value="{idpersetujuan}">
    <input type="hidden" id="status" class="form-control" name="status">

    <div class="block-content">
        <span class="badge badge-primary text-uppercase">Lampiran</span>
        <br><br>
		<div class="table-responsive">
        <table class="table table-bordered table-striped">
            <tbody>
            <?php if (empty($list_lampiran)) { ?>
                <tr>
                    <td class="text-center text-uppercase" colspan="2">-</td>
                </tr>
            <?php } else { ?>
                <?php foreach ($list_lampiran as $row) { ?>
                    <tr>
                        <td>
                            <?= $row->lampiran ?>
                        </td>
                        <td><a href="{base_url}assets/upload/pengajuan/<?= $row->lampiran ?>"
                               class="badge badge-primary" download><i class="fa fa-download"></i>
                                Download</a></td>
                    </tr>
                <?php } ?>
            <?php } ?>
            </tbody>
        </table>
        </div>
        <span class="badge badge-primary text-uppercase">Memo</span>
        <br><br>
        <textarea class="form-control js-summernote" placeholder="Memo" name="memo">{memo}</textarea>
        <br>
        <div class="row">
            <div class="col-sm-12">
                <hr style="margin-top: 0px;">
                <div style="float:right;">
                    <button class="btn btn-success text-uppercase" id="setujui" type="submit" style="font-size:13px;">
                        Setujui
                    </button>
                    <button class="btn btn-danger text-uppercase" id="tolak" type="submit" style="font-size:13px;">
                        Tolak
                    </button>
                </div>
            </div>
        </div>
        <br><br>
    </div>
    <?php echo form_hidden('id', $id); ?>
    <?php echo form_close() ?>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".number").number(true, 0, '.', ',');

        $('.js-summernote').summernote('disable');

        $("#setujui").click(function () {
            event.preventDefault();
            swal({
                title: "Apa anda yakin?",
                text: "Ingin menyetujui data ini ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya !",
                cancelButtonText: "Tidak, batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function () {
                swal({
                    title: 'Berhasil!',
                    text: 'Data telah disetujui.',
                    type: 'success',
                    timer: 2000,
                    showCancelButton: false,
                    showConfirmButton: false
                });

                $("#status").val(1);
                $("#form-work").submit();
                return true;
            });
        });

        $("#tolak").click(function () {
            event.preventDefault();
            swal({
                title: "Apa anda yakin?",
                text: "Ingin menolak data ini ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya !",
                cancelButtonText: "Tidak, batalkan !",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function () {
                swal({
                    title: 'Berhasil!',
                    text: 'Data telah ditolak.',
                    type: 'success',
                    timer: 2000,
                    showCancelButton: false,
                    showConfirmButton: false
                });

                $("#status").val(2);
                $("#form-work").submit();
                return true;
            });
        });
    });

</script>
