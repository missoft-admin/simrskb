<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tpengajuan" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
        <hr style="margin-bottom: 0px;">
	</div>
	<?php echo form_open_multipart('tpengajuan/save','class="form-horizontal push-10-t" id="form-work"') ?>
	<div class="block-content  block-content-narrow">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-5 control-label" for="iduserlogin">Pemohon</label>
					<div class="col-md-7">
						<input type="hidden" name="idpemohon" value="{idpemohon}">
						<input type="text" class="form-control" readonly placeholder="Pemohon" value="{namapemohon}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-5 control-label" for="idtipe">Tanggal Pengajuan</label>
					<div class="col-md-7">
						<input class="js-datepicker form-control" type="text" id="tanggal" name="tanggal" data-date-format="dd/mm/yyyy"
						 placeholder="dd/mm/yyyy" value="<?=$tanggal;?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-5 control-label" for="tanggaldibutuhkan">Tanggal Dibutuhkan</label>
					<div class="col-md-7">
						<input class="js-datepicker form-control" type="text" id="tanggaldibutuhkan" name="tanggaldibutuhkan"
						 data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" value="<?=$tanggal;?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-5 control-label" for="subjek">Subjek</label>
					<div class="col-md-7">
						<input type="text" class="form-control" id="subjek" placeholder="Subjek ( Judul Pengajuan )" name="subjek" value="{subjek}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-5 control-label" for="untukbagian">Untuk Bagian</label>
					<div class="col-md-7">
						<select name="untukbagian" id="untukbagian" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="" disabled selected>Pilih Opsi</option>
							<?php foreach  ($list_unitpelayanan as $row) { ?>
							<option value="<?=$row->id;?>" <?=($untukbagian==$row->id) ? "selected" : "" ?>>
								<?=$row->nama?>
							</option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-5 control-label" for="diperlukanuntuk">Diperlukan Untuk</label>
					<div class="col-md-7">
						<div class="row">
							<div class="col-md-9">
								<select name="diperlukanuntuk" id="diperlukanuntuk" class="js-select2 form-control" style="width: 100%;"
								 data-placeholder="Pilih Opsi">
									<option value="" disabled selected>Pilih Opsi</option>
									<?php foreach  ($list_keperluan as $row) { ?>
									<option value="<?=$row->id;?>" <?=($diperlukanuntuk==$row->id) ? "selected" : "" ?>>
										<?=$row->nama?>
									</option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-3">
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#keperluanModal"><i class="fa fa-plus"></i></button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-5 control-label" for="idvendor">Vendor</label>
					<div class="col-md-7">
						<div class="row">
							<div class="col-md-9">
								<select name="idvendor" id="idvendor" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="" disabled selected>Pilih Opsi</option>
									<?php foreach  ($list_vendor as $row) { ?>
									<option value="<?=$row->id;?>" <?=($idvendor==$row->id) ? "selected" : "" ?> data-norekening="
										<?=$row->norekening?>" data-atasnama="
										<?=$row->atasnama?>" data-bank="
										<?=$row->bank?>">
										<?=$row->nama?>
									</option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-3">
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#vendorModal"><i class="fa fa-plus"></i></button>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-5 control-label" for="jenispembayaran">Jenis Pembayaran</label>
					<div class="col-md-7">
						<select name="jenispembayaran" id="jenispembayaran" class="js-select2 form-control" style="width: 100%;"
						 data-placeholder="Pilih Opsi">
							<option value="1" <?=($jenispembayaran==1 ? 'selected' : '' )?>>Tunai</option>
							<option value="2" <?=($jenispembayaran==2 ? 'selected' : '' )?>>Kontrabon</option>
							<option value="3" <?=($jenispembayaran==3 ? 'selected' : '' )?>>Transfer</option>
							<option value="4" <?=($jenispembayaran==4 ? 'selected' : '' )?>>Termin By Progress</option>
							<option value="5" <?=($jenispembayaran==5 ? 'selected' : '' )?>>Termin Fix (Cicilan)</option>
						</select>
					</div>
				</div>
				<div id="pembayaranKontrabon" <?=($jenispembayaran==2 ? '' : 'hidden' )?>>
					<div class="form-group">
						<label class="col-md-5 control-label" for="tanggalkontrabon">Tanggal Kontrabon</label>
						<div class="col-md-7">
							<input class="js-datepicker form-control" type="text" id="tanggalkontrabon" name="tanggalkontrabon"
							 data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" value="<?=$tanggalkontrabon;?>">
						</div>
					</div>
				</div>
				<div id="pembayaranTransfer" <?=($jenispembayaran==3 ? '' : 'hidden' )?>>
					<div class="form-group">
						<label class="col-md-5 control-label" for="norekening">No. Rekening</label>
						<div class="col-md-7">
							<input type="text" class="form-control" id="norekening" placeholder="No. Rekening" name="norekening">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-5 control-label" for="bank">Bank</label>
						<div class="col-md-7">
							<input type="text" class="form-control" id="bank" placeholder="Bank" name="bank">
						</div>
					</div>
				</div>
				<div id="pembayaranTerminByFix" <?=($jenispembayaran==5 ? '' : 'hidden' )?>>
					<div class="form-group">
						<label class="col-md-5 control-label" for="downpayment">DP (DownPayment)</label>
						<div class="col-md-7">
							<label class="css-input switch switch-sm switch-default">
								<input type="checkbox" <?=($jenispembayaran==5 ? 'checked' : '' )?> id="stDownpayment"><span></span>
							</label>

							<input type="text" style="<?=($jenispembayaran == 5 ? '' : 'display:none')?>" class="js-datepicker form-control"
							 id="tanggaldownpayment" name="tanggaldownpayment" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" value="<?=$tanggaldownpayment;?>">
							<br>
							<input type="text" style="<?=($jenispembayaran == 5 ? '' : 'display:none')?>" class="form-control number" id="downpayment"
							 placeholder="Nominal DP (DownPayment)" name="downpayment" value="{downpayment}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-5 control-label" for="cicilan">Lama Cicilan</label>
						<div class="col-md-7">
							<input type="text" class="form-control number" id="cicilan" placeholder="Lama Cicilan" name="cicilan" value="{cicilan}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-5 control-label" for="nominalcicilan">Nominal Cicilan</label>
						<div class="col-md-7">
							<input type="text" class="form-control number" id="nominalcicilan" readonly placeholder="Nominal Cicilan" name="nominalcicilan"
							 value="{nominalcicilan}">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-5 control-label" for="fakturpajak">Faktur Pajak</label>
					<div class="col-md-7">
						<label class="css-input css-checkbox css-checkbox-primary">
							<input type="checkbox" <?=($fakturpajak ? 'checked' : '' )?> name="fakturpajak"><span></span> Ya
						</label>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="block-content" id="tableTermin" <?=($jenispembayaran==4 ? '' : 'hidden' )?>>
		<span class="badge badge-primary">Termin</span>
		<br><br>
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="terminList">
			<thead>
				<tr>
					<th style="width:10%">Judul Termin</th>
					<th style="width:10%">Deskripsi</th>
					<th style="width:10%">Nominal</th>
					<th style="width:10%">Tanggal</th>
					<th style="width:10%">Aksi</th>
				</tr>
				<tr>
					<th>
						<input type="text" class="form-control detail" id="judultermin" placeholder="Judul Termin" value="">
					</th>
					<th>
						<input type="text" class="form-control detail" id="deskripsi" placeholder="Deskripsi" value="">
					</th>
					<th>
						<input type="text" class="form-control detail number" id="nominal" placeholder="Nominal" value="">
					</th>
					<th>
						<input class="js-datepicker form-control detail" type="text" id="tanggaltermin" data-date-format="dd/mm/yyyy"
						 placeholder="dd/mm/yyyy" value="">
					</th>
					<th>
						<a id="terminAdd" class="btn btn-success">Tambahkan</a>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php if ($this->uri->segment(2) == 'update') { ?>
				<?php foreach  ($list_termin_progress as $row) { ?>
				<tr>
					<td>
						<?=$row->subjek;?>
					</td>
					<td>
						<?=$row->deskripsi;?>
					</td>
					<td>
						<?=number_format($row->nominal);?>
					</td>
					<td>
						<?=$row->tanggal;?>
					</td>
					<td><a href="#" class="btn btn-danger detailRemove" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></a></td>
				</tr>
				<?php } ?>
				<?php } ?>
			</tbody>
		</table>
	</div>
	</div>

	<input type="hidden" class="form-control" id="rowindex" />

	<div class="block-content">
		<span class="badge badge-primary text-uppercase">Barang</span>
		<br><br>
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="detailList">
			<thead>
				<tr>
					<th style="width:10%">Nama Barang</th>
					<th style="width:10%">Merk</th>
					<th style="width:10%">Kuantitas</th>
					<th style="width:10%">Satuan</th>
					<th style="width:10%">Harga</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:10%">Keterangan</th>
					<th style="width:10%">Aksi</th>
				</tr>
				<tr>
					<th>
						<input type="text" class="form-control detail" id="namabarang" placeholder="Nama Barang" value="">
					</th>
					<th>
						<input type="text" class="form-control detail" id="merk" placeholder="Merk" value="">
					</th>
					<th>
						<input type="text" class="form-control detail number" id="kuantitas" placeholder="Kuantitas" value="">
					</th>
					<th>
						<select id="idsatuan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="" disabled selected>Pilih Opsi</option>
							<?php foreach  ($list_satuan as $row) { ?>
							<option value="<?=$row->id;?>">
								<?=$row->nama?>
							</option>
							<?php } ?>
						</select>
					</th>
					<th>
						<input type="text" class="form-control detail number" id="harga" placeholder="Harga" value="">
					</th>
					<th>
						<input type="text" class="form-control detail number" id="jumlah" readonly placeholder="Jumlah" value="">
					</th>
					<th>
						<input type="text" class="form-control detail" id="keterangan" placeholder="Keterangan" value="">
					</th>
					<th>
						<a id="detailAdd" class="btn btn-success">Tambahkan</a>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php if ($this->uri->segment(2) == 'update') { ?>
                    <? $totalnominal = 0; ?>
				<?php foreach  ($list_detail as $row) { ?>
				<tr>
					<td>
						<?=$row->namabarang;?>
					</td>
					<td>
						<?=$row->merk;?>
					</td>
					<td>
						<?=number_format($row->kuantitas);?>
					</td>
					<td style="display:none;">
						<?=$row->idsatuan;?>
					</td>
					<td>
						<?=$row->namasatuan;?>
					</td>
					<td>
						<?=number_format($row->harga);?>
					</td>
					<td>
						<?=number_format($row->jumlah);?>
                        <? $totalnominal += $row->jumlah; ?>
					</td>
					<td>
						<?=$row->keterangan;?>
					</td>
					<td><a href="#" class="btn btn-danger text-uppercase detailRemove" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></a></td>
				</tr>
				<?php } ?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="5" style="text-align:center; font-weight:bold;">TOTAL</td>
					<td colspan="3" id="totalnominalLabel">
						<?= number_format($totalnominal) ?>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
	</div>

	<div class="block-content">
		<span class="badge badge-primary text-uppercase">Lampiran</span> <a class="badge badge-success text-uppercase" id="attachmentAdd"><i class="fa fa-plus"></i>
			Tambah</a>
		<br><br>
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="attachmentList">
			<tbody>
				<?php if ($this->uri->segment(2) == 'update') { ?>
				<?php foreach  ($list_lampiran as $row) { ?>
				<tr>
					<td>
						<?=$row->lampiran?>
					</td>
					<td><a href="{base_url}assets/upload/pengajuan/<?=$row->lampiran?>" class="badge badge-primary" download><i class="fa fa-download"></i>
							Download</a></td>
				</tr>
				<?php } ?>
				<?php }else{ ?>
				<tr>
					<td><input type="file" name="attachment[]"></td>
					<td><a class="badge badge-danger text-uppercase attachmentRemove"><i class="fa fa-times-circle"></i> Hapus</a></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
        </div>

        <div class="row">
            <div class="col-md-12">
                <hr style="margin-top:0px;">
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div style="float:right;">
                    <button class="btn btn-success text-uppercase" type="submit" style="font-size:14px;width:76px;">Simpan</button>
                    <a href="{base_url}tpengajuan" class="btn btn-default text-uppercase" type="reset" style="font-size:14px;width:76px;"><i class="pg-close"></i>Batal</a>
                </div>
            </div>
        </div>

        <input type="hidden" id="detailValue" name="detailValue">
        <input type="hidden" id="terminValue" name="terminValue">
        <input type="hidden" id="totalnominal" name="totalnominal" value="{totalnominal}">
        <br><br>
    </div>


	<?php echo form_hidden('id', $id); ?>
	<?php echo form_close() ?>
</div>

<!-- modal keperluan -->
<div id="keperluanModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<h3 class="block-title">Keperluan</h3>
				</div>
				<div class="modal-body">
					<div class="form-horizontal">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="idpoliklinik">Nama Keperluan</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="fmKeperluan" value="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="modal-footer">
				<div class="buton">
					<button type="button" class="btn btn-primary" id="saveDataJenisKeperluan" data-dismiss="modal">Simpan</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="vendorModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<h3 class="block-title">Vendor</h3>
				</div>
				<div class="modal-body">
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-md-4 control-label" for="nama">Nama</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="fmNama" placeholder="Nama" value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="alamat">Alamat</label>
							<div class="col-md-8">
								<textarea class="form-control" id="fmAlamat" placeholder="Alamat"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="telepon">Telepon</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="fmTelepon" placeholder="Telepon" value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="norekening">No. Rekening</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="fmNorekening" placeholder="No. Rekening" value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="atasnama">Atas Nama</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="fmAtasnama" placeholder="Atas Nama" value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="bank">Bank</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="fmBank" placeholder="Bank" value="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="modal-footer">
				<div class="buton">
					<button type="button" class="btn btn-primary" id="saveDataVendor" data-dismiss="modal">Simpan</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$(".number").number(true, 0, '.', ',');

		$("#idvendor").change(function () {
			// $('#norekening').val($("#idvendor option:selected").data('norekening') + ' Atas Nama ' + $("#idvendor option:selected").data('atasnama'));
			$('#norekening').val($("#idvendor option:selected").data('norekening'));
			$('#bank').val($("#idvendor option:selected").data('bank'));
		});

		$("#kuantitas").keyup(function () {
			var harga = parseFloat($("#harga").val());
			var kuantitas = parseFloat($(this).val());

			$('#jumlah').val(harga * kuantitas);
		});

		$("#harga").keyup(function () {
			var harga = parseFloat($(this).val());
			var kuantitas = parseFloat($("#kuantitas").val());

			$('#jumlah').val(harga * kuantitas);
		});

		$("#stDownpayment").click(function () {
			if ($(this).prop("checked")) {
				$("#tanggaldownpayment").show();
				$("#downpayment").show();
			} else {
				$("#tanggaldownpayment").hide();
				$("#downpayment").hide();
			}
		});

		$("#cicilan").keyup(function () {
			var totalnominal = parseFloat($("#totalnominal").val().replace(/,/g, ''));
			var downpayment = parseFloat($("#downpayment").val().replace(/,/g, ''));

			$('#nominalcicilan').val((totalnominal - (downpayment ? downpayment : 0)) / parseFloat($(this).val()));
		});

		$("#jenispembayaran").change(function () {
			if ($(this).val() == 2) {
				$("#pembayaranKontrabon").show();
				$("#pembayaranTransfer").hide();
				$("#pembayaranTerminByFix").hide();
				$("#tableTermin").hide();
			} else if ($(this).val() == 3) {
				$("#pembayaranKontrabon").hide();
				$("#pembayaranTransfer").show();
				$("#pembayaranTerminByFix").hide();
				$("#tableTermin").hide();
			} else if ($(this).val() == 4) {
				$("#pembayaranKontrabon").hide();
				$("#pembayaranTransfer").hide();
				$("#pembayaranTerminByFix").hide();
				$("#tableTermin").show();
			} else if ($(this).val() == 5) {
				$("#pembayaranKontrabon").hide();
				$("#pembayaranTransfer").hide();
				$("#pembayaranTerminByFix").show();
				$("#tableTermin").hide();
			} else {
				$("#pembayaranKontrabon").hide();
				$("#pembayaranTransfer").hide();
				$("#pembayaranTerminByFix").hide();
				$("#tableTermin").hide();
			}
		});

		$(document).on('click', '#saveDataJenisKeperluan', function () {
			var namakeperluan = $('#fmKeperluan').val();

			$.ajax({
				url: '{site_url}tpengajuan/saveDataJenisKeperluan',
				method: 'POST',
				data: {
					namakeperluan: namakeperluan
				},
				success: function (data) {
					swal({
						title: "Berhasil!",
						text: "Data Keperluan telah ditambahkan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					$('#fmKeperluan').val('');

					$("#diperlukanuntuk").select2('destroy');
					$("#diperlukanuntuk").empty();

					$.ajax({
						url: '{site_url}tpengajuan/getJenisKeperluan',
						method: "GET",
						dataType: "json",
						success: function (data) {
							$("#diperlukanuntuk").append("<option value=''>Pilih Opsi</option>");
							for (var i = 0; i < data.length; i++) {
								$("#diperlukanuntuk").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
							}
							$("#diperlukanuntuk").select2();
						}
					});
				}
			});
		});

		$(document).on('click', '#saveDataVendor', function () {
			var nama = $('#fmNama').val();
			var alamat = $('#fmAlamat').val();
			var telepon = $('#fmTelepon').val();
			var norekening = $('#fmNorekening').val();
			var atasnama = $('#fmAtasnama').val();
			var bank = $('#fmBank').val();

			$.ajax({
				url: '{site_url}tpengajuan/saveDataVendor',
				method: 'POST',
				data: {
					nama: nama,
					alamat: alamat,
					telepon: telepon,
					norekening: norekening,
					atasnama: atasnama,
					bank: bank,
				},
				success: function (data) {
					swal({
						title: "Berhasil!",
						text: "Data Vendor telah ditambahkan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					$('#fmNama').val('');
					$('#fmAlamat').val('');
					$('#fmTelepon').val('');
					$('#fmNorekening').val('');
					$('#fmAtasnama').val('');
					$('#fmBank').val('');

					$("#idvendor").select2('destroy');
					$("#idvendor").empty();

					$.ajax({
						url: '{site_url}tpengajuan/getVendor',
						method: "GET",
						dataType: "json",
						success: function (data) {
							$("#idvendor").append("<option value=''>Pilih Opsi</option>");
							for (var i = 0; i < data.length; i++) {
								$("#idvendor").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
							}
							$("#idvendor").select2();
						}
					});
				}
			});
		});

		$("#detailAdd").click(function () {
			var valid = validateDetail();
			if (!valid) return false;

			var rowIndex;
			var duplicate = false;

			if ($("#rowindex").val() != '') {
				var content = "";
				rowIndex = $("#rowindex").val();
			} else {
				var content = "<tr>";
				$('#detailList tbody tr').filter(function () {
					var $cells = $(this).children('td');
					if ($cells.eq(0).text() === $("#namabarang").val()) {
						sweetAlert("Maaf...", "Barang " + $("#namabarang").val() + " sudah ditambahkan.", "error");
						duplicate = true;
						return false;
					}
				});
			}

			if (duplicate == false) {
				content += "<td>" + $("#namabarang").val() + "</td>";
				content += "<td>" + $("#merk").val() + "</td>";
				content += "<td>" + $.number($("#kuantitas").val()) + "</td>";
				content += "<td style='display:none;'>" + $("#idsatuan option:selected").val(); + "</td>";
				content += "<td>" + $("#idsatuan option:selected").text(); + "</td>";
				content += "<td>" + $.number($("#harga").val()) + "</td>";
				content += "<td>" + $.number($("#jumlah").val()) + "</td>";
				content += "<td>" + $("#keterangan").val() + "</td>";
				content +=
					"<td><a href='#' class='btn btn-danger detailRemove' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash'></i></a></td>";

				if ($("#rowindex").val() != '') {
					$('#detailList tbody tr:eq(' + rowIndex + ')').html(content);
				} else {
					content += "</tr>";
					$('#detailList tbody').append(content);
				}

				calculateTotal();
				detailClear();

			}

        });

		$(document).on("click", ".detailEdit", function () {
			$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
			$("#namabarang").val($(this).closest('tr').find("td:eq(0)").html());
			$("#merk").val($(this).closest('tr').find("td:eq(1)").html());
			$("#kuantitas").val($(this).closest('tr').find("td:eq(2)").html());
			$("#idsatuan").select2('val', $(this).closest('tr').find("td:eq(3)").html());
			$("#harga").val($(this).closest('tr').find("td:eq(5)").html());
			$("#jumlah").val($(this).closest('tr').find("td:eq(6)").html());
			$("#keterangan").val($(this).closest('tr').find("td:eq(7)").html());

			return false;
		});

		$(document).on("click", ".detailRemove", function () {
			if (confirm("Hapus data ?") == true) {
				$(this).closest('td').parent().remove();
				calculateTotal();
			}
			return false;
		});

		$("#terminAdd").click(function () {
			var valid = validateTermin();
			if (!valid) return false;

			var rowIndex;
			var duplicate = false;

			if ($("#rowindex").val() != '') {
				var content = "";
				rowIndex = $("#rowindex").val();
			} else {
				var content = "<tr>";
				$('#terminList tbody tr').filter(function () {
					var $cells = $(this).children('td');
					if ($cells.eq(0).text() === $("#judultermin").val()) {
						sweetAlert("Maaf...", "Judul Termin " + $("#judultermin").val() + " sudah ditambahkan.", "error");
						duplicate = true;
						return false;
					}
				});
			}

			if (duplicate == false) {
				content += "<td>" + $("#judultermin").val() + "</td>";
				content += "<td>" + $("#deskripsi").val() + "</td>";
				content += "<td>" + $("#nominal").val() + "</td>";
				content += "<td>" + $("#tanggaltermin").val() + "</td>";
				content +=
					"<td><a href='#' class='btn btn-danger terminRemove' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash'></i></a></td>";

				if ($("#rowindex").val() != '') {
					$('#terminList tbody tr:eq(' + rowIndex + ')').html(content);
				} else {
					content += "</tr>";
					$('#terminList tbody').append(content);
				}

				detailClear();
			}
		});

		$(document).on("click", ".terminRemove", function () {
			if (confirm("Hapus data ?") == true) {
				$(this).closest('td').parent().remove();
			}
			return false;
		});

		$("#attachmentAdd").click(function () {
			var content = '';
			content += '<tr>';
			content += '<td><input type="file" name="attachment[]"></td>';
			content +=
				'<td><a class="badge badge-danger attachmentRemove"><i class="fa fa-times-circle"></i> Hapus</a></td>';
			content += '</tr>';

			$('#attachmentList tbody').append(content);
		});

		$(document).on("click", ".attachmentRemove", function () {
			if (confirm("Hapus data ?") == true) {
				$(this).closest('td').parent().remove();
			}
			return false;
		});

		$("#form-work").on('submit', function (e) {
			if ($("#idpegawai").val() == "") {
				e.preventDefault();
				sweetAlert("Maaf...", "Pemohon belum dipilih!", "error");
				return false;
			} else if ($("#untukbagian option:selected").val() == "") {
				e.preventDefault();
				sweetAlert("Maaf...", "Untuk Bagian belum dipilih!", "error");
				return false;
			} else if ($("#diperlukanuntuk option:selected").val() == "") {
				e.preventDefault();
				sweetAlert("Maaf...", "Diperlukan Untuk belum dipilih!", "error");
				return false;
			} else if ($("#idvendor option:selected").val() == "") {
				e.preventDefault();
				sweetAlert("Maaf...", "Vendor belum dipilih!", "error");
				return false;
			}

			var form = this;

			var detailTbl = $('table#detailList tbody tr').get().map(function (row) {
				return $(row).find('td').get().map(function (cell) {
					return $(cell).html();
				});
			});

			var terminTbl = $('table#terminList tbody tr').get().map(function (row) {
				return $(row).find('td').get().map(function (cell) {
					return $(cell).html();
				});
			});

			$("#detailValue").val(JSON.stringify(detailTbl));
			$("#terminValue").val(JSON.stringify(terminTbl));

			swal({
				title: "Berhasil!",
				text: "Proses penyimpanan data.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});

	});

	function detailClear() {
		$("#rowindex").val('');
		$(".detail").val('');

		$("#idsatuan").select2("trigger", "select", {
			data: {
				id: '',
				text: ''
			}
		});
		$("#idpegawai").select2("trigger", "select", {
			data: {
				id: '',
				text: ''
			}
		});
	}

	function validateDetail() {
		if ($("#namabarang").val() == "") {
			sweetAlert("Maaf...", "Nama Barang tidak boleh kosong!", "error");
			return false;
		}
		if ($("#merk").val() == "") {
			sweetAlert("Maaf...", "Merk tidak boleh kosong!", "error");
			return false;
		}

		if ($("#idsatuan").val() == "") {
			sweetAlert("Maaf...", "Satuan Barang belum dipilih!", "error");
			return false;
		}

		if ($("#kuantitas").val() == "" || $("#kuantitas").val() == "0") {
			sweetAlert("Maaf...", "Kuantitas tidak boleh kosong!", "error");
			return false;
		}

		if ($("#harga").val() == "" || $("#harga").val() == "0") {
			sweetAlert("Maaf...", "Harga tidak boleh kosong!", "error");
			return false;
		}

		return true;
	}

	function validateTermin() {
		if ($("#judultermin").val() == "") {
			sweetAlert("Maaf...", "Judul Termin tidak boleh kosong!", "error");
			return false;
		}
		if ($("#deskripsi").val() == "") {
			sweetAlert("Maaf...", "Deskripsi tidak boleh kosong!", "error");
			return false;
		}

		if ($("#tanggaltermin").val() == "") {
			sweetAlert("Maaf...", "Tanggal Termin belum dipilih!", "error");
			return false;
		}

		return true;
	}

	function validateAssigment() {
		if ($("#idpegawai").val() == "") {
			sweetAlert("Maaf...", "Pegawai belum dipilih!", "error");
			return false;
		}

		return true;
	}

	function calculateTotal() {
        var downpayment  = parseFloat($("#downpayment").val().replace(/,/g, '')),
            cicilan      = parseFloat($('#cicilan').val());

		var totalnominal = 0;
		$('#detailList tbody tr').each(function () {
			totalnominal += parseFloat($(this).find('td:eq(6)').text().replace(/,/g, ''));
		});



		$("#totalnominalLabel").html($.number(totalnominal));
		$("#totalnominal").val($.number(totalnominal));

        $('#nominalcicilan').val((totalnominal - (downpayment ? downpayment : 0)) / cicilan);
	}

    /* @js new &start */
    $(document).ready(function() {
        $('#jenispembayaran, #idvendor').change(function() {
            var idvendor = $('#idvendor').val(),
                jenispembayaran = parseInt($('#jenispembayaran').val());

            if (idvendor == "" && jenispembayaran !== 3) {
                $('#norekening').val('');
                $('#bank').val('');
            } else {
                $.ajax({
                    type: 'GET',
                    url: "{site_url}Tpengajuan/showVendorRekening/" + parseInt(idvendor),
                    dataType: 'json',
                    success: function (data) {
                        $('#norekening').val(data.norekening);
                        $('#bank').val(data.bank);
                    }
                });
            }
        });
    });
    /* @js new &end */

</script>
