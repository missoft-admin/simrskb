<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tpengajuan" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
        <hr style="margin-bottom: 0px;">
	</div>
	<?php echo form_open_multipart('tpengajuan/saveAktivasiBendahara','class="form-horizontal push-10-t" id="form-work"') ?>
	<div class="block-content  block-content-narrow">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label text-uppercase">No. Pengajuan</label>
					<div class="col-md-10">
						<input type="hidden" name="id" value="{id}">
						<input type="text" class="form-control" readonly placeholder="No. Pengajuan" value="{nopengajuan}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label text-uppercase">Subjek</label>
					<div class="col-md-10">
						<input type="text" class="form-control" readonly placeholder="Subjek" value="{subjek}">
					</div>
				</div>
			</div>
		</div>
	</div>

	<input type="hidden" class="form-control" id="rowindex" />

    <div class="col-md-12">
        <hr>
    </div>
	<div class="block-content">
		<span class="badge badge-primary text-uppercase">{labelPersetujuan}</span>
		<br><br>
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="assignmentList">
			<thead>
				<tr>
					<th style="width:10%">Nama Pegawai</th>
					<th style="width:10%">Aksi</th>
				</tr>
				<tr>
					<th>
						<select id="idpegawai" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="" disabled selected>Pilih Opsi</option>
							<?php foreach  ($list_user as $row) { ?>
							<option value="<?=$row->id;?>">
								<?=$row->name?>
							</option>
							<?php } ?>
						</select>
					</th>
					<th>
						<a id="assignmentAdd" class="btn btn-success">Tambahkan</a>
					</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>

    <div class="col-md-12">
        <hr>
    </div>
	<div class="block-content">
		<span class="badge badge-primary text-uppercase">Memo Kepada Unit Yang Memproses</span><br><br>
		<textarea class="form-control js-summernote" placeholder="Memo" name="memo"></textarea>
	</div>

	<?php if($stepPersetujuan == 3){ ?>

        <div class="col-md-12">
            <hr>
        </div>

	<!-- Proses Wakil Direktur -->
	<div class="block-content">
		<span class="badge badge-primary text-uppercase">Proses Aktivasi Bendahara</span><br><br>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label text-uppercase">Bendahara</label>
					<div class="col-md-10">
						<select id="idbendahara" name="idbendahara" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="" disabled selected>Pilih Opsi</option>
							<?php foreach  ($list_user_bendahara as $row) { ?>
							<option value="<?=$row->id;?>">
								<?=$row->name?>
							</option>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>

        <div class="col-md-12">
            <hr>
        </div>

	<div class="block-content">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label text-uppercase">Vendor</label>
					<div class="col-md-10">
						<input type="text" class="form-control" readonly value="{namavendor}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label text-uppercase">Jenis Pembayaran</label>
					<div class="col-md-10">
						<input type="text" class="form-control" readonly value="<?=GetJenisPembayaranPengajuan($jenispembayaran)?>">
					</div>
				</div>

				<input type="hidden" name="jenispembayaran" value="{jenispembayaran}">

				<?php if($jenispembayaran == 2){ ?>
				<!-- Kontrabon -->
				<div class="form-group">
					<label class="col-md-2 control-label text-uppercase">Tanggal Kontrabon</label>
					<div class="col-md-10">
						<input type="text" class="form-control" readonly value="{tanggalkontrabon}">
					</div>
				</div>
				<?php } ?>

				<?php if($jenispembayaran == 3){ ?>
				<!-- Transfer -->
				<div class="form-group">
					<label class="col-md-2 control-label text-uppercase">No. Rekening</label>
					<div class="col-md-10">
						<input type="text" class="form-control" readonly value="{norekening}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label text-uppercase">Bank</label>
					<div class="col-md-10">
						<input type="text" class="form-control" readonly value="{bank}">
					</div>
				</div>
				<?php } ?>

				<?php if($jenispembayaran == 4){ ?>
				<!-- Termin By Progress -->
				<div class="table-responsive">
				<table class="table table-bordered table-striped" id="terminList">
					<thead>
						<tr>
							<th style="width:10%">Judul Termin</th>
							<th style="width:10%">Deskripsi</th>
							<th style="width:10%">Nominal</th>
							<th style="width:10%">Tanggal</th>
							<th style="width:10%">Status</th>
							<th style="width:10%">Aksi</th>
						</tr>
						<tr>
							<th style="display:none;">
								<input type="text" class="form-control detail" id="iddetailtermin" placeholder="ID Detail Termin" value="">
							</th>
							<th>
								<input type="text" class="form-control detail" id="judultermin" placeholder="Judul Termin" value="">
							</th>
							<th>
								<input type="text" class="form-control detail" id="deskripsi" placeholder="Deskripsi" value="">
							</th>
							<th>
								<input type="text" class="form-control detail number" id="nominal" placeholder="Nominal" value="">
							</th>
							<th>
								<input class="js-datepicker form-control detail" type="text" id="tanggaltermin" data-date-format="dd/mm/yyyy"
								 placeholder="dd/mm/yyyy" value="">
							</th>
							<th>
								<input type="text" class="form-control detail" id="statustermin" readonly placeholder="Status" value="Auto Generate">
							</th>
							<th>
								<a id="terminAdd" class="btn btn-success btn-sm">Tambahkan</a>
							</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach  ($list_termin_progress as $row) { ?>
						<tr>
							<td style="display:none;"><?=$row->id;?></td>
							<td><?=$row->subjek;?></td>
							<td><?=$row->deskripsi;?></td>
							<td><?=number_format($row->nominal);?></td>
							<td><?=$row->tanggal;?></td>
							<td style="display:none"><?=$row->statusaktivasi1;?></td>
							<td class="text-uppercase"><?=GetStatusTermin($row->statusaktivasi1);?></td>
							<td>
								<a href="#" class="btn btn-primary terminAktivasi" data-tipe="terminprogress" title="Aktivasi"><i class="fa fa-check"></i></a>
								<a href="#" class="btn btn-danger terminRemove" data-tipe="terminprogress" title="Hapus"><i class="fa fa-trash"></i></a>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				</div>
				<?php } ?>

				<?php if($jenispembayaran == 5){ ?>
				<!-- Termin Fix -->
				<div class="form-group">
					<label class="col-md-2 control-label text-uppercase">Downpayment</label>
					<div class="col-md-10">
						<input type="text" class="form-control" readonly value="{stdownpayment}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label text-uppercase">Nominal Downpayment</label>
					<div class="col-md-10">
						<input type="text" class="form-control" readonly value="{downpayment}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label text-uppercase">Cicilan</label>
					<div class="col-md-10">
						<input type="text" class="form-control" readonly value="{cicilan}">
					</div>
				</div>
				<div class="table-responsive">
				<table class="table table-bordered table-striped" id="terminFixList">
					<thead>
						<tr>
							<th style="width:10%">Deskripsi</th>
							<th style="width:10%">Nominal</th>
							<th style="width:10%">Tanggal</th>
							<th style="width:10%">Status</th>
							<th style="width:10%">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach  ($list_termin_fix as $row) { ?>
						<tr>
							<td style="display:none;"><?=$row->id;?></td>
							<td><?=$row->deskripsi;?></td>
							<td><?=number_format($row->nominal);?></td>
							<td><?=DMYFormat($row->tanggal);?></td>
							<td style="display:none"><?=$row->statusaktivasi1;?></td>
							<td class="text-uppercase"><?=GetStatusTermin($row->statusaktivasi1);?></td>
							<td>
								<a href="#" class="btn btn-primary btn-sm terminFixEdit" data-tipe="terminfix" title="Ubah"><i class="fa fa-calendar"></i></a>
								<a href="#" class="btn btn-primary btn-sm terminAktivasi" data-tipe="terminfix" title="Aktivasi"><i class="fa fa-check"></i></a>
								<a href="#" class="btn btn-danger btn-sm terminRemove" data-tipe="terminfix" title="Hapus"><i class="fa fa-trash"></i></a>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
				<?php } ?>
			</div>
		</div>
	</div>

        <div class="col-md-12">
            <hr>
        </div>

	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="detailList">
			<thead>
				<tr>
					<th style="width:10%">Nama Barang</th>
					<th style="width:10%">Merk</th>
					<th style="width:10%">Kuantitas</th>
					<th style="width:10%">Satuan</th>
					<th style="width:10%">Harga</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:10%">Keterangan</th>
					<th style="width:10%">Aksi</th>
				</tr>
				<tr id="detailForm" style="display:none">
					<th style="display:none;">
						<input type="text" class="form-control detail" id="iddetailbarang" placeholder="ID Detail Barang" value="">
					</th>
					<th>
						<input type="text" class="form-control detail" id="namabarang" placeholder="Nama Barang" value="">
					</th>
					<th>
						<input type="text" class="form-control detail" id="merk" placeholder="Merk" value="">
					</th>
					<th>
						<input type="text" class="form-control detail number" id="kuantitas" placeholder="Kuantitas" value="">
					</th>
					<th>
						<select id="idsatuan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="" disabled selected>Pilih Opsi</option>
							<?php foreach  ($list_satuan as $row) { ?>
							<option value="<?=$row->id;?>">
								<?=$row->nama?>
							</option>
							<?php } ?>
						</select>
					</th>
					<th>
						<input type="text" class="form-control detail number" id="harga" placeholder="Harga" value="">
					</th>
					<th>
						<input type="text" class="form-control detail number" id="jumlah" readonly placeholder="Jumlah" value="">
					</th>
					<th>
						<input type="text" class="form-control detail" id="keterangan" placeholder="Keterangan" value="">
					</th>
					<th>
						<a id="detailAdd" class="btn btn-success btn-sm">Ubah</a>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach  ($list_detail as $row) { ?>
				<tr>
					<td style="display:none;"><?=$row->id;?></td>
					<td><?=$row->namabarang;?></td>
					<td><?=$row->merk;?></td>
					<td><?=number_format($row->kuantitas);?></td>
					<td style="display:none;"><?=$row->idsatuan;?></td>
					<td><?=$row->namasatuan;?></td>
					<td><?=number_format($row->harga);?></td>
					<td><?=number_format($row->jumlah);?></td>
					<td><?=$row->keterangan;?></td>
					<td><a href="#" class="btn btn-primary btn-sm detailEdit" title="Ubah"><i class="fa fa-pencil"></i></a></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	</div>

        <div class="col-md-12">
            <hr>
        </div>

	<div class="block-content">
		<span class="badge badge-primary text-uppercase">Memo Kepada Bendahara</span><br><br>
		<textarea class="form-control js-summernote" placeholder="Memo Kepada Bendahara" name="memobendahara"></textarea>
	</div>
	<?php } ?>

	<div class="block-content">
		<div class="row">
			<div class="col-sm-3">
			</div>
			<div class="col-sm-9">
                <div style="float:right;">
                    <button class="btn btn-success text-uppercase" type="submit" style="width: 100px;">Simpan</button>
                    <a href="{base_url}tpengajuan" class="btn btn-default text-uppercase" type="reset" style="width: 100px;"><i class="pg-close"></i> Batal</a>
                </div>
			</div>
		</div>
		<input type="hidden" name="steppersetujuan" value="{stepPersetujuan}">
		<input type="hidden" id="countAssignment">

		<input type="hidden" id="assignmentValue" name="assignmentValue">
		<input type="hidden" id="detailValue" name="detailValue">
		<input type="hidden" id="terminValue" name="terminValue">
		<input type="hidden" id="terminFixValue" name="terminFixValue">
		<br><br>
	</div>

	<?php echo form_hidden('id', $id); ?>
	<?php echo form_close() ?>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$(".number").number(true, 0, '.', ',');

		$("#assignmentAdd").click(function () {

			countAssignment = $('table#assignmentList tbody tr').length + 1;
			$('#countAssignment').val(countAssignment);

			var valid = validateAssigment();
			if (!valid) return false;

			var rowIndex;
			var duplicate = false;

			if ($("#rowindex").val() != '') {
				var content = "";
				rowIndex = $("#rowindex").val();
			} else {
				var content = "<tr>";
				$('#assignmentList tbody tr').filter(function () {
					var $cells = $(this).children('td');
					if ($cells.eq(0).text() === $("#idpegawai").val()) {
						sweetAlert("Maaf...", "Pegawai " + $("#idpegawai option:selected").text() + " sudah ditambahkan.", "error");
						duplicate = true;
						return false;
					}
				});
			}

			if (duplicate == false) {
				content += "<td style='display:none'>" + $("#idpegawai option:selected").val() + "</td>";
				content += "<td>" + $("#idpegawai option:selected").text() + "</td>";
				content +=
					"<td><a href='#' class='btn btn-danger assignmentRemove' title='Hapus'><i class='fa fa-trash'></i></a></td>";

				if ($("#rowindex").val() != '') {
					$('#assignmentList tbody tr:eq(' + rowIndex + ')').html(content);
				} else {
					content += "</tr>";
					$('#assignmentList tbody').append(content);
				}

				detailClear();
			}
		});

		$(document).on("click", ".assignmentRemove", function () {
			if (confirm("Hapus data ?") == true) {
				$(this).closest('td').parent().remove();
			}
			return false;
		});

		// Detail Barang
		$("#kuantitas").keyup(function () {
			var harga = parseFloat($("#harga").val());
			var kuantitas = parseFloat($(this).val());

			$('#jumlah').val(harga * kuantitas);
		});

		$("#harga").keyup(function () {
			var harga = parseFloat($(this).val());
			var kuantitas = parseFloat($("#kuantitas").val());

			$('#jumlah').val(harga * kuantitas);
		});

		$("#detailAdd").click(function () {
			$("#detailForm").hide();

			var valid = validateDetail();
			if (!valid) return false;

			var rowIndex;
			var duplicate = false;

			if ($("#rowindex").val() != '') {
				var content = "";
				rowIndex = $("#rowindex").val();
			} else {
				var content = "<tr>";
				$('#detailList tbody tr').filter(function () {
					var $cells = $(this).children('td');
					if ($cells.eq(0).text() === $("#namabarang").val()) {
						sweetAlert("Maaf...", "Barang " + $("#namabarang").val() + " sudah ditambahkan.", "error");
						duplicate = true;
						return false;
					}
				});
			}

			if (duplicate == false) {
				content += "<td style='display:none;'>" + $("#iddetailbarang").val(); + "</td>";
				content += "<td>" + $("#namabarang").val() + "</td>";
				content += "<td>" + $("#merk").val() + "</td>";
				content += "<td>" + $.number($("#kuantitas").val()) + "</td>";
				content += "<td style='display:none;'>" + $("#idsatuan option:selected").val(); + "</td>";
				content += "<td>" + $("#idsatuan option:selected").text(); + "</td>";
				content += "<td>" + $.number($("#harga").val()) + "</td>";
				content += "<td>" + $.number($("#jumlah").val()) + "</td>";
				content += "<td>" + $("#keterangan").val() + "</td>";
				content += "<td><a href='#' class='btn btn-primary detailEdit' title='Ubah'><i class='fa fa-pencil'></i></a></td>";

				if ($("#rowindex").val() != '') {
					$('#detailList tbody tr:eq(' + rowIndex + ')').html(content);
				} else {
					content += "</tr>";
					$('#detailList tbody').append(content);
				}

				calculateTotal();
				detailClear();
			}
		});

		$(document).on("click", ".detailEdit", function () {
			$("#detailForm").show();

			$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
			$("#iddetailbarang").val($(this).closest('tr').find("td:eq(0)").html());
			$("#namabarang").val($(this).closest('tr').find("td:eq(1)").html());
			$("#merk").val($(this).closest('tr').find("td:eq(2)").html());
			$("#kuantitas").val($(this).closest('tr').find("td:eq(3)").html());
			$("#idsatuan").select2('val', $(this).closest('tr').find("td:eq(4)").html());
			$("#harga").val($(this).closest('tr').find("td:eq(6)").html());
			$("#jumlah").val($(this).closest('tr').find("td:eq(7)").html());
			$("#keterangan").val($(this).closest('tr').find("td:eq(8)").html());

			return false;
		});

		// Termin Progress
		$("#terminAdd").click(function () {
			var valid = validateTermin();
			if (!valid) return false;

			var rowIndex;
			var duplicate = false;

			if ($("#rowindex").val() != '') {
				var content = "";
				rowIndex = $("#rowindex").val();
			} else {
				var content = "<tr>";
				$('#terminList tbody tr').filter(function () {
					var $cells = $(this).children('td');
					if ($cells.eq(0).text() === $("#judultermin").val()) {
						sweetAlert("Maaf...", "Judul Termin " + $("#judultermin").val() + " sudah ditambahkan.", "error");
						duplicate = true;
						return false;
					}
				});
			}

			if (duplicate == false) {
				content += "<td>" + $("#judultermin").val() + "</td>";
				content += "<td>" + $("#deskripsi").val() + "</td>";
				content += "<td>" + $("#nominal").val() + "</td>";
				content += "<td>" + $("#tanggaltermin").val() + "</td>";
				content += "<td style='display:none'>" + 0 + "</td>";
				content += "<td>" + GetStatusTermin(0) + "</td>";
				content +=
					"<td><a href='#' class='btn btn-primary terminAktivasi' data-tipe='terminprogress' title='Aktivasi'><i class='fa fa-check'></i></a> <a href='#' class='btn btn-danger terminRemove' title='Hapus'><i class='fa fa-trash'></i></a></td>";

				if ($("#rowindex").val() != '') {
					$('#terminList tbody tr:eq(' + rowIndex + ')').html(content);
				} else {
					content += "</tr>";
					$('#terminList tbody').append(content);
				}

				detailClear();
			}
		});

		$(document).on("click", ".terminAktivasi", function () {
			if (confirm("Aktivasi data ?") == true) {
				if ($(this).data('tipe') == 'terminprogress') {
					$(this).closest('tr').find("td:eq(5)").html(2);
					$(this).closest('tr').find("td:eq(6)").html(GetStatusTermin(2));

					var actionRestore =
						"<td><a href='#' class='btn btn-warning terminRestore' data-tipe='terminprogress' title='Kembalikan Data'><i class='fa fa-reply'></i></a></td>";
					$(this).closest('tr').find("td:eq(7)").html(actionRestore);
				}

				if ($(this).data('tipe') == 'terminfix') {
					$(this).closest('tr').find("td:eq(4)").html(2);
					$(this).closest('tr').find("td:eq(5)").html(GetStatusTermin(2));

					var actionRestore =
						"<td><a href='#' class='btn btn-warning terminRestore' data-tipe='terminfix' title='Kembalikan Data'><i class='fa fa-reply'></i></a></td>";
					$(this).closest('tr').find("td:eq(6)").html(actionRestore);
				}
			}
			return false;
		});

		$(document).on("click", ".terminRemove", function () {
			if (confirm("Hapus data ?") == true) {
				if ($(this).data('tipe') == 'terminprogress') {
					$(this).closest('tr').find("td:eq(5)").html(0);
					$(this).closest('tr').find("td:eq(6)").html(GetStatusTermin(0));

					var actionRestore =
						"<td><a href='#' class='btn btn-warning terminRestore' data-tipe='terminprogress' title='Kembalikan Data'><i class='fa fa-reply'></i></a></td>";
					$(this).closest('tr').find("td:eq(7)").html(actionRestore);
				}

				if ($(this).data('tipe') == 'terminfix') {
					$(this).closest('tr').find("td:eq(4)").html(0);
					$(this).closest('tr').find("td:eq(5)").html(GetStatusTermin(0));

					var actionRestore =
						"<td><a href='#' class='btn btn-warning terminRestore' data-tipe='terminfix' title='Kembalikan Data'><i class='fa fa-reply'></i></a></td>";
					$(this).closest('tr').find("td:eq(6)").html(actionRestore);
				}
			}
			return false;
		});

		$(document).on("click", ".terminRestore", function () {
			if (confirm("Kembalikan data ?") == true) {
				if ($(this).data('tipe') == 'terminprogress') {
					$(this).closest('tr').find("td:eq(5)").html(1);
					$(this).closest('tr').find("td:eq(6)").html(GetStatusTermin(1));

					var actionDefault =
						"<td><a href='#' class='btn btn-primary terminAktivasi' data-tipe='terminprogress' title='Aktivasi'><i class='fa fa-check'></i></a> <a href='#' class='btn btn-danger terminRemove' data-tipe='terminprogress' title='Hapus'><i class='fa fa-trash'></i></a></td>";
					$(this).closest('tr').find("td:eq(7)").html(actionDefault);
				}

				if ($(this).data('tipe') == 'terminfix') {
					$(this).closest('tr').find("td:eq(4)").html(1);
					$(this).closest('tr').find("td:eq(5)").html(GetStatusTermin(1));

					var actionDefault =
						"<td><a href='#' class='btn btn-primary terminFixEdit' data-tipe='terminfix' title='Ubah'><i class='fa fa-calendar'></i></a> <a href='#' class='btn btn-primary terminAktivasi' data-tipe='terminfix' title='Aktivasi'><i class='fa fa-check'></i></a> <a href='#' class='btn btn-danger terminRemove' data-tipe='terminfix' title='Hapus'><i class='fa fa-trash'></i></a></td>";
					$(this).closest('tr').find("td:eq(6)").html(actionDefault);
				}
			}
			return false;
		});

		// Termin Fix
		$(document).on("click", ".terminFixEdit", function () {
			tanggalterminfix = $(this).closest('tr').find("td:eq(3)").html();
			$(this).closest('tr').find("td:eq(3)").html(
				'<input class="js-datepicker form-control" type="text" id="tanggalterminfix" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" value="' +
				tanggalterminfix + '">');
			$('.js-datepicker').datepicker();
			$('#tanggalterminfix').focus();


			var actionUpdate =
				"<td><a href='#' class='btn btn-success terminFixUpdate' data-tipe='terminfix' title='Ubah Data'><i class='fa fa-save'></i></a></td>";
			$(this).closest('tr').find("td:eq(6)").html(actionUpdate);

			return false;
		});

		$(document).on("click", ".terminFixUpdate", function () {
			tanggalterminfix = $('#tanggalterminfix').val();
			$(this).closest('tr').find("td:eq(3)").html(tanggalterminfix);

			var actionDefault =
				"<td><a href='#' class='btn btn-primary terminFixEdit' data-tipe='terminfix' title='Ubah'><i class='fa fa-calendar'></i></a> <a href='#' class='btn btn-primary terminAktivasi' data-tipe='terminfix' title='Aktivasi'><i class='fa fa-check'></i></a> <a href='#' class='btn btn-danger terminRemove' data-tipe='terminfix' title='Hapus'><i class='fa fa-trash'></i></a></td>";
			$(this).closest('tr').find("td:eq(6)").html(actionDefault);

			return false;
		});

		$("#form-work").submit(function (e) {
			var countAssignment = $('#countAssignment').val();
			if (countAssignment) {
				var form = this;

				var assignmentTbl = $('table#assignmentList tbody tr').get().map(function (row) {
					return $(row).find('td').get().map(function (cell) {
						return $(cell).html();
					});
				});

				var detailTbl = $('table#detailList tbody tr').get().map(function (row) {
					return $(row).find('td').get().map(function (cell) {
						return $(cell).html();
					});
				});

				var terminTbl = $('table#terminList tbody tr').get().map(function (row) {
					return $(row).find('td').get().map(function (cell) {
						return $(cell).html();
					});
				});

				var terminFixTbl = $('table#terminFixList tbody tr').get().map(function (row) {
					return $(row).find('td').get().map(function (cell) {
						return $(cell).html();
					});
				});

				$("#assignmentValue").val(JSON.stringify(assignmentTbl));
				$("#detailValue").val(JSON.stringify(detailTbl));
				$("#terminValue").val(JSON.stringify(terminTbl));
				$("#terminFixValue").val(JSON.stringify(terminFixTbl));

				swal({
					title: "Berhasil!",
					text: "Proses penyimpanan data.",
					type: "success",
					timer: 1500,
					showConfirmButton: false
				});
			} else {
				sweetAlert("Maaf...", "Pegawai tidak boleh kosong!", "error");
				return false;
			}
		});
	});

	function detailClear() {
		$("#rowindex").val('');
		$(".detail").val('');

		$("#idpegawai").select2("trigger", "select", {
			data: {
				id: '',
				text: ''
			}
		});
	}

	function validateAssigment() {
		if ($("#idpegawai").val() == "") {
			sweetAlert("Maaf...", "Pegawai belum dipilih!", "error");
			return false;
		}

		return true;
	}

	function validateDetail() {
		if ($("#namabarang").val() == "") {
			sweetAlert("Maaf...", "Nama Barang tidak boleh kosong!", "error");
			return false;
		}
		if ($("#merk").val() == "") {
			sweetAlert("Maaf...", "Merk tidak boleh kosong!", "error");
			return false;
		}

		if ($("#idsatuan").val() == "") {
			sweetAlert("Maaf...", "Satuan Barang belum dipilih!", "error");
			return false;
		}

		if ($("#kuantitas").val() == "" || $("#kuantitas").val() == "0") {
			sweetAlert("Maaf...", "Kuantitas tidak boleh kosong!", "error");
			return false;
		}

		if ($("#harga").val() == "" || $("#harga").val() == "0") {
			sweetAlert("Maaf...", "Harga tidak boleh kosong!", "error");
			return false;
		}

		return true;
	}

	function validateTermin() {
		if ($("#judultermin").val() == "") {
			sweetAlert("Maaf...", "Judul Termin tidak boleh kosong!", "error");
			return false;
		}
		if ($("#deskripsi").val() == "") {
			sweetAlert("Maaf...", "Deskripsi tidak boleh kosong!", "error");
			return false;
		}

		if ($("#tanggaltermin").val() == "") {
			sweetAlert("Maaf...", "Tanggal Termin belum dipilih!", "error");
			return false;
		}

		return true;
	}

	function GetStatusTermin(status) {
		if (status == 0) {
			return '<span class="label label-danger">DIHAPUS</span>';
		} else if (status == 1) {
			return '<span class="label label-primary">BELUM DIAKTIVASI</span>';
		} else if (status == 2) {
			return '<span class="label label-success">DIAKTIVASI</span>';
		}
	}
</script>