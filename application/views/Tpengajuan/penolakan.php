<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tpengajuan" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<?php echo form_open_multipart('tpengajuan/savePenolakan','class="form-horizontal push-10-t" id="form-work"') ?>
	<div class="block-content  block-content-narrow">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label text-uppercase">No. Pengajuan</label>
					<div class="col-md-10">
						<input type="hidden" name="id" value="{id}">
						<input type="text" class="form-control" readonly placeholder="No. Pengajuan" value="{nopengajuan}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label text-uppercase">Subjek</label>
					<div class="col-md-10">
						<input type="text" class="form-control" readonly placeholder="Subjek" value="{subjek}">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="block-content">
		<span class="badge badge-primary text-uppercase">Alasan</span>
		<br><br>
		<textarea class="form-control js-summernote" placeholder="Memo" name="memo">{alasan}</textarea>
		<div class="row">
			<div class="col-sm-12">
                <div style="float:right;">
                    <button class="btn btn-success text-uppercase" type="submit" style="font-size: 13px;" <?= $alasan !== null ? "disabled": "";?>>Simpan</button>
                    <a href="{base_url}tpengajuan" class="btn btn-default text-uppercase" type="reset" style="font-size: 12px;"><i class="pg-close"></i> Batal</a>
                </div>
			</div>
		</div>

		<input type="hidden" id="stproses" name="stproses" value="{stproses}">
		<br><br>
	</div>

	<?php echo form_hidden('id', $id); ?>
	<?php echo form_close() ?>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        let alasan = "<?= $alasan ?>";
        if (alasan !== "") {
            $('.js-summernote').summernote('disable');
        }
    });
</script>
