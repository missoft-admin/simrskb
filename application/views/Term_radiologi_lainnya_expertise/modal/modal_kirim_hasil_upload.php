<!-- Modal Kirim Hasil Upload -->
<div class="modal fade" id="modal-kirim-hasil-upload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="block-options">
                    <li>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </li>
                </ul>
                <h5 class="modal-title">Pengiriman Hasil Foto Radiologi</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form>
                            <div class="form-group">
                                <label for="selectTujuanLab">Tanggal Pengiriman Hasil</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group date">
                                            <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="tanggalPengirimanHasilUpload" value="<?= date("d/m/Y"); ?>">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <input type="text" class="time-datepicker form-control" id="waktuPengirimanHasilUpload" value="<?= date("H:i:s"); ?>">
                                            <span class="input-group-addon"><i class="si si-clock"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="emailKirimHasilUpload">E-mail Kirim Hasil Foto</label>
                                <input type="text" class="js-tags-input form-control" id="emailKirimHasilUpload" value="">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="btn-kirim-hasil-upload">Kirim Hasil</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on("click", "#btn-kirim-hasil-upload", function() {
        $("#cover-spin").show();

        let requestData = {
            transaksi_id: $("#transaksiId").val(),
            tanggal_pengiriman: $('#tanggalPengirimanHasilUpload').val(),
            waktu_pengiriman: $('#waktuPengirimanHasilUpload').val(),
            email_kirim_hasil: $('#emailKirimHasilUpload').val()
        };

        $.ajax({
            url: '{base_url}term_radiologi_lainnya_expertise/kirim_email_hasil_upload',
            type: 'POST',
            dataType: 'json',
            data: requestData,
            success: function(response) {
                if (response.status == true) {
                    $.toaster({
                        priority: 'success',
                        title: 'Berhasil!',
                        message: 'Data berhasil diperbarui dan email dikirim.'
                    });

                    // Re-load Datatable
                    loadListTransaksi();
                } else {
                    swal({
                        title: 'Maaf, Email Tidak Terkirim',
                        text: response.message,
                        type: 'error',
                        showCancelButton: false,
                    });
                }

                $("#cover-spin").hide();
            },
            error: function(xhr, status, error) {
                console.error('Error dalam permintaan AJAX: ' + error);
            }
        });
    });
});

</script>
