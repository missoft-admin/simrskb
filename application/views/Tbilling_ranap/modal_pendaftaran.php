<div class="modal in" id="modal_pendaftaran" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:80%">
        <div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Kunjungan Poliklinik</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal push-10-t">
					<div class="row">
						<div class="col-md-6">                               
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="notransaksi_2" placeholder="No Pendaftaran" name="notransaksi" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Pasien</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="cari_poli" placeholder="No Medrec | Nama Pasien"  value="">
								</div>
							</div>
						</div>
						<div class="col-md-6">                               
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
								<div class="col-md-9">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggal_21" name="tanggal_1" placeholder="From" value=""/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_22" name="tanggal_2" placeholder="To" value=""/>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal"></label>
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="button" onclick="getIndexPoli()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
								</div>
							</div>
						</div>
						<div class="col-md-12">                               
							<div class="table-responsive">
								<table class="table table-bordered" id="index_poli">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="8%">Action</th>
											<th width="10%">Pendaftaran</th>
											<th width="10%">Tanggal Kunjungan</th>
											<th width="8%">No Medrec</th>
											<th width="20%">Nama Pasien</th>
											<th width="15%">Dokter</th>
											<th width="10%">Asal Pasien</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
                    </div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
var idpasienpilih;
var pendaftaran_id_ranap;
var pendaftaran_id_lama;
var pendaftaran_id_baru;
function show_modal_pendaftaran(idpasien,pendaftaran_id,idpoliklinik){
	idpasienpilih=idpasien;
	pendaftaran_id_ranap=pendaftaran_id;
	pendaftaran_id_lama=idpoliklinik;
	$("#modal_pendaftaran").modal('show');
	getIndexPoli();
    $("#cover-spin").hide();
}
function getIndexPoli() {
    $("#cover-spin").show();
    // alert(ruangan_id);
    $('#index_poli').DataTable().destroy();
    table = $('#index_poli').DataTable({
		 autoWidth: false,
            searching: true,
            pageLength: 10,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			 "columnDefs": [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,4,5,6,7] },
						 { "width": "5%", "targets": [0]},
						
					],
            ajax: { 
                url: '{site_url}tpendaftaran_ranap/getIndexPoliCari',
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_1:$("#tanggal_21").val(),
						tanggal_2:$("#tanggal_22").val(),
						notransaksi:$("#notransaksi_2").val(),
						cari_poli:$("#cari_poli").val(),
						idpasien:idpasienpilih,
						
					   }
            },
			"drawCallback": function(settings) {
				$("#cover-spin").hide();
			}
			
    });
}
function get_data_pendaftaran(id){
	pendaftaran_id_baru=id;
	// alert('NEW ' + pendaftaran_id_baru + ' LAMA : '+pendaftaran_id_lama+' RANAP : ' +pendaftaran_id_ranap);
	if (pendaftaran_id_baru==pendaftaran_id_lama){
		swal({
					title: "Dibatalkan!",
					text: "Karena Pendaftaran Yang dipilih Sama.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
		return false;

	}
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Mengganti Kunjungan Poliklinik ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$("#modal_pendaftaran").modal('hide');
		// alert('NEW ' + pendaftaran_id_baru + ' LAMA : '+pendaftaran_id_lama);
		$.ajax({
			url: '{site_url}ttindakan_ranap_admin/update_kunjungan/',
			dataType: "json",
			type: "POST",
			data:{
				pendaftaran_id_lama:pendaftaran_id_lama,
				pendaftaran_id_baru:pendaftaran_id_baru,
				pendaftaran_id_ranap:pendaftaran_id_ranap,
			},
			success: function(data) {
				if (data==true){
					swal({
						title: "Berhasil!",
						text: "Proses.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});
					$('#index_all').DataTable().ajax.reload( null, false );

				}
				$("#cover-spin").hide();
				
			}
		});
	});

	
}
</script>
