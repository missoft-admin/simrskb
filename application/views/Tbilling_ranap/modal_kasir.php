<div class="modal in" id="modal_kasir" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:80%">
        <div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">DATA BILING RAWAT JALAN</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal push-10-t">
					<div class="row">
						
						<div class="col-md-6">                               
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-2 control-label" for="tanggal">Tanggal </label>
								<div class="col-md-8">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggal_kasir_1"  placeholder="From" value="{tanggal_1}"/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_kasir_2"  placeholder="To" value="{tanggal_2}"/>
									</div>
									
								</div>
								<div class="col-md-2">
									<button class="btn btn-success text-uppercase" type="button" onclick="getIndexKasir()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
								</div>
							</div>
							
						</div>
						<div class="col-md-12">                               
							<div class="table-responsive">
								<table class="table table-bordered" id="index_kasir">
									<thead>
										<tr>
											<th>No.</th>
											<th>Tanggal/Jam</th>
											<th>No. Register</th>
											<th>No. Medrec</th>
											<th>Nama Pasien</th>
											<th>Poliklinik</th>
											<th>Kelompok Pasien</th>
											<th>Rujukan</th>
											<th>Aksi</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
                    </div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
var idpasienpilih;
var pendaftaran_id_ranap;
var pendaftaran_id_lama;
var pendaftaran_id_baru;
function show_modal_kasir(idpasien){
	idpasienpilih=idpasien;
	
	$("#modal_kasir").modal('show');
	getIndexKasir();
    $("#cover-spin").hide();
}
function getIndexKasir() {
    $("#cover-spin").show();
    // alert(ruangan_id);
    $('#index_kasir').DataTable().destroy();
    table = $('#index_kasir').DataTable({
		 autoWidth: false,
            searching: true,
            pageLength: 10,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			 "columnDefs": [{
					"width": "5%","targets": 0,"orderable": true
				},
				{"width": "10%","targets": 1,"orderable": true},
				{"width": "10%","targets": 2,"orderable": true},
				{"width": "15%","targets": 3,"orderable": true},
				{"width": "10%","targets": 4,"orderable": true},
				{"width": "10%","targets": 5,"orderable": false},
				{"width": "10%","targets": 6,"orderable": false},
				{"width": "10%","targets": 7,"orderable": false},
				{"width": "20%","targets": 8,"orderable": false},
				{"width": "10%","targets": 9,"orderable": false}
			],
            ajax: { 
                url: '{site_url}ttindakan_ranap_admin/getIndex_kasir',
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggaldari:$("#tanggal_kasir_1").val(),
						tanggalsampai:$("#tanggal_kasir_2").val(),
						idpasien:idpasienpilih,
						
					   }
            },
			"drawCallback": function(settings) {
				$("#cover-spin").hide();
			}
			
    });
}
</script>
