<div class="modal in" id="RincianModalDeposit" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width:80%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title titleRincian">RINCIAN DEPOSIT</h3>
                </div>
                <div class="block-content" style="margin-bottom: 10px;padding-top: 10px;">
							<input type="hidden" id="tempIdRawatInap" name="" value="">
                            <input type="hidden" id="tempIdRow" name="" value="">
							<div class="row">
                                <div class="col-md-6">
                                    <div class="input-group" style="margin-top:15px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">No. Medrec</span>
                                        <input type="text" class="form-control tindakanNoMedrec" value="" readonly="true">
                                    </div>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">Nama Pasien</span>
                                        <input type="text" class="form-control tindakanNamaPasien" value="" readonly="true">
                                    </div>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">No. Telp</span>
                                        <input type="text" class="form-control tindakanNoTelepon" value="" readonly="true">
                                    </div>
                                    <hr>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">Penanggung Jawab</span>
                                        <input type="text" class="form-control tindakanPenanggungJawab" value="" readonly="true">
                                    </div>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">No. Telepon</span>
                                        <input type="text" class="form-control tindakanPenanggungJawabTelepon" value="" readonly="true">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group" style="margin-top:15px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">Kelompok Pasien</span>
                                        <input type="text" class="form-control tindakanKelompokPasien" value="" readonly="true">
                                    </div>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">Nama Perusahaan</span>
                                        <input type="text" class="form-control tindakanNamaPerusahaan" value="" readonly="true">
                                    </div>
                                    <hr>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">Ruangan</span>
                                        <input type="text" class="form-control tindakanRuangan" value="" readonly="true">
                                    </div>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">Kelas</span>
                                        <input type="text" class="form-control tindakanKelas" value="" readonly="true">
                                    </div>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">Bed</span>
                                        <input type="text" class="form-control tindakanBed" value="" readonly="true">
                                    </div>
                                </div>
                            </div>

							<hr>

                            <div class="form-group" style="margin-bottom: 8px;">
                                <label class="col-md-4 control-label">Tanggal</label>
                                <div class="col-md-8" style="margin-bottom: 5px;">
                                    <input type="text" id="depositTanggal" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="Tanggal" value="<?=date("d/m/Y")?>">
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 8px;">
                                <label class="col-md-4 control-label">Metode Pembayaran</label>
                                <div class="col-md-8" style="margin-bottom: 5px;">
                                    <select class="js-select2 form-control" id="depositIdMetodePembayaran" style="width: 100%;" data-placeholder="Pilih Opsi">
                                        <option value=""></option>
                                        <option value="1">Tunai</option>
                                        <option value="2">Debit</option>
                                        <option value="3">Kredit</option>
                                        <option value="4">Transfer</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" style="display: none;margin-bottom:8px;" id="depositMetodeBank">
                                <label class="col-md-4 control-label">Pembayaran</label>
                                <div class="col-md-8" style="margin-bottom: 5px;">
                                    <select class="js-select2 form-control" id="depositIdBank" style="width: 100%;" data-placeholder="Pilih Opsi">
                                        <option value="">Pilih Opsi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom:8px">
                                <label class="col-md-4 control-label" for="">Nominal</label>
                                <div class="col-md-8" style="margin-bottom: 5px;">
                                    <input type="text" class="form-control number" id="depositNominal" placeholder="Nominal" value="">
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom:8px">
                                <label class="col-md-4 control-label" for="">Terima Dari</label>
                                <div class="col-md-8" style="margin-bottom: 5px;">
                                    <input type="text" class="form-control" id="depositTerimaDari" placeholder="Terima Dari" value="">
                                </div>
                            </div>
                            
                            <div class="form-group" style="margin-bottom:8px">
                                <label class="col-md-4 control-label" for="">&nbsp;</label>
                                <div class="col-md-8" style="margin-bottom: 5px;">
                                    <button id="saveDataDeposit" class="btn btn-sm btn-success" type="button"><i class="fa fa-plus-circle"></i> Tambah</button>
                                </div>
                            </div>
                            

                            <div class="col-md-12">
                                <hr style="margin-top:10px; margin-bottom:5px;">
                            </div>

                            <?php if (UserAccesForm($user_acces_form,array('398'))) { ?>
                            <a href="" target="_blank" id="historyPrintAll" class="btn btn-sm btn-success" style="width: 20%;margin-top: 10px;margin-bottom: 10px;float:right"><i class="fa fa-print"></i> Print All</a>
                            <? } ?>
                            <button class="btn btn-sm btn-success" type="button" style="width: 20%;margin-top: 10px;margin-bottom: 10px;margin-right: 5px;float:right" disabled><i class="fa fa-money"></i> Refund</button>

                            <table id="historyDeposit" class="table table-bordered table-striped" style="margin-top: 10px;">
                                <thead>
                                    <tr>
                                        <th style="width:10%">Tanggal</th>
                                        <th style="width:25%">Metode</th>
                                        <th style="width:15%">Nominal</th>
                                        <th style="width:15%">Terima Dari</th>
                                        <th style="width:20%">User Input</th>
                                        <th style="width:15%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr style="background-color:white;">
                                        <td colspan="4" style="text-align:right;text-transform:uppercase;"><b>Total</b></td>
                                        <td id="depositTotal" style="font-weight:bold;"></td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade in black-overlay" id="AlasanHapusDepositModal" role="dialog" style="text-transform:uppercase;">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="margin-top: 150px; padding: 0 !important;">
        <div class="modal-content">
            <div class="block block-themed remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Alasan Hapus</h3>
                </div>
            </div>
            <div class="col-xs-12" style="margin-top: 10px; margin-bottom: 10px;">
                <textarea class="form-control" id="alasanhapus" placeholder="Alasan Hapus" rows="10" required></textarea>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-danger" id="removeDeposit" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>


<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {
	$('.number').number(true, 0, '.', ',');
});
function show_modal_deposit(id){
	$("#historyPrintAll").attr('href', '{site_url}trawatinap_tindakan/print_rincian_deposit/' + id);
	$("#tempIdRawatInap").val(id);
	$("#RincianModalDeposit").modal('show');
	get_data_pasien_deposit(id);
	getHistoryDeposit(id);
}
$(document).on('change', '#depositIdMetodePembayaran', function() {
        var idmetodepembayaran = $(this).val();

	if (idmetodepembayaran == 1) {
		$('#depositMetodeBank').hide();
	} else {

		// Get Bank
		$.ajax({
			url: '{site_url}trawatinap_tindakan/getBank',
			dataType: 'json',
			success: function(data) {
				$("#depositIdBank").empty();
				$("#depositIdBank").append("<option value=''>Pilih Opsi</option>");
				for (var i = 0; i < data.length; i++) {
					$("#depositIdBank").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
				}
			}
		});

		$('#depositMetodeBank').show();
	}
});
$(document).on('click', '#removeDeposit', function() {
	var idrow = $('#tempIdRow').val();
	var idrawatinap = $('#tempIdRawatInap').val();
	var alasanhapus = $('#alasanhapus').val();

	event.preventDefault();

	// Update Status
	$.ajax({
		url: '{site_url}trawatinap_tindakan/removeDataDeposit',
		method: 'POST',
		data: {
			idrow: idrow,
			alasanhapus: alasanhapus,
		},
		success: function(data) {
			swal({
				title: "Berhasil!",
				text: "Data Deposit Telah Dihapus.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});

			$("#alasanhapus").val('');
			getHistoryDeposit(idrawatinap);
		}
	});
});
$(document).on('click', '.openModalAlasanHapusDeposit', function() {
	// $("#AlasanHapusDepositModal").modal('show');
	$('#tempIdRow').val($(this).data('idrow'));
	$('#alasanhapus').val();
});

$(document).on('click', '#saveDataDeposit', function() {
	var idpendaftaran = $("#tempIdRawatInap").val();
	var tanggal = $('#depositTanggal').val();
	var idmetodepembayaran = $('#depositIdMetodePembayaran option:selected').val();
	var idbank = $('#depositIdBank option:selected').val();
	var nominal = $('#depositNominal').val();
	var terimadari = $('#depositTerimaDari').val();
	var idtipe = 1;

	if (tanggal == '') {
		sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
			$('#depositTanggal').focus();
		});
		return false;
	}

	if (idmetodepembayaran == '') {
		sweetAlert("Maaf...", "Metode Pembayaran belum dipilih !", "error").then((value) => {
			$('#depositIdMetodePembayaran').select2('open');
		});
		return false;
	}

	if (idmetodepembayaran != 1) {
		if (idbank == '') {
			sweetAlert("Maaf...", "Bank belum dipilih !", "error").then((value) => {
				$('#depositIdBank').select2('open');
			});
			return false;
		}
	}

	if (nominal == '' || nominal <= 0) {
		sweetAlert("Maaf...", "Nominal tidak boleh kosong !", "error").then((value) => {
			$('#depositNominal').focus();
		});
		return false;
	}

	$.ajax({
		url: '{site_url}trawatinap_tindakan/saveDataDeposit',
		method: 'POST',
		data: {
			idrawatinap: idpendaftaran,
			tanggal: tanggal,
			idmetodepembayaran: idmetodepembayaran,
			idbank: idbank,
			nominal: nominal,
			idtipe: idtipe,
			terimadari: terimadari
		},
		success: function(data) {
			swal({
				title: "Berhasil!",
				text: "Data Deposit Telah Tersimpan.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});

			getHistoryDeposit(idpendaftaran);

			$('#depositIdMetodePembayaran').select2("trigger", "select", {
				data: {
					id: '',
					text: ''
				}
			});
			$('#depositMetodeBank').hide();
			$('#depositIdBank').select2("trigger", "select", {
				data: {
					id: '',
					text: ''
				}
			});
			$('#depositNominal').val('');
			$('#depositTerimaDari').val('');
		}
	});
});

function get_data_pasien_deposit(idpendaftaran){
	
	$.ajax({
		url: "{site_url}Ttindakan_ranap_admin/get_data_deposit/",
		method: "POST",
		dataType: "json",
		data : {
			pendaftaran_id:idpendaftaran,
		},
		success: function(data) {
			$(".tindakanNoMedrec").val(data.no_medrec);
			$(".tindakanNamaPasien").val(data.nama_pasien);
			$(".tindakanKelompokPasien").val(data.nama_kp);
			$(".tindakanNamaPerusahaan").val(data.nama_rekanan);
			$(".tindakanNoTelepon").val(data.nohp);
			$(".tindakanPenanggungJawab").val(data.namapenanggungjawab);
			$(".tindakanPenanggungJawabTelepon").val(data.teleponpenanggungjawab);
			$(".tindakanRuangan").val(data.nama_ruangan);
			$(".tindakanKelas").val(data.nama_kelas);
			$(".tindakanBed").val(data.nama_bed);
		}
	});
	
}
function getHistoryDeposit(idpendaftaran) {
    $.ajax({
        url: '{site_url}trawatinap_tindakan/getHistoryDeposit/' + idpendaftaran,
        dataType: 'json',
        success: function(data) {
            $('#historyDeposit tbody').empty();
			console.log(data.length);
            for (var i = 0; i < data.length; i++) {
				
                var number = (i + 1);
                var metode = '';
                if (data[i].idmetodepembayaran == 1) {
                    metode = data[i].metodepembayaran;
                } else {
                    metode = data[i].metodepembayaran + ' ( ' + data[i].namabank + ' )';
                }
                var action = '';
                action = '<div class="btn-group">';
				
				<?php if (UserAccesForm($user_acces_form, array('397'))) { ?>
                    action += '<a href="{site_url}trawatinap_tindakan/print_kwitansi_deposit/' + data[i].id + '" target="_blank" class="btn btn-sm btn-success">Print <i class="fa fa-print"></i></a>';
				<? } ?>
                
				<?php if (UserAccesForm($user_acces_form, array('396'))) { ?>
					if (data[i].statustransaksi == 1) {
						var action = '<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>';
					} else {
                        action += '<button type="button" data-idrow="' + data[i].id + '" class="btn btn-sm btn-danger openModalAlasanHapusDeposit" data-toggle="modal" data-target="#AlasanHapusDepositModal">Hapus <i class="fa fa-close"></i></button>';
                    }
				<? } ?>

                action += '</div>';

                $('#historyDeposit tbody').append('<tr><td>' + data[i].tanggal + '</td><td>' + metode + '</td><td>' + $.number(data[i].nominal) + '</td><td>' + (data[i].terimadari ? data[i].terimadari : "-") + '</td><td>' + data[i].namapetugas + '</td><td>' + action + '</td></tr>');
            }

            getTotalHistoryDeposit();
        }
    });
}

function getTotalHistoryDeposit() {
    var totalkeseluruhan = 0;
    $('#historyDeposit tbody tr').each(function() {
        totalkeseluruhan += parseFloat($(this).find('td:eq(2)').text().replace(/,/g, ''));
    });

    $("#depositTotal").html($.number(totalkeseluruhan));
}

</script>
