<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}mpengaturan_penginputan_ekpertise_radiologi" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?php echo form_open('mpengaturan_penginputan_ekpertise_radiologi/save','class="form-horizontal" id="form-work"') ?>
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="data-table">
                <thead>
                    <tr>
                        <th style="width:5%" class="text-center">No</th>
                        <th style="width:10%" class="text-center">Tipe Layanan</th>
                        <th style="width:10%" class="text-center">Nama Pemeriksaan</th>
                        <th style="width:10%" class="text-center">Asal Pasien</th>
                        <th style="width:10%" class="text-center">Poliklinik / Kelas</th>
                        <th style="width:10%" class="text-center">Dokter Radiologi</th>
                        <th style="width:10%" class="text-center">Klinis</th>
                        <th style="width:10%" class="text-center">Kesan</th>
                        <th style="width:10%" class="text-center">Usul</th>
                        <th style="width:10%" class="text-center">Hasil</th>
                        <th style="width:10%" class="text-center">Aksi</th>
                    </tr>
                    <tr>
                        <th class="text-center" style="vertical-align: middle;">#</th>
                        <th>
                            <select class="js-select2 form-control" id="tipe_layanan" name="tipe_layanan" style="width: 98%;" data-placeholder="Pilih Opsi">
                                <option value="1">X-Ray</option>
                                <option value="2">USG</option>
                                <option value="3">CT-Scan</option>
                                <option value="4">MRI</option>
                                <option value="5">Lain-lain</option>
                            </select>
                        </th>
                        <th>
                            <select class="js-select2 form-control" id="pemeriksaan_id" name="pemeriksaan_id" style="width: 98%;" data-placeholder="Pilih Opsi">
                                <option value="0">Semua</option>
                                <?php foreach (get_all('merm_pemeriksaan_radiologi', ['status' => 1]) as $row) { ?>
                                <option value="<?=$row->tarif_radiologi_id?>"><?=$row->kode?> <?=$row->nama_pemeriksaan?></option>
                                <?php } ?>
                            </select>
                        </th>
                        <th>
                            <select id="asal_pasien" name="asal_pasien" class="js-select2 form-control" style="width: 98%;" data-placeholder="Pilih Opsi">
                                <option value="0">Semua</option>
                                <option value="1">Poliklinik</option>
                                <option value="2">Instalasi Gawat Daurat (IGD)</option>
                                <option value="3">Rawat Inap</option>
                                <option value="4">One Day Surgery (ODS)</option>
                            </select>
                        </th>
                        <th>
                            <select id="poliklinik_kelas" name="poliklinik_kelas" class="js-select2 form-control" style="width: 98%;" data-placeholder="Pilih Opsi">
                                <option value="0">Semua</option>
                            </select>
                        </th>
                        <th>
                            <select id="dokter_radiologi" name="dokter_radiologi" class="js-select2 form-control" style="width: 98%;" data-placeholder="Pilih Opsi">
                                <option value="0">Semua</option>
                                <?php foreach (get_all('mdokter', ['status' => 1]) as $row) { ?>
                                <option value="<?=$row->id?>"><?=$row->nama?></option>
                                <?php } ?>
                            </select>
                        </th>
                        <th>
                            <select id="klinis" name="klinis" class="js-select2 form-control" style="width: 98%;" data-placeholder="Pilih Opsi">
                                <option value="1">Tampil Wajib</option>
                                <option value="2">Tampil Saja</option>
                                <option value="3">Tidak Tampil</option>
                            </select>
                        </th>
                        <th>
                            <select id="kesan" name="kesan" class="js-select2 form-control" style="width: 98%;" data-placeholder="Pilih Opsi">
                                <option value="1">Tampil Wajib</option>
                                <option value="2">Tampil Saja</option>
                                <option value="3">Tidak Tampil</option>
                            </select>
                        </th>
                        <th>
                            <select id="usul" name="usul" class="js-select2 form-control" style="width: 98%;" data-placeholder="Pilih Opsi">
                                <option value="1">Tampil Wajib</option>
                                <option value="2">Tampil Saja</option>
                                <option value="3">Tidak Tampil</option>
                            </select>
                        </th>
                        <th>
                            <select id="hasil" name="hasil" class="js-select2 form-control" style="width: 98%;" data-placeholder="Pilih Opsi">
                                <option value="1">Tampil Wajib</option>
                                <option value="2">Tampil Saja</option>
                                <option value="3">Tidak Tampil</option>
                            </select>
                        </th>
                        <th>
                            <button type="button" id="btn-add" class="btn btn-success">Tambahkan</button>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($list_setting as $index => $row) { ?>
                    <tr>
                        <td class="text-center"><?=$index + 1;?></td>
                        <td><?=GetTipeLayananRadiologi($row->tipe_layanan);?></td>
                        <td><?=$row->pemeriksaan_id == 0 ? 'Semua' : $row->nama_pemeriksaan;?></td>
                        <td><?=$row->asal_pasien == 0 ? 'Semua' : GetAsalPasienLabel($row->asal_pasien);?></td>
                        <td><?=$row->poliklinik_kelas == 0 ? 'Semua' : $row->poliklinik_kelas_label;?></td>
                        <td><?=$row->dokter_radiologi == 0 ? 'Semua' : $row->nama_dokter;?></td>
                        <td><?=StatusPengaturanPenginputanExpertise($row->klinis);?></td>
                        <td><?=StatusPengaturanPenginputanExpertise($row->kesan);?></td>
                        <td><?=StatusPengaturanPenginputanExpertise($row->usul);?></td>
                        <td><?=StatusPengaturanPenginputanExpertise($row->hasil);?></td>
                        <td hidden><?=$row->tipe_layanan;?></td>
                        <td hidden><?=$row->pemeriksaan_id;?></td>
                        <td hidden><?=$row->asal_pasien;?></td>
                        <td hidden><?=$row->poliklinik_kelas;?></td>
                        <td hidden><?=$row->dokter_radiologi;?></td>
                        <td hidden><?=$row->klinis;?></td>
                        <td hidden><?=$row->kesan;?></td>
                        <td hidden><?=$row->usul;?></td>
                        <td hidden><?=$row->hasil;?></td>
                        <td>
                            <a href="#" class="btn btn-danger btn-remove" data-toggle="tooltip" title="Remove" onclick="confirmDelete('<?=$row->id;?>')">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>

        <?php echo form_close() ?>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#asal_pasien').on('change', function () {
        var selectedAsalPasien = $(this).val();

        // Example: Replace {base_url} with your actual base URL
        var apiUrl = '{base_url}mpengaturan_tujuan_laboratorium/getPoliklinikData/' + selectedAsalPasien;

        $.ajax({
            url: apiUrl,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                // Clear existing options
                $('#poliklinik_kelas').empty();

                // Add new options based on the fetched data
                $('#poliklinik_kelas').append('<option value="0" data-tipe="0">Semua</option>');
                $.each(data, function (index, item) {
                    $('#poliklinik_kelas').append('<option value="' + item.id + '" data-tipe="' + item.tipe + '">' + item.nama + '</option>');
                });

                // Set value to "0" (Semua) and trigger change event for Select2
                $('#poliklinik_kelas').val('0').trigger('change.select2');

                // Keep the label visible
                $('#poliklinik_kelas').siblings('label').addClass('active');
            },
            error: function (error) {
                console.error('Error fetching data:', error);
            }
        });
    });

        $('#btn-add').on('click', function(e) {
            e.preventDefault();

            // Ambil nilai dari input
            var tipe_layanan = $('#tipe_layanan option:selected').val();
            var pemeriksaan_id = $('#pemeriksaan_id option:selected').val();
            var asal_pasien = $('#asal_pasien option:selected').val();
            var poliklinik_kelas = $('#poliklinik_kelas option:selected').val();
            var dokter_radiologi = $('#dokter_radiologi option:selected').val();
            var klinis = $('#klinis option:selected').val();
            var kesan = $('#kesan option:selected').val();
            var usul = $('#usul option:selected').val();
            var hasil = $('#hasil option:selected').val();

            // Cek apakah data sudah ada di tabel
            if (isDuplicate(tipe_layanan, pemeriksaan_id, asal_pasien, poliklinik_kelas, dokter_radiologi, klinis, kesan, usul, hasil)) {
                // Tampilkan SweetAlert pesan error
                
                swal('Duplikasi Data', 'Data poliklinik dan tipe yang sama sudah ada dalam tabel.', 'warning');
            } else {
                // Lanjutkan dengan mengirim formulir atau tindakan lainnya
                $('#form-work').submit();
            }
        });

        // Fungsi untuk mengecek duplikasi data di tabel
        function isDuplicate(tipe_layanan, pemeriksaan_id, asal_pasien, poliklinik_kelas, dokter_radiologi, klinis, kesan, usul, hasil) {
            var isDuplicate = false;

            // Loop melalui baris tabel (kecuali baris header)
            $('#data-table tbody tr').each(function() {
                var existing_tipe_layanan = $(this).find('td:eq(10)').text();
                var existing_pemeriksaan_id = $(this).find('td:eq(11)').text();
                var existing_asal_pasien = $(this).find('td:eq(12)').text();
                var existing_poliklinik_kelas = $(this).find('td:eq(13)').text();
                var existing_dokter_radiologi = $(this).find('td:eq(14)').text();
                var existing_klinis = $(this).find('td:eq(15)').text();
                var existing_kesan = $(this).find('td:eq(16)').text();
                var existing_usul = $(this).find('td:eq(17)').text();
                var existing_hasil = $(this).find('td:eq(18)').text();

                console.log(existing_tipe_layanan == tipe_layanan);
                console.log(existing_pemeriksaan_id == pemeriksaan_id);
                console.log(existing_asal_pasien == asal_pasien);
                console.log(existing_poliklinik_kelas == poliklinik_kelas);
                console.log(existing_dokter_radiologi == dokter_radiologi);
                console.log(existing_klinis == klinis);
                console.log(existing_kesan == kesan);
                console.log(existing_usul == usul);
                console.log(existing_hasil == hasil);

                // Bandingkan dengan nilai input
                if (existing_tipe_layanan == tipe_layanan && existing_pemeriksaan_id == pemeriksaan_id && existing_asal_pasien == asal_pasien && existing_poliklinik_kelas == poliklinik_kelas && existing_dokter_radiologi == dokter_radiologi && existing_klinis == klinis && existing_kesan == kesan && existing_usul == usul && existing_hasil == hasil) {
                    isDuplicate = true;
                    return false; // Hentikan loop jika ada duplikasi
                }
            });

            return isDuplicate;
        }
    });

    function confirmDelete(id) {
        swal({
            title: 'Konfirmasi',
            text: 'Anda yakin ingin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Ya, Hapus',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result) {
                // Lanjutkan dengan mengarahkan ke URL penghapusan
                window.location.href = `{base_url}mpengaturan_penginputan_ekpertise_radiologi/delete/${id}`;
            }
        });
    }
</script>