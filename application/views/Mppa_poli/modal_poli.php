<!-- Modal Cari Pasien -->
<div class="modal in" id="modal_poli" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:70%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Data Poli / Dokter</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-xs-12" for="nama_dokter">Nama Dokter</label>
									<div class="col-xs-12">
										<input type="text" class="form-control" readonly id="nama_dokter" value="">
										<input type="hidden" class="form-control" readonly id="pegawai_id" value="5">
										<input type="hidden" class="form-control" readonly id="tipepegawai" value="2">
										<input type="hidden" class="form-control" readonly id="mppa_id" value="14">
									</div>
								</div>
								
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-xs-12" for="idtipe">Tipe</label>
									<div class="col-xs-12">
										<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
											<option value="#" >- All -</option>
											<option value="1" >Poliklinik</option>
											<option value="2" >IGD</option>
											
										</select>
									</div>
								</div>
								
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-xs-12" for="idtipe">Nama Poli</label>
									<div class="col-xs-12">
										<input type="text" class="form-control" id="nama_poli" value="">
									</div>
								</div>
								
								<div class="form-group" style="margin-top: 10px;">
									<label class="col-xs-12" for="snotelepon"></label>
									<div class="col-xs-12">
										<button class="btn btn-primary" type="button" id="btn_cari" style="width:100%"><i class="fa fa-search"></i> Cari</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<table width="100%" class="table table-bordered table-striped" id="index_poli">
						<thead>
							<tr>
								<th>No.</th>
								<th>Tipe</th>
								<th>Nama Poliklinik</th>
								<th>Setting Dokter</th>
								<th>Setting Lainnya</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$("#btn_cari").click(function(){
	
	loadDataPoli();
});
function check_save_dokter(mppa_id,idpoliklinik,pilih){
	// alert('id : '+id+' Pilih :'+pilih);
	$("#cover-spin").show();
		
	 $.ajax({
			url: '{site_url}mppa_poli/check_save_dokter',
			type: 'POST',
			data: {mppa_id: mppa_id,idpoliklinik:idpoliklinik,pilih:pilih},
			complete: function() {
				$("#cover-spin").hide();
				$('#index_dokter').DataTable().ajax.reload( null, false ); 
				$('#index_all').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Update'});
				
			}
	});
}
function check_save_lain(mppa_id,idpoliklinik,pilih){
	// alert('id : '+id+' Pilih :'+pilih);
	$("#cover-spin").show();
		
	 $.ajax({
			url: '{site_url}mppa_poli/check_save_lain',
			type: 'POST',
			data: {mppa_id: mppa_id,idpoliklinik:idpoliklinik,pilih:pilih},
			complete: function() {
				$("#cover-spin").hide();
				$('#index_dokter').DataTable().ajax.reload( null, false ); 
				$('#index_all').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Update'});
				
			}
	});
}

function loadDataPoli() {
	let nama_dokter=$("#nama_dokter").val();
	let nama_poli=$("#nama_poli").val();
	let idtipe=$("#idtipe").val();
	let pegawai_id=$("#pegawai_id").val();
	let tipepegawai=$("#tipepegawai").val();
	let mppa_id=$("#mppa_id").val();
	$('#index_poli').DataTable().destroy();
		var tablePasien = $('#index_poli').DataTable({
			"pageLength": 10,
			"ordering": false,
			"processing": false,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}mppa_poli/getPoli',
				type: "POST",
				dataType: 'json',
				data: {
					nama_dokter: nama_dokter,
					nama_poli: nama_poli,
					idtipe: idtipe,
					pegawai_id: pegawai_id,
					tipepegawai: tipepegawai,
					mppa_id: mppa_id,
				}
			},
			columnDefs: [
			{  className: "text-right", targets:[0] },
			{  className: "text-center", targets:[1,3,4] },
			 { "width": "5%", "targets": [0] },
			 { "width": "15%", "targets": [1] },
			 { "width": "50%", "targets": [2] },
			 { "width": "15%", "targets": [3,4] }
		]

		});
		
}

</script>
<!-- End Modal Cari Pasien -->