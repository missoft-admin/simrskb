<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<?php if (UserAccesForm($user_acces_form,array('1589'))): ?>
		<hr style="margin-top:0px">
		<?php endif ?>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-bordered table-striped" id="index_all" style="width:100%">
						<thead>
							<tr>
								<th>No</th>
								<th>Tipe</th>
								<th>NIP</th>
								<th>Nama</th>
								<th>Jumlah Poli</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<?$this->load->view('Mppa_poli/modal_poli')?>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var tabel;
$(document).ready(function(){	
	
	get_index();
})	
$("#btn_filter").click(function(){
	
	get_index();
});
function get_index() {
	let tipepegawai=$("#tipepegawai").val();
	let nama=$("#nama").val();
	let nip=$("#nip").val();
	let jenis_profesi_id=$("#jenis_profesi_id").val();
	let spesialisasi_id=$("#spesialisasi_id").val();
	let staktif=$("#staktif").val();
	$('#index_all').DataTable().destroy();
		var tabel = $('#index_all').DataTable({
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			 "scrollX": false,
			"order": [],
			"ajax": {
				url: '{site_url}mppa_poli/getIndex_all',
				type: "POST",
				dataType: 'json',
				data: {
					tipepegawai: tipepegawai,
					nama: nama,
					nip: nip,
					jenis_profesi_id: jenis_profesi_id,
					spesialisasi_id: spesialisasi_id,
					staktif: staktif,
				}
			},
			columnDefs: [
			{  className: "text-right", targets:[0] },
			{  className: "text-center", targets:[1,4] },
			 { "width": "5%", "targets": [0] },
			 { "width": "10%", "targets": [1] },
			 { "width": "15%", "targets": [2,4] },
			 { "width": "20%", "targets": [3,5] }
		]

		});
		
}
function myDelete(id){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus PPA?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
				$("#cover-spin").show();
				 $.ajax({
					url: "{site_url}mppa_poli/hapus_ppa/",
					method: "POST",
					dataType: "json",
					data:{id:id},
					success: function(data) {
						get_index();
						$("#cover-spin").hide();
					}
				});
		});

}
function setting_poli(mppa_id,tipepegawai,pegawai_id,nama_pegawai){
	$("#nama_dokter").val(nama_pegawai);
	$("#mppa_id").val(mppa_id);
	$("#tipepegawai").val(tipepegawai);
	$("#pegawai_id").val(pegawai_id);
	$("#modal_poli").modal('show');
	loadDataPoli();
}
</script>
