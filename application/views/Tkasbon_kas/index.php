<?= ErrorSuccess($this->session) ?>
<?php if ($error != '') echo ErrorMessage($error) ?>
<?php //if (UserAccesForm($user_acces_form,array('1090'))){?>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{site_url}tkasbon/create" class="btn"><i class="fa fa-plus"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-bottom: 0px;">
	</div>
	<div class="block-content">
		<form class="form-horizontal" action="{site_url}trefund/filter" method="post">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label">Tipe</label>
						<div class="col-md-9">
							<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
								<option value="#" <?=($idtipe == '#' ? 'selected' : '')?>>- All Tipe -</option>
								<option value="1" <?=($idtipe == 1 ? 'selected' : '')?>>Dokter</option>
								<option value="2" <?=($idtipe == 2 ? 'selected' : '')?>>Pegawai</option>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label">Pegawai</label>
						<div class="col-md-9">
							<select id="idpegawai" name="idpegawai" class="js-select2 form-control" style="width: 100%;">
								<option value="#" selected>Semua Pegawai</option>

							</select>
						</div>
					</div>

				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label">Status Proses</label>
						<div class="col-md-9">
							<select id="status" name="status" class="js-select2 form-control" style="width: 100%;">
								<option value="#" <?=($status=='#'?'selected':'')?>>Semua proses</option>
								<option value="1" <?=($status=='1'?'selected':'')?>>Menunggu Proses</option>
								<option value="2" <?=($status=='2'?'selected':'')?>>Proses Approval</option>
								<option value="3  <?=($status=='3'?'selected':'')?>">Disetujui</option>
								<option value="4" <?=($status=='4'?'selected':'')?>>Ditolak</option>
								<option value="5" <?=($status=='5'?'selected':'')?>>Telah Ditransaksikan</option>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label">Tanggal</label>
						<div class="col-md-9">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" id="tanggal1" name="tanggal1" placeholder="From" value="">
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" id="tanggal2" name="tanggal2" placeholder="To" value="">
							</div>

						</div>
					</div>
					<?php if (UserAccesForm($user_acces_form,array('1091'))){?>
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="button" id="btn_filter" name="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						</div>
					</div>
					<?}?>
				</div>
			</div>
		</form>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<table id="table_index" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>id</th>
							<th>No</th>
							<th>Tanggal & Waktu</th>
							<th>Tipe</th>
							<th>Nama</th>
							<th>Catatan</th>
							<th>Nominal</th>
							<th>Alokasi</th>
							<th>Status</th>
							<th>Status Proses</th>
							<th width="9%">Aksi</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<?//}?>
<div class="modal fade in" id="modal-alasan-refund" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close">
								<i class="fa fa-close" style="display:block;font-size:15px;"></i>
							</button>
						</li>
					</ul>
					<h3 class="block-title">Hapus Transaksi</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal">
						<div class="form-group" style="margin-bottom: 5px;">
							<div class="col-md-12">
								<label class="control-label-custom text-uppercase" for="">Alasan</label>
								<textarea id="alasan" class="form-control" name="name" rows="7" cols="80"></textarea>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<div class="col-md-12">
								<button class="btn btn-sm btn-success text-uppercase" id="delete-refund" type="button" style="margin:10px 0;" data-dismiss="modal">Simpan
								</button>
							</div>
						</div>
						<input id="idrefund" type="hidden" readonly>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade in black-overlay" id="modal_approval" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<input type="hidden" class="form-control" id="idtipe" placeholder="" name="idtipe" value="">
						<input type="hidden" class="form-control" id="nominal_trx" placeholder="" name="nominal_trx" value="">
						<input type="hidden" class="form-control" id="idtrx" placeholder="" name="idtrx" value="">
						<div class="col-md-12">
							<table width="100%" id="tabel_user" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Step</th>
										<th>User</th>
										<th>Jika Setuju</th>
										<th>Jika Menolak</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

					</div>
					<?php echo form_close() ?>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-primary" id="btn_simpan_approval" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Proses Persetujuan</button>
			</div>

		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_user" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<input type="hidden" class="form-control" id="idrka" placeholder="" name="idrka" value="">
						<div class="col-md-12">
							<table width="100%" id="tabel_user_proses" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Step</th>
										<th>User</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

					</div>
					<?php echo form_close() ?>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>
<div class="modal fade" id="modal_tolak" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Alasan Tolak</h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Alasan</label>
								<div class="col-md-8">
									<textarea class="form-control" rows="2" name="alasan_tolak" id="alasan_tolak"></textarea>
									<input type="hidden" readonly class="form-control" id="id_approval"  placeholder="id_approval" name="id_approval" value="">
								</div>
							</div>
						</div>
					</div>

				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-danger text-uppercase" type="button" name="btn_tolak" id="btn_tolak"><i class="fa fa-save"></i> SIMPAN TOLAK</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.datepicker').datepicker({
			format: "dd/mm/yyyy",
			todayHighlight: true,
		});
		load_index();

		$(document).on('change', '#idtipe', function() {
			if ($("#idtipe").val()=='#'){
				$("#idpegawai").empty();
				$('#idpegawai').append('<option value="#" selected>- Pilih Semua -</option>');
			}else{
				refresh_pegawai();
			}
		});
		function refresh_pegawai(){
			var idtipe=$("#idtipe").val();;
			$.ajax({
				url: '{site_url}tkasbon_kas/refresh_pegawai/'+idtipe,
				dataType: "json",
				success: function(data) {
					$("#idpegawai").empty();
					$('#idpegawai').append('<option value="#">- Pilih Semua -</option>');
					$('#idpegawai').append(data.detail);
				}
			});
		}

		$(document).on('click', '.show-refund', function() {
			var id = $(this).data('idtransaksi');

			$('#modal-alasan-refund').modal('show');
			$('#idrefund').val(id);
		});
		$(document).on('click', '#btn_filter', function() {
			load_index();
		});
		$(document).on('click', '.approval', function() {
			$('#modal_approval').modal();
			let idtipe = $(this).data('idtipe');
			let nominal_trx = $(this).data('nominal');
			let idtrx = $(this).data('id');
			// alert(idtipe + ' '+ nominal_trx);return false;
			// alert(idtipe);
			$('#idtipe').val(idtipe);
			$('#nominal_trx').val(nominal_trx);
			$('#idtrx').val(idtrx);

			load_user_approval();

		});
		function load_user_approval(){
			var idtipe=$("#idtipe").val();
			var nominal_trx=$("#nominal_trx").val();
			$('#tabel_user').DataTable().destroy();
			table=$('#tabel_user').DataTable({
					"autoWidth": false,
					"pageLength": 10,
					"ordering": false,
					"processing": true,
					"serverSide": true,
					"order": [],
					"ajax": {
						url: '{site_url}tkasbon_kas/load_user_approval',
						type: "POST",
						dataType: 'json',
						data : {
							idtipe:idtipe,
							nominal_trx:nominal_trx,

						   }
					},
					"columnDefs": [
						 {  className: "text-right", targets:[0] },
						 {  className: "text-center", targets:[1,2,3] },
						 { "width": "10%", "targets": [0] },
						 { "width": "40%", "targets": [1] },
						 { "width": "25%", "targets": [2,3] },

					]
				});
		}
		$(document).on("click","#btn_simpan_approval",function(){
			swal({
				title: "Apakah Anda Yakin ?",
				text : "Melanjutkan Step Persetujuan?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#d26a5c",
				cancelButtonText: "Tidak Jadi",
			}).then(function() {
				$.ajax({
					url: '{site_url}tkasbon_kas/simpan_proses_peretujuan/'+$("#idtrx").val(),
					type: 'POST',
					complete: function() {
						swal({
							title: "Berhasil!",
							text: "Proses Approval Berhasil.",
							type: "success",
							timer: 1500,
							showConfirmButton: false
						});
						$('#table_index').DataTable().ajax.reload( null, false );
					}
				});
			});

			return false;
		});
		$(document).on("click",".user",function(){
			var table = $('#table_index').DataTable()
			var tr = $(this).parents('tr')
			var id = table.cell(tr,0).data()
			$("#idtrx").val(id);
			load_user();
		});

		$(document).on("click",".verif",function(){
			var table = $('#table_index').DataTable()
			var tr = $(this).parents('tr');
			var id = table.cell(tr,0).data();
			var id_approval=$(this).data('id');

			var tanggal = $(this).data('tanggal');
			var idtipe = $(this).data('idtipe');
			var idpegawai = $(this).data('idpegawai');
			var catatan = $(this).data('catatan');
			var nominal = $(this).data('nominal');

			swal({
				title: "Apakah Anda Yakin  ?",
				text : "Verifikasi Kasbon ini?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#d26a5c",
				cancelButtonText: "Tidak Jadi",
			}).then(function() {
				$.ajax({
					url: '{site_url}tkasbon_kas/verifikasi',
					data: {
						id : id,
						tanggal : tanggal,
						idtipe : idtipe,
						idpegawai : idpegawai,
						catatan : catatan,
						nominal : nominal
					},
					type: 'POST',
					success: function(data) {
						// console.log(data);
						// alert(data);
						$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi berhasil'});
						$('#table_index').DataTable().ajax.reload( null, false );
						// if (data=='"1"'){
						// window.location.href = "{site_url}mrka_approval/next_bendahara/"+id;
						// }
					}
				});
			});

			return false;

		});
		function load_user(){
			var idtrx=$("#idtrx").val();

			$("#modal_user").modal('show');

			// alert(idrka);
			$.ajax({
				url: '{site_url}tkasbon_kas/list_user/'+idtrx,
				dataType: "json",
				success: function(data) {
					$("#tabel_user_proses tbody").empty();
					$("#tabel_user_proses tbody").append(data.detail);
				}
			});
		}
		$(document).on('click', '#delete-refund', function() {
			var id = $('#idrefund').val();
			var alasan = $('#alasan').val();

			$.ajax({
				type: 'POST',
				url: "{site_url}tkasbon_kas/removeKasbon/" + id,
				dataType: 'json',
				data: {
					alasan: alasan
				},
				success: function(data) {
					swal({
						title: "Berhasil!",
						text: "Proses penyimpanan data.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					reload();
				}
			});
		});
		$(document).on("click",".setuju",function(){
			var table = $('#table_index').DataTable()
			var tr = $(this).parents('tr')
			var id = table.cell(tr,0).data()
			var id_approval=$(this).data('id');
			swal({
				title: "Apakah Anda Yakin  ?",
				text : "Menyetujui Pengajuan ini?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#d26a5c",
				cancelButtonText: "Tidak Jadi",
			}).then(function() {
				$.ajax({
					url: '{site_url}tkasbon_kas/setuju_batal/'+id_approval+'/1'+'/'+id,
					type: 'POST',
					success: function(data) {
						// console.log(data);
						// alert(data);
						// $.toaster({priority : 'success', title : 'Succes!', message : ' Pengajuan berhasil disetujui'});
						$('#table_index').DataTable().ajax.reload( null, false );
						// if (data=='"1"'){
						// window.location.href = "{site_url}mrka_approval/next_bendahara/"+id;
						// }
					}
				});
			});

			return false;

		});
		$(document).on("click",".batal",function(){
			var table = $('#table_index').DataTable()
			var tr = $(this).parents('tr')
			var id = table.cell(tr,0).data()
			var id_approval=$(this).data('id');
			swal({
				title: "Apakah Anda Yakin  ?",
				text : "Mengembalikan Status Konfirmasi Pengajuan ini?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#d26a5c",
				cancelButtonText: "Tidak Jadi",
			}).then(function() {
				$.ajax({
					url: '{site_url}tkasbon_kas/setuju_batal/'+id_approval+'/0',
					type: 'POST',
					complete: function() {
						$.toaster({priority : 'success', title : 'Succes!', message : ' Pengembalian berhasil disetujui'});
						$('#table_index').DataTable().ajax.reload( null, false );
					}
				});
			});

			return false;

		});
		$(document).on("click","#btn_tolak",function(){

			var id_approval=$("#id_approval").val();
			var alasan_tolak=$("#alasan_tolak").val();
			swal({
				title: "Apakah Anda Yakin  ?",
				text : "Menolak Pengajuan ini?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#d26a5c",
				cancelButtonText: "Tidak Jadi",
			}).then(function() {
				$.ajax({
					url: '{site_url}tkasbon_kas/tolak/',
					type: 'POST',
					data: {
						id_approval:id_approval,
						alasan_tolak:alasan_tolak,
					},
					complete: function() {
						$('#modal_tolak').modal('hide');
						$.toaster({priority : 'success', title : 'Succes!', message : ' Pengajuan berhasil ditolak'});
						$('#table_index').DataTable().ajax.reload( null, false );

					}
				});
			});

			return false;

		});
		$(document).on("click",".tolak",function(){
			var table = $('#table_index').DataTable()
			var tr = $(this).parents('tr')
			var id = table.cell(tr,0).data()
			var id_approval=$(this).data('id');
			$("#id_approval").val(id_approval)
			$('#modal_tolak').modal('show');
			// modal_tolak

		});
		$(document).on('click', '.edit-refund', function() {
			let id = $(this).data('idtransaksi');
			$('#idTransaksi').val(id);

			$('#modalEditKasbon').modal('show');
			$('#updatePembayaranKasbon').attr('disabled', false);
			$.ajax({
				url: "{base_url}tkasbon_kas/getInfoKasbon/" + id,
				dataType: 'json',
				success: function(data) {
					if (data.idmetode == '1') {
		        $('.formNonTunai').hide();
		        $('#bankKasbon').attr('readonly', true);
		        $('#norekeningKasbon').attr('readonly', true);
		      } else {
		        $('.formNonTunai').show();
		        $('#bankKasbon').val('').removeAttr('readonly');
		        $('#norekeningKasbon').val('').removeAttr('readonly');
		      }

					$('#norefund').val(data.norefund);
					$('#notransaksi').val(data.notransaksi);
					$('#nominalKasbon').val($.number(data.totalrefund));
					$('#metodeKasbon').select2('val', data.idmetode);
					$('#bankKasbon').val(data.bank);
					$('#norekeningKasbon').val(data.norekening);
					$('#alasanKasbon').val(data.alasan);
				}
			});
		});
		$(document).on('click', '.lihat-refund', function() {
			let id = $(this).data('idtransaksi');
			$('#idTransaksi').val(id);

			$('#modalEditKasbon').modal('show');

			$.ajax({
				url: "{base_url}tkasbon_kas/getInfoKasbon/" + id,
				dataType: 'json',
				success: function(data) {
					if (data.idmetode == '1') {
		        $('.formNonTunai').hide();
		        $('#bankKasbon').attr('readonly', true);
		        $('#norekeningKasbon').attr('readonly', true);
		      } else {
		        $('.formNonTunai').show();
		        $('#bankKasbon').val('').removeAttr('readonly');
		        $('#norekeningKasbon').val('').removeAttr('readonly');
		      }

					$('#norefund').val(data.norefund);
					$('#notransaksi').val(data.notransaksi);
					$('#nominalKasbon').val($.number(data.totalrefund));
					$('#metodeKasbon').select2('val', data.idmetode);
					$('#bankKasbon').val(data.bank);
					$('#norekeningKasbon').val(data.norekening);
					$('#alasanKasbon').val(data.alasan);
				}
			});
					$('#updatePembayaranKasbon').attr('disabled', true);

		});
	});
	function load_index(){
		var status=$("#status").val();
		var idtipe=$("#idtipe").val();
		var idpegawai=$("#idpegawai").val();
		var tanggal1=$("#tanggal1").val();
		var tanggal2=$("#tanggal2").val();
		$('#table_index').DataTable().destroy();
		table=$('#table_index').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tkasbon_kas/getIndex',
					type: "POST",
					dataType: 'json',
					data : {
						status:status,
						idtipe:idtipe,
						idpegawai:idpegawai,
						tanggal1:tanggal1,
						tanggal2:tanggal2,
					   }
				},
				"columnDefs": [
					{ "width": "0%", "targets": 0, "visible": false },
					{ "width": "5%", "targets": 1, "visible": true},
					{ "width": "8%", "targets": [2,3,6], "visible": true},
					{ "width": "9%", "targets": [8,9], "visible": true},
					{ "width": "10%", "targets": [5,7], "visible": true},
					{ "width": "12%", "targets": [4,10], "visible": true},
					{ "targets": [4,5,7,10],"class":"text-left" },
					{ "targets": [2,3,8,9],"class":"text-center" },
					{ "targets": [1,6],"class":"text-right" },
					// { "width": "10%", "targets": 3, "visible": true,"class":"text-left" },
					// { "width": "10%", "targets": 4, "visible": true,"class":"text-left" },
					// { "width": "10%", "targets": 5, "visible": true,"class":"text-right" },
					// { "width": "10%", "targets": 6, "visible": true ,"class":"text-left"},
				]
			});
	}
</script>
