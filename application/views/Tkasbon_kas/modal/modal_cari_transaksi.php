<div class="modal in" id="modalCariTransaksi" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-popout" style="width:1200px;">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close">
								<i class="fa fa-close" style="display:block;font-size:15px;"></i>
							</button>
						</li>
					</ul>
					<h3 class="block-title">Cari Transaksi</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="col-md-8 form-horizontal">
							<div class="form-val">
								<label class="col-sm-2" for="">Tipe</label>
								<div class="col-sm-10">
									<select id="tIdTipeTransaksi" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Semua Tipe</option>
										<option value="1">Rawat Jalan</option>
										<option value="2">Rawat Inap</option>
										<option value="3">One Day Surgery (ODS)</option>
									</select>
								</div>
							</div>
							<div class="form-val" style="margin-top:50px">
								<label class="col-sm-2" for="" style="margin-top: 8px;">Tanggal</label>
								<div class="col-sm-10">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input id="tTanggalAwal" class="form-control datepicker" type="text" placeholder="Tanggal Awal" value="<?= date("d/m/Y") ?>">
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input id="tTanggalAkhir" class="form-control datepicker" type="text" placeholder="Tanggal Akhir" value="<?= date("d/m/Y") ?>">
									</div>
								</div>
							</div>
							<div class="form-val" style="margin-top:50px">
								<label class="col-sm-2" for="" style="margin-top: 8px;"></label>
								<div class="col-sm-10" style="margin-top:15px">
									<button type="submit" id="tFilter" class="btn btn-success" style="font-size: 12px;">Filter</button>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div class="text-uppercase">
						<table id="search-list" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Tipe</th>
									<th>No Transaksi</th>
									<th>No Medrec</th>
									<th>Nama Pasien</th>
									<th>Kelompok Pasien</th>
									<th>Dokter Penanggung Jawab</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#btnFind').click(function() {
			let tipeRefund = parseInt($('#tipeRefund').val());
			if (tipeRefund == 0) {
				searchList(0, null, null, 0);
				$("#label-01").html("Total Deposit");
				$("#label-02").html("Total Transaksi");
			} else if (tipeRefund == 1) {
				searchList(1, null, null, 0);
				$("#label-01").html("Total Retur");
				$("#label-02").html("Total Transaksi");
			} else if (tipeRefund == 2) {
				searchList(2, null, null, 0);
				$("#label-01").html("Nama Dokter");
				$("#label-02").html("Kelompok Pasien");
			}
		});

		$('#tFilter').click(function() {
			let tipeRefund = $('#tipeRefund').val();
			let tanggalAwal = $('#tTanggalAwal').val();
			let tanggalAkhir = $('#tTanggalAkhir').val();
			let idTipeTransaksi = $('#tIdTipeTransaksi').val();

			// console.log('tipeRefund' + tipeRefund);
			// console.log('tanggalAwal' + tanggalAwal);
			// console.log('tanggalAkhir' + tanggalAkhir);
			// console.log('idTipeTransaksi' + idTipeTransaksi);

			searchList(tipeRefund, tanggalAwal, tanggalAkhir, idTipeTransaksi);
		});

		$(document).on('click', '.selectTransaksi', function() {
			$(".separator").show();
			$('#tipeTransaksi').val('');

			let reference = $(this).data("reference");
			let tipeRefund = $('#tipeRefund').val();
			let idTransaksi = $(this).data('idtransaksi');

			if (tipeRefund == '0') {
				getInfoRefundDeposit(idTransaksi);
			} else if (tipeRefund == '1') {
				getInfoRefundObat(idTransaksi);
			} else if (tipeRefund == '2') {
				getInfoRefundTransaksi(reference, idTransaksi);
			}

			$('#idTransaksi').val(idTransaksi);
			$('#idTransaksi').select2('destroy');
			$('#idTransaksi').select2();

			$("#modalCariTransaksi .close").click();
		});
	});

	function searchList(tipeRefund, tanggalAwal, tanggalAkhir, idTipeTransaksi) {
		$('#search-list').DataTable().destroy();
		$('#search-list').DataTable({
			"oLanguage": {
				"sEmptyTable": "<i class='text-uppercase'>NO DATA AVAILABLE IN TABLE</i>",
			},
			"ajax": {
				"url": "{base_url}Trefund/searchList",
				"type": "POST",
				"data": {
					"tiperefund": tipeRefund,
					"tanggalawal": tanggalAwal,
					"tanggalakhir": tanggalAkhir,
					"idtipetransaksi": idTipeTransaksi,
				}
			},
			"autoWidth": false,
			"pageLength": 10,
			"lengthChange": false,
			"searching": true,
			"ordering": false,
			"processing": true,
			"bInfo": true,
			"bPaginate": true,
			"order": [],
			"initComplete": function(settings, json) {
			}
		});
	}
</script>
