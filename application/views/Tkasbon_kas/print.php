<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Bukti Kasbon</title>
    <style>
    body{
        font-family: "Courier", Arial,sans-serif;
    }
    @media print {
      table {
        font-size: 24px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .header {
        font-size: 48px !important;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
        font-size: 24px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .header {
        font-size: 48px !important;
        font-weight: bold;
        text-align: center;
      }
      .subheader {
        font-size: 32px !important;
        text-align: center;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <table class="content">
      <tr>
        <td class="header">BUKTI KASBON</td>
      </tr>
      <tr>
        <td class="subheader">{notransaksi}</td>
      </tr>
    </table>
    <br>
    <table class="content-2">
      <tr>
        <td style="width:150px">TGL KASBON</td>
        <td style="width:20px">:</td>
        <td>{tanggal}</td>
        <td style="width:180px">ALOKASI DANA</td>
        <td style="width:20px">:</td>
        <td>{alokasidana}</td>
      </tr>
      <tr>
        <td>NAMA</td>
        <td style="width:20px">:</td>
        <td>{namapegawai}</td>
      </tr>
      <tr>
        <td>NOMINAL</td>
        <td style="width:20px">:</td>
        <td>Rp. {nominal}</td>
      </tr>
      <tr>
        <td>TERBILANG</td>
        <td style="width:20px">:</td>
        <td>{terbilang}</td>
      </tr>
      <tr>
        <td>DESKRIPSI</td>
        <td style="width:20px">:</td>
        <td>{deskripsi}</td>
      </tr>
    </table>
    <br>
    <table>
      <tr>
        <td>
          <table>
            <tr>
              <td style="text-align:center; width: 50px;" class="border-full"><b>BENDAHARA</b></td>
              <td style="text-align:center; width: 120px;" class="border-full"><b>KASIR</b></td>
            </tr>
            <tr>
              <td class="border-full"><br><br><br><br><br><br></td>
              <td class="border-full"><br><br><br><br><br><br></td>
            </tr>
          </table>
        </td>
        <td style="text-align:center; vertical-align: top;">
          <b>Bandung, <?=date("d").' '.MONTHFormat(date("m")).' '.date("Y");?></b><br>Yang Menerima
          <br><br><br><br><br><br>
          _____________________________
        </td>
      </tr>
    </table>
  </body>
</html>
