<?php echo ErrorSuccess($this->session)?>
<?php
    if ($error != '') {
        echo ErrorMessage($error);
    }
?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">

		<hr style="margin-top:0px">
		<div class="row">
			<?php echo form_open('trm_pengelolaan_hold/filter_detail/' . $iddokter . '/' . $idruangan, 'class="form-horizontal" id="form-work"') ?>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Nama</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="namapasien" placeholder="Nama" value="{namapasien}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">No. Medrec</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="nomedrec" placeholder="No. Medrec" value="{nomedrec}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Tipe</label>
					<div class="col-md-8">
						<select name="tipe" class="js-select2 form-control" style="width: 100%;">
							<option value="#" <?=($tipe == '#' ? 'selected' : '')?>>Semua Layanan</option>
							<option value="1" <?=($tipe == '1' ? 'selected' : '')?>>Rawat Jalan</option>
							<option value="2" <?=($tipe == '2' ? 'selected' : '')?>>Instalasi Gawat Darurat (IGD)</option>
							<option value="3" <?=($tipe == '3' ? 'selected' : '')?>>Rawat Inap</option>
							<option value="4" <?=($tipe == '4' ? 'selected' : '')?>>One Day Surgery (ODS)</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Tanggal</label>
					<div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
							<input class="form-control" type="text" name="tanggal_dari" placeholder="Tanggal Dari" value="{tanggal_dari}">
							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
							<input class="form-control" type="text" name="tanggal_sampai" placeholder="Tanggal Sampai" value="{tanggal_sampai}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Status</label>
					<div class="col-md-8">
						<select name="status" class="js-select2 form-control" style="width: 100%;">
							<option value="#" <?=($status == '#' ? 'selected' : '')?>>Semua Status</option>
							<option value="hold" <?=($status == 'hold' ? 'selected' : '')?>>✅ Hold</option>
							<option value="non-hold" <?=($status == 'non-hold' ? 'selected' : '')?>>❎ Hold</option>
							<option value="minus" <?=($status == 'minus' ? 'selected' : '')?>>✅ Berkas ( - )</option>
							<option value="non-minus" <?=($status == 'minus' ? 'selected' : '')?>>❎ Berkas ( - )</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for=""></label>
					<div class="col-md-9">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
		<hr>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>Tanggal</th>
					<th>No. Medrec</th>
					<th>Nama</th>
					<th>Tipe</th>
					<th>Dokter</th>
					<th>Hold</th>
					<th>Berkas (-)</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var uri = '<?= $this->uri->segment(2); ?>';
	var iddokter = '<?= $this->uri->segment(3); ?>';
	var idruangan = '<?= $this->uri->segment(4); ?>';

	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}trm_pengelolaan_hold/getDetail/' + uri + '/' + iddokter + '/' + idruangan,
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "10%", "targets": 0, "orderable": true },
					{ "width": "5%", "targets": 1, "orderable": true },
					{ "width": "10%", "targets": 2, "orderable": true },
					{ "width": "10%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 4, "orderable": true },
					{ "width": "5%", "targets": 5, "orderable": true },
					{ "width": "5%", "targets": 6, "orderable": true },
					{ "width": "10%", "targets": 7, "orderable": true }
				]
			});
	});
</script>
