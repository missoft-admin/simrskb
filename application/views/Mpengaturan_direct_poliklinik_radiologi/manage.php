<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}mpengaturan_direct_poliklinik_radiologi" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?php echo form_open('mpengaturan_direct_poliklinik_radiologi/save','class="form-horizontal" id="form-work"') ?>
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="data-table">
                <thead>
                    <tr>
                        <th style="width:5%" class="text-center">No</th>
                        <th style="width:20%" class="text-center">Poliklinik</th>
                        <th style="width:20%" class="text-center">Tipe Tujuan</th>
                        <th style="width:10%" class="text-center">Aksi</th>
                    </tr>
                    <tr>
                        <th class="text-center" style="vertical-align: middle;">#</th>
                        <th>
                            <select class="js-select2 form-control" id="idpoliklinik" name="idpoliklinik" style="width: 98%;" data-placeholder="Pilih Opsi">
                                <option value="0" selected disabled>Pilih Opsi</option>
                                <?php foreach (get_all('mpoliklinik', ['status' => 1]) as $row) { ?>
                                <option value="<?=$row->id?>"><?=$row->nama?></option>
                                <?php } ?>
                            </select>
                        </th>
                        <th>
                            <select class="js-select2 form-control" id="idtipe" name="idtipe" style="width: 98%;" data-placeholder="Pilih Opsi">
                                <option value="1">X-Ray</option>
                                <option value="2">USG</option>
                                <option value="3">CT-Scan</option>
                                <option value="4">MRI</option>
                                <option value="5">Lain-lain</option>
                            </select>
                        </th>
                        <th>
                            <button type="button" id="btn-add" class="btn btn-success">Tambahkan</button>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($list_setting as $index => $row) { ?>
                    <tr>
                        <td class="text-center"><?=$index + 1;?></td>
                        <td><?=$row->nama_poliklinik;?></td>
                        <td><?=GetTipeRadiologi($row->idtipe);?></td>
                        <td hidden><?=$row->idpoliklinik;?></td>
                        <td hidden><?=$row->idtipe;?></td>
                        <td>
                            <a href="#" class="btn btn-danger btn-remove" data-toggle="tooltip" title="Remove" onclick="confirmDelete('<?=$row->id;?>')">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>

        <?php echo form_close() ?>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#btn-add').on('click', function(e) {
            e.preventDefault();

            // Ambil nilai dari input
            var idpoliklinik = $('#idpoliklinik option:selected').val();
            var idtipe = $('#idtipe option:selected').val();

            // Cek apakah data sudah ada di tabel
            if (isDuplicate(idpoliklinik, idtipe)) {
                // Tampilkan SweetAlert pesan error
                
                swal('Duplikasi Data', 'Data poliklinik dan tipe yang sama sudah ada dalam tabel.', 'warning');
            } else {
                // Lanjutkan dengan mengirim formulir atau tindakan lainnya
                $('#form-work').submit();
            }
        });

        // Fungsi untuk mengecek duplikasi data di tabel
        function isDuplicate(idpoliklinik, idtipe) {
            var isDuplicate = false;

            // Loop melalui baris tabel (kecuali baris header)
            $('#data-table tbody tr').each(function() {
                var existingIdpoliklinik = $(this).find('td:eq(3)').text();
                var existingIdtipe = $(this).find('td:eq(4)').text();

                // Bandingkan dengan nilai input
                if (existingIdpoliklinik == idpoliklinik && existingIdtipe == idtipe) {
                    isDuplicate = true;
                    return false; // Hentikan loop jika ada duplikasi
                }
            });

            return isDuplicate;
        }
    });

    function confirmDelete(id) {
        swal({
            title: 'Konfirmasi',
            text: 'Anda yakin ingin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Ya, Hapus',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result) {
                // Lanjutkan dengan mengarahkan ke URL penghapusan
                window.location.href = `{base_url}mpengaturan_direct_poliklinik_radiologi/delete/${id}`;
            }
        });
    }
</script>