<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mcuti_dokter" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mcuti_dokter/save', 'class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="">Dokter</label>
				<div class="col-md-7">
					<select name="iddokter" class="form-control js-select2">
						<option value="">Pilih Dokter</option>
						<?php foreach (get_all('mdokter', array('status' => '1')) as $row) { ?>
							<option value="<?= $row->id; ?>" <?= ($row->id == $iddokter ? 'selected' : ''); ?>><?= $row->nama; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="">Sebab</label>
				<div class="col-md-7">
					<select name="sebab" class="form-control js-select2" style="width: 100%;">
						<option value="">Pilih Sebab Cuti</option>
						<option value="Cuti" <?= ($sebab == 'Cuti' ? 'selected' : '') ?>>Cuti</option>
						<option value="Cuti Sakit" <?= ($sebab == 'Cuti Sakit' ? 'selected' : '') ?>>Cuti Sakit</option>
						<option value="Tidak Praktek" <?= ($sebab == 'Tidak Praktek' ? 'selected' : '') ?>>Tidak Praktek</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="">Tanggal</label>
				<div class="col-md-7">
					<div class="input-group date">
						<input class="form-control js-datepicker" type="text" name="tanggal_dari" placeholder="Tanggal Dari" value="{tanggal_dari}" data-date-format="dd/mm/yyyy">
						<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
						<input class="form-control js-datepicker" type="text" name="tanggal_sampai" placeholder="Tanggal Sampai" value="{tanggal_sampai}" data-date-format="dd/mm/yyyy">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="">Alasan</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="alasan" placeholder="Alasan" name="alasan" value="{alasan}">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mbank" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
		<?php echo form_hidden('id', $id); ?>
		<?php echo form_close() ?>
	</div>
</div>
