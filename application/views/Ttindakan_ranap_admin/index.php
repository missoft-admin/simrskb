<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>
<?php $user_id = $this->session->userdata('user_id'); ?>
<?
?>
<style>
.text-wrap {      
   word-wrap: break-word;      
   width: 20em;      
}    
</style>
<div class="block">
    <?
		$list_ruangan=get_all('mruangan',array('status'=>1,'idtipe'=>1));
	?>
			<input type="hidden" id="pendaftaran_id_pilih" name="pendaftaran_id_pilih" value="">
			<input type="hidden" id="tab_detail" name="tab_detail" value="{tab_detail}">
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div id="div_expand" class="block block-opt-hidden">
				<div class="block-header bg-primary">
					<div class="block-options-simple">
						<button class="btn btn-xs btn-warning" onclick="click_expand()"  type="button" ><i class="fa fa-expand"></i></button>
					</div>
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 10px;">
							<div class="col-md-2">
								<label class="control-label" for="idruangan_index">Ruangan</label>
								<select id="idruangan_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua Ruangan -</option>
									<?foreach($list_ruangan as $r){?>
									<option value="<?=$r->id?>"><?=$r->nama?></option>
									<?}?>
									
								</select>
							</div>
							<?
									$list_kelas=get_all('mkelas',array('status'=>1));
							?>
							<div class="col-md-2">
								<label class="control-label" for="idkelas_index">Kelas</label>
								<select id="idkelas_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua Kelas -</option>
									<?foreach($list_kelas as $r){?>
									<option value="<?=$r->id?>"><?=$r->nama?></option>
									<?}?>
									
								</select>
							</div>
							<div class="col-md-2">
								<label class="control-label" for="idbed_index">Bed</label>
								<select id="idbed_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua Bed -</option>
								</select>
							</div>
							<div class="col-md-2">
								<label class="control-label" for="iddokter_perujuk_index">Dokter Perujuk</label>
								<select id="iddokter_perujuk_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua Dokter -</option>
									<?foreach($list_dokter as $r){?>
									<option value="<?=$r->id?>"><?=$r->nama?></option>
									<?}?>
								</select>
							</div>
							<div class="col-md-2">
								<label class="control-label" for="iddokter">Dokter</label>
								<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua Dokter -</option>
									<?foreach($list_dokter as $r){?>
									<option value="<?=$r->id?>"><?=$r->nama?></option>
									<?}?>
								</select>
							</div>
							<div class="col-md-2">
								<label class="control-label" for="idkelompokpasien_index">Kelompok Pasien</label>
								<select id="idkelompokpasien_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua -</option>
									<?foreach(get_all('mpasien_kelompok',array('status'=>1)) as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
									<?}?>
								</select>
							</div>
							<?
								$list_rekanan=get_all('mrekanan',array('status'=>1));
							?>
							<div class="col-md-2">
								<label class="control-label" for="idrekanan_index">Asuransi</label>
								<select id="idrekanan_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua -</option>
									<?foreach($list_rekanan as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
									<?}?>
								</select>
							</div>
							<div class="col-md-2">
								<label class="control-label" for="status_tindakan_index">Status Tindakan</label>
								<select id="status_tindakan_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua -</option>
									<option value="3" >DIRAWAT</option>
									<option value="2" >MENUNGGU TRANSAKSI</option>
									<option value="4" >SELESAI TRANSAKSI</option>
									<option value="5" >PULANG</option>
									
								</select>
							</div>
							<div class="col-md-2">
								<label class="control-label" for="idtipe">Tipe</label>
								<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua  -</option>
									<option value="1" >RAWAT INAP</option>
									<option value="2" >ODS</option>
								</select>
							</div>
							<div class="col-md-4">
								<label class="control-label" for="cari_pasien">Pasien</label>
								<div class="input-group">
									<input type="text" class="form-control" id="cari_pasien" placeholder="Nama Pasien | No. Medrec" name="cari_pasien" value="">
									<span class="input-group-btn">
										<button class="btn btn-default" onclick="load_index_all()"  type="button"><i class="fa fa-search"></i> Search</button>
									</span>
								</div>
							</div>
						</div>
				</div>
					
				</div>
				<div class="block-content">
					
					<div class="row pull-10">
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="idruangan_index">Asal Pasien</label>
								<div class="col-md-9">
									<select id="idtipe_asal" name="idtipe_asal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- Semua  -</option>
										<option value="1" >POLIKLINIK</option>
										<option value="2" >IGD</option>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="idkelas_index">Tanggal Pendaftaran</label>
								<div class="col-md-9">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggal_1" name="tanggal_1" placeholder="From" value="{tanggal_1}"/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_2" name="tanggal_2" placeholder="To" value="{tanggal_2}"/>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="idbed">DPJP</label>
								<div class="col-md-9">
									<select id="iddokter_dpjp" class="js-select2 form-control" multiple style="width: 100%;" data-placeholder="Choose one..">
										<?foreach($list_dokter as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="idbed">Kelompok Pasien</label>
								<div class="col-md-9">
									<select id="idkelompokpasien_array" multiple class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach(get_all('mpasien_kelompok',array('status'=>1)) as $r){?>
											<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="no_medrec">No Medrec</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="no_medrec" placeholder="No. Medrec" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="nopendaftaran">No Pendaftaran</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="nopendaftaran_index" placeholder="No Pendaftaran" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="nopendaftaran">Ruang Perawatan</label>
								<div class="col-md-9">
									<select id="idruangan_array" multiple class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach($list_ruangan as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="nopendaftaran">Bed</label>
								<div class="col-md-9">
									<select id="idbed_array" multiple class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach(get_all('mbed',array('status'=>1)) as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="idkelas_index">Tanggal Transaksi</label>
								<div class="col-md-9">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggal_1_trx" placeholder="From" value=""/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_2_trx" placeholder="To" value=""/>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-md-6 push-10">
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="idpoliklinik_array">Nama Poliklinik</label>
								<div class="col-md-9">
									<select id="idpoliklinik_array" multiple class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach(get_all('mpoliklinik',array('status'=>1)) as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="iddokter_perujuk_array">Dokter Perujuk</label>
								<div class="col-md-9">
									<select id="iddokter_perujuk_array" multiple class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach($list_dokter as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="dpjp_pendukung_array">DPJP Pendamping</label>
								<div class="col-md-9">
									<select id="dpjp_pendukung_array" multiple class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach($list_dokter as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="idrekanan_array">Perusahaan Asuransi</label>
								<div class="col-md-9">
									<select id="idrekanan_array" multiple class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach($list_rekanan as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="namapasien">Nama Pasien</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="namapasien" placeholder="Nama Pasien" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="norujukan_index">No Rujukan</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="norujukan_index" placeholder="No Rujukan" value="">
								</div>
							</div>
							
							
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="idkelas_array">Kelas</label>
								<div class="col-md-9">
									<select id="idkelas_array" multiple class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach($list_kelas as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="status_tindakan_array">Status Tindakan</label>
								<div class="col-md-9">
									<select id="status_tindakan_array" multiple class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="3" >DIRAWAT</option>
										<option value="2" >MENUNGGU TRANSAKSI</option>
										<option value="4" >SELESAI TRANSAKSI</option>
										<option value="5" >PULANG</option>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal_1_pulang">Tanggal Pulang</label>
								<div class="col-md-9">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggal_1_pulang" placeholder="From" value=""/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_2_pulang" placeholder="To" value=""/>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="btn_filter_all"></label>
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="button" onclick="load_index_all()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
								</div>
							</div>
								
						</div>
					</div>
				</div>
			</div>
			
			<?php echo form_close() ?>
			
			<div class="block">
				<ul class="nav nav-pills push-10-l">
					<li  id="div_utama_3" class="<?=($tab_utama=='3'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab_utama(3)"><i class="fa fa-bed"></i> Dirawat Semua</a>
					</li>
					
					<li  id="div_utama_2" class="<?=($tab_utama=='2'?'active':'')?>">
						<a href="javascript:void(0)"  onclick="set_tab_utama(2)"><i class="si si-pin"></i> Menunggu Transaksi </a>
					</li>
					<li  id="div_utama_4" class="<?=($tab_utama=='4'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab_utama(4)"><i class="fa fa-check"></i> Selesai Transaksi</a>
					</li>
					<li  id="div_utama_5" class="<?=($tab_utama=='5'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab_utama(5)"><i class="si si-logout"></i> Pulang</a>
					</li>
					<li id="div_utama_1" class="<?=($tab_utama=='1'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab_utama(1)"><i class="si si-list"></i> Semua Pasien</a>
					</li>
				</ul>
				<div class="block-content">
						<input type="hidden" id="tab_utama" name="tab_utama" value="{tab_utama}">
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_all">
										<thead>
											<tr>
												<th width="10%"></th>
												<th width="10%"></th>
												<th width="10%"></th>
												<th width="10%"></th>
												<th width="10%"></th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					
				
				</div>
			</div>
</div>
<? $this->load->view('Tpendaftaran_poli_ttv/modal_alergi')?>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>

<? $this->load->view('Ttindakan_ranap_admin/modal_deposit')?>
<? $this->load->view('Ttindakan_ranap_admin/modal_pendaftaran')?>
<? $this->load->view('Ttindakan_ranap_admin/modal_kasir')?>
<? $this->load->view('Ttindakan_ranap_admin/modal_tipe')?>
<? $this->load->view('Ttindakan_ranap_deposit/modal_tindakan_opr')?>
<script type="text/javascript">
var table;
var tab;
var tab_detail=<?=$tab_detail?>;
var tab_utama=<?=$tab_utama?>;
var st_login;
var st_expand=false;
$(document).ready(function() {
	// $("#RincianGantiPoli").modal('show');
	load_index_all();
});
function modal_terima(pendaftaran_id_terima){
	$("#pendaftaran_id_terima").val(pendaftaran_id_terima);
	$("#modal_terima").modal('show');
}
function terima_ranap(){
	let id=$("#pendaftaran_id_terima").val();
	let tanggalterima=$("#tanggalterima").val();
	let waktuterima=$("#waktuterima").val();
	
	$("#modal_terima").modal('hide');
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menerima Rawat Inap?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap/terima_ranap', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					tanggalterima:tanggalterima,
					waktuterima:waktuterima,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Hapus TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#modal_terima").modal('hide');
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					load_index_all();
				}
			}
		});
	});

}
function show_lock(id,statuskasir){
	let label_='';
	if (statuskasir=='1'){
		label_='Akan Mengkunci Transaksi ?';
	}else{
		label_='Akan Membuka Transaksi Kembali ?';
	}
	swal({
		title: "Apakah Anda Yakin ?",
		text : label_,
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Ttindakan_ranap_admin/set_lock', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					statuskasir:statuskasir,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Hapus Lock.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Proses'});
					load_index_all();
				}
			}
		});
	});

}
$(document).on("change", "#idruangan_index,#idkelas_index", function() {
    get_bed_index();
});
function get_bed_index(){
	let ruangan_id=$("#idruangan_index").val();
	let idkelas=$("#idkelas_index").val();
	$.ajax({
		url: '{site_url}treservasi_bed/get_bed/',
		dataType: "json",
		type: "POST",
		data:{
			idruangan:ruangan_id,
			idkelas:idkelas,
		},
		success: function(data) {
			$("#idbed_index").empty();
			$('#idbed_index').append('<option value="#" selected>- Semua Bed -</option>');
			$('#idbed_index').append(data.detail);
		}
	});

}
function load_index_all(){
	$('#index_all').DataTable().destroy();	
	let tanggal_1=$("#tanggal_1").val();
	let tanggal_2=$("#tanggal_2").val();
	
	
	// alert(ruangan_id);
	table = $('#index_all').DataTable({
            autoWidth: false,
            searching: true,
            serverSide: true,
			
			"processing": false,
            "order": [],
            "pageLength": 100,
            "ordering": false,
			"columnDefs": [
					{ "width": "10%", "targets": 0},
					{ "width": "40%", "targets": 1},
					{ "width": "20%", "targets": 2},
					{ "width": "20%", "targets": 3},
					{ "width": "10%", "targets": 4},
					
				],
            ajax: { 
                url: '{site_url}Ttindakan_ranap_admin/getIndex_all', 
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						tab:tab_utama,
						status_tindakan_index:$("#status_tindakan_index").val(),
						iddokter:$("#iddokter").val(),
						idtipe:$("#idtipe").val(),
						idtipe_asal:$("#idtipe_asal").val(),
						cari_pasien:$("#cari_pasien").val(),
						nopendaftaran:$("#nopendaftaran_index").val(),
						idruangan:$("#idruangan_index").val(),
						idkelas:$("#idkelas_index").val(),
						idbed:$("#idbed_index").val(),
						norujukan:$("#norujukan_index").val(),
						iddokter_perujuk:$("#iddokter_perujuk_index").val(),
						idkelompokpasien:$("#idkelompokpasien_index").val(),
						idrekanan:$("#idrekanan_index").val(),
						idpoliklinik_array:$("#idpoliklinik_array").val(),
						iddokter_perujuk_array:$("#iddokter_perujuk_array").val(),
						iddokter_dpjp:$("#iddokter_dpjp").val(),
						dpjp_pendukung_array:$("#dpjp_pendukung_array").val(),
						idkelompokpasien_array:$("#idkelompokpasien_array").val(),
						idrekanan_array:$("#idrekanan_array").val(),
						no_medrec:$("#no_medrec").val(),
						namapasien:$("#namapasien").val(),
						idruangan_array:$("#idruangan_array").val(),
						idkelas_array:$("#idkelas_array").val(),
						idbed_array:$("#idbed_array").val(),
						status_tindakan_array:$("#status_tindakan_array").val(),
						tanggal_1_trx:$("#tanggal_1_trx").val(),
						tanggal_2_trx:$("#tanggal_2_trx").val(),
						tanggal_1_pulang:$("#tanggal_1_pulang").val(),
						tanggal_2_pulang:$("#tanggal_2_pulang").val(),
						
					   }
            },
			"drawCallback": function( settings ) {
				 $("#index_all thead").remove();
			 }  
        });
	$("#cover-spin").hide();
}
function click_expand(){
	// 
	if (st_expand==false){
		$("#div_expand").removeClass("block-opt-hidden");
		st_expand=true;
	}else{
		$("#div_expand").addClass("block-opt-hidden");
		st_expand=false;
	}
}
function set_tab($tab) {
    tab_detail = $tab;
    // alert(tab);
    // $("#cover-spin").show();
    document.getElementById("div_1").classList.remove("active");
    document.getElementById("div_2").classList.remove("active");
    document.getElementById("div_3").classList.remove("active");
    document.getElementById("div_4").classList.remove("active");
    if (tab_detail == '1') {
        document.getElementById("div_1").classList.add("active");
    }
    if (tab_detail == '2') {
        document.getElementById("div_2").classList.add("active");
    }
    if (tab_detail == '3') {
        document.getElementById("div_3").classList.add("active");
    }
	if (tab_detail == '4') {
        document.getElementById("div_4").classList.add("active");
    }
	
    getIndex_reservasi();
}
function set_tab_utama($tab) {
    tab_utama = $tab;
    // alert(tab);
    // $("#cover-spin").show();
    document.getElementById("div_utama_1").classList.remove("active");
    document.getElementById("div_utama_2").classList.remove("active");
    document.getElementById("div_utama_3").classList.remove("active");
    document.getElementById("div_utama_4").classList.remove("active");
    document.getElementById("div_utama_5").classList.remove("active");
    if (tab_utama == '1') {
        document.getElementById("div_utama_1").classList.add("active");
    }
    if (tab_utama == '2') {
        document.getElementById("div_utama_2").classList.add("active");
    }
    if (tab_utama == '3') {
        document.getElementById("div_utama_3").classList.add("active");
    }
	if (tab_utama == '4') {
        document.getElementById("div_utama_4").classList.add("active");
    }
	if (tab_utama == '5') {
        document.getElementById("div_utama_5").classList.add("active");
    }
	
    load_index_all();
}

</script>
