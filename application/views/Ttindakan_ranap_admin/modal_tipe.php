<div class="modal in" id="modal_tipe" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-popout">
        <div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">EDIT TIPE PASIEN</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal push-10-t">
					<div class="row">
						
						<div class="col-md-12">                               
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-12" for="Tipe Pasien">Tipe Pasien </label>
								<div class="col-md-12">
									<select id="idtipepasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="1" >Pasien RS</option>
										<option value="2" >Pasien Pribadi</option>
										
									</select>
								</div>
								
							</div>
							
						</div>
                    </div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-success" onclick="update_tipe_pasein()" type="button" ><i class="fa fa-save"></i> Simpan</button>
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_catatan" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-popout">
        <div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">EDIT CATATAN</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal push-10-t">
					<div class="row">
						
						<div class="col-md-12">                               
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-12" for="Tipe Pasien">Catatan </label>
								<div class="col-md-12">
									<textarea id="catatan" class="form-control auto_blur" name="story" rows="2" style="width:100%"></textarea>
								</div>
								
							</div>
							
						</div>
                    </div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-success" onclick="update_catatan()" type="button" ><i class="fa fa-save"></i> Simpan</button>
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">

function modal_edit_tipe(pendaftaran_id_ranap,idtipepasien){
	$("#pendaftaran_id_pilih").val(pendaftaran_id_ranap);
	$("#modal_tipe").modal('show');
	$("#idtipepasien").val(idtipepasien).trigger('change');
}
function update_tipe_pasein(){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Mengganti Tipe Pasien ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$("#modal_tipe").modal('hide');
		// alert('NEW ' + pendaftaran_id_baru + ' LAMA : '+pendaftaran_id_lama);
		$.ajax({
			url: '{site_url}ttindakan_ranap_admin/update_tipe_pasein/',
			dataType: "json",
			type: "POST",
			data:{
				id:$("#pendaftaran_id_pilih").val(),
				idtipepasien:$("#idtipepasien").val(),
			},
			success: function(data) {
				if (data==true){
					swal({
						title: "Berhasil!",
						text: "Proses.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});
					$('#index_all').DataTable().ajax.reload( null, false );

				}
				$("#cover-spin").hide();
				
			}
		});
	});
}
function modal_edit_catatan(pendaftaran_id_ranap){
	$("#pendaftaran_id_pilih").val(pendaftaran_id_ranap);
	$("#modal_catatan").modal('show');
	$.ajax({
		url: '{site_url}ttindakan_ranap_admin/get_catatan/',
		dataType: "json",
		type: "POST",
		data:{
			id:pendaftaran_id_ranap,
		},
		success: function(data) {
			$("#catatan").val(data.catatan);
		}
	});
}
function update_catatan(){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Edit Catatan ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$("#modal_catatan").modal('hide');
		// alert('NEW ' + pendaftaran_id_baru + ' LAMA : '+pendaftaran_id_lama);
		$.ajax({
			url: '{site_url}ttindakan_ranap_admin/update_catatan/',
			dataType: "json",
			type: "POST",
			data:{
				id:$("#pendaftaran_id_pilih").val(),
				catatan:$("#catatan").val(),
			},
			success: function(data) {
				if (data==true){
					swal({
						title: "Berhasil!",
						text: "Proses.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});
					$('#index_all').DataTable().ajax.reload( null, false );

				}
				$("#cover-spin").hide();
				
			}
		});
	});
}
</script>
