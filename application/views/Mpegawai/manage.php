<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mpegawai" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mpegawai/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nip">NIP</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nip" placeholder="NIP" name="nip" value="{nip}" required="" aria-required="true">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="nip">NIP HRMS</label>
				<div class="col-md-7">
					<div class="input-group">
						<input type="text" class="form-control" readonly id="nip_hrms" placeholder="NIP HRMS" name="nip_hrms" value="{nip_hrms}" required="" aria-required="true">
						<span class="input-group-btn">
							<button class="btn btn-primary btn" onclick="show_pegawai()" <?=($nip_hrms?'disabled':'')?> type="button" title="Rekening Vendor"><i class="fa fa-search"></i> Cari HRMS</button>
						</span>
					</div>
					
					
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="jeniskelamin">Jenis Kelamin</label>
				<div class="col-md-7">
					<select name="jeniskelamin" id="jeniskelamin" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($jeniskelamin == 1 ? 'selected="selected"':'')?>>Laki-laki</option>
						<option value="2" <?=($jeniskelamin == 2 ? 'selected="selected"':'')?>>Perempuan</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="alamat">Alamat</label>
				<div class="col-md-7">
					<textarea class="form-control" id="alamat" placeholder="Alamat" name="alamat" required="" aria-required="true">{alamat}</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tempatlahir">Tempat Lahir</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="tempatlahir" placeholder="Tempat Lahir" name="tempatlahir" value="{tempatlahir}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tanggallahir">Tanggal Lahir</label>
				<div class="col-md-7">
					<input class="js-datepicker form-control" type="text" id="tanggallahir" name="tanggallahir" value="{tanggallahir}" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="telepon">Telepon</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="telepon" placeholder="Telepon" name="telepon" value="{telepon}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="idkategori">Kategori</label>
				<div class="col-md-7">
					<select name="idkategori" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($idkategori == 1 ? 'selected="selected"':'')?>>Pegawai Umum</option>
						<option value="2" <?=($idkategori == 2 ? 'selected="selected"':'')?>>Pegawai Kamar Operasi</option>
						<option value="3" <?=($idkategori == 3 ? 'selected="selected"':'')?>>Pegawai Fisioterapi</option>
						<option value="4" <?=($idkategori == 4 ? 'selected="selected"':'')?>>Pegawai Anesthesi</option>
						<option value="5" <?=($idkategori == 5 ? 'selected="selected"':'')?>>Pegawai Rawat Inap</option>
						<?php if($statusAvailableApoteker == 0 || $idkategori == 6){ ?>
							<option value="6" <?=($idkategori == 6 ? 'selected="selected"':'')?>>Pegawai Apoteker</option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mpegawai" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>
<?
	$this->load->view('Mpegawai/modal_pegawai_add');
?>