<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('3'))): ?>
			<ul class="block-options">
		        <li>
		            <a href="{base_url}mpegawai/create" class="btn"><i class="fa fa-plus"></i></a>
		        </li>
		    </ul>
		<?php endif ?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<?php if (UserAccesForm($user_acces_form,array('249'))): ?>
		<hr style="margin-top:0px">
		<?php echo form_open('mpegawai/filter','class="form-horizontal" id="form-work"') ?>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien">Kategori</label>
					<div class="col-md-8">
						<select name="idkategori" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="0" <?=($idkategori == 0 ? 'selected="selected"':'')?>>- All -</option>
							<option value="1" <?=($idkategori == 1 ? 'selected="selected"':'')?>>Pegawai Umum</option>
							<option value="2" <?=($idkategori == 2 ? 'selected="selected"':'')?>>Pegawai Kamar Operasi</option>
							<option value="3" <?=($idkategori == 3 ? 'selected="selected"':'')?>>Pegawai Fisioterapi</option>
							<option value="4" <?=($idkategori == 4 ? 'selected="selected"':'')?>>Pegawai Anesthesi</option>
							<option value="5" <?=($idkategori == 5 ? 'selected="selected"':'')?>>Pegawai Rawat Inap</option>
							<?php if($statusAvailableApoteker == 0 || $idkategori == 6){ ?>
								<option value="6" <?=($idkategori == 6 ? 'selected="selected"':'')?>>Pegawai Apoteker</option>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien">Jenis Kelamin</label>
					<div class="col-md-8">
						<select name="jeniskelamin" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="0" <?=($jeniskelamin == 0 ? 'selected="selected"':'')?>>- All -</option>
							<option value="1" <?=($jeniskelamin == 1 ? 'selected="selected"':'')?>>Laki-laki</option>
							<option value="2" <?=($jeniskelamin == 2 ? 'selected="selected"':'')?>>Perempuan</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-12">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close() ?>
		<hr>
		<?php endif ?>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>NIP</th>
					<th>Nama</th>
					<th>Jenis Kelamin</th>
					<th>Kategori</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		</div>
	</div>
</div>
<?
	$this->load->view('Mpegawai/modal_pegawai');
?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mpegawai/getIndex/'+{idkategori}+'/'+{jeniskelamin},
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "10%", "targets": 0, "orderable": true },
					{ "width": "20%", "targets": 1, "orderable": true },
					{ "width": "20%", "targets": 2, "orderable": true },
					{ "width": "15%", "targets": 3, "orderable": true },
					{ "width": "15%", "targets": 4, "orderable": true },
					{ "width": "20%", "targets": 5, "orderable": true }
				]
			});
	});
</script>
