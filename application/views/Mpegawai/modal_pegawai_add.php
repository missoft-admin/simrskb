<div class="modal fade in black-overlay" id="modal_pegawai" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:40%">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">DATA KARYAWAN YANG BELUM TERHUBUNG</h3>
				</div>
				<div class="block-content">
					<div class="block">
						
						<div class="block-content tab-content">
							<div class="tab-pane fade fade-left in active" id="btabs-animated-slideleft-list">
								<h5 class="font-w300 push-15" id="lbl_list_pegawai">Dafar Pegawai </h5>
								<table id="tabel_list_pegawai" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 5%;">No</th>
											<th style="width: 15%;">Nip</th>
											<th style="width: 15%;">Nama</th>
											<th style="width: 15%;">Departmen</th>
											<th style="width: 15%;">Unit</th>
											<th style="width: 5%;">Jabatan</th>
											<th style="width: 8%;">Actions</th>
										</tr>
										
									</thead>
									<tbody>
										
									</tbody>									
								</table>
							</div>
							
							
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function show_pegawai(){
		
		clear_pegawai();
		$("#modal_pegawai").modal('show');
		

		load_list_pegawai();
	}
	function clear_pegawai(){
		$("#idpegawai").val('');
		$("#nip").val('');
		$("#nama").val('');
		$("#nama_hrms").val('');
		$("#nip_hrms").val('');
	}
	
	$(document).on("click",".pilih_pegawai",function(){
		var table = $('#tabel_list_pegawai').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		$("#nip").val(table.cell(tr,1).data());
		$("#nip_hrms").val(table.cell(tr,1).data());
		$("#nama").val(table.cell(tr,2).data());
		
		$("#modal_pegawai").modal('hide');
		var nip=table.cell(tr,1).data();
		$.ajax({
			url: '{site_url}mpegawai/get_data_pegawai_hrms/',
			dataType: "json",
			type: "POST",
			data:{
				nip:nip
			},
			success: function(data) {
				$("#alamat").val(data.alamat);
				$("#jeniskelamin").val(data.jeniskelamin).trigger('change');
				$("#tempatlahir").val(data.tempatlahir);
				$("#tanggallahir").val(data.tanggallahir);
				$("#telepon").val(data.telepon);
			
			}
		});
    });
	
	$(document).on("click","#btn_save_pegawai",function(){
		if (!validate_detail_pegawai()) return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Simpan Nip Pegawai ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			save_nip();
		});

	});
	function save_nip(){
		var idpegawai=$("#idpegawai").val();		
		var nip_hrms=$("#nip_hrms").val();		
		var nama_hrms=$("#nama_hrms").val();		
		
		$.ajax({
			url: '{site_url}mpegawai/save_nip',
			type: 'POST',
			datatype: 'json',
			data: {
				nip_hrms: nip_hrms,
				nama_hrms: nama_hrms,
				idpegawai: idpegawai,
			},
			complete: function() {
				sweetAlert("Berhasil...", "Data Pegawai disimpan!", "success");
				$('#datatable-simrs').DataTable().ajax.reload( null, false );
				clear_pegawai();
				$("#modal_pegawai").modal('hide');
			}
		});
	
	}
	function validate_detail_pegawai()
	{
		// alert($("#id_alat").val());
		if ($("#nip_hrms").val()==''){
			sweetAlert("Maaf...", "Pilih Pegawai di HRMS", "error");
			return false;
		}
		return true;
	}

	function load_list_pegawai(){
		// var idvendor=$("#idvendor").val();
		
		$('#tabel_list_pegawai').DataTable().destroy();
		table=$('#tabel_list_pegawai').DataTable({
			"autoWidth": false,
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}mpegawai/load_list_pegawai',
				type: "POST",
				dataType: 'json',
				data : {
					// idvendor:idvendor,
				   }
			},
			

			
		});
	}
	
	
</script>