<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}msetting_jurnal_kas_lain" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('msetting_jurnal_kas_lain/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>	
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_primary('Proses Posting Mutasi')?></label>
				<div class="col-md-4">
					<select id="st_auto_posting_mutasi" name="st_auto_posting_mutasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="1" <?=(1 == $st_auto_posting_mutasi ? 'selected' : ''); ?>>AUTO POSTING</option>
                            <option value="2" <?=(2 == $st_auto_posting_mutasi ? 'selected' : ''); ?>>MANUAL</option>
                        </select>
				</div>
			</div>
			
			
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_primary('Batas Waktu Batal Mutasi')?></label>
				
				<div class="col-md-4">
					<input type="text" style="width: 100%"  class="form-control number" id="batas_batal_mutasi" placeholder="0" name="batas_batal_mutasi" value="<?=$batas_batal_mutasi?>">
				</div>
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_danger('Proses Posting Pengajuan')?></label>
				<div class="col-md-4">
					<select id="st_auto_posting_pengajuan" name="st_auto_posting_pengajuan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="1" <?=(1 == $st_auto_posting_pengajuan ? 'selected' : ''); ?>>AUTO POSTING</option>
                            <option value="2" <?=(2 == $st_auto_posting_pengajuan ? 'selected' : ''); ?>>MANUAL</option>
                        </select>
				</div>
			</div>
			
			
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_danger('Batas Waktu Batal Pengajuan')?></label>
				
				<div class="col-md-4">
					<input type="text" style="width: 100%"  class="form-control number" id="batas_batal_pengajuan" placeholder="0" name="batas_batal_pengajuan" value="<?=$batas_batal_pengajuan?>">
				</div>
				<div class="col-md-2">
					<button type="button" class="btn btn-sm btn-success" tabindex="8" id="btn_update" title="Update"><i class="fa fa-save"></i> UPDATE</button>
				</div>
			</div>
			
			
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);

	});	
	$(document).on("click","#btn_update",function(){
		if (validate_update()==false)return false;
		var st_auto_posting_mutasi=$("#st_auto_posting_mutasi").val();
		var st_auto_posting_pengajuan=$("#st_auto_posting_pengajuan").val();
		var batas_batal_mutasi=$("#batas_batal_mutasi").val();
		var batas_batal_pengajuan=$("#batas_batal_pengajuan").val();
		$.ajax({
			url: '{site_url}msetting_jurnal_kas_lain/update',
			type: 'POST',
			data: {
				st_auto_posting_mutasi:st_auto_posting_mutasi,batas_batal_mutasi:batas_batal_mutasi,
				st_auto_posting_pengajuan:st_auto_posting_pengajuan,batas_batal_pengajuan:batas_batal_pengajuan,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function validate_update()
	{
		
		if ($("#st_auto_posting_mutasi").val()=='#'){
			sweetAlert("Maaf...", "Setting Harus diisi", "error");
			return false;
		}
		if ($("#batas_batal_mutasi").val()==''){
			sweetAlert("Maaf...", "Batas Waktu Batal Mutasi Harus diisi", "error");
			return false;
		}
		
		if ($("#st_auto_posting_pengajuan").val()=='#'){
			sweetAlert("Maaf...", "Setting Pengajuan Harus diisi", "error");
			return false;
		}
		if ($("#batas_batal_pengajuan").val()==''){
			sweetAlert("Maaf...", "Batas Waktu Pengajuan Batal Harus diisi", "error");
			return false;
		}
		
		
		return true;
	}
	
	
</script>
