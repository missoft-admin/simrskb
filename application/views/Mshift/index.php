<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('2449'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_shift()"><i class="fa fa-bar-chart-o"></i> {title}</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<input type="hidden" id="id" value="">
		<?php if (UserAccesForm($user_acces_form,array('2449'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?>" id="tab_info">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
					
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_shift">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="50%">Nama Shift</th>
											<th width="30%">Range Waktu</th>
											<th width="15%">Action</th>										   
										</tr>
										<tr>
											<th>#</th>
											
											<th>
												<select id="shift_id" name="shift_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="" selected>Pilih Opsi</option>
													<?foreach(list_variable_ref(295) as $row){?>
													<option value="<?=$row->id?>"><?=$row->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<div class="input-group">
													<input  type="text" class="time-datepicker form-control " id="waktu_1" value="" required>
													<span class="input-group-addon"><i class="si si-clock"></i></span>
													<input  type="text" class="time-datepicker form-control " id="waktu_2" value="" required>
												</div>
											</th>
											
											<th>
												<div class="btn-group">
												<?php if (UserAccesForm($user_acces_form,array('2450'))){ ?>
												<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_shift" name="btn_tambah_shift"><i class="fa fa-save"></i> Simpan</button>
												<?}?>
												<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_shift()"><i class="fa fa-refresh"></i> Clear</button>
												</div>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			
			
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	load_shift();		
	
})	

function check_save_dokter(idshift,iddokter,pilih){
	// alert('id : '+id+' Pilih :'+pilih);
	$("#cover-spin").show();
		
	 $.ajax({
			url: '{site_url}mshift/check_save_dokter',
			type: 'POST',
			data: {idshift: idshift,iddokter:iddokter,pilih:pilih},
			complete: function() {
				$("#cover-spin").hide();
				$('#index_dokter').DataTable().ajax.reload( null, false ); 
				$('#index_shift').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Update'});
				
			}
	});
}
function edit_shift(id){
	$("#id").val(id);
	$("#cover-spin").show();
	$.ajax({
			url: '{site_url}mshift/find_shift/'+id,
			dataType: "json",
			success: function(data) {
				$("#cover-spin").hide();
				$("#shift_id").val(data.shift_id).trigger('change');
				$("#waktu_1").val(data.waktu_1);
				$("#waktu_2").val(data.waktu_2);
			}
		});
}
$("#shift_id").change(function(){
		

});
function clear_shift(){
	$("#id").val('');
	$("#shift_id").val('').trigger('change');
	$("#waktu_1").val('');
	$("#waktu_2").val('');
	
	$("#btn_tambah_shift").html('<i class="fa fa-save"></i> Save');
	
}
function load_shift(){
	$('#index_shift').DataTable().destroy();	
	table = $('#index_shift').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mshift/load_shift', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}

$("#btn_tambah_shift").click(function() {
	let id=$("#id").val();
	let waktu_1=$("#waktu_1").val();
	let waktu_2=$("#waktu_2").val();
	let shift_id=$("#shift_id").val();
	
	if (shift_id==''){
		sweetAlert("Maaf...", "Shift", "error");
		return false;
	}
	if (waktu_1==''){
		sweetAlert("Maaf...", "Tentukan Waktu Range", "error");
		return false;
	}
	if (waktu_2==''){
		sweetAlert("Maaf...", "Tentukan Waktu Range", "error");
		return false;
	}
	
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mshift/simpan_shift', 
		dataType: "JSON",
		method: "POST",
		data : {
				id:id,
				shift_id:shift_id,
				waktu_1:waktu_1,
				waktu_2:waktu_2,
				
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==false){
				sweetAlert("Maaf...", "Data Duplicate!", "error");
			}else{
				clear_shift();
				$('#index_shift').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}
			
		}
	});

});

function hapus_shift(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Poliklinik?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mshift/hapus_shift',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_shift').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

</script>