<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Print Kontrabon Rekap</title>
    <style>
	@page {
			margin-top: 140px;
			margin-left:50px;
			margin-right:50px;
			margin-bottom:100px;
            }
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }
	  
      th {
        padding: 5px;
      }
	  td {
		 
        padding: 5px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }
      
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;
		 
      }
	  td {
        padding: 3px;
		 
      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
		  padding-left:5px;
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
		
		header {
			position: fixed;
			top: -110px;
			left: 0px;
			right: 0px;
			height: 0px;

			
		}

		footer {
			position: fixed; 
			bottom: -260px; 
			left: 0px; 
			right: 0px;
			height: 50px; 
		}
		/*main {
			page-break-inside: auto;
		}
		*/
    }
	
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
	<header>
		<table class="content">
		  <tr>
			<td width="20%" class="text-left"><img src="assets/upload/logo/logomenu.jpg" alt="" ></td>
			<td width="30%" class="text-bold text-judul text-center text-top" style="height:50px">REKAPITULASI TAGIHAN RAWAT JALAN <br><br><?=strtoupper($namakontraktor);?><br><?=$nosurat;?>(<?=(date('d-m-Y'))?>)</td>	
			<td width="20%" class="text-bold text-judul text-center text-top" style="height:50px"></td>	
		  </tr>
		</table>
	</header>
	<footer>
		<p style="position:fixed; bottom:40px; font-size: 18px; color: #3abad8;">
        JL. L.L.RE Martadinata No. 28 40115 Jawa Barat, Indonesia<br>T. (022) 4206717 F. (022) 4216436 E. rskb.halmahera@gmail.com ; www.halmaherasiaga.com
      </p>
	</footer>
	<main>		
	<table id="customers">
		<thead>
		  <tr>
			<th width="5%" class="border-full text-right"><strong>NO</strong></th>
			<th width="10%" class="border-full text-center"><strong>TANGGAL BEROBAT</strong></th>
			<th width="10%" class="border-full text-center"><strong>NO. REGISTER</strong></th>
			<th width="10%" class="border-full text-center"><strong>NO. REKAM MEDIS</strong></th>
			<th width="15%" class="border-full text-center"><strong>NAMA PASIEN</strong></th>
			<th width="15%" class="border-full text-center"><strong>DOKTER</strong></th>
			<th width="10%" class="border-full text-center"><strong>ADMINISTRASI</strong></th>
			<th width="10%" class="border-full text-center"><strong>TINDAKAN RJ</strong></th>
			<th width="10%" class="border-full text-center"><strong>LABORATORIUM</strong></th>
			<th width="10%" class="border-full text-center"><strong>RADIOLOGI</strong></th>
			<th width="10%" class="border-full text-center"><strong>FISIOTERAPI</strong></th>
			<th width="10%" class="border-full text-center"><strong>POLIKLINIK ALKES</strong></th>
			<th width="10%" class="border-full text-center"><strong>POLIKLINIK OBAT</strong></th>
			<th width="10%" class="border-full text-center"><strong>FARMASI ALKES</strong></th>
			<th width="10%" class="border-full text-center"><strong>FARMASI OBAT</strong></th>
			<th width="10%" class="border-full text-center"><strong>FARMASI IMPLAN</strong></th>
			<th width="10%" class="border-full text-center"><strong>TOTAL</strong></th>
			<th width="10%" class="border-full text-center"><strong>DIBAYAR PASIEN</strong></th>
			<th width="10%" class="border-full text-center"><strong>DIBAYAR ASURANSI</strong></th>
		  </tr>
		</thead>
		<tbody>
    <?
	$number=0;
	$tot_adm=0;
	$tot_tindakan=0;
	$tot_lab=0;
	$tot_rad=0;
	$tot_fis=0;
	$tot_p_alkes=0;
	$tot_p_obat=0;
	$tot_f_alkes=0;
	$tot_f_obat=0;
	$tot_total=0;
	$tot_pasien=0;
	$tot_asuransi=0;
	
	?>
      <?php foreach ($list_detail as $row){ ?>
        <?php 
		$number = $number + 1; 
		$idpendaftaran=$row->idpendaftaran;
		$kasir_id=$row->kasir_id;
		
		 $administrasi = $this->db->query("SELECT
        	SUM( totalkeseluruhan ) AS totalkeseluruhan
        FROM
        	tpoliklinik_administrasi
        WHERE idpendaftaran = '".$idpendaftaran."' AND status <> 0")->row('totalkeseluruhan');
		
		$tot_adm=$tot_adm + $administrasi;
		
		$q="SELECT SUM(H.totalkeseluruhan) as total
			FROM tkasir K
			LEFT JOIN tpoliklinik_pelayanan H ON H.idtindakan=K.idtindakan
			WHERE K.id='$kasir_id' AND K.idtipe IN(1,2) AND K.`status` <> 0 AND H.`status` <> 0";
			$tindakan=$this->db->query($q)->row('total');
		$tot_tindakan=$tot_tindakan + $tindakan;
		
		 $lab = $this->db->query("SELECT
        	SUM(trujukan_laboratorium_detail.totalkeseluruhan) AS totalkeseluruhan
        FROM
        	tpoliklinik_pendaftaran
        JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
        JOIN trujukan_laboratorium ON tpoliklinik_tindakan.id = trujukan_laboratorium.idtindakan AND trujukan_laboratorium.asalrujukan IN ( 1, 2 )
        JOIN trujukan_laboratorium_detail ON trujukan_laboratorium_detail.idrujukan = trujukan_laboratorium.id
        JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
        WHERE tpoliklinik_pendaftaran.id = '".$idpendaftaran."' AND trujukan_laboratorium.status <> 0")->row('totalkeseluruhan');
		
		
		$tot_lab=$tot_lab + $lab;
		
		$rad = $this->db->query("SELECT SUM(trujukan_radiologi_detail.totalkeseluruhan) as totalkeseluruhan
        FROM
        	tpoliklinik_pendaftaran
        JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
        JOIN trujukan_radiologi ON tpoliklinik_tindakan.id = trujukan_radiologi.idtindakan AND trujukan_radiologi.asalrujukan IN ( 1, 2 )
        JOIN trujukan_radiologi_detail ON trujukan_radiologi_detail.idrujukan = trujukan_radiologi.id
        JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
        WHERE tpoliklinik_pendaftaran.id = '".$idpendaftaran."' AND trujukan_radiologi.status <> 0")->row('totalkeseluruhan');

		$tot_rad=$tot_rad + $rad;
		
		$fis = $this->db->query("SELECT
        	SUM(trujukan_fisioterapi_detail.totalkeseluruhan) AS totalkeseluruhan
        FROM
        	tpoliklinik_pendaftaran
        JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.idpendaftaran = tpoliklinik_pendaftaran.id
        JOIN trujukan_fisioterapi ON tpoliklinik_tindakan.id = trujukan_fisioterapi.idtindakan AND trujukan_fisioterapi.asalrujukan IN ( 1, 2 )
        JOIN trujukan_fisioterapi_detail ON trujukan_fisioterapi_detail.idrujukan = trujukan_fisioterapi.id
        JOIN tkasir ON tkasir.idtindakan = tpoliklinik_tindakan.id AND tkasir.idtipe IN ( 1, 2 )
        WHERE tpoliklinik_pendaftaran.id = '".$idpendaftaran."' AND trujukan_fisioterapi.status <> 0")->row('totalkeseluruhan');
		
		$tot_fis=$tot_fis + $fis;
		
		$p_obat = $this->db->query("SELECT
        	SUM(tpoliklinik_obat.totalkeseluruhan) AS totalkeseluruhan
        FROM
        	tpoliklinik_obat
        LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpoliklinik_obat.idtindakan
        LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
        WHERE
        	tpoliklinik_pendaftaran.id = '".$idpendaftaran."' AND tpoliklinik_obat.status = 1")->row('totalkeseluruhan');
		$p_alkes = $this->db->query("SELECT
        	SUM(tpoliklinik_alkes.totalkeseluruhan) AS totalkeseluruhan
        FROM
        	tpoliklinik_alkes
        LEFT JOIN tpoliklinik_tindakan ON tpoliklinik_tindakan.id = tpoliklinik_alkes.idtindakan
        LEFT JOIN tpoliklinik_pendaftaran ON tpoliklinik_pendaftaran.id = tpoliklinik_tindakan.idpendaftaran
        WHERE
        	tpoliklinik_pendaftaran.id = '".$idpendaftaran."' AND tpoliklinik_alkes.status = 1")->row('totalkeseluruhan');
			
		$tot_p_alkes=$tot_p_alkes + $p_alkes;		
		$tot_p_obat=$tot_p_obat + $p_obat;
		
		$q="SELECT 
			SUM(CASE WHEN D.idtipe='1' THEN D.totalharga ELSE 0 END) as tot_alkes  
			,SUM(CASE WHEN D.idtipe='3' THEN D.totalharga ELSE 0 END) as tot_obat  
			,SUM(CASE WHEN D.idtipe='2' THEN D.totalharga ELSE 0 END) as tot_implan  

			FROM tkasir K
			LEFT JOIN tpasien_penjualan H ON H.idtindakan=K.idtindakan
			LEFT JOIN (
				SELECT H.idpenjualan,H.idtipe,H.idbarang,H.totalharga,H.`status` FROM tpasien_penjualan_nonracikan H 
					UNION ALL
					SELECT H.idpenjualan,D.idtipe,D.idbarang,D.totalharga,H.`status` FROM `tpasien_penjualan_racikan` H
					INNER JOIN tpasien_penjualan_racikan_obat D ON D.idracikan=H.id
			) D ON D.idpenjualan=H.id AND D.`status` <> 0

			WHERE K.id='$kasir_id' AND K.idtipe IN(1,2) AND K.`status` <> 0 AND H.`status` <> 0
			";
		$farmasi=$this->db->query($q)->row();
		if ($farmasi){
			$f_alkes= $farmasi->tot_alkes;
			$f_obat= $farmasi->tot_obat;
			$f_implan= $farmasi->tot_implan;
		}else{
			$f_alkes= 0;
			$f_obat= 0;
			$f_implan= 0;
		
		}
		
		$tot_f_alkes=$tot_f_alkes + $f_alkes;
		$tot_f_obat=$tot_f_obat + $f_obat;
		$tot_f_implan=$tot_f_implan + $f_implan;
		?>
        <tr>
          <td class="border-full text-right"> <?=$number?>&nbsp;&nbsp;</td>
		  <td class="border-full text-center"><?=HumanDateShort($row->tanggaldaftar)?></td>
		  <td class="border-full text-center"><?=strtoupper($row->nopendaftaran)?></td>
		  <td class="border-full text-center"><?=strtoupper($row->no_medrec)?></td>
		  <td class="border-full text-left"><?=($row->namapasien)?></td>
		  <td class="border-full text-left"><?=($row->namadokter)?></td>
		  <td class="border-full text-right"><?php echo number_format($administrasi,0)?>&nbsp;</td>
		  <td class="border-full text-right"><?php echo number_format($tindakan,0)?>&nbsp;</td>
		  <td class="border-full text-right"><?php echo number_format($lab,0)?>&nbsp;</td>
		  <td class="border-full text-right"><?php echo number_format($rad,0)?>&nbsp;</td>
		  <td class="border-full text-right"><?php echo number_format($fis,0)?>&nbsp;</td>
		  <td class="border-full text-right"><?php echo number_format($p_alkes,0)?>&nbsp;</td>
		  <td class="border-full text-right"><?php echo number_format($p_obat,0)?>&nbsp;</td>
		  <td class="border-full text-right"><?php echo number_format($f_alkes,0)?>&nbsp;</td>
		  <td class="border-full text-right"><?php echo number_format($f_obat,0)?>&nbsp;</td>
		  <td class="border-full text-right"><?php echo number_format($f_implan,0)?>&nbsp;</td>
		  <?
			$bayar_pasien=$row->total - $row->nominal;
			$tot_total=$tot_total + $row->total;
			$tot_pasien=$tot_pasien + $bayar_pasien;
			$tot_asuransi=$tot_asuransi + $row->nominal;
			
		  ?>
		  
		  <td class="border-full text-right"><?php echo number_format($row->total,0)?>&nbsp;</td>
		  <td class="border-full text-right"><?php echo number_format($bayar_pasien,0)?>&nbsp;</td>
		  <td class="border-full text-right"><?php echo number_format($row->nominal,0)?>&nbsp;</td>
        </tr>
      <? 
		
	  } ?>
		<tr>
			<td colspan="6"  class="border-full text-center text-bold" style="height:30px">TOTAL</td>
			<td  class="border-full text-right text-bold"><?=number_format($tot_adm,0)?>&nbsp; </td>
			<td  class="border-full text-right text-bold"><?=number_format($tot_tindakan,0)?>&nbsp; </td>
			<td  class="border-full text-right text-bold"><?=number_format($tot_lab,0)?>&nbsp; </td>
			<td  class="border-full text-right text-bold"><?=number_format($tot_rad,0)?>&nbsp; </td>
			<td  class="border-full text-right text-bold"><?=number_format($tot_fis,0)?>&nbsp; </td>
			<td  class="border-full text-right text-bold"><?=number_format($tot_p_alkes,0)?>&nbsp; </td>
			<td  class="border-full text-right text-bold"><?=number_format($tot_p_obat,0)?>&nbsp; </td>
			<td  class="border-full text-right text-bold"><?=number_format($tot_f_alkes,0)?>&nbsp; </td>
			<td  class="border-full text-right text-bold"><?=number_format($tot_f_obat,0)?>&nbsp; </td>
			<td  class="border-full text-right text-bold"><?=number_format($tot_f_implan,0)?>&nbsp; </td>
			<td  class="border-full text-right text-bold"><?=number_format($tot_total,0)?>&nbsp; </td>
			<td  class="border-full text-right text-bold"><?=number_format($tot_pasien,0)?>&nbsp; </td>
			<td  class="border-full text-right text-bold"><?=number_format($tot_asuransi,0)?>&nbsp; </td>			 
		</tr>
		</tbody>
    </table>
	<br>
	<table id="customers" >
		 <tr>
			<td>Tanggal cetak <?=(date('d-m-Y H:i:s'))?> <?=$this->session->userdata('user_name')?></td>
			
        </tr>
		
	</table>	
	
	</main>
	
	<br>
    
  </body>
</html>
