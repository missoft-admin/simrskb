<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work-1"'); ?>
                <div class="col-md-6"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Nomor Tagihan</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{no_klaim}" disabled>
                            <input class="form-control" type="hidden" name="klaim_id" id="klaim_id" value="{id}" >
							<input class="form-control" type="hidden" name="idkelompokpasien" id="idkelompokpasien" value="{idkelompokpasien}" >
                            <input class="form-control" type="hidden" name="idrekanan" id="idrekanan" value="{idrekanan}" >
                            <input class="form-control" type="hidden" name="disabel" id="disabel" value="{disabel}" >
                        </div> 
                    </div>
                    <div class="form-group" style="margin-bottom: 15px;"> 
                        <label class="col-md-3 control-label">Nama Rekanan</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{rekanan_nama}" disabled>
                        </div> 
                    </div>
                                      
                </div>
				<div class="col-md-6" style="margin-bottom: 5px;"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Tanggal Penagihan</label> 
                        <div class="col-md-4"> 
                            <input class="form-control" value="<?=HumanDateShort($tanggal_tagihan)?>" disabled>
                        </div> 
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Tanggal Jatuh Tempo</label> 
                        <div class="col-md-4"> 
                            <input class="form-control" value="<?=HumanDateShort($jatuh_tempo_bayar)?>" disabled>
                        </div> 
                    </div>
                    
                    
                </div>
            <?php echo form_close(); ?>
        </div>
        
        <div class="form-group" style="margin-top: 20px;"></div>
    </div>
</div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">DETAIL TAGIHAN</h3>
    </div>
    <div class="block-content">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Registrasi</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_reg" placeholder="No Registrasi" name="no_reg" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Medrec</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_medrec" placeholder="No. Medrec" name="no_medrec" value="">
                    </div>
                </div>
				
                
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="nama_pasien" placeholder="Nama Pasien" name="nama_pasien" value="">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Daftar</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tgl_trx" name="tgl_trx" placeholder="From" value=""/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tgl_trx2" name="tgl_trx2" placeholder="To" value=""/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
			</div>
            <?php echo form_close(); ?>
        </div>
        <div class="form-group" style="margin-bottom: 30px;"></div>
		<div class="table-responsive">
        <table class="table table-bordered table-striped table-responsive" id="index_list">
            <thead>
                <tr>
                    <th>#</th>
                    <th>#</th>
                    <th>#</th>
                    <th>TANGGAL</th>
                    <th>NO. REG</th>
                    <th>MEDREC</th>
                    <th>PASIEN</th>
                    <th>POLI</th>
                    <th>DOKTER</th>
                    <th>TOTAL</th>
                    <th>ASURANSI</th>
                    <th>OLEH PASIEN</th>
                    <th>AKSI</th>
                </tr>
            </thead>
            <tbody> </tbody>
              
        </table>
    </div>
        <div class="form-group" style="margin-top: 20px;"></div>
    </div>
</div>
<div class="modal fade" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Pengalihan Jatuh Tempo </h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">
							<label class="col-md-4 control-label" for="status">Tanggal Asal</label>
							<div class="col-md-8">
							<div class="input-group">
								<input class="form-control" readonly type="text" id="tgl_asal" name="tgl_asal" value="<?=HumanDateShort($tanggal_tagihan)?>">
								<input class="form-control" readonly type="hidden" id="xst_multiple" name="xst_multiple">
								<input class="form-control" readonly type="hidden" id="xidpasien" name="xidpasien" value="{idpasien}">
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
							</div>
						</div> 	
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Kelompok Pasien</label>
								<div class="col-md-8">
									<select id="xidkelompokpasien" name="xidkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach($list_kelompok_pasien as $row){?>
											<option value="<?=$row->id?>" <?=($idkelompokpasien==$row->id ? 'selected':'')?>><?=$row->nama?></option>									
										<?}?>
										
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Nama Rekanan</label>
								<div class="col-md-8">
									<select id="xidrekanan" name="xidrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#">- Pilih Rekanan -</option>
										<?foreach($list_rekanan as $row){?>
											<option value="<?=$row->id?>" <?=($idrekanan==$row->id ? 'selected':'')?>><?=$row->nama?></option>									
										<?}?>
										
									</select>
								</div>
							</div>				
						</div>		
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Tanggal</label>
								<div class="col-md-8">
									<select id="xtgl_pilih" name="xtgl_pilih" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										
									</select>
								</div>
							</div>				
						</div>				
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="xid" placeholder="No. PO" name="xid" value="">
								<input type="hidden" class="form-control" id="xtanggal_tagihan" placeholder="No. PO" name="xtanggal_tagihan" value="<?=HumanDateShort($tanggal_tagihan)?>">
								<input type="hidden" class="form-control" id="xtipe" placeholder="No. PO" name="xtipe" value="{tipe}">
								<input type="hidden" class="form-control" id="xload" placeholder="No. PO" name="xload" value="">
								<input type="hidden" class="form-control" id="xjml" placeholder="No. PO" name="xjml" value="1">
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_ubah" id="btn_ubah"><i class="fa fa-save"></i> SIMPAN</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">


    var table;

    $(document).ready(function(){
			// alert($("#xidkelompokpasien").val());
		if ($("#xidkelompokpasien").val()=='1'){
			$('#div_rekanan').show();
		}else{
			$('#div_rekanan').hide();
		}
		
		load_rajal();
	
    });
	$(document).on("change", "#xidkelompokpasien", function() {
		// load_list_tanggal();
		var idkelompokpasien=$("#xidkelompokpasien").val();
		$.ajax({
			url: '{site_url}tklaim_rincian/get_st_multiple',
			dataType: "json",
			type: 'POST',
			data: {idkelompokpasien: idkelompokpasien},
			success: function(data) {
				$("#xst_multiple").val(data.st_multiple);
				// alert('Load Tanggal');
				load_list_tanggal();
			}
		});
		if ($(this).val()=='1'){
			$("#xidrekanan").val("#").trigger('change');
			$('#div_rekanan').show();
		}else{
			$('#div_rekanan').hide();
		}
		
	});
    function load_rajal() {
		var klaim_id=$("#klaim_id").val();
		var no_reg=$("#no_reg").val();
		var no_medrec=$("#no_medrec").val();
		var nama_pasien=$("#nama_pasien").val();
		var tgl_trx=$("#tgl_trx").val();
		var tgl_trx2=$("#tgl_trx2").val();
		var disabel=$("#disabel").val();
		// alert(tgl_trx);
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 100,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tklaim_rincian/load_rajal/',
			type: "POST",
			dataType: 'json',
			data: {
				klaim_id: klaim_id,no_reg: no_reg,
				no_medrec: no_medrec,nama_pasien: nama_pasien,
				tgl_trx:tgl_trx,tgl_trx2:tgl_trx2,disabel:disabel,
			}
		},
		columnDefs: [
					{"targets": [0,1], "visible": false },
					 {  className: "text-right", targets:[9,10,11] },
					 {  className: "text-center", targets:[3,4,5] },
					 {  className: "text-left", targets:[6] },
					 { "width": "3%", "targets": [2] },
					 { "width": "5%", "targets": [4] },
					 { "width": "8%", "targets": [1,10,11] },
					 { "width": "10%", "targets": [12,6] },
					 // { "width": "15%", "targets": [8] }

					]
		});
	}
	
	$("#btn_filter").click(function() {
		load_rajal();
	});
	$(document).on("click", ".ganti", function() {
		var table = $('#index_list').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()
		$("#xid").val(id);
		
		$("#xidkelompokpasien").val($("#idkelompokpasien").val()).trigger('change');
		$("#xidrekanan").val($("#idrekanan").val()).trigger('change');
		load_list_tanggal();
		$('#modal_edit').modal('show');
		
	});
	function load_list_tanggal(){
		var st_multiple=$("#xst_multiple").val();
		var idpasien=$("#xidpasien").val();
		var idkelompokpasien=$("#xidkelompokpasien").val();
		var idrekanan=$("#xidrekanan").val();
		var tipe=$("#xtipe").val();
		if (idkelompokpasien && idrekanan && tipe){
		// alert('KELO :'+idkelompokpasien+' Tipe: '+tipe+' REKA :'+idrekanan);
			// $.ajax({
				// url: '{site_url}tklaim_rincian/list_tanggal/'+idkelompokpasien+'/'+idrekanan+'/'+tipe,
				// dataType: "json",
					// type: 'POST',
				// data: {idkelompokpasien: idkelompokpasien,idrekanan:idrekanan,tipe:tipe,st_multiple:st_multiple,idpasien:idpasien},
				// dataType: "json",
				// success: function(data) {
					// $("#xtgl_pilih").empty();
					// $('#xtgl_pilih').append(data.detail);
				// }
			// });
			$.ajax({
				url: '{site_url}tklaim_rincian/list_tanggal',
				dataType: "json",
				type: 'POST',
				data: {idkelompokpasien: idkelompokpasien,idrekanan:idrekanan,tipe:tipe,st_multiple:st_multiple,idpasien:idpasien},
				success: function(data) {
					$("#xtgl_pilih").empty();
					$('#xtgl_pilih').append(data.detail);
				}
			});
		}
	}
	$(document).on("change", "#xidkelompokpasien", function() {
		load_list_tanggal();
		if ($(this).val()=='1'){
			$("#xidrekanan").val("#").trigger('change');
			$('#div_rekanan').show();
		}else{
			$('#div_rekanan').hide();
		}
		
	});
	$(document).on("change", "#xidrekanan", function() {
		load_list_tanggal();
		
	});
	$(document).on("click", "#btn_ubah", function() {
		if ($("#xtgl_pilih").val()=='' || $("#xtgl_pilih").val()==null){
			sweetAlert("Maaf...", "Tanggal belum dipilih!", "error");
			return false;
		}
		// alert();
		if ($("#xidkelompokpasien").val()=='1' && $("#xidrekanan").val()=='#'){
			sweetAlert("Maaf...", "Rekanan Harus diisi!", "error");
			return false;
		}
		
		swal({
			title: "Anda Yakin ?",
			text : "Akan Memindahkan "+$("#xjml").val()+" Faktur ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			edit_tanggal_satu();
		});
		
	});
	function edit_tanggal_satu(){
		var klaim_id=$("#xid").val();		
		var tanggal_tagihan=$("#xtgl_pilih").val();		
		var idkelompokpasien=$("#xidkelompokpasien").val();		
		var idrekanan=$("#xidrekanan").val();		
		var tipe=$("#xtipe").val();		
		var st_multiple=$("#xst_multiple").val();
		var idpasien=$("#xidpasien").val();	
		// alert(klaim_id+' tg l'+tanggal_tagihan+' Kel '+idkelompokpasien+' rekan '+idrekanan+' tipe '+tipe);
		// return false;
		var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}tklaim_rincian/edit_tanggal_satu',
			type: 'POST',
			// data: {klaim_id: klaim_id,tanggal_tagihan:tanggal_tagihan,idkelompokpasien:idkelompokpasien,idrekanan:idrekanan,tipe:tipe},
			data: {klaim_id: klaim_id,tanggal_tagihan:tanggal_tagihan,idkelompokpasien:idkelompokpasien,idrekanan:idrekanan,tipe:tipe,st_multiple:st_multiple,idpasien:idpasien},
			complete: function() {

				$.toaster({priority : 'success', title : 'Succes!', message : ' Memindahkan Tanggal Jatuh Tempo'});
				// location.reload();
				// filter_form();
				$("#modal_edit").modal('hide');
				$("#cover-spin").hide();
				load_rajal();
			}
		});
	}
</script>