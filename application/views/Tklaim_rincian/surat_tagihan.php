<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Surat Tagihan</title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 24px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
      .border-dobble{
        border-bottom-style: double;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
        font-size: 24px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
        font-size: 14px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
      .border-dobble{
        border-bottom-style: double;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <div style="margin-left:5%;margin-right:12%;">
      <table>
        <tr>
          <td colspan="2">
            <img style="width:300px;" src="assets/upload/logo/logomenu.jpg">
          </td>
        </tr>
        <tr>
          <td colspan="2" class="text-right">Bandung, <?=date("d").' '.MONTHFormat(date("m")).' '.date("Y");?></td>
        </tr>
        <tr>
          <td style="width:200px">Nomor Tagihan</td>
          <td>: <?=$nosurat;?></td>
        </tr>
       
        <tr>
          <td>Hal</td>
          <td>: Tagihan <?=($tipe=='1'?'rawat jalan':'rawat inap')?></td>
        </tr>
        <tr>
          <td>Lampiran</td>
          <td>: 1 (Satu) Berkas</td>
        </tr>
        <tr>
          <td colspan="2" class="text-bold">
            Kepada Yth,<br>
            Bagian Klaim<br>
            <?=strtoupper($namakontraktor);?><br>
            Di Tempat
          </td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2" style="text-align:justify">Dengan hormat,<br>Bersama ini kami kirimkan kwitansi dan nota perincian tagihan 
		  <?if ($idpasien){?>
		  <?=($tipe=='1'?'rawat jalan':'rawat inap')?> a.n Pasien <b><?=$namapasien?></b>,<b> Rp <?=number_format($tagihanrekanan);?>,- (<?=ucwords(numbers_to_words($tagihanrekanan))?> Rupiah)</b>.
		  <?}else{?>
		  <?=($tipe=='1'?'rawat jalan':'rawat inap')?>,<b> Rp <?=number_format($tagihanrekanan);?>,- (<?=ucwords(numbers_to_words($tagihanrekanan))?> Rupiah)</b>.
		  
		  <?}?>
		  <br><br>Adapun biaya tersebut dapat dipindah bukukan ke <b>Bank Mandiri Cabang Buah Batu Bandung Dengan Nomor Rekening 130-00-44206061 atas nama RS Halmahera Siaga</b> dan 
		  <strong> bukti transfer bank </strong> dimohon unutuk dikirimkan ke Rumah Sakit Khusus Bedah Halmahera  Siaga melalui email dengan alamat 
		  <b><u>admranap.halmahera@gmail.com</u></u> / <u>penagihan.halmahera@gmail.com</u></b>.
		  <br><br>Atas perhatian dan kerjasama yang baik, Kami ucapkan terimakasih.</td>
        </tr>
      </table>

      <br>

      <table>
        <tr>
          <td style="width:70%">&nbsp;</td>
          <td><b>Hormat Kami<br>RSKB Halmahera Siaga</b></td>
        </tr>
        <tr>
          <td style="width:70%">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td style="width:70%">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td style="width:70%">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td style="width:70%">&nbsp;</td>
          <td><strong><u>(<?= $userinput ?>)</u><br>Bagian Penagihan</strong></td>
        </tr>
      </table>

      <p style="position:fixed; bottom:40px; font-size: 18px; color: #3abad8;">
        JL. L.L.RE Martadinata No. 28 40115 Jawa Barat, Indonesia<br>T. (022) 4206717 F. (022) 4216436 E. rskb.halmahera@gmail.com ; www.halmaherasiaga.com
      </p>
    </div>
  </body>
</html>
