<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>

<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Tagihan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_klaim" placeholder="No Tagihan" name="no_klaim" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Kelompok Pasien</label>
                    <div class="col-md-8">
                        <select id="idkelompokpasien" name="idkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Kelompok Pasien -</option>
							<?foreach($list_kelompok_pasien as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Rekanan</label>
                    <div class="col-md-8">
                        <select id="idrekanan" name="idrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Perusahaan -</option>
							<?foreach($list_rekanan as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Status</label>
                    <div class="col-md-8">
						<select id="status_kirim" name="status_kirim" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua Status -</option>
							<option value="0">Belum Dikirim</option>
							<option value="1">Sudah Dikirim</option>							
						</select>
					</div>
                </div>
				          
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tipe</label>
                    <div class="col-md-8">
						<select id="tipe" name="tipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Tipe -</option>
							<option value="1" <?=("1" == $tipe ? 'selected' : ''); ?>>Rawat Jalan</option>
							<option value="2" <?=("2" == $tipe ? 'selected' : ''); ?>>Ranap / ODS</option>
							
						</select>
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Jumlah Hari</label>
                    <div class="col-md-8">
						<select id="status_pengiriman" name="tipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Pengiriman -</option>
							<option value="1">Tepat Waktu</option>
							<option value="2">Terlambat</option>
							<option value="3">Lebih Cepat</option>
							
						</select>
					</div>
                </div>
				
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Tagihan</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
                            <input class="form-control" type="text" id="tanggal_tagihan" name="tanggal_tagihan" placeholder="From" value="{tanggal_tagihan}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_tagihan2" name="tanggal_tagihan2" placeholder="To" value="{tanggal_tagihan2}"/>
                        </div>
                    </div>
                </div>
               
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-4" hidden>
                        <button class="btn btn-danger text-uppercase" type="button" name="btn_verif_filter" id="btn_verif_filter" style="font-size:13px;width:100%;"><i class="fa fa-check-square-o"></i> Verifikasi By Filter</button>
                    </div>
					<div class="col-md-8" hidden>
                        <button disabled class="btn btn-primary text-uppercase" type="button" name="btn_verif_page" id="btn_verif_page" style="font-size:13px;width:100%;"><i class="fa fa-check"></i> Verifikasi Per page</button>
                    </div>
                </div>
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th hidden width="5%">TIPE</th>
                    <th hidden width="2%">ID</th>
                    <th width="10%">NO</th>
                    <th width="10%">NO TAGIHAN</th>
                    <th width="10%"> NAMA REKANAN</th>
                    <th width="10%">JENIS LAYANAN</th>
                    <th width="10%">JML FAKTUR</th>
                    <th width="10%">TOT TAGIHAN</th>
                    <th width="10%">JATUH TEMPO PENAGIHAN</th>
                    <th width="15%">STATUS</th>
                    <th width="15%">STATUS PENGIRIMAN</th>
                    <th width="15%">JATUH TEMPO PEMBAYARAN</th>
                    <th width="15%">TGL KIRIM</th>
                    <th width="15%">NO RESI</th>
                    <th width="15%" class="text-center">AKSI</th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
</div>
<div class="modal fade" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Pengalihan Jatuh Tempo </h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">
							<label class="col-md-4 control-label" for="status">Tanggal Asal</label>
							<div class="col-md-8">
							<div class="input-group">
								<input class="form-control" readonly type="text" id="tgl_asal" name="tgl_asal">
								<input class="form-control" readonly type="hidden" id="xst_multiple" name="xst_multiple">
								<input class="form-control" readonly type="hidden" id="xidpasien" name="xidpasien">

								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
							</div>
						</div> 	
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Kelompok Pasien</label>
								<div class="col-md-8">
									<select id="xidkelompokpasien" disabled name="xidkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach($list_kelompok_pasien as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>									
										<?}?>
										
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Nama Rekanan</label>
								<div class="col-md-8">
									<select id="xidrekanan" name="xidrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#">- Pilih Rekanan -</option>
										<?foreach($list_rekanan as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>									
										<?}?>
										
									</select>
								</div>
							</div>				
						</div>		
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Tanggal</label>
								<div class="col-md-8">
									<select id="xtgl_pilih" name="xtgl_pilih" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										
									</select>
								</div>
							</div>				
						</div>				
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="xid" placeholder="No. PO" name="xid" value="">
								<input type="hidden" class="form-control" id="xtanggal_tagihan" placeholder="No. PO" name="xtanggal_tagihan" value="">
								<input type="hidden" class="form-control" id="xtipe" placeholder="No. PO" name="xtipe" value="">
								<input type="hidden" class="form-control" id="xload" placeholder="No. PO" name="xload" value="">
								<input type="hidden" class="form-control" id="xjml" placeholder="No. PO" name="xjml" value="">
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_ubah" id="btn_ubah"><i class="fa fa-save"></i> SIMPAN</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_kirim" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary">
                    <h3 class="block-title">PENGIRIMAN BERKAS TAGIHAN</h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">
							<label class="col-md-4 control-label" for="status">Nomor Tagihan</label>
							<div class="col-md-8">
								<input class="form-control" readonly type="text" id="no_klaim2" name="no_klaim2">
							</div>
						</div> 
						<div class="col-md-12" style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">Nama Rekanan</label>
							<div class="col-md-8">
								<input class="form-control" readonly type="text" id="nama_rekanan" name="nama_rekanan">
							</div>
						</div>
						<div class="col-md-12" style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">Jenis Layanan</label>
							<div class="col-md-8">
								<input class="form-control" readonly type="text" id="jenis_layanan" name="jenis_layanan">
							</div>
						</div>
						<div class="col-md-12" style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">Tanggal Kirim</label>
							<div class="col-md-8">
								<div class="js-datepicker input-group date" data-date-format="dd-mm-yyyy">
									<input class="form-control" type="text" id="tanggal_kirim" name="tanggal_kirim" placeholder="Tanggal Kirim" value="<?=date('d-m-Y')?>"/>
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
						</div>
						<div class="col-md-12" style="margin-top: 5px;">
							<label class="col-md-4 control-label" for="status">No Resi</label>
							<div class="col-md-8">
								<input class="form-control" type="text" id="noresi" name="noresi" value="">
							</div>
						</div>
						<div class="col-md-12" style="margin-top: 5px;">						
							<div class="form-group" style="margin-top: 5px;">
								<div class="col-md-12">
									<textarea class="form-control js-summernote" id="keterangan"></textarea>
								</div>
							</div>				
						</div>		
						<div class="col-md-12" style="margin-top: 5px;">
							<div class="col-md-12">
								<form class="dropzone" action="{base_url}tklaim_rincian/upload_files" method="post" enctype="multipart/form-data">
									<input id="idtransaksi" name="idtransaksi" type="hidden">
										<DIV class="dz-message needsclick">    
										<span class="note needsclick">(Bukti Resi
										Drag kesini)</span>
									  </DIV>
								  </form>
							</div>
						</div>
						<div class="col-md-12" style="margin-top: 5px;">
							<div class="col-md-12">
								<div class="table-responsive">
								<table class="table table-bordered table-striped table-responsive" id="listFile">
									<thead>
										<tr>
											<th width="20%">File</th>
											<th width="15%">Upload By</th>
											<th width="5%">Size</th>
											<th width="5%">Aksi</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
							</div>
						</div> 	
							
								
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="tid" placeholder="No. PO" name="tid" value="">
								
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_kirim" id="btn_kirim"><i class="fa fa-send"></i> KIRIM</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
var myDropzone 
$(document).ready(function(){
	// 	
	$(".number").number(true,0,'.',',');
	load_index();
   $('#keterangan').summernote({
		  height: 50,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})	
		Dropzone.autoDiscover = false;
		myDropzone = new Dropzone(".dropzone", { 
		   autoProcessQueue: true,
		   maxFilesize: 30,
		});
		myDropzone.on("complete", function(file) {
		  
		  refresh_image();
		  myDropzone.removeFile(file);
		  
		});
});
function load_index(){
	var no_klaim=$("#no_klaim").val();
	var idkelompokpasien=$("#idkelompokpasien").val();
	var idrekanan=$("#idrekanan").val();
	var status=$("#status_kirim").val();
	// alert(status);
	var tipe=$("#tipe").val();
	var tanggal_tagihan=$("#tanggal_tagihan").val();
	var tanggal_tagihan2=$("#tanggal_tagihan2").val();
	var status_pengiriman=$("#status_pengiriman").val();
	
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0,1,15,16,17,18,19], "visible": false },
							{ "width": "3%", "targets": [2] },
							{ "width": "5%", "targets": [5,6] },
							{ "width": "8%", "targets": [9,7,8,10,11,12,13] },
							{ "width": "10%", "targets": [3,4] },
							{ "width": "15%", "targets": [14] },
						 {"targets": [4,13], className: "text-left" },
						 {"targets": [2,7], className: "text-right" },
						 {"targets": [3,5,6,8,9,10,11,12], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tklaim_rincian/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						no_klaim:no_klaim,idkelompokpasien:idkelompokpasien,
						idrekanan:idrekanan,status:status,
						tipe:tipe,status_pengiriman:status_pengiriman,
						tanggal_tagihan2:tanggal_tagihan2,tanggal_tagihan:tanggal_tagihan,
					   }
            }
        });
}
$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_index();
});

$(document).on("click", ".verifikasi", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var tipe=table.cell(tr,0).data()
	var noreg=table.cell(tr,6).data()
	var tipekontraktor=table.cell(tr,20).data()
	var idkontraktor=table.cell(tr,21).data()
	var tanggal_jt=table.cell(tr,15).data()
	// alert(tanggal_jt);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Verifikasi No. Registrasi "+noreg+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		verifikasi(id,tipe,tipekontraktor,idkontraktor,tanggal_jt);
	});
});


function verifikasi($id,$tipe,$tipekontraktor,$idkontraktor,$tanggal_jt){
	console.log($id);
	var id=$id;		
	var tipe=$tipe;		
	var tipekontraktor=$tipekontraktor;		
	var idkontraktor=$idkontraktor;		
	var tanggal_jt=$tanggal_jt;		
	
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tklaim_rincian/verifikasi',
		type: 'POST',
		data: {id: id,tipe:tipe,tipekontraktor:tipekontraktor,idkontraktor:idkontraktor,tanggal_jt:tanggal_jt},
		complete: function(data) {
			console.log(data);
			$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi'});
			table.ajax.reload( null, false ); 
		}
	});
}

$(document).on("click", ".batal_verifikasi", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var nopenerimaan=table.cell(tr,4).data()
	// alert(id);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Membatalkan Verifikasi No. Penerimaan "+nopenerimaan+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		batal_verifikasi(id);
	});
});

function batal_verifikasi($id){
	// console.log($id);
	var id=$id;		
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tklaim_rincian/batal_verifikasi',
		type: 'POST',
		data: {id: id},
		complete: function() {
			$.toaster({priority : 'success', title : 'Succes!', message : ' Membatalkan Verifikasi'});
			table.ajax.reload( null, false ); 
		}
	});
}
$(document).on("click", ".ganti", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var tipe=table.cell(tr,0).data()
	var id=table.cell(tr,1).data()
	var idkelompokpasien=table.cell(tr,15).data()
	var idrekanan=table.cell(tr,16).data()
	var tanggal_tagihan=table.cell(tr,8).data()
	var st_cari=table.cell(tr,17).data()
	var jml=table.cell(tr,6).data()
	var st_multiple=table.cell(tr,18).data()
	var idpasien=table.cell(tr,19).data()
	$("#xjml").val(jml).val();
	$("#xtipe").val(tipe).val();
	$("#xid").val(id).val();
	$("#xidkelompokpasien").val(idkelompokpasien).trigger('change');
	$("#tgl_asal").val(tanggal_tagihan);
	$("#xtanggal_tagihan").val(tanggal_tagihan);
	if (idkelompokpasien!='1'){
		$('#div_rekanan').hide();
	}else{
		$('#div_rekanan').show();
		$("#xidrekanan").val(idrekanan).trigger('change');	
	}
	$("#xst_multiple").val(st_multiple);
	$("#xidpasien").val(idpasien);
	load_list_tanggal();
	$('#modal_edit').modal('show');
	
});
function load_list_tanggal(){
	var idkelompokpasien=$("#xidkelompokpasien").val();
	var idrekanan=$("#xidrekanan").val();
	var tipe=$("#xtipe").val();
	var st_multiple=$("#xst_multiple").val();
	var idpasien=$("#xidpasien").val();
	if (idkelompokpasien && tipe){
	// alert(tipe+' idrekanan'+idrekanan);
		$.ajax({
			url: '{site_url}tklaim_rincian/list_tanggal',
			dataType: "json",
			type: 'POST',
			data: {idkelompokpasien: idkelompokpasien,idrekanan:idrekanan,tipe:tipe,st_multiple:st_multiple,idpasien:idpasien},
			success: function(data) {
				$("#xtgl_pilih").empty();
				$('#xtgl_pilih').append(data.detail);
			}
		});
	}
}
$(document).on("change", "#xidkelompokpasien", function() {
	// load_list_tanggal();
	var idkelompokpasien=$("#xidkelompokpasien").val();
	$.ajax({
		url: '{site_url}tklaim_rincian/get_st_multiple',
		dataType: "json",
		type: 'POST',
		data: {idkelompokpasien: idkelompokpasien},
		success: function(data) {
			$("#xst_multiple").val(data.st_multiple);
			// alert('Load Tanggal');
			load_list_tanggal();
		}
	});
	if ($(this).val()=='1'){
		$("#xidrekanan").val("#").trigger('change');
		$('#div_rekanan').show();
	}else{
		$('#div_rekanan').hide();
	}
	
});
$(document).on("change", "#xidrekanan", function() {
	load_list_tanggal();
	
});
$(document).on("click", "#btn_ubah", function() {
	if ($("#xtgl_pilih").val()=='' || $("#xtgl_pilih").val()==null){
		sweetAlert("Maaf...", "Tanggal belum dipilih!", "error");
		return false;
	}
	// alert();
	if ($("#xidkelompokpasien").val()=='1' && $("#xidrekanan").val()=='#'){
		sweetAlert("Maaf...", "Rekanan Harus diisi!", "error");
		return false;
	}
	// if ($("#xtanggal_tagihan").val()==$("#xtgl_pilih").val()){
		// sweetAlert("Maaf...", "Rekanan Harus diisi!", "error");
		// return false;
	// }
	swal({
		title: "Anda Yakin ?",
		text : "Akan Memindahkan "+$("#xjml").val()+" Faktur ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		edit_tanggal_all();
	});
	
});
function edit_tanggal_all(){
	var klaim_id=$("#xid").val();		
	var tanggal_tagihan=$("#xtgl_pilih").val();		
	var idkelompokpasien=$("#xidkelompokpasien").val();		
	var idrekanan=$("#xidrekanan").val();		
	var tipe=$("#xtipe").val();		
	var st_multiple=$("#xst_multiple").val();
	var idpasien=$("#xidpasien").val();
	var table = $('#index_list').DataTable();
	$.ajax({
		url: '{site_url}tklaim_rincian/edit_tanggal_all',
		type: 'POST',
		data: {klaim_id: klaim_id,tanggal_tagihan:tanggal_tagihan,idkelompokpasien:idkelompokpasien,idrekanan:idrekanan,tipe:tipe,st_multiple:st_multiple,idpasien:idpasien},
		complete: function() {

			$.toaster({priority : 'success', title : 'Succes!', message : ' Memindahkan Tanggal Jatuh Tempo'});
			// location.reload();
			// filter_form();
			$("#modal_edit").modal('hide');
			$("#cover-spin").hide();
			table.ajax.reload( null, false ); 
		}
	});
}
$(document).on("click", ".stop", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var tanggal=table.cell(tr,8).data()
	swal({
		title: "Anda Yakin ?",
		text : "Akan Stop Periode Tanggal "+tanggal+"  ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
			$.ajax({
			url: '{site_url}tklaim_rincian/stop',
			type: 'POST',
			data: {id: id},
			complete: function() {

				$.toaster({priority : 'success', title : 'Succes!', message : ' Stop Tanggal Jatuh Tempo'});
				// location.reload();
				// filter_form();
				$("#cover-spin").hide();
				table.ajax.reload( null, false ); 
			}
		});
		// edit_tanggal_all();
	});
	
});
$(document).on("click", ".buka", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var tanggal=table.cell(tr,8).data()
	swal({
		title: "Anda Yakin ?",
		text : "Akan Buka Kembali Periode Tanggal "+tanggal+"  ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
			$.ajax({
			url: '{site_url}tklaim_rincian/buka',
			type: 'POST',
			data: {id: id},
			complete: function() {

				$.toaster({priority : 'success', title : 'Succes!', message : ' Membuka Tagihan Tanggal Jatuh Tempo'});
				// location.reload();
				// filter_form();
				$("#cover-spin").hide();
				table.ajax.reload( null, false ); 
			}
		});
		// edit_tanggal_all();
	});
	
});
$(document).on("click", ".kirim", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var tanggal=table.cell(tr,8).data()
	var no_klaim=table.cell(tr,3).data()
	var nama_rekanan=table.cell(tr,4).data()
	var jenis_layanan=table.cell(tr,0).data()
	if (jenis_layanan=='1'){
		jenis_layanan='RAWAT JALAN';
	}else{
		jenis_layanan='RAWAT INAP / ODS';
		
	}
	$("#tid").val(id);
	$.ajax({
		url: '{site_url}tklaim_rincian/get_data_kirim/'+id,
		dataType: "json",
		success: function(data) {
			console.log(data.detail.keterangan);
			$('#noresi').val(data.detail.noresi);
			$('#tanggal_kirim').val(data.detail.tanggal_kirim);
			if (data.detail.keterangan !=''){
				
				$('#keterangan').summernote('code',data.detail.keterangan);
			}else{
				$('#keterangan').summernote('code',' ');
			
			}
		}
	});
	$("#no_klaim2").val(no_klaim);
	$("#nama_rekanan").val(nama_rekanan);
	$("#jenis_layanan").val(jenis_layanan);
	$("#idtransaksi").val(id);
	refresh_image();
	$("#modal_kirim").modal('show');
});
function refresh_image(){
	var id=$("#idtransaksi").val();
	
	$.ajax({
		url: '{site_url}tklaim_rincian/refresh_image/'+id,
		dataType: "json",
		success: function(data) {
			// if (data.detail!=''){
				$('#listFile tbody').empty();
				$("#listFile tbody").append(data.detail);
			// }
			// console.log();
			
		}
	});
}
function hapus_file($id){
	var id=$id;
	
	swal({
		title: "Anda Yakin ?",
		text : "Akan Menghapus File "+id+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		hapus_file_resi(id);
	});
}
function hapus_file_resi($id){
	// console.log($id);
	var id=$id;		
	var klaim_id=$("#idtransaksi").val();		

	$.ajax({
		url: '{site_url}tklaim_rincian/hapus_file',
		type: 'POST',
		data: {id: id},
		complete: function() {
			refresh_image();
		}
	});
}
// $(document).on("click", ".removeData", function() {
	// var table = $('#index_list').DataTable();
	// tr = table.row($(this).parents('tr')).index();
	// var id=table.cell(tr,1).data()
	// var nopenerimaan=table.cell(tr,4).data()
	// // alert(id);return false;
	// swal({
		// title: "Anda Yakin ?",
		// text : "Akan Membatalkan Verifikasi No. Penerimaan "+nopenerimaan+" ?",
		// type : "success",
		// showCancelButton: true,
		// confirmButtonText: "Ya",
		// confirmButtonColor: "#34a263",
		// cancelButtonText: "Batalkan",
	// }).then(function() {
		// // pause_so();
		// batal_verifikasi(id);
	// });
// });
$(document).on("click", "#btn_kirim", function() {
	var id=$("#tid").val();
	var noresi=$("#noresi").val();
	var keterangan=$("#keterangan").summernote('code');
	var tanggal_kirim=$("#tanggal_kirim").val();
	// alert(keterangan);
	// return false;
	if ($("#noresi").val()==''){
		sweetAlert("Maaf...", "No Resi Harus diisi!", "error");
		return false;
	}
	if ($("#tanggal_kirim").val()==''){
		sweetAlert("Maaf...", "Tangga Pengiriman Harus diisi!", "error");
		return false;
	}
	
	$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tklaim_rincian/kirim',
			type: 'POST',
			data: {
				id: id,
				noresi: noresi,
				keterangan: keterangan,
				tanggal_kirim: tanggal_kirim,
				},
			complete: function() {
				 $('#index_list').DataTable().ajax.reload( null, false );
				// load_index();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Kirim Tagihan'});
				// location.reload();
					$("#modal_kirim").modal('hide');
				// filter_form();
				$("#cover-spin").hide();
			}
		});
});
</script>