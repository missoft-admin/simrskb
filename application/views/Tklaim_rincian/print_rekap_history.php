<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Print Kontrabon Rekap</title>
    <style>
	@page {
			margin-top: 140px;
			margin-left:50px;
			margin-right:50px;
			margin-bottom:100px;
            }
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }
	  
      th {
        padding: 5px;
      }
	  td {
		 
        padding: 5px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }
      
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;
		 
      }
	  td {
        padding: 3px;
		 
      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
		  padding-left:5px;
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
		
		header {
			position: fixed;
			top: -110px;
			left: 0px;
			right: 0px;
			height: 0px;

			
		}

		footer {
			position: fixed; 
			bottom: -260px; 
			left: 0px; 
			right: 0px;
			height: 50px; 
		}
		/*main {
			page-break-inside: auto;
		}
		*/
    }
	
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
	<header>
		<table class="content">
		  <tr>
			<td width="20%" class="text-left"><img src="assets/upload/logo/logomenu.jpg" alt="" ></td>
			<td width="30%" class="text-bold text-judul text-center text-top" style="height:50px">INFORMASI TAGIHAN SEBELUMNYA <br><br><?=strtoupper($namakontraktor);?></td>	
			<td width="20%" class="text-bold text-judul text-center text-top" style="height:50px"></td>	
		  </tr>
		</table>
	</header>
	<footer>
		<p style="position:fixed; bottom:40px; font-size: 18px; color: #3abad8;">
        JL. L.L.RE Martadinata No. 28 40115 Jawa Barat, Indonesia<br>T. (022) 4206717 F. (022) 4216436 E. rskb.halmahera@gmail.com ; www.halmaherasiaga.com
      </p>
	</footer>
	<main>		
	<table id="customers">
		<thead>
		  <tr>
			<th width="5%" class="border-full text-right"><strong>NO</strong></th>
			<th width="10%" class="border-full text-center"><strong>NO TAGIHAN</strong></th>
			<th width="10%" class="border-full text-center"><strong>JENIS TAGIHAN</strong></th>
			<th width="10%" class="border-full text-center"><strong>TANGGAL TAGIHAN</strong></th>
			<th width="15%" class="border-full text-center"><strong>TANGGAL KIRIM</strong></th>
			<th width="15%" class="border-full text-center"><strong>TANGGAL JATUH TEMPO</strong></th>
			<th width="10%" class="border-full text-center"><strong>UMUR TAGIHAN</strong></th>
			<th width="10%" class="border-full text-center"><strong>TOTAL TAGIHAN</strong></th>
			<th width="10%" class="border-full text-center"><strong>SISA TAGIHAN</strong></th>
			
		  </tr>
		</thead>
		<tbody>
    <?
	$number=0;
	$tot_tagihan=0;
	$tot_sisa=0;
	
	?>
      <?php foreach ($list_detail as $row){ ?>
        <?php 
		$number = $number + 1; 
		
		$tot_tagihan=$tot_tagihan + $row->total_tagihan;
		$tot_sisa=$tot_sisa + $row->sisa_tagihan;
		?>
        <tr>
          <td class="border-full text-right"> <?=$number?>&nbsp;&nbsp;</td>
		  <td class="border-full text-center"><?=strtoupper($row->no_klaim)?></td>
		  <td class="border-full text-center"><?=($row->tipe=='1'?'RAWAT JALAN':'RAWAT INAP')?></td>
		  <td class="border-full text-center"><?=HumanDateShort($row->tanggal_tagihan)?></td>
		  <td class="border-full text-center"><?=HumanDateShort($row->tanggal_kirim)?></td>
		  <td class="border-full text-center"><?=HumanDateShort($row->jatuh_tempo_bayar)?></td>
		  <td class="border-full text-center"><?=strtoupper($row->umur)?> Bulan</td>
		  <td class="border-full text-right"><?php echo number_format($row->total_tagihan,0)?>&nbsp;</td>
		  <td class="border-full text-right"><?php echo number_format($row->sisa_tagihan,0)?>&nbsp;</td>		 
        </tr>
      <? 
		
	  } ?>
		<tr>
			<td colspan="7"  class="border-full text-center text-bold" style="height:30px">TOTAL</td>
			<td  class="border-full text-right text-bold"><?=number_format($tot_tagihan,0)?>&nbsp; </td>
			<td  class="border-full text-right text-bold"><?=number_format($tot_sisa,0)?>&nbsp; </td>
			
		</tr>
		</tbody>
    </table>
	<br>
	<table id="customers" >
		 <tr>
			<td width="50%">Tanggal cetak <?=(date('d-m-Y H:i:s'))?> <?=$this->session->userdata('user_name')?></td>
			<td width="30%"></td>
			<td width="25%" class="text-center">Bandung, <?=date("d").' '.MONTHFormat(date("m")).' '.date("Y");?></td>
        </tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td class="text-center"><?= $userinput ?></td>
		</tr>
		
	</table>	
	
	</main>
	
	<br>
    
  </body>
</html>
