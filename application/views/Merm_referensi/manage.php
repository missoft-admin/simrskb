<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}merm_referensi/index/{ref_head_id}" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('merm_referensi/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="ref_head_id">Jenis</label>
				<div class="col-md-7">
					<div class="input-group">
						<select id="ref_head_id"  name="ref_head_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
							<option value="#" <?=($ref_head_id=='#'?'selected':'')?>>-Pilih Jenis-</option>
							<?php foreach ($list_head as $row) { ?>
							  <option value="<?=$row->id?>" <?=($row->id == $ref_head_id ? 'selected="selected"':'')?>><?=$row->referensi_nama?></option>
							<? } ?>
						</select>
						<span class="input-group-btn">
							<button class="btn btn-success" title="Refresh" type="button" id="btn_refresh"><i class="fa fa-refresh"></i></button>
							<button class="btn btn-primary" title="Add"  type="button" id="btn_tambah"><i class="fa fa-plus"></i></button>
							
						</span>
					</div>
					
				</div>
			</div>
			
			<div class="form-group" id="div_3">
				<label class="col-md-3 control-label" for="ref">Referensi</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="ref" placeholder="Referensi" name="ref" value="{ref}">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="nilai">Value</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nilai" placeholder="Value" name="nilai" value="{nilai}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="st_default">Set Default</label>
				<div class="col-md-7">
					<select id="st_default"  name="st_default" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
						  <option value="1" <?=(1 == $st_default ? 'selected="selected"':'')?>>YA</option>
						  <option value="0" <?=(0 == $st_default ? 'selected="selected"':'')?>>TIDAK</option>
						
					</select>
				</div>
			</div>
			s		
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}merm_referensi" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>

</div>
<div class="modal" id="modal_head" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width:50%">
        <form class="form-horizontal" action="javascript:void(0)" id="frm1">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">ADD  Referensi Head</h3>
                    </div>
                    <div class="block-content">
						<div class="block">
						<ul class="nav nav-tabs" data-toggle="tabs" id="tabs">
							<li class="active" id="tab_list">
								<a href="#tab_list_head"><i class="fa fa-list-ol"></i> List Referensi Head</a>
							</li>
							<li id="tab_add">
								<a href="#tab_add_head"><i class="fa fa-pencil"></i> Add / Edit  Referensi Head</a>
							</li>
							
						</ul>
						<div class="block-content tab-content">
							<div class="tab-pane fade fade-left in active" id="tab_list_head">
								<h5 class="font-w300 push-15" id="lbl_list_head">Dafar Referensi</h5>
								<table class="table table-bordered table-striped" id="index_head">
                                    <thead>
                                        <th>NO</th>
                                        <th>Referernsi Head</th>
                                        <th>Status</th>
                                        <th>Tools</th>
                                    </thead>
                                    <tbody></tbody>
                                  
                                </table>
							</div>
							<div class="tab-pane fade fade-left" id="tab_add_head">
								<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
								<div class="row">
									<div class="col-md-12">
										
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="referensi_nama">Nama Head</label>
											<div class="col-md-10">
												<input class="form-control" type="text" id="referensi_nama" >	
												<input class="form-control" type="hidden" id="id_head" >	
											</div>
										</div>
										
									</div>
									<div class="col-md-12">
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-2 control-label" for="idprespektif"></label>
											<div class="col-md-10">
												<button class="btn btn-success text-uppercase" type="button" name="btn_save_head" id="btn_save_head"><i class="fa fa-save"></i> Simpan</button>
												<button class="btn btn-warning text-uppercase" type="button" id="btn_clear_head" name="btn_clear_head" ><i class="fa fa-refresh"></i> Clear</button>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_close() ?>
							</div>
							
						</div>
					</div>
                        
                                     
                  </div>
                </div>
                <div class="modal-footer" style="margin-top:20px">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".number").number(true,0,'.',',');
		$("#ref").focus();
	})	
	
	
	function validate_final(){
		
		if ($("#ref_head_id").val()=='#'){
			sweetAlert("Maaf...", "Tentukan Jenis", "error");
			return false;
		}
		if ($("#ref").val()==''){
			sweetAlert("Maaf...", "Tentukan Referensi", "error");
			return false;
		}
		
		if ($("#nilai").val()==''){
			sweetAlert("Maaf...", "Tentukan Nilai", "error");
			return false;
		}
		
		return true;

		
	}
	$("#btn_tambah").click(function(){
		$("#modal_head").modal('show');
		LoadHead();
		$("#referensi_nama").focus();
	});
	$("#btn_refresh").click(function(){
		refresh_head_isian();
	});
	function refresh_head_isian(){
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}merm_referensi/refresh_head',
			method: "POST",
			dataType: "json",
			data: {
				// "idpoli": idpoli
			},
			success: function(data) {
				$("#ref_head_id").empty();
				
				$("#ref_head_id").append(data);
				$("#cover-spin").hide();
			},
			error: function(data) {
				console.log(data);
			}
		});
	}
	$("#btn_clear_head").click(function(){
		clear_head();
	});
$("#btn_save_head").click(function(){
	save_head();
});
function edit_head(id){
	clear_head();
	$("#cover-spin").show();
	$("#id_head").val(id);
	$('[href="#tab_add_head"]').tab('show');
	$.ajax({
			url: '{site_url}merm_referensi/load_head',
			type: 'POST',
			dataType: 'json',
			data: {
				id: id,
				
			},
			success: function(data) {
				$("#referensi_nama").val(data.referensi_nama);
					
				$("#cover-spin").hide();
				// table.ajax.reload( null, false ); 
			}
		});
}

function hapus_head(id){
	// clear_head();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
	$("#cover-spin").show();
		$.ajax({
			url: '{site_url}merm_referensi/hapus_head',
			type: 'POST',
			dataType: 'json',
			data: {
				id: id,
			},
			success: function(data) {
				swal({
					title: "Berhasil!",
					text: "Data diproses.",
					type: "success",
					timer: 1500,
					showConfirmButton: false
				});
					$("#cover-spin").hide();

				LoadHead();
			}
		});
	});

	
	
}
function aktifkan_head(id){
	// clear_head();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Aktifkan ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
	$("#cover-spin").show();
		$.ajax({
			url: '{site_url}merm_referensi/aktifkan_head',
			type: 'POST',
			dataType: 'json',
			data: {
				id: id,
			},
			success: function(data) {
				swal({
					title: "Berhasil!",
					text: "Data diproses.",
					type: "success",
					timer: 1500,
					showConfirmButton: false
				});
					$("#cover-spin").hide();

				LoadHead();
			}
		});
	});

	
	
}
function LoadHead() {
		var idpasien=$("#idpasien").val();
		
		$('#index_head').DataTable().destroy();
		var table = $('#index_head').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}merm_referensi/LoadHead/',
			type: "POST",
			dataType: 'json',
			data: {
				idpasien: idpasien,
				
			}
		},
		columnDefs: [
					 // {  className: "text-center", targets:[0,4] },
					 // { "width": "5%", "targets": [0] },
					 // { "width": "10%", "targets": [2,3,4] },
					 // { "width": "15%", "targets": [1,5] },

					]
		});
	}
function clear_head(){
	$("#id_head").val('')
	
	$("#referensi_nama").val('');
}
	function save_head(){
		

		var id_head=$("#id_head").val();		
		var referensi_nama=$("#referensi_nama").val();		
		
		if (referensi_nama==''){
			sweetAlert("Maaf...", "Tentukan Nama", "error");
			return false;

		}
		
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}merm_referensi/save_head',
			type: 'POST',
			data: {
				id_head: id_head,
				referensi_nama: referensi_nama,
			},
			success: function(data) {
				// $.toaster({priority : 'success', title : 'Succes!', message : ' Update Pembayaran'});
				if (data==false){
					swal({
							title: "Gagal!",
							text: "Data tidak diproses.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});
					
				}else{
					clear_head();
						$('[href="#tab_list_head"]').tab('show');
						LoadHead();
						swal({
							title: "Berhasil!",
							text: "Data diproses.",
							type: "success",
							timer: 1500,
							showConfirmButton: false
						});
					refresh_head_isian();
				}
				// location.reload();
				$("#cover-spin").hide();
				// table.ajax.reload( null, false ); 
			}
		});
	
	}
</script>