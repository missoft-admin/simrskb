<div class="modal" id="modal_head_ref" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width:50%">
        <form class="form-horizontal" action="javascript:void(0)" id="frm1">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title lbl_judul"> </h3>
                    </div>
                    <div class="block-content">
						<div class="block">
						<ul class="nav nav-tabs" data-toggle="tabs" id="tabs">
							<li class="active" id="tab_list">
								<a href="#tab_list_head"><i class="fa fa-list-ol"></i> <label class="lbl_judul">List Referensi</label></a>
							</li>
							
						</ul>
						<div class="block-content tab-content">
							<div class="tab-pane fade fade-left in active" id="tab_list_head">
								<div class="row">
									<div class="col-md-12">
										<input class="form-control" type="hidden" id="nama_variable" value="">	
										<input class="form-control" type="hidden" id="nilai_variable" value="">	
										<input class="form-control" type="hidden" id="ref_head_id" value="">	
										<input class="form-control" type="hidden" id="st_edited_ref" value="0">	
										<input class="form-control" type="hidden" id="idref" value="">	
										
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="referensi_nama">Referensi</label>
											<div class="col-md-10">
												<input class="form-control" type="text" id="referensi_nama" >	
											</div>
										</div>
										
									</div>
									<div class="col-md-12">
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-2 control-label" for="idprespektif"></label>
											<div class="col-md-10">
												<button class="btn btn-success text-uppercase" onclick="saveRef()" type="button" name="btn_save_head" id="btn_save_head"><i class="fa fa-save"></i> Simpan</button>
												<button class="btn btn-warning text-uppercase" onclick="clearRef()" type="button" id="btn_clear_head" name="btn_clear_head" ><i class="fa fa-refresh"></i> Clear</button>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<table class="table table-bordered table-striped" id="tabel_index_ref">
											<thead>
												<th>NO</th>
												<th>Referernsi</th>
												<th>Status</th>
												<th>Tools</th>
											</thead>
											<tbody></tbody>
										  
										</table>
										
									</div>
								</div>
							</div>
							
						</div>
					</div>
                        
                                     
                  </div>
                </div>
                <div class="modal-footer" style="margin-top:20px">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
	
function add_referensi(nama_variable,ref_head_id,judul){
	$(".lbl_judul").html(judul);
	$("#ref_head_id").val(ref_head_id);
	$("#nama_variable").val(nama_variable);
	$("#modal_head_ref").modal('show');
	clearRef();
	
}
function saveRef(){
	$("#cover-spin").show();
	let nama_variable=$("#nama_variable").val();
	$.ajax({
		url: '{site_url}Merm_referensi/saveRef', 
		dataType: "JSON",
		method: "POST",
		data : {
				id:$("#idref").val(),
				ref_head_id:$("#ref_head_id").val(),
				ref:$("#referensi_nama").val(),
				nilai:$("#nilai_variable").val(),
				st_edited_ref:$("#st_edited_ref").val(),
				
			},
		success: function(data) {
			$("#cover-spin").hide();
			clearRef();
			refresh_ref_isian();
			// $("#btn_save_head").html('<i class="fa fa-save"></i> Simpan Edit');
			// $("#st_edited_ref").val(1);
			// $("#nilai_variable").val(data.id);
			// $("#referensi_nama").val(data.ref);
		}
	});
}
function refresh_ref_isian(){
	let nama_variable=$("#nama_variable").val();
	let ref_head_id=$("#ref_head_id").val();
	if (nama_variable!=''){
	$("#cover-spin").show();
		$.ajax({
			url: '{site_url}merm_referensi/refresh_ref_isian',
			method: "POST",
			dataType: "json",
			data: {
				ref_head_id: ref_head_id
			},
			success: function(data) {
				$("#"+nama_variable).empty();
				
				$("#"+nama_variable).append(data);
				$("#cover-spin").hide();
			},
			error: function(data) {
				console.log(data);
			}
		});
	}
}
function removeDataRef(id){
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus Ref?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}Merm_referensi/removeDataRef', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				},
			success: function(data) {
				$("#cover-spin").hide();
				clearRef();
				refresh_ref_isian();
			}
		});
	});	
	
}
function editDataRef(id){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Merm_referensi/editDataRef', 
		dataType: "JSON",
		method: "POST",
		data : {
				id:id,
				
			},
		success: function(data) {
			$("#cover-spin").hide();
			$("#btn_save_head").html('<i class="fa fa-save"></i> Simpan Edit');
			$("#st_edited_ref").val(1);
			$("#nilai_variable").val(data.nilai);
			$("#idref").val(data.id);
			$("#referensi_nama").val(data.ref);
		}
	});
}
function clearRef(){
	load_tabel_referensi();
	$("#st_edited_ref").val('0');
	$("#idref").val('');
	$("#referensi_nama").val('');
	$("#btn_save_head").html('<i class="fa fa-save"></i> Simpan');
}
function load_tabel_referensi(){
	$.ajax({
			url: '{site_url}Merm_referensi/load_tabel_referensi', 
			dataType: "JSON",
			method: "POST",
			data : {
					ref_head_id:$("#ref_head_id").val(),
					
				},
			success: function(data) {
				$("#tabel_index_ref tbody").empty();
				$("#tabel_index_ref tbody").append(data.tabel);
			}
		});
}
</script>