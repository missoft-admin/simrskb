<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">
		
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:25px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status_pembayaran">No Registrasi</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="notransaksi" placeholder="No Registrasi" name="notransaksi" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Approval</label>
                    <div class="col-md-8">
                        <select id="status_approval" name="status_approval" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#"  <?=($status_approval=='#'?'selected':'')?>>- Semua Status -</option>
							<option value="1" <?=($status_approval=='1'?'selected':'')?>>Menunggu</option>
							<option value="2" <?=($status_approval=='2'?'selected':'')?>>Disetujui</option>
							<option value="3" <?=($status_approval=='3'?'selected':'')?>>Ditolak</option>
							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Status Pembayaran</label>
                    <div class="col-md-8">
                        <select id="status_pembayaran" name="status_pembayaran" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#"  <?=($status_pembayaran=='#'?'selected':'')?>>- Semua Status -</option>
							<option value="0" <?=($status_pembayaran=='0'?'selected':'')?>>Belum dibayar</option>
							<option value="1" <?=($status_pembayaran=='1'?'selected':'')?>>Telah dibayar</option>							
						</select>
                    </div>
                </div>
            </div>
			<div class="col-md-6">	
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Dokter</label>
                    <div class="col-md-8">
                        <select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua Dokter -</option>
							<?
								$list_dokter=$this->db->query("SELECT M.id,M.nama from mdokter M WHERE M.`status`='1' ORDER BY M.nama ASC")->result();
							?>
							<?foreach($list_dokter as $row){?>	
								<option value="<?=$row->id?>"><?=$row->nama?></option>
							<?}?>
						</select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Jatuh Tempo</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggal_jatuhtempo1" name="tanggal_jatuhtempo1" placeholder="From" value="{tanggal_jatuhtempo1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_jatuhtempo2" name="tanggal_jatuhtempo2" placeholder="To" value="{tanggal_jatuhtempo2}"/>
                        </div>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Pembayaran</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggal_pembayaran1" name="tanggal_pembayaran1" placeholder="From" value="{tanggal_pembayaran1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_pembayaran2" name="tanggal_pembayaran2" placeholder="To" value="{tanggal_pembayaran2}"/>
                        </div>
                    </div>
                </div>
               
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>                        
                    </div>
					
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th hidden width="2%">ID</th>
                    <th width="10%">NO</th>
                    <th width="10%">NO REGISTRASI</th>
                    <th width="10%">DOKTER</th>
                    <th width="10%">TANGGAL PEMBAYARAN</th>
                    <th width="10%">NOMINAL</th>
                    <th width="10%">TANGGAL JATUH TEMPO</th>
                    <th width="10%">PEMBAYARAN</th>
                    <th width="10%">APPROVAL</th>
                    <th width="15%" class="text-center">AKSI</th>
                    
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_user" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<input type="hidden" class="form-control" id="idrka" placeholder="" name="idrka" value="">
						<div class="col-md-12">
							<div class="table-responsive">
							<table width="100%" id="tabel_user_proses" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Step</th>
										<th>User</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						</div>
						
					</div>
					<?php echo form_close() ?>
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
			
		</div>
	</div>
</div>
<div class="modal fade" id="modal_tolak" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Alasan Tolak</h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status_pembayaran">Alasan</label>
								<div class="col-md-8">
									<textarea class="form-control" rows="2" name="alasan_tolak" id="alasan_tolak"></textarea>
									<input type="hidden" readonly class="form-control" id="id_approval"  placeholder="id_approval" name="id_approval" value="">
								</div>
							</div>
						</div>							
					</div>
					
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-danger text-uppercase" type="button" name="btn_tolak" id="btn_tolak"><i class="fa fa-save"></i> SIMPAN TOLAK</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	load_index();
   
});
function load_index(){
	var notransaksi=$("#notransaksi").val();
	var status_approval=$("#status_approval").val();
	var iddokter=$("#iddokter").val();
	var status_pembayaran=$("#status_pembayaran").val();
	var tanggal_jatuhtempo1=$("#tanggal_jatuhtempo1").val();
	var tanggal_jatuhtempo2=$("#tanggal_jatuhtempo2").val();
	var tanggal_pembayaran1=$("#tanggal_pembayaran1").val();
	var tanggal_pembayaran2=$("#tanggal_pembayaran2").val();
	$('#index_list').DataTable().destroy();
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0], "visible": false },
							{ "width": "3%", "targets": [1] },
							{ "width": "8%", "targets": [2,4,5,6,7,8,9] },
							{ "width": "15%", "targets": [3] },
						 {"targets": [1,5], className: "text-right" },
						 {"targets": [2,4,5,6,7,8,9], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}thonor_bayar/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						notransaksi:notransaksi,status_pembayaran:status_pembayaran,status_approval:status_approval,
						tanggal_pembayaran1:tanggal_pembayaran1,
						tanggal_pembayaran2:tanggal_pembayaran2,
						tanggal_jatuhtempo1:tanggal_jatuhtempo1,
						tanggal_jatuhtempo2:tanggal_jatuhtempo2,
						iddokter:iddokter,
					   }
            }
        });
}
$("#btn_filter").click(function() {
	// alert('ID');
	// table.destroy();
	load_index();
});


function verif($id){
	var table = $('#index_list').DataTable()
		var id = $id;
		// alert(id_approval);return false;
		swal({
			title: "Apakah Anda Yakin  ?",
			text : "Verifikasi Honor Dokter ini?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}thonor_bayar/verifikasi/'+id,
				type: 'POST',
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Honor Dokter Berhasil Diverifikasi'});
					$('#index_list').DataTable().ajax.reload( null, false );
				}
			});
		});
}

</script>