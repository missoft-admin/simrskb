<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Print Piutang Karyawan Per Karyawan</title>
    <style>
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }
	  
      th {
        padding: 5px;
      }
	  td {
        padding: 5px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }
      
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;
		 
      }
	  td {
        padding: 3px;
		 
      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
    }
	
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
  
	<?php 
	  $i=0;
  $len = count($list_detail);
	foreach ($list_detail as $row){ 
	$i=$i+1;
	?>
    <table class="content">
      <tr>
        <td rowspan="3" width="30%" class="text-center"><img src="{logo1_rs}" alt="" width="60" height="60"></td>
        <td rowspan="2" colspan="3" width="50%"  class="text-center text-bold text-judul"><u>RINCIAN TAGIHAN KARYAWAN</u></td>
		<td rowspan="2" width="20%"></td>		
      </tr>
	  <tr></tr>
	  <tr>
		<td width="24%" class="text-left text-bold text-top">NO TAGIHAN</td>
		<td width="20%" colspan="3" class="text-top">:  <?=$row->no_piutang?></td>		
	  </tr>
	  <tr>
	    <td  class="text-center">{alamat_rs1}</td>
		<td  class="text-left text-bold text-top">NAMA KARYAWAN / DOKTER</td>
		<td width="20%" colspan="3" class="text-top">: <?=$row->nama?></td>
	  </tr>
	  <tr>
	    <td  class="text-center"><?=$telepon_rs.' '.$fax_rs?></td>
		<td class="text-left text-bold text-top">TANGGAL TAGIHAN</td>
		<td class=" text-top" colspan="3" >: <?=HumanDateShort($row->tanggal_tagihan)?></td>
		
	  </tr>
	 <tr>
	    <td  class="text-center"></td>
		<td class="text-left text-bold text-top">TIPE TAGIHAN</td>
		<td class=" text-top" colspan="3" >: <?=$row->nama_tipe?></td>
		
	  </tr>
    </table>
    
	<br>
    <table id="customers">
      <tr>
        <th width="4%" class="border-full text-center"><strong>NO</strong></th>
        <th width="10%" class="border-full text-center"><strong>LAYANAN </strong></th>
        <th width="10%" class="border-full text-center"><strong>NO REGISTER</strong></th>
        <th width="8%" class="border-full text-center"><strong>TANGGAL KUNJUNGAN </strong></th>
        <th width="8%" class="border-full text-center"><strong>TANGGAL TRANSAKSI</strong></th>
        <th width="8%" class="border-full text-center"><strong>NO MEDREC</strong></th>
        <th width="15%" class="border-full text-center"><strong>NAMA PASIEN</strong></th>
        <th width="10%" class="border-full text-center"><strong>STATUS TAGIHAN</strong></th>
        <th width="10%" class="border-full text-center"><strong>STATUS CICILAN</strong></th>
        <th width="10%" class="border-full text-center"><strong>NOMINAL</strong></th>
      </tr>
      <?php 
	  $piutang_id=$row->id;
	  $tipepegawai=$row->tipepegawai;
	  $iddokter=$row->iddokter;
	  $idpegawai=$row->idpegawai;
	  $q="SELECT TD.id,TD.pendaftaran_id,TD.kasir_id,TP.tanggaldaftar,TP.nopendaftaran,TP.no_medrec,CONCAT(TP.title,'. ',TP.namapasien) as namapasien
			,TD.nominal
			,TH.tanggal_tagihan,TD.tipe,TD.jml_cicilan,K.tanggal as tanggal_kasir,'RJ' as tipe_nama,TD.status_lunas,TD.piutang_id,TD.tipepegawai
			,TD.cicilan_ke
			from tpiutang_detail TD
			LEFT JOIN tpiutang TH ON TH.id=TD.piutang_id
			LEFT JOIN tpoliklinik_pendaftaran TP ON TP.id=TD.pendaftaran_id
			LEFT JOIN mpoliklinik M ON TP.idpoliklinik=M.id
			LEFT JOIN mdokter D ON D.id=TP.iddokter
			LEFT JOIN tkasir K ON K.id=TD.kasir_id
			WHERE TD.piutang_id='$piutang_id' AND TD.tipe='1' AND TD.tipepegawai='$tipepegawai' AND TD.iddokter='$iddokter' AND TD.idpegawai='$idpegawai'
			GROUP BY TP.id

			UNION ALL

			SELECT TD.id,TD.pendaftaran_id,TD.kasir_id,TP.tanggal as tanggaldaftar,TP.nopenjualan as nopendaftaran,TP.nomedrec as no_medrec,TP.nama as namapasien
			,TD.nominal
			,TH.tanggal_tagihan,TD.tipe,TD.jml_cicilan,K.tanggal as tanggal_kasir,'Pembelian Obat' as tipe_nama,TD.status_lunas,TD.piutang_id,TD.tipepegawai
			,TD.cicilan_ke
			from tpiutang_detail TD
			LEFT JOIN tpiutang TH ON TH.id=TD.piutang_id
			LEFT JOIN tpasien_penjualan TP ON TP.id=TD.pendaftaran_id
			LEFT JOIN tkasir K ON K.id=TD.kasir_id
			WHERE TD.piutang_id='$piutang_id' AND TD.tipe='3' AND TD.tipepegawai='$tipepegawai' AND TD.iddokter='$iddokter' AND TD.idpegawai='$idpegawai'
			GROUP BY TP.id

			UNION ALL

			SELECT TD.id,TD.pendaftaran_id,TD.kasir_id,TP.tanggaldaftar,TP.nopendaftaran,TP.no_medrec,CONCAT(TP.title,'. ',TP.namapasien) as namapasien
			,TBD.nominal,TD.tanggal_tagihan,TD.tipe,TD.jml_cicilan,TB.tanggal as tanggal_kasir,'RI/ODS' as tipe_nama,TD.status_lunas,TD.piutang_id,TD.tipepegawai
			,TD.cicilan_ke
			from tpiutang_detail TD
			LEFT JOIN trawatinap_pendaftaran TP ON TP.id=TD.pendaftaran_id
			LEFT JOIN trawatinap_tindakan_pembayaran TB ON TB.idtindakan=TP.id AND TB.statusbatal=0
			LEFT JOIN trawatinap_tindakan_pembayaran_detail TBD ON TBD.idtindakan=TB.id
			WHERE TD.piutang_id='$piutang_id' AND TD.tipe='2'  AND TD.tipepegawai='$tipepegawai' AND TD.iddokter='$iddokter' AND TD.idpegawai='$idpegawai'
			GROUP BY TP.id";
	  $list_query=$this->db->query($q)->result();
	  $number = 0; $total=0;$total_all=0;?>
      <?php foreach ($list_query as $r){ ?>
        <?php 
		$number = $number + 1; 
		
		$total = $r->nominal;
		$total_all = $total_all + $total ;
		// $total=0;
		?>
        <tr>
          <td class="border-full text-right"> <?=$number?>&nbsp;&nbsp;</td>
		  <td class="border-full text-center">&nbsp;&nbsp;<?=strtoupper($r->tipe_nama)?></td>
          <td class="border-full text-center">&nbsp;&nbsp;<?=($r->nopendaftaran)?></td>
          <td class="border-full text-center">&nbsp;&nbsp;<?=HumanDateShort($r->tanggaldaftar)?></td>
          <td class="border-full text-center">&nbsp;&nbsp;<?=HumanDateShort($r->tanggal_kasir)?></td>
          <td class="border-full text-center">&nbsp;&nbsp;<?=($r->no_medrec)?></td>
          <td class="border-full text-left">&nbsp;&nbsp;<?=($r->namapasien)?></td>
          <td class="border-full text-center">&nbsp;&nbsp;<?= ($r->cicilan_ke=='1' ? 'TAGIHAN BARU':'TAGIHAN LAMA')?></td>
          <td class="border-full text-center">&nbsp;&nbsp;<?= ($r->jml_cicilan>1 ? 'CICILAN '.$r->cicilan_ke.'/'.$r->jml_cicilan:'LANGSUNG')?></td>
		  <td class="border-full text-right"><?=number_format($r->nominal,0)?>&nbsp;&nbsp;</td>
		 
        </tr>
      <? } ?>
		<tr>
			<td colspan="9"  class="border-full text-center text-bold">TOTAL</td>
			<td  class="border-full text-right text-bold"><?=number_format($total_all,0)?>&nbsp;&nbsp;</td>
			
		</tr>
    </table><br>
    <table>
		<tr>
			<td width="80%"><p class="text-muted text-italic">Tanggal Cetak <?=date('d-m-Y H:i:s').' | '.$this->session->userdata('user_name')?>.</p>.</td>
			<td width="20%" class="text-center">Kepala Bagian Keuangan <br><br><br><br></td>
		</tr>
		<tr><td width="80%" class="text-center"></td><td width="20%"></td></tr>
		<tr><td width="80%" class="text-center"></td><td width="20%"></td></tr>
		<tr><td width="80%" class="text-center"></td><td width="20%"></td></tr>
		<tr><td width="80%" class="text-center"></td><td width="20%"></td></tr>
		<tr><td width="80%" class="text-center"></td><td width="20%"></td></tr>
		<tr>
			<td width="80%"></td>
			<td width="20%" class="text-center">( _______________________________ )</td>
		</tr>
	</table>
	<?php if ($i<$len){ ?>
	<p style="page-break-before: always;">
	<?}?>
	<?}?>
  </body>
</html>
