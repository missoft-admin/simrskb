<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Tagihan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_piutang" placeholder="No Tagihan" name="no_piutang" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Deskripsi</label>
                    <div class="col-md-8">
                        <select id="tanggal_id" name="tanggal_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Deskripsi-</option>
							<?foreach($list_deskripsi as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
				
				
				          
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Status</label>
                    <div class="col-md-8">
						<select id="status_kirim" name="status_kirim" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua Status -</option>
							<option value="0">Belum Diproses</option>
							<option value="1">Sudah Diproses</option>							
						</select>
					</div>
                </div>
				
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Tagihan</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
                            <input class="form-control" type="text" id="tanggal_tagihan" name="tanggal_tagihan" placeholder="From" value="{tanggal_tagihan}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_tagihan2" name="tanggal_tagihan2" placeholder="To" value="{tanggal_tagihan2}"/>
                        </div>
                    </div>
                </div>
               
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-4" hidden>
                        <button class="btn btn-danger text-uppercase" type="button" name="btn_verif_filter" id="btn_verif_filter" style="font-size:13px;width:100%;"><i class="fa fa-check-square-o"></i> Verifikasi By Filter</button>
                    </div>
					<div class="col-md-8" hidden>
                        <button disabled class="btn btn-primary text-uppercase" type="button" name="btn_verif_page" id="btn_verif_page" style="font-size:13px;width:100%;"><i class="fa fa-check"></i> Verifikasi Per page</button>
                    </div>
                </div>
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th hidden width="2%">ID</th>
                    <th hidden width="5%">TIPE</th>
                    <th width="10%">#</th>
                    <th width="10%">NO TAGIHAN</th>
                    <th width="10%">DESKRIPSI</th>
                    <th width="10%">TANGGAL TAGIHAN</th>
                    <th width="10%">TOT TAGIHAN</th>
                    <th width="15%">STATUS</th>
                    <th width="15%" class="text-center">AKSI</th>
                    <th width="15%" class="text-center">AKSI</th>
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
</div>
<div class="modal fade" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Pengalihan Tanggal Tagihan </h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">
							<label class="col-md-4 control-label" for="status">Tanggal Asal</label>
							<div class="col-md-8">
							<div class="input-group">
								<input class="form-control" readonly type="text" id="tgl_asal" name="tgl_asal" value="">

								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
							</div>
						</div> 	
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Deskripsi</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="xdeskripsi" placeholder="" name="xdeskripsi" value="">
								</div>
							</div>
						</div>
						
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Tanggal Baru</label>
								<div class="col-md-8">
									<select id="xtgl_pilih" name="xtgl_pilih" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										
									</select>
								</div>
							</div>				
						</div>				
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="xid" placeholder="No. PO" name="xid" value="">
								<input type="hidden" class="form-control" id="xtanggal_id" placeholder="No. PO" name="xtanggal_id" value="">
								<input type="hidden" class="form-control" id="xtanggal_tagihan" placeholder="No. PO" name="xtanggal_tagihan" value="">
								<input type="hidden" class="form-control" id="xidpengaturan" placeholder="No. PO" name="xidpengaturan" value="">
								
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_ubah" id="btn_ubah"><i class="fa fa-save"></i> SIMPAN</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	// 	
	$(".number").number(true,0,'.',',');
	load_index();
   
});
function load_index(){
	var no_piutang=$("#no_piutang").val();
	var tanggal_id=$("#tanggal_id").val();
	var status=$("#status_kirim").val();
	// alert(status);
	var tanggal_tagihan=$("#tanggal_tagihan").val();
	var tanggal_tagihan2=$("#tanggal_tagihan2").val();
	
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0,1,9], "visible": false },
							{ "width": "5%", "targets": [2] },
							{ "width": "10%", "targets": [3,5] },
							{ "width": "15%", "targets": [4,6,7,8] },
						 // {"targets": [4,13], className: "text-left" },
						 {"targets": [2,6], className: "text-right" },
						 {"targets": [3,5,7,8], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tpiutang_rincian/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						no_piutang:no_piutang,tanggal_id:tanggal_id,
						status:status,
						tanggal_tagihan2:tanggal_tagihan2,tanggal_tagihan:tanggal_tagihan,
					   }
            }
        });
}
$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_index();
});

$(document).on("click", ".verifikasi", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var tipe=table.cell(tr,0).data()
	var noreg=table.cell(tr,6).data()
	var tipekontraktor=table.cell(tr,20).data()
	var idkontraktor=table.cell(tr,21).data()
	var tanggal_jt=table.cell(tr,15).data()
	// alert(tanggal_jt);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Verifikasi No. Registrasi "+noreg+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		verifikasi(id,tipe,tipekontraktor,idkontraktor,tanggal_jt);
	});
});


function verifikasi($id,$tipe,$tipekontraktor,$idkontraktor,$tanggal_jt){
	console.log($id);
	var id=$id;		
	var tipe=$tipe;		
	var tipekontraktor=$tipekontraktor;		
	var idkontraktor=$idkontraktor;		
	var tanggal_jt=$tanggal_jt;		
	
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tpiutang_rincian/verifikasi',
		type: 'POST',
		data: {id: id,tipe:tipe,tipekontraktor:tipekontraktor,idkontraktor:idkontraktor,tanggal_jt:tanggal_jt},
		complete: function(data) {
			console.log(data);
			$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi'});
			table.ajax.reload( null, false ); 
		}
	});
}

$(document).on("click", ".batal_verifikasi", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var nopenerimaan=table.cell(tr,4).data()
	// alert(id);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Membatalkan Verifikasi No. Penerimaan "+nopenerimaan+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		batal_verifikasi(id);
	});
});
function batal_verifikasi($id){
	// console.log($id);
	var id=$id;		
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tpiutang_rincian/batal_verifikasi',
		type: 'POST',
		data: {id: id},
		complete: function() {
			$.toaster({priority : 'success', title : 'Succes!', message : ' Membatalkan Verifikasi'});
			table.ajax.reload( null, false ); 
		}
	});
}
$(document).on("click", ".ganti", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,0).data()
	var tanggal_id=table.cell(tr,1).data()
	var idpengaturan=table.cell(tr,9).data()
	var tanggal=table.cell(tr,5).data()
	var deskripsi=table.cell(tr,4).data()
	
	
	$("#xid").val(id);
	$("#xtanggal_id").val(tanggal_id);
	$("#tgl_asal").val(tanggal).val();
	$("#xtanggal_tagihan").val(tanggal).val();
	$("#xdeskripsi").val(deskripsi).val();
	$("#xidpengaturan").val(idpengaturan).val();
	load_list_tanggal();
	
	$('#modal_edit').modal('show');
	
});
function load_list_tanggal(){
	var idpengaturan=$("#xidpengaturan").val();
	if (idpengaturan){
		$.ajax({
			url: '{site_url}tpiutang_rincian/list_tanggal_by_tangggal_id/'+idpengaturan,
			dataType: "json",
			success: function(data) {
				$("#xtgl_pilih").empty();
				$('#xtgl_pilih').append(data.detail);
			}
		});
	}
}

$(document).on("click", "#btn_ubah", function() {
	if ($("#xtgl_pilih").val()=='' || $("#xtgl_pilih").val()==null){
		sweetAlert("Maaf...", "Tanggal belum dipilih!", "error");
		return false;
	}
	// alert();
	
	swal({
		title: "Anda Yakin ?",
		text : "Akan Memindahkan Semua Tanggal Penagihan  ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		
		edit_tanggal_all();
	});
	
});
function edit_tanggal_all(){
	var piutang_id=$("#xid").val();		
	var idpengaturan=$("#xidpengaturan").val();		
	var tanggal_id=$("#xtgl_pilih").val();		
	var tanggal_tagihan=$("#xtgl_pilih option:selected").text();	
// alert(tanggal_id);
// return false;	
	$("#cover-spin").show();
	var table = $('#index_list').DataTable();
	$.ajax({
		url: '{site_url}tpiutang_rincian/edit_tanggal_all',
		type: 'POST',
		data: {
			piutang_id: piutang_id,tanggal_tagihan:tanggal_tagihan
			,tanggal_id: tanggal_id
			,idpengaturan: idpengaturan
		
		},
		complete: function() {

			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Memindahkan Tanggal tagihan'});
			// location.reload();
			// filter_form();
			$("#modal_edit").modal('hide');
			table.ajax.reload( null, false ); 
		}
	});
}

$(document).on("click", ".kirim", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,0).data()
	var tanggal=table.cell(tr,5).data()
	swal({
		title: "Anda Yakin ?",
		text : "Akan Proses Selesai Tanggal : "+tanggal+"  ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpiutang_rincian/kirim',
			type: 'POST',
			data: {
				id: id,
				
				},
			complete: function() {
				 $('#index_list').DataTable().ajax.reload( null, false );
				$.toaster({priority : 'success', title : 'Succes!', message : ' Kirim Tagihan'});
				$("#cover-spin").hide();
			}
		});
	});
	// var tanggal=table.cell(tr,8).data()
	// var no_piutang=table.cell(tr,3).data()
	// var nama_rekanan=table.cell(tr,4).data()
	// var jenis_layanan=table.cell(tr,0).data()
	// if (jenis_layanan=='1'){
		// jenis_layanan='RAWAT JALAN';
	// }else{
		// jenis_layanan='RAWAT INAP / ODS';
		
	// }
	// $("#tid").val(id);
	// $.ajax({
		// url: '{site_url}tpiutang_rincian/get_data_kirim/'+id,
		// dataType: "json",
		// success: function(data) {
			// console.log(data.detail.keterangan);
			// $('#noresi').val(data.detail.noresi);
			// $('#tanggal_kirim').val(data.detail.tanggal_kirim);
			// if (data.detail.keterangan !=''){
				
				// $('#keterangan').summernote('code',data.detail.keterangan);
			// }else{
				// $('#keterangan').summernote('code',' ');
			
			// }
		// }
	// });
	// $("#no_piutang2").val(no_piutang);
	// $("#nama_rekanan").val(nama_rekanan);
	// $("#jenis_layanan").val(jenis_layanan);
	
	// $("#modal_kirim").modal('show');
});

</script>