<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work-1"'); ?>
                <div class="col-md-6"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Nomor Tagihan</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{no_piutang}" disabled>
                            <input class="form-control" type="hidden" name="piutang_id" id="piutang_id" value="{id}" >
							<input class="form-control" type="hidden" name="idpengaturan" id="idpengaturan" value="{idpengaturan}" >
                            <input class="form-control" type="hidden" name="tanggal_id" id="tanggal_id" value="{tanggal_id}" >
                            <input class="form-control" type="hidden" name="iddokter" id="iddokter" value="{iddokter}" >
                            <input class="form-control" type="hidden" name="idpegawai" id="idpegawai" value="{idpegawai}" >
                            <input class="form-control" type="hidden" name="tipepegawai" id="tipepegawai" value="{tipepegawai}" >
                            <input class="form-control" type="hidden" name="tipe" id="tipe" value="{tipe}" >
                            <input class="form-control" type="hidden" name="disabel" id="disabel" value="{disabel}" >
                        </div> 
                    </div>
                    <div class="form-group" style="margin-bottom: 15px;"> 
                        <label class="col-md-3 control-label">Nama</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{atas_nama}" disabled>
                        </div> 
                    </div>
                                      
                </div>
				<div class="col-md-6" style="margin-bottom: 5px;"> 
                    
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Tanggal Tagihan</label> 
                        <div class="col-md-4"> 
                            <input class="form-control" value="<?=HumanDateShort($tanggal_tagihan)?>" disabled>
                        </div> 
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Deskripsi</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{deskripsi}" disabled>
                        </div> 
                    </div>
                    
                </div>
				<div class="col-md-12" style="margin-bottom: 5px;"> 
				<div class="table-responsive">
				<table class="table table-bordered table-striped table-responsive" id="index_list">
					<thead>
						<tr>
							<th>#</th>
							<th>#</th>
							<th>#</th>
							<th>Tanggal Kunjungan</th>
							<th>Tanggal Trx</th>
							<th>Layanan</th>
							<th>No Register</th>
							<th>No. Medrec</th>
							<th>Pasien</th>
							<th>Tagihan</th>
							<th>Status Tagihan</th>
							<th>Nominal</th>
							<th>Status</th>
							<th>AKSI  <div class="btn-group" role="group">
									<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										<span class="caret"></span>
									</button>
									<ul class="dropdown-menu dropdown-menu-right">
										<li class="dropdown-header">Pilih Action</li>
										<li>
											<a tabindex="-1" class="lunas_semua" href="javascript:void(0)">Lunaskan Semua</a>
										</li>
										
									</ul>
								</div>
							</th>
							
						</tr>
					</thead>
					<tbody> </tbody>
					  
				</table>
				</div>
				</div>
            <?php echo form_close(); ?>
        </div>
        
        <div class="form-group" style="margin-top: 20px;"></div>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">


    var table;

    $(document).ready(function(){
			// alert($("#xidkelompokpasien").val());
	
		load_detail();
		
    });
	
    function load_detail() {
		var piutang_id=$("#piutang_id").val();
		var tipepegawai=$("#tipepegawai").val();
		var iddokter=$("#iddokter").val();
		var idpegawai=$("#idpegawai").val();
		var disabel=$("#disabel").val();
		var tipe=$("#tipe").val();
		// alert(tgl_trx);
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tpiutang_rincian/load_detail_trx/',
			type: "POST",
			dataType: 'json',
			data: {
				piutang_id: piutang_id,
				tipepegawai: tipepegawai,
				iddokter: iddokter,
				idpegawai:idpegawai,
				disabel:disabel,
				tipe:tipe,
			}
		},
		columnDefs: [
					{"targets": [0,1], "visible": false },
					 {  className: "text-right", targets:[2,11] },
					 {  className: "text-center", targets:[3,4,5,6,7,9,10,12,13] },
					 {  className: "text-left", targets:[8] },
					 { "width": "3%", "targets": [2] },
					 { "width": "5%", "targets": [5,9] },
					 { "width": "8%", "targets": [3,4,6,7,10,11,12] },
					 { "width": "10%", "targets": [8,13] },

					]
		});
	}
	
	$(document).on("click", ".lunaskan", function() {
		var table = $('#index_list').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()
		
		swal({
			title: "Anda Yakin ?",
			text : "Akan Melunaskan ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			// pause_so();
			lunaskan(id);
		});
	});


	function lunaskan($id){
		console.log($id);
		var id=$id;		
		
		table = $('#index_list').DataTable()	
		$.ajax({
			url: '{site_url}tpiutang_rincian/lunaskan_detail_trx',
			type: 'POST',
			data: {id: id},
			complete: function(data) {
				console.log(data);
				$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi'});
				table.ajax.reload( null, false ); 
			}
		});
	}
	$(document).on("click", ".lunas_semua", function() {
		
		swal({
			title: "Anda Yakin ?",
			text : "Akan Melunaskan ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			// pause_so();
			lunaskansemua();
		});
	});
	function lunaskansemua(){
		var table = $('#index_list').DataTable();		
		 var data = table.rows().data();
		 var arr_id=[];
		 
		 data.each(function (value, index) {
				// alert('ID : '+table.cell(index,22).data());return false;
				console.log(table.cell(index,12).data());
			 if (table.cell(index,12).data()=='<span class="label label-danger">BELUM LUNAS</span>'){
				arr_id.push(table.cell(index,0).data()); 
				
			 }
			
		 });
		 if (arr_id){
			 $("#cover-spin").show();
			 // alert(arr_tanggal_jt);return false;
			$.ajax({
				url: '{site_url}tpiutang_rincian/lunaskansemua',
				type: 'POST',
				data: {
						arr_id: arr_id,
						
					 },
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Selesai Verifikasi'});
					$('#index_list').DataTable().ajax.reload()
					$("#cover-spin").hide();
				}
			});
		}
	}
</script>