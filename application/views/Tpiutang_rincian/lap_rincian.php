<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Print Piutang Karyawan All</title>
    <style>
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }
	  
      th {
        padding: 5px;
      }
	  td {
        padding: 5px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }
      
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;
		 
      }
	  td {
        padding: 3px;
		 
      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
    }
	
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <table class="content">
      <tr>
        <td rowspan="3" width="30%" class="text-center"><img src="{logo1_rs}" alt="" width="60" height="60"></td>
        <td rowspan="2" colspan="3" width="50%"  class="text-center text-bold text-judul"><u>REKAPITULASI TAGIHAN KARYAWAN</u></td>
		<td rowspan="2" width="20%"></td>		
      </tr>
	  <tr></tr>
	  <tr>
		<td width="14%" class="text-left text-bold text-top">NO TAGIHAN</td>
		<td width="20%" colspan="3" class="text-top">:  {no_piutang}</td>		
	  </tr>
	  <tr>
	    <td  class="text-center">{alamat_rs1}</td>
		<td  class="text-left text-bold text-top">DEKRIPISI</td>
		<td width="20%" colspan="3" class="text-top">: <?=($deskripsi)?></td>
	  </tr>
	  <tr>
	    <td  class="text-center"><?=$telepon_rs.' '.$fax_rs?></td>
		<td class="text-left text-bold text-top">TANGGAL TAGIHAN</td>
		<td class=" text-top" colspan="3" >: <?=HumanDateShort($tanggal_tagihan)?></td>
		
	  </tr>
	  <tr>
	    <td  class="text-center"></td>
		<td class="text-left text-bold text-top">STATUS TAGIHAN</td>
		<td class=" text-top" colspan="3" >: <?=($status_kirim==0?'BELUM DIPROSES':'SUDAH DIPROSES')?></td>
		
	  </tr>
    </table>
    
	<br>
    <table id="customers">
      <tr>
        <th width="4%" class="border-full text-center"><strong>NO</strong></th>
        <th width="10%" class="border-full text-center"><strong>TIPE </strong></th>
        <th width="30%" class="border-full text-center"><strong>PEGAWAI / DOKTER</strong></th>
        <th width="8%" class="border-full text-center"><strong>OBAT BEBAS </strong></th>
        <th width="8%" class="border-full text-center"><strong>BEROBAT</strong></th>
        <th width="8%" class="border-full text-center"><strong>LAIN-LAIN</strong></th>
        <th width="10%" class="border-full text-center"><strong>JUMLAH</strong></th>
        <th width="20%" class="border-full text-center"><strong>KETERANGAN</strong></th>
      </tr>
      <?php $number = 0; $total=0;$total_all=0;?>
      <?php foreach ($list_detail as $row){ ?>
        <?php 
		$number = $number + 1; 
		
		$total = $row->ob + $row->ri + $row->rj ;
		$total_all = $total_all + $total ;
		// $total=0;
		
		?>
        <tr>
          <td class="border-full text-right"> <?=$number?>&nbsp;&nbsp;</td>
		  <td class="border-full text-center">&nbsp;&nbsp;<?=strtoupper($row->nama_tipe)?></td>
          <td class="border-full text-left">&nbsp;&nbsp;<?=($row->nama)?></td>
		  <td class="border-full text-right"><?=number_format($row->ob,0)?>&nbsp;&nbsp;</td>
		  <td class="border-full text-right"><?=number_format($row->rj,0)?>&nbsp;&nbsp;</td>
		  <td class="border-full text-right"><?=number_format($row->ri,0)?>&nbsp;&nbsp;</td>
		  <td class="border-full text-right"><?=number_format($total,0)?>&nbsp;&nbsp;</td>
		  <?
		  $pembayaran_id_group=$row->pembayaran_id_group;
			// $q="SELECT GROUP_CONCAT(CASE WHEN D.cara_bayar='1' 
			// THEN CONCAT('LANGSUNG ',' @',FORMAT(D.nominal,0),' (',DATE_FORMAT(D.tanggal_tagihan,'%d-%m-%Y'),')') 
			// ELSE CONCAT('CICILAN ',D.cicilan_ke,'/',D.jml_cicilan,' @',FORMAT(D.nominal,0),' (',DATE_FORMAT(D.tanggal_tagihan,'%d-%m-%Y'),')') END SEPARATOR '<br>') as tmp 
			// FROM tpiutang_detail D
// WHERE D.pembayaran_id IN ($pembayaran_id_group) AND  D.tanggal_tagihan='".YMDFormat($row->tanggal_tagihan)."'
// ORDER BY D.pembayaran_id ASC,D.cicilan_ke DESC";
			$q="SELECT GROUP_CONCAT(CASE WHEN D.cara_bayar='1' 
			THEN CONCAT('LANGSUNG ',' @',FORMAT(D.nominal,0),' (',DATE_FORMAT(CASE WHEN D.tipe='1' THEN rj.tanggaldaftar WHEN D.tipe='3' THEN fr.tanggal WHEN D.tipe='2' THEN ri.tanggaldaftar END,'%d-%m-%Y'),')') 
			ELSE CONCAT('CICILAN ',D.cicilan_ke,'/',D.jml_cicilan,' @',FORMAT(D.nominal,0),' (',DATE_FORMAT(CASE WHEN D.tipe='1' THEN rj.tanggaldaftar WHEN D.tipe='3' THEN fr.tanggal WHEN D.tipe='2' THEN ri.tanggaldaftar END,'%d-%m-%Y'),')') END SEPARATOR '<br>') as tmp 
			FROM tpiutang_detail D
			LEFT JOIN tpoliklinik_pendaftaran rj ON rj.id=D.pendaftaran_id AND D.tipe='1'
			LEFT JOIN tpasien_penjualan fr ON fr.id=D.pendaftaran_id AND D.tipe='3'
			LEFT JOIN trawatinap_pendaftaran ri ON ri.id=D.pendaftaran_id AND D.tipe='2'
WHERE D.pembayaran_id IN ($pembayaran_id_group) AND  D.tanggal_tagihan='".YMDFormat($row->tanggal_tagihan)."'
ORDER BY D.pembayaran_id ASC,D.cicilan_ke DESC";
$ket=$this->db->query($q)->row('tmp');
		  // ?>
          <td class="border-full text-left"><?=$ket?></td>
        </tr>
      <? } ?>
		<tr>
			<td colspan="6"  class="border-full text-center text-bold">TOTAL</td>
			<td  class="border-full text-right text-bold"><?=number_format($total_all,0)?>&nbsp;&nbsp;</td>
			<td  class="border-full text-center text-bold"></td>
			
		</tr>
    </table><br>
    <table>
		<tr>
			<td width="80%"><p class="text-muted text-italic">Tanggal Cetak <?=date('d-m-Y H:i:s').' | '.$this->session->userdata('user_name')?>.</p>.</td>
			<td width="20%" class="text-center">Kepala Bagian Keuangan <br><br><br><br></td>
		</tr>
		<tr><td width="80%" class="text-center"></td><td width="20%"></td></tr>
		<tr><td width="80%" class="text-center"></td><td width="20%"></td></tr>
		<tr><td width="80%" class="text-center"></td><td width="20%"></td></tr>
		<tr><td width="80%" class="text-center"></td><td width="20%"></td></tr>
		<tr><td width="80%" class="text-center"></td><td width="20%"></td></tr>
		<tr>
			<td width="80%"></td>
			<td width="20%" class="text-center">( _______________________________ )</td>
		</tr>
	</table>
	
  </body>
</html>
