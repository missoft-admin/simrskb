<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work-1"'); ?>
                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-3 control-label">Nomor Tagihan</label>
                        <div class="col-md-9">
                            <input class="form-control" value="{no_piutang}" disabled>
                            <input class="form-control" type="hidden" name="piutang_id" id="piutang_id" value="{id}" >
							<input class="form-control" type="hidden" name="idpengaturan" id="idpengaturan" value="{idpengaturan}" >
                            <input class="form-control" type="hidden" name="tanggal_id" id="tanggal_id" value="{tanggal_id}" >
                            <input class="form-control" type="hidden" name="disabel" id="disabel" value="{disabel}" >
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 15px;">
                        <label class="col-md-3 control-label">Deskripsi</label>
                        <div class="col-md-9">
                            <input class="form-control" value="{deskripsi}" disabled>
                        </div>
                    </div>

                </div>
				<div class="col-md-6" style="margin-bottom: 5px;">

                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-3 control-label">Tanggal Tagihan</label>
                        <div class="col-md-4">
                            <input class="form-control" value="<?=HumanDateShort($tanggal_tagihan)?>" disabled>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-3 control-label">Status Tagihan</label>
                        <div class="col-md-4">
                            <input class="form-control" value="<?=($status_kirim==0?'BELUM DIPROSES':'SUDAH DIPROSES')?>" disabled>
                        </div>
                    </div>

                </div>
            <?php echo form_close(); ?>
        </div>

        <div class="form-group" style="margin-top: 20px;"></div>
    </div>
</div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">DETAIL TAGIHAN</h3>
    </div>
    <div class="block-content">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Jenis Tagihan</label>
					<div class="col-md-8">
						<select id="xtipepegawai" name="xtipepegawai" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>-Semua Jenis-</option>
							<option value="1">Tagihan Karyawan</option>
							<option value="2">Tagihan Dokter</option>
						</select>
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Dokter / Karyawan</label>
					<div class="col-md-8">
						<select id="xidpeg_dok" name="xidpeg_dok" class="form-control" style="width: 100%;" data-placeholder="Choose one..">

						</select>
					</div>
                </div>


            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Status</label>
                    <div class="col-md-8">
                        <select id="status" name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua Status -</option>
							<option value="0" >BELUM LUNAS</option>
							<option value="1">LUNAS</option>

						</select>
                    </div>
                </div>

                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
			</div>
            <?php echo form_close(); ?>
        </div>
        <div class="form-group" style="margin-bottom: 30px;"></div>
		<div class="table-responsive">
        <table class="table table-bordered table-striped table-responsive" id="index_list">
            <thead>
                <tr>
                    <th>#</th>
                    <th>#</th>
                    <th>#</th>
                    <th>JENIS</th>
                    <th>ATAS NAMA</th>
                    <th>NOMINAL</th>
                    <th>JUMLAH TRX</th>
                    <th>STATUS</th>
                    <th>AKSI  <div class="btn-group" role="group">
							<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu dropdown-menu-right">
								<li class="dropdown-header">Pilih Action</li>
								<li>
									<a tabindex="-1" class="lunas_semua" href="javascript:void(0)">Lunaskan Semua</a>
								</li>

							</ul>
						</div>
					</th>

                </tr>
            </thead>
            <tbody> </tbody>

        </table>
    </div>
        <div class="form-group" style="margin-top: 20px;"></div>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">


    var table;

    $(document).ready(function(){
			// alert($("#xidkelompokpasien").val());

		load_detail();
		$("#xidpeg_dok").select2({
			minimumInputLength: 2,
			noResults: 'Dokter Tidak Ditemukan.',
			ajax: {
				url: '{site_url}tpiutang_verifikasi/js_pegawai_dokter/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,tipe:$("#xtipepegawai").val()
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.nama ,
								id: item.id
							}
						})
					};
				}
			}
		});
    });
	$(document).on("change", "#xtipepegawai", function() {
		$("#xidpeg_dok").val(null).trigger('change');
	});
    function load_detail() {
		var piutang_id=$("#piutang_id").val();
		var xtipepegawai=$("#xtipepegawai").val();
		var xidpeg_dok=$("#xidpeg_dok").val();
		var disabel=$("#disabel").val();
		var status=$("#status").val();
		// alert(tgl_trx);
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tpiutang_rincian/load_detail/',
			type: "POST",
			dataType: 'json',
			data: {
				piutang_id: piutang_id,
				xtipepegawai: xtipepegawai,
				xidpeg_dok: xidpeg_dok,
				status:status,
				disabel:disabel,
			}
		},
		columnDefs: [
					{"targets": [0,1], "visible": false },
					 {  className: "text-right", targets:[2,5] },
					 {  className: "text-center", targets:[3,6,7] },
					 {  className: "text-left", targets:[4] },
					 { "width": "5%", "targets": [2] },
					 { "width": "8%", "targets": [3,5,6,7] },
					 { "width": "10%", "targets": [4] },
					 { "width": "15%", "targets": [8] }

					]
		});
	}

	$("#btn_filter").click(function() {
		load_detail();
	});

	$(document).on("click", ".lunaskan", function() {
		var table = $('#index_list').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()

		swal({
			title: "Anda Yakin ?",
			text : "Akan Melunaskan ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			// pause_so();
			lunaskan(id);
		});
	});



	function lunaskan($id){
		console.log($id);
		var id=$id;

		table = $('#index_list').DataTable()
		$.ajax({
			url: '{site_url}tpiutang_rincian/lunaskan_detail_trx',
			type: 'POST',
			data: {id: id},
			complete: function(data) {
				console.log(data);
				$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi'});
				table.ajax.reload( null, false );
			}
		});
	}
	$(document).on("click", ".lunas_semua", function() {

		swal({
			title: "Anda Yakin ?",
			text : "Akan Melunaskan ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			// pause_so();
			lunaskansemua();
		});
	});
	function lunaskansemua(){
		var table = $('#index_list').DataTable();
		 var data = table.rows().data();
		 var arr_id=[];

		 data.each(function (value, index) {
				// alert('ID : '+table.cell(index,22).data());return false;
				console.log(table.cell(index,6).data());
				console.log(table.cell(index,7).data());
			 if (table.cell(index,6).data()=='1' && table.cell(index,7).data() =='<span class="label label-danger">BELUM LUNAS</span>'){
				arr_id.push(table.cell(index,0).data());

			 }

		 });
		 if (arr_id){
			 $("#cover-spin").show();
			 // alert(arr_tanggal_jt);return false;
			$.ajax({
				url: '{site_url}tpiutang_rincian/lunaskansemua',
				type: 'POST',
				data: {
						arr_id: arr_id,

					 },
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Selesai Verifikasi'});
					$('#index_list').DataTable().ajax.reload()
					$("#cover-spin").hide();
				}
			});
		}
	}
</script>
