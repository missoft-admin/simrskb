<style>
table.fixed {
      table-layout: fixed;
      width: 100%;
    }
    table.fixed td {
      overflow: hidden;
    }
</style>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tvalidasi_pendapatan_ranap/save_edit_obat_ko','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
<div class="block">
	<div class="block-header">
		
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
		<input type="hidden" readonly class="form-control"  name="idvalidasi" value="{idvalidasi}">
		<input type="hidden" readonly class="form-control"  name="idtipe" value="{idtipe}">
		<input type="hidden" readonly class="form-control"  name="jenis" value="{jenis}">
        <div class="row">
            <div class="col-md-6">
				 <div class="input-group"  style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No Pendaftaran</span>
                    <input type="text" class="form-control tindakanAlamatPasien" value="{notransaksi}" readonly="true">
                </div>
				 <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Tgl Pendaftaran</span>
                    <input type="text" class="form-control tindakanNoHandphone" value="<?=HumanDateShort($tanggal_pendaftaran)?>" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No. Medrec</span>
                    <input type="text" class="form-control tindakanNoMedrec" value="{no_medrec}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Nama Pasien</span>
                    <input type="text" class="form-control tindakanNamaPasien" value="{namapasien}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Dokter</span>
                    <input type="text" class="form-control tindakanNamaDokter" value="{dokter}" readonly="true">
                </div>
               
                             
            </div>
            <div class="col-md-6">
                <div class="input-group" style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Kelompok Pasien</span>
                    <input type="text" class="form-control tindakanKelompokPasien" value="{namakelompok}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Nama Perusahaan</span>
                    <input type="text" class="form-control tindakanNamaPerusahaan" value="{namarekanan}" readonly="true">
                </div>
                <hr>
                
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Kelas Perawatan</span>
                    <input type="text" class="form-control tindakanNamaKelas" value="{kelas}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Bed</span>
                    <input type="text" class="form-control tindakanNamaBed" value="{bed}" readonly="true">
                </div>
                <hr>
                
            </div>
        </div>
	</div>
	
	<div class="block-content">
		<h3 class="block-title"> <?=text_default(GetTipeKategoriBarang($idtipe))?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped" id="index_hasil" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:15%"  class="text-center">ITEM BIAYA</th>
								<th style="width:8%"  class="text-center">DISKON Rp.</th>
								<th style="width:10%"  class="text-center">TOTAL</th>
								<th style="width:25%"  class="text-center">GROUP JUAL</th>
								<th style="width:25%"  class="text-center">GROUP DISKON</th>
								<th style="display: none">ID</th>
							</tr>
						</thead>
						<tbody>
							<? foreach($list_obat as $r){?>
							<tr>
								<td class="text-left"><?=$r->namabarang?></td>
								<td class="text-right"><?=number_format($r->diskon_rp)?></td>
								<td class="text-right"><?=number_format($r->totalkeseluruhan)?></td>
								
								<td>
									<select name="idgroup_penjualan[]"  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="0" <?=($r->idgroup_penjualan==null?'selected':'')?>>Belum dipilih</option>
									<?foreach($list_group as $row){?>
										<option value="<?=$row->id?>" <?=($r->idgroup_penjualan==$row->id?'selected':'')?>><?=$row->nama?></option>
									<?}?>
									</select>
								</td>
								<td>
									<select  name="idgroup_diskon[]" <?=($r->diskon_rp>0?'':'disabled')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($r->idgroup_diskon==null?'selected':'')?>>Belum dipilih</option>
										<?foreach($list_group as $row){?>
											<option value="<?=$row->id?>" <?=($r->idgroup_diskon==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</td>
								<td style="display: none"><input type="text" readonly class="form-control" id="iddet_obat"  name="iddet_obat[]" value="<?=$r->id?>"></td>
							</tr>
							<?}?>
						</tbody>
					</table>
			</div>
		</div>
	</div>
	
	
	<div class="block-content">
		 <hr style="margin-top:10px">
		 <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<div class="col-xs-12 text-right">
						<a href="{base_url}tvalidasi_pendapatan_ranap/detail/<?=$idvalidasi?>" class="btn btn-default" type="reset"><i class="fa fa-reply"></i> Kembali</a>
						<?if ($disabel==''){?>
						<button class="btn btn-primary" name="btn_simpan" type="submit" value="1"><i class="fa fa-save"></i> Update</button>
						<?}?>
					</div>
				</div>
				
            </div>
        </div>
		<hr style="margin-bottom:10px">		
		
	</div>
</div>
<?php echo form_close(); ?>


<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
  
});


function validate_final(){
	$("#cover-spin").show();
	$("*[disabled]").not(true).removeAttr("disabled");
	return true;
}

</script>