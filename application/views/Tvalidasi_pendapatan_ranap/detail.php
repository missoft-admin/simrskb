<?=ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}
?>

<style media="screen">
.text-bold {
    font-weight: bold;
}

.separator-header {
    margin-top: 450px;
}

.modal {
    overflow: auto !important;
}

</style>
<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">        
        <h3 class="block-title">{title} </h3>
    </div>
				<input type="hidden" readonly class="form-control"  id="idvalidasi" name="idvalidasi" value="{idvalidasi}">

    <div class="block-content">
        <div class="row">
            <div class="col-md-6">
				 <div class="input-group"  style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No Pendaftaran</span>
                    <input type="text" class="form-control tindakanAlamatPasien" value="{notransaksi}" readonly="true">
                </div>
				 <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Tgl Pendaftaran</span>
                    <input type="text" class="form-control tindakanNoHandphone" value="<?=HumanDateShort($tanggal_pendaftaran)?>" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No. Medrec</span>
                    <input type="text" class="form-control tindakanNoMedrec" value="{no_medrec}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Nama Pasien</span>
                    <input type="text" class="form-control tindakanNamaPasien" value="{namapasien}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Dokter</span>
                    <input type="text" class="form-control tindakanNamaDokter" value="{dokter}" readonly="true">
                </div>
               
                             
            </div>
            <div class="col-md-6">
                <div class="input-group" style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Kelompok Pasien</span>
                    <input type="text" class="form-control tindakanKelompokPasien" value="{namakelompok}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Nama Perusahaan</span>
                    <input type="text" class="form-control tindakanNamaPerusahaan" value="{namarekanan}" readonly="true">
                </div>
                <hr>
                <?php if ($idtipe == 1) {?>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Kelas Perawatan</span>
                    <input type="text" class="form-control tindakanNamaKelas" value="{kelas}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Bed</span>
                    <input type="text" class="form-control tindakanNamaBed" value="{bed}" readonly="true">
                </div>
                <hr>
                <?php } ?>
            </div>
        </div>
		<?if ($list_null){?>
		<div class="block-content">
			<hr style="margin-top:10px">
			<div class="row">			
				<div class="col-md-12">
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h3 class="font-w300 push-15"><strong>Ada <?=count($list_null)?> Akun yang belum ditentukan</strong></h3>
						<?foreach($list_null as $r){?>
						<p><?=$r->keterangan?></p>
						
						<?}?>
					</div>
				</div>
			</div>
		</div>
		<?}?>
        <div class="content-verification">
			<div class="row">
				<hr>
				<?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
				<div class="col-md-6">
					
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label" for="status">Group Diskon</label>
						<div class="col-md-8">
							<select name="idgroup_diskon" id="idgroup_diskon" <?=($nominal_diskon>0?'':'disabled')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" <?=($idgroup_diskon==null?'selected':'')?>>Belum dipilih</option>
							<?foreach($list_group as $row){?>
								<option value="<?=$row->id?>" <?=($idgroup_diskon==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label" for="tanggal">Nominal Diskon</label>
						<div class="col-md-8">
							<input type="text" class="form-control number" value="{nominal_diskon}" readonly="true">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label" for="status">Group Round</label>
						<div class="col-md-8">
							<select name="idgroup_round" id="idgroup_round" <?=($nominal_round>0?'':'disabled')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" <?=($idgroup_round==null?'selected':'')?>>Belum dipilih</option>
							<?foreach($list_group as $row){?>
								<option value="<?=$row->id?>" <?=($idgroup_round==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label" for="tanggal">Nominal Diskon</label>
						<div class="col-md-8">
							<input type="text" class="form-control number" value="{nominal_round}" readonly="true">
						</div>
					</div>
					
					
				</div>
				<div class="col-md-6">
					
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label" for="tanggal">Group Gabungan</label>
						<div class="col-md-8">
							<select name="idgroup_gabungan" id="idgroup_gabungan" <?=($st_gabungan=='0'?'disabled':'')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" <?=($idgroup_gabungan==null?'selected':'')?>>Belum dipilih</option>
								<?foreach($list_group as $row){?>
									<option value="<?=$row->id?>" <?=($idgroup_gabungan==$row->id?'selected':'')?>><?=$row->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Nominal Gabungan</label>
						<div class="col-md-8">
							<input type="text" class="form-control number" value="<?=($st_gabungan=='1'?$nominal_gabungan:0)?>" readonly="true">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label" for=""></label>
						<div class="col-md-8">
							<button class="btn btn-success text-uppercase" type="button" name="btn_simpan" id="btn_simpan" style="font-size:13px;width:100%;float:right;"><i class="fa fa-save"></i> Update</button>
						</div>
					</div>
					
				</div>
				
				<?php echo form_close(); ?>
			</div>
				<hr style="margin-top:10px">
		</div>
		<?if ($st_gabungan=='0'){?>
        <div class="content-verification">
			
            <hr>

            <?php $totalSemua = 0; ?>
            <?php $totalRanapRuangan = 0; ?>
            <?php if ($idtipe == 1) {?>
            <b><span class="label label-success" style="font-size:12px">RUANGAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                        <th style="width:20%">Ruangan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Hari</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th><th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody id="tempRuanganRanap">
                    <?php foreach ($this->model->getTindakanRanap($idvalidasi,22) as $row) {?>
                    <?php
					?>
                    <tr>
                        <td><?=$notransaksi?></td>
                        <td><?=$row->namatarif?></td>                       
                        <td><?=number_format($row->total)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, ($row->total * $row->kuantitas)); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php $totalRanapRuangan = $totalRanapRuangan + $row->totalkeseluruhan; ?>
                    <?php $totalSemua = $totalSemua + $row->totalkeseluruhan; ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="3"><?=number_format($totalRanapRuangan)?></td>
                    </tr>
                </tfoot>
            </table>
            <?php } ?>
            <hr>

			
            <b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">FULL CARE</span></b>
            <table id="historyTindakan" class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th><th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRanapFullCare = 0; ?>
                    <?php $totalRanapFullCareVerif = 0; ?>
                    <?php foreach ($this->model->getTindakanRanap($idvalidasi,4) as $row) {?>
                    
                    <tr>
                        <td><?=$notransaksi?></td>
                        <td><?=$row->namatarif?></td>                        
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td hidden><?=$row->iddokter?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td>
                            <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    				
					<?php 
						$totalRanapFullCare = $totalRanapFullCare + $row->totalkeseluruhan;
						$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					} ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRanapFullCare)?></td>
                    </tr>
                </tfoot>
            </table>
			
            <hr>
			
            <b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">ECG</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRanapECG = 0; ?>
                    <?php $totalRanapECGVerif = 0; ?>
                     <?php foreach ($this->model->getTindakanRanap($idvalidasi,5) as $row) {?>
                   
                    <tr>
                        <td><?=$notransaksi?></td>               
                        
                        <td><?=$row->namatarif?></td>                        
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRanapECG = $totalRanapECG + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRanapECG)?></td>
                    </tr>
                </tfoot>
            </table>
			
            <hr>
			
            <b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">VISITE DOKTER</span></b>
            <table id="historyVisiteDokter" class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>                       
                        <th style="width:35%">Tindakan</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th><th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRanapVisite = 0; ?>
                    <?php $totalRanapVisiteVerif = 0; ?>
                   <?php foreach ($this->model->getTindakanRanap($idvalidasi,18) as $row) {?>
                  
                    <tr>
                        <td><?=$notransaksi?></td>                        
                        <td><?=$row->namatarif?></td>                        
                        <td hidden><?=number_format($row->subtotal)?></td>
                        <td hidden><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRanapVisite = $totalRanapVisite + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="4"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRanapVisite)?></td>
                    </tr>
                </tfoot>
            </table>
           
            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">SEWA ALAT</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRanapSewaAlat = 0; ?>
                    <?php $totalRanapSewaAlatVerif = 0; ?>
                    <?php foreach ($this->model->getTindakanRanap($idvalidasi,7) as $row) {?>
                   
                    <tr>
                        <td><?=$notransaksi?></td>
                        <td><?=$row->namatarif?></td>                        
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRanapSewaAlat = $totalRanapSewaAlat + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRanapSewaAlat)?></td>
                    </tr>
                </tfoot>
            </table>
			
            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">AMBULANCE</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRanapAmbulance = 0; ?>
                    <?php $totalRanapAmbulanceVerif = 0; ?>
                    <?php foreach ($this->model->getTindakanRanap($idvalidasi, 6) as $row) {?>
                   
                    <tr>
                        <td><?=$notransaksi?></td>
						<td><?=$row->namatarif?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRanapAmbulance = $totalRanapAmbulance + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRanapAmbulance)?></td>
                    </tr>
                </tfoot>
            </table>
			
            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">OBAT</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>                       
                        <th style="width:20%">Obat</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRanapObat = 0; ?>
                    <?php  ?>
                    <?php foreach ($this->model->getObatAlkesRanap($idvalidasi,'3') as $row) {?>
                  
                    <tr>
                        <td><?=($row->tipe_farmasi=='1'?$row->nopenjualan : $notransaksi)?></td>
                        
                        <td><?=$row->namabarang?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_obat_ranap/<?=$idvalidasi?>/<?=$row->idtipe?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRanapObat = $totalRanapObat + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>

                   
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRanapObat)?></td>
                    </tr>
                </tfoot>
            </table>
			
            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                       
                        <th style="width:20%">Alat Kesehatan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRanapAlkes = 0;$asal='2,5'; ?>
                    <?php foreach ($this->model->getObatAlkesRanap($idvalidasi,'1',$asal) as $row) {?>
                    <?php
					
					
					?>
                     <tr>
                        <td><?=($row->tipe_farmasi=='1'?$row->nopenjualan : $notransaksi)?></td>
                        
                        <td><?=$row->namabarang?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                          <a href="{base_url}tvalidasi_pendapatan_ranap/edit_obat_ranap/<?=$idvalidasi?>/<?=$row->idtipe?>/2_5" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRanapAlkes = $totalRanapAlkes + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>

                   
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRanapAlkes)?></td>
                    </tr>
                </tfoot>
            </table>
			
            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
            <b><span class="label label-default" style="font-size:12px">ALAT BANTU</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>                       
                        <th style="width:20%">Alat Kesehatan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRanapAlkesBantu = 0; ?>
                    <?php $asal='3'; ?>
                     <?php foreach ($this->model->getObatAlkesRanap($idvalidasi,'1',$asal) as $row) {?>
                    <?php
					
					
					?>
                     <tr>
                        <td><?=($row->tipe_farmasi=='1'?$row->nopenjualan : $notransaksi)?></td>
                        
                        <td><?=$row->namabarang?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                          <a href="{base_url}tvalidasi_pendapatan_ranap/edit_obat_ranap/<?=$idvalidasi?>/<?=$row->idtipe?>/3" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRanapAlkesBantu = $totalRanapAlkesBantu + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>

                  
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRanapAlkesBantu)?></td>
                    </tr>
                </tfoot>
            </table>
			
            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RAWAT INAP</span></b>
            <b><span class="label label-default" style="font-size:12px">LAIN-LAIN</span></b>
            <table id="historyTindakan" class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRanapLainLain = 0; ?>
                    <?php foreach ($this->model->getTindakanRanap($idvalidasi, 8) as $row) {?>
                    <?php
										
					?>
                    <tr>
                        <td><?=$notransaksi?></td>
                        
                        
                        <td><?=$row->namatarif?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
							<a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRanapLainLain = $totalRanapLainLain + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRanapLainLain)?></td>
                    </tr>
                </tfoot>
            </table>
			
            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN IGD</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalPoli = 0; ?>
                    <?php $totalPoliVerif = 0; ?>
                    <?php foreach ($this->model->getTindakanRanap($idvalidasi, 20) as $row) {?>
                    <?php
										
					?>
                    <tr>
                        <td><?=$notransaksi?></td>
                        <td><?=$row->namatarif?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalPoli = $totalPoli + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalPoli)?></td>
                    </tr>
                </tfoot>
            </table>
	
            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN IGD</span></b>
            <b><span class="label label-default" style="font-size:12px">OBAT</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>                       
                        <th style="width:10%">Obat</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalPoliObat = 0; ?>
                    <?php foreach ($this->model->getObatAlkesIGD($idvalidasi, 3) as $row) {?>
                    <?php
					
					
					?>
                    <tr>
                        <td><?=($row->tipe_farmasi=='1'?$row->nopenjualan : $notransaksi)?></td>
                        
                        <td><?=$row->namabarang?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                          <a href="{base_url}tvalidasi_pendapatan_ranap/edit_obat_igd/<?=$idvalidasi?>/<?=$row->idtipe?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalPoliObat = $totalPoliObat + $row->totalkeseluruhan;
				$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>

                   
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalPoliObat)?></td>
                    </tr>
                </tfoot>
            </table>
            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN IGD</span></b>
            <b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>                       
                        <th style="width:10%">Alat Kesehatan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalPoliAlkes = 0; ?>
                    <?php foreach ($this->model->getObatAlkesIGD($idvalidasi, 1) as $row) {?>
                    <?php
					
					
					?>
					<tr>
                        <td><?=($row->tipe_farmasi=='1'?$row->nopenjualan : $notransaksi)?></td>                        
                        <td><?=$row->namabarang?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                          <a href="{base_url}tvalidasi_pendapatan_ranap/edit_obat_igd/<?=$idvalidasi?>/<?=$row->idtipe?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalPoliAlkes = $totalPoliAlkes + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>

                   
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalPoliAlkes)?></td>
                    </tr>
                </tfoot>
            </table>


            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN LABORATORIUM</span></b>
            <b><span class="label label-default" style="font-size:12px">UMUM</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Rujukan</th>
                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalLab = 0; ?>
                    <?php $totalLabVerif = 0; ?>
                    <?php foreach ($this->model->getTindakanRanap($idvalidasi, 9) as $row) {?>
                    <?php
					
					
					?>
                    <tr>
                        <td><?=$notransaksi?></td>
						<td><?=$row->namatarif?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalLab = $totalLab + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalLab)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN LABORATORIUM</span></b>
            <b><span class="label label-default" style="font-size:12px">PATHOLOGI ANATOMI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Rujukan</th>
                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalLabPA = 0; ?>
                    <?php $totalLabPAVerif = 0; ?>
                    <?php foreach ($this->model->getTindakanRanap($idvalidasi, 10) as $row) {?>
                    <?php
					
					
					?>
                   <tr>
                        <td><?=$notransaksi?></td>
						<td><?=$row->namatarif?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalLabPA = $totalLabPA + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalLabPA)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN LABORATORIUM</span></b>
            <b><span class="label label-default" style="font-size:12px">PMI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Rujukan</th>
                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalLabPMI = 0; ?>
                    <?php $totalLabPMIVerif = 0; ?>
                    <?php foreach ($this->model->getTindakanRanap($idvalidasi, 11) as $row) {?>
                    <?php
					
					
					?>
                   <tr>
                        <td><?=$notransaksi?></td>
						<td><?=$row->namatarif?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalLabPMI = $totalLabPMI + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalLabPMI)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
            <b><span class="label label-default" style="font-size:12px">X-RAY</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Rujukan</th>
                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRadXray = 0; ?>
                    <?php $totalRadXrayVerif = 0; ?>
                     <?php foreach ($this->model->getTindakanRanap($idvalidasi, 12) as $row) {?>
                    <?php
					
					
					?>
                    <tr>
                        <td><?=$notransaksi?></td>
						<td><?=$row->namatarif?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRadXray = $totalRadXray + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRadXray)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
            <b><span class="label label-default" style="font-size:12px">USG</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Rujukan</th>
                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRadUSG = 0; ?>
                    <?php $totalRadUSGVerif = 0; ?>
                    <?php foreach ($this->model->getTindakanRanap($idvalidasi, 13) as $row) {?>
                    <?php
					
					
					?>
                    <tr>
                        <td><?=$notransaksi?></td>
						<td><?=$row->namatarif?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRadUSG = $totalRadUSG + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRadUSG)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
            <b><span class="label label-default" style="font-size:12px">CT SCAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Rujukan</th>
                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRadCTScan = 0; ?>
                    <?php $totalRadCTScanVerif = 0; ?>
                    <?php foreach ($this->model->getTindakanRanap($idvalidasi, 14) as $row) {?>
                    <?php
					
					
					?>
                   <tr>
                        <td><?=$notransaksi?></td>
						<td><?=$row->namatarif?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRadCTScan = $totalRadCTScan + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRadCTScan)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
            <b><span class="label label-default" style="font-size:12px">MRI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Rujukan</th>
                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRadMRI = 0; ?>
                    <?php $totalRadMRIVerif = 0; ?>
                    <?php foreach ($this->model->getTindakanRanap($idvalidasi, 15) as $row) {?>
                    <?php
					
					
					?>
                    <tr>
                        <td><?=$notransaksi?></td>
						<td><?=$row->namatarif?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRadMRI = $totalRadMRI + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRadMRI)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
            <b><span class="label label-default" style="font-size:12px">BMD</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Rujukan</th>
                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalRadBMD = 0; ?>
                    <?php $totalRadBMDVerif = 0; ?>
                    <?php foreach ($this->model->getTindakanRanap($idvalidasi, 16) as $row) {?>
                    <?php
					
					
					?>
                    <tr>
                        <td><?=$notransaksi?></td>
						<td><?=$row->namatarif?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalRadBMD = $totalRadBMD + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalRadBMD)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN FISIOTERAPI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Rujukan</th>
                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalFisio = 0; ?>
                    <?php $totalFisioVerif = 0; ?>
                    <?php foreach ($this->model->getTindakanRanap($idvalidasi, 17) as $row) {?>
                    <?php
					
					
					?>
                   <tr>
                        <td><?=$notransaksi?></td>
						<td><?=$row->namatarif?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalFisio = $totalFisio + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalFisio)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
            <b><span class="label label-default" style="font-size:12px">SEWA ALAT</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalOKSewaAlat = 0; ?>
                    <?php $totalOKSewaAlatVerif = 0; ?>
                    <?php foreach ($this->model->getTindakanRanap($idvalidasi, 3) as $row) {?>
                    <?php
					
					?>
                    <tr>
                        <td><?=$notransaksi?></td>
						<td><?=$row->namatarif?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalOKSewaAlat = $totalOKSewaAlat + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalOKSewaAlat)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
            <b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>                       
                        <th style="width:20%">Alat Kesehatan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalOKAlkes = 0; ?>
                    <?php $totalOKAlkesVerif = 0; ?>
                    <?php foreach ($this->model->getObatKO($idvalidasi,1,3) as $row) {?>
                    <?php
					
					
					?>
                    <tr>
                        <td><?=$notransaksi?></td>
                        <td><?=$row->namabarang?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <a href="{base_url}tvalidasi_pendapatan_ranap/edit_obat_ko/<?=$idvalidasi?>/<?=$row->idtipe?>/<?=$row->jenis?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalOKAlkes = $totalOKAlkes + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalOKAlkes)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
            <b><span class="label label-default" style="font-size:12px">OBAT</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                       
                        <th style="width:20%">Obat</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalOKObat = 0; ?>
                    <?php $totalOKObatVerif = 0; ?>
                    <?php foreach ($this->model->getObatKO($idvalidasi,3,2) as $row) {?>
                    <?php
					
					
					?>
                    <tr>
                        <td><?=$notransaksi?></td>
                        <td><?=$row->namabarang?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <a href="{base_url}tvalidasi_pendapatan_ranap/edit_obat_ko/<?=$idvalidasi?>/<?=$row->idtipe?>/<?=$row->jenis?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalOKObat = $totalOKObat + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalOKObat)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
            <b><span class="label label-default" style="font-size:12px">OBAT NARCOSE</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                       
                        <th style="width:20%">Obat</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalOKObatNarcose = 0; ?>
                    <?php $totalOKObatNarcoseVerif = 0; ?>
                    <?php foreach ($this->model->getObatKO($idvalidasi,3,1) as $row) {?>
                    <?php
					
					
					?>
                    <tr>
                        <td><?=$notransaksi?></td>
                        <td><?=$row->namabarang?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <a href="{base_url}tvalidasi_pendapatan_ranap/edit_obat_ko/<?=$idvalidasi?>/<?=$row->idtipe?>/<?=$row->jenis?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalOKObatNarcose = $totalOKObatNarcose + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalOKObatNarcose)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
            <b><span class="label label-default" style="font-size:12px">IMPLAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                       
                        <th style="width:20%">Implan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalOKImplan = 0; ?>
                    <?php $totalOKImplanVerif = 0; ?>
                    <?php foreach ($this->model->getObatKO($idvalidasi,2,4) as $row) {?>
                    <?php
					
					
					?>
                    <tr>
                        <td><?=$notransaksi?></td>
                        <td><?=$row->namabarang?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                            <a href="{base_url}tvalidasi_pendapatan_ranap/edit_obat_ko/<?=$idvalidasi?>/<?=$row->idtipe?>/<?=$row->jenis?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalOKImplan = $totalOKImplan + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalOKImplan)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
            <b><span class="label label-default" style="font-size:12px">SEWA KAMAR</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:20%">Jumlah Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalOKSewaKamar = 0; ?>
                    <?php $totalOKSewaKamarVerif = 0; ?>
                    <?php foreach ($this->model->getTindakanRanap($idvalidasi,2) as $row) {?>
                    <?php
					
					
					?>
                    <tr>
                        <td><?=$notransaksi?></td>
						<td><?=$row->namatarif?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalOKSewaKamar = $totalOKSewaKamar + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan; 
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="6"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalOKSewaKamar)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
            <b><span class="label label-default" style="font-size:12px">JASA DOKTER OPERATOR</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>                       
                        <th style="width:10%">Tindakan</th>
                        <th style="width:20%">Nama Dokter</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th><th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalOKJasaDokterOperator = 0; ?>
                    <?php $totalOKJasaDokterOperatorVerif = 0; ?>
                    <?php foreach ($this->model->getTindakanRanap($idvalidasi,19) as $row) {?>
                    <?php
					
					
					?>
                    <tr>
                        <td><?=$notransaksi?></td>
						<td><?=$row->namatarif?></td>
						<td><?=$row->nama_dokter?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->subtotal- $row->diskon_rp)?></td>
                        
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalOKJasaDokterOperator = $totalOKJasaDokterOperator + ($row->subtotal- $row->diskon_rp);
					$totalSemua = $totalSemua + ($row->subtotal- $row->diskon_rp);
					
					
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalOKJasaDokterOperator)?></td>
                    </tr>
                </tfoot>
            </table>
            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
            <b><span class="label label-default" style="font-size:12px">JASA DOKTER ANESTHESI</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>                       
                        <th style="width:15%">Tindakan</th>
                        <th style="width:20%">Nama Dokter</th>
                        <th style="width:10%">Nominal Acuan</th>
                        <th style="width:7%">Persentase (%)</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:8%">Diskon (%)</th>
                        <th style="width:10%">Total Tarif</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalOKJasaDokterAnesthesi = 0; ?>
                    <?php $totalOKJasaDokterAnesthesiVerif = 0; ?>
                    <?php foreach ($this->model->getDokterAnastesi($idvalidasi) as $row) {?>
                    <td><?=$notransaksi?></td>
						<td><?=$row->namatarif?></td>
						<td><?=$row->nama_dokter?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->persen)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_dokter_anesthesi/<?=$idvalidasi?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalOKJasaDokterAnesthesi = $totalOKJasaDokterAnesthesi + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan;
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalOKJasaDokterAnesthesi)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN OPERASI</span></b>
            <b><span class="label label-default" style="font-size:12px">JASA ASISTEN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>                       
                        <th style="width:15%">Tindakan</th>
                        <th style="width:20%">Nama Dokter</th>
                        <th style="width:10%">Nominal Acuan</th>
                        <th style="width:7%">Persentase (%)</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:8%">Diskon (%)</th>
                        <th style="width:10%">Total Tarif</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalOKJasaAsisten = 0; ?>
                    <?php $totalOKJasaAsistenVerif = 0; ?>
                    <?php foreach ($this->model->getDokterAsisten($idvalidasi) as $row) {?>
                    <tr>
                        <td><?=$notransaksi?></td>
                        <td><?=$row->namatarif?></td>
                        <td><?=$row->nama_dokter?></td>
                        <td><?=number_format($row->subtotal)?></td>
                        <td><?=number_format($row->persen)?></td>
                        <td><?=number_format($row->total)?></td>
                        <td hidden><?=number_format($row->diskon_rp, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon_rp, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_dokter_asisten/<?=$idvalidasi?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php
					$totalOKJasaAsisten = $totalOKJasaAsisten + $row->totalkeseluruhan;
					$totalSemua = $totalSemua + $row->totalkeseluruhan;
					?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalOKJasaAsisten)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">ADMINISTRASI</span></b>
            <b><span class="label label-default" style="font-size:12px">RAWAT JALAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:10%">Diskon (%)</th>
                        <th style="width:20%">Tarif Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalAdmRajal = 0; ?>
                    <?php foreach ($this->model->getTindakanRanap($idvalidasi,21) as $row) {?>
                    <?php
					
					
					?>
                    <tr>
                        <td><?=$notransaksi?></td>
                        <td><?=$row->namatarif?></td>
                        <td><?=number_format($row->total)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_tindakan/<?=$idvalidasi?>/<?=$row->id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php $totalAdmRajal = $totalAdmRajal + $row->totalkeseluruhan; $totalSemua = $totalSemua + $row->totalkeseluruhan;?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="4"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalAdmRajal)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

           
            <b><span class="label label-success" style="font-size:12px">ADMINISTRASI</span></b>
            <b><span class="label label-default" style="font-size:12px">RAWAT INAP</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Pendaftaran</th>
                       
                        <th style="width:20%">Tindakan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:10%">Diskon (%)</th>
                        <th style="width:20%">Tarif Setelah Diskon</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:5%">Aksi</th>
                    </tr>
                </thead>
                <tbody id="tempAdministrasiRanap">
                    <?php $totalAdmRanap = 0; ?>
                    <?php foreach ($this->model->getAdmRanap($idvalidasi,1) as $row) {?>
                    <?php
					
					
					?>
                    <tr>
                        <td><?=$notransaksi?></td>
                        <td><?=$row->namatarif?></td>
                        <td><?=number_format($row->total)?></td>
                        <td><?= calcDiscountPercent($row->diskon_rp, $row->total); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_ranap/edit_adm/<?=$idvalidasi?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php $totalAdmRanap = $totalAdmRanap + $row->totalkeseluruhan; $totalSemua = $totalSemua + $row->totalkeseluruhan;?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="5"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalAdmRanap)?></td>
                    </tr>
                </tfoot>
            </table>
		
 
            <hr>

            <?php //$totalKeseluruhan = $totalSebelumAdm + $totalAdmRanap; ?>
           

            <table class="table table-bordered table-striped" style="margin-top: 20px;">
                <thead>
                    <tr>
                        <td align="right" style="font-size:15px"><b>TOTAL KESELURUHAN</b></td>
                        <td align="left" class="text-bold" id="grandTotal" align="center" style="font-size:15px"><?=number_format($totalSemua)?></td>
                    </tr>
                    
                    <tr>
                        <td align="right" style="font-size:15px"><b>TOTAL DEPOSIT</b></td>
                        <td align="left" class="text-bold" id="grandTotalDeposit" align="center" style="font-size:15px"><span id="totalDeposit"><?=number_format($deposit)?></span></td>
                    </tr>
                    <tr>
                        <td align="right" style="font-size:15px"><b>TOTAL HARUS DIBAYAR</b></td>
                        <td align="left" class="text-bold" id="grandTotalPembayaran" align="center" style="font-size:15px"><?=number_format($totalharusdibayar)?></td>
                    </tr>
                </thead>
            </table>
			
        </div>
		<?}?>
    </div>
</div>
<div id="transaction-only">
    <div class="block">
        <div class="block-content">
			<?if ($st_gabungan=='0'){?>
            <h5 style="margin-bottom: 10px;">Pembayaran</h5>
            <table id="cara_pembayaran" class="table table-bordered table-striped" style="margin-top: 20px;">
                <tbody>
                    <tr>
                        <th style="width: 85%;" colspan="5"><label class="pull-right"><b>SUB TOTAL Rp.</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
							<input class="form-control input-sm " readonly type="text" id="gt_all" name="gt_all" value="<?=number_format($totalSemua)?>">
						</th>
					</tr>
					<tr>
						<th style="width: 85%;" colspan="5"><label class="pull-right"><b>DISKON</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
							<div class="input-group">
								<span class="input-group-addon">Rp</span>
								<input class="form-control" disabled type="text" id="diskon_rp" name="diskon_rp" placeholder="Discount Rp" value="{nominal_diskon}">
							</div>
							<div class="input-group">
								<span class="input-group-addon"> %. </span>
								<input class="form-control" disabled type="text" id="diskon_persen" name="diskon_persen" placeholder="Discount %"  value="{diskonpersen}">
							</div>
						</th>
					</tr>
					<tr>
						<th style="width: 85%;" colspan="5"><label class="pull-right"><b>TOTAL Rp.</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
							<input class="form-control input-sm " readonly type="text" id="gt_rp" name="gt_rp" value="<?=number_format($totalharusdibayar)?>">
						</th>
					</tr>
					<tr>
						<th style="width: 85%;" colspan="5"><label class="pull-right"><b>TOTAL DEPOSIT</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
							<input class="form-control input-sm " readonly type="text" id="gt_deposit" name="gt_deposit" value="<?=number_format($deposit)?>">
						</th>
					</tr>
					<tr>
						<th style="width: 85%;" colspan="5"><label class="pull-right"><b>TOTAL HARUS DIBAYAR</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
							<input class="form-control input-sm " readonly type="text" id="gt_final" name="gt_final" value="<?=number_format($totalharusdibayar)?>">
						</th>
					</tr>
					
				</tbody>
			</table>
			<?}?>
			<div class="row">
				<div class="col-sm-12">
					<?if ($st_gabungan=='0'){?>
					<div class="progress progress-mini">
						<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
					</div>

                    <div class="alert alert-danger alert-dismissable" id="alertSelisihPembayaran" hidden>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h3 class="font-w300 push-15">Selisih Pembayaran</h3>
                        <p>Terdapat Selisih Pembayaran Rp. <span id="nominalSelisihPembayaran"></span></p>
                    </div>
					<?}?>
					<div class="control-group">
                        
						<div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0; margin-bottom:20px">
							<table id="manage_tabel_pembayaran" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 5%;">#</th>
										<th style="width: 25%;">Jenis Pembayaran</th>
										<th style="width: 35%;">Keterangan</th>
										<th style="width: 10%;">Nominal</th>
										<th style="width: 10%;" class="editOnly">Aksi</th>
									</tr>
								</thead>
								<input type="hidden" id="rowindex">
								<input type="hidden" id="nomor">
								<tbody>
									<?php $no = 1;$total_pembayaran=0; ?>
									<?php foreach ($detail_pembayaran as $row) {?>
										<tr>
											<td hidden><?=$row->idmetode?></td>
											<td><?=$no++?></td>
											<td><?=metodePembayaran($row->idmetode)?></td>
											<td><?=$row->metode_nama?></td>
											<td><?= number_format($row->nominal)?></td>
										
											<td class="editOnly">
                                                <a href="{base_url}tvalidasi_pendapatan_ranap/edit_pembayaran/<?=$idvalidasi?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
											</td>
											
										</tr>
									<?php
										$total_pembayaran = $total_pembayaran + $row->nominal;
									} ?>
								</tbody>
								<tfoot id="foot_pembayaran">
									<tr class="bg-light dker">
										<th colspan="3"><label class="pull-right"><b>Pembayaran Rp.</b></label></th>
                                        <th colspan="2">
                                            <?php if ($total_pembayaran): ?>
                                            <input type="text" class="form-control input-sm number" readonly id="bayar_rp" name="bayar_rp" value="<?=$total_pembayaran?>" />
                                            
                                            <?php endif?>
                                        </th>
                                    </tr>
									<?if ($st_gabungan=='0'){?>
                                    <tr class="bg-light dker">
                                        <th colspan="3"><label class="pull-right"><b>Sisa Rp.</b></label></th>
                                        <th colspan="2">
                                            <input type="text" class="form-control input-sm number" readonly id="sisa_rp" name="sisa_rp" value="<?=($totalharusdibayar-$total_pembayaran)?>" />
                                        </th>
                                    </tr>
									<?}?>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
		<div class="block-content">
		<h3 class="block-title"> <?=text_danger('Ringkasan Akun')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
			<div class="col-md-12">
					<table class="table table-bordered table-striped fixed" id="index_ringkasan" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:3%"  class="text-center">NO </th>
								<th style="width:15%"  class="text-center">NO AKUN</th>								
								<th style="width:30%"  class="text-center">AKUN</th>								
								<th style="width:20%"  class="text-center">DEBIT</th>								
								<th style="width:20%"  class="text-center">KREDIT</th>								
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
			</div>
		</div>
	</div>
    </div>
	
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('.number').number(true, 0, '.', ',');
    $('.discount').number(true, 2, '.', ',');
	load_ringkasan();
   
    $(".time-datepicker").datetimepicker({
        format: "HH:mm:ss"
    });

	checkSelisihPembayaran();


   
});
$(document).on('click', '#btn_simpan', function() {
	var idgroup_diskon = $("#idgroup_diskon").val();
	var idgroup_gabungan = $("#idgroup_gabungan").val();
	var idgroup_round = $("#idgroup_round").val();
	var idvalidasi = $("#idvalidasi").val();
	// alert(idgroup_diskon);
	// return false;

	$.ajax({
		url: '{site_url}tvalidasi_pendapatan_ranap/updateDetail',
		method: 'POST',
		dataType: 'json',
		data: {
			idvalidasi: idvalidasi,
			idgroup_diskon: idgroup_diskon,
			idgroup_gabungan: idgroup_gabungan,
			idgroup_round: idgroup_round,
		},
		success: function(data) {
			// alert(data.status);
			  if (data.status) {
				swal("Informasi", "Update Berhasil", "success");
				load_ringkasan();
			  }
		}
	});
});
	
function load_ringkasan(){
	$("#cover-spin").show();
	var id=$("#idvalidasi").val();
	var disabel=$("#disabel").val();
	var idhead='';
	$('#index_ringkasan tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_pendapatan_ranap/load_ringkasan/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel,idhead: idhead},
		success: function(data) {
			// console.log(data.tabel);
			$('#index_ringkasan').append(data.tabel);
			$(".opsi").select2({dropdownAutoWidth: false});
			$("#cover-spin").hide();
			// $(".tgl").datepicker();
			// $(".simpan").hide();
		}
	});
}
function checkSelisihPembayaran() {
    var nomnialSelisih = $("#sisa_rp").val();
    if (nomnialSelisih > 0) {
        $("#alertSelisihPembayaran").show();
        $("#nominalSelisihPembayaran").html($.number(nomnialSelisih));
    } else {
        $("#alertSelisihPembayaran").hide();
    }
}

</script>
