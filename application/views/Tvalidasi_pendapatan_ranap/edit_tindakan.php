<style>
table.fixed {
      table-layout: fixed;
      width: 100%;
    }
    table.fixed td {
      overflow: hidden;
    }
</style>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tvalidasi_pendapatan_ranap/save_edit_tindakan','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Tarif / Tindakan</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control"  name="notransaksi" value="{namatarif}">
                        <input type="hidden" readonly class="form-control" name="idvalidasi" value="{idvalidasi}">
                        <input type="hidden" readonly class="form-control" id="id"  name="id" value="{id}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Jenis Tarif</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="notransaksi"  name="notransaksi" value="<?=GetTipeTarifTindakan($idjenis)?>">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">User Input</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="notransaksi"  name="notransaksi" value="">
                        
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Group Diskon</label>
                    <div class="col-md-8">
                        <select id="group_diskon_all" name="group_diskon_all" <?=($diskon_rp>0?'':'disabled')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="0" <?=($group_diskon_all==null?'selected':'')?>>Belum dipilih</option>
							<?foreach($list_group as $row){?>
								<option value="<?=$row->id?>" <?=($group_diskon_all==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
                        
                    </div>
                </div>
               
				
                
            </div>
			<div class="col-md-6">
				 <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Dokter</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_transaksi" placeholder="Dokter" name="tanggal_transaksi" value="<?=($nama_dokter)?>">
                        
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Harga Master</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control decimal" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=($totalkeseluruhan)?>">
                        
                    </div>
                </div>
                
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Total Transaksi</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control decimal" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=($totalkeseluruhan)?>">
                        
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Total Diskon</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control decimal" id="diskon_rp" placeholder="No. Faktur" name="diskon_rp" value="<?=($diskon_rp)?>">
                        
                    </div>
                </div>
                
				
               
			</div>
            
        </div>
	</div>
	
	<div class="block-content">
		<h3 class="block-title"> <?=text_default('TARIF')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped" id="index_hasil" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:15%"  class="text-center">ITEM BIAYA</th>
								<th style="width:8%"  class="text-right">HARGA</th>
								<th style="width:8%"  class="text-center">DISKON Rp.</th>
								<th style="width:5%"  class="text-center">DISKON (%)</th>
								<th style="width:10%"  class="text-center">TOTAL</th>
								<th style="width:20%"  class="text-center">GROUP PENDAPATAN</th>
								<th style="width:20%"  class="text-center">GROUP DISKON</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="text-center"><strong>JASA SARANA</strong></td>
								<td class="text-right"><?=number_format($jasasarana,2)?></td>
								<td class="text-right"><?=number_format($jasasarana_diskon,2)?></td>
								<td class="text-right"><?=($jasasarana_diskon>0?number_format($jasasarana_diskon/$jasasarana *100,2): 0)?></td>
								<td class="text-right"><?=number_format($jasasarana-$jasasarana_diskon,2)?></td>
								<td>
									<select id="group_jasasarana" name="group_jasasarana" <?=($jasasarana>0?'':'disabled')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="0" <?=($group_jasasarana==null?'selected':'')?>>Belum dipilih</option>
									<?foreach($list_group as $row){?>
										<option value="<?=$row->id?>" <?=($group_jasasarana==$row->id?'selected':'')?>><?=$row->nama?></option>
									<?}?>
									</select>
								</td>
								<td>
									<select id="group_jasasarana_diskon" name="group_jasasarana_diskon" <?=($jasasarana>0?'':'disabled')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($group_jasasarana_diskon==null?'selected':'')?>>Belum dipilih</option>
										<?foreach($list_group as $row){?>
											<option value="<?=$row->id?>" <?=($group_jasasarana_diskon==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="text-center"><strong>JASA PELAYANAN</strong></td>
								<td class="text-right"><?=number_format($jasapelayanan,2)?></td>
								<td class="text-right"><?=number_format($jasapelayanan_diskon,2)?></td>
								<td class="text-right"><?=($jasapelayanan_diskon>0?number_format($jasapelayanan_diskon/$jasapelayanan *100,2): 0)?></td>
								<td class="text-right"><?=number_format($jasapelayanan-$jasapelayanan_diskon,2)?></td>
								<td>
									<select id="group_jasapelayanan" name="group_jasapelayanan" <?=($jasapelayanan>0?'':'disabled')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="0" <?=($group_jasapelayanan==null?'selected':'')?>>Belum dipilih</option>
									<?foreach($list_group as $row){?>
										<option value="<?=$row->id?>" <?=($group_jasapelayanan==$row->id?'selected':'')?>><?=$row->nama?></option>
									<?}?>
									</select>
								</td>
								<td>
									<select id="group_jasapelayanan_diskon" <?=($jasapelayanan>0?'':'disabled')?> name="group_jasapelayanan_diskon" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($group_jasapelayanan_diskon==null?'selected':'')?>>Belum dipilih</option>
										<?foreach($list_group as $row){?>
											<option value="<?=$row->id?>" <?=($group_jasapelayanan_diskon==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="text-center"><strong>BHP</strong></td>
								<td class="text-right"><?=number_format($bhp,2)?></td>
								<td class="text-right"><?=number_format($bhp_diskon,2)?></td>
								<td class="text-right"><?=($bhp_diskon>0?number_format($bhp_diskon/$bhp *100,2): 0)?></td>
								<td class="text-right"><?=number_format($bhp-$bhp_diskon,2)?></td>
								<td>
									<select id="group_bhp" name="group_bhp" <?=($bhp>0?'':'disabled')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="0" <?=($group_bhp==null?'selected':'')?>>Belum dipilih</option>
									<?foreach($list_group as $row){?>
										<option value="<?=$row->id?>" <?=($group_bhp==$row->id?'selected':'')?>><?=$row->nama?></option>
									<?}?>
									</select>
								</td>
								<td>
									<select id="group_bhp_diskon" name="group_bhp_diskon" <?=($bhp>0?'':'disabled')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($group_bhp_diskon==null?'selected':'')?>>Belum dipilih</option>
										<?foreach($list_group as $row){?>
											<option value="<?=$row->id?>" <?=($group_bhp_diskon==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="text-center"><strong>BIAYA PERAWATAN</strong></td>
								<td class="text-right"><?=number_format($biayaperawatan,2)?></td>
								<td class="text-right"><?=number_format($biayaperawatan_diskon,2)?></td>
								<td class="text-right"><?=($biayaperawatan_diskon>0?number_format($biayaperawatan_diskon/$biayaperawatan *100,2): 0)?></td>
								<td class="text-right"><?=number_format($biayaperawatan-$biayaperawatan_diskon,2)?></td>
								<td>
									<select id="group_biayaperawatan" name="group_biayaperawatan" <?=($biayaperawatan>0?'':'disabled')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="0" <?=($group_biayaperawatan==null?'selected':'')?>>Belum dipilih</option>
									<?foreach($list_group as $row){?>
										<option value="<?=$row->id?>" <?=($group_biayaperawatan==$row->id?'selected':'')?>><?=$row->nama?></option>
									<?}?>
									</select>
								</td>
								<td>
									<select id="group_biayaperawatan_diskon" name="group_biayaperawatan_diskon" <?=($biayaperawatan>0?'':'disabled')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($group_biayaperawatan_diskon==null?'selected':'')?>>Belum dipilih</option>
										<?foreach($list_group as $row){?>
											<option value="<?=$row->id?>" <?=($group_biayaperawatan_diskon==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</td>
							</tr>
						</tbody>
					</table>
			</div>
		</div>
	</div>
	
	<div class="block-content">
		 <hr style="margin-top:10px">
		 <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<div class="col-xs-12 text-right">
						<a href="{base_url}tvalidasi_pendapatan_ranap/detail/<?=$idvalidasi?>" class="btn btn-default" type="reset"><i class="fa fa-reply"></i> Kembali</a>
						<?if ($disabel==''){?>
						<button class="btn btn-primary" name="btn_simpan" type="submit" value="1"><i class="fa fa-save"></i> Update</button>
						<?}?>
					</div>
				</div>
				
            </div>
        </div>
		<hr style="margin-bottom:10px">		
		
	</div>
</div>
<?php echo form_close(); ?>
<?
	$this->load->view('Tklaim_monitoring/modal_list');
?>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
  
});


function validate_final(){
	if ($("#diskon_rp").val() > 0 && $("#group_diskon_all").val()=='0'){
		sweetAlert("Maaf...", "Group Diskon Belum di isi!", "error");
		return false;
	}
	$("*[disabled]").not(true).removeAttr("disabled");
	return true;
}

</script>