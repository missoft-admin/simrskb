<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="modal fade in black-overlay" id="modal_cari" role="dialog" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:70%;">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Honor Dokter Transaksi</h3>
				</div>
				<div class="block-content">
					<div class="form-horizontal">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" for="" style="margin-top: 5px;">KATEGORI</label>
							<div class="col-md-4">
								<select id="idkategori" name="idkategori" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#">- Semua -</option>
									<?foreach($list_kategori as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>									
									<?}?>
									
								</select>
							</div>

							<label class="col-md-2 control-label" for="" style="margin-top: 5px;">Dokter</label>
							<div class="col-md-4">
								<select id="iddokter" class="js-select2 form-control" style="width: 100%;"
									data-placeholder="Pilih Opsi" multiple>
									<?php foreach (get_all('mdokter', array('status' => 1)) as $row){?>
									<option value="<?= $row->id?>" <?=($iddokter == $row->id ? 'selected="selected"':'')?>>
										<?= $row->nama?></option>
									<?php }?>
								</select>
							</div>
						</div>
						
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" for="" style="margin-top: 5px;">No Registrasi</label>
							<div class="col-md-4">
								<input type="text" class="form-control" id="notransaksi" placeholder="No Registrasi" name="notransaksi" value="">
							</div>

							<label class="col-md-2 control-label" for="example-daterange1">Tanggal Pembayaran</label>
							<div class="col-md-4">
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" id="tanggalpembayaran_dari" placeholder="From"
										value="">
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" id="tanggalpembayaran_sampai" placeholder="To"
										value="">
								</div>
							</div>
						</div>

						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" for="" style="margin-top: 10px;"></label>
							<div class="col-md-4"></div>
							
							<label class="col-md-2 control-label" for="" style="margin-top: 10px;"></label>
							<div class="col-md-4"> <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button></div>
						</div>

					</div>
					<br>
					<div class="table-responsive">
					<table class="table table-bordered table-striped" id="index_trx" style="width: 100%;">
						<thead>
							<tr>
								<th>X</th>
								<th>X</th>
								<th>NO REGISTRASI</th>
								<th>DOKTER</th>
								<th>KATEGORI</th>
								<th>TANGGAL PEMBAYARAN</th>
								<th>NOMINAL</th>
								<th>JATUH TEMPO</th>
								<th>STATUS</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
				</div>
			</div>
			
		</div>
	</div>
</div>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}thonor_approval" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?php echo form_open('thonor_approval/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="form-group" >
				<div class="col-md-10">
					<button class="btn btn-success" type="button" onclick="cari_trx()" id="btn_cari" <?=$disabel?> title="Cari"><i class="fa fa-search-plus"></i> Cari Honor Dokter</button>	
				</div>
			</div>
			<div class="form-group" >
				<div class="col-md-12">
					<input type="hidden" class="form-control" readonly id="list_trx" placeholder="Nama Bagi Hasil" name="list_trx" value="<?=$list_trx?>">
					<input type="hidden" class="form-control" readonly id="id" placeholder="Nama Bagi Hasil" name="id" value="<?=$id?>">
					<input type="hidden" class="form-control" readonly id="disabel" placeholder="Nama Bagi Hasil" name="disabel" value="<?=$disabel?>">
					<div class="table-responsive">
					<table class="table table-bordered table-striped" id="index_hasil" style="width: 100%;">
						<thead>
							<tr>
								<th>X</th>
								<th>X</th>
								<th>NO REGISTRASI</th>
								<th>DOKTER</th>
								<th>KATEGORI</th>
								<th>TANGGAL PEMBAYARAN</th>
								<th>NOMINAL</th>
								<th>JATUH TEMPO</th>
								<th>STATUS</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					
				</div>
				</div>
			</div>
			<div class="form-group" >
				<div class="col-md-4">
					<button type="submit" class="btn btn-primary" <?=$disabel?> id="btn_generate"><i class="fa fa-angle-double-down"></i> Create Approval</button>
					<button type="button" class="btn btn-danger" <?=$disabel?> id="btn_clear"><i class="fa fa-refresh"></i> Clear Data</button>
					
				</div>
				<div class="col-md-4">
					
				</div>
			</div>
			
			
		
			<?php echo form_close() ?>
	</div>

</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
arr_id=[];
	$(document).ready(function(){
		var id=$("#id").val();
		var list_trx=$("#list_trx").val();
		if (id !=''){
			arr_id=list_trx.split(",");
			// alert(arr_id	);
			// load_generate();
		}
		$(".number").number(true,0,'.',',');
		$(".decimal").number(true,2,'.',',');
		// cari_trx();
		load_hasil();
	})	
	function cari_trx(){
		$("#modal_cari").modal('show');
		load_trx();
	}
	
	
	function load_trx() {
		// alert('sini');
		var idkategori=$("#idkategori").val();
		var list_trx=$("#list_trx").val();
		var iddokter=$("#iddokter").val();
		var notransaksi=$("#notransaksi").val();
		var tanggalpembayaran_dari=$("#tanggalpembayaran_dari").val();
		var tanggalpembayaran_sampai=$("#tanggalpembayaran_sampai").val();
		var id=$("#id").val();
		
		$('#index_trx').DataTable().destroy();
		var table = $('#index_trx').DataTable({
		"pageLength": 100,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"lengthChange": true,
		"order": [],
		"ajax": {
			url: '{site_url}thonor_approval/load_trx/',
			type: "POST",
			dataType: 'json',
			data: {
				idkategori: idkategori,
				list_trx: list_trx,
				iddokter: iddokter,
				notransaksi: notransaksi,
				tanggalpembayaran_dari: tanggalpembayaran_dari,
				tanggalpembayaran_sampai: tanggalpembayaran_sampai,
				id: id,
				
			}
		},
		columnDefs: [
					{"targets": [0], "visible": false },
					 { "width": "5%", "targets": [1]},
					 { "width": "8%", "targets": [2,5,7,8,9]},
					 { "width": "10%", "targets": [4,6]},
					 { "width": "15%", "targets": [3]},
					 { "targets": [1,6],className: "text-right" },
					 { "targets": [2,4,5,7,8,9],className: "text-center" },
					 // { "width": "40%", "targets": [3],className: "text-left" },
					 // { "width": "20%", "targets": [4],className: "text-left" }
					]
		});
	}
	function load_hasil() {
		// var idkategori=$("#idkategori").val();
		var list_trx=$("#list_trx").val();
		var disabel=$("#disabel").val();
		// alert(list_trx);
		
		$('#index_hasil').DataTable().destroy();
		var table = $('#index_hasil').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"lengthChange": false,
		"order": [],
		"ajax": {
			url: '{site_url}thonor_approval/load_hasil/',
			type: "POST",
			dataType: 'json',
			data: {
				// idkategori: idkategori,
				list_trx: list_trx,disabel: disabel,
				// iddokter: iddokter,
				
			}
		},
		columnDefs: [
					{"targets": [0], "visible": false },
					 { "width": "5%", "targets": [1]},
					 { "width": "8%", "targets": [2,5,7,8,9]},
					 { "width": "10%", "targets": [4,6]},
					 { "width": "15%", "targets": [3]},
					 { "targets": [1,6],className: "text-right" },
					 { "targets": [2,4,5,7,8,9],className: "text-center" },
					 // { "width": "40%", "targets": [3],className: "text-left" },
					 // { "width": "20%", "targets": [4],className: "text-left" }
					]
		});
		// clear_generate();
	}
	// $("#idkategori").change(function(){
		// if ($("#idkategori").val()!='#'){
			// $("#btn_cari").attr('disabled',false);
		// }else{
			// $("#btn_cari").attr('disabled',true);
			
		// }
	// });
	$("#btn_filter").click(function(){
		load_trx();
	});
	$("#btn_clear").click(function(){
		arr_id=[];
		$("#list_trx").val('');
		load_hasil();
	});
	
	$(document).on("click",".pilih",function(){	
		var table = $('#index_trx').DataTable();
		var table2 = $('#index_hasil').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()
		arr_id.push(id);
		$(this).closest('td').parent().remove();
		var list_trx=$("#list_trx").val();
		if (list_trx==''){
			list_trx=id;
		}else{
			list_trx +=','+id;
		}
		$("#list_trx").val(list_trx);
		// table.ajax.reload( null, false );
		// alert(arr_id)
		// load_trx();
		load_hasil();
		$.toaster({priority : 'success', title : 'Succes!', message : ' ditambahkan'});
	});
	
	$(document).on("click",".hapus",function(){	
		//fruits.indexOf("Apple")
		var table = $('#index_hasil').DataTable();
		// // var table2 = $('#index_hasil').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()
		delete arr_id[arr_id.indexOf(id)];
		
		// alert(arr_id);
		var list_trx='';
		var str_list='';
		var arrayLength = arr_id.length;
		for (var i = 0; i < arrayLength; i++) {
			if (arr_id[i]!=null){
				console.log(arr_id[i]);
				if (list_trx==''){
					list_trx=arr_id[i];
				}else{
					list_trx +=','+arr_id[i];
				}
			}
				$("#list_trx").val(list_trx);
			//Do something
		}
		
		load_hasil();
		$.toaster({priority : 'success', title : 'Succes!', message : ' Dihapus'});
	});
	
	function load_generate(){
		var id=$("#id").val();
		var disabel=$("#disabel").val();
		$('#index_generate tbody').empty();
		$.ajax({
			url: '{site_url}thonor_approval/load_generate_edit/',
			dataType: "json",
			type: "POST",
			data: {id: id,disabel: disabel},
			success: function(data) {
				$('#index_generate').append(data.tabel);
				$(".opsi").select2({dropdownAutoWidth: false});
				$(".tgl").datepicker();
			}
		});
	}
	function validate_final(){
		
		var st_lolos='1';
		var table = $('#index_hasil').DataTable();
		var tgl;
		var rowCount = table.settings()[0].json.recordsTotal;
		// alert(rowCount);return false;
		if (rowCount==0){
				sweetAlert("Maaf...", "Honor belum dipilih!", "error");
				st_lolos='0';
				return false;
			}
		
		if (st_lolos=='0'){
			return false;
		}else{
			return true;
			
		}
	}
</script>