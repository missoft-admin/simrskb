<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1813'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_general()"><i class="fa fa-bell"></i> General</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1815'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_event"  onclick="load_tab2()"><i class="fa fa-check-square-o"></i> Logic</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1822'))){ ?>
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab_3"><i class="fa fa-check-square-o"></i> Label Form</a>
		</li>
		<?}?>
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('1813'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_info">
			
			<?php echo form_open('setting_eresep/save_eresep','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_edit_catatan">Izin Edit Catatan</label>
						<div class="col-md-2">
							<select id="st_edit_catatan" name="st_edit_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_edit_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_edit_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_edit">Waktu Edit</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_edit" name="lama_edit" value="{lama_edit}" placeholder="Waktu Edit">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_edit">Edit Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_edit" name="orang_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_edit=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_edit=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_hapus_catatan">Izin Hapus Catatan</label>
						<div class="col-md-2">
							<select id="st_hapus_catatan" name="st_hapus_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_hapus_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_hapus_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_hapus">Waktu Hapus</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_hapus" name="lama_hapus" value="{lama_hapus}" placeholder="Waktu Hapus">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_hapus">Hapus Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_hapus" name="orang_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_hapus=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_hapus=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_duplikasi_catatan">Izin Duplikasi Catatan</label>
						<div class="col-md-2">
							<select id="st_duplikasi_catatan" name="st_duplikasi_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_duplikasi_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_duplikasi_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_duplikasi">Waktu Duplikasi</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_duplikasi" name="lama_duplikasi" value="{lama_duplikasi}" placeholder="Waktu Duplikasi">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_duplikasi">Duplikasi Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_duplikasi" name="orang_duplikasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_duplikasi=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_duplikasi=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-2">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5 class="pull-right"><?=text_primary('SETTING ITER')?></h5>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_iter_default">Default ITER</label>
						<div class="col-md-2">
							<select id="st_iter_default" name="st_iter_default" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_iter_default=='1'?'selected':'')?>>YA</option>
								<option value="0" <?=($st_iter_default=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="max_iter">Maxs ITER</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="max_iter" name="max_iter" value="{max_iter}" placeholder="Maks Iter">
								<span class="input-group-addon">KALI</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_duplikasi">Jumlah ITER Default</label>
						<div class="col-md-2 div_catatan">
							<select id="jml_iter" name="jml_iter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<?for($i=0;$i<=10;$i++){?>
								<option value="<?=$i?>" <?=($jml_iter==$i?'selected':'')?>><?=$i?></option>
								<?}?>
								
							</select>
						</div>
					</div>
				</div>
				
				<?php if (UserAccesForm($user_acces_form,array('1814'))){ ?>
				<div class="col-md-12 push-10">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="statusstok"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_general" name="btn_save_general"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
				<?}?>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1815'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_event">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('HAK AKSES FORMULIR')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_hak_akses">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Profesi</th>
										<th width="15%">Spesialisasi</th>
										<th width="15%">PPA</th>
										<th width="15%">Melihat</th>
										<th width="10%">Input</th>
										<th width="10%">Edit</th>
										<th width="10%">Hapus</th>
										<th width="10%">Cetak</th>
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="profesi_id" name="profesi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" <?=($profesi_id=='#'?'selected':'')?>>-Pilih Profesi-</option>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" <?=($profesi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="spesialisasi_id" name="spesialisasi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($spesialisasi_id=='#'?'selected':'')?>>-SEMUA-</option>
												<?foreach(list_variable_ref(22) as $r){?>
													<option value="<?=$r->id?>" <?=($spesialisasi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										<th>
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($mppa_id=='#'?'selected':'')?>>-SEMUA-</option>
												
											</select>
										</th>
										
										<th>
											<select id="st_lihat" name="st_lihat" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_lihat=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_lihat=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_input" name="st_input" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_input=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_input=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_edit" name="st_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_edit=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_edit=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_hapus" name="st_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_hapus=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_hapus=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_cetak" name="st_cetak" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_cetak=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_cetak=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<?php if (UserAccesForm($user_acces_form,array('1816'))){ ?>
											<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_hak_akses" name="btn_tambah_hak_akses"><i class="fa fa-plus"></i> Tambah</button>
											<?}?>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
					</div>
				</div>
				</div>
				
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1822'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?> " id="tab_3">
			
			<?php echo form_open('setting_eresep/save_eresep_label','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="form-group">
				<div class="col-md-12">
					<div class="col-md-12">
						<label class="text-primary">PESAN INFORMASI</label>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<textarea class="form-control js-summernote" name="pesan_informasi"><?=$pesan_informasi?></textarea>
					</div>
				</div>
				</div>
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">JUDUL FORMULIR</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="judul_ina" value="{judul_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="judul_eng" value="{judul_eng}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Waktu Permintaan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="waktu_req" <?=($waktu_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="waktu_ina" value="{waktu_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="waktu_eng" value="{waktu_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tujuan Farmasi</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="tujuan_req" <?=($tujuan_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="tujuan_ina" value="{tujuan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="tujuan_eng" value="{tujuan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Dokter Pemberi Resep</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="dokter_req" <?=($dokter_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="dokter_ina" value="{dokter_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="dokter_eng" value="{dokter_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Berat Badan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="bb_req" <?=($bb_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="bb_ina" value="{bb_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="bb_eng" value="{bb_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tinggi Badan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="tb_req" <?=($tb_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="tb_ina" value="{tb_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="tb_eng" value="{tb_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Luas Permukaan Tubuh</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="luas_req" <?=($luas_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="luas_ina" value="{luas_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="luas_eng" value="{luas_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Diagnosa</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="diagnosa_req" <?=($diagnosa_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="diagnosa_ina" value="{diagnosa_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="diagnosa_eng" value="{diagnosa_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Menyusui</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="menyusui_req" <?=($menyusui_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="menyusui_ina" value="{menyusui_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="menyusui_eng" value="{menyusui_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Terdapat Gangguan Ginjal</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="gangguan_req" <?=($gangguan_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="gangguan_ina" value="{gangguan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="gangguan_eng" value="{gangguan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Pasien Puasa</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="puasa_req" <?=($puasa_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="puasa_ina" value="{puasa_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="puasa_eng" value="{puasa_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Terdapat Alergi</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="alergi_req" <?=($alergi_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="alergi_ina" value="{alergi_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="alergi_eng" value="{alergi_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Detail Alergi</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="alergi_detail_req" <?=($alergi_detail_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="alergi_detail_ina" value="{alergi_detail_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="alergi_detail_eng" value="{alergi_detail_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Prioritas</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="prioritas_req" <?=($prioritas_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="prioritas_ina" value="{prioritas_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="prioritas_eng" value="{prioritas_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Resep Pasien Pulang</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="pulang_req" <?=($pulang_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="pulang_ina" value="{pulang_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="pulang_eng" value="{pulang_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Catatan Pemeriksaan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="catatan_req" <?=($catatan_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="catatan_ina" value="{catatan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="catatan_eng" value="{catatan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Catatan Keterangan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="css-input switch switch-sm switch-primary">
								<input type="checkbox" name="ket_req" <?=($ket_req=='1'?'checked':'')?>><span></span> Required
							</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="ket_ina" value="{ket_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="ket_eng" value="{ket_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-top:15px">
				
					<div class="col-md-12">
						<div class="col-md-6">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_label" name="btn_save_general"><i class="fa fa-save"></i> Simpan</button>
						</div>
						
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
	$('.js-summernote').summernote({
		  height: 50,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})

		$(".number").number(true,0,'.',',');
	var tab=$("#tab").val();
	if (tab=='1'){
		load_general();
	}
	if (tab=='2'){
			load_tab2();
	}

})	
function load_general(){
	$('.js-summernote').summernote({
	  height: 50,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	});	

}
function load_tab2(){
	load_hak_akses();		
}
// LOGIC
function load_hak_akses(){
	$('#index_hak_akses').DataTable().destroy();	
	table = $('#index_hak_akses').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_eresep/load_hak_akses', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}

$("#btn_tambah_hak_akses").click(function() {
	let profesi_id=$("#profesi_id").val();
	let spesialisasi_id=$("#spesialisasi_id").val();
	let mppa_id=$("#mppa_id").val();
	let st_lihat=$("#st_lihat").val();
	let st_input=$("#st_input").val();
	let st_edit=$("#st_edit").val();
	let st_hapus=$("#st_hapus").val();
	let st_cetak=$("#st_cetak").val();
	
	if (profesi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Profesi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_eresep/simpan_hak_akses', 
		dataType: "JSON",
		method: "POST",
		data : {
				profesi_id:profesi_id,
				spesialisasi_id:spesialisasi_id,
				mppa_id:mppa_id,
				st_lihat:st_lihat,
				st_input:st_input,
				st_edit:st_edit,
				st_hapus:st_hapus,
				st_cetak:st_cetak,
			
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				$("#profesi_id").val('#').trigger('change');
				$("#spesialisasi_id").val('#').trigger('change');
				$("#mppa_id").val('#').trigger('change');
				$("#st_cetak").val('0').trigger('change');
				$("#st_hapus").val('0').trigger('change');
				$("#st_edit").val('0').trigger('change');
				$("#st_input").val('0').trigger('change');
				$("#st_lihat").val('0').trigger('change');
				$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});

});
function hapus_hak_akses(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_eresep/hapus_hak_akses',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

$("#profesi_id").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_eresep/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id").empty();
				$("#mppa_id").append(data);
			}
		});
	}else{
		
				$("#mppa_id").empty();
	}

});

</script>