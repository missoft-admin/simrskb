<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}mkategori_akun" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content block-content-narrow">
        <?php echo form_open('mkategori_akun/save','class="form-horizontal push-10-t"'); ?>
        <div class="form-group">
            <label class="col-md-3 control-label" for="">Nama</label>
            <div class="col-md-7">
                <input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="">Jika Bertambah</label>
            <div class="col-md-7">
                <select name="jikabertambah" id="jikabertambah" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                    <option value="" selected disabled>Pilih Opsi</option>
                    <option value="DB" <?=($jikabertambah == 'DB' ? 'selected="selected"':'')?>>Debit</option>
                    <option value="CR" <?=($jikabertambah == 'CR' ? 'selected="selected"':'')?>>Kredit</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="">Jika Berkurang</label>
            <div class="col-md-7">
                <select name="jikaberkurang" id="jikaberkurang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                    <option value="" selected disabled>Pilih Opsi</option>
                    <option value="DB" <?=($jikaberkurang == 'DB' ? 'selected="selected"':'')?>>Debit</option>
                    <option value="CR" <?=($jikaberkurang == 'CR' ? 'selected="selected"':'')?>>Kredit</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="">Pos Saldo</label>
            <div class="col-md-7">
                <select name="possaldo" id="possaldo" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                    <option value="" selected disabled>Pilih Opsi</option>
                    <option value="DB" <?=($possaldo == 'DB' ? 'selected="selected"':'')?>>Debit</option>
                    <option value="CR" <?=($possaldo == 'CR' ? 'selected="selected"':'')?>>Kredit</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="">Pos Laporan</label>
            <div class="col-md-7">
                <select name="poslaporan" id="poslaporan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                    <option value="" selected disabled>Pilih Opsi</option>
                    <option value="NRC" <?=($poslaporan == 'NRC' ? 'selected="selected"':'')?>>Neraca</option>
                    <option value="LBR" <?=($poslaporan == 'LBR' ? 'selected="selected"':'')?>>Laba Rugi</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-9 col-md-offset-3">
                <button class="btn btn-success" type="submit">Simpan</button>
                <a href="{base_url}mkategori_akun" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
            </div>
        </div>
        <?php echo form_hidden('id', $id); ?>
        <?php echo form_close() ?>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#jikabertambah").change(function() {
		const jikaBertambah = $(this).val();
		if (jikaBertambah == 'DB') {
			$("#jikaberkurang").val('CR');
			$("#jikaberkurang").select2("destroy");
			$("#jikaberkurang").select2();
		} else {
			$("#jikaberkurang").val('DB');
			$("#jikaberkurang").select2("destroy");
			$("#jikaberkurang").select2();
		}
	});

	$("#jikaberkurang").change(function() {
		const jikaBerkurang = $(this).val();
		if (jikaBerkurang == 'DB') {
			$("#jikabertambah").val('CR');
			$("#jikabertambah").select2("destroy");
			$("#jikabertambah").select2();
		} else {
			$("#jikabertambah").val('DB');
			$("#jikabertambah").select2("destroy");
			$("#jikabertambah").select2();
		}
	});
});
</script>
