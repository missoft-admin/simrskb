<?= ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        
        <h3 class="block-title">{title}</h3>
    </div>
	<div class="block-content">
		<?php echo form_open('Mnomorakuntansi/export','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">Kategori Akun</label>
				<div class="col-md-11">
					<input type="text" readonly class="form-control" name="kategoriakun" value="{kategoriakun}">
					<input type="hidden" readonly class="form-control" name="status" value="#">
					<input type="hidden" readonly class="form-control" name="bertambah" value="#">
					<input type="hidden" readonly class="form-control" name="berkurang" value="#">
					<input type="hidden" readonly class="form-control" id="idkategori"  name="idkategori" value="{idkategori}">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">Header Akun</label>
				<div class="col-md-11">
					<select name="headerakun[]" id="headerakun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
						<?php foreach  ($list_header as $row) { ?>
							<option value="'<?=$row->noakun?>'"><?=$row->namaakun?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 15px;">
				<label class="col-md-1" for="">Nomor Akun</label>
				<div class="col-md-11">
					<select name="noakun[]" id="noakun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
						<?php foreach  ($list_akun as $row) { ?>
							<option value="<?=$row->id?>"><?=$row->noakun.'-'.$row->namaakun?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-1" for=""></label>
				<div class="col-md-11">
					<button class="btn btn-info" type="button" id="btn_filter"><i class="fa fa-filter"></i> Filter</button>
					<button class="btn btn-success" type="submit" name="btn_export" value="1"><i class="fa fa-file-excel-o"></i> Export Excel</button>
					<button class="btn btn-danger" type="submit" name="btn_export" value="2"><i class="fa fa-file-pdf-o"></i> Export PDF</button>
					<button class="btn btn-primary" type="submit" name="btn_export" value="3"><i class="fa fa-print"></i> Print</button>
				</div>
			</div>
		
		<hr>
		<div class="table-responsive">
        <table class="table table-bordered table-striped" id="index_list" style="width: 100%;">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th width="15%">No Header</th>
                    <th width="10%">No Akun</th>
                    <th width="20%">Nama Akun</th>
                    <th width="10%">Kategori</th>
                    <th width="5%">Status</th>
                    <th width="10%">Saldo</th>
                    <th width="20%">Aksi</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div>
		<?php echo form_close(); ?>
    </div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
 $(document).ready(function(){
       var idkategori=$("#idkategori").val();
	   // alert(tanggalkontrabon);
		load_detail();
    })
	$(document).on("click","#btn_filter",function(){	
		load_detail();		
	});
	function load_detail() {
		// alert('sini');
		var idkategori=$("#idkategori").val();
		var headerakun=$("#headerakun").val();
		var noakun=$("#noakun").val();
		// alert(headerakun);
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}Mkategori_akun/load_detail/',
			type: "POST",
			dataType: 'json',
			data: {
				idkategori: idkategori,
				headerakun: headerakun,
				noakun: noakun,
			}
		},
		columnDefs: [
					 {  className: "text-right", targets:[0,6] },
					 {  className: "text-center", targets:[4,5,7] },
					 { "width": "5%", "targets": [0] },
					 { "width": "8%", "targets": [2,4,6] },
					 { "width": "10%", "targets": [7] }
					 // { "width": "15%", "targets": [2,13] }

					]
		});
	}
</script>