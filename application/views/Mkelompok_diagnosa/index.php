<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('287'))){ ?>
<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('288'))){ ?>	
		<ul class="block-options">
        <li>
            <a href="{base_url}mkelompok_diagnosa/create" class="btn"><i class="fa fa-plus"></i></a>
        </li>
    </ul>
		<?}?>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:25px">
		<?php if (UserAccesForm($user_acces_form,array('289'))){ ?>	
		<div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
			
			<div class="col-md-6"  style="margin-bottom: 5px;">
				<div class="form-group">
					<label class="col-md-4 control-label" for="distributor">Lokasi Tubuh</label>
                    <div class="col-md-8">
						<div class="input-daterange input-group">						
							<select name="lokasi_tubuh_id" id="lokasi_tubuh_id" class="js-select2 form-control" style="width: 100%;">
								<option value="#">Semua Lokasi Tubuh</option>
								<?foreach ($lokasi_tubuh_list as $row){?>
									<option value="<?=$row->id?>" <?=($lokasi_tubuh_id==$row->id?'selected':'')?>><?=$row->nama?></option>
								<?}?>
							</select>
							
						</div>
					</div>
				</div>
				<div class="form-group"  style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="distributor">Kelompok Operasi</label>
                    <div class="col-md-8">
						<div class="input-daterange input-group">						
							<select name="kelompok_operasi_id" id="kelompok_operasi_id" class="js-select2 form-control" style="width: 100%;">
								<option value="#">Semua Kelompok Operasi</option>
								<?foreach ($kelompok_operasi_list as $row){?>
									<option value="<?=$row->id?>" <?=($kelompok_operasi_id==$row->id?'selected':'')?>><?=$row->nama?></option>
								<?}?>
							</select>
							
						</div>
					</div>
				</div>
				
            </div>
			<div class="col-md-6">  
				<div class="form-group">
					<label class="col-md-4 control-label" for="distributor">Status</label>
                    <div class="col-md-8">
						<div class="input-daterange input-group">						
							<select name="status_aktif" id="status_aktif" class="js-select2 form-control" style="width: 100%;">
								<option value="#">Semua Status</option>
								<option value="1">AKTIF</option>
								<option value="2">TIDAK AKTIF</option>
								
							</select>
							
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;" id="div_new">
                    <label class="col-md-4 control-label" for=""></label>
                    
					<div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" id="btn_filter" name="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter </button>
                    </div>
                </div>
            </div>
			
			
            <?php echo form_close(); ?>
			
        </div>
		<?}?>
	</div>
	<div class="block-content">
		
		
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th width="10%">No</th>
					<th width="20%">Nama</th>
					<th width="20%">Lokasi Tubuh</th>
					<th width="20%">Kel.Operasi</th>
					<th width="10%">Status ICD</th>
					<th width="10%">Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		</div>
	</div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		LoadIndex();
	});
	function LoadIndex(){
		var lokasi_tubuh_id=$("#lokasi_tubuh_id").val();
		var kelompok_operasi_id=$("#kelompok_operasi_id").val();
		var status_aktif=$("#status_aktif").val();
		
		$('#datatable-simrs').DataTable().destroy();
		var table = $('#datatable-simrs').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}mkelompok_diagnosa/getIndex/',
			type: "POST",
			dataType: 'json',
			data: {
				lokasi_tubuh_id: lokasi_tubuh_id,
				kelompok_operasi_id: kelompok_operasi_id,
				status_aktif: status_aktif,
			}
		},
		"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": true },
					{ "width": "30%", "targets": 1, "orderable": true },
					{ "width": "20%", "targets": 2, "orderable": true },
					{ "width": "20%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 4, "orderable": true },
					{ "width": "15%", "targets": 5, "orderable": true },
				]
		});
	}
	$(document).on("click","#btn_filter",function(){
		LoadIndex();
	});
</script>
