<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1726'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_cahaya()"><i class="fa fa-bar-chart-o"></i> {title}</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('1726'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?>" id="tab_info">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
						<div class="form-group">
							<label class="col-xs-12" for="satuan_cahaya">Satuan Cahaya</label>
							<div class="col-xs-4">
								<div class="input-group">
									<input class="form-control" type="text" id="satuan_cahaya" name="satuan_cahaya" value="{satuan_cahaya}" placeholder="Satuan Cahaya">
									<span class="input-group-btn">
										<button class="btn btn-success" onclick="update_satuan()" type="button"><i class="fa fa-save"></i>  Update</button>
									</span>
								</div>
								
							</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_cahaya">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="40%">Range Cahaya</th>
											<th width="25%">Kategori</th>
											<th width="15%">Warna</th>
											<th width="15%">Action</th>										   
										</tr>
										<tr>
											<th>#<input class="form-control" type="hidden" id="cahaya_id" name="cahaya_id" value="" placeholder="id"></th>
											<th>
												<div class="input-group" style="width:100%">
													<input class="form-control decimal" type="text" id="cahaya_1" name="cahaya_1" placeholder="<?=$satuan_cahaya?>" value="" >
													<span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
													
													<input class="form-control decimal" type="text" id="cahaya_2" name="cahaya_2" placeholder="<?=$satuan_cahaya?>" value="" >
													
													
												</div>
											</th>
											<th>
												<input class="form-control" type="text" style="width:100%" id="kategori_cahaya" name="kategori_cahaya" placeholder="Kategori" value="" >
											</th>
											<th>
												
												<div class="js-colorpicker input-group colorpicker-element">
													<input class="form-control" type="text" id="warna" name="warna" value="#0000">
													<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
												</div>
											
											</th>
											<th>
												<div class="btn-group">
												<?php if (UserAccesForm($user_acces_form,array('1727'))){ ?>
												<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_cahaya" name="btn_tambah_cahaya"><i class="fa fa-save"></i> Simpan</button>
												<?}?>
												<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_cahaya()"><i class="fa fa-refresh"></i> Clear</button>
												</div>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			
			
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
	$(".decimal").number(true,1,'.',',');	
	load_cahaya();		
	
})	
function clear_cahaya(){
	$("#cahaya_id").val('');
	$("#cahaya_1").val('');
	$("#cahaya_2").val('');
	$("#kategori_cahaya").val('');
	$("#warna").val('#000');
	$("#btn_tambah_cahaya").html('<i class="fa fa-save"></i> Save');
	
}
function load_cahaya(){
	$('#index_cahaya').DataTable().destroy();	
	table = $('#index_cahaya').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mcahaya/load_cahaya', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function update_satuan(){
	let satuan_cahaya=$("#satuan_cahaya").val();
	
	if (satuan_cahaya==''){
		sweetAlert("Maaf...", "Tentukan Ssatuan", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mcahaya/update_satuan', 
		dataType: "JSON",
		method: "POST",
		data : {
				satuan_cahaya:satuan_cahaya,
				
				
			},
		complete: function(data) {
			location.reload();
		}
	});
}
$("#btn_tambah_cahaya").click(function() {
	let cahaya_id=$("#cahaya_id").val();
	let cahaya_1=$("#cahaya_1").val();
	let cahaya_2=$("#cahaya_2").val();
	let kategori_cahaya=$("#kategori_cahaya").val();
	let warna=$("#warna").val();
	
	if (cahaya_1==''){
		sweetAlert("Maaf...", "Tentukan Range Cahaya", "error");
		return false;
	}
	if (cahaya_2==''){
		sweetAlert("Maaf...", "Tentukan Range Cahaya", "error");
		return false;
	}
	if (kategori_cahaya==''){
		sweetAlert("Maaf...", "Tentukan Kategori", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mcahaya/simpan_cahaya', 
		dataType: "JSON",
		method: "POST",
		data : {
				cahaya_id:cahaya_id,
				cahaya_1:cahaya_1,
				cahaya_2:cahaya_2,
				kategori_cahaya:kategori_cahaya,
				warna:warna,
				
			},
		complete: function(data) {
			clear_cahaya();
			$('#index_cahaya').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
function edit_cahaya(cahaya_id){
	$("#cahaya_id").val(cahaya_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mcahaya/find_cahaya',
		type: 'POST',
		dataType: "JSON",
		data: {id: cahaya_id},
		success: function(data) {
			$("#btn_tambah_cahaya").html('<i class="fa fa-save"></i> Edit');
			$("#cahaya_id").val(data.id);
			$("#cahaya_1").val(data.cahaya_1);
			$("#cahaya_2").val(data.cahaya_2);
			$("#warna").val(data.warna).trigger('change');
			$("#kategori_cahaya").val(data.kategori_cahaya);
			
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_cahaya(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Cahaya?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mcahaya/hapus_cahaya',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_cahaya').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

</script>