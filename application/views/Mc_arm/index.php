<?=ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{site_url}Mc_arm/create" class="btn"><i class="fa fa-plus"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-bottom: 0px;">
	</div>
	<div class="block-content">
		<table id="mcarm" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No Registrasi</th>
					<th>Nama</th>
					<th>Total Lembaran</th>
          <th>Status</th>
          <th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<!-- {{ @action }} -->
			</tbody>
		</table>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">

  $(document).ready(function () {
		var showCarm	= $('#mcarm').DataTable({
			"oLanguage":{
				"sEmptyTable": "<i class='text-uppercase'>NO DATA AVAILABLE IN TABLE</i>",
			},
			"ajax": {
				"url":"{site_url}Mc_arm/showCarm",
				"type": "POST"
			},
			"autoWidth": false,
			"pageLength": 10,
			"lengthChange": true,
			"ordering": true,
			"processing": true,
			"order": []
		});


		$(document).on('click', '.del-mcarm', function() {
			var id = $(this).attr('data-value');

			swal({
				title: "Anda Yakin ?",
				text : "Ingin menghapus data ini ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				$.ajax({
					url: '{site_url}Mc_arm/deleteCarm',
					type: 'POST',
					dataType: 'json',
					data: {id:id},
					complete: function(data) {
						showCarm.ajax.reload();
					}
				});
			});

		});

		// @function:status-carm &start
		$(document).on('click', '.status-carm', function() {
			var status = $(this).attr('data-value');
			$(event.target).closest('tr').each(function () {
				var id = $(this).find('td:eq(0)').text();
				if (parseInt(status) == 1) {
					status = 0;
				} else {
					status = 1;
				}

				$.ajax({
					url: '{site_url}Mc_arm/changeStatusCarm',
					type: 'POST',
					dataType: 'json',
					data: {id:id, status:status},
					complete: function() {
						showCarm.ajax.reload();
						swal("Berhasil!", "Berhasil memperbaharui status!", "success");
					}
				});
			});
		});
		// @function:status-carm &end
  });

</script>
