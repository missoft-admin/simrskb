<?=ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{site_url}Mc_arm" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-bottom: 0px;">
	</div>
	<div class="block-content" style="padding-bottom:20px;">
		<?php echo form_open_multipart('Mc_arm/saveCarm','class="form-horizontal" id="form-mcarm"') ?>
			<div class="row">

				<div class="col-md-6">

					<div class="form-group">
						<label class="col-md-4 control-label text-uppercase" for="name-tarif" style="float:left;">Nama</label>
						<div class="col-md-8">
							<input type="text" id="name-tarif" class="form-control" name="name-tarif" required="" aria-required="true">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label text-uppercase" for="name-carm" style="float:left;">Tarif C - ARM</label>
						<div class="col-md-8">
							<select id="name-carm" class="form-control name-carm" name="name-carm[]" multiple required="" aria-required="true" data-placeholder="PILIH CARM ...">
								<!-- {{ @action }} -->
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label text-uppercase" for="pemilik-tarif" style="float:left;">Bagian PS</label>
						<div class="col-md-8">
							<div class="input-group">
								<input type="number" id="pemilik-tarif" class="form-control" name="pemilik-tarif" required="" aria-required="true" min="0" max="100" value="80">
								<span class="input-group-addon">%</span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-4 control-label text-uppercase" for="potongan-tarif" style="float:left;">Potongan RS</label>
						<div class="col-md-8">
							<div class="input-group">
								<input type="number" id="potongan-tarif" class="form-control" name="potongan-tarif" required="" aria-required="true" min="0" max="100" value="20">
								<span class="input-group-addon">%</span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label text-uppercase col-md-4" for="note-carm" style="float:left;">Catatan</label>
						<div class="col-md-8">
							<textarea id="note-carm" class="form-control" name="note-carm" rows="4" cols="80"></textarea>
						</div>
					</div>
				</div>


				<div class="col-md-6">

					<div class="form-group">
						<label class="control-label text-uppercase col-md-4" for="mulai-tarif" style="float:left;">Mulai Berlaku</label>
						<div class="col-md-8">
							<div class="input-group">
								<input type="text" id="mulai-tarif" class="form-control" name="mulai-tarif" required="" aria-required="true">
								<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label text-uppercase col-md-4" for="perhitungan-tarif" style="float:left;">TGL Perhitungan</label>
						<div class="col-md-4">
							<div class="input-group">
								<input type="text" id="awal-tarif" class="form-control" name="awal-tarif" required="" aria-required="true" value="01">
							</div>
						</div>
						<div class="col-md-4">
							<div class="input-group">
								<input type="text" id="akhir-tarif" class="form-control" name="akhir-tarif" required="" aria-required="true" value="<?=date('t');?>">
							</div>
						</div>
					</div>

				</div>
			</div>


			<hr>

			<input type="text" id="mcarm-detail" name="mcarm-detail" hidden="true" readonly>
			<input type="text" id="mcarm-id" name="mcarm-id" hidden="true" readonly>

			<h5 class="text-uppercase" style="padding-bottom:10px;">File Attachments</h5>
			<table id="table-file-attachment" class="table table-bordered" style="margin-top:10px;">
				<thead>
					<tr>
						<td colspan="2">
							<button id="add-file-attachment" class="btn btn-xs btn-primary text-uppercase" type="button" name="button"><i class="fa fa-plus"></i>&nbsp;Tambah</button>
						</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<input type="file" name="attachment[]" multiple>
						</td>
						<td>
							<button class="btn btn-xs btn-danger btn-file-carm" type="button" name="button"><i class="fa fa-close"></i></button>
						</td>
					</tr>
				</tbody>
			</table>

			<button type="submit" class="btn btn-sm btn-success text-uppercase" name="save" style="width:120px;">Simpan</button>
		<?php echo form_close() ?>

    <hr>

		<h5 class="text-uppercase" style="padding-bottom:10px;">Detail Pemilik Saham</h5>
    <table id="table-pemegang-saham" class="table table-bordered">
      <thead>
        <th>Nama</th>
        <th>Lembaran</th>
				<th>Aksi</th>
      </thead>
      <tbody id="pemegang-saham">
				<!-- {{ @action }} -->
      </tbody>
			<tr>
				<td><select type="text" id="name-psaham" class="form-control js-select2" required="" data-placeholder="Pilih Pemilik Saham ..." style="width:100%;"><option value="">Pilih Pemilik Saham ...</option></select></td>
				<td><input type="number" id="lembaran" class="form-control" style="width:100%;"></td>
				<td><button id="save-mcarm-detail" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></button></td>
			</tr>
    </table>
	</div>
</div>

<script src="{js_path}custom/url.js"></script>
<script type="text/javascript">
// @common &start

var ecarmbtn = "<button class='btn btn-sm btn-primary edit-mcarm-detail'><i class='fa fa-pencil'></i></button>",
		dcarmbtn = "<button class='btn btn-sm btn-danger del-mcarm-detail'><i class='fa fa-close'></i></button>";


function getCarmId(id) {
	var option;

	$.ajax({
		url: '{site_url}Mc_arm/getCarmId',
		dataType: 'json',
		success: function(data) {
			for (var iter = 0; iter < data.length; iter++) {
				if (data[iter].idpemilik == 0 || data[iter].idpemilik == "" ) {
					option = '<option class="se'+data[iter].id+'" value="'+data[iter].id+'" disabled>'+data[iter].nama+'</option>';
				} else {
					option = '<option class="se'+data[iter].id+'" value="'+data[iter].id+'">'+data[iter].nama+'</option>';
				}

				$('#name-carm').append(option);
			}

			$('#name-carm').select2();
		}
	}).done(function() {
		if (id != undefined) {
			showCarm(id);
		}
	});
}

function getDetailTable() {
	var detail = $('#table-pemegang-saham #pemegang-saham tr').get().map(function(row) {
		return $(row).find('td').get().map(function(cell) {
			return $(cell).html();
		});
	});
	$("#mcarm-detail").val(JSON.stringify(detail));
}

function getPemilikSaham() {
	var option;

	$.ajax({
		url: '{site_url}Mc_arm/getPemilikSaham',
		dataType: 'json',
		success: function(data) {
			for (var iter = 0; iter < data.length; iter++) {
				option = '<option value="'+data[iter].nama_pemilik+'|'+data[iter].id+'">'+data[iter].nama_pemilik+'</option>';
				$('#name-psaham').append(option);
			}
		}
	});
}

function showCarm(id) {
	console.log(id);
	$.ajax({
		url: '{site_url}Mc_arm/showCarmIn/'+id,
		dataType: 'json',
		success: function(data) {

			var tarif = data.tarif_carm.split(',');

			$('#name-tarif').val(data.nama_carm);
			$('#pemilik-tarif').val(data.bagian_pemilik);
			$('#potongan-tarif').val(data.potongan_rs);
			$('#note-carm').val(data.catatan);

			$('#mulai-tarif').val(data.tanggal_berlaku);
			$('#awal-tarif').val(data.tanggal_awal);
			$('#akhir-tarif').val(data.tanggal_akhir);

			$('#name-carm').select2('destroy');

			for (var iter = 0; iter < tarif.length; iter++) {
				var el = '#name-carm .se'+tarif[iter];

				$(el).attr('selected', 'selected');
			}

			$('#name-carm').select2();
			getDetailTable();
		}
	});
}

function showCarmDetail(id) {
	var table;

	$.ajax({
		url: '{site_url}Mc_arm/showCarmDetail/'+id,
		dataType: 'json',
		success: function(data) {
			$('#table-pemegang-saham #pemegang-saham').empty();
			for (var iter = 0; iter < data.length; iter++) {
				table = '<tr class="sde'+data[iter].id+'">';
				table += '<td>'+data[iter].nama+'</td>';
				table += '<td>'+data[iter].lembaran+'</td>';
				table += '<td>'+ecarmbtn+'&nbsp;'+dcarmbtn+'</td>';
				// table += '<td>'+data[iter].id+'</td>';
				table += '</tr>';
				$('#table-pemegang-saham #pemegang-saham').append(table);
			}
		}
	});
}
// @common &end


	$(document).ready(function () {
		// @common &start
		$('#mulai-tarif').datepicker({
			format: "yyyy/mm/dd"
		});

		$('#awal-tarif, #akhir-tarif').datepicker({
			format: "dd",
		});

		getPemilikSaham();

		var id = '{id}';

console.log(id);
		if (id != undefined) {
			getCarmId(id);
			showCarmDetail(id);
			$('#mcarm-id').val(id);
		} else {
			getCarmId();
		}
		// @common &end

		// @function:save-mcarm-detail &start
		$('#save-mcarm-detail').click(function () {
			var nama 		 = $('#name-psaham').val(),
					lembaran = $('#lembaran').val();

			namas = nama.split('|');
			var str = '.sm'+namas[1],
					len = $('#table-pemegang-saham').find(str).length;

			if (nama !== "" && lembaran !== "" && len == 0) {
				var el = "<tr>";
						el += '<td class="sm'+namas[1]+'">'+namas[0]+'</td>';
						el += "<td>"+lembaran+"</td>";
						el += "<td>"+ecarmbtn+"&nbsp;"+dcarmbtn+"</td>";
						el += "</tr>";

				$('#name-psaham').val('').trigger('change');
				$('#lembaran').val('');
				$('#table-pemegang-saham #pemegang-saham').append(el);
			}

			getDetailTable();
		});
		// @function:save-mcarm-detail &end

	});

	// @function:del-mcarm-detail &start
	$(document).on('click', '.del-mcarm-detail', function () {
		$(this).closest('tr').remove();
		getDetailTable();
	});
	// @function:del-mcarm-detail &end

	// @function:edit-mcarm-detail &start
	$(document).on('click', '.edit-mcarm-detail', function () {
		$(event.target).closest('tr').each(function () {
			var nama = $(this).find('td:eq(0)').text(),
					lembaran = parseInt($(this).find('td:eq(1)').text());

			var in1 = '<select type="text" id="name-psaham" class="name-psaham form-control js-select2" required="" data-placeholder="Pilih Pemilik Saham ..." style="width:100%;"><option value="">Pilih Pemilik Saham ...</option></select>',
					in2 = '<input class="form-control lembaran-temp" value="'+lembaran+'">';

			getPemilikSaham();
			$('.name-psaham').select2();

			var act = '<button type="button" class="btn btn-sm btn-info sedit-mcarm-detail"><i class="fa fa-check"></i></button>&nbsp;';
					act += '<button type="button" class="btn btn-sm btn-warning uedit-mcarm-detail" data-value="'+nama+"|"+lembaran+'"><i class="fa fa-undo"></i></button>';

			$(this).find('td:eq(0)').text('').append(in1);
			$(this).find('td:eq(1)').text('').append(in2);
			$(this).find('td:eq(2)').text('').append(act);
		});
	});
	// @function:edit-mcarm-detail &end

	// @function:sedit-mcarm-detail &start
	$(document).on('click', '.sedit-mcarm-detail', function () {
		$(event.target).closest('tr').each(function () {
			var nama_temp = $(this).find('td:eq(0) .name-psaham').val(),
					lembaran_temp = $(this).find('td:eq(1) .lembaran-temp').val();

			$(this).find('td:eq(0)').text('').append(nama_temp);
			$(this).find('td:eq(1)').text('').append(lembaran_temp);
			$(this).find('td:eq(2)').text('').append(ecarmbtn+"&nbsp"+dcarmbtn);
		});

		getDetailTable();
	});
	// @function:sedit-mcarm-detail &end

	// @function:uedit-mcarm-detail &start
	$(document).on('click', '.uedit-mcarm-detail', function () {
		var val = $(this).attr('data-value').split('|');

		$(event.target).closest('tr').each(function () {
			$(this).find('td:eq(0)').text('').append(val[0]);
			$(this).find('td:eq(1)').text('').append(val[1]);
			$(this).find('td:eq(2)').text('').append(ecarmbtn+"&nbsp"+dcarmbtn);
		});
	});
	// @function:uedit-mcarm-detail &end

	// pemilik-tarif & potongan-tarif
	function divideTarif(percent_s) {
		if (isNaN(percent_s)) {
			percent_s = 0;
		}

		percent_r = 100 - percent_s;
		return percent_r;
	}

	$('#pemilik-tarif').keyup(function() {
		var percent_pe = parseInt($(this).val());

		if ($(this).val() < 0) {
			$(this).val(0);
		}

		percent_po = divideTarif(percent_pe);
		$('#potongan-tarif').val(percent_po);
	});

	$('#potongan-tarif').keyup(function() {
		var percent_po = parseInt($(this).val());

		if ($(this).val() < 0) {
			$(this).val(0);
		}

		percent_pe = divideTarif(percent_po);
		$('#pemilik-tarif').val(percent_pe);
	});

	// @
	$('#add-file-attachment').click(function() {
		var el = '<tr>';
				el += '<td><input type="file" name="attachment[]" multiple></td>';
				el += '<td><button class="btn btn-xs btn-danger btn-file-carm" type="button" name="button"><i class="fa fa-close"></i></button></td>';
				el += '</tr>';

		$('#table-file-attachment tbody').append(el);
	});

	$(document).on('click', '.btn-file-carm', function() {
		$(this).closest('tr').remove();
	});
</script>
