<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}msetting_jurnal_kasbon" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('msetting_jurnal_kasbon/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>	
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_primary('Pengaturan Posting')?></label>
				<input type="hidden" class="form-control" id="tmp_idakun_kredit" placeholder="0" name="tmp_idakun_kredit" value="{idakun_kredit}">
				<div class="col-md-4">
					<select id="st_auto_posting" name="st_auto_posting" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="1" <?=(1 == $st_auto_posting ? 'selected' : ''); ?>>AUTO POSTING</option>
                            <option value="2" <?=(2 == $st_auto_posting ? 'selected' : ''); ?>>MANUAL</option>
                        </select>
				</div>
			</div>
			<?
			$rows2=get_all('msumber_kas',array(),'nama','ASC');
			$rows=get_all('mdistributor',array(),'nama','ASC');
			?>
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_primary('Batas Waktu Batal')?></label>
				
				<div class="col-md-4">
					<input type="text" style="width: 100%"  class="form-control number" id="batas_batal" placeholder="0" name="batas_batal" value="<?=$batas_batal?>">
				</div>
				<div class="col-md-2">
					<button type="button" class="btn btn-sm btn-success" tabindex="8" id="btn_update" title="Update"><i class="fa fa-save"></i> UPDATE</button>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-1 control-label"><h4><?=text_success('SETTING AKUN KASBON')?></h4></label>
				
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<table id="tabel_kasbon" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 0%;">#</th>															
								<th style="width: 20%;">Tipe</th>															
								<th style="width: 20%;">Kategori </th>															
								<th style="width: 20%;">Nama Dokter / Pegawai </th>															
								<th style="width: 25%;">No Akun </th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td></td>
								<td>
									<select name="idtipe_kasbon" id="idtipe_kasbon" style="width: 100%" id="step" class="js-select2 form-control input-sm">										
										<option value="#" selected>- Pilih Tipe -</option>																			
										<option value="1">Dokter</option>																			
										<option value="2">Pegawai</option>																			
										
									</select>										
								</td>
								<td>
									<select name="idkategori_kasbon" id="idkategori_kasbon" style="width: 100%" id="step" class="js-select2 form-control input-sm">										
										<option value="#" selected>- All -</option>	
									</select>										
								</td>
								<td>
									<select name="idpegawai_kasbon" id="idpegawai_kasbon" style="width: 100%" id="step" class="js-select2 form-control input-sm">										
										<option value="#" selected>- All -</option>	
									</select>										
								</td>								
								
								<td>
									<select name="idakun_kasbon" id="idakun_kasbon" style="width: 100%" class="js-select2 form-control input-sm idakun">										
										
									</select>
									<input type="hidden" class="form-control" id="id_edit_kasbon" placeholder="0" name="id_edit_kasbon" value="">
								</td>	
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_kasbon" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_kasbon" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						
				</table>
				</div>
			</div>
			
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		// load_unit();
		list_akun();
		load_kasbon();
		
		clear_input_kasbon();

	});	
	$(document).on("click","#btn_update",function(){
		if (validate_update()==false)return false;
		var st_auto_posting=$("#st_auto_posting").val();
		var idakun_kredit=$("#idakun_kredit").val();
		var batas_batal=$("#batas_batal").val();
		$("#tmp_idakun_kredit").val(idakun_kredit)
		$.ajax({
			url: '{site_url}msetting_jurnal_kasbon/update',
			type: 'POST',
			data: {
				st_auto_posting:st_auto_posting,idakun_kredit:idakun_kredit,batas_batal:batas_batal,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$("#idakun_kredit").val($("#tmp_idakun_kredit").val()).trigger('change');
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function validate_update()
	{
		
		if ($("#st_auto_posting").val()=='#'){
			sweetAlert("Maaf...", "Setting Harus diisi", "error");
			return false;
		}
		if ($("#batas_batal").val()==''){
			sweetAlert("Maaf...", "Batas Waktu Batal Harus diisi", "error");
			return false;
		}
		if ($("#idakun_kredit").val()=='#'){
			sweetAlert("Maaf...", "No AKun Pembayaran Kredit Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	$("#idtipe_kasbon").change(function(){
		list_kategori($(this).val());
		list_pegawai();
	});
	$("#idkategori_kasbon").change(function(){
		list_pegawai();
	});

	function list_kategori($idtipe){
		if ($idtipe!='#'){			
			$.ajax({
				url: '{site_url}msetting_jurnal_kasbon/list_kategori/'+$idtipe,
				dataType: "json",
				success: function(data) {
					$("#idkategori_kasbon").empty();
					$('#idkategori_kasbon').append('<option value="#" selected>- All -</option>');
					$('#idkategori_kasbon').append(data.detail);
					
				}
			});		
		}else{
			$("#idkategori_kasbon").empty();
			$('#idkategori_kasbon').append('<option value="#" selected>- All -</option>');
		}
	}
	function list_pegawai(){
		var idtipe=$("#idtipe_kasbon").val();
		var idkategori=$("#idkategori_kasbon").val();
		if (idtipe !='#'){
			
			$.ajax({
				url: '{site_url}msetting_jurnal_kasbon/list_pegawai/',
				type: "POST",
				dataType: 'json',
				data:{
					idtipe:idtipe,
					idkategori:idkategori,
				},
				success: function(data) {
					$("#idpegawai_kasbon").empty();
					$('#idpegawai_kasbon').append('<option value="#" selected>- All -</option>');
					$('#idpegawai_kasbon').append(data.detail);
					
				}
			});		
		}else{
			$("#idpegawai_kasbon").empty();
			$('#idpegawai_kasbon').append('<option value="#" selected>- All -</option>');
		}
		
	}
	function list_akun(){
		$.ajax({
			url: '{site_url}msetting_jurnal_kasbon/list_akun',
			dataType: "json",
			success: function(data) {
				$(".idakun").empty();
				$('.idakun').append('<option value="#" selected>- Pilih Akun -</option>');
				$('.idakun').append(data.detail);
				
			}
		});		
	}
	
	
	function validate_add_kasbon()
	{
		
		
		if ($("#idtipe_kasbon").val()=='#'){
			sweetAlert("Maaf...", "Tipe Harus diisi", "error");
			return false;
		}
		if ($("#idakun_kasbon").val()=='#'){
			sweetAlert("Maaf...", "No AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_kasbon(){
		$("#idtipe_kasbon").val('#').trigger('change');	
		$("#idkategori_kasbon").val('#').trigger('change');	
		$("#idpegawai_kasbon").val('#').trigger('change');	
		$("#id_edit_kasbon").val('');		
		$("#idakun_kasbon").val('#').trigger('change');
		// alert($("#idakun").val());
	}
	$(document).on("click","#simpan_kasbon",function(){
		if (validate_add_kasbon()==false)return false;
		var id_edit=$("#id_edit_kasbon").val();
		var idtipe=$("#idtipe_kasbon").val();
		var idkategori=$("#idkategori_kasbon").val();
		var idkategori=$("#idkategori_kasbon").val();
		var idpegawai=$("#idpegawai_kasbon").val();
		var idakun=$("#idakun_kasbon").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_kasbon/simpan_kasbon',
			type: 'POST',
			data: {
				id_edit:id_edit,idkategori:idkategori,
				idkategori: idkategori,idpegawai:idpegawai,idakun:idakun,idtipe:idtipe,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_kasbon').DataTable().ajax.reload( null, false );
					clear_input_kasbon();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	function hapus_kasbon($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_kasbon/hapus_kasbon/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_kasbon').DataTable().ajax.reload( null, false );
						clear_input_kasbon();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_kasbon",function(){
		
		clear_input_kasbon();
	});
	function load_kasbon(){
		var idlogic=$("#id").val();
		
		$('#tabel_kasbon').DataTable().destroy();
		var table = $('#tabel_kasbon').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_kasbon/load_kasbon/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	
	
</script>
