<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>BUKTI PERMINTAAN INFORMASI MEDIS</title>
    <style>
    @page {
        size: 75mm 600mm;
        margin-top: 7mm;
        margin-left: 2mm;
        margin-right: 3mm;
        margin-bottom: 5mm;
    }

    body {
        -webkit-print-color-adjust: exact;
    }


    table {
        font-family: "Courier", Verdana, sans-serif;
        /* font-size: 20px !important; */
        font-size: 22px !important;
        border-collapse: collapse !important;
        width: 100% !important;
    }

    td {
        padding: 1px;

    }

    /* span {
        font-size: 18px !important;
    } */

    .content td {
        padding: 1px;
        border: 0px solid #6033FF;
    }

    .content-2 td {
        margin: 0px;
        border: 0px solid #6033FF;
    }

    /* .content-rincian td {
        font-size: 16px !important;
    } */

    /* border-normal */
    .border-full {
        border: 0px solid #000 !important;
    }

    .border-bottom {
        border-bottom: 1px solid #000 !important;
    }

    .border-bottom-top {
        border-bottom: 1px solid #000 !important;
        border-top: 1px solid #000 !important;
    }

    .border-bottom-top-left {
        border-bottom: 1px solid #000 !important;
        border-top: 1px solid #000 !important;
        border-left: 1px solid #000 !important;
    }

    .border-left {
        border-left: 1px solid #000 !important;
    }

    /* border-thick */
    .border-thick-top {
        border-top: 1px dotted #000 !important;
    }

    .border-thick-bottom {
        border-bottom: 2px dotted #000 !important;
    }

    .border-dotted {
        border-width: 1px;
        border-bottom-style: dotted;
    }

    /* text-position */
    .text-center {
        text-align: center !important;
    }

    .text-left {
        text-align: left !important;
    }

    .text-right {
        text-align: right !important;
    }

    /* text-style */
    .text-italic {
        font-style: italic;
    }

    .text-bold {
        font-weight: bold;
    }

    </style>
</head>

<body>
    <div>
        <table class="content" style="width:100%">
            <tr>
                <td class="text-center border-thick-bottom">
                    <strong>RSKB HALMAHERA SIAGA<br><span>JL. LLRE. MARTADINATA NO.28 BANDUNG<span></strong><br><br>
                </td>
            </tr>
        </table>
        <table class="content-2" style="width:100%">
            <tr>
                <td colspan="3" class="text-center border-thick-bottom">
                    <strong>BUKTI PERMINTAAN INFORMASI MEDIS</strong>
                </td>
            </tr>
            <tr>
                <td style="width:29%">Tanggal</td>
                <td style="width:1%">:</td>
                <td style="width:70%"><?=$tanggal?></td>
            </tr>
            <tr>
                <td>Pemohon</td>
                <td>:</td>
                <td><?=$nama_pemohon?></td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td>:</td>
                <td><?=strip_tags($keterangan)?></td>
            </tr>
        </table>

        <table class="content-2" style="width:100%">
            <tr>
                <td class="text-center text-header">
                    <strong>RINCIAN PENGAJUAN</strong>
                </td>
            </tr>
            <?php $number = 0; ?>
            <?php $total = 0; ?>
            <?php foreach ($list_kunjungan as $row) { ?>
            <?php $number = $number + 1; ?>
            <tr>
                <td style="border: 1px solid #000;">
                    <table class="content-2 content-rincian" style="width:100%: border: 1px solid #000;">
                        <tr>
                            <td class="text-center" rowspan="7" width="5%"><?=$number?></td>
                            <td class="border-full text-left" width="29%">Nama Pasien</td>
                            <td class="border-full text-left" width="1%">:</td>
                            <td class="border-full text-left" width="70%"><?=$row->no_medrec?> - <?=$row->nama_pasien?></td>
                        </tr>
                        <tr>
                            <td class="border-full text-left">Jenis Kunjungan</td>
                            <td class="border-full text-left">:</td>
                            <td class="border-full text-left"><?=$row->jenis_kunjungan?></td>
                        </tr>
                        <tr>
                            <td class="border-full text-left">Dokter</td>
                            <td class="border-full text-left">:</td>
                            <td class="border-full text-left"><?=$row->nama_dokter?></td>
                        </tr>
                        <tr>
                            <td class="border-full text-left">Jenis Pengajuan</td>
                            <td class="border-full text-left">:</td>
                            <td class="border-full text-left"><?=$row->jenis_pengajuan?></td>
                        </tr>
                        <tr>
                            <td class="border-full text-left">Est Selesai</td>
                            <td class="border-full text-left">:</td>
                            <td class="border-full text-left"><?=$row->estimasi_selesai?></td>
                        </tr>
                        <tr>
                            <td class="border-full text-left">Tarif</td>
                            <td class="border-full text-left">:</td>
                            <td class="border-full text-left">Rp. <?=number_format($row->nominal)?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <?php $total += $row->nominal; ?>
            <?php } ?>
        </table>

        <br>
        <table style="width:100%">
            <tr>
                <td style="width:50%">Petugas,</td>
                <td style="width:50%">Jumlah <?=number_format($total)?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>(<?=$petugas?>)</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2" style="font-size: 18px !important;">* Bukti ini bukan merupakan bukti pembayaran</td>
            </tr>
            <tr>
                <td colspan="2" style="font-size: 18px !important;">* Bukti ini digunakan untuk pengambilan hasil</td>
            </tr>
        </table>
    </div>
</body>

</html>
