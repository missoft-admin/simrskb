<div class="modal in" id="berkas-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:70%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Data Kunjungan Pasien</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="">No. Medrec</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="modal-nomedrec" placeholder="No. Medrec" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="">Nama Pasien</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="modal-namapasien" placeholder="Nama Pasien" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="">Tanggal Lahir</label>
									<div class="col-md-8">
										<input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="modal-tanggallahir" placeholder="Tanggal Lahir" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for=""></label>
									<div class="col-md-8">
										<button class="btn btn-success text-uppercase" type="button" id="btnFilterModal" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="">DPJP</label>
									<div class="col-md-8">
										<select class="select2 form-control" id="modal-dpjp" style="width: 100%;" data-placeholder="Dokter Penanggung Jawab">
                                            <option value="">Pilih Opsi</option>
                                            <?php foreach(get_all('mdokter', array('status' => 1)) as $row) { ?>
                                                <option value="<?= $row->id; ?>"><?= $row->nama; ?></option>
                                            <? } ?>
                                        </select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="">Tanggal Kunjungan</label>
									<div class="col-md-8">
										<input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="modal-tanggalkunjungan" placeholder="Tanggal Kunjungan" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="">Jenis Kunjungan</label>
									<div class="col-md-8">
										<select class="select2 form-control" id="modal-jeniskunjungan" style="width: 100%;" data-placeholder="Jenis Kunjungan">
                                            <option value="">Pilih Opsi</option>
                                            <option value="1">Rawat Jalan</option>
                                            <option value="2">Instalasi Gawat Darurat (IGD)</option>
                                            <option value="3">Rawat Inap</option>
                                            <option value="4">One Day Surgery (ODS)</option>
                                        </select>
									</div>
								</div>
							</div>
						</div>
					</div>

					<hr>

					<table width="100%" class="table table-bordered table-striped" id="datatable-pasien">
						<thead>
							<tr>
								<th>No. Medrec</th>
								<th>Nama Pasien</th>
								<th>Tanggal Lahir</th>
								<th>Tanggal Kunjungan</th>
								<th>Tipe</th>
								<th>Dokter</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".select2").select2();

        $("#btnFilterModal").click(function() {
            var nomedrec = $("#modal-nomedrec").val();
            var namapasien = $("#modal-namapasien").val();
            var tanggallahir = $("#modal-tanggallahir").val();
            var dpjp = $("#modal-dpjp option:selected").val();
            var tanggalkunjungan = $("#modal-tanggalkunjungan").val();
            var jeniskunjungan = $("#modal-jeniskunjungan option:selected").val();

            $('#datatable-pasien').DataTable().destroy();
            getBerkasPasien(nomedrec, namapasien, tanggallahir, dpjp, tanggalkunjungan, jeniskunjungan);
        });
    });
    
    function getBerkasPasien(nomedrec, namapasien, tanggallahir, dpjp, tanggalkunjungan, jeniskunjungan) {
		$('#datatable-pasien').DataTable({
			"pageLength": 6,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}trm_pengajuan_informasi/getBerkasPasien',
				type: "POST",
				dataType: 'json',
				data: {
					nomedrec: nomedrec,
                    namapasien: namapasien,
                    tanggallahir: tanggallahir,
                    dpjp: dpjp,
                    tanggalkunjungan: tanggalkunjungan,
                    jeniskunjungan: jeniskunjungan,
				}
			},
			"columnDefs": [
				{ "width": "10%", "targets": 0, "orderable": true },
                { "width": "10%", "targets": 1, "orderable": true },
                { "width": "10%", "targets": 2, "orderable": true },
                { "width": "10%", "targets": 3, "orderable": true },
                { "width": "10%", "targets": 4, "orderable": true },
                { "width": "10%", "targets": 5, "orderable": true },
			]
		});
	}
</script>