<?php echo ErrorSuccess($this->session)?>
<?php
    if ($error != '') {
        echo ErrorMessage($error);
    }
?>

<style>
	.control-label {
		text-align: left !important;
	}
</style>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
		<ul class="block-options">
			<li>
				<a href="{base_url}trm_pengajuan_informasi/index" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
	</div>
	<div class="block-content">

		<hr style="margin-top:-10px">

		<div class="col-12">
			<?php echo form_open('trm_pengajuan_informasi/save', 'class="form-horizontal" id="form-work"') ?>

			<h4 style="margin-bottom: 15px;">Data Pemohon</h4>

			<div class="form-group">
				<label class="col-md-2 control-label" for="">Tanggal Pengajuan</label>
				<div class="col-md-10">
					<input class="form-control js-datepicker" type="text" name="tanggal" id="tanggal_pengajuan" placeholder="Tanggal Pengajuan" value="{tanggal}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-2 control-label" for="">Nama Pemohon</label>
				<div class="col-md-10">
					<input class="form-control" type="text" name="nama_pemohon" placeholder="Nama Pemohon" value="{nama_pemohon}">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="">Keterangan</label>
				<div class="col-md-10">
					<textarea class="form-control summernote js-summernote-custom-height" placeholder="Keterangan" name="keterangan" rows="1">{keterangan}</textarea>
				</div>
			</div>

			<hr>

			<div id="formDataKunjungan">
				<h4>Pemilihan Data Kunjungan</h4>
				<div class="form-group" style="margin-bottom: 0px; padding-bottom:0px;">
					<label class="col-md-2 control-label" for="nama">Data Pasien</label>
					<div class="col-md-8" style="padding-right: 10px;">
						<select name="idpasien" id="idpasien" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach($list_berkas as $row) { ?>
								<option value="<?= $row->id; ?>"
									data-idberkas='<?= $row->id; ?>'
									data-nomedrec='<?= $row->no_medrec; ?>'
									data-namapasien='<?= $row->nama_pasien; ?>'
									data-tanggalkunjungan='<?= $row->tanggal_kunjungan; ?>'
									data-layanan='<?= GetJenisLayananBerkas($row->layanan); ?>'
									data-poliklinikkelas='<?= $row->poliklinik_kelas; ?>'
									data-namadokter='<?= $row->nama_dokter; ?>'
								><?= $row->no_medrec; ?> - <?= $row->nama_pasien; ?> - <?= GetJenisLayananBerkas($row->layanan); ?> - <?= $row->poliklinik_kelas; ?></option>
							<? } ?>
						</select>
					</div>
					<div class="col-md-2" style="padding-left: 0px;">
						<button type="button" class="btn btn-success" id="btnSearch" data-toggle="modal" data-target="#berkas-modal" style="width:100%;">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
				
				<br>

				<div class="col-12 row">
					<div class="col-md-6">
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="">No. Medrec</label>
							<div class="col-md-8">
								<input class="form-control clear" type="text" id="nomedrec" disabled value="">
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="">Tanggal Kunjungan</label>
							<div class="col-md-8">
								<input class="form-control clear" type="text" id="tanggal_kunjungan" disabled value="">
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="">Poliklinik / Kelas</label>
							<div class="col-md-8">
								<input class="form-control clear" type="text" id="poliklinik_kelas" disabled value="">
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="">Jenis Pengajuan</label>
							<div class="col-md-8">
								<select class="select2 form-control" id="jenis_pengajuan" style="width: 100%;" data-placeholder="">
									<option value="">Pilih Opsi</option>
									<?php foreach(get_all('mpengajuan_skd', array('status' => 1)) as $row) { ?>
										<option value="<?= $row->id; ?>"><?= $row->nama; ?></option>
									<? } ?>
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="">Nominal Tarif</label>
							<div class="col-md-8">
								<input class="form-control clear" type="text" id="biaya" placeholder="" readonly value="">
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="">
								<button type="button" class="btn btn-success" id="addDataKunjungan" style="width:100%;">
									<i class="fa fa-plus"></i> Tambahkan
								</button>
							</label>
							<div class="col-md-8">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="">Nama Pasien</label>
							<div class="col-md-8">
								<input class="form-control clear" type="text" id="nama_pasien" disabled value="">
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="">Layanan</label>
							<div class="col-md-8">
								<input class="form-control clear" type="text" id="layanan" disabled value="">
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="">Dokter</label>
							<div class="col-md-8">
								<input class="form-control clear" type="text" id="nama_dokter" disabled value="">
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="">Pengajuan Ke</label>
							<div class="col-md-8">
								<input class="form-control clear" type="text" id="pengajuan_ke" placeholder="" readonly value="">
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-4 control-label" for="">Estimasi Selesai</label>
							<div class="col-md-8">
								<input class="form-control js-datepicker clear" type="text" id="estimasi_selesai" placeholder="" value="">
							</div>
						</div>
					</div>
				</div>

				<hr>
			</div>

			<div class="col-12">
				<table class="table table-bordered" id="detailPengajuan">
					<thead>
						<tr>
							<td>No. Medrec</td>
							<td>Nama Pasien</td>
							<td>Tanggal Kunjungan</td>
							<td>Jenis Kunjungan</td>
							<td>Dokter</td>
							<td>Jenis Pengajuan</td>
							<td>Estimasi Selesai</td>
							<td>Nominal</td>
							<td>Aksi</td>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>

			<input type="hidden" id="idberkas" value="">
			<input type="hidden" id="idsetting" value="">
			<input type="hidden" id="group_diskon_all" value="">
			<input type="hidden" id="tarif_rs" value="">
			<input type="hidden" id="tarif_dokter" value="">

			<hr>

			<button type="submit" class="btn btn-success" id="btnSubmit" style="float: right;">
				<i class="fa fa-save"></i> Simpan
			</button>

			<br><br><br>
			
			<?php echo form_hidden('idtemp', $idtemp) ?>
			<?php echo form_hidden('id', $id) ?>
			<?php echo form_close() ?>
		</div>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script>
	const idtransaksi = '{idtemp}';
	const isReadonly = Boolean('{isReadonly}');
	
    $(document).ready(function() {
		loadDataKunjungan(idtransaksi);

		if (isReadonly) {
			$('input, select, #btnSearch').attr('disabled', 'disabled');
			$('#btnSubmit, #addDataKunjungan, #formDataKunjungan').hide();
			$('.summernote').summernote('disable');
		}

		$(document).on("click", ".selectBerkas", function() {
			var idberkas = $(this).data('idberkas');
			var nomedrec = $(this).data('nomedrec');
			var namapasien = $(this).data('namapasien');
			var tanggal_kunjungan = $(this).data('tanggalkunjungan');
			var layanan = $(this).data('layanan');
			var poliklinik_kelas = $(this).data('poliklinikkelas');
			var namadokter = $(this).data('namadokter');

			$("#idberkas").val(idberkas);
			$("#nomedrec").val(nomedrec);
			$("#nama_pasien").val(namapasien);
			$("#tanggal_kunjungan").val(tanggal_kunjungan);
			$("#layanan").val(layanan);
			$("#poliklinik_kelas").val(poliklinik_kelas);
			$("#nama_dokter").val(namadokter);
		});

		$("#idpasien").change(function() {
			var idberkas = $('option:selected', this).data('idberkas');
			var nomedrec = $('option:selected', this).data('nomedrec');
			var namapasien = $('option:selected', this).data('namapasien');
			var tanggal_kunjungan = $('option:selected', this).data('tanggalkunjungan');
			var layanan = $('option:selected', this).data('layanan');
			var poliklinik_kelas = $('option:selected', this).data('poliklinikkelas');
			var namadokter = $('option:selected', this).data('namadokter');

			$("#idberkas").val(idberkas);
			$("#nomedrec").val(nomedrec);
			$("#nama_pasien").val(namapasien);
			$("#tanggal_kunjungan").val(tanggal_kunjungan);
			$("#layanan").val(layanan);
			$("#poliklinik_kelas").val(poliklinik_kelas);
			$("#nama_dokter").val(namadokter);
		});

		$("#jenis_pengajuan").change(function() {
			var jenisPengajuan = $(this).val();
			var idberkas = $('#idberkas').val();
			var tanggalPengajuan = $('#tanggal_pengajuan').val();
			var tanggalKunjungan = $('#tanggal_kunjungan').val();

			if (jenisPengajuan != 0) {
				getSettingPengajuan(idberkas, jenisPengajuan, tanggalKunjungan, tanggalPengajuan);
			}
		});

		$("#addDataKunjungan").click(function() {
			var dataForm = {
				idberkas: $('#idberkas').val(),
				tanggal_kunjungan: $('#tanggal_kunjungan').val(),
				tanggal_pengajuan: $('#tanggal_pengajuan').val(),
				jenis_pengajuan: $('#jenis_pengajuan').val(),
				pengajuan_ke: $('#pengajuan_ke').val(),
				estimasi_selesai: $('#estimasi_selesai').val(),
				idsetting: $('#idsetting').val(),
				group_diskon_all: $('#group_diskon_all').val(),
				tarif_rs: $('#tarif_rs').val(),
				tarif_dokter: $('#tarif_dokter').val(),
				nominal_tarif: $('#biaya').val(),
			}

			if (validationSetDataKunjungan(dataForm)) {
				setDataKunjungan(dataForm);
			}
		});

		$(document).on("click", ".removeDataKunjungan", function() {
			var idkunjungan = $(this).data('id');

			swal({
				title: "Anda Yakin ?",
				text : "untuk menghapus data kunjungan ini ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				removeDataKunjungan(idkunjungan);
			});
		});
	});

	function loadDataKunjungan(idtransaksi) {
		$.ajax({
			url: '{site_url}trm_pengajuan_informasi/getDataKunjungan/' + idtransaksi,
			dataType: "json",
			success: function(data) {
				$('#detailPengajuan tbody').html('');

				renderingTableDataKunjungan(data);

				if (isReadonly != '') {
					$('.removeDataKunjungan').attr('disabled', 'disabled');
				}
			},
			error: function(data) {
				console.log(data);
			}
		});
	}

	function renderingTableDataKunjungan(data) {
		data.map((item) => {
			$('#detailPengajuan tbody').append(`
				<tr>
					<td>${item.no_medrec}</td>
					<td>${item.nama_pasien}</td>
					<td>${item.tanggal_kunjungan}</td>
					<td>${item.jenis_kunjungan}</td>
					<td>${item.nama_dokter}</td>
					<td>${item.jenis_pengajuan}</td>
					<td>${item.estimasi_selesai}</td>
					<td>${$.number(item.nominal)}</td>
					<td>
						<button type="button" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeDataKunjungan" data-id="${item.id}">
							<i class="fa fa-trash"></i>
						</button>
					</td>
				</tr>
			`);
		});
	}

	function getSettingPengajuan(idberkas, jenisPengajuan, tanggalKunjungan, tanggalPengajuan) {
		$.ajax({
			url: '{site_url}trm_pengajuan_informasi/getSettingPengajuan',
			method: "POST",
			dataType: "json",
			data: {
				"idberkas": idberkas,
				"jenis_pengajuan": jenisPengajuan,
				"tanggal_kunjungan": tanggalKunjungan,
				"tanggal_pengajuan": tanggalPengajuan,
			},
			success: function(data) {
				if (data.status_setting == 'NOT_FOUND') {
					$("#addDataKunjungan").hide();
					sweetAlert("Maaf...", "Data Setting Tarif Pengajuan tidak ditemukan!", "error");
				} else {
					$("#addDataKunjungan").show();
					$('#estimasi_selesai').val(data.tanggal_estimasi_selesai);
					$('#pengajuan_ke').val(data.nomor_urut_pengajuan);
					$('#idsetting').val(data.idsetting);
					$('#group_diskon_all').val(data.group_diskon_all);
					$('#tarif_rs').val(data.tarif_rs);
					$('#tarif_dokter').val(data.tarif_dokter);
					$('#biaya').val($.number(data.total_tarif));
				}
			},
			error: function(data) {
				console.log(data);
			}
		});
	}

	function removeDataKunjungan(idkunjungan) {
		$.ajax({
			url: '{site_url}trm_pengajuan_informasi/removeDataKunjungan/' + idkunjungan,
			dataType: "json",
			success: function(data) {
				loadDataKunjungan(idtransaksi);
			},
			error: function(data) {
				console.log(data);
			}
		});
	}

	function validationSetDataKunjungan(data) {
		if (data.idberkas == '') {
			sweetAlert("Maaf...", "Data Kunjungan Pasien belum dipilih!", "error");
			return false;
		}
		if (data.jenis_pengajuan == '') {
			sweetAlert("Maaf...", "Jenis Pengajuan harus diisi!", "error");
			return false;
		}

		return true;
	}

	function setDataKunjungan(item) {
		$.ajax({
			url: '{site_url}trm_pengajuan_informasi/setDataKunjungan',
			method: "POST",
			dataType: "json",
			data: {
				'idtransaksi': idtransaksi,
				'idberkas': item.idberkas,
				'tanggal_kunjungan': item.tanggal_kunjungan,
				'tanggal_pengajuan': item.tanggal_pengajuan,
				'jenis_pengajuan': item.jenis_pengajuan,
				'pengajuan_ke': item.pengajuan_ke,
				'estimasi_selesai': item.estimasi_selesai,
				'idsetting': item.idsetting,
				'group_diskon_all': item.group_diskon_all,
				'tarif_rs': item.tarif_rs,
				'tarif_dokter': item.tarif_dokter,
				'nominal_tarif': item.nominal_tarif,
			},
			success: function(data) {
				loadDataKunjungan(data.idtransaksi);
				resetFormInputDataKunjungan();
			},
			error: function(data) {
				console.log(data);
			}
		});
	}

	function resetFormInputDataKunjungan() {
		$('#jenis_pengajuan').select2('val', 0);
		$('.clear').val('');
	}
</script>
<?php $this->load->view('Trm_pengajuan_informasi/modal'); ?>