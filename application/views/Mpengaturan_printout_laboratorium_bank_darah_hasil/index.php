<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mpengaturan_printout_laboratorium_bank_darah_hasil" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<ul class="nav nav-tabs">
			<li class="<?= ($tab == 'FORMAT_1' ? 'active' : ''); ?>">
				<a href="{base_url}mpengaturan_printout_laboratorium_bank_darah_hasil/index/FORMAT_1">Format 1</a>
			</li>
			<li class="<?= ($tab == 'FORMAT_2' ? 'active' : ''); ?>">
				<a href="{base_url}mpengaturan_printout_laboratorium_bank_darah_hasil/index/FORMAT_2">Format 2</a>
			</li>
		</ul>

		<!-- Tab Content -->
    <div class="tab-content mt-2">
        <!-- Tab 1 Content -->
        <?php if ($tab == 'FORMAT_1') { ?>
            <?php echo form_open_multipart('mpengaturan_printout_laboratorium_bank_darah_hasil/save','class="form-horizontal push-10-t" id="form-work"') ?>

							<input type="hidden" id="tipe" name="tipe" value="FORMAT_1" required="" aria-required="true">

							<div class="form-group">
								<label class="col-md-12" for="nama_file">Nama File</label>
								<div class="col-md-12">
									<input type="text" class="form-control" id="nama_file" placeholder="Nama File" name="nama_file" value="{nama_file}" required="" aria-required="true">
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-12" for="">Logo Header</label>
										<div class="col-md-12">
											<div class="box">
												<input type="file" id="file-3" class="inputfile inputfile-3" style="display:none;" name="logo_header" value="{logo_header}" />
												<label for="file-3" class="btn-default">
													<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg>
													<span>Pilih File&hellip;</span>
												</label>
											</div>
										</div>
									</div>

									<?php if($logo_header != ''){ ?>
										<div class="form-group">
											<label class="col-md-12 control-label" for=""></label>
											<div class="col-md-12">
												<img class="img-fluid" src="{upload_path}pengaturan_printout_laboratorium_hasil/{logo_header}" style="width: 100%; height: 250px; object-fit: cover;"/>
											</div>
										</div>
									<?php } ?>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-12" for="">Logo Footer</label>
										<div class="col-md-12">
											<div class="box">
												<input type="file" id="file-2" class="inputfile inputfile-2" style="display:none;" name="logo_footer" value="{logo_footer}" />
												<label for="file-2" class="btn-default">
													<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg>
													<span>Pilih File&hellip;</span>
												</label>
											</div>
										</div>
									</div>

									<?php if($logo_footer != ''){ ?>
										<div class="form-group">
											<label class="col-md-12 control-label" for=""></label>
											<div class="col-md-12">
												<img class="img-fluid" src="{upload_path}pengaturan_printout_laboratorium_hasil/{logo_footer}" style="width: 100%; height: 250px; object-fit: cover;"/>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>

							<hr>

							<h5 style="margin-bottom: 10px;" for="">Judul Header</h5>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-12" for=""><i>Indonesia Version</i></label>
										<div class="col-md-12">
											<textarea class="form-control summernote js-summernote-custom-height" name="label_header" placeholder="Header Name (Indonesia Version)">{label_header}</textarea>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-12" for=""><i>English Version</i></label>
										<div class="col-md-12">
											<textarea class="form-control summernote js-summernote-custom-height" name="label_header_eng" placeholder="Header Name (English Version)">{label_header_eng}</textarea>
										</div>
									</div>
								</div>
							</div>

							<div id="section_header">
								<h4 class="text-primary" style="margin-bottom: 10px;" for="">Header Data</h4>
								<div class="row">
									<!-- 'tampilkan_noregister' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Nomor Register</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_noregister" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_noregister == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_noregister == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_noregister" data-label-eng-id="label_noregister_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_nomedrec' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Nomor Medrec</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_nomedrec" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_nomedrec == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_nomedrec == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_nomedrec" data-label-eng-id="label_nomedrec_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_nama' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Nama Pasien</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_nama" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_nama == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_nama == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_nama" data-label-eng-id="label_nama_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_kelompok_pasien' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Kelompok Pasien</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_kelompok_pasien" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_kelompok_pasien == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_kelompok_pasien == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_kelompok_pasien" data-label-eng-id="label_kelompok_pasien_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_nama_asuransi' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Nama Asuransi</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_nama_asuransi" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_nama_asuransi == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_nama_asuransi == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_nama_asuransi" data-label-eng-id="label_nama_asuransi_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_alamat' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Alamat</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_alamat" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_alamat == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_alamat == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_alamat" data-label-eng-id="label_alamat_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_dokter_perujuk' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Dokter Perujuk</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_dokter_perujuk" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_dokter_perujuk == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_dokter_perujuk == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_dokter_perujuk" data-label-eng-id="label_dokter_perujuk_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_nomor_laboratorium' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Nomor Laboratorium</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_nomor_laboratorium" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_nomor_laboratorium == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_nomor_laboratorium == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_nomor_laboratorium" data-label-eng-id="label_nomor_laboratorium_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_tanggal_lahir' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Tanggal Lahir</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_tanggal_lahir" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_tanggal_lahir == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_tanggal_lahir == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_tanggal_lahir" data-label-eng-id="label_tanggal_lahir_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_umur' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Umur</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_umur" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_umur == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_umur == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_umur" data-label-eng-id="label_umur_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_rujukan' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Rujukan</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_rujukan" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_rujukan == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_rujukan == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_rujukan" data-label-eng-id="label_rujukan_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_barcode' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Barcode</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_barcode" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_barcode == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_barcode == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_barcode" data-label-eng-id="label_barcode_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>
								</div>
							</div>

							<div id="section_body" style="margin-top: 20px;">
								<h4 class="text-primary" style="margin-bottom: 10px;" for="">Isi Data</h4>
								<div class="row">
									<!-- 'tampilkan_nama_pemeriksaan' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Nama Pemeriksaan</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_nama_pemeriksaan" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_nama_pemeriksaan == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_nama_pemeriksaan == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_nama_pemeriksaan" data-label-eng-id="label_nama_pemeriksaan_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_hasil' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Hasil</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_hasil" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_hasil == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_hasil == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_hasil" data-label-eng-id="label_hasil_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_flag' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Flag</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_flag" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_flag == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_flag == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_flag" data-label-eng-id="label_flag_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_flag_tidak_normal' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Flag Tidak Normal</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_flag_tidak_normal" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_flag_tidak_normal == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_flag_tidak_normal == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_flag_tidak_normal" data-label-eng-id="label_flag_tidak_normal_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_flag_kritis' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Flag Kritis</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_flag_kritis" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_flag_kritis == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_flag_kritis == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_flag_kritis" data-label-eng-id="label_flag_kritis_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>
								</div>
							</div>

							<div id="section_footer" style="margin-top: 20px;">
								<h4 class="text-primary" style="margin-bottom: 10px;" for="">Footer</h4>
								<div class="row">
									<!-- 'tampilkan_tanda_tangan' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Tanda Tangan</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_tanda_tangan" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_tanda_tangan == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_tanda_tangan == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_tanda_tangan" data-label-eng-id="label_tanda_tangan_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_waktu_input_order' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Waktu Input Order</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_waktu_input_order" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_waktu_input_order == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_waktu_input_order == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_waktu_input_order" data-label-eng-id="label_waktu_input_order_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_waktu_sampling' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Waktu Sampling</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_waktu_sampling" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_waktu_sampling == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_waktu_sampling == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_waktu_sampling" data-label-eng-id="label_waktu_sampling_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_waktu_input_hasil' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Waktu Input Hasil</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_waktu_input_hasil" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_waktu_input_hasil == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_waktu_input_hasil == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_waktu_input_hasil" data-label-eng-id="label_waktu_input_hasil_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_waktu_cetak' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Waktu Cetak</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_waktu_cetak" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_waktu_cetak == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_waktu_cetak == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_waktu_cetak" data-label-eng-id="label_waktu_cetak_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_jumlah_cetak' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Jumlah Cetak</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_jumlah_cetak" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_jumlah_cetak == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_jumlah_cetak == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_jumlah_cetak" data-label-eng-id="label_jumlah_cetak_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_user_input_order' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">User Input Order</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_user_input_order" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_user_input_order == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_user_input_order == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_user_input_order" data-label-eng-id="label_user_input_order_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_user_sampling' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">User Sampling</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_user_sampling" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_user_sampling == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_user_sampling == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_user_sampling" data-label-eng-id="label_user_sampling_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_user_input_hasil' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">User Input Hasil</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_user_input_hasil" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_user_input_hasil == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_user_input_hasil == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_user_input_hasil" data-label-eng-id="label_user_input_hasil_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_user_cetak' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">User Cetak</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_user_cetak" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_user_cetak == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_user_cetak == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_user_cetak" data-label-eng-id="label_user_cetak_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>
								</div>
							</div>

							<hr>

							<h5 style="margin-bottom: 10px;" for="">Footer Notes</h5>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-12" for=""><i>Indonesia Version</i></label>
										<div class="col-md-12">
											<textarea class="form-control summernote js-summernote-custom-height" name="footer_notes" placeholder="Header Name (Indonesia Version)">{footer_notes}</textarea>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-12" for=""><i>English Version</i></label>
										<div class="col-md-12">
											<textarea class="form-control summernote js-summernote-custom-height" name="footer_notes_eng" placeholder="Header Name (English Version)">{footer_notes_eng}</textarea>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-12 text-right">
									<button class="btn btn-success" type="submit">Simpan</button>
									<a href="{base_url}mpengaturan_printout_laboratorium_bank_darah_hasil" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
								</div>
							</div>

							<br /><br />
						<?php echo form_close() ?>
        <? } ?>

        <!-- Tab 2 Content -->
        <?php if ($tab == 'FORMAT_2') { ?>
            <?php echo form_open_multipart('mpengaturan_printout_laboratorium_bank_darah_hasil/save','class="form-horizontal push-10-t" id="form-work"') ?>

							<input type="hidden" id="tipe" name="tipe" value="FORMAT_2" required="" aria-required="true">

							<div class="form-group">
								<label class="col-md-12" for="nama_file">Nama File</label>
								<div class="col-md-12">
									<input type="text" class="form-control" id="nama_file" placeholder="Nama File" name="nama_file" value="{nama_file}" required="" aria-required="true">
								</div>
							</div>

							<h5 style="margin-bottom: 10px;" for="">LOGO</h5>
							<div class="row">
								<div class="col-md-1">
									<?php if($logo != ''){ ?>
									<div class="form-group">
										<div class="col-md-12">
											<img class="img-fluid" src="{upload_path}pengaturan_printout_laboratorium_hasil/{logo}" width="100px" height="100px" style="object-fit: cover; border-radius: 10px; border: 1px solid #DEDEDE;"/>
										</div>
									</div>
									<?php } ?>
								</div>
								<div class="col-md-11">
									<div class="form-group">
										<div class="col-md-12">
											<div class="box">
												<input type="file" id="file-3" class="inputfile inputfile-3" style="display:none;" name="logo" value="{logo}" />
												<label for="file-3" class="btn-default">
													<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg>
													<span>Pilih File&hellip;</span>
												</label>
											</div>
										</div>
									</div>
								</div>
							</div>

							<hr>

							<h5 style="margin-bottom: 10px;" for="">Alamat</h5>
							<div class="row">
								<div class="col-md-12">
									<textarea class="form-control summernote js-summernote-custom-height" name="alamat" placeholder="Alamat">{alamat}</textarea>
								</div>
							</div>

							<h5 style="margin-bottom: 10px;" for="">Phone</h5>
							<div class="row">
								<div class="col-md-12">
									<textarea class="form-control summernote js-summernote-custom-height" name="phone" placeholder="Phone">{phone}</textarea>
								</div>
							</div>

							<h5 style="margin-bottom: 10px;" for="">Website</h5>
							<div class="row">
								<div class="col-md-12">
									<textarea class="form-control summernote js-summernote-custom-height" name="website" placeholder="Website">{website}</textarea>
								</div>
							</div>

							<h5 style="margin-bottom: 10px;" for="">Judul Header</h5>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-12" for=""><i>Indonesia Version</i></label>
										<div class="col-md-12">
											<textarea class="form-control summernote js-summernote-custom-height" name="label_header" placeholder="Header Name (Indonesia Version)">{label_header}</textarea>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-12" for=""><i>English Version</i></label>
										<div class="col-md-12">
											<textarea class="form-control summernote js-summernote-custom-height" name="label_header_eng" placeholder="Header Name (English Version)">{label_header_eng}</textarea>
										</div>
									</div>
								</div>
							</div>

							<div id="section_header">
								<h4 class="text-primary" style="margin-bottom: 10px;" for="">Header Data</h4>
								<div class="row">
									<!-- 'tampilkan_noregister' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Nomor Register</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_noregister" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_noregister == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_noregister == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_noregister" data-label-eng-id="label_noregister_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_nomedrec' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Nomor Medrec</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_nomedrec" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_nomedrec == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_nomedrec == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_nomedrec" data-label-eng-id="label_nomedrec_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_nama' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Nama Pasien</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_nama" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_nama == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_nama == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_nama" data-label-eng-id="label_nama_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_kelompok_pasien' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Kelompok Pasien</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_kelompok_pasien" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_kelompok_pasien == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_kelompok_pasien == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_kelompok_pasien" data-label-eng-id="label_kelompok_pasien_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_nama_asuransi' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Nama Asuransi</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_nama_asuransi" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_nama_asuransi == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_nama_asuransi == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_nama_asuransi" data-label-eng-id="label_nama_asuransi_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_alamat' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Alamat</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_alamat" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_alamat == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_alamat == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_alamat" data-label-eng-id="label_alamat_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_dokter_perujuk' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Dokter Perujuk</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_dokter_perujuk" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_dokter_perujuk == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_dokter_perujuk == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_dokter_perujuk" data-label-eng-id="label_dokter_perujuk_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_nomor_laboratorium' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Nomor Laboratorium</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_nomor_laboratorium" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_nomor_laboratorium == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_nomor_laboratorium == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_nomor_laboratorium" data-label-eng-id="label_nomor_laboratorium_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_tanggal_lahir' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Tanggal Lahir</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_tanggal_lahir" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_tanggal_lahir == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_tanggal_lahir == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_tanggal_lahir" data-label-eng-id="label_tanggal_lahir_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_umur' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Umur</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_umur" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_umur == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_umur == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_umur" data-label-eng-id="label_umur_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_rujukan' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Rujukan</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_rujukan" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_rujukan == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_rujukan == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_rujukan" data-label-eng-id="label_rujukan_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_barcode' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Barcode</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_barcode" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_barcode == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_barcode == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_barcode" data-label-eng-id="label_barcode_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>
								</div>
							</div>

							<div id="section_body" style="margin-top: 20px;">
								<h4 class="text-primary" style="margin-bottom: 10px;" for="">Isi Data</h4>
								<div class="row">
									<!-- 'tampilkan_nama_pemeriksaan' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Nama Pemeriksaan</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_nama_pemeriksaan" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_nama_pemeriksaan == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_nama_pemeriksaan == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_nama_pemeriksaan" data-label-eng-id="label_nama_pemeriksaan_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_hasil' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Hasil</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_hasil" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_hasil == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_hasil == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_hasil" data-label-eng-id="label_hasil_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_flag' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Flag</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_flag" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_flag == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_flag == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_flag" data-label-eng-id="label_flag_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_flag_tidak_normal' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Flag Tidak Normal</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_flag_tidak_normal" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_flag_tidak_normal == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_flag_tidak_normal == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_flag_tidak_normal" data-label-eng-id="label_flag_tidak_normal_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_flag_kritis' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Flag Kritis</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_flag_kritis" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_flag_kritis == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_flag_kritis == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_flag_kritis" data-label-eng-id="label_flag_kritis_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>
								</div>
							</div>

							<div id="section_footer" style="margin-top: 20px;">
								<h4 class="text-primary" style="margin-bottom: 10px;" for="">Footer</h4>
								<div class="row">
									<!-- 'tampilkan_tanda_tangan' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Tanda Tangan</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_tanda_tangan" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_tanda_tangan == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_tanda_tangan == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_tanda_tangan" data-label-eng-id="label_tanda_tangan_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_waktu_input_order' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Waktu Input Order</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_waktu_input_order" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_waktu_input_order == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_waktu_input_order == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_waktu_input_order" data-label-eng-id="label_waktu_input_order_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_waktu_sampling' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Waktu Sampling</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_waktu_sampling" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_waktu_sampling == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_waktu_sampling == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_waktu_sampling" data-label-eng-id="label_waktu_sampling_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_waktu_input_hasil' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Waktu Input Hasil</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_waktu_input_hasil" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_waktu_input_hasil == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_waktu_input_hasil == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_waktu_input_hasil" data-label-eng-id="label_waktu_input_hasil_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_waktu_cetak' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Waktu Cetak</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_waktu_cetak" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_waktu_cetak == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_waktu_cetak == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_waktu_cetak" data-label-eng-id="label_waktu_cetak_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_jumlah_cetak' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">Jumlah Cetak</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_jumlah_cetak" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_jumlah_cetak == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_jumlah_cetak == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_jumlah_cetak" data-label-eng-id="label_jumlah_cetak_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_user_input_order' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">User Input Order</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_user_input_order" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_user_input_order == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_user_input_order == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_user_input_order" data-label-eng-id="label_user_input_order_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_user_sampling' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">User Sampling</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_user_sampling" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_user_sampling == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_user_sampling == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_user_sampling" data-label-eng-id="label_user_sampling_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_user_input_hasil' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">User Input Hasil</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_user_input_hasil" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_user_input_hasil == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_user_input_hasil == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_user_input_hasil" data-label-eng-id="label_user_input_hasil_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>

									<!-- 'tampilkan_user_cetak' -->
									<div class="col-md-3">
											<div class="form-group">
													<label class="col-md-12" for="">User Cetak</label>
													<div class="col-md-12">
															<div class="input-group" style="display: flex;">
																	<select name="tampilkan_user_cetak" class="js-select2 form-control" data-placeholder="Pilih Opsi">
																			<option value="1" <?=($tampilkan_user_cetak == 1 ? 'selected="selected"':'')?>>Tampilkan</option>
																			<option value="0" <?=($tampilkan_user_cetak == 0 ? 'selected="selected"':'')?>>Tidak Tampilkan</option>
																	</select>
																	<div class="input-group-append">
																			<button type="button" class="btn btn-success edit-button" data-toggle="modal" data-target="#modalEditLabel" data-label-id="label_user_cetak" data-label-eng-id="label_user_cetak_eng">
																					<i class="fa fa-pencil"></i>
																			</button>
																	</div>
															</div>
													</div>
											</div>
									</div>
								</div>
							</div>

							<hr>

							<h5 style="margin-bottom: 10px;" for="">Footer Notes</h5>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-12" for=""><i>Indonesia Version</i></label>
										<div class="col-md-12">
											<textarea class="form-control summernote js-summernote-custom-height" name="footer_notes" placeholder="Header Name (Indonesia Version)">{footer_notes}</textarea>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-md-12" for=""><i>English Version</i></label>
										<div class="col-md-12">
											<textarea class="form-control summernote js-summernote-custom-height" name="footer_notes_eng" placeholder="Header Name (English Version)">{footer_notes_eng}</textarea>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-12 text-right">
									<button class="btn btn-success" type="submit">Simpan</button>
									<a href="{base_url}mpengaturan_printout_laboratorium_bank_darah_hasil" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
								</div>
							</div>

							<br /><br />
						<?php echo form_close() ?>
        </div>
				<? } ?>
    </div>
	</div>
</div>

<!-- Modal Edit Label -->
<div class="modal fade" id="modalEditLabel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="block-options">
                    <li>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </li>
                </ul>
                <h5 class="modal-title" id="exampleModalLabel">Atur Label</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-12" for=""><i>Indonesia Version</i></label>
													<div class="col-md-12">
														<textarea class="form-control summernote js-summernote-custom-height" id="modalLabelIndo"></textarea>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="col-md-12" for=""><i>English Version</i></label>
													<div class="col-md-12">
														<textarea class="form-control summernote js-summernote-custom-height" id="modalLabelEng"></textarea>
													</div>
												</div>
											</div>
										</div>
                </div>
                <div class="modal-footer">
										<button id="modalSaveBtn" class="btn btn-sm btn-success" type="button" data-dismiss="modal">Simpan</button>
										<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="label_noregister" value="{label_noregister}">
<input type="hidden" id="label_noregister_eng" value="{label_noregister_eng}">
<input type="hidden" id="label_nomedrec" value="{label_nomedrec}">
<input type="hidden" id="label_nomedrec_eng" value="{label_nomedrec_eng}">
<input type="hidden" id="label_nama" value="{label_nama}">
<input type="hidden" id="label_nama_eng" value="{label_nama_eng}">
<input type="hidden" id="label_kelompok_pasien" value="{label_kelompok_pasien}">
<input type="hidden" id="label_kelompok_pasien_eng" value="{label_kelompok_pasien_eng}">
<input type="hidden" id="label_nama_asuransi" value="{label_nama_asuransi}">
<input type="hidden" id="label_nama_asuransi_eng" value="{label_nama_asuransi_eng}">
<input type="hidden" id="label_alamat" value="{label_alamat}">
<input type="hidden" id="label_alamat_eng" value="{label_alamat_eng}">
<input type="hidden" id="label_dokter_perujuk" value="{label_dokter_perujuk}">
<input type="hidden" id="label_dokter_perujuk_eng" value="{label_dokter_perujuk_eng}">
<input type="hidden" id="label_nomor_laboratorium" value="{label_nomor_laboratorium}">
<input type="hidden" id="label_nomor_laboratorium_eng" value="{label_nomor_laboratorium_eng}">
<input type="hidden" id="label_tanggal_lahir" value="{label_tanggal_lahir}">
<input type="hidden" id="label_tanggal_lahir_eng" value="{label_tanggal_lahir_eng}">
<input type="hidden" id="label_umur" value="{label_umur}">
<input type="hidden" id="label_umur_eng" value="{label_umur_eng}">
<input type="hidden" id="label_rujukan" value="{label_rujukan}">
<input type="hidden" id="label_rujukan_eng" value="{label_rujukan_eng}">
<input type="hidden" id="label_barcode" value="{label_barcode}">
<input type="hidden" id="label_barcode_eng" value="{label_barcode_eng}">
<input type="hidden" id="label_nama_pemeriksaan" value="{label_nama_pemeriksaan}">
<input type="hidden" id="label_nama_pemeriksaan_eng" value="{label_nama_pemeriksaan_eng}">
<input type="hidden" id="label_hasil" value="{label_hasil}">
<input type="hidden" id="label_hasil_eng" value="{label_hasil_eng}">
<input type="hidden" id="label_flag" value="{label_flag}">
<input type="hidden" id="label_flag_eng" value="{label_flag_eng}">
<input type="hidden" id="label_flag_tidak_normal" value="{label_flag_tidak_normal}">
<input type="hidden" id="label_flag_tidak_normal_eng" value="{label_flag_tidak_normal_eng}">
<input type="hidden" id="label_flag_kritis" value="{label_flag_kritis}">
<input type="hidden" id="label_flag_kritis_eng" value="{label_flag_kritis_eng}">
<input type="hidden" id="label_satuan" value="{label_satuan}">
<input type="hidden" id="label_satuan_eng" value="{label_satuan_eng}">
<input type="hidden" id="label_nilai_normal" value="{label_nilai_normal}">
<input type="hidden" id="label_nilai_normal_eng" value="{label_nilai_normal_eng}">
<input type="hidden" id="label_nilai_kritis" value="{label_nilai_kritis}">
<input type="hidden" id="label_nilai_kritis_eng" value="{label_nilai_kritis_eng}">
<input type="hidden" id="label_metode" value="{label_metode}">
<input type="hidden" id="label_metode_eng" value="{label_metode_eng}">
<input type="hidden" id="label_sumber_spesimen" value="{label_sumber_spesimen}">
<input type="hidden" id="label_sumber_spesimen_eng" value="{label_sumber_spesimen_eng}">
<input type="hidden" id="label_keterangan" value="{label_keterangan}">
<input type="hidden" id="label_keterangan_eng" value="{label_keterangan_eng}">
<input type="hidden" id="label_petugas_profesi" value="{label_petugas_profesi}">
<input type="hidden" id="label_petugas_profesi_eng" value="{label_petugas_profesi_eng}">
<input type="hidden" id="label_catatan" value="{label_catatan}">
<input type="hidden" id="label_catatan_eng" value="{label_catatan_eng}">
<input type="hidden" id="label_interpretasi_hasil" value="{label_interpretasi_hasil}">
<input type="hidden" id="label_interpretasi_hasil_eng" value="{label_interpretasi_hasil_eng}">
<input type="hidden" id="label_penanggung_jawab" value="{label_penanggung_jawab}">
<input type="hidden" id="label_penanggung_jawab_eng" value="{label_penanggung_jawab_eng}">
<input type="hidden" id="label_tanda_tangan" value="{label_tanda_tangan}">
<input type="hidden" id="label_tanda_tangan_eng" value="{label_tanda_tangan_eng}">
<input type="hidden" id="label_waktu_input_order" value="{label_waktu_input_order}">
<input type="hidden" id="label_waktu_input_order_eng" value="{label_waktu_input_order_eng}">
<input type="hidden" id="label_waktu_sampling" value="{label_waktu_sampling}">
<input type="hidden" id="label_waktu_sampling_eng" value="{label_waktu_sampling_eng}">
<input type="hidden" id="label_waktu_input_hasil" value="{label_waktu_input_hasil}">
<input type="hidden" id="label_waktu_input_hasil_eng" value="{label_waktu_input_hasil_eng}">
<input type="hidden" id="label_waktu_cetak" value="{label_waktu_cetak}">
<input type="hidden" id="label_waktu_cetak_eng" value="{label_waktu_cetak_eng}">
<input type="hidden" id="label_jumlah_cetak" value="{label_jumlah_cetak}">
<input type="hidden" id="label_jumlah_cetak_eng" value="{label_jumlah_cetak_eng}">
<input type="hidden" id="label_user_input_order" value="{label_user_input_order}">
<input type="hidden" id="label_user_input_order_eng" value="{label_user_input_order_eng}">
<input type="hidden" id="label_user_sampling" value="{label_user_sampling}">
<input type="hidden" id="label_user_sampling_eng" value="{label_user_sampling_eng}">
<input type="hidden" id="label_user_input_hasil" value="{label_user_input_hasil}">
<input type="hidden" id="label_user_input_hasil_eng" value="{label_user_input_hasil_eng}">
<input type="hidden" id="label_user_cetak" value="{label_user_cetak}">
<input type="hidden" id="label_user_cetak_eng" value="{label_user_cetak_eng}">

<input type="hidden" id="labelId" value="">
<input type="hidden" id="labelEngId" value="">

<script src="{js_path}core/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
		$('.edit-button').on('click', function() {
        var labelId = $(this).data('label-id');
        var labelEngId = $(this).data('label-eng-id');

        // Set modal textarea content based on labelId and labelEngId
        var textareaContent = $('#' + labelId).val();
        var textareaEngContent = $('#' + labelEngId).val();

        $('#labelId').val(labelId);
        $('#labelEngId').val(labelEngId);
        $('#modalLabelIndo').summernote('code', textareaContent);
        $('#modalLabelEng').summernote('code', textareaEngContent);
    });

    $('#modalSaveBtn').on('click', function() {
        // Extract textarea content for both languages
        var tipe = $('#tipe').val();
        var labelId = $('#labelId').val();
        var labelEngId = $('#labelEngId').val();
        var textareaContent = $('#modalLabelIndo').summernote('code');
        var textareaEngContent = $('#modalLabelEng').summernote('code');

        // Set content back to original textareas
        $('#' + labelId).val(textareaContent);
        $('#' + labelEngId).val(textareaEngContent);

        // AJAX submit
        $.ajax({
            url: '{base_url}mpengaturan_printout_laboratorium_bank_darah_hasil/updateDataLabel', // Ganti dengan URL endpoint penyimpanan data di server Anda
            type: 'POST',
            data: {
                tipe: tipe,
                labelId: labelId,
                labelEngId: labelEngId,
                textareaContent: textareaContent,
                textareaEngContent: textareaEngContent
                // Anda dapat menambahkan data tambahan yang perlu disimpan di sini
            },
            success: function(response) {
                // Handle success response (jika diperlukan)
                console.log(response);

								// Refresh halaman setelah menerima respons sukses
								location.reload();
            },
            error: function(error) {
                // Handle error response (jika diperlukan)
                console.error(error);
            }
        });
    });
	});
</script>