<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1902'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_general()"><i class="fa fa-bell"></i> General</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1903'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_event"  onclick="load_tab2()"><i class="fa fa-check-square-o"></i> Logic</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1904'))){ ?>
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab_3" "><i class="fa fa-sort-alpha-asc"></i> Label</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1905'))){ ?>
		<li class="<?=($tab=='4'?'active':'')?>">
			<a href="#tab_4" onclick="set_tab_4()"><i class="si si-book-open"></i> Content</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1906'))){ ?>
		<li class="<?=($tab=='5'?'active':'')?>">
			<a href="#tab_5"  onclick="set_tab_5()"><i class="si si-wallet"></i> Default Harga</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1907'))){ ?>
		<li class="<?=($tab=='6'?'active':'')?>">
			<a href="#tab_6" ><i class="fa fa-percent"></i> Default Persentase</a>
		</li>
		<?}?>
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('1902'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_info">
			
			<?php echo form_open('setting_rencana_biaya/save_assesmen','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12" hidden>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_header">Judul Header<span style="color:red;">*</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_header" name="judul_header" value="{judul_header}" placeholder="Header">
						</div>
					</div>
				</div>
				<div class="col-md-12" hidden>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_footer">Judul Footer<span style="color:red;">*</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_footer" name="judul_footer" value="{judul_footer}" placeholder="Footer">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_edit_catatan">Izin Edit Catatan</label>
						<div class="col-md-2">
							<select id="st_edit_catatan" name="st_edit_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_edit_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_edit_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_edit">Waktu Edit</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_edit" name="lama_edit" value="{lama_edit}" placeholder="Waktu Edit">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_edit">Edit Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_edit" name="orang_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_edit=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_edit=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_hapus_catatan">Izin Hapus Catatan</label>
						<div class="col-md-2">
							<select id="st_hapus_catatan" name="st_hapus_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_hapus_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_hapus_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_hapus">Waktu Hapus</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_hapus" name="lama_hapus" value="{lama_hapus}" placeholder="Waktu Hapus">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_hapus">Hapus Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_hapus" name="orang_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_hapus=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_hapus=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_duplikasi_catatan">Izin Duplikasi Catatan</label>
						<div class="col-md-2">
							<select id="st_duplikasi_catatan" name="st_duplikasi_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_duplikasi_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_duplikasi_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_duplikasi">Waktu Duplikasi</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_duplikasi" name="lama_duplikasi" value="{lama_duplikasi}" placeholder="Waktu Duplikasi">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_duplikasi">Duplikasi Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_duplikasi" name="orang_duplikasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_duplikasi=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_duplikasi=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				
				<?php if (UserAccesForm($user_acces_form,array('1695'))){ ?>
				<div class="col-md-12 push-10">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="statusstok"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_general" name="btn_save_general"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
				<?}?>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1903'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_event">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('HAK AKSES FORMULIR')?></h5>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_hak_akses">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Profesi</th>
										<th width="15%">Spesialisasi</th>
										<th width="15%">PPA</th>
										<th width="15%">Melihat</th>
										<th width="10%">Input</th>
										<th width="10%">Edit</th>
										<th width="10%">Hapus</th>
										<th width="10%">Cetak</th>
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="profesi_id" name="profesi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" <?=($profesi_id=='#'?'selected':'')?>>-Pilih Profesi-</option>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" <?=($profesi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="spesialisasi_id" name="spesialisasi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($spesialisasi_id=='#'?'selected':'')?>>-SEMUA-</option>
												<?foreach(list_variable_ref(22) as $r){?>
													<option value="<?=$r->id?>" <?=($spesialisasi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										<th>
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($mppa_id=='#'?'selected':'')?>>-SEMUA-</option>
												
											</select>
										</th>
										
										<th>
											<select id="st_lihat" name="st_lihat" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_lihat=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_lihat=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_input" name="st_input" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_input=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_input=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_edit" name="st_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_edit=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_edit=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_hapus" name="st_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_hapus=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_hapus=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_cetak" name="st_cetak" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_cetak=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_cetak=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<?php if (UserAccesForm($user_acces_form,array('1697'))){ ?>
											<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_hak_akses" name="btn_tambah_hak_akses"><i class="fa fa-plus"></i> Tambah</button>
											<?}?>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
					</div>
				</div>
				
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1904'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?> " id="tab_3">
			
			<?php echo form_open('setting_rencana_biaya/save_label','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">JUDUL PERENCANAAN</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="judul_per_ina" value="{judul_per_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="judul_per_eng" value="{judul_per_eng}">
						</div>
					</div>
				</div>
				
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Nama Pasien & No Rekam Medis</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="nama_pasien_ina" value="{nama_pasien_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="nama_pasien_eng" value="{nama_pasien_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tanggal lahir dan umur</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="ttl_ina" value="{ttl_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="ttl_eng" value="{ttl_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Dengan Diagnosa</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="diagnosa_per_ina" value="{diagnosa_per_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="diagnosa_per_eng" value="{diagnosa_per_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Nama Tindakan Pembedahan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="nama_tindakan_ina" value="{nama_tindakan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="nama_tindakan_eng" value="{nama_tindakan_eng}">
						</div>
						
					</div>
				</div>
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tipe Pelayanan</label>&nbsp;&nbsp;&nbsp;&nbsp;
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="tipe_ina" value="{tipe_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="tipe_eng" value="{tipe_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">ICU</label>&nbsp;&nbsp;&nbsp;&nbsp;
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="icu_ina" value="{icu_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="icu_eng" value="{icu_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">CITO</label>&nbsp;&nbsp;&nbsp;&nbsp;
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="cito_ina" value="{cito_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="cito_eng" value="{cito_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Dokter Bedah</label>&nbsp;&nbsp;&nbsp;&nbsp;
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="dokter_ina" value="{dokter_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="dokter_eng" value="{dokter_eng}">
						</div>
						
					</div>
				</div>
				
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Catatan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="catatan_ina" value="{catatan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="catatan_eng" value="{catatan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Rencana Pemakaian Implant</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="rencana_implant_ina" value="{rencana_implant_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="rencana_implant_eng" value="{rencana_implant_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tabel No</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="table_no_ina" value="{table_no_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="table_no_eng" value="{table_no_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tabel Nama Alat Implant</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="table_alat_ina" value="{table_alat_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="table_alat_eng" value="{table_alat_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tabel Ukuran</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="table_ukuran_ina" value="{table_ukuran_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="table_ukuran_eng" value="{table_ukuran_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tabel Merk</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="table_merk_ina" value="{table_merk_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="table_merk_eng" value="{table_merk_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tabel Catatan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="table_catatan_ina" value="{table_catatan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="table_catatan_eng" value="{table_catatan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tabel Jumlah</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="table_jumlah_ina" value="{table_jumlah_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="table_jumlah_eng" value="{table_jumlah_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tabel Perkiraan Biaya</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="table_biaya_ina" value="{table_biaya_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="table_biaya_eng" value="{table_biaya_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tabel Aksi</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="table_aksi_ina" value="{table_aksi_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="table_aksi_eng" value="{table_aksi_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Table - Catatan Footer</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="table_footer_ina" value="{table_footer_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="table_footer_eng" value="{table_footer_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Kelompok Operasi</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="kel_op_ina" value="{kel_op_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="kel_op_eng" value="{kel_op_eng}">
						</div>
						
					</div>
				</div>
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Jenis Operasi</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="jenis_ina" value="{jenis_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="jenis_eng" value="{jenis_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Perkiraan Lama Dirawat</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="lama_ina" value="{lama_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="lama_eng" value="{lama_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Kelompok Pasien</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="kel_pasien_ina" value="{kel_pasien_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="kel_pasien_eng" value="{kel_pasien_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Perusahaan Asuransi</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="asuransi_ina" value="{asuransi_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="asuransi_eng" value="{asuransi_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Section Biaya Operasi</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="section_biaya_ina" value="{section_biaya_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="section_biaya_eng" value="{section_biaya_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Kamar Operasi</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="kamar_ina" value="{kamar_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="kamar_eng" value="{kamar_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Pemakaian obat dan alkes dikamar operasi</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="pakai_obat_ina" value="{pakai_obat_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="pakai_obat_eng" value="{pakai_obat_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Biaya Implant Yang dipilih</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="biaya_implan_ina" value="{biaya_implan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="biaya_implan_eng" value="{biaya_implan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Jasa Medis Dokter Operator</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="jasa_dokter_operator_ina" value="{jasa_dokter_operator_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="jasa_dokter_operator_eng" value="{jasa_dokter_operator_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Jasa Medis Dokter Anestesi</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="jasa_dokter_anes_ina" value="{jasa_dokter_anes_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="jasa_dokter_anes_eng" value="{jasa_dokter_anes_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Asisten Dikamar Operasi</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="jasa_asisten_ina" value="{jasa_asisten_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="jasa_asisten_eng" value="{jasa_asisten_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Section Total Biaya</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="total_biaya_ina" value="{total_biaya_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="total_biaya_eng" value="{total_biaya_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Section Biaya Rawat Inap</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="section_biaya_ranap_ina" value="{section_biaya_ranap_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="section_biaya_ranap_eng" value="{section_biaya_ranap_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Ruang Perawatan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="biaya_ruang_ina" value="{biaya_ruang_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="biaya_ruang_eng" value="{biaya_ruang_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Pemakaiann Obat dan Alkes Rawat Inap</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="biaya_obat_ina" value="{biaya_obat_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="biaya_obat_eng" value="{biaya_obat_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Pemeriksaan Penunjang</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="biaya_penunjang_ina" value="{biaya_penunjang_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="biaya_penunjang_eng" value="{biaya_penunjang_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Visit Dokter</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="biaya_visit_ina" value="{biaya_visit_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="biaya_visit_eng" value="{biaya_visit_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Ruang ICU</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="biaya_icu_ina" value="{biaya_icu_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="biaya_icu_eng" value="{biaya_icu_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Total Biaya Rawat Inap</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="total_biaya_ranap_ina" value="{total_biaya_ranap_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="total_biaya_ranap_eng" value="{total_biaya_ranap_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Total Estimasi Biaya</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="total_estimasi_biaya_ina" value="{total_estimasi_biaya_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="total_estimasi_biaya_eng" value="{total_estimasi_biaya_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Yang Menjelaskan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="yang_menjelaskan_ina" value="{yang_menjelaskan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="yang_menjelaskan_eng" value="{yang_menjelaskan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Petugas Billing</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="petugas_biling_ina" value="{petugas_biling_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="petugas_biling_eng" value="{petugas_biling_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">DPJP</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="dpjp_ina" value="{dpjp_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="dpjp_eng" value="{dpjp_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:25px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tanda tangan pasien</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="ttd_pasien_ina" value="{ttd_pasien_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="ttd_pasien_eng" value="{ttd_pasien_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:25px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tabel Penjelasan No</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="ket_no_ina" value="{ket_no_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="ket_no_eng" value="{ket_no_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:25px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tabel Penjelasan Penjelasan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="ket_penjelasan_ina" value="{ket_penjelasan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="ket_penjelasan_eng" value="{ket_penjelasan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:25px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tabel Penjelasan Paraf</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="ket_paraf_ina" value="{ket_paraf_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="ket_paraf_eng" value="{ket_paraf_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:25px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Kelas Diambil</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="kelas_diambil_ina" value="{kelas_diambil_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="kelas_diambil_eng" value="{kelas_diambil_eng}">
						</div>
						
					</div>
				</div>
				
				<div class="col-md-12 push-10">
					<div class="form-group" style="margin-bottom: 5px;">
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_label" name="btn_save_label"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
				
				
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1905'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='4'?'active in':'')?>" id="tab_4">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
					<div class="form-group">
						<div class="col-md-4">
							<button class="btn btn-danger btn-xs" type="button" id="add_content"><i class="fa fa-plus"></i> Content</button>
						</div>
						
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<table class="table" id="index_content">
								<thead>
									<tr>
										<th class="text-center" style="width: 50px;">#</th>
										<th>Content</th>
										<th class="hidden-xs" style="width: 15%;">Opsi</th>
										<th class="hidden-xs" style="width: 15%;">Created</th>
										<th class="text-center" style="width: 100px;">Actions</th>
									</tr>
								</thead>
								<tbody>
									
									
								</tbody>
							</table>
						</div>
						
					</div>
					
				</div>
				
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1906'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='5'?'active in':'')?>" id="tab_5">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('PEMAKAIAN OBAT DAN ALAT KESEHATAN DIKAMAR OPERASI')?></h5>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_obat">
									<thead>
										<tr>
											<th width="5%" class="text-center">No</th>
											<th width="15%" class="text-center">Kelompok Operasi</th>
											<th width="12%" class="text-center">Jenis operasi</th>
											<th width="12%" class="text-center">Kelas III</th>
											<th width="12%" class="text-center">Kelas II</th>
											<th width="12%" class="text-center">Kelas I</th>
											<th width="12%" class="text-center">UTAMA</th>
											<th width="12%" class="text-center">Action</th>										   
										</tr>
										<tr>
											<th>#</th>
											<th>
												<select id="kelompok_operasi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>-Kelompok Operasi-</option>
													<?foreach(get_all('mkelompok_operasi',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="jenis_operasi_id"  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" selected>-Jenis Operasi-</option>
													<?foreach(get_all('erm_jenis_operasi',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>" <?=($spesialisasi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th><input class="form-control number"  type="text" id="harga_obat_3" ></th>
											<th><input class="form-control number"  type="text" id="harga_obat_2" ></th>
											<th><input class="form-control number"  type="text" id="harga_obat_1" ></th>
											<th><input class="form-control number"  type="text" id="harga_obat_u" ></th>
											<th>
												<button class="btn btn-primary btn-sm" type="button" onclick="simpan_obat()"><i class="fa fa-plus"></i> Tambah</button>
												<button class="btn btn-warning btn-sm" type="button" onclick="clear_obat()"><i class="fa fa-refresh"></i> Clear</button>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('PEMAKAIAN OBAT DAN ALAT KESEHATAN DI RAWAT INAP')?></h5>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_ranap">
									<thead>
										<tr>
											<th width="5%" class="text-center">No</th>
											<th width="15%" class="text-center">ICU</th>
											<th width="15%" class="text-center">KELOMPOK PASIEN</th>
											<th width="15%" class="text-center">PERUSAHAAN ASURANSI</th>
											<th width="10%" class="text-center">Kelas III</th>
											<th width="10%" class="text-center">Kelas II</th>
											<th width="10%" class="text-center">Kelas I</th>
											<th width="10%" class="text-center">UTAMA</th>
											<th width="10%" class="text-center">Action</th>										   
										</tr>
										<tr>
											<th>#</th>
											<th>
												<select id="icu_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(list_variable_ref(122) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="idkelompok_pasien_ranap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(get_all('mpasien_kelompok',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
												</select>
											</th>
											<th>
												<select id="idrekanan_ranap"  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(get_all('mrekanan',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th><input class="form-control number" style="width: 100%;"  type="text" id="harga_3_ranap" ></th>
											<th><input class="form-control number" style="width: 100%;"  type="text" id="harga_2_ranap" ></th>
											<th><input class="form-control number" style="width: 100%;"  type="text" id="harga_1_ranap" ></th>
											<th><input class="form-control number" style="width: 100%;"  type="text" id="harga_u_ranap" ></th>
											<th>
												<button class="btn btn-primary btn-sm" type="button" onclick="simpan_ranap()"><i class="fa fa-plus"></i> Tambah</button>
												<button class="btn btn-warning btn-sm" type="button" onclick="clear_ranap()"><i class="fa fa-refresh"></i></button>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('PEMERIKSAAN PENUNJANG')?></h5>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_penunjang">
									<thead>
										<tr>
											<th width="5%" class="text-center">No</th>
											<th width="15%" class="text-center">ICU</th>
											<th width="15%" class="text-center">KELOMPOK PASIEN</th>
											<th width="15%" class="text-center">PERUSAHAAN ASURANSI</th>
											<th width="10%" class="text-center">Kelas III</th>
											<th width="10%" class="text-center">Kelas II</th>
											<th width="10%" class="text-center">Kelas I</th>
											<th width="10%" class="text-center">UTAMA</th>
											<th width="10%" class="text-center">Action</th>										   
										</tr>
										<tr>
											<th>#</th>
											<th>
												<select id="icu_penunjang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(list_variable_ref(122) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="idkelompok_pasien_penunjang" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(get_all('mpasien_kelompok',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
												</select>
											</th>
											<th>
												<select id="idrekanan_penunjang"  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(get_all('mrekanan',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th><input class="form-control number" style="width: 100%;"  type="text" id="harga_3_penunjang" ></th>
											<th><input class="form-control number" style="width: 100%;"  type="text" id="harga_2_penunjang" ></th>
											<th><input class="form-control number" style="width: 100%;"  type="text" id="harga_1_penunjang" ></th>
											<th><input class="form-control number" style="width: 100%;"  type="text" id="harga_u_penunjang" ></th>
											<th>
												<button class="btn btn-primary btn-sm" type="button" onclick="simpan_penunjang()"><i class="fa fa-plus"></i> Tambah</button>
												<button class="btn btn-warning btn-sm" type="button" onclick="clear_penunjang()"><i class="fa fa-refresh"></i></button>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('VISIT DOKTER')?></h5>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_visit">
									<thead>
										<tr>
											<th width="5%" class="text-center">No</th>
											<th width="15%" class="text-center">ICU</th>
											<th width="15%" class="text-center">KELOMPOK PASIEN</th>
											<th width="15%" class="text-center">PERUSAHAAN ASURANSI</th>
											<th width="10%" class="text-center">Kelas III</th>
											<th width="10%" class="text-center">Kelas II</th>
											<th width="10%" class="text-center">Kelas I</th>
											<th width="10%" class="text-center">UTAMA</th>
											<th width="10%" class="text-center">Action</th>										   
										</tr>
										<tr>
											<th>#</th>
											<th>
												<select id="icu_visit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(list_variable_ref(122) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="idkelompok_pasien_visit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(get_all('mpasien_kelompok',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
													
												</select>
											</th>
											<th>
												<select id="idrekanan_visit"  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="0" selected>-SEMUA-</option>
													<?foreach(get_all('mrekanan',array('status'=>1)) as $r){?>
														<option value="<?=$r->id?>"><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th><input class="form-control number" style="width: 100%;"  type="text" id="harga_3_visit" ></th>
											<th><input class="form-control number" style="width: 100%;"  type="text" id="harga_2_visit" ></th>
											<th><input class="form-control number" style="width: 100%;"  type="text" id="harga_1_visit" ></th>
											<th><input class="form-control number" style="width: 100%;"  type="text" id="harga_u_visit" ></th>
											<th>
												<button class="btn btn-primary btn-sm" type="button" onclick="simpan_visit()"><i class="fa fa-plus"></i> Tambah</button>
												<button class="btn btn-warning btn-sm" type="button" onclick="clear_visit()"><i class="fa fa-refresh"></i></button>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1907'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='6'?'active in':'')?> " id="tab_6">
			
			<?php echo form_open('setting_rencana_biaya/save_persen','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="dokter_anestesi">Default Persentase Dokter Anestesi</label>
						<div class="col-md-2">
							<div class="input-group">
							<input class="form-control decimal" type="text" id="dokter_anestesi" name="dokter_anestesi" value="{dokter_anestesi}" placeholder="dokter_anestesi">
								<span class="input-group-addon">%</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="assisten_operator">Default Persentase Assisten Operator</label>
						<div class="col-md-2">
							<div class="input-group">
							<input class="form-control decimal" type="text" id="assisten_operator" name="assisten_operator" value="{assisten_operator}" placeholder="assisten_operator">
								<span class="input-group-addon">%</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="assisten_anestesi">Default Persentase Assisten Anestesi</label>
						<div class="col-md-2">
							<div class="input-group">
							<input class="form-control decimal" type="text" id="assisten_anestesi" name="assisten_anestesi" value="{assisten_anestesi}" placeholder="assisten_anestesi">
								<span class="input-group-addon">%</span>
							</div>
						</div>
					</div>
				</div>
				
				
				<?php if (UserAccesForm($user_acces_form,array('1907'))){ ?>
				<div class="col-md-12 push-10-t">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_persen" name="btn_save_persen"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
				<?}?>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
	</div>
</div>

<div class="modal" id="modal_content" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Content</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal push-10-t">
					<div class="row">
                        <input type="hidden" readonly id="idcontent" name="idcontent" value="">

						<div class="col-md-12">                               
							<div class="form-group">
								<label class="col-md-2 control-label">No Urut</label>
								<div class="col-md-9"> 
									<input tabindex="0" type="text" class="form-control number" maxlength="3" id="no" placeholder="No Urut" value="">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Content</label>
								<div class="col-md-9"> 
									<textarea class="form-control js-summernote footer" name="isi" id="isi"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Jenis Jawaban</label>
								<div class="col-md-9"> 
									<select tabindex="5" id="jenis_isi"   name="reservasi_cara" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="1"  <?=($jenis_isi == "1" ? 'selected="selected"' : '')?>>Option</option>
										<option value="2" <?=($jenis_isi == 2 ? 'selected="selected"' : '')?>>Free Text</option>
										<option value="3" <?=($jenis_isi == 3 ? 'selected="selected"' : '')?>>Tanda Tangan</option>
										
									</select>
								</div>
							</div>
							<div class="form-group div_opsi">
								<label class="col-md-2 control-label">Value</label>
								<div class="col-md-9"> 
									<select id="ref_id" name="ref_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
																		
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">                               
							<br>
						</div>
                    </div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-primary" type="submit" type="button" id="btn_simpan_content"><i class="fa fa-check"></i> Simpan</button>
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
		$(".number").number(true,0,'.',',');
	var tab=$("#tab").val();
if (tab=='1' || tab=='3'){
		load_general();
	}
	if (tab=='2'){
		load_tab2();
	}
	if (tab=='4'){
		set_tab_4();
	}
	if (tab=='5'){
		set_tab_5();
	}

})	
function set_tab_5(){
	clear_obat();
	load_obat();
	
	clear_ranap();
	load_ranap();
	
	clear_penunjang();
	load_penunjang();
	
	clear_visit();
	load_visit();
}
//VISIT
function clear_visit(){
	$("#icu_visit").val("0").trigger('change');
	$("#idkelompok_pasien_visit").val("0").trigger('change');
	$("#idrekanan_visit").val("0").trigger('change');
	$("#harga_3_visit").val("0");
	$("#harga_2_visit").val("0");
	$("#harga_1_visit").val("0");
	$("#harga_u_visit").val("0");
}
function simpan_visit(){
	let idkelompok_pasien=$("#idkelompok_pasien_visit").val();
	let icu=$("#icu_visit").val();
	let idrekanan=$("#idrekanan_visit").val();
	let harga_1=$("#harga_1_visit").val();
	let harga_2=$("#harga_2_visit").val();
	let harga_3=$("#harga_3_visit").val();
	let harga_u=$("#harga_u_visit").val();
	
	if (idkelompok_pasien_visit=='#'){
		sweetAlert("Maaf...", "Tentukan Kelompok Operasi", "error");
		return false;
	}
	if (icu_visit==''){
		sweetAlert("Maaf...", "Tentukan Jenis Operasi", "error");
		return false;
	}
	
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_rencana_biaya/simpan_visit', 
		dataType: "JSON",
		method: "POST",
		data : {
				idrekanan:idrekanan,
				idkelompok_pasien:idkelompok_pasien,
				icu:icu,
				harga_1:harga_1,
				harga_2:harga_2,
				harga_3:harga_3,
				harga_u:harga_u,
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				clear_visit();
				$('#index_visit').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});
}
function hapus_visit(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_rencana_biaya/hapus_visit',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_visit').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function load_visit(){
	// alert('sini');
	$('#index_visit').DataTable().destroy();	
	table = $('#index_visit').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_rencana_biaya/load_visit', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
//PENUNJANG
function clear_penunjang(){
	$("#icu_penunjang").val("0").trigger('change');
	$("#idkelompok_pasien_penunjang").val("0").trigger('change');
	$("#idrekanan_penunjang").val("0").trigger('change');
	$("#harga_3_penunjang").val("0");
	$("#harga_2_penunjang").val("0");
	$("#harga_1_penunjang").val("0");
	$("#harga_u_penunjang").val("0");
}
function simpan_penunjang(){
	let idkelompok_pasien=$("#idkelompok_pasien_penunjang").val();
	let icu=$("#icu_penunjang").val();
	let idrekanan=$("#idrekanan_penunjang").val();
	let harga_1=$("#harga_1_penunjang").val();
	let harga_2=$("#harga_2_penunjang").val();
	let harga_3=$("#harga_3_penunjang").val();
	let harga_u=$("#harga_u_penunjang").val();
	
	if (idkelompok_pasien_penunjang=='#'){
		sweetAlert("Maaf...", "Tentukan Kelompok Operasi", "error");
		return false;
	}
	if (icu_penunjang==''){
		sweetAlert("Maaf...", "Tentukan Jenis Operasi", "error");
		return false;
	}
	
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_rencana_biaya/simpan_penunjang', 
		dataType: "JSON",
		method: "POST",
		data : {
				idrekanan:idrekanan,
				idkelompok_pasien:idkelompok_pasien,
				icu:icu,
				harga_1:harga_1,
				harga_2:harga_2,
				harga_3:harga_3,
				harga_u:harga_u,
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				clear_penunjang();
				$('#index_penunjang').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});
}
function hapus_penunjang(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_rencana_biaya/hapus_penunjang',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_penunjang').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function load_penunjang(){
	// alert('sini');
	$('#index_penunjang').DataTable().destroy();	
	table = $('#index_penunjang').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_rencana_biaya/load_penunjang', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
//RANAP
function clear_ranap(){
	$("#icu_ranap").val("0").trigger('change');
	$("#idkelompok_pasien_ranap").val("0").trigger('change');
	$("#idrekanan_ranap").val("0").trigger('change');
	$("#harga_3_ranap").val("0");
	$("#harga_2_ranap").val("0");
	$("#harga_1_ranap").val("0");
	$("#harga_u_ranap").val("0");
}
function simpan_ranap(){
	let idkelompok_pasien=$("#idkelompok_pasien_ranap").val();
	let icu=$("#icu_ranap").val();
	let idrekanan=$("#idrekanan_ranap").val();
	let harga_1=$("#harga_1_ranap").val();
	let harga_2=$("#harga_2_ranap").val();
	let harga_3=$("#harga_3_ranap").val();
	let harga_u=$("#harga_u_ranap").val();
	
	if (idkelompok_pasien_ranap=='#'){
		sweetAlert("Maaf...", "Tentukan Kelompok Operasi", "error");
		return false;
	}
	if (icu_ranap==''){
		sweetAlert("Maaf...", "Tentukan Jenis Operasi", "error");
		return false;
	}
	
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_rencana_biaya/simpan_ranap', 
		dataType: "JSON",
		method: "POST",
		data : {
				idrekanan:idrekanan,
				idkelompok_pasien:idkelompok_pasien,
				icu:icu,
				harga_1:harga_1,
				harga_2:harga_2,
				harga_3:harga_3,
				harga_u:harga_u,
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				clear_ranap();
				$('#index_ranap').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});
}
function hapus_ranap(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_rencana_biaya/hapus_ranap',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_ranap').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function load_ranap(){
	// alert('sini');
	$('#index_ranap').DataTable().destroy();	
	table = $('#index_ranap').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_rencana_biaya/load_ranap', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
$("#kelompok_operasi_id").change(function(){
	// $.ajax({
		// url: '{site_url}setting_rencana_biaya/find_jenis_operasi/'+$(this).val(),
		// dataType: "json",
		// success: function(data) {
			// $('#jenis_operasi_id').empty();
			// $('#jenis_operasi_id').append(data);
		// }
	// });

});
function clear_obat(){
	$("#jenis_operasi_id").val("").trigger('change');
	$("#kelompok_operasi_id").val("#").trigger('change');
	$("#harga_obat_3").val("0");
	$("#harga_obat_2").val("0");
	$("#harga_obat_1").val("0");
	$("#harga_obat_u").val("0");
}
function simpan_obat(){
	let kelompok_operasi_id=$("#kelompok_operasi_id").val();
	let jenis_operasi_id=$("#jenis_operasi_id").val();
	let harga_obat_1=$("#harga_obat_1").val();
	let harga_obat_2=$("#harga_obat_2").val();
	let harga_obat_3=$("#harga_obat_3").val();
	let harga_obat_u=$("#harga_obat_u").val();
	
	if (kelompok_operasi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Kelompok Operasi", "error");
		return false;
	}
	if (jenis_operasi_id==''){
		sweetAlert("Maaf...", "Tentukan Jenis Operasi", "error");
		return false;
	}
	
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_rencana_biaya/simpan_obat', 
		dataType: "JSON",
		method: "POST",
		data : {
				kelompok_operasi_id:kelompok_operasi_id,
				jenis_operasi_id:jenis_operasi_id,
				harga_obat_1:harga_obat_1,
				harga_obat_2:harga_obat_2,
				harga_obat_3:harga_obat_3,
				harga_obat_u:harga_obat_u,
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				clear_obat();
				$('#index_obat').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});
}
function hapus_obat(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_rencana_biaya/hapus_obat',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_obat').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function load_obat(){
	$('#index_obat').DataTable().destroy();	
	table = $('#index_obat').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_rencana_biaya/load_obat', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function load_general(){
	$('.js-summernote').summernote({
	  height: 50,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	});	

}
function load_tab2(){
	load_hak_akses();		
	load_formulir();	
}

// LOGIC
function load_hak_akses(){
	$('#index_hak_akses').DataTable().destroy();	
	table = $('#index_hak_akses').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_rencana_biaya/load_hak_akses', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
$("#btn_tambah_hak_akses").click(function() {
	let profesi_id=$("#profesi_id").val();
	let spesialisasi_id=$("#spesialisasi_id").val();
	let mppa_id=$("#mppa_id").val();
	let st_lihat=$("#st_lihat").val();
	let st_input=$("#st_input").val();
	let st_edit=$("#st_edit").val();
	let st_hapus=$("#st_hapus").val();
	let st_cetak=$("#st_cetak").val();
	
	if (profesi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Profesi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_rencana_biaya/simpan_hak_akses', 
		dataType: "JSON",
		method: "POST",
		data : {
				profesi_id:profesi_id,
				spesialisasi_id:spesialisasi_id,
				mppa_id:mppa_id,
				st_lihat:st_lihat,
				st_input:st_input,
				st_edit:st_edit,
				st_hapus:st_hapus,
				st_cetak:st_cetak,
			
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				$("#profesi_id").val('#').trigger('change');
				$("#spesialisasi_id").val('#').trigger('change');
				$("#mppa_id").val('#').trigger('change');
				$("#st_cetak").val('0').trigger('change');
				$("#st_hapus").val('0').trigger('change');
				$("#st_edit").val('0').trigger('change');
				$("#st_input").val('0').trigger('change');
				$("#st_lihat").val('0').trigger('change');
				$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});

});
function hapus_hak_akses(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_rencana_biaya/hapus_hak_akses',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function hapus_default(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Default?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_rencana_biaya/hapus_default',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_tampil_default').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

$("#profesi_id").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_rencana_biaya/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id").empty();
				$("#mppa_id").append(data);
			}
		});
	}else{
		
				$("#mppa_id").empty();
	}

});
$("#operand_tahun").change(function(){
	if ($(this).val()=='0'){//Tidak Ditentukan
		$("#operand_bulan").val($(this).val()).trigger('change');
		$("#operand_hari").val($(this).val()).trigger('change');
		$("#umur_bulan").val($(this).val()).trigger('change');
		$("#umur_hari").val($(this).val()).trigger('change');
		$(".bulan").attr('disabled','disabled');
		$(".hari").attr('disabled','disabled');
	}else{
		$(".bulan").removeAttr('disabled');
		// $(".hari").removeAttr('disabled');
	}

});
$("#operand_bulan").change(function(){
	if ($(this).val()=='0'){//Tidak Ditentukan
		$("#operand_hari").val($(this).val()).trigger('change');
		$(".hari").attr('disabled','disabled');
		$("#umur_hari").val($(this).val()).trigger('change');
	}else{
		$(".hari").removeAttr('disabled');
	}

});
$("#st_spesifik_rencana_biaya").change(function(){
	if ($(this).val()=='1'){
		$(".setting_tidak").show();
	}else{
		$(".setting_tidak").hide();
	}

});
$("#btn_tambah_formulir").click(function() {
	let idtipe=$("#idtipe").val();
	let idpoli=$("#idpoli").val();
	let statuspasienbaru=$("#statuspasienbaru").val();
	let pertemuan_id=$("#pertemuan_id").val();
	let st_tujuan_terakhir=$("#st_tujuan_terakhir").val();
	let operand=$("#operand").val();
	// alert(operand);
	let lama_terakhir_tujan=$("#lama_terakhir_tujan").val();
	let st_formulir=$("#st_formulir").val();
	
	if (idtipe=='#'){
		sweetAlert("Maaf...", "Tentukan Tujuan", "error");
		return false;
	}
	
	if (st_formulir=='0'){
		sweetAlert("Maaf...", "Tentukan Status Formulir", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_rencana_biaya/simpan_formulir', 
		dataType: "JSON",
		method: "POST",
		data : {
				idtipe:idtipe,
				idpoli:idpoli,
				statuspasienbaru:statuspasienbaru,
				pertemuan_id:pertemuan_id,
				st_tujuan_terakhir:st_tujuan_terakhir,
				operand:operand,
				lama_terakhir_tujan:lama_terakhir_tujan,
				st_formulir:st_formulir,

			
			},
		success: function(data) {
			// alert(data);
				$("#cover-spin").hide();
			if (data==true){
				$("#idtipe").val('#').trigger('change');
				$("#idpoli").val('#').trigger('change');
				$("#statuspasienbaru").val('#').trigger('change');
				$("#pertemuan_id").val('#').trigger('change');
				$("#operand").val('0').trigger('change');
				$("#st_tujuan_terakhir").val('0').trigger('change');
				$("#lama_terakhir_tujan").val('0');
				$("#st_formulir").val('0').trigger('change');
				$('#index_tampil').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});

			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
		}
	});

});
$("#idtipe").change(function(){
		$.ajax({
			url: '{site_url}setting_rencana_biaya/find_poli/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#idpoli").empty();
				$("#idpoli").append(data);
			}
		});

});
$("#idtipe_default").change(function(){
		$.ajax({
			url: '{site_url}setting_rencana_biaya/find_poli/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#idpoli_default").empty();
				$("#idpoli_default").append(data);
			}
		});

});
function load_formulir(){
	$('#index_tampil').DataTable().destroy();	
	table = $('#index_tampil').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			"columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [3,2,4,5,6] },
					// { "width": "15%", "targets": [1]},
					// { "width": "8%", "targets": [8]},
					// { "width": "30%", "targets": 3,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}setting_rencana_biaya/load_formulir', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function hapus_formulir(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_rencana_biaya/hapus_formulir',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_tampil').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function set_tab_4(){
	load_content();
}
$("#btn_simpan_content").click(function() {
	if ($("#no").val()==''){
		sweetAlert("Maaf...", "Tentukan No Urut", "error");
		return false;
	}
	if ($("#no").val()=='0'){
		sweetAlert("Maaf...", "Tentukan No Urut", "error");
		return false;
	}

	if ($("#isi").val()==''){
		sweetAlert("Maaf...", "Isi Content ", "error");
		return false;
	}
	var idcontent=$("#idcontent").val();
	var no=$("#no").val();
	var isi=$("#isi").val();
	var jenis_isi=$("#jenis_isi").val();
	var ref_id=$("#ref_id").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_rencana_biaya/simpan_content/',
		dataType: "json",
		type: "POST",
		data: {
			idcontent:idcontent,
			jenis_isi:jenis_isi,
			ref_id:ref_id,
			no: no,isi:isi
		},
		success: function(data) {
			$("#modal_content").modal('hide');
			$("#cover-spin").hide();
			load_content();
		}
	});

});
$("#add_content").click(function() {
	$("#modal_content").modal('show');
	$("#idcontent").val('');
	var rowCount = $('#index_content tr').length;
	$("#no").val(rowCount);
	$('#isi').summernote('code','');
	var id='';
	$.ajax({
			url: '{site_url}setting_rencana_biaya/load_jawaban',
			type: 'POST',
			dataType: "json",
			data: {id: id},
			success: function(data) {
				console.log(data);
				$("#ref_id").empty();
				$('#ref_id').append(data);
				$("#cover-spin").hide();
			}
	});
});
function edit_content(id){
	$("#idcontent").val(id);
	$("#modal_content").modal('show');
	$("#cover-spin").show();
		
	 $.ajax({
			url: '{site_url}setting_rencana_biaya/load_jawaban',
			type: 'POST',
			dataType: "json",
			data: {id: id},
			success: function(data) {
				console.log(data);
				$("#ref_id").empty();
				$('#ref_id').append(data);
				$("#cover-spin").hide();
			}
	});
	$("#cover-spin").show();
	$.ajax({
			url: '{site_url}setting_rencana_biaya/get_edit',
			type: 'POST',
			dataType: "json",
			data: {id: id},
			success: function(data) {
				console.log(data);
				$('#isi').summernote('code',data.isi);
				// $("#isi").html(data.isi);
				$("#jenis_isi").val(data.jenis_isi).trigger('change');
				$("#no").val(data.no);
				$("#cover-spin").hide();
				
			}
	});
}
$("#jenis_isi").change(function() {
	let jenis_isi=$("#jenis_isi").val();
	if (jenis_isi=='1'){
		$(".div_opsi").css("display", "block");
	}else{
		
		$(".div_opsi").css("display", "none");
	}
	
});
function load_content(){
	$.ajax({
		url: '{site_url}setting_rencana_biaya/load_content/',
		dataType: "json",
		success: function(data) {
			$("#index_content tbody").empty();
			$('#index_content tbody').append(data);
		}
	});

}
function hapus(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_rencana_biaya/hapus',
				type: 'POST',
				data: {id: id},
				complete: function() {
					load_content();
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
</script>