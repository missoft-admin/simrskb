<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('2364'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_general()"><i class="fa fa-bell"></i> General</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2366'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_event"  onclick="load_tab2()"><i class="fa fa-check-square-o"></i> Logic</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2368'))){ ?>
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab_3"><i class="fa fa-check-square-o"></i> Label Form</a>
		</li>
		<?}?>
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('2364'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_info">
			
			<?php echo form_open('setting_survey_kepuasan/save_survey_kepuasan','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_edit_catatan">Izin Edit Catatan</label>
						<div class="col-md-2">
							<select id="st_edit_catatan" name="st_edit_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_edit_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_edit_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_edit">Waktu Edit</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_edit" name="lama_edit" value="{lama_edit}" placeholder="Waktu Edit">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_edit">Edit Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_edit" name="orang_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_edit=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_edit=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_hapus_catatan">Izin Hapus Catatan</label>
						<div class="col-md-2">
							<select id="st_hapus_catatan" name="st_hapus_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_hapus_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_hapus_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_hapus">Waktu Hapus</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_hapus" name="lama_hapus" value="{lama_hapus}" placeholder="Waktu Hapus">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_hapus">Hapus Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_hapus" name="orang_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_hapus=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_hapus=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_duplikasi_catatan">Izin Duplikasi Catatan</label>
						<div class="col-md-2">
							<select id="st_duplikasi_catatan" name="st_duplikasi_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_duplikasi_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_duplikasi_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_duplikasi">Waktu Duplikasi</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_duplikasi" name="lama_duplikasi" value="{lama_duplikasi}" placeholder="Waktu Duplikasi">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_duplikasi">Duplikasi Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_duplikasi" name="orang_duplikasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_duplikasi=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_duplikasi=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_header_ina">Judul Header <span style="color:red;"> INA</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_header_ina" name="judul_header_ina" value="{judul_header_ina}" placeholder="Header">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_header_eng">Judul Header <span style="color:red;"> ENG</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_header_eng" name="judul_header_eng" value="{judul_header_eng}" placeholder="Header">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_footer_ina">Judul Footer <span style="color:red;"> INA </span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_footer_ina" name="judul_footer_ina" value="{judul_footer_ina}" placeholder="Footer">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_footer_eng">Judul Footer <span style="color:red;"> ENG </span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_footer_eng" name="judul_footer_eng" value="{judul_footer_eng}" placeholder="Footer">
						</div>
					</div>
				</div>
				
				<?php if (UserAccesForm($user_acces_form,array('2365'))){ ?>
				<div class="col-md-12 push-10">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="statusstok"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_general" name="btn_save_general"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
				<?}?>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2366'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_event">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('HAK AKSES FORMULIR')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_hak_akses">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Profesi</th>
										<th width="15%">Spesialisasi</th>
										<th width="15%">PPA</th>
										<th width="15%">Melihat</th>
										<th width="10%">Input</th>
										<th width="10%">Edit</th>
										<th width="10%">Hapus</th>
										<th width="10%">Cetak</th>
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="profesi_id" name="profesi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" <?=($profesi_id=='#'?'selected':'')?>>-Pilih Profesi-</option>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" <?=($profesi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="spesialisasi_id" name="spesialisasi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($spesialisasi_id=='#'?'selected':'')?>>-SEMUA-</option>
												<?foreach(list_variable_ref(22) as $r){?>
													<option value="<?=$r->id?>" <?=($spesialisasi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										<th>
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($mppa_id=='#'?'selected':'')?>>-SEMUA-</option>
												
											</select>
										</th>
										
										<th>
											<select id="st_lihat" name="st_lihat" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_lihat=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_lihat=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_input" name="st_input" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_input=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_input=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_edit" name="st_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_edit=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_edit=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_hapus" name="st_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_hapus=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_hapus=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_cetak" name="st_cetak" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_cetak=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_cetak=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<?php if (UserAccesForm($user_acces_form,array('2367'))){ ?>
											<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_hak_akses" name="btn_tambah_hak_akses"><i class="fa fa-plus"></i> Tambah</button>
											<?}?>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
					</div>
				</div>
				</div>
				
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2368'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?> " id="tab_3">
			
			<?php echo form_open_multipart('setting_survey_kepuasan/save_survey_kepuasan_label','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				
				<div class="form-group">
					
					<div class="col-md-12">
						<div class="col-md-12">
							<img class="img-avatar"  id="output_img" src="{upload_path}app_setting/{logo}" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Logo Login (100x100)</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="box">
								<input type="file" id="file-3" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;" name="logo" value="{logo}" />
								<label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label>Alamat</label>
							<input type="text" class="form-control" name="alamat_rs" value="{alamat_rs}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label>Phone</label>
							<input type="text" class="form-control" name="phone_rs" value="{phone_rs}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label>Web</label>
							<input type="text" class="form-control" name="web_rs" value="{web_rs}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Label Header Identtias</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="label_identitas_ina" value="{label_identitas_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="label_identitas_eng" value="{label_identitas_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Nomor Register</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="no_reg_ina" value="{no_reg_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="no_reg_eng" value="{no_reg_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Nomor Rekam Medis</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="no_rm_ina" value="{no_rm_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="no_rm_eng" value="{no_rm_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Nama Pasien</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="nama_pasien_ina" value="{nama_pasien_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="nama_pasien_eng" value="{nama_pasien_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tanggal Lahir</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="ttl_ina" value="{ttl_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="ttl_eng" value="{ttl_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Umur</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="umur_ina" value="{umur_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="umur_eng" value="{umur_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Jenis Kelamin</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="jk_ina" value="{jk_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="jk_eng" value="{jk_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Asal Pasien</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="asal_pasien_ina" value="{asal_pasien_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="asal_pasien_eng" value="{asal_pasien_eng}">
						</div>
						
					</div>
				</div>
				
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Detail</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="detail_ina" value="{detail_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="detail_eng" value="{detail_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Dokter</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="dokter_ina" value="{dokter_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="dokter_eng" value="{dokter_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Profile Survey</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="profile_ina" value="{profile_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="profile_eng" value="{profile_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Nama Profile</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="profile_nama_ina" value="{profile_nama_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="profile_nama_eng" value="{profile_nama_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tanggal lahir profile</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="ttl_profile_ina" value="{ttl_profile_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="ttl_profile_eng" value="{ttl_profile_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Umur Profile</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="umur_profile_ina" value="{umur_profile_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="umur_profile_eng" value="{umur_profile_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Jenis Kelamin Profile</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="jk_profile_ina" value="{jk_profile_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="jk_profile_eng" value="{jk_profile_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Pekerjaan Profile</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="pekerjaan_profile_ina" value="{pekerjaan_profile_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="pekerjaan_profile_eng" value="{pekerjaan_profile_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Pendidikan Profile</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="pendidikan_profile_ina" value="{pendidikan_profile_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="pendidikan_profile_eng" value="{pendidikan_profile_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Mengetahui RS</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="mengetahui_rs_ina" value="{mengetahui_rs_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="mengetahui_rs_eng" value="{mengetahui_rs_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Mengetahui RS Lainnya</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="mengetahui_lain_ina" value="{mengetahui_lain_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="mengetahui_lain_eng" value="{mengetahui_lain_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Waktu Survey</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="waktu_survey_ina" value="{waktu_survey_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="waktu_survey_eng" value="{waktu_survey_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Nama Survey</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="nama_survey_ina" value="{nama_survey_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="nama_survey_eng" value="{nama_survey_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Total Penilaian</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="total_nilai_ina" value="{total_nilai_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="total_nilai_eng" value="{total_nilai_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Nilai Perunsur</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="nilai_perunsur_ina" value="{nilai_perunsur_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="nilai_perunsur_eng" value="{nilai_perunsur_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">NRR Per-Unsur</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="nrr_perunsur_ina" value="{nrr_perunsur_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="nrr_perunsur_eng" value="{nrr_perunsur_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">NRR Tertimbang</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="nrr_tertimbang_ina" value="{nrr_tertimbang_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="nrr_tertimbang_eng" value="{nrr_tertimbang_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Satuan Kepuasan Masyarakat</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="satuan_kepuasan_ina" value="{satuan_kepuasan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="satuan_kepuasan_eng" value="{satuan_kepuasan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Hasil Survey</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="hasil_survey_ina" value="{hasil_survey_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="hasil_survey_eng" value="{hasil_survey_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Pertanyaan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="pertanyaan_ina" value="{pertanyaan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="pertanyaan_eng" value="{pertanyaan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Jawaban</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="jawaban_ina" value="{jawaban_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="jawaban_eng" value="{jawaban_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Total Penilaian</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="total_penilaian_ina" value="{total_penilaian_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="total_penilaian_eng" value="{total_penilaian_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Kritik & Saran</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="kritik_ina" value="{kritik_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="kritik_eng" value="{kritik_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Responden</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="responden_ina" value="{responden_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="responden_eng" value="{responden_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Notes Footer</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<textarea class="form-control js-summernote" name="notes_footer_ina" id="notes_footer_ina"><?=$notes_footer_ina?></textarea>
						</div>
						<div class="col-md-6">
							<label>English</label>
							<textarea class="form-control js-summernote" name="notes_footer_eng" id="notes_footer_eng"><?=$notes_footer_eng?></textarea>
						</div>
						
					</div>
				</div>
				
				<div class="form-group" style="margin-top:15px">
				
					<div class="col-md-12">
						<div class="col-md-6">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_label" name="btn_save_general"><i class="fa fa-save"></i> Simpan</button>
						</div>
						
					</div>
				</div>
			</div>
			
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
function loadFile_img(event){
	// alert('load');
	var output_img = document.getElementById('output_img');
	output_img.src = URL.createObjectURL(event.target.files[0]);
}
$(document).ready(function(){	
	$('.js-summernote').summernote({
		  height: 50,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})

		$(".number").number(true,0,'.',',');
	var tab=$("#tab").val();
	if (tab=='1'){
		load_general();
	}
	if (tab=='2'){
			load_tab2();
	}
	
})	

function load_general(){
	$('.js-summernote').summernote({
	  height: 50,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	});	

}
function load_tab2(){
	load_hak_akses();		
}
// LOGIC
function load_hak_akses(){
	$('#index_hak_akses').DataTable().destroy();	
	table = $('#index_hak_akses').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_survey_kepuasan/load_hak_akses', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}

$("#btn_tambah_hak_akses").click(function() {
	let profesi_id=$("#profesi_id").val();
	let spesialisasi_id=$("#spesialisasi_id").val();
	let mppa_id=$("#mppa_id").val();
	let st_lihat=$("#st_lihat").val();
	let st_input=$("#st_input").val();
	let st_edit=$("#st_edit").val();
	let st_hapus=$("#st_hapus").val();
	let st_cetak=$("#st_cetak").val();
	
	if (profesi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Profesi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_survey_kepuasan/simpan_hak_akses', 
		dataType: "JSON",
		method: "POST",
		data : {
				profesi_id:profesi_id,
				spesialisasi_id:spesialisasi_id,
				mppa_id:mppa_id,
				st_lihat:st_lihat,
				st_input:st_input,
				st_edit:st_edit,
				st_hapus:st_hapus,
				st_cetak:st_cetak,
			
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				$("#profesi_id").val('#').trigger('change');
				$("#spesialisasi_id").val('#').trigger('change');
				$("#mppa_id").val('#').trigger('change');
				$("#st_cetak").val('0').trigger('change');
				$("#st_hapus").val('0').trigger('change');
				$("#st_edit").val('0').trigger('change');
				$("#st_input").val('0').trigger('change');
				$("#st_lihat").val('0').trigger('change');
				$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});

});
function hapus_hak_akses(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_survey_kepuasan/hapus_hak_akses',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

$("#profesi_id").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_survey_kepuasan/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id").empty();
				$("#mppa_id").append(data);
			}
		});
	}else{
		
				$("#mppa_id").empty();
	}

});

</script>