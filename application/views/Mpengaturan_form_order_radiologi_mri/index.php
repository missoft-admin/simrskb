<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mpengaturan_form_order_radiologi_mri" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?php echo form_open('mpengaturan_form_order_radiologi_mri/save','class="form-horizontal push-10-t" id="form-work"') ?>
			<div class="form-group">
				<label class="col-md-12" for="">Pesan Informasi Berhasil</label>
				<div class="col-md-12">
					<textarea class="form-control summernote js-summernote-custom-height" name="pesan_informasi_berhasil" placeholder="Pesan Informasi Berhasil">{pesan_informasi_berhasil}</textarea>
				</div>
			</div>

			<h5 style="margin-bottom: 10px;" for="">Judul Formulir</h5>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_judul" placeholder="Judul Formulir (Indonesia Version)">{label_judul}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_judul_eng" placeholder="Judul Formulir (English Version)">{label_judul_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Tujuan Radiologi</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_tujuan_radiologi" <?=($required_tujuan_radiologi == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_tujuan_radiologi" placeholder="Tujuan Radiologi (Indonesia Version)">{label_tujuan_radiologi}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_tujuan_radiologi_eng" placeholder="Tujuan Radiologi (English Version)">{label_tujuan_radiologi_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Dokter Peminta Pemeriksaan</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_dokter_peminta_pemeriksaan" <?=($required_dokter_peminta_pemeriksaan == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_dokter_peminta_pemeriksaan" placeholder="Dokter Peminta Pemeriksaan (Indonesia Version)">{label_dokter_peminta_pemeriksaan}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_dokter_peminta_pemeriksaan_eng" placeholder="Dokter Peminta Pemeriksaan (English Version)">{label_dokter_peminta_pemeriksaan_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Diagnosa</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_diagnosa" <?=($required_diagnosa == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_diagnosa" placeholder="Diagnosa (Indonesia Version)">{label_diagnosa}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_diagnosa_eng" placeholder="Diagnosa (English Version)">{label_diagnosa_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Catatan Pemeriksaan</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_catatan_pemeriksaan" <?=($required_catatan_pemeriksaan == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_catatan_pemeriksaan" placeholder="Catatan Pemeriksaan (Indonesia Version)">{label_catatan_pemeriksaan}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_catatan_pemeriksaan_eng" placeholder="Catatan Pemeriksaan (English Version)">{label_catatan_pemeriksaan_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Waktu Pemeriksaan</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_waktu_pemeriksaan" <?=($required_waktu_pemeriksaan == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_waktu_pemeriksaan" placeholder="Waktu Pemeriksaan (Indonesia Version)">{label_waktu_pemeriksaan}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_waktu_pemeriksaan_eng" placeholder="Waktu Pemeriksaan (English Version)">{label_waktu_pemeriksaan_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Prioritas</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_prioritas" <?=($required_prioritas == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_prioritas" placeholder="Prioritas (Indonesia Version)">{label_prioritas}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_prioritas_eng" placeholder="Prioritas (English Version)">{label_prioritas_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Pasien Puasa</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_pasien_puasa" <?=($required_pasien_puasa == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_pasien_puasa" placeholder="Pasien Puasa (Indonesia Version)">{label_pasien_puasa}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_pasien_puasa_eng" placeholder="Pasien Puasa (English Version)">{label_pasien_puasa_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Pengiriman Hasil</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_pengiriman_hasil" <?=($required_pengiriman_hasil == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_pengiriman_hasil" placeholder="Pengiriman Hasil (Indonesia Version)">{label_pengiriman_hasil}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_pengiriman_hasil_eng" placeholder="Pengiriman Hasil (English Version)">{label_pengiriman_hasil_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Alergi Bahan Kontras</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_alergi_bahan_kontras" <?=($required_alergi_bahan_kontras == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_alergi_bahan_kontras" placeholder="Alergi Bahan Kontras (Indonesia Version)">{label_alergi_bahan_kontras}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_alergi_bahan_kontras_eng" placeholder="Alergi Bahan Kontras (English Version)">{label_alergi_bahan_kontras_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Pasien Hamil</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_pasien_hamil" <?=($required_pasien_hamil == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_pasien_hamil" placeholder="Pasien Hamil (Indonesia Version)">{label_pasien_hamil}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_pasien_hamil_eng" placeholder="Pasien Hamil (English Version)">{label_pasien_hamil_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Catatan / Keterangan</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_catatan" <?=($required_catatan == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_catatan" placeholder="Catatan / Keterangan (Indonesia Version)">{label_catatan}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_catatan_eng" placeholder="Catatan / Keterangan (English Version)">{label_catatan_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 text-right">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mpengaturan_form_order_radiologi_mri" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>

			<br /><br />

	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
	});
</script>