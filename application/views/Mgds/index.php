<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1743'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_gds()"><i class="fa fa-bar-chart-o"></i> {title}</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('1743'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?>" id="tab_info">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
						<div class="form-group">
							<label class="col-xs-12" for="satuan_gds">Satuan GDS</label>
							<div class="col-xs-4">
								<div class="input-group">
									<input class="form-control" type="text" id="satuan_gds" name="satuan_gds" value="{satuan_gds}" placeholder="Satuan GDS">
									<span class="input-group-btn">
										<button class="btn btn-success" onclick="update_satuan()" type="button"><i class="fa fa-save"></i>  Update</button>
									</span>
								</div>
								
							</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_gds">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="40%">Range GDS</th>
											<th width="25%">Kategori</th>
											<th width="15%">Warna</th>
											<th width="15%">Action</th>										   
										</tr>
										<tr>
											<th>#<input class="form-control" type="hidden" id="gds_id" name="gds_id" value="" placeholder="id"></th>
											<th>
												<div class="input-group" style="width:100%">
													<input class="form-control decimal" type="text" id="gds_1" name="gds_1" placeholder="<?=$satuan_gds?>" value="" >
													<span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
													
													<input class="form-control decimal" type="text" id="gds_2" name="gds_2" placeholder="<?=$satuan_gds?>" value="" >
													
													
												</div>
											</th>
											<th>
												<input class="form-control" type="text" style="width:100%" id="kategori_gds" name="kategori_gds" placeholder="Kategori" value="" >
											</th>
											<th>
												
												<div class="js-colorpicker input-group colorpicker-element">
													<input class="form-control" type="text" id="warna" name="warna" value="#0000">
													<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
												</div>
											
											</th>
											<th>
												<div class="btn-group">
												<?php if (UserAccesForm($user_acces_form,array('1744'))){ ?>
												<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_gds" name="btn_tambah_gds"><i class="fa fa-save"></i> Simpan</button>
												<?}?>
												<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_gds()"><i class="fa fa-refresh"></i> Clear</button>
												</div>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			
			
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
	$(".decimal").number(true,1,'.',',');	
	load_gds();		
	
})	
function clear_gds(){
	$("#gds_id").val('');
	$("#gds_1").val('');
	$("#gds_2").val('');
	$("#kategori_gds").val('');
	$("#warna").val('#000');
	$("#btn_tambah_gds").html('<i class="fa fa-save"></i> Save');
	
}
function load_gds(){
	$('#index_gds').DataTable().destroy();	
	table = $('#index_gds').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mgds/load_gds', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function update_satuan(){
	let satuan_gds=$("#satuan_gds").val();
	
	if (satuan_gds==''){
		sweetAlert("Maaf...", "Tentukan Ssatuan", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mgds/update_satuan', 
		dataType: "JSON",
		method: "POST",
		data : {
				satuan_gds:satuan_gds,
				
				
			},
		complete: function(data) {
			location.reload();
		}
	});
}
$("#btn_tambah_gds").click(function() {
	let gds_id=$("#gds_id").val();
	let gds_1=$("#gds_1").val();
	let gds_2=$("#gds_2").val();
	let kategori_gds=$("#kategori_gds").val();
	let warna=$("#warna").val();
	
	if (gds_1==''){
		sweetAlert("Maaf...", "Tentukan Range GDS", "error");
		return false;
	}
	if (gds_2==''){
		sweetAlert("Maaf...", "Tentukan Range GDS", "error");
		return false;
	}
	if (kategori_gds==''){
		sweetAlert("Maaf...", "Tentukan Kategori", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mgds/simpan_gds', 
		dataType: "JSON",
		method: "POST",
		data : {
				gds_id:gds_id,
				gds_1:gds_1,
				gds_2:gds_2,
				kategori_gds:kategori_gds,
				warna:warna,
				
			},
		complete: function(data) {
			clear_gds();
			$('#index_gds').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
function edit_gds(gds_id){
	$("#gds_id").val(gds_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mgds/find_gds',
		type: 'POST',
		dataType: "JSON",
		data: {id: gds_id},
		success: function(data) {
			$("#btn_tambah_gds").html('<i class="fa fa-save"></i> Edit');
			$("#gds_id").val(data.id);
			$("#gds_1").val(data.gds_1);
			$("#gds_2").val(data.gds_2);
			$("#warna").val(data.warna).trigger('change');
			$("#kategori_gds").val(data.kategori_gds);
			
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_gds(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus GDS?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mgds/hapus_gds',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_gds').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

</script>