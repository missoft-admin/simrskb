<?= ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        
        <h3 class="block-title">{title}</h3>
    </div>
	<div class="block-content">
		  <?php echo form_open('Tbuku_besar/export','class="form-horizontal" id="form-work" target="_blank"') ?>
		<input type="hidden" class="form-control" disabled id="idakun" name="idakun" value="{idakun}" />
		
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">Tanggal</label>
				<div class="col-md-5">
					<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
						<input class="form-control" type="text" id="tanggal_trx1" name="tanggal_trx1" placeholder="From" value="<?=($tanggal_trx1==''?HumanDateShort($tanggal_awal_akuntansi):$tanggal_trx1)?>"/>
						<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
						<input class="form-control" type="text" id="tanggal_trx2" name="tanggal_trx2" placeholder="To" value="{tanggal_trx2}"/>
					</div>
				</div>				
			</div>
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">No Akun</label>
				<div class="col-md-11">
					<select name="idakun_arr[]" id="idakun_arr" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Akun" multiple>
						<?php foreach  ($list_akun as $row) { ?>
							<option value="'<?=$row->id?>'" <?=($row->id==$idakun?'selected':'')?>><?=$row->noakun.' - '.$row->namaakun?></option>
						<?php } ?>
					</select>
				</div>
				
			</div>
			
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">No. Bukti</label>
				<div class="col-md-5">
					<input type="text" class="form-control" <?=$disabel?> placeholder="No. Bukti" name="notransaksi" id="notransaksi" value="{notransaksi}" />
				</div>
				<label class="col-md-1" for="">No. Validasi</label>
				<div class="col-md-5">
					<input type="text" class="form-control" <?=$disabel?> placeholder="No. Validasi" id="nojurnal" name="nojurnal" value="{nojurnal}" />
				</div>
			</div>
			
			<div class="form-group" style="margin-bottom: 15px;margin-top: 5px;">
				<label class="col-md-1" for="">Jenis Jurnal</label>
				<div class="col-md-11">
					<select name="ref_validasi[]" id="ref_validasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Jenis Jurnal" multiple>
						<?php foreach  ($list_ref as $row) { ?>
							<option value="'<?=$row->ref_validasi?>'"><?=$row->nama?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			
			<div class="form-group" style="margin-bottom: 15px;">
				<label class="col-md-1" for=""></label>
				<div class="col-md-11">
					<button class="btn btn-info" type="button" id="btn_filter"><i class="fa fa-filter"></i> Filter</button>
					<button class="btn btn-success" type="submit" name="btn_export" value="1"><i class="fa fa-file-excel-o"></i> Export Excel</button>
					<button  class="btn btn-danger" type="submit" name="btn_export" value="2"><i class="fa fa-file-pdf-o"></i> Export PDF</button>
					<button  class="btn btn-primary" type="submit" name="btn_export" value="3"><i class="fa fa-print"></i> Print</button>
				</div>
			</div>
			
			<?php echo form_close(); ?>
			
    </div>
</div>
<div class="block">
    <div class="block-header">        
        <h3 class="block-title">Detail Transaksi</h3>
    </div>
	<div class="block-content">
		 <div class="form-horizontal">		
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">No. Akun</label>
				<div class="col-md-5">
					<input type="text" class="form-control" readonly placeholder="No Akun" id="noakun" value="{noakun}" />
				</div>
				<label class="col-md-1" for="">Pos Saldo</label>
				<div class="col-md-5">
					<input type="text" class="form-control" readonly placeholder="Posisi Saldo" id="posisi" name="posisi" value="{posisi}" />
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">Nama Akun</label>
				<div class="col-md-5">
					<input type="text" class="form-control" readonly placeholder="Nama Akun" id="namaakun" value="{namaakun}" />
				</div>
				<label class="col-md-1" for="">Saldo Awal</label>
				<div class="col-md-5">
					<input type="text" class="form-control decimal" readonly placeholder="Saldo Awal" id="saldoawal" name="saldoawal" value="{saldoawal}" />
					<input type="hidden" readonly class="form-control" readonly placeholder="Offset" id="offset" name="offset" value="0" />
					<input type="hidden" readonly class="form-control" readonly placeholder="Offset" id="tmp_idakun" name="tmp_idakun" value="" />
					<input type="hidden" readonly class="form-control" readonly placeholder="Offset" id="tmp_saldo" name="tmp_saldo" value="" />
					<input type="hidden" readonly class="form-control" readonly placeholder="Offset" id="tmp_no" name="tmp_no" value="" />
					<input type="hidden" readonly class="form-control" readonly placeholder="Offset" id="jml_page" name="jml_page" value="" />
					<input type="hidden" readonly class="form-control" readonly placeholder="Offset" id="jml_all" name="jml_all" value="0" />
				</div>
			</div>
			<hr>
				<div class="table-responsive">
				<table class="table table-bordered table-striped table-header-bg" id="index_list" style="width: 100%;">
					<thead>
						<tr>
							<th width="5%" class="text-center">No</th>
							<th width="8%" class="text-center">Tanggal</th>
							<th width="8%" class="text-center">No Validasi</th>
							<th width="8%" class="text-center">No. Bukti</th>
							<th width="15%" class="text-center">Nama Akun</th>
							<th width="8%" class="text-center">Debit</th>
							<th width="8%" class="text-center">Kredit</th>
							<th width="10%" class="text-center">Saldo</th>
							<th width="12%" class="text-center">Keterangan</th>
							<th width="10%" class="text-center">Asal Jurnal</th>
							<th width="10%" class="text-center">Aksi</th>
						</tr>
					</thead>
					<tbody>						
					</tbody>
					<tfoot>
						<tr>
							<td colspan="9"></td>
							<td colspan="2" class="text-right">
									<div class="btn-group">
										<button class="btn btn-info btn-xs" type="button" id="btn_next"><i class="si si-control-forward"></i> Next</button>
										<button class="btn btn-danger btn-xs" type="button" id="btn_last"><i class="si si-control-end"></i> Last</button>
										
									</div>
							</td>
						</tr>
					</tfoot>
				</table>
		</div>
		</div>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var idakun;
var table;
var len_arr;
	$(document).ready(function(){
        idakun=$("#idakun").val();
		$(".decimal").number(true,2,'.',',');
		
		$("#offset").val('0');
		$("#tmp_idakun").val('');
		$("#tmp_saldo").val('0');
		$("#tmp_no").val('0');
		$("#jml_page").val('50');
		$("#btn_next").attr("style", "visibility: hidden");
		$("#btn_last").attr("style", "visibility: hidden");
		
		var idakun_arr=$("#idakun_arr").val();	
		len_arr=0;
		if (idakun_arr!=null){
			len_arr=idakun_arr.length;
			idakun=idakun_arr[0];
			// alert(idakun_arr[0]);
			if (len_arr=='1'){
				$("#offset").val('0');
				$("#tmp_idakun").val('');
				$("#tmp_saldo").val('0');
				$("#tmp_no").val('0');
				$("#jml_page").val('50');
				$("#jml_all").val('0');
				$('#index_list tbody').empty();
				load_info_akun();
				load_jurnal();		
			}
		}
    })
	
	$(document).on("click","#btn_filter",function(){	
		$("#offset").val('0');
		$("#tmp_idakun").val('');
		$("#tmp_saldo").val('0');
		$("#tmp_no").val('0');
		$("#jml_page").val('50');
		$("#jml_all").val('0');
		$('#index_list tbody').empty();
		load_info_akun();
		load_jurnal();		
	});
	$(document).on("click","#btn_next",function(){			
		load_jurnal();		
	});
	$(document).on("click","#btn_last",function(){		
		$("#jml_page").val($("#jml_all").val());
		load_jurnal();		
	});
	
	function load_jurnal(){
		var idakun_arr=$("#idakun_arr").val();		
		var ref_validasi=$("#ref_validasi").val();
		var tanggal_trx1=$("#tanggal_trx1").val();
		var tanggal_trx2=$("#tanggal_trx2").val();
		var notransaksi=$("#notransaksi").val();
		var nojurnal=$("#nojurnal").val();
		var offset=$("#offset").val();
		var tmp_saldo=$("#tmp_saldo").val();
		var tmp_idakun=$("#tmp_idakun").val();
		var tmp_no=$("#tmp_no").val();
		var jml_page=$("#jml_page").val();
		var jml_all=$("#jml_all").val();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Tbuku_besar/load_jurnal/',
			dataType: "json",
			type: "POST",
			data: {
					idakun_arr: idakun_arr,
					ref_validasi: ref_validasi,
					tanggal_trx1: tanggal_trx1,
					tanggal_trx2: tanggal_trx2,
					notransaksi: notransaksi,
					nojurnal: nojurnal,
					offset: offset,
					tmp_saldo: tmp_saldo,
					tmp_idakun: tmp_idakun,
					tmp_no: tmp_no,
					jml_page: jml_page,
					jml_all: jml_all,
			},
			success: function(data) {
				$('#index_list tbody').append(data.tabel);
				$("#offset").val(data.offset);
				$("#tmp_no").val(data.no);
				$("#jml_all").val(data.jml_all);
				$("#tmp_idakun").val(data.tmp_idakun);
				$("#tmp_saldo").val(data.tmp_saldo);
				if (data.no < data.jml_all){
					$("#btn_next").attr("style", "visibility: show");
					$("#btn_last").attr("style", "visibility: show");
				}else{
					$("#btn_next").attr("style", "visibility: hidden");
					$("#btn_last").attr("style", "visibility: hidden");
				}
				$("#cover-spin").hide();
				
			}
		});
		
		
	}
	function load_info_akun(){
		var idakun_arr=$("#idakun_arr").val();	
		var noakun='';var namaakun='';var posisi='';var saldoawal='';
		len_arr=0;
		if (idakun_arr!=null){
			len_arr=idakun_arr.length;
			var tanggal_trx1=$("#tanggal_trx1").val();
			idakun=idakun_arr[0];
			// alert(idakun_arr[0]);
			if (len_arr=='1'){
				$.ajax({
					url: '{site_url}Tbuku_besar/load_info_akun/',
					method: "POST",
					data : {idakun:idakun,tanggal:tanggal_trx1},
					dataType: "json",
					success: function(data) {
						noakun=data.noakun;			
						namaakun=data.namaakun;			
						posisi=data.possaldo;			
						saldoawal=data.saldo;			
						$("#noakun").val(noakun);
						$("#namaakun").val(namaakun);
						$("#posisi").val(posisi);
						$("#saldoawal").val(saldoawal);	
						$("#cover-spin").hide();				
					}
				});
			}
		}
		$("#noakun").val(noakun);
		$("#namaakun").val(namaakun);
		$("#posisi").val(posisi);
		$("#saldoawal").val(saldoawal);
		// var len_arr=idakun_arr.length;
	}
	function load_detail() {
		load_info_akun();
		var idakun_arr=$("#idakun_arr").val();		
		var ref_validasi=$("#ref_validasi").val();
		var tanggal_trx1=$("#tanggal_trx1").val();
		var tanggal_trx2=$("#tanggal_trx2").val();
		var notransaksi=$("#notransaksi").val();
		var nojurnal=$("#nojurnal").val();
		// alert(len_arr);
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}Tbuku_besar/load_index/',
			type: "POST",
			dataType: 'json',
			data: {
				idakun_arr: idakun_arr,
				ref_validasi: ref_validasi,
				tanggal_trx1: tanggal_trx1,
				tanggal_trx2: tanggal_trx2,
				notransaksi: notransaksi,
				nojurnal: nojurnal,
				
			}
		},
		columnDefs: [
					 {"targets": [0], "visible": false },
					 {  className: "text-right", targets:[6,5,7] },
					 {  className: "text-center", targets:[1,2,3,9] },
					 { "width": "5%", "targets": [0] },
					 { "width": "8%", "targets": [1,2,3,5,6,7,9] },
					 { "width": "8%", "targets": [10] },
					 { "width": "15%", "targets": [4] }
					 // { "width": "15%", "targets": [2,13] }

					]
		});
	}
</script>