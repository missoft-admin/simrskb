<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mjenis_info_icu" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('mjenis_info_icu/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="nama">No Urut	</label>
						<div class="col-md-10">
							<input type="text" class="form-control" id="nourut" placeholder="No Urut" name="nourut" value="{nourut}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label" for="nama">Jenis Informasi</label>
						<div class="col-md-10">
							<input type="text" class="form-control" id="nama" placeholder="Jenis Informasi" name="nama" value="{nama}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label" for="nama_english">Jenis Informasi (ENG)</label>
						<div class="col-md-10">
							<input type="text" class="form-control" id="nama_english" placeholder="Jenis Informasi English" name="nama_english" value="{nama_english}">
						</div>
					</div>
					
					
					<div class="form-group">
						<div class="col-md-12 col-md-offset-2">
							<button class="btn btn-success" name="btn_simpan" type="submit" value="1">Simpan</button>
							<a href="{base_url}mjenis_info_icu" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
						</div>
					</div>
					
				
			</div>

			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>

</div>
<input type="hidden" id="id" value="{id}">

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		
	});
	
	function loadFile_img(event){
		// alert('load');
		var output_img = document.getElementById('output_img');
		output_img.src = URL.createObjectURL(event.target.files[0]);
	}
	
	function validate_final(){
		
		
		if ($("#nama").val()==''){
			sweetAlert("Maaf...", "Tentukan Jenis Informasi, "error");
			return false;
		}
		
		$("*[disabled]").not(true).removeAttr("disabled");
		$("#cover-spin").show();
		return true;

		
	}
	
	
</script>