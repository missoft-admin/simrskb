<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<button data-toggle="modal" data-target="#EditPotonganDokterModal" class="btn addPotonganDokter"><i class="fa fa-plus"></i> Tambah</button>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<hr style="margin-top:0px">
		<div class="row">
			<?php echo form_open('tpotongan_dokter/filter','class="form-horizontal" id="form-work"') ?>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Tanggal</label>
					<div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                <input class="form-control" type="text" name="tanggal_dari" placeholder="From" value="{tanggal_dari}">
                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                <input class="form-control" type="text" name="tanggal_sampai" placeholder="To" value="{tanggal_sampai}">
            </div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="iddokter">Dokter</label>
					<div class="col-md-8">
						<select name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi..">
							<option value="#" selected>Semua Dokter</option>
							<?php foreach (get_all('mdokter', array('status' => '1')) as $row) { ?>
								<option value="<?=$row->id?>" <?=($iddokter == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="iddokter">Jenis Potongan</label>
					<div class="col-md-8">
						<select name="idpotongan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi..">
							<option value="#" selected>Semua Potongan</option>
							<?php foreach (get_all('mpotongan_dokter', array('status' => '1')) as $row) { ?>
								<option value="<?=$row->id?>" <?=($idpotongan == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="status_potongan">Status Potongan</label>
					<div class="col-md-8">
						<select name="status_potongan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi..">
							<option value="#" selected>Semua Status</option>
							<option value="1" <?=($status_potongan == '1' ? 'selected' : '')?>>Rutin</option>
							<option value="2" <?=($status_potongan == '2' ? 'selected' : '')?>>Non Rutin</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="status_periode">Status Periode</label>
					<div class="col-md-8">
						<select name="status_periode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi..">
							<option value="#" selected>Semua Periode</option>
							<option value="1" <?=($status_periode == '1' ? 'selected' : '')?>>Setiap Tanggal</option>
							<option value="2" <?=($status_periode == '2' ? 'selected' : '')?>>Setiap Bulan</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
		<hr>

		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No.</th>
					<th>Tanggal</th>
					<th>Dokter</th>
					<th>Potongan Dokter</th>
					<th>Catatan</th>
					<th>Nominal</th>
					<th>Status Pendapatan</th>
					<th>Status Periode</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tpotongan_dokter/getIndex/{uri}',
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": true },
					{ "width": "10%", "targets": 1, "orderable": true },
					{ "width": "10%", "targets": 2, "orderable": true },
					{ "width": "10%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 4, "orderable": true },
					{ "width": "10%", "targets": 5, "orderable": true },
					{ "width": "10%", "targets": 6, "orderable": true },
					{ "width": "10%", "targets": 7, "orderable": true },
					{ "width": "10%", "targets": 8, "orderable": true }
				]
			});
	});

	$(document).ready(function() {

	});
</script>

<?php $this->load->view('Tpotongan_dokter/modal'); ?>
