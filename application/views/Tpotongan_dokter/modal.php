<div class="modal fade in black-overlay" id="EditPotonganDokterModal" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 50%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">POTONGAN DOKTER</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<input type="hidden" id="tPotonganDokterRowId" readonly value="">
							<div class="col-md-12">
								<div class="form-group formDokter">
									<label class="col-md-3 control-label">Dokter</label>
									<div class="col-md-9">
                    <select id="tPotonganDokterIdDokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi..">
        							<option value="" selected>Pilih Dokter</option>
        							<?php foreach (get_all('mdokter', array('status' => '1')) as $row) { ?>
        								<option value="<?=$row->id?>"><?=$row->nama?></option>
        							<?php } ?>
        						</select>
									</div>
								</div>
								<div class="form-group formDokter">
									<label class="col-md-3 control-label">Jenis Potongan</label>
									<div class="col-md-9">
                    <select id="tPotonganDokterIdPotongan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi..">
        							<option value="" selected>Pilih Dokter</option>
        							<?php foreach (get_all('mpotongan_dokter', array('status' => '1')) as $row) { ?>
        								<option value="<?=$row->id?>"><?=$row->nama?></option>
        							<?php } ?>
        						</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Catatan</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tPotonganDokterCatatan" placeholder="Catatan" value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Nominal</label>
									<div class="col-md-9">
										<input type="text" class="form-control number input-sm" id="tPotonganDokterNominal" placeholder="Nominal" value="">
									</div>
								</div>
                <div class="form-group">
									<label class="col-md-3 control-label">Status Potongan</label>
									<div class="col-md-9">
                    <select id="tPotonganDokterStatusPotongan" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                      <option value="">Pilih Opsi</option>
                      <option value="1">Rutin</option>
        							<option value="2">Non Rutin</option>
        						</select>
									</div>
								</div>
								<div class="form-group formRutin" hidden>
									<label class="col-md-3 control-label">Status Periode</label>
									<div class="col-md-9">
                    <select id="tPotonganDokterStatusPeriode" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
        							<option value="">Pilih Opsi</option>
        							<option value="1">Setiap Tanggal</option>
        							<option value="2">Setiap Bulan</option>
        						</select>
									</div>
								</div>
								<div class="form-group formNonRutin" hidden>
									<label class="col-md-3 control-label">Tanggal Pembayaran</label>
									<div class="col-md-9">
                    <select id="tPotonganDokterTanggalPembayaran" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
											<option value="">Pilih Opsi</option>
										</select>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-primary" id="saveDataPotonganDokter" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
				<button class="btn btn-sm btn-default" id="cancelDataPotonganDokter" type="button" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('.number').number(true, 0, '.', ',');

    $(document).on('click', '.addPotonganDokter', function() {
      $("#tPotonganDokterRowId").val('');
      $("#tPotonganDokterIdDokter").val('');
      $("#tPotonganDokterNominal").val('');
      $("#tPotonganDokterStatusPotongan").val('');
      $("#tPotonganDokterStatusPeriode").val('');
      $("#tPotonganDokterTanggalPembayaran").val('');
    });

    $(document).on('change', '#tPotonganDokterIdDokter', function() {
			loadPeriodePembayaranHonorDokter($(this).val());
		});

    $(document).on('change', '#tPotonganDokterStatusPotongan', function() {
			if ($(this).val() == '1') {
        $(".formRutin").show();
        $(".formNonRutin").hide();
      } else if ($(this).val() == '2') {
        $(".formRutin").hide();
        $(".formNonRutin").show();
      }
		});

    $(document).on('click', '.editPotonganDokter', function() {
		  var idrow = $(this).data('idrow');
		  var iddokter = $(this).data('iddokter');
		  var tanggal_pembayaran = $(this).data('tanggal_pembayaran');
		  var tanggal_jatuhtempo = $(this).data('tanggal_jatuhtempo');
		  var status_potongan = $(this).data('status_potongan');
		  var status_periode = $(this).data('status_periode');
			var catatan, nominal;

			catatan = $(this).closest('tr').find("td:eq(3)").html();
			nominal = $(this).closest('tr').find("td:eq(4)").html();

			$("#tPotonganDokterRowId").val(idrow);
			$("#tPotonganDokterIdDokter").val(iddokter);
			$("#tPotonganDokterCatatan").val(catatan);
			$("#tPotonganDokterNominal").val(jasamedis);
			$("#tPotonganDokterTanggalPembayaran").val(tanggal_pembayaran);
			$("#tPotonganDokterStatusPotongan").val(status_potongan);
			$("#tPotonganDokterStatusPeriode").val(status_periode);
		});

    $(document).on('click', '.deletePotonganDokter', function() {
  		var idrow = $(this).data('idrow');

  		event.preventDefault();
  		swal({
  				title: "Apakah anda yakin ingin menghapus data ini?",
  				type: "warning",
  				showCancelButton: true,
  				confirmButtonColor: "#DD6B55",
  				confirmButtonText: "Ya",
  				cancelButtonText: "Tidak",
  				closeOnConfirm: false,
  				closeOnCancel: false
  			}).then(function() {
  				$.ajax({
  					url: '{site_url}tpotongan_dokter/delete',
  					method: 'POST',
  					data: {
  						idrow: idrow
  					},
  					success: function(data) {
  						swal({
  							title: "Berhasil!",
  							text: "Data Potongan Dokter Telah Dihapus.",
  							type: "success",
  							timer: 1500,
  							showConfirmButton: false
  						});

  						location.reload();
  					}
  				});
  			});
  	});

    $(document).on('click', '#saveDataPotonganDokter', function() {
      var idrow = $('#tPotonganDokterRowId').val();
      var iddokter = $('#tPotonganDokterIdDokter option:selected').val();
      var namadokter = $('#tPotonganDokterIdDokter option:selected').text();
      var idpotongan = $('#tPotonganDokterIdPotongan option:selected').val();
      var catatan = $('#tPotonganDokterCatatan').val();
      var nominal = $('#tPotonganDokterNominal').val();
      var status_potongan = $('#tPotonganDokterStatusPotongan option:selected').val();
      var status_periode = $('#tPotonganDokterStatusPeriode option:selected').val();
      var tanggal_pembayaran = $('#tPotonganDokterTanggalPembayaran option:selected').val();
      var tanggal_jatuhtempo = $('#tPotonganDokterTanggalPembayaran option:selected').data('jatuhtempo');

      if(iddokter == ''){
        sweetAlert("Maaf...", "Dokter belum dipilih !", "error").then((value) => {
          $('#tPotonganDokterCatatan').focus();
        });
        return false;
      }

      if(catatan == ''){
        sweetAlert("Maaf...", "Catatan tidak boleh kosong !", "error").then((value) => {
          $('#tPotonganDokterCatatan').focus();
        });
        return false;
      }

      if(nominal == ''){
        sweetAlert("Maaf...", "Nominal tidak boleh kosong !", "error").then((value) => {
          $("#tPotonganDokterNominal").select2('open');
        });
        return false;
      }

      $.ajax({
        url: '{site_url}tpotongan_dokter/save',
        method: 'POST',
        data: {
          idrow: idrow,
          iddokter: iddokter,
          namadokter: namadokter,
          idpotongan: idpotongan,
          catatan: catatan,
          nominal: nominal,
          status_potongan: status_potongan,
          status_periode: status_periode,
          tanggal_pembayaran: tanggal_pembayaran,
          tanggal_jatuhtempo: tanggal_jatuhtempo
        },
        success: function(data) {
          swal({
            title: "Berhasil!",
            text: "Data Potongan Dokter Telah Tersimpan.",
            type: "success",
            timer: 1500,
            showConfirmButton: false
          });

          location.reload();
        }
      });
    });
  });

  function loadPeriodePembayaranHonorDokter(iddokter) {
		$.ajax({
			url: '{site_url}tverifikasi_transaksi/GetPeriodePembayaranHonorDokter/' + iddokter,
			dataType: "json",
			success: function(data) {
				$("#tPotonganDokterTanggalPembayaran").empty();
				$('#tPotonganDokterTanggalPembayaran').append(data.list_option);
			}
		});
	}
</script>
