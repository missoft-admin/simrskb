<?= ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        
        <h3 class="block-title">{title}</h3>
    </div>
	<div class="block-content">
	<?if ($jml=='0'){?>
		<div class="form-horizontal row col-md-12">			
			<div class="form-group">
				<div class="col-md-11">
					<button class="btn btn-info" type="button" id="btn_create"><i class="fa fa-plus"></i> Create</button>					
				</div>
			</div>
		</div>
	<?}?>
		<div class="table-responsive">
        <table class="table table-bordered table-striped" id="index_list" style="width: 100%;">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th width="15%">Periode</th>
                    <th width="10%">Tanggal Saldo Awal</th>
                    <th width="20%">Status</th>
                    <th width="10%">Status Close</th>
                    <th width="20%">Aksi</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div>
</div>
</div>
<div class="modal" id="modal_create" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg"  style="width:25%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title" id='title_barang'>Create Saldo Awal</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal">
						<input type="hidden" id="retur_id">
						<div class="row">
							<div class="col-md-12">								
								<div class="form-group" id="div_tgl_kbo">
									<label class="col-md-3 control-label" for="example-datetimepicker4">Tanggal Awal</label>
									<div class="col-md-9">
										<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
											<input class="js-datepicker form-control" type="text" id="tanggal_saldo" name="tanggal_saldo" placeholder="Choose a date..">
											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
					<button class="btn btn-sm btn-success" id="btn_update" type="button">Create</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
       var idkategori=$("#idkategori").val();
		$(".decimal").number(true,2,'.',',');
	   // alert(tanggalkontrabon);
		load_index();
    })
	$(document).on("click","#btn_create",function(){	
		$("#modal_create").modal('show');
	});
	$(document).on("click","#btn_update",function(){	
		if (!validate_detail()) return false;
		var tanggal=$("#tanggal_saldo").val();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Membuat Saldo Awal Periode "+tanggal+"?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			create_saldo();
		});
	
	});
	function atur_saldo($id){
		$("#modal_create").modal('show');
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}Mperiode_akun/get_data_saldo/'+$id,
			dataType: "json",
			success: function(data) {
				$("#nama_akun").val(data.noakun+' - '+data.namaakun);				
				$("#tanggal_terakhir").val(data.tanggal_terakhir);				
				$("#saldo_berjalan").val(data.saldo);				
				$("#saldo_set").val(0);				
				$("#idakun").val(data.id);	
				$("#cover-spin").hide();				
			}
		});
	}
	function validate_detail() {
		// alert('sini');
		if ($("#tanggal_saldo").val() == "" || $("#tanggal_saldo").val() == null) {
			sweetAlert("Maaf...", "Tanggal Harus diisi!", "error");
			$("#tanggal_saldo").focus();
			return false;
		}
		
		
		return true;
	}
	
	$(document).on("change","#tanggal_saldo",function(){	
		
		
	});
	function create_saldo(){
		$("#cover-spin").show();
		var tanggal_saldo=$("#tanggal_saldo").val();
		table = $('#index_list').DataTable()	
		$.ajax({
			url: '{site_url}Mperiode_akun/create_saldo',
			type: 'POST',
			data: {
				tanggal_saldo: tanggal_saldo,
				},
			complete: function() {
				$("#modal_create").modal('hide');
				$.toaster({priority : 'success', title : 'Succes!', message : ' Set Saldo'});
				table.ajax.reload( null, false ); 
				$("#cover-spin").hide();
			}
		});
	}
	
	function load_index() {
		// alert('sini');
		
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}Mperiode_akun/load_index/',
			type: "POST",
			dataType: 'json',
			data: {
				
			}
		},
		columnDefs: [
					 // {  className: "text-right", targets:[0,6] },
					 {  className: "text-center", targets:[2,3,4] },
					 { "width": "5%", "targets": [0] },
					 { "width": "15%", "targets": [2,3,4] },
					 { "width": "30%", "targets": [3,5] }
					 // { "width": "15%", "targets": [2,13] }

					]
		});
	}
</script>