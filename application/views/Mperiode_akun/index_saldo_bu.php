<?= ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        
        <h3 class="block-title">{title}</h3>
		<hr style="margin-top:20px">
		 <?php echo form_open('Mnomorakuntansi/export','class="form-horizontal" id="form-work" target="_blank"') ?>
		  <input type="hidden" id="idperiode" name="idperiode" value="{idperiode}">
		  <input type="hidden" id="st_aktif" name="st_aktif" value="{st_aktif}">
		  <div class="form-horizontal row col-md-12">
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">Kategori Akun</label>
				<div class="col-md-5">
					<select name="idkategori" id="idkategori" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="#" selected>- All Kategori -</option>
						<?php foreach  ($list_kategori as $row) { ?>
							<option value="<?=$row->id?>"><?=$row->nama?></option>
						<?php } ?>
					</select>
				</div>
				<label class="col-md-1" for="">Periode</label>
				<div class="col-md-5">
					<input class="form-control" disabled type="text" id="periode" name="periode" value="{periode_nama}">
				</div>
				
			</div>
		
			<div class="form-group" style="margin-bottom: 5px;margin-top: 15px;">
				<label class="col-md-1" for="">Header Akun</label>
				<div class="col-md-11">
					<select name="headerakun[]" id="headerakun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
						<?php foreach  ($list_header as $row) { ?>
							<option value="'<?=$row->noakun?>'"><?=$row->namaakun?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">Nomor Akun</label>
				<div class="col-md-11">
					<select name="noakun[]" id="noakun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
						<?php foreach  ($list_akun as $row) { ?>
							<option value="<?=$row->id?>"><?=$row->noakun.'-'.$row->namaakun?></option>
						<?php } ?>
					</select>
					
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 15px;">
				
			</div>
			<div class="form-group">
				<label class="col-md-1" for=""></label>
				<div class="col-md-11">
					<button class="btn btn-success" type="button" id="btn_filter"><i class="fa fa-filter"></i> Filter</button>
					<?if ($st_aktif=='0'){?>
					<button class="btn btn-primary" type="button" id="btn_posting"><i class="si si-check"></i> Posting Saldo</button>
					<?}?>
				</div>
			</div>
		</div>
		<?php echo form_close(); ?>
		
    </div>
	<div class="block-content">
		 
		<div class="table-responsive">
        <table class="table table-bordered table-striped" id="index_list" style="width: 100%;">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th width="5%">No</th>
                    <th width="15%">No Header</th>
                    <th width="10%">No Akun</th>
                    <th width="20%">Nama Akun</th>
                    <th width="10%">Kategori</th>
                    <th width="5%">Debit</th>
                    <th width="10%">Kredit</th>
                </tr>
            </thead>
            <tbody></tbody>
			<tfoot align="right">
				<tr><th class="text-center" colspan="6">TOTAL</th>
				<th><input class="form-control-sm decimal" disabled type="hidden" id="tot_debet" name="tot_debet"><label id="label_debet"></label></th>
				<th><input class="form-control decimal" disabled type="hidden" id="tot_kredit" name="tot_kredit"><label id="label_kredit"></label></th></tr>
			</tfoot>
        </table>
	
    </div>
    </div>
	
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var st_aktif;
var editor;
var table;
var idedit;
var idperiode;
var nominal_awal;
var nominal_debet;
var nominal_kredit;
	$(document).ready(function(){
		// alert(removeComa('5,600,000.00'))
       idperiode=$("#idperiode").val();
       st_aktif=$("#st_aktif").val();
       var idkategori=$("#idkategori").val();
		$(".decimal").number(true,2,'.',',');
		load_detail();	
		
		get_total_DK();
    })
	$(document).on("click","#btn_filter",function(){	
		load_detail();		
	});
	$(document).on("click","#btn_posting",function(){	
		
		if (!validate_detail()) return false;
		
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Memulai Dengan Periode yang telah disetting?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			simpan_posting();
		});


		
	});
	function validate_detail() {
		if ($("#tot_debet").val() != $("#tot_kredit").val()){
			sweetAlert("Maaf...", "Debit dan Kredit Berbeda", "error");
			return false;
		}
		if ($("#tot_debet").val() == 0){
			sweetAlert("Maaf...", "Debit dan Kredit Berbeda", "error");
			return false;
		}
		
		return true;
	}
	function simpan_posting(){
		
		$("#cover-spin").show();		
		table = $('#index_list').DataTable()	
		// alert(idedit + ' : ' + idperiode)
		$.ajax({
			url: '{site_url}mperiode_akun/simpan_posting',
			type: 'POST',
			dataType: 'json',
			data: {
				idperiode: idperiode,
				},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Set Saldo'});
				$("#cover-spin").hide();
				window.location.href = "<?php echo site_url('mperiode_akun/'); ?>";
			}
		});
	}
	function get_total_DK(){
		$.ajax({
			url: '{site_url}mperiode_akun/get_total_DK/'+idperiode,
			dataType: 'json',			
			success: function(data) {
				// $("#label_debet").html((2222.91));
				// alert(data.tot_debet);
				$("#tot_debet").val(data.tot_debet);
				$("#tot_kredit").val(data.tot_kredit);				
				$("#label_debet").html(addCommas(data.tot_debet));
				$("#label_kredit").html(addCommas(data.tot_kredit));
				if (data.tot_debet != data.tot_kredit){
					$("#btn_posting").attr('disabled',true);
				}else{
					$("#btn_posting").attr('disabled',false);
				}
				
			}
		});
	}
	$('#index_list tbody').on('click', '.money_debet', function() {
		if (st_aktif=='1') return false;
		if (!$('#index_list').hasClass("editing")) {
			tr = table.row( $(this).parents('tr') ).index()
			idedit=table.cell(tr,0).data();
			$('#index_list').addClass("editing");
			var thisCell = table.cell(this);
			// alert();
			nominal_awal=removeComa(thisCell.data());
			// alert(nominal_awal);
			thisCell.data(`<input type="text" class="change_debet" value="${thisCell.data()}"/>`);
			$(".change_debet").number(true,2,'.',',');
			$(".change_debet").select();
			$(".change_debet").focus();
		}
	});
	function removeComa($num){
		// alert(($num.replaceAll(',','')));
		return parseFloat(($num.replaceAll(',','')))
	}
	$('#index_list tbody').on("blur", ".change_debet", () => {
		nominal_debet=$(".change_debet").val();
		table.cell($(".change_debet").parents('td')).data($.number($(".change_debet").val(),2,'.',','));
		$('#index_list').removeClass("editing");
		// alert(nominal_awal + ' : '+nominal_debet);
		if (nominal_awal != nominal_debet){
			simpan_debet();			
		}
	});
	
	function simpan_debet(){
		$("#cover-spin").show();		
		table = $('#index_list').DataTable()	
		// alert(idedit + ' : ' + nominal_debet)
		$.ajax({
			url: '{site_url}mperiode_akun/simpan_debet',
			type: 'POST',
			dataType: 'json',
			data: {
				idperiode: idperiode,
				idedit: idedit,
				nominal: nominal_debet,
				},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Set Saldo'});
				$("#cover-spin").hide();
				get_total_DK();
			}
		});
	}
	$('#index_list tbody').on('click', '.money_kredit', function() {
		if (st_aktif=='1') return false;
		if (!$('#index_list').hasClass("editing")) {
			tr = table.row( $(this).parents('tr') ).index()
			idedit=table.cell(tr,0).data();
			$('#index_list').addClass("editing");
			var thisCell = table.cell(this);
			nominal_awal=removeComa(thisCell.data());
			thisCell.data(`<input type="text" class="change_kredit" value="${thisCell.data()}"/>`);
			$(".change_kredit").number(true,2,'.',',');
			$(".change_kredit").select();
			$(".change_kredit").focus();
		}
	});
	$('#index_list tbody').on("blur", ".change_kredit", () => {
		nominal_kredit=$(".change_kredit").val();
		table.cell($(".change_kredit").parents('td')).data($.number($(".change_kredit").val(),2,'.',','));
		$('#index_list').removeClass("editing");
		if (nominal_awal != nominal_kredit){
			simpan_kredit();			
		}
		// simpan_kredit();
	});
	
	function simpan_kredit(){
		$("#cover-spin").show();		
		table = $('#index_list').DataTable()	
		// alert(idedit + ' : ' + idperiode)
		$.ajax({
			url: '{site_url}mperiode_akun/simpan_kredit',
			type: 'POST',
			dataType: 'json',
			data: {
				idperiode: idperiode,
				idedit: idedit,
				nominal: nominal_kredit,
				},
			complete: function() {
				// alert(tot_debet);
				$.toaster({priority : 'success', title : 'Succes!', message : ' Set Saldo'});
				$("#cover-spin").hide();
				get_total_DK();
			}
		});
	}
	function load_detail() {
		// alert('sini');
		var idperiode=$("#idperiode").val();
		var idkategori=$("#idkategori").val();
		var headerakun=$("#headerakun").val();
		var noakun=$("#noakun").val();
		$('#index_list').DataTable().destroy();
		table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}mperiode_akun/load_detail/',
			type: "POST",
			dataType: 'json',
			data: {
				idperiode: idperiode,
				headerakun: headerakun,
				noakun: noakun,
				idkategori: idkategori,
			}
		},
		columnDefs: [
					{"targets": [0,2], "visible": false },
					 {  className: "text-right", targets:[0] },
					 {  className: "text-center", targets:[3,5] },
					 { "width": "3%", "targets": [1] },
					 { "width": "8%", "targets": [3,5] },
					 { "width": "10%", "targets": [6,7] },
					 { "width": "35%", "targets": [4] },
					 {  className: "text-right text-primary money_debet", targets:[6] },
					 {  className: "text-right text-danger  money_kredit", targets:[7] },
					  // { targets: [6,7], render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) }

					]
		});
		
	}
	function addCommas(inputText) {
		// pattern works from right to left
		var commaPattern = /(\d+)(\d{3})(\.\d*)*$/;
		var callback = function (match, p1, p2, p3) {
			return p1.replace(commaPattern, callback) + ',' + p2 + (p3 || '');
		};
		return inputText.replace(commaPattern, callback);
	}
	
	
</script>