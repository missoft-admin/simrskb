<?= ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error); ?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}mnomorakuntansi" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?= form_open('mnomorakuntansi/save','class="form-horizontal"') ?>
        <div class="form-group">
            <label class="col-md-2" for="">Kategori Akun</label>
            <div class="col-md-8">
                <select class="form-control js-select2" <?=$disabel?> id="kategoriakun" name="kategoriakun">
                    <?php $kategoriAkun = get_all('mkategori_akun', ['status' => '1']); ?>
                    <option value="#">Pilih Opsi</option>
                    <?php foreach ($kategoriAkun as $row) { ?>
                        <option value="<?= $row->id; ?>" <?=($kategoriakun == $row->id) ? 'selected' : '' ?> 
                            data-bertambah="<?= $row->jikabertambah; ?>"
                            data-berkurang="<?= $row->jikaberkurang; ?>"
                            data-possaldo="<?= $row->possaldo; ?>"
                            data-poslaporan="<?= $row->poslaporan; ?>"
                        ><?= $row->nama; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="">Tipe Akun</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-10" id="FormTipeAkun">
                        <select class="form-control js-select2" <?=$disabel?> id="tipeakun" name="tipeakun" style="width: 100%;">
                            <option value="1" <?=($tipeakun == '1') ? "selected" : "" ?>>Header Utama</option>
                            <option value="2" <?=($tipeakun == '2') ? "selected" : "" ?>>Akun Header Dari</option>
                            <option value="3" <?=($tipeakun == '3') ? "selected" : "" ?>>Sub Akun Dari</option>
                        </select>
                    </div>
                    <div class="col-md-2" id="FormHeaderUtama">
                        <input type="text" class="form-control" <?=$disabel?> id="xnoheader">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group" id="FormAkunHeader" <?= ($tipeakun == '1' ? 'style="display:none;"' : ''); ?>>
            <label class="col-md-2" for="">No. Header</label>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-10">
                        <select class="js-select2 form-control" <?=$disabel?> id="noheader" name="noheader" style="width:100%" data-placeholder="Pilih no header..">
                            <option value="#">Pilih Opsi</option>
                            <?php foreach ($list_akun as $row) { ?>
                                <option value="<?= $row->noakun; ?>" <?=($noheader == $row->noakun) ? 'selected' : '' ?> 
                                    data-bertambah="<?= $row->bertambah; ?>"
                                    data-berkurang="<?= $row->berkurang; ?>"
                                    data-possaldo="<?= $row->possaldo; ?>"
                                    data-poslaporan="<?= $row->poslaporan; ?>"
                                ><?= $row->noakun; ?> - <?= $row->namaakun; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <input type="text" <?=$disabel?> class="form-control" id="xnoakun">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="">Nomor Akun</label>
            <div class="col-md-8">
                <input type="text" class="form-control" <?=$disabel?> name="noakun" id="noakun" readonly value="{noakun}" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="">Nama Akun</label>
            <div class="col-md-8">
                <input type="text" class="form-control" <?=$disabel?> name="namaakun" value="{namaakun}" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="">Level Akun</label>
            <div class="col-md-8">
                <input type="text" class="form-control" <?=$disabel?> id="level" name="level" readonly value="{level}" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="">Jika Bertambah</label>
            <div class="col-md-8">
                <select class="form-control js-select2" <?=$disabel?> id="bertambah" name="bertambah">
                    <option value="0">Mengikuti Parent</option>
                    <option value="DB" <?=($bertambah == 'DB') ? "selected" : "" ?>>Debit</option>
                    <option value="CR" <?=($bertambah == 'CR') ? "selected" : "" ?>>Kredit</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="">Jika Berkurang</label>
            <div class="col-md-8">
                <select class="form-control js-select2" <?=$disabel?> id="berkurang" name="berkurang">
                    <option value="0">Mengikuti Parent</option>
                    <option value="DB" <?=($berkurang == 'DB') ? "selected" : "" ?>>Debit</option>
                    <option value="CR" <?=($berkurang == 'CR') ? "selected" : "" ?>>Kredit</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="">Pos Saldo</label>
            <div class="col-md-8">
                <select class="form-control js-select2" <?=$disabel?> id="possaldo" name="possaldo">
                    <option value="0">Mengikuti Parent</option>
                    <option value="DB" <?=($possaldo == 'DB') ? "selected" : "" ?>>Debit</option>
                    <option value="CR" <?=($possaldo == 'CR') ? "selected" : "" ?>>Kredit</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2" for="">Pos Saldo</label>
            <div class="col-md-8">
                <select class="form-control js-select2" <?=$disabel?> id="poslaporan" name="poslaporan">
                    <option value="0">Mengikuti Parent</option>
                    <option value="NRC" <?=($poslaporan == 'NRC') ? "selected" : "" ?>>Neraca</option>
                    <option value="LBR" <?=($poslaporan == 'LBR') ? "selected" : "" ?>>Laba Rugi</option>
                </select>
            </div>
        </div>
        
        <input type="hidden" id="parent_bertambah" name="parent_bertambah" value="">
        <input type="hidden" id="parent_berkurang" name="parent_berkurang" value="">
        <input type="hidden" id="parent_possaldo" name="parent_possaldo" value="">
        <input type="hidden" id="parent_poslaporan" name="parent_poslaporan" value="">
		<?if ($disabel==''){?>
        <div class="form-group">
            <div class="col-md-9 col-md-offset-3">
                <button class="btn btn-success" type="submit">Submit</button>
                <a href="{base_url}mnomorakuntansi" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
            </div>
        </div>
		<?}?>
        <?= form_hidden('id', $id); ?>
        <?= form_close() ?>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

    $(document).on("change", "#kategoriakun, #tipeakun", function() {
        const kategoriAkun = $("#kategoriakun option:selected").val();
        const tipeAkun = $("#tipeakun option:selected").val();

        if (tipeAkun == '1') {
            $("#FormHeaderUtama").show();
            $("#FormTipeAkun").removeClass('col-md-12');
            $("#FormTipeAkun").addClass('col-md-10');
        } else {
            $("#FormHeaderUtama").hide();
            $("#FormTipeAkun").removeClass('col-md-10');
            $("#FormTipeAkun").addClass('col-md-12');
        }

        getDataAkun(kategoriAkun, tipeAkun);
    });

    $(document).on("change", "#noheader", function() {
        resetAkun();

        const parentKategori = $("#noheader option:selected");
        const parentBertambah = parentKategori.data("bertambah");
        const parentBerkurang = parentKategori.data("berkurang");
        const parentPosSaldo = parentKategori.data("possaldo");
        const parentPosLaporan = parentKategori.data("poslaporan");

        $("#parent_bertambah").val(parentBertambah);
        $("#parent_berkurang").val(parentBerkurang);
        $("#parent_possaldo").val(parentPosSaldo);
        $("#parent_poslaporan").val(parentPosLaporan);

        const kategoriAkun = $("#kategoriakun option:selected").val();
        const tipeAkun = $("#tipeakun option:selected").val();

        if (tipeAkun == '2') {
            getNomorAkunAvailable(kategoriAkun, parentKategori.val(), 1);
        } else if (tipeAkun == '3') {
            getNomorAkunAvailable(kategoriAkun, parentKategori.val(), 2);
        }
    });

    $(document).on("change", "#xnoheader", function() {
        generateNoAkunHeader();
    });
    
    $(document).on("change", "#xnoakun", function() {
        generateNoAkun();
    });

    $("#bertambah").change(function() {
		const bertambah = $(this).val();
		if (bertambah == 'DB') {
			$("#berkurang").val('CR');
			$("#berkurang").select2("destroy");
			$("#berkurang").select2({ width: '100%' });
		} else if (bertambah == 'CR') {
			$("#berkurang").val('DB');
			$("#berkurang").select2("destroy");
			$("#berkurang").select2({ width: '100%' });
		} else {
            $("#berkurang").val('0');
			$("#berkurang").select2("destroy");
			$("#berkurang").select2({ width: '100%' });
        }
	});

	$("#berkurang").change(function() {
		const berkurang = $(this).val();
		if (berkurang == 'DB') {
			$("#bertambah").val('CR');
			$("#bertambah").select2("destroy");
			$("#bertambah").select2({ width: '100%' });
		} else if (berkurang == 'CR') {
			$("#bertambah").val('DB');
			$("#bertambah").select2("destroy");
			$("#bertambah").select2({ width: '100%' });
		} else {
            $("#bertambah").val('0');
			$("#bertambah").select2("destroy");
			$("#bertambah").select2({ width: '100%' });
        }
	});
});

function getNomorHeaderUtama(kategoriAkun) {
    $.ajax({
        url: '{base_url}mnomorakuntansi/getNomorHeaderUtama/' + kategoriAkun,
        success: function(noHeader) {
            $("#xnoheader").val(noHeader);
            $("#noakun").val(rpad(kategoriAkun + noHeader) + '.' + '00' + '.' + '00');
            $("#level").val('0');
        }
    });
}

function getAkunHeader(kategoriAkun) {
    $("#noheader").html("<option value=''>Pilih Opsi</option>");

    $.ajax({
        url: '{base_url}mnomorakuntansi/getAkunHeader/' + kategoriAkun,
        dataType: 'json',
        success: function(data) {
            data.map((item) => {
                $("#noheader").append(`<option value="${item.noakun}"
                    data-bertambah="${item.bertambah}"
                    data-berkurang="${item.berkurang}"
                    data-possaldo="${item.possaldo}"
                    data-poslaporan="${item.poslaporan}"
                >${item.noakun} - ${item.namaakun}</option>`);
            })
        }
    });
}

function getSubAkunHeader(kategoriAkun) {
    $("#noheader").html("<option value=''>Pilih Opsi</option>");

    $.ajax({
        url: '{base_url}mnomorakuntansi/getSubAkunHeader/' + kategoriAkun,
        dataType: 'json',
        success: function(data) {
            data.map((item) => {
                $("#noheader").append(`<option value="${item.noakun}"
                    data-bertambah="${item.bertambah}"
                    data-berkurang="${item.berkurang}"
                    data-possaldo="${item.possaldo}"
                    data-poslaporan="${item.poslaporan}" ${item.noakun == '{noakun}' ? 'selected' : ''}
                >${item.noakun} - ${item.namaakun}</option>`);
            })
        }
    });
}

function getNomorAkunAvailable(kategoriAkun, noHeader, level) {
    $("#xnoakun").val("");

    $.ajax({
        url: '{base_url}mnomorakuntansi/getNomorAkunAvailable/' + kategoriAkun  + '/' + noHeader + '/' + level,
        dataType: 'json',
        success: function(noAkun) {
            $("#xnoakun").val(noAkun);
            generateNoAkun(noAkun);
        }
    });
}

function checkDuplicateAkun(noAkun) {
    $.ajax({
        type: "POST",
        url: '{base_url}mnomorakuntansi/checkDuplicateAkun/',
        data: {
            noakun: noAkun, 
        },
        success: function(response) {
            if (response != 'available') {
                alert('Nomor akun telah digunakan!');
                $("#noakun").val('');
                $("#xnoakun").val('');
                $("#level").val('');
                $("#xnoakun").focus();
            }
        }
    });
}

function resetAkun() {
    $("#noakun").val('');
    $("#xnoakun").val('');
    $("#level").val('');
    $("#parent_bertambah").val('');
    $("#parent_berkurang").val('');
    $("#parent_possaldo").val('');
    $("#parent_poslaporan").val('');
}

function generateNoAkun(noAkunAvailable = '') {
    var string = $("#noheader").val();
    var count = 0;
    for (var i = 0; i < string.length; i++) {
        if (string.charAt(i) == '.') {
            ++count;
        }
    }
    
    var noAkun = '';
    const tipeAkun = $("#tipeakun option:selected").val();
    if (tipeAkun == '2') {
        if (noAkunAvailable == '') {
            noAkun = string.substr(0, 3) + rpad($("#xnoakun").val()) + '.00';
        } else {
            noAkun = string.substr(0, 3) + rpad(noAkunAvailable) + '.00';
        }
        level = 1;
    } else if (tipeAkun == '3') {
        if (noAkunAvailable == '') {
            noAkun = string.substr(0, 6) + lpad($("#xnoakun").val());
        } else {
            noAkun = string.substr(0, 6) + lpad(noAkunAvailable);
        }
        level = 2;
    }

    $("#noakun").val(noAkun);
    $("#level").val(level);
    
    checkDuplicateAkun(noAkun);
}

function generateNoAkunHeader() {
    const kategoriAkun = $("#kategoriakun option:selected").val();
    const noAkun = rpad(kategoriAkun + $("#xnoheader").val()) + '.00.00';
    const level = 0;

    $("#noakun").val(noAkun);
    $("#level").val(level);
    
    checkDuplicateAkun(noAkun);
}

function getDataAkun(kategoriAkun, tipeAkun) {
    if (tipeAkun == '1') {
        $("#FormAkunHeader").hide();

        const parentKategori = $("#kategoriakun option:selected");
        const parentBertambah = parentKategori.data("bertambah");
        const parentBerkurang = parentKategori.data("berkurang");
        const parentPosSaldo = parentKategori.data("possaldo");
        const parentPosLaporan = parentKategori.data("poslaporan");

        $("#parent_bertambah").val(parentBertambah);
        $("#parent_berkurang").val(parentBerkurang);
        $("#parent_possaldo").val(parentPosSaldo);
        $("#parent_poslaporan").val(parentPosLaporan);

        getNomorHeaderUtama(kategoriAkun);
    } else if (tipeAkun == '2') {
        $("#FormAkunHeader").show();
        getAkunHeader(kategoriAkun);
        resetAkun();
    } else if (tipeAkun == '3') {
        $("#FormAkunHeader").show();
        getSubAkunHeader(kategoriAkun);
        resetAkun();
    }
}

function lpad(d) {
    return (d < 10) ? '0' + d.toString() : d.toString();
}

function rpad(d) {
    return (d < 10) ? d.toString() + '0' : d.toString();
}

</script>
