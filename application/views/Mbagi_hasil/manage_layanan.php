<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}mbagi_hasil" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mbagi_hasil_setting/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			
			<div class="form-group" id="div_3">
				<label class="col-md-3 control-label" for="nama">Nama Bagi Hasil</label>
				<div class="col-md-7">
					<input type="text" class="form-control" readonly id="nama" placeholder="Nama Bagi Hasil" name="nama" value="{nama}">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="bagian_rs">Bagian RS %</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="text" class="form-control decimal" readonly id="bagian_rs" placeholder="Bagian RS" name="bagian_rs" value="{bagian_rs}">
						<span class="input-group-addon"><i class="fa fa-percent"></i></span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="bagian_rs">Bagian Pemilik Saham %</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="text" class="form-control decimal" readonly id="bagian_ps" placeholder="Bagian Pemilik Saham" name="bagian_ps" value="{bagian_ps}">
						<span class="input-group-addon"><i class="fa fa-percent"></i></span>
					</div>
					
				</div>
			</div>
			<div class="form-group" >
				<label class="col-md-3 control-label" for="nama">Tanggal Mulai</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input"  readonly type="text" id="tanggal_mulai" name="tanggal_mulai" value="<?=HumanDateShort($tanggal_mulai)?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
			</div>
			
			<?php echo form_close() ?>
	</div>

</div>

<? if ($id !=''){ ?>
<input type="hidden" class="form-control input-sm number" id="mbagi_hasil_id" name="mbagi_hasil_id"  value="{id}"/>	

<div class="block">
	<div class="block-header">
		<ul class="block-options">
				
		</ul>
		<h3 class="block-title">LIST TARIF LAYANAN</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
			<div class="form-group">
				<div class="col-md-12">
					<div class="btn-group">
							<button class="btn btn-success" type="button"><i class="fa fa-plus"></i> Tambah</button>
							<div class="btn-group">
								<button class="btn btn-success dropdown-toggle" data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li class="dropdown-header">Layanan</li>
									<li><a  href="javascript:open_modal(1);"> Administrasi</a></li>
									<li><a  href="javascript:open_modal(2);"> Rawat Jalan</a></li>
									<li><a  href="javascript:open_modal(3);"> Rawat Inap</a></li>
									<li><a  href="javascript:open_modal(4);"> Rawat Inap Visite</a></li>
									<li><a  href="javascript:open_modal(5);"> Radiologi</a></li>
									<li><a  href="javascript:open_modal(6);"> Laboratorium</a></li>
									<li><a  href="javascript:open_modal(7);"> Fisioterapi</a></li>
									<li><a  href="javascript:open_modal(9);"> Kamar Operasi</a></li>
									<li><a  href="javascript:open_modal(8);"> Sewa Alat</a></li>
									
								</ul>
							</div>
						</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<div class="table-responsive">
					<table class="table table-bordered table-striped" id="index_layanan" style="width: 100%;">
						<thead>
							<tr>
								<th hidden>Actions</th>
								<th hidden>Actions</th>
								<th style="width: 15%;">No</th>
								<th style="width: 15%;">Tipe</th>
								<th style="width: 20%;">Nama Tarif</th>	
								<th style="width: 10%;">Jasa Sarana</th>								
								<th style="width: 10%;">Jasa Pelayanan</th>	
								<th style="width: 10%;">Jasa BHP</th>	
								<th style="width: 10%;">Biaya Perawatan</th>	
								<th style="width: 13%;">Actions</th>
							</tr>
							
						</thead>
						<tbody></tbody>							
					</table>
					
				</div>
				</div>
			</div>
			<?php echo form_close() ?>
	</div>

</div>

<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>

<? $this->load->view('Mbagi_hasil/modal/mod_cari_adm') ?>
<? $this->load->view('Mbagi_hasil/modal/mod_cari_rajal') ?>
<? $this->load->view('Mbagi_hasil/modal/mod_cari_ranap') ?>
<? $this->load->view('Mbagi_hasil/modal/mod_cari_ranap_visite') ?>
<? $this->load->view('Mbagi_hasil/modal/mod_cari_rad') ?>
<? $this->load->view('Mbagi_hasil/modal/mod_cari_lab') ?>
<? $this->load->view('Mbagi_hasil/modal/mod_cari_fis') ?>
<? $this->load->view('Mbagi_hasil/modal/mod_cari_sewa_alat') ?>
<? $this->load->view('Mbagi_hasil/modal/mod_cari_ko') ?>
<script type="text/javascript">
$(document).ready(function() {
	// $("#modal_ko").modal('show');
	// load_ko();
	load_pelayanan();
});
function open_modal($id){
	if ($id=='1'){
		$("#modal_administrasi").modal('show');
		// load_administrasi();
	}
	if ($id=='2'){
		$("#modal_rajal").modal('show');
		// load_rajal();
	}
	if ($id=='3'){
		$("#modal_ranap").modal('show');
		// load_ranap();
	}
	if ($id=='4'){
		$("#modal_ranap_visite").modal('show');
		// load_ranap();
	}
	if ($id=='5'){
		$("#modal_rad").modal('show');
		// load_ranap();
	}
	if ($id=='6'){
		$("#modal_lab").modal('show');
		// load_ranap();
	}
	if ($id=='7'){
		$("#modal_fis").modal('show');
		// load_ranap();
	}
	if ($id=='8'){
		$("#modal_sewa_alat").modal('show');
		// load_ranap();
	}if ($id=='9'){
		$("#modal_ko").modal('show');
		// load_ranap();
	}
}
function load_pelayanan(){

	var mbagi_hasil_id=$("#mbagi_hasil_id").val();
	
	$('#index_layanan').DataTable().destroy();
	table = $('#index_layanan').DataTable({
		autoWidth: false,
		searching: true,
		// "lengthChange": false,
		serverSide: true,
		"processing": true,
		"order": [],
		"pageLength": 10,
		"ordering": false,
		columnDefs: [{ "targets": [0,1], "visible": false },
						{ "width": "5%", "targets": [2,5,6,7,8] },
						{ "width": "10%", "targets": [3,9] },
						{ "width": "40%", "targets": [4] },
						{"targets": [4], className: "text-left" },
					 // {"targets": [], className: "text-right" },
						{"targets": [3,5,6,7,8,9], className: "text-center" }
					 ],
		ajax: { 
			url: '{site_url}mbagi_hasil/load_pelayanan', 
			type: "POST" ,
			dataType: 'json',
			data : {
					mbagi_hasil_id:mbagi_hasil_id
				   }
		}
	});
}
$(document).on('click', '.js_check', function() {
	var table = $('#index_layanan').DataTable();
	tr = table.row($(this).parents('tr')).index()
	var id=table.cell(tr,0).data();
	if ($(this).is(':checked')) {
		update_realtime(id,1,1);
	}else{
		update_realtime(id,1,0);
	}
});
$(document).on('click', '.jp_check', function() {
	var table = $('#index_layanan').DataTable();
	tr = table.row($(this).parents('tr')).index()
	var id=table.cell(tr,0).data();
	if ($(this).is(':checked')) {
		update_realtime(id,2,1);
	}else{
		update_realtime(id,2,0);
	}
});
$(document).on('click', '.bhp_check', function() {
	var table = $('#index_layanan').DataTable();
	tr = table.row($(this).parents('tr')).index()
	var id=table.cell(tr,0).data();
	if ($(this).is(':checked')) {
		update_realtime(id,3,1);
	}else{
		update_realtime(id,3,0);
	}
});
$(document).on('click', '.bp_check', function() {
	var table = $('#index_layanan').DataTable();
	tr = table.row($(this).parents('tr')).index()
	var id=table.cell(tr,0).data();
	if ($(this).is(':checked')) {
		update_realtime(id,4,1);
	}else{
		update_realtime(id,4,0);
	}
});
function update_realtime($id,$urutan,$nilai){
	$.ajax({
		url: '{site_url}mbagi_hasil/update_realtime',
		type: 'POST',
		data: {id: $id,urutan: $urutan,nilai: $nilai},
		complete: function() {
			$.toaster({priority : 'success', title : 'Succes!', message : ' Save Data'});
		}
	});
}
$(document).on('click', '.js_adm', function() {		
	if ($(this).is(':checked')) {
		$(this).closest('tr').find(".input_js").val('1');
	}else{
		$(this).closest('tr').find(".input_js").val('0');
	}
	hitung_cek_adm($(this));
});

$(document).on('click', '.jp_adm', function() {		
	if ($(this).is(':checked')) {
		$(this).closest('tr').find(".input_jp").val('1');
	}else{
		$(this).closest('tr').find(".input_jp").val('0');
	}
	hitung_cek_adm($(this));
});
$(document).on('click', '.bhp_adm', function() {		
	if ($(this).is(':checked')) {
		$(this).closest('tr').find(".input_bhp").val('1');
	}else{
		$(this).closest('tr').find(".input_bhp").val('0');
	}
	hitung_cek_adm($(this));
});
$(document).on('click', '.bp_adm', function() {		
	if ($(this).is(':checked')) {
		$(this).closest('tr').find(".input_bp").val('1');
	}else{
		$(this).closest('tr').find(".input_bp").val('0');
	}
	hitung_cek_adm($(this));
});

	function simpan_layanan($nama_tabel){
		var arr_id=[];
		var arr_js=[];
		var arr_jp=[];
		var arr_bhp=[];
		var arr_bp=[];
		var arr_tabel=[];
		var mbagi_hasil_id=$("#mbagi_hasil_id").val();
		// alert($nama_tabel);
		$('#'+$nama_tabel+' tbody tr').each(function() {		
			if (parseInt($(this).find(".input_tot").val())>0){
				arr_js.push($(this).find(".input_js").val());
				arr_jp.push($(this).find(".input_jp").val());
				arr_bhp.push($(this).find(".input_bhp").val());
				arr_bp.push($(this).find(".input_bp").val());
				arr_id.push($(this).find(".input_id").val());
				arr_tabel.push($(this).find(".input_tabel").val());
				console.log($(this).find(".input_tabel").val());
			}
		
        });
		
		var table = $('#'+$nama_tabel).DataTable();
		var table2 = $('#index_layanan').DataTable();
		$.ajax({
			url: '{site_url}mbagi_hasil/simpan_layanan',
			type: 'POST',
			data: {arr_id: arr_id,arr_tabel: arr_tabel,arr_js: arr_js,arr_jp: arr_jp,arr_bhp: arr_bhp,arr_bp: arr_bp,mbagi_hasil_id: mbagi_hasil_id},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Save Data'});
				table.ajax.reload( null, false );
				table2.ajax.reload( null, false );
			}
		});
	}
	function hitung_cek_adm($tab){
		var total;
		total=parseInt($tab.closest('tr').find(".input_js").val()) + parseInt($tab.closest('tr').find(".input_jp").val()) + parseInt($tab.closest('tr').find(".input_bhp").val()) + parseInt($tab.closest('tr').find(".input_bp").val());
		$tab.closest('tr').find(".input_tot").val(total)
		// alert(total);
	}
	function hapus_layanan($id){
		var id=$id;
		var table = $('#index_layanan').DataTable();
		swal({
				title: "Anda Yakin ?",
				text : "Untuk Hapus data?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				
					$.ajax({
						url: '{site_url}mbagi_hasil/hapus_layanan',
						type: 'POST',
						data: {id: id},
						complete: function() {
							$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
							table.ajax.reload( null, false );
						}
					});
			});
			
	}
	
</script>