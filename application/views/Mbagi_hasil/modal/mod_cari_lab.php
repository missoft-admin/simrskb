<div class="modal fade in black-overlay" id="modal_lab" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 50%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">PENCARIAN TARIF LABORATORIUM</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_lab">Tipe</label>
									<div class="col-md-9">
										<select name="idtipe_lab" id="idtipe_lab" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
											<option value="#" selected>Pilih Opsi</option>
											<option value="1">Umum</option>
											<option value="2">Pathologi Anatomi</option>
											<option value="3">PMI</option>
										</select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_lab">Nama</label>
									<div class="col-md-9">
										<input type="text" style="width: 100%"  class="form-control" id="nama_lab" placeholder="Nama Tarif" name="nama_lab" value="">									
									</select>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 10px;">
									<label class="col-md-3 control-label" for="jenis_lab">Header</label>
									<div class="col-md-9">
										<select name="idpath_lab" id="idpath_lab" class="js-select2 form-control" style="width: 100%;" data-placeholder=" - All Kategori - ">
											<option value="#" selected>- All -</option>
											
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="btn_cari_lab"></label>
									<div class="col-md-9">
										<button class="btn btn-sm btn-success" id="btn_cari_lab" type="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-search"></i> Cari</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<table id="tabel_lab" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 25%;" hidden></th>
								<th style="width: 25%;" hidden></th>
								<th style="width: 5%;">X</th>
								<th style="width: 10%;">TIPE</th>
								<th style="width: 20%;">NAMA TARIF</th>
								<th style="width: 10%;">JASA SARANA</th>
								<th style="width: 10%;">JASA PELAYANAN</th>
								<th style="width: 10%;">BHP</th>
								<th style="width: 10%;">BIAYA PERAWATAN</th>
								<th style="width: 20%;">ACTION</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">				
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Keluar</button>
				<button class="btn btn-sm btn-success" type="button" id="btn_simpan_lab">Simpan</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
var table;
	$(document).ready(function() {
		// load_lab();
	});
	$(document).on('click', '#btn_cari_lab', function() {
		load_lab();
	});
	$(document).on('click', '#btn_simpan_lab', function() {		
		simpan_layanan('tabel_lab');
	});
	function load_lab(){
		var idtipe_lab=$("#idtipe_lab").val();
		var nama_lab=$("#nama_lab").val();
		var idpath_lab=$("#idpath_lab").val();
		// alert(idpath_lab);
		$('#tabel_lab').DataTable().destroy();
		table = $('#tabel_lab').DataTable({
            autoWidth: false,
            searching: false,
            "lengthChange": false,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,		
			columnDefs: [{ "targets": [0,1,9], "visible": false },
							{ "width": "5%", "targets": [2,5,6,7,8] },
							{ "width": "10%", "targets": [3] },
							{ "width": "40%", "targets": [4] },
							{"targets": [4], className: "text-left" },
						 // {"targets": [], className: "text-right" },
							{"targets": [3,5,6,7,8,9], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}mbagi_hasil/load_lab', 
                type: "POST" ,
                dataType: 'json',
				data : {
						idtipe_lab:idtipe_lab,nama_lab:nama_lab,idpath_lab:idpath_lab
					   }
            }
        });
	}
	$("#idtipe_lab").change(function(){
		if ($(this).val()!=''){
			$.ajax({
			url: '{site_url}mtarif_laboratorium/find_headparent_all/'+$(this).val(),
			dataType: "json",
			success: function(data) {
			$('#idpath_lab').find('option').remove().end().append('<option value="#" selected>- All -</option>').val('#').trigger("liszt:updated");
			$('#idpath_lab').append(data.detail);
			
			}
			});
		}

	});

</script>
