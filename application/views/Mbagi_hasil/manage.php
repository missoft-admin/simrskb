<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<? if ($config==''){?>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mbagi_hasil" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mbagi_hasil/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			
			<div class="form-group" id="div_3">
				<label class="col-md-3 control-label" for="nama">Nama Bagi Hasil</label>
				<div class="col-md-7">
					<input type="hidden" class="form-control" id="config" placeholder="Nama Bagi Hasil" name="config" value="{config}">
					<input type="text" class="form-control" id="nama" placeholder="Nama Bagi Hasil" name="nama" value="{nama}">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="bagian_rs">Bagian RS %</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="text" class="form-control decimal" id="bagian_rs" placeholder="Bagian RS" name="bagian_rs" value="{bagian_rs}">
						<span class="input-group-addon"><i class="fa fa-percent"></i></span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="bagian_rs">Bagian Pemilik Saham %</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="text" class="form-control decimal" id="bagian_ps" placeholder="Bagian Pemilik Saham" name="bagian_ps" value="{bagian_ps}">
						<span class="input-group-addon"><i class="fa fa-percent"></i></span>
					</div>
					
				</div>
			</div>
			<div class="form-group" >
				<label class="col-md-3 control-label" for="nama">Pembayaran Asuransi</label>
				<div class="col-md-7">
					<select name="status_langsung" id="status_langsung" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="#" <?=($status_langsung =='#' ? 'selected="selected"':'')?>>- Pilih -</option>
						<option value="1" <?=($status_langsung == 1 ? 'selected="selected"':'')?>>Dibayar Langsung</option>
						<option value="2" <?=($status_langsung == 2 ? 'selected="selected"':'')?>>Menunggu Pembayaran Asuransi</option>
					</select>
				</div>
			</div>
			<div class="form-group" >
				<label class="col-md-3 control-label" for="nama">Tanggal Mulai</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input"  type="text" id="tanggal_mulai" name="tanggal_mulai" value="<?=($tanggal_mulai!=''?HumanDateShort($tanggal_mulai):'')?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="keterangan">Keterangan</label>
				<div class="col-md-7">
					<textarea class="form-control js-summernote" name="keterangan" id="keterangan"><?=$keterangan?></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mbagi_hasil" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>

</div>
<?}else{?>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mbagi_hasil" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mbagi_hasil/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			
			<div class="form-group" id="div_3">
				<label class="col-md-3 control-label" for="nama">Nama Bagi Hasil</label>
				<div class="col-md-7">
					<input type="text" class="form-control" readonly id="nama" placeholder="Nama Bagi Hasil" name="nama" value="{nama}">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="bagian_rs">Bagian RS %</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="text" class="form-control decimal" readonly id="bagian_rs" placeholder="Bagian RS" name="bagian_rs" value="{bagian_rs}">
						<span class="input-group-addon"><i class="fa fa-percent"></i></span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="bagian_rs">Bagian Pemilik Saham %</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="text" class="form-control decimal" readonly id="bagian_ps" placeholder="Bagian Pemilik Saham" name="bagian_ps" value="{bagian_ps}">
						<span class="input-group-addon"><i class="fa fa-percent"></i></span>
					</div>
					
				</div>
			</div>
			<div class="form-group" >
				<label class="col-md-3 control-label" for="nama">Tanggal Mulai</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input"  readonly type="text" id="tanggal_mulai" name="tanggal_mulai" value="<?=HumanDateShort($tanggal_mulai)?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
			</div>
			
			<?php echo form_close() ?>
	</div>

</div>
<?}?>
<? if ($id !=''){ ?>
<input type="hidden" class="form-control input-sm number" id="mbagi_hasil_id" name="mbagi_hasil_id"  value="{id}"/>	

<div class="block">
	<div class="block-header">
		<ul class="block-options">
       
    </ul>
		<h3 class="block-title">TANGGAL PERHITUNGAN</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
			<input type="hidden" class="form-control input-sm number" id="tanggal_id" name="tanggal_id"  value=""/>	
			<div class="form-group">
				<div class="col-md-12">
					<div class="table-responsive">
					<table class="table table-bordered table-striped" id="index_tanggal" style="width: 100%;">
						<thead>
							<tr>
								<th hidden>Actions</th>
								<th style="width: 25%;">Tanggal</th>
								<th style="width: 25%;">Deskripsi</th>
								<th style="width: 12%;">User</th>								
								<th style="width: 13%;">Actions</th>
							</tr>
							<tr>
								<td hidden></td>
								<td>
									<select name="tanggal_hari" tabindex="16"  style="width: 100%" id="tanggal_hari" data-placeholder="Tanggal" class="js-select2 form-control input-sm">
										<option value="#">-Tanggal-</option>
										<?for ($i=1;$i<=31;$i++){?>
										<option value="<?=$i?>"><?=$i?></option>
										<?}?>
									</select>										
								</td>
								<td>
									<input type="text" class="form-control input-sm" id="deskripsi" name="deskripsi"  value="" style="width:100%" />										
								</td>
														
								<td>									
									<?=$this->session->userdata('user_name').'<br>'.date('d-m-Y H:i:s')?>
								</td>
								
								<td>									
									<button type="button" class="btn btn-xs btn-primary" tabindex="8" id="btn_simpan_tanggal" title="Masukan Item"><i class="fa fa-save"></i> Simpan</button>
									<button type="button" class="btn btn-xs btn-success" tabindex="8" id="btn_update_tanggal" title="Update"><i class="fa fa-save"></i> Update</button>
									<button type="button" class="btn btn-xs btn-warning" tabindex="8" id="btn_batal_tanggal" title="Batal"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody></tbody>							
					</table>
					
				</div>
				</div>
			</div>
			<?php echo form_close() ?>
	</div>

</div>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
       
    </ul>
		<h3 class="block-title">PEMILIK SAHAM</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
			<input type="hidden" class="form-control input-sm number" id="ps_id" name="ps_id"  value=""/>	
			<div class="form-group">
				<div class="col-md-12">
					<div class="table-responsive">
					<table class="table table-bordered table-striped" id="index_pemilik_saham" style="width: 100%;">
						<thead>
							<tr>
								<th hidden>Actions</th>
								<th hidden>Actions</th>
								<th style="width: 25%;">Tanggal</th>
								<th style="width: 25%;">Tipe</th>
								<th style="width: 25%;">Lembar Saham</th>
								<th style="width: 12%;">User</th>								
								<th style="width: 13%;">Actions</th>
							</tr>
							<tr>
								<td hidden></td>
								<td hidden></td>
								<td>
									<select id="id_pemilik_saham" name="id_pemilik_saham" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="#">-Pilih Pemilik Saham-</option>
										<?foreach($list_ps as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>										
								</td>
								<td>
									<input type="text" class="form-control input-sm" readonly id="tipe_nama" name="tipe_nama"  value="" style="width:100%" />										
								</td>
								<td>
									<input type="text" class="form-control input-sm number" id="jumlah_lembar" name="jumlah_lembar"  value="" style="width:100%" />										
								</td>
														
								<td>									
									<?=$this->session->userdata('user_name').'<br>'.date('d-m-Y H:i:s')?>
								</td>
								
								<td>									
									<button type="button" class="btn btn-xs btn-primary" tabindex="8" id="btn_simpan_pemilik_saham" title="Masukan Item"><i class="fa fa-save"></i> Simpan</button>
									<button type="button" class="btn btn-xs btn-success" tabindex="8" id="btn_update_pemilik_saham" title="Update"><i class="fa fa-save"></i> Update</button>
									<button type="button" class="btn btn-xs btn-warning" tabindex="8" id="btn_batal_pemilik_saham" title="Batal"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody></tbody>							
					</table>
					
				</div>
				</div>
			</div>
			<?php echo form_close() ?>
	</div>

</div>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
       
    </ul>
		<h3 class="block-title">UPLOAD DOKUMEN</h3>
	</div>
	<div class="block-content ">
		<div class="row">
			<div class="form-group">
				<div class="col-md-12">
					<form class="dropzone" action="{base_url}mbagi_hasil/upload_files" method="post" enctype="multipart/form-data">
						<input name="idtransaksi" type="hidden" value="{id}">
					  </form>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="form-group">
				<div class="col-md-12">
					<div class="table-responsive">
					 <table class="table table-bordered table-striped table-responsive" id="listFile">
						<thead>
							<tr>
								<th width="1%">No</th>
								<th width="20%">File</th>
								<th width="15%">Upload By</th>
								<th width="5%">Size</th>
								<th width="5%">Aksi</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
				</div>
			</div>
		</div>
	</div>

</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var myDropzone 
	$(document).ready(function(){
		$(".number").number(true,0,'.',',');
		$(".decimal").number(true,2,'.',',');
		$('#keterangan').summernote({
			  height: 100,   //set editable area's height
			  codemirror: { // codemirror options
				theme: 'monokai'
			  }	
			})
		if ($("#config").val()!=''){
		Dropzone.autoDiscover = false;
		myDropzone = new Dropzone(".dropzone", { 
		   autoProcessQueue: true,
		   maxFilesize: 30,
		});
		myDropzone.on("complete", function(file) {
		  
		  refresh_image();
		  myDropzone.removeFile(file);
		  
		});
		}
		clear_all_tanggal();
		clear_all_pemilik_saham();
		load_tanggal();
		load_pemilik_saham();
		refresh_image();
	})	
	function refresh_image(){
		var id=$("#mbagi_hasil_id").val();
		$('#listFile tbody').empty();
		$.ajax({
			url: '{site_url}mbagi_hasil/refresh_image/'+id,
			dataType: "json",
			success: function(data) {
				// if (data.detail!=''){
					$('#listFile tbody').empty();
					$("#listFile tbody").append(data.detail);
				// }
				// console.log();
				
			}
		});
	}
	$("#bagian_rs").keyup(function(){
		var bagian_rs=$("#bagian_rs").val();
		var sisa=100 - parseFloat(bagian_rs);		
		$("#bagian_ps").val(sisa);
	});
	
	$("#bagian_ps").keyup(function(){
		var bagian_ps=$("#bagian_ps").val();
		var sisa=100 - parseFloat(bagian_ps);		
		$("#bagian_rs").val(sisa);
	});
	
	function validate_final(){
		var bagian_rs=$("#bagian_rs").val();
		var bagian_ps=$("#bagian_ps").val();
		var total_bagian=parseFloat(bagian_ps) + parseFloat(bagian_rs);
		
		
		if ($("#bagian_rs").val()==''){
			sweetAlert("Maaf...", "Tentukan Bagian RS", "error");
			return false;
		}
		if ($("#bagian_ps").val()==''){
			sweetAlert("Maaf...", "Tentukan Bagian Pemilik Saham", "error");
			return false;
		}
		if ($("#tanggal_mulai").val()==''){
			sweetAlert("Maaf...", "Tentukan Tanggal Mulai", "error");
			return false;
		}
		if ($("#status_langsung").val()=='#'){
			sweetAlert("Maaf...", "Pembayaran Asuransi Harus dipilih", "error");
			return false;
		}
		if (total_bagian !=100){
			alert(total_bagian);
			sweetAlert("Maaf...", "Persentase Pemilik Saham Salah", "error");
			return false;
		}
		return true;
	}
	// TANGGAL
	$(document).on("click","#btn_batal_tanggal",function(){	
		clear_all_tanggal();		
	});
	function clear_all_tanggal(){
		$('#tanggal_hari').val('#').trigger('change');
		$('#deskripsi').val('');
		$('#tanggal_id').val('');
		
		$("#btn_simpan_tanggal").show()
		$("#btn_update_tanggal").hide()
		$("#btn_batal_tanggal").hide()
	}
	function load_tanggal() {
		var mbagi_hasil_id=$("#mbagi_hasil_id").val();
		
		
		// $('#index_list').DataTable().destroy();
		var table = $('#index_tanggal').DataTable({
		"pageLength": 50,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"lengthChange": true,
		"order": [],
		"ajax": {
			url: '{site_url}mbagi_hasil/load_tanggal/',
			type: "POST",
			dataType: 'json',
			data: {
				mbagi_hasil_id: mbagi_hasil_id,
			}
		},
		columnDefs: [
					{"targets": [0], "visible": false },
					 // {  className: "text-right", targets:[2] },
					 {  className: "text-center", targets:[1,3] },
					 // { "width": "30%", "targets": [0,1] },
					 // { "width": "20%", "targets": [0,1,2,3,4] },
					 // { "width": "10%", "targets": [2,4] }
					 // { "width": "15%", "targets": [2,13] }

					]
		});
	}
	$(document).on("click","#btn_simpan_tanggal",function(){	
		var tanggal_hari=$("#tanggal_hari").val();
		var deskripsi=$("#deskripsi").val();
		
		if (tanggal_hari=='#'){
			sweetAlert("Maaf...", "Tanggal Harus diisi!", "error");
			return false;
		}
		if (deskripsi==''){
			sweetAlert("Maaf...", "Deskripsi Harus diisi!", "error");
			return false;
		}
		
		swal({
				title: "Anda Yakin ?",
				text : "Untuk Save Data ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				save_tanggal();
			});
	});
	$("#tanggal_hari").change(function(){
		cek_duplicate_tanggal();
	});
	function save_tanggal(){
		var mbagi_hasil_id=$("#mbagi_hasil_id").val();
		var tanggal_hari=$("#tanggal_hari").val();
		var deskripsi=$("#deskripsi").val();
		var table = $('#index_tanggal').DataTable();
		$.ajax({
			url: '{site_url}mbagi_hasil/save_tanggal',
			type: 'POST',
			data: {mbagi_hasil_id: mbagi_hasil_id,tanggal_hari: tanggal_hari,deskripsi: deskripsi},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Save Data'});
				table.ajax.reload( null, false );
				clear_all_tanggal();
			}
		});
		
	}
	function cek_duplicate_tanggal(){
		var tanggal_id=$("#tanggal_id").val();
		var mbagi_hasil_id=$("#mbagi_hasil_id").val();
		var tanggal_hari=$("#tanggal_hari").val();
		if (tanggal_hari !='#'){
		$.ajax({
			url: '{site_url}mbagi_hasil/cek_duplicate_tanggal/'+mbagi_hasil_id+'/'+tanggal_hari+'/'+tanggal_id,
			type: 'POST',
			// data: {id_jt: id_jt},
			dataType: "json",
			success: function(data) {
				var jml=data.detail.jml;
				if (jml > 0){
					sweetAlert("Maaf...", "Tanggal  Duplicate!", "error");
					$("#btn_simpan_tanggal").hide();
					clear_all_tanggal();
					
				}else{
					if ($("#id_jt").val()==''){
						$("#btn_simpan_tanggal").show()
						
					}
				}
			}
		});
		}
	}
	$(document).on("click",".edit_tanggal",function(){	
		var table = $('#index_tanggal').DataTable();
        tr = table.row($(this).parents('tr')).index()
		var tanggal_id=table.cell(tr,0).data();
		var tanggal_hari=table.cell(tr,1).data();
		var deskripsi=table.cell(tr,2).data();
		$('#tanggal_id').val(tanggal_id);
		$('#tanggal_hari').val(tanggal_hari).trigger('change');
		$('#deskripsi').val(deskripsi);
		$("#btn_simpan_tanggal").hide()
		$("#btn_update_tanggal").show()
		$("#btn_batal_tanggal").show()
	});
	$(document).on("click","#btn_update_tanggal",function(){	
		var tanggal_hari=$("#tanggal_hari").val();
		var deskripsi=$("#deskripsi").val();
		
		if (tanggal_hari=='#'){
			sweetAlert("Maaf...", "Tanggal Harus diisi!", "error");
			return false;
		}
		if (deskripsi==''){
			sweetAlert("Maaf...", "Deskripsi Harus diisi!", "error");
			return false;
		}
		swal({
				title: "Anda Yakin ?",
				text : "Untuk Update Data ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				update_tanggal();
			});
	});
	function update_tanggal(){
		var tanggal_id=$("#tanggal_id").val();
		var mbagi_hasil_id=$("#mbagi_hasil_id").val();
		var tanggal_hari=$("#tanggal_hari").val();
		var deskripsi=$("#deskripsi").val();
		var table = $('#index_tanggal').DataTable();
		$.ajax({
			url: '{site_url}mbagi_hasil/update_tanggal',
			type: 'POST',
			data: {tanggal_id: tanggal_id,mbagi_hasil_id: mbagi_hasil_id,tanggal_hari: tanggal_hari,deskripsi: deskripsi},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Update Data'});
				table.ajax.reload( null, false );
				clear_all_tanggal();
				// $("#cover-spin").hide();
				// filter_form();
			}
		});
		
	}
	$(document).on("click",".hapus_tanggal",function(){	
		var table = $('#index_tanggal').DataTable();
		tr = table.row($(this).parents('tr')).index()
		var tanggal_id=table.cell(tr,0).data();
		$('#tanggal_id').val(tanggal_id);
        swal({
				title: "Anda Yakin ?",
				text : "Untuk Hapus data?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				hapus_tanggal();
			});
	});
	function hapus_tanggal(){
		var tanggal_id=$("#tanggal_id").val();
		var table = $('#index_tanggal').DataTable();
		$.ajax({
			url: '{site_url}mbagi_hasil/hapus_tanggal',
			type: 'POST',
			data: {tanggal_id: tanggal_id},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
				table.ajax.reload( null, false );
				clear_all_tanggal();
				// $("#cover-spin").hide();
				// filter_form();
			}
		});
		
	}
	// PEMILIK SAHAM
	$(document).on("click","#btn_batal_pemilik_saham",function(){	
		clear_all_pemilik_saham();		
	});
	function clear_all_pemilik_saham(){
		$('#id_pemilik_saham').val('#').trigger('change');
		$('#jumlah_lembar').val('');
		$('#tipe_nama').val('');
		$('#ps_id').val('');
		
		$("#btn_simpan_pemilik_saham").show()
		$("#btn_update_pemilik_saham").hide()
		$("#btn_batal_pemilik_saham").hide()
	}
	function load_pemilik_saham() {
		var mbagi_hasil_id=$("#mbagi_hasil_id").val();
		
		var table = $('#index_pemilik_saham').DataTable({
		"pageLength": 50,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"lengthChange": true,
		"order": [],
		"ajax": {
			url: '{site_url}mbagi_hasil/load_pemilik_saham/',
			type: "POST",
			dataType: 'json',
			data: {
				mbagi_hasil_id: mbagi_hasil_id,
			}
		},
		columnDefs: [
					{"targets": [0,1], "visible": false },
					 // {  className: "text-right", targets:[2] },
					 {  className: "text-center", targets:[1,3] },
					 // { "width": "30%", "targets": [0,1] },
					 // { "width": "20%", "targets": [0,1,2,3,4] },
					 // { "width": "10%", "targets": [2,4] }
					 // { "width": "15%", "targets": [2,13] }

					]
		});
	}
	$(document).on("click","#btn_simpan_pemilik_saham",function(){	
		var id_pemilik_saham=$("#id_pemilik_saham").val();
		var jumlah_lembar=$("#jumlah_lembar").val();
		
		if (id_pemilik_saham=='#'){
			sweetAlert("Maaf...", "Pemilik Saham Harus diisi!", "error");
			return false;
		}
		if (jumlah_lembar=='' || jumlah_lembar=='0'){
			sweetAlert("Maaf...", "Lembar Saham Harus diisi!", "error");
			return false;
		}
		
		swal({
				title: "Anda Yakin ?",
				text : "Untuk Save Data ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				save_pemilik_saham();
			});
	});
	$("#id_pemilik_saham").change(function(){
		cek_duplicate_pemilik_saham();
		get_tipe_pemilik_saham();
	});
	function save_pemilik_saham(){
		var mbagi_hasil_id=$("#mbagi_hasil_id").val();
		var id_pemilik_saham=$("#id_pemilik_saham").val();
		var jumlah_lembar=$("#jumlah_lembar").val();
		var table = $('#index_pemilik_saham').DataTable();
		$.ajax({
			url: '{site_url}mbagi_hasil/save_pemilik_saham',
			type: 'POST',
			data: {mbagi_hasil_id: mbagi_hasil_id,id_pemilik_saham: id_pemilik_saham,jumlah_lembar: jumlah_lembar},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Save Data'});
				table.ajax.reload( null, false );
				clear_all_pemilik_saham();
			}
		});
		
	}
	function cek_duplicate_pemilik_saham(){
		var ps_id=$("#ps_id").val();
		var mbagi_hasil_id=$("#mbagi_hasil_id").val();
		var id_pemilik_saham=$("#id_pemilik_saham").val();
		if (id_pemilik_saham !='#'){
		$.ajax({
			url: '{site_url}mbagi_hasil/cek_duplicate_pemilik_saham/'+mbagi_hasil_id+'/'+id_pemilik_saham+'/'+ps_id,
			type: 'POST',
			// data: {id_jt: id_jt},
			dataType: "json",
			success: function(data) {
				var jml=data.detail.jml;
				if (jml > 0){
					sweetAlert("Maaf...", "Pemilik Saham  Duplicate!", "error");
					$("#btn_simpan_pemilik_saham").hide();
					clear_all_pemilik_saham();
					
				}else{
					if ($("#id_jt").val()==''){
						$("#btn_simpan_pemilik_saham").show()
						
					}
				}
			}
		});
		}
	}
	function get_tipe_pemilik_saham(){
		var id_pemilik_saham=$("#id_pemilik_saham").val();
		if (id_pemilik_saham !='#'){
		$.ajax({
			url: '{site_url}mbagi_hasil/get_tipe_pemilik_saham/'+id_pemilik_saham,
			type: 'POST',
			// data: {id_jt: id_jt},
			dataType: "json",
			success: function(data) {
				var tipe_nama=data.detail.tipe_nama;
				$("#tipe_nama").val(tipe_nama);
			}
		});
		}
	}
	$(document).on("click",".edit_pemilik_saham",function(){	
		var table = $('#index_pemilik_saham').DataTable();
        tr = table.row($(this).parents('tr')).index()
		var ps_id=table.cell(tr,0).data();
		var id_pemilik_saham=table.cell(tr,1).data();
		var jumlah_lembar=table.cell(tr,4).data();
		$('#ps_id').val(ps_id);
		// alert(id_pemilik_saham);
		$('#id_pemilik_saham').val(id_pemilik_saham).trigger('change');
		$('#jumlah_lembar').val(jumlah_lembar);
		$("#btn_simpan_pemilik_saham").hide()
		$("#btn_update_pemilik_saham").show()
		$("#btn_batal_pemilik_saham").show()
	});
	$(document).on("click","#btn_update_pemilik_saham",function(){	
		var id_pemilik_saham=$("#id_pemilik_saham").val();
		var jumlah_lembar=$("#jumlah_lembar").val();
		
		if (id_pemilik_saham=='#'){
			sweetAlert("Maaf...", "Pemilik Saham Harus diisi!", "error");
			return false;
		}
		if (jumlah_lembar=='' || jumlah_lembar=='0'){
			sweetAlert("Maaf...", "Lembar Saham Harus diisi!", "error");
			return false;
		}
		
		swal({
				title: "Anda Yakin ?",
				text : "Untuk Update Data ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				update_pemilik_saham();
			});
	});
	function update_pemilik_saham(){
		var ps_id=$("#ps_id").val();
		var mbagi_hasil_id=$("#mbagi_hasil_id").val();
		var id_pemilik_saham=$("#id_pemilik_saham").val();
		var jumlah_lembar=$("#jumlah_lembar").val();
		var table = $('#index_pemilik_saham').DataTable();
		$.ajax({
			url: '{site_url}mbagi_hasil/update_pemilik_saham',
			type: 'POST',
			data: {ps_id: ps_id,mbagi_hasil_id: mbagi_hasil_id,id_pemilik_saham: id_pemilik_saham,jumlah_lembar: jumlah_lembar},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Update Data'});
				table.ajax.reload( null, false );
				clear_all_pemilik_saham();
				// $("#cover-spin").hide();
				// filter_form();
			}
		});
		
	}
	$(document).on("click",".hapus_pemilik_saham",function(){	
		var table = $('#index_pemilik_saham').DataTable();
		tr = table.row($(this).parents('tr')).index()
		var ps_id=table.cell(tr,0).data();
		$('#ps_id').val(ps_id);
        swal({
				title: "Anda Yakin ?",
				text : "Untuk Hapus data?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				hapus_pemilik_saham();
			});
	});
	function hapus_pemilik_saham(){
		var ps_id=$("#ps_id").val();
		var table = $('#index_pemilik_saham').DataTable();
		$.ajax({
			url: '{site_url}mbagi_hasil/hapus_pemilik_saham',
			type: 'POST',
			data: {ps_id: ps_id},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
				table.ajax.reload( null, false );
				clear_all_pemilik_saham();
				// $("#cover-spin").hide();
				// filter_form();
			}
		});
		
	}
	$(document).on("click",".removeFile",function(){	
		  
	});
	function removeFile($id){
		 var id=$id;
        swal({
				title: "Anda Yakin ?",
				text : "Untuk Hapus data?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				$.ajax({
					url: '{site_url}mbagi_hasil/removeFile',
					type: 'POST',
					data: {id: id},
					complete: function() {
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
						refresh_image();
						// $("#cover-spin").hide();
						// filter_form();
					}
				});
			});
	}
</script>