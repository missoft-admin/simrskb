<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Print Pemesanan</title>
    <style>
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }
	  
      th {
        padding: 5px;
      }
	  td {
        padding: 5px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }
      
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;
		 
      }
	  td {
        padding: 3px;
		 
      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
    }
	
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <table class="content">
      <tr>
        <td rowspan="3" width="30%" class="text-center"><img src="{logo1_rs}" alt="" width="60" height="60"></td>
        <td rowspan="2" colspan="3" width="50%"  class="text-center text-bold text-judul"><u>PEMESANAN BARANG</u></td>
		<td rowspan="2" width="20%"></td>		
      </tr>
	  <tr></tr>
	  <tr>
		<td width="14%" class="text-left text-bold text-top">TANGGAL</td>
		<td width="20%" colspan="3" class="text-top">: <?=HumanDateLong($tanggal)?></td>		
	  </tr>
	  <tr>
	    <td  class="text-center">{alamat_rs1}</td>
		<td  class="text-left text-bold text-top">NO PEMESANAN</td>
		<td width="20%" colspan="3" class="text-top">: {nopemesanan}</td>
	  </tr>
	  <tr>
	    <td  class="text-center"><?=$telepon_rs.' '.$fax_rs?></td>
		<td class="text-left text-bold text-top">DISTRIBUTOR</td>
		<td class=" text-top" colspan="3" >: {namadistributor}</td>
		
	  </tr>
	  <tr>
	    <td  class="text-center"></td>
		<td class="text-left text-bold text-top">SUMBER DANA</td>
		<td class=" text-top" colspan="3" >: RUMAH SAKIT</td>
		
	  </tr>
    </table>
    
	<br>
    <table id="customers">
      <tr>
        <th width="4%" class="border-full text-right"><strong>NO</strong></th>
        <th width="8%" class="border-full text-center"><strong>KODE BARANG </strong></th>
        <th width="8%" class="border-full text-center"><strong>TIPE BARANG</strong></th>
        <th width="30%" class="border-full text-center"><strong>NAMA BARANG </strong></th>
        <th width="10%" class="border-full text-center"><strong>QTY</strong></th>
        <th width="8%" class="border-full text-center"><strong>SATUAN</strong></th>
        <th width="10%" class="border-full text-center"><strong>HARGA</strong></th>
        <th width="10%" class="border-full text-center"><strong>TOTAL</strong></th>
        <th width="10%" class="border-full text-center"><strong>QTY TERIMA</strong></th>
      </tr>
      <?php $number = 0; $total_bayar=0;$harga=0;?>
      <?php foreach ($list_detail as $row){ ?>
        <?php 
		$number = $number + 1; 
		if ($row->opsisatuan=='1'){
			$harga=$row->hargabeli*$row->kuantitas_kecil;
		}else{
			$harga=$row->hargabeli_besar*$row->kuantitas_besar;
			
		}
		$total_bayar = $total_bayar + $harga ;
		// $total=0;
		?>
        <tr>
          <td class="border-full text-right"> <?=$number?>&nbsp;&nbsp;</td>
          <td class="border-full text-center">&nbsp;&nbsp;<?=strtoupper($row->kode)?></td>
		  <td class="border-full text-center">&nbsp;&nbsp;<?=strtoupper($row->namatipe)?></td>
		  <td class="border-full text-left"><?=strtoupper($row->namabarang)?></td>
		  <td class="border-full text-center"><?php echo ($row->opsisatuan=='1'?$row->kuantitas_kecil:$row->kuantitas_besar) ?></td>
		  <td class="border-full text-center"><?php echo ($row->opsisatuan=='1'?$row->satuan_kecil:$row->satuan_besar).' / '.($row->opsisatuan=='1'?'KECIL':'BESAR') ?></td>
		  <td class="border-full text-right"><?php echo ($row->opsisatuan=='1'?number_format($row->hargabeli,0):number_format($row->hargabeli_besar,0)) ?></td>
		  <td class="border-full text-right"><?php echo ($row->opsisatuan=='1'?number_format($row->hargabeli*$row->kuantitas_kecil,0):number_format($row->hargabeli_besar*$row->kuantitas_besar,0)) ?></td>
		  <td class="border-full text-right"><?=number_format($row->kuantitas_terima)?></td>
        </tr>
      <? } ?>
		<tr>
			<td colspan="7"  class="border-full text-center text-bold">TOTAL</td>
			<td  class="border-full text-right text-bold"><?=number_format($total_bayar,0)?></td>
			<td  class="border-full text-right text-bold"></td>
			
		</tr>
    </table><br>
	
    
	<table>
		<tr>
			<td width="30%" class="text-left"><p class="text-muted">Faktur ini dianggap sebagai kwitansi yang sah.</p>	</td>
			<td width="70%" colspan="2" class="text-right">
			
			Tanggal Terima : <?=HumanDateLong($tanggalpenerimaan)?> | No. Penerimaan : <?=$nopenerimaan?> | No. Faktur : <?=$nofakturexternal?></td>
		</tr>
		<tr>
			<td width="30%" class="text-center">Paraf Petugas Gudang </td>
			<td width="40%"></td>
			<td width="30%" class="text-center">Paraf Petugas Menerima </td>
		</tr>
		<tr>
			<td width="30%" class="text-center"><img src="<?= base_url(); ?>qrcode/qr_code_ppa/<?= $mppa_id; ?>" width="100px"></td>
			<td width="40%"></td>
			<td width="30%" class="text-center"><img src="<?= base_url(); ?>qrcode/qr_code_ppa/<?= $mppa_id_terima; ?>" width="100px"></td>
		</tr>
		<tr>
			<td width="30%" class="text-center">({userpemesanan})</td>
			<td width="40%"></td>
			<td width="30%" class="text-center">({userpenerimaan})</td>
		</tr>
	</table>
  </body>
</html>
