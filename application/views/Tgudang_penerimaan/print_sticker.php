<!DOCTYPE html>
<html>
<!--untuk e-ticket
nanti di printer nya harus di setting kertas nya dulu
dengan ukuran 
160mm x 200 mm 
Ketas Label 121 Merk FOX
-->
<head>
	<title>Label Obat</title>
	<style type="text/css">
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
	body{
        font-family: 'font';
    }

		@page {
            margin-top: 2mm;
            margin-left: 25mm;
            margin-bottom: 2mm;
        }
		
		{
		  box-sizing: border-box;
		}

		.column {
		  float: left;
		  padding: 10px;
		  width: 78mm;
		  height : 35mm;
		}
		.row {
			margin-top: 2px;
			margin-bottom: 5px;
		}
		.row:after {
		  content: "";
		  display: table;
		  clear: both;
		}
		.b{
			font-size: 16px;
			text-align: center!important;
		}
		P.blocktext {
			font-size: 16px;
			margin-left: auto;
			margin-right: auto;
			text-align: center!important;
		}
		span.barcode {
			font-size: 31px;
			text-align: center!important;
			height: 25px;
			display: block;
		}
		span.blocktext {
			font-size: 16px;
			text-align: center!important;
			display: block;
			margin-top:15px;
		}
		
</style>
</head>

<body>
<?php  $generator = new Picqer\Barcode\BarcodeGeneratorPNG(); ?>
<?php 

for ($i=1; $i <= $akhir; $i+=2) {
$warna='#87CEEB';	

?>	
<div class="row">
  <div class="column" style="background:#fff">
    <span class="blocktext"><strong><?=$final[$i]['namabarang']?> </span>
	<span class="blocktext"><?=($final[$i]['namabarang'])?'No. Batch : ':''?><?=$final[$i]['nobatch']?></span>			
	<span class="blocktext"><?=($final[$i]['namabarang'])?'Tgl Terima : ':''?><?=$final[$i]['tgl_terima']?></span>			
	<span class="blocktext"><?=($final[$i]['namabarang'])?'Tgl Kadaluarsa : ':''?> <?=$final[$i]['tanggalkadaluarsa']?></P><br>
	<span class="barcode text-center" ><?=($final[$i]['namabarang'])?'<img width="100" src="data:image/png;base64,' . base64_encode($generator->getBarcode($final[$i]['barcode'], $generator::TYPE_CODE_128)) . '">':''?> </P><br>
  </div>

  <div class="column" style="background:#fff">
    <span class="blocktext"><strong><?=$final[$i+1]['namabarang']?> </span>
	<span class="blocktext"><?=($final[$i+1]['namabarang'])?'No. Batch : ':''?><?=$final[$i+1]['nobatch']?></span>			
	<span class="blocktext"><?=($final[$i+1]['namabarang'])?'Tgl Terima : ':''?><?=$final[$i+1]['tgl_terima']?></span>			
	<span class="blocktext"><?=($final[$i+1]['namabarang'])?'Tgl Kadaluarsa : ':''?> <?=$final[$i+1]['tanggalkadaluarsa']?></P><br>
	<span class="barcode"><?=($final[$i+1]['namabarang'])?'<img width="100" src="data:image/png;base64,' . base64_encode($generator->getBarcode($final[$i]['barcode'], $generator::TYPE_CODE_128)) . '">':''?> </P><br>
  </div>
</div>
	
			
	
	<? if ((($i+1) % 10 == 0)&&$akhir>($i+1)){?>
		<p style="height:0; page-break-before:always; margin:0; border-top:none; ">
	<?}?>
<?}?>
</body>
</html>
