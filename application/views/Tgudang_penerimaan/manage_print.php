<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<!-- Developer : @RendyIchtiarSaputra & @GunaliRezqiMauludi -->
<div class="block">
	<div class="block-header">
		
		<h3 class="block-title">{title}</h3>
	</div>
<?php echo form_open_multipart('tgudang_penerimaan/print_label','class="form-horizontal" id="form-work" target="_blank"') ?>
	<div class="block-content">
		<hr style="margin-top:0px">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-3 control-label" for="nomedrec">Label & Start</label>
					<div class="col-md-3">
					<select id="jenis" name="jenis" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
						<option value="1" selected >Label 121</option>
						<option value="2">Label 112</option>
					</select>
					</div>
					<div class="col-md-3">
					<select id="start_awal" name="start_awal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
						<?php for ($i=1; $i <= 10; $i++) {?>
							<option value="<?=$i?>" <?=($start_awal == $i ? 'selected' : '')?>><?=$i?></option>
						<?php } ?>
					</select>
					
					</div>
				</div>
				<input type="hidden" id="counter" name="counter" value="0">
				<input type="hidden" id="sisa" name="sisa" value="10">
				<input type="hidden" name="idpenjualan" value="{idpenjualan}">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-3 control-label" for=""></label>
					<div class="col-md-6">
						<button class="btn btn-success text-uppercase" disabled type="submit" id="button" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-print"></i> Cetak Label</button>
					</div>
				</div>

			</div>
			<div class="col-md-6">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<p><i class="fa fa-check"></i> Setting Kertas Printer A4</p>
				</div>
				
			</div>


		</div>
		<hr>
		<? $no=0;?>
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-obat">
			<thead>
				<tr>
					<th class="text-center" style="width: 5%;">
						<label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
							X
						</label>
					</th>
					<th style="width: 7%;">Qty</th>
					<th style="width: 10%;">NAMA BARANG</th>
					<th style="width: 10%;">NO. BATCH</th>
					<th style="width: 10%;">EXPIRED</th>
					<th style="width: 10%;">TGL TERIMA</th>
					<th style="width: 10%;">BARCODE</th>
				</tr>
			</thead>
			<tbody>
				<?
					foreach($list_data as $row){
					$no +=1;
				?>
					<tr>
						
						<td class="text-center">
							<label class="css-input css-checkbox css-checkbox-primary">
								<input type="checkbox" class="chck" id="chck_obat[<?=$no?>]" name="chck_obat[<?=$no?>]" checked  value="1"><span></span>
							</label>
						</td>
						<td hidden><input class="form-control " readonly type="text" id="namabarang[<?=$no?>]" name="namabarang[<?=$no?>]" value="<?=$row->namabarang?>"></td>
						<td hidden><input class="form-control " readonly type="text" id="nobatch[<?=$no?>]" name="nobatch[<?=$no?>]" value="<?=$row->nobatch?>"></td>
						<td hidden><input class="form-control " readonly type="text" id="tanggalkadaluarsa[<?=$no?>]" name="tanggalkadaluarsa[<?=$no?>]" value="<?=HumanDateShort($row->tanggalkadaluarsa)?>"></td>
						<td hidden><input class="form-control " readonly type="text" id="tgl_terima[<?=$no?>]" name="tgl_terima[<?=$no?>]" value="<?=HumanDateShort($row->tgl_terima)?>"></td>
						<td hidden><input class="form-control " readonly type="text" id="barcode[<?=$no?>]" name="barcode[<?=$no?>]" value="<?=$row->kode?>"></td>
						<td ><input class="form-control number"  type="text" id="jml[<?=$no?>]" name="jml[<?=$no?>]" value="<?=$row->kuantitas?>"></td>
						<td hidden><input class="form-control " readonly type="text" id="idetail[<?=$no?>]" name="idetail[<?=$no?>]" value="<?=$row->id?>"></td>
						<td hidden><input class="form-control " readonly type="text" id="checked[<?=$no?>]" name="checked[<?=$no?>]" value="1"></td>
						<td ><?=$row->namabarang?></td>
						<td ><?=$row->nobatch?></td>
						<td ><?=HumanDateShort($row->tanggalkadaluarsa)?></td>
						<td ><?=HumanDateShort($row->tgl_terima)?></td>
						<td ><?=$row->kode?></td>
					</tr>
					<?
				}?>
			</tbody>

		</table>
		
	</div>
	</div>
	<?php echo form_close() ?>
</div>


<!-- End Modal Action END PRINT -->

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
		// $('.number').number(true, 0);
		$(".number").number(true,0,',','.');
		hitung_sisa();
		$('input[type="checkbox"]').change(function(){
			if($(this).prop("checked") == true){
                // console.log("Checkbox is checked.");
				$(this).closest('tr').find("td:eq(8) input").val('1');
            }
            else if($(this).prop("checked") == false){
                // console.log("Checkbox is unchecked.");
				$(this).closest('tr').find("td:eq(8) input").val('0');
            }
			hitung_sisa();
			
		 })
		 $('#jenis').change(function(){
			// alert($(this).val());
			var akhir;
			if ($(this).val()=='2'){
				akhir=153;
			}else{
				akhir=10;
			}
			
			$('#start_awal')
					.find('option')
					.remove()			
					.end()
					.append('<option value="1">1</option>')
					.val('1').trigger("change");
			for (i = 2; i <= akhir; i++) {
			  $('#start_awal').append('<option value="' + i + '">' + i + '</option>');
			}

			
					
		 })
		 $('.number').keyup(function(){
			
			hitung_sisa();
			
		 })

	});
	$("#start_awal").change(function(){
		hitung_sisa();
	});
	function hitung_sisa(){
		
		var jml_print=0;
		$('#datatable-obat tbody tr').each(function(){
			// console.log($(this).find("td:eq(6) input").val());
			// var input = document.querySelector('input[type=checkbox]');
			if ($(this).find("td:eq(8) input").val()=='1'){
				jml_print=jml_print + parseFloat($(this).find("td:eq(6) input").val());
				// console.log('jml:'+jml_print);
			}
			
			// $(this).find('td').each(function(){
				// //do your stuff, you can use $(this) to get current cell
			// })
			
		})
		$("#counter").val(jml_print);
		if (parseFloat($("#counter").val())==0){
			$("#button").attr('disabled',true);
		}else{
			$("#button").attr('disabled',false);
		}
	}
</script>
