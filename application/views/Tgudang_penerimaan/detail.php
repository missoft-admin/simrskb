<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title} <?=($status=='0'?'<span class="label label-danger">DIBATALKAN</span>':'')?></h3>
    </div>

    <?php echo form_open('tgudang_penerimaan/save_edit', 'class="form-horizontal push-10-t" id="form-work"') ?>
    <div class="block-content block-content-narrow">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-3 control-label">No Penerimaan</label>
                    <div class="col-md-9">
                        <input type="hidden" class="form-control" name="id" id="id" value="{id}">
                        <input type="text" class="form-control" name="nopenerimaan" id="nopenerimaan" value="{nopenerimaan}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Distributor</label>
                    <div class="col-md-9">
                        <input class="form-control" disabled type="text" name="distributor" id="distributor" value="{distributor}">
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-md-3 control-label">No Faktur</label>
                    <div class="col-md-9">
                        <input disabled  class="form-control" type="text" name="nofakturexternal" id="nofakturexternal" value="{nofakturexternal}">
                        <input class="form-control" type="hidden" name="iddistributor" id="iddistributor" value="{iddistributor}">
                        <input class="form-control" type="hidden" name="st_hapus_alih" id="st_hapus_alih" value="0">
                    </div>
                </div>
				
                <div class="form-group">
					<label class="col-md-3 control-label" for="example-datetimepicker4">Tanggal Penerimaan</label>
					<div class="col-md-4">
						<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
                            <input disabled  class="form-control" type="text" id="tanggalpenerimaan" name="tanggalpenerimaan" placeholder="From" value="<?=DMYFormat2($tgl_terima)?>"/>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
					</div>
					
				</div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Keterangan</label>
                    <div class="col-md-9">
                        <textarea  disabled class="form-control" name="keterangan" id="keterangan"><?=$keterangan?></textarea>
                    </div>
                </div>
                
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-3 control-label">Total Barang</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control number" name="totalbarang" id="totalbarang" value="{totalbarang}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Total Harga</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control decimal" name="totalharga" id="totalharga" value="{totalharga}" disabled>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-3 control-label">Pembayaran</label>
                    <div class="col-md-9">
                        <select disabled name="pembayaran" id="pembayaran" class="form-control">
                            <option value="1" <?=($tipe_bayar=='1'?'selected':'')?>>Tunai</option>
                            <option value="2" <?=($tipe_bayar=='2'?'selected':'')?>>Kredit</option>
                        </select>
                    </div>
                </div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="example-datetimepicker4">Tanggal Jatuh Tempo</label>
					<div class="col-md-4">
						<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
							<input  disabled class="js-datepicker form-control" type="text" id="tanggaljatuhtempo" name="tanggaljatuhtempo" value="<?=($tipe_bayar=='2'?DMYFormat2($tanggaljatuhtempo):'')?>" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">

							<span class="input-group-addon">
								<span class="fa fa-calendar"></span>
							</span>
						</div>
					</div>
				</div>
				<?if($status=='0'){?>
				<div class="form-group">
                    <label class="col-md-3 control-label">Alasan</label>
                    <div class="col-md-9">
                        <textarea  disabled class="form-control" name="alasanbatal" id="alasanbatal"><?=$alasanbatal?></textarea>
                    </div>
                </div>
				<?}?>
            </div>
            <div class="clearfix"></div>
            <hr />
			<div class="col-md-12" >
				
				<table class="table table-striped table-borderless table-header-bg" id="data-barang">
					<thead>
						<tr>
							<th width="5%" class="text-center">Tipe</th>
							<th width="20%" class="text-center">Barang</th>
							<th width="8%" class="text-center">Jumlah Pesan</th>
							<th width="8%" class="text-center">Harga + PPN</th>
							<th width="8%" class="text-center">Diskon</th>
							<th width="8%" class="text-center">Jumlah Terima</th>
							<th width="10%" class="text-center">Total</th>
							<th width="10%" class="text-center">Keterangan</th>
							<th width="10%" class="text-center">No.Batch</th>
							
						</tr>
					</thead>
					<tbody>
						<?foreach($detail as $row){?>
							<tr>
								<td class="text-center"><?=$row->namatipe?></td>
								<td class="text-left"><?=$row->nama_barang?></td>
								<td class="text-center"><?=$row->pesan.' '.$row->namasatuan.' / '.$row->jenissatuan?></td>
								<td class="text-right"><?=number_format($row->harga,2)?></td>
								<td class="text-right">(Rp) <?=number_format($row->nominaldiskon,2)?><br><?=number_format($row->diskon,0)?> (%)</td>
								<td class="text-center"><?=number_format($row->kuantitas,0).' '.$row->namasatuan?></td>
								<td class="text-right"><?=number_format($row->totalharga,2)?></td>
								<?if ($row->status=='1'){?>
									<?if ($row->pesan > $row->kuantitas){?>
										<td class="text-center"><span class="label label-warning">Diterima Sebagian</span></td><!-- 7 -->
									<?}else{?>
										<td class="text-center"><span class="label label-success">Diterima Semua</span></td>
									<?}?>
								<?}else{?>
									<td class="text-center"><span class="label label-danger">DIBATALKAN</span></td><!-- 7 -->
								<?}?>
								<td class="text-center"><?=$row->nobatch?></td><!-- 8 -->
								
								<td hidden></td>
								<td hidden><input type="text" class="form-control" name="e_iddetpesan[]" value="<?=$row->idpemesanandetail?>"></td>
								<td hidden></td><!-- 12 -->
								<td hidden><input type="text" class="form-control" name="e_idtipe[]" value="<?=$row->idtipe?>"></td><!-- 13 -->
								<td hidden><input type="text" class="form-control" name="e_idbarang[]" value="<?=$row->idbarang?>"></td>
								<td hidden><input type="text" class="form-control" name="e_iddet[]" value="<?=$row->id?>"></td>								
								<td hidden><input type="text" class="form-control" name="e_harga[]" value="<?=$row->harga-$row->nominalppn?>"></td>
								<td hidden><input type="text" class="form-control" name="e_ppn[]" value="<?=$row->nominalppn?>"></td>
								<td hidden><input type="text" class="form-control" name="e_harga_plus_ppn[]" value="<?=$row->harga?>"></td>
								<td hidden><input type="text" class="form-control" name="e_harga_diskon[]" value="<?=$row->nominaldiskon?>"></td>
								<td hidden><input type="text" class="form-control" name="e_harga_plus_diskon[]" value="<?=$row->harga-$row->nominaldiskon?>"></td>
								<td hidden><input type="text" class="form-control" name="e_harga_total_fix[]" value="<?=$row->totalharga?>"></td>
								<td hidden><input type="text" class="form-control" name="e_no_batch[]" value="<?=$row->nobatch?>"></td>
								<td hidden><input type="text" class="form-control" name="e_st_edit[]" value="0"></td><!-- 23 -->
								<td hidden><input type="text" class="form-control" name="e_st_hapus[]" value="0"></td>
								<td hidden><input type="text" class="form-control" name="e_st_alih[]" value="0"></td>
								<td hidden><input type="text" class="form-control" name="e_total_alih[]" value="0"></td><!-- 26 -->
								<td hidden><input type="text" class="form-control" name="e_kuantitas[]" value="<?=$row->kuantitas?>"></td>
								<td hidden><input type="text" class="form-control" name="e_kuantitas_sisa[]" value="<?=$row->pesan-($row->sudah_terima-$row->kuantitas)?>"></td>
								<td hidden><input type="text" class="form-control" name="e_harga_master[]" value="<?=$row->harga_master?>"></td>
								<td hidden><input type="text" class="form-control" name="e_tgl_kadaluarsa[]" value="<?=$row->tanggalkadaluarsa?>"></td>
								<td hidden><input type="text" class="form-control" name="e_jml_Terima[]" value="<?=$row->sudah_terima?>"></td>
								<td hidden><input type="text" class="form-control" name="e_satuan[]" value="<?=$row->namasatuan?>"></td>
								<td hidden><input type="text" class="form-control" name="e_opsisatuan[]" value="<?=$row->opsisatuan?>"></td>
							</tr>
						
						<?					
						}?>
					</tbody>
				</table>
			</div>
			<br>
			<br>
			<br>
			<hr />
			<div class="col-md-12" id="div_hapus" hidden>
				<h3 class="block-title">DATA HAPUS & ALIHKAN DISTRIBUTOR</h3>
				<table class="table table-striped table-borderless table-header-bg" id="data_hapus">
					<thead>
						<tr>
							<th width="5%" class="text-center">Tipe</th>
							<th width="15%" class="text-center">Barang</th>
							<th width="5%" class="text-center">Jumlah</th>
							<th width="15%" class="text-center">Keterangan</th>
							<th width="15%" class="text-center">Distributor</th>
							<th width="20%" class="text-center">Alasan</th>
							
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
        </div>
        <div style="margin-top:50px;margin-bottom:20px;" class="text-right">
			<?if ($this->uri->segment(4)){?>
           
			<?}else{?>
            <a href="{site_url}tgudang_penerimaan" class="btn btn-default btn-md">Kembali </a>
			<?}?>
			
        </div>
		<div class="row">
		<div class="col-md-6">
			<h4 class="block-title">Info User</h4>
			<table class="table table-bordered" id="data_user">
				<thead>
					<tr>
						<th width="5%" class="text-left">Jenis</th>
						<th width="15%" class="text-left">Nama User</th>
						<th width="5%" class="text-left">Tanggal</th>
						
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class='text-left'>USER PEMBUAT</td>
						<td  class='text-left'><?= $detail_user['userpemesanan'] ?></td>
						<td  class='text-left'><?= $detail_user['tgl_pemesanan'] ?></td>
					<tr>
					<tr>
						<td class='text-left'>USER KONFIRMASI</td>
						<td  class='text-left'><?= $detail_user['nama_user_konfirmasi'] ?></td>
						<td  class='text-left'><?= $detail_user['tanggal_konfirmasi'] ?></td>
					<tr>
					<tr>
						<td class='text-left'>USER BAG. KEUANGAN</td>
						<td  class='text-left'><?= $detail_user['nama_user_approval'] ?></td>
						<td  class='text-left'><?= $detail_user['tanggal_approval'] ?></td>
					<tr>
					<tr>
						<td class='text-left'>USER PEMESANAN</td>
						<td  class='text-left'><?= $detail_user['nama_user_finalisasi'] ?></td>
						<td  class='text-left'><?= $detail_user['tgl_finalisasi'] ?></td>
					<tr>
					<?if ($status=='0'){?>
					<tr>
						<td class='text-left'>USER PEMBATALAN</td>
						<td  class='text-left'><?= $detail_user_batal['nama_user_batal'] ?></td>
						<td  class='text-left'><?= $detail_user_batal['datetime_batal'] ?></td>
					<tr>
					<?}?>
					
					
				</tbody>
			</table>
		</div>
		<div class="col-md-6">
				<h4 class="block-title">Lampiran</h4>
				
				<div class="block">					
					 <table class="table table-bordered" id="data_user">
						<thead>
							<tr>
								<th width="50%" class="text-center">File</th>
								<th width="15%" class="text-center">Size</th>
								<th width="35%" class="text-center">User</th>
								<th width="35%" class="text-center">X</th>
								
							</tr>
						</thead>
						<tbody>
							<? foreach($list_lampiran as $r){ ?>
							<tr>
								<td class='text-left'><?echo '<a href="'.base_url().'assets/upload/penerimaan/'.$r->file_name.'" target="_blank">'.substr($r->file_name, 11).'</a>'?></td>
								<td class='text-left'><?=$r->size?></td>
								<td class='text-left'><?=$r->user_upload.'-'.HumanDateLong($r->tanggal_upload)?></td>
								<td class='text-left'><a href="{base_url}tgudang_penerimaan/hapus_lampiran/<?=$r->id?>/<?=$id?>" class="btn btn-xs btn-danger" title="Hapus"><li class="fa fa-trash-o"></li></button></td>
							<tr>
							<?}?>
						</tbody>
					</table>
				</div>
				
			</div>
		</div>
    </div>
    <?php echo form_close() ?>
</div>
<div class="modal" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title" id='title_barang'>Verifikasi Penerimaan BARANG</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal">
                        <input type="hidden" id="rowIndex">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Barang</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" readonly id="xnamabarang">
                                    </div>
									
                                </div>
                                <div id="div_harga" class="form-group" >
                                    <label class="col-md-3 control-label">Harga</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control number" id="xharga">
										<div id="val_harga" class="help-block animated fadeInDown"></div>
                                        <input type="hidden" class="form-control number" id="xharga_master">										
                                    </div>
									<label class="col-md-2 control-label">PPN</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control number" id="xppn">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="col-md-3 control-label">Harga + PPN</label>
                                    <div class="col-md-3">
                                        <input type="text" readonly class="form-control number" id="xharga_plus_ppn">
										
                                    </div>									
                                </div>
								<div class="form-group">
									<label class="col-md-3 control-label">Diskon Rp.</label>
									<div class="col-md-9">
										<div class="input-group">
											<input class="form-control number" type="text" id="xdiskon_rp"  placeholder="Disc Rp">
											<span class="input-group-addon">Diskon %</span>
											<input class="form-control diskon" type="text" id="xdiskon_persen"  placeholder="Disc %">
										</div>
									</div>
								</div>
								<div class="form-group">
                                    <label class="col-md-3 control-label">Harga Final Rp.</label>
                                    <div class="col-md-3">
                                        <input type="text" readonly class="form-control number" id="xharga_plus_diskon">										
                                    </div>									
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Kuantitas</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control number" id="xkuantitas">										
                                        <input type="hidden" readonly class="form-control number" id="xkuantitas_sisa">										
                                    </div>		
									<label class="col-md-2 control-label">Total Rp.</label>
                                    <div class="col-md-4">
                                        <input type="text" readonly class="form-control number" id="xharga_total_fix">
                                    </div>
                                </div>
                                
								<div class="form-group">
									<label class="col-md-3 control-label" for="example-datetimepicker4">Tgl Kadaluarsa</label>
									<div class="col-md-4">
										<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="js-datepicker form-control" type="text" id="tanggalkadaluarsa" name="tanggalkadaluarsa" placeholder="Choose a date..">
											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
										</div>
									</div>
								</div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">No Batch</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control detail" id="nobatch">
                                    </div>
                                </div>
                            </div>
                            
                                
                        </div>
                    </form>
                </div>
				 <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-sm btn-success" id="btn_update" type="submit">Update</button>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
    var table, t, tanggal;
	$(".number").number(true,0,'.',',');
	$(".diskon").number(true,2,'.',',');
	$(".decimal").number(true,2,'.',',');
	// $("#pembayaran").select2();
	$('#pembayaran').select2();
    $(document).ready(function() {
			
    });
	
	$("#pembayaran").change(function() {
		if ($(this).val()=='2'){
			if($("#tanggalpenerimaan").val()==''){
				sweetAlert("Maaf...", "Tanggal Penerimaan Harus diisi!", "error");
			}else{
				var tgl_terima=moment($("#tanggalpenerimaan").val(),'DD-MM-YYYY');
				$("#tanggaljatuhtempo").val(addDays(new Date(tgl_terima), 14));
			}
			// alert();			
		}
	});	
	$("#tanggalpenerimaan").change(function() {
		if ($("#pembayaran").val()=='2'){
			var tgl_terima=moment($("#tanggalpenerimaan").val(),'DD-MM-YYYY');
			// alert(tgl_terima);
			// $("#tanggaljatuhtempo").val(addDays(new Date(tgl_terima), 14));
			
		}
	});	
	function addDays(theDate, days) {
		// alert(theDate);
		var today=new Date(theDate.getTime() + days*24*60*60*1000);
		// var today = new Date();
		var dd = today.getDate();

		var mm = today.getMonth()+1; 
		var yyyy = today.getFullYear();
		if(dd<10) 
		{
			dd='0'+dd;
		} 

		if(mm<10) 
		{
			mm='0'+mm;
		} 
		today = dd+'-'+mm+'-'+yyyy;
		return today;
	}
	
	$("#btn_update").click(function() {
			if (validate()){
				$("#modal_edit").modal("hide");				
				$('#data-barang tbody tr').each(function() {
					var tr = $(this).closest('tr');					
					var $cells = $(this).children('td');
					var satuan=tr.find("td:eq(32) input").val();
					if (tr[0].sectionRowIndex==$("#rowIndex").val()){
						tr.find("td:eq(23) input").val('1');//st_edit
						tr.find("td:eq(3)").text(formatNumber($("#xharga_plus_ppn").val()));
						tr.find("td:eq(6)").text(formatNumber($("#xharga_total_fix").val()));
						tr.find("td:eq(4)").text(formatNumber($("#xdiskon_rp").val()));
						// alert($("xkuantitas").val()+' '+$("xkuantitas_sisa").val());
						if (parseFloat($("#xkuantitas").val()) < parseFloat($("#xkuantitas_sisa").val())){
							tr.find("td:eq(7)").html('<span class="label label-danger">Barang Sebagian</span>');
						}else{
							tr.find("td:eq(7)").html('<span class="label label-success">Semua Barang</span>');
						}
						tr.find("td:eq(5)").html('<span class="label label-info">'+formatNumber($("#xkuantitas").val())+' '+satuan+'</span>');
						tr.find("td:eq(11)").text($("#nobatch").val());
						tr.find("td:eq(22) input").val($("#nobatch").val());
						tr.find("td:eq(30) input").val($("#tanggalkadaluarsa").val());
						tr.find("td:eq(19) input").val($("#xdiskon_rp").val());
						tr.find("td:eq(16) input").val($("#xharga").val());
						tr.find("td:eq(17) input").val($("#xppn").val());
						tr.find("td:eq(18) input").val($("#xharga_plus_ppn").val());
						tr.find("td:eq(19) input").val($("#xdiskon_rp").val());
						tr.find("td:eq(20) input").val($("#xharga_plus_diskon").val());
						tr.find("td:eq(27) input").val($("#xkuantitas").val());
						tr.find("td:eq(28) input").val($("#xkuantitas_sisa").val());
						tr.find("td:eq(29) input").val($("#xharga_master").val());
						tr.find("td:eq(31) input").val($("#xkuantitas").val());
						tr.find("td:eq(21) input").val($("#xharga_total_fix").val());
						
					}
				});
				gen_total_uang();
		}
        
    });
    
	var table, tr;
	$(document).on("click",".edit",function(){
		 var tr = $(this).closest('tr');
		 // alert(tr.find('td:eq(0)').text());
		$('#xnamabarang').val(tr.find('td:eq(1)').text());
		$('#xharga').val(tr.find('td:eq(16) input').val());
		$('#xppn').val(tr.find('td:eq(17) input').val());
		$('#xharga_plus_ppn').val(tr.find('td:eq(18) input').val());
		$('#xdiskon_rp').val(tr.find('td:eq(19) input').val());
		$('#xharga_plus_diskon').val(tr.find('td:eq(20) input').val());
		$('#xkuantitas').val(tr.find('td:eq(27) input').val());
		$('#xkuantitas_sisa').val(tr.find('td:eq(28) input').val());
		$('#xharga_master').val(tr.find('td:eq(29) input').val());
		$('#tanggalkadaluarsa').val(tr.find('td:eq(30) input').val());
		$('#nobatch').val(tr.find('td:eq(22) input').val());
		$('#modal_edit').modal('show');
        $("#rowIndex").val(tr[0].sectionRowIndex);
		gen_harga();
		cek_harga();
	});
	$(document).on("click",".hapus",function(){
		$("#st_hapus_alih").val('1');
		$("#div_hapus").show();
		 var tr = $(this).closest('tr');
		 var content = "<tr>";
		 content += "<td class='text-center'>" + tr.find('td:eq(0)').text() + "</td>"; //0 Nomor
		 content += "<td  class='text-left'>" + tr.find('td:eq(1)').text() + "</td>"; //1 
		 content += "<td  class='text-center'>" + tr.find('td:eq(28) input').val()+' '+tr.find('td:eq(32) input').val()+ "</td>"; //2 
		 if (tr.find('td:eq(27) input').val() == tr.find('td:eq(28) input').val()){
			content += '<td  class="text-center"><span class="label label-danger">DIBATALKAN SEMUA</span></td>'; //2 
		 }else{
			content += '<td  class="text-center"><span class="label label-danger">DIBATALKAN SEBAGIAN</span></td>'; //2 
		 }
		 content += '<td ><select class="form-control e_dist" name="d_iddistributor[]" style="width:100%"><option value="#" selected>TIDAK PERLU DIISI</option></select></td>'; //3
		 content += '<td ><input type="text" class="form-control" name="d_alasan[]" value=""></td>'; //4
		 content += '<td hidden><input type="text" class="form-control" name="d_iddet[]" value="'+tr.find('td:eq(15) input').val()+'"></td>'; //5
		 
		 if (tr.find('td:eq(27) input').val() == tr.find('td:eq(28) input').val()){
			content += '<td hidden><input type="text" class="form-control" name="d_status[]" value="1"></td>'; //ALL
		 }else{
			 content += '<td hidden><input type="text" class="form-control" name="d_status[]" value="2"></td>'; //SEBAGIAN
		 }
		 content += '<td hidden><input type="text" class="form-control" name="d_kuantitas[]" value="'+ tr.find('td:eq(28) input').val()+'"></td>'; //5
		 content += "</tr>";
		 tr.remove();
		$('#data_hapus tbody').append(content);
		get_distributor();
		
	});
	$(document).on("click",".alih",function(){
		$("#st_hapus_alih").val('1');
		$("#div_hapus").show();
		 var tr = $(this).closest('tr');
		 var content = "<tr>";
		 content += "<td class='text-center'>" + tr.find('td:eq(0)').text() + "</td>"; //0 Nomor
		 content += "<td  class='text-left'>" + tr.find('td:eq(1)').text() + "</td>"; //1 
		 content += "<td  class='text-center'>" + tr.find('td:eq(28) input').val()+' '+tr.find('td:eq(32) input').val()+ "</td>"; //2 
		 content += '<td  class="text-center"><span class="label label-warning">DIALIHKAN</span></td>'; //2 
		 content += '<td ><select class="form-control e_dist" name="d_iddistributor[]" style="width:100%"><option value="#" selected>-Silahkan Pilih-</option></select></td>'; //3
		 content += '<td ><input type="text" class="form-control" name="d_alasan[]" value=""></td>'; //4
		 content += '<td hidden><input type="text" class="form-control" name="d_iddet[]" value="'+tr.find('td:eq(15) input').val()+'"></td>'; //5
		 content += '<td hidden><input type="text" class="form-control" name="d_status[]" value="3"></td>'; //5
		 content += '<td hidden><input type="text" class="form-control" name="d_kuantitas[]" value="'+ tr.find('td:eq(28) input').val()+'"></td>'; //5
		 content += "</tr>";
		
		tr.remove();
		$('#data_hapus tbody').append(content);
		get_distributor();
		
	});
	function get_distributor() {
        // get nopemesanan
       $(".e_dist").select2({
			minimumInputLength: 2,
			noResults: 'Distributor Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}tgudang_penerimaan/get_distributor/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,

			 data: function (params) {
				  var query = {
					search: params.term,
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.nama,
								id: item.id,
							}
						})
					};
				}
			}
		});
    }
	function gen_total_uang(){
			var tot=0;
			var tot_barang=0;
		$('#data-barang tbody tr').each(function() {
			var tr = $(this).closest('tr');	
			tot=tot+ parseFloat(tr.find("td:eq(21) input").val());
			tot_barang=tot_barang+ parseFloat(tr.find("td:eq(31) input").val());
			 // console.log(tr.find("td:eq(21) input").val());
			
		});
		$("#totalharga").val(tot);
		$("#totalbarang").val(tot_barang);
	}
	function formatNumber (num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}
    
	

    function grandTotal() {
        var totalbarang = 0;
        var totalharga = 0;
        $('#data-barang tbody tr').each(function() {
            totalbarang += parseFloat($(this).find('td:eq(6)').text().replace(/\,/g, ""));
            totalharga += parseFloat($(this).find('td:eq(7)').text().replace(/\,/g, ""));
        });

        $("#totalbarang").val(isNaN(totalbarang) ? 0 : totalbarang)
        $("#totalharga").val(isNaN(totalharga) ? 0 : totalharga)
    }

    function validateHead() {
        if ($('#nopemesanan').val() == '') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'No Pemesanan tidak boleh kosong'
            });
            return false;
        }
        if ($('#tanggalpenerimaan').val() == '') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Tanggal Penerimaan tidak boleh kosong'
            });
            return false;
        }
        if ($('#tanggaljatuhtempo').val() == '') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Tanggal Jatuh Tempo tidak boleh kosong'
            });
            return false;
        }
        if ($('#totalharga').val() == '' || $('#totalharga').val() <= 0) {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Data belum lengkap. Total terima barang masih 0'
            });
            return false;
        }
        return true;
    }

    function validate() {
        if ($('#xharga_total_fix').val() == 0) {
            sweetAlert("Maaf...", "Harga Dan kuantitas harus diisi!", "error");
            return false;
        }
        
        if ($('#tanggalkadaluarsa').val() == '') {
            sweetAlert("Maaf...", "Tanggal Kadaluarsa harus diisi!", "error");
            return false;
        }
        if ($('#nobatch').val() == '' || $('#nobatch').val() == '-') {
            sweetAlert("Maaf...", "No Batch Harus diisi!", "error");
            return false;
        }
        return true;
    }

    
	$("#xharga").keyup(function(){
		if ($("#xharga").val()==''){
			$("#xharga").val(0)
		}
		var ppn=$("#xppn").val();
		var harppn=parseFloat($(this).val()) + (parseFloat($(this).val())*ppn/100);
		$("#xharga_plus_ppn").val(harppn);
		cek_harga();
	});
	$("#xdiskon_rp").keyup(function(){
		if ($("#xdiskon_rp").val()==''){
			$("#xdiskon_rp").val(0)
		}
		
		if (parseFloat($(this).val()) > $("#xharga_plus_ppn").val()){
			$(this).val($("#xharga_plus_ppn").val());
		}
		var diskon_persen=parseFloat((parseFloat($(this).val()*100))/$("#xharga_plus_ppn").val());
		$("#xdiskon_persen").val(diskon_persen);
		gen_harga();
	});
	$("#xdiskon_persen").keyup(function(){
		if ($("#xdiskon_persen").val()==''){
			$("#xdiskon_persen").val(0)
		}
		if (parseFloat($(this).val()) > 100){
			$(this).val(100);
		}
		var diskon_rp=parseFloat($("#xharga_plus_ppn").val() * parseFloat($(this).val())/100);
		$("#xdiskon_rp").val(diskon_rp);
		gen_harga();
	});
	$("#xkuantitas").keyup(function(){
		if (parseFloat($("#xkuantitas").val()) > parseFloat($("#xkuantitas_sisa").val())){
			sweetAlert("Maaf...", "Kuantitas Lebih Besar Dari Sisa", "error");
			$("#xkuantitas").val($("#xkuantitas_sisa").val());			
		}		
		gen_harga();
	});
	function gen_harga(){
		var harga_final=parseFloat($("#xharga_plus_ppn").val()) - parseFloat($("#xdiskon_rp").val());
		var xharga_total_fix=parseFloat($("#xkuantitas").val()) * harga_final;
		$("#xharga_plus_diskon").val(harga_final);
		$("#xharga_total_fix").val(xharga_total_fix);
	}
	$("#xppn").keyup(function(){
		if ($("#xppn").val()>100){
			$("#xppn").val(100)
		}
		if ($("#xppn").val()==''){
			$("#xppn").val(0)
		}
		var ppn=$("#xppn").val();
		var harppn=parseFloat($("#xharga").val()) + (parseFloat($("#xharga").val())*ppn/100);
		$("#xharga_plus_ppn").val(harppn);
		cek_harga();
	});
	function cek_harga(){
		var selisih;
		var harppn=$("#xharga_plus_ppn").val();
		$('#div_harga').removeClass('has-error');			
		$('#div_harga').removeClass('has-success');	
		if (parseFloat(harppn) != $("#xharga_master").val()){
			if (parseFloat(harppn) > $("#xharga_master").val()){
				selisih =  parseFloat(harppn) - parseFloat($("#xharga_master").val());
						
				$('#div_harga').addClass('has-error');
				$('#val_harga').text('Harga Lebih Mahal Rp. '+ formatNumber(selisih));
			}else{
				selisih =  parseFloat($("#xharga_master").val()) - parseFloat(harppn) ;
				
				$('#div_harga').addClass('has-success');
				$('#val_harga').text('Harga Lebih Murah Rp. '+formatNumber(selisih));
			}
			
		}else{
			$('#val_harga').text('');
		}	
		gen_harga();
	}
    function validate_header() {
		// alert('Validasi');
		var rowCount = $('#data-barang tr').length;
		var rowCountHapus = $('#data_hapus tr').length;
		// alert(rowCount);
        if (parseFloat($('#totalbarang').val()) == 0 && rowCount > 1) {
            sweetAlert("Maaf...", "Barang harus diverifikasi!", "error");
            return false;
        }
        
        if ($('#nofakturexternal').val() == '') {
            sweetAlert("Maaf...", "No Faktur harus diisi!", "error");
            return false;
        }
		if ($('#tanggalpenerimaan').val() == '') {
            sweetAlert("Maaf...", "Tanggal Harus di isi!", "error");
            return false;
        }
        if ($('#tanggaljatuhtempo').val() == '' && $("#pembayaran").val()=='2') {
            sweetAlert("Maaf...", "Tanggal Jatuh Tempo Harus di isi!", "error");
            return false;
        }
		var ada_kosong_dist='1';
		var ada_kosong_dist_alasan='1';
		var ada_kosong_hapus='1';
		if (rowCountHapus>1){
			$('#data_hapus tbody tr').each(function() {
				var tr = $(this).closest('tr');	
				 // alert(tr.find("td:eq(13) input").val());
				 console.log('DIST'+tr.find("td:eq(4) select").val());
				if (tr.find("td:eq(7) input").val()=='3' && tr.find("td:eq(4) select").val()=='#'){
					ada_kosong_dist='0';
				}
				if (tr.find("td:eq(7) input").val()=='3' && tr.find("td:eq(5) input").val()==''){
					ada_kosong_dist_alasan='0';
				}
				if (tr.find("td:eq(7) input").val()=='1' && tr.find("td:eq(5) input").val()==''){
					ada_kosong_hapus='0';					
				}
				if (tr.find("td:eq(7) input").val()=='2' && tr.find("td:eq(5) input").val()==''){
					ada_kosong_hapus='0';					
				}
				console.log(tr.find("td:eq(7) input").val()+'-'+tr.find("td:eq(5) input").val());
				
			});
			console.log(ada_kosong_dist+';'+ada_kosong_dist_alasan+';'+ada_kosong_hapus);
			if (ada_kosong_dist=='0'){
				sweetAlert("Maaf...", "Harus diisi Distributor!", "error");
				return false;
			}
			if (ada_kosong_dist_alasan=='0'){
				sweetAlert("Maaf...", "Harus diisi Alasan Ganti Distributor!", "error");
				return false;
			}
			if (ada_kosong_hapus=='0'){
				sweetAlert("Maaf...", "Harus diisi Alasan Hapus!", "error");
				return false;
			}
		}
        return true;
    }
	$("#simpan-penerimaan").click(function() {
		// alert('Simpan');
		 if (validate_header()){
			$("#form-work").submit();
		}
		
		return false;
	});
	$(document).on("click","#btn_back",function(){
		window.history.back()
	});
</script>