<?= ErrorSuccess($this->session) ?>
<?php if ($error != '') echo ErrorMessage($error) ?>
<div id="cover-spin"></div>
<style media="screen">
	.highlight-refund {
		background-color: #8bc34a2b !important;
	}
</style>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tgudang_penerimaan" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-bottom: 0px;">
	</div>
	<div class="block-content">
		<?php echo form_open('tgudang_penerimaan/save_pembayaran','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="col-md-2 control-label" for="catatan">No Transaksi</label>
						<div class="col-md-4">
							<input type="text" disabled class="form-control" id="nopenerimaan" placeholder="nopenerimaan" name="nopenerimaan" value="{nopenerimaan}" required="" aria-required="true">
						</div>
						<label class="col-md-2 control-label" for="idtipe">Tanggal Transaksi</label>
						<div class="col-md-4">
							<input class="form-control" type="hidden" name="id" id="id" type="text"  value="<?=($id)?>">
							<input disabled class="js-datepicker form-control" type="text" id="tanggalpenerimaan" name="tanggalpenerimaan" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" value="<?=HumanDateShort($tanggalpenerimaan);?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label" for="idtipe">Distributor</label>
						<div class="col-md-4">
							<input class="form-control" disabled name="distributor" id="distributor" type="text" readonly value="<?=($distributor)?>">
						</div>
						<label class="col-md-2 control-label" for="idpegawai">Tipe Bayar</label>
						<div class="col-md-4">
							<input class="form-control" disabled name="tipe_pemilik_nama" id="tipe_pemilik_nama" type="text" readonly value="<?=($tipe_bayar=='1'?'TUNAI':'KREDIT')?>">
							
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-2 control-label" for="nominal">Total Transaksi</label>
						<div class="col-md-4">
							<input type="text" disabled class="form-control number" id="nominal_all" placeholder="Nominal" name="nominal_all" value="{totalharga}" required="" aria-required="true">
						</div>
						<label class="col-md-2 control-label" for="nominal">No Faktur</label>
						<div class="col-md-4">
							<input type="text" disabled class="form-control" id="nofakturexternal" placeholder="Nominal" name="nofakturexternal" value="{nofakturexternal}" required="" aria-required="true">
						</div>
						
					</div>
					
					<div class="form-group" style="margin-bottom: 5px;">
						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped table-borderless table-header-bg" id="data-barang">
					<thead>
						<tr>
							<th width="5%" class="text-center">Tipe</th>
							<th width="20%" class="text-center">Barang</th>
							<th width="8%" class="text-center">Jumlah Terima</th>
							<th width="8%" class="text-center">Harga + PPN</th>
							<th width="8%" class="text-center">Diskon</th>
							<th width="10%" class="text-center">Total</th>
							<th width="10%" class="text-center">Keterangan</th>
							<th width="5%" class="text-center" >Status Bayar</th>
							<th width="10%" class="text-center" hidden>Aksi</th>
							
						</tr>
					</thead>
					<tbody>
						<?
						$total_all=0;
						$total_bayar=0;
						foreach($list_barang as $row){
							$total_all=$total_all + $row->totalharga;
							if ($row->st_acces=='1'){
								$total_bayar=$total_bayar + $row->totalharga;								
							}
							?>
							<tr>
								<td class="text-center"><?=$row->namatipe?></td>
								<td class="text-left"><?=$row->nama_barang?></td>
								<td class="text-center"><?=number_format($row->kuantitas,0).' '.$row->namasatuan?></td>
								<td class="text-right"><?=number_format($row->harga,2)?></td>
								<td class="text-right">(Rp) <?=number_format($row->nominaldiskon,2)?><br><?=number_format($row->diskon,0)?> (%)</td>
								<td class="text-right"><?=number_format($row->totalharga,2)?></td>
								<td class="text-center"><?=($row->st_acces=='1'?text_success('Akses User Anda'):text_danger('Akses User Lain'))?></span></td><!-- 7 -->								
								<td class="text-center" ><?=($row->head_bayar_id!=''?'<i class="si si-check fa-2x text-success"></i>':'<i class="si si-close fa-2x text-danger"></i>')?></td><!-- 8 -->
								<td hidden>
									<input type="hidden" class="form-control" name="e_iddet[]" value="<?=($row->st_acces=='1'?$row->id:'0')?>">
								</td>
							</tr>
						
						<?					
						}?>
					</tbody>
					<tfoot>
						<tr style="background-color: #f0f0f0;">
						  <td colspan='5' class="text-center"><b>TOTAL</b></td>
						  <td class="text-right"><b><?=number_format($total_all,2)?></b></td>
						  <td></td>
						  <td class="text-right"  hidden><b><?=number_format($total_bayar,2)?></b></td>
						  <td ></td>
						</tr>
				  </tfoot>
				</table>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label class="col-md-2 control-label" for="nominal">Total Transaksi</label>
						<div class="col-md-2">
							<input type="text" disabled class="form-control number" id="nominal" placeholder="Nominal" name="nominal" value="<?=$totalharga?>" required="" aria-required="true">
						</div>
						<div class="col-md-2">
							<a href="{base_url}tgudang_penerimaan/detail_bayar/<?=$id?>" <?=$disabel?> <?=($error!=''?'disabled':'')?> class="btn btn-danger btn_hide" type="button" id="btn_add_pembayaran" title="Add"><i class="fa fa-plus"></i> Pembayaran</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="block-content block-content">						
					<input type="hidden" class="form-control" id="rowindex">
					<input type="hidden" class="form-control" id="iddet">
					<div class="form-group">
						 <table class="table table-bordered" id="list_pembayaran">
							<thead>
								<tr>
									<th width="5%" class="text-center">NO</th>
									<th width="10%" class="text-center">Tanggal</th>
									<th width="15%" class="text-center">User</th>
									<th width="15%" class="text-center">Sumber Kas</th>
									<th width="15%" class="text-center">Nominal</th>
									<th width="15%" class="text-center">Actions</th>
									
								</tr>
							</thead>
							</thead>
							<tbody>	
								<? 
									$no=1;
									$tot=0;
									if ($list_pembayaran){
										foreach($list_pembayaran as $row){ 
										$tot=$tot+$row->total_bayar;
										?>
											<tr>
											<td><?=$no?></td>
											<td class="text-center"><?=HumanDateLong($row->tanggal_trx)?></td>
											<td class="text-center"><?=$row->nama_user?></td>
											<td><?=$row->kas?></td>
											<td align="right"><?=number_format($row->total_bayar,2)?></td>
											<td align="center"><a type='button' href="{base_url}tgudang_penerimaan/detail_bayar/<?=$row->penerimaan_id.'/'.$row->id?>"  <?=$disabel?>  class='btn btn-info btn-xs'  title='Edit'><i class='fa fa-pencil'></i></a>&nbsp;&nbsp;<button type='button'  <?=$disabel?>  class='btn btn-danger btn-xs pembayaran_remove' title='Remove' onclick="hapus_bayar_head(<?=$row->id?>)"><i class='fa fa-trash-o'></i></button></td>
											
											</tr>
										<?
										$no=$no+1;
										}
									}
								?>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="4" class="text-right"> <strong>Total Bayar <?=$total_bayar?></strong></td>
									<td> <input class="form-control input number" readonly type="text" id="total_bayar" name="total_bayar" value="<?=$tot?>"/></td>
									<td></td>
								</tr>
								<tr>
									<td colspan="4" class="text-right"> <strong>Sisa</strong></td>
									<td><input class="form-control input number" readonly type="text" id="total_sisa" name="total_sisa" value="0"/> </td>
									<td></td>
								</tr>
							</tfoot>
						</table>
					</div>
					
				</div> 
			</div>

		<hr>
		
		</form>
	</div>
</div>

<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/number_format.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
	//PEMBAYARAN
	$(document).ready(function(){
		$('.number').number(true, 2, '.', ',');
		
		checkTotal();
	})
	
	function formatNumber (num) {
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}
	function checkTotal(){
		var total_pembayaran = $("#total_bayar").val();
		
		$("#total_sisa").val(parseFloat($("#nominal").val()) - parseFloat(total_pembayaran));
	}
	function hapus_bayar_head($id){


		var id=$id;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Pembayaran ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tgudang_penerimaan/hapus_bayar_head/'+id,
				complete: function() {
					$("#btn_simpan").hide();
					
					sweetAlert("Success...", "Penghapusan Berhasil!", "success");
					location.reload();
				}
			});
		});

		
		
		// alert(id+' '+tgl_baru);
		
	}
</script>
