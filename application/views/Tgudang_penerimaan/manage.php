<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <?php echo form_open('tgudang_penerimaan/save', 'class="form-horizontal push-10-t"') ?>
    <input type="hidden" name="id" value="{id}" id="id" />
    <div class="block-content block-content-narrow">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-3 control-label">No Pemesanan</label>
                    <div class="col-md-9">
                        <input class="form-control" type="text" value="{nopemesanan}" disabled />
                        <input type="hidden" name="nopemesanan" value="{idpemesanan}" id="nopemesanan" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">No Faktur</label>
                    <div class="col-md-9">
                        <input class="form-control" type="text" id="nofakturexternal" value="{nofakturexternal}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Tanggal Penerimaan</label>
                    <div class="col-md-9">
                        <input class="js-datepicker form-control" type="text" id="tanggalpenerimaan" data-date-format="yyyy-mm-dd" value="<?= date("Y-m-d", strtotime($tanggalpenerimaan)) ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Tanggal Jatuh Tempo</label>
                    <div class="col-md-9">
                        <input class="js-datepicker form-control" type="text" id="tanggaljatuhtempo" data-date-format="yyyy-mm-dd" value="{tanggaljatuhtempo}">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-3 control-label">Total Barang</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="totalbarang" readonly value="{totalbarang}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Total Harga</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="totalharga" readonly value="{totalharga}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Keterangan</label>
                    <div class="col-md-9">
                        <textarea class="form-control" id="keterangan"></textarea>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <hr />
            <table class="table table-striped table-bordered" id="data-barang">
                <thead>
                    <tr>
                        <th>Tipe</th>
                        <th>Nama</th>
                        <th>No Batch</th>
                        <th>Harga</th>
                        <th>PPN</th>
                        <th>Diskon</th>
                        <th>Jml Terima</th>
                        <th>Total Harga</th>
                        <th>Jml Sisa</th>
                        <th width="5%">Aksi</th>
                        <th hidden>idtipe</th>
                        <th hidden>idbarang</th>
                        <th hidden>iddistributor</th>
                        <th hidden>xsisa</th>
                        <th hidden>kadaluarsa</th>
                        <th hidden>totaldiskon</th>
                        <th hidden>totalppn</th>
                        <th hidden>namasatuan</th>
                        <th hidden>opsisatuan</th>
                        <th hidden>totalsebelumdiskon</th>
                        <th hidden>idpemesanandetail</th>
                        <th hidden>idpenerimaandetail</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($detail as $d) : ?>
                        <tr>
                            <td><?= GetTipeKategoriBarang($d->idtipe) ?></td>
                            <td><?= $this->model->getNamaBarang($d->idtipe, $d->idbarang); ?></td>
                            <td><?= $d->nobatch ?></td>
                            <td><?= $d->harga ?></td>
                            <td><?= $d->ppn ?></td>
                            <td><?= $d->diskon ?></td>
                            <td><?= $d->kuantitas ?></td>
                            <td><?= $d->totalharga ?></td>
                            <td><?= $d->kuantitaspemesanan - $d->kuantitas ?></td>
                            <td width="5%">
								<a href='#edit' class='edit-detail'>
								<i class='fa fa-pencil'></i></a><
							/td>
                            <td hidden><?= $d->idtipe ?></td>
                            <td hidden><?= $d->idbarang ?></td>
                            <td hidden><?= $iddistributor ?></td>
                            <td hidden><?= $d->kuantitaspemesanan ?></td>
                            <td hidden><?= $d->tanggalkadaluarsa ?></td>
                            <td hidden><?= $d->nominaldiskon * $d->kuantitas ?></td>
                            <td hidden><?= $d->nominalppn * $d->kuantitas ?></td>
                            <td hidden><?= $d->namasatuan ?></td>
                            <td hidden><?= $d->opsisatuan ?></td>
                            <td hidden><?= $d->totalsebelumdiskon ?></td>
                            <td hidden><?= $d->idpemesanandetail ?></td>
                            <td hidden><?= $d->id ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div style="margin-top:50px;margin-bottom:20px;" class="text-right">
            <a href="{site_url}tgudang_penerimaan" class="btn btn-default btn-md">Kembali</a>
            <button class="btn btn-sm btn-success" type="button" id="simpan-penerimaan">Sunting</button>
        </div>
    </div>
    <?php echo form_close() ?>
</div>

<div class="modal" id="penerimaan-item" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title" id='title_barang'></h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal">
                        <input type="hidden" id="rowIndex">
                        <input type="hidden" name="idpenerimaan" id="idpenerimaan">
                        <input type="hidden" name="idpemesanandetail" id="idpemesanandetail">
                        <input type="hidden" name="idpenerimaandetail" id="idpenerimaandetail">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Harga Beli</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control detail" id="hargabeli">
                                        <input type="hidden" class="form-control detail" id="hargabelivalidasi">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Diskon</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control detail" id="diskon">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Nominal Diskon</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control detail" id="nominaldiskon" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">PPN</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control detail" id="ppn">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Nominal PPN</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control detail" id="nominalppn" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Kuantitas Terima</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control detail" id="kuantitas">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Sisa</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="xsisa" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tgl Kadaluarsa</label>
                                    <div class="col-md-9">
                                        <input class="js-datepicker form-control detail" type="text" id="tanggalkadaluarsa" data-date-format="yyyy-mm-dd">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">No Batch</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control detail" id="nobatch">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Total Sebelum Diskon</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control detail" id="totalsebelumdiskon" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Total Diskon</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control detail" id="totaldiskon" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Total Setelah Diskon</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control detail" id="totalhargasetelahdiskon" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Total Sebelum PPN</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control detail" id="totalsebelumppn" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Total PPN</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control detail" id="totalppn" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Total Setelah PPN</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control detail" id="totalhargasetelahppn" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Total Harga</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control detail" id="totalhargadet" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">&nbsp;</label>
                                    <div class="col-md-9">
                                        <button class="btn btn-sm btn-warning btn-block" type="button" id="simpan-detail">
                                            Simpan
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
    var table, t, tanggal;

    $(document).ready(function() {
        $('#simpan-detail, .detail').attr('disabled', true);
        $('#simpan-penerimaan').click(simpanPenerimaanClick);
        $(".edit-detail").click(editDetailClick);
        $('#simpan-detail').click(simpanDetailClick);
    });

    function simpanDetailClick() {
        if (validate()) {
            setTimeout(function() {
                table = $('#data-barang').DataTable({
                    searching: false,
                    paging: false,
                    sort: false,
                    info: false,
                    destroy: true,
                    autoWidth: false,
                    columnDefs: [{
                        targets: [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],
                        visible: false
                    }, ]
                })

                var rowIndex = $('#rowIndex').val();
                table.cell(rowIndex, 2).data($('#nobatch').val());
                table.cell(rowIndex, 3).data($('#hargabeli').val());
                table.cell(rowIndex, 4).data($('#ppn').val());
                table.cell(rowIndex, 5).data($('#diskon').val());
                table.cell(rowIndex, 6).data($('#kuantitas').val());
                table.cell(rowIndex, 7).data($('#totalhargadet').val());
                table.cell(rowIndex, 8).data($('#xsisa').val());
                table.cell(rowIndex, 14).data($('#tanggalkadaluarsa').val());
                table.cell(rowIndex, 15).data($('#totaldiskon').val());
                table.cell(rowIndex, 16).data($('#totalppn').val());
                table.cell(rowIndex, 19).data($('#totalsebelumdiskon').val());
                table.cell(rowIndex, 20).data($('#idpemesanandetail').val());
                table.cell(rowIndex, 21).data($('#idpenerimaandetail').val());
                $.toaster({
                    priority: 'success',
                    title: 'Sukses!',
                    message: 'Data telah ditambahkan'
                });

                setTimeout(function() {
                    grandTotal()
                    $('.detail, #totalhargadet, #xsisa').val('')
                    $('#simpan-detail, .detail').attr('disabled', true)
                }, 100);

                $("#penerimaan-item").modal("hide");

            }, 500)
        }
    }

    function editDetailClick() {
        $('#ppn,#diskon').keyup(discountKeyup);
        $('#hargabeli,#ppn,#diskon,#kuantitas').keyup(itemKeyup);
        $('#hargabeli, #hargabelivalidasi').blur(hargaBlur);

        $("#penerimaan-item").modal("show");
        $('#simpan-detail, .detail').attr('disabled', false)


        table = $('#data-barang').DataTable({
            searching: false,
            paging: false,
            sort: false,
            info: false,
            destroy: true,
            autoWidth: false,
            columnDefs: [{
                targets: [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],
                visible: false
            }, ]
        })

        var rowIndex = table.row($(this).parents('tr')).index()
        $("#idpemesanandetail").val(table.cell(rowIndex, 20).data());
        $("#idpenerimaandetail").val(table.cell(rowIndex, 21).data());
        $("#title_barang").text(table.cell(rowIndex, 1).data());
        $('#rowIndex').val(rowIndex)
        $('#nobatch').val(table.cell(rowIndex, 2).data())
        $('#hargabeli').val(table.cell(rowIndex, 3).data())
        $('#hargabelivalidasi').val(table.cell(rowIndex, 3).data())
        $('#ppn').val(table.cell(rowIndex, 4).data())
        $('#diskon').val(table.cell(rowIndex, 5).data())
        $('#kuantitas').val(table.cell(rowIndex, 6).data())
        $('#xsisa').val(table.cell(rowIndex, 13).data() - table.cell(rowIndex, 6).data())
        $('#tanggalkadaluarsa').val(table.cell(rowIndex, 14).data())

        var hargabeli = parseFloat($('#hargabeli').val());
        var ppn = parseFloat($('#ppn').val());
        var diskon = parseFloat($('#diskon').val());
        var kuantitas = parseFloat($('#kuantitas').val());
        var xsisa = parseFloat(table.cell($('#rowIndex').val(), 13).data());

        var nominaldiskon = diskon / 100 * hargabeli;
        var hargaincdiskon = hargabeli - nominaldiskon;
        var nominalppn = ppn / 100 * hargaincdiskon;
        var hargaincdiskondanppn = hargabeli - nominaldiskon + nominalppn;

        $('#nominalppn').val(nominalppn)
        $('#nominaldiskon').val(nominaldiskon)

        var totalsebelumdiskon = hargabeli * kuantitas;
        $('#totalsebelumdiskon').val(totalsebelumdiskon)
        var totaldiskon = nominaldiskon * kuantitas;
        $('#totaldiskon').val(totaldiskon);
        $('#totalhargasetelahdiskon').val(totalsebelumdiskon - totaldiskon);

        var totalsebelumppn = totalsebelumdiskon - (nominaldiskon * kuantitas);
        $('#totalsebelumppn').val(totalsebelumppn);
        var totalppn = nominalppn * kuantitas;
        $('#totalppn').val(totalppn);
        $('#totalhargasetelahppn, #totalhargadet').val(totalsebelumppn + totalppn);
    }

    function grandTotal() {
        var totalbarang = 0;
        var totalharga = 0;
        $('#data-barang tbody tr').each(function() {
            totalbarang += parseFloat($(this).find('td:eq(6)').text().replace(/\,/g, ""));
            totalharga += parseFloat($(this).find('td:eq(7)').text().replace(/\,/g, ""));
        });

        $("#totalbarang").val(isNaN(totalbarang) ? 0 : totalbarang)
        $("#totalharga").val(isNaN(totalharga) ? 0 : totalharga)
    }

    function validateHead() {
        if ($('#nopemesanan').val() == '') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'No Pemesanan tidak boleh kosong'
            });
            return false;
        }
        if ($('#tanggalpenerimaan').val() == '') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Tanggal Penerimaan tidak boleh kosong'
            });
            return false;
        }
        if ($('#tanggaljatuhtempo').val() == '') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Tanggal Jatuh Tempo tidak boleh kosong'
            });
            return false;
        }
        if ($('#totalharga').val() == '' || $('#totalharga').val() <= 0) {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Data belum lengkap. Total terima barang masih 0'
            });
            return false;
        }
        return true;
    }

    function validate() {
        if ($('#hargabeli').val() == '' || $('#hargabeli').val() == 0) {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Harga beli tidak boleh kosong'
            });
            return false;
        }
        if ($('#ppn').val() == '') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'PPN tidak boleh kosong'
            });
            return false;
        }
        if ($('#diskon').val() == '') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Diskon tidak boleh kosong'
            });
            return false;
        }
        if ($('#kuantitas').val() == '' || $('#kuantitas').val() == 0) {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Kuantitas tidak boleh kosong'
            });
            return false;
        }
        if (parseFloat($('#xsisa').val()) < 0) {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Kuantitas tidak boleh lebih besar dari jumlah sisa'
            });
            return false;
        }
        if ($('#tanggalkadaluarsa').val() == '') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Tanggal Kadaluarsa tidak boleh kosong'
            });
            return false;
        }
        if ($('#nobatch').val() == '' || $('#nobatch').val() == '-') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'No Batch tidak boleh kosong'
            });
            return false;
        }
        return true;
    }

    function discountKeyup() {
        if (parseFloat($('#ppn').val()) > 100) {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'PPN max 100'
            });
            $('#ppn').val(100)
            return false;
        }
        if (parseFloat($('#diskon').val()) > 100) {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Diskon max 100'
            });
            $('#diskon').val(100)
            return false;
        }
    }

    function hargaBlur() {
        var hargabeli = parseFloat($('#hargabeli').val())
        var hargabelivalidasi = parseFloat($('#hargabelivalidasi').val())
        if (hargabeli > hargabelivalidasi) {
            swal({
                text: 'Harga lebih besar dari sebelumnya?',
                showCancelButton: true,
                confirmButtonColor: '#d26a5c',
                confirmButtonText: 'Lanjutkan!',
                html: false,
                preConfirm: function() {
                    return new Promise(function(resolve) {
                        setTimeout(function() {
                            resolve()
                        }, 50);
                    })
                }
            }).then(function() {
                return true;
            }).catch(function(reason) {
                $('#hargabeli').val(hargabelivalidasi);
            })
        }
    }

    function itemKeyup() {
        table = $('#data-barang').DataTable({
            searching: false,
            paging: false,
            sort: false,
            info: false,
            destroy: true,
            autoWidth: false,
            columnDefs: [{
                targets: [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],
                visible: false
            }, ]
        })

        var hargabeli = parseFloat($('#hargabeli').val());
        var ppn = parseFloat($('#ppn').val());
        var diskon = parseFloat($('#diskon').val());
        var kuantitas = parseFloat($('#kuantitas').val());
        var xsisa = parseFloat(table.cell($('#rowIndex').val(), 13).data());

        var nominaldiskon = diskon / 100 * hargabeli;
        var hargaincdiskon = hargabeli - nominaldiskon;
        var nominalppn = ppn / 100 * hargaincdiskon;
        var hargaincdiskondanppn = hargabeli - nominaldiskon + nominalppn;

        $('#nominalppn').val(nominalppn)
        $('#nominaldiskon').val(nominaldiskon)

        var totalsebelumdiskon = hargabeli * kuantitas;
        $('#totalsebelumdiskon').val(totalsebelumdiskon)
        var totaldiskon = nominaldiskon * kuantitas;
        $('#totaldiskon').val(totaldiskon);
        $('#totalhargasetelahdiskon').val(totalsebelumdiskon - totaldiskon);

        var totalsebelumppn = totalsebelumdiskon - (nominaldiskon * kuantitas);
        $('#totalsebelumppn').val(totalsebelumppn);
        var totalppn = nominalppn * kuantitas;
        $('#totalppn').val(totalppn);
        $('#totalhargasetelahppn, #totalhargadet').val(totalsebelumppn + totalppn);
        $('#xsisa').val(xsisa - kuantitas)

    }

    function simpanPenerimaanClick() {
        table = $('#data-barang').DataTable({
            searching: false,
            paging: false,
            sort: false,
            info: false,
            destroy: true,
            autoWidth: false,
            columnDefs: [{
                targets: [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],
                visible: false
            }, ]
        })

        if (table.rows().count() !== 0) {

            if (validateHead()) {
                var arr = table.rows().data().toArray();
                $.ajax({
                    url: '{site_url}tgudang_penerimaan/update',
                    data: {
                        detailValue: arr,
                        id: $('#id').val(),
                        nopemesanan: $('#nopemesanan').val(),
                        nofakturexternal: $('#nofakturexternal').val(),
                        keterangan: $('#keterangan').val(),
                        tanggalpenerimaan: $('#tanggalpenerimaan').val(),
                        tanggaljatuhtempo: $('#tanggaljatuhtempo').val(),
                        totalbarang: $('#totalbarang').val(),
                        totalharga: $('#totalharga').val(),
                    },
                    dataType: 'text',
                    type: 'POST',
                    beforeSend: function() {
                        // $('#simpan-penerimaan').html('<i class="fa fa-sun-o fa-spin"></i> Simpan').attr('disabled', true);
                    },
                    success: function(cb) {
                        window.location = "{site_url}tgudang_penerimaan";
                    }
                });
            }
        } else {
            swal('Kesalahan', 'Data masih kosong!', 'warning')
        }
    }
</script>