<div class="block">
	<div class="block-header">
		<ul class="block-options">
            <li><a href="{site_url}tgudang_penerimaan" class="btn"><i class="fa fa-reply"></i></a></li>
        </ul>
		<h3 class="block-title">{title}</h3>
	</div>	
	<div class="block-content">
		<form class="form-horizontal">
			<div class="form-group c" id="f_namabarang">
				<label class="col-md-3 control-label">No Penerimaan</label>
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input disabled type="text" class="form-control" aria-required="true" value="{nopenerimaan}">
					</div>
				</div>
			</div>
			<div class="form-group c" id="f_namabarang">
				<label class="col-md-3 control-label">Distributor</label>
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input disabled type="text" class="form-control" aria-required="true" value="{distributor}">
					</div>
				</div>
			</div>
			<div class="form-group c" id="f_namabarang">
				<label class="col-md-3 control-label">No Faktur External</label>
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input disabled type="text" class="form-control" aria-required="true" value="{nofakturexternal}">
					</div>
				</div>
			</div>
			<div class="form-group c" id="f_namabarang">
				<label class="col-md-3 control-label">Total Barang</label>
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input disabled type="text" class="form-control" aria-required="true" value="{totalbarang}">
					</div>
				</div>
			</div>
			<div class="form-group c" id="f_namabarang">
				<label class="col-md-3 control-label">Total Harga</label>
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input disabled type="text" class="form-control" aria-required="true" value="{totalharga}">
					</div>
				</div>
			</div>
			<div class="form-group c" id="f_namabarang">
				<label class="col-md-3 control-label">Diskon</label>
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input disabled type="text" class="form-control" aria-required="true" value="{diskon}">
					</div>
				</div>
			</div>
			<div class="form-group c" id="f_namabarang">
				<label class="col-md-3 control-label">PPN</label>
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input disabled type="text" class="form-control" aria-required="true" value="{ppn}">
					</div>
				</div>
			</div>
			<div class="form-group c" id="f_namabarang">
				<label class="col-md-3 control-label">Total Akhir</label>
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input disabled type="text" class="form-control" aria-required="true" value="{totalakhir}">
					</div>
				</div>
			</div>
		</form>
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
				<tr>
					<th>Nama Barang</th>
					<th>No Batch</th>
					<th>Kuantitas</th>
					<th>Harga</th>
                    <th>T. Harga</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($this->tgudang_penerimaan->viewDetail($id) as $r): ?>
					<tr>
						<td><?= $this->tgudang_penerimaan->getNamaBarang($r->idtipe,$r->idbarang) ?></td>
						<td><?= $r->nobatch ?></td>
						<td><?= $r->kuantitas ?></td>
						<td><?= $r->harga ?></td>
						<td><?= $r->harga*$r->kuantitas ?></td>
					</tr>
				<?php endforeach  ?>
			</tbody>
		</table>
	</div>
	</div>
	<!-- end content -->

</div>