<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>{title}</title>

        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{asset_path}favicons/favicon.png">

        <link rel="icon" type="image/png" href="{asset_path}favicons/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="{asset_path}favicons/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="{asset_path}favicons/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="{asset_path}favicons/favicon-160x160.png" sizes="160x160">
        <link rel="icon" type="image/png" href="{asset_path}favicons/favicon-192x192.png" sizes="192x192">

        <link rel="apple-touch-icon" sizes="57x57" href="{asset_path}favicons/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="{asset_path}favicons/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="{asset_path}favicons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="{asset_path}favicons/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="{asset_path}favicons/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="{asset_path}favicons/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="{asset_path}favicons/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="{asset_path}favicons/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="{asset_path}favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Web fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

        <!-- Bootstrap and OneUI CSS framework -->
        <link rel="stylesheet" href="{css_path}bootstrap.min.css">
        <link rel="stylesheet" id="css-main" href="{css_path}oneui.css">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="{css_path}themes/flat.min.css"> -->
        <!-- END Stylesheets -->
    </head>
    <body>
        <!-- Page Content -->
        <div class="content content-boxed">
            <!-- Invoice -->
            <div class="block block-opt-fullscreen">
                <div class="block-header">
                    <ul class="block-options">
                        <li>
                            <button type="button" onclick="App.initHelper('print-page');">
                                <i class="si si-printer"></i> Print Invoice
                            </button>
                        </li>
                    </ul>
                    <h3 class="block-title">#{nopenerimaan}</h3>
                </div>
                <div class="block-content block-content-narrow">
                    <div class="h1 text-left push-30-t push-30">
                        <div class="row">
                            <div class="col-xs-2 col-sm-2 col-lg-2">
                                <img src="{assets_path}upload/logo/logoreport.jpg">
                            </div>
                            <div class="col-xs-10 col-sm-10 col-lg-10 push-10-t">
                                <div class="h4 text-left">RSKB HALMAHERA SIAGA</div>
                                <div class="h5 text-left">JL. LLRE. MARTADINATA NO 28</div>
                                <div class="h5 text-left">+6222-4206061</div>
                                <div class="h5 text-left">BANDUNG</div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row items-push-2x">
                        <!-- Company Info -->
                        <div class="col-xs-3 col-sm-3 col-lg-3">
                            TANGGAL TERIMA<br>
                            NOMOR PEMESANAN<br>
                            VENDOR<br>
                            SUMBER DANA<br>
                        </div>
                        <div class="col-xs-9 col-sm-9 col-lg-9">
                            {tanggalpenerimaan}<br>
                            {nopenerimaan}<br>
                            {distributor}<br>
                            RUMAH SAKIT<br>
                        </div>
                        <!-- END Company Info -->
                    </div>
                    <!-- END Invoice Info -->

                    <!-- Table -->
                    <div class="table-responsive push-50">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 50px;"></th>
                                    <th class="text-center" style="width: 100px;">TIPE</th>
                                    <th>BARANG</th>
                                    <th class="text-center" style="width: 100px;">NO BATCH</th>
                                    <th class="text-center" style="width: 100px;">KUANTITAS</th>
                                    <th class="text-right" style="width: 100px;">PPN</th>
                                    <th class="text-right" style="width: 100px;">DISKON</th>
                                    <th class="text-right" style="width: 100px;">HARGA</th>
                                    <th class="text-right" style="width: 100px;">TOTAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $total=0; $no=1; foreach ($detail as $r): ?>
                                    <?php $th = $r->totalharga ?>
                                    <?php $total += $th; ?>
                                    <tr>
                                        <td class="text-center"><?php echo $no ?></td>
                                        <td class="text-center">
                                            <?php $tipe = array(null,'ALKES','IMPLAN','OBAT','LOGISTIK'); ?>
                                            <?php echo $tipe[$r->idtipe] ?>
                                        </td>
                                        <td>
                                            <p class="font-w600 push-10">
                                                <?php echo $this->model->getNamaBarang($r->idtipe,$r->idbarang) ?> 
                                            </p>
                                        </td>
                                        <td class="text-center"><?php echo $r->nobatch ?></td>
                                        <td class="text-center"><span class="badge badge-primary"><?php echo $r->kuantitas ?></span></td>
                                        <td class="text-right"><?php echo number_format($r->ppn) ?></td>
                                        <td class="text-right"><?php echo number_format($r->diskon) ?></td>
                                        <td class="text-right"><?php echo number_format($r->harga) ?></td>
                                        <td class="text-right"><?php echo number_format($r->totalharga) ?></td>
                                    </tr>
                                    <?php $no++ ?>
                                <?php endforeach ?>
                                <tr class="active">
                                    <td colspan="8" class="font-w700 text-uppercase text-right">Total</td>
                                    <td class="font-w700 text-right"><?php echo number_format($total) ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <p class="text-muted">Faktur ini dianggap sebagai kwitansi yang sah.</p>
                        <p class="text-muted">Paraf Petugas Gudang</p>
                        <p class="text-muted" style="margin-top:80px;">(.................................)</p>
                    </div>
                    <!-- END Table -->

                    <!-- Footer -->
                    <hr class="hidden-print">
                    <!-- END Footer -->
                </div>
            </div>
            <!-- END Invoice -->
        </div>
        <!-- END Page Content -->


        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
        <script src="{js_path}core/jquery.min.js"></script>
        <script src="{js_path}core/bootstrap.min.js"></script>
        <script src="{js_path}core/jquery.slimscroll.min.js"></script>
        <script src="{js_path}core/jquery.scrollLock.min.js"></script>
        <script src="{js_path}core/jquery.appear.min.js"></script>
        <script src="{js_path}core/jquery.countTo.min.js"></script>
        <script src="{js_path}core/jquery.placeholder.min.js"></script>
        <script src="{js_path}core/js.cookie.min.js"></script>
        <script src="{js_path}app.js"></script>
    </body>
</html>