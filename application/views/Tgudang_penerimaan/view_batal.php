<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
<div id="cover-spin"></div>
    <?php echo form_open('tgudang_penerimaan/save_batal', 'class="form-horizontal push-10-t" id="form-work" onsubmit="return validate_final()"') ?>
    <div class="block-content block-content-narrow">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-3 control-label">No Pemesanan</label>
                    <div class="col-md-9">
                        <input type="hidden" class="form-control" name="id" id="id" value="{id}">
                        <input type="text" class="form-control" name="nopenerimaan" id="nopenerimaan" value="{nopenerimaan}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Distributor</label>
                    <div class="col-md-9">
                        <input class="form-control" disabled type="text" name="distributor" id="distributor" value="{distributor}">
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-md-3 control-label">No Faktur</label>
                    <div class="col-md-9">
                        <input disabled  class="form-control" type="text" name="nofakturexternal" id="nofakturexternal" value="{nofakturexternal}">
                        <input class="form-control" type="hidden" name="iddistributor" id="iddistributor" value="{iddistributor}">
                        <input class="form-control" type="hidden" name="st_hapus_alih" id="st_hapus_alih" value="0">
                    </div>
                </div>
				
                <div class="form-group">
					<label class="col-md-3 control-label" for="example-datetimepicker4">Tanggal Penerimaan</label>
					<div class="col-md-4">
						<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
                            <input disabled  class="form-control" type="text" id="tanggalpenerimaan" name="tanggalpenerimaan" placeholder="From" value="<?=DMYFormat2($tgl_terima)?>"/>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
					</div>
					
				</div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Keterangan</label>
                    <div class="col-md-9">
                        <textarea  disabled class="form-control" name="keterangan" id="keterangan"><?=$keterangan?></textarea>
                    </div>
                </div>
                
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-3 control-label">Total Barang</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control number" name="totalbarang" id="totalbarang" value="{totalbarang}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Total Harga</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control decimal" name="totalharga" id="totalharga" value="{totalharga}" disabled>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-3 control-label">Pembayaran</label>
                    <div class="col-md-9">
                        <select disabled name="pembayaran" id="pembayaran" class="form-control">
                            <option value="1" <?=($tipe_bayar=='1'?'selected':'')?>>Tunai</option>
                            <option value="2" <?=($tipe_bayar=='2'?'selected':'')?>>Kredit</option>
                        </select>
                    </div>
                </div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="example-datetimepicker4">Tanggal Jatuh Tempo</label>
					<div class="col-md-4">
						<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
							<input  disabled class="js-datepicker form-control" type="text" id="tanggaljatuhtempo" name="tanggaljatuhtempo" value="<?=($tipe_bayar=='2'?DMYFormat2($tgl_terima):'')?>" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">

							<span class="input-group-addon">
								<span class="fa fa-calendar"></span>
							</span>
						</div>
					</div>
				</div>
				<div class="form-group" id="div_alasan">
					<label class="col-md-3 control-label" for="example-datetimepicker4">Alasan</label>
					<div class="col-md-9">
						<textarea  class="form-control" name="alasan_all" id="alasan_all"></textarea>
					</div>
					<input type="hidden" class="form-control" name="st_batal_all" id="st_batal_all" value="0">
				</div>
            </div>
            <div class="clearfix"></div>
            <hr />
			<div class="col-md-12" >
				
				<table class="table table-striped table-borderless table-header-bg" id="data-barang">
					<thead>
						<tr>
							<th width="5%" class="text-center">Tipe</th>
							<th width="15%" class="text-center">Barang</th>
							<th width="10%" class="text-center">No.Batch</th>
							<th width="8%" class="text-center">Jumlah Pesan</th>
							<th width="8%" class="text-center">Jumlah Terima</th>
							<th width="8%" class="text-center">Keterangan</th>
							<th width="10%" class="text-center">Total</th>
							<th width="8%" class="text-center">Action</th>
							
						</tr>
					</thead>
					<tbody>
						<?foreach($detail as $row){
							$status_hapus=0;
							?>
							<tr>
								<td class="text-center"><?=$row->namatipe?></td>
								<td class="text-left"><?=$row->nama_barang?></td>
								<td class="text-center"><?=$row->nobatch?></td><!-- 8 -->
								<td class="text-center"><?=$row->pesan.' '.$row->namasatuan.' / '.$row->jenissatuan?></td>
								<td class="text-center"><?=number_format($row->kuantitas,0).' '.$row->namasatuan?></td>
								<?if ($row->status=='1'){?>
									<?if ($row->pesan > $row->kuantitas){?>
										<td class="text-center"><span class="label label-warning">Diterima Sebagian</span></td><!-- 7 -->
									<?}else{?>
										<td class="text-center"><span class="label label-success">Diterima Semua</span></td>
									<?}?>
								<?}else{
									$status_hapus='1';
									?>
									<td class="text-center"><span class="label label-danger">SUDAH DIBATALKAN</span></td><!-- 7 -->
								<?}?>
								<td class="text-right"><?=number_format($row->totalharga,2)?></td>
								<td class="text-center">
									<?if ($row->status=='1'){?>
									<button class="btn btn-xs btn-danger hapus"  type="button" title="Batalkan Penerimaan Barang"><i class="glyphicon glyphicon-remove"></i></button>
									<?}?>
								</td>

								<td hidden><input type="text" readonly class="form-control" name="st_hapus[]" value="<?=$status_hapus?>"></td><!-- 10 -->
								<td hidden><input type="text" class="form-control" name="e_iddetpesan[]" value="<?=$row->idpemesanandetail?>"></td>
								<td hidden></td><!-- 10 -->
								<td hidden><input type="text" class="form-control" name="e_idtipe[]" value="<?=$row->idtipe?>"></td><!-- 11 -->
								<td hidden><input type="text" class="form-control" name="e_idbarang[]" value="<?=$row->idbarang?>"></td><!-- 12 -->
								<td hidden><input type="text" class="form-control" name="e_iddet[]" value="<?=$row->id?>"></td>		<!-- 13 -->						
								<td hidden><input type="text" class="form-control" name="e_harga[]" value="<?=$row->harga-$row->nominalppn?>"></td><!-- 14 -->
								<td hidden><input type="text" class="form-control" name="e_ppn[]" value="<?=$row->nominalppn?>"></td><!-- 15 -->
								<td hidden><input type="text" class="form-control" name="e_harga_plus_ppn[]" value="<?=$row->harga?>"></td><!-- 16 -->
								<td hidden><input type="text" class="form-control" name="e_harga_diskon[]" value="<?=$row->nominaldiskon?>"></td><!-- 17 -->
								<td hidden><input type="text" class="form-control" name="e_harga_plus_diskon[]" value="<?=$row->harga-$row->nominaldiskon?>"></td><!-- 18 -->
								<td hidden><input type="text" class="form-control" name="e_harga_total_fix[]" value="<?=$row->totalharga?>"></td><!-- 19 -->
								<td hidden><input type="text" class="form-control" name="e_no_batch[]" value="<?=$row->nobatch?>"></td><!-- 20 -->
								<td hidden><input type="text" class="form-control" name="e_st_edit[]" value="0"></td><!-- 21 -->
								<td hidden><input type="text" class="form-control" name="e_st_hapus[]" value="0"></td><!-- 22 -->
								<td hidden><input type="text" class="form-control" name="e_st_alih[]" value="0"></td><!-- 23 -->
								<td hidden><input type="text" class="form-control" name="e_total_alih[]" value="0"></td><!-- 24 -->
								<td hidden><input type="text" class="form-control" name="e_kuantitas[]" value="<?=$row->kuantitas?>"></td<!-- 25 -->
								<td hidden><input type="text" class="form-control" name="e_kuantitas_sisa[]" value="<?=$row->pesan-($row->sudah_terima-$row->kuantitas)?>"></td><!-- 26 -->
								<td hidden><input type="text" class="form-control" name="e_harga_master[]" value="<?=$row->harga_master?>"></td><!-- 27 -->
								<td hidden><input type="text" class="form-control" name="e_tgl_kadaluarsa[]" value="<?=$row->tanggalkadaluarsa?>"></td><!-- 28 -->
								<td hidden><input type="text" class="form-control" name="e_jml_Terima[]" value="<?=$row->sudah_terima?>"></td><!-- 29 -->
								<td hidden><input type="text" class="form-control" name="e_satuan[]" value="<?=$row->namasatuan?>"></td>
								<td hidden><input type="text" class="form-control" name="e_opsisatuan[]" value="<?=$row->opsisatuan?>"></td>
							</tr>
						
						<?					
						}?>
					</tbody>
				</table>
			</div>
			<br>
			<br>
			<br>
			<hr />
			<div class="col-md-12" id="div_hapus" hidden>
				<h3 class="block-title">DATA DIBATALKAN</h3>
				<table class="table table-striped table-borderless table-header-bg" id="data_hapus">
					<thead>
						<tr>
							<th width="5%" class="text-center">Tipe</th>
							<th width="15%" class="text-center">Barang</th>
							<th width="5%" class="text-center">Jumlah</th>
							<th width="15%" class="text-center">Keterangan</th>
							<th width="15%" class="text-center" hidden>Distributor</th>
							<th width="20%" class="text-center">Alasan</th>
							<th width="5%" class="text-center">Batalkan</th>
							
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
        </div>
        <div style="margin-top:50px;margin-bottom:20px;" class="text-right">
            <a href="{site_url}tgudang_penerimaan" class="btn btn-default btn-sm">Kembali</a>
            <button class="btn btn-sm btn-danger" type="submit" id="simpan_batal">Simpan Pembatalan</button>
        </div>
		<div class="row">
		<div class="col-md-6">
			<h4 class="block-title">Info User</h4>
			<table class="table table-bordered" id="data_user">
				<thead>
					<tr>
						<th width="5%" class="text-left">Jenis</th>
						<th width="15%" class="text-left">Nama User</th>
						<th width="5%" class="text-left">Tanggal</th>
						
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class='text-left'>USER PEMBUAT</td>
						<td  class='text-left'><?= $detail_user['userpemesanan'] ?></td>
						<td  class='text-left'><?= $detail_user['tgl_pemesanan'] ?></td>
					<tr>
					<tr>
						<td class='text-left'>USER KONFIRMASI</td>
						<td  class='text-left'><?= $detail_user['nama_user_konfirmasi'] ?></td>
						<td  class='text-left'><?= $detail_user['tanggal_konfirmasi'] ?></td>
					<tr>
					<tr>
						<td class='text-left'>USER BAG. KEUANGAN</td>
						<td  class='text-left'><?= $detail_user['nama_user_approval'] ?></td>
						<td  class='text-left'><?= $detail_user['tanggal_approval'] ?></td>
					<tr>
					<tr>
						<td class='text-left'>USER PEMESANAN</td>
						<td  class='text-left'><?= $detail_user['nama_user_finalisasi'] ?></td>
						<td  class='text-left'><?= $detail_user['tgl_finalisasi'] ?></td>
					<tr>
				</tbody>
			</table>
		</div>
		</div>
    </div>
    <?php echo form_close() ?>
</div>

</div>
<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
    var table, t, tanggal;
	$(".number").number(true,0,'.',',');
	$(".diskon").number(true,2,'.',',');
	$(".decimal").number(true,2,'.',',');
	// $("#pembayaran").select2();
	$('#pembayaran').select2();
    $(document).ready(function() {
			cek_all();
    });
	
	var table, tr;
	
	$(document).on("click",".hapus",function(){
		$("#div_hapus").show();
		 var tr = $(this).closest('tr');
		 var jml=parseFloat(tr.find('td:eq(25) input').val());
		 if (cek_duplikasi(tr[0].sectionRowIndex)==false){
			 return false;
		 }
		 var content = "<tr>";
		 content += "<td class='text-center'>" + tr.find('td:eq(0)').text() + "</td>"; //0 Nomor
		 content += "<td  class='text-left'>" + tr.find('td:eq(1)').text() + "</td>"; //1 
		 content += "<td  class='text-center'>" + jml +' '+tr.find('td:eq(30) input').val()+ "</td>"; //2 
		 content += '<td  class="text-center"><span class="label label-danger">DIBATALKAN</span></td>'; //2 
		 
		 content += '<td hidden><select class="form-control e_dist" name="d_iddistributor[]" style="width:100%"><option value="#" selected>TIDAK PERLU DIISI</option></select></td>'; //3
		 content += '<td ><input type="text" class="form-control alasan" name="d_alasan[]" value=""></td>'; //4
		 content += '<td hidden><input type="text" class="form-control" name="d_iddet[]" value="'+tr.find('td:eq(13) input').val()+'"></td>'; //5
		 
		
		content += '<td hidden><input type="text" class="form-control" name="d_status[]" value="1"></td>'; //SEBAGIAN
		 
		 content += '<td hidden><input type="text" class="form-control" name="d_kuantitas[]" value="'+ jml +'"></td>'; //8
		 content += '<td hidden><input type="text" class="form-control" name="d_index_row[]" value="'+ tr[0].sectionRowIndex +'"></td>'; //9
		 content += '<td ><button class="btn btn-xs btn-danger remove"  type="button" title="Batalkan Aksi"><i class="fa fa-trash-o"></i></button></td>'; //10
		 content += "</tr>";
		 // tr.remove();
		$('#data_hapus tbody').append(content);
		show_button(tr[0].sectionRowIndex);
		
	});
	function cek_duplikasi($row){
		var duplikasi=0;
		$('#data_hapus tbody tr').each(function() {
			var tr = $(this).closest('tr');	
			 
			if (tr.find("td:eq(9) input").val()==$row){
				duplikasi='1';
			}
			
			
		});
		if (duplikasi){
			sweetAlert("Maaf...", "Barang Sudah ada Dalam List!", "error");
            return false;
		}
		return true;
	}
	function show_button($row){
		// var stok=get_stok_dipake($row);
		$('#data-barang tbody tr').each(function() {
			var tr = $(this).closest('tr');	
			if (tr[0].sectionRowIndex==$row){				
				var button='';				
				button += '<span class="label label-danger">DIBATALKAN</span>';					
				tr.find("td:eq(7)").html(button);
				tr.find("td:eq(8) input").val(1);
			}
			
			
		});
		cek_all();
		// return sisa-jml;
	}
	function show_normal_button($row){
		// var stok=get_stok_dipake($row);
		$('#data-barang tbody tr').each(function() {
			var tr = $(this).closest('tr');	
			if (tr[0].sectionRowIndex==$row){
				
				var button='';
				
					button += '<button class="btn btn-xs btn-danger hapus"  type="button" title="Batalkan Penerimaan Barang"><i class="glyphicon glyphicon-remove"></i></button>';
					
				tr.find("td:eq(7)").html(button);
				tr.find("td:eq(8) input").val(0);
			}
			
			
		});
		cek_all();
		// return sisa-jml;
	}
	function cek_all(){
		var totalbarang = 0;
        var totalharga = 0;
		var st_all=1;
		$('#data-barang tbody tr').each(function() {
			var tr = $(this).closest('tr');	
			if (tr.find("td:eq(8) input").val()=='0'){
				st_all=0;
				 // totalbarang += parseFloat($(this).find('td:eq(6)').text().replace(/\,/g, ""));
				 totalbarang += parseFloat($(this).find('td:eq(25) input').val());
				 totalharga += parseFloat($(this).find('td:eq(19) input').val());
			}
			
		});
		console.log(totalharga);
		$("#totalbarang").val(isNaN(totalbarang) ? 0 : totalbarang)
        $("#totalharga").val(isNaN(totalharga) ? 0 : totalharga)
		$("#st_batal_all").val(st_all);
		if (st_all=='1'){
			$("#div_alasan").show();
			$(".alasan").attr('readonly',true);
		}else{
			$("#div_alasan").hide();
			$(".alasan").attr('readonly',false);
		}
			console.log(st_all);
		// return sisa-jml;
	}
	$(document).on("click",".remove",function(){
		var row='';
		 var tr = $(this).closest('tr');
		 row=tr.find('td:eq(9) input').val();
		 tr.remove();
		 show_normal_button(row);
		
	});
	
	
	function formatNumber (num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}
    
    function validate_final() {
        if ($('#st_batal_all').val() == 1 && $('#alasan_all').val() == '') {
            sweetAlert("Maaf...", "Karena dibatalkan semua Alasan Global Harus diisi!", "error");
			$('#alasan_all').focus();
            return false;
        }
        var rowCount = $('#data_hapus tr').length;
		if (rowCount <=1){
			sweetAlert("Maaf...", "Tidak ada Item yang dihapus!", "error");
            return false;
		}else{
			var ada_kosong_hapus='1';
			if (rowCount>1){
				$('#data_hapus tbody tr').each(function() {
					var tr = $(this).closest('tr');	
					
					if (tr.find("td:eq(5) input").val()==''){
						ada_kosong_hapus='0';					
					}
					
					
				});
				
				if (ada_kosong_hapus=='0'){
					sweetAlert("Maaf...", "Harus Per Barang diisi Alasan Hapus!", "error");
					return false;
				}
			}
		}
		// var pertanyaan;
		// if ($('#st_batal_all').val() == 1){
			// pertanyaan='Apakah Anda Yakin akan membatalkan seluruh transaksi penerimaan ini ?';
		// }else{
			// pertanyaan='Apakah Anda Yakin akan membatalkan seluruh transaksi penerimaan ini ?';
			
		// }
		// swal({
			// title: "Apakah Anda Yakin ?",
			// text : "Akan Membatalkan Transaki ?",
			// type : "success",
			// showCancelButton: true,
			// confirmButtonText: "Ya",
			// confirmButtonColor: "#34a263",
			// cancelButtonText: "Batalkan",
		// }).then(function() {
			// $("#simpan_batal").hide();
			// $("#cover-spin").show();
			// $("*[disabled]").not(true).removeAttr("disabled");
			// return true;
		// });
		$("#simpan_batal").hide();
		$("#cover-spin").show();
		$("*[disabled]").not(true).removeAttr("disabled");
		return true;
		
    }
	$(document).on("keyup","#alasan_all",function(){
		$(".alasan").val($("#alasan_all").val());
		
	});
	
</script>