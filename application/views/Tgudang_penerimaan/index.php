<?php echo ErrorSuccess($this->session); ?>
<?php if ('' != $error) {
    echo ErrorMessage($error);
} ?>
<?php if (UserAccesForm($user_acces_form,array('1138'))){ ?>
<div class="block">
    <div class="block-header">
        <?php if (UserAccesForm($user_acces_form,array('1139'))){ ?>
            <ul class="block-options">
                <li>
                    <a href="{site_url}tgudang_penerimaan/create2" class="btn btn-default" id="tambah-penerimaan">
                        <i class="fa fa-plus"></i> Tambah
                    </a>
                </li>
            </ul>
        <?php } ?>
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:25px">
        <div class="row">
            <?php echo form_open('tgudang_penerimaan/filter', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Distributor</label>
                    <div class="col-md-8">
                        <select id="iddistributor" name="iddistributor" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#" <?=($iddistributor == '#' ? 'selected' : ''); ?>>Semua Distributor</option>
                            <?php foreach ($list_distributor as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?=($iddistributor == $row->id ? 'selected' : ''); ?>><?php echo $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">No. Penerimaan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_trx" placeholder="No. Penerimaan" name="no_trx" value="{no_trx}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">No. PO</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_po" placeholder="No. PO" name="no_po" value="{no_po}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">No. Faktur</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_faktur" placeholder="No. Faktur" name="no_faktur" value="{no_faktur}">
                    </div>
                </div>
                
            </div>
			<div class="col-md-6">
                
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Status</label>
                    <div class="col-md-8">
                        <select id="status" name="statuspenerimaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="0">Semua Status</option>
                            <option value="1" <?=(1 == $statuspenerimaan ? 'selected' : ''); ?>>Belum Terima</option>
                            <option value="2" <?=(2 == $statuspenerimaan ? 'selected' : ''); ?>>Sudah Terima</option>
                        </select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Cara Bayar</label>
                    <div class="col-md-8">
                        <select id="status" name="tipe_bayar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#" <?=('#' == $tipe_bayar ? 'selected' : ''); ?>>Semua</option>
                            <option value="1" <?=(1 == $tipe_bayar ? 'selected' : ''); ?>>TUNAI</option>
                            <option value="2" <?=(2 == $tipe_bayar ? 'selected' : ''); ?>>KREDIT</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" name="tanggaldari" placeholder="From" value="{tanggaldari}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" name="tanggalsampai" placeholder="To" value="{tanggalsampai}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    
    <!-- start content -->
    <div class="block-content">
		<div class="table-responsive">
        <table class="table table-bordered table-striped" id="index_list">
            <thead>
                <tr>
                    <th>ID</th>
                    <th width="3%" class="text-center">#</th>
                    <th width="8%" class="text-center">NO PENERIMAAN</th>
                    <th width="8%" class="text-center">TANGGAL</th>
                    <th width="8%" class="text-center">NO PEMESANAN</th>
                    <th width="7%" class="text-center">TGL TERIMA</th>
                    <th width="10%" class="text-center">NO FAKTUR</th>
                    <th width="15%" class="text-center">DISTRIBUTOR</th>
                    <th width="8%" class="text-center">TIPE PEMBAYARAN</th>
                    <th width="8%" class="text-center">JATUH TEMPO</th>
                    <th width="10%" class="text-center">TOTAL</th>
                    <th width="10%" class="text-center">ACTION</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
		</div>
    </div>
    <!-- end content -->

</div>
<?}?>
<div class="modal" id="modal-detail-penerimaan" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title"></h3>
                </div>
                <div class="block-content">
					<div class="table-responsive">
                    <table class="table table-striped table-bordered" id="detail-penerimaan">
                        <thead>
                            <tr>
                                <th>Tipe</th>
                                <th>Nama</th>
                                <th>No Batch</th>
                                <th>Harga</th>
                                <th>PPN</th>
                                <th>Diskon</th>
                                <th>J Pesan</th>
                                <th>J Terima</th>
                                <th>J Sisa</th>
                                <th>T Harga</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>                    
                </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-detail-penerimaan" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Detail Penerimaan</h3>
                </div>
                <div class="block-content">
					<div class="table-responsive">
                    <table class="table table-striped table-bordered" id="detail-penerimaan">
                        <thead>
                            <tr>
                                <th>Tipe</th>
                                <th>Nama</th>
                                <th>No Batch</th>
                                <th>Harga</th>
                                <th>PPN</th>
                                <th>Diskon</th>
                                <th>J Pesan</th>
                                <th>J Terima</th>
                                <th>J Sisa</th>
                                <th>T Harga</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>                    
                </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                </div>                
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal_edit_no_faktur" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <form class="form-horizontal" action="javascript:edit_no_faktur();" id="frm1">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">Edit Nomor Faktur</h3>
                    </div>
                    <div class="block-content">
                        <input type="hidden" name="idpenerimaan">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nomor Faktur</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="nofakturexternal" placeholder="Nomor Faktur" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                        <button class="btn btn-sm btn-success" type="submit">Simpan</button>
                    </div>                
                </div>
            </div>
        </form>
    </div>
</div>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
$(document).on('click', '#tambah-penerimaan', function(){
    
    noPemesanan();
    $('#simpan-detail, .detail').attr('disabled',true);
    $('.detail, #totalharga, #xsisa, #totalbarang, #tanggalpenerimaan, #tanggaljatuhtempo').val('');
    $('#tanggalpenerimaan').val(tanggal);
    $('#tanggaljatuhtempo').val(tanggal);
});

$('#index_list tbody').on('click', 'td a.view-detail-penerimaan', function(){
    var idpenerimaan = table.cell($(this).parents('tr'),10).data()

    $('#detail-penerimaan').DataTable({
        pageLength: 10,        
        info: false,
        sort: false,
        searching: false,
        paging: false,
        destroy: true,
        order: [ [1,'desc'] ],
        ajax: { 
            url: '{site_url}tgudang_penerimaan/detailPenerimaan', 
            type: "POST",
            data: {idpenerimaan: idpenerimaan} 
        },
        columns: [
            {data: "idtipe"},
            {data: "idbarang"},
            {data: "nobatch"},
            {data: "harga"},
            {data: "ppn"},
            {data: "diskon"},
            {data: "kuantitas"},
            {data: "kuantitas"},
            {data: "idbarang"},
            {data: "totalharga"},
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

            var tipe = [null, 'Alkes', 'Implan', 'Obat', 'Logistik']
            $('td:eq(0)', nRow).text(tipe[aData['idtipe']])


            $.ajax({
                url: '{site_url}tgudang_penerimaan/ajaxDetail',
                data: {idpenerimaan: idpenerimaan, idtipe: aData['idtipe'], idbarang: aData['idbarang']},
                method: 'post',
                dataType: 'json',
                success: function(res) {
                    $('td:eq(1)',nRow).html(res.namabarang)
                    $('td:eq(6)',nRow).html(res.jmlpesan)
                    $('td:eq(7)',nRow).html(res.jmltrm)
                    $('td:eq(8)',nRow).html(res.jmlpesan - res.jmltrm)
                }
            })
            return nRow;
        }
    })

    $('#index_list').DataTable().ajax.reload()
})

$(document).ready(function(){
    // table = $('#index_list').DataTable({
        // pageLength: 50,  
        // serverSide: true, 
        // destroy: true,
        // ajax: { "url": '{site_url}tgudang_penerimaan/ajax_list', "type": "POST" },
        // columns: [
            // {
                // data: "nopenerimaan",
                // render: function(data) {
                    // return '<label class="label label-default">'+data+'</label>';
                // }
            // },
            // {data: "tanggalpenerimaan"},
            // {data: "nofakturexternal"},
            // {data: "distributor"},
            // {data: "totalpsn", "render": $.fn.DataTable.render.number('.',',') },
            // {data: "totalpernahtrm", "render": $.fn.DataTable.render.number('.',',') },
            // {data: "totaltrm", "render": $.fn.DataTable.render.number('.',',') },
            // {data: "totalsisa", "render": $.fn.DataTable.render.number('.',',') },
            // {data: "totalharga", "render": $.fn.DataTable.render.number('.',',') },
            // {
                // data: "id",
                // width: '12%', className: 'text-right',
                // render: function(data,type,row) {
                    // var aksi = `<div class="btn-group">`;
                    // aksi += `<a href="#detail" class="btn btn-info btn-xs view-detail-penerimaan" data-toggle="modal" data-target="#modal-detail-penerimaan"><i class="fa fa-search"></i></a>`;
                    // <?php if (button_roles('tgudang_penerimaan/print_penerimaan')) : ?>
                        // aksi += `<a onclick="window.open( '{site_url}tgudang_penerimaan/print_penerimaan/`+row.id+`', '_blank' );" class="btn btn-default btn-xs"><i class="fa fa-print"></i></a>`;
                    // <?php endif; ?>
                    // aksi += `<a href="{site_url}tgudang_penerimaan/edit/`+row.id+`" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>`;
                    // aksi += `</div>`;
                    // return aksi;
                // }
            // },
            // {data: "id"},
        // ],
        // columnDefs: [
            // {targets: [10], visible: false},
            // {targets: [2,3,4,5,6,7,8], sorting: false}
        // ],
    // })
	load_index();
})
function load_index(){
	// alert('SINI');
	table = $('#index_list').DataTable({
		autoWidth: false,
		searching: false,
		"processing": true,
		pageLength: 50,
		serverSide: true,
		"order": [],
		"pageLength": 10,
		"ordering": false,		
		columnDefs: [{ "targets": [0], "visible": false },
						 {"targets": [1], className: "text-center" },
						 {"targets": [2], className: "text-center" },
						 {"targets": [3], className: "text-center" },
						 {"targets": [4], className: "text-center" },
						 {"targets": [5], className: "text-center" },
						 {"targets": [7], className: "text-left" },
						 {"targets": [8], className: "text-center" },
						 {"targets": [9], className: "text-center" },
						 {"targets": [10], className: "text-right" }
						 ],
		ajax: { 
			url: '{site_url}tgudang_penerimaan/get_index', 
			type: "POST" ,
			dataType: 'json'
		}
	});
}
function edit_no_faktur() {
    var form = $('#frm1')[0];
    var formData = new FormData(form);
    $.ajax({
        url: '{site_url}tgudang_penerimaan/edit_no_faktur',
        method: 'post',
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        success: function(res) {
            if(res.status == 200) {
                swal('Berhasil',res.msg,'success');
                setTimeout(function() {
                    location.reload();
                }, 1000);
            } else {
                swal('Error',res.msg,'error');
            }
        }
    })
}


</script>
