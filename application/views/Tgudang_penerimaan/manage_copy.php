<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        	<li><a href="{site_url}tgudang_penerimaan" class="btn"><i class="fa fa-reply"></i></a></li>
    	</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<?php echo form_open('tgudang_penerimaan/save','class="form-horizontal push-10-t"') ?>
	<div class="block-content block-content-narrow">
		<div class="form-group">
			<label class="col-md-3 control-label" for="nopemesanan">No Pemesanan</label>
			<div class="col-md-7">
				<select name="nopemesanan" id="nopemesanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.."></select>
			</div>
		</div>
	</div>
	<div class="block-content">
		<div class="row">
			<div class="col-sm-12">
				<div class="control-group">
					<div class="col-md-12">
						<table class="table table-striped table-bordered js-dataTable-full" id="detail_list">
							<thead>
								<tr>
									<th>Tipe</th>
									<th>Barang</th>
									<th>Distributor</th>
									<th>Harga Beli</th>
									<th>Kuantitas</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
						<br><br>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="text-right bg-light lter">
	<button class="btn btn-info" type="submit">Simpan</button>
	<a href="{site_url}tgudang_pemesanan" class="btn btn-default">Batal</a>
</div>
<?php echo form_hidden('id', $id); ?>
<?php echo form_close() ?>

<!-- Fade In Modal -->
<div class="modal in" id="modal-penerimaan-barang" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Terms &amp; Conditions</h3>
                </div>
                <div class="block-content">
                	<form class="form-horizontal" id="form_detail"></form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                <button class="btn btn-sm btn-primary" id="edit-penerimaan-barang" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Ok</button>
            </div>
        </div>
    </div>
</div>
<!-- END Fade In Modal -->

<script src="{js_path}custom/basic.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		// get nopemesanan
		$.getJSON('{site_url}tgudang_penerimaan/get_nopemesanan', function(data){
			var dataoption = '<option value="#">Pilih Opsi</option>';
			$.each(data, function(i,item){
				dataoption += '<option value="'+item.id+'">'+item.nopemesanan+'</option>';
			})
			$('#nopemesanan').empty()
			$('#nopemesanan').append(dataoption)
		})
	})

	$('#detail_list tbody').on('click','tr', function(){
		// alert( $('#detail_list').DataTable().row(this).index() )
		// console.log( $('#detail_list tbody').children('tr').length )
		var rows = $('tr');
		console.log( $('#detail_list').DataTable().rows().data() )
		// console.log( $('#detail_list tr').length )


		// alert( $('#detail_list').DataTable().cell(this).data() )
		removeClass('#fhargabeli')
		add_input('#form_detail','Harga Beli','hargabeli', $('td:eq(3)').text() )

		removeClass('#fkuantitas')
		add_input('#form_detail','Jumlah Terima','kuantitas', $('td:eq(4)').text() )

		removeClass('#fnobatch')
		add_input('#form_detail','No Batch','nobatch', '' )		

		removeClass('#fkuantitas-pemesanan')
		add_input('#form_detail','Kuantitas Pemesanan','kuantitas-pemesanan', $('td:eq(4)').text(), 'text', 'fa-lock' )
		disabled('#kuantitas-pemesanan')
	})

	$(document).on('change', '#nopemesanan', function(){
		var n = $(this).val()

		$('table#detail_list tbody').empty();
		// get detail Pemesanan
		$.getJSON('{site_url}tgudang_penerimaan/get_nopemesanan_detail/'+n, function(data){

			$.each(data, function(i,item){
	            tr = $('<tr/>');
	            tr.append("<td>" + item.tipebarang + "</td>"); //0
	            tr.append("<td>" + item.namabarang + "</td>"); //1
	            tr.append("<td>" + item.distributor + "</td>"); //2
	            tr.append("<td>" + item.harga + "</td>"); //3
	            tr.append("<td>" + item.kuantitas + "</td>"); //4
	            tr.append("<td><a href='#' class='on-default detail_edit' data-toggle='modal' data-target='#modal-penerimaan-barang' title='Edit'><i class='fa fa-edit'></i></a></td>");
	            tr.append("<td hidden>" + item.idbarang + "</td>"); //6
	            tr.append("<td hidden>" + item.iddistributor + "</td>"); //7
	            tr.append("<td hidden></td>"); //8
	            $('table#detail_list tbody').append(tr);
			})

			// alert(JSON.stringify(data))
		})
	})


	$(document).on('click', '#edit-penerimaan-barang', function(){
        var validate = detail_validate();
        if(!validate) {
            return false;
        }
        // $('#detail_list').DataTable().cell( this ).data()
	})

    function detail_validate(){
        if($("#kuantitas").val() == ""){
        				
            return false;
        }
        if( $("#kuantitas").val() > $("#kuantitas-pemesanan").val() ){
        	alert('Kuantitas tidak boleh lebih besar dari jumlah barang pemesanan!')
            return false;
        }
        return true;
    }	
</script>
