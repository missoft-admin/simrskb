<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>

    <?php echo form_open('tgudang_penerimaan/save_edit', 'class="form-horizontal push-10-t" id="form-work" onsubmit="return validate_header()"') ?>
    <div class="block-content block-content-narrow">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-3 control-label">No Pemesanan</label>
                    <div class="col-md-9">
                        <input type="hidden" class="form-control" name="tkontrabon_id" id="tkontrabon_id" value="{tkontrabon_id}" readonly>
                        <input type="hidden" class="form-control" name="id_header_kontrabon" id="id_header_kontrabon" value="{id_header_kontrabon}" readonly>
                        <input type="hidden" class="form-control" name="id" id="id" value="{id}" readonly>
                        <input type="text" class="form-control" name="nopenerimaan" id="nopenerimaan" value="{nopenerimaan}" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Distributor</label>
                    <div class="col-md-9">
                        <input class="form-control" readonly type="text" name="distributor" id="distributor" value="{distributor}">
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-md-3 control-label">No Faktur</label>
                    <div class="col-md-9">
                        <input class="form-control" type="text" name="nofakturexternal" id="nofakturexternal" value="{nofakturexternal}">
                        <input class="form-control" type="hidden" name="iddistributor" id="iddistributor" value="{iddistributor}">
                        <input class="form-control" type="hidden" name="st_hapus_alih" id="st_hapus_alih" value="0">
                    </div>
                </div>
				
                <div class="form-group">
					<label class="col-md-3 control-label" for="example-datetimepicker4">Tanggal Penerimaan</label>
					<div class="col-md-4">
						<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
                            <input class="form-control" type="text" id="tanggalpenerimaan" name="tanggalpenerimaan" placeholder="From" value="<?=DMYFormat2($tgl_terima)?>"/>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
					</div>
					
				</div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Keterangan</label>
                    <div class="col-md-9">
                        <textarea class="form-control" name="keterangan" id="keterangan"><?=$keterangan?></textarea>
                    </div>
                </div>
                
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-3 control-label">Total Barang</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control number" name="totalbarang" id="totalbarang" value="{totalbarang}" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Total Harga</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control diskon" name="totalharga" id="totalharga" value="{totalharga}" readonly>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-3 control-label">Pembayaran</label>
                    <div class="col-md-9">
                        <select name="pembayaran" id="pembayaran" class="form-control">
                            <option value="1" <?=($tipe_bayar=='1'?'selected':'')?>>Tunai</option>
                            <option value="2" <?=($tipe_bayar=='2'?'selected':'')?>>Kredit</option>
                        </select>
                    </div>
                </div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="example-datetimepicker4">Tanggal Jatuh Tempo</label>
					<div class="col-md-4">
						<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
							<input class="js-datepicker form-control" type="text" id="tanggaljatuhtempo" name="tanggaljatuhtempo" value="<?=($tipe_bayar=='2'?DMYFormat2($tanggaljatuhtempo):'')?>" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">

							<span class="input-group-addon">
								<span class="fa fa-calendar"></span>
							</span>
						</div>
					</div>
					<?if($id_header_kontrabon !=''){//Kontrabon?>
					<div class="col-md-4">
						<button class="btn btn-danger btn-sm" type="button" id="btn_update_tanggal" disabled><li class="fa fa-save"></li> Update Tanggal Kontrabon</button>
						<input class="js-datepicker form-control" type="hidden" id="tgl_awal" name="tgl_awal" value="<?=($tipe_bayar=='2'?DMYFormat2($tanggaljatuhtempo):'')?>" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">
					</div>
					<?}?>
				</div>
            </div>
            <div class="clearfix"></div>
            <hr />
			<div class="col-md-12" >
				
				<table class="table table-striped table-borderless table-header-bg" id="data-barang">
					<thead>
						<tr>
							<th width="5%" class="text-center">Tipe</th>
							<th width="20%" class="text-center">Barang</th>
							<th width="8%" class="text-center">Jumlah Pesan</th>
							<th width="8%" class="text-center">Harga + PPN</th>
							<th width="8%" class="text-center">Diskon</th>
							<th width="8%" class="text-center">Jumlah Terima</th>
							<th width="10%" class="text-center">Total</th>
							<th width="10%" class="text-center">Keterangan</th>
							<th width="10%" class="text-center">No.Batch</th>
							<th width="5%" class="text-center">Aksi</th>
							
						</tr>
					</thead>
					<tbody>
						<?foreach($detail as $row){?>
							<tr>
								<td class="text-center"><?=$row->namatipe?></td>
								<td class="text-left"><?=$row->nama_barang?></td>
								<td class="text-center"><?=$row->pesan.' '.$row->namasatuan.' / '.$row->jenissatuan?></td>
								<td class="text-right"><?=number_format($row->harga,2)?></td>
								<td class="text-right">(Rp) <?=number_format($row->nominaldiskon,2)?><br><?=number_format($row->diskon,0)?> (%)</td>
								<td class="text-center"><?=number_format($row->kuantitas,0).' '.$row->namasatuan?></td>
								<td class="text-right"><?=number_format($row->totalharga,2)?></td>
								<?if ($row->pesan > $row->kuantitas){?>
									<td class="text-center"><span class="label label-warning">Diterima Sebagian</span></td><!-- 7 -->
								<?}else{?>
									<td class="text-center"><span class="label label-success">Diterima Semua</span></td>
								<?}?>
								<td class="text-center"><?=$row->nobatch?></td><!-- 8 -->
								<?if ($row->status=='1'){?>
								<td class="text-center">
									<?if ($this->uri->segment(4)>1){?>
										<span class="label label-danger">KONTRABON</span>
									<?}else{?>
									<button class="btn btn-xs btn-success edit"  type="button" title="Terima Barang"><i class="fa fa-pencil"></i></button>	
									<?}?>
									
									<!--<button class="btn btn-xs btn-danger hapus"  type="button" title="Hapus Barang"><i class="glyphicon glyphicon-remove"></i></button>-->
								</td><!-- 9 -->
								<?}else{?>
									<td class="text-center"><span class="label label-danger">DIHAPUS</span></td><!-- 7 -->
								<?}?>
								<td hidden></td>
								<td hidden><input type="text" class="form-control" name="e_iddetpesan[]" value="<?=$row->idpemesanandetail?>"></td>
								<td hidden></td><!-- 12 -->
								<td hidden><input type="text" class="form-control" name="e_idtipe[]" value="<?=$row->idtipe?>"></td><!-- 13 -->
								<td hidden><input type="text" class="form-control" name="e_idbarang[]" value="<?=$row->idbarang?>"></td>
								<td hidden><input type="text" class="form-control" name="e_iddet[]" value="<?=$row->id?>"></td>								
								<td hidden><input type="text" class="form-control" name="e_harga[]" value="<?=$row->harga-$row->nominalppn?>"></td>
								<td hidden><input type="text" class="form-control" name="e_ppn[]" value="<?=$row->ppn?>"></td>
								<td hidden><input type="text" class="form-control" name="e_harga_plus_ppn[]" value="<?=$row->harga?>"></td>
								<td hidden><input type="text" class="form-control" name="e_harga_diskon[]" value="<?=$row->nominaldiskon?>"></td>
								<td hidden><input type="text" class="form-control" name="e_harga_plus_diskon[]" value="<?=$row->harga-$row->nominaldiskon?>"></td>
								<td hidden><input type="text" class="form-control" name="e_harga_total_fix[]" value="<?=$row->totalharga?>"></td>
								<td hidden><input type="text" class="form-control" name="e_no_batch[]" value="<?=$row->nobatch?>"></td>
								<td hidden><input type="text" class="form-control" name="e_st_edit[]" value="0"></td><!-- 23 -->
								<td hidden><input type="text" class="form-control" name="e_st_hapus[]" value="0"></td>
								<td hidden><input type="text" class="form-control" name="e_st_alih[]" value="0"></td>
								<td hidden><input type="text" class="form-control" name="e_total_alih[]" value="0"></td><!-- 26 -->
								<td hidden><input type="text" class="form-control" name="e_kuantitas[]" value="<?=$row->kuantitas?>"></td>
								<td hidden><input type="text" class="form-control" name="e_kuantitas_sisa[]" value="<?=$row->pesan-($row->sudah_terima-$row->kuantitas)?>"></td>
								<td hidden><input type="text" class="form-control" name="e_harga_master[]" value="<?=$row->harga_master?>"></td>
								<td hidden><input type="text" class="form-control" name="e_tgl_kadaluarsa[]" value="<?=$row->tanggalkadaluarsa?>"></td>
								<td hidden><input type="text" class="form-control" name="e_jml_Terima[]" value="<?=$row->sudah_terima?>"></td>
								<td hidden><input type="text" class="form-control" name="e_satuan[]" value="<?=$row->namasatuan?>"></td>
								<td hidden><input type="text" class="form-control" name="e_opsisatuan[]" value="<?=$row->opsisatuan?>"></td>
								
								<td hidden><input type="text" class="form-control" name="e_harga_asli[]" value="<?=$row->harga_asli?>"></td><!-- 34-->
								<td hidden><input type="text" class="form-control" name="e_diskon[]" value="<?=$row->diskon?>"></td><!-- 35-->
								<td hidden><input type="text" class="form-control" name="e_diskon_rp[]" value="<?=$row->nominaldiskon?>"></td><!-- 36-->
								<td hidden><input type="text" class="form-control" name="e_harga_after_diskon[]" value="<?=$row->harga_after_diskon?>"></td><!-- 37-->
								<td hidden><input type="text" class="form-control" name="e_pajak[]" value="<?=$row->ppn?>"></td><!-- 38-->
								<td hidden><input type="text" class="form-control" name="e_pajak_rp[]" value="<?=$row->nominalppn?>"></td><!-- 39-->
								<td hidden><input type="text" class="form-control" name="e_harga_after_ppn[]" value="<?=$row->harga_after_ppn?>"></td><!-- 40-->
								<td hidden><input type="text" class="form-control" name="e_harga_total_final[]" value="<?=$row->totalharga?>"></td><!-- 41-->
							</tr>
						
						<?					
						}?>
					</tbody>
				</table>
			</div>
			<br>
			<br>
			<br>
			<hr />
			<div class="col-md-12" id="div_hapus" hidden>
				<h3 class="block-title">DATA HAPUS & ALIHKAN DISTRIBUTOR</h3>
				<table class="table table-striped table-borderless table-header-bg" id="data_hapus">
					<thead>
						<tr>
							<th width="5%" class="text-center">Tipe</th>
							<th width="15%" class="text-center">Barang</th>
							<th width="5%" class="text-center">Jumlah</th>
							<th width="15%" class="text-center">Keterangan</th>
							<th width="15%" class="text-center">Distributor</th>
							<th width="20%" class="text-center">Alasan</th>
							
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
        </div>
        <div style="margin-top:50px;margin-bottom:20px;" class="text-right">
			<?if ($tkontrabon_id !='' && $id_header_kontrabon==''){?>
				<a href="{site_url}tkontrabon_verifikasi" name="btn_kembali" class="btn btn-default btn-sm">Kembali </a>
			<?}elseif($id_header_kontrabon !=''){//Kontrabon?>
				<button class="btn btn-primary btn-sm" type="button" id="btn_back"><li class="fa fa-mail-reply"></li> Ke Kontrabon</button>
			<?}else{?>
            <a href="{site_url}tgudang_penerimaan" class="btn btn-default btn-sm">Kembali </a>
			<?}?>
			<?if ($tkontrabon_id!='' && $id_header_kontrabon==''){?>
			<button class="btn btn-sm btn-success simpan-penerimaan" type="submit" name="btn_simpan" value="2">Update</button>
			<button class="btn btn-sm btn-warning simpan-penerimaan2" type="submit" name="btn_simpan" value="4">Update & Verifikasi</button>
			<button class="btn btn-sm btn-primary simpan-penerimaan2" type="submit" name="btn_simpan" value="3">Update & Aktivasi</button>
			<?}elseif($id_header_kontrabon=='' && $tkontrabon_id==''){//Kontrabon?>
            <button class="btn btn-sm btn-success simpan-penerimaan" type="submit" name="btn_simpan" value="1">Simpan</button>
			
			<?}else{?>
			<?}?>
        </div>
		
    </div>
    <?php echo form_close() ?>
	<div class="block-content block-content-narrow">
		<div class="row">
			<div class="col-md-6">
				<h4 class="block-title">Info User</h4>
				<table class="table table-bordered" id="data_user">
					<thead>
						<tr>
							<th width="5%" class="text-left">Jenis</th>
							<th width="15%" class="text-left">Nama User</th>
							<th width="5%" class="text-left">Tanggal</th>
							
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class='text-left'>USER PEMBUAT</td>
							<td  class='text-left'><?= $detail_user['userpemesanan'] ?></td>
							<td  class='text-left'><?= $detail_user['tgl_pemesanan'] ?></td>
						<tr>
						<tr>
							<td class='text-left'>USER KONFIRMASI</td>
							<td  class='text-left'><?= $detail_user['nama_user_konfirmasi'] ?></td>
							<td  class='text-left'><?= $detail_user['tanggal_konfirmasi'] ?></td>
						<tr>
						<tr>
							<td class='text-left'>USER BAG. KEUANGAN</td>
							<td  class='text-left'><?= $detail_user['nama_user_approval'] ?></td>
							<td  class='text-left'><?= $detail_user['tanggal_approval'] ?></td>
						<tr>
						<tr>
							<td class='text-left'>USER PEMESANAN</td>
							<td  class='text-left'><?= $detail_user['nama_user_finalisasi'] ?></td>
							<td  class='text-left'><?= $detail_user['tgl_finalisasi'] ?></td>
						<tr>
					</tbody>
				</table>
			</div>
			
			<div class="col-md-6">
				<h4 class="block-title">Lampiran</h4>
				<div class="block">
					<form action="{base_url}tgudang_penerimaan/upload_lampiran" enctype="multipart/form-data" class="dropzone" id="image-upload">
						<input type="hidden" class="form-control" name="idpenerimaan" id="idpenerimaan" value="{id}" readonly>
						<div>
						  <h5>setiap klik tombol Upload Files max 2 file yang di upload</h5>
						</div>
					  </form>
				</div>
				
				<div class="block">					
					 <table class="table table-bordered" id="list_lampiran">
						<thead>
							<tr>
								<th width="50%" class="text-center">File</th>
								<th width="15%" class="text-center">Size</th>
								<th width="35%" class="text-center">User</th>
								<th width="35%" class="text-center">X</th>
								
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
				
			</div>
			
		</div>
	</div>
</div>
<div class="modal" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title" id='title_barang'>Verifikasi Penerimaan BARANG</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal">
                        <input type="hidden" id="rowIndex">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Barang</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" readonly id="xnamabarang">
                                    </div>
									
                                </div>
                                <div id="div_harga" class="form-group div_harga" >
                                    <label class="col-md-3 control-label">Harga</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control decimal" id="xharga">
										<div id="val_harga" class="help-block animated fadeInDown"></div>
                                        										
                                    </div>
									<label class="col-md-2 control-label">Harga Master</label>
                                    <div class="col-md-3">
                                        <input type="text" readonly class="form-control decimal" id="xharga_master">
                                    </div>
                                </div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Diskon %.</label>
									<div class="col-md-9">
										<div class="input-group">
											<input class="form-control diskon" type="text" id="xdiskon_persen"  placeholder="Disc %">
											<span class="input-group-addon">Diskon Rp</span>
											<input class="form-control decimal" type="text" id="xdiskon_rp"  placeholder="Disc Rp">
										</div>
									</div>
								</div>
								<div class="form-group">
                                    <label class="col-md-3 control-label">Harga Before PPN</label>
                                    <div class="col-md-3">
                                        <input type="text" readonly class="form-control decimal" id="xharga_after_diskon">
										
                                    </div>
									
                                </div>
								<div class="form-group">
									<label class="col-md-3 control-label">PPN %.</label>
									<div class="col-md-9">
										<div class="input-group">
											<input type="text" class="form-control diskon" id="xppn">
											<span class="input-group-addon">PPN Rp</span>
											<input type="text" class="form-control decimal" readonly id="xppn_nominal">
										</div>
									</div>
								</div>
								
								
								<div class="form-group div_harga">
                                    <label class="col-md-3 control-label">Harga Final Rp.</label>
                                    <div class="col-md-3">
                                        <input type="text" readonly class="form-control decimal" id="xharga_after_ppn">										
                                    </div>									
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Kuantitas</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control number" id="xkuantitas">										
                                        <input type="hidden" readonly class="form-control number" id="xkuantitas_sisa">										
                                    </div>		
									<label class="col-md-2 control-label">Total Rp.</label>
                                    <div class="col-md-4">
                                        <input type="text" readonly class="form-control decimal" id="xharga_total_fix">
                                    </div>
                                </div>
                                
								<div class="form-group">
									<label class="col-md-3 control-label" for="example-datetimepicker4">Tgl Kadaluarsa</label>
									<div class="col-md-4">
										<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="js-datepicker form-control" type="text" id="tanggalkadaluarsa" name="tanggalkadaluarsa" placeholder="Choose a date..">
											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
										</div>
									</div>
								</div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">No Batch</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control detail" id="nobatch">
                                    </div>
                                </div>
                            </div>
                            
                                
                        </div>
                    </form>
                </div>
				 <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-sm btn-success" id="btn_update" type="submit">Update</button>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
    var table, t, tanggal;
	$(".decimal").number(true,2,'.',',');
	$(".number").number(true,0,'.',',');
	$(".diskon").number(true,2,'.',',');
	// $("#pembayaran").select2();
	$('#pembayaran').select2();
    $(document).ready(function() {
			refresh_image();
			Dropzone.autoDiscover = false;
  
			var myDropzone = new Dropzone(".dropzone", { 
			   autoProcessQueue: true,
			   maxFilesize: 30,
			});
			
			myDropzone.on("complete", function(file) {
			  
			  refresh_image();
			  myDropzone.removeFile(file);
			  
			});
			// $('#uploadFile').click(function(){
			   // myDropzone.processQueue();
			// });
			// $('#reload').click(function(){
			   // location.reload();
			// });
    });
	function refresh_image(){
		var id=$("#id").val();
		
		$.ajax({
			url: '{site_url}tgudang_penerimaan/refresh_image/'+id,
			dataType: "json",
			success: function(data) {
				if (data.detail!=''){
					$('#list_lampiran tbody').empty();
					$("#list_lampiran tbody").append(data.detail);
				}
				// console.log();
				
			}
		});
	}
	$("#pembayaran").change(function() {
		if ($(this).val()=='2'){
			if($("#tanggalpenerimaan").val()==''){
				sweetAlert("Maaf...", "Tanggal Penerimaan Harus diisi!", "error");
			}else{
				
				var tgl_terima=moment($("#tanggalpenerimaan").val(),'DD-MM-YYYY');
				$("#tanggaljatuhtempo").val(addDays(new Date(tgl_terima), 14));
			}
			// alert();			
		}
	});	
	$("#tanggalpenerimaan").change(function() {
		// alert('sini');
		if ($("#pembayaran").val()=='2'){
			var tgl_terima=moment($("#tanggalpenerimaan").val(),'DD-MM-YYYY');
			// alert(tgl_terima);
			// $("#tanggaljatuhtempo").val(addDays(new Date(tgl_terima), 14));
			
		}
	});	
	function addDays(theDate, days) {
		// alert(theDate);
		var today=new Date(theDate.getTime() + days*24*60*60*1000);
		// var today = new Date();
		var dd = today.getDate();

		var mm = today.getMonth()+1; 
		var yyyy = today.getFullYear();
		if(dd<10) 
		{
			dd='0'+dd;
		} 

		if(mm<10) 
		{
			mm='0'+mm;
		} 
		today = dd+'-'+mm+'-'+yyyy;
		return today;
	}
	
	$("#btn_update").click(function() {
			if (validate()){
				$("#modal_edit").modal("hide");				
				$('#data-barang tbody tr').each(function() {
					var tr = $(this).closest('tr');					
					var $cells = $(this).children('td');
					var satuan=tr.find("td:eq(32) input").val();
					if (tr[0].sectionRowIndex==$("#rowIndex").val()){
						tr.find("td:eq(23) input").val('1');//st_edit
						tr.find("td:eq(3)").text(formatNumber($("#xharga_after_ppn").val()));
						tr.find("td:eq(6)").text(formatNumber($("#xharga_total_fix").val()));
						tr.find("td:eq(4)").text(formatNumber($("#xdiskon_rp").val()));
						// alert($("xkuantitas").val()+' '+$("xkuantitas_sisa").val());
						if (parseFloat($("#xkuantitas").val()) < parseFloat($("#xkuantitas_sisa").val())){
							tr.find("td:eq(7)").html('<span class="label label-danger">Barang Sebagian</span>');
						}else{
							tr.find("td:eq(7)").html('<span class="label label-success">Semua Barang</span>');
						}
						tr.find("td:eq(5)").html('<span class="label label-info">'+formatNumber($("#xkuantitas").val())+' '+satuan+'</span>');
						tr.find("td:eq(11)").text($("#nobatch").val());
						tr.find("td:eq(22) input").val($("#nobatch").val());
						tr.find("td:eq(30) input").val($("#tanggalkadaluarsa").val());
						tr.find("td:eq(19) input").val($("#xdiskon_rp").val());
						tr.find("td:eq(16) input").val($("#xharga").val());
						tr.find("td:eq(17) input").val($("#xppn").val());
						tr.find("td:eq(18) input").val($("#xharga_after_ppn").val());
						tr.find("td:eq(19) input").val($("#xdiskon_rp").val());
						tr.find("td:eq(20) input").val($("#xharga_after_ppn").val());
						tr.find("td:eq(27) input").val($("#xkuantitas").val());
						tr.find("td:eq(28) input").val($("#xkuantitas_sisa").val());
						tr.find("td:eq(29) input").val($("#xharga_master").val());
						tr.find("td:eq(31) input").val($("#xkuantitas").val());
						tr.find("td:eq(21) input").val($("#xharga_total_fix").val());
						
						//baru
						tr.find("td:eq(34) input").val($("#xharga").val());//Harga Asli
						tr.find("td:eq(35) input").val($("#xdiskon_persen").val());//Diskon
						tr.find("td:eq(36) input").val($("#xdiskon_rp").val());//Diskon RP
						tr.find("td:eq(37) input").val($("#xharga_after_diskon").val());//Harga After Diskon
						tr.find("td:eq(38) input").val($("#xppn").val());//ppn
						tr.find("td:eq(39) input").val($("#xppn_nominal").val());//PPN_RP
						tr.find("td:eq(40) input").val($("#xharga_after_ppn").val());//Harga After PPN
						tr.find("td:eq(41) input").val($("#xharga_total_fix").val());//Harga After PPN * Kuantitas
						
					}
				});
				gen_total_uang();
		}
        
    });
    
	var table, tr;
	$(document).on("click",".edit",function(){
		 var tr = $(this).closest('tr');
		 // alert(tr.find('td:eq(0)').text());
		$('#xnamabarang').val(tr.find('td:eq(1)').text());
		$('#xharga').val(tr.find('td:eq(34) input').val());
		$('#xdiskon_persen').val(tr.find('td:eq(35) input').val());
		$('#xdiskon_rp').val(tr.find('td:eq(36) input').val());
		$('#xharga_after_diskon').val(tr.find('td:eq(37) input').val());
		$('#xppn').val(tr.find('td:eq(38) input').val());
		$('#xppn_nominal').val(tr.find('td:eq(39) input').val());
		$('#xharga_after_ppn').val(tr.find('td:eq(40) input').val());
		$('#xharga_total_fix').val(tr.find('td:eq(41) input').val());
		
		$('#xkuantitas').val(tr.find('td:eq(27) input').val());
		$('#xkuantitas_sisa').val(tr.find('td:eq(28) input').val());
		$('#xharga_master').val(tr.find('td:eq(29) input').val());
		$('#tanggalkadaluarsa').val(tr.find('td:eq(30) input').val());
		$('#nobatch').val(tr.find('td:eq(22) input').val());
		// $("#xppn_nominal").val(parseFloat($("#xharga").val())*parseFloat($('#xppn').val())/100);
		// var diskon_persen=parseFloat((parseFloat($('#xdiskon_rp').val()*100))/$("#xharga_after_ppn").val());
		// $("#xdiskon_persen").val(diskon_persen);
		
		$('#modal_edit').modal('show');
        $("#rowIndex").val(tr[0].sectionRowIndex);
		gen_harga();
		cek_harga();
	});
	
	$(document).on("click",".hapus",function(){
		$("#st_hapus_alih").val('1');
		$("#div_hapus").show();
		 var tr = $(this).closest('tr');
		 var content = "<tr>";
		 content += "<td class='text-center'>" + tr.find('td:eq(0)').text() + "</td>"; //0 Nomor
		 content += "<td  class='text-left'>" + tr.find('td:eq(1)').text() + "</td>"; //1 
		 content += "<td  class='text-center'>" + tr.find('td:eq(28) input').val()+' '+tr.find('td:eq(32) input').val()+ "</td>"; //2 
		 if (tr.find('td:eq(27) input').val() == tr.find('td:eq(28) input').val()){
			content += '<td  class="text-center"><span class="label label-danger">DIBATALKAN SEMUA</span></td>'; //2 
		 }else{
			content += '<td  class="text-center"><span class="label label-danger">DIBATALKAN SEBAGIAN</span></td>'; //2 
		 }
		 content += '<td ><select class="form-control e_dist" name="d_iddistributor[]" style="width:100%"><option value="#" selected>TIDAK PERLU DIISI</option></select></td>'; //3
		 content += '<td ><input type="text" class="form-control" name="d_alasan[]" value=""></td>'; //4
		 content += '<td hidden><input type="text" class="form-control" name="d_iddet[]" value="'+tr.find('td:eq(15) input').val()+'"></td>'; //5
		 
		 if (tr.find('td:eq(27) input').val() == tr.find('td:eq(28) input').val()){
			content += '<td hidden><input type="text" class="form-control" name="d_status[]" value="1"></td>'; //ALL
		 }else{
			 content += '<td hidden><input type="text" class="form-control" name="d_status[]" value="2"></td>'; //SEBAGIAN
		 }
		 content += '<td hidden><input type="text" class="form-control" name="d_kuantitas[]" value="'+ tr.find('td:eq(28) input').val()+'"></td>'; //5
		 content += "</tr>";
		 tr.remove();
		$('#data_hapus tbody').append(content);
		get_distributor();
		
	});
	$(document).on("click",".alih",function(){
		$("#st_hapus_alih").val('1');
		$("#div_hapus").show();
		 var tr = $(this).closest('tr');
		 var content = "<tr>";
		 content += "<td class='text-center'>" + tr.find('td:eq(0)').text() + "</td>"; //0 Nomor
		 content += "<td  class='text-left'>" + tr.find('td:eq(1)').text() + "</td>"; //1 
		 content += "<td  class='text-center'>" + tr.find('td:eq(28) input').val()+' '+tr.find('td:eq(32) input').val()+ "</td>"; //2 
		 content += '<td  class="text-center"><span class="label label-warning">DIALIHKAN</span></td>'; //2 
		 content += '<td ><select class="form-control e_dist" name="d_iddistributor[]" style="width:100%"><option value="#" selected>-Silahkan Pilih-</option></select></td>'; //3
		 content += '<td ><input type="text" class="form-control" name="d_alasan[]" value=""></td>'; //4
		 content += '<td hidden><input type="text" class="form-control" name="d_iddet[]" value="'+tr.find('td:eq(15) input').val()+'"></td>'; //5
		 content += '<td hidden><input type="text" class="form-control" name="d_status[]" value="3"></td>'; //5
		 content += '<td hidden><input type="text" class="form-control" name="d_kuantitas[]" value="'+ tr.find('td:eq(28) input').val()+'"></td>'; //5
		 content += "</tr>";
		
		tr.remove();
		$('#data_hapus tbody').append(content);
		get_distributor();
		
	});
	function get_distributor() {
        // get nopemesanan
       $(".e_dist").select2({
			minimumInputLength: 2,
			noResults: 'Distributor Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}tgudang_penerimaan/get_distributor/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,

			 data: function (params) {
				  var query = {
					search: params.term,
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.nama,
								id: item.id,
							}
						})
					};
				}
			}
		});
    }
	function gen_total_uang(){
			var tot=0;
			var tot_barang=0;
		$('#data-barang tbody tr').each(function() {
			var tr = $(this).closest('tr');	
			tot=tot+ parseFloat(tr.find("td:eq(21) input").val());
			tot_barang=tot_barang+ parseFloat(tr.find("td:eq(31) input").val());
			 // console.log(tr.find("td:eq(21) input").val());
			
		});
		$("#totalharga").val(tot);
		$("#totalbarang").val(tot_barang);
	}
	function formatNumber (num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}
    
	

    function grandTotal() {
        var totalbarang = 0;
        var totalharga = 0;
        $('#data-barang tbody tr').each(function() {
            totalbarang += parseFloat($(this).find('td:eq(6)').text().replace(/\,/g, ""));
            totalharga += parseFloat($(this).find('td:eq(7)').text().replace(/\,/g, ""));
        });

        $("#totalbarang").val(isNaN(totalbarang) ? 0 : totalbarang)
        $("#totalharga").val(isNaN(totalharga) ? 0 : totalharga)
    }

    function validateHead() {
        if ($('#nopemesanan').val() == '') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'No Pemesanan tidak boleh kosong'
            });
            return false;
        }
        if ($('#tanggalpenerimaan').val() == '') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Tanggal Penerimaan tidak boleh kosong'
            });
            return false;
        }
        if ($('#tanggaljatuhtempo').val() == '') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Tanggal Jatuh Tempo tidak boleh kosong'
            });
            return false;
        }
        if ($('#totalharga').val() == '' || $('#totalharga').val() <= 0) {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Data belum lengkap. Total terima barang masih 0'
            });
            return false;
        }
        return true;
    }

    function validate() {
        if ($('#xharga_total_fix').val() == 0 && $('#xdiskon_rp').val() == 0) {
            sweetAlert("Maaf...", "Harga Dan kuantitas harus diisi!", "error");
            return false;
        }
        
        if ($('#tanggalkadaluarsa').val() == '') {
            sweetAlert("Maaf...", "Tanggal Kadaluarsa harus diisi!", "error");
            return false;
        }
        if ($('#nobatch').val() == '' || $('#nobatch').val() == '-') {
            sweetAlert("Maaf...", "No Batch Harus diisi!", "error");
            return false;
        }
        return true;
    }

    
	$("#xharga").keyup(function(){
		if ($("#xharga").val()==''){
			$("#xharga").val(0)
		}
		
		var diskon_rp=parseFloat($("#xharga").val() * parseFloat($("#xdiskon_persen").val())/100);
		$("#xdiskon_rp").val(diskon_rp);
		$("#xharga_after_diskon").val(parseFloat($("#xharga").val()-diskon_rp));
		var ppn=$("#xppn").val();
		$("#xppn_nominal").val(parseFloat($("#xharga_after_diskon").val())*ppn/100);
		
		gen_harga();
		cek_harga();
	});
	$("#xdiskon_rp").keyup(function(){
		if ($("#xdiskon_rp").val()==''){
			$("#xdiskon_rp").val(0)
		}
		
		if (parseFloat($(this).val()) > $("#xharga").val()){
			$(this).val($("#xharga").val());
		}
		var diskon_persen=parseFloat((parseFloat($(this).val()*100))/$("#xharga").val());
		$("#xdiskon_persen").val(diskon_persen);
		$("#xharga_after_diskon").val($("#xharga").val()-$(this).val());
		var ppn=$("#xppn").val();
		$("#xppn_nominal").val(parseFloat($("#xharga_after_diskon").val())*ppn/100);
		gen_harga();
		cek_harga();
	});
	$("#xdiskon_persen").keyup(function(){
		if ($("#xdiskon_persen").val()==''){
			$("#xdiskon_persen").val(0)
		}
		if (parseFloat($(this).val()) > 100){
			$(this).val(100);
		}
		var diskon_rp=parseFloat($("#xharga").val() * parseFloat($(this).val())/100);
		$("#xdiskon_rp").val(diskon_rp);
		$("#xharga_after_diskon").val($("#xharga").val()-diskon_rp);
		
		var ppn=$("#xppn").val();
		$("#xppn_nominal").val(parseFloat($("#xharga_after_diskon").val())*ppn/100);
		gen_harga();
		cek_harga();
	});
	$("#xkuantitas").keyup(function(){
		if (parseFloat($("#xkuantitas").val()) > parseFloat($("#xkuantitas_sisa").val())){
			sweetAlert("Maaf...", "Kuantitas Lebih Besar Dari Sisa", "error");
			$("#xkuantitas").val($("#xkuantitas_sisa").val());			
		}		
		gen_harga();
	});
	function gen_harga(){
		var harga_berfore_ppn=parseFloat($("#xharga").val()) - parseFloat($("#xdiskon_rp").val());
		$("#xharga_after_diskon").val(harga_berfore_ppn);
		var harga_final=parseFloat($("#xharga_after_diskon").val()) + parseFloat($("#xppn_nominal").val());
		console.log(harga_final);
		$("#xharga_after_ppn").val(harga_final);
		var xharga_total_fix=parseFloat($("#xkuantitas").val()) * harga_final;
		$("#xharga_total_fix").val(xharga_total_fix);
	}
	$("#xppn").keyup(function(){
		if ($("#xppn").val()>100){
			$("#xppn").val(100)
		}
		if ($("#xppn").val()==''){
			$("#xppn").val(0)
		}
		var ppn=$("#xppn").val();
		// var harppn=parseFloat($("#xharga_after_diskon").val()) - (parseFloat($("#xharga_after_diskon").val())*ppn/100);
		$("#xppn_nominal").val(parseFloat($("#xharga_after_diskon").val())*ppn/100);
		// $("#xharga_after_diskon").val(harppn);
		
		gen_harga();
		cek_harga();
	});
	function cek_harga(){
		var selisih;
		var harppn=$("#xharga_after_ppn").val();
		$('.div_harga').removeClass('has-error');			
		$('.div_harga').removeClass('has-success');	
		if (parseFloat(harppn) != $("#xharga_master").val()){
			if (parseFloat(harppn) > $("#xharga_master").val()){
				selisih =  parseFloat(harppn) - parseFloat($("#xharga_master").val());
						
				$('.div_harga').addClass('has-error');
				$('#val_harga').text('Harga Lebih Mahal Rp. '+ formatNumber(selisih));
			}else{
				selisih =  parseFloat($("#xharga_master").val()) - parseFloat(harppn) ;
				
				$('.div_harga').addClass('has-success');
				$('#val_harga').text('Harga Lebih Murah Rp. '+formatNumber(selisih));
			}
			
		}else{
			$('#val_harga').text('');
		}
		
		gen_harga();
	}
    function validate_header() {
		// alert('Validasi');
		var rowCount = $('#data-barang tr').length;
		var rowCountHapus = $('#data_hapus tr').length;
		// alert(rowCount);
        if (parseFloat($('#totalbarang').val()) == 0 && rowCount > 1) {
			if (rowCountHapus <= 1){				
				sweetAlert("Maaf...", "Silhakan Pilih barang Yang mau diverifikasi!", "error");
				return false;
			}
        }
        
        if ($('#nofakturexternal').val() == '') {
            sweetAlert("Maaf...", "No Faktur harus diisi!", "error");
            return false;
        }
		if ($('#tanggalpenerimaan').val() == '') {
            sweetAlert("Maaf...", "Tanggal Harus di isi!", "error");
            return false;
        }
        if ($('#tanggaljatuhtempo').val() == '' && $("#pembayaran").val()=='2') {
            sweetAlert("Maaf...", "Tanggal Jatuh Tempo Harus di isi!", "error");
            return false;
        }
		var ada_kosong_dist='1';
		var ada_kosong_dist_alasan='1';
		var ada_kosong_hapus='1';
		if (rowCountHapus>1){
			$('#data_hapus tbody tr').each(function() {
				var tr = $(this).closest('tr');	
				 // alert(tr.find("td:eq(13) input").val());
				 console.log('DIST'+tr.find("td:eq(4) select").val());
				if (tr.find("td:eq(7) input").val()=='3' && tr.find("td:eq(4) select").val()=='#'){
					ada_kosong_dist='0';
				}
				if (tr.find("td:eq(7) input").val()=='3' && tr.find("td:eq(5) input").val()==''){
					ada_kosong_dist_alasan='0';
				}
				if (tr.find("td:eq(7) input").val()=='1' && tr.find("td:eq(5) input").val()==''){
					ada_kosong_hapus='0';					
				}
				if (tr.find("td:eq(7) input").val()=='2' && tr.find("td:eq(5) input").val()==''){
					ada_kosong_hapus='0';					
				}
				console.log(tr.find("td:eq(7) input").val()+'-'+tr.find("td:eq(5) input").val());
				
			});
			console.log(ada_kosong_dist+';'+ada_kosong_dist_alasan+';'+ada_kosong_hapus);
			if (ada_kosong_dist=='0'){
				sweetAlert("Maaf...", "Harus diisi Distributor!", "error");
				return false;
			}
			if (ada_kosong_dist_alasan=='0'){
				sweetAlert("Maaf...", "Harus diisi Alasan Ganti Distributor!", "error");
				return false;
			}
			if (ada_kosong_hapus=='0'){
				sweetAlert("Maaf...", "Harus diisi Alasan Hapus!", "error");
				return false;
			}
		}
        return true;
    }
	$(document).on("click","#btn_back",function(){
		window.history.back()
	});
	$(document).on("change","#tanggaljatuhtempo",function(){	
		var tgl_edit_kbo=$("#tanggaljatuhtempo").val();
		cek_tanggal(tgl_edit_kbo);		
	});
	function cek_tanggal($tgl_edit_kbo){
		$.ajax({
			url: '{site_url}tkontrabon/cek_tanggal',
			type: 'POST',
			data: {tgl_edit_kbo:$tgl_edit_kbo},
			success : function(data) {
				if (data=='error'){
					sweetAlert("Maaf...", "Tanggal Sudah ada Kontrabon!", "error");
					$("#btn_update_tanggal").attr("disabled",true);
					$(".simpan-penerimaan2").attr("disabled",true);
					$(".simpan-penerimaan").attr("disabled",true);
					return false;
				}else{
					$("#btn_update_tanggal").attr("disabled",false);
					$(".simpan-penerimaan2").attr("disabled",false);
					$(".simpan-penerimaan").attr("disabled",false);
					return true;
				}
				
			}
		});
	}
	$(document).on("click","#btn_update_tanggal",function(){
		var tgl_baru=$("#tanggaljatuhtempo").val();
		var tgl_awal=$("#tgl_awal").val();
		if (tgl_baru != tgl_awal){
			swal({
				title: "Anda Yakin ?",
				text : "Akan Memindahkan Tanggal Kontrabon ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				$("#cover-spin").show();
				// edit_tanggal();
				pindah_kontrabon();
			});
		// alert(tgl_baru);
		}
	});
	function pindah_kontrabon(){
		var tkontrabon_id=$("#tkontrabon_id").val();
		var id_header_kontrabon=$("#id_header_kontrabon").val();
		var tgl_baru=$("#tanggaljatuhtempo").val();
		var id=$("#id").val();
		$.ajax({
			url: '{site_url}tgudang_penerimaan/edit_tanggal',
			type: 'POST',
			data: {id: id,tgl_baru:tgl_baru,tkontrabon_id:tkontrabon_id},
			complete: function() {
				// window.location.replace("");
				// window.location.href = "<?php echo site_url('tgudang_penerimaan/edit/"+id+"'); ?>";
				window.history.back()
				// location.reload();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Memindahkan Tanggal Kontrabon'});
				// $("#cover-spin").hide();
				// table.ajax.reload( null, false ); 
			}
		});
		
		// alert(id+' '+tgl_baru);
		
	}
	$(document).on("click",".hapus_file",function(){	
		var currentRow=$(this).closest("tr");          
         var id=currentRow.find("td:eq(4)").text();
		$.ajax({
			url: '{site_url}tgudang_penerimaan/hapus_file',
			type: 'POST',
			data: {id: id},
			success : function(data) {				
				$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
				currentRow.remove();
				// table.ajax.reload( null, false );				
			}
		});
	});
	// $(".simpan-penerimaan").click(function() {
		// alert('Simpan');
		 // if (validate_header()){
			// $("#form-work").submit();
		// }
		
		// return false;
	// });
	// $(".simpan-penerimaan2").click(function() {
		// alert('Simpan');
		 // if (validate_header()){
			// $("#form-work").submit();
		// }
		
		// return false;
	// });
	
</script>