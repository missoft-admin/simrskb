<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>

    <?php echo form_open('tgudang_penerimaan/save', 'class="form-horizontal push-10-t" id="form-work"') ?>
    <div class="block-content block-content-narrow">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-3 control-label">No Pemesanan</label>
                    <div class="col-md-9">
                        <select name="nopemesanan" id="nopemesanan" class="js-select2 form-control" style="width: 100%;"></select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">No Faktur</label>
                    <div class="col-md-9">
                        <input class="form-control" type="text" name="nofakturexternal" id="nofakturexternal" value="">
                        <input class="form-control" type="hidden" name="st_hapus_alih" id="st_hapus_alih" value="0">
                    </div>
                </div>
				
                <div class="form-group">
					<label class="col-md-3 control-label" for="example-datetimepicker4">Tanggal Penerimaan</label>
					<div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
                            <input class="form-control" type="text" id="tanggalpenerimaan" name="tanggalpenerimaan" placeholder="Terima" value="{tanggalpenerimaan}"/>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
					</div>
					
				</div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Keterangan</label>
                    <div class="col-md-9">
                        <textarea class="form-control" name="keterangan" id="keterangan"></textarea>
                    </div>
                </div>
                
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-3 control-label">Total Barang</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control number" name="totalbarang" id="totalbarang" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Total Harga</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control number" name="totalharga" id="totalharga" readonly>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-3 control-label">Pembayaran</label>
                    <div class="col-md-8">
                        <select name="pembayaran" id="pembayaran" class="form-control">
                            <option value="1">Tunai</option>
                            <option value="2" selected>Kredit</option>
                        </select>
                    </div>
                </div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="example-datetimepicker4">Tanggal Jatuh Tempo</label>
					<div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
							<input class="js-datepicker form-control" type="text" id="tanggaljatuhtempo" name="tanggaljatuhtempo" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">

							<span class="input-group-addon">
								<span class="fa fa-calendar"></span>
							</span>
						</div>
					</div>
				</div>
            </div>
            <div class="clearfix"></div>
            <hr />
			<div class="col-md-12" >
				
				<table class="table table-striped table-borderless table-header-bg" id="data-barang">
					<thead>
						<tr>
							<th width="5%" class="text-center">Tipe</th>
							<th width="12%" class="text-center">Barang</th>
							<th width="8%" class="text-center">Jumlah Pesan</th>
							<th width="8%" class="text-center">Harga + PPN</th>
							<th width="8%" class="text-center">Diskon</th>
							<th width="5%" class="text-center">Sisa</th>
							<th width="5%" class="text-center">Sudah Terima</th>
							<th width="8%" class="text-center">Jumlah Terima</th>
							<th width="8%" class="text-center">Total</th>
							<th width="10%" class="text-center">Keterangan</th>
							<th width="10%" class="text-center">No.Batch</th>
							<th width="15%" class="text-center">Aksi</th>
							
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
			<br>
			<br>
			<br>
			<hr />
			<div class="col-md-12" id="div_hapus" hidden>
				<h3 class="block-title">DATA HAPUS & ALIHKAN DISTRIBUTOR</h3>
				<table class="table table-striped table-borderless table-header-bg" id="data_hapus">
					<thead>
						<tr>
							<th width="5%" class="text-center">Tipe</th>
							<th width="15%" class="text-center">Barang</th>
							<th width="5%" class="text-center">Jumlah</th>
							<th width="15%" class="text-center">Keterangan</th>
							<th width="15%" class="text-center">Distributor</th>
							<th width="20%" class="text-center">Alasan</th>
							<th width="5%" class="text-center">Batalkan</th>
							
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
        </div>
        <div style="margin-top:50px;margin-bottom:20px;" class="text-right">
            <a href="{site_url}tgudang_penerimaan" class="btn btn-default btn-md">Kembali</a>
            <button class="btn btn-sm btn-success" type="submit" id="simpan-penerimaan">Simpan</button>
        </div>
		<div class="row">
		<div class="col-md-6">
			<h4 class="block-title">Info User</h4>
			<table class="table table-bordered" id="data_user">
				<thead>
					<tr>
						<th width="5%" class="text-left">Jenis</th>
						<th width="15%" class="text-left">Nama User</th>
						<th width="5%" class="text-left">Tanggal</th>
						
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		</div>
    </div>
    <?php echo form_close() ?>
</div>
<div class="modal" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title" id='title_barang'>Verifikasi Penerimaan BARANG</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal">
                        <input type="hidden" id="rowIndex">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Barang</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" readonly id="xnamabarang">
                                    </div>
									
                                </div>
                                <div id="div_harga" class="form-group" >
                                    <label class="col-md-3 control-label">Harga</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control decimal" id="xharga">
										<div id="val_harga" class="help-block animated fadeInDown"></div>
                                        <input type="hidden" class="form-control decimal" id="xharga_master">										
                                    </div>
									<label class="col-md-2 control-label">PPN %</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control diskon" id="xppn">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="col-md-3 control-label">Harga + PPN</label>
                                    <div class="col-md-3">
                                        <input type="text" readonly class="form-control decimal" id="xharga_plus_ppn">
										
                                    </div>
									<label class="col-md-2 control-label">PPN Rp.</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control decimal" readonly id="xppn_nominal">
                                    </div>
                                </div>
								<div class="form-group">
									<label class="col-md-3 control-label">Diskon Rp.</label>
									<div class="col-md-9">
										<div class="input-group">
											<input class="form-control decimal" type="text" id="xdiskon_rp"  placeholder="Disc Rp">
											<span class="input-group-addon">Diskon %</span>
											<input class="form-control diskon" type="text" id="xdiskon_persen"  placeholder="Disc %">
										</div>
									</div>
								</div>
								<div class="form-group">
                                    <label class="col-md-3 control-label">Harga Final Rp.</label>
                                    <div class="col-md-3">
                                        <input type="text" readonly class="form-control decimal" id="xharga_plus_diskon">										
                                    </div>									
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Kuantitas</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control number" id="xkuantitas">										
                                        <input type="hidden" readonly class="form-control number" id="xkuantitas_sisa">										
                                    </div>		
									<label class="col-md-2 control-label">Total Rp.</label>
                                    <div class="col-md-4">
                                        <input type="text" readonly class="form-control decimal" id="xharga_total_fix">
                                    </div>
                                </div>
                                
								<div class="form-group">
									<label class="col-md-3 control-label" for="example-datetimepicker4">Tgl Kadaluarsa</label>
									<div class="col-md-4">
										<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="js-datepicker form-control" type="text" id="tanggalkadaluarsa" name="tanggalkadaluarsa" placeholder="Choose a date..">
											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
										</div>
									</div>
								</div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">No Batch</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control detail" id="nobatch">
                                    </div>
                                </div>
                            </div>
                            
                                
                        </div>
                    </form>
                </div>
				 <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-sm btn-success" id="btn_update" type="submit">Update</button>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
    var table, t, tanggal;
	$(".decimal").number(true,2,'.',',');
	$(".number").number(true,0,'.',',');
	$(".diskon").number(true,2,'.',',');
	// $("#pembayaran").select2();
	$('#pembayaran').select2();
	$(".e_dist").select2();
    $(document).ready(function() {
        // tanggal = moment().format("DD-MM-YYYY");
		


        noPemesanan();
		$("#pembayaran").val(2).trigger('change');
        // $('#simpan-detail, .detail').attr('disabled', true);
        // $('.detail, #totalharga, #xsisa, #totalbarang, #tanggalpenerimaan, #tanggaljatuhtempo').val('');
        // $('#tanggalpenerimaan').val(tanggal);
        // $('#tanggaljatuhtempo').val(tanggal);

        $('#nopemesanan').change(nopemesananChange);
        // $('#simpan-penerimaan').click(simpanPenerimaanClick);
    });
	$("#pembayaran").change(function() {
		// alert($("#tanggalpenerimaan").val());
		if ($(this).val()=='2'){
			if($("#tanggalpenerimaan").val()==''){
				sweetAlert("Maaf...", "Tanggal Penerimaan Harus diisi!", "error");
			}else{
				var tgl_terima=moment($("#tanggalpenerimaan").val(),'DD-MM-YYYY');
				$("#tanggaljatuhtempo").val(addDays(new Date(tgl_terima), 14));
			}
			// alert();			
		}
	});	
	$("#tanggalpenerimaan").change(function() {
		if ($("#pembayaran").val()=='2'){
			var tgl_terima=moment($("#tanggalpenerimaan").val(),'DD-MM-YYYY');
			// alert(tgl_terima);
			$("#tanggaljatuhtempo").val(addDays(new Date(tgl_terima), 14));
			
		}
	});	
	function addDays(theDate, days) {
		// alert(theDate);
		var today=new Date(theDate.getTime() + days*24*60*60*1000);
		// var today = new Date();
		var dd = today.getDate();

		var mm = today.getMonth()+1; 
		var yyyy = today.getFullYear();
		if(dd<10) 
		{
			dd='0'+dd;
		} 

		if(mm<10) 
		{
			mm='0'+mm;
		} 
		today = dd+'-'+mm+'-'+yyyy;
		return today;
	}
	
	$("#btn_update").click(function() {
			if (validate()){
				$("#modal_edit").modal("hide");				
				$('#data-barang tbody tr').each(function() {
					var tr = $(this).closest('tr');					
					var $cells = $(this).children('td');
					var satuan=tr.find("td:eq(32) input").val();
					if (tr[0].sectionRowIndex==$("#rowIndex").val()){
						tr.find("td:eq(23) input").val('1');//st_edit
						tr.find("td:eq(3)").text(formatNumber($("#xharga_plus_ppn").val()));
						// tr.find("td:eq(8)").text(formatNumber($("#xkuantitas").val()));
						tr.find("td:eq(9)").text(formatNumber($("#xharga_total_fix").val()));
						tr.find("td:eq(4)").text(formatNumber($("#xdiskon_rp").val()));
						// alert($("xkuantitas").val()+' '+$("xkuantitas_sisa").val());
						if (parseFloat($("#xkuantitas").val()) < parseFloat(tr.find("td:eq(28) input").val())){
							tr.find("td:eq(10)").html('<span class="label label-danger">Barang Sebagian</span>');
						}else{
							tr.find("td:eq(10)").html('<span class="label label-success">Semua Barang</span>');
						}
						if (parseFloat($("#xkuantitas").val())==0){
							tr.find("td:eq(10)").html('-');
						}
						
						var sisa=parseFloat(tr.find("td:eq(28) input").val())-parseFloat(tr.find("td:eq(31) input").val());
						tr.find("td:eq(8)").html('<span class="label label-info">'+formatNumber($("#xkuantitas").val())+' '+satuan+'</span>');
						tr.find("td:eq(11)").text($("#nobatch").val());
						tr.find("td:eq(22) input").val($("#nobatch").val());
						tr.find("td:eq(30) input").val($("#tanggalkadaluarsa").val());
						tr.find("td:eq(19) input").val($("#xdiskon_rp").val());
						tr.find("td:eq(16) input").val($("#xharga").val());
						tr.find("td:eq(17) input").val($("#xppn").val());
						tr.find("td:eq(18) input").val($("#xharga_plus_ppn").val());
						tr.find("td:eq(19) input").val($("#xdiskon_rp").val());
						tr.find("td:eq(20) input").val($("#xharga_plus_diskon").val());
						tr.find("td:eq(27) input").val($("#xkuantitas").val());
						// tr.find("td:eq(28) input").val($("#xkuantitas_sisa").val());
						tr.find("td:eq(29) input").val($("#xharga_master").val());
						tr.find("td:eq(31) input").val($("#xkuantitas").val());
						tr.find("td:eq(21) input").val($("#xharga_total_fix").val());
						
					}
				});
				gen_total_uang();
				show_button($("#rowIndex").val());
		}
        
    });
    function nopemesananChange() {

        var n = $(this).val();
        if ($(this).val()!=''){
			$("#totalbarang").val(0);
			$("#totalharga").val(0);
			$("#keterangan").val('');
			$("#data-barang tbody").empty();
			$("#data_user tbody").empty();
			$.ajax({
			url: '{site_url}tgudang_penerimaan/find_detail/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				$.each(data, function (i,r) {
				var content = "<tr>";
					content += "<td class='text-center'>" + r.namatipe + "</td>"; //0 Nomor
					content += "<td  class='text-left'>" + r.namabarang + "</td>"; //1 
					content += "<td  class='text-center'>" + r.kuantitas + ' '+r.satuan+ ' / ' + r.jenissatuan+ "</td>"; //2 
					content += "<td class='text-right'>" + formatNumber(r.harga_master) + "</td>"; //3
					content += "<td class='text-right'>0</td>"; //4
					content += "<td >"+r.sisa+ ' '+r.satuan+"</td>"; //5 
					content += "<td class='text-center'>" + formatNumber(r.sudah_terima) + ' '+r.satuan+"</td>"; //6
					content += "<td hidden></td>"; //7
					content += "<td class='text-right'>0</td>"; //8
					content += "<td class='text-right'>" + formatNumber(r.harga_master * r.sisa) + "</td>"; //9
					var status='';
					if (r.sudah_terima>0 && r.sisa>0){
						status='<span class="label label-danger">Baru Sebagian</span>';
					}else{
						status='-';
					}
					content += "<td class='text-center'>"+status+"</td>"; //10
					content += "<td class='text-center'>-</td>"; //10
					
					
					if (r.sisa>0){
					content += '<td class="text-center"><button class="btn btn-xs btn-success edit"  type="button" title="Terima Barang"><i class="fa fa-pencil"></i></button> ';
					 <?php if (UserAccesForm($user_acces_form,array('1140'))){ ?>
					content += '<button class="btn btn-xs btn-danger hapus"  type="button" title="Hapus Barang"><i class="glyphicon glyphicon-remove"></i></button> ';
					 <?}?>
					  <?php if (UserAccesForm($user_acces_form,array('1141'))){ ?>
					content += '<button class="btn btn-xs btn-warning alih"  type="button" title="Alihkan"><i class="si si-shuffle"></i> Alihkan</button></td>';//12
					  <?}?>
					}else{
						content += '<td class="text-center"><span class="label label-success">Selesai</span></td>'; //12 
					}
					content += '<td hidden><input type="text" class="form-control" name="e_idtipe[]" value="'+r.idtipe+'"></td>'; //13
					content += '<td hidden><input type="text" class="form-control" name="e_idbarang[]" value="'+r.idbarang+'"></td>'; //14
					content += '<td hidden><input type="text" class="form-control" name="e_iddet[]" value="'+r.id+'"></td>'; //15
					content += '<td hidden><input type="text" class="form-control" name="e_harga[]" value="'+r.harga_master+'"></td>'; //16
					content += '<td hidden><input type="text" class="form-control" name="e_ppn[]" value="0"></td>'; //17 
					content += '<td hidden><input type="text" class="form-control" name="e_harga_plus_ppn[]" value="'+r.harga_master+'"></td>'; //18
					content += '<td hidden><input type="text" class="form-control" name="e_harga_diskon[]" value="0"></td>'; //19
					content += '<td hidden><input type="text" class="form-control" name="e_harga_plus_diskon[]" value="'+r.harga_master+'"></td>'; //20
					content += '<td hidden><input type="text" class="form-control" name="e_harga_total_fix[]" value="0"></td>'; //21 
					content += '<td hidden><input type="text" class="form-control" name="e_no_batch[]" value=""></td>'; //22 
					content += '<td hidden><input type="text" class="form-control" name="e_st_edit[]" value=""></td>'; //23 
					content += '<td hidden><input type="text" class="form-control" name="e_st_hapus[]" value=""></td>'; //24 
					content += '<td hidden><input type="text" class="form-control" name="e_st_alih[]" value=""></td>'; //25 
					content += '<td hidden><input type="text" class="form-control" name="e_total_alih[]" value=""></td>'; //26 
					content += '<td hidden><input type="text"  name="e_kuantitas[]" value="'+r.sisa+'"></td>'; //27 
					content += '<td hidden><input type="text"  name="e_kuantitas_sisa[]" value="'+r.sisa+'"></td>'; //28
					content += '<td hidden><input type="text" class="form-control" name="e_harga_master[]" value="'+r.harga_master+'"></td>'; //29
					content += '<td hidden><input type="text" class="form-control" name="e_tgl_kadaluarsa[]" value=""></td>'; //30
					content += '<td hidden><input type="text" name="e_jml_Terima[]" value="0"></td>'; //31
					content += '<td hidden><input type="text" class="form-control" name="e_satuan[]" value="'+r.satuan+'"></td>'; //32
					content += '<td hidden><input type="text" class="form-control" name="e_opsisatuan[]" value="'+r.opsisatuan+'"></td>'; //33
					console.log(r);
				
				content += "</tr>";
				$('#data-barang tbody').append(content);
				});
			}
			});
			
			$.ajax({
			url: '{site_url}tgudang_penerimaan/find_detail_user/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				
				var content = "<tr>";
					content += "<td class='text-left'>USER PEMBUAT</td>"; //0 Nomor
					content += "<td  class='text-left'>" + data.userpemesanan + "</td>"; //1 
					content += "<td  class='text-left'>" + data.tgl_pemesanan + "</td>"; //1 
					content += "</tr>";
					content += "<tr>"; 
					content += "<td class='text-left'>USER KONFIRMASI</td>"; 
					content += "<td  class='text-left'>" + data.nama_user_konfirmasi + "</td>"; 
					content += "<td  class='text-left'>" + data.tanggal_konfirmasi + "</td>"; 
					content += "</tr>";
					content += "<tr>"; 
					content += "<td class='text-left'>USER BAG. KEUANGAN</td>"; 
					content += "<td  class='text-left'>" + data.nama_user_approval + "</td>"; 
					content += "<td  class='text-left'>" + data.tanggal_approval + "</td>"; 
					content += "</tr>";
					content += "<tr>"; 
					content += "<td class='text-left'>USER PEMESANAN</td>"; 
					content += "<td  class='text-left'>" + data.nama_user_finalisasi + "</td>"; 
					content += "<td  class='text-left'>" + data.tgl_finalisasi + "</td>"; 
					content += "</tr>";
				$('#data_user tbody').append(content);
				
			}
			});
			// find_detail_user
		}

        // $('#simpan-detail').click(simpanDetailClick);
    }
	var table, tr;
	
	$(document).on("click",".hapus",function(){
		
		$("#div_hapus").show();
		 var tr = $(this).closest('tr');
		 var jml=parseFloat(tr.find('td:eq(28) input').val())-parseFloat(tr.find('td:eq(31) input').val());
		 if (cek_duplikasi(tr[0].sectionRowIndex)==false){
			 return false;
		 }
		 var content = "<tr>";
		 content += "<td class='text-center'>" + tr.find('td:eq(0)').text() + "</td>"; //0 Nomor
		 content += "<td  class='text-left'>" + tr.find('td:eq(1)').text() + "</td>"; //1 
		 content += "<td  class='text-center'>" + jml +' '+tr.find('td:eq(32) input').val()+ "</td>"; //2 
		 content += '<td  class="text-center"><span class="label label-danger">DIBATALKAN</span></td>'; //2 
		 
		 content += '<td ><select class="form-control e_dist" name="d_iddistributor[]" style="width:100%"><option value="#" selected>TIDAK PERLU DIISI</option></select></td>'; //3
		 content += '<td ><input type="text" class="form-control" name="d_alasan[]" value=""></td>'; //4
		 content += '<td hidden><input type="text" class="form-control" name="d_iddet[]" value="'+tr.find('td:eq(15) input').val()+'"></td>'; //5
		 
		
		content += '<td hidden><input type="text" class="form-control" name="d_status[]" value="1"></td>'; //SEBAGIAN
		 
		 content += '<td hidden><input type="text" class="form-control" name="d_kuantitas[]" value="'+ jml +'"></td>'; //5
		 content += '<td hidden><input type="text" class="form-control" name="d_index_row[]" value="'+ tr[0].sectionRowIndex +'"></td>'; //6
		 content += '<td ><button class="btn btn-xs btn-danger remove"  type="button" title="Batalkan Aksi"><i class="fa fa-trash-o"></i></button></td>'; //6
		 content += "</tr>";
		 // tr.remove();
		$('#data_hapus tbody').append(content);
		show_button(tr[0].sectionRowIndex);
		get_distributor();
		
	});
	$(document).on("click",".remove",function(){
		var row='';
		 var tr = $(this).closest('tr');
		 row=tr.find('td:eq(9) input').val();
		 tr.remove();
		 show_button(row);
		
	});
	
	$(document).on("click",".alih",function(){
		$("#st_hapus_alih").val('1');
		$("#div_hapus").show();
		 var tr = $(this).closest('tr');
		 var jml=parseFloat(tr.find('td:eq(28) input').val())-parseFloat(tr.find('td:eq(31) input').val());
		 if (cek_duplikasi(tr[0].sectionRowIndex)==false){
			 return false;
		 }
		 var content = "<tr>";
		 content += "<td class='text-center'>" + tr.find('td:eq(0)').text() + "</td>"; //0 Nomor
		 content += "<td  class='text-left'>" + tr.find('td:eq(1)').text() + "</td>"; //1 
		 content += "<td  class='text-center'>" + jml +' '+tr.find('td:eq(32) input').val()+ "</td>"; //2 
		 content += '<td  class="text-center"><span class="label label-warning">DIALIHKAN</span></td>'; //3 
		 content += '<td ><select class="form-control e_dist" name="d_iddistributor[]" style="width:100%"><option value="#" selected>-Silahkan Pilih-</option></select></td>'; //3
		 content += '<td ><input type="text" class="form-control" name="d_alasan[]" value=""></td>'; //5
		 content += '<td hidden><input type="text" class="form-control" name="d_iddet[]" value="'+tr.find('td:eq(15) input').val()+'"></td>'; //5
		 content += '<td hidden><input type="text" class="form-control" name="d_status[]" value="3"></td>'; //7
		 content += '<td hidden><input type="text" class="form-control" name="d_kuantitas[]" value="'+ jml +'"></td>'; //8
		 content += '<td hidden><input type="text" class="form-control" name="d_index_row[]" value="'+ tr[0].sectionRowIndex +'"></td>'; //9
		 content += '<td ><button class="btn btn-xs btn-danger remove"  type="button" title="Batalkan Aksi"><i class="fa fa-trash-o"></i></button></td>'; //6
		 content += "</tr>";
		
		// tr.remove();
		$('#data_hapus tbody').append(content);
		show_button(tr[0].sectionRowIndex);
		get_distributor();
		
	});
	
	$(document).on("click",".edit",function(){
		 var tr = $(this).closest('tr');
		 // alert(tr.find('td:eq(0)').text());
		 var stok_terpakai=get_stok_dipake(tr[0].sectionRowIndex) + parseFloat(tr.find('td:eq(31) input').val());
		 if (stok_terpakai==0){
			 sweetAlert("Maaf...", "Sisa Habis!", "error");
            return false;
		 }
		$('#xnamabarang').val(tr.find('td:eq(1)').text());
		$('#xharga').val(tr.find('td:eq(16) input').val());
		$('#xppn').val(tr.find('td:eq(17) input').val());
		$('#xharga_plus_ppn').val(tr.find('td:eq(18) input').val());
		$('#xdiskon_rp').val(tr.find('td:eq(19) input').val());
		$('#xharga_plus_diskon').val(tr.find('td:eq(20) input').val());
		$('#xkuantitas').val(tr.find('td:eq(27) input').val());
		// alert(stok_terpakai);
		$('#xkuantitas_sisa').val(stok_terpakai);
		$('#xharga_master').val(tr.find('td:eq(29) input').val());
		$('#tanggalkadaluarsa').val(tr.find('td:eq(30) input').val());
		$('#nobatch').val(tr.find('td:eq(22) input').val());
		$("#xppn_nominal").val(parseFloat($("#xharga").val())*parseFloat($('#xppn').val())/100);
		var diskon_persen=parseFloat((parseFloat($('#xdiskon_rp').val()*100))/$("#xharga_plus_ppn").val());
		$("#xdiskon_persen").val(diskon_persen);
		
		$('#modal_edit').modal('show');
        $("#rowIndex").val(tr[0].sectionRowIndex);
		// alert();
		gen_harga();
		cek_harga();
	});
	function cek_duplikasi($row){
		var duplikasi=0;
		$('#data_hapus tbody tr').each(function() {
			var tr = $(this).closest('tr');	
			 
			if (tr.find("td:eq(9) input").val()==$row){
				duplikasi='1';
			}
			
			
		});
		if (duplikasi){
			sweetAlert("Maaf...", "Barang Sudah ada Dalam List!", "error");
            return false;
		}
		return true;
	}
	function get_stok_dipake($row){
		var jml=0;
		var sisa=0;
		$('#data_hapus tbody tr').each(function() {
			var tr = $(this).closest('tr');	
			 
			if (tr.find("td:eq(9) input").val()==$row){
				jml=jml + parseFloat(tr.find("td:eq(8) input").val());
				
			}
			
			
		});
		$('#data-barang tbody tr').each(function() {
			var tr = $(this).closest('tr');	
			 
			if (tr[0].sectionRowIndex==$row){
				jml=jml + parseFloat(tr.find("td:eq(31) input").val());
				sisa=parseFloat(tr.find("td:eq(28) input").val());
			}
			
			
		});
		return sisa-jml;
	}
	function show_button($row){
		var stok=get_stok_dipake($row);
		$('#data-barang tbody tr').each(function() {
			var tr = $(this).closest('tr');	
			if (tr[0].sectionRowIndex==$row){
				
				var button='';
				if (stok>0){
					button += '<button class="btn btn-xs btn-success edit"  type="button" title="Terima Barang"><i class="fa fa-pencil"></i></button> ';
					button += '<button class="btn btn-xs btn-danger hapus"  type="button" title="Hapus Barang"><i class="glyphicon glyphicon-remove"></i></button> ';
					button += '<button class="btn btn-xs btn-warning alih"  type="button" title="Alihkan"><i class="si si-shuffle"></i> Alihkan</button>';//12
				}else{
					button += '<button class="btn btn-xs btn-success edit"  type="button" title="Terima Barang"><i class="fa fa-pencil"></i></button> ';
				}
					
				tr.find("td:eq(12)").html(button);
			}
			
			
		});
		// return sisa-jml;
	}
	
	function get_distributor() {
        // get nopemesanan
       $(".e_dist").select2({
			minimumInputLength: 2,
			noResults: 'Distributor Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}tgudang_penerimaan/get_distributor/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,

			 data: function (params) {
				  var query = {
					search: params.term,
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.nama,
								id: item.id,
							}
						})
					};
				}
			}
		});
    }
	function gen_total_uang(){
			var tot=0;
			var tot_barang=0;
		$('#data-barang tbody tr').each(function() {
			var tr = $(this).closest('tr');	
			tot=tot+ parseFloat(tr.find("td:eq(21) input").val());
			tot_barang=tot_barang+ parseFloat(tr.find("td:eq(31) input").val());
			 // console.log(tr.find("td:eq(21) input").val());
			
		});
		$("#totalharga").val(tot);
		$("#totalbarang").val(tot_barang);
	}
	function formatNumber (num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}
    
	
    function noPemesanan() {
        // get nopemesanan
        $.getJSON('{site_url}tgudang_penerimaan/get_nopemesanan', function(data) {
            var dataoption = '<option value="#">Pilih No. Pemesanan</option>';
            $.each(data, function(i, item) {
                dataoption += '<option value="' + item.id + '">(' + item.nopemesanan + ') ' + item.nama + '</option>';
            })
            $('#nopemesanan').empty()
            $('#nopemesanan').append(dataoption)
        })
    }
	

    function grandTotal() {
        var totalbarang = 0;
        var totalharga = 0;
        $('#data-barang tbody tr').each(function() {
            totalbarang += parseFloat($(this).find('td:eq(6)').text().replace(/\,/g, ""));
            totalharga += parseFloat($(this).find('td:eq(7)').text().replace(/\,/g, ""));
        });

        $("#totalbarang").val(isNaN(totalbarang) ? 0 : totalbarang)
        $("#totalharga").val(isNaN(totalharga) ? 0 : totalharga)
    }

    function validateHead() {
        if ($('#nopemesanan').val() == '') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'No Pemesanan tidak boleh kosong'
            });
            return false;
        }
        if ($('#tanggalpenerimaan').val() == '') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Tanggal Penerimaan tidak boleh kosong'
            });
            return false;
        }
        if ($('#tanggaljatuhtempo').val() == '') {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Tanggal Jatuh Tempo tidak boleh kosong'
            });
            return false;
        }
        if ($('#totalharga').val() == '' || $('#totalharga').val() <= 0) {
            $.toaster({
                priority: 'danger',
                title: 'Error!',
                message: 'Data belum lengkap. Total terima barang masih 0'
            });
            return false;
        }
        return true;
    }

    function validate() {
        // if ($('#xharga_total_fix').val() == 0) {
            // sweetAlert("Maaf...", "Harga Dan kuantitas harus diisi!", "error");
            // return false;
        // }
        
        if ($('#tanggalkadaluarsa').val() == '' && $('#xharga_total_fix').val() != 0) {
            sweetAlert("Maaf...", "Tanggal Kadaluarsa harus diisi!", "error");
            return false;
        }
        if (($('#nobatch').val() == '' || $('#nobatch').val() == '-') && ($('#xharga_total_fix').val() != 0)) {
            sweetAlert("Maaf...", "No Batch Harus diisi!", "error");
            return false;
        }
        return true;
    }

    
	$("#xharga").keyup(function(){
		if ($("#xharga").val()==''){
			$("#xharga").val(0)
		}
		var ppn=$("#xppn").val();
		var harppn=parseFloat($(this).val()) + (parseFloat($(this).val())*ppn/100);
		$("#xharga_plus_ppn").val(harppn);
		cek_harga();
	});
	$("#xdiskon_rp").keyup(function(){
		if ($("#xdiskon_rp").val()==''){
			$("#xdiskon_rp").val(0)
		}
		
		if (parseFloat($(this).val()) > $("#xharga_plus_ppn").val()){
			$(this).val($("#xharga_plus_ppn").val());
		}
		var diskon_persen=parseFloat((parseFloat($(this).val()*100))/$("#xharga_plus_ppn").val());
		$("#xdiskon_persen").val(diskon_persen);
		gen_harga();
	});
	$("#xdiskon_persen").keyup(function(){
		if ($("#xdiskon_persen").val()==''){
			$("#xdiskon_persen").val(0)
		}
		if (parseFloat($(this).val()) > 100){
			$(this).val(100);
		}
		var diskon_rp=parseFloat($("#xharga_plus_ppn").val() * parseFloat($(this).val())/100);
		$("#xdiskon_rp").val(diskon_rp);
		gen_harga();
	});
	$("#xkuantitas").keyup(function(){
		if (parseFloat($("#xkuantitas").val()) > parseFloat($("#xkuantitas_sisa").val())){
			sweetAlert("Maaf...", "Kuantitas Lebih Besar Dari Sisa", "error");
			$("#xkuantitas").val($("#xkuantitas_sisa").val());			
		}		
		gen_harga();
	});
	function gen_harga(){
		var harga_final=parseFloat($("#xharga_plus_ppn").val()) - parseFloat($("#xdiskon_rp").val());
		var xharga_total_fix=parseFloat($("#xkuantitas").val()) * harga_final;
		$("#xharga_plus_diskon").val(harga_final);
		$("#xharga_total_fix").val(xharga_total_fix);
	}
	$("#xppn").keyup(function(){
		if ($("#xppn").val()>100){
			$("#xppn").val(100)
		}
		if ($("#xppn").val()==''){
			$("#xppn").val(0)
		}
		var ppn=$("#xppn").val();
		var harppn=parseFloat($("#xharga").val()) + (parseFloat($("#xharga").val())*ppn/100);
		$("#xppn_nominal").val(parseFloat($("#xharga").val())*ppn/100);
		$("#xharga_plus_ppn").val(harppn);
		cek_harga();
	});
	function cek_harga(){
		var selisih;
		var harppn=$("#xharga_plus_ppn").val();
		$('#div_harga').removeClass('has-error');			
		$('#div_harga').removeClass('has-success');	
		if (parseFloat(harppn) != $("#xharga_master").val()){
			if (parseFloat(harppn) > $("#xharga_master").val()){
				selisih =  parseFloat(harppn) - parseFloat($("#xharga_master").val());
						
				$('#div_harga').addClass('has-error');
				$('#val_harga').text('Harga Lebih Mahal Rp. '+ formatNumber(selisih));
			}else{
				selisih =  parseFloat($("#xharga_master").val()) - parseFloat(harppn) ;
				
				$('#div_harga').addClass('has-success');
				$('#val_harga').text('Harga Lebih Murah Rp. '+formatNumber(selisih));
			}
			
		}else{
			$('#val_harga').text('');
		}
		
		gen_harga();
	}
    function validate_header() {
		// alert('Validasi');
		var rowCount = $('#data-barang tr').length;
		var rowCountHapus = $('#data_hapus tr').length;
		// alert(rowCount);
		if (rowCountHapus>1){
			$("#st_hapus_alih").val('1');
		}else{
			$("#st_hapus_alih").val('0');
		}
        if (parseFloat($('#totalbarang').val()) == 0 && rowCount > 1) {
			if (rowCountHapus <=1){
            sweetAlert("Maaf...", "Barang harus diverifikasi!", "error");
            return false;
			}
        }
        if (rowCountHapus <=1){
			if ($('#nofakturexternal').val() == '') {
				sweetAlert("Maaf...", "No Faktur harus diisi!", "error");
				return false;
			}
			if ($('#tanggalpenerimaan').val() == '') {
				sweetAlert("Maaf...", "Tanggal Harus di isi!", "error");
				return false;
			}
			if ($('#tanggaljatuhtempo').val() == '' && $("#pembayaran").val()=='2') {
				sweetAlert("Maaf...", "Tanggal Jatuh Tempo Harus di isi!", "error");
				return false;
			}
		}
		var ada_kosong_dist='1';
		var ada_kosong_dist_alasan='1';
		var ada_kosong_hapus='1';
		if (rowCountHapus>1){
			$('#data_hapus tbody tr').each(function() {
				var tr = $(this).closest('tr');	
				 // alert(tr.find("td:eq(13) input").val());
				 console.log('DIST'+tr.find("td:eq(4) select").val());
				if (tr.find("td:eq(7) input").val()=='3' && tr.find("td:eq(4) select").val()=='#'){
					ada_kosong_dist='0';
				}
				if (tr.find("td:eq(7) input").val()=='3' && tr.find("td:eq(5) input").val()==''){
					ada_kosong_dist_alasan='0';
				}
				if (tr.find("td:eq(7) input").val()=='1' && tr.find("td:eq(5) input").val()==''){
					ada_kosong_hapus='0';					
				}
				if (tr.find("td:eq(7) input").val()=='2' && tr.find("td:eq(5) input").val()==''){
					ada_kosong_hapus='0';					
				}
				console.log(tr.find("td:eq(7) input").val()+'-'+tr.find("td:eq(5) input").val());
				
			});
			console.log(ada_kosong_dist+';'+ada_kosong_dist_alasan+';'+ada_kosong_hapus);
			if (ada_kosong_dist=='0'){
				sweetAlert("Maaf...", "Harus diisi Distributor!", "error");
				return false;
			}
			if (ada_kosong_dist_alasan=='0'){
				sweetAlert("Maaf...", "Harus diisi Alasan Ganti Distributor!", "error");
				return false;
			}
			if (ada_kosong_hapus=='0'){
				sweetAlert("Maaf...", "Harus diisi Alasan Hapus!", "error");
				return false;
			}
		}
        return true;
    }
	$("#simpan-penerimaan").click(function() {
		// alert('Simpan');
		 if (validate_header()){
			 $("#simpan-penerimaan").hide();
				$("#cover-spin").show();

			$("#form-work").submit();
		}
		
		return false;
	});
	
</script>