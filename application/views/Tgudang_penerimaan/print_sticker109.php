<!DOCTYPE html>
<html>
<!--untuk e-ticket
nanti di printer nya harus di setting kertas nya dulu
dengan ukuran 
160mm x 200 mm 
Ketas Label 121 Merk FOX
-->
<head>
	<title>Label Obat</title>
	<style type="text/css">
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
	body{
        font-family: 'font';
    }

		@page {
            margin-top: 8.6mm;
            margin-left: 6.8mm;
            margin-bottom: 2mm;
        }
		
		{
		  box-sizing: border-box;
		}

		.column {
		  float: left;
		  padding-top: 5px;
		  width: 20mm;
		  height : 7.2mm;
		}
		.batas {
		  float: left;
		  padding-top: 5px;
		  width: 2.1mm;
		}

		.row:after {
		  content: "";
		  display: table;
		  clear: both;
		  margin-bottom: 1.6mm;
		}
		
		.b{
			font-size: 8px;
			text-align: center!important;
		}
		span.blocktext {
			font-size: 8px;
			text-align: center!important;
			display: block;
			margin-top:1px;
		}
		span.bold {
			font-weight: bold;
		}
		P.barcode {
			font-size: 31px;
			margin-left: auto;
			margin-right: auto;

			text-align: center!important;
		}
</style>
</head>

<body>
<?php  //$generator = new Picqer\Barcode\BarcodeGeneratorPNG(); ?>
<?php 

for ($i=1; $i <= $akhir; $i+=9) {
$warna='#87CEEB';	

?>	
<div class="row">
  <div class="column" style="background:#fff">
    <span class="blocktext bold"><?=$final[$i]['namabarang']?> </span>
    <span class="blocktext"><?=$final[$i]['nobatch']?> </span>
    <span class="blocktext"><?=$final[$i]['tanggalkadaluarsa']?> </span>
  </div>
  <div class="batas" style="background:#fff"></div>
  <div class="column" style="background:#fff">
    <span class="blocktext bold"><?=$final[$i+1]['namabarang']?> </span>
    <span class="blocktext"><?=$final[$i+1]['nobatch']?> </span>
    <span class="blocktext"><?=$final[$i+1]['tanggalkadaluarsa']?> </span>
	
  </div>
  <div class="batas" style="background:#fff"></div>
  <div class="column" style="background:#fff">
    <span class="blocktext bold"><?=$final[$i+2]['namabarang']?> </span>
    <span class="blocktext"><?=$final[$i+2]['nobatch']?> </span>
    <span class="blocktext"><?=$final[$i+2]['tanggalkadaluarsa']?> </span>
	
  </div>
  <div class="batas" style="background:#fff"></div>
  <div class="column" style="background:#fff">
    <span class="blocktext bold"><?=$final[$i+3]['namabarang']?> </span>
    <span class="blocktext"><?=$final[$i+3]['nobatch']?> </span>
    <span class="blocktext"><?=$final[$i+3]['tanggalkadaluarsa']?> </span>
	
  </div>
  <div class="batas" style="background:#fff"></div>
  <div class="column" style="background:#fff">
    <span class="blocktext bold"><?=$final[$i+4]['namabarang']?> </span>
    <span class="blocktext"><?=$final[$i+4]['nobatch']?> </span>
    <span class="blocktext"><?=$final[$i+4]['tanggalkadaluarsa']?> </span>
	
  </div>
  <div class="batas" style="background:#fff"></div>
  <div class="column" style="background:#fff">
    <span class="blocktext bold"><?=$final[$i+5]['namabarang']?> </span>
    <span class="blocktext"><?=$final[$i+5]['nobatch']?> </span>
    <span class="blocktext"><?=$final[$i+5]['tanggalkadaluarsa']?> </span>
	
  </div>
  <div class="batas" style="background:#fff"></div>
  <div class="column" style="background:#fff">
    <span class="blocktext bold"><?=$final[$i+6]['namabarang']?> </span>
    <span class="blocktext"><?=$final[$i+6]['nobatch']?> </span>
    <span class="blocktext"><?=$final[$i+6]['tanggalkadaluarsa']?> </span>
	
  </div>
  <div class="batas" style="background:#fff"></div>
  <div class="column" style="background:#fff">
    <span class="blocktext bold"><?=$final[$i+7]['namabarang']?> </span>
    <span class="blocktext"><?=$final[$i+7]['nobatch']?> </span>
    <span class="blocktext"><?=$final[$i+7]['tanggalkadaluarsa']?> </span>
	
  </div>
  <div class="batas" style="background:#fff"></div>
  <div class="column" style="background:#fff">
    <span class="blocktext bold"><?=$final[$i+8]['namabarang']?> </span>
    <span class="blocktext"><?=$final[$i+8]['nobatch']?> </span>
    <span class="blocktext"><?=$final[$i+8]['tanggalkadaluarsa']?> </span>
	
  </div>
</div>
			
	
	<? if ((($i+8) % 135 == 0)&&$akhir>($i+8)){?>
		<span style="height:0; page-break-before:always; margin:0; border-top:none; ">
	<?}?>
<?}?>
</body>
</html>
