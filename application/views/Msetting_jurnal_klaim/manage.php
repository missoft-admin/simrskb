<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}msetting_jurnal_klaim" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('msetting_jurnal_klaim/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>	
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_primary('Proses Approval')?></label>
				<input type="hidden" class="form-control" id="tmp_idakun_kredit" placeholder="0" name="tmp_idakun_kredit" value="{idakun_kredit}">
				<div class="col-md-4">
					<select id="st_auto_posting" name="st_auto_posting" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="1" <?=(1 == $st_auto_posting ? 'selected' : ''); ?>>AUTO POSTING</option>
                            <option value="2" <?=(2 == $st_auto_posting ? 'selected' : ''); ?>>MANUAL</option>
                        </select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_primary('Akun Pembayaran Tunai')?> </label>
				
				<div class="col-md-8">
					<select id="idakun_tunai" name="idakun_tunai" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
					<?foreach($list_akun as $r){?>
						<option value="<?=$r->id?>" <?=($r->id == $idakun_tunai? 'selected' : ''); ?>><?=$r->noakun.' - '.$r->namaakun.' ('.$r->id.')'?></option>
					<?}?>
					</select>
				</div>
				
			</div>
			<?
			$rows2=get_all('msumber_kas',array(),'nama','ASC');
			$rows=get_all('mdistributor',array(),'nama','ASC');
			?>
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_primary('Batas Waktu Batal')?></label>
				
				<div class="col-md-4">
					<input type="text" style="width: 100%"  class="form-control number" id="batas_batal" placeholder="0" name="batas_batal" value="<?=$batas_batal?>">
				</div>
				<div class="col-md-2">
					<button type="button" class="btn btn-sm btn-success" tabindex="8" id="btn_update" title="Update"><i class="fa fa-save"></i> UPDATE</button>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-1 control-label"><h4><?=text_success('SETTING AKUN PELUNASAN PIUTANG')?></h4></label>
				
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<table id="tabel_piutang" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 0%;">#</th>															
								<th style="width: 20%;">Kelompok Pasien</th>															
								<th style="width: 20%;">Rekanan </th>															
								<th style="width: 25%;">No Akun </th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td></td>
								<td>
									<select name="idkelompokpasien_piutang" id="idkelompokpasien_piutang" style="width: 100%" id="step" class="js-select2 form-control input-sm idkelompokpasien">										
																												
										
									</select>										
								</td>
								<td>
									<select name="idrekanan_piutang" id="idrekanan_piutang" style="width: 100%" id="step" class="js-select2 form-control input-sm distirbutor">										
										<option value="#" selected>- All Rekanan -</option>	
									</select>										
								</td>								
								
								<td>
									<select name="idakun_piutang" id="idakun_piutang" style="width: 100%" class="js-select2 form-control input-sm idakun">										
										
									</select>
									<input type="hidden" class="form-control" id="id_edit_piutang" placeholder="0" name="id_edit_piutang" value="">
								</td>	
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_piutang" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_piutang" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						
				</table>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-1 control-label"><h4><?=text_primary('SETTING AKUN OTHER LOSS')?></h4></label>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<table id="tabel_loss" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 0%;">#</th>															
								<th style="width: 20%;">Kelompok Pasien </th>															
								<th style="width: 20%;">Rekanan </th>															
								<th style="width: 25%;">No Akun </th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td></td>
								<td>
									<select name="idkelompokpasien_loss" id="idkelompokpasien_loss" style="width: 100%" id="step" class="js-select2 form-control input-sm idkelompokpasien">										
									</select>										
								</td>
								<td>
									<select name="idrekanan_loss" id="idrekanan_loss" style="width: 100%" id="step" class="js-select2 form-control input-sm distirbutor">										
										<option value="#" selected>- All Rekanan -</option>	
									</select>										
								</td>								
								
								<td>
									<select name="idakun_loss" id="idakun_loss" style="width: 100%" class="js-select2 form-control input-sm idakun">										
										
									</select>
									<input type="hidden" class="form-control" id="id_edit_loss" placeholder="0" name="id_edit_loss" value="">
								</td>	
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_loss" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_loss" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody></tbody>			
					</table>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-1 control-label"><h4><?=text_danger('SETTING AKUN OTHER INCOME')?></h4></label>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<table id="tabel_income" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 0%;">#</th>															
								<th style="width: 20%;">Kelompok Pasien </th>															
								<th style="width: 20%;">Rekanan </th>															
								<th style="width: 25%;">No Akun </th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td></td>
								<td>
									<select name="idkelompokpasien_income" id="idkelompokpasien_income" style="width: 100%" id="step" class="js-select2 form-control input-sm idkelompokpasien">										
																												
										
									</select>										
								</td>
								<td>
									<select name="idrekanan_income" id="idrekanan_income" style="width: 100%" class="js-select2 form-control input-sm distirbutor">										
										<option value="#" selected>- All Rekanan -</option>	
									</select>										
								</td>								
								
								<td>
									<select name="idakun_income" id="idakun_income" style="width: 100%" class="js-select2 form-control input-sm idakun">										
										
									</select>
									<input type="hidden" class="form-control" id="id_edit_income" placeholder="0" name="id_edit_income" value="">
								</td>	
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_income" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_income" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody></tbody>			
					</table>
				</div>
			</div>
			
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		// load_unit();
		list_akun();
		list_kelompok();
		load_piutang();
		load_loss();
		load_income();
		// load_kas();
		clear_input_piutang();
		clear_input_income();
		clear_input_loss();

	});	
	$(document).on("click","#btn_update",function(){
		if (validate_update()==false)return false;
		var st_auto_posting=$("#st_auto_posting").val();
		var idakun_tunai=$("#idakun_tunai").val();
		var batas_batal=$("#batas_batal").val();
		$("#tmp_idakun_kredit").val(idakun_tunai)
		$.ajax({
			url: '{site_url}msetting_jurnal_klaim/update',
			type: 'POST',
			data: {
				st_auto_posting:st_auto_posting,idakun_tunai:idakun_tunai,batas_batal:batas_batal,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$("#idakun_tunai").val($("#tmp_idakun_kredit").val()).trigger('change');
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function validate_update()
	{
		
		if ($("#st_auto_posting").val()=='#'){
			sweetAlert("Maaf...", "Setting Harus diisi", "error");
			return false;
		}
		if ($("#batas_batal").val()==''){
			sweetAlert("Maaf...", "Batas Waktu Batal Harus diisi", "error");
			return false;
		}
		if ($("#idakun_kredit").val()=='#'){
			sweetAlert("Maaf...", "No AKun Pembayaran Kredit Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	$(".idkelompokpasien").change(function(){
		list_rekanan($(this).val());
	});

	function list_rekanan($idkelompokpasien){
		$.ajax({
			url: '{site_url}msetting_jurnal_klaim/list_rekanan/'+$idkelompokpasien,
			dataType: "json",
			success: function(data) {
				$(".distirbutor").empty();
				$('.distirbutor').append('<option value="#" selected>- All Rekanan -</option>');
				$('.distirbutor').append(data.detail);
				
			}
		});		
	}
	function list_akun(){
		$.ajax({
			url: '{site_url}msetting_jurnal_klaim/list_akun',
			dataType: "json",
			success: function(data) {
				$(".idakun").empty();
				$('.idakun').append('<option value="#" selected>- Pilih Akun -</option>');
				$('.idakun').append(data.detail);
				
			}
		});		
	}
	function list_kelompok(){
		$.ajax({
			url: '{site_url}msetting_jurnal_klaim/list_kelompok',
			dataType: "json",
			success: function(data) {
				$(".idkelompokpasien").empty();
				$('.idkelompokpasien').append('<option value="#" selected>- Pilih Kelompok Pasien -</option>');
				$('.idkelompokpasien').append(data.detail);
				
			}
		});		
	}
	
	
	function validate_add_piutang()
	{
		
		
		if ($("#idkelompokpasien_piutang").val()=='#'){
			sweetAlert("Maaf...", "Kelompok Pasien Harus diisi", "error");
			return false;
		}
		if ($("#idakun_piutang").val()=='#'){
			sweetAlert("Maaf...", "No AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_piutang(){
		$("#idkelompokpasien_piutang").val('#').trigger('change');	
		$("#idrekanan_piutang").val('#').trigger('change');	
		$("#id_edit_piutang").val('');		
		$("#idakun_piutang").val('#').trigger('change');
		// alert($("#idakun").val());
	}
	$(document).on("click","#simpan_piutang",function(){
		if (validate_add_piutang()==false)return false;
		var id_edit=$("#id_edit_piutang").val();
		var idkelompokpasien=$("#idkelompokpasien_piutang").val();
		var idrekanan=$("#idrekanan_piutang").val();
		var idakun=$("#idakun_piutang").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_klaim/simpan_piutang',
			type: 'POST',
			data: {
				id_edit:id_edit,idrekanan:idrekanan,
				idakun:idakun,idkelompokpasien:idkelompokpasien,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_piutang').DataTable().ajax.reload( null, false );
					clear_input_piutang();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	function hapus_piutang($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_klaim/hapus_piutang/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_piutang').DataTable().ajax.reload( null, false );
						clear_input_piutang();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_piutang",function(){
		
		clear_input_piutang();
	});
	function load_piutang(){
		var idlogic=$("#id").val();
		
		$('#tabel_piutang').DataTable().destroy();
		var table = $('#tabel_piutang').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_klaim/load_piutang/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	//PPN
	
	function validate_add_loss()
	{
		if ($("#idkelompokpasien_loss").val()=='#'){
			sweetAlert("Maaf...", "Kelompok Pasien Harus diisi", "error");
			return false;
		}
		
		if ($("#idakun_loss").val()=='#'){
			sweetAlert("Maaf...", "No AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	
	function clear_input_loss(){
		$("#idrekanan_loss").val('#').trigger('change');	
			
		$("#id_edit_loss").val('');		
		$("#idakun_loss").val('#').trigger('change');
		// alert($("#idakun").val());
	}
	$(document).on("click","#simpan_loss",function(){
		if (validate_add_loss()==false)return false;
		var id_edit=$("#id_edit_loss").val();
		var idrekanan=$("#idrekanan_loss").val();
		var idkelompokpasien=$("#idkelompokpasien_loss").val();
		var idakun=$("#idakun_loss").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_klaim/simpan_loss',
			type: 'POST',
			data: {
				id_edit:id_edit,idrekanan:idrekanan,idakun:idakun,idkelompokpasien:idkelompokpasien,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_loss').DataTable().ajax.reload( null, false );
					clear_input_loss();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function hapus_loss($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_klaim/hapus_loss/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_loss').DataTable().ajax.reload( null, false );
						clear_input_loss();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_loss",function(){
		
		clear_input_loss();
	});
	function load_loss(){
		var idlogic=$("#id").val();
		
		$('#tabel_loss').DataTable().destroy();
		var table = $('#tabel_loss').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_klaim/load_loss/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
//DISKON

	function validate_add_income()
	{
		if ($("#idkelompokpasien_income").val()=='#'){
			sweetAlert("Maaf...", "Kelompok Pasien Harus diisi", "error");
			return false;
		}
		
		if ($("#idakun_income").val()=='#'){
			sweetAlert("Maaf...", "No AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_income(){
		$("#idrekanan_income").val('#').trigger('change');	
					
		$("#id_edit_income").val('');		
		$("#idakun_income").val('#').trigger('change');
	}
	$(document).on("click","#simpan_income",function(){
		if (validate_add_income()==false)return false;
		var id_edit=$("#id_edit_income").val();
		var idkelompokpasien=$("#idkelompokpasien_income").val();
		var idrekanan=$("#idrekanan_income").val();
		var idakun=$("#idakun_income").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_klaim/simpan_income',
			type: 'POST',
			data: {
				id_edit:id_edit,idrekanan:idrekanan,
				idakun:idakun,idkelompokpasien:idkelompokpasien,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_income').DataTable().ajax.reload( null, false );
					clear_input_income();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function hapus_income($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_klaim/hapus_income/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_income').DataTable().ajax.reload( null, false );
						clear_input_income();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_income",function(){
		
		clear_input_income();
	});
	function load_income(){
		var idlogic=$("#id").val();
		
		$('#tabel_income').DataTable().destroy();
		var table = $('#tabel_income').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_klaim/load_income/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	
</script>
