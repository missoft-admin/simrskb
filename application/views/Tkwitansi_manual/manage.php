<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}tkwitansi_manual" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('Tkwitansi_manual/save','class="form-horizontal"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tglpendaftaran">Tanggal</label>
				<div class="col-md-7">
					<div class="row">
						<div class="col-md-4">
							<input tabindex="2" type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" name="tanggal" value="{tanggal}">
						</div>
						<div class="col-md-8">
							<input tabindex="3" type="text" class="time-datepicker form-control" name="waktu" value="{waktu}">
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Pasien</label>
				<div class="col-md-7">
					<select class="js-select2 form-control" name="pasien_id" style="width: 100%;" data-placeholder="Pilih pasien..">
						<option value="#">Select an Option</option>
						<?php foreach ($list_pasien as $row) { ?>
							<option value="<?=$row->id;?>" <?=($pasien_id==$row->id) ? "selected" : "" ?>><?=$row->no_medrec.' '.$row->title.'. '.$row->nama;?></option>
						<? } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Tipe</label>
				<div class="col-md-7">
					<select class="js-select2 form-control" name="tipe" id="tipe" style="width: 100%;" data-placeholder="Pilih tipe..">
						<option value="1">Kwitansi Non Tunggakan</option>
						<option value="2">Kwitansi Tunggakan</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Metode</label>
				<div class="col-md-7">
					<select name="idmetode" id="idmetode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($idmetode==1?'selected':'')?>>Tunai</option>
						<option value="2" <?=($idmetode==2?'selected':'')?>>Debit</option>
						<option value="3" <?=($idmetode==3?'selected':'')?>>Kartu Kredit</option>
						<option value="4" <?=($idmetode==4?'selected':'')?>>Transfer</option>
					</select>
				</div>
			</div>
			<div class="form-group" id="div_bank" <?=($idmetode!=1?'':'hidden')?>>
				<label class="col-md-3 control-label" for="idbank">Bank</label>
				<div class="col-md-7">
					<select name="idbank" id="idbank" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach ($list_bank as $row) {?>
						<option value="<?=$row->id?>" <?=($idbank==$row->id?'selected':'')?>><?=$row->nama . ' (' . $row->atasnama . ')'?></option>
						<?php }?>
					</select>
				</div>
			</div>
			<div class="form-group" id="div_cc" <?=($idmetode==3?'':'hidden')?>>
				<label class="col-md-3 control-label" for="nama">Ket. CC </label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="ket_cc" placeholder="Ket. CC" name="ket_cc" value="{ket_cc}">
				</div>
			</div>
			<div class="form-group" id="div_trace" <?=($idmetode==2 || $idmetode==4 ?'':'hidden')?>>
				<label class="col-md-3 control-label" for="nama">Trace Number</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="trace_number" placeholder="Trace Number" name="trace_number" value="{trace_number}">
				</div>
			</div>
			<div class="form-group" id="frm_jaminan">
				<label class="col-md-3 control-label" for="example-hf-email">Jaminan</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="jaminan" placeholder="Jaminan" name="jaminan" value="{jaminan}">
				</div>
			</div>
			<div class="form-group" id="frm_nominal">
				<label class="col-md-3 control-label" for="example-hf-email">Nominal</label>
				<div class="col-md-7">
					<input type="text" class="form-control number" id="nominal" placeholder="Nominal" name="nominal" value="{nominal}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Keterangan</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="keterangan" placeholder="Keterangan" name="keterangan" value="{keterangan}">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Submit</button>
					<a href="{base_url}tkwitansi_manual" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
		$('.number').number(true, 0, '.', ',');

		$(".time-datepicker").datetimepicker({
			format: "HH:mm:ss"
		});

		$("#frm_jaminan").hide();
		$("#frm_nominal").show();
		$("#tipe").change(function() {
			if($(this).val() == 1){
				$("#frm_jaminan").hide();
				$("#frm_nominal").show();
			}else{
				$("#frm_jaminan").show();
				$("#frm_nominal").hide();
			}
		});

		$("#idmetode").change(function(){
		  if ($("#idmetode").val()=="1"){
		    //CASH
		    $("#div_cc").hide();
		    $("#div_trace").hide();
		    $("#div_bank").hide();
		    $("#keterangan").val('Tunai');
		  }

			if ($("#idmetode").val()=="2" || $("#idmetode").val()=="3" || $("#idmetode").val()=="4"){
		    $("#div_kontraktor").hide();
		    $("#div_kontraktor2").hide();
		    if ($("#idmetode").val()=="3"){
		      $("#div_cc").show();
		    }else{
		      $("#div_cc").hide();
		    }
		    $("#div_trace").show();
		    $("#div_bank").show();
		    $("#keterangan").val($("#idmetode option:selected").text()+' : '+$("#idbank option:selected").text());
		  }
		});

		$("#idbank").change(function() {
			$("#keterangan").val($("#idmetode option:selected").text() + ' : ' + $("#idbank option:selected").text());
		});

		$("#ket_cc").keyup(function() {
			$("#keterangan").val($("#idmetode option:selected").text() + ' : ' + $("#idbank option:selected").text() + ', Keterangan CC : ' + $("#ket_cc").val());
		});
	});
</script>
