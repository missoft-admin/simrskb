<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}tkwitansi_manual/add" class="btn"><i class="fa fa-plus"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<table class="table table-bordered table-striped js-dataTable-full">
			<thead>
				<tr>
					<th>No</th>
					<th>Tanggal</th>
					<th>Pasien</th>
					<th>Keterangan</th>
					<th>Nominal / Jaminan</th>
					<th>Tipe</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php $number = 0; ?>
				<?php foreach($list_index as $row){ ?>
					<?php $number = $number + 1; ?>
					<tr>
						<td><?php echo $number; ?></td>
						<td><?php echo $row->tanggal; ?></td>
						<td><?php echo $row->nama_pasien; ?></td>
						<td><?php echo $row->keterangan; ?></td>
						<td><?php echo ($row->tipe == 1 ? number_format($row->nominal) : $row->jaminan); ?></td>
						<td><?php echo ($row->tipe == 1 ? 'NON TUNGGAKAN' : 'TUNGGAKAN'); ?></td>
						<td>
							<div class="btn-group">
								<a class="btn btn-primary btn-sm" href="{base_url}tkwitansi_manual/edit/<?php echo $row->id; ?>" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>&nbsp;
								<a class="btn btn-danger btn-sm" href="{base_url}tkwitansi_manual/remove/<?php echo $row->id; ?>" data-toggle="tooltip" title="Remove"><i class="fa fa-trash-o"></i></a>&nbsp;
								<a class="btn btn-success btn-sm" href="{base_url}tkwitansi_manual/print_kwitansi/<?php echo $row->id; ?>" target="_blank"><i class="fa fa-print"></i></a>
							</div>
						</td>
					</tr>
				<? } ?>
			</tbody>
		</table>
	</div>
</div>
