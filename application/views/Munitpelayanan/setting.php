<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-rounded">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}munitpelayanan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('munitpelayanan/save_user_setting','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama Unit Kerja</label>
				<div class="col-md-9">
					<input type="text" class="form-control" id="nama" readonly placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="id" readonly placeholder="Nama" name="id" value="{id}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="deskripsi">User</label>
				<div class="col-md-7"> 
					<select name="user[]" class="form-control user"></select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}munitpelayanan" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_close() ?>
	</div>
</div>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$.ajax({
			url: '{site_url}munitpelayanan/get_user', 
			type: 'post',
			dataType: 'json',
			success: function(res) {
				$('.user').select2({multiple: true, data: res});
			}
		})
		setTimeout(function() {
			
			$.ajax({
				type: "POST", 
				url: '{site_url}munitpelayanan/get_user_setting/{id}',
				dataType: 'JSON', 
				success: function(data) {
					var arr = [];
					for (var i in data) { arr.push(data[i].id); }
					$('.user').val(arr).trigger('change'); 
				} 
			});

		}, 500);
	});
	
	
</script>