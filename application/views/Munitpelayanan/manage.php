<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}munitpelayanan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('munitpelayanan/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="lokasi">Lokasi</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="lokasi" placeholder="Lokasi" name="lokasi" value="{lokasi}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="deskripsi">Deskripsi</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="deskripsi" placeholder="Deskripsi" name="deskripsi" value="{deskripsi}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="deskripsi">Tipe Barang</label>
				<div class="col-md-7"> 
					<select name="tipebarang[]" class="form-control tipebarang"></select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="deskripsi">Unit Pelayanan Peminta</label>
				<div class="col-md-7"> 
					<select name="unitpelayanan_permintaan[]" class="form-control unitpelayanan_permintaan"></select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="deskripsi">User</label>
				<div class="col-md-7"> 
					<select name="user[]" class="form-control user"></select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="deskripsi">Otomatis Terima</label>
				<div class="col-md-7"> 
					<select name="st_otomatis_terima" id="st_otomatis_terima" class="js-select2 form-control" style="width: 100%;">
						<option value="0" <?=$st_otomatis_terima=='0'?'selected':''?>>Tidak</option>
						<option value="1" <?=$st_otomatis_terima=='1'?'selected':''?>>Ya</option>											
					</select>	
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="jml_hari_terima">Jumlah Hari</label>
				<div class="col-md-7">
					<input type="text" class="form-control decimal" id="jml_hari_terima" placeholder="Jumlah Hari Terima" name="jml_hari_terima" value="{jml_hari_terima}">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}munitpelayanan" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".decimal").number(true,0,'.',',');
		cek_jml_hari();
		$('.tipebarang').select2({
			placeholder: 'Select an Option',
			multiple: true,
			data: [
				{id: '1', text: 'Alkes'},
				{id: '2', text: 'Implan'},
				{id: '3', text: 'Obat'},
				{id: '4', text: 'Logistik'},
			]
		}).val('{tipebarang}').trigger('change')

		$.ajax({
			url: '{site_url}munitpelayanan/get_unitpelayanan', 
			type: 'post',
			dataType: 'json',
			success: function(res) {
				$('.unitpelayanan_permintaan').select2({multiple: true, data: res});
			}
		})

		$.ajax({
			url: '{site_url}munitpelayanan/get_user', 
			type: 'post',
			dataType: 'json',
			success: function(res) {
				$('.user').select2({multiple: true, data: res});
			}
		})

		setTimeout(function() {
			$.ajax({
				type: "POST", 
				url: '{site_url}munitpelayanan/get_unitpelayanan_permintaan/{id}',
				dataType: 'JSON', 
				success: function(data) {
					var arr = [];
					for (var i in data) { arr.push(data[i].id); }
					$('.unitpelayanan_permintaan').val(arr).trigger('change'); 
				} 
			});

			$.ajax({
				type: "POST", 
				url: '{site_url}munitpelayanan/get_unitpelayanan_tipebarang/{id}',
				dataType: 'JSON', 
				success: function(data) {
					var arr = [];
					for (var i in data) { arr.push(data[i].id); }
					$('.tipebarang').val(arr).trigger('change'); 
				} 
			});

			$.ajax({
				type: "POST", 
				url: '{site_url}munitpelayanan/get_unitpelayanan_user/{id}',
				dataType: 'JSON', 
				success: function(data) {
					var arr = [];
					for (var i in data) { arr.push(data[i].id); }
					$('.user').val(arr).trigger('change'); 
				} 
			});

		}, 500);
	})	
	$("#st_otomatis_terima").change(function(){
		cek_jml_hari();
	});

	function cek_jml_hari(){
		if ($("#st_otomatis_terima").val()=='1'){
			$("#jml_hari_terima").attr('readonly',false);
		}else{
			$("#jml_hari_terima").attr('readonly',true);
			
		}
	}
</script>