<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<?
$user_id=$this->session->userdata('user_id');
?>
<div class="block push-10">
	<ul class="nav nav-pills">
		<li id="div_11" class="<?=($tab_utama=='11'?'active':'')?>">
			<a href="javascript:void(0)" onclick="set_tab_utama(11)"><i class="fa fa-user-md"></i> Telaah Resep Baru</a>
		</li>
		<li  id="div_22" class="<?=($tab_utama=='22'?'active':'')?>">
			<a href="javascript:void(0)"  onclick="set_tab_utama(22)"><i class="si si-check"></i> Konfirmasi Resep </a>
		</li>
		<li  id="div_33" class="<?=($tab_utama=='33'?'active':'')?>">
			<a href="javascript:void(0)"  onclick="set_tab_utama(33)"><i class="si si-basket-loaded"></i> Selesai Transaksi </a>
		</li>
		<li  id="div_44" class="<?=($tab_utama=='44'?'active':'')?>">
			<a href="javascript:void(0)"  onclick="set_tab_utama(44)"><i class="fa fa-handshake-o"></i> Telah Diserahkan </a>
		</li>
		<li  id="div_55" class="<?=($tab_utama=='55'?'active':'')?>">
			<a href="javascript:void(0)"  onclick="set_tab_utama(55)"><i class="si si-ban"></i> Tidak Diambil </a>
		</li>
		<li  id="div_66" class="<?=($tab_utama=='66'?'active':'')?>">
			<a href="javascript:void(0)"  onclick="set_tab_utama(66)"><i class="fa fa-list-ul"></i> List Transaksi </a>
		</li>
		
	</ul>
	<div class="block-content bg-primary push-10-t div_index">
			<input type="hidden" id="tab" name="tab" value="<?=$tab?>">
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			
			<div class="row pull-10">
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="login1-password">Tujuan Farmasi</label>
						<div class="col-xs-12">
							<select id="tujuan_farmasi_id" name="tujuan_farmasi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" <?=($tujuan_farmasi_id=='#'?'selected':'')?>>- Semua Tujuan -</option>
								<?foreach($list_tujuan as $r){?>
								<option value="<?=$r->id?>" <?=($jumlah_tujuan==1?'selected':'')?>><?=$r->nama_tujuan?></option>
								<?}?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-2 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">Asal Pasien</label>
						<div class="col-xs-12">
							<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" <?=($idpoli=='#'?'selected':'')?>>- Semua -</option>
								<option value="1" <?=($idpoli=='1'?'selected':'')?>>Poliklinik</option>
								<option value="2" <?=($idpoli=='2'?'selected':'')?>>IGD</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="iddokter">Dokter</label>
						<div class="col-xs-12">
							<select id="iddokter" name="iddokter"  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" <?=($iddokter=='#'?'selected':'')?>>- Semua Dokter -</option>
								<?foreach($list_dokter as $r){?>
								<option value="<?=$r->id?>" <?=($iddokter==$r->id?'selected':'')?>><?=$r->nama?></option>
								<?}?>
								
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="tanggal">Tanggal</label>
						<div class="col-xs-12">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="js-datepicker form-control div_tidak_dikenal" type="text" id="tanggal" data-date-format="dd-mm-yyyy"  value="{tanggal}" placeholder="Tanggal Layanan">
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="js-datepicker form-control div_tidak_dikenal" type="text" id="tanggal_2"  data-date-format="dd-mm-yyyy"  value="{tanggal_2}" placeholder="Tanggal Layanan">
							</div>
							
						</div>
					</div>
				</div>
				
				<div class="col-md-1 push-10">
					<div class="form-group">
						<label class="col-xs-12">&nbsp;&nbsp;</label>
						<div class="col-xs-12">
							<span class="input-group-btn ">
								<button class="btn btn-success btn-block" onclick="refresh_all()" id="btn_login" type="button" title="Login"><i class="fa fa-search"></i> Cari</button>&nbsp;
							</span>
						</div>
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
	</div>
</div>
<input type="hidden" id="tujuan_array" value="{str_array}">
<input type="hidden" id="tujuan_id" value="{array_tujuan_antrian}">
<input type="hidden" id="tujuan_id_spesifik" value="">
<div class="block div_index" >
	<ul class="nav nav-pills">
		<li id="div_1" class="<?=($tab=='1'?'active':'')?>">
			<a href="javascript:void(0)" onclick="set_tab(1)"><i class="si si-list"></i> DATA FARMASI RAWAT INAP</a>
		</li>
		
	</ul>
	<div class="block-content">
			<input type="hidden" id="tab" name="tab" value="{tab}">
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row pull-10">
				
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">No Medrec</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="no_medrec" placeholder="No. Medrec" name="no_medrec" value="{no_medrec}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Nama Pasien</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="namapasien" placeholder="Nama Pasien" name="namapasien" value="{namapasien}">
						</div>
					</div>
					
					
				</div>
				<div class="col-md-6">
					
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Status</label>
						<div class="col-md-9">
							<select id="status_transaksi" name="status_transaksi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" selected>- Semua Status -</option>
								<option value="2">MENUNGGU ANTRIAN</option>
								<option value="3">TELAH DILAYANI</option>
								<option value="4">TERLEWATI</option>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label" for="btn_filter_all"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="button" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						</div>
					</div>
					
				</div>
			</div>
			<?php echo form_close() ?>
			<div class="row   ">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table" id="index_all">
							<thead>
								<tr>
									<th width="10%"></th>
									<th width="10%"></th>
									<th width="10%"></th>
									<th width="10%"></th>
									<th width="10%"></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		
	
	</div>
</div>
<div class="block " id="div_penjualan">
	<div class="block-content ">
		<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				<?php echo form_open('tpasien_penjualan/filter','class="form-horizontal" id="form-work"') ?>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">No Penjualan</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="nopenjualan" value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">No Medrec</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="nomedrec_penjualan" value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Nama Pasien</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="namapasien_penjualan" value="">
						</div>
					</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="">Asal Pasien</label>
							<div class="col-md-8">
								<select id="idasalpasien_penjualan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="0" selected>Semua Asal Pasien</option>
									<option value="1" >Poliklinik</option>
									<option value="2" >Instalasi Gawat Darurat</option>
									<option value="3" >Rawat Inap</option>
								</select>
							</div>
						</div>
					<div class="form-group">
		          <label class="col-md-4 control-label" for="example-daterange1">Tanggal</label>
		          <div class="col-md-8">
		            <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
		                <input class="form-control" type="text" id="tanggaldari" placeholder="From" value="<?=date('d-m-Y')?>">
		                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
		                <input class="form-control" type="text" id="tanggalsampai" placeholder="To" value="<?=date('d-m-Y')?>">
		            </div>
		          </div>
		      </div>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Kelompok Pasien</label>
						<div class="col-md-8">
							<select id="idkelompokpasien_penjualan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                <option value="#" selected>Semua Kelompok Pasien</option>
                <?php foreach (get_all('mpasien_kelompok') as $row){?>
                  <option value="<?=$row->id ?>" ><?=$row->nama ?></option>
                <?php } ?>
              </select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Status</label>
						<div class="col-md-8">
							 <select id="status_penjualan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="0" selected>Semua Status</option>
								<option value="1" >Belum Ditindak</option>
								<option value="2" >Sudah Ditindak</option>
								<option value="3" >Sudah Diserahkan</option>
							  </select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for=""></label>
						<div class="col-md-8">							
							<button class="btn btn-success text-uppercase" onclick="load_penjualan()" type="button" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
							
						</div>
					</div>
				</div>
				<?php echo form_close() ?>
			</div>
			<hr style="margin-top:10px">

			<!-- <h4 class="font-w300 push-15">Home Tab</h4> -->
			<table width="100%" id="index_penjualan" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Tanggal/Jam</th>
						<th>No. Jual</th>
						<th>No. Medrec</th>
						<th>Nama Pasien</th>
						<th>Asal Pasien</th>
						<th>Status</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		<?php echo form_close() ?>
			
		
	
	</div>
</div>
<div class="modal in" id="modal_panel_kendal" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title" id="judul_panel_kendali">PENYERAHAN OBAT</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12">
								
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div class="col-md-12 ">
											<label for="example-input-normal" id="label_keterangan_panel_kendali"></label>
											<select tabindex="5" id="mppa_id" class="form-control " style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="" >Pilih Opsi</option>
												<?foreach($list_ppa_all as $row){?>
												<option value="<?=$row->id?>"><?=$row->nama?></option>
												<?}?>
												
											</select>
										</div>
										
									</div>
								</div>
								
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						<input type="hidden" id="assesmen_id" value="">
						<input type="hidden" id="tipe" value="">
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="simpan_set()" ><i class="fa fa-save"></i> SIMPAN</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<? $this->load->view('Tpendaftaran_poli_ttv/modal_alergi')?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
var tab;
var st_login='1';
var tab_utama=<?=$tab_utama?>;
$(document).ready(function(){	
	load_index_all();
	tab=$("#tab").val();
	// alert($("#tab").val());
	set_tab_utama(tab_utama);
	// cek_login();
	// set_tab(tab);
});
function refresh_all(){
	load_index_all();
}
function set_tab_utama($tab){
	// alert('sini');
	tab_utama=$tab;
	$(".div_index").show();
	$("#div_penjualan").hide();
	document.getElementById("div_11").classList.remove("active");
	document.getElementById("div_22").classList.remove("active");
	document.getElementById("div_33").classList.remove("active");
	document.getElementById("div_44").classList.remove("active");
	document.getElementById("div_55").classList.remove("active");
	document.getElementById("div_66").classList.remove("active");
	
	if (tab_utama=='11'){
		document.getElementById("div_11").classList.add("active");
	}
	if (tab_utama=='22'){
		document.getElementById("div_22").classList.add("active");
	}
	if (tab_utama=='33'){
		document.getElementById("div_33").classList.add("active");
	}
	if (tab_utama=='44'){
		document.getElementById("div_44").classList.add("active");
	}
	if (tab_utama=='55'){
		document.getElementById("div_55").classList.add("active");
	}
	if (tab_utama=='66'){
		document.getElementById("div_66").classList.add("active");
		$(".div_index").hide();
		$("#div_penjualan").show();
	}
	if (tab_utama<66){
		load_index_all();
		
	}
}
function set_petugas_etiket(assesmen_id,tipe){
	$("#assesmen_id").val(assesmen_id);
	$("#tipe").val(tipe);
	$("#mppa_id").select2({
		dropdownParent: $("#modal_panel_kendal")
	  });
	  $("#judul_panel_kendali").text('PEMBERI E-TICKET');
	  $("#label_keterangan_panel_kendali").text('DIBERIKAN ETIKET OLEH');
	$("#modal_panel_kendal").modal('show');
}
function set_petugas_pengambil(assesmen_id,tipe){
	$("#assesmen_id").val(assesmen_id);
	$("#tipe").val(tipe);
	$("#mppa_id").select2({
		dropdownParent: $("#modal_panel_kendal")
	  });
	  $("#judul_panel_kendali").text('PENGAMBIL OBAT');
	  $("#label_keterangan_panel_kendali").text('OBAT DIAMBIL OLEH');
	$("#modal_panel_kendal").modal('show');
}
function simpan_set(){
	let assesmen_id=$("#assesmen_id").val();
	let tipe=$("#tipe").val();
	let mppa_id=$("#mppa_id").val();
	if (mppa_id==null || mppa_id==''){
		sweetAlert("Maaf...", "Petugas harus diisi !", "error");
		return false;
	}
	$("#cover-spin").show();
	$("#modal_panel_kendal").modal('hide');
	 $.ajax({
		url: '{site_url}Tfarmasi_tindakan_ri/simpan_set',
		type: 'POST',
		dataType: "json",
		data: {
			tipe: tipe,
			assesmen_id: assesmen_id,
			mppa_id: mppa_id,
		
		},
		success: function(data) {
			
			$("#cover-spin").hide();
			if (data==false){
				swal({
					title: "Maaf!",
					text: "No Antrian Tidak tersedia",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				load_index_all();
			}
		}
	});
}
function batalkan_transaksi(assesmen_id,status_tindakan,idpenjualan){
	//2:Proses;3:validasi
	let label_tindakan='';
	if (status_tindakan=='2'){
		label_tindakan=' Batalkan Transaksi ? Tindakan ini akan mengembalikan Stok Barang';
	}
	if (status_tindakan=='3'){
		label_tindakan=' Batalkan Validasi ? Tindakan ini hanya mengembalikan Status Tindakan';
	}
	swal({
		title: "Apakah Anda Yakin ?",
		text : label_tindakan,
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$.ajax({
			url: '{site_url}Tfarmasi_tindakan_ri/batalkan_transaksi',
			type: 'POST',
			dataType: "json",
			data: {
				status_tindakan: status_tindakan,
				assesmen_id: assesmen_id,
				idpenjualan: idpenjualan,
			
			},
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==false){
					swal({
						title: "Maaf!",
						text: "No Antrian Tidak tersedia",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});
				}else{
					load_index_all();
				}
			}
		});
	});

}

function set_tab($tab){
	tab=$tab;
	// alert($tab);
	$("#cover-spin").show();
	document.getElementById("div_1").classList.remove("active");
	document.getElementById("div_2").classList.remove("active");
	document.getElementById("div_3").classList.remove("active");
	document.getElementById("div_4").classList.remove("active");
		$("#status_transaksi").removeAttr("disabled");
		$("#status_reservasi").removeAttr("disabled");
	if (tab=='1'){
		document.getElementById("div_1").classList.add("active");
		$("#status_transaksi").val('1').trigger('change');
		$("#status_transaksi").attr('disabled', 'disabled');
	}
	if (tab=='2'){
		document.getElementById("div_2").classList.add("active");
		$("#status_transaksi").val(2).trigger('change');
		// $("#status_transaksi").removeAttr("disabled");
		$("#status_transaksi").attr('disabled', 'disabled');
	}
	if (tab=='3'){
		$("#status_transaksi").val('3').trigger('change');
		document.getElementById("div_3").classList.add("active");
		$("#status_transaksi").attr('disabled', 'disabled');
	}
	if (tab=='4'){
		document.getElementById("div_4").classList.add("active");
		$("#status_transaksi").val('4').trigger('change');
		// $("#status_reservasi").val('0').trigger('change');
		$("#status_transaksi").attr('disabled', 'disabled');
	}
	load_index_all();
}
function call_antrian_manual(antrian_id){
	let tujuan_id=$("#tujuan_id").val();
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}Tfarmasi_tindakan_ri/call_antrian',
		type: 'POST',
		dataType: "json",
		data: {
			tujuan_id: tujuan_id,
			assesmen_id: antrian_id,
		
		},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==false){
				swal({
					title: "Maaf!",
					text: "No Antrian Tidak tersedia",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				load_index_all();
			}
		}
	});
}
function call_lewati(antrian_id,antrian_id_next){
	let tujuan_id=$("#tujuan_id").val();
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}Tfarmasi_tindakan_ri/call_lewati',
		type: 'POST',
		dataType: "json",
		data: {
			tujuan_id: tujuan_id,
			assesmen_id: antrian_id,
			assesmen_id_next: antrian_id_next,
		
		},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==false){
				swal({
					title: "Maaf!",
					text: "No Antrian Tidak tersedia",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				load_index_all();
			}
		}
	});
}
function recall_manual(antrian_id){
	let tujuan_id=$("#tujuan_id").val();
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}Tfarmasi_tindakan_ri/call_antrian',
		type: 'POST',
		dataType: "json",
		data: {
			tujuan_id: tujuan_id,
			assesmen_id: antrian_id,
		
		},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==false){
				swal({
					title: "Maaf!",
					text: "No Antrian Tidak tersedia",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				load_index_all();
			}
		}
	});
}
$(document).on("click","#btn_filter_all",function(){
	refresh_all();
});
$(document).on("change","#tanggal",function(){
	load_index_all();
});
$(document).on("change","#tujuan_farmasi_id",function(){
	if ($("#tujuan_farmasi_id").val()!='#'){
		$.ajax({
			url: '{site_url}Tfarmasi_tindakan_ri/get_tujuan_spesifik',
			method: "POST",
			dataType: "json",
			data: {
				"tujuan_farmasi_id": $("#tujuan_farmasi_id").val()
			},
			success: function(data) {
				$("#tujuan_id_spesifik").val(data);
			},
			error: function(data) {
				console.log(data);
			}
		});
	}
});

function get_dokter(){
	var idpoli = $("#idpoli").val();
	$("#iddokter").empty();

	$.ajax({
		url: '{site_url}Tfarmasi_tindakan_ri/get_dokter_poli',
		method: "POST",
		dataType: "json",
		data: {
			"idpoli": idpoli
		},
		success: function(data) {
			
			$("#iddokter").append(data);
			// for (var i = 0; i < data.length; i++) {
					// $("#iddokter").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
			// }

			// $("#iddokter").selectpicker("refresh");
			// if ($("#status_cari").val()=='1'){
				// $('#kabupaten1').val($("#ckabupaten").val()).trigger('change');
			// }
		},
		error: function(data) {
			console.log(data);
		}
	});
}
function load_index_all(){
	$('#index_all').DataTable().destroy();	
	let idpoli=$("#idpoli").val();
	let iddokter=$("#iddokter").val();
	let tujuan_farmasi_id=$("#tujuan_farmasi_id").val();
	let no_medrec=$("#no_medrec").val();
	let namapasien=$("#namapasien").val();
	let tanggal=$("#tanggal").val();
	let tanggal_2=$("#tanggal_2").val();
	let st_login=$("#st_login").val();
	let str_array=$("#tujuan_array").val();
	
	// alert(tujuan_farmasi_id);
	table = $('#index_all').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			
			"processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			 "columnDefs": [{
                "width": "5%",
                "targets": 0
            },
            {
                "width": "30%",
                "targets": 1
            },
            {
                "width": "30%",
                "targets": 2
            },
            {
                "width": "30%",
                "targets": 3
            },
            {
                "width": "5%",
                "targets": 4
            },

        ],
            ajax: { 
                url: '{site_url}Tfarmasi_tindakan_ri/getIndex_all', 
                type: "POST" ,
                dataType: 'json',
				data : {
						idpoli:idpoli,
						iddokter:iddokter,
						tujuan_farmasi_id:tujuan_farmasi_id,
						tab:tab,
						no_medrec:no_medrec,
						namapasien:namapasien,
						tanggal:tanggal,
						tanggal_2:tanggal_2,
						st_login:st_login,
						str_array:str_array,
						tab_utama:tab_utama,
						
						
					   }
            },
			"drawCallback": function(settings) {
				$("#index_all thead").remove();
				$("#cover-spin").hide();
			}
        });
	$("#cover-spin").hide();
}
function load_penjualan(){
	$('#index_penjualan').DataTable().destroy();	
	let nopenjualan=$("#nopenjualan").val();
	let no_medrec=$("#nomedrec_penjualan").val();
	let namapasien=$("#namapasien_penjualan").val();
	let idasalpasien=$("#idasalpasien_penjualan").val();
	let tanggaldari=$("#tanggaldari").val();
	let tanggalsampai=$("#tanggalsampai").val();
	let idkelompokpasien=$("#idkelompokpasien_penjualan").val();
	let status=$("#status_penjualan").val();
	
	// alert(tujuan_farmasi_id);
	table = $('#index_penjualan').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			
			"processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [{
					"width": "5%",
					"targets": 0,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 1,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 2,
					"orderable": true
				},
				{
					"width": "15%",
					"targets": 3,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 4,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 5,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 6,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 7,
					"orderable": true
				}
			],
            ajax: { 
                url: '{site_url}Tfarmasi_tindakan_ri/getIndex_penjualan', 
                type: "POST" ,
                dataType: 'json',
				data : {
						nopenjualan:nopenjualan,
						no_medrec:no_medrec,
						namapasien:namapasien,
						idasalpasien:idasalpasien,
						tanggaldari:tanggaldari,
						tanggalsampai:tanggalsampai,
						idkelompokpasien:idkelompokpasien,
						status:status,
					   }
            },
			"drawCallback": function(settings) {
				$("#cover-spin").hide();
			}
        });
	$("#cover-spin").hide();
}
 
	
</script>