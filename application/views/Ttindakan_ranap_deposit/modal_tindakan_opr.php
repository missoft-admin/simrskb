<div class="modal fade in black-overlay" id="modal_tindaka_opr" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Persetujuan Tindakan</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('#','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12">
							<table width="100%" id="tabel_tindakan_opr" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th style="width:15%" class="text-center">Tanggal Tindakan</th>
										<th style="width:20%" class="text-center">Nama Tindakan</th>
										<th style="width:15%" class="text-center">Total Deposit</th>
										<th style="width:15%" class="text-center">Total Deposit ALL</th>
										<th style="width:20%" class="text-center">Approved By</th>
										<th style="width:15%" class="text-center">Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

					</div>
					<?php echo form_close() ?>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
var table;
var myDropzone 

function show_list_tindakan(id){
	$("#modal_tindaka_opr").modal('show');
	$("#cover-spin").show();
	$('#tabel_tindakan_opr tbody').empty();
	$.ajax({
		url: '{site_url}ttindakan_ranap_deposit/list_tindakan_opr/'+id,
		dataType: "json",
		success: function(data) {
			if (data.detail!=''){
				$("#cover-spin").hide();
				$('#tabel_tindakan_opr tbody').empty();
				$("#tabel_tindakan_opr tbody").append(data.detail);
			}
			// console.log();
			
		}
	});
}
</script>
