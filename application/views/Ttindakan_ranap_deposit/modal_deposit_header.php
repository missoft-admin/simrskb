<input type="hidden" id="st_upload" name="st_upload" value="<?=(UserAccesForm($user_acces_form,array('2619'))?'1':'0')?>">
<input type="hidden" id="head_id" name="head_id" value="">
<input type="hidden" id="idpasien" name="idpasien" value="166">
<input type="hidden" id="pendaftaran_id_pilih" name="pendaftaran_id_pilih" value="">
<input type="hidden" id="perencanaan_id" name="perencanaan_id" value="">
<div class="modal in" id="Modal_Deposit_header" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width:80%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
                    <h3 class="block-title titleRincian">TRANSAKSI DEPOSIT</h3>
                </div>
                <div class="block-content" >
					<form class="form-horizontal" action="#" method="post" enctype="multipart/form-data" onsubmit="return false;">
						<input type="hidden" id="st_setuju_tanpa_deposit" name="" value="{st_setuju_tanpa_deposit}">
						<input type="hidden" id="idrawatinap_header" name="" value="">
						<input type="hidden" id="idkelas_header" name="" value="">
						<input type="hidden" id="idheader" name="" value="">
						<div class="row">
						<div class="col-xs-12">
							<div class="form-group" style="margin-bottom:5px">
								<div class="col-xs-12">
									<h5 class="text-center">TRANSAKSI DEPOSIT</h5>
								</div>
							</div>
							
							<div class="form-group" style="margin-bottom:-5px">
								<div class="col-xs-12">
									<label for="header_nama" class="text-primary">DATA PASIEN</label>
								</div>
								
							</div>
							<div class="form-group" style="margin-bottom:5px">
								<div class="col-xs-3">
									<label for="header_nama">Pasien</label>
									<input class="form-control" disabled type="text" id="header_nama" placeholder="">
								</div>
								<div class="col-xs-3">
									<label for="header_umur">Tanggal Lahir / Umur</label>
									<input class="form-control" disabled type="text" id="header_umur" placeholder="">
								</div>
								<div class="col-xs-3">
									<label for="header_kelompok">Kelompok Pasien</label>
									<input class="form-control" disabled type="text" id="header_kelompok" placeholder="">
								</div>
								<div class="col-xs-3">
									<label for="header_nama_asuransi">Asuransi</label>
									<input class="form-control" disabled type="text" id="header_nama_asuransi" placeholder="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom:5px">
								<div class="col-xs-3">
									<label for="header_dpjp">DPJP</label>
									<input class="form-control" disabled type="text" id="header_dpjp" placeholder="">
								</div>
								<div class="col-xs-3">
									<label for="header_jenis_pelayanan">Jenis Pelayanan</label>
									<input class="form-control" disabled type="text" id="header_jenis_pelayanan" placeholder="">
								</div>
								<div class="col-xs-3">
									<label for="header_ruangan">Ruangan</label>
									<input class="form-control" disabled type="text" id="header_ruangan" placeholder="">
								</div>
								<div class="col-xs-3">
									<label for="header_masuk">Tanggal masuk</label>
									<input class="form-control" disabled type="text" id="header_masuk" placeholder="">
								</div>
							</div>
							
							<div class="form-group" style="margin-bottom:5px">
								<div class="col-xs-12">
									<label for="header_nama" class="text-primary">TRANSAKSI DEPOSIT</label>
								</div>
							</div>
							<div class="form-group" style="margin-bottom:5px">
								<div class="col-xs-3">
									<label for="header_dpjp">TIPE</label>
									<select id="header_tipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0">-Pilih Tipe-</option>
										<option value="1">Pembedahan</option>
										<option value="2">Konservatif</option>
										
									</select>
								</div>
								<div class="col-xs-6">
									<label for="header_jenis_pelayanan">Data Estimasi Biaya</label>
									<div class="input-group">
									<input class="form-control" disabled type="text" id="header_estimasi" placeholder="">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" onclick="load_estimasi()" id="btn_load_estimasi"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</div>
								<div class="col-xs-3">
								</div>
								<div class="col-xs-3">
									<label for="header_masuk">Deskripsi</label>
									<input class="form-control" disabled type="text" id="header_deskripsi" placeholder="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom:5px">
								<div class="col-xs-3">
									<label for="header_diagnosa">Diagnosa</label>
									<input class="form-control" disabled type="text" id="header_diagnosa" placeholder="">
								</div>
								<div class="col-xs-3">
									<label for="header_jenis_pelayanan">Tindakan</label>
									<input class="form-control" disabled type="text" id="header_tindakan" placeholder="">
								</div>
								<div class="col-xs-3">
									<label for="header_ruangan">Total Estimasi Biaya</label>
									<input class="form-control number" disabled type="text" id="header_total_estimasi" placeholder="">
								</div>
								<div class="col-xs-3">
									<label for="header_masuk">Nilai Acuan Minimal Deposit Awal</label>
									<input class="form-control number" disabled type="text" id="header_nilai_acuan" placeholder="">
								</div>
							</div>
							<div class="form-group div_add_tindakan" >
								<div class="col-xs-12">
									<button class="btn btn-sm btn-primary" onclick="create_tindakan_new()" type="button" ><i class="fa fa-save"></i> SIMPAN TINDAKAN</button>
								</div>
							</div>
							<div class="form-group div_add_pembayaran" style="margin-bottom:5px">
								<div class="col-xs-12">
									<label for="header_nama" class="text-primary">PEMBAYARAN</label>
								</div>
							</div>
							<div class="form-group div_add_pembayaran" style="margin-bottom:5px">
								<div class="col-xs-6">
									<label for="header_tindakan_id">Tipe Deposit</label>
									<select id="header_tindakan_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										
									</select>
								</div>
								<div class="col-xs-3">
									<label for="header_jenis_pelayanan">Total Deposit Diproses</label>
									<input class="form-control number" disabled type="text" id="header_total_diproses" placeholder="">
								</div>
								<div class="col-xs-3">
									<label for="header_ruangan">Sisa Deposit Acuan</label>
									<input class="form-control number" disabled type="text" id="header_sisa_acuan" placeholder="">
								</div>
							</div>
							<div class="form-group div_add_pembayaran" >
								<div class="col-xs-12">
									<?php if (UserAccesForm($user_acces_form,array('2617'))){?>
									<button class="btn btn-sm btn-success" onclick="add_pembayaran()" type="button" ><i class="fa fa-credit-card"></i> PEMBAYARAN DEPOSIT</button>
									<?}?>
									<?php if (UserAccesForm($user_acces_form,array('2616'))){?>
									<button class="btn btn-sm btn-warning" onclick="list_transaksi()" type="button" ><i class="fa fa-list-ul"></i> LIST TRANSAKSI</button>
									<?}?>
									<?php if (UserAccesForm($user_acces_form,array('2618'))){?>
									<button class="btn btn-sm btn-danger btn_batal_header" onclick="hapus_header_deposit()" type="button" ><i class="fa fa-trash"></i> BATAL TINDAKAN</button>
									<?}?>
								</div>
							</div>
							<div class="form-group div_add_pembayaran" >
								<div class="col-xs-12">
									<table id="historyDeposit" class="table table-bordered table-striped" style="margin-top: 10px;">
										<thead>
											<tr>
												<th style="width:10%">Action</th>
												<th style="width:8%">Tipe Deposit</th>
												<th style="width:10%">No Transaksi</th>
												<th style="width:8%">Jenis Deposit</th>
												<th style="width:8%">Tanggal Deposit</th>
												<th style="width:8%">Metode</th>
												<th style="width:9%">Bank</th>
												<th style="width:10%">Nominal</th>
												<th style="width:10%">Terima Dari</th>
												<th style="width:10%">Upload</th>
												<th style="width:10%">Status</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
										<tfoot>
											<tr style="background-color:white;">
												<td colspan="4" style="text-align:right;text-transform:uppercase;"><b>Total</b></td>
												<td id="depositTotal" style="font-weight:bold;"></td>
												<td></td>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
						</div>
					</form>
					<div class="clearfix"></div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
				</div>
		</div>
	</div>
</div>
</div>
<div class="modal in" id="Modal_Estimasi" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width:80%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button onclick="hide_estimasi()" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
                    <h3 class="block-title titleRincian">DATA ESTIMASI BIAYA</h3>
                </div>
                <div class="block-content" style="margin-bottom: 10px;padding-top: 10px;">
					
					<div class="form-group">
						<div class="col-md-12">                               
							<div class="table-responsive">
								<table class="table table-bordered" id="index_estimasi">
									<thead>
										<tr>
											<th style="width:5%">Action</th>
											<th style="width:10%">Nominal Estimasi</th>
											<th style="width:15%">No. Reg Estimasi</th>
											<th style="width:15%">Diagnosa</th>
											<th style="width:15%">Nama Tindakan</th>
											<th style="width:15%">Dokter</th>
											<th style="width:15%">Status</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
					
					<div class="clearfix"></div>
				
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" onclick="hide_estimasi()"><i class="fa fa-close"></i> Close</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal in" id="RincianModalDeposit" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popout" style="width:40%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button onclick="hide_pembayaran()" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
                    <h3 class="block-title titleRincian">PEMBAYARAN DEPOSIT PASIEN</h3>
                </div>
                <div class="block-content" style="margin-bottom: 10px;padding-top: 10px;">
					<div class="form-group" style="margin-bottom:8px">
						<label class="col-md-4 control-label" for="">Tipe</label>
						<div class="col-md-8" style="margin-bottom: 5px;">
							<input type="text" disabled class="form-control" id="depositTipe" placeholder="Tipe" value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 8px;">
						<label class="col-md-4 control-label">Jenis Deposit</label>
						<div class="col-md-8" style="margin-bottom: 5px;">
							<select class="js-select2 form-control" id="depositJenis" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="" selected></option>
								<?foreach(get_all('mdeposit_jenis') as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					<div class="form-group div_awal" style="margin-bottom:8px">
						<label class="col-md-4 control-label" for="">Nominal Acuan</label>
						<div class="col-md-8" style="margin-bottom: 5px;">
							<input type="text" disabled class="form-control number" id="depositAcuan" placeholder="Acuan" value="">
						</div>
					</div>
					<div class="form-group div_awal" style="margin-bottom:15px">
						<label class="col-md-4 control-label" for="">Deposit Sudah Diterima</label>
						<div class="col-md-8" style="margin-bottom: 5px;">
							<input type="text" disabled class="form-control number" id="depositTerima" placeholder="Diterima" value="">
						</div>
					</div>
					<div class="form-group div_awal" style="margin-bottom:8px">
						<label class="col-md-4 control-label" for="">Sisa Deposit Acuan</label>
						<div class="col-md-8" style="margin-bottom: 5px;">
							<input type="text" disabled class="form-control number" id="depositSisa" placeholder="Sisa" value="">
						</div>
					</div>
					<div class="form-group div_lanjutan" style="margin-bottom:8px">
						<label class="col-md-4 control-label" for="">Total Transaksi Saat Ini</label>
						<div class="col-md-8" style="margin-bottom: 5px;">
							<input type="text" disabled class="form-control number" id="depositAcuanRanap" placeholder="Total Transaksi" value="">
						</div>
					</div>
					<div class="form-group div_lanjutan" style="margin-bottom:15px">
						<label class="col-md-4 control-label" for="">Deposit Sudah Diterima</label>
						<div class="col-md-8" style="margin-bottom: 5px;">
							<input type="text" disabled class="form-control number" id="depositTerimaAll" placeholder="Diterima" value="">
						</div>
					</div>
					<div class="form-group div_lanjutan" style="margin-bottom:8px">
						<label class="col-md-4 control-label" for="">Sisa Deposit dari Transaksi</label>
						<div class="col-md-8" style="margin-bottom: 5px;">
							<input type="text" disabled class="form-control number" id="depositSisatrx" placeholder="Sisa" value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 8px;">
						<label class="col-md-4 control-label">Tanggal</label>
						<div class="col-md-8" style="margin-bottom: 5px;">
							<input type="text" id="depositTanggal" <?=($st_edit_deposit=='0'?'disabled':'')?> class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="Tanggal" value="<?=date("d/m/Y")?>">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 8px;">
						<label class="col-md-4 control-label">Metode Pembayaran</label>
						<div class="col-md-8" style="margin-bottom: 5px;">
							<select class="js-select2 form-control" id="depositIdMetodePembayaran" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value=""></option>
								<option value="1">Tunai</option>
								<option value="2">Debit</option>
								<option value="3">Kredit</option>
								<option value="4">Transfer</option>
							</select>
						</div>
					</div>
					<div class="form-group" style="display: none;margin-bottom:8px;" id="depositMetodeBank">
						<label class="col-md-4 control-label">Pembayaran</label>
						<div class="col-md-8" style="margin-bottom: 5px;">
							<select class="js-select2 form-control" id="depositIdBank" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="" selected>Pilih Opsi</option>
								<?foreach(get_all('mbank',array('status'=>1)) as $r){?>
								<option value="<?=$r->id?>"><?=$r->nama?></option>
								
								<?}?>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom:8px">
						<label class="col-md-4 control-label" for="">Nominal</label>
						<div class="col-md-8" style="margin-bottom: 5px;">
							<input type="text" class="form-control number" id="depositNominal" placeholder="Nominal" value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom:8px">
						<label class="col-md-4 control-label" for="">Terima Dari</label>
						<div class="col-md-8" style="margin-bottom: 5px;">
							<input type="text" class="form-control" id="depositTerimaDari" placeholder="Terima Dari" value="">
						</div>
					</div>
					<input type="hidden" class="form-control" id="edit_id" value="">
					<div class="clearfix"></div>
				
				</div>
				<div class="modal-footer">
					<?if ($st_proses_selesai_deposit=='1'){?>
					<button class="btn btn-sm btn-primary" type="button" onclick="simpan_deposit(1)"><i class="fa fa-check-square-o"></i> Proses Selesai</button>
					<?}?>
					<?if ($st_draft_deposit=='1'){?>
					<button class="btn btn-sm btn-success" type="button" onclick="simpan_deposit(0)"><i class="fa fa-save"></i> Proses</button>
					<?}?>
					<button class="btn btn-sm btn-default" type="button" onclick="hide_pembayaran()"><i class="fa fa-close"></i> Close</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="AlasanHapusDepositModal" role="dialog" style="text-transform:uppercase;">
    <div class="modal-dialog modal-dialog-popout" style="margin-top: 150px; padding: 0 !important;">
        <div class="modal-content">
            <div class="block block-themed remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Alasan Hapus</h3>
                </div>
            </div>
            <div class="col-xs-12" style="margin-top: 10px; margin-bottom: 10px;">
				<input type="hidden" class="form-control" id="alasanhapus_id" value="">
				<input type="text" class="form-control" id="alasanhapus" value="">
                
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-danger" onclick="hapus_deposit()" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in black-overlay" id="modal_approval" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12">
						<input type="hidden" class="form-control" id="idtrx_approval" placeholder="" name="idtrx_approval" value="">
						<div class="form-group" style="margin-bottom: 18px;">
						<div class="col-md-12">
							<label class="control-label" for="">Alasan Hapus</label>
							<input type="text" class="form-control" id="alasanhapus_2" value="">
						</div>
						</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group" style="margin-bottom: 8px;">
						<div class="col-md-12">
						<div class="col-md-12">
							<table width="100%" id="tabel_user" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th style="width:20%">Step</th>
										<th style="width:30%">Deskripsi</th>
										<th style="width:20%">User</th>
										<th style="width:15%">Jika Setuju</th>
										<th style="width:15%">Jika Menolak</th>
									</tr>
									
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						</div>
						</div>

					</div>
					<?php echo form_close() ?>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-primary" id="btn_simpan_approval" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Proses Persetujuan</button>
			</div>

		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_user" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12">
							<table width="100%" id="tabel_user_proses" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th style="width:25%">Step</th>
										<th style="width:25%">Deskripsi</th>
										<th style="width:25%">User</th>
										<th style="width:25%">Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

					</div>
					<?php echo form_close() ?>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_tindakan" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Persetujuan Tindakan</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('#','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12">
							<table width="100%" id="tabel_tindakan" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th style="width:15%" class="text-center">Tanggal Tindakan</th>
										<th style="width:20%" class="text-center">Nama Tindakan</th>
										<th style="width:15%" class="text-center">Total Deposit</th>
										<th style="width:15%" class="text-center">Total Deposit ALL</th>
										<th style="width:20%" class="text-center">Approved By</th>
										<th style="width:15%" class="text-center">Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

					</div>
					<?php echo form_close() ?>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_setuju_tindakan" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Persetujuan Tindakan</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('#','class="form-horizontal" id="form-work"') ?>
					<div class="row">
					<?
						$tgl_setuju=date('d-m-Y H:i:s');
					?>
						<div class="col-md-12">
							<div class="form-group">
								<div class="col-md-12">
									<label class="control-label" for="nama">Tanggal Setuju</label>
									<div class="input-group date">
									<input class="js-datetimepicker form-control input" <?=($st_edit_tanggal_persetujuan=='0'?'disabled':'')?> type="text" id="tanggal_persetujuan_tindakan" value="<?=$tgl_setuju?>" />
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
									</div>
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<label class="control-label" for="nama">Nama Tindakan</label>
									<input type="text" readonly class="form-control" id="nama_tindakan_setuju" value="">
									<input type="hidden" readonly class="form-control" id="idpersetujuan" value="">
								</div>
								
							</div>
							
						</div>

					</div>
					<?php echo form_close() ?>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-primary" type="button" onclick="simpan_setuju_tindakan()"><i class="fa fa-save"></i> SIMPAN PERSETUJUAN</button>
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_bukti" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">UPLOAD BUKTI</h3>
				</div>
				<div class="block-content">
									<input type="hidden" class="form-control" name="idupload2" id="idupload2" value="" readonly>
					<?php if (UserAccesForm($user_acces_form,array('2619'))){ ?>
					<form action="{base_url}ttindakan_ranap_deposit/upload" enctype="multipart/form-data"  class="dropzone" id="image-upload">
						<div class="row" disabled>
							<div class="form-group" style="margin-bottom: 8px;">
								<div class="col-md-12">
									<input type="hidden" class="form-control" name="idupload" id="idupload" value="" readonly>
									<div>
									  <h5>Bukti Upload</h5>
									</div>
								</div>

							</div>
						</div>
					</form>
					<?}?>
					<br>
					<?php echo form_open('#','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="form-group" style="margin-bottom: 8px;">
							<div class="col-md-12">
							<div class="col-md-12">
								<table class="table table-bordered" id="list_file">
									<thead>
										<tr>
											<th width="50%" class="text-center">File</th>
											<th width="15%" class="text-center">Size</th>
											<th width="35%" class="text-center">User</th>
											<th width="35%" class="text-center">X</th>
											
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
							</div>
						</div>

					</div>
					<?php echo form_close() ?>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>
<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
var table;
var myDropzone 
$(document).ready(function() {
	$('.number').number(true, 0, '.', ',');
	 $(".js-datetimepicker").datetimepicker({
		format: "DD-MM-YYYY HH:mm:ss",
		// stepping: 30
	});
	if ($("#st_upload").val()=='1'){
		
	Dropzone.autoDiscover = false;
		myDropzone = new Dropzone(".dropzone", { 
		   autoProcessQueue: true,
		   maxFilesize: 30,
		});
		myDropzone.on("complete", function(file) {
		  
		  refresh_image();
		  list_deposit();
		  myDropzone.removeFile(file);
		  
		});
	}
	$(document).on("change","#header_tipe",function(){
		load_tipe();
		
	});
	$(document).on("change","#header_tindakan_id",function(){
		let head_id=$("#header_tindakan_id").val();
		let id=$("#idrawatinap_header").val();
		load_data_head(head_id,id);
		list_deposit();
	});
	$(document).on("change","#depositJenis",function(){
		pilih_jenis_deposit();
	});
});
function show_modal_persetujuan(id){
	$("#idrawatinap_header").val(id);
	$("#modal_tindakan").modal({backdrop: 'static', keyboard: false}).show();
	list_tindakan(id);
}
function list_tindakan(id){
	$("#cover-spin").show();
	$('#tabel_tindakan tbody').empty();
	$.ajax({
		url: '{site_url}ttindakan_ranap_deposit/list_tindakan/'+id,
		dataType: "json",
		success: function(data) {
			if (data.detail!=''){
				$("#cover-spin").hide();
				$('#tabel_tindakan tbody').empty();
				$("#tabel_tindakan tbody").append(data.detail);
			}
			// console.log();
			
		}
	});
}
function show_bukti(id){
	$("#idupload2").val(id);
	$("#idupload").val(id);
	$("#modal_bukti").modal({backdrop: 'static', keyboard: false}).show();
	refresh_image();
}
function show_setuju_tindakan(id){
	
	$("#idpersetujuan").val(id);
	$("#modal_setuju_tindakan").modal({backdrop: 'static', keyboard: false}).show();
	$.ajax({
		url: '{site_url}ttindakan_ranap_deposit/get_data_tindakan', 
		dataType: "JSON",
		method: "POST",
		data : {
				idpersetujuan:$("#idpersetujuan").val(),
			   },
		success: function(data) {
			$("#nama_tindakan_setuju").val(data.nama_tindakan);
		}
	});
}
function simpan_setuju_tindakan(){
	
	let id=$("#idpersetujuan").val();
	let setuju_date=$("#tanggal_persetujuan_tindakan").val();
	$("#cover-spin").show();
	// $("#modal_setuju_tindakan").modal({backdrop: 'static', keyboard: false}).show();
	$.ajax({
		url: '{site_url}ttindakan_ranap_deposit/simpan_setuju_tindakan', 
		dataType: "JSON",
		method: "POST",
		data : {
				id:id,
				setuju_date:setuju_date,
			   },
		success: function(data) {
			$("#cover-spin").hide();
			$("#modal_setuju_tindakan").modal('hide');
			list_tindakan($("#idrawatinap_header").val());
			$("#idrawatinap_header").val('');
			load_filter();
		}
	});
}
function refresh_image(){
	var id=$("#idupload2").val();
	$('#list_file tbody').empty();
	$.ajax({
		url: '{site_url}ttindakan_ranap_deposit/refresh_image/'+id,
		dataType: "json",
		success: function(data) {
			if (data.detail!=''){
				$('#list_file tbody').empty();
				$("#box_file2").attr("hidden",false);
				$("#list_file tbody").append(data.detail);
			}
			// console.log();
			
		}
	});
}
$(document).on("click",".hapus_file",function(){	
	var currentRow=$(this).closest("tr");          
	 var id=currentRow.find("td:eq(4)").text();
	 $.ajax({
		url: '{site_url}ttindakan_ranap_deposit/hapus_file',
		type: 'POST',
		data: {id: id},
		success : function(data) {				
			$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
			currentRow.remove();
			list_deposit();
			// table.ajax.reload( null, false );				
		}
	});
});
function load_user_approval(id){
	// modal_approval
	$("#idtrx_approval").val(id);
	$("#btn_simpan_approval").attr('disabled',true);
	$("#modal_approval").modal({backdrop: 'static', keyboard: false}).show();
	$('#tabel_user').DataTable().destroy();
	table=$('#tabel_user').DataTable({
		"autoWidth": false,
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"order": [],
		"ajax": {
			url: '{site_url}ttindakan_ranap_deposit/load_user_approval',
			type: "POST",
			dataType: 'json',
			data : {
				id:id,

			   }
		},
		"columnDefs": [
			 // {  className: "text-right", targets:[0] },
			 {  className: "text-center", targets:[1,2,3,4] },
			 // { "width": "10%", "targets": [0] },
			 // { "width": "40%", "targets": [1] },
			 // { "width": "25%", "targets": [2,3] },

		]
	});
	setTimeout(function() {
	  validate_approval();
	}, 1500);

}
function validate_approval(){
	var table = $('#tabel_user').dataTable();
	 //Get the total rows
	 if (table.fnGetData().length > 0){
		 $("#btn_simpan_approval").attr('disabled',false);
	 }
}
function list_user(idtrx){
	$("#modal_user").modal('show');

	// alert(idrka);
	$.ajax({
		url: '{site_url}ttindakan_ranap_deposit/list_user/'+idtrx,
		dataType: "json",
		success: function(data) {
			$("#tabel_user_proses tbody").empty();
			$("#tabel_user_proses tbody").append(data.detail);
		}
	});
}
$(document).on("click","#btn_simpan_approval",function(){
	let text = $("#alasanhapus_2").val();
	let length = text.length;
	if (length < 10) {
		sweetAlert("Maaf...", "Tentukan Alasan Minimal 10 Karakter!", "error");
		$("#modal_approval").modal({backdrop: 'static', keyboard: false}).show();
		$('#alasanhapus').focus();
		return false;
	}
		
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Melanjutkan Step Persetujuan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#d26a5c",
		cancelButtonText: "Tidak Jadi",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}ttindakan_ranap_deposit/simpan_proses_peretujuan/',
			type: "POST",
			dataType: 'json',
			data : {
				id:$("#idtrx_approval").val(),
				alasanhapus:$("#alasanhapus_2").val(),

			   },
			complete: function() {
				$("#cover-spin").hide();
				swal({
					title: "Berhasil!",
					text: "Proses Approval Berhasil.",
					type: "success",
					timer: 1500,
					showConfirmButton: false
				});
				list_deposit();
				load_filter();
			}
		});
	});

	return false;
});
function pilih_jenis_deposit(){
	let depositJenis=$("#depositJenis").val();
	if (depositJenis=='1'){//awal
		$(".div_awal").show();
		$(".div_lanjutan").hide();
		load_awal();
	}else{
		$(".div_awal").hide();
		$(".div_lanjutan").show();
		get_total_keseluruhan_rannap();
	}
}
function hide_pembayaran(){
	$("#edit_id").val('');
	$("#RincianModalDeposit").modal('hide');
	$("#Modal_Deposit_header").modal('show');
}
function add_pembayaran(){
	$("#RincianModalDeposit").modal({backdrop: 'static', keyboard: false}).show();
	$("#Modal_Deposit_header").modal('hide');
	$("#depositTipe").val($("#header_tindakan_id option:selected").text());
	$("#depositJenis").val(1).trigger('change');
	load_awal();
	
}
function load_awal(){
	$("#depositAcuan").val($("#header_nilai_acuan").val());
	$("#depositTerima").val($("#header_total_diproses").val());
	$("#depositSisa").val($("#header_sisa_acuan").val());
	if ($("#depositSisa").val()>0){
		$("#depositNominal").val($("#depositSisa").val());
	}else{
		
		$("#depositNominal").val(0);
	}
}
function get_total_keseluruhan_rannap(){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}ttindakan_ranap_deposit/get_total_keseluruhan_rannap', 
		dataType: "JSON",
		method: "POST",
		data : {
				idrawatinap:$("#idrawatinap_header").val(),
			   },
		success: function(data) {
			$("#cover-spin").hide();
			$("#depositAcuanRanap").val(data.totalkeseluruhan);
			$("#depositTerimaAll").val($("#deposit_diterima_all").val());
			$("#depositSisatrx").val(data.totalkeseluruhan- parseFloat($("#depositTerimaAll").val()));
			if ($("#edit_id").val()==''){
				
			$("#depositNominal").val($("#depositSisatrx").val());
			}
		}
	});
	
}
function edit_deposit(id,head_id){
	$("#edit_id").val(id);
	$("#header_tindakan_id").val(head_id).trigger('change');
	$("#head_id").val(head_id);
	$.ajax({
		url: '{site_url}ttindakan_ranap_deposit/edit_deposit', 
		dataType: "JSON",
		method: "POST",
		data : {
				id:id,
			   },
		success: function(data) {
			$("#cover-spin").hide();
			// alert(data.idmetodepembayaran);
			$("#Modal_Deposit_header").modal('hide');
			$("#RincianModalDeposit").modal({backdrop: 'static', keyboard: false}).show();
			
			$("#depositTipe").val(data.deskripsi);
			$("#depositTanggal").val(data.tanggal);
			$("#depositTerimaDari").val(data.terimadari);
			$("#depositIdMetodePembayaran").val(data.idmetodepembayaran).trigger('change');
			$("#depositIdBank").val(data.idbank).trigger('change');
			$("#depositNominal").val(data.nominal);
			$("#depositJenis").val(data.jenis_deposit_id).trigger('change');
			setTimeout(function() {
				$("#depositAcuan").val($("#header_nilai_acuan").val());
				$("#depositTerima").val($("#header_total_diproses").val());
				$("#depositSisa").val($("#header_sisa_acuan").val());
			}, 1000);


			
			
		}
	});
	
}

function simpan_deposit(st_proses){
	var idpendaftaran = $("#idrawatinap_header").val();
	var head_id = $('#header_tindakan_id').val();
	var tanggal = $('#depositTanggal').val();
	var idmetodepembayaran = $('#depositIdMetodePembayaran option:selected').val();
	var idbank = $('#depositIdBank option:selected').val();
	var nominal = $('#depositNominal').val();
	var terimadari = $('#depositTerimaDari').val();
	var jenis_deposit_id = $('#depositJenis').val();
	var edit_id = $('#edit_id').val();
	var idtipe = 1;
	
	if (tanggal == '') {
		sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
			$('#depositTanggal').focus();
		});
		return false;
	}

	if (idmetodepembayaran == '') {
		sweetAlert("Maaf...", "Metode Pembayaran belum dipilih !", "error").then((value) => {
			$('#depositIdMetodePembayaran').select2('open');
		});
		return false;
	}

	if (idmetodepembayaran != 1) {
		if (idbank == '') {
			sweetAlert("Maaf...", "Bank belum dipilih !", "error").then((value) => {
				$('#depositIdBank').select2('open');
			});
			return false;
		}
	}

	if (nominal == '' || nominal <= 0) {
		sweetAlert("Maaf...", "Nominal tidak boleh kosong !", "error").then((value) => {
			$('#depositNominal').focus();
		});
		return false;
	}
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}ttindakan_ranap_deposit/saveDataDeposit',
		method: 'POST',
		data: {
			idrawatinap: idpendaftaran,
			tanggal: tanggal,
			idmetodepembayaran: idmetodepembayaran,
			idbank: idbank,
			nominal: nominal,
			idtipe: idtipe,
			terimadari: terimadari,
			head_id: head_id,
			st_proses: st_proses,
			jenis_deposit_id: jenis_deposit_id,
			edit_id: edit_id,
		},
		success: function(data) {
			swal({
				title: "Berhasil!",
				text: "Data Deposit Telah Tersimpan.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});

			list_deposit();
			hide_pembayaran()
			$('#depositIdMetodePembayaran').select2("trigger", "select", {
				data: {
					id: '',
					text: ''
				}
			});
			$('#depositIdBank').select2("trigger", "select", {
				data: {
					id: '',
					text: ''
				}
			});
			$("#cover-spin").hide();
			list_deposit();
			$('#edit_id').val('');
			$('#depositNominal').val('');
			$('#depositTerimaDari').val('');
			load_filter();
			get_total_proses();
		}
	});
}
function show_hapus_biasa(id){
	$("#alasanhapus_id").val(id);
	$("#AlasanHapusDepositModal").modal({backdrop: 'static', keyboard: false}).show();
	$("#cover-spin").show();
	$.ajax({
			url: '{site_url}ttindakan_ranap_deposit/get_alasan', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					
				   },
			success: function(data) {
				$("#cover-spin").hide();
				$('#alasanhapus').val(data.alasanhapus);
			}
		});
	$('#alasanhapus').focus();
}
function proses_deposit(id){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Generate Kwitansi Deposit ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}ttindakan_ranap_deposit/proses_deposit', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					   },
				success: function(data) {
					$("#cover-spin").hide();
					list_deposit();
					get_total_proses();
					swal({
						title: "Berhasil!",
						text: "Proses.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

				}
			});
		});
}
function hapus_deposit(){
	let id=$("#alasanhapus_id").val();
	let text = $("#alasanhapus").val();
	let length = text.length;
	if (length < 10) {
		sweetAlert("Maaf...", "Tentukan Alasan Minimal 10 Karakter!", "error");
		$("#AlasanHapusDepositModal").modal({backdrop: 'static', keyboard: false}).show();
		$('#alasanhapus').focus();
		return false;
	}
		
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Deposit ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}ttindakan_ranap_deposit/hapus_deposit', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						alasanhapus:$("#alasanhapus").val(),
						
					   },
				success: function(data) {
					list_deposit();
					load_filter();
					$("#AlasanHapusDepositModal").modal('hide');
					$("#cover-spin").hide();
					swal({
						title: "Berhasil!",
						text: "Proses.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

				}
			});
		});

}
function hapus_header_deposit(){
	let id=$("#head_id").val();
	
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data Header / Tindakan Deposit ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}ttindakan_ranap_deposit/hapus_header_deposit', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						
					   },
				success: function(data) {
					load_filter();
					$("#Modal_Deposit_header").modal('hide');
				}
			});
		});

}
function list_deposit(){
	let head_id=$("#idrawatinap_header").val();
	$.ajax({
		url: '{site_url}ttindakan_ranap_deposit/list_deposit',
		dataType: "JSON",
		method: "POST",
		data : {
				head_id:head_id,
			   },
		success: function(data) {
			$("#historyDeposit tfoot").empty();
			$("#historyDeposit tbody").empty();
			$("#historyDeposit tbody").append(data.tabel);
			$("#historyDeposit tfoot").append(data.foot);
			
		}
	});

}
$(document).on('change', '#depositIdMetodePembayaran', function() {
        var idmetodepembayaran = $(this).val();

	if (idmetodepembayaran == 1) {
		$('#depositMetodeBank').hide();
	} else {

		// Get Bank
		// $.ajax({
			// url: '{site_url}trawatinap_tindakan/getBank',
			// dataType: 'json',
			// success: function(data) {
				// $("#depositIdBank").empty();
				// $("#depositIdBank").append("<option value=''>Pilih Opsi</option>");
				// for (var i = 0; i < data.length; i++) {
					// $("#depositIdBank").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
				// }
			// }
		// });

		$('#depositMetodeBank').show();
	}
});
function set_div_tindakan(st_add){
	if (st_add=='1'){//ADD
		$(".div_add_tindakan").show();
		$(".div_add_pembayaran").hide();
		$("#header_tipe").removeAttr("disabled");
		clear_data_awal();
		
	}else{
		$(".div_add_tindakan").hide();
		$(".div_add_pembayaran").show();
		list_head();
		$("#header_tipe").attr('disabled', 'disabled');
	}
}
function list_transaksi(){
	 window.open('{site_url}trawatinap_tindakan/print_rincian_deposit/'+$("#idrawatinap_header").val(), '_blank').focus();
}
function clear_data_awal(){
	$("#header_estimasi").val('');
	$("#perencanaan_id").val('');
	$("#header_diagnosa").val('');
	$("#header_tindakan").val('');
	$("#header_total_estimasi").val('');
	$("#header_nilai_acuan").val('');
}
function load_tipe(){
		clear_data_awal();
	if ($("#header_tipe").val()=='1'){
		$("#btn_load_estimasi").attr('disabled', false);
	}
	if ($("#header_tipe").val()=='2'){
		$("#btn_load_estimasi").attr('disabled', true);
		get_data_konservatif();
	}
	$.ajax({
		url: '{site_url}ttindakan_ranap_deposit/get_tipe', 
		dataType: "JSON",
		method: "POST",
		data : {
				tipe_deposit:$("#header_tipe").val(),
				idrawatinap:$("#idrawatinap_header").val(),
			   },
		success: function(data) {
			$("#cover-spin").hide();
			$("#header_deskripsi").val(data.deskripsi);
		}
	});
}
function get_data_konservatif(){
	$.ajax({
		url: '{site_url}ttindakan_ranap_deposit/get_data_konservatif', 
		dataType: "JSON",
		method: "POST",
		data : {
				idrawatinap:$("#idrawatinap_header").val(),
			   },
		success: function(data) {
			$("#cover-spin").hide();
			$("#header_nilai_acuan").val(data.nilai_acuan);
			$("#header_diagnosa").val(data.dengan_diagnosa);
			$("#header_total_estimasi").val(0);
			get_total_proses();
		}
	});
}
function create_tindakan_new(){
	// alert($("#header_tipe").val());
	if ($("#header_tipe").val()=='0'){
		sweetAlert("Maaf...", "Tentukan Tipe!", "error");
		return false;
	}
	if ($("#header_tipe").val()==''){
		sweetAlert("Maaf...", "Tentukan Tipe!", "error");
		return false;
	}
	if ($("#header_tipe").val()=='1'){//pembedahan
		if ($("#perencanaan_id").val()==''){
			sweetAlert("Maaf...", "Tentukan Estimasi Biaya!", "error");
			return false;
		}
		
	}else{
		if ($("#header_nilai_acuan").val()=='0'){
			sweetAlert("Maaf...", "Nilai Acuan Deposit!", "error");
			return false;
		}
	}
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Membuat Tindakan Baru ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}ttindakan_ranap_deposit/create_tindakan_new', 
				dataType: "JSON",
				method: "POST",
				data : {
						idrawatinap:$("#idrawatinap_header").val(),
						idpasien:$("#idpasien").val(),
						tipe_deposit:$("#header_tipe").val(),
						nama_tindakan:$("#header_tindakan").val(),
						diagnosa:$("#header_diagnosa").val(),
						total_estimasi:$("#header_total_estimasi").val(),
						nominal_acuan:$("#header_nilai_acuan").val(),
						perencanaan_id:$("#perencanaan_id").val(),
						deskripsi:$("#header_deskripsi").val(),
						perencanaan_nama:$("#header_estimasi").val(),
					   },
				success: function(data) {
					let head_id=data.id;
					$("#header_tindakan_id").val(head_id)
					let id=$("#idrawatinap_header").val();
					load_data_head(head_id,id);
					$("#cover-spin").hide();
					swal({
						title: "Berhasil!",
						text: "Proses.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});
					load_filter();
				}
			});
		});

	
}

function hide_estimasi(){
	$("#Modal_Estimasi").modal('hide');
	$("#Modal_Deposit_header").modal('show');
}
function pilih_estimasi(id){
	load_data_estimasi(id);
	hide_estimasi();
}
function load_estimasi(){
	$("#Modal_Estimasi").modal('show');
	$("#Modal_Deposit_header").modal('hide');
	 $('#index_estimasi').DataTable().destroy();
    table = $('#index_estimasi').DataTable({
		 autoWidth: false,
            searching: true,
            pageLength: 10,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
				{"targets": [0,6],  className: "text-center" },
				{"targets": [1],  className: "text-right" },
			
			],
            ajax: { 
                url: '{site_url}ttindakan_ranap_deposit/list_estimasi_admin',
                type: "POST" ,
                dataType: 'json',
				data : {
						idpasien:$("#idpasien").val(),
						
					   }
            },
			"drawCallback": function(settings) {
				$("#cover-spin").hide();
			}
			
    });
}
function show_modal_deposit_header(id){
	$("#Modal_Deposit_header").modal('show');
	$("#idrawatinap_header").val(id);
	$("#header_tipe").val("0").trigger('change');
	load_data_pasien(id);
	set_div_tindakan(1);
	list_deposit();
	
}
function show_modal_deposit_header_add(id,head_id){
	$("#Modal_Deposit_header").modal('show');
	$("#idrawatinap_header").val(id);
	$("#head_id").val(head_id);
	// $("#header_tipe").val("").trigger('change');
	load_data_pasien(id);
	load_data_head(head_id,id);
	list_deposit();
	get_total_proses();
	
}

function load_data_head(head_id,idrawatinap){
	$("#head_id").val(head_id);
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}ttindakan_ranap_deposit/load_data_head', 
		dataType: "JSON",
		method: "POST",
		data : {
				head_id:head_id,
				idrawatinap:idrawatinap,
			   },
		success: function(data) {
			$("#cover-spin").hide();
			
			$("#header_tipe").val(data.tipe_deposit).trigger('change');
			$("#header_tindakan").val(data.nama_tindakan);
			$("#header_diagnosa").val(data.diagnosa);
			$("#header_total_estimasi").val(data.total_estimasi);
			$("#header_nilai_acuan").val(data.nominal_acuan);
			$("#perencanaan_id").val(data.perencanaan_id);
			$("#header_estimasi").val(data.perencanaan_nama);
			$("#header_deskripsi").val(data.deskripsi);
			set_div_tindakan(0);
			get_total_proses();
		}
	});
}
function load_data_pasien(id){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}ttindakan_ranap_deposit/get_data_pasien', 
		dataType: "JSON",
		method: "POST",
		data : {
				idrawatinap:id,
			   },
		success: function(data) {
			$("#cover-spin").hide();
			$("#idpasien").val(data.idpasien);
			$("#header_nama").val(data.nama);
			$("#depositTerimaDari").val(data.namapasien);
			$("#header_umur").val(data.umur);
			$("#header_kelompok").val(data.nama_kelompok);
			$("#header_nama_asuransi").val(data.nama_asuransi);
			$("#header_dpjp").val(data.nama_dokter);
			$("#header_jenis_pelayanan").val(data.nama_pelayanan);
			$("#header_ruangan").val(data.ruangan);
			$("#header_masuk").val(data.tanggaldaftar);
			$("#idkelas_header").val(data.idkelas);
		}
	});
}
function load_data_estimasi(id){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}ttindakan_ranap_deposit/load_data_estimasi', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id:id,
			   },
		success: function(data) {
			$("#cover-spin").hide();
			$("#header_estimasi").val(data.nopermintaan+' - '+data.user_created+' - '+data.created_date);
			$("#header_diagnosa").val(data.dengan_diagnosa);
			$("#header_tindakan").val(data.rencana_tindakan);
			$("#header_total_estimasi").val(data.total_estimasi);
			$("#header_nilai_acuan").val(data.minimal_acuan);
			$("#perencanaan_id").val(data.assesmen_id);
		}
	});
}
function list_head(){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}ttindakan_ranap_deposit/list_head', 
		dataType: "JSON",
		method: "POST",
		data : {
				idrawatinap:$("#idrawatinap_header").val(),
			   },
		success: function(data) {
			$("#cover-spin").hide();
			$("#header_tindakan_id").empty();
			$("#header_tindakan_id").append(data.opsi);
			$("#header_tindakan_id").val($("#head_id").val()).trigger('change.select2');;
		}
	});
}
function get_total_proses(){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}ttindakan_ranap_deposit/get_total_proses', 
		dataType: "JSON",
		method: "POST",
		data : {
				idrawatinap:$("#idrawatinap_header").val(),
				head_id:$("#head_id").val(),
			   },
		success: function(data) {
			$("#cover-spin").hide();
			$("#header_total_diproses").val(data.nominal);
			// alert(data.nominal);
			if ($("#header_total_diproses").val()=='0'){
				$(".btn_batal_header").show();
			}else{
				$(".btn_batal_header").hide();
				
			}
			let header_sisa_acuan=$("#header_nilai_acuan").val()-data.nominal;
			if (header_sisa_acuan<0){
			$("#header_sisa_acuan").val(0);;
				
			}else{
			$("#header_sisa_acuan").val(header_sisa_acuan);;
				
			}
		}
	});
}
function selisih(){
	
}
</script>
