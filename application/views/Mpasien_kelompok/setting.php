<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-rounded">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mpasien_kelompok" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mpasien_kelompok/save_jt','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Kelompok Pasien</label>
				<div class="col-md-9">
					<input type="text" class="form-control" id="nama" readonly placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Multiple Pasien</label>
				<div class="col-md-3">
					<select name="st_multiple" id="st_multiple" tabindex="16"  style="width: 100%" data-placeholder="Tipe" class="js-select2 form-control input-sm">
						<option value="#" <?=($st_multiple=='#'?'selected':'')?>>-Pilih-</option>
						<option value="1" <?=($st_multiple=='1'?'selected':'')?>>Multiple Pasien</option>
						<option value="2" <?=($st_multiple=='2'?'selected':'')?>>Unik Pasien</option>						
					</select>	
				</div>
				<div class="col-md-6">
					<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="btn_simpan_multiple" title="Update"><i class="fa fa-save"></i> UPDATE</button>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama"></label>
				
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama"></label>
				<div class="col-md-9">
					<div class="table-responsive">
					<table class="table table-bordered table-striped" id="index_list2" style="width: 100%;">
						<thead>
							<tr>
								<th style="width: 20%;">Tipe</th>
								<th style="width: 20%;">Tanggal Jatuh Tempo</th>
								<th style="width: 20%;">Maks. Kirim</th>								
								<th style="width: 20%;">Jatuh Tempo Bayar</th>								
								<th style="width: 20%;">Last Update</th>								
								<th style="width: 20%;">Actions</th>
								<th hidden>Actions</th>
							</tr>
							<tr>								
								<td>
									<select name="tipe" id="tipe" tabindex="16"  style="width: 100%" data-placeholder="Tipe" class="js-select2 form-control input-sm">
										<option value="#">-Tipe-</option>
										<option value="1">Rawat Jalan</option>
										<option value="2">Rawat Inap</option>
										
									</select>										
								</td>
								<td>
									<select name="tanggal_hari" tabindex="16"  style="width: 100%" id="tanggal_hari" data-placeholder="Tanggal" class="js-select2 form-control input-sm">
										<option value="#">-Tanggal-</option>
										<?for ($i=1;$i<=31;$i++){?>
										<option value="<?=$i?>"><?=$i?></option>
										<?}?>
									</select>										
								</td>
								<td>									
									<input type="text" class="form-control input-sm number" id="batas_kirim" name="batas_kirim"  value="10"/>
								</td>
								
								<td>									
									<input type="text" class="form-control input-sm number" id="jml_hari" name="jml_hari"  value="30"/>
								</td>
								
								<td>									
									<?=$this->session->userdata('user_name').'<br>'.date('d-m-Y H:i:s')?>
								</td>
								
								<td>									
									<button type="button" class="btn btn-xs btn-primary" tabindex="8" id="btn_simpan" title="Masukan Item"><i class="fa fa-save"></i> Simpan</button>
									<button type="button" class="btn btn-xs btn-success" tabindex="8" id="btn_update" title="Update"><i class="fa fa-save"></i> Update</button>
									<button type="button" class="btn btn-xs btn-warning" tabindex="8" id="btn_batal" title="Update"><i class="fa fa-refresh"></i> Batal</button>
								</td>
							</tr>
						</thead>
						<tbody></tbody>							
					</table>
					<div>
					<div class="table-responsive">
					<table class="table table-bordered table-striped" id="index_list" style="width: 100%;">
						<thead>
							<tr>
								<th hidden></th>
								<th hidden></th>
								<th style="width: 20%;">Tipe</th>
								<th style="width: 20%;">Tanggal Jatuh Tempo</th>
								<th style="width: 20%;">Maks. Kirim</th>								
								<th style="width: 20%;">Jatuh Tempo Bayar</th>								
								<th style="width: 20%;">Last Update</th>								
								<th style="width: 20%;">Actions</th>
							</tr>
							
						</thead>
						<tbody></tbody>							
					</table>
				</div>
				</div>
			</div>
			<input readonly type="hidden" class="form-control input-sm" id="id" name="id"  value="{id}"/>
			<input readonly type="hidden" class="form-control input-sm" id="id_jt" name="id_jt"  value=""/>

			
			<?php echo form_close() ?>
	</div>
</div>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".number").number(true,0,',','.');
		Load_Jatuhtempo();
		clear_all();
	});
	$(document).on("change","#tanggal_hari,#tipe",function(){	
		var tanggal_hari=$("#tanggal_hari").val();
		var tipe=$("#tipe").val();
		// alert(tanggal_hari);
		if (tanggal_hari !='#' && tipe !='#'){
			cek_duplicate();
		}
	});
	$(document).on("click","#btn_simpan",function(){	
		var tipe=$("#tipe").val();
		var idkelompokpasien=$("#id").val();
		var tanggal_hari=$("#tanggal_hari").val();
		var jml_hari=$("#jml_hari").val();
		var batas_kirim=$("#batas_kirim").val();
		if (tipe=='#'){
			sweetAlert("Maaf...", "Tipe Harus diisi!", "error");
			return false;
		}
		if (idkelompokpasien==''){
			sweetAlert("Maaf...", "Kelompok Pasien Tidak Valid!", "error");
			return false;
		}
		if (tanggal_hari=='#'){
			sweetAlert("Maaf...", "Tanggal 	Jatuh Tempo Harus diisi!", "error");
			return false;
		}
		if (jml_hari=='' || jml_hari=='0'){
			sweetAlert("Maaf...", "Jumlah Hari Harus diisi!", "error");
			return false;
		}
		if (batas_kirim=='' || batas_kirim=='0'){
			sweetAlert("Maaf...", "Maks Hari Harus diisi!", "error");
			return false;
		}
		
		swal({
				title: "Anda Yakin ?",
				text : "Untuk Save Data ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				save_date();
			});
	});
	function cek_duplicate(){
		var idkelompokpasien=$("#id").val();
		var tanggal_hari=$("#tanggal_hari").val();
		var tipe=$("#tipe").val();
		// alert();
		$.ajax({
			url: '{site_url}mpasien_kelompok/cek_duplicate/'+idkelompokpasien+'/'+tanggal_hari+'/'+tipe+'/'+$("#id_jt").val(),
			type: 'POST',
			// data: {id_jt: id_jt},
			dataType: "json",
			success: function(data) {
				var jml=data.detail.jml;
				if (jml > 0){
					sweetAlert("Maaf...", "Tanggal Jatuh Tempo Duplicate!", "error");
					$("#btn_simpan").hide();
					clear_all();
					
				}else{
					if ($("#id_jt").val()==''){
						$("#btn_simpan").show()
						
					}
				}
			}
		});
	}
	function save_date(){
		var idkelompokpasien=$("#id").val();
		var tipe=$("#tipe").val();
		// alert(idkelompokpasien);
		var tanggal_hari=$("#tanggal_hari").val();
		var batas_kirim=$("#batas_kirim").val();
		var jml_hari=$("#jml_hari").val();
		var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}mpasien_kelompok/save_jt',
			type: 'POST',
			data: {tipe: tipe,idkelompokpasien: idkelompokpasien,tanggal_hari:tanggal_hari,jml_hari:jml_hari,batas_kirim:batas_kirim},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Save Data'});
				table.ajax.reload( null, false );
				clear_all();
			}
		});
		
	}
	$(document).on("click","#btn_simpan_multiple",function(){	
		var idkelompokpasien=$("#id").val();
		var st_multiple=$("#st_multiple").val();
		if (st_multiple=='#'){
			sweetAlert("Maaf...", "Tipe Multiple Pasien Harus diisi!", "error");
			return false;
		}
		$.ajax({
			url: '{site_url}mpasien_kelompok/save_multiple',
			type: 'POST',
			data: {st_multiple: st_multiple,idkelompokpasien: idkelompokpasien},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Update Data Berhasil'});
				location.reload();
			}
		});
		
	});
	$(document).on("click","#btn_update",function(){	
		var tipe=$("#tipe").val();
		var id_jt=$("#id_jt").val();
		var tanggal_hari=$("#tanggal_hari").val();
		var jml_hari=$("#jml_hari").val();
		var batas_kirim=$("#batas_kirim").val();
		if (tipe=='#'){
			sweetAlert("Maaf...", "Tipe Harus diisi!", "error");
			return false;
		}
		if (id_jt==''){
			sweetAlert("Maaf...", "ID Tidak Valid!", "error");
			return false;
		}
		if (tanggal_hari=='#'){
			sweetAlert("Maaf...", "Tanggal 	Jatuh Tempo Harus diisi!", "error");
			return false;
		}
		if (jml_hari=='' || jml_hari=='0'){
			sweetAlert("Maaf...", "Tanggal Hari Harus diisi!", "error");
			return false;
		}
		if (batas_kirim=='' || batas_kirim=='0'){
			sweetAlert("Maaf...", "Maks Hari Harus diisi!", "error");
			return false;
		}
		swal({
				title: "Anda Yakin ?",
				text : "Untuk Update Data ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				update_date();
			});
	});
	function update_date(){
		var id_jt=$("#id_jt").val();
		// alert(idkelompokpasien);
		var tipe=$("#tipe").val();
		var tanggal_hari=$("#tanggal_hari").val();
		var jml_hari=$("#jml_hari").val();
		var batas_kirim=$("#batas_kirim").val();
		var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}mpasien_kelompok/update_date',
			type: 'POST',
			data: {id_jt: id_jt,tipe:tipe,tanggal_hari:tanggal_hari,jml_hari:jml_hari,batas_kirim:batas_kirim},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Update Data'});
				table.ajax.reload( null, false );
				clear_all();
				// $("#cover-spin").hide();
				// filter_form();
			}
		});
		
	}
	function Load_Jatuhtempo() {
		// alert('sini');
		var idkelompokpasien=$("#id").val();
		
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 50,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}mpasien_kelompok/Load_Jatuhtempo/',
			type: "POST",
			dataType: 'json',
			data: {
				idkelompokpasien: idkelompokpasien,
			}
		},
		columnDefs: [
					{"targets": [0,1], "visible": false },
					 // {  className: "text-right", targets:[2] },
					 {  className: "text-center", targets:[2] },
					 // { "width": "30%", "targets": [0,1] },
					 // { "width": "20%", "targets": [0,1,2,3,4] },
					 // { "width": "10%", "targets": [2,4] }
					 // { "width": "15%", "targets": [2,13] }

					]
		});
	}
	
	$(document).on("click",".edit",function(){	
		var table = $('#index_list').DataTable();
        tr = table.row($(this).parents('tr')).index()
		var id_jt=table.cell(tr,0).data();
		var tipe=table.cell(tr,1).data();
		var tanggal_hari=table.cell(tr,3).data();
		var batas_kirim=table.cell(tr,4).data();
		var jml_hari=table.cell(tr,5).data();
		$('#id_jt').val(id_jt);
		$('#tipe').val(tipe).trigger('change');
		$('#tanggal_hari').val(tanggal_hari).trigger('change');
		$('#jml_hari').val(jml_hari);
		$('#batas_kirim').val(batas_kirim);
		$("#btn_simpan").hide()
		$("#btn_update").show()
		$("#btn_batal").show()
	});
	$(document).on("click",".hapus",function(){	
		var table = $('#index_list').DataTable();
		tr = table.row($(this).parents('tr')).index()
		var id_jt=table.cell(tr,0).data();
		$('#id_jt').val(id_jt);
        swal({
				title: "Anda Yakin ?",
				text : "Untuk Hapus data?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				hapus_data();
			});
	});
	function hapus_data(){
		var id_jt=$("#id_jt").val();
		var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}mpasien_kelompok/hapus_data',
			type: 'POST',
			data: {id_jt: id_jt},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
				table.ajax.reload( null, false );
				clear_all();
				// $("#cover-spin").hide();
				// filter_form();
			}
		});
		
	}
	$(document).on("click","#btn_batal",function(){	
		clear_all();
		
	});
	function clear_all(){
		$('#tanggal_hari').val('#').trigger('change');
		$('#batas_kirim').val(0);
		$('#jml_hari').val(0);
		$('#id_jt').val('');
		$("#btn_simpan").show()
		$("#btn_update").hide()
		$("#btn_batal").hide()
	}
	
</script>