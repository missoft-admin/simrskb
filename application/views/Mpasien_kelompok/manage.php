<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mpasien_kelompok" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mpasien_kelompok/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" readonly placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tadm_rawatjalan">Tarif ADM Rajal (Administrasi)</label>
				<div class="col-md-7">
					<select name="tadm_rawatjalan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_administrasi',array('idtipe' => '1', 'idjenis' => '1', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tadm_rawatjalan == $row->id) ? "selected" : "" ?>><?=GetJenisAdministrasi($row->idjenis).' ( '.$row->nama.' )';?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tkartu_rawatjalan">Tarif ADM Rajal (Cetak Kartu)</label>
				<div class="col-md-7">
					<select name="tkartu_rawatjalan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_administrasi',array('idtipe' => '1', 'idjenis' => '2', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tkartu_rawatjalan == $row->id) ? "selected" : "" ?>><?=GetJenisAdministrasi($row->idjenis).' ( '.$row->nama.' )';?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tadm_rawatinap">Tarif ADM Ranap (Administrasi)</label>
				<div class="col-md-7">
					<select name="tadm_rawatinap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_administrasi',array('idtipe' => '2', 'idjenis' => '1', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tadm_rawatinap == $row->id) ? "selected" : "" ?>><?=GetJenisAdministrasi($row->idjenis).' ( '.$row->nama.' )';?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tkartu_rawatinap">Tarif ADM Ranap (Cetak Kartu)</label>
				<div class="col-md-7">
					<select name="tkartu_rawatinap" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_administrasi',array('idtipe' => '2', 'idjenis' => '2', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tkartu_rawatinap == $row->id) ? "selected" : "" ?>><?=GetJenisAdministrasi($row->idjenis).' ( '.$row->nama.' )';?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="toperasi_sewaalat">Tarif Sewa Alat Kamar Operasi</label>
				<div class="col-md-7">
					<select name="toperasi_sewaalat" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_operasi_sewaalat',array('level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($toperasi_sewaalat == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group" style="display:none">
				<label class="col-md-3 control-label" for="toperasi_tindakan">Tarif Jasa Tindakan Operasi</label>
				<div class="col-md-7">
					<select name="toperasi_tindakan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_operasi',array('idtipe' => '3', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($toperasi_tindakan == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="toperasi_jenis">Tarif Jenis Operasi</label>
				<div class="col-md-7">
					<select name="toperasi_jenis[]" class="js-select2 form-control" multiple style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mjenis_operasi', array('status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=in_array($row->id, $toperasi_jenis) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tradiologi_xray">Tarif Radiologi (X-RAY)</label>
				<div class="col-md-7">
					<select name="tradiologi_xray" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_radiologi',array('idtipe' => '1', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tradiologi_xray == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tradiologi_usg">Tarif Radiologi (USG)</label>
				<div class="col-md-7">
					<select name="tradiologi_usg" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_radiologi',array('idtipe' => '2', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tradiologi_usg == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tradiologi_ctscan">Tarif Radiologi (CT-Scan)</label>
				<div class="col-md-7">
					<select name="tradiologi_ctscan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_radiologi',array('idtipe' => '3', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tradiologi_ctscan == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tradiologi_mri">Tarif Radiologi (MRI)</label>
				<div class="col-md-7">
					<select name="tradiologi_mri" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_radiologi',array('idtipe' => '4', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tradiologi_mri == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tradiologi_bmd">Tarif Radiologi (BMD)</label>
				<div class="col-md-7">
					<select name="tradiologi_bmd" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_radiologi',array('idtipe' => '5', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tradiologi_bmd == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tlaboratorium_umum">Tarif Laboratorium Umum</label>
				<div class="col-md-7">
					<select name="tlaboratorium_umum" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_laboratorium',array('idtipe' => '1', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tlaboratorium_umum == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tlaboratorium_pa">Tarif Laboratorium PA</label>
				<div class="col-md-7">
					<select name="tlaboratorium_pa" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_laboratorium',array('idtipe' => '2', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tlaboratorium_pa == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tlaboratorium_pmi">Tarif Laboratorium PMI</label>
				<div class="col-md-7">
					<select name="tlaboratorium_pmi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_laboratorium',array('idtipe' => '3', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tlaboratorium_pmi == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tfisioterapi">Tarif Fisioterapi</label>
				<div class="col-md-7">
					<select name="tfisioterapi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_fisioterapi',array('level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tfisioterapi == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tigd">Tarif IGD</label>
				<div class="col-md-7">
					<select name="tigd" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatjalan',array('level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tigd == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatjalan">Tarif Poliklinik</label>
				<div class="col-md-7">
					<select name="trawatjalan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatjalan',array('level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatjalan == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_fullcare1">Tarif Ranap Full Care (Perawatan)</label>
				<div class="col-md-7">
					<select name="trawatinap_fullcare1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '1', 'idtipe' => '1', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_fullcare1 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_ecg1">Tarif Ranap ECG (Perawatan)</label>
				<div class="col-md-7">
					<select name="trawatinap_ecg1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '1', 'idtipe' => '2', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_ecg1 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_visite1">Tarif Visite Dokter (Perawatan)</label>
				<div class="col-md-7">
					<select name="trawatinap_visite1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_visitedokter',array('idruangan' => '1', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_visite1 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_visite1_spesial">Tarif Visite Dokter Spesialis (Perawatan)</label>
				<div class="col-md-7">
					<select name="trawatinap_visite1_spesial" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_visitedokter',array('idruangan' => '1', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_visite1_spesial == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_sewaalat1">Tarif Ranap Sewa Alat (Perawatan)</label>
				<div class="col-md-7">
					<select name="trawatinap_sewaalat1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '1', 'idtipe' => '4', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_sewaalat1 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_ambulance1">Tarif Ranap Ambulance (Perawatan)</label>
				<div class="col-md-7">
					<select name="trawatinap_ambulance1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '1', 'idtipe' => '5', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_ambulance1 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_fullcare2">Tarif Ranap Full Care (HCU)</label>
				<div class="col-md-7">
					<select name="trawatinap_fullcare2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '2', 'idtipe' => '1', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_fullcare2 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_ecg2">Tarif Ranap ECG (HCU)</label>
				<div class="col-md-7">
					<select name="trawatinap_ecg2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '2', 'idtipe' => '2', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_ecg2 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_visite2">Tarif Visite Dokter (HCU)</label>
				<div class="col-md-7">
					<select name="trawatinap_visite2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_visitedokter',array('idruangan' => '2', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_visite2 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_visite2_spesial">Tarif Visite Dokter Spesialis (HCU)</label>
				<div class="col-md-7">
					<select name="trawatinap_visite2_spesial" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_visitedokter',array('idruangan' => '2', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_visite2_spesial == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_sewaalat2">Tarif Ranap Sewa Alat (HCU)</label>
				<div class="col-md-7">
					<select name="trawatinap_sewaalat2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '2', 'idtipe' => '4', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_sewaalat2 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_ambulance2">Tarif Ranap Ambulance (HCU)</label>
				<div class="col-md-7">
					<select name="trawatinap_ambulance2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '2', 'idtipe' => '5', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_ambulance2 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_fullcare3">Tarif Ranap Full Care (ICU)</label>
				<div class="col-md-7">
					<select name="trawatinap_fullcare3" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '3', 'idtipe' => '1', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_fullcare3 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_ecg3">Tarif Ranap ECG (ICU)</label>
				<div class="col-md-7">
					<select name="trawatinap_ecg3" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '3', 'idtipe' => '2', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_ecg3 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_visite3">Tarif Visite Dokter (ICU)</label>
				<div class="col-md-7">
					<select name="trawatinap_visite3" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_visitedokter',array('idruangan' => '3', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_visite3 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_visite3_spesial">Tarif Visite Dokter Spesialis (ICU)</label>
				<div class="col-md-7">
					<select name="trawatinap_visite3_spesial" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_visitedokter',array('idruangan' => '3', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_visite3_spesial == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_sewaalat3">Tarif Ranap Sewa Alat (ICU)</label>
				<div class="col-md-7">
					<select name="trawatinap_sewaalat3" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '3', 'idtipe' => '4', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_sewaalat3 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_ambulance3">Tarif Ranap Ambulance (ICU)</label>
				<div class="col-md-7">
					<select name="trawatinap_ambulance3" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '3', 'idtipe' => '5', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_ambulance3 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_fullcare4">Tarif Ranap Full Care (Isolasi)</label>
				<div class="col-md-7">
					<select name="trawatinap_fullcare4" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '3', 'idtipe' => '1', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_fullcare4 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_ecg4">Tarif Ranap ECG (Isolasi)</label>
				<div class="col-md-7">
					<select name="trawatinap_ecg4" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '3', 'idtipe' => '2', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_ecg4 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_visite4">Tarif Visite Dokter (Isolasi)</label>
				<div class="col-md-7">
					<select name="trawatinap_visite4" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_visitedokter',array('idruangan' => '4', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_visite4 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_visite4_spesial">Tarif Visite Dokter Spesialis (Isolasi)</label>
				<div class="col-md-7">
					<select name="trawatinap_visite4_spesial" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_visitedokter',array('idruangan' => '4', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_visite4_spesial == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_sewaalat4">Tarif Ranap Sewa Alat (Isolasi)</label>
				<div class="col-md-7">
					<select name="trawatinap_sewaalat4" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '4', 'idtipe' => '4', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_sewaalat4 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_ambulance4">Tarif Ranap Ambulance (Isolasi)</label>
				<div class="col-md-7">
					<select name="trawatinap_ambulance4" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '4', 'idtipe' => '5', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_ambulance4 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="truang_perawatan">Tarif Full Care (OK)</label>
				<div class="col-md-7">
					<select name="trawatinap_fullcare5" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '6', 'idtipe' => '1', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_fullcare5 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="truang_perawatan">Tarif Ruang (Perawatan)</label>
				<div class="col-md-7">
					<select name="truang_perawatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_ruangperawatan',array('idtipe' => '1', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($truang_perawatan == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="truang_hcu">Tarif Ruang (HCU)</label>
				<div class="col-md-7">
					<select name="truang_hcu" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_ruangperawatan',array('idtipe' => '2', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($truang_hcu == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="truang_icu">Tarif Ruang (ICU)</label>
				<div class="col-md-7">
					<select name="truang_icu" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_ruangperawatan',array('idtipe' => '3', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($truang_icu == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="truang_isolasi">Tarif Ruang (Isolasi)</label>
				<div class="col-md-7">
					<select name="truang_isolasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_ruangperawatan',array('idtipe' => '4', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($truang_isolasi == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mpasien_kelompok" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>
