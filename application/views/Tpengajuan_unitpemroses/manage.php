<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}tpengajuan_unitpemroses" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
        <hr style="margin-bottom: 0px;">
    </div>
    <?php echo form_open_multipart('tpengajuan/save','class="form-horizontal push-10-t" id="form-work"') ?>
    <div class="block-content">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-md-2 control-label text-uppercase" for="nopengajuan">No. Pengajuan</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" id="nopengajuan" name="nopengajuan" value="<?= $detail[0]->nopengajuan ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label text-uppercase" for="subjek">Subjek</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" id="subjek" name="subjek" value="<?= $detail[0]->subjek ?>" readonly>
                    </div>
                </div>
            </div>
        </div>

        <hr style="margin-top:2px;">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-4 control-label text-uppercase" for="tglpengajuan">Tgl Pengajuan</label>
                    <div class="col-md-8">
                        <input class="form-control" type="text" id="tglpengajuan" name="tglpengajuan" value="<?= DMYFormat($detail[0]->tanggal); ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label text-uppercase" for="vendor">Vendor</label>
                    <div class="col-md-8">
                        <input class="form-control" type="text" id="vendor" name="vendor" value="<?= $detail[0]->namavendor ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label text-uppercase" for="jenispembayaran">Jenis Pembayaran</label>
                    <div class="col-md-8">
                        <input class="form-control" type="text" id="jenispembayaran" name="jenispembayaran" value="<?= GetJenisPembayaranPengajuan($detail[0]->jenispembayaran) ?>" readonly>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label text-uppercase" for="statusbendahara">Status Bendahara</label>
                </div>
                <div class="form-group">
                    <div class="col-md-2"></div>
                    <div class="col-md-10" style="font-size: 14px;float:left;">{statusbendahara}</div>
                </div>

            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-4 control-label text-uppercase" for="tgldibutuhkan">Tgl Dibutuhkan</label>
                    <div class="col-md-8">
                        <input class="form-control" type="text" id="tgldibutuhkan" name="tgldibutuhkan" value="<?= DMYFormat($detail[0]->tanggaldibutuhkan); ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label text-uppercase" for="untukbagian">Untuk Bagian</label>
                    <div class="col-md-8">
                        <input class="form-control" type="text" id="untukbagian" name="untukbagian" value="<?= $detail[0]->namaunitpelayanan ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label text-uppercase" for="diperlukanuntuk">Diperlukan Untuk</label>
                    <div class="col-md-8">
                        <input class="form-control" type="text" id="diperlukanuntuk" name="diperlukanuntuk" value="<?= $detail[0]->namakeperluan ?>" readonly>
                    </div>
                </div>

                <?php if ($detail[0]->jenispembayaran == 4 || $detail[0]->jenispembayaran == 5) { ?>
                    <div class="form-group">
                        <label class="col-md-4 control-label text-uppercase" for="subjek">Termin Dipilih</label>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-10" style="font-size: 14px;float:left">{statusunit}</div>
                    </div>
                <?php } ?>

            </div>
        </div>

        <hr style="margin-top:2px;">

        <div class="row">
            <div class="col-md-6"><label class="label label-default text-uppercase" style="font-size:13px;">Progress</label></div>
            <div class="col-md-6"><button type="button" class="btn btn-primary text-uppercase" data-toggle="modal" data-target="#modal-add-proses" style="font-size:13px;margin-top:-3px;float:right;"><i class="fa fa-plus-circle"></i>&nbsp;Tambahkan</button></div>
            <div class="col-md-12">
                <table id="table-progress" class="table table-striped table-bordered" style="margin-top:20px;">
                    <tr class="text-uppercase">
                        <th>Tanggal</th>
                        <th>Deskripsi</th>
                        <th>Aksi</th>
                    </tr>
                    <tr id="row-progress">
                        <td class="text-center" colspan="3">-</td>
                    </tr>
                </table>
            </div>
        </div>

    </div>

    <?php echo form_close() ?>
</div>

<div id="modal-add-proses" class="modal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Tambah Proses</h3>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-3 text-uppercase control-label" for="idpoliklinik">Tanggal</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="fmKeperluan" value="">
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-3 text-uppercase control-label" for="idpoliklinik">Deskripsi</label>
                            <div class="col-md-8">
                                <textarea class="form-control" rows="4"></textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-8">
                                <hr>
                                <label class="label label-primary text-uppercase" style="font-size: 12px;">lampiran</label>
                                <button id="add-file1" class="btn btn-xs btn-success text-uppercase"><i class="fa fa-plus"></i></button>
                                <button id="min-file1" class="btn btn-xs btn-danger text-uppercase"><i class="fa fa-minus"></i></button>

                                <br>
                                <div id="file1" style="margin-top:10px;">
                                    <input type="file" class="file1 form-control-file" id="exampleFormControlFile1" style="margin-top:3px;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div>
                    <button class="btn btn-sm btn-success text-uppercase" style="width:100px;">Simpan</button>
                    <button class="btn btn-sm btn-danger text-uppercase" data-dismiss="modal" style="width:100px;">Batal</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#add-file1').click(function() {
            let el = $('#file1 .file1:first').clone().val('');
            $('#file1').append(el);
        });
        $('#min-file1').click(function() {
            if ($('.file1').length !== 1) {
                $('#file1 .file1:last').remove();
            } else {
                $('#file1 .file1:last').val('');
            }
        });
    });
</script>