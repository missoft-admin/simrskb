<!-- @tab navigation &start -->
<div class="content-grid">
    <div class="row">

        <?php if($this->session->userdata('user_idpermission') != 5) {?>
            <div class="col-xs-6 col-sm-4 col-lg-2">
                <a class="block block-link-hover2 text-center" href="{base_url}tpengajuan">
                    <div class="block-content block-content-full bg-modern">
                        <i class="si si-user fa-4x text-white"></i>
                        <div class="font-w600 text-white-op push-15-t text-uppercase">Unit</div>
                    </div>
                </a>
            </div>

            <?php if($this->session->userdata('user_idpermission') == 4) {?>
                <div class="col-xs-6 col-sm-4 col-lg-2">
                    <a class="block block-link-hover2 text-center" href="{base_url}tpengajuan_bendahara">
                        <div class="block-content block-content-full bg-modern">
                            <i class="si si-user fa-4x text-white"></i>
                            <div class="font-w600 text-white-op push-15-t text-uppercase">Bendahara</div>
                        </div>
                    </a>
                </div>
            <?php } ?>

            <div class="col-xs-6 col-sm-4 col-lg-2">
                <a class="block block-link-hover2 text-center" href="#">
                    <div class="block-content block-content-full bg-primary-dark">
                        <i class="si si-user fa-4x text-white"></i>
                        <div class="font-w600 text-white-op push-15-t text-uppercase">Unit Proses</div>
                    </div>
                </a>
            </div>
        <?php } ?>

        <?php if ($this->session->userdata('user_idpermission') == 5) { ?>
            <div class="col-xs-6 col-sm-4 col-lg-2">
                <a class="block block-link-hover2 text-center" href="#">
                    <div class="block-content block-content-full bg-modern">
                        <i class="si si-user fa-4x text-white"></i>
                        <div class="font-w600 text-white-op push-15-t text-uppercase">Wadir</div>
                    </div>
                </a>
            </div>
        <?php } ?>

        <!-- 1 unit, 2 unit, 3 bendahara & 4 unit proses -->
        <!-- #comment : (1& 4 biasa) (1, 2, 4 & bendahara) (2 wadir) -->

        <!-- 5 wadir & 4 bendahara -->
        <!---->
    </div>
</div>
<!-- @tab navigation &end -->

<?php echo ErrorSuccess($this->session)?>
<?php //if($error != '') {echo ErrorMessage($error)}?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}tpengajuan_unitpemroses/create" class="btn"><i class="fa fa-plus"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
        <hr style="margin-bottom: 0;">
    </div>
    <div class="block-content">
		<div class="table-responsive">
        <table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
            <thead>
            <tr>
                <th>No</th>
                <th>No. Pengajuan</th>
                <th>Tgl Pengajuan</th>
                <th>Tgl Dibutuhkan</th>
                <th>Pemohon</th>
                <th>Subjek</th>
                <th>Unit</th>
                <th>Diperlukan Untuk</th>
                <th>Status Bendahara</th>
                <th>Status</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    </div>
</div>

<div id="modal-end-proses" class="modal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Proses Selesai</h3>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-3 text-uppercase control-label" for="idpoliklinik">Tanggal</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="fmKeperluan" value="">
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-3 text-uppercase control-label" for="idpoliklinik">Deskripsi</label>
                            <div class="col-md-8">
                                <textarea class="form-control" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-3 text-uppercase control-label" for="idpoliklinik">No Bukti Pembayaran</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="fmKeperluan" value="">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-8">
                                <hr>
                                <label class="label label-primary text-uppercase" style="font-size: 12px;">lampiran</label>
                                <button id="add-file2" class="btn btn-xs btn-success text-uppercase"><i class="fa fa-plus"></i></button>
                                <button id="min-file2" class="btn btn-xs btn-danger text-uppercase"><i class="fa fa-minus"></i></button>

                                <br>
                                <div id="file2" style="margin-top:10px;">
                                    <input type="file" class="file2 form-control-file" id="exampleFormControlFile1" style="margin-top:3px;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div>
                    <button class="btn btn-sm btn-success text-uppercase" style="width:100px;">Simpan</button>
                    <button class="btn btn-sm btn-danger text-uppercase" data-dismiss="modal" style="width:100px;">Batal</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
    // Initialize when page loads
    $(document).ready(function() {
        BaseTableDatatables.init();
        var datablesimrs = $('#datatable-simrs').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "scrollX": true,
            "order": [],
            "ajax": {
                url: '{site_url}tpengajuan_unitpemroses/getIndex',
                type: "POST",
                dataType: 'json'
            }
        });
    });

    $(document).ready(function() {
        $('#add-file2').click(function() {
            let el = $('#file2 .file2:first').clone().val('');
            $('#file2').append(el);
        });
        $('#min-file2').click(function() {
            if ($('.file2').length !== 1) {
                $('#file2 .file2:last').remove();
            } else {
                $('#file2 .file2:last').val('');
            }
        });
    });
</script>
