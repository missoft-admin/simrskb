<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
			<div class="block-content">
					<div class="row">
					<?php echo form_open_multipart('antrian_print_out/save_general', 'class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
						
						<div class="form-group">
							<label class="col-md-2 control-label" for="judul_header">Judul Header<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="judul_header" placeholder="Judul Header" name="judul_header" value="{judul_header}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="judul_sub_header">Judul Sub Header<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="judul_sub_header" placeholder="Judul Sub Header" name="judul_sub_header" value="{judul_sub_header}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="footer">Footer<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="footer" placeholder="Footer" name="footer" value="{footer}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="footer_sub">Footer Sub<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="footer_sub" placeholder="Footer Sub" name="footer_sub" value="{footer_sub}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-2 control-label" for="st_tampil_tanggal">Tampilkan Tanggal & Jam Cetak<span style="color:red;">*</span></label>
								<div class="col-md-2">
									<select id="st_tampil_tanggal" name="st_tampil_tanggal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($st_tampil_tanggal=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($st_tampil_tanggal=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 col-md-offset-2">
								<button class="btn btn-success" type="submit">Simpan</button>
								<a href="{base_url}antrian_print_out/index/1" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
							</div>
						</div>
						<?php echo form_hidden('id', $id); ?>
						<?php echo form_close() ?>
					</div>
			</div>
			
			
		</div>
			
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
var table;

$(document).ready(function(){	
})	
function validate_final(){
	
	$("#cover-spin").show();
}

</script>