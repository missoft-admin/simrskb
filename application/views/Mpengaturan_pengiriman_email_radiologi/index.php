<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mpengaturan_pengiriman_email_radiologi" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?php echo form_open('mpengaturan_pengiriman_email_radiologi/save','class="form-horizontal push-10-t" id="form-work"') ?>
			<div class="form-group">
				<label class="col-md-12" for="">Judul Email</label>
				<div class="col-md-12">
					<textarea class="form-control summernote js-summernote-custom-height" name="judul" placeholder="Judul Email">{judul}</textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-12" for="">Body Email</label>
				<div class="col-md-12">
					<textarea class="form-control summernote js-summernote-custom-height" name="body" placeholder="Body Email">{body}</textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-12" for="">Footer Pengiriman</label>
				<div class="col-md-12">
					<input type="text" class="form-control" name="footer_pengirim" value="{footer_pengirim}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-12" for="">Footer Email</label>
				<div class="col-md-12">
					<textarea class="form-control summernote js-summernote-custom-height" name="footer_email" placeholder="Footer Email">{footer_pengirim}</textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-12" for="">File Dikirim</label>
				<div class="col-md-12">
					<select name="file_dikirim[]" class="js-select2 form-control" style="width: 100%;" multiple>
						<option value="1" <?=(in_array('1', $option_file_dikirim) ? 'selected' : ''); ?>>File Hasil 1</option>
						<option value="2" <?=(in_array('2', $option_file_dikirim) ? 'selected' : ''); ?>>File Hasil 2</option>
					</select>	
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 text-right">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mpengaturan_pengiriman_email_radiologi" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>

			<br /><br />

	</div>
</div>