<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}tpencairan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('tpencairan/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="deskripsi">Tanggal</label>
				<div class="col-md-7">
					<input class="js-datepicker form-control" type="text" id="tanggal" name="tanggal" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" value="{tanggal}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="deskripsi">Deskripsi</label>
				<div class="col-md-7">
					<textarea class="form-control" id="deskripsi" placeholder="Deskripsi" name="deskripsi" required="" aria-required="true">{deskripsi}</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="catatan">Catatan</label>
				<div class="col-md-7">
					<textarea class="form-control" id="catatan" placeholder="Catatan" name="catatan" required="" aria-required="true">{catatan}</textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}tpencairan" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>
