<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<style media="screen">
#datatable-simrs td {
	cursor: pointer;
}
</style>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}tpencairan/create" class="btn"><i class="fa fa-plus"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<div class="row">
			<?php echo form_open('tpencairan/filter','class="form-horizontal" id="form-work"') ?>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="example-daterange1">Tanggal</label>
						<div class="col-md-8">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" name="tanggaldari" placeholder="From" value="{tanggaldari}">
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
							</div>
						</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Deskripsi</label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="deskripsi" placeholder="Deskripsi" value="{deskripsi}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Status</label>
					<div class="col-md-8">
						<select name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#" selected>Semua Status</option>
							<option value="0" <?=($status == '0' ? 'selected' : '')?>>Sedang Diproses</option>
							<option value="1" <?=($status == '1' ? 'selected' : '')?>>Telah Diproses</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>

		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No.</th>
					<th>Tanggal</th>
					<th>Deskripsi</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>

<!-- start modal -->
<div class="modal in" id="AksiModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:90%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<h3 class="block-title">AKSI</h3>
				</div>
				<div class="block-content" style="margin-bottom: 10px;padding-top: 10px;">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#tabPengajuan" data-toggle="tab">Pengajuan</a>
						</li>
						<li>
							<a href="#tabKontrabon" data-toggle="tab">Kontrabon</a>
						</li>
						<li>
							<a href="#tabCarm" data-toggle="tab">C-Arm</a>
						</li>
						<li>
							<a href="#tabHonorDokter" data-toggle="tab">Honor Dokter</a>
						</li>
						<li>
							<a href="#tabKasbon" data-toggle="tab">Kasbon</a>
						</li>
						<li>
							<a href="#tabGajiKaryawan" data-toggle="tab">Gaji Karyawan</a>
						</li>
						<li>
							<a href="#tabFeeKlinik" data-toggle="tab">Fee Klinik</a>
						</li>
					</ul>

					<div class="tab-content ">
						<div class="tab-pane active" id="tabPengajuan">
							<br>
							<table id="historyPengajuan" class="table table-bordered table-striped" width="100%" style="margin-bottom: 0px;">
								<thead>
									<tr>
										<th style="display:none;">
											<input type="text" id="fPengajuanRowId" readonly value="">
										</th>
										<th colspan="8">
											<div class="input-group" style="width: 100%;">
												<select id="fPengajuanId" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
													<option value="">Pilih Opsi</option>
												</select>
												<span id="fPengajuanSearch" class="btn btn-primary input-group-addon" data-toggle="modal" data-target="#SubPengajuanModal"><i class="fa fa-search"></i></span>
											</div>
										</th>
										<th id="actionDataPengajuan" style="width:10%">
											<button class="btn btn-primary saveData" data-idtipe="pengajuan"><i class="fa fa-plus-circle"></i> Tambah</button>
										</th>
									</tr>
									<tr>
										<th>No. Transaksi</th>
										<th>Tipe</th>
										<th>Nama Transaksi</th>
										<th>Termin / Cicilan</th>
										<th>Tanggal Pengajuan</th>
										<th>Tanggal Dibutuhkan</th>
										<th>Pemohon</th>
										<th>Keperluan Unit</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

						<div class="tab-pane" id="tabKontrabon">
							<br>
							<table id="historyKontrabon" class="table table-bordered table-striped" width="100%" style="margin-bottom: 0px;">
								<thead>
									<tr>
										<th style="display:none;">
											<input type="text" id="fKontrabonRowId" readonly value="">
										</th>
										<th colspan="4" style="width:90%">
											<div class="input-group" style="width: 100%;">
												<select id="fKontrabonId" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
													<option value="">Pilih Opsi</option>
												</select>
												<span id="fKontrabonSearch" class="btn btn-primary input-group-addon" data-toggle="modal" data-target="#SubKontrabonModal"><i class="fa fa-search"></i></span>
											</div>
										</th>
										<th id="actionDataKontrabon" style="width:10%">
											<button class="btn btn-primary saveData" data-idtipe="kontrabon"><i class="fa fa-plus-circle"></i> Tambah</button>
										</th>
									</tr>
									<tr>
										<th>No. Kontrabon</th>
										<th>Tanggal</th>
										<th>Tipe</th>
										<th>Nominal</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

						<div class="tab-pane" id="tabCarm">
							<br>
							<table id="historyCarm" class="table table-bordered table-striped" width="100%" style="margin-bottom: 0px;">
								<thead>
									<tr>
										<th style="display:none;">
											<input type="text" id="fCarmRowId" readonly value="">
										</th>
										<th colspan="3" style="width:90%">
											<div class="input-group" style="width: 100%;">
												<select id="fCarmId" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
													<option value="">Pilih Opsi</option>
												</select>
												<span id="fCarmSearch" class="btn btn-primary input-group-addon" data-toggle="modal" data-target="#SubCarmModal"><i class="fa fa-search"></i></span>
											</div>
										</th>
										<th id="actionDataCarm" style="width:10%">
											<button class="btn btn-primary saveData" data-idtipe="carm"><i class="fa fa-plus-circle"></i> Tambah</button>
										</th>
									</tr>
									<tr>
										<th>No. Transaksi</th>
										<th>Nama</th>
										<th>Nominal</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

						<div class="tab-pane" id="tabHonorDokter">
							<br>
							<table id="historyHonorDokter" class="table table-bordered table-striped" width="100%" style="margin-bottom: 0px;">
								<thead>
									<tr>
										<th style="display:none;">
											<input type="text" id="fHonorDokterRowId" readonly value="">
										</th>
										<th colspan="4" style="width:90%">
											<div class="input-group" style="width: 100%;">
												<select id="fHonorDokterId" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
													<option value="">Pilih Opsi</option>
												</select>
												<span id="fHonorDokterSearch" class="btn btn-primary input-group-addon" data-toggle="modal" data-target="#SubHonorDokterModal"><i class="fa fa-search"></i></span>
											</div>
										</th>
										<th id="actionDataHonorDokter" style="width:10%">
											<button class="btn btn-primary saveData" data-idtipe="honordokter"><i class="fa fa-plus-circle"></i> Tambah</button>
										</th>
									</tr>
									<tr>
										<th>Nama Dokter</th>
										<th>Periode Bulan</th>
										<th>Periode Tahun</th>
										<th>Nominal</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

						<div class="tab-pane" id="tabKasbon">
							<br>
							<table id="historyKasbon" class="table table-bordered table-striped" width="100%" style="margin-bottom: 0px;">
								<thead>
									<tr>
										<th style="display:none;">
											<input type="text" id="fKasbonRowId" readonly value="">
										</th>
										<th colspan="5" style="width:90%">
											<div class="input-group" style="width: 100%;">
												<select id="fKasbonId" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
													<option value="">Pilih Opsi</option>
												</select>
												<span id="fKasbonSearch" class="btn btn-primary input-group-addon" data-toggle="modal" data-target="#SubKasbonModal"><i class="fa fa-search"></i></span>
											</div>
										</th>
										<th id="actionDataKasbon" style="width:10%">
											<button class="btn btn-primary saveData" data-idtipe="kasbon"><i class="fa fa-plus-circle"></i> Tambah</button>
										</th>
									</tr>
									<tr>
										<th>Tanggal</th>
										<th>Tipe</th>
										<th>Nama</th>
										<th>Catatan</th>
										<th>Nominal</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

						<div class="tab-pane" id="tabGajiKaryawan">
							<br>
							<table id="historyGajiKaryawan" class="table table-bordered table-striped" width="100%" style="margin-bottom: 0px;">
								<thead>
									<tr>
										<th style="display:none;">
											<input type="text" id="fGajiKaryawanRowId" readonly value="">
										</th>
										<th colspan="3" style="width:90%">
											<div class="input-group" style="width: 100%;">
												<select id="fGajiKaryawanId" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
													<option value="">Pilih Opsi</option>
												</select>
												<span id="fGajiKaryawanSearch" class="btn btn-primary input-group-addon" data-toggle="modal" data-target="#SubGajiKaryawanModal"><i class="fa fa-search"></i></span>
											</div>
										</th>
										<th id="actionDataGajiKaryawan" style="width:10%">
											<button class="btn btn-primary saveData" data-idtipe="gajikaryawan"><i class="fa fa-plus-circle"></i> Tambah</button>
										</th>
									</tr>
									<tr>
										<th>No. Transaksi</th>
										<th>Variable</th>
										<th>Nominal</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

						<div class="tab-pane" id="tabFeeKlinik">
							<br>
							<table id="historyFeeKlinik" class="table table-bordered table-striped" width="100%" style="margin-bottom: 0px;">
								<thead>
									<tr>
										<th style="display:none;">
											<input type="text" id="fFeeKlinikRowId" readonly value="">
										</th>
										<th colspan="3" style="width:90%">
											<div class="input-group" style="width: 100%;">
												<select id="fFeeKlinikId" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
													<option value="">Pilih Opsi</option>
												</select>
												<span id="fFeeKlinikSearch" class="btn btn-primary input-group-addon" data-toggle="modal" data-target="#SubFeeKlinikModal"><i class="fa fa-search"></i></span>
											</div>
										</th>
										<th id="actionDataFeeKlinik" style="width:10%">
											<button class="btn btn-primary saveData" data-idtipe="feeklinik"><i class="fa fa-plus-circle"></i> Tambah</button>
										</th>
									</tr>
									<tr>
										<th>Nama Klinik</th>
										<th>Periode</th>
										<th>Nominal</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>
<!-- eof modal aksi -->

<!-- start sub modal pengajuan -->
<div class="modal fade in black-overlay" id="SubPengajuanModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="margin-top: 150px; padding: 0 !important;">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Pengajuan</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="col-md-12 form-horizontal">
							<div class="form-val">
								<label class="col-sm-4" for="">Tipe</label>
								<div class="col-sm-8">
									<select style="width: 100%" id="idtipepengajuan" class="form-control input-sm">
										<option value="0">Semua Tipe</option>
										<option value="1">Tunai</option>
										<option value="2">Kontrabon</option>
										<option value="3">Transfer</option>
										<option value="4">Termin By Progress</option>
										<option value="5">Termin By Fix (Cicilan)</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<table width="100%" id="listPengajuanTable" class="table table-striped table-striped table-hover table-bordered">
						<thead>
							<tr>
								<th></th>
								<th>No. Transaksi</th>
								<th>Nama Transaksi</th>
								<th>Tanggal Pengajuan</th>
								<th>Tanggal Dibutuhkan</th>
								<th>Pemohon</th>
								<th>Keperluan Unit</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-success saveBatch" data-idtipe="pengajuan" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
			</div>
		</div>
	</div>
</div>
<!-- eof sub modal pengajuan -->

<!-- start sub modal kontrabon -->
<div class="modal fade in black-overlay" id="SubKontrabonModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="margin-top: 150px; padding: 0 !important;">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Kontrabon</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="col-md-12 form-horizontal">
							<div class="form-val">
								<label class="col-sm-4" for="">Tipe</label>
								<div class="col-sm-8">
									<select style="width: 100%" id="idtipekontrabon" class="form-control input-sm">
										<option value="0">Semua Tipe</option>
										<option value="1">Cheq</option>
										<option value="2">Tunai</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<table width="100%" id="listKontrabonTable" class="table table-striped table-striped table-hover table-bordered">
						<thead>
							<tr>
								<th></th>
								<th>No. Kontrabon</th>
								<th>Tanggal</th>
								<th>Tipe</th>
								<th>Nominal</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-success saveBatch" data-idtipe="kontrabon" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
			</div>
		</div>
	</div>
</div>
<!-- eof sub modal pengajuan -->

<!-- start sub modal carm -->
<div class="modal fade in black-overlay" id="SubCarmModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="margin-top: 150px; padding: 0 !important;">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar C-Arm</h3>
				</div>
				<div class="block-content">
					<table width="100%" id="listCarmTable" class="table table-striped table-striped table-hover table-bordered">
						<thead>
							<tr>
								<th></th>
								<th>No. Transaksi</th>
								<th>Periode</th>
								<th>Nominal</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-success saveBatch" data-idtipe="carm" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
			</div>
		</div>
	</div>
</div>
<!-- eof sub modal carm -->

<!-- start sub modal honor dokter -->
<div class="modal fade in black-overlay" id="SubHonorDokterModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="margin-top: 150px; padding: 0 !important;">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Honor Dokter</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="col-md-12 form-horizontal">
							<div class="form-group" style="margin-bottom:3px">
								<label class="col-sm-4" for="">Tahun</label>
								<div class="col-sm-8">
									<select style="width: 100%" id="tahunhonor" class="form-control input-sm">
										<?php for($tahun=date("Y"); $tahun >= 2017; $tahun--){?>
											<option value="<?=$tahun?>" <?=($tahun == date("Y") ? 'selected' : '')?>><?=$tahun?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom:3px">
								<label class="col-sm-4" for="">Bulan</label>
								<div class="col-sm-8">
									<select style="width: 100%" id="bulanhonor" class="form-control input-sm">
										<option value="01" <?=('01' == date("m") ? 'selected' : '')?>>Januari</option>
										<option value="02" <?=('02' == date("m") ? 'selected' : '')?>>Februari</option>
										<option value="03" <?=('03' == date("m") ? 'selected' : '')?>>Maret</option>
										<option value="04" <?=('04' == date("m") ? 'selected' : '')?>>April</option>
										<option value="05" <?=('05' == date("m") ? 'selected' : '')?>>Mei</option>
										<option value="06" <?=('06' == date("m") ? 'selected' : '')?>>Juni</option>
										<option value="07" <?=('07' == date("m") ? 'selected' : '')?>>Juli</option>
										<option value="08" <?=('08' == date("m") ? 'selected' : '')?>>Agustus</option>
										<option value="09" <?=('09' == date("m") ? 'selected' : '')?>>September</option>
										<option value="10" <?=('10' == date("m") ? 'selected' : '')?>>Oktober</option>
										<option value="11" <?=('11' == date("m") ? 'selected' : '')?>>November</option>
										<option value="12" <?=('12' == date("m") ? 'selected' : '')?>>Desember</option>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom:3px">
								<label class="col-sm-4" for="">Dokter</label>
								<div class="col-sm-8">
									<select class="js-select2 form-control" style="width: 100%;" id="iddokterhonor" data-placeholder="Pilih Opsi">
										<option value="0">Semua Dokter</option>
										<?php foreach (get_all('mdokter', array('status' => '1')) as $row){?>
											<option value="<?=$row->id ?>"><?=$row->nama ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<table width="100%" id="listHonorDokterTable" class="table table-striped table-striped table-hover table-bordered">
						<thead>
							<tr>
								<th></th>
								<th>Nama Dokter</th>
								<th>Periode Bulan</th>
								<th>Periode Tahun</th>
								<th>Nominal</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-success saveBatch" data-idtipe="honordokter" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
			</div>
		</div>
	</div>
</div>
<!-- eof sub modal honor dokter -->

<!-- start sub modal kasbon -->
<div class="modal fade in black-overlay" id="SubKasbonModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="margin-top: 150px; padding: 0 !important;">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Kasbon</h3>
				</div>
				<div class="block-content">
					<table width="100%" id="listKasbonTable" class="table table-striped table-striped table-hover table-bordered">
						<thead>
							<tr>
								<th></th>
								<th>Tanggal</th>
								<th>Tipe</th>
								<th>Nama</th>
								<th>Catatan</th>
								<th>Nominal</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-success saveBatch" data-idtipe="kasbon" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
			</div>
		</div>
	</div>
</div>
<!-- eof sub modal kasbon -->

<!-- start sub modal gajikaryawan -->
<div class="modal fade in black-overlay" id="SubGajiKaryawanModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="margin-top: 150px; padding: 0 !important;">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Gaji Karyawan</h3>
				</div>
				<div class="block-content">
					<table width="100%" id="listGajiKaryawanTable" class="table table-striped table-striped table-hover table-bordered">
						<thead>
							<tr>
								<th></th>
								<th>No. Transaksi</th>
								<th>Deskripsi</th>
								<th>Nominal</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-success saveBatch" data-idtipe="gajikaryawan" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
			</div>
		</div>
	</div>
</div>
<!-- eof sub modal gajikaryawan -->

<!-- start sub modal feeklinik -->
<div class="modal fade in black-overlay" id="SubFeeKlinikModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="margin-top: 150px; padding: 0 !important;">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Fee Klinik</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="col-md-12 form-horizontal">
							<div class="form-group" style="margin-bottom:3px">
								<label class="col-sm-4" for="">Tahun</label>
								<div class="col-sm-8">
									<select style="width: 100%" id="tahunfeeklinik" class="form-control input-sm">
										<?php for($tahun=date("Y"); $tahun >= 2017; $tahun--){?>
											<option value="<?=$tahun?>" <?=($tahun == date("Y") ? 'selected' : '')?>><?=$tahun?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom:3px">
								<label class="col-sm-4" for="">Bulan</label>
								<div class="col-sm-8">
									<select style="width: 100%" id="bulanfeeklinik" class="form-control input-sm">
										<option value="01" <?=('01' == date("m") ? 'selected' : '')?>>Januari</option>
										<option value="02" <?=('02' == date("m") ? 'selected' : '')?>>Februari</option>
										<option value="03" <?=('03' == date("m") ? 'selected' : '')?>>Maret</option>
										<option value="04" <?=('04' == date("m") ? 'selected' : '')?>>April</option>
										<option value="05" <?=('05' == date("m") ? 'selected' : '')?>>Mei</option>
										<option value="06" <?=('06' == date("m") ? 'selected' : '')?>>Juni</option>
										<option value="07" <?=('07' == date("m") ? 'selected' : '')?>>Juli</option>
										<option value="08" <?=('08' == date("m") ? 'selected' : '')?>>Agustus</option>
										<option value="09" <?=('09' == date("m") ? 'selected' : '')?>>September</option>
										<option value="10" <?=('10' == date("m") ? 'selected' : '')?>>Oktober</option>
										<option value="11" <?=('11' == date("m") ? 'selected' : '')?>>November</option>
										<option value="12" <?=('12' == date("m") ? 'selected' : '')?>>Desember</option>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom:3px">
								<label class="col-sm-4" for="">RS / Klinik</label>
								<div class="col-sm-8">
									<select class="js-select2 form-control" style="width: 100%;" id="idrumahsakitfeeklinik" data-placeholder="Pilih Opsi">
										<option value="0">Semua RS / Klinik</option>
										<?php foreach (get_all('mrumahsakit', array('status' => '1', 'idkategori' => '1')) as $row){?>
											<option value="<?=$row->id ?>"><?=$row->nama ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<table width="100%" id="listFeeKlinikTable" class="table table-striped table-striped table-hover table-bordered">
						<thead>
							<tr>
								<th></th>
								<th>Nama Klinik</th>
								<th>Periode</th>
								<th>Nominal</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-success saveBatch" data-idtipe="feeklinik" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
			</div>
		</div>
	</div>
</div>
<!-- eof sub modal feeklinik -->

<input type="hidden" id="tempIdRow" name="" value="">
<input type="hidden" id="tempIdPencairan" name="" value="">

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<!-- magnificPopup -->
<link rel="stylesheet" href="{plugins_path}magnific-popup/magnific-popup.css">
<script src="{plugins_path}magnific-popup/magnific-popup.min.js"></script>

<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		var dt = $('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tpencairan/getIndex/' + '<?=$this->uri->segment(2)?>',
					type: "POST",
					dataType: 'json'
				},
				"columns": [
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
	      ],
				"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": true },
					{ "width": "5%", "targets": 1, "orderable": true },
					{ "width": "10%", "targets": 2, "orderable": true },
					{ "width": "10%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 4, "orderable": true },
				]
			});

			// Array to track the ids of the details displayed rows
			var detailRows = [];

			$('#datatable-simrs tbody').on( 'click', 'tr td.details-control', function (){
					var tr = $(this).closest('tr');
					var row = dt.row( tr );
					var idx = $.inArray( tr.attr('id'), detailRows );

					if ( row.child.isShown() ) {
							tr.removeClass( 'details' );
							row.child.hide();

							// Remove from the 'open' array
							detailRows.splice( idx, 1 );
					} else {
							var idpencairan = tr.find("td:eq(0) span").data('idpencairan');

							$.ajax({
								url:"{site_url}tpencairan/getDataPencairan/" + idpencairan,
								dataType:"json",
								success:function(data){
									tr.addClass('details');
									row.child( format(data) ).show();

									// Add to the 'open' array
									if ( idx === -1 ) {
										detailRows.push( tr.attr('id') );
									}
								}
							});

					}
			});

			// On each draw, loop over the `detailRows` array and show any child rows
			dt.on('draw', function(){
					$.each( detailRows, function(i, id){
							$('#'+id+' td.details-control').trigger('click');
					});
			});
	});

	$(document).ready(function() {
		$('.number').number(true, 0, '.', ',');

		// Aksi : Pengajuan, Kontrabon, C-Arm, Honor Dokter, Kasbon, Gaji Karyawan, Fee Klinik
		$(document).on('click', '.openModalAksi', function() {
			var idpencairan = $("#tempIdPencairan").val();

			// Selected Navigation Tab
			$('.nav-tabs a[href="#' + $(this).data('tab') + '"]').tab('show');

			// Get History Data
			getHistoryPengajuan(idpencairan);
			getHistoryKontrabon(idpencairan);
			getHistoryCarm(idpencairan);
			getHistoryHonorDokter(idpencairan);
			getHistoryKasbon(idpencairan);
			getHistoryGajiKaryawan(idpencairan);
			getHistoryFeeKlinik(idpencairan);
		});

		// SubAksi Modal
		// Select : Pengajuan
		$("#fPengajuanId").select2({
		  minimumInputLength: 2,
		  ajax: {
		    url: '{site_url}tpencairan/getPengajuan',
		    dataType: 'json',
		    type: "POST",
		    quietMillis: 50,
		    data: function(params) {
		      var query = {
		        search: params.term,
		      }
		      return query;
		    },
		    processResults: function(data) {
					console.log(data);
		      return {
		        results: $.map(data, function(item) {
		          return {
		            text: item.notransaksi,
		            id: item.id
		          }
		        })
		      };
		    }
		  }
		});

		// Select : Kontrabon
		$("#fKontrabonId").select2({
		  minimumInputLength: 2,
		  ajax: {
		    url: '{site_url}tpencairan/getKontrabon',
		    dataType: 'json',
		    type: "POST",
		    quietMillis: 50,
		    data: function(params) {
		      var query = {
		        search: params.term,
		      }
		      return query;
		    },
		    processResults: function(data) {
		      return {
		        results: $.map(data, function(item) {
		          return {
		            text: item.notransaksi,
		            id: item.id
		          }
		        })
		      };
		    }
		  }
		});

		// Select : C-Arm
		$("#fCarmId").select2({
		  minimumInputLength: 2,
		  ajax: {
		    url: '{site_url}tpencairan/getCarm',
		    dataType: 'json',
		    type: "POST",
		    quietMillis: 50,
		    data: function(params) {
		      var query = {
		        search: params.term,
		      }
		      return query;
		    },
		    processResults: function(data) {
		      return {
		        results: $.map(data, function(item) {
		          return {
		            text: item.notransaksi,
		            id: item.id
		          }
		        })
		      };
		    }
		  }
		});

		$("#fHonorDokterId").select2({
		  minimumInputLength: 2,
		  ajax: {
		    url: '{site_url}tpencairan/getHonorDokter',
		    dataType: 'json',
		    type: "POST",
		    quietMillis: 50,
		    data: function(params) {
		      var query = {
		        search: params.term,
		      }
		      return query;
		    },
		    processResults: function(data) {
		      return {
		        results: $.map(data, function(item) {
		          return {
		            text: item.namadokter + ', Periode ' + monthLabel(parseInt(item.bulan)) + ' ' + item.tahun,
		            id: item.id
		          }
		        })
		      };
		    }
		  }
		});

		$("#fKasbonId").select2({
		  minimumInputLength: 2,
		  ajax: {
		    url: '{site_url}tpencairan/getKasbon',
		    dataType: 'json',
		    type: "POST",
		    quietMillis: 50,
		    data: function(params) {
		      var query = {
		        search: params.term,
		      }
		      return query;
		    },
		    processResults: function(data) {
		      return {
		        results: $.map(data, function(item) {
		          return {
		            text: item.tanggal + ' - ' + item.namapegawai + ' - ' + item.catatan,
		            id: item.id
		          }
		        })
		      };
		    }
		  }
		});

		$("#fGajiKaryawanId").select2({
		  minimumInputLength: 2,
		  ajax: {
		    url: '{site_url}tpencairan/getGajiKaryawan',
		    dataType: 'json',
		    type: "POST",
		    quietMillis: 50,
		    data: function(params) {
		      var query = {
		        search: params.term,
		      }
		      return query;
		    },
		    processResults: function(data) {
		      return {
		        results: $.map(data, function(item) {
		          return {
		            text: item.notransaksi,
		            id: item.id
		          }
		        })
		      };
		    }
		  }
		});

		$("#fFeeKlinikId").select2({
		  minimumInputLength: 2,
		  ajax: {
		    url: '{site_url}tpencairan/getFeeKlinik',
		    dataType: 'json',
		    type: "POST",
		    quietMillis: 50,
		    data: function(params) {
		      var query = {
		        search: params.term,
		      }
		      return query;
		    },
		    processResults: function(data) {
		      return {
		        results: $.map(data, function(item) {
		          return {
		            text: item.namarumahsakit + ', Periode ' + monthLabel(parseInt(item.bulan)) + ' ' + item.tahun,
		            id: item.id
		          }
		        })
		      };
		    }
		  }
		});

		// Change Tipe :Pengajuan
		$(document).on('change', '#idtipepengajuan', function() {
			var idtipepengajuan = $(this).val();

			listPengajuanTable(idtipepengajuan);
		});

		$(document).on('change', '#idtipekontrabon', function() {
			var idtipekontrabon = $(this).val();

			listKontrabonTable(idtipekontrabon);
		});

		$(document).on('change', '#tahunhonor, #bulanhonor, #iddokterhonor', function() {
			var tahun = $('#tahunhonor').val();
			var bulan = $('#bulanhonor').val();
			var iddokter = $('#iddokterhonor').val();

			listHonorDokterTable(tahun, bulan, iddokter);
		});

		$(document).on('change', '#tahunfeeklinik, #bulanfeeklinik, #idrumahsakitfeeklinik', function() {
			var tahun = $('#tahunfeeklinik').val();
			var bulan = $('#bulanfeeklinik').val();
			var idrumahsakit = $('#idrumahsakitfeeklinik').val();

			listFeeKlinikTable(tahun, bulan, idrumahsakit);
		});

		// Search : Pengajuan
		$(document).on('click', '#fPengajuanSearch', function() {
			var idtipepengajuan = $("#idtipepengajuan").val();

			listPengajuanTable(idtipepengajuan);
		});

		$(document).on('click', '#fKontrabonSearch', function() {
			var idtipekontrabon = $("#idtipekontrabon").val();

			listKontrabonTable(idtipekontrabon);
		});

		$(document).on('click', '#fCarmSearch', function() {
			$('#listCarmTable').DataTable().destroy();
			$('#listCarmTable').DataTable({
					"autoWidth": false,
					"pageLength": 5,
					"ordering": true,
					"processing": true,
					"serverSide": true,
					"order": [],
					"ajax": {
						url: '{site_url}tpencairan/getListCarm',
						type: "POST",
						dataType: 'json'
					}
				});
		});

		$(document).on('click', '#fHonorDokterSearch', function() {
			var tahun = $('#tahunhonor').val();
			var bulan = $('#bulanhonor').val();
			var iddokter = $('#iddokterhonor').val();

			listHonorDokterTable(tahun, bulan, iddokter);
		});

		$(document).on('click', '#fKasbonSearch', function() {
			$('#listKasbonTable').DataTable().destroy();
			$('#listKasbonTable').DataTable({
					"autoWidth": false,
					"pageLength": 5,
					"ordering": true,
					"processing": true,
					"serverSide": true,
					"order": [],
					"ajax": {
						url: '{site_url}tpencairan/getListKasbon',
						type: "POST",
						dataType: 'json'
					}
				});
		});

		$(document).on('click', '#fGajiKaryawanSearch', function() {
			$('#listGajiKaryawanTable').DataTable().destroy();
			$('#listGajiKaryawanTable').DataTable({
					"autoWidth": false,
					"pageLength": 5,
					"ordering": true,
					"processing": true,
					"serverSide": true,
					"order": [],
					"ajax": {
						url: '{site_url}tpencairan/getListGajiKaryawan',
						type: "POST",
						dataType: 'json'
					}
				});
		});

		$(document).on('click', '#fFeeKlinikSearch', function() {
			var tahun = $('#tahunfeeklinik').val();
			var bulan = $('#bulanfeeklinik').val();
			var idrumahsakit = $('#idrumahsakitfeeklinik').val();

			listFeeKlinikTable(tahun, bulan, idrumahsakit);
		});

		// Selected Content
		$(document).on('click', '.selectContentTable', function() {
			var idpencairan = $("#tempIdPencairan").val();
			var idtransaksi = $(this).closest('tr').find("td:eq(1) a").data('idtransaksi');
			var notransaksi = $(this).closest('tr').find("td:eq(1) a").data('notransaksi');
			var idtipe = $(this).closest('tr').find("td:eq(1) a").data('idtipe');

			if(idtipe == 'pengajuan'){
				$('#fPengajuanId').select2("trigger", "select", {data : {id: idtransaksi, text: notransaksi}});
			}else if(idtipe == 'kontrabon'){
				$('#fKontrabonId').select2("trigger", "select", {data : {id: idtransaksi, text: notransaksi}});
			}else if(idtipe == 'carm'){
				$('#fCarmId').select2("trigger", "select", {data : {id: idtransaksi, text: notransaksi}});
			}else if(idtipe == 'honordokter'){
				$('#fHonorDokterId').select2("trigger", "select", {data : {id: idtransaksi, text: notransaksi}});
			}else if(idtipe == 'kasbon'){
				$('#fKasbonId').select2("trigger", "select", {data : {id: idtransaksi, text: notransaksi}});
			}else if(idtipe == 'gajikaryawan'){
				$('#fGajiKaryawanId').select2("trigger", "select", {data : {id: idtransaksi, text: notransaksi}});
			}else if(idtipe == 'feeklinik'){
				$('#fFeeKlinikId').select2("trigger", "select", {data : {id: idtransaksi, text: notransaksi}});
			}
		});

		$(document).on('click', '.saveData', function() {
			var idpencairan = $("#tempIdPencairan").val();
			var idtipe = $(this).data('idtipe');

			if(idtipe == 'pengajuan'){
				var idtransaksi = $('#fPengajuanId').val();
				var idrow = $('#fPengajuanRowId').val();
			}else if(idtipe == 'kontrabon'){
				var idtransaksi = $('#fKontrabonId').val();
				var idrow = $('#fKontrabonRowId').val();

				if(idtransaksi == ''){
					sweetAlert("Maaf...", "No. Pengajuan belum dipilih !", "error").then((value) => {
						$('#fPengajuanId').focus();
					});
					return false;
				}
			}else if(idtipe == 'carm'){
				var idtransaksi = $('#fCarmId').val();
				var idrow = $('#fCarmRowId').val();

				if(idtransaksi == ''){
					sweetAlert("Maaf...", "No. Transaksi belum dipilih !", "error").then((value) => {
						$('#fCarmId').focus();
					});
					return false;
				}
			}else if(idtipe == 'honordokter'){
				var idtransaksi = $('#fHonorDokterId').val();
				var idrow = $('#fHonorDokterRowId').val();

				if(idtransaksi == ''){
					sweetAlert("Maaf...", "Dokter belum dipilih !", "error").then((value) => {
						$('#fHonorDokterId').focus();
					});
					return false;
				}
			}else if(idtipe == 'kasbon'){
				var idtransaksi = $('#fKasbonId').val();
				var idrow = $('#fKasbonRowId').val();

				if(idtransaksi == ''){
					sweetAlert("Maaf...", "No. Kasbon belum dipilih !", "error").then((value) => {
						$('#fKasbonId').focus();
					});
					return false;
				}
			}else if(idtipe == 'gajikaryawan'){
				var idtransaksi = $('#fGajiKaryawanId').val();
				var idrow = $('#fGajiKaryawanRowId').val();

				if(idtransaksi == ''){
					sweetAlert("Maaf...", "Gaji Karyawan belum dipilih !", "error").then((value) => {
						$('#fGajiKaryawanId').focus();
					});
					return false;
				}
			}else if(idtipe == 'feeklinik'){
				var idtransaksi = $('#fFeeKlinikId').val();
				var idrow = $('#fFeeKlinikRowId').val();

				if(idtransaksi == ''){
					sweetAlert("Maaf...", "Klinik belum dipilih !", "error").then((value) => {
						$('#fFeeKlinikId').focus();
					});
					return false;
				}
			}

			$.ajax({
				url: '{site_url}tpencairan/saveData',
				method: 'POST',
				data: {
					idpencairan: idpencairan,
					idtransaksi: idtransaksi,
					idtipe: idtipe,
					idrow: idrow,
				},
				success: function(data) {
					swal({
						title: "Berhasil!",
						text: "Data Transaksi Telah Tersimpan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					if(idtipe == 'pengajuan'){
						getHistoryPengajuan(idpencairan);
					}else if(idtipe == 'kontrabon'){
						getHistoryKontrabon(idpencairan);
					}else if(idtipe == 'carm'){
						getHistoryCarm(idpencairan);
					}else if(idtipe == 'honordokter'){
						getHistoryHonorDokter(idpencairan);
					}else if(idtipe == 'kasbon'){
						getHistoryKasbon(idpencairan);
					}else if(idtipe == 'gajikaryawan'){
						getHistoryGajiKaryawan(idpencairan);
					}else if(idtipe == 'feeklinik'){
						getHistoryFeeKlinik(idpencairan);
					}

					butonUnlock(idtipe);
				}
			});
		});

		$(document).on('click', '.editData', function() {
		  var idtransaksi = $(this).data('idtransaksi');
		  var notransaksi = $(this).data('notransaksi');
		  var idrow = $(this).data('idrow');

			butonLock(idtipe);
		});

		$(document).on('click', '.cancelData', function() {
			var idtipe = $(this).data('idtipe');

			butonUnlock(idtipe);
		});

		$(document).on('click', '.removeData', function() {
			var idpencairan = $('#tempIdPencairan').val();
			var idtipe = $(this).data('idtipe');
		  var idrow = $(this).data('idrow');

		  event.preventDefault();
		  swal({
		      title: "Apakah anda yakin ingin menghapus data ini?",
		      type: "warning",
		      showCancelButton: true,
		      confirmButtonColor: "#DD6B55",
		      confirmButtonText: "Ya",
		      cancelButtonText: "Tidak",
		      closeOnConfirm: false,
		      closeOnCancel: false
		    }).then(function() {
		      // Approve Status
		      $.ajax({
		        url: '{site_url}tpencairan/removeData',
		        method: 'POST',
		        data: {
		          idrow: idrow,
		        },
		        success: function(data) {
		          swal({
		            title: "Berhasil!",
		            text: "Data Transaksi Telah Dihapus.",
		            type: "success",
		            timer: 1500,
		            showConfirmButton: false
		          });

							if(idtipe == 'pengajuan'){
								getHistoryPengajuan(idpencairan);
							}else if(idtipe == 'kontrabon'){
								getHistoryKontrabon(idpencairan);
							}else if(idtipe == 'carm'){
								getHistoryCarm(idpencairan);
							}else if(idtipe == 'honordokter'){
								getHistoryHonorDokter(idpencairan);
							}else if(idtipe == 'kasbon'){
								getHistoryKasbon(idpencairan);
							}else if(idtipe == 'gajikaryawan'){
								getHistoryGajiKaryawan(idpencairan);
							}else if(idtipe == 'feeklinik'){
								getHistoryFeeKlinik(idpencairan);
							}
		        }
		      });
		    });
		});

		$(document).on('click', '.removeDataGroup', function() {
			var idpencairan = $('#tempIdPencairan').val();
			var idtipe = $(this).data('idtipe');
		  var idrow = $(this).data('idrow');

		  event.preventDefault();
		  swal({
		      title: "Apakah anda yakin ingin menghapus data ini?",
		      type: "warning",
		      showCancelButton: true,
		      confirmButtonColor: "#DD6B55",
		      confirmButtonText: "Ya",
		      cancelButtonText: "Tidak",
		      closeOnConfirm: false,
		      closeOnCancel: false
		    }).then(function() {
		      // Approve Status
		      $.ajax({
		        url: '{site_url}tpencairan/removeDataGroup',
		        method: 'POST',
		        data: {
		          idtipe: idtipe,
		          idrow: idrow,
		        },
		        success: function(data) {
		          swal({
		            title: "Berhasil!",
		            text: "Data Transaksi Telah Dihapus.",
		            type: "success",
		            timer: 1500,
		            showConfirmButton: false
		          });

							if(idtipe == 'pengajuan'){
								getHistoryPengajuan(idpencairan);
							}else if(idtipe == 'kontrabon'){
								getHistoryKontrabon(idpencairan);
							}else if(idtipe == 'carm'){
								getHistoryCarm(idpencairan);
							}else if(idtipe == 'honordokter'){
								getHistoryHonorDokter(idpencairan);
							}else if(idtipe == 'kasbon'){
								getHistoryKasbon(idpencairan);
							}else if(idtipe == 'gajikaryawan'){
								getHistoryGajiKaryawan(idpencairan);
							}else if(idtipe == 'feeklinik'){
								getHistoryFeeKlinik(idpencairan);
							}
		        }
		      });
		    });
		});

		// Batch Process
		$(document).on("click", ".saveBatch", function() {
			var idpencairan = $("#tempIdPencairan").val();
			var idtipe = $(this).data('idtipe');
			var multipleData = [];

			if(idtipe == 'pengajuan'){
				$('#listPengajuanTable').find('tr').each(function () {
		        var row = $(this);
		        if (row.find('input[type="checkbox"]').is(':checked')) {
							multipleData.push(row.children('td').find('label input').eq(0).val());
		        }
		    });
			}else if(idtipe == 'kontrabon'){
				$('#listKontrabonTable').find('tr').each(function () {
		        var row = $(this);
		        if (row.find('input[type="checkbox"]').is(':checked')) {
							multipleData.push(row.children('td').find('label input').eq(0).val());
		        }
		    });
			}else if(idtipe == 'carm'){
				$('#listCarmTable').find('tr').each(function () {
		        var row = $(this);
		        if (row.find('input[type="checkbox"]').is(':checked')) {
							multipleData.push(row.children('td').find('label input').eq(0).val());
		        }
		    });
			}else if(idtipe == 'honordokter'){
				$('#listHonorDokterTable').find('tr').each(function () {
		        var row = $(this);
		        if (row.find('input[type="checkbox"]').is(':checked')) {
							multipleData.push(row.children('td').find('label input').eq(0).val());
		        }
		    });
			}else if(idtipe == 'kasbon'){
				$('#listKasbonTable').find('tr').each(function () {
		        var row = $(this);
		        if (row.find('input[type="checkbox"]').is(':checked')) {
							multipleData.push(row.children('td').find('label input').eq(0).val());
		        }
		    });
			}else if(idtipe == 'gajikaryawan'){
				$('#listGajiKaryawanTable').find('tr').each(function () {
		        var row = $(this);
		        if (row.find('input[type="checkbox"]').is(':checked')) {
							multipleData.push(row.children('td').find('label input').eq(0).val());
		        }
		    });
			}else if(idtipe == 'feeklinik'){
				$('#listFeeKlinikTable').find('tr').each(function () {
		        var row = $(this);
		        if (row.find('input[type="checkbox"]').is(':checked')) {
							multipleData.push(row.children('td').find('label input').eq(0).val());
		        }
		    });
			}

			var encodeMultipleData = JSON.stringify(multipleData);

			$.ajax({
				url: '{site_url}tpencairan/saveBatch',
				method: 'POST',
				data: {
					idpencairan: idpencairan,
					idtransaksi: encodeMultipleData,
					idtipe: idtipe
				},
				success: function(data) {
					swal({
						title: "Berhasil!",
						text: "Data Transaksi Telah Tersimpan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					if(idtipe == 'pengajuan'){
						getHistoryPengajuan(idpencairan);
					}else if(idtipe == 'kontrabon'){
						getHistoryKontrabon(idpencairan);
					}else if(idtipe == 'carm'){
						getHistoryCarm(idpencairan);
					}else if(idtipe == 'honordokter'){
						getHistoryHonorDokter(idpencairan);
					}else if(idtipe == 'kasbon'){
						getHistoryKasbon(idpencairan);
					}else if(idtipe == 'gajikaryawan'){
						getHistoryGajiKaryawan(idpencairan);
					}else if(idtipe == 'feeklinik'){
						getHistoryFeeKlinik(idpencairan);
					}

					butonUnlock(idtipe);
				}
			});

		});

	});

	// Get Header Group
	function getHeaderGroupKontrabon(tanggal, idcarabayar) {
		$.ajax({
			url: '{site_url}tpencairan/getHeaderGroupKontrabon/' + tanggal + '/' + idcarabayar,
			dataType: 'json',
			async: false,
			success: function(data) {
				var html = '';

				html += '<tr>';
				html += '<td colspan="4" style="background-color: #e6e6e6;"><span class="label label-md label-primary" style="font-size: 11px;">' + data.tanggal + ' - ' + data.carabayar + '</label></td>';
				html += '<td style="background-color: #e6e6e6;"><a href="{base_url}tkas/create/kontrabon/' + data.tanggal + '/' + data.idcarabayar + '" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-arrow-right"></i> Transaksi</a></td>';
				html += '</tr>';

				$('#historyKontrabon tbody').append(html);
			}
		});
	}

	function getHeaderGroupCarm(idtransaksi) {
		$.ajax({
			url: '{site_url}tpencairan/getHeaderGroupCarm/' + idtransaksi,
			dataType: 'json',
			async: false,
			success: function(data) {
				var html = '';

				html += '<tr>';
				html += '<td colspan="3" style="background-color: #e6e6e6;"><span class="label label-md label-primary" style="font-size: 11px;">' + data.notransaksi + '</label></td>';
				html += '<td style="background-color: #e6e6e6;"><button type="button" data-idrow="' + data.idtransaksi + '" data-idtipe="carm" class="btn btn-xs btn-danger removeDataGroup"><i class="fa fa-trash"></i> Hapus</button></td>';
				html += '</tr>';

				$('#historyCarm tbody').append(html);
			}
		});
	}

	function getHeaderGroupGajiKaryawan(idtransaksi) {
		$.ajax({
			url: '{site_url}tpencairan/getHeaderGroupGajiKaryawan/' + idtransaksi,
			dataType: 'json',
			async: false,
			success: function(data) {
				var html = '';

				html += '<tr>';
				html += '<td colspan="3" style="background-color: #e6e6e6;"><span class="label label-md label-primary" style="font-size: 11px;">' + data.notransaksi + '</label></td>';
				html += '<td style="background-color: #e6e6e6;"><button type="button" data-idrow="' + data.idtransaksi + '" data-idtipe="gajikaryawan" class="btn btn-xs btn-danger removeDataGroup"><i class="fa fa-trash"></i> Hapus</button></td>';
				html += '</tr>';

				$('#historyGajiKaryawan tbody').append(html);
			}
		});
	}

	// Get History
	function getHistoryPengajuan(idpencairan) {
		$.ajax({
			url: '{site_url}tpencairan/getHistoryPengajuan/' + idpencairan,
			dataType: 'json',
			success: function(data) {
				var html = '';
				$('#historyPengajuan tbody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);
					html += '<tr>';
					html += '<td>' + data[i].nopengajuan + '</td>';
					html += '<td>' + data[i].tipe + '</td>';
					html += '<td>' + data[i].subjek + '</td>';
					html += '<td>' + data[i].termin + '</td>';
					html += '<td>' + data[i].tanggal + '</td>';
					html += '<td>' + data[i].tanggaldibutuhkan + '</td>';
					html += '<td>' + data[i].namapemohon + '</td>';
					html += '<td>' + data[i].namaunit + '</td>';
					// html += '<td><button type="button" data-idrow="' + data[i].idrow + '" data-idpengajuan="' + data[i].idpengajuan + '" data-idrow="' + data[i].idrow + '" class="btn btn-xs btn-primary editDataPengajuan"><i class="fa fa-pencil"></i> Ubah</button> <button type="button" data-idrow="' + data[i].idrow + '" class="btn btn-xs btn-danger removeData"><i class="fa fa-trash"></i> Hapus</button></td>';
					html += '<td><button type="button" data-idrow="' + data[i].idrow + '" data-idtipe="pengajuan" class="btn btn-xs btn-danger removeData"><i class="fa fa-trash"></i> Hapus</button> <a href="{base_url}tkas/create/pengajuan/' + data[i].id + '" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-arrow-right"></i> Transaksi</a></td>';
					html += '</tr>';

					$('#historyPengajuan tbody').html(html);
				}
			}
		});
	}

	function getHistoryKontrabon(idpencairan) {
		$.ajax({
			url: '{site_url}tpencairan/getHistoryKontrabon/' + idpencairan,
			dataType: 'json',
			success: function(data) {
				var html = '';
				var tanggalTemp = 0;
				var idcarabayarTemp = 0;
				$('#historyKontrabon tbody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);

					if(data[i].tanggalkontrabon != tanggalTemp && data[i].carabayar != idcarabayarTemp){
						getHeaderGroupKontrabon(data[i].tanggalkontrabon, data[i].carabayar);
					}

					html += '<tr>';
					html += '<td>' + data[i].nokontrabon + '</td>';
					html += '<td>' + data[i].tanggalkontrabon + '</td>';
					html += '<td>' + data[i].tipe + '</td>';
					html += '<td>' + $.number(data[i].grandtotalnominal) + '</td>';
					html += '<td><button type="button" data-idrow="' + data[i].idrow + '" data-idtipe="kontrabon" class="btn btn-xs btn-danger removeData"><i class="fa fa-trash"></i> Hapus</button></td>';
					html += '</tr>';

					$('#historyKontrabon tbody').append(html);
					tanggalTemp = data[i].tanggalkontrabon;
					idcarabayarTemp = data[i].carabayar;
				}
			}
		});
	}

	function getHistoryCarm(idpencairan) {
		$.ajax({
			url: '{site_url}tpencairan/getHistoryCarm/' + idpencairan,
			dataType: 'json',
			success: function(data) {
				var html = '';
				var idtransaksiTemp = 0;
				$('#historyCarm tbody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);

					if(data[i].idtransaksi != idtransaksiTemp){
						getHeaderGroupCarm(data[i].idtransaksi);
					}

					html += '<tr>';
					html += '<td>' + data[i].notransaksi + '</td>';
					html += '<td>' + data[i].namapemilik + '</td>';
					html += '<td>' + $.number(data[i].total) + '</td>';
					html += '<td><a href="{base_url}tkas/create/carm/' + data[i].id + '" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-arrow-right"></i> Transaksi</a></td>';
					html += '</tr>';

					$('#historyCarm tbody').append(html);
					idtransaksiTemp = data[i].idtransaksi;
				}
			}
		});
	}

	function getHistoryHonorDokter(idpencairan) {
		$.ajax({
			url: '{site_url}tpencairan/getHistoryHonorDokter/' + idpencairan,
			dataType: 'json',
			success: function(data) {
				var html = '';
				$('#historyHonorDokter tbody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);
					html += '<tr>';
					html += '<td>' + data[i].namadokter + '</td>';
					html += '<td>' + monthLabel(parseInt(data[i].bulan)) + '</td>';
					html += '<td>' + data[i].tahun + '</td>';
					html += '<td>' + $.number(data[i].nominal) + '</td>';
					html += '<td><button type="button" data-idrow="' + data[i].idrow + '" data-idtipe="honordokter" class="btn btn-xs btn-danger removeData"><i class="fa fa-trash"></i> Hapus</button> <a href="{base_url}tkas/create/honordokter/' + data[i].id + '" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-arrow-right"></i> Transaksi</a></td>';
					html += '</tr>';

					$('#historyHonorDokter tbody').html(html);
				}
			}
		});
	}

	function getHistoryKasbon(idpencairan) {
		$.ajax({
			url: '{site_url}tpencairan/getHistoryKasbon/' + idpencairan,
			dataType: 'json',
			success: function(data) {
				var html = '';
				$('#historyKasbon tbody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);
					html += '<tr>';
					html += '<td>' + data[i].tanggal + '</td>';
					html += '<td>' + data[i].tipe + '</td>';
					html += '<td>' + data[i].namapegawai + '</td>';
					html += '<td>' + data[i].catatan + '</td>';
					html += '<td>' + $.number(data[i].nominal) + '</td>';
					html += '<td><button type="button" data-idrow="' + data[i].idrow + '" data-idtipe="kasbon" class="btn btn-xs btn-danger removeData"><i class="fa fa-trash"></i> Hapus</button> <a href="{base_url}tkas/create/kasbon/' + data[i].id + '" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-arrow-right"></i> Transaksi</a></td>';
					html += '</tr>';

					$('#historyKasbon tbody').html(html);
				}
			}
		});
	}

	function getHistoryGajiKaryawan(idpencairan) {
		$.ajax({
			url: '{site_url}tpencairan/getHistoryGajiKaryawan/' + idpencairan,
			dataType: 'json',
			success: function(data) {
				var html = '';
				var idtransaksiTemp = 0;
				$('#historyGajiKaryawan tbody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);

					if(data[i].idtransaksi != idtransaksiTemp){
						getHeaderGroupGajiKaryawan(data[i].idtransaksi);
					}

					if(data[i].idsub == 0 && data[i].subrekapan == 1){
						html += '<tr>';
						html += '<td colspan="4"><b>' + data[i].namavariable + '</b></td>';
						html += '</tr>';
					}else{
						html += '<tr>';
						html += '<td>' + data[i].notransaksi + '</td>';
						html += '<td>' + data[i].namavariable + '</td>';
						html += '<td>' + $.number(data[i].nominal) + '</td>';
						html += '<td><a href="{base_url}tkas/create/gajikaryawan/' + data[i].id + '" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-arrow-right"></i> Transaksi</a></td>';
						html += '</tr>';
					}

					$('#historyGajiKaryawan tbody').append(html);
					idtransaksiTemp = data[i].idtransaksi;
				}
			}
		});
	}

	function getHistoryFeeKlinik(idpencairan) {
		$.ajax({
			url: '{site_url}tpencairan/getHistoryFeeKlinik/' + idpencairan,
			dataType: 'json',
			success: function(data) {
				var html = '';
				$('#historyFeeKlinik tbody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);
					html += '<tr>';
					html += '<td>' + data[i].namarumahsakit + '</td>';
					html += '<td>' + monthLabel(parseInt(data[i].bulan)) + ' ' + data[i].tahun + '</td>';
					html += '<td>' + $.number(data[i].total) + '</td>';
					html += '<td><button type="button" data-idrow="' + data[i].idrow + '" data-idtipe="feeklinik" class="btn btn-xs btn-danger removeData"><i class="fa fa-trash"></i> Hapus</button> <a href="{base_url}tkas/create/feeklinik/' + data[i].id + '" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-arrow-right"></i> Transaksi</a></td>';
					html += '</tr>';

					$('#historyFeeKlinik tbody').html(html);
				}
			}
		});
	}

	// List Dynamic Content
	function listPengajuanTable(idtipepengajuan) {
		$('#listPengajuanTable').DataTable().destroy();
		$('#listPengajuanTable').DataTable({
				"autoWidth": false,
				"pageLength": 5,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tpencairan/getListPengajuan/' + idtipepengajuan,
					type: "POST",
					dataType: 'json'
				}
			});
	}

	function listKontrabonTable(idtipekontrabon) {
		$('#listKontrabonTable').DataTable().destroy();
		$('#listKontrabonTable').DataTable({
			"autoWidth": false,
			"pageLength": 5,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}tpencairan/getListKontrabon/' + idtipekontrabon,
				type: "POST",
				dataType: 'json'
			}
		});
	}

	function listHonorDokterTable(tahun, bulan, iddokter) {
		$('#listHonorDokterTable').DataTable().destroy();
		$('#listHonorDokterTable').DataTable({
			"autoWidth": false,
			"pageLength": 5,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}tpencairan/getListHonorDokter/' + tahun + '/' + bulan + '/' + iddokter,
				type: "POST",
				dataType: 'json'
			}
		});
	}

	function listFeeKlinikTable(tahun, bulan, idrumahsakit) {
		$('#listFeeKlinikTable').DataTable().destroy();
		$('#listFeeKlinikTable').DataTable({
			"autoWidth": false,
			"pageLength": 5,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}tpencairan/getListFeeKlinik/' + tahun + '/' + bulan + '/' + idrumahsakit,
				type: "POST",
				dataType: 'json'
			}
		});
	}

	function format(data) {
		var rows = '';

		rows += '<div class="col-md-12">';
		rows += '	<div class="col-md-6">';
		rows += '		<table class="table" style="background-color: transparent;">';
		rows += '			<tr>';
		rows += '				<td width="30%"><b>Tanggal Pencairan</b></td>';
		rows += '				<td width="70%">' + data.tanggal + '</td>';
		rows += '			</tr>';
		rows += '			<tr>';
		rows += '				<td width="30%"><b>Deskripsi</b></td>';
		rows += '				<td width="70%">' + data.deskripsi + '</td>';
		rows += '			</tr>';
		rows += '	</table>';
		rows += '	</div>';
		rows += '	<div class="col-md-6">';
		rows += '		<table class="table" style="background-color: transparent;">';
		rows += '			<tr>';
		rows += '				<td width="30%"><b>Catatan</b></td>';
		rows += '				<td width="70%">' + data.catatan + '</td>';
		rows += '			</tr>';
		rows += '	</table>';
		rows += '	</div>';
		rows += '</div>';
		rows += '<div class="col-md-12" style="margin: 10px;">';
    rows += '	<div class="btn-group">';
		rows += '		<button data-toggle="modal" data-target="#AksiModal" data-tab="tabPengajuan" class="btn btn-primary btn-sm openModalAksi"><i class="fa fa-newspaper-o"></i> Pengajuan</button>';
		rows += '		<button data-toggle="modal" data-target="#AksiModal" data-tab="tabKontrabon" class="btn btn-primary btn-sm openModalAksi"><i class="fa fa-newspaper-o"></i> Kontrabon</button>';
		rows += '		<button data-toggle="modal" data-target="#AksiModal" data-tab="tabCarm" class="btn btn-primary btn-sm openModalAksi"><i class="fa fa-newspaper-o"></i> C-Arm</button>';
		rows += '		<button data-toggle="modal" data-target="#AksiModal" data-tab="tabHonorDokter" class="btn btn-primary btn-sm openModalAksi"><i class="fa fa-newspaper-o"></i> Honor Dokter</button>';
		rows += '		<button data-toggle="modal" data-target="#AksiModal" data-tab="tabKasbon" class="btn btn-primary btn-sm openModalAksi"><i class="fa fa-newspaper-o"></i> Kasbon</button>';
		rows += '		<button data-toggle="modal" data-target="#AksiModal" data-tab="tabGajiKaryawan" class="btn btn-primary btn-sm openModalAksi"><i class="fa fa-newspaper-o"></i> Gaji Karyawan</button>';
		rows += '		<button data-toggle="modal" data-target="#AksiModal" data-tab="tabFeeKlinik" class="btn btn-primary btn-sm openModalAksi"><i class="fa fa-newspaper-o"></i> Fee Klinik</button>';
    rows += '	</div>';
    rows += '	&nbsp;&nbsp;&nbsp;';
		rows += '	<div class="btn-group">';
		rows += '		<a href="{base_url}tpencairan/review/' + data.id + '" target="_blank" class="btn btn-success btn-sm"><i class="fa fa-newspaper-o"></i> Rekapan</a>';
		rows += '	</div>';
		rows += '</div>';

		$("#tempIdPencairan").val(data.id);

		return rows;
	}

	function butonLock(idtipe){
		var action = '<div class="btn-group"><button class="btn btn-primary saveData" data-idtipe="' + idtipe + '"><i class="fa fa-check"> Simpan</i></button> <button class="cancelData" class="btn btn-danger"><i class="fa fa-reply"> Batal</i></button></div>';

		if(idtipe == 'pengajuan'){
			// Disabled Button Action
			$("#historyPengajuan button").prop( "disabled", true);
			$("#actionDataPengajuan").html(action);

			$("#fPengajuanRowId").val(idrow);
			$('#fPengajuanId').select2("trigger", "select", {data : {id: idtransaksi, text: notransaksi}});
		}else if(idtipe == 'kontrabon'){
			// Disabled Button Action
			$("#historyKontrabon button").prop( "disabled", true);
			$("#actionDataKontrabon").html(action);

			$("#fKontrabonRowId").val(idrow);
			$('#fKontrabonId').select2("trigger", "select", {data : {id: idtransaksi, text: notransaksi}});
		}else if(idtipe == 'carm'){
			// Disabled Button Action
			$("#historyCarm button").prop( "disabled", true);
			$("#actionDataCarm").html(action);

			$("#fCarmRowId").val(idrow);
			$('#fCarmId').select2("trigger", "select", {data : {id: idtransaksi, text: notransaksi}});
		}else if(idtipe == 'honordokter'){
			// Disabled Button Action
			$("#historyHonorDokter button").prop( "disabled", true);
			$("#actionDataHonorDokter").html(action);

			$("#fHonorDokterRowId").val(idrow);
			$('#fHonorDokterId').select2("trigger", "select", {data : {id: idtransaksi, text: notransaksi}});
		}else if(idtipe == 'kasbon'){
			// Disabled Button Action
			$("#historyKasbon button").prop( "disabled", true);
			$("#actionDataKasbon").html(action);

			$("#fKasbonRowId").val(idrow);
			$('#fKasbonId').select2("trigger", "select", {data : {id: idtransaksi, text: notransaksi}});
		}else if(idtipe == 'gajikaryawan'){
			// Disabled Button Action
			$("#historyGajiKaryawan button").prop( "disabled", true);
			$("#actionDataGajiKaryawan").html(action);

			$("#fGajiKaryawanRowId").val(idrow);
			$('#fGajiKaryawanId').select2("trigger", "select", {data : {id: idtransaksi, text: notransaksi}});
		}else if(idtipe == 'feeklinik'){
			// Disabled Button Action
			$("#historyFeeKlinik button").prop( "disabled", true);
			$("#actionDataFeeKlinik").html(action);

			$("#fFeeKlinikRowId").val(idrow);
			$('#fFeeKlinikId').select2("trigger", "select", {data : {id: idtransaksi, text: notransaksi}});
		}
	}

	function butonUnlock(idtipe){
		var action = '<button class="btn btn-primary saveData" data-idtipe="' + idtipe + '"><i class="fa fa-plus-circle"></i> Tambah</button>';

		if(idtipe == 'pengajuan'){
			// Enable Button Action
			$("#historyPengajuan button").prop( "disabled", false);
			$("#actionDataPengajuan").html(action);

			// Clear Form
			$('#fPengajuanRowId').val("");
			$('#fPengajuanId').select2("trigger", "select", {data : {id: '',text: ''}});
		}else if(idtipe == 'kontrabon'){
			// Enable Button Action
			$("#historyKontrabon button").prop( "disabled", false);
			$("#actionDataKontrabon").html(action);

			// Clear Form
			$('#fKontrabonRowId').val("");
			$('#fKontrabonId').select2("trigger", "select", {data : {id: '',text: ''}});
		}else if(idtipe == 'carm'){
			// Enable Button Action
			$("#historyCarm button").prop( "disabled", false);
			$("#actionDataCarm").html(action);

			// Clear Form
			$('#fCarmRowId').val("");
			$('#fCarmId').select2("trigger", "select", {data : {id: '',text: ''}});
		}else if(idtipe == 'honordokter'){
			// Enable Button Action
			$("#historyHonorDokter button").prop( "disabled", false);
			$("#actionDataHonorDokter").html(action);

			// Clear Form
			$('#fHonorDokterRowId').val("");
			$('#fHonorDokterId').select2("trigger", "select", {data : {id: '',text: ''}});
		}else if(idtipe == 'kasbon'){
			// Enable Button Action
			$("#historyKasbon button").prop( "disabled", false);
			$("#actionDataKasbon").html(action);

			// Clear Form
			$('#fKasbonRowId').val("");
			$('#fKasbonId').select2("trigger", "select", {data : {id: '',text: ''}});
		}else if(idtipe == 'gajikaryawan'){
			// Enable Button Action
			$("#historyGajiKaryawan button").prop( "disabled", false);
			$("#actionDataGajiKaryawan").html(action);

			// Clear Form
			$('#fGajiKaryawanRowId').val("");
			$('#fGajiKaryawanId').select2("trigger", "select", {data : {id: '',text: ''}});
		}else if(idtipe == 'feeklinik'){
			// Enable Button Action
			$("#historyFeeKlinik button").prop( "disabled", false);
			$("#actionDataFeeKlinik").html(action);

			// Clear Form
			$('#fFeeKlinikRowId').val("");
			$('#fFeeKlinikId').select2("trigger", "select", {data : {id: '',text: ''}});
		}
	}

	function monthLabel(date)
	{
		var text;
	  switch (date) {
	    case 1 :
					text = "Januari";
	        break;
	    case 2 :
					text = "Februari";
	        break;
	    case 3 :
					text = "Maret";
	        break;
	    case 4 :
					text = "April";
	        break;
	    case 5 :
					text = "Mei";
	        break;
	    case 6 :
					text = "Juni";
	        break;
	    case 7 :
					text = "Juli";
	        break;
	    case 8 :
					text = "Agustus";
	        break;
	    case 9 :
					text = "September";
	        break;
	    case 10 :
					text = "Oktober";
	        break;
	    case 11 :
					text = "November";
	        break;
	    case 12 :
					text = "Desember";
	        break;
	    }
	    return text;
	}
</script>
