<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<style media="screen">
	.text-bold {
		font-weight: bold;
	}
</style>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tpencairan" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">

		<div class="btn-group">
			<a href="{base_url}#" class="btn btn-primary"><i class="fa fa-print"></i> Cetak Rekapan</a>
		</div>

		<hr>

		<b><span class="label label-success" style="font-size:12px">PENGAJUAN</span></b>
		<table id="historyPengajuan" class="table table-bordered table-striped" width="100%" style="margin-top: 10px;">
			<thead>
				<tr>
					<th>No. Transaksi</th>
					<th>Tipe</th>
					<th>Nama Transaksi</th>
					<th>Termin / Cicilan</th>
					<th>Tanggal Pengajuan</th>
					<th>Tanggal Dibutuhkan</th>
					<th>Pemohon</th>
					<th>Keperluan Unit</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>

		<hr>

		<b><span class="label label-success" style="font-size:12px">KONTRABON</span></b>
		<table id="historyKontrabon" class="table table-bordered table-striped" width="100%" style="margin-top: 10px;">
			<thead>
				<tr>
					<th>No. Kontrabon</th>
					<th>Tanggal</th>
					<th>Tipe</th>
					<th>Nominal</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>

		<hr>

		<b><span class="label label-success" style="font-size:12px">C-ARM</span></b>
		<table id="historyCarm" class="table table-bordered table-striped" width="100%" style="margin-top: 10px;">
			<thead>
				<tr>
					<th>No. Transaksi</th>
					<th>Nama</th>
					<th>Nominal</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>

		<hr>

		<hr>

		<b><span class="label label-success" style="font-size:12px">HONOR DOKTER</span></b>
		<table id="historyHonorDokter" class="table table-bordered table-striped" width="100%" style="margin-top: 10px;">
			<thead>
				<tr>
					<th>Nama Dokter</th>
					<th>Periode Bulan</th>
					<th>Periode Tahun</th>
					<th>Nominal</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>

		<hr>

		<b><span class="label label-success" style="font-size:12px">KASBON</span></b>
		<table id="historyKasbon" class="table table-bordered table-striped" width="100%" style="margin-top: 10px;">
			<thead>
				<tr>
					<th>Tanggal</th>
					<th>Tipe</th>
					<th>Nama</th>
					<th>Catatan</th>
					<th>Nominal</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>

		<hr>

		<b><span class="label label-success" style="font-size:12px">GAJI KARYAWAN</span></b>
		<table id="historyGajiKaryawan" class="table table-bordered table-striped" width="100%" style="margin-top: 10px;">
			<thead>
				<tr>
					<th>No. Transaksi</th>
					<th>Variable</th>
					<th>Nominal</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>

		<hr>

		<b><span class="label label-success" style="font-size:12px">FEE KLINIK</span></b>
		<table id="historyFeeKlinik" class="table table-bordered table-striped" width="100%" style="margin-top: 10px;">
			<thead>
				<tr>
					<th>Nama Klinik</th>
					<th>Periode</th>
					<th>Nominal</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>

	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>


<script type="text/javascript">
	$(document).ready(function() {

		var idpencairan = {idpencairan};

		// Get History Data
		getHistoryPengajuan(idpencairan);
		getHistoryKontrabon(idpencairan);
		getHistoryCarm(idpencairan);
		getHistoryHonorDokter(idpencairan);
		getHistoryKasbon(idpencairan);
		getHistoryGajiKaryawan(idpencairan);
		getHistoryFeeKlinik(idpencairan);

		$(document).on('click', '.removeData', function() {
			var idtipe = $(this).data('idtipe');
		  var idrow = $(this).data('idrow');

		  event.preventDefault();
		  swal({
		      title: "Apakah anda yakin ingin menghapus data ini?",
		      type: "warning",
		      showCancelButton: true,
		      confirmButtonColor: "#DD6B55",
		      confirmButtonText: "Ya",
		      cancelButtonText: "Tidak",
		      closeOnConfirm: false,
		      closeOnCancel: false
		    }).then(function() {
		      // Approve Status
		      $.ajax({
		        url: '{site_url}tpencairan/removeData',
		        method: 'POST',
		        data: {
		          idrow: idrow,
		        },
		        success: function(data) {
		          swal({
		            title: "Berhasil!",
		            text: "Data Transaksi Telah Dihapus.",
		            type: "success",
		            timer: 1500,
		            showConfirmButton: false
		          });

							if(idtipe == 'pengajuan'){
								getHistoryPengajuan(idpencairan);
							}else if(idtipe == 'kontrabon'){
								getHistoryKontrabon(idpencairan);
							}else if(idtipe == 'carm'){
								getHistoryCarm(idpencairan);
							}else if(idtipe == 'honordokter'){
								getHistoryHonorDokter(idpencairan);
							}else if(idtipe == 'kasbon'){
								getHistoryKasbon(idpencairan);
							}else if(idtipe == 'gajikaryawan'){
								getHistoryGajiKaryawan(idpencairan);
							}else if(idtipe == 'feeklinik'){
								getHistoryFeeKlinik(idpencairan);
							}
		        }
		      });
		    });
		});

		$(document).on('click', '.removeDataGroup', function() {
			var idpencairan = $('#tempIdPencairan').val();
			var idtipe = $(this).data('idtipe');
		  var idrow = $(this).data('idrow');

		  event.preventDefault();
		  swal({
		      title: "Apakah anda yakin ingin menghapus data ini?",
		      type: "warning",
		      showCancelButton: true,
		      confirmButtonColor: "#DD6B55",
		      confirmButtonText: "Ya",
		      cancelButtonText: "Tidak",
		      closeOnConfirm: false,
		      closeOnCancel: false
		    }).then(function() {
		      // Approve Status
		      $.ajax({
		        url: '{site_url}tpencairan/removeDataGroup',
		        method: 'POST',
		        data: {
		          idtipe: idtipe,
		          idrow: idrow,
		        },
		        success: function(data) {
		          swal({
		            title: "Berhasil!",
		            text: "Data Transaksi Telah Dihapus.",
		            type: "success",
		            timer: 1500,
		            showConfirmButton: false
		          });

							if(idtipe == 'pengajuan'){
								getHistoryPengajuan(idpencairan);
							}else if(idtipe == 'kontrabon'){
								getHistoryKontrabon(idpencairan);
							}else if(idtipe == 'carm'){
								getHistoryCarm(idpencairan);
							}else if(idtipe == 'honordokter'){
								getHistoryHonorDokter(idpencairan);
							}else if(idtipe == 'kasbon'){
								getHistoryKasbon(idpencairan);
							}else if(idtipe == 'gajikaryawan'){
								getHistoryGajiKaryawan(idpencairan);
							}else if(idtipe == 'feeklinik'){
								getHistoryFeeKlinik(idpencairan);
							}
		        }
		      });
		    });
		});

	});

	// Get Header Group
	function getHeaderGroupKontrabon(tanggal, idcarabayar) {
		$.ajax({
			url: '{site_url}tpencairan/getHeaderGroupKontrabon/' + tanggal + '/' + idcarabayar,
			dataType: 'json',
			async: false,
			success: function(data) {
				var html = '';

				html += '<tr>';
				html += '<td colspan="4" style="background-color: #e6e6e6;"><span class="label label-md label-primary" style="font-size: 11px;">' + data.tanggal + ' - ' + data.carabayar + '</label></td>';
				html += '<td style="background-color: #e6e6e6;"><a href="{base_url}tkas/create/kontrabon/' + data.tanggal + '/' + data.idcarabayar + '" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-arrow-right"></i> Transaksi</a></td>';
				html += '</tr>';

				$('#historyKontrabon tbody').append(html);
			}
		});
	}

	function getHeaderGroupCarm(idtransaksi) {
		$.ajax({
			url: '{site_url}tpencairan/getHeaderGroupCarm/' + idtransaksi,
			dataType: 'json',
			async: false,
			success: function(data) {
				var html = '';

				html += '<tr>';
				html += '<td colspan="3" style="background-color: #e6e6e6;"><span class="label label-md label-primary" style="font-size: 11px;">' + data.notransaksi + '</label></td>';
				html += '<td style="background-color: #e6e6e6;"><button type="button" data-idrow="' + data.idtransaksi + '" data-idtipe="carm" class="btn btn-xs btn-danger removeDataGroup"><i class="fa fa-trash"></i> Hapus</button></td>';
				html += '</tr>';

				$('#historyCarm tbody').append(html);
			}
		});
	}

	function getHeaderGroupGajiKaryawan(idtransaksi) {
		$.ajax({
			url: '{site_url}tpencairan/getHeaderGroupGajiKaryawan/' + idtransaksi,
			dataType: 'json',
			async: false,
			success: function(data) {
				var html = '';

				html += '<tr>';
				html += '<td colspan="3" style="background-color: #e6e6e6;"><span class="label label-md label-primary" style="font-size: 11px;">' + data.notransaksi + '</label></td>';
				html += '<td style="background-color: #e6e6e6;"><button type="button" data-idrow="' + data.idtransaksi + '" data-idtipe="gajikaryawan" class="btn btn-xs btn-danger removeDataGroup"><i class="fa fa-trash"></i> Hapus</button></td>';
				html += '</tr>';

				$('#historyGajiKaryawan tbody').append(html);
			}
		});
	}

	// Get History
	function getHistoryPengajuan(idpencairan) {
		$.ajax({
			url: '{site_url}tpencairan/getHistoryPengajuan/' + idpencairan,
			dataType: 'json',
			success: function(data) {
				var html = '';
				$('#historyPengajuan tbody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);
					html += '<tr>';
					html += '<td>' + data[i].nopengajuan + '</td>';
					html += '<td>' + data[i].tipe + '</td>';
					html += '<td>' + data[i].subjek + '</td>';
					html += '<td>' + data[i].termin + '</td>';
					html += '<td>' + data[i].tanggal + '</td>';
					html += '<td>' + data[i].tanggaldibutuhkan + '</td>';
					html += '<td>' + data[i].namapemohon + '</td>';
					html += '<td>' + data[i].namaunit + '</td>';
					html += '<td><button type="button" data-idrow="' + data[i].idrow + '" data-idtipe="pengajuan" class="btn btn-xs btn-danger removeData"><i class="fa fa-trash"></i> Hapus</button> <a href="{base_url}tkas/create/pengajuan/' + data[i].id + '" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-arrow-right"></i> Transaksi</a></td>';
					html += '</tr>';

					$('#historyPengajuan tbody').html(html);
				}
			}
		});
	}

	function getHistoryKontrabon(idpencairan) {
		$.ajax({
			url: '{site_url}tpencairan/getHistoryKontrabon/' + idpencairan,
			dataType: 'json',
			success: function(data) {
				var html = '';
				var tanggalTemp = 0;
				var idcarabayarTemp = 0;
				$('#historyKontrabon tbody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);

					if(data[i].tanggalkontrabon != tanggalTemp && data[i].carabayar != idcarabayarTemp){
						getHeaderGroupKontrabon(data[i].tanggalkontrabon, data[i].carabayar);
					}

					html += '<tr>';
					html += '<td>' + data[i].nokontrabon + '</td>';
					html += '<td>' + data[i].tanggalkontrabon + '</td>';
					html += '<td>' + data[i].tipe + '</td>';
					html += '<td>' + $.number(data[i].grandtotalnominal) + '</td>';
					html += '<td><button type="button" data-idrow="' + data[i].idrow + '" data-idtipe="kontrabon" class="btn btn-xs btn-danger removeData"><i class="fa fa-trash"></i> Hapus</button></td>';
					html += '</tr>';

					$('#historyKontrabon tbody').append(html);
					tanggalTemp = data[i].tanggalkontrabon;
					idcarabayarTemp = data[i].carabayar;
				}
			}
		});
	}

	function getHistoryCarm(idpencairan) {
		$.ajax({
			url: '{site_url}tpencairan/getHistoryCarm/' + idpencairan,
			dataType: 'json',
			success: function(data) {
				var html = '';
				var idtransaksiTemp = 0;
				$('#historyCarm tbody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);

					if(data[i].idtransaksi != idtransaksiTemp){
						getHeaderGroupCarm(data[i].idtransaksi);
					}

					html += '<tr>';
					html += '<td>' + data[i].notransaksi + '</td>';
					html += '<td>' + data[i].namapemilik + '</td>';
					html += '<td>' + $.number(data[i].total) + '</td>';
					html += '<td><a href="{base_url}tkas/create/carm/' + data[i].id + '" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-arrow-right"></i> Transaksi</a></td>';
					html += '</tr>';

					$('#historyCarm tbody').append(html);
					idtransaksiTemp = data[i].idtransaksi;
				}
			}
		});
	}

	function getHistoryHonorDokter(idpencairan) {
		$.ajax({
			url: '{site_url}tpencairan/getHistoryHonorDokter/' + idpencairan,
			dataType: 'json',
			success: function(data) {
				var html = '';
				$('#historyHonorDokter tbody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);
					html += '<tr>';
					html += '<td>' + data[i].namadokter + '</td>';
					html += '<td>' + monthLabel(parseInt(data[i].bulan)) + '</td>';
					html += '<td>' + data[i].tahun + '</td>';
					html += '<td>' + $.number(data[i].nominal) + '</td>';
					html += '<td><button type="button" data-idrow="' + data[i].idrow + '" data-idtipe="honordokter" class="btn btn-xs btn-danger removeData"><i class="fa fa-trash"></i> Hapus</button> <a href="{base_url}tkas/create/honordokter/' + data[i].id + '" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-arrow-right"></i> Transaksi</a></td>';
					html += '</tr>';

					$('#historyHonorDokter tbody').html(html);
				}
			}
		});
	}

	function getHistoryKasbon(idpencairan) {
		$.ajax({
			url: '{site_url}tpencairan/getHistoryKasbon/' + idpencairan,
			dataType: 'json',
			success: function(data) {
				var html = '';
				$('#historyKasbon tbody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);
					html += '<tr>';
					html += '<td>' + data[i].tanggal + '</td>';
					html += '<td>' + data[i].tipe + '</td>';
					html += '<td>' + data[i].namapegawai + '</td>';
					html += '<td>' + data[i].catatan + '</td>';
					html += '<td>' + $.number(data[i].nominal) + '</td>';
					html += '<td><button type="button" data-idrow="' + data[i].idrow + '" data-idtipe="kasbon" class="btn btn-xs btn-danger removeData"><i class="fa fa-trash"></i> Hapus</button> <a href="{base_url}tkas/create/kasbon/' + data[i].id + '" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-arrow-right"></i> Transaksi</a></td>';
					html += '</tr>';

					$('#historyKasbon tbody').html(html);
				}
			}
		});
	}

	function getHistoryGajiKaryawan(idpencairan) {
		$.ajax({
			url: '{site_url}tpencairan/getHistoryGajiKaryawan/' + idpencairan,
			dataType: 'json',
			success: function(data) {
				var html = '';
				var idtransaksiTemp = 0;
				$('#historyGajiKaryawan tbody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);

					if(data[i].idtransaksi != idtransaksiTemp){
						getHeaderGroupGajiKaryawan(data[i].idtransaksi);
					}

					if(data[i].idsub == 0 && data[i].subrekapan == 1){
						html += '<tr>';
						html += '<td colspan="4"><b>' + data[i].namavariable + '</b></td>';
						html += '</tr>';
					}else{
						html += '<tr>';
						html += '<td>' + data[i].notransaksi + '</td>';
						html += '<td>' + data[i].namavariable + '</td>';
						html += '<td>' + $.number(data[i].nominal) + '</td>';
						html += '<td><a href="{base_url}tkas/create/gajikaryawan/' + data[i].id + '" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-arrow-right"></i> Transaksi</a></td>';
						html += '</tr>';
					}

					$('#historyGajiKaryawan tbody').append(html);
					idtransaksiTemp = data[i].idtransaksi;
				}
			}
		});
	}

	function getHistoryFeeKlinik(idpencairan) {
		$.ajax({
			url: '{site_url}tpencairan/getHistoryFeeKlinik/' + idpencairan,
			dataType: 'json',
			success: function(data) {
				var html = '';
				$('#historyFeeKlinik tbody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);
					html += '<tr>';
					html += '<td>' + data[i].namarumahsakit + '</td>';
					html += '<td>' + monthLabel(parseInt(data[i].bulan)) + ' ' + data[i].tahun + '</td>';
					html += '<td>' + $.number(data[i].total) + '</td>';
					html += '<td><button type="button" data-idrow="' + data[i].idrow + '" data-idtipe="feeklinik" class="btn btn-xs btn-danger removeData"><i class="fa fa-trash"></i> Hapus</button> <a href="{base_url}tkas/create/feeklinik/' + data[i].id + '" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-arrow-right"></i> Transaksi</a></td>';
					html += '</tr>';

					$('#historyFeeKlinik tbody').html(html);
				}
			}
		});
	}

	function monthLabel(date)
	{
		var text;
	  switch (date) {
	    case 1 :
					text = "Januari";
	        break;
	    case 2 :
					text = "Februari";
	        break;
	    case 3 :
					text = "Maret";
	        break;
	    case 4 :
					text = "April";
	        break;
	    case 5 :
					text = "Mei";
	        break;
	    case 6 :
					text = "Juni";
	        break;
	    case 7 :
					text = "Juli";
	        break;
	    case 8 :
					text = "Agustus";
	        break;
	    case 9 :
					text = "September";
	        break;
	    case 10 :
					text = "Oktober";
	        break;
	    case 11 :
					text = "November";
	        break;
	    case 12 :
					text = "Desember";
	        break;
	    }
	    return text;
	}
</script>
