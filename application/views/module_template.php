<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>RSKB Halmahera | {title}</title>

  <meta name="description" content="Sistem Rumah Sakit RSKB Halmahera">
  <meta name="author" content="Sinergi SIMRS">
  <meta name="robots" content="noindex, nofollow">

  <link rel="apple-touch-icon" sizes="57x57" href="{img_path}favicons/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="{img_path}favicons/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="{img_path}favicons/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="{img_path}favicons/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="{img_path}favicons/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="{img_path}favicons/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="{img_path}favicons/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="{img_path}favicons/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="{img_path}favicons/apple-icon-180x180.png">

  <link rel="icon" type="image/png" sizes="192x192" href="{img_path}favicons/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="{img_path}favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="{img_path}favicons/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="{img_path}favicons/favicon-16x16.png">

  <link rel="manifest" href="{img_path}favicons/manifest.json">

  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="{img_path}favicons/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <link rel="stylesheet" href="{plugins_path}bootstrap-datepicker/bootstrap-datepicker3.min.css">
  <link rel="stylesheet" href="{plugins_path}bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
  <link rel="stylesheet" href="{plugins_path}bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="{plugins_path}select2/select2.min.css">
  <link rel="stylesheet" href="{plugins_path}select2/select2-bootstrap.min.css">
  <link rel="stylesheet" href="{plugins_path}jquery-auto-complete/jquery.auto-complete.min.css">
  <link rel="stylesheet" href="{plugins_path}ion-rangeslider/css/ion.rangeSlider.min.css">
  <link rel="stylesheet" href="{plugins_path}ion-rangeslider/css/ion.rangeSlider.skinHTML5.min.css">
  <link rel="stylesheet" href="{plugins_path}dropzonejs/dropzone.min.css">
  <link rel="stylesheet" href="{plugins_path}jquery-tags-input/jquery.tagsinput.min.css">

  <link rel="stylesheet" href="{js_path}plugins/datatables/jquery.dataTables.min.css">
  <link rel="stylesheet" href="{js_path}plugins/slick/slick.min.css">
  <link rel="stylesheet" href="{js_path}plugins/slick/slick-theme.min.css">
  <link rel="stylesheet" href="{plugins_path}jquery-auto-complete/jquery.auto-complete.min.css">
  <script type="text/javascript" src="{js_path}core/jquery.min.js"></script>

  <link rel="stylesheet" href="{js_path}plugins/sweetalert2/sweetalert2.min.css">

  <link rel="stylesheet" href="{ajax}datatables.net-bs4/css/dataTables.bootstrap4.css">
  <script src="{ajax}datatables.net-bs4/jquery.dataTables.min.js"></script>
  <script src="{ajax}datatable.js"></script>

  <link rel="stylesheet" href="{css_path}bootstrap.min.css">
  <link rel="stylesheet" href="{css_path}fonts.googleapis.com.css">
  <link rel="stylesheet" id="css-main" href="{css_path}oneui.css">
  <link rel="stylesheet" href="{css_path}ionicons.min.css">

  <script src="{plugins_path}bootstrap-select/bootstrap-select.min.js"></script>
  <link rel="stylesheet" href="{plugins_path}bootstrap-select/bootstrap-select.css">

  <link rel="stylesheet" href="{js_path}plugins/summernote/summernote.min.css">
  <link rel="stylesheet" href="{js_path}plugins/simplemde/simplemde.min.css">

  <link rel="stylesheet" id="css-theme" href="{css_path}themes/modern.min.css">
  
  <script src="{plugins_path}autoNumeric/autoNumeric.min.js"></script>
  
  <!-- PhotoSwipe -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.0/photoswipe.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.0/default-skin/default-skin.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.0/photoswipe.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.0/photoswipe-ui-default.min.js"></script>

  <style>
  .text-red {
    color: #ff3115;
}
    #cover-spin {
      position: fixed;
      width: 100%;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
      background-color: rgba(255, 255, 255, 0.7);
      z-index: 9999;
      display: none;
    }

    @-webkit-keyframes spin {
      from {
        -webkit-transform: rotate(0deg);
      }

      to {
        -webkit-transform: rotate(360deg);
      }
    }

    @keyframes spin {
      from {
        transform: rotate(0deg);
      }

      to {
        transform: rotate(360deg);
      }
    }

    #cover-spin::after {
      content: '';
      display: block;
      position: absolute;
      left: 48%;
      top: 40%;
      width: 40px;
      height: 40px;
      border-style: solid;
      border-color: black;
      border-top-color: transparent;
      border-width: 4px;
      border-radius: 50%;
      -webkit-animation: spin .8s linear infinite;
      animation: spin .8s linear infinite;
    }


    /* Absolute Center Spinner */
    .loading-full {
      position: fixed;
      z-index: 99999;
      height: 2em;
      width: 2em;
      overflow: show;
      margin: auto;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
    }

    /* Transparent Overlay */
    .loading-full:before {
      content: '';
      display: block;
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background: radial-gradient(rgba(20, 20, 20, .8), rgba(0, 0, 0, .8));

      background: -webkit-radial-gradient(rgba(20, 20, 20, .8), rgba(0, 0, 0, .8));
    }

    /* :not(:required) hides these rules from IE9 and below */
    .loading-full:not(:required) {
      /* hide "loading..." text */
      font: 0/0 a;
      color: transparent;
      text-shadow: none;
      background-color: transparent;
      border: 0;
    }

    .loading-full:not(:required):after {
      content: '';
      display: block;
      font-size: 10px;
      width: 1em;
      height: 1em;
      margin-top: -0.5em;
      -webkit-animation: spinner 150ms infinite linear;
      -moz-animation: spinner 150ms infinite linear;
      -ms-animation: spinner 150ms infinite linear;
      -o-animation: spinner 150ms infinite linear;
      animation: spinner 150ms infinite linear;
      border-radius: 0.5em;
      -webkit-box-shadow: rgba(255, 255, 255, 0.75) 1.5em 0 0 0, rgba(255, 255, 255, 0.75) 1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) 0 1.5em 0 0, rgba(255, 255, 255, 0.75) -1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) -1.5em 0 0 0, rgba(255, 255, 255, 0.75) -1.1em -1.1em 0 0, rgba(255, 255, 255, 0.75) 0 -1.5em 0 0, rgba(255, 255, 255, 0.75) 1.1em -1.1em 0 0;
      box-shadow: rgba(255, 255, 255, 0.75) 1.5em 0 0 0, rgba(255, 255, 255, 0.75) 1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) 0 1.5em 0 0, rgba(255, 255, 255, 0.75) -1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) -1.5em 0 0 0, rgba(255, 255, 255, 0.75) -1.1em -1.1em 0 0, rgba(255, 255, 255, 0.75) 0 -1.5em 0 0, rgba(255, 255, 255, 0.75) 1.1em -1.1em 0 0;
    }

    /* Animation */

    @-webkit-keyframes spinner {
      0% {
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
      }

      100% {
        -webkit-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }

    @-moz-keyframes spinner {
      0% {
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
      }

      100% {
        -webkit-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }

    @-o-keyframes spinner {
      0% {
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
      }

      100% {
        -webkit-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }

    @keyframes spinner {
      0% {
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
      }

      100% {
        -webkit-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }
  </style>
</head>

<body>
  <?php $menu = $this->uri->segment(1); ?>
  <div class="loading-full" style="display:none;">Loading&#8230;</div>
  <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
    <nav id="sidebar">
      <div id="sidebar-scroll">
        <div class="sidebar-content">
          <div class="side-header side-content" style="background-color: #fff;">
            <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout"
              data-action="sidebar_close">
              <i class="fa fa-times"></i>
            </button>

            <a class="h5 text-white" href="{base_url}dashboard">
              <img src="{upload_path}logo/logomenu.jpg" alt="" style="width: 120px; margin-left: 25%;">
            </a>
          </div>
          <div class="side-content side-content-full">
            <ul class="nav-main">
              <?php $this->load->view('module_navigation'); ?>
            </ul>
          </div>
        </div>
      </div>
    </nav>

    <header id="header-navbar" class="content-mini content-mini-full">
      <?php $this->load->view('module_header');?>
    </header>

    <main id="main-container">
      <?php if($menu != 'dashboard'){ ?>
      <div class="content">
      <?php } ?>

        <?php if($menu != 'dashboard'){ ?>
        <ol class="breadcrumb push-15">
          <?php echo BackendBreadcrum($breadcrum)?>
        </ol>
        <?php } ?>
		<div id="cover-spin"></div>
        <?php $this->load->view($content);?>
        <?php if($menu != 'dashboard'){ ?>
      </div>
      <?php } ?>
    </main>

    <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
      <div class="pull-left">
        <a class="font-w600" href="#" target="_blank">SIMRS RSKB Halmahera &copy; Des 2020</span>
      </div>
      <div class="pull-right">
        Created with <i class="fa fa-heart text-city"></i> by <a class="font-w600" href="https://kurvasoft.com/" target="_blank">Kurvasoft</a>
      </div>
    </footer>
  </div>


  <script src="{js_path}core/bootstrap.min.js"></script>
  <script src="{js_path}core/jquery.slimscroll.min.js"></script>
  <script src="{js_path}core/jquery.scrollLock.min.js"></script>
  <script src="{js_path}core/jquery.appear.min.js"></script>
  <script src="{js_path}core/jquery.countTo.min.js"></script>
  <script src="{js_path}core/jquery.placeholder.min.js"></script>
  <script src="{js_path}core/js.cookie.min.js"></script>
  <script src="{js_path}app.js"></script>

  <script src="{plugins_path}bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  <script src="{plugins_path}bootstrap-datetimepicker/moment.min.js"></script>
  <script src="{plugins_path}bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
  <script src="{plugins_path}bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
  <script src="{plugins_path}bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
  <script src="{plugins_path}select2/select2.full.min.js"></script>
  <script src="{plugins_path}masked-inputs/jquery.maskedinput.min.js"></script>
  <script src="{plugins_path}jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
  <script src="{plugins_path}jquery-auto-complete/jquery.auto-complete.min.js"></script>
  <script src="{plugins_path}ion-rangeslider/js/ion.rangeSlider.min.js"></script>
  <script src="{plugins_path}dropzonejs/dropzone.min.js"></script>
  <script src="{plugins_path}jquery-tags-input/jquery.tagsinput.min.js"></script>
  <script src="{plugins_path}datatables/jquery.dataTables.min.js"></script>
  <script src="{plugins_path}slick/slick.min.js"></script>
  <!-- <script src="{plugins_path}chartjs/Chart.min.js"></script> -->
  <script src="{plugins_path}custom-file-input/custom-file-input.js"></script>

  <script src="{plugins_path}bootstrap-notify/bootstrap-notify.min.js"></script>
  <script src="{plugins_path}sweetalert2/es6-promise.auto.min.js"></script>
  <script src="{plugins_path}sweetalert2/sweetalert2.min.js"></script>

  <script src="{js_path}plugins/summernote/summernote.min.js"></script>
  <script src="{js_path}plugins/ckeditor/ckeditor.js"></script>
  <script src="{js_path}plugins/simplemde/simplemde.min.js"></script>
  <script src="{js_path}pages/base_forms_pickers_more.js"></script>
  <script src="{js_path}pages/base_tables_datatables.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
  <script>
    // Sidebar Menu Set Config
    if (localStorage.getItem("sidebar")) {
      if (localStorage.getItem("sidebar") == 'full') {
        $("#page-container").removeClass('sidebar-mini');
      } else if (localStorage.getItem("sidebar") == 'mini') {
        $("#page-container").addClass('sidebar-mini');
      }
    } else {
      localStorage.setItem("sidebar", "full");
    }

    $("#sidebar_menu").click(function () {
      if (localStorage.getItem("sidebar") == 'full') {
        localStorage.setItem("sidebar", "mini");
      } else {
        localStorage.setItem("sidebar", "full");
      }
    });

    $('.table-responsive').on('show.bs.dropdown', function () {
        $('.table-responsive').css( "overflow", "inherit" );
    });

    $('.table-responsive').on('hide.bs.dropdown', function () {
        $('.table-responsive').css( "overflow", "auto" );
    })

    // $("button[type='submit']").click(async function() {
    //   let formInput = await $('input:required, select:required, textarea:required').filter(function() {
    //     return this.value.trim() == '';
    //   }).length;
    //
    //   if (await formInput == 0) {
    //     $(this).hide();
    //     $('.loading-full').show();
    //   }
    // });

    jQuery(function () {
      App.initHelpers(['datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs',
        'rangeslider', 'tags-inputs', 'slick', 'summernote', 'ckeditor', 'simplemde'
      ]);

      $('#datatable-simrs tbody').on('click', 'a.removeData', function () {
        table = $('#datatable-simrs').DataTable();
        urlIndex = $(this).data('urlindex');
        urlRemove = $(this).data('urlremove');
        warningText = ($(this).data('messagewarning') ? $(this).data('messagewarning') : 'Hapus record tersebut?');
        successText = ($(this).data('messagesuccess') ? $(this).data('messagesuccess') : 'Data telah dihapus.');
        setTimeout(function () {
          swal({
            text: warningText,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d26a5c',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            html: false,
            preConfirm: function () {
              return new Promise(function (resolve) {
                setTimeout(function () {
                  resolve();
                }, 100);
              })
            }
          }).then(
            function (result) {
              if (successText == 'Data telah dihapus.') {
                $.ajax({
                  url: urlRemove
                });
                table.ajax.reload();
                swal('Berhasil!', successText, 'success');
                setTimeout(function () {
                  window.location = urlIndex;
                }, 100);
              } else {
                window.location = urlRemove;
              }
            }
          )
        }, 100);
      });

      $('#datatable-simrs tbody').on('click', 'a.aktifData', function () {
        table = $('#datatable-simrs').DataTable();
        urlIndex = $(this).data('urlindex');
        urlRemove = $(this).data('urlremove');
        warningText = ($(this).data('messagewarning') ? $(this).data('messagewarning') : 'Aktifkan item tersebut?');
        successText = ($(this).data('messagesuccess') ? $(this).data('messagesuccess') : 'Data telah aktif.');
        setTimeout(function () {
          swal({
            text: warningText,
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#d26a5c',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            html: false,
            preConfirm: function () {
              return new Promise(function (resolve) {
                setTimeout(function () {
                  resolve();
                }, 100);
              })
            }
          }).then(
            function (result) {
              if (successText == 'Data telah diaktifkan.') {
                $.ajax({
                  url: urlRemove
                });
                table.ajax.reload();
                swal('Berhasil!', successText, 'success');
                setTimeout(function () {
                  window.location = urlIndex;
                }, 100);
              } else {
                window.location = urlRemove;
              }
            }
          )
        }, 100);
      });
    });

    function remove_row(obj) {
      event.preventDefault();
      swal({
        title: "Apa anda yakin?",
        text: "Anda tidak akan dapat memulihkan data ini!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya , batalkan !",
        cancelButtonText: "Tidak, batalkan !",
        closeOnConfirm: false,
        closeOnCancel: false
      }).then(function () {
        swal({
          title: 'Success!',
          text: 'Succes! Pembatalan Berhasil.',
          type: 'success',
          timer: 2000,
          showCancelButton: false,
          showConfirmButton: false
        });
        window.location = obj;
      }, function (dismiss) {
        if (dismiss == 'cancel') {
          swal({
            title: 'Cancelled!',
            text: 'Cancelled! Pembatalan dibatalkan!.',
            type: 'error',
            timer: 2000,
            showCancelButton: false,
            showConfirmButton: false
          });
        }
      });
    }

    function serahkan_row(obj) {
      event.preventDefault();
      swal({
        title: "Apa anda yakin?",
        text: "Anda tidak akan dapat mengembalikan status penyerahan!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya , serahkan  !",
        cancelButtonText: "Tidak, serahkan !",
        closeOnConfirm: false,
        closeOnCancel: false
      }).then(function () {
        swal({
          title: 'Success!',
          text: 'Succes! Penyerahan Berhasil.',
          type: 'success',
          timer: 2000,
          showCancelButton: false,
          showConfirmButton: false
        });
        window.location = obj;
      }, function (dismiss) {
        if (dismiss == 'cancel') {
          swal({
            title: 'Cancelled!',
            text: 'Cancelled! Penyerahan dibatalkan!.',
            type: 'error',
            timer: 2000,
            showCancelButton: false,
            showConfirmButton: false
          });
        }
      });
    }
	$(document).on("click",".menu_click",function(){
		$("#cover-spin").show();
	});

  </script>
</body>

</html>