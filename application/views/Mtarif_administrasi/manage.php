<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
      <li>
          <a href="{base_url}mtarif_administrasi/index/<?=$this->uri->segment(3)?>" class="btn"><i class="fa fa-reply"></i></a>
      </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mtarif_administrasi/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="idtipe">Tipe</label>
				<div class="col-md-7">
					<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($idtipe == 1 ? 'selected="selected"':'')?>>Rawat Jalan</option>
						<option value="2" <?=($idtipe == 2 ? 'selected="selected"':'')?>>Rawat Inap</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="idtipe">Jenis Tarif</label>
				<div class="col-md-7">
					<select name="idjenis" id="idjenis" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($idjenis == 1 ? 'selected="selected"':'')?>>Administrasi</option>
						<option value="2" <?=($idjenis == 2 ? 'selected="selected"':'')?>>Cetak Kartu</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div id="form-detail-rawatjalan" style="display:none">
				<div class="form-group">
					<label class="col-md-3 control-label" for="jasasarana">Jasa Sarana</label>
					<div class="col-md-7">
						<input type="text" class="form-control number" id="jasasarana" onkeyup="sumTotal()" placeholder="Jasa Sarana" name="jasasarana" value="{jasasarana}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="jasapelayanan">Jasa Pelayanan</label>
					<div class="col-md-7">
						<input type="text" class="form-control number" id="jasapelayanan" onkeyup="sumTotal()" placeholder="Jasa Pelayanan" name="jasapelayanan" value="{jasapelayanan}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="bhp">BHP</label>
					<div class="col-md-7">
						<input type="text" class="form-control number" id="bhp" onkeyup="sumTotal()" placeholder="BHP" name="bhp" value="{bhp}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="biayaperawatan">Biaya Perawatan</label>
					<div class="col-md-7">
						<input type="text" class="form-control number" id="biayaperawatan" onkeyup="sumTotal()" placeholder="Biaya Perawatan" name="biayaperawatan" value="{biayaperawatan}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="total">Total Tarif</label>
					<div class="col-md-7">
						<input type="text" class="form-control number" id="total" placeholder="Total Tarif" name="total" value="{total}" readonly>
					</div>
				</div>
			</div>
			<div id="form-detail-rawatinap" style="display:none">
				<div class="form-group">
					<label class="col-md-3 control-label" for="mintarif">Minimal Tarif</label>
					<div class="col-md-7">
						<input type="text" class="form-control number" id="mintarif" placeholder="Minimal Tarif" name="mintarif" value="{mintarif}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="maxtarif">Maximal Tarif</label>
					<div class="col-md-7">
						<input type="text" class="form-control number" id="maxtarif" placeholder="Maximal Tarif" name="maxtarif" value="{maxtarif}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="persentasetarif">Persentase Tarif</label>
					<div class="col-md-7">
						<div class="input-group">
							<input type="text" class="form-control percent" id="persentasetarif" placeholder="Persentase Tarif" name="persentasetarif" value="{persentasetarif}">
							<span class="input-group-addon">%</span>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mtarif_administrasi/index/<?=$this->uri->segment(3)?>" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>

			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	var idtipe = {idtipe};

  $(document).ready(function() {
		$(".number").number(true, 0, '.', ',');
		$(".percent").number(true, 2, '.', ',');

		$("#persentasetarif").keyup(function(){
			if($(this).val() > 100){
				$(this).val(100);
			}
		});

		if(idtipe == 1){
			$("#form-detail-rawatjalan").show();
			$("#form-detail-rawatinap").hide();
		}else if(idtipe == 2){
			$("#form-detail-rawatjalan").hide();
			$("#form-detail-rawatinap").show();
		}else{
			$("#form-detail-rawatjalan").hide();
			$("#form-detail-rawatinap").hide();
		}

		$(document).on("change","#idtipe",function(){
			var idtipe = $(this).val();
			if(idtipe == 1){
				$("#form-detail-rawatjalan").show();
				$("#form-detail-rawatinap").hide();
			}else if(idtipe == 2){
				$("#form-detail-rawatjalan").hide();
				$("#form-detail-rawatinap").show();
			}else{
				$("#form-detail-rawatjalan").hide();
				$("#form-detail-rawatinap").hide();
			}
		});
	});

	function sumTotal() {
		var jasasarana 			= ($("#jasasarana").val() ? parseFloat($("#jasasarana").val().replace(/,/g, '')) : 0);
		var jasapelayanan 	= ($("#jasapelayanan").val() ? parseFloat($("#jasapelayanan").val().replace(/,/g, '')) : 0);
		var bhp 						= ($("#bhp").val() ? parseFloat($("#bhp").val().replace(/,/g, '')) : 0);
		var biayaperawatan 	= ($("#biayaperawatan").val() ? parseFloat($("#biayaperawatan").val().replace(/,/g, '')) : 0);

		var totaltarif = jasasarana + jasapelayanan + bhp + biayaperawatan;
		$("#total").val(totaltarif.toLocaleString(undefined, {
			minimumFractionDigits: 0
		}));
	}
</script>
