<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
      <li>
          <a href="{base_url}mtarif_administrasi/index/<?=$this->uri->segment(3)?>" class="btn"><i class="fa fa-reply"></i></a>
      </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mtarif_administrasi/save_setting','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama</label>
				<div class="col-md-7">
					<input type="text" class="form-control" disabled value="{nama}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="jasasarana">Jasa Sarana</label>
				<div class="col-md-7">
				  <div class="row">
					<div class="col-md-6">
							   <input type="text" class="form-control number" disabled value="{jasasarana}">
					</div>
					<div class="col-md-6">
					  <select name="group_jasasarana" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
						<option value="">Group Pembayaran</option>
						<?php foreach ($list_group_pembayaran as $row) { ?>
						  <option value="<?=$row->id?>" <?=($row->id == $group_jasasarana ? 'selected="selected"':'')?>><?=$row->nama?></option>
						<? } ?>
					  </select>
					</div>
				  </div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="jasasarana">Jasa Sarana Diskon</label>
				<div class="col-md-7">
				  <div class="row">
					<div class="col-md-6">
							   <input type="text" class="form-control number" disabled value="{jasasarana_diskon}">
					</div>
					<div class="col-md-6">
					  <select name="group_jasasarana_diskon" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
						<option value="">Group Pembayaran</option>
						<?php foreach ($list_group_pembayaran as $row) { ?>
						  <option value="<?=$row->id?>" <?=($row->id == $group_jasasarana_diskon ? 'selected="selected"':'')?>><?=$row->nama?></option>
						<? } ?>
					  </select>
					</div>
				  </div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="jasapelayanan">Jasa Pelayanan</label>
				<div class="col-md-7">
				  <div class="row">
					<div class="col-md-6">
							   <input type="text" class="form-control number" disabled value="{jasapelayanan}">
					</div>
					<div class="col-md-6">
					  <select name="group_jasapelayanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
						<option value="">Group Pembayaran</option>
						<?php foreach ($list_group_pembayaran as $row) { ?>
						  <option value="<?=$row->id?>" <?=($row->id == $group_jasapelayanan ? 'selected="selected"':'')?>><?=$row->nama?></option>
						<? } ?>
					  </select>
					</div>
				  </div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="jasasarana_diskon">Jasa Pelayanan Diskon</label>
				<div class="col-md-7">
				  <div class="row">
					<div class="col-md-6">
							   <input type="text" class="form-control number" disabled value="{jasasarana_diskon}">
					</div>
					<div class="col-md-6">
					  <select name="group_jasapelayanan_diskon" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
						<option value="">Group Pembayaran</option>
						<?php foreach ($list_group_pembayaran as $row) { ?>
						  <option value="<?=$row->id?>" <?=($row->id == $group_jasapelayanan_diskon ? 'selected="selected"':'')?>><?=$row->nama?></option>
						<? } ?>
					  </select>
					</div>
				  </div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="bhp">BHP</label>
				<div class="col-md-7">
				  <div class="row">
					<div class="col-md-6">
							   <input type="text" class="form-control number" disabled value="{bhp}">
					</div>
					<div class="col-md-6">
					  <select name="group_bhp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
						<option value="">Group Pembayaran</option>
						<?php foreach ($list_group_pembayaran as $row) { ?>
						  <option value="<?=$row->id?>" <?=($row->id == $group_bhp ? 'selected="selected"':'')?>><?=$row->nama?></option>
						<? } ?>
					  </select>
					</div>
				  </div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="bhp">BHP Diskon</label>
				<div class="col-md-7">
				  <div class="row">
					<div class="col-md-6">
							   <input type="text" class="form-control number" disabled value="{bhp_diskon}">
					</div>
					<div class="col-md-6">
					  <select name="group_bhp_diskon" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
						<option value="">Group Pembayaran</option>
						<?php foreach ($list_group_pembayaran as $row) { ?>
						  <option value="<?=$row->id?>" <?=($row->id == $group_bhp_diskon ? 'selected="selected"':'')?>><?=$row->nama?></option>
						<? } ?>
					  </select>
					</div>
				  </div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="biayaperawatan">Biaya Perawatan</label>
				<div class="col-md-7">
				  <div class="row">
					<div class="col-md-6">
							   <input type="text" class="form-control number" disabled value="{biayaperawatan}">
					</div>
					<div class="col-md-6">
					  <select name="group_biayaperawatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
						<option value="">Group Pembayaran</option>
						<?php foreach ($list_group_pembayaran as $row) { ?>
						  <option value="<?=$row->id?>" <?=($row->id == $group_biayaperawatan ? 'selected="selected"':'')?>><?=$row->nama?></option>
						<? } ?>
					  </select>
					</div>
				  </div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="biayaperawatan">Biaya Perawatan Diskon</label>
				<div class="col-md-7">
				  <div class="row">
					<div class="col-md-6">
							   <input type="text" class="form-control number" disabled value="{biayaperawatan_dikson}">
					</div>
					<div class="col-md-6">
					  <select name="group_biayaperawatan_diskon" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
						<option value="">Group Pembayaran</option>
						<?php foreach ($list_group_pembayaran as $row) { ?>
						  <option value="<?=$row->id?>" <?=($row->id == $group_biayaperawatan_diskon ? 'selected="selected"':'')?>><?=$row->nama?></option>
						<? } ?>
					  </select>
					</div>
				  </div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="total">Total Tarif</label>
				<div class="col-md-7">
					<input type="text" class="form-control number" disabled value="{total}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="total">Group Diskon Tarif</label>
				<div class="col-md-7">
					 <select name="group_diskon_all" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
						<option value="">Group Diskon Tarif</option>
						<?php foreach ($list_group_pembayaran as $row) { ?>
						  <option value="<?=$row->id?>" <?=($row->id == $group_diskon_all ? 'selected="selected"':'')?>><?=$row->nama?></option>
						<? } ?>
					  </select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mtarif_administrasi/index/<?=$this->uri->segment(3)?>" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>

			<?php echo form_hidden('id', $id); ?>
			<?php echo form_hidden('idtipe', $idtipe); ?>
			<?php echo form_close() ?>
	</div>
</div>
