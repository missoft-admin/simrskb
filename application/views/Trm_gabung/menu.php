<div class="content-grid">
	<div class="row">
		<?php if (UserAccesForm($user_acces_form,array('1193'))){ ?>
		<div class="col-xs-6 col-sm-4 col-lg-2">
			<a class="block block-link-hover2 text-center" href="<?=($this->uri->segment(1)=='trm_gabung'?'javascript:void(0)':'{site_url}trm_gabung')?>">
				<div class="block-content block-content-full <?=($this->uri->segment(1)=='trm_gabung'?'bg-primary':'')?>">
					<i class="fa fa-credit-card fa-4x text-<?=($this->uri->segment(1)=='trm_gabung'?'white':'danger')?>"></i>
					<div class="font-w600 <?=($this->uri->segment(1)=='trm_gabung'?'text-white-op':'')?> push-15-t">Gabung Rekam Medis</div>
				</div>
			</a>
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1224'))){ ?>
		<div class="col-xs-6 col-sm-4 col-lg-2">
			<a class="block block-link-hover3 text-center" href="<?=($this->uri->segment(1)=='trm_gabung_history'?'javascript:void(0)':'{site_url}trm_gabung_history')?>">
				<div class="block-content block-content-full <?=($this->uri->segment(1)=='trm_gabung_history'?'bg-primary':'')?>">
					<i class="fa fa-history fa-4x text-<?=($this->uri->segment(1)=='trm_gabung_history'?'white':'danger')?>"></i>
					<div class="font-w600 <?=($this->uri->segment(1)=='trm_gabung_history'?'text-white-op':'')?> push-15-t">History</div>
				</div>
			</a>
		</div>
		<?}?>
	</div>
</div>
