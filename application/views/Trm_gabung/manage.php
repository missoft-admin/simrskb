<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('1193'))){ ?>
<?
	$this->load->view('Trm_gabung/menu');
?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}Trefund/index" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-bottom: 0px;">
	</div>
	<div class="block-content">
		<form class="form-horizontal" action="{base_url}trm_gabung/save" method="POST">
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-danger alert-dismissable" id="alertInfo" hidden>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		        <p></p>
		      </div>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px; margin-left: 0;">
						<b><span class="label label-success" style="font-size:12px">Data Pasien Lama</span></b>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">No. Medrec</label>
						<div class="col-md-10">
							<div class="row">
								<div class="col-md-8">
									<input type="hidden" id="idpasien" name="idpasien" readonly required value="">
									<input type="text" id="nomedrec" class="form-control" name="nomedrec" readonly value="">
								</div>
								<div class="col-md-4">
									<button class="btn btn-primary" id="btnFind" type="button" style="width:100%;" data-toggle="modal" data-target="#modalFindPasien">Cari Pasien</button>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">Nama Pasien</label>
						<div class="col-md-10">
							<input type="text" id="namapasien" class="form-control" name="namapasien" readonly value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">Tanggal Lahir</label>
						<div class="col-md-10">
							<input type="text" id="tanggallahir" class="form-control" name="tanggallahir" readonly value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">Jenis Kelamin</label>
						<div class="col-md-10">
							<input type="text" id="jeniskelamin" class="form-control" name="jeniskelamin" readonly value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">Alamat</label>
						<div class="col-md-10">
							<input type="text" id="alamatpasien" class="form-control" name="alamatpasien" readonly value="">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px; margin-left: 0;">
						<b><span class="label label-success" style="font-size:12px">Data Pasien Baru</span></b>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">No. Medrec</label>
						<div class="col-md-10">
							<div class="row">
								<div class="col-md-8">
									<input type="text" id="nomedrec_new" class="form-control" name="nomedrec_new" readonly value="">
								</div>
								<div class="col-md-4">
									<button class="btn btn-primary" id="btnFind_new" type="button" style="width:100%;" data-toggle="modal" data-target="#modalFindPasien">Cari Pasien</button>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">Nama Pasien</label>
						<div class="col-md-10">
							<input type="hidden" id="idpasien_new" name="idpasien_new" readonly required value="">
							<input type="text" id="namapasien_new" class="form-control" name="namapasien_new" readonly value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">Tanggal Lahir</label>
						<div class="col-md-10">
							<input type="text" id="tanggallahir_new" class="form-control" name="tanggallahir_new" readonly value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">Jenis Kelamin</label>
						<div class="col-md-10">
							<input type="text" id="jeniskelamin_new" class="form-control" name="jeniskelamin_new" readonly value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2 control-label" style="text-align: left;">Alamat</label>
						<div class="col-md-10">
							<input type="text" id="alamatpasien_new" class="form-control" name="alamatpasien_new" readonly value="">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<hr>
					<label class="control-label" style="text-align: left; margin-bottom: 10px;">Alasan Digabung :</label>
					<textarea name="alasan" class="form-control" rows="8" cols="80" required></textarea>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-12">
					<div style="float:right;padding:0 0 14px 0;">
						<button class="btn btn-success" type="submit" name="button" style="width:100px;font-size:12px;">Proses</button>
						<a href="{base_url}Trefund/index" class="btn btn-danger" type="button" name="button" style="width:100px;font-size:12px;">Batal</a>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<?}?>
<input type="hidden" id="tipepasien" value="">

<!-- Modal Cari Pasien -->
<div class="modal in" id="modalFindPasien" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:70%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Data Pasien</h3>
				</div>
				<div class="block-content">
					<hr>
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snomedrec">No. Medrec</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snomedrec" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snamapasien">Nama Pasien</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snamapasien" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snama_keluarga">Penanggung Jawab</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snama_keluarga" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;display:none">
									<label class="col-md-4 control-label" for="sjeniskelamin">Jenis Kelamin</label>
									<div class="col-md-8">
										<label class="css-input css-radio css-radio-sm css-radio-default push-10-r">
											<input type="radio" class="sjeniskelamin" name="sjeniskelamin" value="1"><span></span> Laki-laki
										</label>
										<label class="css-input css-radio css-radio-sm css-radio-default">
											<input type="radio" class="sjeniskelamin" name="sjeniskelamin" value="2"><span></span> Perempuan
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="salamat">Alamat</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="salamat" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="stanggallahir">Tanggal Lahir</label>
									<div class="col-md-8">
										<input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="stanggallahir" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snotelepon">Telepon</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snotelepon" value="">
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<table width="100%" class="table table-bordered table-striped" id="datatable-pasien">
						<thead>
							<tr>
								<th>No. Medrec</th>
								<th>Nama</th>
								<th>Jenis Kelamin</th>
								<th>Tanggal Lahir</th>
								<th>Alamat</th>
								<th>Telepon</th>
								<th>Keluarga</th>
								<th>Tlp. Keluarga</th>
							</tr>
						</thead>
						<tbody id="pasienmodaltabel">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Modal Cari Pasien -->

<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function() {

		let delay = (function() {
			let timer = 0;
			return function(callback, ms) {
				clearTimeout(timer);
				timer = setTimeout(callback, ms);
			};
		})();

		let idpasien = '<?=$idpasien?>';
		if (idpasien != '') {
			getDataPasien(idpasien, 'pasienlama');
		}

		$("#btnFind").click(function() {
			$("#tipepasien").val('pasienlama');
		});

		$("#btnFind_new").click(function() {
			$("#tipepasien").val('pasienbaru');
		});

		$(document).on("click", ".selectPasien", function() {
			let idpasien = $(this).data('idpasien');
			let tipepasien = $("#tipepasien").val();
			getDataPasien(idpasien, tipepasien);
		});

		// Apply the search
		$("#snomedrec").keyup(function() {
			let snomedrec = $(this).val();
			let snamapasien = $("#snamapasien").val();
			let snama_keluarga = $("#snama_keluarga").val();
			let salamat = $("#salamat").val();
			let stanggallahir = $("#stanggallahir").val();
			let snotelepon = $("#snotelepon").val();
			if ($(this).val() != ''){
				delay(function() {
					$('#datatable-pasien').DataTable().destroy();
					loadDataPasien(snomedrec, snamapasien, snama_keluarga, salamat, stanggallahir, snotelepon);
				}, 1000);
			}
		});

		$("#snamapasien").keyup(function() {
			let snomedrec = $("#snomedrec").val();
			let snamapasien = $(this).val();
			let snama_keluarga = $("#snama_keluarga").val();
			let salamat = $("#salamat").val();
			let stanggallahir = $("#stanggallahir").val();
			let snotelepon = $("#snotelepon").val();
			if ($(this).val() != ''){
			delay(function() {
				$('#datatable-pasien').DataTable().destroy();
				loadDataPasien(snomedrec, snamapasien, snama_keluarga, salamat, stanggallahir, snotelepon);
			}, 1000);
			}
		});

		$("#snama_keluarga").keyup(function() {
			let snomedrec = $("#snomedrec").val();
			let snamapasien = $("#snamapasien").val();
			let snama_keluarga = $(this).val();
			let salamat = $("#salamat").val();
			let stanggallahir = $("#stanggallahir").val();
			let snotelepon = $("#snotelepon").val();
			if ($(this).val() != ''){
			delay(function() {
				$('#datatable-pasien').DataTable().destroy();
				loadDataPasien(snomedrec, snamapasien, snama_keluarga, salamat, stanggallahir, snotelepon);
			}, 1000);
			}
		});

		$("#salamat").keyup(function() {
			let snomedrec = $("#snomedrec").val();
			let snamapasien = $("#snamapasien").val();
			let snama_keluarga = $("#snama_keluarga").val();
			let salamat = $(this).val();
			let stanggallahir = $("#stanggallahir").val();
			let snotelepon = $("#snotelepon").val();
			if ($(this).val() != ''){
			delay(function() {
				$('#datatable-pasien').DataTable().destroy();
				loadDataPasien(snomedrec, snamapasien, snama_keluarga, salamat, stanggallahir, snotelepon);
			}, 1000);
			}
		});

		$("#stanggallahir").change(function() {
			let snomedrec = $("#snomedrec").val();
			let snamapasien = $("#snamapasien").val();
			let snama_keluarga = $("#snama_keluarga").val();
			let salamat = $("#salamat").val();
			let stanggallahir = $(this).val();
			let snotelepon = $("#snotelepon").val();
			if ($(this).val() != ''){
			delay(function() {
				$('#datatable-pasien').DataTable().destroy();
				loadDataPasien(snomedrec, snamapasien, snama_keluarga, salamat, stanggallahir, snotelepon);
			}, 1000);
			}
		});

		$("#snotelepon").keyup(function() {
			let snomedrec = $("#snomedrec").val();
			let snamapasien = $("#snamapasien").val();
			let snama_keluarga = $("#snama_keluarga").val();
			let salamat = $("#salamat").val();
			let stanggallahir = $("#stanggallahir").val();
			let snotelepon = $(this).val();
			if ($(this).val() != ''){
			delay(function() {
				$('#datatable-pasien').DataTable().destroy();
				loadDataPasien(snomedrec, snamapasien, snama_keluarga, salamat, stanggallahir, snotelepon);
			}, 1000);
			}
		});
	});

	function loadDataPasien(snomedrec, snamapasien, snama_keluarga, salamat, stanggallahir, snotelepon) {
		let tablePasien = $('#datatable-pasien').DataTable({
			"pageLength": 8,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}trm_gabung/getPasien',
				type: "POST",
				dataType: 'json',
				data: {
					snomedrec: snomedrec,
					snamapasien: snamapasien,
					snama_keluarga: snama_keluarga,
					salamat: salamat,
					stanggallahir: stanggallahir,
					snotelepon: snotelepon,
				}
			},
			"columnDefs": [{
				"targets": [0],
				"orderable": true
			}]
		});
	}

	function getDataPasien(idpasien, tipepasien) {
		$.ajax({
			url: "{site_url}tpoliklinik_pendaftaran/getDataPasien/" + idpasien,
			method: "GET",
			dataType: "json",
			success: function(data) {
				if (tipepasien == 'pasienlama') {
					$('#idpasien').val(idpasien);
					$('#nomedrec').val(data[0].no_medrec);
					$('#namapasien').val(data[0].nama);
					$('#tanggallahir').val(data[0].tanggal_lahir);
					$('#alamatpasien').val(data[0].alamat_jalan);

					if (data[0].jenis_kelamin == '1') {
						$('#jeniskelamin').val('Laki-laki');
					} else {
						$('#jeniskelamin').val('Perempuan');
					}
				} else {
					$('#idpasien_new').val(idpasien);
					$('#nomedrec_new').val(data[0].no_medrec);
					$('#namapasien_new').val(data[0].nama);
					$('#tanggallahir_new').val(data[0].tanggal_lahir);
					$('#alamatpasien_new').val(data[0].alamat_jalan);

					if (data[0].jenis_kelamin == '1') {
						$('#jeniskelamin_new').val('Laki-laki');
					} else {
						$('#jeniskelamin_new').val('Perempuan');
					}

					checkMatchPasien();
				}
			}
		});
	}

	function checkMatchPasien() {
		let namapasien = $('#namapasien').val();
		let tanggallahir = $('#tanggallahir').val();
		let jeniskelamin = $('#jeniskelamin').val();

		let namapasien_new = $('#namapasien_new').val();
		let tanggallahir_new = $('#tanggallahir_new').val();
		let jeniskelamin_new = $('#jeniskelamin_new').val();

		if (namapasien != namapasien_new || tanggallahir != tanggallahir_new || jeniskelamin != jeniskelamin_new) {
			$('#alertInfo').show();
			$('#alertInfo p').html('Data Pasien Berbeda, Apakah Anda Yakin Akan Menggabungkan?');
		} else {
			$('#alertInfo').hide();
			$('#alertInfo p').html('');
		}
	}
</script>
