<!-- Modal Data Permintaan -->
<div class="modal fade" id="modal-data-permintaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Data Permintaan</h3>
				</div>
                <div class="block-content">
                    <div id="action-data-permintaan"></div>
                    <br>
                    <table class="table table-striped table-striped table-hover table-bordered" id="table-data-permintaan">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Nama Pemeriksaan</th>
                                <th class="text-center">Harga</th>
                                <th class="text-center">Kuantitas</th>
                                <th class="text-center">Diskon</th>
                                <th class="text-center">Total Keseluruhan</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                            <tr>
                            <td class="text-right" colspan="5">Total</td>
                            <td class="text-right"></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>