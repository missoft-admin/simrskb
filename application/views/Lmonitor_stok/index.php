<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('1369'))){ ?>
<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<hr style="margin-top:0px">
				<?php echo form_open('lgudang_stok/print_barang','class="form-horizontal" id="form-work" target="_blank"') ?>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="idunitpelayanan">Unit Pelayanan</label>
					<div class="col-md-8">
						<select name="idunitpelayanan" id="idunitpelayanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
							<?php foreach  ($list_unitpelayanan as $row) { ?>
								<option value="<?=$row->id?>" <?=($idunitpelayanan == $row->id) ? "selected" : "" ?>><?=$row->nama?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				
			</div>
			<div class="col-md-6">					
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="statusstok"></label>
						<div class="col-md-4">
							<button class="btn btn-success text-uppercase" type="button" id="btn_filter" name="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						</div>
					</div>
					
			</div>
		</div>
			<?php echo form_close() ?>
		<hr>

		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive" id="div_tabel"></div>
	</div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
$(document).ready(function(){
        // loadBarang();
    })
	$("#idunitpelayanan").change(function(){
		// alert($(this).val());
		// if ($(this).val()!='#'){
			// // load_tipe();
		// }
		
	});
	function clear_table(){
		$tabel='<table class="table" id="index_list">';
		$tabel +='<thead></thead><tbody></tbody></table>';
		$("#div_tabel").html($tabel);
	}
	$(document).on("click", "#btn_filter", function() {
		// // alert($("#idunitpelayanan").val());
		// if ($("#idunitpelayanan").val()=='#'){
			// sweetAlert("Maaf...", "Silahkan pilih Unit Pelayanan Untuk mempercepat pencarian", "error");
			// return false;
		// }else{
			loadBarang();
		// }
	});
	function loadBarang() {
		var unit=$("#idunitpelayanan").val();
		var data = [];
		data=$('#idunitpelayanan').select2('data');
		clear_table();
		// alert(unit);
		if (unit){
			
	
		var content='';
		content +='<tr>';
		content +='<th>NO</th>';
		content +='<th>KODE</th>';
		content +='<th>BARANG</th>';
		content +='<th>TIPE</th>';
		content +='<th>SATUAN</th>';
		$('#index_list thead').empty();
		for (i = 0; i < data.length; i++) {
		  console.log(data[i].text+' '+data[i].id)
		  content +='<th>'+data[i].text+'</th>';
		}
		content +='</tr>';
		$('#index_list thead').append(content);
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}lmonitor_stok/getBarang/',
			type: "POST",
			dataType: 'json',
			data: {
				idunit: unit,
			}
		},
		columnDefs: [
			
		]
		});
	}
		// alert(data.length);
		// alert($("#order_by").val());
		// var order_by=$("#order_by").val();
		// var idunit=$("#idunitpelayanan").val();
		// var idtipe=$("#idtipe").val();
		// var idkategori=$("#idkategori").val();
		// var statusstok=$("#statusstok").val();
		// $('#index_list').DataTable().destroy();
		
	}
	// $(document).on("click", "#btn_print", function() {
		
			// print_barang();
	// });
	// function print_barang() {
		// $.ajax({        
           // url: '{site_url}lgudang_stok/print_barang/',
            // type: "POST",
            // data: {
				// idunit: idunit,
				// idtipe: idtipe,
				// idkategori: idkategori,
				// statusstok: statusstok,
				// order_by: order_by,
			// }
                   
    // }); 
	// }
	function clear_data(){
		
		$('#index_list').DataTable().destroy();
		$('#index_list tbody').empty();
		var table = $('#index_list').DataTable( {
			"searching": true,
			columnDefs: []
		} );
	}
	function load_tipe(){
		clear_data();
		var idunit=$("#idunitpelayanan").val();
		$('#idtipe').find('option').remove();
		
		$('#idkategori').find('option').remove();
		$('#idkategori').append('<option value="#" selected>- Pilih Semua -</option>');
		var def='';
		if (idunit !='#'){
				$('#idtipe').append('<option value="#" selected>- Pilih Semua -</option>');
						
				$.ajax({
					url: '{site_url}tstockopname/list_tipe/'+idunit,
					dataType: "json",
					success: function(data) {
						$.each(data, function (i,row) {
						$('#idtipe')
						.append('<option value="' + row.id + '">' + row.text + '</option>');
						});
					}
				});

		}
	}
	$("#idtipe").change(function(){
		load_kategori();
	});
	function load_kategori(){
		var idtipe=$("#idtipe").val();
		$('#idkategori').find('option').remove();
		$('#idkategori').append('<option value="#" selected>- Pilih Semua -</option>');
		if (idtipe !='#'){
						
				$.ajax({
					url: '{site_url}tstockopname/list_kategori/'+idtipe,
					dataType: "json",
					success: function(data) {
						$.each(data, function (i,row) {
							
						$('#idkategori')
						.append('<option value="' + row.path + '">' + TreeView(row.level, row.nama) + '</option>');
						});
					}
				});

		}
	}
	function TreeView($level, $name)
	{
		// console.log($name);
		$indent = '';
		for ($i = 0; $i < $level; $i++) {
			if ($i > 0) {
				$indent += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			} else {
				$indent += '';
			}
		}

		if ($i > 0) {
			$indent += '└─';
		} else {
			$indent += '';
		}

		return $indent + ' ' +$name;
	}
</script>
