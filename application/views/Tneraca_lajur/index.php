<?= ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<style>
.tableFix { /* Scrollable parent element */
  position: relative;
  overflow: auto;
  height: 500px;
}

.tableFix table{
  width: 100%;
  border-collapse: collapse;
}

.tableFix th,
.tableFix td{
  padding: 8px;
  text-align: left;
}

.tableFix thead th {
  position: sticky;  /* Edge, Chrome, FF */
  top: 0px;
  background: #fff;  /* Some background is needed */
}
</style>
<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        
        <h3 class="block-title">{title}</h3>
    </div>
	<div class="block-content">
		  <?php echo form_open('Tneraca_lajur/export','class="form-horizontal" id="form-work" target="_blank"') ?>
		<input type="hidden" class="form-control" disabled id="idakun" name="idakun" value="{idakun}" />
		
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">Tanggal</label>
				<div class="col-md-5">
					<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
						<input class="form-control" type="text" id="tanggal_trx1" name="tanggal_trx1" placeholder="From" value="<?=($tanggal_trx1==''?HumanDateShort($tanggal_awal_akuntansi):'')?>"/>
						<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
						<input class="form-control" type="text" id="tanggal_trx2" name="tanggal_trx2" placeholder="To" value="{tanggal_trx2}"/>
					</div>
				</div>				
			</div>
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">Kategori Akun</label>
				<div class="col-md-11">
					<select name="idkategori_arr[]" id="idkategori_arr" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Kategori" multiple>
						<?php foreach  (get_all('mkategori_akun') as $row) { ?>
							<option value="'<?=$row->id?>'" ><?=$row->nama?></option>
						<?php } ?>
					</select>
				</div>
				
			</div>
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">Posisi Saldo</label>
				<div class="col-md-2">
					<select name="possaldo[]" id="possaldo" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Posisi Saldo" multiple>
						
							<option value="'DB'" >DEBIT</option>
							<option value="'CR'" >KREDIT</option>
						
					</select>
				</div>
				<label class="col-md-1" for="">Posisi Laporan</label>
				<div class="col-md-2">
					<select name="poslaporan[]" id="poslaporan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Posisi Laporan" multiple>
						
							<option value="'NRC'" >NERACA</option>
							<option value="'LR'" >LABA RUGI</option>
						
					</select>
				</div>
				
			</div>
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">Header Akun</label>
				<div class="col-md-11">
					<select name="header_akun[]" id="header_akun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Akun" multiple>
						<?php foreach  (get_all('makun_nomor',array('level'=>0)) as $row) { ?>
							<option value="'<?=$row->noakun?>'"> <?=$row->noakun.' - '.$row->namaakun?></option>
						<?php } ?>
					</select>
				</div>
				
			</div>
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">No Akun</label>
				<div class="col-md-11">
					<select name="idakun_arr[]" id="idakun_arr" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Akun" multiple>
						<?php foreach  ($list_akun as $row) { ?>
							<option value="'<?=$row->id?>'" <?=($row->id==$idakun?'selected':'')?>><?=$row->noakun.' - '.$row->namaakun?></option>
						<?php } ?>
					</select>
				</div>
				
			</div>
			
			<?if ($idakun){?>
			
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-1" for="">No. Bukti</label>
				<div class="col-md-5">
					<input type="text" class="form-control" <?=$disabel?> placeholder="No. Bukti" id="notransaksi" value="{notransaksi}" />
				</div>
				<label class="col-md-1" for="">No. Validasi</label>
				<div class="col-md-5">
					<input type="text" class="form-control" <?=$disabel?> placeholder="No. Validasi" id="nojurnal" name="nojurnal" value="{nojurnal}" />
				</div>
			</div>
			
			<div class="form-group" style="margin-bottom: 15px;margin-top: 5px;">
				<label class="col-md-1" for="">Jenis Jurnal</label>
				<div class="col-md-11">
					<select name="ref_validasi[]" id="ref_validasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Jenis Jurnal" multiple>
						<?php foreach  ($list_ref as $row) { ?>
							<option value="'<?=$row->ref_validasi?>'"><?=$row->nama?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<?}?>
			<div class="form-group" style="margin-top: 15px;">
				<label class="col-md-1" for=""></label>
				<div class="col-md-11">
					<button class="btn btn-info" type="button" id="btn_filter"><i class="fa fa-filter"></i> Filter</button>
					<button class="btn btn-success" type="submit" name="btn_export" value="1"><i class="fa fa-file-excel-o"></i> Export Excel</button>
					<?if ($idakun){?>
					<button disabled class="btn btn-danger" type="submit" name="btn_export" value="2"><i class="fa fa-file-pdf-o"></i> Export PDF</button>
					<button disabled class="btn btn-primary" type="submit" name="btn_export" value="3"><i class="fa fa-print"></i> Print</button>
					<?}?>
				</div>
			</div>
			
			<?php echo form_close(); ?>
			
    </div>
</div>
<div class="block">
    <div class="block-header">        
        <h3 class="block-title">Detail Transaksi</h3>
    </div>
	<div class="block-content">
		 <div class="form-horizontal">		
			
				<div class="table-responsive">
				<div class="tableFix2">
				<table class="table table-bordered table-striped table-header-bg" id="index_list" style="width: 100%;">
					<thead>
						<tr>
							<th width="5%" rowspan="2" class="text-center">NO. AKUN</th>
							<th width="15%" rowspan="2" class="text-center">NAMA AKUN</th>
							<th width="5%" rowspan="2" class="text-center">POS SALDO</th>
							<th width="16%" colspan="2" class="text-center">NERACA SALDO</th>
							<th width="5%" rowspan="2" class="text-center">POS LAP</th>
							<th width="16%" colspan="2" class="text-center">LABA RUGI</th>
							<th width="16%" colspan="2" class="text-center">NERACA</th>
							<th width="16%" colspan="2" class="text-center">SALDO AWAL</th>
						</tr>
						<tr>
							<th  class="text-center">DEBIT</th>
							<th>KREDIT</th>
							<th>DEBIT</th>
							<th>KREDIT</th>
							<th>DEBIT</th>
							<th>KREDIT</th>
							<th>DEBIT</th>
							<th>KREDIT</th>							
						</tr>
					</thead>
					
					<tbody></tbody>
					<tfoot>
						<tr class="info">
							<td colspan="3" class="text-center"><strong>JUMLAH</strong></td>
							<td><label id="ns_tot_debet"></label></td>
							<td><label id="ns_tot_kredit"></label></td>
							<td></td>
							<td><label id="lr_tot_debet"></label></td>
							<td><label id="lr_tot_kredit"></label></td>
							<td><label id="nr_tot_debet"></label></td>
							<td><label id="nr_tot_kredit"></label></td>
							<td><label id="sa_tot_debet"></label></td>
							<td><label id="sa_tot_kredit"></label></td>
							
						</tr>
						<tr class="active">
							<td colspan="3" class="text-center"><strong>JUMLAH (RUGI) BERSIH</strong></td>
							<td></td>
							<td></td>
							<td></td>
							<td class="text-right"><label id="bersih_lr"></label></td>
							<td></td>
							<td></td>
							<td class="text-right"><label id="bersih_nr"></label></td>
							<td></td>
							<td></td>
						</tr>
						<tr  class="success">
							<td colspan="3" class="text-center"><strong>JUMLAH SETELAH LABA RUGI</strong></td>
							<td></td>
							<td></td>
							<td></td>
							<td class="text-right"><label id="bersih_lr_debet"></label></td>
							<td class="text-right"><label id="bersih_lr_kredit"></label></td>
							<td class="text-right"><label id="bersih_nr_debet"></label></td>
							<td class="text-right"><label id="bersih_nr_kredit"></label></td>
							<td></td>
							<td></td>
						</tr>
					</tfoot>
				</table>
				</div>
				</div>
		</div>
    </div>
</div>
<link rel="stylesheet" href="{js_path}plugins/datatables/fixedHeader.dataTables.min.css">
<script src="{js_path}pages/base_index_datatable.js"></script>

<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var idakun;
var len_arr;
	$(document).ready(function(){		
		
    })
	
	
	$(document).on("click","#btn_filter",function(){	
		
		load_neraca();
	});
	function load_neraca(){
		$("#cover-spin").show();
		var idkategori=$("#idkategori_arr").val();
		var possaldo=$("#possaldo").val();
		var poslaporan=$("#poslaporan").val();
		var header_akun=$("#header_akun").val();
		var tanggal_trx1=$("#tanggal_trx1").val();
		var tanggal_trx2=$("#tanggal_trx2").val();
		var idakun_arr=$("#idakun_arr").val();
		$('#index_list').DataTable().destroy();
		$('#index_list tbody').empty();
		$.ajax({
			url: '{site_url}Tneraca_lajur/load_neraca/',
			dataType: "json",
			type: "POST",
			data: {
					tanggal_trx1: tanggal_trx1,
					tanggal_trx2: tanggal_trx2,
					idkategori: idkategori,
					possaldo: possaldo,
					poslaporan: poslaporan,
					header_akun: header_akun,
					idakun_arr: idakun_arr,
			},
			success: function(data) {
				$('#index_list tbody').append(data.tabel);
				$("#ns_tot_debet").text(data.ns_tot_debet);
				$("#ns_tot_kredit").text(data.ns_tot_kredit);
				$("#lr_tot_debet").text(data.lr_tot_debet);
				$("#lr_tot_kredit").text(data.lr_tot_kredit);
				
				$("#nr_tot_debet").text(data.nr_tot_debet);
				$("#nr_tot_kredit").text(data.nr_tot_kredit);
				$("#sa_tot_debet").text(data.sa_tot_debet);
				$("#sa_tot_kredit").text(data.sa_tot_kredit);
				$("#bersih_lr").text(data.bersih_lr);
				$("#bersih_nr").text(data.bersih_nr);
				
				$("#bersih_nr_debet").text(data.bersih_nr_debet);
				$("#bersih_nr_kredit").text(data.bersih_nr_kredit);
				$("#bersih_lr_debet").text(data.bersih_lr_debet);
				$("#bersih_lr_kredit").text(data.bersih_lr_kredit);
				
				
				$('#index_list').DataTable({
				scrollY: 400,
				scrollCollapse: true,
				paging: false,
				searching: true,
				ordering: false,
				ordering: false,
				info: false,
				columnDefs: [
							 // {"targets": [0], "visible": false },
							 {  className: "text-right", targets:[0,3,4,6,7,8,9,10,11] },
							 {  className: "text-center", targets:[2,5] },
							 { "width": "3%", "targets": [2,5] },
							 { "width": "9%", "targets": [3,4,6,7,8,9,10,11] },
							 // { "width": "8%", "targets": [10] },
							 // { "width": "15%", "targets": [4] }
							 // { "width": "15%", "targets": [2,13] }

							]
			});
				$("#cover-spin").hide();
				
			}
		});
		
	}
	
	
</script>