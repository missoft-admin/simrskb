<?php  $generator = new Picqer\Barcode\BarcodeGeneratorPNG(); ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Hasil Pemeriksaan Laboratorium</title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
    }
    
    pre {
      font-family: 'Times New Roman', Times, serif;
    }

    @media print {
      * {
        font-size: 13px !important;
      }
      table {
        font-size: 13px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      * {
        font-size: 13px !important;
      }
      table {
        font-size: 13px !important;
        border-collapse: collapse !important;
        width: 100% !important;
        page-break-inside: avoid !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }
    </style>
  </head>
  <body>
    <?php $colspan = $pengaturan_printout['tampilkan_nama_pemeriksaan'] + $pengaturan_printout['tampilkan_hasil'] + $pengaturan_printout['tampilkan_satuan'] + $pengaturan_printout['tampilkan_nilai_normal'] + $pengaturan_printout['tampilkan_nilai_kritis'] + $pengaturan_printout['tampilkan_metode'] + $pengaturan_printout['tampilkan_sumber_spesimen'] + $pengaturan_printout['tampilkan_keterangan']; ?>

    <table>
      <!-- header -->
      <tr>
        <td style="text-align: center;" colspan="<?= $colspan; ?>">
          <img src="assets/upload/pengaturan_printout_laboratorium_hasil/<?= strip_tags($pengaturan_printout['logo']); ?>" width="100px"><br>
          &nbsp;<?= strip_tags($pengaturan_printout['alamat']); ?><br>
          &nbsp;<?= strip_tags($pengaturan_printout['phone']); ?><br>
          &nbsp;<?= strip_tags($pengaturan_printout['website']); ?>
        </td>
      </tr>
      
      <!-- separator -->
      <!-- <tr>
        <td colspan="<= $colspan; ?>">&nbsp;</td>
      </tr> -->

      <!-- title -->
      <tr>
        <td style="text-align: center;" colspan="<?= $colspan; ?>">
          <b><?= strip_tags($pengaturan_printout['label_header']); ?></b>
          <br>
          <i><?= strip_tags($pengaturan_printout['label_header_eng']); ?></i>
        </td>
      </tr>

      <!-- separator -->
      <tr>
        <td colspan="<?= $colspan; ?>">&nbsp;</td>
      </tr>
      
      <!-- meta data -->
      <tr>
        <td colspan="<?= $colspan; ?>">
          <table>
            <tr>
              <td style="width: 150px; margin: 3px;">
                <?= strip_tags($pengaturan_printout['label_noregister']); ?><br>
                <i><?= strip_tags($pengaturan_printout['label_noregister_eng']); ?></i>
              </td>
              <td style="width: 20px; margin: 3px;" class="text-center">:</td>
              <td style="margin: 3px;"><?=$nomor_register;?></td>

              <td style="width: 150px; margin: 3px;">
                <?= strip_tags($pengaturan_printout['label_nomedrec']); ?><br>
                <i><?= strip_tags($pengaturan_printout['label_nomedrec_eng']); ?></i>
              </td>
              <td style="width: 20px; margin: 3px;" class="text-center">:</td>
              <td style="margin: 3px;"><?=$nomor_medrec;?></td>
            </tr>
            <tr>
              <td style="width: 150px; margin: 3px;">
                <?= strip_tags($pengaturan_printout['label_nama']); ?><br>
                <i><?= strip_tags($pengaturan_printout['label_nama_eng']); ?></i>
              </td>
              <td style="width: 20px; margin: 3px;" class="text-center">:</td>
              <td style="margin: 3px;"><?=$nama_pasien;?></td>

              <td style="width: 150px; margin: 3px;">
                <?= strip_tags($pengaturan_printout['label_kelompok_pasien']); ?><br>
                <i><?= strip_tags($pengaturan_printout['label_kelompok_pasien_eng']); ?></i>
              </td>
              <td style="width: 20px; margin: 3px;" class="text-center">:</td>
              <td style="margin: 3px;"><?=$kelompok_pasien;?></td>
            </tr>
            <tr>
              <td style="width: 150px; margin: 3px;">
                <?= strip_tags($pengaturan_printout['label_alamat']); ?><br>
                <i><?= strip_tags($pengaturan_printout['label_alamat_eng']); ?></i>
              </td>
              <td style="width: 20px; margin: 3px;" class="text-center">:</td>
              <td style="margin: 3px;"><?=$alamat_pasien;?></td>

              <td style="width: 150px; margin: 3px;">
                <?= strip_tags($pengaturan_printout['label_nama_asuransi']); ?><br>
                <i><?= strip_tags($pengaturan_printout['label_nama_asuransi_eng']); ?></i>
              </td>
              <td style="width: 20px; margin: 3px;" class="text-center">:</td>
              <td style="margin: 3px;"><?=($rekanan_asuransi ? $rekanan_asuransi : '-');?></td>
            </tr>
            <tr>
              <td style="width: 150px; margin: 3px;">
                <?= strip_tags($pengaturan_printout['label_dokter_perujuk']); ?><br>
                <i><?= strip_tags($pengaturan_printout['label_dokter_perujuk_eng']); ?></i>
              </td>
              <td style="width: 20px; margin: 3px;" class="text-center">:</td>
              <td style="margin: 3px;"><?=$dokter_perujuk;?></td>

              <td style="width: 150px; margin: 3px;">
                <?= strip_tags($pengaturan_printout['label_tanggal_lahir']); ?><br>
                <i><?= strip_tags($pengaturan_printout['label_tanggal_lahir_eng']); ?></i>
              </td>
              <td style="width: 20px; margin: 3px;" class="text-center">:</td>
              <td style="margin: 3px;"><?=date("Y-m-d", strtotime($tanggal_lahir));?></td>
            </tr>
            <tr>
              <td style="width: 150px; margin: 3px;">
                <?= strip_tags($pengaturan_printout['label_nomor_laboratorium']); ?><br>
                <i><?= strip_tags($pengaturan_printout['label_nomor_laboratorium_eng']); ?></i>
              </td>
              <td style="width: 20px; margin: 3px;" class="text-center">:</td>
              <td style="margin: 3px;"><?= str_pad($nomor_laboratorium, 4, "0", STR_PAD_LEFT);?></td>

              <td style="width: 150px; margin: 3px;">
                <?= strip_tags($pengaturan_printout['label_umur']); ?><br>
                <i><?= strip_tags($pengaturan_printout['label_umur_eng']); ?></i>
              </td>
              <td style="width: 20px; margin: 3px;" class="text-center">:</td>
              <td style="margin: 3px;"><?=$umur_tahun;?> Tahun <?=$umur_bulan;?> Bulan <?=$umur_hari;?> Hari</td>
            </tr>
            <tr>
              <td colspan="3" style="margin: 3px;">
                <img width="300px" src="data:image/png;base64, <?= base64_encode($generator->getBarcode(str_pad($nomor_laboratorium, 4, "0", STR_PAD_LEFT), $generator::TYPE_EAN_13)); ?>">
              </td>

              <td style="width: 150px; margin: 3px;">
                <?= strip_tags($pengaturan_printout['label_rujukan']); ?><br>
                <i><?= strip_tags($pengaturan_printout['label_rujukan_eng']); ?></i>
              </td>
              <td style="width: 20px; margin: 3px;" class="text-center">:</td>
              <td style="margin: 3px;"><?=GetAsalRujukan($asal_rujukan);?></td>
            </tr>
          </table>
        </td>
      </tr>

      <!-- separator -->
      <tr>
        <td colspan="<?= $colspan; ?>">&nbsp;</td>
      </tr>

      <!-- pemeriksaan -->
      <tr>
        <?php if ($pengaturan_printout['tampilkan_nama_pemeriksaan']) { ?>
        <td class="text-center text-bold border-full" style="width:20%">
          <?= strip_tags($pengaturan_printout['label_nama_pemeriksaan']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_nama_pemeriksaan_eng']); ?></i>
        </td>
        <?php } ?>
        <?php if ($pengaturan_printout['tampilkan_hasil']) { ?>
        <td class="text-center text-bold border-full" style="width:10%">
          <?= strip_tags($pengaturan_printout['label_hasil']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_hasil_eng']); ?></i>
        </td>
        <?php } ?>
        <?php if ($pengaturan_printout['tampilkan_satuan']) { ?>
        <td class="text-center text-bold border-full" style="width:10%">
          <?= strip_tags($pengaturan_printout['label_satuan']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_satuan_eng']); ?></i>
        </td>
        <?php } ?>
        <?php if ($pengaturan_printout['tampilkan_nilai_normal']) { ?>
        <td class="text-center text-bold border-full" style="width:10%">
          <?= strip_tags($pengaturan_printout['label_nilai_normal']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_nilai_normal_eng']); ?></i>
        </td>
        <?php } ?>
        <?php if ($pengaturan_printout['tampilkan_nilai_kritis']) { ?>
        <td class="text-center text-bold border-full" style="width:10%">
          <?php echo strip_tags($pengaturan_printout['label_nilai_kritis']); ?><br>
          <i><?php echo strip_tags($pengaturan_printout['label_nilai_kritis_eng']); ?></i>
        </td>
        <?php } ?>
        <?php if ($pengaturan_printout['tampilkan_metode']) { ?>
        <td class="text-center text-bold border-full" style="width:10%">
          <?= strip_tags($pengaturan_printout['label_metode']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_metode_eng']); ?></i>
        </td>
        <?php } ?>
        <?php if ($pengaturan_printout['tampilkan_sumber_spesimen']) { ?>
        <td class="text-center text-bold border-full" style="width:10%">
          <?php echo strip_tags($pengaturan_printout['label_sumber_spesimen']); ?><br>
          <i><?php echo strip_tags($pengaturan_printout['label_sumber_spesimen_eng']); ?></i>
        </td>
        <?php } ?>
        <?php if ($pengaturan_printout['tampilkan_keterangan']) { ?>
        <td class="text-center text-bold border-full" style="width:10%">
          <?php echo strip_tags($pengaturan_printout['label_keterangan']); ?><br>
          <i><?php echo strip_tags($pengaturan_printout['label_keterangan_eng']); ?></i>
        </td>
        <?php } ?>
      </tr>
      <?php foreach ($daftar_pemeriksaan as $row){ ?>
        <?php if($row->idkelompok == 1){ ?>
          <tr>
            <td class="border-full text-bold" colspan="<?= $colspan; ?>" style="background-color: #e8f2f3;"><?=TreeView(0, strtoupper($row->nama))?></td>
          </tr>
        <?php }else{ ?>
          <?php
            $tarif_laboratorium = $this->model->get_tarif_laboratorium($row->idlaboratorium);
            $satuan = $tarif_laboratorium->singkatan_satuan ?? '0';
            $nilai_normal = $tarif_laboratorium->nilai_normal ?? '';
            $nilai_kritis = $tarif_laboratorium->nilai_kritis ?? '';
            $metode = $tarif_laboratorium->metode ?? '';
            $sumber_spesimen = $tarif_laboratorium->sumber_spesimen ?? '';
          ?>
          <tr>
            <?php if ($pengaturan_printout['tampilkan_nama_pemeriksaan']) { ?>
              <td class="border-full"><?php echo TreeView(1, strtoupper($row->nama)); ?></td>
            <?php } ?>
            <?php if ($pengaturan_printout['tampilkan_hasil']) { ?>
              <td class="text-center border-full">
                <?= htmlspecialchars($row->hasil); ?>
                <?= ($pengaturan_printout['tampilkan_flag'] && $row->flag ? '<span style="color: ' . $pengaturan_form['warna_flag'] . '">' . $row->flag. '</span>' : ''); ?>
                <?= ($pengaturan_printout['tampilkan_flag_tidak_normal'] ? '<span style="color: ' . $pengaturan_form['warna_tidak_normal'] . '">' . ($row->flag_normal == 2 ? '*' : ''). '</span>' : ''); ?>
                <?= ($pengaturan_printout['tampilkan_flag_kritis'] ? '<span style="color: ' . $pengaturan_form['warna_nilai_kritis'] . '">' . ($row->flag_kritis == 2 ? '!' : ''). '</span>' : ''); ?>
              </td>
            <?php } ?>
            <?php if ($pengaturan_printout['tampilkan_satuan']) { ?>
              <td class="text-center border-full"><?= $row->satuan ?? $satuan; ?></td>
            <?php } ?>
            <?php if ($pengaturan_printout['tampilkan_nilai_normal']) { ?>
              <td class="text-center border-full"><pre><?= $row->nilainormal ?? $nilai_normal, '<br>'; ?></pre></td>
            <?php } ?>
            <?php if ($pengaturan_printout['tampilkan_nilai_kritis']) { ?>
              <td class="text-center border-full"><pre><?= $row->nilaikritis ?? $nilai_kritis; ?></pre></td>
            <?php } ?>
            <?php if ($pengaturan_printout['tampilkan_metode']) { ?>
              <td class="text-center border-full"><?= $row->metode ?? $metode; ?></td>
            <?php } ?>
            <?php if ($pengaturan_printout['tampilkan_sumber_spesimen']) { ?>
              <td class="text-center border-full"><?php $row->sumber_spesimen ?? $sumber_spesimen; ?></td>
            <?php } ?>
            <?php if ($pengaturan_printout['tampilkan_keterangan']) { ?>
              <td class="text-center border-full"><?= $row->keterangan; ?></td>
            <?php } ?>
          </tr>
        <?php } ?>
      <?php } ?>

      <!-- separator -->
      <tr>
        <td colspan="<?= $colspan; ?>">&nbsp;</td>
      </tr>
      
      <!-- footer -->
      <tr>
        <td colspan="<?= $colspan; ?>">
          <table>
            <tr>
              <td>
                <table>
                  <tr>
                    <td width="150px">
                      <?= strip_tags($pengaturan_printout['label_catatan']); ?><br>
                      <i><?= strip_tags($pengaturan_printout['label_catatan_eng']); ?></i>
                    </td>
                    <td width="10px">:</td>
                    <td><?=$catatan?></td>
                  </tr>
                  <tr>
                    <td width="150px">
                      <?= strip_tags($pengaturan_printout['label_interpretasi_hasil']); ?><br>
                      <i><?= strip_tags($pengaturan_printout['label_interpretasi_hasil_eng']); ?></i>
                    </td>
                    <td width="10px">:</td>
                    <td><?=$interpretasi_hasil_pemeriksaan?></td>
                  </tr>
                  <tr>
                    <td colspan="3" style="font-size:10px!important"><?= strip_tags($pengaturan_printout['label_waktu_input_order']); ?> : <?= $tanggal_input_pemeriksaan; ?> | <?= strip_tags($pengaturan_printout['label_waktu_sampling']); ?> : <?= $tanggal_input_sampling; ?> | <?= strip_tags($pengaturan_printout['label_waktu_cetak']); ?> : <?= $tanggal_cetak; ?></td>
                  </tr>
                  <tr>
                    <td colspan="3" style="font-size:10px!important"><?= strip_tags($pengaturan_printout['label_jumlah_cetak']); ?> : <?= $cetakan_ke; ?> - <?= strip_tags($pengaturan_printout['label_user_input_order']); ?>: <?= $user_input_pemeriksaan; ?> / <?= strip_tags($pengaturan_printout['label_user_sampling']); ?>: <?= $user_input_sampling; ?> / <?= strip_tags($pengaturan_printout['label_user_input_hasil']); ?>: <?= $user_input_hasil; ?> / <?= strip_tags($pengaturan_printout['label_user_cetak']); ?>: <?= $user_cetak; ?></i></td>
                  </tr>
                  <tr>
                    <td colspan="3" style="font-size:10px!important"><?= strip_tags($pengaturan_printout['footer_notes']); ?></td>
                  </tr>
                </table>
              </td>
              <td>
                <table>
                  <tr>
                    <td style="width:30%" class="text-center text-bold"><?= strip_tags($pengaturan_printout['label_tanda_tangan']); ?> <br> <i><?= strip_tags($pengaturan_printout['label_tanda_tangan_eng']); ?></i></td>
                  </tr>
                  <tr>
                    <td style="width:30%" class="text-center"><img src="<?= base_url(); ?>qrcode/qr_code_ttd/<?= $user_cetak_id; ?>" width="100px"></td>
                  </tr>
                  <tr>
                    <td style="width:30%" class="text-center text-bold">( <?= $user_cetak; ?> )</td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>
