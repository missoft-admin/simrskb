<script type="text/javascript">
function inputHasilPemeriksaan(transaksiId) {
    window.location = '{base_url}term_laboratorium_umum_hasil/proses/' + transaksiId;
}

function editHasilPemeriksaan(transaksiId) {
    window.open('{base_url}term_laboratorium_umum_hasil/proses/' + transaksiId + '/edit_hasil_pemeriksaan', '_blank');
}

function lihatHasilPemeriksaan(transaksiId) {
    window.open('{base_url}term_laboratorium_umum_hasil/proses/' + transaksiId + '/lihat_hasil_pemeriksaan', '_blank');
}

function editTarifPemeriksaan(asalRujukan, pendaftaranId, transaksiId) {
    window.open('{base_url}term_laboratorium_umum/tindakan/' + asalRujukan + '/' + pendaftaranId + '/erm_lab/laboratorium_pemeriksaan/' + transaksiId + '/' + 'edit_pemeriksaan' , '_blank');
}

function batalTransaksi(transaksiId) {
    swal({
        title: 'Batalkan Permintaan',
        text: 'Apakah Anda yakin ingin membatalkan permintaan ini? Jika dilakukan pembatalan, permintaan Anda tidak akan terkirim ke laboratorium tujuan. Tekan "Ya" untuk membatalkan.',
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#d33",
        cancelButtonText: "Batalkan",
    }).then((willSubmit) => {
        if (willSubmit) {
            $.ajax({
                url: '{site_url}term_laboratorium_umum/batal_draft_permintaan/' + transaksiId,
                success: function(result) {
                    $.toaster({
                        priority: 'success',
                        title: 'Berhasil!',
                        message: 'Data berhasil dihapus.'
                    });

                    // Re-load Datatable
                    loadListTransaksi();
                },
                error: function(error) {
                    console.error('Error deleting order:', error);
                    alert('Error deleting order. Please try again.');
                }
            });
        }
    });
}

function validasiTransaksi(transaksiId) {
    swal({
        title: 'Validasi Transaksi',
        text: 'Apakah Anda yakin ingin melakukan validasi transaksi ini? Jika dilakukan validasi, transaksi Anda akan diproses lebih lanjut. Tekan "Ya" untuk melakukan validasi.',
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Ya",
        confirmButtonColor: "#d33",
        cancelButtonText: "Batalkan",
    }).then((willSubmit) => {
        if (willSubmit) {
            $.ajax({
                url: '{site_url}term_laboratorium_umum_hasil/validasi_transaksi/' + transaksiId,
                success: function(result) {
                    $.toaster({
                        priority: 'success',
                        title: 'Berhasil!',
                        message: 'Data berhasil divalidasi.'
                    });

                    // Re-load Datatable
                    loadListTransaksi();
                },
                error: function(error) {
                    console.error('Error validation order:', error);
                    alert('Error validation order. Please try again.');
                }
            });
        }
    });
}

function cetakHasilPemeriksaan(hasilPemeriksaanId, typeFormat) {
    window.open('{base_url}term_laboratorium_umum_hasil/cetak_hasil_pemeriksaan/' + hasilPemeriksaanId + '/' + typeFormat, '_blank');
}

function kirimEmailHasilPemeriksaan(transaksiId, pasienId, tujuanLaboratorium) {
    loadDataPetugasPengirimHasil(tujuanLaboratorium, '');
    loadDataPetugasPenerimaHasil(tujuanLaboratorium, '');
    getEmailPasien(pasienId);

    $('#transaksiId').val(transaksiId);
}

function kirimUlangEmailHasilPemeriksaan(transaksiId) {
    getEmailPengirimanHasilPemeriksaanTerakhir(transaksiId);

    $('#transaksiId').val(transaksiId);
}

function riwayatPengirimanEmailHasilPemeriksaan(transaksiId) {
    window.open('{base_url}term_laboratorium_umum_hasil/riwayat_pengiriman_email_hasil_pemeriksaan/' + transaksiId, '_blank');
}

function riwayatHasilPemeriksaan(transaksiId) {
    window.open('{base_url}term_laboratorium_umum_hasil/riwayat_hasil_pemeriksaan/' + transaksiId, '_blank');
}

function riwayatCetakHasilPemeriksaan(hasilPemeriksaanId) {
    $.ajax({
        url: '{base_url}term_laboratorium_umum_hasil/riwayat_cetak_hasil_pemeriksaan/' + hasilPemeriksaanId,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            if (response.status == 'success') {
                var tableBody = $('#table-history-cetak tbody');
                tableBody.empty(); // Clear existing rows

                $.each(response.data, function(index, item) {
                    var row = '<tr>' +
                        '<td class="text-center">' + (index + 1) + '</td>' +
                        '<td class="text-center">' + item.urutan_cetak + '</td>' +
                        '<td class="text-center">' + item.petugas_cetak_hasil + '<br>' + item.waktu_cetak_hasil + '</td>' +
                        '</tr>';
                    tableBody.append(row);
                });
            } else {
                alert('Error fetching data');
            }
        },
        error: function(xhr, status, error) {
            console.error(error);
            alert('Error fetching data');
        }
    });
}

function riwayatLaboratorium(pendaftaranId, pasienId) {
    window.open('{base_url}term_laboratorium_umum/riwayat_laboratorium/' + pendaftaranId + '/' + pasienId, '_blank');
}

function dataLaboratorium(asalRujukan, pendaftaranId, transaksiId) {
    window.open('{base_url}term_laboratorium_umum/data_laboratorium/' + asalRujukan + '/' + pendaftaranId + '/' + transaksiId, '_blank');
}

function stikerIdentitas(transaksiId) {
    window.open('{base_url}term_laboratorium_umum/stiker_identitas/' + transaksiId, '_blank');
}

function labelTabungDarah(transaksiId) {
    window.open('{base_url}term_laboratorium_umum/label_tabung_darah/' + transaksiId, '_blank');
}

function inputBMHPTagihan(pendaftaranId) {
    window.open('{base_url}tpoliklinik_trx/tindakan/' + pendaftaranId + '/erm_trx/input_bmhp', '_blank');
}                    

function inputBMHPNonTagihan(pendaftaranId) {
    window.open('{base_url}tpoliklinik_trx/tindakan/' + pendaftaranId + '/erm_trx/input_bmhp', '_blank');
}    

function loadDataPetugasPengambilSample(params, value) {
    $.ajax({
        url: '{site_url}term_laboratorium_umum/petugas_pengambilan_sample/' + params,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            $("#petugasPengambilSample").select2('destroy');
            $("#petugasPengambilSample").empty();
            
            response.petugas.map(function(petugas) {
                let selected = '';
                if (petugas.iduser == value) {
                    selected = 'selected';
                } else if (petugas.status_default == '1') {
                    selected = 'selected';
                }

                $("#petugasPengambilSample").append('<option value="' + petugas.iduser + '" ' + selected + '>' + petugas.nama_petugas + '</option>');
            });

            $("#petugasPengambilSample").select2();
        },
        error: function(error) {
            console.error('Error loading data:', error);
        }
    });
}

function loadDataPetugasPengujian(params, value) {
    $.ajax({
        url: '{site_url}term_laboratorium_umum/petugas_pengujian_sample/' + params,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            $("#petugasPengujian").select2('destroy');
            $("#petugasPengujian").empty();
            
            response.petugas.map(function(petugas) {
                let selected = '';
                if (petugas.iduser == value) {
                    selected = 'selected';
                } else if (petugas.status_default == '1') {
                    selected = 'selected';
                }

                $("#petugasPengujian").append('<option value="' + petugas.iduser + '" ' + selected + '>' + petugas.nama_petugas + '</option>');
            });

            $("#petugasPengujian").select2();
        },
        error: function(error) {
            console.error('Error loading data:', error);
        }
    });
}

function loadDataPetugasPengirimHasil(params, value) {
    $.ajax({
        url: '{site_url}term_laboratorium_umum/petugas_pengiriman_hasil/' + params,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            $("#petugasPengirimHasil").select2('destroy');
            $("#petugasPengirimHasil").empty();

            response.petugas.map(function(petugas) {
                let selected = '';
                if (petugas.iduser == value) {
                    selected = 'selected';
                } else if (petugas.status_default == '1') {
                    selected = 'selected';
                }

                $("#petugasPengirimHasil").append('<option value="' + petugas.iduser + '" ' + selected + '>' + petugas.nama_petugas + '</option>');
            });

            $("#petugasPengirimHasil").select2();
        },
        error: function(error) {
            console.error('Error loading data:', error);
        }
    });
}

function loadDataDokterLaboratorium(params, value) {
    $.ajax({
        url: '{site_url}term_laboratorium_umum/dokter_laboratorium/' + params,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            $("#dokterLaboratorium").select2('destroy');
            $("#dokterLaboratorium").empty();
            
            response.dokter.map(function(dokter) {
                let selected = '';
                if (dokter.iddokter == value) {
                    selected = 'selected';
                } else if (dokter.status_default == '1') {
                    selected = 'selected';
                }

                $("#dokterLaboratorium").append('<option value="' + dokter.iddokter + '" ' + selected + '>' + dokter.nama_dokter + '</option>');
            });

            $("#dokterLaboratorium").select2();
        },
        error: function(error) {
            console.error('Error loading data:', error);
        }
    });
}

function loadDataPetugasPenerimaHasil(params, value) {
    $.ajax({
        url: '{site_url}term_laboratorium_umum/petugas_penerimaan_hasil/' + params,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            $("#petugasPenerimaHasil").select2('destroy');
            $("#petugasPenerimaHasil").empty();

            response.petugas.map(function(petugas) {
                let selected = '';
                if (petugas.iduser == value) {
                    selected = 'selected';
                } else if (petugas.status_default == '1') {
                    selected = 'selected';
                }

                $("#petugasPenerimaHasil").append('<option value="' + petugas.iduser + '" ' + selected + '>' + petugas.nama_petugas + '</option>');
            });

            $("#petugasPenerimaHasil").select2();
        },
        error: function(error) {
            console.error('Error loading data:', error);
        }
    });
}

function getEmailPasien(pasienId) {
    $('#emailKirimHasil').importTags('');
    $.ajax({
        url: '{base_url}term_laboratorium_umum_hasil/get_email_pasien/' + pasienId,
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            $('#emailKirimHasil').addTag(data.email);
        },
        error: function(xhr, status, error) {
            console.error('Error fetching email pasien:', error);
        }
    });
}

function getEmailPengirimanHasilPemeriksaanTerakhir(transaksiId) {
    $('#emailKirimUlangHasil').importTags('');
    $.ajax({
        url: '{base_url}term_laboratorium_umum_hasil/get_email_pengiriman_hasil_pemeriksaan_terakhir/' + transaksiId,
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            $('#emailKirimUlangHasil').importTags(data.email);
        },
        error: function(xhr, status, error) {
            console.error('Error fetching email pasien:', error);
        }
    });
}

</script>
