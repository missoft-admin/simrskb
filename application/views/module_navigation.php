
<?php $menu = $this->uri->segment(1); ?>


<?php if (in_array('1', $user_acces_menu, false) == '1'): ?>
	<li>
		<a <?=menuIsActive('dashboard')?> href="{base_url}dashboard"><i class="si si-speedometer"></i><span class="sidebar-mini-hide">Dashboard </a>
	</li>
<?php endif ?>

<?php if (UserAccesForm($user_acces_form, array('1505'))) {?>
	<li>
		<a <?=menuIsActive('tbooking_dashboard')?> href="{base_url}tbooking_dashboard"><i class="si si-call-in"></i><span class="sidebar-mini-hide">Dashboard Reservasi</a>
	</li>
<?php } ?>
<?php if (UserAccesForm($user_acces_form, array('21'))) {?>
	<li>
		<a <?=menuIsActive('executive_dashboard')?> href="{base_url}executive_dashboard"><i class="si si-drawer"></i><span class="sidebar-mini-hide">Executive Dashboard</a>
	</li>
<?php } ?>


<?php
	// Menu Master Data
	if (in_array('2', $user_acces_menu, false) == '1') {
		if (UserAccesForm($user_acces_form,
				array(
					'2','6','16','19','22','24','25','27','32','39','44','48',
					'85','96','118','129','133','138','143','229','148','234',
					'239','153','158','163','168','173','224','283','278','178',
					'183','187','191','195','199','203','207','211','287','292',
					'297','302','307','312'
				)
			)
		) {
			$this->load->view('_navigation/nav_master', array('menu' => $menu));
		}
	}
?>

<?php
	// Menu Transaksi
	if (in_array('3', $user_acces_menu, false) == '1') {
		$this->load->view('_navigation/nav_transaksi', array('menu' => $menu));
	}
?>

<?php
	// Menu Laporan
	if (in_array('4', $user_acces_menu, false) == '1') {
		$this->load->view('_navigation/nav_laporan', array('menu' => $menu));
	}
?>

<?php
	// Menu Setting
	if (in_array('5', $user_acces_menu, false) == '1') {
		if (UserAccesForm($user_acces_form, array('215','244','248'))) {
			$this->load->view('_navigation/nav_setting', array('menu' => $menu));
		}
	}
?>

<?php
	// Menu Keuangan
	if (in_array('6', $user_acces_menu, false) == '1') {
		$this->load->view('_navigation/nav_keuangan', array('menu' => $menu));
	}
?>
<?php
	// Menu Apps
	$this->load->view('_navigation/nav_apps', array('menu' => $menu));
	
?>
<?php
	// Menu Apps
	if (in_array('20', $user_acces_menu, false) == '1') {
	$this->load->view('_navigation/nav_billing', array('menu' => $menu));
	}
?>

