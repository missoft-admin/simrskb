<?if($st_gc=='1'){?>
		<div class="row">
			<div class="col-md-12">
				<table>
					<tr>
						<td width="10%"><img class="img-avatar"  id="output_img" src="{upload_path}app_setting/<?=($logo_gc?$logo_gc:'no_image.png')?>" /></td>
						<td  width="90%" class="text-center text-bold"><h3>{judul_gc}</h3></td>
					</tr>
				</table>
				<table class="table table-bordered table-striped"  id="tabel_gc">
					<thead>
						<tr>
							<td width="2%" class="text-center text-bold text-italic text-header"></td>
							<td width="70%" class="text-center text-bold text-italic text-header">{sub_header_gc}</td>
							<td  width="28%" class="text-center text-bold text-italic text-header">{sub_header_side_gc}</td>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
				
				<div class="col-md-6 push-5 push-0-l" <?=($st_general=='1'?'hidden':'')?>>
					<label class="col-md-12" for="validation-classic-city">MENGIJINKAN / TIDAK MENGIJINKAN	</label>
					<div class="col-md-12 push-5">
						<select name="jawaban_perssetujuan" <?=($st_general=='1'?'disabled':'')?> id="jawaban_perssetujuan" class="js-select2 form-control" style="width: 100%;" data-placeholder="MENGIJINKAN / TIDAK MENGIJINKAN">
							<option value="" <?=($jawaban_perssetujuan == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
							<option value="MENGIJINKAN" <?=($jawaban_perssetujuan == 'MENGIJINKAN' ? 'selected="selected"' : '')?>>MENGIJINKAN</option>
							<option value="TIDAK MENGIJINKAN" <?=($jawaban_perssetujuan == 'TIDAK MENGIJINKAN' ? 'selected="selected"' : '')?>>TIDAK MENGIJINKAN</option>
						</select>
					</div>
				</div>
				<table>
					<tr>
						<td  width="100%" class="text-left">{footer_1_gc}</td>
					</tr>
					
					<tr>
						<td  width="100%" class="text-left"><?=trim($footer_2_gc)?></td>
					</tr>
				</table>
				<table>
					<tr>
						<td  width="70%" class="text-left">NAMA PASIEN</td>
						<td  width="30%" class="text-left">JENIS KELAMIN</td>
					</tr>
					<tr>
						<td  width="70%" class="text-left">
							<input type="text" disabled class="form-control" id="namapasien" placeholder="Nama Lengkap" name="namapasien" value="{namapasien}" required>
						</td>
						<td  width="30%" class="text-left">
								<select tabindex="7" disabled  class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="">Pilih Opsi</option>
									<?foreach(list_variable_ref(1) as $row){?>
									<option value="<?=$row->id?>" <?=($jenis_kelamin == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
									<?}?>
								</select>
						</td>
					</tr>
					<tr>
						<td  width="70%" class="text-left">{ttd_1_gc}</td>
						<td  width="30%" class="text-left">HUBUNGAN</td>
					</tr>
					<tr>
						<td  width="70%" class="text-left">
							<input disabled type="text" class="form-control" placeholder="Nama Penanggung Jawab" name="namapenanggungjawab" value="{namapenanggungjawab}" required>
						</td>
						<td  width="30%" class="text-left">
								<select disabled  class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</option>
									<?foreach(list_variable_ref(9) as $row){?>
									<option value="<?=$row->id?>" <?=($hubunganpenanggungjawab == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
									<?}?>
								</select>
						</td>
					</tr>
					
				</table>
				<div class="col-md-6 push-5 push-0-l">
					<label class="col-md-12" for="validation-classic-city">Tanda Tangan</label>
					<div class="col-md-6">
						<?if ($ttd_gc){?>
							<div class="img-container fx-img-rotate-r">
								<img class="img-responsive" src="<?=$ttd_gc?>" alt="">
								<div class="img-options">
									<div class="img-options-content">
										<div class="btn-group btn-group-sm">
											<a class="btn btn-default" onclick="modal_ttd_gc()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
											<a class="btn btn-default btn-danger" onclick="hapus_ttd_gc()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
										</div>
									</div>
								</div>
							</div>
						<?}else{?>
							<button class="btn btn-sm btn-success" onclick="modal_ttd_gc()" id="btn_ttd_final" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
						<?}?>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<input class="form-control" readonly name="ttd_gc" id="ttd_gc" value="{ttd_gc}" type="hidden">
			</div>
		</div>
		<div class="row">
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger " type="submit" name="btn_simpan"  value="2"><i class="fa fa-save"></i> Update & Keluar</button>	
				<?if ($st_general=='0'){?>
					<?if ($st_hk=='1' && $st_hak=='0'){?>
					<button class="btn btn-sm btn-primary btn_gc" type="submit" name="btn_simpan"  value="5"><i class="fa fa-arrow-right"></i> Lanjut Hak & Kewajiban</button>
					<?}else{?>
						<?if ($st_hk=='1' && $st_hak='0'){?>
						<button class="btn btn-sm btn-primary btn_gc" type="submit" name="btn_simpan"  value="5"><i class="fa fa-arrow-right"></i> Lanjut Hak & Kewajiban</button>
						<?}else{?>
						<button class="btn btn-sm btn-success btn_gc" type="submit" name="btn_simpan"  value="51"><i class="fa fa-arrow-right"></i> Selesai </button>
						
						<?}?>
					<?}?>
				<?}else{?>
					<a href="{base_url}tpendaftaran_ranap" class="btn btn-sm btn-default" name="btn_simpan" value="2" type="button" ><i class="fa fa-reply"></i> Kembali</a>
				<?}?>
			</div>
		</div>
		
	
	<?}?>

