<div class="row">
	<div class="col-md-12">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-12 control-label text-primary"><h5 align="left"><b>DATA KUNJUNGAN</b></h5></label>
			<div class="col-md-12">
				<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
			</div>
		</div>
	</div>
	<div class="col-md-6"><!--DATA KUNJUNGAN 1-->
		
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 col-xs-12 push-5 control-label" for="tglpendaftaran">Pasien<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<input tabindex="2" type="text" readonly class="form-control namapasien"  value="<?=$namapasien?>" required>
				
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 col-xs-12 push-5 control-label" for="nopendaftaran_poli">No. Pendaftaran<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<div class="input-group">
					<input type="text" readonly class="form-control nopendaftaran_poli"  placeholder="No Pendaftaran"  value="{nopendaftaran_poli}">
					<span class="input-group-btn">
						<button class="btn btn-primary" type="button" onclick="show_modal_pendaftaran()"><i class="fa fa-pencil"></i></button>
					</span>
				</div>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 col-xs-12 control-label" for="tglpendaftaran">Asal Rujukan<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select tabindex="1" disabled class="js-select2 form-control idtipe_asal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="" <?=($idtipe_asal == "" ? 'selected="selected"' : '')?>>Pilih Tipe</option>
					<option value="1" <?=($idtipe_asal == 1 ? 'selected="selected"' : '')?>>Poliklinik</option>
					<option value="2" <?=($idtipe_asal == 2 ? 'selected="selected"' : '')?>>IGD</option>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 col-xs-12 push-5 control-label" for="tglpendaftaran">Perencanaan Rawat Inap</label>
			<div class="col-md-8">
				<div class="input-group">
				<input type="text" readonly class="form-control nopermintaan" disabled placeholder="No Permintaan"  value="{nopermintaan}">
					<span class="input-group-btn">
						<button class="btn btn-primary" onclick="show_modal_perencanaan()" type="button"><i class="fa fa-pencil"></i></button>
					</span>
				</div>
			</div>
		</div>
		
	</div>
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="antrian_id">No. Antrian <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select tabindex="34"  disabled class="js-select2 form-control antrian_id" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="#" selected>TANPA NO ANTRIAN</option>
					<?php foreach ($list_antrian as $r):?>
					<option value="<?= $r->id; ?>" <?=($r->id==$antrian_id?'selected':'')?>><?= $r->kodeantrian.' - '.HumanDateShort($r->tanggal); ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="iddokter_perujuk">Dokter Perujuk <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select disabled class="js-select2 form-control iddokter_perujuk" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="" selected></option>
					<?php foreach (get_all('mdokter',array('status'=>1)) as $r):?>
					<option value="<?= $r->id; ?>" <?=($iddokter_perujuk==$r->id?'selected':'')?>><?=$r->nama; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="idpoliklinik_asal">Poliklinik <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select tabindex="34" disabled class="js-select2 form-control idpoliklinik_asal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="" selected></option>
					<?php foreach (get_all('mpoliklinik',array('status'=>1)) as $r):?>
					<option value="<?= $r->id; ?>" <?=($idpoliklinik_asal==$r->id?'selected':'')?>><?=$r->nama; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="nama_panggilan">Diagnosa </label>
			<div class="col-md-8">
				<textarea class="form-control diagnosa" placeholder="Diagnosa" disabled required="" aria-required="true"><?=$diagnosa?></textarea>
			</div>
		</div>
		
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-12 control-label text-primary"><h5 align="left"><b>DATA PENDAFTARAN</b></h5></label>
			<div class="col-md-12">
				<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 col-xs-12 push-5 control-label" for="idtipe">Tipe Pelayanan<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select  id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<option value="1" <?=($idtipe == 1 ? 'selected="selected"' : '')?>>RAWAT INAP</option>
					<option value="2" <?=($idtipe == 2 ? 'selected="selected"' : '')?>>ODS</option>
				</select>
				
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 col-xs-12 push-5 control-label" for="kasus_kepolisian">Kasus Kepolisian<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select  id="kasus_kepolisian" name="kasus_kepolisian" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<option value="1" <?=($kasus_kepolisian == 1 ? 'selected="selected"' : '')?>>YA</option>
					<option value="2" <?=($kasus_kepolisian == 2 ? 'selected="selected"' : '')?>>TIDAK</option>
				</select>
				
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 col-xs-12 push-5 control-label" for="idasalpasien">Cara Masuk<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select name="idasalpasien" id="idasalpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<?php foreach (get_all('mpasien_asal') as $r):?>
						<option value="<?= $r->id; ?>" <?=($idasalpasien == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></option>
					<?php endforeach; ?>
				</select>
				
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 col-xs-12 push-5 control-label" for="idkelompokpasien">Kelompok Pasien<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select tabindex="34" name="idkelompokpasien" id="idkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="" <?=($idkelompokpasien==''?'selected':'')?>>Pilih Opsi</option>
					<?php foreach ($list_cara_bayar as $r):?>
					<option value="<?= $r->id; ?>" <?=($r->id == $idkelompokpasien ? 'selected' : '')?>><?= $r->nama; ?></option>
					<?php endforeach; ?>
				</select>
				
			</div>
		</div>
		<div class="form-group div_asuransi" style="margin-bottom: 10px;display:none">
			<label class="col-md-4 control-label" for="idrekanan">Nama Perusahaan / Asuransi <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select  name="idrekanan" id="idrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="" <?=($idrekanan==''?'selected':'')?>>Pilih Opsi</option>
					<?php foreach ($list_asuransi as $r) {?>
					<option value="<?= $r->id?>" <?=($idrekanan == $r->id ? 'selected="selected"' : '')?>><?= $r->nama?></option>
					<?php }?>
				</select>
			</div>
		</div>
		
		<div class="form-group div_foto" style="margin-bottom: 10px;display:none">
			<label class="col-md-4 control-label" for="idrekanan">No Kartu<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<div class="input-group">
					<input class="form-control" name="nokartu" id="nokartu" value="{nokartu}" type="text">
					<span class="input-group-btn">
						<button class="btn btn-success" type="button" id="btn_cari_kartu"><i class="fa fa-search"></i></button>
						<button class="btn btn-primary" type="button" id="btn_tambah_kartu"><i class="fa fa-plus"></i></button>
						
					</span>
				</div>
			</div>
		</div>
		<div class="form-group div_foto" style="margin-bottom: 10px;display:none">
			<label class="col-md-4 control-label" for="idrekanan">Nama Participant<span style="color:red;">*</span></label>
			<div class="col-md-8">
					<input class="form-control" name="nama_kartu" id="nama_kartu" value="{nama_kartu}" type="text">
			</div>
		</div>
		<div class="form-group div_foto" style="margin-bottom: 10px;display:none">
			<label class="col-md-4 control-label" for="idrekanan">No Reference <span style="color:red;">*</span></label>
			<div class="col-md-8">
					<input class="form-control" name="noreference_kartu" id="noreference_kartu" value="{noreference_kartu}" type="text">
					<input class="form-control" name="kartu_id" id="kartu_id" value="{kartu_id}" type="hidden">
			</div>
		</div>
			<?
	if ($foto_kartu==''){
		$foto_kartu='default.png';
						}
						?>
		<div class="form-group div_foto" style="margin-bottom: 10px;display:none">
			<label class="col-md-4 control-label" for="foto_kartu">Foto Kartu Asuransi<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<a id="url_foto" href="{upload_path}foto_kartu/<?=$foto_kartu?>" target="_blank">
					<img class="img-responsive" for="foto_kartu"  id="foto_kartu_view" src="{upload_path}foto_kartu/<?=$foto_kartu?>" id="output_img" style="width:20%" />
				</a>
			</div>
		</div>
		<div class="form-group div_foto" style="margin-bottom: 10px;display:none">
			<label class="col-md-4 control-label" for="foto_kartu"> <span style="color:red;">*</span></label>
			<div class="col-md-8">
			
					<input type="file"  accept="image/*" onchange="loadFile_foto(event)"  class="btn btn-sm btn-primary" id="foto_kartu" name="foto_kartu" value="{foto_kartu}" />
			</div>
		</div>
		
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="iddokterpenanggungjawab">DPJP Utama<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select  name="iddokterpenanggungjawab" id="iddokterpenanggungjawab" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="" <?=($iddokterpenanggungjawab==''?'selected':'')?>>Pilih Opsi</option>
					<?php foreach (get_all('mdokter',array('status'=>1)) as $r) {?>
					<option value="<?= $r->id?>" <?=($iddokterpenanggungjawab == $r->id ? 'selected="selected"' : '')?>><?= $r->nama?></option>
					<?php }?>
				</select>
			</div>
		</div>
	</div>
	<?

		$disabel_tipe='';
		$q="select st_tipe_pasien_edit FROM setting_ranap ";
		$st_tipe_pasien_edit=$this->db->query($q)->row('st_tipe_pasien_edit');
		if ($st_tipe_pasien_edit=='2'){
			$disabel_tipe='disabled';
		}
		$list_ruangan=get_all('mruangan',array('status'=>1,'idtipe'=>'1','st_tampil'=>1));
		$list_kelas=get_all('mkelas',array('status'=>1));
		$list_bed=get_all('mbed',array('status'=>1));
	?>
	<?php if ($id != '' && ($idasalpasien == '2' || $idasalpasien == '3')) {?>
			<?php
				if ($idasalpasien == 2) {
					$idtipeRS = 1;
				} elseif ($idasalpasien == 3) {
					$idtipeRS = 2;
				}
				
			}else{
				$idtipeRS='';
			}
			?>
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 col-xs-12 push-5 control-label" for="tanggaldaftar">Tanggal Daftar <span style="color:red;">*</span></label>
			<div class="col-md-3 col-xs-6">
				<div class="input-group date">
					<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tanggaldaftar" placeholder="HH/BB/TTTT" name="tanggaldaftar" value="<?= $tanggaldaftar ?>" required>
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
			</div>
			<div class="col-md-3 col-xs-6">
				<div class="input-group">
					<input tabindex="3" type="text" class="time-datepicker form-control auto_blur" id="waktudaftar" name="waktudaftar" value="<?= $waktudaftar ?>" required>
					<span class="input-group-addon"><i class="si si-clock"></i></span>
				</div>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 col-xs-12 push-5 control-label" for="dirawat_ke">Dirawat Ke - </label>
			<div class="col-md-6">
				<div class="input-group">
					<input type="text" class="form-control " id="dirawat_ke" name="dirawat_ke" readonly placeholder="Ke"  value="{dirawat_ke}" >
					<span class="input-group-btn">
						<button class="btn btn-info" type="button" onclick="show_dirawat_ke()"><i class="fa fa-search"></i> Lihat History</button>
					</span>
				</div>
			</div>
		</div>
		<div class="form-group div_rujukan" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="idrujukan">Nama Fasilitas Kesehatan <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<div class="input-group">
					<select tabindex="33" name="idrujukan" id="idrujukan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mrumahsakit', ['idtipe' => $idtipeRS]) as $r) {?>
							<option value="<?= $r->id?>" <?=($idrujukan == $r->id ? 'selected="selected"' : '')?>><?= $r->nama?></option>
						<?php }?>
					</select>
					
					<span class="input-group-btn">
						<button class="btn btn-warning" title="refresh" onclick="set_asal_rujukan()" type="button"><i class="fa fa-refresh"></i></button>
						<a href="{base_url}mrumahsakit/create" title="Tambah Faskes" target="_blannk" class="btn btn-success" type="button"><i class="fa fa-plus"></i></a>
						
					</span>
				</div>
				
			</div>
		</div>
		<div class="form-group " style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="idrujukan">Tipe Pasien <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select <?=$disabel_tipe?> name="idtipepasien" id="idtipepasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="1" <?=($idtipepasien == 1 ? 'selected="selected"' : '')?>>Pasien RS</option>
					<option value="2" <?=($idtipepasien == 2 ? 'selected="selected"' : '')?>>Pasien Pribadi</option>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="st_dpjp_pendamping">DPJP Pendamping<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select  name="st_dpjp_pendamping" id="st_dpjp_pendamping" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="0" <?=($st_dpjp_pendamping=='0'?'selected':'')?>>Tidak Ditentukan</option>
					<option value="1" <?=($st_dpjp_pendamping=='1'?'selected':'')?>>Ditentukan</option>
					
				</select>
			</div>
		</div>
		<div class="form-group div_dpjp" style="margin-bottom: 10px;">
			<div class="col-md-2">
			</div>
			<div class="col-md-10">
			<table id="tabel_dpjp" class="table table-bordered">
				<thead>
					<tr>
						<th style="width:15%" class="text-center">No Urut</th>
						<th style="width:70%" class="text-left">Dokter</th>
						<th style="width:15%" class="text-center">Action</th>
					</tr>
					<tr>
						<th style="width:15%" class="text-center">
							<select name="no_dpjp" id="no_dpjp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
								<option value="" selected>Pilih Opsi</option>
								<?php for($i=1;$i<=20;$i++) {?>
								<option value="<?= $i?>"><?= $i?></option>
								<?php }?>
							</select>
						</th>
						<th style="width:70%" class="text-center">
									<select  name="dpjp" id="dpjp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
										<option value="" selected>Pilih Opsi</option>
										<?php foreach (get_all('mdokter',array('status'=>1)) as $r) {?>
										<option value="<?= $r->id?>"><?= $r->nama?></option>
										<?php }?>
									</select>
						</th>
						<th style="width:15%" class="text-center">
							<button class="btn btn-primary" title="Tambah" onclick="add_dpjp()" type="button"><i class="fa fa-plus"></i></button>
						</th>
					</tr>
				</thead>
				<tbody>
					<?foreach($list_dokter_pendamping as $r){?>
						<tr>
							<td class='text-center'><?=$r->nourut?><input type="hidden" class="dpjp_id" name="dpjp_id[]" value="<?=$r->iddokter?>"><input type="hidden" name="no_dpjp[]" class="no_dpjp" value="<?=$r->nourut?>"></td>
							<td><?=$r->nama?></td>
							<td class='text-center'><button class="btn btn-danger btn-sm" title="Hapus" onclick="hapus_dpjp(this)" type="button"><i class="fa fa-trash"></i></button></td>
						</tr>
					<?}?>
				</tbody>
			</table>
			
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 col-xs-12 push-5 control-label" for="dirawat_ke">Kelengkapan Data<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<div class="input-group">
					<p class="nice-copy" id="text_skrining"></p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-12 control-label text-primary"><h5 align="left"><b>BED</b></h5></label>
			<div class="col-md-12">
				<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
			</div>
		</div>
		<div class="form-group " style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="idruangan">Ruangan <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select name="idruangan" id="idruangan" class="js-select2 form-control div_ods" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="" <?=($idruangan == "" ? 'selected="selected"' : '')?>>Pilih Opsi</option>
					<?foreach($list_ruangan as $r){?>
					<option value="<?=$r->id?>" <?=($idruangan == $r->id ? 'selected="selected"' : '')?>><?=$r->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		<div class="form-group " style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="idruangan">Kelas <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select name="idkelas" id="idkelas" class="js-select2 form-control div_ods" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="" <?=($idkelas == "" ? 'selected="selected"' : '')?>>Pilih Opsi</option>
					<?foreach($list_kelas as $r){?>
					<option value="<?=$r->id?>" <?=($idkelas == $r->id ? 'selected="selected"' : '')?>><?=$r->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="idbed">Bed <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select  name="idbed" id="idbed" class="js-select2 form-control div_ods" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="0" <?=($idbed == "0" ? 'selected="selected"' : '')?>>Tidak Dipilih</option>
					<?foreach($list_bed as $r){?>
					<option value="<?=$r->id?>" <?=($idbed == $r->id ? 'selected="selected"' : '')?>><?=$r->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
	</div>
	<div class="col-md-6 div_permintaan">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label text-primary text-right"><h5 align="right"><b>BED PERMINTAAN</b></h5></label>
			<div class="col-md-12">
				<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
			</div>
		</div>
		<div class="form-group " style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="idruangan_permintaan">Ruangan</label>
			<div class="col-md-8">
				<select name="idruangan_permintaan" id="idruangan_permintaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
					<option value="0" <?=($idruangan_permintaan == "" ? 'selected="selected"' : '')?>>Pilih Opsi</option>
					<?foreach($list_ruangan as $r){?>
					<option value="<?=$r->id?>" <?=($idruangan_permintaan == $r->id ? 'selected="selected"' : '')?>><?=$r->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		<div class="form-group " style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="idkelas_permintaan">Kelas </label>
			<div class="col-md-8">
				<select name="idkelas_permintaan" id="idkelas_permintaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
					<option value="0" <?=($idkelas_permintaan == "" ? 'selected="selected"' : '')?>>Pilih Opsi</option>
					<?foreach($list_kelas as $r){?>
					<option value="<?=$r->id?>" <?=($idkelas_permintaan == $r->id ? 'selected="selected"' : '')?>><?=$r->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		<div class="form-group " style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="idbed_permintaan">Bed</label>
			<div class="col-md-8">
				<select name="idbed_permintaan" id="idbed_permintaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
					<option value="0" <?=($idbed_permintaan == "0" ? 'selected="selected"' : '')?>>Pilih Opsi</option>
					<?foreach($list_bed as $r){?>
					<option value="<?=$r->id?>" <?=($idbed_permintaan == $r->id ? 'selected="selected"' : '')?>><?=$r->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
	</div>
	
</div>
	
<?if($id!=''){?>
<div class="block-content block-content-mini block-content-full border-t">
<div class="row">
	<div class="form-group" style="margin-bottom: 10px;">
	<div class="col-md-12 text-right">
		<a href="{base_url}tpendaftaran_ranap" class="btn btn-sm btn-default" name="btn_simpan" value="2" type="submit" ><i class="fa fa-reply"></i> Kembali</a>
		<button class="btn btn-sm btn-primary " type="submit" name="btn_simpan"  value="1"><i class="fa fa-save"></i> Update</button>				
		<button class="btn btn-sm btn-danger " type="submit" name="btn_simpan"  value="2"><i class="fa fa-save"></i> Update & Keluar</button>				
	</div>
	</div>
</div>
</div>
<?}else{?>
	<div class="block-content block-content-mini block-content-full border-t">
	<div class="row">
		<div class="form-group" style="margin-bottom: 10px;">
		<div class="col-md-12 text-right">
			<a href="{base_url}tpendaftaran_ranap" class="btn btn-sm btn-default" name="btn_simpan" value="2" type="submit" ><i class="fa fa-reply"></i> Kembali</a>
			<button class="btn btn-sm btn-primary " type="submit" name="btn_simpan"  value="1"><i class="fa fa-save"></i> Update</button>				
			<button class="btn btn-sm btn-danger " type="submit" name="btn_simpan"  value="2"><i class="fa fa-save"></i> Update & Keluar</button>				
		</div>
		</div>
	</div>
	</div>
<?}?>