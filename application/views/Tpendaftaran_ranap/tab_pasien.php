<div class="row">
	<div class="col-md-12">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-12 control-label text-primary"><h5 align="left"><b>DATA KUNJUNGAN</b></h5></label>
			<div class="col-md-12">
				<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
			</div>
		</div>
	</div>
	<div class="col-md-6"><!--DATA KUNJUNGAN 1-->
		
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 col-xs-12 push-5 control-label" for="nopendaftaran_poli">No. Pendaftaran<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<div class="input-group">
					<input type="text" readonly class="form-control nopendaftaran_poli" id="nopendaftaran_poli" name="nopendaftaran_poli" placeholder="No Pendaftaran"  value="{nopendaftaran_poli}" required>
					<span class="input-group-btn">
						<button class="btn btn-primary" type="button" onclick="show_modal_pendaftaran()"><i class="fa fa-pencil"></i></button>
					</span>
				</div>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 col-xs-12 control-label" for="tglpendaftaran">Asal Rujukan<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select tabindex="1" name="idtipe_asal" disabled id="idtipe_asal" class="js-select2 form-control idtipe_asal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="" <?=($idtipe_asal == "" ? 'selected="selected"' : '')?>>Pilih Tipe</option>
					<option value="1" <?=($idtipe_asal == 1 ? 'selected="selected"' : '')?>>Poliklinik</option>
					<option value="2" <?=($idtipe_asal == 2 ? 'selected="selected"' : '')?>>IGD</option>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 col-xs-12 push-5 control-label" for="tglpendaftaran">Perencanaan Rawat Inap</label>
			<div class="col-md-8">
				<div class="input-group">
				<input type="text" readonly class="form-control nopermintaan" id="nopermintaan" name="nopermintaan" placeholder="No Permintaan"  value="{nopermintaan}">
					<span class="input-group-btn">
						<button class="btn btn-primary" onclick="show_modal_perencanaan()" type="button"><i class="fa fa-pencil"></i></button>
					</span>
				</div>
			</div>
		</div>
		
	</div>
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="antrian_id">No. Antrian <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select tabindex="34"  name="antrian_id" id="antrian_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="#" selected>TANPA NO ANTRIAN</option>
					<?php foreach ($list_antrian as $r):?>
					<option value="<?= $r->id; ?>" <?=($r->id==$antrian_id?'selected':'')?>><?= $r->kodeantrian.' - '.HumanDateShort($r->tanggal); ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="iddokter_perujuk">Dokter Perujuk <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select name="iddokter_perujuk" id="iddokter_perujuk" class="js-select2 form-control iddokter_perujuk" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="" selected></option>
					<?php foreach (get_all('mdokter',array('status'=>1)) as $r):?>
					<option value="<?= $r->id; ?>" <?=($iddokter_perujuk==$r->id?'selected':'')?>><?=$r->nama; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="idpoliklinik_asal">Poliklinik <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select tabindex="34"  name="idpoliklinik_asal" id="idpoliklinik_asal" class="js-select2 form-control idpoliklinik_asal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="" selected></option>
					<?php foreach (get_all('mpoliklinik',array('status'=>1)) as $r):?>
					<option value="<?= $r->id; ?>" <?=($idpoliklinik_asal==$r->id?'selected':'')?>><?=$r->nama; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="nama_panggilan">Diagnosa </label>
			<div class="col-md-8">
				<textarea class="form-control diagnosa" id="diagnosa" placeholder="Diagnosa" name="diagnosa" required="" aria-required="true"><?=$diagnosa?></textarea>
			</div>
		</div>
		
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<input type="hidden" id="idpasien" name="idpasien" value="{idpasien}">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-12 control-label text-primary"><h5 align="left"><b>DATA PASIEN</b></h5></label>
			<div class="col-md-12">
				<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
			</div>
		</div>
	</div>
	<div class="col-md-6">
		
		<div class="form-group" style="margin-bottom: 10px; padding-bottom:0px;">
			<label class="col-md-4 col-xs-12 push-5 control-label" for="namapasien">Nama <span style="color:red;">*</span></label>
			<div class="col-md-3 col-xs-4 push-5" style="padding-right: 10px;">
				<select tabindex="5" id="titlepasien" name="title" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<?php foreach (get_all('mpasien_title') as $r):?>
						<option value="<?= $r->singkatan; ?>" <?=($title_nama === $r->singkatan ? 'selected="selected"' : '')?>><?= $r->singkatan; ?></option>
					<?php endforeach; ?>
					</select>
			</div>
			<div class="col-md-5 col-xs-8 push-5" style="padding-left: 0px;">
				<input tabindex="6" type="text" class="form-control" id="namapasien" placeholder="Nama Lengkap" name="namapasien" value="{namapasien}" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="nik">NIK <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<input tabindex="6" type="text" class="form-control number" onkeyup="this.value=this.value.replace(/[^\d]/,'')" id="nik" placeholder="NIK" name="nik" value="{nik}" minlength="16" maxlength="16" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="ibu_kandung">Nama Ibu Kandung <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<input tabindex="6" type="text" class="form-control" id="nama_ibu_kandung" placeholder="Nama Ibu Kandung" name="nama_ibu_kandung" value="{nama_ibu_kandung}" required>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label" for="tgllahir">Tanggal Lahir<span style="color:red;">*</span></label>
			<div class="col-md-3  col-xs-12 push-5">
				<select tabindex="10" name="tgl_lahir" id="hari" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="0">Hari</option>
					<?php for ($i = 1; $i < 32; $i++) {?>
						<?php $hari = str_pad($i, 2, '0', STR_PAD_LEFT); ?>
						<option value="<?= $hari; ?>" <?=($tgl_hari == $hari ? 'selected="selected"' : '')?>><?= $hari; ?></option>
					<?php }?>
				</select>
			</div>
			<div class="col-md-2  col-xs-12 push-5">
				<select tabindex="11" name="bulan_lahir" id="bulan" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="00">Bulan</option>
					<option value="01" <?=($tgl_bulan == '01' ? 'selected="selected"' : '')?>>Januari</option>
					<option value="02" <?=($tgl_bulan == '02' ? 'selected="selected"' : '')?>>Februari</option>
					<option value="03" <?=($tgl_bulan == '03' ? 'selected="selected"' : '')?>>Maret</option>
					<option value="04" <?=($tgl_bulan == '04' ? 'selected="selected"' : '')?>>April</option>
					<option value="05" <?=($tgl_bulan == '05' ? 'selected="selected"' : '')?>>Mei</option>
					<option value="06" <?=($tgl_bulan == '06' ? 'selected="selected"' : '')?>>Juni</option>
					<option value="07" <?=($tgl_bulan == '07' ? 'selected="selected"' : '')?>>Juli</option>
					<option value="08" <?=($tgl_bulan == '08' ? 'selected="selected"' : '')?>>Agustus</option>
					<option value="09" <?=($tgl_bulan == '09' ? 'selected="selected"' : '')?>>September</option>
					<option value="10" <?=($tgl_bulan == '10' ? 'selected="selected"' : '')?>>Oktober</option>
					<option value="11" <?=($tgl_bulan == '11' ? 'selected="selected"' : '')?>>November</option>
					<option value="12" <?=($tgl_bulan == '12' ? 'selected="selected"' : '')?>>Desember</option>
				</select>
			</div>
			<div class="col-md-3  col-xs-12 push-5" >
				<select tabindex="12" name="tahun_lahir" id="tahun" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="0">Tahun</option>
					<?php for ($i = date('Y'); $i >= 1900; $i--) {?>
						<option value="<?= $i; ?>" <?=($tgl_tahun == $i ? 'selected="selected"' : '')?>><?= $i; ?></option>
					<?php }?>
				</select>
			</div>
		</div>
		<div class="form-group " >
			<label class="col-md-4 col-xs-12 control-label" for="umur">Umur</label>
			<div class="col-md-3 col-xs-12 push-5">
				<input type="text" class="form-control pas_tidak_dikenal" id="umurtahun" name="umurtahun" value="{umurtahun}" placeholder="Tahun" readonly="true" required>
			</div>
			<div class="col-md-2 col-xs-12 push-5">
				<input type="text" class="form-control pas_tidak_dikenal" id="umurbulan" name="umurbulan" value="{umurbulan}" placeholder="Bulan" readonly="true" required>
			</div>
			<div class="col-md-3 col-xs-12 push-5">
				<input type="text" class="form-control pas_tidak_dikenal" id="umurhari" name="umurhari" value="{umurhari}" placeholder="Hari" readonly="true" required>
			</div>
		</div>
		<input type="hidden" name="created" value="<?= date(' d-M-Y H:i:s ')?>">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="jenis_kelamin">Jenis Kelamin <span style="color:red;">*</span></label>
			<div class="col-md-8">
				<select tabindex="7" id="jenis_kelamin" name="jenis_kelamin" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(1) as $row){?>
					<option value="<?=$row->id?>" <?=($jenis_kelamin == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="nohp">No Hp</label>
			<div class="col-md-8">
				<input tabindex="20" type="text" class="form-control pas_tidak_dikenal" id="nohp" placeholder="No HP" name="nohp" value="{nohp}" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="warganegara">Kewarganegaraan</label>
			<div class="col-md-8">
				<select tabindex="25" id="warganegara" name="warganegara" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(18) as $row){?>
					<option value="<?=$row->id?>" <?=($warganegara == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
					
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="pendidikan">Pendidikan</label>
			<div class="col-md-8">
				<select tabindex="27" id="pendidikan" name="pendidikan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(13) as $row){?>
					<option value="<?=$row->id?>" <?=($pendidikan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="statuskawin">Status Pernikahan</label>
			<div class="col-md-8">
				<select tabindex="8" id="statuskawin" name="statuskawin" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(7) as $row){?>
					<option value="<?=$row->id?>" <?=($statuskawin == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
					
				</select>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="tglpendaftaran">No Medrec<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<input readonly class="form-control" id="no_medrec" placeholder="NO. MEDREC" name="no_medrec" value="{no_medrec}" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="nama_panggilan">Nama Panggilan </label>
			<div class="col-md-8">
				<input tabindex="6" type="text" class="form-control" id="nama_panggilan" placeholder="Nama Panggilan" name="nama_panggilan" value="{nama_panggilan}" >
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 col-xs-12 push-5 control-label" for="noidentitas">No Identitas</label>
			<div class="col-md-3 col-xs-4 push-5" style="padding-right: 10px;">
				<select tabindex="18" id="jenis_id" name="jenis_id" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="1" <?=($jenis_id == 1 ? 'selected="selected"' : '')?>>KTP</option>
					<option value="2" <?=($jenis_id == 2 ? 'selected="selected"' : '')?>>SIM</option>
					<option value="3" <?=($jenis_id == 3 ? 'selected="selected"' : '')?>>Pasport</option>
					<option value="4" <?=($jenis_id == 4 ? 'selected="selected"' : '')?>>Kartu Pelajar/Mahasiswa</option>
				</select>
			</div>
			<div class="col-md-5 col-xs-8 push-5" style="margin-left:0px;padding-left: 0px;">
				<input tabindex="19" type="text" class="form-control pas_tidak_dikenal" id="noidentitas" placeholder="No Identitas" name="noidentitas" value="{noidentitas}" required>
			</div>
		</div>
		
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="tempat_lahir">Tempat Lahir</label>
			<div class="col-md-8">
				<input tabindex="9" type="text" class="form-control pas_tidak_dikenal" id="tempat_lahir" placeholder="Tempat Lahir" name="tempat_lahir" value="{tempat_lahir}" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="golongan_darah">Golongan Darah</label>
			<div class="col-md-8">
				<select tabindex="23" id="golongan_darah" name="golongan_darah" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(11) as $row){?>
					<option value="<?=$row->id?>" <?=($golongan_darah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
					
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="agama">Agama</label>
			<div class="col-md-8">
				<select tabindex="24" id="agama_id" name="agama_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(3) as $row){?>
					<option value="<?=$row->id?>" <?=($agama_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
					
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="telepon">Telepon Rumah</label>
			<div class="col-md-8">
				<input tabindex="21" type="text" class="form-control" id="telepon" placeholder="Telepon Rumah" name="telepon" value="{telepon}">
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="email">Email</label>
			<div class="col-md-8">
				<input tabindex="22" type="text" class="form-control" id="email" placeholder="Email" name="email" value="{email}" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="suku">Suku</label>
			<div class="col-md-8">
				<select tabindex="26" id="suku_id" name="suku_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(4) as $row){?>
					<option value="<?=$row->id?>" <?=($suku_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
					
				</select>
				<input type="hidden" class="form-control" id="suku" placeholder="Nama Penanggung Jawab" name="suku" value="{suku}" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="pekerjaan">Pekerjaan</label>
			<div class="col-md-8">
				<select tabindex="28" id="pekerjaan" name="pekerjaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(6) as $row){?>
					<option value="<?=$row->id?>" <?=($pekerjaan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="bahasa">Bahasa Yang Dikuasai </label>
			<div class="col-md-8">
				<select tabindex="23" id="bahasa" name="bahasa[]" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Bahasa" multiple>
					
				</select>
			</div>
		</div>
	</div>
	
</div>
<div class="row">
	<div class="col-md-6"><!--ALAMAT DOMISISIL 1-->
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-12 control-label text-primary"><h5 align="left"><b>ALAMAT DOMISILI</b></h5></label>
			<div class="col-md-12">
				<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
			</div>
		</div>
		
		<div class="form-group" >
			<label class="col-md-4 control-label" for="alamat">Alamat<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<textarea tabindex="13" class="form-control pas_tidak_dikenal" id="alamatpasien" placeholder="Alamat" name="alamatpasien" required><?= $alamatpasien?></textarea>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 0px;">
			<div class="col-md-4"></div>
			<label class="col-md-3 control-label" for="provinsi_id"><p align="left">Provinsi</p></label>
			<div class="col-md-5">
				<?php if ($id == null) {?>
				<select tabindex="14" name="provinsi_id" id="provinsi_id" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="" selected>Pilih Opsi</option>
						<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
						<option value="<?= $a->id; ?>" <?= ($a->id == '12') ? 'selected' : ''; ?>><?= $a->nama; ?></option>
					<?php endforeach; ?>
					</select>
				<?php } else {?>
				<select tabindex="14" name="provinsi_id" id="provinsi_id" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
						<option value="<?= $a->id; ?>" <?=($provinsi_id == $a->id ? 'selected="selected"' : '')?>><?= $a->nama; ?></option>
					<?php endforeach; ?>
					</select>
				<?php }?>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<div class="col-md-4"></div>
			<label class="col-md-3" for="kabupaten_id"><left>Kabupaten/Kota </left></label>
			<div class="col-md-5">
				<select tabindex="15" name="kabupaten_id" id="kabupaten_id" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
				<?if ($provinsi_id){?>
					<?php foreach (get_all('mfwilayah', ['parent_id' => $provinsi_id]) as $a):?>
						<option value="<?= $a->id; ?>" <?=($kabupaten_id == $a->id ? 'selected="selected"' : '')?>><?= $a->nama; ?></option>
					<?php endforeach; ?>
				<?}?>
				</select>
			</div>
		</div>
		
		<div class="form-group" style="margin-bottom: 10px;">
			<div class="col-md-4"></div>
			<label class="col-md-3 control-label" for="kecamatan_id" style="text-align:left;">Kecamatan</label>
			<div class="col-md-5">
				
				<select tabindex="16" name="kecamatan_id" id="kecamatan_id" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Kecamatan" required>
					<?if ($kecamatan_id){?>
						<?php foreach (get_all('mfwilayah', ['parent_id' => $kabupaten_id]) as $a):?>
							<option value="<?= $a->id; ?>" <?=($kecamatan_id == $a->id ? 'selected="selected"' : '')?>><?= $a->nama; ?></option>
						<?php endforeach; ?>
					<?}?>
				</select>
				
			</div>
		</div>
		
		<div class="form-group" style="margin-bottom: 10px;">
			<div class="col-md-4"></div>
			<label class="col-md-3 control-label" for="kelurahan_id" style="text-align:left;">Kelurahan</label>
			<div class="col-md-5">
				<select tabindex="17" name="kelurahan_id" id="kelurahan_id" class="js-select2 form-control pas_tidak_dikenal" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<?if ($kelurahan_id){?>
						<?php foreach (get_all('mfwilayah', ['parent_id' => $kecamatan_id]) as $a):?>
							<option value="<?= $a->id; ?>" <?=($kelurahan_id == $a->id ? 'selected="selected"' : '')?>><?= $a->nama; ?></option>
						<?php endforeach; ?>
					<?}?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<div class="col-md-4"></div>
			<label class="col-md-3 control-label" for="kodepos" style="text-align:left;">Kode Pos</label>
			<div class="col-md-5">
				<input tabindex="18" type="text" class="form-control pas_tidak_dikenal" id="kodepos" placeholder="Kode Pos" name="kodepos" value="{kodepos}" readonly="true" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 0px;">
			<div class="col-md-4"></div>
			<label class="col-md-3 control-label" for="rw"><p align="left">RW</p></label>
			<div class="col-md-5">
				
				<input type="text" class="form-control pas_tidak_dikenal" id="rw" name="rw" value="{rw}" placeholder="RW" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 0px;">
			<div class="col-md-4"></div>
			<label class="col-md-3 control-label" for="rt"><p align="left">RT</p></label>
			<div class="col-md-5">
				
				<input type="text" class="form-control pas_tidak_dikenal" id="rt" name="rt" value="{rt}" placeholder="RT" required>
			</div>
		</div>
		<hr>
		<input type="hidden" id="status_cari" name="status_cari" value="0">
		
		
	</div>
	<div class="col-md-6"><!--DATA ALAMAT KARTU IDENTITAS-->
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label text-primary"><h5 align="left"><b>ALAMAT KARTU IDENTITIAS</b></h5></label>
			<div class="col-md-8">
				<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
					<input type="checkbox" id="chk_st_domisili"  name="chk_st_domisili" <?=($chk_st_domisili?'checked':'')?>><span></span> Sama dengan Domisili
				</label>
			</div>
			<div class="col-md-12">
				<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
			</div>
		</div>
		<div class="form-group" >
			<label class="col-md-4 control-label" for="alamat">Alamat<span style="color:red;">*</span></label>
			<div class="col-md-8">
				<textarea tabindex="13"   class="form-control" id="alamat_jalan_ktp" placeholder="Alamat" name="alamat_jalan_ktp" required><?= $alamat_jalan_ktp?></textarea>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 0px;">
			<div class="col-md-4"></div>
			<label class="col-md-3 control-label" for="provinsi"><p align="left">Provinsi</p></label>
			<div class="col-md-5">
				
				<select tabindex="14" name="provinsi_id_ktp"   id="provinsi_id_ktp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
						<option value="<?= $a->id; ?>" <?=($provinsi_id_ktp == $a->id ? 'selected="selected"' : '')?>><?= $a->nama; ?></option>
					<?php endforeach; ?>
					</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<div class="col-md-4"></div>
			<label class="col-md-3" for="kabupaten_id_ktp"><left>Kabupaten/Kota</left></label>
			<div class="col-md-5">
				
				<select tabindex="15" name="kabupaten_id_ktp"   id="kabupaten_id_ktp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</<option>
						<?php foreach (get_all('mfwilayah', ['parent_id' => $provinsi_id_ktp]) as $r):?>
						<option value="<?= $r->id; ?>" <?=($kabupaten_id_ktp == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
						<?php endforeach; ?>
					</select>
			</div>
		</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label"   for="kecamatan_id_ktp" style="text-align:left;">Kecamatan</label>
				<div class="col-md-5">
					
					<select tabindex="16" name="kecamatan_id_ktp"   id="kecamatan_id_ktp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required >
						<option value="">Pilih Opsi</<option>
						<?php foreach (get_all('mfwilayah', ['parent_id' => $kabupaten_id_ktp]) as $r):?>
						<option value="<?= $r->id; ?>"<?=($kecamatan_id_ktp == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label" for="kelurahan_id_ktp" style="text-align:left;">Kelurahan</label>
				<div class="col-md-5">
					<select tabindex="17" name="kelurahan_id_ktp"   id="kelurahan_id_ktp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</<option>
						<?php foreach (get_all('mfwilayah', ['parent_id' => $kecamatan_id_ktp]) as $r):?>
						<option value="<?= $r->id; ?>"<?=($kelurahan_id_ktp == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label" for="kodepos_ktp" style="text-align:left;">Kode Pos</label>
				<div class="col-md-5">
					<input tabindex="18" type="text" class="form-control"   id="kodepos_ktp" placeholder="Kode Pos" name="kodepos_ktp" value="{kodepos_ktp}" readonly="true" required>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 0px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label" for="rw_ktp"><p align="left">RW</p></label>
				<div class="col-md-5">
					
					<input type="text" class="form-control" id="rw_ktp" name="rw_ktp" value="{rw_ktp}" placeholder="RW" required>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 0px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label" for="rt_ktp"><p align="left">RT</p></label>
				<div class="col-md-5">
					
					<input type="text" class="form-control" id="rt_ktp" name="rt_ktp" value="{rt_ktp}" placeholder="RT" required>
				</div>
			</div>
		
	</div>
	
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="hubungan"></label>
			<label class="col-md-9 control-label text-primary"><h5 align="left"><b>DATA PENANGGUNG JAWAB</b></h5></label>
			<div class="col-md-12">
				<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="namapenanggungjawab">Nama</label>
			<div class="col-md-8">
				<input type="text" class="form-control pas_tidak_dikenal div_pj" id="namapenanggungjawab" placeholder="Nama Penanggung Jawab" name="namapenanggungjawab" value="{namapenanggungjawab}" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="hubungan">Hubungan</label>
			<div class="col-md-8">
				<select tabindex="30" id="hubunganpenanggungjawab" name="hubunganpenanggungjawab" class="js-select2 form-control pas_tidak_dikenal div_pj" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(9) as $row){?>
					<option value="<?=$row->id?>" <?=($hubunganpenanggungjawab == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
				</select>
				
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-md-4 control-label" for="tgllahir">Tanggal Lahir<span style="color:red;">*</span></label>
			<div class="col-md-3  col-xs-12 push-5">
				<select tabindex="10" name="tgl_lahirpenanggungjawab" id="tgl_lahirpenanggungjawab" class="js-select2 form-control div_pj_select" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="0">Hari</option>
					<?php for ($i = 1; $i < 32; $i++) {?>
						<?php $hari = str_pad($i, 2, '0', STR_PAD_LEFT); ?>
						<option value="<?= $hari; ?>" <?=($tgl_lahirpenanggungjawab == $hari ? 'selected="selected"' : '')?>><?= $hari; ?></option>
					<?php }?>
				</select>
			</div>
			<div class="col-md-2  col-xs-12 push-5">
				<select tabindex="11" name="bulan_lahirpenanggungjawab" id="bulan_lahirpenanggungjawab" class="js-select2 form-control div_pj_select" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="00">Bulan</option>
					<option value="01" <?=($bulan_lahirpenanggungjawab == '01' ? 'selected="selected"' : '')?>>Januari</option>
					<option value="02" <?=($bulan_lahirpenanggungjawab == '02' ? 'selected="selected"' : '')?>>Februari</option>
					<option value="03" <?=($bulan_lahirpenanggungjawab == '03' ? 'selected="selected"' : '')?>>Maret</option>
					<option value="04" <?=($bulan_lahirpenanggungjawab == '04' ? 'selected="selected"' : '')?>>April</option>
					<option value="05" <?=($bulan_lahirpenanggungjawab == '05' ? 'selected="selected"' : '')?>>Mei</option>
					<option value="06" <?=($bulan_lahirpenanggungjawab == '06' ? 'selected="selected"' : '')?>>Juni</option>
					<option value="07" <?=($bulan_lahirpenanggungjawab == '07' ? 'selected="selected"' : '')?>>Juli</option>
					<option value="08" <?=($bulan_lahirpenanggungjawab == '08' ? 'selected="selected"' : '')?>>Agustus</option>
					<option value="09" <?=($bulan_lahirpenanggungjawab == '09' ? 'selected="selected"' : '')?>>September</option>
					<option value="10" <?=($bulan_lahirpenanggungjawab == '10' ? 'selected="selected"' : '')?>>Oktober</option>
					<option value="11" <?=($bulan_lahirpenanggungjawab == '11' ? 'selected="selected"' : '')?>>November</option>
					<option value="12" <?=($bulan_lahirpenanggungjawab == '12' ? 'selected="selected"' : '')?>>Desember</option>
				</select>
			</div>
			<div class="col-md-3  col-xs-12 push-5" >
				<select tabindex="12" name="tahun_lahirpenanggungjawab" id="tahun_lahirpenanggungjawab" class="js-select2 form-control div_pj_select" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="0">Tahun</option>
					<?php for ($i = date('Y'); $i >= 1900; $i--) {?>
						<option value="<?= $i; ?>" <?=($tahun_lahirpenanggungjawab == $i ? 'selected="selected"' : '')?>><?= $i; ?></option>
					<?php }?>
				</select>
			</div>
		</div>
		<div class="form-group " >
			<label class="col-md-4 col-xs-12 control-label" for="umur">Umur</label>
			<div class="col-md-3 col-xs-12 push-5">
				<input type="text" class="form-control div_pj_select" id="umur_tahunpenanggungjawab" name="umur_tahunpenanggungjawab" value="{umur_tahunpenanggungjawab}" placeholder="Tahun" readonly="true" required>
			</div>
			<div class="col-md-2 col-xs-12 push-5">
				<input type="text" class="form-control div_pj_select" id="umur_bulanpenanggungjawab" name="umur_bulanpenanggungjawab" value="{umur_bulanpenanggungjawab}" placeholder="Bulan" readonly="true" required>
			</div>
			<div class="col-md-3 col-xs-12 push-5">
				<input type="text" class="form-control div_pj_select" id="umur_haripenanggungjawab" name="umur_haripenanggungjawab" value="{umur_haripenanggungjawab}" placeholder="Hari" readonly="true" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="pekerjaan">Pekerjaan</label>
			<div class="col-md-8">
				<select tabindex="28" id="pekerjaannpenanggungjawab" name="pekerjaannpenanggungjawab" class="js-select2 form-control div_pj_select" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(6) as $row){?>
					<option value="<?=$row->id?>" <?=($pekerjaannpenanggungjawab == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="pendidikanpenanggungjawab">Pendidikan</label>
			<div class="col-md-8">
				<select tabindex="27" id="pendidikanpenanggungjawab" name="pendidikanpenanggungjawab" class="js-select2 form-control div_pj_select" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(13) as $row){?>
					<option value="<?=$row->id?>" <?=($pendidikanpenanggungjawab == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="agama">Agama</label>
			<div class="col-md-8">
				<select tabindex="24" id="agama_idpenanggungjawab" name="agama_idpenanggungjawab" class="js-select2 form-control div_pj_select" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(3) as $row){?>
					<option value="<?=$row->id?>" <?=($agama_idpenanggungjawab == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
					
				</select>
			</div>
		</div>
		
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="alamatpenanggungjawab">Alamat</label>
			<div class="col-md-8">
				<textarea tabindex="31" class="form-control pas_tidak_dikenal div_pj" id="alamatpenanggungjawab" placeholder="Alamat Penanggung Jawab" name="alamatpenanggungjawab"><?= $alamatpenanggungjawab?></textarea>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 0px;">
			<div class="col-md-4"></div>
			<label class="col-md-3 control-label" for="provinsi_idpenanggungjawab"><p align="left">Provinsi</p></label>
			<div class="col-md-5">
				<?php if ($id == null) {?>
				<select tabindex="14" name="provinsi_idpenanggungjawab" id="provinsi_idpenanggungjawab" class="js-select2 form-control div_pj_select" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
						<option value="<?= $a->id; ?>" <?= ($a->id == '12') ? 'selected' : ''; ?>><?= $a->nama; ?></option>
					<?php endforeach; ?>
					</select>
				<?php } else {?>
				<select tabindex="14" name="provinsi_idpenanggungjawab" id="provinsi_idpenanggungjawab" class="js-select2 form-control div_pj_select" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
						<option value="<?= $a->id; ?>" <?=($provinsi_idpenanggungjawab == $a->id ? 'selected="selected"' : '')?>><?= $a->nama; ?></option>
					<?php endforeach; ?>
					</select>
				<?php }?>
			</div>
		</div>
		<div id="kabupaten_id" style="display: block;">
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-4"></div>
				<label class="col-md-3" for="kabupaten_idpenanggungjawab"><left>Kabupaten/Kota</left> </label>
				<div class="col-md-5">
					<?php if ($id == '') {?>
					<select tabindex="15" name="kabupaten_idpenanggungjawab" id="kabupaten_idpenanggungjawab" class="js-select2 form-control div_pj_select" style="width: 100%;" data-placeholder="Pilih Opsi" required>
							<option value="">Pilih Opsi</<option>
							<?php foreach (get_all('mfwilayah', ['parent_id' => '12']) as $r):?>
							<option value="<?= $r->id; ?>" <?= ($r->id == $kabupaten_idpenanggungjawab) ? 'selected' : ''; ?>><?= $r->nama; ?></<option>
							<?php endforeach; ?>
						</select>
					<?php } else {?>
					<select tabindex="15" name="kabupaten_idpenanggungjawab" id="kabupaten_idpenanggungjawab" class="js-select2 form-control div_pj_select" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</<option>
							<?php foreach (get_all('mfwilayah', ['parent_id' => $provinsi_id]) as $r):?>
							<option value="<?= $r->id; ?>" <?=($kabupaten_idpenanggungjawab == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
							<?php endforeach; ?>
						</select>
					<?php }?>
				</div>
			</div>
		</div>
		<div id="kecamatan" style="display: block;">
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label" for="kecamatan_idpenanggungjawab" style="text-align:left;">Kecamatan<?=$kecamatan_idpenanggungjawab?></label>
				<div class="col-md-5">
					<?php if ($kecamatan_idpenanggungjawab == '') {?>
					<select tabindex="16" name="kecamatan_idpenanggungjawab" id="kecamatan_idpenanggungjawab" class="js-select2 form-control div_pj_select" style="width: 100%;" data-placeholder="Pilih Kecamatan" required>
						<option value="">Pilih Opsi</<option>
						<?php foreach (get_all('mfwilayah', ['parent_id' => '213']) as $r):?>
						<option value="<?= $r->id; ?>"><?= $r->nama; ?></<option>
						<?php endforeach; ?>
					</select>
					<?php } else {?>
					<select tabindex="16" name="kecamatan_idpenanggungjawab" id="kecamatan_idpenanggungjawab" class="js-select2 form-control div_pj_select" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</<option>
						<?php foreach (get_all('mfwilayah', ['parent_id' => $kabupaten_idpenanggungjawab]) as $r):?>
						<option value="<?= $r->id; ?>"<?=($kecamatan_idpenanggungjawab == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
						<?php endforeach; ?>
					</select>
					<?php }?>
				</div>
			</div>
		</div>
		
		<div id="kelurahan" style="display: block;">
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label" for="kelurahan_idpenanggungjawab" style="text-align:left;">Kelurahan</label>
				<div class="col-md-5">
					<select tabindex="17" name="kelurahan_idpenanggungjawab" id="kelurahan_idpenanggungjawab" class="js-select2 form-control div_pj_select" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</<option>
						<?php foreach (get_all('mfwilayah', ['parent_id' => $kecamatan_idpenanggungjawab]) as $r):?>
						<option value="<?= $r->id; ?>"<?=($kelurahan_idpenanggungjawab == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label" for="kodepos" style="text-align:left;">Kode Pos</label>
				<div class="col-md-5">
					<input tabindex="18" type="text" class="form-control div_pj_select" id="kodepospenanggungjawab" placeholder="Kode Pos" name="kodepospenanggungjawab" value="{kodepospenanggungjawab}" readonly="true" required>
				</div>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 0px;">
			<div class="col-md-4"></div>
			<label class="col-md-3 control-label" for="rwpenanggungjawab"><p align="left">RW</p></label>
			<div class="col-md-5">
				
				<input type="text" class="form-control div_pj_select" id="rwpenanggungjawab" name="rwpenanggungjawab" value="{rwpenanggungjawab}" placeholder="RW" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 0px;">
			<div class="col-md-4"></div>
			<label class="col-md-3 control-label" for="rtpenanggungjawab"><p align="left">RT</p></label>
			<div class="col-md-5">
				
				<input type="text" class="form-control div_pj_select" id="rtpenanggungjawab" name="rtpenanggungjawab" value="{rtpenanggungjawab}" placeholder="RT" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="teleponpenanggungjawab">Telepon</label>
			<div class="col-md-8">
				<input tabindex="32" type="text" class="form-control div_pj_select div_pj" id="teleponpenanggungjawab" placeholder="Telepon" name="teleponpenanggungjawab" value="{teleponpenanggungjawab}">
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 20px;">
			<label class="col-md-4 control-label" for="noidentitaspenanggungjawab">No Identitas</label>
			<div class="col-md-8">
				<input tabindex="33" type="text" class="form-control div_pj_select div_pj" id="noidentitaspenanggungjawab" placeholder="No Identitas Penanggung Jawab" name="noidentitaspenanggungjawab" value="{noidentitaspenanggungjawab}" required>
				
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4  text-success h5" for="namapengantar"><strong>DATA PENGANTAR</strong></label>
			<div class="col-md-5">
				<select tabindex="28" id="chk_st_pengantar" name="chk_st_pengantar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<option value="0" <?=($chk_st_pengantar ==0 ? 'selected="selected"' : '')?>>Tidak Sama Dengan Penanggungjawab</option>
					<option value="1" <?=($chk_st_pengantar ==1 ? 'selected="selected"' : '')?>>Sama Penanggungjawab</option>
				</select>
				
			</div>
		</div>
		<hr style="margin-top: 10px;margin-bottom: 15px;background: #000;">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="namapengantar">Nama </label>
			<div class="col-md-8">
				<input type="text" class="form-control div_data_pengantar"   id="namapengantar" placeholder="Nama Pengantar" name="namapengantar" value="<?=$namapengantar?>" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="hubunganpengantar">Hubungan</label>
			<div class="col-md-8">
				<select tabindex="30" id="hubunganpengantar" name="hubunganpengantar"   class="js-select2 form-control div_data_pengantar" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(9) as $row){?>
					<option value="<?=$row->id?>" <?=($hubunganpengantar == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label" for="tgllahir">Tanggal Lahir<span style="color:red;">*</span></label>
			<div class="col-md-3  col-xs-12 push-5">
				<select tabindex="10" name="tgl_lahirpengantar" id="tgl_lahirpengantar" class="js-select2 form-control div_data_pengantar" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="0">Hari</option>
					<?php for ($i = 1; $i < 32; $i++) {?>
						<?php $hari = str_pad($i, 2, '0', STR_PAD_LEFT); ?>
						<option value="<?= $hari; ?>" <?=($tgl_hari == $hari ? 'selected="selected"' : '')?>><?= $hari; ?></option>
					<?php }?>
				</select>
			</div>
			<div class="col-md-2  col-xs-12 push-5">
				<select tabindex="11" name="bulan_lahirpengantar" id="bulan_lahirpengantar" class="js-select2 form-control div_data_pengantar" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="00">Bulan</option>
					<option value="01" <?=($tgl_bulan == '01' ? 'selected="selected"' : '')?>>Januari</option>
					<option value="02" <?=($tgl_bulan == '02' ? 'selected="selected"' : '')?>>Februari</option>
					<option value="03" <?=($tgl_bulan == '03' ? 'selected="selected"' : '')?>>Maret</option>
					<option value="04" <?=($tgl_bulan == '04' ? 'selected="selected"' : '')?>>April</option>
					<option value="05" <?=($tgl_bulan == '05' ? 'selected="selected"' : '')?>>Mei</option>
					<option value="06" <?=($tgl_bulan == '06' ? 'selected="selected"' : '')?>>Juni</option>
					<option value="07" <?=($tgl_bulan == '07' ? 'selected="selected"' : '')?>>Juli</option>
					<option value="08" <?=($tgl_bulan == '08' ? 'selected="selected"' : '')?>>Agustus</option>
					<option value="09" <?=($tgl_bulan == '09' ? 'selected="selected"' : '')?>>September</option>
					<option value="10" <?=($tgl_bulan == '10' ? 'selected="selected"' : '')?>>Oktober</option>
					<option value="11" <?=($tgl_bulan == '11' ? 'selected="selected"' : '')?>>November</option>
					<option value="12" <?=($tgl_bulan == '12' ? 'selected="selected"' : '')?>>Desember</option>
				</select>
			</div>
			<div class="col-md-3  col-xs-12 push-5" >
				<select tabindex="12" name="tahun_lahirpengantar" id="tahun_lahirpengantar" class="js-select2 form-control div_data_pengantar" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="0">Tahun</option>
					<?php for ($i = date('Y'); $i >= 1900; $i--) {?>
						<option value="<?= $i; ?>" <?=($tahun_lahirpengantar == $i ? 'selected="selected"' : '')?>><?= $i; ?></option>
					<?php }?>
				</select>
			</div>
		</div>
		<div class="form-group " >
			<label class="col-md-4 col-xs-12 control-label" for="umur">Umur</label>
			<div class="col-md-3 col-xs-12 push-5">
				<input type="text" class="form-control div_data_pengantar" id="umur_tahunpengantar" name="umur_tahunpengantar" value="{umur_tahunpengantar}" placeholder="Tahun" readonly="true" required>
			</div>
			<div class="col-md-2 col-xs-12 push-5">
				<input type="text" class="form-control div_data_pengantar" id="umur_bulanpengantar" name="umur_bulanpengantar" value="{umur_bulanpengantar}" placeholder="Bulan" readonly="true" required>
			</div>
			<div class="col-md-3 col-xs-12 push-5">
				<input type="text" class="form-control div_data_pengantar" id="umur_haripengantar" name="umur_haripengantar" value="{umur_haripengantar}" placeholder="Hari" readonly="true" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="pekerjaan">Pekerjaan</label>
			<div class="col-md-8">
				<select tabindex="28" id="pekerjaannpengantar" name="pekerjaannpengantar" class="js-select2 form-control div_data_pengantar" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(6) as $row){?>
					<option value="<?=$row->id?>" <?=($pekerjaannpengantar == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="pendidikanpengantar">Pendidikan</label>
			<div class="col-md-8">
				<select tabindex="27" id="pendidikanpengantar" name="pendidikanpengantar" class="js-select2 form-control div_data_pengantar" style="width: 100%;" data-placeholder="Pilih Opsi" required>
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(13) as $row){?>
					<option value="<?=$row->id?>" <?=($pendidikanpengantar == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
				</select>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="agama">Agama</label>
			<div class="col-md-8">
				<select tabindex="24" id="agama_idpengantar" name="agama_idpengantar" class="js-select2 form-control div_data_pengantar" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(3) as $row){?>
					<option value="<?=$row->id?>" <?=($agama_idpengantar == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
					
				</select>
			</div>
		</div>
		
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="alamatpengantar">Alamat</label>
			<div class="col-md-8">
				<textarea tabindex="31" class="form-control div_data_pengantar div_pj" id="alamatpengantar" placeholder="Alamat Pengantar" name="alamatpengantar"><?= $alamatpengantar?></textarea>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 0px;">
			<div class="col-md-4"></div>
			<label class="col-md-3 control-label" for="provinsi_idpengantar"><p align="left">Provinsi</p></label>
			<div class="col-md-5">
				<?php if ($id == null) {?>
				<select tabindex="14" name="provinsi_idpengantar" id="provinsi_idpengantar" class="js-select2 form-control div_data_pengantar" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
						<option value="<?= $a->id; ?>" <?= ($a->id == '12') ? 'selected' : ''; ?>><?= $a->nama; ?></option>
					<?php endforeach; ?>
					</select>
				<?php } else {?>
				<select tabindex="14" name="provinsi_idpengantar" id="provinsi_idpengantar" class="js-select2 form-control div_data_pengantar" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mfwilayah', ['jenis' => '1']) as $a):?>
						<option value="<?= $a->id; ?>" <?=($provinsi_idpengantar == $a->id ? 'selected="selected"' : '')?>><?= $a->nama; ?></option>
					<?php endforeach; ?>
					</select>
				<?php }?>
			</div>
		</div>
		<div id="kabupaten_id" style="display: block;">
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-4"></div>
				<label class="col-md-3" for="kabupaten_idpengantar"><left>Kabupaten/Kota</left> </label>
				<div class="col-md-5">
					<?php if ($id == '') {?>
					<select tabindex="15" name="kabupaten_idpengantar" id="kabupaten_idpengantar" class="js-select2 form-control div_data_pengantar" style="width: 100%;" data-placeholder="Pilih Opsi" required>
							<option value="">Pilih Opsi</<option>
							<?php foreach (get_all('mfwilayah', ['parent_id' => '12']) as $r):?>
							<option value="<?= $r->id; ?>" <?= ($r->id == $kabupaten_idpengantar) ? 'selected' : ''; ?>><?= $r->nama; ?></<option>
							<?php endforeach; ?>
						</select>
					<?php } else {?>
					<select tabindex="15" name="kabupaten_idpengantar" id="kabupaten_idpengantar" class="js-select2 form-control div_data_pengantar" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</<option>
							<?php foreach (get_all('mfwilayah', ['parent_id' => $provinsi_id]) as $r):?>
							<option value="<?= $r->id; ?>" <?=($kabupaten_idpengantar == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
							<?php endforeach; ?>
						</select>
					<?php }?>
				</div>
			</div>
		</div>
		<div id="kecamatan" style="display: block;">
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label" for="kecamatan_idpengantar" style="text-align:left;">Kecamatan</label>
				<div class="col-md-5">
					<?php if ($id == '') {?>
					<select tabindex="16" name="kecamatan_idpengantar" id="kecamatan_idpengantar" class="js-select2 form-control div_data_pengantar" style="width: 100%;" data-placeholder="Pilih Kecamatan" required>
						<option value="">Pilih Opsi</<option>
						<?php foreach (get_all('mfwilayah', ['parent_id' => '213']) as $r):?>
						<option value="<?= $r->id; ?>"><?= $r->nama; ?></<option>
						<?php endforeach; ?>
					</select>
					<?php } else {?>
					<select tabindex="16" name="kecamatan_idpengantar" id="kecamatan_idpengantar" class="js-select2 form-control div_data_pengantar" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</<option>
						<?php foreach (get_all('mfwilayah', ['parent_id' => $kabupaten_idpengantar]) as $r):?>
						<option value="<?= $r->id; ?>"<?=($kecamatan_idpengantar == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
						<?php endforeach; ?>
					</select>
					<?php }?>
				</div>
			</div>
		</div>
		
		<div id="kelurahan" style="display: block;">
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label" for="kelurahan_idpengantar" style="text-align:left;">Kelurahan</label>
				<div class="col-md-5">
					<select tabindex="17" name="kelurahan_idpengantar" id="kelurahan_idpengantar" class="js-select2 form-control div_data_pengantar" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</<option>
						<?php foreach (get_all('mfwilayah', ['parent_id' => $kecamatan_idpengantar]) as $r):?>
						<option value="<?= $r->id; ?>"<?=($kelurahan_idpengantar == $r->id ? 'selected="selected"' : '')?>><?= $r->nama; ?></<option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<div class="col-md-4"></div>
				<label class="col-md-3 control-label" for="kodepos" style="text-align:left;">Kode Pos</label>
				<div class="col-md-5">
					<input tabindex="18" type="text" class="form-control div_data_pengantar" id="kodepospengantar" placeholder="Kode Pos" name="kodepospengantar" value="{kodepospengantar}" readonly="true" required>
				</div>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 0px;">
			<div class="col-md-4"></div>
			<label class="col-md-3 control-label" for="rwpengantar"><p align="left">RW</p></label>
			<div class="col-md-5">
				
				<input type="text" class="form-control div_data_pengantar" id="rwpengantar" name="rwpengantar" value="{rwpengantar}" placeholder="RW" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 0px;">
			<div class="col-md-4"></div>
			<label class="col-md-3 control-label" for="rtpengantar"><p align="left">RT</p></label>
			<div class="col-md-5">
				
				<input type="text" class="form-control div_data_pengantar" id="rtpengantar" name="rtpengantar" value="{rtpengantar}" placeholder="RT" required>
			</div>
		</div>
		
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="telepon">Telepon</label>
			<div class="col-md-8">
				<input tabindex="32" type="text" class="form-control div_data_pengantar"   id="teleponpengantar" placeholder="Telepon" name="teleponpengantar" value="{teleponpengantar}" required>
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 20px;">
			<label class="col-md-4 control-label" for="noidentitaspengantar">No Identitas</label>
			<div class="col-md-8">
				<input tabindex="33" type="text"   class="form-control div_data_pengantar" id="noidentitaspengantar" placeholder="No Identitas Pengantar" name="noidentitaspengantar" value="{noidentitaspengantar}" required>
				
			</div>
		</div>
		
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-8 control-label text-danger"><h5 align="left"><b>INFORMASI PASIEN TIDAK DIKENAL</b></h5></label>
			<div class="col-md-4">
				
			</div>
			<div class="col-md-12">
				<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
			</div>
		</div>
		<div class="form-group form_tidak_dikenal" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="nama_keluarga_td">Nama Keluarga</label>
			<div class="col-md-8">
				<input type="text" class="form-control div_tidak_dikenal"   id="nama_keluarga_td" placeholder="Nama Keluarga" name="nama_keluarga_td" value="{nama_keluarga_td}" >
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="hubungan">Hubungan</label>
			<div class="col-md-8">
				<select tabindex="30" id="hubungan_td" name="hubungan_td" class="js-select2 form-control pas_tidak_dikenal div_pj" style="width: 100%;" data-placeholder="Pilih Opsi" >
					<option value="">Pilih Opsi</option>
					<?foreach(list_variable_ref(9) as $row){?>
					<option value="<?=$row->id?>" <?=($hubungan_td == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
					<?}?>
				</select>
				
			</div>
		</div>
		<div class="form-group form_tidak_dikenal" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="hubungan_pengantar">Alamat</label>
			<div class="col-md-8">
				<input type="text" class="form-control div_tidak_dikenal"   id="alamat_td" placeholder="Alamat" name="alamat_td" value="{alamat_td}" >
				
			</div>
		</div>
		
	</div>
	<div class="col-md-6">
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-12 control-label"><h5 align="left"><b>Data Lain Lain</b></h5></label>
			<div class="col-md-12">
				<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
			</div>
		</div>
		<div class="form-group" style="margin-bottom: 10px;">
			<label class="col-md-4 control-label" for="catatan">Catatan</label>
			<div class="col-md-8">
				<textarea tabindex="46" class="form-control" rows="4" id="catatan" placeholder="Catatan" name="catatan"><?= $catatan?></textarea>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="block block-themed">
			<div class="block-header bg-info">
				<ul class="block-options">
					
					<li>
						<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="fa fa-address-book-o"></i> KONTAK TAMBAHAN &nbsp;&nbsp;&nbsp; <button type="button" onclick="tambah_kontak()" class="btn btn-xs btn-success" title="Tambah Kontak Tambahan"><i class="fa fa-plus"></i></button></h3>
			</div>
			<div class="block-content">
					<div hidden>
						<table>
							<tr class="copied">
								<td>
									<select tabindex="23" name="level_kontak[]" class="div_select2  form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
										<option value="2" selected>Secondary</option>
										<option value="1">Primary</option>
									</select>
								</td>
								<td>
									<select tabindex="23" name="jenis_kontak[]" class="div_select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
										<option value="">Pilih Opsi</option>
										<?foreach(list_variable_ref(20) as $row){?>
										<option value="<?=$row->id?>" <?=($golongan_darah == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
										<?}?>
									</select>
											
								</td>
								<td>
									<input type="text" class="form-control" placeholder="Kontak" name="nama_kontak[]" value=""  >
									<input type="hidden" name="id_kontak_tambahan[]" value="">
								</td>
								<td>
									<div class="btn-group">
										<button class="btn btn-xs btn-danger" onclick="hapus_kontak(this)" type="button" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-trash"></i></button>
									</div>
								</td>
							</tr>
						</table>
					</div>
					<div class="pasted">
						<table id="pasted" class="table table-bordered">
							<thead>
								<tr>
								<th class="text-center">Primary / Secondary</th>
								<th class="text-center">Jenis Kontak</th>
								<th class="text-center">Kontak</th>
								<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								
								
								<?foreach($list_kontak as $r_kontak){
							$jenis_kontak_id=$r_kontak->jenis_kontak;
							$level_kontak_id=$r_kontak->level_kontak;
							?>
							<tr>
								<td>
										<select tabindex="23" name="level_kontak[]" class="js_select2  form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
											<option value="2" <?=($level_kontak_id=='2'?'selected':'')?>>Secondary</option>
											<option value="1" <?=($level_kontak_id=='1'?'selected':'')?>>Primary</option>
										</select>
								</td>
								<td>
										<select tabindex="23" name="jenis_kontak[]" class="js_select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
											<option value="">Pilih Opsi</option>
											<?foreach(list_variable_ref(20) as $row){?>
											<option value="<?=$row->id?>" <?=($jenis_kontak_id == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
											<?}?>
											
										</select>
								</td>
								<td>
										<input type="text" class="form-control" placeholder="Kontak" name="nama_kontak[]" value="<?=$r_kontak->nama_kontak?>"  >
										<input type="hidden" name="id_kontak_tambahan[]" value="<?=$r_kontak->id?>">
								</td>
								<td>
									<div class="btn-group">
										<button class="btn btn-xs btn-danger" onclick="hapus_kontak_id(this,<?=$r_kontak->id?>)" type="button" data-toggle="tooltip" title="" data-original-title="Remove"><i class="fa fa-trash"></i></button>
									</div>
								</td>
							</tr>
							
							<?}?>
							</tbody>
						</table>
						
					</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="block block-themed">
			<div class="block-header bg-info">
				<ul class="block-options">
					
					<li>
						<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="fa fa-history"></i> Kunjungan Terakhir &nbsp;&nbsp;&nbsp;</h3>
			</div>
			<div class="block-content">
					<div class="form-group " id="div_history" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label text-primary"><h5 align="left"><b>Kunjungan Terakhir</b></h5> </label> 
						<div class="col-md-6 historykunjungan">
							<button class="btn btn-minw btn-rounded btn-noborder btn-success push-5 btn-xs" type="button" onclick="show_history_kunjungan()"><i class="fa fa-history"></i> Lihat</button>
						</div>
						
						<div class="col-md-12 historykunjungan">
								<div class="block">
									<div class="block-content">
										<ul class="list list-activity push">
											<li>
												<i class="si si-pencil text-primary"></i>
												<div class="font-w600" id="his_poli">
													Poliklinik - OPSIONAL
												</div>
												<div id="div_nama_dokter"><a href="javascript:void(0)"><i class="fa fa-user-md text-info"></i> <span  id="his_dokter">NAMA DOKTER</span></a></div>
												<div><small class="text-muted" id="his_tanggal">12-08-2023</small></div>
											</li>
										</ul>
									</div>
								</div>
						</div>
					</div>
					
			</div>
		</div>
	</div>
</div>
<?if($id!=''){?>
<div class="block-content block-content-mini block-content-full border-t">
<div class="row">
	<div class="form-group" style="margin-bottom: 10px;">
	<div class="col-md-12 text-right">
		<a href="{base_url}tpendaftaran_ranap" class="btn btn-sm btn-default" name="btn_simpan" value="2" type="submit" ><i class="fa fa-reply"></i> Kembali</a>
		<button class="btn btn-sm btn-primary " type="submit" name="btn_simpan"  value="1"><i class="fa fa-save"></i> Update</button>			
		<button class="btn btn-sm btn-danger " type="submit" name="btn_simpan"  value="2"><i class="fa fa-save"></i> Update & Keluar</button>		
	</div>
	</div>
</div>
</div>
<?}?>