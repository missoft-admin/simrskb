<div class="modal" id="modal_kartu" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width:50%">
        <form class="form-horizontal" action="javascript:void(0)" id="frm1">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">ADD Kartu</h3>
                    </div>
                    <div class="block-content">
						<div class="block">
						<ul class="nav nav-tabs" data-toggle="tabs" id="tabs">
							<li class="active" id="tab_list">
								<a href="#tab_list_kartu"><i class="fa fa-list-ol"></i> List Kartu</a>
							</li>
							<li id="tab_add">
								<a href="#tab_add_kartu"><i class="fa fa-pencil"></i> Add / Edit Kartu</a>
							</li>
							
						</ul>
						<div class="block-content tab-content">
							<div class="tab-pane fade fade-left in active" id="tab_list_kartu">
								<h5 class="font-w300 push-15" id="lbl_list_kartu_add">Dafar Kartu</h5>
								<table class="table table-bordered table-striped" id="index_kartu_add">
                                    <thead>
                                        <th>NO</th>
                                        <th>Asuransi</th>
                                        <th>No Kartu</th>
                                        <th>Nama Participant</th>
                                        <th>Status</th>
                                        <th>Tools</th>
                                    </thead>
                                    <tbody></tbody>
                                  
                                </table>
							</div>
							<div class="tab-pane fade fade-left" id="tab_add_kartu">
								<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
								<div class="row">
									<div class="col-md-12">
										
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="tipeid">Tipe</label>
											<div class="col-md-10">
												<select tabindex="34" id="kelompokpasien_kartu_add" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
													<option value="" selected>Pilih Opsi</option>
													<?php foreach ($list_cara_bayar as $r):?>
													<?if ($r->id!='5'){?>
													<option value="<?= $r->id; ?>" ><?= $r->nama; ?></option>
													<?}?>
													<?php endforeach; ?>
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;" id="div_rekanan_kartu_add">
											<label class="col-md-2 control-label" for="tipeid">Asuransi</label>
											<div class="col-md-10">
												<select tabindex="35"  id="idrekanan_kartu_add" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
													<option value="" selected>Pilih Opsi</option>
													<?php foreach ($list_asuransi as $r) {?>
													<option value="<?= $r->id?>" ><?= $r->nama?></option>
													<?php }?>
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="nokartu">No. Kartu</label>
											<div class="col-md-10">
												<input class="form-control" type="text" id="nokartu2" >	
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="nama_partisipant">Atas Nama</label>
											<div class="col-md-10">
												<input class="form-control" type="text" id="nama_partisipant" >	
												<input class="form-control" type="hidden" id="id_kartu" >	
											</div>
										</div>
										
									</div>
									<div class="col-md-12">
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-2 control-label" for="idprespektif"></label>
											<div class="col-md-10">
												<button class="btn btn-success text-uppercase" type="button" name="btn_save_kartu_add" id="btn_save_kartu_add"><i class="fa fa-save"></i> Simpan</button>
												<button class="btn btn-warning text-uppercase" type="button" id="btn_clear_kartu_add" name="btn_clear_kartu_add" ><i class="fa fa-refresh"></i> Clear</button>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_close() ?>
							</div>
							
						</div>
					</div>
                        
                                     
                  </div>
                </div>
                <div class="modal-footer" style="margin-top:20px">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade in black-overlay" id="modal_upload_foto_kartu" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title"><i class="fa fa-file-image-o"></i> Upload Foto</h3>
				</div>
				<div class="block-content">
					<div class="col-md-12" style="margin-top: 10px;">
						
						<div class="block">
							<form action="{base_url}tpendaftaran_poli/upload_kartu" enctype="multipart/form-data" class="dropzone" id="image-upload">
								<input type="hidden" class="form-control" name="id_foto_add" id="id_foto_add" value="" readonly>
								<div>
								  <h5>Bukti Foto Kartu</h5>
								</div>
							</form>
						</div>
						
					</div> 
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
			
		</div>
	</div>
</div>

<script type="text/javascript">
var myDropzone ;
	
	$(document).ready(function(){
		Dropzone.autoDiscover = false;
		myDropzone = new Dropzone(".dropzone", { 
		   autoProcessQueue: true,
		   maxFilesize: 30,
		});
		myDropzone.on("complete", function(file) {
		  
		  LoadKartu();
		  myDropzone.removeFile(file);
		  $("#modal_upload_foto_kartu").modal('hide');
		});
	})	
$("#kelompokpasien_kartu_add").change(function(){
	if ($("#kelompokpasien_kartu_add").val()=='1'){//asuransi
		document.getElementById('div_rekanan_kartu_add').style.display = 'block';
	}else{
		document.getElementById('div_rekanan_kartu_add').style.display = 'none';
		
	}
});
function upload_kartu(id){
	$("#id_foto_add").val(id);
	$("#modal_upload_foto_kartu").modal('show');
}
$("#btn_clear_kartu_add").click(function(){
	clear_kartu_add();
});
$("#btn_save_kartu_add").click(function(){
	save_kartu_add();
});
function edit_kartu(id){
	clear_kartu_add();
	$("#cover-spin").show();
	$("#id_kartu").val(id);
	$('[href="#tab_add_kartu"]').tab('show');
	$.ajax({
			url: '{site_url}tpendaftaran_poli/load_kartu',
			type: 'POST',
			dataType: 'json',
			data: {
				id: id,
				
			},
			success: function(data) {
				$("#nokartu2").val(data.nokartu);
					$("#kelompokpasien_kartu_add").val(data.kelompokpasien).trigger('change');
					$("#idrekanan_kartu_add").val(data.idrekanan).trigger('change');
					$("#nama_partisipant").val(data.nama_partisipant);
				// location.reload();
				$("#cover-spin").hide();
				// table.ajax.reload( null, false ); 
			}
		});
}
function pilih_kartu(id){
	$("#cover-spin").show();
	$("#kartu_id").val(id);
	$('[href="#tab_add_kartu"]').tab('show');
	$.ajax({
			url: '{site_url}tpendaftaran_poli/load_kartu',
			type: 'POST',
			dataType: 'json',
			data: {
				id: id,
				
			},
			success: function(data) {
				console.log(data);
					$("#kelompokpasien").val(data.kelompokpasien).trigger('change');
					$("#idrekanan").val(data.idrekanan).trigger('change');
					$("#nokartu").val(data.nokartu);
					$("#nama_kartu").val(data.nama_partisipant);
					
					 var img = document.getElementById('foto_kartu_view');
					img.src = data.url_foto;
					document.getElementById('url_foto').href = data.url_foto;
					
				$("#cover-spin").hide();
				$("#modal_kartu").modal('hide');
			}
		});
}
function hapus_kartu(id){
	// clear_kartu_add();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menghapus ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
	$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_poli/hapus_kartu',
			type: 'POST',
			dataType: 'json',
			data: {
				id: id,
			},
			success: function(data) {
				swal({
					title: "Berhasil!",
					text: "Data diproses.",
					type: "success",
					timer: 1500,
					showConfirmButton: false
				});
					$("#cover-spin").hide();

				LoadKartu();
			}
		});
	});

	
	
}
function aktifkan_kartu(id){
	// clear_kartu_add();
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Aktifkan ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
	$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_poli/aktifkan_kartu',
			type: 'POST',
			dataType: 'json',
			data: {
				id: id,
			},
			success: function(data) {
				swal({
					title: "Berhasil!",
					text: "Data diproses.",
					type: "success",
					timer: 1500,
					showConfirmButton: false
				});
					$("#cover-spin").hide();

				LoadKartu();
			}
		});
	});

	
	
}
function LoadKartu() {
		var idpasien=$("#idpasien").val();
		
		$('#index_kartu_add').DataTable().destroy();
		var table = $('#index_kartu_add').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}tpendaftaran_poli/LoadKartu/',
			type: "POST",
			dataType: 'json',
			data: {
				idpasien: idpasien,
				
			}
		},
		columnDefs: [
					 {  className: "text-center", targets:[0,4] },
					 { "width": "5%", "targets": [0] },
					 { "width": "10%", "targets": [2,3,4] },
					 { "width": "15%", "targets": [1,5] },

					]
		});
	}
function clear_kartu_add(){
	$("#id_kartu").val('')
	$("#kelompokpasien_kartu_add").val('').trigger('change');
	$("#idrekanan_kartu_add").val('').trigger('change');
	$("#nokartu2").val('');
	$("#nama_partisipant").val('');
}
	function save_kartu_add(){
		

		var id_kartu=$("#id_kartu").val();		
		var idpasien=$("#idpasien").val();		
		var kelompokpasien=$("#kelompokpasien_kartu_add").val();		
		var idrekanan=$("#idrekanan_kartu_add").val();		
		var nokartu=$("#nokartu2").val();	
		// alert(nokartu);
// return false;		
		var nama_partisipant=$("#nama_partisipant").val();		
		if (kelompokpasien==''){
			sweetAlert("Maaf...", "Tentukan Tipe", "error");
			return false;

		}
		if (kelompokpasien=='1'){
			if (idrekanan==''){
			sweetAlert("Maaf...", "Tentukan Asuransi", "error");
			return false;
				
			}

		}
		if (nokartu==''){
			sweetAlert("Maaf...", "Tentukan No Kartu", "error");
			return false;

		}
		if (nama_partisipant==''){
			sweetAlert("Maaf...", "Tentukan Nama Participant", "error");
			return false;

		}
		
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_poli/save_kartu',
			type: 'POST',
			data: {
				id_kartu: id_kartu,
				idpasien: idpasien,
				kelompokpasien: kelompokpasien,
				idrekanan: idrekanan,
				nokartu: nokartu,
				nama_partisipant: nama_partisipant,
			},
			success: function(data) {
				// $.toaster({priority : 'success', title : 'Succes!', message : ' Update Pembayaran'});
				if (data==false){
					swal({
							title: "Gagal!",
							text: "Data tidak diproses.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});
					
				}else{
					clear_kartu_add();
						$('[href="#tab_list_kartu"]').tab('show');
						LoadKartu();
						swal({
							title: "Berhasil!",
							text: "Data diproses.",
							type: "success",
							timer: 1500,
							showConfirmButton: false
						});
					
				}
				// location.reload();
				$("#cover-spin").hide();
				// table.ajax.reload( null, false ); 
			}
		});
	
	}
</script>