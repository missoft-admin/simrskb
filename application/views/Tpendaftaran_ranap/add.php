<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<style>
	#sig canvas{
		width: 100% !important;
		height: auto;
	}
</style>
<style>
    body {
      -webkit-print-color-adjust: exact;
    }
	
	
   table {
		font-family: Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;
		 
      }
	  td {
        padding: 13px;
		 
      }
      .content td {
        padding: 10px;
		border: 0px solid #6033FF;
      }
	  .has-error2 {
		border-color: #d26a5c;
	}
	  .select2-selection {
		  border-color: green; /* example */
		}
      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 14px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-bottom-top-left {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
		border-bottom:1px solid #000 !important;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
	
    </style>
	<style>
	.text-muted {
	  color: #919191;
	  font-weight: normal;
	  font-style: italic;
	  
	}
.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
</style>
<div class="block">
<?if ($id==''){
	$nama_class="inactive";
	?>
  <div class="js-wizard-classic-validation block">

<?}else{
	$nama_class="";
}
	?>
		<!-- Step Tabs -->
		<ul class="nav nav-tabs" data-toggle="tabs">
			<li class="<?=($tab=='1'?'active':'')?>">
				<a class="<?=$nama_class?>" href="#validation-classic-step1" data-toggle="tab"> <i class="si si-user-follow"></i> Pilih Pasien </a>
			</li>
			<li class="<?=($tab=='2'?'active':'')?>">
				<a class="<?=$nama_class?>" href="#validation-classic-step2" data-toggle="tab"><i class="fa fa-calendar-check-o"></i> Tentukan Pendaftaran </a>
			</li>
			
			<?if($st_lm=='1'){?>
			<li class="<?=($tab=='3'?'active':'')?>">
				<a class="<?=$nama_class?>"  href="#validation-classic-step3" data-toggle="tab"><i class="fa fa-file-text-o"></i>  Lembar Masuk & Keluar <?=($st_lembar?'&nbsp;&nbsp;<span class="badge badge-success pull-right"> Sudah </span>':'&nbsp;&nbsp;<span class="badge badge-danger pull-right"> Belum </span>')?></a>
			</li>
			<?}?>
			<?if($st_sp=='1' && $st_lembar=='1'){?>
			<li class="<?=($tab=='4'?'active':'')?>">
				<a class="<?=$nama_class?>"  href="#validation-classic-step4" data-toggle="tab"><i class="fa fa-file-text-o"></i>  Surat Pernyataan <?=($st_pernyataan?'&nbsp;&nbsp;<span class="badge badge-success pull-right"> Sudah </span>':'&nbsp;&nbsp;<span class="badge badge-danger pull-right"> Belum </span>')?></a>
			</li>
			<?}?>
			<?if($st_gc=='1' && $st_pernyataan=='1'){?>
			<li class="<?=($tab=='5'?'active':'')?>">
				<a class="<?=$nama_class?>"  href="#validation-classic-step5" data-toggle="tab"><i class="fa fa-file-text-o"></i>  General Consent <?=($st_general?'&nbsp;&nbsp;<span class="badge badge-success pull-right"> Sudah </span>':'&nbsp;&nbsp;<span class="badge badge-danger pull-right"> Belum </span>')?></a>
			</li>
			<?}?>
			<?if($st_hk=='1' && $st_general=='1'){?>
			<li class="<?=($tab=='6'?'active':'')?>">
				<a class="<?=$nama_class?>"  href="#validation-classic-step6" data-toggle="tab"><i class="fa fa-file-text-o"></i>  Hak & Kewajiban <?=($st_hak?'&nbsp;&nbsp;<span class="badge badge-success pull-right"> Sudah </span>':'&nbsp;&nbsp;<span class="badge badge-danger pull-right"> Belum </span>')?></a>
			</li>
			<?}?>
			
			
		</ul>
			<!-- Steps Content -->
			<?php echo form_open_multipart('tpendaftaran_ranap/save_register_poli', 'class="js-form1 validation form-horizontal" id="form1" ') ?>
			
			<div class="block-content tab-content">
				<!-- Step 1 -->
				<input type="hidden" id="id" name="id" value="{id}">
				<input type="hidden" id="perencanaan_id" name="perencanaan_id" value="{perencanaan_id}">
				<input type="hidden" id="nama_tabel" name="nama_tabel" value="">
				<input type="hidden" id="pendaftaran_poli_id" name="pendaftaran_poli_id" value="{pendaftaran_poli_id}">
				<input type="hidden" id="st_lm" name="st_lm" value="{st_lm}">
				<input type="hidden" id="st_sp" name="st_sp" value="{st_sp}">
				<input type="hidden" id="st_gc" name="st_gc" value="{st_gc}">
				<input type="hidden" id="st_hk" name="st_hk" value="{st_hk}">
				<input type="hidden" id="idruangan_default" name="idruangan_default" value="{idruangan_default}">
				<input type="hidden" id="idkelas_default" name="idkelas_default" value="{idkelas_default}">
				<input type="hidden" id="idbed_default" name="idbed_default" value="{idbed_default}">
				<input type="hidden" id="treservasi_bed_id" name="treservasi_bed_id" value="{treservasi_bed_id}">
				<div class="tab-pane push-5-t push-50 <?=($tab=='1'?'active':'')?>" id="validation-classic-step1">
					<?$this->load->view('Tpendaftaran_ranap/tab_pasien')?>
					
				</div>
				<!-- END Step 1 -->

				<!-- Step 2 -->
				<div class="tab-pane push-5-t push-50 <?=($tab=='2'?'active':'')?>" id="validation-classic-step2">
					<?$this->load->view('Tpendaftaran_ranap/tab_pendaftaran')?>
				</div>
				<?if ($id !=''){?>
				<div class="tab-pane push-5-t push-50 <?=($tab=='3'?'active':'')?>" id="validation-classic-step3">
					<?$this->load->view('Tpendaftaran_ranap/tab_lm')?>
				</div>
				<div class="tab-pane push-10-t push-50 <?=($tab=='4'?'active':'')?>" id="validation-classic-step4">					
					<?$this->load->view('Tpendaftaran_ranap/tab_sp')?>
				</div>
				
				<div class="tab-pane push-10-t push-50 <?=($tab=='5'?'active':'')?>" id="validation-classic-step5">					
					<?$this->load->view('Tpendaftaran_ranap/tab_gc')?>
					
				</div>
				
				<div class="tab-pane push-10-t push-50 <?=($tab=='6'?'active':'')?>" id="validation-classic-step6">					
					<?$this->load->view('Tpendaftaran_ranap/tab_hk')?>
				</div>
				<?}?>
				<!-- END Step 3 -->
			</div>
			
			<!-- END Steps Content -->
			<?if ($id==''){?>
			<!-- Steps Navigation -->
			<div class="block-content block-content-mini block-content-full border-t">
				<div class="row">
					<div class="col-xs-6">
						<button class="wizard-prev btn btn-default" type="button"><i class="fa fa-arrow-left"></i> Previous</button>
					</div>
					<div class="col-xs-6 text-right">
						<button class="wizard-next btn btn-success" type="button">Next <i class="fa fa-arrow-right"></i></button>
						<button class="wizard-finish btn btn-primary" type="submit" value="1" name="btn_simpan"><i class="fa fa-check"></i> Submit</button>
					
					</div>
				</div>
			</div>
			<?}?>
		<?php echo form_close() ?>
		<!-- END Form -->
		<?if ($id==''){?>
			</div>
		<?}?>
</div>
<div class="modal in" id="modal_paraf" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Paraf Pasien</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<input type="hidden" readonly id="ttd_id" name="ttd_id" value="">
								<input type="hidden" readonly id="nama_tabel" name="nama_tabel" value="">
								<textarea id="signature64" name="signed" style="display: none"></textarea>
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" id="btn_save_paraf"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Penanggung Jawab </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig_2" ></div>
									</div>
								</div>
								<textarea id="signature64_2" name="signed_2" style="display: none"></textarea>
							</div>
							<input type="hidden" readonly id="ttd_id_2" name="ttd_id_2" value="{id}">
							<input type="hidden" readonly id="nama_id_ttd" value="">
							<input type="hidden" readonly id="nama_tabel_ttd" value="">
							<input type="hidden" readonly id="nama_field_ttd" value="">
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear_2"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" id="btn_save_ttd"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modal_pendaftaran" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog modal-lg" >
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Kunjungan Poliklinik</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal push-10-t">
					<div class="row">
						<div class="col-md-6">                               
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="notransaksi_2" placeholder="No Pendaftaran" name="notransaksi" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Pasien</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="cari_poli" placeholder="No Medrec | Nama Pasien"  value="">
								</div>
							</div>
						</div>
						<div class="col-md-6">                               
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
								<div class="col-md-9">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggal_21" name="tanggal_1" placeholder="From" value="{tanggal_1}"/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_22" name="tanggal_2" placeholder="To" value="{tanggal_2}"/>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal"></label>
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="button" onclick="getIndexPoli()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
								</div>
							</div>
						</div>
						<div class="col-md-12">                               
							<div class="table-responsive">
								<table class="table table-bordered" id="index_poli">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="8%">Action</th>
											<th width="10%">Pendaftaran</th>
											<th width="10%">Tanggal Kunjungan</th>
											<th width="8%">No Medrec</th>
											<th width="20%">Nama Pasien</th>
											<th width="15%">Dokter</th>
											<th width="10%">Asal Pasien</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
                    </div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="modal_perencanaan" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog modal-lg" >
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Perencanaan Rawat Inap</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal push-10-t">
					<div class="row">
						<div class="col-md-6">                               
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">No Perencanaan</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="nopermintaan_filter" placeholder="No Perencanaan" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Pencarian </label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="cari_rencana" placeholder="Nama Pasien | No. Medrec" value="">
								</div>
							</div>
						</div>
						<div class="col-md-6">                               
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Tanggal Rencana Masuk</label>
								<div class="col-md-9">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggal_11" name="tanggal_11" placeholder="From" value="{tanggal_1}"/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_12" name="tanggal_12" placeholder="To" value="{tanggal_2}"/>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal"></label>
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="button" onclick="getIndexRencana()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
								</div>
							</div>
						</div>
						<div class="col-md-12">                               
							<div class="table-responsive">
								<table class="table table-bordered" id="index_rencana">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="8%">Action</th>
											<th width="10%">No Perencanaan</th>
											<th width="10%">Rencana Pelayanan</th>
											<th width="8%">No Pendaftaran</th>
											<th width="15%">Nama Pasien</th>
											<th width="10%">Dokter</th>
											<th width="10%">Asal Pasien</th>
											<th width="10%">Dengan Diagnosa</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
                    </div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="modal_history_ranap" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog modal-lg" >
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Perencanaan Rawat Inap</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal push-10-t">
					<div class="row">
						<div class="col-md-12">                               
							<div class="table-responsive">
								<table class="table table-bordered" id="index_his_ranap">
									<thead>
										<tr>
											<th width="10%">Dirawat Ke-</th>
											<th width="10%">Tanggal Daftar</th>
											<th width="10%">No Pendaftaran</th>
											<th width="10%">No Perencanaan</th>
											<th width="15%">Diagnosa</th>
											<th width="15%">Dokter Perujuk</th>
											<th width="15%">DPJP</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
                    </div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<?$this->load->view('Tpendaftaran_poli/modal_history')?>
<?$this->load->view('Tbooking/modal_pasien')?>
<script src="{js_path}plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{js_path}pages/base_forms_wizard.js"></script>
<?$this->load->view('Tpendaftaran_ranap/modal_add_kartu')?>
<script type="text/javascript">

var sumber_depan;
var table;
var idpasien;
var st_copy;
var st_load_manual;
var st_skrining=$("#st_skrining").val();
var st_general=$("#st_general").val();
var st_covid=$("#st_covid").val();
$(document).ready(function(){	
	st_load_manual=false;
	idpasien= $('#idpasien').val();
	// alert(idpasien);
	$(".time-datepicker").datetimepicker({
		format: "HH:mm:ss"
	});
	clear_dpjp();
	show_hide_button();
	
	$("#chk_st_domisili").prop("checked", true);
	$("#tgl_lahirpenanggungjawab").val($("#tgl_lahirpenanggungjawab").val()).trigger('change');
	
	if (idpasien){
		get_last_kunjungan(idpasien);
		load_bahasa(idpasien);
		get_logic_pendaftaran();
	}
	if ($("#id").val()!=''){
		
		get_logic_pendaftaran();
		load_index_lm();
		load_index_gc();
		load_index_hk();
		
		$(".js_select2").select2("");
		set_asal_rujukan();
		set_kelompok_pasien();
		
	}else{
		if (idpasien){
			
		}else{
			$("#provinsi_id").val($("#provinsi_id").val()).trigger('change');
			$("#provinsi_id_ktp").val($("#provinsi_id").val()).trigger('change.select2');
		}
		
	}
	$("#st_dpjp_pendamping").val($("#st_dpjp_pendamping").val()).trigger('change');
	// get_data_perencanaan(18);
	set_asal_rujukan();
})	
function show_modal_pendaftaran(){
	$("#modal_pendaftaran").modal('show');
	getIndexPoli();
}
$("#st_dpjp_pendamping").change(function() {
	let st_dpjp_pendamping=$("#st_dpjp_pendamping").val();
	if (st_dpjp_pendamping=='0'){
		$(".div_dpjp").hide();
	}else{
		$(".div_dpjp").show();
	}
	
});
function show_modal_perencanaan(){
	$("#modal_perencanaan").modal('show');
	getIndexRencana();
}
function getIndexRencana() {
   
    $("#cover-spin").show();
    // alert(ruangan_id);
    $('#index_rencana').DataTable().destroy();
    table = $('#index_rencana').DataTable({
		 autoWidth: false,
            searching: true,
            pageLength: 10,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			 "columnDefs": [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,4,5,6,7] },
						 { "width": "5%", "targets": [0]},
						
					],
            ajax: { 
                url: '{site_url}tpendaftaran_ranap/getIndexRencanaCari',
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_1:$("#tanggal_31").val(),
						tanggal_2:$("#tanggal_32").val(),
						nopermintaan:$("#nopermintaan_filter").val(),
						cari_rencana:$("#cari_rencana").val(),
						idpasien:$("#idpasien").val(),
						
					   }
            },
			"drawCallback": function(settings) {
				$("#cover-spin").hide();
			}
			
    });
    $("#cover-spin").hide();
}
function getIndexPoli() {
    $("#cover-spin").show();
    // alert(ruangan_id);
    $('#index_poli').DataTable().destroy();
    table = $('#index_poli').DataTable({
		 autoWidth: false,
            searching: true,
            pageLength: 10,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			 "columnDefs": [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,4,5,6,7] },
						 { "width": "5%", "targets": [0]},
						
					],
            ajax: { 
                url: '{site_url}tpendaftaran_ranap/getIndexPoliCari',
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_1:$("#tanggal_21").val(),
						tanggal_2:$("#tanggal_22").val(),
						notransaksi:$("#notransaksi_2").val(),
						cari_poli:$("#cari_poli").val(),
						idpasien:$("#idpasien").val(),
						
					   }
            },
			"drawCallback": function(settings) {
				$("#cover-spin").hide();
			}
			
    });
    $("#cover-spin").hide();
}
function show_dirawat_ke(){
	$("#modal_history_ranap").modal('show');
	getIndexHisRanap();
}
function getIndexHisRanap() {
	let idpasien=$("#idpasien").val();
    $("#cover-spin").show();
    // alert(ruangan_id);
    $('#index_his_ranap').DataTable().destroy();
    table = $('#index_his_ranap').DataTable({
		 autoWidth: false,
            searching: true,
            pageLength: 10,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			 "columnDefs": [
						// {  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,4,5,6,0] },
						 // { "width": "5%", "targets": [0]},
						
					],
            ajax: { 
                url: '{site_url}tpendaftaran_ranap/getIndexHisRanap',
                type: "POST" ,
                dataType: 'json',
				data : {
						idpasien:idpasien
					   }
            },
			"drawCallback": function(settings) {
				$("#cover-spin").hide();
			}
			
    });
    $("#cover-spin").hide();
}
function get_data_pendaftaran(id){
	$("#cover-spin").show();
	$("#modal_pendaftaran").modal('hide');
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_data_pendaftaran/',
		dataType: "json",
		type: "POST",
		data:{
			id:id,
		},
		success: function(data) {
			$("#cover-spin").hide();
			get_set_detai_data(data);
			// $("#pendaftaran_id").val(id);
			$(".idtipe_asal").val(data.idtipe).trigger('change');
			$(".nopendaftaran_poli").val(data.nopendaftaran);
			$(".iddokter_perujuk").val(data.iddokter).trigger('change');
			$(".idpoliklinik_asal").val(data.idpoliklinik).trigger('change');
			// $("#nama_dokter").val(data.nama_dokter);
			// $("#asal_poli").val(data.asal_poli);
		}
	});
}
function get_data_perencanaan(id){
	$("#cover-spin").show();
	$("#modal_perencanaan").modal('hide');
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_data_perencanaan/',
		dataType: "json",
		type: "POST",
		data:{
			id:id,
		},
		success: function(data) {
			$("#cover-spin").hide();
			get_set_detai_data(data);
			$("#perencanaan_id").val(id);
			$(".nopermintaan").val(data.nopermintaan);
			$(".diagnosa").val(data.diagnosa);
			$(".idtipe_asal").val(data.idtipe).trigger('change');
			// $(".nopendaftaran_poli").val(data.nopendaftaran);
			$(".nopendaftaran_poli").val('');
			$(".iddokter_perujuk").val(data.iddokter).trigger('change');
			$(".idpoliklinik_asal").val(data.idpoliklinik).trigger('change');
			$("#idtipe").val(data.tipe).trigger('change.select2');
			$("#iddokterpenanggungjawab").val(data.iddokterpenanggungjawab).trigger('change.select2');
		}
	});
}
function add_dpjp(){
	if ($("#no_dpjp").val()==''){
		sweetAlert("Maaf...", "Tentukan No Urut", "error");
		return false;
	}
	if ($("#dpjp").val()==''){
		sweetAlert("Maaf...", "Tentukan Dokter", "error");
		return false;
	}
	let st_duplicate=false;
	$('#tabel_dpjp tbody tr').each(function() {
		let dpjp_id=$(this).find(".dpjp_id").val();
		if (dpjp_id==$("#dpjp").val()){
			st_duplicate=true;
		}
		
	});
	if (st_duplicate==true){
		sweetAlert("Maaf...", "Duplicate Dokter", "error");
		return false;
	}

	let nama_dokter=$("#dpjp option:selected").text();
	let button='<button class="btn btn-danger btn-sm" title="Hapus" onclick="hapus_dpjp(this)" type="button"><i class="fa fa-trash"></i></button>';
	let tabel='';
	tabel +="<tr>";
	tabel +='<td class="text-center"><input type="hidden" class="dpjp_id" name="dpjp_id[]" value="'+$("#dpjp").val()+'"><input type="hidden" name="no_dpjp[]" class="no_dpjp" value="'+$("#no_dpjp").val()+'">'+$("#no_dpjp").val()+'</td>';
	tabel +="<td>"+nama_dokter+"</td>";
	tabel +="<td class='text-center'>"+button+"</td>";
	tabel +="</tr>";
	$("#tabel_dpjp tbody").append(tabel);
	clear_dpjp();
}
function clear_dpjp(){
	var no_dpjp =parseFloat($('#tabel_dpjp tr').length)-1;
	$("#no_dpjp").val(no_dpjp).trigger('change.select2');
	$("#dpjp").val('').trigger('change.select2');
	
}
function hapus_dpjp(row){
	 row.closest("tr").remove();
	 clear_dpjp();
}
$("#idkelas,#idruangan").change(function() {
	let idruangan = $("#idruangan").val();
	let idkelas = $("#idkelas").val();
	let id = $("#id").val();
	// alert(idruangan)
	getRanapBed(idruangan, idkelas, id);
});
$("#idkelas_permintaan,#idruangan_permintaan").change(function() {
	let idruangan = $("#idruangan_permintaan").val();
	let idkelas = $("#idkelas_permintaan").val();
	let id = $("#id").val();
	// alert(idruangan)
	getRanapBedAll(idruangan, idkelas, id);
});
function getRanapBedAll(idruangan, idkelas, id) {
	$.ajax({
		url: "{site_url}tpendaftaran_ranap/getBedAll/",
		method: "POST",
		dataType: "json",
		data : {
			idruangan:idruangan,
			idkelas:idkelas,
			id:id,
			
		},
		success: function(data) {
			$("#idbed_permintaan").empty();
			$("#idbed_permintaan").append("<option value=''>Pilih Opsi</option>");
			data.map((item) => {
				$("#idbed_permintaan").append('<option value="'+item.id+'"> '+item.nama+'</option>');
			})
			$("#idbed_permintaan").selectpicker('refresh');
		}
	});
	
}
function getRanapBed(idruangan, idkelas, id) {
	$.ajax({
		url: "{site_url}tpendaftaran_ranap/getBed/",
		method: "POST",
		dataType: "json",
		data : {
			idruangan:idruangan,
			idkelas:idkelas,
			id:id,
			
		},
		success: function(data) {
			$("#idbed").empty();
			$("#idbed").append("<option value=''>Pilih Opsi</option>");
			data.map((item) => {
				$("#idbed").append('<option value="'+item.id+'" '+(item.id==='0'?'disabled':'')+'> '+item.nama+'</option>');
			})
			$("#idbed").selectpicker('refresh');
		}
	});
	
}
$("#idtipe_asal,#idpoliklinik_asal,#iddokterpenanggungjawab,#idkelompokpasien,#idrekanan,#idtipe").change(function() {
	get_logic_tipe_pasien();
});
function get_logic_tipe_pasien() {
	let asal_pasien=$("#idtipe_asal").val();
	let idpoli=$("#idpoliklinik_asal").val();
	let iddokter=$("#iddokterpenanggungjawab").val();
	let idkelompokpasien=$("#idkelompokpasien").val();
	let idrekanan=$("#idrekanan").val();
	let idtujuan=$("#idtipe").val();

	$.ajax({
		url: "{site_url}tpendaftaran_ranap/get_logic_tipe_pasien/",
		method: "POST",
		dataType: "json",
		data : {
			asal_pasien:asal_pasien,
			idpoli:idpoli,
			iddokter:iddokter,
			idkelompokpasien:idkelompokpasien,
			idrekanan:idrekanan,
			idtujuan:idtujuan,
			
		},
		success: function(data) {
			$("#idtipepasien").val(data.idtipepasien).trigger('change.select2');
			get_logic_pendaftaran();
		}
	});

}
function get_set_detai_data(data){
	console.log(data);
	let idpasien=data.idpasien;
	$("#dirawat_ke").val(data.dirawat_ke);
	$("#pendaftaran_poli_id").val(data.id);
	$("#idpasien").val(data.idpasien);
	$(".namapasien").val(data.namapasien);
	$("#namapasien").val(data.namapasien);
	$("#nik").val(data.nik);
	$("#umurhari").val(data.umurhari);
	$("#umurbulan").val(data.umurbulan);
	$("#umurtahun").val(data.umurtahun);
	$("#nama_ibu_kandung").val(data.nama_ibu_kandung);
	$("#email").val(data.email);
	$("#no_medrec").val(data.no_medrec);
	$("#nama_panggilan").val(data.nama_panggilan);
	$("#tempat_lahir").val(data.tempat_lahir);
	$("#nohp").val(data.nohp);
	$("#telprumah").val(data.telepon);
	$("#alamatpasien").val(data.alamatpasien);
	$("#rw").val(data.rw);
	$("#rt").val(data.rt);
	$("#kodepos").val(data.kodepos);
	$("#catatan").val(data.catatan);
	$("#noidentitas").val(data.noidentitas);
	$("#jenis_id").val(data.jenis_id).trigger('change.select2');
	$("#idasalpasien").val(data.idasalpasien).trigger('change');
	$("#idkelompokpasien").val(data.idkelompokpasien).trigger('change.select2');
	$("#idtipepasien").val(data.idtipepasien).trigger('change.select2');
	
	
	
	// $("#chk_st_domisili").val(data.chk_st_domisili);
	$("#warganegara").val(data.warganegara).trigger('change');
	$("#golongan_darah").val(data.golongan_darah).trigger('change');
	$("#agama_id").val(data.agama_id).trigger('change');
	$("#jenis_kelamin").val(data.jenis_kelamin).trigger('change');
	$("#pendidikan").val(data.pendidikan).trigger('change');
	$("#pekerjaan").val(data.pekerjaan).trigger('change');
	$("#statuskawin").val(data.statuskawin).trigger('change');
	$("#suku_id").val(data.suku_id).trigger('change');
	$("#bahasa").val(data.bahasa).trigger('change');
	var tanggalLahir = data.tanggal_lahir;
	var tahun = tanggalLahir.substr(0, 4);
	var bulan = tanggalLahir.substr(5, 2);
	var hari = tanggalLahir.substr(8, 2);
	// alert(bulan);
	// $('#tahun').val(tahun);
	$("#tahun").val(tahun).trigger('change');
	$("#bulan").val(bulan).trigger('change');
	$("#hari").val(hari).trigger('change');
	load_bahasa(idpasien);
	load_kontak(idpasien);
	get_last_kunjungan(idpasien);
	$("#provinsi_id").empty();
	$("#provinsi_id").append(data.list_provinsi);
	$("#kabupaten_id").empty();
	$("#kabupaten_id").append(data.list_kab);
	$("#kecamatan_id").empty();
	$("#kecamatan_id").append(data.list_kec);
	$("#kelurahan_id").empty();
	$("#kelurahan_id").append(data.list_desa);
	if (data.chk_st_domisili=='1'){
		$("#chk_st_domisili").prop("checked", true);
		$("#provinsi_id_ktp").empty();
		$("#provinsi_id_ktp").append(data.list_provinsi);
		$("#kabupaten_id_ktp").empty();
		$("#kabupaten_id_ktp").append(data.list_kab);
		$("#kecamatan_id_ktp").empty();
		$("#kecamatan_id_ktp").append(data.list_kec);
		$("#kelurahan_id_ktp").empty();
		$("#kelurahan_id_ktp").append(data.list_desa);
		$("#rw_ktp").val(data.rw);
		$("#rt_ktp").val(data.rt);
		$("#kodepos_ktp").val(data.kodepos);
		$("#alamat_jalan_ktp").val(data.alamatpasien);
	}else{
		$("#chk_st_domisili").prop("checked", false);
		$("#provinsi_id_ktp").append(data.list_provinsi);
		$("#kabupaten_id_ktp").empty();
		$("#kecamatan_id_ktp").empty();
		$("#kelurahan_id_ktp").empty();
	}
	$("#namapenanggungjawab").val(data.namapenanggungjawab);
	$("#teleponpenanggungjawab").val(data.teleponpenanggungjawab);
	$("#noidentitaspenanggungjawab").val(data.noidentitaspenanggungjawab);
	$("#alamatpenanggungjawab").val(data.alamatpenanggungjawab);
	$("#hubunganpenanggungjawab").val(data.hubungan).trigger('change.select2');
	$("#chk_st_pengantar").val(data.chk_st_pengantar).trigger('change.select2');
	if (data.chk_st_pengantar=='1'){
		$("#namapengantar").val(data.namapenanggungjawab);
		$("#teleponpengantar").val(data.teleponpenanggungjawab);
		$("#noidentitaspengantar").val(data.noidentitaspenanggungjawab);
		$("#alamatpengantar").val(data.alamatpenanggungjawab);
		$("#hubunganpengantar").val(data.hubungan).trigger('change.select2');
	}
}

function tambah_kontak(){
	var nextKontak = $(".copied").clone();
        // add corresponding classes (remove old first):
        nextKontak.removeClass('copied').addClass('new_kontak');
		console.log(nextKontak);
		nextKontak.find('select.div_select2').removeClass('div_select2').addClass('js_select2');
		// console.log(xselect2);		
		$('#pasted tbody').append(nextKontak);
		$(".js_select2").select2("");
	
}
function load_kontak(idpasien){
	$('#pasted tbody').empty();;
	$("#cover-spin").show();
	if (idpasien){
		$.ajax({
			url: "{site_url}tpendaftaran_ranap/load_kontak/" + idpasien,
			method: "POST",
			dataType: "json",
			success: function(data) {
				$('#pasted tbody').append(data);
				$(".js_select2").select2("");
				$("#cover-spin").hide();
			}
		});
		
	}
	
}
$("#provinsi_id").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_kota',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			$("#kabupaten_id").empty();
			$("#kecamatan_id").empty();
			$("#kelurahan_id").empty();
			$("#kodepos").val('');
			$("#kabupaten_id").append(data.list_data);
			if (check){
				$("#provinsi_id_ktp").val(kode).trigger('change.select2');
				$("#kabupaten_id_ktp").empty();
				$("#kecamatan_id_ktp").empty();
				$("#kelurahan_id_ktp").empty();
				$("#kodepos_ktp").val('');
				$("#kabupaten_id_ktp").append(data.list_data);
			}
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#kabupaten_id").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_kota',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			// alert(kode);
			$("#kecamatan_id").empty();
			$("#kelurahan_id").empty();
			$("#kodepos").val('');
			$("#kecamatan_id").append(data.list_data);
			if (check){
				$("#kabupaten_id_ktp").val(kode).trigger('change.select2');
				$("#kecamatan_id_ktp").empty();
				$("#kelurahan_id_ktp").empty();
				$("#kodepos_ktp").val('');
				$("#kecamatan_id_ktp").append(data.list_data);
			}
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#kecamatan_id").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_kota',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			$("#kelurahan_id").empty();
			$("#kodepos").val('');
			$("#kelurahan_id").append(data.list_data);
			if (check){
				$("#kecamatan_id_ktp").val(kode).trigger('change.select2');
				$("#kelurahan_id_ktp").empty();
				$("#kodepos_ktp").val('');
				$("#kelurahan_id_ktp").append(data.list_data);
			}
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#kelurahan_id").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/getKodepos',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			$("#kodepos").val(data.kodepos);
			if (check){
			$("#kelurahan_id_ktp").val(kode).trigger('change.select2');
				$("#kodepos_ktp").val(data.kodepos);
			}
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#provinsi_id_ktp").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_kota',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			$("#kabupaten_id_ktp").empty();
			$("#kecamatan_id_ktp").empty();
			$("#kelurahan_id_ktp").empty();
			$("#kodepos_ktp").val('');
			$("#kabupaten_id_ktp").append(data.list_data);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#kabupaten_id_ktp").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_kota',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			// alert(kode);
			$("#kecamatan_id_ktp").empty();
			$("#kelurahan_id_ktp").empty();
			$("#kodepos_ktp").val('');
			$("#kecamatan_id_ktp").append(data.list_data);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#kecamatan_id_ktp").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_kota',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			$("#kelurahan_id_ktp").empty();
			$("#kodepos_ktp").val('');
			$("#kelurahan_id_ktp").append(data.list_data);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#kelurahan_id_ktp").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/getKodepos',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			$("#kodepos_ktp").val(data.kodepos);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#provinsi_idpenanggungjawab").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_kota',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			$("#kabupaten_idpenanggungjawab").empty();
			$("#kecamatan_idpenanggungjawab").empty();
			$("#kelurahan_idpenanggungjawab").empty();
			$("#kodepospenanggungjawab").val('');
			$("#kabupaten_idpenanggungjawab").append(data.list_data);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#kabupaten_idpenanggungjawab").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_kota',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			// alert(kode);
			$("#kecamatan_idpenanggungjawab").empty();
			$("#kelurahan_idpenanggungjawab").empty();
			$("#kodepospenanggungjawab").val('');
			$("#kecamatan_idpenanggungjawab").append(data.list_data);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#kecamatan_idpenanggungjawab").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_kota',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			$("#kelurahan_idpenanggungjawab").empty();
			$("#kodepospenanggungjawab").val('');
			$("#kelurahan_idpenanggungjawab").append(data.list_data);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#kelurahan_idpenanggungjawab").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/getKodepos',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			$("#kodepospenanggungjawab").val(data.kodepos);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#provinsi_idpengantar").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_kota',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			$("#kabupaten_idpengantar").empty();
			$("#kecamatan_idpengantar").empty();
			$("#kelurahan_idpengantar").empty();
			$("#kodepospengantar").val('');
			$("#kabupaten_idpengantar").append(data.list_data);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#kabupaten_idpengantar").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_kota',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			// alert(kode);
			$("#kecamatan_idpengantar").empty();
			$("#kelurahan_idpengantar").empty();
			$("#kodepospengantar").val('');
			$("#kecamatan_idpengantar").append(data.list_data);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#kecamatan_idpengantar").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_kota',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			$("#kelurahan_idpengantar").empty();
			$("#kodepospengantar").val('');
			$("#kelurahan_idpengantar").append(data.list_data);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#kelurahan_idpengantar").change(function() {
	var kode = $(this).val();
	let check = $("#chk_st_domisili").is(":checked");
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/getKodepos',
		method: "POST",
		dataType: "json",
		data: {
			"kode": kode
		},
		success: function(data) {
			$("#kodepospengantar").val(data.kodepos);
			
		},
		error: function(data) {
			console.log(data);
		}
	});
});
$("#suku_id").change(function() {
	$("#suku").val($("#suku_id option:selected").text());
});
$("#tahun_lahirpenanggungjawab,#bulan_lahirpenanggungjawab,#tgl_lahirpenanggungjawab").change(function() {
	let tahun=$("#tahun_lahirpenanggungjawab").val();
	let bulan=$("#bulan_lahirpenanggungjawab").val();
	let hari=$("#tgl_lahirpenanggungjawab").val();
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_umur',
		method: "POST",
		dataType: "json",
		data: {
			"tahun": tahun,"bulan": bulan,"hari": hari,
		},
		success: function(data) {
			$("#umur_tahunpenanggungjawab").val(data.tahun);
			$("#umur_bulanpenanggungjawab").val(data.bulan);
			$("#umur_haripenanggungjawab").val(data.hari);
			
		}
	});
});
$("#tahun_lahirpengantar,#bulan_lahirpengantar,#tgl_lahirpengantar").change(function() {
	let tahun=$("#tahun_lahirpengantar").val();
	let bulan=$("#bulan_lahirpengantar").val();
	let hari=$("#tgl_lahirpengantar").val();
	// console.log('sini');
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_umur',
		method: "POST",
		dataType: "json",
		data: {
			"tahun": tahun,"bulan": bulan,"hari": hari,
		},
		success: function(data) {
			$("#umur_tahunpengantar").val(data.tahun);
			$("#umur_bulanpengantar").val(data.bulan);
			$("#umur_haripengantar").val(data.hari);
			
		}
	});
});
function set_sama_domisili(){
	$("#status_cari").val(1);
	$("#alamat_jalan_ktp").val($("#alamatpasien").val());
	
	console.log('functionsama');
	$("#rw_ktp").val($("#rw").val());
	$("#rt_ktp").val($("#rt").val());
	$("#kodepos_ktp").val($("#kodepos").val());
	
}
function set_beda_domisili(){
	// $("#alamat_jalan_ktp").val('');
	// $("#provinsi_id_ktp").val("").trigger('change.select2');
	// $("#kabupaten_id_ktp").empty();
	// $("#kecamatan_id_ktp").empty();
	// $("#kelurahan_id_ktp").empty();
	// $("#kodepos_ktp").val('');
	// $("#rw_ktp").val('');
	// $("#rt_ktp").val('');
}
$("#chk_st_pengantar").on("change", function(){
check =$("#chk_st_pengantar").val();
// alert(check);
	$("#chk_st_pengantar2").val(check).trigger('change');
    if(check=='1') {
        set_sama_pengantar();
		 
    } else {
        set_beda_pengantar();
    }
}); 
$("#hubunganpenanggungjawab").on("change", function(){
check =$("#chk_st_pengantar").val();
    if(check=='1') {
        set_sama_pengantar();
    } else {
        set_beda_pengantar();
    }
}); 
$(".div_pj_select").on("change", function(){
check =$("#chk_st_pengantar").val();
    if(check=='1') {
        set_sama_pengantar();
    } else {
        set_beda_pengantar();
    }
}); 

function set_sama_pengantar(){
	st_copy=true;
	$("#namapengantar").val($("#namapenanggungjawab").val());
	$("#hubunganpengantar").val($("#hubunganpenanggungjawab").val()).trigger('change.select2');
	$("#alamatpengantar").val($("#alamatpenanggungjawab").val());
	$("#teleponpengantar").val($("#teleponpenanggungjawab").val());
	$("#noidentitaspengantar").val($("#noidentitaspenanggungjawab").val());
	
	$("#tgl_lahirpengantar").val($("#tgl_lahirpenanggungjawab").val()).trigger('change');
	$("#bulan_lahirpengantar").val($("#bulan_lahirpenanggungjawab").val()).trigger('change');
	$("#tahun_lahirpengantar").val($("#tahun_lahirpenanggungjawab").val()).trigger('change');
	
	$("#umur_tahunpengantar").val($("#umur_tahunpenanggungjawab").val());
	$("#umur_bulanpengantar").val($("#umur_bulanpenanggungjawab").val());
	$("#umur_haripengantar").val($("#umur_haripenanggungjawab").val());
	// $("#noidentitaspengantar").val($("#noidentitaspenanggungjawab").val());
	
	$("#pekerjaannpengantar").val($("#pekerjaannpenanggungjawab").val()).trigger('change.select2');
	$("#pendidikanpengantar").val($("#pendidikanpenanggungjawab").val()).trigger('change.select2');
	$("#agama_idpengantar").val($("#agama_idpenanggungjawab").val()).trigger('change.select2');
	
	var $options = $("#provinsi_idpenanggungjawab > option").clone();
    $('#provinsi_idpengantar').empty();
    $('#provinsi_idpengantar').append($options);
    $('#provinsi_idpengantar').val($('#provinsi_idpenanggungjawab').val()).trigger('change.select2');
	
	var $options = $("#kabupaten_idpenanggungjawab > option").clone();
    $('#kabupaten_idpengantar').empty();
    $('#kabupaten_idpengantar').append($options);
    $('#kabupaten_idpengantar').val($('#kabupaten_idpenanggungjawab').val()).trigger('change.select2');
	
	var $options = $("#kecamatan_idpenanggungjawab > option").clone();
    $('#kecamatan_idpengantar').empty();
    $('#kecamatan_idpengantar').append($options);
    $('#kecamatan_idpengantar').val($('#kecamatan_idpenanggungjawab').val()).trigger('change.select2');
	
	var $options = $("#kelurahan_idpenanggungjawab > option").clone();
    $('#kelurahan_idpengantar').empty();
    $('#kelurahan_idpengantar').append($options);
    $('#kelurahan_idpengantar').val($('#kelurahan_idpenanggungjawab').val()).trigger('change.select2');
	$("#kodepospengantar").val($("#kodepospenanggungjawab").val());
	$("#rwpengantar").val($("#rwpenanggungjawab").val());
	$("#rtpengantar").val($("#rtpenanggungjawab").val());
	
	
	
	$(".div_data_pengantar").attr('disabled',true);
	st_copy=false;
}
function set_beda_pengantar(){
	// $("#namapengantar").val('');
	// $("#hubunganpengantar").val("").trigger('change');
	// $("#alamatpengantar").val('');
	// $("#teleponpengantar").val('');
	// $("#noidentitaspengantar").val('');
	$(".div_data_pengantar").removeAttr('disabled');
}
function load_bahasa(idpasien){
	// $("#cover-spin").show();
	// if (idpasien){
		$.ajax({
			url: "{site_url}tpendaftaran_ranap/load_bahasa/" + idpasien,
			method: "POST",
			dataType: "json",
			success: function(data) {
				$('#bahasa').empty();
				$('#bahasa').append(data);
				// $(".js_select2").select2("");
				// $("#cover-spin").hide();
			}
		});
	// }
	
}
function hapus_kontak(row){
	 row.closest("tr").remove();
}
function hapus_kontak_id(row,id){
	// alert(id);
	let idpasien=$("#idpasien").val();
	$("#cover-spin").show();
	 $.ajax({
		url: "{site_url}tpendaftaran_ranap/hapus_kontak/",
		method: "POST",
		dataType: "json",
		data:{id:id},
		success: function(data) {
			// row.closest("tr").remove();
			load_kontak(idpasien);
			$("#cover-spin").hide();
		}
	});
}
$("#jenis_kelamin,#statuskawin").change(function() {
	let statuskawin = ($("#statuskawin option:selected").val() != '' ? $("#statuskawin option:selected").val() : 1);
	let jeniskelamin = ($("#jenis_kelamin option:selected").val() != '' ? $("#jenis_kelamin option:selected").val() : 1);

	getTitlePasien($("#umurtahun").val(), $("#umurbulan").val(), $("#umurhari").val(), statuskawin, jeniskelamin);
});
function cek_hasil(){
	  window.history.back();
}

// $("#form1").validate({
  // submitHandler: function(form) {
   // $("#cover-spin").show();
   // $("*[disabled]").not(true).removeAttr("disabled");
   // form.submit();
// }
// });

$("#form1").on("submit", function(){
	if($("#form1").valid()){
	//loader
	   $("*[disabled]").not(true).removeAttr("disabled");
	 $("#cover-spin").show();
	}
}); 

$(".div_pj").on("keyup", function(){
	check =$("#chk_st_pengantar").val();
	if (check=='1'){
		set_sama_pengantar();
	}
});
$("#hubungan").on("change", function(){
	check =$("#chk_st_pengantar").val();
	if (check=='1'){
		set_sama_pengantar();
	}
}); 
$("#alamatpasien,#rw,#rt").keyup(function() {
check = $("#chk_st_domisili").is(":checked");
    if(check) {
        set_sama_domisili();
    } 
});

function get_last_kunjungan(idpasien){
	$id=$("#id").val();
	if (idpasien){
		$.ajax({
			url: "{site_url}tpendaftaran_ranap/get_last_kunjungan/" + idpasien+'/'+$id,
			method: "POST",
			dataType: "json",
			success: function(data) {
				if (data !=null){
					$('.historykunjungan').show();
					$("#his_poli").text(data.nama_poli);
					$("#his_dokter").text(data.nama_dokter);
					if (data.nama_dokter){
						document.getElementById('div_nama_dokter').style.display = 'block';
					}else{
						document.getElementById('div_nama_dokter').style.display = 'none';
					}
					$("#his_tanggal").text(data.tanggaldaftar);
				}else{
					$('.historykunjungan').hide();
					
				}
				
			}
		});
	}else{
		$('.historykunjungan').hide();
		
	}
	
}
$(document).on("click", ".selectPasien", function() {
	$("#cover-spin").show();
	var idpasien = ($(this).data('idpasien'));
	load_detail_pasien(idpasien);
	setTimeout(function() {
	  let newOption = new Option($("#no_medrec").val(), $("#idpasien").val(), true, true);
		$("#tmp_medrec").append(newOption);
	}, 1000);

	
	return false;
});
$(document).on("change", "#tmp_medrec", function() {
	if ($(this).val()!=null){
		$("#cover-spin").show();
			
		var idpasien = $(this).val();
		load_detail_pasien(idpasien);
	}
	// return false;
});

$(document).on("click", "#btn_cari_kartu", function() {
	show_kartu(1);
});
$(document).on("click", "#btn_tambah_kartu", function() {
	show_kartu(2);
});
function load_detail_pasien(idpasien){
	// $("#cover-spin").show();
	// $.ajax({
		// url: "{site_url}tpendaftaran_ranap/getDataPasien/" + idpasien,
		// method: "POST",
		// dataType: "json",
		// success: function(data) {
			// if (data[0].noid_lama){
				// sweetAlert("Informasi", "Data PASIEN <strong>'"+data[0].noid_lama+"'</strong> tidak akan digunakan karena <br> telah digabungkan dengan data PASIEN <strong>'"+data[0].no_medrec+"'</strong> <br><br> data PASIEN <strong>'"+data[0].no_medrec+"'</strong> akan digunakan.", "info");
			// }
			// st_load_manual=true;
			// var status_kawin = (data[0].status_kawin != 0 ? data[0].status_kawin : 1);
			// var jenis_kelamin = (data[0].jenis_kelamin != 0 ? data[0].jenis_kelamin : 1);

			// $('#status_cari').val(1);
			// $('#ckabupaten').val(data[0].kabupaten_id);
			// $('#ckecamatan').val(data[0].kecamatan_id);
			// $('#ckelurahan').val(data[0].kelurahan_id);
			
			// $('#ckabupaten_ktp').val(data[0].kabupaten_id_ktp);
			// // alert(data[0].kabupaten_id_ktp);
			// $('#ckecamatan_ktp').val(data[0].kecamatan_id_ktp);
			// $('#ckelurahan_ktp').val(data[0].kelurahan_id_ktp);

			// $('#idpasien').val(data[0].id);
			// $('#no_medrec').val(data[0].no_medrec);
			// $('#no_medrec_lama').val(data[0].noid_lama);
			// $('#nama').val(data[0].nama);
			// $('#alamat_jalan').val(data[0].alamat_jalan);
			// $('#jenis_kelamin').select2("val", jenis_kelamin);
			// $('#jenis_id').select2("val", data[0].jenis_id);
			// $('#noidentitas').val(data[0].ktp);
			// $('#nohp').val(data[0].hp);
			// $('#telprumah').val(data[0].telepon);
			// $('#email').val(data[0].email);
			// $('#tempat_lahir').val(data[0].tempat_lahir);


			// // Pecahan Tanggal Lahir (Hari, Bulan, Tahun)
			// var tanggalLahir = data[0].tanggal_lahir;
			// var tahun = tanggalLahir.substr(0, 4);
			// var bulan = tanggalLahir.substr(5, 2);
			// var hari = tanggalLahir.substr(8, 2);
			// // alert(bulan);
			// // $('#tahun').val(tahun);
			// $("#tahun").val(tahun).trigger('change');
			// $("#bulan").val(bulan).trigger('change');
			// $("#hari").val(hari).trigger('change');

			// generate_tanggal_lahir();
			// $('#golongan_darah').val(data[0].golongan_darah).trigger('change');
			// $('#agama_id').val(data[0].agama_id).trigger('change');
			// $('#warganegara').val(data[0].warganegara).trigger('change');
			// $('#suku_id').val(data[0].suku_id).trigger('change');
			// $('#suku').val(data[0].suku).trigger('change');
			// $('#statuskawin').val(status_kawin).trigger('change').trigger('change');
			// $('#pendidikan').val(data[0].pendidikan_id).trigger('change').trigger('change');
			// $('#pekerjaan').val(data[0].pekerjaan_id).trigger('change').trigger('change');
			// $('#catatan').val(data[0].catatan);
			// $('#namapenanggungjawab').val(data[0].nama_keluarga);
			// $('#hubungan').val(data[0].hubungan_dengan_pasien).trigger('change');
			// $('#alamatpenanggungjawab').val(data[0].alamat_keluarga);
			// $('#teleponpenanggungjawab').val(data[0].telepon_keluarga);
			// $('#rw').val(data[0].rw);
			// $('#rt').val(data[0].rt);
			// $('#noidentitaspenanggung').val(data[0].ktp_keluarga);
			// $('#nik').val(data[0].nik);
			// $('#nama_ibu_kandung').val(data[0].nama_ibu_kandung);
			// $('#nama_panggilan').val(data[0].nama_panggilan);
			// $('#bahasa').val(data[0].bahasa).trigger('change');
			// $('#provinsi_id').val(data[0].provinsi_id).trigger('change');
			// $('#kabupaten1').val(data[0].kabupaten_id).trigger('change');
			// $("#cover-spin").hide();
			
			
			// generate_rincian();
			// get_last_kunjungan(idpasien);
			// load_kontak(idpasien);
			
			// // if (data[0].chk_st_domisili==null){
			// // }
				// set_sama_domisili();
			
			// // show_kartu(1);
			// $("#cover-spin").hide();
			// setTimeout(function() {
				// let check =$("#chk_st_pengantar").val();
				// if (check=='1'){
					// set_sama_pengantar();
				// }
			  // copy_data_penanggunjawab_ke_detail();
			// }, 2000);

			
			// st_load_manual=false;
		// }
	// });
}

function show_kartu($tab){
	$("#modal_kartu").modal('show');
	if ($tab=='1'){
	$('[href="#tab_list_kartu"]').tab('show');
		LoadKartu();
	}else{
	$('[href="#tab_add_kartu"]').tab('show');
		
	}
}
// $("#hari,#bulan,#tahun").change(function() {
	// console.log(st_load_manual);
	// if (st_load_manual==false){
		// // generate_tanggal_lahir();
	// }
// });	

// function generate_tanggal_lahir(){
// // $("#hari").change(function() {
	// var hari = $("#hari").val();
	// var bulan = $("#bulan").val();
	// var tahun = $("#tahun").val();
	// var tanggal = tahun + "-" + bulan + "-" + hari;
	// $.ajax({
		// url: '{site_url}tpoliklinik_pendaftaran/getDate',
		// method: "POST",
		// dataType: "json",
		// data: {
			// "hari": hari,
			// "bulan": bulan,
			// "tahun": tahun,
			// "tanggal": tanggal
		// },
		// success: function(data) {
			// if (tahun == "0" && bulan == "0") {
				// $("#umur_tahun").val("0");
				// $("#umur_bulan").val("0");
				// $("#umur_hari").val(hari);
			// } else {
				// $("#umur_tahun").val(data.date[0].tahun);
				// $("#umur_bulan").val(data.date[0].bulan);
				// $("#umur_hari").val(data.date[0].hari);
			// }

			// let statuskawin = ($("#statuskawin option:selected").val() != '' ? $("#statuskawin option:selected").val() : 1);
			// let jeniskelamin = ($("#jenis_kelamin option:selected").val() != '' ? $("#jenis_kelamin option:selected").val() : 1);

			// getTitlePasien(data.date[0].tahun, data.date[0].bulan, data.date[0].hari, statuskawin, jeniskelamin);
		// }
	// });
		// // });
// }

function getTitlePasien(tahun, bulan, hari, statuskawin, jenis_kelamin) {
	console.log(tahun+' Bulan '+bulan+' Jenis K '+jenis_kelamin);
		if (tahun == 0 && bulan <=5) {
			$('#titlepasien').val("By").trigger('change');
		} else if ((tahun == 0 && bulan > 5) || tahun <= 15) {
	console.log('Anak Bro');
			$('#titlepasien').val("An").trigger('change');
		} else if (tahun >= 16 && jenis_kelamin == '1') {
			$('#titlepasien').val("Tn").trigger('change');
		} else if (tahun >= 15 && jenis_kelamin == '2' && statuskawin == 1) {
			$('#titlepasien').val("Nn").trigger('change');
		} else if (jenis_kelamin == '2' && statuskawin == 2) {
			$('#titlepasien').val("Ny").trigger('change');
		} else if (jenis_kelamin == '2' && statuskawin == 3) {
			$('#titlepasien').val("Ny").trigger('change');
		} else if (jenis_kelamin == '2' && statuskawin == 4) {
			$('#titlepasien').val("Tn").trigger('change');
	console.log('Tuan Bro');
			
		}
	console.log($('#titlepasien').val());
	}
	
// Perjanjian
// $("#reservasi_tipe_id").change(function() {
	// change_reservasi_tipe();
// });
// $("#tipepoli").change(function() {
	// $("#reservasi_tipe_id").val(1).trigger('change');
// });
// function change_reservasi_tipe(){
	// var idtipe = $("#tipepoli").val();
	// var reservasi_tipe_id = $("#reservasi_tipe_id").val();
	// if (reservasi_tipe_id=='1'){//Dokter
		// $("#iddokter").attr('required',"true");
		// if (idtipe!='2'){
			// $(".btn_cari_dokter").removeAttr('disabled');
		// }else{
			// $(".btn_cari_dokter").attr('disabled',"true");
		// }
		
	// }else{
		// $("#iddokter").removeAttr('required');
		// $(".btn_cari_dokter").attr('disabled',"true");
		
	// }
	// $.ajax({
		// url: '{site_url}tpendaftaran_ranap/get_poli',
		// method: "POST",
		// dataType: "json",
		// data: {
			// "reservasi_tipe_id": reservasi_tipe_id,
			// "idtipe": idtipe,
		// },
		// success: function(data) {
			// $("#idpoli").empty();
			// $("#iddokter").empty();
			// $("#tanggal").empty();
			// $("#jadwal_id").empty();
			// $("#idpoli").append("<option value=''>Pilih Poliklinik</option>");
			// for (var i = 0; i < data.length; i++) {
					// $("#idpoli").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
			// }
			// if (idtipe=='2'){//IGD
				
				// $("#reservasi_tipe_id").attr('disabled',true);
				// $("#idpoli").val(22).trigger('change');
				// $("#jadwal_id").removeAttr('required');
				
			// }else{
				// $("#reservasi_tipe_id").removeAttr('disabled');
				// $("#jadwal_id").attr('required',"true");
				
			// }
			// $("#idpoli").selectpicker("refresh");
			// // if ($("#status_cari").val()=='1'){
				// // $('#kabupaten1').val($("#ckabupaten").val()).trigger('change');
			// // }
		// },
		// error: function(data) {
			// console.log(data);
		// }
	// });
// }
// function get_dokter(){
	// var idpoli = $("#idpoli").val();
	// $("#iddokter").empty();
	// if ($("#reservasi_tipe_id").val()=='1'){
			// $("#tanggal").empty();
		
	// }
	// generate_rincian();
	// $.ajax({
		// url: '{site_url}tpendaftaran_ranap/get_dokter_poli',
		// method: "POST",
		// dataType: "json",
		// data: {
			// "idpoli": idpoli
		// },
		// success: function(data) {
			
			// $("#iddokter").append("<option value=''>Pilih Dokter</option>");
			// for (var i = 0; i < data.length; i++) {
					// $("#iddokter").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
			// }

			// $("#iddokter").selectpicker("refresh");
			// // if ($("#status_cari").val()=='1'){
				// // $('#kabupaten1').val($("#ckabupaten").val()).trigger('change');
			// // }
		// },
		// error: function(data) {
			// console.log(data);
		// }
	// });
// }

// $("#iddokter").change(function() {
	// let idtipe=$("#tipepoli").val();
	// if (idtipe=='1'){
		// get_tanggal();
		
	// }else{
		// get_tanggal_igd();
	// }
	// // cek_duplicate();
// });

$("#idkelompokpasien").change(function() {
	set_kelompok_pasien();
});
function set_kelompok_pasien(){
		let kelompokpasien=$("#idkelompokpasien").val();
	if (kelompokpasien=='1'){
		$(".div_asuransi").css("display", "block");
	}else{
		
		$(".div_asuransi").css("display", "none");
	}
	if (kelompokpasien=='5'){
		$(".div_foto").css("display", "none");
	}else{
		
		$(".div_foto").css("display", "block");
	}
	generate_rincian();
}
$("#idasalpasien").change(function() {
	set_asal_rujukan();
	load_faskes_1();
});
function set_asal_rujukan(){
	var asal = $("#idasalpasien").val();
	if (asal == "1") {
		$(".div_rujukan").css("display", "none");
	} else {
		$(".div_rujukan").css("display", "block");
		
	}
}
function load_faskes_1(){
	$("#cover-spin").show();
	var asal = $("#idasalpasien").val();
	$.ajax({
		url: '{site_url}tpoliklinik_pendaftaran/getRujukan',
		method: "POST",
		dataType: "json",
		data: {
			"asal": asal
		},
		success: function(data) {
			$("#idrujukan").empty();
			$("#idrujukan").append("<option value=''>Pilih Opsi</option>");
			for (var i = 0; i < data.length; i++) {
				$("#idrujukan").append("<option value='" + data[i].id + "' >" + data[i].nama + "</option>");
			}
			$("#idrujukan").selectpicker("refresh");
			$("#cover-spin").hide();
		}
	});
}
var loadFile_foto = function(event) {
var output_img = document.getElementById('output_img');
output_img.src = URL.createObjectURL(event.target.files[0]);
};
$("#iddokter,#nama,#no_medrec,#pertemuan_id,#idrekanan,#jadwal_id").change(function() {
	generate_rincian();
});

function clear_pasien(){
	console.log('sini');
	$('#email').val('');
	$('#idpasien').val('');
	$('#no_medrec').val('');
	$('#no_medrec_lama').val('');
	$('#nama').val('');
	$('#alamat_jalan').val('');
	$('#jenis_kelamin').val(null).trigger('change');
	$('#jenis_id').val(null).trigger('change');
	$('#noidentitas').val('');
	$('#nohp').val('');
	$('#telprumah').val('');
	$('#email').val('');
	$('#tempat_lahir').val('');


	// Pecahan Tanggal Lahir (Hari, Bulan, Tahun)
	var tanggalLahir = '';
	// var tahun = tanggalLahir.substr(0, 4);
	// var bulan = tanggalLahir.substr(5, 2);
	// var hari = tanggalLahir.substr(8, 2);
	// alert(bulan);
	// $('#tahun').val(tahun);
	$("#tahun").val(0).trigger('change.select2');
	$("#bulan").val('00').trigger('change.select2');
	$("#hari").val(0).trigger('change.select2');

	$('#umur_tahun').val('');
	$('#umur_bulan').val('');
	$('#umur_hari').val('');
	
	$('#golongan_darah').val(null).trigger('change');
	$('#agama_id').val(null).trigger('change');
	$('#warganegara').val(null).trigger('change');
	$('#suku_id').val(null).trigger('change');
	$('#suku').val('');
	$('#statuskawin').val(null).trigger('change');
	$('#pendidikan').val(null).trigger('change');
	$('#pekerjaan').val(null).trigger('change');
	$('#catatan').val('');
	$('#namapenanggungjawab').val('');
	$('#kelurahan1').val(null).trigger('change');
	$('#kecamatan1').val(null).trigger('change');
	$('#kecamatan_id_ktp').val(null).trigger('change');
	$('#hubungan').val(null).trigger('change');
	$('#alamatpenanggungjawab').val('');
	$('#teleponpenanggungjawab').val('');
	$('#ckabupaten').val(213);
	$('#kodepos').val('');
	$('#rw').val('');
	$('#rt').val('');
	$('#noidentitaspenanggung').val('');
	$('#provinsi_id').val(12).trigger('change');
	$('#kabupaten1').val(213).trigger('change');
	$('#nik').val('');
	$('#nama_ibu_kandung').val('');
	$('#nama_panggilan').val('');
	$('#bahasa').val(null).trigger('change');
	$("#cover-spin").hide();
	generate_rincian();
	get_last_kunjungan('');
	load_kontak('');
	$("#cover-spin").hide();
	copy_data_penanggunjawab_ke_detail();
}

$("#st_kecelakaan").change(function() {
	set_jenis_kecelakaan();
});
function set_jenis_kecelakaan(){
	if ($("#st_kecelakaan").val()=='1'){
		$(".div_kecelakaan").show();
	}else{
		
		$(".div_kecelakaan").hide();
	}
}
function generate_rincian(){
	// $(".lbl_dokter").val($("#iddokter option:selected").text());
	// $(".lbl_tujuan").val($("#idpoli option:selected").text());
	// $(".lbl_jenis_pertemuan").val($("#pertemuan_id option:selected").text());
	// $("#suku").val($("#suku_id option:selected").text());
	// $(".lbl_asuransi").val($("#idrekanan option:selected").text());
	// $(".lbl_status_pasien").val($("#statuspasienbaru option:selected").text());
	// $(".lbl_jenis_booking").val($("#reservasi_cara option:selected").text());
	// $(".lbl_cara_pembayaran").val($("#idkelompokpasien option:selected").text());
	// $(".lbl_tanggal_janji").val($("#tanggal option:selected").text() + ' ('+$("#jadwal_id option:selected").text()+')');
	// $(".lbl_nama").val($("#nama").val());
	// $(".lbl_norekammedis").val($("#no_medrec").val());
	// // $("#suku").val($("#no_medrec").val());
	// $(".lbl_asuransi").val($("#no_medrec").val());
	cek_duplicate();
}
$("#iddokterpenanggungjawab,#idtipe").change(function() {
	get_logic_pendaftaran();
	
});
function get_logic_pendaftaran(){
	let iddokter=$("#iddokterpenanggungjawab").val();
	let idtujuan=$("#idtipe").val();
	
	  $.ajax({
		url: '{site_url}tpendaftaran_ranap/get_logic_pendaftaran',
		method: "POST",
		dataType: "json",
		data: {
			"idtujuan": idtujuan,
			"iddokter": iddokter,
		},
		success: function(data) {
			$("#st_lm").val(data.st_lm);
			$("#st_sp").val(data.st_sp);
			$("#st_gc").val(data.st_gc);
			$("#st_hk").val(data.st_hk);
			$("#text_skrining").html(data.text_skrining);
			show_hide_button()
			if (data.st_hk=='1'){
				$(".wizard-finish").html('<i class="fa fa-check"></i>  Lanjut Hak & Kewajiban 2');
			}
			if (data.st_gc=='1'){
				$(".wizard-finish").html('<i class="fa fa-check"></i>  Lanjut General Consent');
			}
			
			if (data.st_sp=='1'){
				$(".wizard-finish").html('<i class="fa fa-check"></i>  Lanjut Lembar Pernyataan');
			}
			if (data.st_lm=='1'){
				$(".wizard-finish").html('<i class="fa fa-check"></i>  Lanjut Lembar Masuk Keluar');
			}
			
		}
	});
	if (idtujuan=='2'){
		// $(".div_bed").hide();
		$("#idruangan").val($("#idruangan_default").val()).trigger('change.select2');
		$("#idkelas").val($("#idkelas_default").val()).trigger('change.select2');
		$("#idbed").val($("#idbed_default").val()).trigger('change.select2');
		$(".div_permintaan").hide();
		$(".div_ods").prop('disabled',true);
	}else{
		$(".div_permintaan").show();
		$(".div_ods").removeAttr('disabled');
		
	}
}
function show_hide_button(){
	let total_skrining=0;
	total_skrining = parseFloat($("#st_sp").val()) + parseFloat($("#st_lm").val())+ parseFloat($("#st_gc").val());
	if (total_skrining>0){
		$("#btn_skrining").show();
	}else{
		$("#btn_skrining").hide();
		// alert('sini');
		
	}
	if ($("#st_sp").val()=='1' || $("#st_lm").val()=='1'){
		$("#btn_skrining").text('Lanjut Skrining Pasien');
	}
	if ($("#st_gc").val()=='1'){
		$("#btn_skrining").text('Lanjut General Consent');
	}
}
function cek_duplicate(){
	
	let idtipe=$("#tipepoli").val();
	let iddokter=$("#iddokter").val();
	let jadwal_id=$("#jadwal_id").val();
	let idpoli=$("#idpoli").val();
	let idpasien=$("#idpasien").val();
	let tanggal=$("#tanggal").val();
	if (idtipe=='1'){
		
	$.ajax({
		url: '{site_url}tbooking/cek_duplicate',
		method: "POST",
		dataType: "json",
		data: {
			"iddokter": iddokter,
			"jadwal_id": jadwal_id,
			"idpoli": idpoli,
			"idpasien": idpasien,
			"tanggal": tanggal,
		},
		success: function(data) {
			console.log(data);
			if (data){//Duplicate
				$(".div_duplicate").show();
				$(".div_not_duplicate").hide();
			}else{
				$(".div_duplicate").hide();
				$(".div_not_duplicate").show();
			}
		}
	});
	}
}
function load_index_lm(){
	// $("#cover-spin").show();
	var id=$("#id").val();
	// alert(id);
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/load_index_lm/'+id,
		dataType: "json",
		success: function(data) {
			$("#tabel_lm tbody").empty();
			$("#tabel_lm tbody").append(data.tabel);
			$(".js-select2").select2();
			validate_lm();
			$("#cover-spin").hide();
		}
	});
}
function load_index_hk(){
	// $("#cover-spin").show();
	var id=$("#id").val();
	// alert(id);
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/load_index_hk/'+id,
		dataType: "json",
		success: function(data) {
			$("#tabel_hk tbody").empty();
			$("#tabel_hk tbody").append(data.tabel);
			$(".js-select2").select2();
			validate_lm();
			$("#cover-spin").hide();
		}
	});
}
$(document).on("change",".nilai_lm",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var approval_id=tr.find(".approval_id").val();
	var nilai_id=$(this).val();
	$.ajax({
		  url: '{site_url}tpendaftaran_ranap/update_nilai_lm/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				approval_id:approval_id,nilai_id:nilai_id,
		  },success: function(data) {
				validate_lm();
			}
		});
	
});
$(document).on("change",".nilai_hk",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var approval_id=tr.find(".approval_id").val();
	var nilai_id=$(this).val();
	$.ajax({
		  url: '{site_url}tpendaftaran_ranap/update_nilai_hk/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				approval_id:approval_id,nilai_id:nilai_id,
		  },success: function(data) {
				// validate_lm();
			}
		});
	
});
function load_index_gc(){
	// $("#cover-spin").show();
	var id=$("#id").val();
	// alert(id);
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/load_index_gc/'+id,
		dataType: "json",
		success: function(data) {
			$("#tabel_gc tbody").empty();
			$("#tabel_gc tbody").append(data.tabel);
			$(".js-select2").select2();
			validate_gc();
			$("#cover-spin").hide();
		}
	});
}
function modal_faraf($pendaftaran_id,$nama_tabel){
	$("#ttd_id").val($pendaftaran_id);
	$("#nama_tabel").val($nama_tabel);
	$("#modal_paraf").modal('show');
}
function modal_ttd(){
	$("#nama_id_ttd").val('id');
	$("#nama_tabel_ttd").val('trawatinap_pendaftaran');
	$("#nama_field_ttd").val('ttd_lm');
	$("#modal_ttd").modal('show');
}
function modal_ttd_sp(){
	$("#nama_id_ttd").val('id');
	$("#nama_tabel_ttd").val('trawatinap_pendaftaran');
	$("#nama_field_ttd").val('ttd_sp');
	$("#modal_ttd").modal('show');
}
function modal_ttd_gc(){
	$("#nama_id_ttd").val('id');
	$("#nama_tabel_ttd").val('trawatinap_pendaftaran');
	$("#nama_field_ttd").val('ttd_gc');
	$("#modal_ttd").modal('show');
}
function hapus_paraf($pendaftaran_id,$nama_tabel){
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}tpendaftaran_ranap/hapus_paraf/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				pendaftaran_id:$pendaftaran_id,
				nama_tabel:$nama_tabel,
		  },success: function(data) {
				$("#modal_paraf").modal('hide');
				if ($nama_tabel=='trawatinap_pendaftaran_lm'){
	// alert($nama_tabel);
					load_index_lm();
				}
				if ($nama_tabel=='trawatinap_pendaftaran_gc'){
					load_index_gc();
				}
				if ($nama_tabel=='trawatinap_pendaftaran_hk'){
					load_index_hk();
				}
				$("#cover-spin").hide();
			}
		});
}

$(document).on("click","#btn_save_paraf",function(){
	var pendaftaran_id=$("#ttd_id").val();
	var signature64=$("#signature64").val();
	var nama_tabel=$("#nama_tabel").val();
	$nama_tabel=$("#nama_tabel").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}tpendaftaran_ranap/save_ttd/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				pendaftaran_id:pendaftaran_id,signature64:signature64,nama_tabel:nama_tabel,
		  },success: function(data) {
				$("#modal_paraf").modal('hide');
				if ($nama_tabel=='trawatinap_pendaftaran_lm'){
					load_index_lm();
				}
				if ($nama_tabel=='trawatinap_pendaftaran_gc'){
					load_index_gc();
				}
				if ($nama_tabel=='trawatinap_pendaftaran_hk'){
					load_index_hk();
				}
				$("#cover-spin").hide();
			}
		});
	
});
$(document).on("click","#btn_save_ttd",function(){
	var pendaftaran_id=$("#ttd_id_2").val();
	var signature64=$("#signature64_2").val();
	var nama_tabel=$("#nama_tabel_ttd").val();
	var nama_field=$("#nama_field_ttd").val();
	var nama_id_ttd=$("#nama_id_ttd").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}tpendaftaran_ranap/save_ttd_2/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				pendaftaran_id:pendaftaran_id,
				signature64:signature64,
				nama_tabel:nama_tabel,
				nama_field:nama_field,
				nama_id_ttd:nama_id_ttd,
		  },success: function(data) {
				location.reload();
			}
		});
	
});
function hapus_ttd(){
	var pendaftaran_id=$("#ttd_id_2").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Batal Tandatangan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		$.ajax({
			  url: '{site_url}tpendaftaran_ranap/hapus_ttd/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					pendaftaran_id:pendaftaran_id,
					// nama_tabel:nama_tabel,
			  },success: function(data) {
					location.reload();
				}
		});

	});
	
}
function hapus_ttd_lm(){
	var pendaftaran_id=$("#id").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Batal Tandatangan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		$.ajax({
			  url: '{site_url}tpendaftaran_ranap/hapus_ttd_lm/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					pendaftaran_id:pendaftaran_id,
					// nama_tabel:nama_tabel,
			  },success: function(data) {
					location.reload();
				}
		});

	});
	
}
function hapus_ttd_sp(){
	var pendaftaran_id=$("#id").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Batal Tandatangan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		$.ajax({
			  url: '{site_url}tpendaftaran_ranap/hapus_ttd_sp/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					pendaftaran_id:pendaftaran_id,
					// nama_tabel:nama_tabel,
			  },success: function(data) {
					location.reload();
				}
		});

	});
	
}
function hapus_ttd_gc(){
	var pendaftaran_id=$("#id").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Batal Tandatangan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		$.ajax({
			  url: '{site_url}tpendaftaran_ranap/hapus_ttd_gc/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					pendaftaran_id:pendaftaran_id,
					// nama_tabel:nama_tabel,
			  },success: function(data) {
					location.reload();
				}
		});

	});
	
}
function validate_gc(){
	let st_next=true;
	let jawaban_perssetujuan=$("#jawaban_perssetujuan").val();
	if (jawaban_perssetujuan==''){
		st_next=false;
	}
	if ($("#jawaban_ttd").val()==''){
		st_next=false;
		
	}
	
	if (st_next==false){
		$('.btn_gc').attr('disabled', true);
	}else{
		$('.btn_gc').attr('disabled', false);
		
	}
	if (st_general=='1'){
		$('.nilai_gc').attr('disabled', true);
		$('.nilai_gc_free').attr('disabled', true);
		$('.btn_ttd').attr('disabled', true);
	}else{
		
		$('.nilai_gc').attr('disabled', false);
		$('.nilai_gc_free').attr('disabled', false);
		$('.btn_ttd').attr('disabled', false);
	}
}
function validate_lm(){
	let st_next=true;
	let ttd_lm=$("#ttd_lm").val();
	if (ttd_lm==''){
		st_next=false;
	}
	
	if (st_next==false){
		$('.btn_lm').attr('disabled', true);
	}else{
		$('.btn_lm').attr('disabled', false);
		
	}
	// if (st_lembar=='1'){
		// $('.nilai_lm').attr('disabled', true);
		// $('.nilai_lm_free').attr('disabled', true);
		// $('.btn_ttd_lm').attr('disabled', true);
	// }else{
		
		// $('.nilai_lm').attr('disabled', false);
		// $('.nilai_lm_free').attr('disabled', false);
		// $('.btn_ttd_lm').attr('disabled', false);
	// }
}
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
$('#clear').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
var sig_2 = $('#sig_2').signature({syncField: '#signature64_2', syncFormat: 'PNG'});
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig_2.signature('clear');
	$("#signature64_2").val('');
});


$(document).on("change","#jawaban_perssetujuan",function(){
	// alert($(this));
	var pendaftaran_id=$("#id").val();
	var jawaban_perssetujuan=$("#jawaban_perssetujuan").val();
	$.ajax({
		  url: '{site_url}tpendaftaran_ranap/update_persetujuan_gc/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				pendaftaran_id:pendaftaran_id,
				jawaban_perssetujuan:jawaban_perssetujuan,
		  },success: function(data) {
				validate_gc();
			}
		});
	
});
$(document).on("blur",".nilai_gc_free",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var approval_id=tr.find(".sp_id").val();
	var nilai_id=$(this).val();
	$.ajax({
	  url: '{site_url}tpendaftaran_ranap/update_nilai_gc_free/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			approval_id:approval_id,nilai_id:nilai_id,
	  },success: function(data) {
			validate_gc();
		}
	});
});
$(document).on("blur",".nilai_hk_free",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var approval_id=tr.find(".approval_id").val();
	// alert(approval_id);
	var nilai_id=$(this).val();
	$.ajax({
	  url: '{site_url}tpendaftaran_ranap/update_nilai_hk_free/',
	  dataType: "json",
	  type: 'POST',
	  data: {
			approval_id:approval_id,nilai_id:nilai_id,
	  },success: function(data) {
			// validate_hk();
		}
	});
});

//SP
function load_index_sp(){
	$("#cover-spin").show();
	var id=$("#id").val();
	// alert(id);
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/load_index_sp/'+id,
		dataType: "json",
		success: function(data) {
			$("#tabel_sp tbody").empty();
			$("#tabel_sp tbody").append(data.tabel);
			$(".js-select2").select2();
			validate_sp();
			$("#cover-spin").hide();
		}
	});
}
$(document).on("change",".nilai_sp",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var sp_id=tr.find(".sp_id").val();
	var nilai_id=$(this).val();
	$.ajax({
		  url: '{site_url}tpendaftaran_ranap/update_nilai_sp/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				sp_id:sp_id,nilai_id:nilai_id,
		  },success: function(data) {
				validate_sp();
			}
		});
	
});
function validate_sp(){
	let st_next=true;
	$('#tabel_sp tbody tr').each(function() {
		let opsi=$(this).find(".nilai_sp").val();
		if (opsi==''){
			st_next=false;
		}
			
	});
	if (st_next==false){
		$('.btn_sp').attr('disabled', true);
	}else{
		$('.btn_sp').attr('disabled', false);
		
	}
	if (st_skrining=='1'){
		$('.nilai_sp').attr('disabled', true);
		$('.nilai_sp_free').attr('disabled', true);
		$('.btn_ttd_sp').attr('disabled', true);
	}else{
		
		$('.nilai_sp').attr('disabled', false);
		$('.nilai_sp_free').attr('disabled', false);
		$('.btn_ttd_sp').attr('disabled', false);
	}
}
function load_index_sc(){
	$("#cover-spin").show();
	var id=$("#id").val();
	// alert(id);
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/load_index_sc/'+id,
		dataType: "json",
		success: function(data) {
			$("#tabel_sc tbody").empty();
			$("#tabel_sc tbody").append(data.tabel);
			$(".js-select2").select2();
			validate_sc();
			$("#cover-spin").hide();
		}
	});
}
$(document).on("change",".nilai_sc",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var sc_id=tr.find(".sc_id").val();
	var nilai_id=$(this).val();
	$.ajax({
		  url: '{site_url}tpendaftaran_ranap/update_nilai_sc/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				sc_id:sc_id,nilai_id:nilai_id,
		  },success: function(data) {
				validate_sc();
			}
		});
	
});
function validate_sc(){
	let st_next=true;
	$('#tabel_sc tbody tr').each(function() {
		let opsi=$(this).find(".nilai_sc").val();
		if (opsi==''){
			st_next=false;
		}
			
	});
	if (st_next==false){
		$('.btn_sc').attr('disabled', true);
		$('.btn_sc').attr('disabled', true);
	}else{
		$('.btn_sc').attr('disabled', false);
		
	}
	if (st_covid=='1'){
		$('.nilai_sc').attr('disabled', true);
		$('.nilai_sc_free').attr('disabled', true);
		$('.btn_ttd_sc').attr('disabled', true);
	}else{
		
		$('.nilai_sc').attr('disabled', false);
		$('.nilai_sc_free').attr('disabled', false);
		$('.btn_ttd_sc').attr('disabled', false);
	}
}
$("#chk_st_domisili").on("click", function(){
check = $("#chk_st_domisili").is(":checked");
    if(check) {
		set_sama_domisili();
		copy_provinsi_ktp();
		copy_kabupaten_ktp();
		copy_kecamatan_ktp();
		copy_desa_ktp();
        // $("#provinsi_id_ktp").val($("#provinsi_id").val()).trigger('change');
		// $("#kabupaten_id_ktp").val($("#ckabupaten_ktp").val()).trigger('change');
		// $("#kecamatan_id_ktp").val($("#ckecamatan_ktp").val()).trigger('change');
		// $("#kelurahan_id_ktp").val($("#ckelurahan_ktp	").val()).trigger('change');
		console.log($("#kabupaten1").val());
    } else {
        set_beda_domisili();
    }
}); 
function copy_kabupaten_ktp(){
	var $options = $("#kabupaten_id > option").clone();
    $('#kabupaten_id_ktp').empty();
    $('#kabupaten_id_ktp').append($options);
    $('#kabupaten_id_ktp').val($('#kabupaten_id').val()).trigger('change.select2');
}
function copy_provinsi_ktp(){
	var $options = $("#provinsi_id > option").clone();
    $('#provinsi_id_ktp').empty();
    $('#provinsi_id_ktp').append($options);
    $('#provinsi_id_ktp').val($('#provinsi_id').val()).trigger('change.select2');
}
function copy_kecamatan_ktp(){
	var $options = $("#kecamatan_id > option").clone();
    $('#kecamatan_id_ktp').empty();
    $('#kecamatan_id_ktp').append($options);
    $('#kecamatan_id_ktp').val($('#kecamatan_id').val()).trigger('change.select2');
}
function copy_desa_ktp(){
	var $options = $("#kelurahan_id > option").clone();
    $('#kelurahan_id_ktp').empty();
    $('#kelurahan_id_ktp').append($options);
    $('#kelurahan_id_ktp').val($('#kelurahan_id').val()).trigger('change.select2');
}
</script>