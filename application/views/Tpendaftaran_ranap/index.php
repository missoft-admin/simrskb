<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>
<?php $user_id = $this->session->userdata('user_id'); ?>
<?
?>
<div class="block">
    <ul class="nav nav-tabs" data-toggle="tabs">
        <li class="<?=($tab=='0'?'active':'')?>">
            <a href="#tab_0" ><i class="si si-list"></i> Pendaftaran</a>
        </li>
		
        <li class="<?=($tab=='2'?'active':'')?>">
            <a href="#tab_2"  ><i class="si si-pin"></i> Kunjungan Rawat Jalan </a>
        </li>
        <li class="<?=($tab=='3'?'active':'')?>">
            <a href="#tab_3" ><i class="fa fa-check-square-o"></i> Perencanaan</a>
        </li>
		<li class="<?=($tab=='1'?'active':'')?>">
            <a href="#tab_1" ><i class="fa fa-send-o"></i> Reservasi</a>
        </li>
    </ul>
    <input type="hidden" id="tab" name="tab" value="{tab}">
    <input type="hidden" id="tab_detail" name="tab_detail" value="{tab_detail}">
    <div class="block-content tab-content">
		<div class="tab-pane fade fade-left <?=($tab=='0'?'active in':'')?>" id="tab_0">
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div id="div_expand" class="block block-opt-hidden">
				<div class="block-header bg-primary">
					<div class="block-options-simple">
							<button class="btn btn-xs btn-warning" onclick="click_expand()"  type="button" ><i class="fa fa-expand"></i></button>
							<a class="btn btn-xs btn-danger" href="{base_url}tpendaftaran_ranap/add_daftar" ><i class="fa fa-plus"></i> Tambah Pendaftaran</a>
						
					</div>
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 10px;">
							<div class="col-md-2">
								<label class="control-label" for="idtipe">TIPE</label>
								<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua  -</option>
									<option value="1" >RAWAT INAP</option>
									<option value="2" >ODS</option>
								</select>
							</div>
							<div class="col-md-2">
								<label class="control-label" for="idtipe_asal">Asal</label>
								<select id="idtipe_asal" name="idtipe_asal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua  -</option>
									<option value="1" >POLIKLINIK</option>
									<option value="2" >IGD</option>
								</select>
							</div>
							<div class="col-md-2">
								<label class="control-label" for="iddokter">Dokter</label>
								<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua Dokter -</option>
									<?foreach($list_dokter as $r){?>
									<option value="<?=$r->id?>"><?=$r->nama?></option>
									<?}?>
									
									
								</select>
							</div>
							<div class="col-md-3">
								<label class="control-label" for="tanggal_1">Tanggal Pendaftaran</label>
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" id="tanggal_1" name="tanggal_1" placeholder="From" value="{tanggal_1}"/>
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" id="tanggal_2" name="tanggal_2" placeholder="To" value="{tanggal_2}"/>
								</div>
							</div>
							<div class="col-md-3">
								<label class="control-label" for="cari_pasien">Pasien</label>
								<div class="input-group">
									<input type="text" class="form-control" id="cari_pasien" placeholder="Nama Pasien | No. Medrec" name="cari_pasien" value="">
									<span class="input-group-btn">
										<button class="btn btn-default" onclick="load_index_all()"  type="button"><i class="fa fa-search"></i> Search</button>
									</span>
								</div>
							</div>
						</div>
				</div>
					
				</div>
				<div class="block-content">
					
					<div class="row pull-10">
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="nopendaftaran">No Pendaftaran</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="nopendaftaran_index" placeholder="No Pendaftaran" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="idruangan_index">Ruangan</label>
								<div class="col-md-9">
									<select id="idruangan_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- Semua Ruangan -</option>
										<?foreach(get_all('mruangan',array('status'=>1,'idtipe' => 1)) as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="idkelas_index">Kelas</label>
								<div class="col-md-9">
									<select id="idkelas_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- Semua Kelas -</option>
										<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="idbed_index">Bed</label>
								<div class="col-md-9">
									<select id="idbed_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- Semua Bed -</option>
									</select>
								</div>
							</div>
							
						</div>
						<div class="col-md-6 push-10">
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">No Rujukan</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="norujukan_index" placeholder="No Rujukan" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="iddokter_perujuk_index">Dokter Perujuk</label>
								<div class="col-md-9">
									<select id="iddokter_perujuk_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- Semua Dokter -</option>
										<?foreach($list_dokter as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
										
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="idkelompokpasien_index">Kelompok Pasien</label>
								<div class="col-md-9">
									<select id="idkelompokpasien_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- Semua -</option>
										<?foreach(get_all('mpasien_kelompok',array('status'=>1)) as $r){?>
											<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="idrekanan_index">Perusahaan</label>
								<div class="col-md-9">
									<select id="idrekanan_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- Semua -</option>
										<?foreach(get_all('mrekanan',array('status'=>1)) as $r){?>
											<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="btn_filter_all"></label>
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="button" onclick="load_index_all()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
								</div>
							</div>
								
						</div>
					</div>
				</div>
			</div>
			
			<?php echo form_close() ?>
			
			<div class="block">
				<ul class="nav nav-pills">
					<li id="div_utama_1" class="<?=($tab_utama=='1'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab_utama(1)"><i class="si si-list"></i> Semua</a>
					</li>
					<li  id="div_utama_2" class="<?=($tab_utama=='2'?'active':'')?>">
						<a href="javascript:void(0)"  onclick="set_tab_utama(2)"><i class="si si-pin"></i> Menunggu Dirawat </a>
					</li>
					<li  id="div_utama_3" class="<?=($tab_utama=='3'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab_utama(3)"><i class="fa fa-bed"></i> Dirawat</a>
					</li>
					<li  id="div_utama_4" class="<?=($tab_utama=='4'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab_utama(4)"><i class="fa fa-times"></i> Dibatalkan</a>
					</li>
					<li  id="div_utama_5" class="<?=($tab_utama=='5'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab_utama(5)"><i class="si si-logout"></i> Pulang</a>
					</li>
					
				</ul>
				<div class="block-content">
						<input type="hidden" id="tab_utama" name="tab_utama" value="{tab_utama}">
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_all">
										<thead>
											<tr>
												<th width="10%">a</th>
												<th width="10%">b</th>
												<th width="10%">c</th>
												<th width="10%">d</th>
												<th width="10%">e</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					
				
				</div>
			</div>

		</div>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?>" id="tab_1">
			<?php echo form_open_multipart('#', 'class="js-validation-bootstrap form-horizontal" id="form_input" ') ?>
			<div class="block">
				<div class="row pull-10">
					<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Tanggal Rencana Masuk</label>
								<div class="col-md-9">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggal_11" name="tanggal_11" placeholder="From" value="{tanggal_1}"/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_12" name="tanggal_12" placeholder="To" value="{tanggal_2}"/>
									</div>
								</div>
							</div>
							
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Ruangan</label>
								<div class="col-md-9">
									<select id="idruangan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- All -</option>
										<?foreach($list_ruang as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
										
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Kelas</label>
								<div class="col-md-9">
									<select id="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- All -</option>
										<?foreach($list_kelas as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
										
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Bed</label>
								<div class="col-md-9">
									<select id="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- All -</option>
										<?foreach($list_bed as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
										
										
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6 push-10">
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Tanggal Input</label>
								<div class="col-md-9">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_input_2" name="tanggal_input_2" placeholder="To" value=""/>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">No Medrec</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="no_medrec_1" placeholder="No. Medrec" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Nama Pasien</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="namapasien_1" placeholder="Nama Pasien" value="">
								</div>
							</div>
							
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="btn_filter_all"></label>
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="button" onclick="getIndex_reservasi()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
								</div>
							</div>
								
						</div>
				</div>
			</div>
			<div class="block">
				<ul class="nav nav-pills">
					<li id="div_1" class="<?=($tab_detail=='1'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab(1)"><i class="si si-list"></i> Semua</a>
					</li>
					<li id="div_2" class="<?=($tab_detail=='2'?'active':'')?>">
						<a href="javascript:void(0)"  onclick="set_tab(2)"><i class="si si-pin"></i> Belum Diproses </a>
					</li>
					<li id="div_3" class="<?=($tab_detail=='3'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab(3)"><i class="fa fa-check-square-o"></i> Telah Diproses</a>
					</li>
					<li id="div_4" class="<?=($tab_detail=='4'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab(4)"><i class="fa fa-times"></i> Batal</a>
					</li>
					
				</ul>
				<div class="block-content">
					<div class="row   ">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-bordered" id="index_ranap">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="8%">Tanggal</th>
											<th width="12%">Pasien</th>
											<th width="10%">Rencana Ruangan</th>
											<th width="5%">Rencana Kelas</th>
											<th width="8%">Rencana Bed</th>
											<th width="10%">Tanggal Rencana</th>
											<th width="12%">DPJP</th>
											<th width="8%">Priority</th>
											<th width="10%">Status</th>
											<th width="15%">Aksi</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_2">
			<?php echo form_open_multipart('#', 'class="js-validation-bootstrap form-horizontal" id="form_input_poli" ') ?>
			<div class="row pull-10">
				<div class="col-md-6">
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
							<div class="col-md-9">
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" id="tanggal_21" name="tanggal_1" placeholder="From" value="{tanggal_1}"/>
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" id="tanggal_22" name="tanggal_2" placeholder="To" value="{tanggal_2}"/>
								</div>
							</div>
						</div>
						
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
							<div class="col-md-9">
								<select tabindex="13" id="idtipe_2" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="#" selected>All</option>
									<option value="1">POLIKLINIK</option>
									<option value="2">IGD</option>
									
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Poliklinik</label>
							<div class="col-md-9">
								<select id="idpoli_2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- All -</option>
									<?foreach($list_poli as $r){?>
									<option value="<?=$r->id?>"><?=$r->nama?></option>
									<?}?>
									
									
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Dokter Perujuk</label>
							<div class="col-md-9">
								<select id="iddokter_2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua Dokter -</option>
									<?foreach(get_all('mdokter',array('status'=>1)) as $r){?>
									<option value="<?=$r->id?>"><?=$r->nama?></option>
									<?}?>
									
									
								</select>
							</div>
						</div>
						
						
						
						
					</div>
					<div class="col-md-6 push-10">
						
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="notransaksi_2" placeholder="No Pendaftaran" name="notransaksi" value="">
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">No Medrec</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="no_medrec_2" placeholder="No. Medrec" name="no_medrec" value="">
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Nama Pasien</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="namapasien_2" placeholder="Nama Pasien" name="namapasien" value="">
							</div>
						</div>
						
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-3 control-label" for="btn_filter_all"></label>
							<div class="col-md-9">
								<button class="btn btn-success text-uppercase" type="button" onclick="getIndexPoli()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
							</div>
						</div>
							
					</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered" id="index_poli">
							<thead>
								<tr>
									<th width="5%">No</th>
									<th width="8%">Action</th>
									<th width="10%">Pendaftaran</th>
									<th width="10%">Tanggal Kunjungan</th>
									<th width="8%">No Medrec</th>
									<th width="20%">Nama Pasien</th>
									<th width="15%">Dokter</th>
									<th width="10%">Asal Pasien</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?>" id="tab_3">
			<?php echo form_open_multipart('#', 'class="js-validation-bootstrap form-horizontal" id="form_input_rencana" ') ?>
			<div class="row pull-10">
				<div class="col-md-6">
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
							<div class="col-md-9">
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" id="tanggal_31" name="tanggal_1" placeholder="From" value="{tanggal_1}"/>
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" id="tanggal_32" name="tanggal_2" placeholder="To" value="{tanggal_2}"/>
								</div>
							</div>
						</div>
						
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
							<div class="col-md-9">
								<select tabindex="13" id="idtipe_3" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="#" selected>All</option>
									<option value="1">POLIKLINIK</option>
									<option value="2">IGD</option>
									
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Poliklinik</label>
							<div class="col-md-9">
								<select id="idpoli_3" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- All -</option>
									<?foreach($list_poli as $r){?>
									<option value="<?=$r->id?>"><?=$r->nama?></option>
									<?}?>
									
									
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Dokter Perujuk</label>
							<div class="col-md-9">
								<select id="iddokter_3" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua Dokter -</option>
									<?foreach(get_all('mdokter',array('status'=>1)) as $r){?>
									<option value="<?=$r->id?>"><?=$r->nama?></option>
									<?}?>
									
									
								</select>
							</div>
						</div>
						
						
						
						
					</div>
					<div class="col-md-6 push-10">
						
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">No Perencanaan</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="notransaksi_3" placeholder="No Perencanaan" name="notransaksi" value="">
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">No Medrec</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="no_medrec_3" placeholder="No. Medrec" name="no_medrec" value="">
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Nama Pasien</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="namapasien_3" placeholder="Nama Pasien" name="namapasien" value="">
							</div>
						</div>
						
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-3 control-label" for="btn_filter_all"></label>
							<div class="col-md-9">
								<button class="btn btn-success text-uppercase" type="button" onclick="getIndexRencana()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
							</div>
						</div>
							
					</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered" id="index_rencana">
							<thead>
								<tr>
									<th width="5%">No</th>
									<th width="8%">Action</th>
									<th width="10%">No Perencanaan</th>
									<th width="10%">Rencana Pelayanan</th>
									<th width="8%">No Pendaftaran</th>
									<th width="15%">Nama Pasien</th>
									<th width="10%">Dokter</th>
									<th width="10%">Asal Pasien</th>
									<th width="10%">Dengan Diagnosa</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
	</div>
</div>
<div class="modal in" id="modal_batal" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_batal',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
									<input type="hidden" id="pendaftaran_id_hapus" value="{pendaftaran_id_hapus}">
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_daftar()" id="btn_hapus"><i class="fa fa-refresh"></i> Simpan Pembatalan</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?
	$waktuterima=date('H:i:s');
	$tanggalterima=date('d-m-Y');
?>
<div class="modal in" id="modal_terima" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Penerimaan Pasien Rawat Inap </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 col-xs-12 push-5 control-label" for="tanggaldaftar">Tanggal Daftar <span style="color:red;">*</span></label>
										<div class="col-md-4 col-xs-6">
											<div class="input-group date">
												<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tanggalterima" placeholder="HH/BB/TTTT" value="<?= $tanggalterima ?>" required>
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
										<div class="col-md-4 col-xs-6">
											<div class="input-group">
												<input tabindex="3" type="text" class="time-datepicker form-control auto_blur" id="waktuterima" name="waktuterima" value="<?= $waktuterima ?>" required>
												<span class="input-group-addon"><i class="si si-clock"></i></span>
											</div>
										</div>
									</div>
									<input type="hidden" id="pendaftaran_id_terima" value="">
								</div>
							</div>
							
						</div>
					</div>
					
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-danger" type="button" onclick="terima_ranap()" id="btn_terima"><i class="fa fa-refresh"></i> Simpan</button>
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<? $this->load->view('Tpendaftaran_poli_ttv/modal_alergi')?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
var tab;
var tab_detail=<?=$tab_detail?>;
var tab_utama=<?=$tab_utama?>;
var st_login;
var st_expand=false;
$(document).ready(function() {
	// click_expand();
    getIndexPoli();
    getIndexRencana();
    getIndex_reservasi();
	load_index_all();
});
function modal_terima(pendaftaran_id_terima){
	$("#pendaftaran_id_terima").val(pendaftaran_id_terima);
	$("#modal_terima").modal('show');
}
function load_index_all(){
	$('#index_all').DataTable().destroy();	
	let tanggal_1=$("#tanggal_1").val();
	let tanggal_2=$("#tanggal_2").val();
	
	
	// alert(ruangan_id);
	table = $('#index_all').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			
			"processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "10%", "targets": 0},
					{ "width": "50%", "targets": 1},
					{ "width": "30%", "targets": 2},
					{ "width": "10%", "targets": 3},
					
				],
            ajax: { 
                url: '{site_url}tpendaftaran_ranap/getIndex_all', 
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						tab:tab_utama,
						iddokter:$("#iddokter").val(),
						idtipe:$("#idtipe").val(),
						idtipe_asal:$("#idtipe_asal").val(),
						cari_pasien:$("#cari_pasien").val(),
						nopendaftaran:$("#nopendaftaran_index").val(),
						idruangan:$("#idruangan_index").val(),
						idkelas:$("#idkelas_index").val(),
						idbed:$("#idbed_index").val(),
						norujukan:$("#norujukan_index").val(),
						iddokter_perujuk:$("#iddokter_perujuk_index").val(),
						idkelompokpasien:$("#idkelompokpasien_index").val(),
						idrekanan:$("#idrekanan_index").val(),
						
					   }
            },
			"drawCallback": function( settings ) {
				 $("#index_all thead").remove();
			 }  
        });
	$("#cover-spin").hide();
}
function click_expand(){
	// 
	if (st_expand==false){
		$("#div_expand").removeClass("block-opt-hidden");
		st_expand=true;
	}else{
		$("#div_expand").addClass("block-opt-hidden");
		st_expand=false;
	}
}
function set_tab($tab) {
    tab_detail = $tab;
    // alert(tab);
    // $("#cover-spin").show();
    document.getElementById("div_1").classList.remove("active");
    document.getElementById("div_2").classList.remove("active");
    document.getElementById("div_3").classList.remove("active");
    document.getElementById("div_4").classList.remove("active");
    if (tab_detail == '1') {
        document.getElementById("div_1").classList.add("active");
    }
    if (tab_detail == '2') {
        document.getElementById("div_2").classList.add("active");
    }
    if (tab_detail == '3') {
        document.getElementById("div_3").classList.add("active");
    }
	if (tab_detail == '4') {
        document.getElementById("div_4").classList.add("active");
    }
	
    getIndex_reservasi();
}
function set_tab_utama($tab) {
    tab_utama = $tab;
    // alert(tab);
    // $("#cover-spin").show();
    document.getElementById("div_utama_1").classList.remove("active");
    document.getElementById("div_utama_2").classList.remove("active");
    document.getElementById("div_utama_3").classList.remove("active");
    document.getElementById("div_utama_4").classList.remove("active");
    document.getElementById("div_utama_5").classList.remove("active");
    if (tab_utama == '1') {
        document.getElementById("div_utama_1").classList.add("active");
    }
    if (tab_utama == '2') {
        document.getElementById("div_utama_2").classList.add("active");
    }
    if (tab_utama == '3') {
        document.getElementById("div_utama_3").classList.add("active");
    }
	if (tab_utama == '4') {
        document.getElementById("div_utama_4").classList.add("active");
    }
	if (tab_utama == '5') {
        document.getElementById("div_utama_5").classList.add("active");
    }
	
    load_index_all();
}

$(document).on("change", "#idkelas", function() {
    get_bed();
});
$(document).on("change", "#ruangan_id", function() {
	$("#idkelas").val("#").trigger('change');
    get_bed();
});
$(document).on("change", "#idruangan_index,#idkelas_index", function() {
    get_bed_index();
});
function get_bed_index(){
	let ruangan_id=$("#idruangan_index").val();
	let idkelas=$("#idkelas_index").val();
	$.ajax({
		url: '{site_url}treservasi_bed/get_bed/',
		dataType: "json",
		type: "POST",
		data:{
			idruangan:ruangan_id,
			idkelas:idkelas,
		},
		success: function(data) {
			$("#idbed_index").empty();
			$('#idbed_index').append('<option value="#" selected>- Semua Bed -</option>');
			$('#idbed_index').append(data.detail);
		}
	});

}
function getIndexPoli() {
    $("#cover-spin").show();
    // alert($("#tanggal_22").val());
    $('#index_poli').DataTable().destroy();
    table = $('#index_poli').DataTable({
		 autoWidth: false,
            searching: true,
            pageLength: 10,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			 "columnDefs": [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,4,5,6,7] },
						 { "width": "5%", "targets": [0]},
						
					],
            ajax: { 
                url: '{site_url}tpendaftaran_ranap/getIndexPoli',
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_1:$("#tanggal_21").val(),
						tanggal_2:$("#tanggal_22").val(),
						idtipe:$("#idtipe_2").val(),
						idpoli:$("#idpoli_2").val(),
						iddokter:$("#iddokter_2").val(),
						notransaksi:$("#notransaksi_2").val(),
						no_medrec:$("#no_medrec_2").val(),
						namapasien:$("#namapasien_2").val(),
						
					   }
            },
			"drawCallback": function(settings) {
				$("#cover-spin").hide();
			}
			
    });
    $("#cover-spin").hide();
}
function getIndexRencana() {
   
    $("#cover-spin").show();
    // alert(ruangan_id);
    $('#index_rencana').DataTable().destroy();
    table = $('#index_rencana').DataTable({
		 autoWidth: false,
            searching: true,
            pageLength: 10,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			 "columnDefs": [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,4,5,6,7] },
						 { "width": "5%", "targets": [0]},
						
					],
            ajax: { 
                url: '{site_url}tpendaftaran_ranap/getIndexRencana',
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_1:$("#tanggal_31").val(),
						tanggal_2:$("#tanggal_32").val(),
						idtipe:$("#idtipe_3").val(),
						idpoli:$("#idpoli_3").val(),
						iddokter:$("#iddokter_3").val(),
						notransaksi:$("#notransaksi_3").val(),
						no_medrec:$("#no_medrec_3").val(),
						namapasien:$("#namapasien_3").val(),
						
					   }
            },
			"drawCallback": function(settings) {
				$("#cover-spin").hide();
			}
			
    });
    $("#cover-spin").hide();
}
function getIndex_reservasi() {
    $('#index_ranap').DataTable().destroy();
    let idkelas = $("#idkelas").val();
    let idruangan = $("#idruangan").val();
    let idbed = $("#idbed").val();
    $("#cover-spin").show();
    // alert(ruangan_id);
    table = $('#index_ranap').DataTable({
        autoWidth: false,
        searching: true,
        serverSide: true,
        "processing": true,
        "order": [],
        "pageLength": 10,
        "ordering": false,
         "columnDefs": [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,4,5,6,7,8,9,10] },
						 { "width": "5%", "targets": [0]},
						
					],
        ajax: {
            url: '{site_url}tpendaftaran_ranap/getIndex_reservasi',
            type: "POST",
            dataType: 'json',
            data: {
                idbed: idbed,
                idkelas: idkelas,
                idruangan: idruangan,
                tab_detail: tab_detail,
				no_medrec:$("#no_medrec_1").val(),
				namapasien:$("#namapasien_1").val(),
				tanggal_1:$("#tanggal_11").val(),
				tanggal_2:$("#tanggal_12").val(),
				tanggal_input_1:$("#tanggal_input_1").val(),
				tanggal_input_2:$("#tanggal_input_2").val(),
            },
			
        },
        "drawCallback": function(settings) {
            // $("#index_ranap thead").remove();
            $("#cover-spin").hide();
        }
    });
    $("#cover-spin").hide();
}
function batal(id){
	swal({
		title: "Anda Yakin ?",
		text : "Membatalkan Pendaftaran ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/batal', 
			dataType: "JSON",
			method: "POST",
			data : {
					id :  id,
				},
			success: function(data) {
				$("#cover-spin").hide();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Proses Berhasil Disimpan'});
				$('#index_ranap').DataTable().ajax.reload( null, false );
			
			}
		});
	});
}
function show_modal_batal(id){
	$("#modal_batal").modal('show');
	$("#pendaftaran_id_hapus").val(id);
}
function hapus_record_daftar(){
	let id=$("#pendaftaran_id_hapus").val();
	let keterangan_hapus=$("#keterangan_hapus").val();
	let alasan_id=$("#alasan_id").val();
	
	
	if (alasan_id=='' || alasan_id==null){
		swal({
			title: "Gagal!",
			text: "Tentukan Alasan.",
			type: "error",
			timer: 1000,
			showConfirmButton: false
		});

		return false;
	}
	$("#modal_hapus").modal('hide');
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Membatalkan Pendaftaran ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap/hapus_record_daftar', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					keterangan_hapus:keterangan_hapus,
					alasan_id:alasan_id,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Hapus TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#modal_batal").modal('hide');
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					load_index_all();
				}
			}
		});
	});

}
function terima_ranap(){
	let id=$("#pendaftaran_id_terima").val();
	let tanggalterima=$("#tanggalterima").val();
	let waktuterima=$("#waktuterima").val();
	
	$("#modal_terima").modal('hide');
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menerima Rawat Inap?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap/terima_ranap', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					tanggalterima:tanggalterima,
					waktuterima:waktuterima,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Hapus TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#modal_terima").modal('hide');
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					load_index_all();
				}
			}
		});
	});

}
function get_bed(){
	let ruangan_id=$("#idruangan").val();
	let idkelas=$("#idkelas").val();
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/get_bed/',
		dataType: "json",
		type: "POST",
		data:{
			idruangan:ruangan_id,
			idkelas:idkelas,
		},
		success: function(data) {
			$("#idbed").empty();
			$('#idbed').append('<option value="#" selected>- All Bed -</option>');
			$('#idbed').append(data.detail);
		}
	});

}


</script>
