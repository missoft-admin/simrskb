<?if($st_sp=='1'){?>
		<div class="row">
			<div class="col-md-12">
			<div class="col-md-12">
				<table>
					<tr>
						<td width="100%" class="text-center"><img class="img-avatar" style="width:100px;height:100px" id="output_img" src="{upload_path}logo_setting/<?=($logo_form_sp?$logo_form_sp:'no_image.png')?>" /></td>
					</tr>
					<tr>
						<td width="100%" class="text-center"><?=$alamat_form_sp?><br><?=$telepone_form_sp?><br><?=$email_form_sp?></td>
					</tr>
					<tr>
						<td width="100%" class="text-center text-header"><strong><?=$judul_ina_sp?><br><i><?=$judul_eng_sp?></i></strong></td>
					</tr>
					
				</table>
				
				<div class="row">
					<table>
						<tr>
							<td colspan="6" class=""><strong>{paragraf_1_ina_sp}</strong> <br><i class="">{paragraf_1_eng_sp}</i></td>
						</tr>
						<tr>
							<td colspan="6" class="text-primary h5"><strong>{label_header_pj_ina_sp}</strong> <br><i class="">{label_header_pj_eng_sp}</i></td>
						</tr>
						
						<tr>
							<td style="width:13%" class="text-bold">{nama_pj_ina_sp}<br><i class="text-muted">{nama_pj_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: {namapenanggungjawab}</td>
							<td style="width:13%" class="text-bold">{ttl_pj_ina_sp}<br><i class="text-muted">{ttl_pj_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=HumanDateShort($tanggal_lahirpenanggungjawab)?></td>
							<td style="width:13%" class="text-bold">{umur_pj_ina_sp}<br><i class="text-muted">{umur_pj_eng_sp}</i></td><td style="width:20%" class="font-s12 ">:  {umur_tahunpenanggungjawab} Tahun {umur_bulanpenanggungjawab} Bulan {umur_haripenanggungjawab} Hari</td>
						</tr>
						<tr>
							<td style="width:13%" class="text-bold">{pekerjaan_pj_ina_sp}<br><i class="text-muted">{pekerjaan_pj_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=get_nama_ref($pekerjaannpenanggungjawab,6)?></td>
							<td style="width:13%" class="text-bold">{pendidikan_pj_ina_sp}<br><i class="text-muted">{pendidikan_pj_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=get_nama_ref($pendidikanpenanggungjawab,13)?></td>
							<td style="width:13%" class="text-bold">{agama_pj_ina_sp}<br><i class="text-muted">{agama_pj_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=get_nama_ref($agama_idpenanggungjawab,3)?></td>
						</tr>
						<tr>
							<td style="width:13%" class="text-bold">{alamat_pj_ina_sp}<br><i class="text-muted">{alamat_pj_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: {alamatpenanggungjawab}</td>
							<td style="width:13%" class="text-bold">{provinsi_pj_ina_sp}<br><i class="text-muted">{provinsi_pj_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=get_nama_kota($provinsi_idpenanggungjawab)?></td>
							<td style="width:13%" class="text-bold">{kab_pj_ina_sp}<br><i class="text-muted">{kab_pj_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=get_nama_kota($kabupaten_idpenanggungjawab)?></td>
						</tr>
						<tr>
							<td style="width:13%" class="text-bold">{kec_pj_ina_sp}<br><i class="text-muted">{kec_pj_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=get_nama_kota($kecamatan_idpenanggungjawab)?></td>
							<td style="width:13%" class="text-bold">{kel_pj_ina_sp}<br><i class="text-muted">{kel_pj_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=get_nama_kota($kelurahan_idpenanggungjawab)?></td>
							<td style="width:13%" class="text-bold">{kode_pos_pj_ina_sp}<br><i class="text-muted">{kode_pos_pj_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: {kodepospenanggungjawab}</td>
						</tr>
						<tr>
							<td style="width:13%" class="text-bold">{rw_pj_ina_sp}<br><i class="text-muted">{rw_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: {rwpenanggungjawab}</td>
							<td style="width:13%" class="text-bold">{rt_pj_ina_sp}<br><i class="text-muted">{rt_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: {rtpenanggungjawab}</td>
							<td style="width:13%" class="text-bold">{telephone_ina_sp}<br><i class="text-muted">{telephone_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: {teleponpenanggungjawab}</td>
						</tr>
						<tr>
							<td style="width:13%" class="text-bold">{nik_pj_ina_sp}<br><i class="text-muted">{nik_pj_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: {noidentitaspenanggungjawab}</td>
							<td style="width:13%" class="text-bold">{hubungan_pj_ina_sp}<br><i class="text-muted">{hubungan_pj_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=get_nama_ref($hubunganpenanggungjawab,9)?></td>
							<td style="width:13%" class="text-bold"></td><td style="width:20%" class="font-s12 "></td>
						</tr>
						<tr>
							<td colspan="6" class=""><strong>{paragraf_2_ina_sp}</strong> <br><i class="">{paragraf_2_eng_sp}</i></td>
						</tr>
						<tr>
							<td colspan="6" class="text-primary h5"><strong>{header_ina_sp}</strong> <br><i class="">{header_eng_sp}</i></td>
						</tr>
						<tr>
							<td style="width:13%" class="text-bold">{no_register_ina_sp}<br><i class="text-muted">{no_register_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: {nopendaftaran}</td>
							<td style="width:13%" class="text-bold">{no_rekam_medis_ina_sp}<br><i class="text-muted">{no_rekam_medis_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: {no_medrec}</td>
							<td style="width:13%" class="text-bold">{nama_pasien_ina_sp}<br><i class="text-muted">{nama_pasien_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: {namapasien}</td>
						</tr>
						<tr>
							<td style="width:13%" class="text-bold">{ttl_ina_sp}<br><i class="text-muted">{ttl_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=HumanDateShort($tanggal_lahir)?></td>
							<td style="width:13%" class="text-bold">{umur_ina_sp}<br><i class="text-muted">{umur_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: {umurtahun} Tahun {umurbulan} Bulan {umurhari} Hari</td>
							<td style="width:13%" class="text-bold">{jk_ina_sp}<br><i class="text-muted">{jk_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=get_nama_ref($jenis_kelamin,1)?></td>
						</tr>
						<tr>
							<td style="width:13%" class="text-bold">{pekerjaan_ina_sp}<br><i class="text-muted">{pekerjaan_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=get_nama_ref($pekerjaan,6)?></td>
							<td style="width:13%" class="text-bold">{pendidikan_ina_sp}<br><i class="text-muted">{pendidikan_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=get_nama_ref($pendidikan,13)?></td>
							<td style="width:13%" class="text-bold">{agama_ina_sp}<br><i class="text-muted">{agama_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=get_nama_ref($agama_id,3)?></td>
						</tr>
						<tr>
							<td style="width:13%" class="text-bold">{alamat_ina_sp}<br><i class="text-muted">{alamat_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: {alamatpasien}</td>
							<td style="width:13%" class="text-bold">{provinsi_ina_sp}<br><i class="text-muted">{provinsi_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=get_nama_kota($provinsi_id)?></td>
							<td style="width:13%" class="text-bold">{kab_ina_sp}<br><i class="text-muted">{kab_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=get_nama_kota($kabupaten_id)?></td>
						</tr>
						<tr>
							<td style="width:13%" class="text-bold">{kec_ina_sp}<br><i class="text-muted">{kec_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=get_nama_kota($kecamatan_id)?></td>
							<td style="width:13%" class="text-bold">{kel_ina_sp}<br><i class="text-muted">{kel_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=get_nama_kota($kelurahan_id)?></td>
							<td style="width:13%" class="text-bold">{kode_pos_ina_sp}<br><i class="text-muted">{kode_pos_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: {kodepos}</td>
						</tr>
						<tr>
							<td style="width:13%" class="text-bold">{rw_ina_sp}<br><i class="text-muted">{rw_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: {rw}</td>
							<td style="width:13%" class="text-bold">{rt_ina_sp}<br><i class="text-muted">{rt_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: {rt}</td>
							<td style="width:13%" class="text-bold">{wn_ina_sp}<br><i class="text-muted">{wn_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=get_nama_ref($warganegara,18)?></td>
						</tr>
						<tr>
							<td style="width:13%" class="text-bold">{telephone_ina_sp}<br><i class="text-muted">{telephone_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: {telepon}</td>
							<td style="width:13%" class="text-bold">{nohp_ina_sp}<br><i class="text-muted">{nohp_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: {nohp}</td>
							<td style="width:13%" class="text-bold">{nik_ina_sp}<br><i class="text-muted">{nik_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: {nik}</td>
						</tr>
						
						
						<tr>
							<td colspan="6" class="text-primary h5"><strong>{detail_pendaftaran_ina_sp}</strong> <br><i class="">{detail_pendaftaran_eng_sp}</i></td>
						</tr>
						
						<tr>
							<td style="width:13%" class="text-bold">{tipe_pasien_ina_sp}<br><i class="text-muted">{tipe_pasien_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=($idtipe=='1'?'RAWAT INAP':'ODS')?></td>
							<td style="width:13%" class="text-bold">{kel_pasien_ina_sp}<br><i class="text-muted">{kel_pasien_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=$nama_kelompok_pasien ?></td>
							<td style="width:13%" class="text-bold">{dpjp_utama_ina_sp}<br><i class="text-muted">{dpjp_utama_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=$nama_dokter_dpjp ?></td>
						</tr>
						
						<tr>
							<td style="width:13%" class="text-bold">{ruangan_ina_sp}<br><i class="text-muted">{ruangan_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=$nama_ruangan ?></td>
							<td style="width:13%" class="text-bold">{kelas_ina_sp}<br><i class="text-muted">{kelas_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=$nama_kelas ?></td>
							<td style="width:13%" class="text-bold">{bed_ina_sp}<br><i class="text-muted">{bed_eng_sp}</i></td><td style="width:20%" class="font-s12 ">: <?=$nama_bed ?></td>
						</tr>
						<tr>
							<td colspan="6" class=""><strong>{paragraf_3_ina_sp}</strong> <br><i class="">{paragraf_3_eng_sp}</i></td>
						</tr>
					</table>
					
				</div>
				</div>
			</div>
			<div class="col-md-12">
			
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-12">
					<table>
						<tr>
							<td style="width:67%">&nbsp;</td>
							<td style="width:33%" class="text-bold text-center">{ttd_pasien_ina_sp}<br><i class="text-muted">{ttd_pasien_eng_sp}</i></td>
						</tr>
						<tr>
							
							<td style="width:67%">&nbsp;</td>
							<td style="width:33%" class="text-bold text-center">
										<?if ($ttd_sp){?>
											<div class="img-container fx-img-rotate-r text-center">
												<img class="" style="width:120px;height:120px; text-align: center;" src="<?=$ttd_sp?>" alt="" title="">
												<div class="img-options">
													<div class="img-options-content">
														<div class="btn-group btn-group-sm">
															<a class="btn btn-default" onclick="modal_ttd_sp()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
															<a class="btn btn-default btn-danger" onclick="hapus_ttd_sp()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
														</div>
													</div>
												</div>
											</div>
										<?}else{?>
											<button class="btn btn-sm btn-success" onclick="modal_ttd_sp()" id="btn_ttd_sp" type="button"><i class="fa fa-paint-brush"></i> Tanda Tangan</button>
										<?}?>
								<input class="form-control" readonly name="ttd_sp" id="ttd_sp" value="{ttd_sp}" type="hidden">
								
								</td>
						</tr>
						<tr>
							<td style="width:67%">&nbsp;</td>
							<td style="width:33%" class="text-bold text-center text-bold"><i class="text-bold">{namapenanggungjawab}</i></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="modal-footer">
					<button class="btn btn-sm btn-danger " type="submit" name="btn_simpan"  value="2"><i class="fa fa-save"></i> Update & Keluar</button>	
				<?if ($st_pernyataan=='0'){?>
					<?if ($st_gc=='1' && $st_general=='0'){?>
						<button class="btn btn-sm btn-primary btn_sp" <?=($ttd_sp==''?'disabled':'')?> type="submit" name="btn_simpan"  value="4"><i class="fa fa-arrow-right"></i> Lanjut General Consent</button>
					<?}else{?>
						<?if($st_hk=='1' && $st_hak=='0'){?>
						<button class="btn btn-sm btn-primary btn_sp" <?=($ttd_sp==''?'disabled':'')?> type="submit" name="btn_simpan"  value="5"><i class="fa fa-arrow-right"></i> Lanjut Hak dan Kewajiban</button>
						<?}else{?>
						<button class="btn btn-sm btn-success btn_sp" <?=($ttd_sp==''?'disabled':'')?> type="submit" name="btn_simpan"  value="41"><i class="fa fa-arrow-right"></i> Selesai </button>
						
						<?}?>
					<?}?>
				<?}else{?>
					<a href="{base_url}tpendaftaran_ranap" class="btn btn-sm btn-default" name="btn_simpan" value="2" type="button" ><i class="fa fa-reply"></i> Kembali</a>
				<?}?>
			</div>
		</div>
		
	
	<?}?>

