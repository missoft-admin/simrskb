<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>
<?
	$this->load->view('Tstockopname/tombol');
?>
<div class="block">
    <div class="block-header">

        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:25px">
        <div class="row">
            <?php echo form_open('tstockopname/filter', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Unit Pelayanan</label>
                    <div class="col-md-8">
                        <select id="iddariunit" name="iddariunit" class="form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#" <?=($iddariunit=='#'?'selected':'')?>>-Silahkan Pilih Unit-</option>
							<?php if ($list_unitpelayanan):?>
							<?foreach($list_unitpelayanan as $row){?>
                            <option value="<?=$row->id;?>" <?=($iddariunit==$row->id)?'selected':''?>><?=$row->nama;?></option>
                            <?php }?>
                            <?php endif;?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Periode</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <select id="bulan" name="bulan" class="form-control" style="width: 100%;" data-placeholder="Choose one..">
							</select>

                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-4 control-label">Kode SO</label>
                    <div class="col-md-8">
                        <input class="form-control" readonly type="text" id="nostockopname"  name="nostockopname" value="{nostockopname}" />
                        <input class="form-control" readonly type="hidden" id="id"  name="id" value="" />
                    </div>
                </div>

                <div class="form-group" style="margin-bottom: 5px;" id="div_new">
					<?php if (UserAccesForm($user_acces_form,array('268'))){ ?>
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" disabled type="button" id="btn_new" name="btn_new" style="font-size:13px;width:100%;float:right;"><i class="fa fa-plus"></i> Membuat Stok Opname</button>
                    </div>
					<?}?>
                </div>

				<div class="form-group" style="margin-bottom: 5px;" id="div_edit" hidden>
                    <label class="col-md-4 control-label" for=""></label>
					<?php if (UserAccesForm($user_acces_form,array('270'))){ ?>
                    <div class="col-md-4">

                        <button class="btn btn-danger text-uppercase" type="button" id="btn_save" name="btn_save" style="font-size:13px;width:100%;float:right;"><i class="fa fa-save"></i> Simpan / Selesai </button>
                    </div>
					<?}?>
					<?php if (UserAccesForm($user_acces_form,array('269'))){ ?>
					<div class="col-md-4">
                        <button class="btn btn-warning text-uppercase" type="button" id="btn_pause" name="btn_pause" style="font-size:13px;width:100%;float:right;"><i class="fa fa-pause"></i> Pause</button>
                    </div>
					<?}?>
                </div>
            </div>
			<div class="col-md-6">

                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label">Tanggal Create SO</label>
                    <div class="col-md-4">
                        <input class="js-datepicker form-control" type="text" id="tgl_so" data-date-format="dd-mm-yyyy" value="<?= $tgl_so ?>">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label">Tanggal Input SO</label>
                    <div class="col-md-4">
                        <input class="js-datepicker form-control" type="text" id="tgl_input" data-date-format="dd-mm-yyyy" value="<?= date("d-m-Y", strtotime($tgl_input)) ?>">
                    </div>
                </div>
            </div>
			<div class="col-md-12" id="progress" hidden>
				<div class="alert alert-danger alert-dismissable">
					<h5 class="font-w300 push-15">Proses Update Stok</h5>
					<i class="fa fa-2x fa-cog fa-spin text-danger"></i>
					Proses Update database <a class="alert-link" href="javascript:void(0)">...</a>
				</div>
			</div>
				<hr style="margin-top:25px">
            <?php echo form_close(); ?>

        </div>
    </div>

    <div class="block-content" id="div_info" hidden>
		<div class="row">

			<div class="col-sm-12">
				<!-- Warning Alert -->
				<div class="alert alert-warning alert-dismissable">
					<h4 class="font-w300 push-15">Info</h4>
					<p id="lbl_info"></p>
				</div>
				<!-- END Warning Alert -->
			</div>

		</div>
	</div>

</div>

<div class="block" id="div_proses" hidden>
    <div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('271'))){ ?>
        <div class="block-options-simple">
			<a href="{site_url}tstockopname/format_data/1" target="_blank" id="btn_download" class="btn btn-xs btn-primary" type="button"><i class="si si-doc"></i> Download Format</a>
		</div>
		<?}?>
        <h3 class="block-title">Proses Stok Opname</h3>
		<hr style="margin-top:15px">
        <div class="row">
            <?php echo form_open('tstockopname/filter', 'class="form-horizontal" id="form"'); ?>
            <div class="col-md-12">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="distributor">Tipe</label>
                    <div class="col-md-5">
                        <select id="idtipe" name="idtipe" class="form-control" style="width: 100%;" data-placeholder="Choose one..">

                        </select>
                    </div>
                </div>

            </div>
			<div class="col-md-12">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="distributor">Kategori</label>
                    <div class="col-md-5">
                        <select id="idkategori" name="idkategori" class="form-control" style="width: 100%;" data-placeholder="Choose one..">

                        </select>
                    </div>
                </div>

            </div>
			<div class="col-md-12">

                <div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-2 control-label">Nama Barang</label>
                    <div class="col-md-3">
                        <input class="form-control" placeholder="Filter By Nama Barang" type="text" id="nama_barang"  value="">
                    </div>
					<div class="col-md-2" >
                        <button class="btn btn-success text-uppercase" type="button" id="btn_filter" name="btn_filter" style="font-size:13px;width:100%;float:left;"><i class="fa fa-filter"></i> Filter </button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>

    <div class="block-content" id="div_tabel">
        <table class="table table-bordered table-striped" id="index_list" style="width: 100%;">
            <thead>
                <tr>
                    <th style="width:5%">#</th>
                    <th style="width:5%">No</th>
                    <th style="width:15%">Nama Barang</th>
                    <th style="width:5%">Tipe</th>
                    <th style="width:5%">Stok Trx</th>
                    <th style="width:5%">Stok Create</th>
                    <th style="width:5%">Stok Fisik</th>
                    <th style="width:5%">Selisih</th>
                    <th style="width:15%">Catatan</th>
                    <th style="width:10%">HPP</th>
                    <th style="width:10%">Nilai</th>
                    <th style="width:10%">Nilai Selisih</th>
                    <th style="width:10%">Status</th>
                    <th style="width:15%" align="center">
						<div class="btn-group" role="group">
							<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu dropdown-menu-right">
								<li class="dropdown-header">Pilih Action</li>
								<li>
									<a tabindex="-1" class="sesuai_semua" href="javascript:void(0)">Sesuai Semua</a>
								</li>

							</ul>
						</div>

					</th>
                    <th style="width:0%">so_id</th>
                    <th style="width:0%">idstok</th>
                    <th style="width:0%">idunitpelayanan</th>
                    <th style="width:0%">idtipe</th>
                    <th style="width:0%">idbarang</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>


</div>
<div class="modal" id="modalEdit" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title" id='title_barang'>Verifikasi Barang Tidak Sesuai</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tipe Barang</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" readonly id="xnamatipe">
                                    </div>
                                </div>


							</div>
							<div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Nama Barang</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" readonly id="xnamabarang">
                                    </div>
                                </div>
							</div>
							<div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Stok Sistem Create SO</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control number" readonly id="xstoksistem">
                                    </div>
									<label class="col-md-2 control-label">Stok Sistem Real</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" readonly id="xsistemreal">
                                    </div>
                                </div>
							</div>
							<div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Stok Fisik</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control number"  id="xstokfisik">
                                    </div>
									<label class="col-md-2 control-label">Selisih</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control number" readonly id="xselisih">
                                    </div>
                                </div>
							</div>
							<div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">HPP</label>
                                    <div class="col-md-3">
                                        <input type="text" readonly class="form-control number"  id="xhpp">
                                    </div>
									<label class="col-md-2 control-label">Nilai</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control number" readonly id="xnilai">
                                    </div>
                                </div>
							</div>
							<div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Nilai Selisih</label>
                                    <div class="col-md-3">
                                        <input type="text" readonly class="form-control number"  id="xnilaiselisih">
                                    </div>

                                </div>
							</div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Catatan</label>
                                    <div class="col-md-9">
										<textarea class="form-control js-summernote-custom-height" id="xcatatan" placeholder="Catatan" name="xcatatan" rows="4"></textarea>
                                    </div>
                                </div>
							</div>

                       </div>
					   <input type="hidden" id="xid_so">
					   <input type="hidden" id="xidstok">
					   <input type="hidden" id="xidunit">
					   <input type="hidden" id="xidtipe">
					   <input type="hidden" id="xidbarang">
                    </form>
                </div>
				 <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-sm btn-success" id="btn_update">Update</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="modalKS" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title" id='tittle_ks'>Kartu Stok</h3>

                </div>


                <div class="block-content">
                    <div class="row">
						<?php echo form_open('#', 'class="form-horizontal" id="form"'); ?>
						<div class="col-md-12">
							<label class="col-md-2 control-label" for="tanggal">Tanggal</label>
							<div class="col-md-8">
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" id="tanggaldari"  name="tanggaldari" placeholder="From" value=""/>
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" id="tanggalsampai" name="tanggalsampai" placeholder="To" value=""/>
								</div>
							</div>
							<div class="col-md-2">
								<button class="btn btn-success text-uppercase" type="button" id="btn_cari" name="btn_cari" style="font-size:13px;width:100%;float:left;"><i class="fa fa-filter"></i> Filter </button>
							</div>
							<input type="hidden" id="cidtipe">
							<input type="hidden" id="cidbarang">
							<input type="hidden" id="cidunit">
						</div>

						<?php echo form_close(); ?>
					</div>
					<hr style="margin-top:15px">
					<table class="table table-bordered table-striped" id="index_ks" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:5%">No</th>
								<th style="width:10%">Tanggal</th>
								<th style="width:10%">No Transaksi</th>
								<th style="width:40%">Keterangan</th>
								<th style="width:10%">Masuk</th>
								<th style="width:10%">Keluar</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
                </div>
				 <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
    var table;
    $(document).ready(function() {
        $("#gudangtipe").select2();
        $("#iddariunit").select2();
        $("#idkategori").select2();
        // $("#tahun").select2();
        $("#bulan").select2();
        $("#idtipe").select2();
		// $('#modalEdit').show();
        $(".number").number(true,0,'.',',');
		$("#tgl_so").attr('disabled',true);
		initial();

    });

	$("#iddariunit").change(function(){
		// alert($(this).val());
		if ($(this).val()!='#'){
			initial();
		}else{
			hide_all();
		}

	});
	$("#idtipe").change(function(){
		load_kategori();
	});
	function hide_all(){
		$("#id").val('');
		$("#btn_new").attr("disabled", 'disabled');
		$("#div_edit").hide();
		// $("#div_tabel").hide();
		$("#div_info").hide();
		$("#div_proses").hide();
	}
	$(document).on("click", "#btn_new", function() {
		swal({
			title: "Anda Yakin ?",
			text : "Membuat dan Memulai proses stockopname "+$("#iddariunit option:selected").text()+"?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			create_so();
		});
	});
	$(document).on("click", "#btn_pause", function() {
		swal({
			title: "Anda Yakin ?",
			text : "Menunda / PAUSE proses stockopname "+$("#iddariunit option:selected").text()+"?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			pause_so();
		});
	});


	function pause_so(){
		var id=$("#id").val();

		$.ajax({
			url: '{site_url}tstockopname/pause_so',
			type: 'POST',
			data: {id: id},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Menunda stockopname'});
				initial();
			}
		});
	}
	$(document).on("click", "#btn_save", function() {
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Memproses stockopname "+$("#iddariunit option:selected").text()+"? Setelah diproses data stokopname secara otomatis akan berpindah kehalaman history",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			selesai_so();
		});
	});


	function selesai_so(){
		var id=$("#id").val();
		hide_all();
		$("#progress").show();
		$.ajax({
			url: '{site_url}tstockopname/selesai_so',
			type: 'POST',
			data: {id: id},
			complete: function() {
				$("#progress").hide();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Selesai stockopname'});
				initial();
			}
		});
	}
	function create_so(){
		var iddariunit=$("#iddariunit").val();
		var tgl_input=$("#tgl_input").val();
		var bulan=$("#bulan").val();
		$.ajax({
			url: '{site_url}tstockopname/create_so',
			type: 'POST',
			data: {iddariunit: iddariunit,tgl_input: tgl_input,bulan: bulan},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : 'memulai stockopname'});
				initial();
			}
		});
	}
	function initial(){
		var iddariunit=$("#iddariunit").val();
		// alert(iddariunit);
		if (iddariunit!='#'){
		$.ajax({
			url: '{site_url}tstockopname/cari_so/'+iddariunit,
			dataType: "JSON",
			method: "GET",
			success: function(data) {
				// console.log(data);
				if (data.status=='0'){//CREATE
					hide_all();
					$("#btn_new").removeAttr('disabled');
					$("#div_new").show();
				}else if(data.status=='1'){//RUNNING
					hide_all();
					$("#btn_download").attr("href", "{site_url}tstockopname/format_data/"+data.id);
					$("#div_edit").show();
					$("#div_proses").show();
					load_tipe();
				}else{//PAUSE
					hide_all();
					$("#div_info").show();
					$("#lbl_info").text('NO SO : '+data.nostockopname+'  Sedang diPAUSE, Silahkan Pilih Menu History Untuk Melanjutkan.');
				}
				$("#id").val(data.id);
				loadPeriode($("#id").val());
				$("#tgl_so").attr('disabled',false);
				$("#tgl_so").val(data.tgl_so);

				$("#tgl_input").val(data.tgl_input);
				$("#nostockopname").val(data.nostockopname);
				$("#tgl_so").attr('disabled',true);
			}
		});
		}else{
			hide_all();
		}
	}
	function loadPeriode($id){
		var id=$id;
		// alert(id);
		$('#bulan').find('option').remove();
		$.ajax({
			url: '{site_url}tstockopname/list_periode/'+id,
			dataType: "json",
			success: function(data) {
				$.each(data, function (i,row) {
					console.log(data);
				$('#bulan')
				.append('<option value="' + row.id + '">' + row.text + '</option>');
				});
			}
		});
	}
	function clear_data(){

		$('#index_list').DataTable().destroy();
		$('#index_list tbody').empty();
		var table = $('#index_list').DataTable( {
			"searching": false,
			columnDefs: [{ targets: [0, 14,15,16,17,18], visible: false}]
		} );
	}
	function load_tipe(){
		clear_data();
		var idunit=$("#iddariunit").val();
		$('#idtipe').find('option').remove();

		$('#idkategori').find('option').remove();
		$('#idkategori').append('<option value="#" selected>- Pilih Semua -</option>');
		var def='';
		if (idunit !='#'){
				$('#idtipe').append('<option value="#" selected>- Pilih Semua -</option>');

				$.ajax({
					url: '{site_url}tstockopname/list_tipe/'+idunit,
					dataType: "json",
					success: function(data) {
						$.each(data, function (i,row) {
						$('#idtipe')
						.append('<option value="' + row.id + '">' + row.text + '</option>');
						});
					}
				});

		}
	}
	function load_kategori(){
		var idtipe=$("#idtipe").val();
		$('#idkategori').find('option').remove();
		$('#idkategori').append('<option value="#" selected>- Pilih Semua -</option>');
		if (idtipe !='#'){

				$.ajax({
					url: '{site_url}tstockopname/list_kategori/'+idtipe,
					dataType: "json",
					success: function(data) {
						$.each(data, function (i,row) {

						$('#idkategori')
						.append('<option value="' + row.path + '">' + TreeView(row.level, row.nama) + '</option>');
						});
					}
				});

		}
	}
	function TreeView($level, $name)
	{
		// console.log($name);
		$indent = '';
		for ($i = 0; $i < $level; $i++) {
			if ($i > 0) {
				$indent += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			} else {
				$indent += '';
			}
		}

		if ($i > 0) {
			$indent += '└─';
		} else {
			$indent += '';
		}

		return $indent + ' ' +$name;
	}
	$(document).on("click", "#btn_filter", function() {
		loadBarang();
	});
	$(document).on("click", "#btn_cari", function() {
		loadKS();
	});
	$(document).on("click", ".tidak", function() {
	    table = $('#index_list').DataTable()
		var tr = $(this).parents('tr')
		var nama_barang = table.cell(tr,2).data()
		var tipebarang = table.cell(tr,3).data()

		var id_so = table.cell(tr,14).data()
		var idstok = table.cell(tr,15).data()
		var idunit = table.cell(tr,16).data()
		var idtipe = table.cell(tr,17).data()
		var idbarang = table.cell(tr,18).data()

		$("#xid_so").val(id_so);
		$("#xidstok").val(idstok);
		$("#xidunit").val(idunit);
		$("#xidtipe").val(idtipe);
		$("#xidbarang").val(idbarang);

		$("#xstoksistem").val(remove_class(table.cell(tr,5).data()));
		$("#xsistemreal").val(remove_class(table.cell(tr,4).data()));
		$("#xhpp").val(remove_class(table.cell(tr,9).data()));
		$("#xselisih").val(remove_class(table.cell(tr,7).data()));
		$("#xnilai").val(remove_class(table.cell(tr,10).data()));
		$("#xnilaiselisih").val(remove_class(table.cell(tr,11).data()));


		$("#xnamatipe").val(tipebarang);
		$("#xnamabarang").val(nama_barang);
		$("#xstokfisik").val(remove_class(table.cell(tr,6).data()));
		$("#xcatatan").summernote("code",table.cell(tr,8).data());
		$('#modalEdit').modal('show');
		$("#xstokfisik").focus();
		// alert(id_so);
	});
	$(document).on("click", ".ks", function() {
		 table = $('#index_list').DataTable()
		var tr = $(this).parents('tr')
		var nama_barang = table.cell(tr,2).data()

		var idunit = table.cell(tr,16).data()
		var idtipe = table.cell(tr,17).data()
		var idbarang = table.cell(tr,18).data()
		$("#cidtipe").val(idtipe);
		$("#cidbarang").val(idbarang);
		$("#cidunit").val(idunit);
		$("#tittle_ks").text('Kartu Stok '+nama_barang);
		loadKS();

		$('#modalKS').modal('show');
	});
	$(document).on("click", ".sesuai_semua", function() {
		swal({
			title: "Anda Yakin ?",
			text : "Sudah Mengecek kesesuaian Data ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			sesuai_semua();
		});
	});
	$(document).on("click", ".sesuai", function() {
		table = $('#index_list').DataTable()
		var tr = $(this).parents('tr')
		var nama_barang = table.cell(tr,2).data()
		var tipebarang = table.cell(tr,3).data()

		var id_so = table.cell(tr,14).data()
		var idstok = table.cell(tr,15).data()
		var idunit = table.cell(tr,16).data()
		var idtipe = table.cell(tr,17).data()
		var idbarang = table.cell(tr,18).data()
		var stokfisik =remove_class(table.cell(tr,5).data())
		swal({
			title: "Anda Yakin ?",
			text : "Sudah Mengecek kesesuaian Data ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			setTimeout(function() {
				$.ajax({
					url: '{site_url}tstockopname/set_sesuai',
					type: 'POST',
					data: {id_so: id_so,idstok: idstok,idunit: idunit,idtipe: idtipe,idbarang: idbarang,stokfisik: stokfisik},
					complete: function() {
						$.toaster({priority : 'success', title : 'Succes!', message : 'data berhasil disimpan'});
						table.ajax.reload( null, false );
						// $('#index_list').DataTable().ajax.reload()
					}
				});
			}, 100);
		});
	});
	$(document).on("click", ".reset", function() {
		table = $('#index_list').DataTable()
		var tr = $(this).parents('tr')
		var nama_barang = table.cell(tr,2).data()
		var tipebarang = table.cell(tr,3).data()

		var id_so = table.cell(tr,14).data()
		var idstok = table.cell(tr,15).data()
		var idunit = table.cell(tr,16).data()
		var idtipe = table.cell(tr,17).data()
		var idbarang = table.cell(tr,18).data()
		var stokfisik =remove_class(table.cell(tr,5).data())
		swal({
			title: "Anda Yakin ?",
			text : "Akan mereset pengecekan dan akan mengembalikan ke stok initial ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			setTimeout(function() {
				$.ajax({
					url: '{site_url}tstockopname/set_reset',
					type: 'POST',
					data: {id_so: id_so,idstok: idstok,idunit: idunit,idtipe: idtipe,idbarang: idbarang,stokfisik: stokfisik},
					complete: function() {
						$.toaster({priority : 'success', title : 'Succes!', message : 'data berhasil direset'});
						table.ajax.reload( null, false );
						// $('#index_list').DataTable().ajax.reload()
					}
				});
			}, 100);
		});
	});
	function sesuai_semua(){
		// alert('cek');
		var id_so;
		var idstok;
		var idunit;
		var idtipe;
		var idbarang;
		var catatan;
		var stokfisik;
		var st_valid=1;

		 var table = $('#index_list').DataTable();
		 var data = table.rows().data();
		 data.each(function (value, index) {
			 if (table.cell(index,0).data()=='2'){
				 st_valid='0';
			 }

		 });
		 if (st_valid=='0'){
			 sweetAlert("Maaf...", "Ada Row yang tidak sesuai!", "error");
             return false;
		 }
		 data.each(function (value, index) {
			id_so=table.cell(index,14).data()
			idstok=table.cell(index,15).data()
			idunit=table.cell(index,16).data()
			idtipe=table.cell(index,17).data()
			idbarang=table.cell(index,18).data()
			stokfisik=remove_class(table.cell(index,6).data());
			console.log('SO:'+id_so+' IDSTOK :'+idstok+' unit:'+idunit+' idtipe:'+idtipe+' idbarang :'+idbarang+' Fisik: '+stokfisik);
			setTimeout(function() {
				$.ajax({
					url: '{site_url}tstockopname/set_semua_sesuai',
					type: 'POST',
					data: {id_so: id_so,idstok: idstok,idunit: idunit,idtipe: idtipe,idbarang: idbarang,stokfisik: stokfisik},
					complete: function() {
						$.toaster({priority : 'success', title : 'Succes!', message : 'data berhasil disimpan'});
						// $('#index_list').DataTable().ajax.reload()
					}
				});
			}, 100);
		 });

		 setTimeout(function() {
			table.ajax.reload( null, false );
			// sweetAlert("Maaf...", "Data Berhasil", "error");
		 }, 2000);



	}
	function loadKS(){
		var idunit=$("#cidunit").val();
		var idbarang=$("#cidbarang").val();
		var idtipe=$("#cidtipe").val();
		var tanggaldari=$("#tanggaldari").val();
		var tanggalsampai=$("#tanggalsampai").val();
		// alert(tanggaldari);
		$('#index_ks').DataTable().destroy();
		var table = $('#index_ks').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"order": [],
		"ajax": {
			url: '{site_url}tstockopname/getKS/',
			type: "POST",
			dataType: 'json',
			data: {
				idunit: idunit,
				idtipe: idtipe,
				idbarang: idbarang,
				tanggaldari: tanggaldari,
				tanggalsampai: tanggalsampai,
			}
		},
		columnDefs: []
		});

	}
	function remove_class($str){
		var str=$str;
		var res = str.toString().replace('<span class="label label-danger">', '');
			res = res.toString().replace('</span>', '');
			res = res.toString().replace('<span class="label label-warning">', '');
		return res;
	}
	$('#form').on('keyup keypress', function(e) {
	  var keyCode = e.keyCode || e.which;
	  if (keyCode === 13) {
		loadBarang();
		return false;
	  }
	});
	$(document).on("click", "#btn_update", function() {
		set_tidak_sesuai();
	});
	$("#xstokfisik").keyup(function(){
		hitung_nilai();
	});
	function hitung_nilai(){
		console.log('FIFIK '+$("#xstokfisik").val());
		console.log('SITEM '+$("#xstoksistem").val());
		console.log('SELISIH '+$("#xselisih").val());
		$("#xselisih").val(parseFloat($("#xstokfisik").val())-parseFloat($("#xstoksistem").val()));
		$("#xnilai").val(parseFloat($("#xstokfisik").val())*parseFloat($("#xhpp").val()));
		$("#xnilaiselisih").val(parseFloat($("#xselisih").val())*parseFloat($("#xhpp").val()));

	}
	function set_tidak_sesuai(){
		var id_so=$("#xid_so").val();
		var idstok=$("#xidstok").val();
		var idunit=$("#xidunit").val();
		var idtipe=$("#xidtipe").val();
		var idbarang=$("#xidbarang").val();
		var catatan=$("#xcatatan").val();
		var stokfisik=$("#xstokfisik").val();
		console.log('SO:'+id_so+' IDSTOK :'+idstok+' unit:'+idunit+' idtipe:'+idtipe+' idbarang :'+idbarang+' Catatan : '+catatan+' Fisik: '+stokfisik);
		if (catatan){
			$.ajax({
				url: '{site_url}tstockopname/set_tidak_sesuai',
				type: 'POST',
				data: {id_so: id_so,idstok: idstok,idunit: idunit,idtipe: idtipe,idbarang: idbarang,catatan: catatan,stokfisik: stokfisik},
				complete: function() {
					$('#modalEdit').modal('toggle');
					$.toaster({priority : 'success', title : 'Succes!', message : 'data berhasil disimpan'});
					$('#index_list').DataTable().ajax.reload( null, false )
					// initial();
				}
			});
		}else{
			sweetAlert("Maaf...", "Catatan Harus diisi!", "error");
            return false;
		}
	}
	function loadBarang() {
		var id=$("#id").val();
		var idunit=$("#iddariunit").val();
		var idtipe=$("#idtipe").val();
		var nama_barang=$("#nama_barang").val();
		var idkategori=$("#idkategori").val();
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}tstockopname/getBarang/',
			type: "POST",
			dataType: 'json',
			data: {
				id: id,
				idunit: idunit,
				idtipe: idtipe,
				nama_barang: nama_barang,
				idkategori: idkategori,
			}
		},
		columnDefs: [{"targets": [0, 14,15,16,17,18], "visible": false },
					 {  className: "text-right", targets:[1,4,5,6,7,9,10,11] },
					 {  className: "text-center", targets:[3,12,13] },
					 { "width": "5%", "targets": [1,3,4,5,6,7] },
					 { "width": "8%", "targets": [9,10,11] },
					 { "width": "10%", "targets": [12] },
					 { "width": "15%", "targets": [2,13] }

		]
		});
	}

</script>
