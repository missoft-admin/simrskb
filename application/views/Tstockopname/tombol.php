<div class="content-grid">
	<div class="row">
		<?php if (UserAccesForm($user_acces_form,array('267'))){ ?>
		<div class="col-xs-6 col-sm-4 col-lg-2">
			<a class="block block-link-hover2 text-center" href="<?=($tombol=='index'?'javascript:void(0)':'{site_url}tstockopname/index')?>">
				<div class="block-content block-content-full <?=($tombol=='index'?'bg-primary':'')?>">
					<i class="si si-wrench fa-4x text-<?=($tombol=='index'?'white':'danger')?>"></i>
					<div class="font-w600 <?=($tombol=='index'?'text-white-op':'')?> push-15-t">Stock Opname</div>
				</div>
			</a>
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('274'))){ ?>
		<div class="col-xs-6 col-sm-4 col-lg-2">
			<a class="block block-link-hover3 text-center" href="<?=($tombol=='history'?'javascript:void(0)':'{site_url}tstockopname/history')?>">
				<div class="block-content block-content-full <?=($tombol=='history'?'bg-primary':'')?>">
					<i class="fa fa-history fa-4x text-<?=($tombol=='history'?'white':'danger')?>"></i>
					<div class="font-w600 <?=($tombol=='history'?'text-white-op':'')?> push-15-t">History</div>
				</div>
			</a>
		</div>
		<?}?>
		
	</div>
</div>