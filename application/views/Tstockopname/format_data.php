<?php 'Naha'?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Stokopname <?=$nama_unit?></title>
  <style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
	
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }
	  
      th {
        padding: 5px;
      }
	  td {
        padding: 5px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }
      
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;
		 
      }
	  td {
        padding: 3px;
		 
      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
    }
	
    
  </style>
  <script type="text/javascript">
    try {
      this.print();
    } catch (e) {
      window.onload = window.print;
    }
  </script>
</head>

<body>
  <table class="content">
      <tr>
        <td rowspan="3" width="30%" class="text-center"><img src="{logo1_rs}" alt="" width="60" height="60"></td>
        <td rowspan="2" colspan="3" width="50%"  class="text-center text-bold text-judul"><u>FORMAT STOKOPNAME</u></td>
		<td rowspan="2" width="20%"></td>		
      </tr>
	  <tr></tr>
	  <tr>
		<td width="14%" class="text-left text-bold text-top">Tanggal SO</td>
		<td width="20%" colspan="" class="text-top">: <?=HumanDateLong($tgl_so)?></td>		
		<td width="10%" colspan="" class="text-top text-bold text-top">Tanggal Input</td>		
		<td width="20%" colspan="" class="text-top">: <?=HumanDate($tgl_input)?></td>		
	  </tr>
	  <tr>
	    <td  class="text-center">{alamat_rs1}</td>
		<td  class="text-left text-bold text-top">No. SO</td>
		<td width="20%" colspan="" class="text-top">: {nostockopname}</td>
		<td width="10%" colspan="" class="text-top text-bold text-top">Periode</td>		
		<td width="20%" colspan="" class="text-top">: <?=$nama_periode?></td>
	  </tr>
	  <tr>
	    <td  class="text-center"><?=$telepon_rs.' '.$fax_rs?></td>
		<td class="text-left text-bold text-top">Unit Pelayanan</td>
		<td class=" text-top" colspan="" >: <?=($nama_unit)?></td>
		<td width="20%" colspan="" class="text-top text-bold text-top">User Created</td>		
		<td width="20%" colspan="" class="text-top">: <?=($user_create)?></td>
	  </tr>
	  <tr>
	    <td  class="text-center"></td>
		<td class="text-left text-bold text-top">User Finished</td>
		<td class=" text-top">: <?=$user_finish?></td>
		<td width="20%" colspan="" class="text-top text-bold text-top">Tanggal Selesai</td>		
		<td width="20%" colspan="" class="text-top">: <?=HumanDateLong($tgl_finish)?></td>		
	  </tr>
	 
    </table>
  <br/>
  <br/>
  <table width="100%">
    <thead>
      <tr>
        <th class="border-full">NO</th>
        <th class="border-full">NAMA BARANG</th>
        <th class="border-full">TIPE / KATEGORI</th>
        <th class="border-full">STOK SISTEM</th>
        <th class="border-full">STOK FISIK</th>
        <th class="border-full">SELISIH</th>
        <th class="border-full">STATUS</th>
        <th class="border-full">CATATAN</th>
        <th class="border-full">HPP</th>
        <th class="border-full">NILAI</th>
      </tr>
    </thead>
    <tbody>
      <?php $i=1;foreach ($details as $row):?>
      <tr>
        <td class="border-full"><?=$i?></td>
        <td class="border-full"><?=$row->nama?></td>
        <td class="border-full"><?=$row->namatipe?></td>
        <td class="border-full"><?=$row->stoksistem?></td>
        <td class="border-full"></td>
        <td class="border-full"></td>
        <td class="border-full"></td>
        <td class="border-full"></td>
        <td class="border-full"><?=number_format($row->hpp,0)?></td>
        <td class="border-full"></td>
      </tr>
      <?php $i++;endforeach;?>
    </tbody>
  </table>
  <br/>
  
</body>

</html>