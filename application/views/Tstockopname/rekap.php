<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>
<?
	$this->load->view('Tstockopname/tombol');
?>
<div class="block">
    <div class="block-header">
        
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:25px">
        <div class="row">
            <?php echo form_open('tstockopname/filter', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label">Kode SO</label>
                    <div class="col-md-8">
                        <input class="form-control" readonly type="text" id="nostockopname"  name="nostockopname" value="{nostockopname}" />
                        <input class="form-control" readonly type="hidden" id="id"  name="id" value="{id}" />
                        <input class="form-control" readonly type="hidden" id="iddariunit"  name="iddariunit" value="{idunitpelayanan}" />
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label">Unit Pelayanan</label>
                    <div class="col-md-8">
                        <input class="form-control" readonly type="text" id="nama_unit"  name="nama_unit" value="{nama_unit}" />
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label">Periode</label>
                    <div class="col-md-8">
                        <input class="form-control" readonly type="text" id="periode"  name="periode" value="<?= get_bulan(substr($periode,4,2)).' '.substr($periode,0,4) ?>" />
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label">User Created</label>
                    <div class="col-md-8">
                        <input class="form-control" readonly type="text" id="user_create"  name="user_create" value="<?=$user_create ?>" />
                    </div>
                </div>               
                
            </div>
			<div class="col-md-6">
                
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label">Tanggal Create SO</label>
                    <div class="col-md-4">
                        <input class="js-datepicker form-control" disabled type="text" id="tgl_so" data-date-format="dd-mm-yyyy" value="<?= $tgl_so ?>">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label">Tanggal Input SO</label>
                    <div class="col-md-4">
                        <input class="js-datepicker form-control" disabled type="text" id="tgl_input" data-date-format="dd-mm-yyyy" value="<?= date("d-m-Y", strtotime($tgl_input)) ?>">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label">Tanggal Selesai</label>
                    <div class="col-md-4">
                        <input class="js-datepicker form-control" disabled type="text" id="tgl_finish" data-date-format="dd-mm-yyyy" value="<?= date("d-m-Y H:i:s", strtotime($tgl_finish)) ?>">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label">User Finished</label>
                    <div class="col-md-8">
                        <input class="form-control" disabled type="text" id="user_finish"  name="user_finish" value="<?=$user_finish ?>" />
                    </div>
                </div>   
            </div>
			
				<hr style="margin-top:25px">
            <?php echo form_close(); ?>
			
        </div>
    </div>

  
</div>

<div class="block" id="div_proses">
    <div class="block-header">
        
        <h3 class="block-title">List Barang Stok Opname</h3>
		<hr style="margin-top:15px">
        <div class="row">
            <?php echo form_open('tstockopname/filter', 'class="form-horizontal" id="form"'); ?>
            <div class="col-md-12">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="distributor">Tipe</label>
                    <div class="col-md-5">
                        <select id="idtipe" name="idtipe" class="form-control" style="width: 100%;" data-placeholder="Choose one..">
                          
                        </select>
                    </div>
                </div>
                
            </div>
			<div class="col-md-12">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="distributor">Kategori</label>
                    <div class="col-md-5">
                        <select id="idkategori" name="idkategori" class="form-control" style="width: 100%;" data-placeholder="Choose one..">
                          
                        </select>
                    </div>
                </div>
                
            </div>
			<div class="col-md-12">
                
                <div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-2 control-label">Nama Barang</label>
                    <div class="col-md-3">
                        <input class="form-control" placeholder="Filter By Nama Barang" type="text" id="nama_barang"  value="">
                    </div>
					<div class="col-md-2" >
                        <button class="btn btn-success text-uppercase" type="button" id="btn_filter" name="btn_filter" style="font-size:13px;width:100%;float:left;"><i class="fa fa-filter"></i> Filter </button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
	
    <div class="block-content" id="div_tabel">
        <table class="table table-bordered table-striped" id="index_list" style="width: 100%;">
            <thead>
                <tr>
                    <th style="width:5%">#</th>
                    <th style="width:5%">No</th>
                    <th style="width:15%">Nama Barang</th>
                    <th style="width:5%">Tipe</th>
                    <th style="width:5%">Stok Sistem</th>
                    <th style="width:5%">Stok Fisik</th>
                    <th style="width:5%">Selisih</th>
                    <th style="width:15%">Catatan</th>
                    <th style="width:10%">HPP</th>
                    <th style="width:10%">Nilai</th>
                    <th style="width:10%">Nilai Selisih</th>
                    <th style="width:10%">Status</th>
                    <th style="width:15%">Aksi</th>
                    <th style="width:0%">so_id</th>
                    <th style="width:0%">idunitpelayanan</th>
                    <th style="width:0%">idtipe</th>
                    <th style="width:0%">idbarang</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
	

</div>

<div class="modal" id="modalKS" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title" id='tittle_ks'>Kartu Stok</h3>
					
                </div>
				
						
                <div class="block-content">
                    <div class="row">
						<?php echo form_open('#', 'class="form-horizontal" id="form"'); ?>
						<div class="col-md-12">
							<label class="col-md-2 control-label" for="tanggal">Tanggal</label>
							<div class="col-md-8">
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" id="tanggaldari"  name="tanggaldari" placeholder="From" value=""/>
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" id="tanggalsampai" name="tanggalsampai" placeholder="To" value=""/>
								</div>
							</div>
							<div class="col-md-2">
								<button class="btn btn-success text-uppercase" type="button" id="btn_cari" name="btn_cari" style="font-size:13px;width:100%;float:left;"><i class="fa fa-filter"></i> Filter </button>
							</div>
							<input type="hidden" id="cidtipe">
							<input type="hidden" id="cidbarang">
							<input type="hidden" id="cidunit">
						</div>
						
						<?php echo form_close(); ?>
					</div>
					<hr style="margin-top:15px">
					<table class="table table-bordered table-striped" id="index_ks" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:5%">No</th>
								<th style="width:10%">Tanggal</th>
								<th style="width:10%">No Transaksi</th>
								<th style="width:40%">Keterangan</th>
								<th style="width:10%">Masuk</th>
								<th style="width:10%">Keluar</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
                </div>
				 <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
    var table;
    $(document).ready(function() {
        // $("#iddariunit").select2();
        $("#idkategori").select2();
        $("#idtipe").select2();
        $(".number").number(true,0,'.',',');
		load_tipe();
		loadBarang();
    });
	
	
	$("#idtipe").change(function(){
		load_kategori();
	});
	function hide_all(){
		$("#id").val('');
		$("#btn_new").attr("disabled", 'disabled');
		$("#div_edit").hide();
		// $("#div_tabel").hide();
		$("#div_info").hide();
		$("#div_proses").hide();
		
	}
	
	function clear_data(){
		
		$('#index_list').DataTable().destroy();
		$('#index_list tbody').empty();
		var table = $('#index_list').DataTable( {
			"searching": false,
			columnDefs: [{ targets: [0,13,14,15,16], visible: false}]
		} );
	}
	function load_tipe(){
		clear_data();
		var idunit=$("#iddariunit").val();
		$('#idtipe').find('option').remove();
		
		$('#idkategori').find('option').remove();
		$('#idkategori').append('<option value="#" selected>- Pilih Semua -</option>');
		var def='';
		if (idunit !='#'){
				$('#idtipe').append('<option value="#" selected>- Pilih Semua -</option>');
						
				$.ajax({
					url: '{site_url}tstockopname/list_tipe/'+idunit,
					dataType: "json",
					success: function(data) {
						$.each(data, function (i,row) {
						$('#idtipe')
						.append('<option value="' + row.id + '">' + row.text + '</option>');
						});
					}
				});

		}
	}
	function load_kategori(){
		var idtipe=$("#idtipe").val();
		$('#idkategori').find('option').remove();
		$('#idkategori').append('<option value="#" selected>- Pilih Semua -</option>');
		if (idtipe !='#'){
						
				$.ajax({
					url: '{site_url}tstockopname/list_kategori/'+idtipe,
					dataType: "json",
					success: function(data) {
						$.each(data, function (i,row) {
							
						$('#idkategori')
						.append('<option value="' + row.path + '">' + TreeView(row.level, row.nama) + '</option>');
						});
					}
				});

		}
	}
	function TreeView($level, $name)
	{
		// console.log($name);
		$indent = '';
		for ($i = 0; $i < $level; $i++) {
			if ($i > 0) {
				$indent += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			} else {
				$indent += '';
			}
		}

		if ($i > 0) {
			$indent += '└─';
		} else {
			$indent += '';
		}

		return $indent + ' ' +$name;
	}
	$(document).on("click", "#btn_filter", function() {
		loadBarang();
	});
	
	$(document).on("click", ".ks", function() {
		 // table = $('#index_list').DataTable()
		// var tr = $(this).parents('tr')
		// var nama_barang = table.cell(tr,2).data()
		
		// var idunit = table.cell(tr,14).data()
		// var idtipe = table.cell(tr,15).data()
		// var idbarang = table.cell(tr,16).data()
		// $("#cidtipe").val(idtipe);
		// $("#cidbarang").val(idbarang);
		// $("#cidunit").val(idunit);
		// $("#tittle_ks").text('Kartu Stok '+nama_barang);
		// loadKS();
		
		// $('#modalKS').modal('show');
	});
	function loadKS(){
		var idunit=$("#cidunit").val();
		var idbarang=$("#cidbarang").val();
		var idtipe=$("#cidtipe").val();
		var tanggaldari=$("#tanggaldari").val();
		var tanggalsampai=$("#tanggalsampai").val();
		// alert(tanggaldari);
		$('#index_ks').DataTable().destroy();
		var table = $('#index_ks').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"order": [],
		"ajax": {
			url: '{site_url}tstockopname/getKS/',
			type: "POST",
			dataType: 'json',
			data: {
				idunit: idunit,
				idtipe: idtipe,
				idbarang: idbarang,
				tanggaldari: tanggaldari,
				tanggalsampai: tanggalsampai,
			}
		},
		columnDefs: []
		});
		
	}
	
	$('#form').on('keyup keypress', function(e) {
	  var keyCode = e.keyCode || e.which;
	  if (keyCode === 13) { 
		loadBarang();
		return false;
	  }
	});
	
	function loadBarang() {
		var iddariunit=$("#iddariunit").val();
		var id=$("#id").val();
		var idtipe=$("#idtipe").val();
		var nama_barang=$("#nama_barang").val();
		var idkategori=$("#idkategori").val();
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"order": [],
		"ajax": {
			url: '{site_url}tstockopname/getBarangRekap/',
			type: "POST",
			dataType: 'json',
			data: {
				id: id,
				iddariunit: iddariunit,
				idtipe: idtipe,
				nama_barang: nama_barang,
				idkategori: idkategori,
			}
		},
		columnDefs: [{"targets": [0,13,14,15,16], "visible": false }]
		});
	}
	
</script>