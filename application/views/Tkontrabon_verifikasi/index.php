<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Tipe Bayar</label>
                    <div class="col-md-8">
                        <select id="tipe" name="tipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Tipe -</option>
							<option value="1" <?=("1" == $tipe ? 'selected' : ''); ?>>PEMESANAN GUDANG</option>
							<option value="2" <?=("2" == $tipe ? 'selected' : ''); ?>>RETUR</option>
							<option value="3" <?=("3" == $tipe ? 'selected' : ''); ?>>MODUL PENGAJUAN</option>
							
						</select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Distributor</label>
                    <div class="col-md-8">
                        <select id="iddistributor" name="iddistributor" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Distributor -</option>
							<?foreach($list_distributor as $row){?>
								<option value="<?=$row->id?>" <?=($row->id==$iddistributor?'selected':'')?>><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Pemesanan / Pengajuan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_po" placeholder="No. PO / No Pengajuan" name="no_po" value="{no_po}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Penerimaan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_terima" placeholder="No. Penerimaan" name="no_terima" value="{no_terima}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Faktur</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_fakur" placeholder="No. Faktur" name="no_fakur" value="{no_fakur}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label text-left" id="lbl_warning" for=""></label>
                    <div class="col-md-8">
						<div class="alert alert-success alert-dismissable" id="label_warning">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<p><strong>Silahkan Tentukan Filter Pencarian selanjutnya  Klik Tombol Filter</strong></p>
						</div>
					</div>
                </div>
                
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Cara Bayar</label>
                    <div class="col-md-8">
						<select id="cara_bayar" name="cara_bayar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Cara Bayar -</option>
							<option value="1" <?=("1" == $cara_bayar ? 'selected' : ''); ?>>TUNAI</option>
							<option value="2" <?=("2" == $cara_bayar ? 'selected' : ''); ?>>KREDIT</option>
							
						</select>
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Status</label>
                    <div class="col-md-8">
                        <select id="status" name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Status -</option>
							<option value="0" <?=("0" == $status ? 'selected' : ''); ?>>Belum Diverifikasi</option>
							<option value="1" <?=("1" == $status ? 'selected' : ''); ?>>Sudah Diverifikasi</option>
							<option value="2" <?=("2" == $status ? 'selected' : ''); ?>>Telah Diaktivasi</option>
							<option value="3" <?=("3" == $status ? 'selected' : ''); ?>>Telah Dikontrabon</option>
							
						</select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Penerimaan</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggalterima1" name="tanggalterima1" placeholder="From" value="{tanggalterima1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggalterima2" name="tanggalterima2" placeholder="To" value="{tanggalterima2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Jatuh Tempo</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggaljt1"" name="tanggaljt1" placeholder="From" value="{tanggaljt1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggaljt2" name="tanggaljt2" placeholder="To" value="{tanggaljt2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-4" hidden>
                        <button class="btn btn-danger text-uppercase" type="button" name="btn_verif_filter" id="btn_verif_filter" style="font-size:13px;width:100%;"><i class="fa fa-check-square-o"></i> Verifikasi By Filter</button>
                    </div>
					<div class="col-md-8">
                        <button class="btn btn-primary text-uppercase" type="button" name="btn_verif_page" id="btn_verif_page" style="font-size:13px;width:100%;"><i class="fa fa-check"></i> Verifikasi Per page</button>
                    </div>
                </div>
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th hidden width="5%">TIPE</th>
                    <th hidden width="2%">ID</th>
                    <th width="10%">NO</th>
                    <th width="10%">TIPE</th>
                    <th width="10%">NO PENERIMAAN / NO.PENGAJUAN</th>
                    <th width="10%">NO PEMESANAN</th>
                    <th width="10%">TGL TRX</th>
                    <th width="10%">TGL TERIMA</th>
                    <th width="10%">NO FAKTUR</th>
                    <th width="15%">DISTRIBUTOR</th>
                    <th width="15%">NOMINAL</th>
                    <th width="15%">CARA BAYAR</th>
                    <th width="15%">J. TEMPO</th>
                    <th width="15%">STATUS</th>
                    <th width="15%" class="text-center">AKSI</th>
                    <th width="15%" class="text-center">AKSI</th>
                    <th width="15%" class="text-center">AKSI</th>
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
</div>
<div class="modal" id="modal_edit_pengembalian" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title" id='title_barang'>Validasi Retur</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal">
						<input type="hidden" id="retur_id">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="col-md-3 control-label">Distributor</label>
									<div class="col-md-9">
										<input type="text" class="form-control detail" id="nama_distributor" disabled>
										<input type="hidden" class="form-control" id="trx_id" readonly>
										<input type="hidden" class="form-control" id="tpengembalian_id" readonly>
										<input type="hidden" class="form-control" id="tpenerimaan_id" readonly>
										<input type="hidden" class="form-control" id="ref_tabel" readonly>
										<input type="hidden" class="form-control" id="st_penerimaan" readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Jenis Retur</label>
									
									<div class="col-md-4">
										<h4 id="div_jenis"></h4>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Total Retur Rp.</label>
									
									<div class="col-md-4">
										<div class="input-group">
											<input class="form-control decimal" disabled type="text" id="total" name="total">
											<span class="input-group-btn">
												<button class="btn btn-primary" type="button"  onclick="goto_retur()" id="button_lihat_retur"><i class="fa fa-eye"></i> Lihat</button>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Total Pengganti Rp.</label>
									<div class="col-md-4">
										<div class="input-group">
											<input class="form-control decimal" disabled type="text" id="total_pengganti" name="total_pengganti">
											<span class="input-group-btn">
												<button class="btn btn-primary" type="button" onclick="goto_pengganti()" id="button_lihat_ganti"><i class="fa fa-eye"></i> Lihat</button>
											</span>
										</div>
									</div>
									<div class="col-md-5">
										<h4><span class="label label-primary" id="div_info"></span></h4>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Selisih Rp.</label>
									<div class="col-md-4">
										<div class="input-group">
											<input class="form-control decimal" disabled type="text" id="total_selisih" name="total_selisih">
											<span class="input-group-btn">
												<button class="btn btn-primary" type="button" disabled id="button_lihat_ganti"><i class="fa fa-eye"></i></button>
											</span>
										</div>
									</div>
									<div class="col-md-5">
										<h4><span class="label label-primary" id="div_info"></span></h4>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Cara Pengembalian</label>
									<div class="col-md-9">
										<select id="tipe_kembali" name="tipe_kembali" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="0">-BELUM DITENTUKAN-</option>
											<option value="1">TUNAI</option>
											<option value="2">POTONG KBO</option>											
										</select>
									</div>
								</div>
								<div class="form-group" id="div_tgl_kbo">
									<label class="col-md-3 control-label" for="example-datetimepicker4">Tanggal KBO</label>
									<div class="col-md-4">
										<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
											<input class="js-datepicker form-control" type="text" id="tanggalkontrabon" name="tanggalkontrabon" placeholder="Choose a date..">
											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
										</div>
									</div>
								</div>
							</div>
							
								
						</div>
					</form>
				</div>
				 <div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
					<button class="btn btn-sm btn-success" id="btn_update" type="button">Update</button>
					<button class="btn btn-sm btn-warning" id="btn_update_verifikasi" type="button">Update & Verifikasi</button>
				</div>
				</div>
			</div>
		</div>
</div>
<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
function goto_pengganti(){
	window.open('{site_url}tgudang_penerimaan/detail/'+$("#tpenerimaan_id").val(),'_blank');
}
function goto_retur(){
	window.open('{site_url}tgudang_pengembalian/detail/'+$("#tpengembalian_id").val(),'_blank');
}
function get_retur_data($ref_tabel='',$id=''){
	$("#trx_id").val($id);
	$("#ref_tabel").val($ref_tabel);
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tkontrabon_verifikasi/get_retur_data/'+$ref_tabel+'/'+$id,
		dataType: "json",
		success: function(data) {
			$("#cover-spin").hide();
			console.log(data);
			$("#nama_distributor").val(data.distributor);
			$("#tpenerimaan_id").val(data.tpenerimaan_id);
			if (data.tpenerimaan_id==''){
				$("#button_lihat_ganti").attr('disabled',true);
			}else{
				$("#button_lihat_ganti").attr('disabled',false);
			}
			$("#tpengembalian_id").val(data.tpengembalian_id);
			$("#total").val(data.totalharga);
			$("#total_pengganti").val(data.totalpengganti);
			$("#tanggalkontrabon").val(data.tanggalkontrabon);
			if (parseFloat(data.totalharga) > parseFloat(data.totalpengganti)){	
				$("#div_info").removeClass("label-primary label-success label-danger");
				$("#div_info").addClass("label-success");
				$("#div_info").text('Distributor Kurang Bayar');
				$("#st_penerimaan").val(1);
				$("#total_selisih").val(data.totalharga - data.totalpengganti);
			}else if (parseFloat(data.totalharga) < parseFloat(data.totalpengganti)){	
				$("#div_info").removeClass("label-primary label-success label-danger");
				$("#div_info").addClass("label-danger");
				$("#div_info").text('Rumah sakit Kurang Bayar');
				$("#st_penerimaan").val(2);
				$("#total_selisih").val(data.totalpengganti - data.totalharga);
			}else{
				$("#div_info").removeClass("label-primary label-success label-danger");
				$("#div_info").addClass("label-primary");
				$("#div_info").text('Tidak ada Selisih');
				$("#st_penerimaan").val(0);
				$("#total_selisih").val(0);
			}
				$("#div_jenis").html(data.jenis_retur_nama);
			$("#tipe_kembali").val(data.tipe_kembali).trigger('change');
		}
	});
	$("#modal_edit_pengembalian").modal('show');
}
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
	// load_index();
   
});

function load_index(){
	var tipe=$("#tipe").val();
	var iddistributor=$("#iddistributor").val();
	var no_po=$("#no_po").val();
	var no_terima=$("#no_terima").val();
	var no_fakur=$("#no_fakur").val();
	var cara_bayar=$("#cara_bayar").val();
	var status=$("#status").val();
	var tanggalterima1=$("#tanggalterima1").val();
	var tanggalterima2=$("#tanggalterima2").val();
	var tanggaljt1=$("#tanggaljt1").val();
	var tanggaljt2=$("#tanggaljt2").val();
	// alert(tanggalterima1);
	$('#index_list').DataTable().destroy();
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0,1,15,16], "visible": false },
							{ "width": "3%", "targets": [2] },
							{ "width": "8%", "targets": [4,5,6,7,10,12] },
							{ "width": "5%", "targets": [3,8,11,13] },
							{ "width": "10%", "targets": [9,14] },
							// { "width": "15%", "targets": [14] },
						 {"targets": [2], className: "text-left" },
						 {"targets": [10], className: "text-right" },
						 {"targets": [3,4,5,6,7,11,12], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tkontrabon_verifikasi/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						tipe:tipe,iddistributor:iddistributor,no_po:no_po,no_terima:no_terima,
						no_fakur:no_fakur,cara_bayar:cara_bayar,status:status,
						tanggalterima1:tanggalterima1,tanggalterima2:tanggalterima2,tanggaljt1:tanggaljt1,tanggaljt2:tanggaljt2
					   }
            }
        });
}
$("#btn_filter").click(function() {
	// alert('ID');
	// table.destroy();
	load_index();
});

$(document).on("click", ".verifikasi", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var nopenerimaan=table.cell(tr,4).data()
	// alert(id);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Verifikasi No. Penerimaan "+nopenerimaan+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		verifikasi(id);
	});
});
function verifikasi($id){
	console.log($id);
	var id=$id;		
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tkontrabon_verifikasi/verifikasi',
		type: 'POST',
		data: {id: id},
		complete: function() {
			table.ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi'});
		}
	});
}
$(document).on("click", ".verifikasi_pengajuan", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var nopenerimaan=table.cell(tr,4).data()
	// alert(id);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Verifikasi No. Pengajuan "+nopenerimaan+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		verifikasi_pengajuan(id);
	});
});
function verifikasi_pengajuan($id){
	console.log($id);
	var id=$id;		
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tkontrabon_verifikasi/verifikasi_pengajuan',
		type: 'POST',
		data: {id: id},
		complete: function() {
			$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi'});
			table.ajax.reload( null, false ); 
			$("#cover-spin").hide();
		}
	});
}
$(document).on("click", ".verifikasi_kas", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var nopenerimaan=table.cell(tr,4).data()
	// alert(id);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Verifikasi No. Penerimaan "+nopenerimaan+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		verifikasi_kas(id);
	});
});
function verifikasi_kas($id){
	console.log($id);
	var id=$id;		
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tkontrabon_verifikasi/verifikasi_kas',
		type: 'POST',
		data: {id: id},
		complete: function() {
			table.ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi'});
		}
	});
}

$(document).on("click", ".batal_verifikasi_retur", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var nopenerimaan=table.cell(tr,4).data()
	// alert(id);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Membatalkan Verifikasi Retur Uang No Penerimaan "+nopenerimaan+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		batal_verifikasi_retur(id);
	});
});
function batal_verifikasi_retur($id){
	// console.log($id);
	var id=$id;		
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tkontrabon_verifikasi/batal_verifikasi_retur',
		type: 'POST',
		data: {id: id},
		complete: function() {
			table.ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Membatalkan Verifikasi'});
		}
	});
}
$(document).on("click", ".batal_verifikasi", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var nopenerimaan=table.cell(tr,4).data()
	// alert(id);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Membatalkan Verifikasi No. Penerimaan "+nopenerimaan+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		batal_verifikasi(id);
	});
});
function batal_verifikasi($id){
	// console.log($id);
	var id=$id;		
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tkontrabon_verifikasi/batal_verifikasi',
		type: 'POST',
		data: {id: id},
		complete: function() {
			table.ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Membatalkan Verifikasi'});
		}
	});
}
$(document).on("click", ".batal_verifikasi_tunai", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var nopenerimaan=table.cell(tr,4).data()
	// alert(id);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Membatalkan Verifikasi No. Penerimaan "+nopenerimaan+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		batal_verifikasi_tunai(id);
	});
});
function batal_verifikasi_tunai($id){
	// console.log($id);
	var id=$id;		
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tkontrabon_verifikasi/batal_verifikasi_tunai',
		type: 'POST',
		data: {id: id},
		complete: function() {
			table.ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Membatalkan Verifikasi'});
		}
	});
}
$(document).on("click", ".batal_verifikasi_pengajuan", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var nopenerimaan=table.cell(tr,4).data()
	// alert(id);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Membatalkan Verifikasi No. Pengajuan "+nopenerimaan+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		batal_verifikasi_pengajuan(id);
	});
});
function batal_verifikasi_pengajuan($id){
	// console.log($id);
	var id=$id;		
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tkontrabon_verifikasi/batal_verifikasi_pengajuan',
		type: 'POST',
		data: {id: id},
		complete: function() {
			table.ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Membatalkan Verifikasi'});
		}
	});
}
$(document).on("click", ".batal_activation", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var nopenerimaan=table.cell(tr,4).data()
	// alert(id);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Membatalkan Activation No. Penerimaan "+nopenerimaan+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		batal_activation(id);
	});
});
function batal_activation($id){
	// console.log($id);
	var id=$id;		
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tkontrabon_verifikasi/batal_activation',
		type: 'POST',
		data: {id: id},
		complete: function() {
			table.ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Membatalkan Verifikasi'});
		}
	});
}

$(document).on("click", ".edit_retur_uang", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var nopenerimaan=table.cell(tr,4).data()
	var dist=table.cell(tr,9).data()
	var tot=table.cell(tr,10).data()
	var tgl=table.cell(tr,12).data()
	if (tgl != '<span class="label label-warning">BELUM DITENTUKAN</span>'){
		$("#tanggalkontrabon").val(tgl);
	}else{
		$("#tanggalkontrabon").val('');
	}
	$.ajax({
		url: '{site_url}tkontrabon_verifikasi/get_retur/'+id,
		dataType: "json",
		success: function(data) {
			$("#tipe_kembali").val(data).trigger('change');
		
		}
	});


	$("#retur_id").val(id);
	$("#nama_distributor").val(dist);
	$("#total").val(tot);
	$("#modal_edit_pengembalian").modal('show');
});

$("#tipe_kembali").change(function(){
	if ($(this).val()!='2'){
		$("#div_tgl_kbo").hide();
	}else{
		$("#div_tgl_kbo").show();
		
	}
});
$("#tipe").change(function(){
	if ($(this).val()!="#"){
		load_distributor($(this).val());		
	}else{
		$("#iddistributor").empty();
		$('#iddistributor').append('<option value="#" selected>- Semua Distributor -</option>');
	}
});
function load_distributor($tipe){
	$.ajax({
			url: '{site_url}tkontrabon_verifikasi/load_distributor/'+$tipe,
			dataType: "json",
			success: function(data) {
				$("#iddistributor").empty();
				if ($tipe=='3'){
					$('#iddistributor').append('<option value="#" selected>- Semua Vendor -</option>');					
				}else{
					$('#iddistributor').append('<option value="#" selected>- Semua Distributor -</option>');					
				}
				$('#iddistributor').append(data.detail);
			}
		});
}


$(document).on("click", "#btn_update", function() {
	var id=$('#retur_id').val();		
	var tgl_kbo=$('#tanggalkontrabon').val();		
	var tipe_kembali=$('#tipe_kembali').val();	
	if (tipe_kembali=='2' && tgl_kbo==''){
		 sweetAlert("Maaf...", "Tanggal KBO Harus diisi!", "error");
         return false;
	}
	$("#modal_edit_pengembalian").modal('hide');
	$("#cover-spin").show();
	var tpenerimaan_id=$('#tpenerimaan_id').val();		
	var tpengembalian_id=$('#tpengembalian_id').val();	
	var trx_id=$('#trx_id').val();	
	var ref_tabel=$('#ref_tabel').val();	
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tkontrabon_verifikasi/update_retur_uang',
		type: 'POST',
		data: {id: id,tgl_kbo:tgl_kbo,tipe_kembali:tipe_kembali,ref_tabel:ref_tabel,trx_id:trx_id},
		complete: function() {
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Membatalkan Verifikasi'});
			table.ajax.reload( null, false ); 
		}
	});
});
$(document).on("click", "#btn_update_verifikasi", function() {
	// if ($("#tanggalkontrabon").val() == null || $("#tanggalkontrabon").val() == ''){
		// sweetAlert("Maaf...", "Silahkan Isi Tanggal KBO !", "error");
		// return false;
	// }
	if ($("#tipe_kembali").val() == '0'){
		sweetAlert("Maaf...", "Silahkan Tentukan Tipe pengembalian !", "error");
		return false;
	}
	var tgl_kbo=$('#tanggalkontrabon').val();		
	var tipe_kembali=$('#tipe_kembali').val();	
	if (tipe_kembali=='2' && tgl_kbo==''){
		 sweetAlert("Maaf...", "Tanggal KBO Harus diisi!", "error");
         return false;
	}
	swal({
		title: "Anda Yakin ?",
		text : "Akan Verifikasi Pengembalian Uang ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		update_retur_uang_verifikasi();
	});
});

function update_retur_uang_verifikasi(){
	var id=$('#retur_id').val();		
	var tgl_kbo=$('#tanggalkontrabon').val();		
	var tpenerimaan_id=$('#tpenerimaan_id').val();	
	var tpengembalian_id=$('#tpengembalian_id').val();	
	var tipe_kembali=$('#tipe_kembali').val();	
	var total_selisih=$('#total_selisih').val();	
	var st_penerimaan=$('#st_penerimaan').val();	
	
	var trx_id=$('#trx_id').val();	
	var ref_tabel=$('#ref_tabel').val();	
	
	$("#modal_edit_pengembalian").modal('hide');
	$("#cover-spin").show();
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tkontrabon_verifikasi/update_retur_uang_verifikasi',
		type: 'POST',
		data: {id: id,tgl_kbo:tgl_kbo,tipe_kembali:tipe_kembali,tpenerimaan_id:tpenerimaan_id,tpengembalian_id:tpengembalian_id,trx_id:trx_id,ref_tabel:ref_tabel,st_penerimaan:st_penerimaan,total_selisih:total_selisih},
		complete: function() {
			table.ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Membatalkan Verifikasi'});
		}
	});
}
$(document).on("click", "#btn_verif_filter", function() {
	
	swal({
		title: "Anda Yakin ?",
		text : "Akan Verifikasi  Semua berdasarkan pencarian ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		// update_retur_uang_verifikasi();
	});
});
$(document).on("click", "#btn_verif_page", function() {
	
	swal({
		title: "Anda Yakin ?",
		text : "Akan Verifikasi page ini tipe pemesanan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		
		verifikasi_per_page();
	});
});
function verifikasi_per_page(){
	var jml=0;
	var id;
	var tipe='1';
	var arr_id=[];
	var arr_tipe=[];
	var arr_verif_name=[];
	 var table = $('#index_list').DataTable();		
	 var data = table.rows().data();
	 
	 var table = $('#index_list').DataTable();		
	 var data = table.rows().data();
	 data.each(function (value, index) {
		 if (table.cell(index,16).data()!=''){
			 id=table.cell(index,1).data();
			 tipe=table.cell(index,0).data();
				arr_id.push(id);
				arr_tipe.push(tipe);
				arr_verif_name.push(table.cell(index,16).data());
		 }
		
		// console.log(id);
		
	 });
	 if (arr_id){
		 jml=arr_id.length;
	 }
	 // arr_verif_name.lenght;
	 // alert(jml);return false;
	 if (jml < 1){
		 sweetAlert("Maaf...", "Tidak ada data yang bisa di verif!", "error");
		 return false;
	 }
	 // alert('verifl');
	 // return false;
	 $("#cover-spin").show();
	 setTimeout(function() {
		$.ajax({
			url: '{site_url}tkontrabon_verifikasi/verifikasi_per_page',
			type: 'POST',
			data: {id: arr_id,tipe: arr_tipe,verif_nama: arr_verif_name},
			complete: function() {
				$("#cover-spin").hide();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Selesai Dikirim'});
				// $('#index_list').DataTable().ajax.reload()
			}
		});
	}, 200);
	 setTimeout(function() {
		table.ajax.reload( null, false ); 
		// sweetAlert("Maaf...", "Data Berhasil", "error");
	 }, 500);
}
</script>