<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}medc" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('medc/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama EDC</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama EDC" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Bank</label>
				<div class="col-md-7">
					<select id="bank_id" name="bank_id" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="" <?=($bank_id==''?'selected':'')?> selected>- Pilih Bank -</option>
						<?foreach(get_all('mbank',array('status'=>1)) as $row){?>
							<option value="<?=$row->id?>" <?=$bank_id==$row->id?'selected':''?>><?=$row->nama?></option>									
						<?}?>
						
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Lokasi</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="lokasi" placeholder="Lokasi" name="lokasi" value="{lokasi}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Akses User</label>
				<div class="col-md-7">
					<select name="userid[]" class="js-select2 form-control" multiple="multiple">
					<?foreach($list_user as $row){?>
						<option value="<?=$row->id?>" <?=$row->selected?>><?=$row->name?></option>
					<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="deskripsi">Deskripsi</label>
				<div class="col-md-7">
					<textarea class="form-control js-summernote" name="deskripsi" id="deskripsi"><?=$deskripsi?></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}medc" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

	
	})	
</script>