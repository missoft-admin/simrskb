<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mreferensi_rujukan_keluar" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mreferensi_rujukan_keluar/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama_tujuan">Nama Tujuan</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama_tujuan" placeholder="Nama Tujuan" name="nama_tujuan" value="{nama_tujuan}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama_lengkap">Nama Lengkap</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama_lengkap" placeholder="Nama Lengkap" name="nama_lengkap" value="{nama_lengkap}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="">Jenis Layanan</label>
				<div class="col-md-7">
					<select name="jenis_layanan[]" class="js-select2 form-control" style="width: 100%;" multiple>
						<option value="1" <?=(in_array('1', $option_jenis_layanan) ? 'selected' : ''); ?>>Laboratorium</option>
						<option value="2" <?=(in_array('2', $option_jenis_layanan) ? 'selected' : ''); ?>>Radiologi</option>
					</select>	
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="alamat">Alamat</label>
				<div class="col-md-7">
					<textarea class="form-control" name="alamat" rows="3">{alamat}</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nomor_telepon">Nomor Telepon</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nomor_telepon" placeholder="Nomor Telepon" name="nomor_telepon" value="{nomor_telepon}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="email">Email</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="email" placeholder="Email" name="email" value="{email}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nomor_perjanjian">Nomor Perjanjian Kerjasama</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nomor_perjanjian" placeholder="Nomor Perjanjian Kerjasama" name="nomor_perjanjian" value="{nomor_perjanjian}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="">Tanggal Perjanjian Kerjasama</label>
				<div class="col-md-7">
					<div class="input-group date">
						<input class="form-control js-datepicker" type="text" name="tanggal_perjanjian_dari" placeholder="Tanggal Dari" value="{tanggal_perjanjian_dari}" data-date-format="dd/mm/yyyy">
						<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
						<input class="form-control js-datepicker" type="text" name="tanggal_perjanjian_sampai" placeholder="Tanggal Sampai" value="{tanggal_perjanjian_sampai}" data-date-format="dd/mm/yyyy">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="pic">PIC</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="pic" placeholder="PIC" name="pic" value="{pic}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mreferensi_rujukan_keluar" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>
