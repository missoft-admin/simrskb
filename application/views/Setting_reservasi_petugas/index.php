<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1562'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_1" onclick="load_general()"><i class="fa fa-bell"></i> General</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1564'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_2"  onclick="load_logic()"><i class="fa fa-check-square-o"></i> Logic Skrining</a>
		</li>
		<?}?>
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('1562'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_1">
			
			<?php echo form_open('setting_reservasi_petugas/save_pendaftaran','class="form-horizontal" id="form-work"') ?>
			<div class="row">
				
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="about_us_judul">Pesan Pendaftaran Berhasil<span style="color:red;">*</span></label>
						<div class="col-md-10">
							<textarea class="form-control js-summernote" name="pesan_berhasil" id="pesan_berhasil"><?=$pesan_berhasil?></textarea>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="tanggal">Auto Validasi</label>
						<div class="col-md-4">
							<select id="st_auto_validasi" name="st_auto_validasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_auto_validasi=='1'?'selected':'')?>>YA</option>
								<option value="0" <?=($st_auto_validasi=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-2 control-label" for="tanggal">Antrian Ditampilkan</label>
						<div class="col-md-4">
							<select id="st_antrian_tampil" name="st_antrian_tampil" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_antrian_tampil=='1'?'selected':'')?>>YA</option>
								<option value="0" <?=($st_antrian_tampil=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="tanggal">Poliklinik</label>
						<div class="col-md-10">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_poli">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="40%">Tipe</th>
											<th width="40%">Poliklinik</th>
											<th width="15%">Action</th>										   
										</tr>
										<tr>
											<th width="5%">#</th>
											<th width="40%">
												<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" <?=($idtipe=='#'?'selected':'')?>>-Pilih Tipe-</option>
													<option value="1" <?=($idtipe=='1'?'selected':'')?>>Poliklinik</option>
													<option value="2" <?=($idtipe=='2'?'selected':'')?>>Instalasi Gawat Darurat</option>
													
												</select>
											</th>
											<th width="40%">
												<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" <?=($idpoli=='#'?'selected':'')?>>-Pilih Poliklinik-</option>
												</select>
											</th>
											<th width="15%">
												<?php if (UserAccesForm($user_acces_form,array('1471'))){ ?>
												<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_poli" name="btn_tambah_poli"><i class="fa fa-plus"></i> Tambah</button>
												<?}?>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
						
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="tanggal">Cara Pembayaran</label>
						<div class="col-md-10">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_bayar">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="80%">Kelompok Pasien</th>
											<th width="15%">Action</th>										   
										</tr>
										<tr>
											<th width="5%">#</th>
											<th width="40%">
												<select id="idkelompokpasien" name="idkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													
												</select>
											</th>
											
											<th width="15%">
												<?php if (UserAccesForm($user_acces_form,array('1473'))){ ?>
												
												<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_kp" name="btn_tambah_kp"><i class="fa fa-plus"></i> Tambah</button>
												<?}?>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
						
					</div>
				</div>
				<?php if (UserAccesForm($user_acces_form,array('1476'))){ ?>
				<div class="col-md-12 push-10">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="statusstok"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_general" name="btn_save_general"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
				<?}?>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1564'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_2">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_logic">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="10%">Jenis Pertemuan</th>
											<th width="15%">Tipe</th>
											<th width="15%">Poliklinik</th>
											<th width="15%">Dokter</th>
											<th width="10%">GC</th>
											<th width="10%">Skrining Pasien</th>
											<th width="10%">Skrining Covid</th>
											<th width="10%">Action</th>										   
										</tr>
										<tr>
											<th>#</th>
											<th>
												<select id="jenis_pertemuan_id" name="jenis_pertemuan_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													
													<option value="#" <?=($jenis_pertemuan_id=='#'?'selected':'')?>>-Pilih Jenis Pertemuan-</option>
													<?foreach(list_variable_ref(15) as $r){?>
														<option value="<?=$r->id?>" <?=($jenis_pertemuan_id==$r->id?'selected':'')?>><?=$r->nama?></option>
													<?}?>
													
												</select>
											</th>
											<th>
												<select id="idtipe_2" name="idtipe_2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" <?=($idtipe_2=='#'?'selected':'')?>>-Pilih Tipe-</option>
													<option value="1" <?=($idtipe_2=='1'?'selected':'')?>>Poliklinik</option>
													<option value="2" <?=($idtipe_2=='2'?'selected':'')?>>Instalasi Gawat Darurat</option>
													
												</select>
											</th>
											<th>
												<select id="idpoli_2" name="idpoli_2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" <?=($idpoli_2=='0'?'selected':'')?>>-All Poliklinik-</option>
												</select>
											</th>
											<th>
												<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="0" <?=($iddokter=='0'?'selected':'')?>>-All Dokter-</option>
													<?foreach($list_dokter as $row){?>
													<option value="<?=$row->id?>"><?=$row->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="st_gc" name="st_gc" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1" <?=($st_gc=='1'?'selected':'')?>>YA</option>
													<option value="0" <?=($st_gc=='0'?'selected':'')?>>TIDAK</option>
													
												</select>
											</th>
											<th>
												<select id="st_sp" name="st_sp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1" <?=($st_sp=='1'?'selected':'')?>>YA</option>
													<option value="0" <?=($st_sp=='0'?'selected':'')?>>TIDAK</option>
													
												</select>
											</th>
											<th>
												<select id="st_sc" name="st_sc" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1" <?=($st_sc=='1'?'selected':'')?>>YA</option>
													<option value="0" <?=($st_sc=='0'?'selected':'')?>>TIDAK</option>
													
												</select>
											</th>
											<th>
												<?php if (UserAccesForm($user_acces_form,array('1477'))){ ?>
												<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_logic" name="btn_tambah_logic"><i class="fa fa-plus"></i> Tambah</button>
												<?}?>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
						
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
	</div>
</div>

<div class="modal" id="modal_rekanan" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Asuransi</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="col-md-12">                               
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_rekanan">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="80%">Asuransi</th>
											<th width="15%">Pilih</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
						<div class="col-md-12">                               
							<br>
						</div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
$(document).ready(function(){	
	var tab=$("#tab").val();
	if (tab=='1'){
		load_general();
	}
	if (tab=='2'){
		load_logic();		
	}

})	
function load_general(){
	$('.js-summernote').summernote({
	  height: 100,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	});	
	load_poliklinik();
	list_kp();
	load_kelompok_pasien();
}
//PENDAFTARAN
$("#idtipe").change(function() {
	let idtipe=$("#idtipe").val();
	$.ajax({
		url: '{site_url}setting_reservasi_petugas/list_poli', 
		dataType: "JSON",
		method: "POST",
		data : {idtipe:idtipe},
		success: function(data) {
			$("#idpoli").empty();
			$("#idpoli").append(data);
		}
	});

});
function list_kp(){
	$.ajax({
		url: '{site_url}setting_reservasi_petugas/list_kp', 
		dataType: "JSON",
		method: "POST",
		data : {},
		success: function(data) {
			$("#idkelompokpasien").empty();
			$("#idkelompokpasien").append(data);
		}
	});

}
$("#btn_tambah_poli").click(function() {
	let idtipe=$("#idtipe").val();
	let idpoli=$("#idpoli").val();
	if ($("#idtipe").val()=='#'){
		sweetAlert("Maaf...", "Tentukan Tipe", "error");
		return false;
	}
	if ($("#idpoli").val()=='#'){
		sweetAlert("Maaf...", "Tentukan Poliklinik", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_reservasi_petugas/simpan_poli', 
		dataType: "JSON",
		method: "POST",
		data : {idpoli:idpoli},
		complete: function(data) {
			$("#idtipe").val('#').trigger('change');
			$('#index_poli').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});

$("#btn_tambah_kp").click(function() {
	let idkelompokpasien=$("#idkelompokpasien").val();
	if ($("#idkelompokpasien").val()=='#'){
		sweetAlert("Maaf...", "Tentukan Kelompok Pasien", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_reservasi_petugas/simpan_kp', 
		dataType: "JSON",
		method: "POST",
		data : {idkelompokpasien:idkelompokpasien},
		complete: function(data) {
			list_kp();
			$('#index_bayar').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
function load_poliklinik(){
	$('#index_poli').DataTable().destroy();	
	table = $('#index_poli').DataTable({
            autoWidth: false,
            searching: false,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_reservasi_petugas/load_poliklinik', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}

function hapus_poli(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Poliklinik?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_reservasi_petugas/hapus_poli',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_poli').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

function hapus_kelompok(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Kelompok Pasien?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_reservasi_petugas/hapus_kelompok',
				type: 'POST',
				data: {id: id},
				complete: function() {
					list_kp();
					$('#index_bayar').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function load_kelompok_pasien(){
	$('#index_bayar').DataTable().destroy();	
	table = $('#index_bayar').DataTable({
            autoWidth: false,
            searching: false,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_reservasi_petugas/load_kelompok_pasien', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}

function load_rekanan(id){
	$("#modal_rekanan").modal('show');
	$('#index_rekanan').DataTable().destroy();	
	table = $('#index_rekanan').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_reservasi_petugas/load_rekanan', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function check_save(id,pilih){
	// alert('id : '+id+' Pilih :'+pilih);
	$("#cover-spin").show();
		
	 $.ajax({
			url: '{site_url}setting_reservasi_petugas/check_save',
			type: 'POST',
			data: {id: id,pilih:pilih},
			complete: function() {
				$("#cover-spin").hide();
				$('#index_rekanan').DataTable().ajax.reload( null, false ); 
				$('#index_bayar').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Update'});
				
			}
	});
}

// LOGIC
function load_logic(){
	$('#index_logic').DataTable().destroy();	
	table = $('#index_logic').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_reservasi_petugas/load_logic', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
$("#idtipe_2").change(function() {
	let idtipe=$("#idtipe_2").val();
	let jenis_pertemuan_id=$("#jenis_pertemuan_id").val();
	$("#idpoli_2").empty();
	$.ajax({
		url: '{site_url}setting_reservasi_petugas/list_poli_2', 
		dataType: "JSON",
		method: "POST",
		data : {idtipe:idtipe,jenis_pertemuan_id:jenis_pertemuan_id},
		success: function(data) {
			
			$("#idpoli_2").append(data);
		}
	});

});
$("#btn_tambah_logic").click(function() {
	let jenis_pertemuan_id=$("#jenis_pertemuan_id").val();
	let idtipe=$("#idtipe_2").val();
	let idpoli=$("#idpoli_2").val();
	let iddokter=$("#iddokter").val();
	let st_gc=$("#st_gc").val();
	let st_sp=$("#st_sp").val();
	let st_sc=$("#st_sc").val();
	
	if (jenis_pertemuan_id=='#'){
		sweetAlert("Maaf...", "Tentukan Jenis Pertemuan", "error");
		return false;
	}
	if (idtipe=='#'){
		sweetAlert("Maaf...", "Tentukan Tipe", "error");
		return false;
	}
	if (idpoli=='#'){
		sweetAlert("Maaf...", "Tentukan Poliklinik", "error");
		return false;
	}
	if (iddokter=='#'){
		sweetAlert("Maaf...", "Tentukan Dokter", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_reservasi_petugas/simpan_logic', 
		dataType: "JSON",
		method: "POST",
		data : {
				jenis_pertemuan_id:jenis_pertemuan_id,
				idtipe:idtipe,
				idpoli:idpoli,
				iddokter:iddokter,
				st_gc:st_gc,
				st_sp:st_sp,
				st_sc:st_sc,
			
			},
		complete: function(data) {
			$("#jenis_pertemuan_id").val('#').trigger('change');
			$("#idtipe_2").val('#').trigger('change');
			$("#idpoli_2").val('0').trigger('change');
			$("#iddokter").val('0').trigger('change');
			$('#index_logic').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
function hapus_logic(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_reservasi_petugas/hapus_logic',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_logic').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
</script>