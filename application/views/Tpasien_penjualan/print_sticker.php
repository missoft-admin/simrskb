<!DOCTYPE html>
<html>
<!--untuk e-ticket
nanti di printer nya harus di setting kertas nya dulu
dengan ukuran 
160mm x 200 mm 
Ketas Label 121 Merk FOX
-->

<head>
	<title>E-Ticket Obat</title>
	<style type="text/css">
		@page {
			margin-top: 0;
			margin-left: 6.8em;
			margin-bottom: 0.5em;
		}

		* {
			color: black;
			font-style: normal;
			text-decoration: none;
			font-family: "Arial Rounded MT Bold", sans-serif;
			text-align: left;
			vertical-align: top;
			white-space: pre-wrap;
		}

		table {
			font-size: 12px !important;
			border-collapse: collapse !important;
			font-family: "Segoe UI", Arial, sans-serif;
		}

		.sembuh {
			font-size: 12px !important;
		}

		.text-header {
			font-size: 16px !important;
		}

		.installasi {
			font-size: 14px !important;
			font-weight: bold;
		}

		td {
			padding: 0px;
		}

		.text-center {
			text-align: center !important;
		}

		.text-left {
			text-align: left !important;
		}

		.cara_makan {
			color: blue;
		}
		.kegunaan {
			color: red;
		}

		.text-right {
			text-align: right !important;
		}

		/* text-style */
		.text-italic {
			font-style: italic;
		}

		.text-bold {
			font-weight: bold;
		}

	</style>
</head>

<body>
	<?php 
	$urutan=0;
	for ($i=1; $i <= 10; $i+=2) { $urutan=$i;?>
	<table style="width:160mm;">
		<tr>
			<td>
				<?
					if ($urutan>=$start_awal && $urutan<=$start_akhir){
						if ($list_data_ticket_obat_all[$urutan-1]->idtipe=='1'){
							$warna='#FFFFFFF';
						}else{
							$warna='#87CEEB';
						}
						
					}else{
						$warna='#FFFFFFF';
					}
					
				?>
				<table class="tg" style="width:75mm;" border="0" bgcolor="<?=$warna?>">
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<?
						$img='';
						if($urutan>=$start_awal && $urutan<=$start_akhir){
						$img='logolandscape.png';
						}else{
							$img='logoblank.png';
						}
						?>
						<td width="10mm" class="text-left" rowspan="3"><img src="{site_url}assets/upload/logo/<?=$img?>" alt="" style="width: 140px;height: 50px;"></td>
						<td></td>
						<td width="42mm" class="installasi" colspan="4"><?=($urutan>=$start_awal && $urutan<=$start_akhir)?'INSTALASI FARMASI':'&nbsp;'?></td>
					</tr>
					<tr>
						<td></td>
						<td class="text-left text-bold" colspan="4"><?=($urutan>=$start_awal && $urutan<=$start_akhir)?'Apoteker : '.$nama_apoteker:'&nbsp;'?></td>
					</tr>
					<tr>
						<td></td>
						<td class="text-left text-bold" colspan="4"><?=($urutan>=$start_awal && $urutan<=$start_akhir)?'SIPA : '.$sipa:'&nbsp;'?></td>
					</tr>
					<tr>
						<td></td>
						<td class="text-left text-bold" colspan="4"></td>
					</tr>
					<tr>
						<td><?=($urutan>=$start_awal && $urutan<=$start_akhir)?'No. '.$list_data_ticket_obat_all[$urutan-1]->nopenjualan:'&nbsp;'?></td>
						<td></td>
						<td></td>
						<td></td>
						<td colspan="2"><?=($urutan>=$start_awal && $urutan<=$start_akhir)?'Bandung, '.date('d/m/Y'):'&nbsp;'?></td>
					</tr>
					<tr>
						<td><?=($urutan>=$start_awal && $urutan<=$start_akhir)?(($list_data_ticket_obat_all[$urutan-1]->st_rujukan_non_rujukan=='N')?'Obat Bebas':GetAsalRujukan($list_data_ticket_obat_all[$urutan-1]->asalrujukan)):'&nbsp;'?></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td class="text-center" colspan="7"><?=($urutan>=$start_awal && $urutan<=$start_akhir) ? $list_data_ticket_obat_all[$urutan-1]->title.' .'.$list_data_ticket_obat_all[$urutan-1]->nama.' - '.$list_data_ticket_obat_all[$urutan-1]->nomedrec  : '&nbsp;'?></td>
					</tr>
					
					<tr>
						<td class="text-center" colspan="7"><?=($urutan>=$start_awal && $urutan<=$start_akhir) ? (($list_data_ticket_obat_all[$urutan-1]->tgl_lahir<>'0000-00-00') ? HumanDateShort($list_data_ticket_obat_all[$urutan-1]->tgl_lahir).' ('.$list_data_ticket_obat_all[$urutan-1]->umurtahun.'Th '.$list_data_ticket_obat_all[$urutan-1]->umurbulan.' Bln '.$list_data_ticket_obat_all[$urutan-1]->umurhari.' hr )'.' ('.($list_data_ticket_obat_all[$urutan-1]->jenis_kelamin=='1'?'L':'P').')' : '&nbsp;') : '&nbsp;'?></td>
					</tr>
					<tr>
						<td class="text-center" colspan="7"><?=($urutan>=$start_awal && $urutan<=$start_akhir) ? $list_data_ticket_obat_all[$urutan-1]->namaracikan : '&nbsp;'?></td>
					</tr>
					<tr>
						<td class="text-center" colspan="7"><strong><?=($urutan>=$start_awal && $urutan<=$start_akhir) ? ($type_etiket=='0'? $list_data_ticket_obat_all[$urutan-1]->label_obat_syrup : $list_data_ticket_obat_all[$urutan-1]->label_obat_lain) : '&nbsp;'?></strong></td>
					</tr>
					<tr>
						<td class="text-center" colspan="7"><span class="cara_makan"><?=($urutan>=$start_awal && $urutan<=$start_akhir) ? cara_minum($list_data_ticket_obat_all[$urutan-1]->jenispenggunaan).waktu_minum($list_data_ticket_obat_all[$urutan-1]->waktu_minum) : '&nbsp;'?></span></td>
					</tr>
					<tr>
						<td class="text-center" colspan="7"><span class="kegunaan"><?=($urutan>=$start_awal && $urutan<=$start_akhir) ? (($list_data_ticket_obat_all[$urutan-1]->kegunaan) ? $list_data_ticket_obat_all[$urutan-1]->kegunaan:'&nbsp;') : '&nbsp;'?></span></td>
					</tr>
					<tr>
						<td class="text-center" colspan="7"><?=($urutan>=$start_awal && $urutan<=$start_akhir)?'Expire Date :':'&nbsp;'?> <span class="cara_makan"><?=($urutan>=$start_awal && $urutan<=$start_akhir) ? HumanDateShort_exp($list_data_ticket_obat_all[$urutan-1]->expire_date) : '&nbsp;'?></span></td>
					</tr>
					<tr>
						<td class="text-center sembuh" colspan="7"><?=($urutan>=$start_awal && $urutan<=$start_akhir)?'SEMOGA LEKAS SEMBUH':'&nbsp;'?></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</td>
			<td style="width:0mm">&nbsp;</td>
			<?$urutan=$urutan+1;?>
			<td>
				<?
					if ($urutan>=$start_awal && $urutan<=$start_akhir){
						if ($list_data_ticket_obat_all[$urutan-1]->idtipe=='1'){
							$warna='#FFFFFFF';
						}else{
							$warna='#87CEEB';
						}
						
					}else{
						$warna='#FFFFFFF';
					}
				?>
				<table class="tg" style="width:75mm;" border="0" bgcolor="<?=$warna?>">
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<?
						$img='';
						if($urutan>=$start_awal && $urutan<=$start_akhir){
						$img='logolandscape.png';
						}else{
							$img='logoblank.png';
						}
						?>
						<td width="10mm" class="text-left" rowspan="3"><img src="{site_url}assets/upload/logo/<?=$img?>" alt="" style="width: 140px;height: 50px;"></td>
						<td></td>
						<td width="42mm" class="installasi" colspan="4"><?=($urutan>=$start_awal && $urutan<=$start_akhir)?'INSTALASI FARMASI':'&nbsp;'?></td>
					</tr>
					<tr>
						<td></td>
						<td class="text-left text-bold" colspan="4"><?=($urutan>=$start_awal && $urutan<=$start_akhir)?'Apoteker : '.$nama_apoteker:'&nbsp;'?></td>
					</tr>
					<tr>
						<td></td>
						<td class="text-left text-bold" colspan="4"><?=($urutan>=$start_awal && $urutan<=$start_akhir)?'SIPA : '.$sipa:'&nbsp;'?></td>
					</tr>
					<tr>
						<td></td>
						<td class="text-left text-bold" colspan="4"></td>
					</tr>
					<tr>
						<td><?=($urutan>=$start_awal && $urutan<=$start_akhir)?'No. '.$list_data_ticket_obat_all[$urutan-1]->nopenjualan:'&nbsp;'?></td>
						<td></td>
						<td></td>
						<td></td>
						<td colspan="2"><?=($urutan>=$start_awal && $urutan<=$start_akhir)?'Bandung, '.date('d/m/Y'):'&nbsp;'?></td>
					</tr>
					<tr>
						<td><?=($urutan>=$start_awal && $urutan<=$start_akhir)?(($list_data_ticket_obat_all[$urutan-1]->st_rujukan_non_rujukan=='N')?'Obat Bebas':GetAsalRujukan($list_data_ticket_obat_all[$urutan-1]->asalrujukan)):'&nbsp;'?></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td class="text-center" colspan="7"><?=($urutan>=$start_awal && $urutan<=$start_akhir) ? $list_data_ticket_obat_all[$urutan-1]->title.' .'.$list_data_ticket_obat_all[$urutan-1]->nama.' - '.$list_data_ticket_obat_all[$urutan-1]->nomedrec  : '&nbsp;'?></td>
					</tr>
					
					<tr>
						<td class="text-center" colspan="7"><?=($urutan>=$start_awal && $urutan<=$start_akhir) ? (($list_data_ticket_obat_all[$urutan-1]->tgl_lahir<>'0000-00-00') ? HumanDateShort($list_data_ticket_obat_all[$urutan-1]->tgl_lahir).' ('.$list_data_ticket_obat_all[$urutan-1]->umurtahun.'Th '.$list_data_ticket_obat_all[$urutan-1]->umurbulan.' Bln '.$list_data_ticket_obat_all[$urutan-1]->umurhari.' hr )'.' ('.($list_data_ticket_obat_all[$urutan-1]->jenis_kelamin=='1'?'L':'P').')' : '&nbsp;') : '&nbsp;'?></td>
					</tr>
					<tr>
						<td class="text-center" colspan="7"><?=($urutan>=$start_awal && $urutan<=$start_akhir) ? $list_data_ticket_obat_all[$urutan-1]->namaracikan : '&nbsp;'?></td>
					</tr>
					<tr>
						<td class="text-center" colspan="7"><strong><?=($urutan>=$start_awal && $urutan<=$start_akhir) ? ($type_etiket=='0'? $list_data_ticket_obat_all[$urutan-1]->label_obat_syrup : $list_data_ticket_obat_all[$urutan-1]->label_obat_lain) : '&nbsp;'?></strong></td>
					</tr>
					<tr>
						<td class="text-center" colspan="7"><span class="cara_makan"><?=($urutan>=$start_awal && $urutan<=$start_akhir) ? cara_minum($list_data_ticket_obat_all[$urutan-1]->jenispenggunaan).waktu_minum($list_data_ticket_obat_all[$urutan-1]->waktu_minum) : '&nbsp;'?></span></td>
					</tr>
					<tr>
						<td class="text-center" colspan="7"><span class="kegunaan"><?=($urutan>=$start_awal && $urutan<=$start_akhir) ? (($list_data_ticket_obat_all[$urutan-1]->kegunaan) ? $list_data_ticket_obat_all[$urutan-1]->kegunaan:'&nbsp;') : '&nbsp;'?></span></td>
					</tr>
					<tr>
						<td class="text-center" colspan="7"><?=($urutan>=$start_awal && $urutan<=$start_akhir)?'Expire Date :':'&nbsp;'?> <span class="cara_makan"><?=($urutan>=$start_awal && $urutan<=$start_akhir) ? HumanDateShort_exp($list_data_ticket_obat_all[$urutan-1]->expire_date) : '&nbsp;'?></span></td>
					</tr>
					<tr>
						<td class="text-center sembuh" colspan="7"><?=($urutan>=$start_awal && $urutan<=$start_akhir)?'SEMOGA LEKAS SEMBUH':'&nbsp;'?></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="height : 3mm"></td>
		</tr>
	</table>
	<?php } ?>
</body>

</html>
