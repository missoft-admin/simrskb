<div class="modal fade in black-overlay" id="obat_non_racikan-modal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Obat</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-8">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Tipe Barang</label>
								<div class="col-md-8">
									<select name="tipeid" id="tipeid" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="#" selected>-Pilih Semua-</option>
										<?foreach ($list_tipe_barang as $r){?>
										<option value="<?=$r->idtipe?>"><?=$r->nama_tipe?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="idkat">Kategori Barang</label>
								<div class="col-md-8">
									<select name="idkat" id="idkat" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="#" selected>-Pilih Semua-</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<div class="col-md-12">
									<button class="btn btn-success text-uppercase" type="button" id="btn_filter_barang" name="btn_filter_barang" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i>
										Filter</button>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content">
					<table width="100%" id="table-Rujukan" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Kode Obat</th>
								<th>Nama Obat</th>
								<th>Satuan</th>
								<th>Stok</th>
								<th>Catatan</th>
								<th>Kartu Stok</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade in black-overlay" id="list_obat_racikan-modal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List Obat Racikan UNTUK [<span id="nama_obat_racikan"></span>]</h3>
				</div>
				<div class="block-content">
					<table id="manage-tabel-Racikan" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 20%;">Nama Obat</th>
								<th style="width: 10%;">Harga</th>
								<th style="width: 5%;">Satuan</th>
								<th style="width: 7%;">Qty</th>
								<th style="width: 9%;">Disc(%)</th>
								<th style="width: 12%;">Jumlah</th>
								<th style="width: 12%;">Actions</th>
							</tr>
							<tr>
								<td>
									<div id="" class="input-group">
										<select name="lbl_obat_id2" tabindex="16" style="width: 100%" id="lbl_obat_id2" data-placeholder="Cari Obat" class="form-control input-sm"></select>
										<span class="input-group-btn">
											<button data-toggle="modal" data-target="#obat_racikan-modal" tabindex="17" class="btn btn-sm btn-info" type="button" id="search_obat_2"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</td>
								<td><label class="datatrans" id="lbltarif2"> - </label></td>
								<td><label class="datatrans" id="lblsatuan2"> - </label></td>
								<td><input type="text" class="form-control input-sm" tabindex="18" id="lbl_qty2" onkeypress="return hanyaAngka(event)" /></td>
								<td><input type="text" class="form-control input-sm" tabindex="19" id="lbl_disc2" onkeypress="return hanyaAngka(event)" /></td>
								<td><label class="datatrans" id="lbl_jumlah2"> - </label></td>
								<td>
									<input type="hidden" id="idtipe2">
									<input type="hidden" id="tstok2">
									<input type="hidden" id="nomor2">
									<input type="hidden" id="rowindex2">
									<input type="hidden" class="form-control" id="lbl_nama_obat2" />
									<input type="hidden" class="form-control" id="ttarif2" />
									<input type="hidden" class="form-control" id="tjumlah2" />
									<button type="button" class="btn btn-sm  btn-primary" tabindex="20" id="addjual2" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
									<button type='button' class='btn btn-sm btn-danger' tabindex="21" id="canceljual2" title="Refresh"><i class='glyphicon glyphicon-remove'></i></button>
								</td>
							</tr>
						</thead>
						<tbody></tbody>
						<tfoot id="foot-total-nonracikan">
							<tr>
								<th colspan="5" class="hidden-phone">
									<span class="pull-right"><b>TOTAL</b></span></th>
								<input type="hidden" id="tempgrandtotal2" name="tempgrandtotal">
								<th colspan="2"><b><span id="lblgrandtotal2">0</span></b></th>
							</tr>
						</tfoot>
					</table>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-primary" disabled id="btn_ok_racikan" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade in black-overlay" id="obat_racikan-modal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:60%; margin-top:100px">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Daftar Obat</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-8">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid2">Tipe Barang</label>
								<div class="col-md-8">
									<select name="tipeid2" id="tipeid2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="#" selected>-Pilih Semua-</option>
										<?foreach ($list_tipe_barang as $r){?>
										<option value="<?=$r->idtipe?>"><?=$r->nama_tipe?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="idkat2">Kategori Barang</label>
								<div class="col-md-8">
									<select name="idkat2" id="idkat2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="#" selected>-Pilih Semua-</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<div class="col-md-12">
									<button class="btn btn-success text-uppercase" type="button" id="btn_filter_barang2" name="btn_filter_barang2" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i>
										Filter</button>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="block-content">
					<table width="100%" id="table-Rujukan2" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Kode Obat</th>
								<th>Nama Obat</th>
								<th>Satuan</th>
								<th>Stok</th>
								<th>Catatan</th>
								<th>Kartu Stok</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal in" id="cari-pasien-modal" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:70%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Data Pasien</h3>
				</div>
				<div class="block-content">
					<hr>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snomedrec">No. Medrec</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snomedrec" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snamapasien">Nama Pasien</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snamapasien" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="sjeniskelamin">Jenis Kelamin</label>
									<div class="col-md-8">
										<label class="css-input css-radio css-radio-sm css-radio-default push-10-r">
											<input type="radio" class="sjeniskelamin" name="sjeniskelamin" value="1"><span></span> Laki-laki
										</label>
										<label class="css-input css-radio css-radio-sm css-radio-default">
											<input type="radio" class="sjeniskelamin" name="sjeniskelamin" value="2"><span></span> Perempuan
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="salamat">Alamat</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="salamat" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="stanggallahir">Tanggal Lahir</label>
									<div class="col-md-8">
										<input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="stanggallahir" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snotelepon">Telepon</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snotelepon" value="">
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<table width="100%" class="table table-bordered table-striped" id="datatable-pasien">
						<thead>
							<tr>
								<th>No. Medrec</th>
								<th>Nama</th>
								<th>Jenis Kelamin</th>
								<th>Tanggal Lahir</th>
								<th>Alamat</th>
								<th>Telepon</th>
								<th>Status Kawin</th>
							</tr>
						</thead>
						<tbody id="pasienmodaltabel">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade in black-overlay" id="waktu_modal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">WAKTU Minum Obat</h3>
				</div>
				<div class="block-content">
					<table id="manage-tabel-Waktu" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 40%;">Waktu Minum</th>
								<th style="width: 30%;">Jam Minum</th>
								<th style="width: 30%;">Actions</th>
							</tr>
							<tr>
								<td>
									<select id="waktu_minum" class="form-control  input-sm" tabindex="3">
										<option value="" selected>-Pilih-</option>
										<option value="PAGI">PAGI</option>
										<option value="SIANG">SIANG</option>
										<option value="SORE">SORE</option>
										<option value="MALAM">MALAM</option>
									</select>
								</td>
								<td>
									<input type="text" name="jam_minum" id="jam_minum" value="00:00" class="form-control" placeholder="" value="" required="" aria-required="true">
								</td>

								<td>
									<input type="hidden" id="nomor_waktu">
									<button type="button" class="btn btn-sm  btn-primary" tabindex="20" id="addwaktu" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
									<button type='button' class='btn btn-sm btn-danger' tabindex="21" id="cancelwaktu" title="Hapus"><i class='glyphicon glyphicon-remove'></i></button>
								</td>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-primary" disabled id="btn_ok_waktu" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade in black-overlay" id="waktu_racikan_modal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">WAKTU Minum Obat</h3>
				</div>
				<div class="block-content">
					<table id="manage-tabel-Waktu_racikan" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 40%;">Waktu Minum</th>
								<th style="width: 30%;">Jam Minum</th>
								<th style="width: 30%;">Actions</th>
							</tr>
							<tr>
								<td>
									<select id="waktu_minum_racikan" class="form-control  input-sm" tabindex="3">
										<option value="" selected>-Pilih-</option>
										<option value="PAGI">PAGI</option>
										<option value="SIANG">SIANG</option>
										<option value="SORE">SORE</option>
										<option value="MALAM">MALAM</option>
									</select>
								</td>
								<td>
									<input type="text" name="jam_minum_racikan" id="jam_minum_racikan" value="00:00" class="form-control" placeholder="" value="" required="" aria-required="true">
								</td>

								<td>
									<input type="hidden" id="nomor_waktu_racikan">
									<button type="button" class="btn btn-sm  btn-primary" tabindex="20" id="addwakturacikan" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
									<button type='button' class='btn btn-sm btn-danger' tabindex="21" id="cancelwakturacikan" title="Hapus"><i class='glyphicon glyphicon-remove'></i></button>
								</td>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-primary" disabled id="btn_ok_waktu_racikan" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
				</div>
			</div>
		</div>
	</div>
</div>
