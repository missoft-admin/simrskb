<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-rounded">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tpasien_penjualan/index" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-bottom: 0px;">
	</div>
<div class="block-content">
<form id="form1"  class=" form-horizontal push-10-t" action="{base_url}/tpasien_penjualan/save" method="post">
<input type="hidden"  name="json-detail-nonracikan" id="json-detail-nonracikan" value=""/>
<input type="hidden"  name="json-detail-racikan" id="json-detail-racikan" value=""/>
<input type="hidden"  name="json-detail-obat-racikan" id="json-detail-obat-racikan" value=""/>
<input type="hidden"  name="post-totalbarang" id="post-totalbarang" value=""/>
<input type="hidden"  name="id_penjualan" id="id_penjualan" value="{id_penjualan}"/>
<!-- Start Input Data Pasien -->
<?php if($st_rujukan_non_rujukan=='R'){?>
	<div class="col-sm-6 col-lg-12">
		<?php }else{?>
	<div class="col-sm-12 col-lg-12">
		<?php }?>
		<div class="block block-rounded block-bordered">
				<div class="block-header">
					<h3 class="block-title">DETAIL PASIEN</h3>
				</div>
				<div class="block-content">

					<table class="table">

						<?php
						if($st_rujukan_non_rujukan=='N'){
						?>
						<tr>
							<th width="20%">Tanggal <span class="text-danger">*</span></th>
							<td width="85%">
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
												<input class="js-datepicker form-control input-sm" type="text" id="manage-input-tanggal" name="manage-input-tanggal" data-date-format="dd-mm-yyyy" value="<?=date('d-m-Y')?>"/>
										</div>
								</div>
							</td>
						</tr>
						<tr>
							<th>Resep <span class="text-danger">*</span></th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-6">
											<select class="js-select2 form-control input-sm" id="manage-select-resep" name="manage-select-resep">
											  <option <?=($statusresep=='0')?'selected':''?> value="0">Tidak Ada</option>
											  <option <?=($statusresep=='1')?'selected':''?> value="1">Ada</option>
											</select>
										</div>
								</div>
							</td>
						</tr>
						<tr>
							<th>Kategori <span class="text-danger">*</span></th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
											<input type="hidden" id="idkelompokpasien" name="idkelompokpasien" value="0" />
											<select class="js-select2 form-control input-sm" id="idkategori" name="idkategori">
											  <option <?=($idkategori=='0')?'selected':''?> value="0">- Penjualan Bebas -</option>
											  <option <?=($idkategori=='1')?'selected':''?> value="1">Pasien RS</option>
											  <option <?=($idkategori=='2')?'selected':''?> value="2">Pegawai</option>
											  <option <?=($idkategori=='3')?'selected':''?> value="3">Dokter</option>
											</select>
										</div>
								</div>
							</td>
						</tr>
						<input type="hidden" class="form-control input-sm" readonly id="idpasien" name="idpasien" value="<?=$idpasien?>"/>
						<tr id="tr_cari_data" hidden>
							<th>No Medrec / ID <span class="text-danger">*</span></th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
									<div class="col-sm-6" id="data_pasien" hidden >
										<div class="input-group" >
											<input type="text" class="form-control input-sm" readonly id="no_medrec" name="no_medrec" value="{no_medrec}" />
											<span class="input-group-btn">
												<button id="btn_cari_data" disabled data-toggle="modal" data-target="#cari-pasien-modal"  class="btn btn-sm btn-info" type="button"><i class="fa fa-search"></i>  Cari Data</button>
											</span>
										</div>
									</div>
									<div class="col-sm-12" id="data_non_pasien" hidden >
										<select name="dokter_pegawai_id" id="dokter_pegawai_id" data-placeholder="Cari Data" class="form-control  input-sm" style="width:100%"></select>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<th>Nama <span class="text-danger">*</span></th>
									<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
											<input type="text"  class="form-control input-sm" id="nama" name="nama" value="<?=$nama?>"/>
										</div>
								</div>
							</td>
						</tr>
						<tr>
							<th>Alamat <span class="text-danger">*</span></th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
											<textarea class="form-control input-sm"  id="alamat" name="alamat"><?=$alamat?></textarea>
										</div>
								</div>
							</td>
						</tr>
						<tr>
							<th>No. Telp</th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
									<div class="col-sm-12">
										<input type="text" class="form-control input-sm"  name="telprumah" id="telprumah" value="{telprumah}"/>
									</div>
								</div>
							</td>
						</tr>
						<?php
					}else{
							foreach($listpasien1 as $listpasien){
$mpasien=getwhere('id',$listpasien->idasalpasien,'mpasien_asal')->row();
$rowsPasien=getwhere('id',$listpasien->idasalpasien,'mpasien_asal')->num_rows();
								if($rowsPasien>0){
$asalpasien=$mpasien->nama;
								}else{
$asalpasien='';
								}
							?>
							<input type="hidden" name="asalrujukan" id="asalrujukan" value="<?=$asalrujukan?>">
							<input type="hidden" name="getIDtindakan" id="getIDtindakan" value="<?=(!empty($id_penjualan))?$listpasien->idtindakan:''?>">
							<input type="hidden" name="getIDpasien" id="getIDpasien" value="<?=(!empty($id_penjualan))?$listpasien->idpasien:''?>">
							<!-- <input type="hidden" name="getIDbatal" id="getIDbatal" value="{getIDbatal}"> -->
						<tr width="30%">
							<th width="20%">Tanggal <span class="text-danger">*</span></th>
							<td width="35%">
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
												<input class="js-datepicker form-control input-sm" type="text" id="manage-input-tanggal" name="manage-input-tanggal" data-date-format="dd-mm-yyyy" value="<?=date('d-m-Y')?>"/>
										</div>
								</div>
							</td>
							<th>Rujukan</th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
											<input readonly type="text" class="form-control input-sm" id="manage-input-rujukan-pasien" name="manage-input-rujukan-pasien"  value="<?=(!empty($id_penjualan))?$asalpasien:''?>"/>
										</div>
								</div>
							</td>
						</tr>
						<tr>
							<th>Status Pasien <span class="text-danger">*</span></th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
									<div class="col-sm-12">
										<label class="css-input css-radio css-radio-sm css-radio-primary push-10-r">
											<input type="radio" id="manage-input-status-pasien" name="manage-input-status-pasien" checked value="1" /><span></span> Pasien RS
										</label>
									</div>
								</div>
							</td>
							<th>Kelompok Pasien</th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
									<div class="col-sm-12">
										<input readonly type="text" class="form-control input-sm" id="manage-input-nama-kelompok" name="manage-input-nama-kelompok" value="<?=(!empty($id_penjualan))?$listpasien->namakelompok:''?>" />
										<input readonly type="hidden" class="form-control input-sm" id="idkelompokpasien" name="idkelompokpasien" value="<?=(!empty($id_penjualan))?$listpasien->idkelompokpasien:''?>" />
									</div>
								</div>
							</td>

						</tr>
						<tr>
							<th>Resep <span class="text-danger">*</span></th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
											<select class="js-select2 form-control input-sm" id="manage-select-resep" name="manage-select-resep">
											  <option <?=($statusresep=='1')?'selected':''?> value="1">Ada</option>
											  <option <?=($statusresep=='0')?'selected':''?> value="0">Tidak Ada</option>
											</select>
										</div>
								</div>
							</td>
							<?php if($listpasien->asalrujukan != 3){ ?>
								<th>Poliklinik</th>
								<td>
									<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
											<div class="col-sm-12">
												<input readonly type="text" class="form-control input-sm" id="manage-input-nama-poliklinik" name="manage-input-nama-poliklinik" value="<?=(!empty($id_penjualan))?$listpasien->namapoliklinik:''?>" />
											</div>
									</div>
								</td>
							<?php }else{ ?>
								<th>Kelas</th>
								<td>
									<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
											<div class="col-sm-12">
												<select name="idkelas" id="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
												
													<?php foreach (get_all('mkelas',array('status' => 1,'')) as $row){?>
														<option value="<?=$row->id?>" <?=($row->id == $idkelas ? 'selected':'')?>><?=$row->nama?></option>
													<?php } ?>
												
												</select>
											</div>
									</div>
								</td>
							<?php } ?>

						</tr>
						<tr>
							<th>No. Medrec <span class="text-danger">*</span></th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
											<input type="text" class="form-control input-sm" id="manage-input-medrec-pasien" name="manage-input-medrec-pasien" readonly value="<?=(!empty($id_penjualan))?$listpasien->no_medrec:''?>" />
										</div>
								</div>
							</td>

							<?php if($listpasien->asalrujukan != 3){ ?>
								<th>Alergi Obat</th>
								<td>
									<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
											<div class="col-sm-12">
												<textarea readonly class="form-control input-sm" id="manage-input-alergi-pasien" name="manage-input-alergi-pasien"><?=(!empty($id_penjualan))?$listpasien->alergi_obat:''?></textarea>
											</div>
									</div>
								</td>
							<?php }else{ ?>
								<th>Bed</th>
								<td>
									<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
											<div class="col-sm-12">
												<select name="idbed" id="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
													<option value=''>Pilih Opsi</option>														
													<?php foreach ($list_bed as $row){?>
														<option value="<?=$row->id?>" <?=($row->id == $idbed ? 'selected':'')?>><?=$row->nama?></option>
													<?php } ?>
												</select>
											</div>
									</div>
								</td>
							<?php } ?>



						</tr>
						<tr>
							<th>Nama Pasien <span class="text-danger">*</span></th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
											<input readonly type="text" class="form-control input-sm" id="manage-input-nama-pasien" name="manage-input-nama-pasien" value="<?=(!empty($id_penjualan))?$listpasien->namapasien:''?>" />
										</div>
								</div>
							</td>
							<?php if($listpasien->asalrujukan != 3){ ?>

							<?php }else{ ?>
								<th>Alergi Obat</th>
								<td>
									<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
											<div class="col-sm-12">
												<textarea readonly class="form-control input-sm" id="manage-input-alergi-pasien" name="manage-input-alergi-pasien"><?=(!empty($id_penjualan))?$listpasien->alergi_obat:''?></textarea>
											</div>
									</div>
								</td>
								
							<?php } ?>

						</tr>
						<tr>
							<th>Alamat <span class="text-danger">*</span></th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
											<textarea readonly class="form-control input-sm" id="manage-textarea-alamat-pasien" name="manage-textarea-alamat-pasien"><?=(!empty($id_penjualan))?$listpasien->alamat_jalan:''?></textarea>
										</div>
								</div>
							</td>
							<th>Dokter</th>
								<td>
									<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
									<div class="col-sm-12">
										<select name="iddokter" id="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="" <?=($iddokter == '' ? 'selected' : '')?>>Pilih Dokter</option>
											<?foreach($list_dokter as $row){?>
												<option value="<?=$row->id?>" <?=($iddokter==$row->id ? 'selected':'')?>><?=$row->nama?></option>											
											<?}?>
										</select>
									</div>
									</div>
								</td>
						</tr>
						<tr>
							<th>No. Telp</th>
							<td>
							<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
									<div class="col-sm-12">
										<input readonly type="text" class="form-control input-sm" name="manage-input-notelp-pasien" id="manage-input-notelp-pasien" value="<?=(!empty($id_penjualan))?$listpasien->telepon:''?>" />
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<th>Asal Pasien</th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
									<div class="col-sm-12">
										<input readonly type="text" class="form-control input-sm" id="manage-input-asal-pasien" name="manage-input-asal-pasien" value="<?=GetAsalRujukan($listpasien->asalrujukan)?>" />
									</div>
								</div>
							</td>
						</tr>
						<?php
					}
					}
					?>
					</table>
				<!-- </div>
			</div>
		</div> -->
	</div>
</div>
</div>
		<!-- End Input Data Pasien -->

		<!-- List Obat Non-Racikan -->
			<div class="row">
				<div class="col-sm-12">
					<div class="progress progress-mini">
						<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
					</div>
					<h3 style="margin-bottom: 10px;">NON RACIKAN</h5>
					<div class="control-group">
						<div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0;">
							<table id="manage-tabel-nonRacikan" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 20%;">Nama Obat</th>
										<th style="width: 7%;">Tarif</th>
										<th style="width: 5%;">Satuan</th>
										<th style="width: 7%;">Cara Pakai</th>
										<th style="width: 10%;">Aturan</th>
										<th style="width: 7%;">Qty</th>
										<th style="width: 5%;">Disc(%)</th>
										<th style="width: 7%;">Tuslah</th>
										<th style="width: 11%;">Expire</th>
										<th style="width: 10%;">Jumlah</th>
										<th style="width: 15%;">Actions</th>
									</tr>
									<tr>
										<td><div id="" class="input-group">
											<select name="lbl_obat_id" tabindex="1" id="lbl_obat_id" data-placeholder="Cari Obat" class="form-control  input-sm"></select>
											<span class="input-group-btn">
												<button data-toggle="modal" data-target="#obat_non_racikan-modal" class="btn btn-sm btn-info" type="button" id="search_obat_1"><i class="fa fa-search"></i></button>
											</span>
										</div></td>
										<td><label class="datatrans" id="lbltarif"> - </label></td>
										<td><label class="datatrans" id="lblsatuan"> - </label></td>
										<td>
											<input class="form-control input-sm" tabindex="2" type="text" id="lbl_cara_pakai" onkeypress="return hanyaAngka(event)" onfocus="inMask(this)"  />
										</td>
										<td>
											<select id="pemakaian"class="form-control  input-sm" tabindex="3">
												<option value="SBM">SBM</option>
												<option value="SSM">SSM</option>
												<option value="BM">BM</option>
											</select>
										</td>
										<td><input type="text" class="form-control  input-sm" tabindex="4" id="lbl_qty" onkeypress="return hanyaAngka(event)"/></td>
										<td><input type="text" class="form-control input-sm" id="lbl_disc" tabindex="5" onkeypress="return hanyaAngka(event)" /></td>
										<td><input type="text" class="form-control input-sm" id="lbl_tuslah" tabindex="6" onkeypress="return hanyaAngka(event)"/></td>
										<td><input class="form-control input-sm" type="text" tabindex="7" id="lbl_expire" name="lbl_expire" data-date-format="dd-mm-yyyy" value="-"/></td>
										<td><label class="datatrans" id="lbl_jumlah"> - </label></td>
										<td>
											<input type="hidden" id="idtipe" value="0">
											<input type="hidden" id="tstatusverifikasi" value="0">
											<input type="hidden" id="tstok" >
											<input type="hidden" id="nomor" >
											<input type="hidden" id="rowindex" >
											<input type="hidden" class="form-control" id="lbl_nama_obat"/>
											<input type="hidden" class="form-control" id="ttarif"/>
											<input type="hidden" class="form-control" id="tjumlah"/>
											<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="addjual" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
											<button type='button' class='btn btn-sm btn-danger' tabindex="9" id="canceljual" title="Refresh"><i class='fa fa-times'></i></button>
									</td>
									</tr>
								</thead>
								<tbody>
									<?php if ($list_non_racikan){
										foreach($list_non_racikan as $row){

									?>
										<tr>
										<td><?=$row->nama?></td>
										<td style='display:none'><?=$row->idbarang?></td>
										<td style='display:none'><?=$row->harga?></td>
										<td align="right"><?=number_format($row->harga,0)?></td>
										<td><?=$row->singkatan?></td>
										<td><?=$row->carapakai?></td>
										<td><?=$row->jenispenggunaan?></td>
										<td align="right"><?=$row->kuantitas?></td>
										<td align="right"><?=$row->diskon?></td>
										<td align="right"><?=$row->tuslah?></td>
										<td align="center"><?=HumanDateShort($row->expire_date)?></td>
										<td align="right"><?=number_format($row->totalharga,0)?></td>
										<td style='display:none'><?=$row->totalharga?></td>
										<td>
											<?php if($row->statusverifikasi == 0){ ?>
												<button type='button' class='btn btn-sm btn-info edit'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus'><i class='glyphicon glyphicon-remove'></i></button>
											<?php }else{ ?>
												<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>
											<?php } ?>
										</td>
										<td style='display:none'><?=$row->id?></td>
										<td style='display:none'><?=$row->statusverifikasi?></td>
										<td style='display:none'><?=$row->idtipe?></td>
										</tr>
										<?php }
									}?>
								</tbody>
								<tfoot id="foot-total-nonracikan">
									<tr>
										<th colspan="9" class="hidden-phone">

											<span class="pull-right"><b>TOTAL</b></span></th>
											<input type="hidden" id="tempgrandtotal" name="tempgrandtotal" value="{total_non_racikan}">
										<th colspan="2"><b><span id="lblgrandtotal"><?=number_format($total_non_racikan)?></span></b></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- Modal Obat Non Racikan -->
			<div class="modal fade in black-overlay" id="obat_non_racikan-modal" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg modal-dialog-popout">
					<div class="modal-content">
						<div class="block block-themed block-transparent remove-margin-b">
							<div class="block-header bg-success">
								<ul class="block-options">
									<li>
										<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
									</li>
								</ul>
								<h3 class="block-title">Daftar Obat</h3>
							</div>
							<div class="block-content">

									<table width="100%" id="table-Rujukan" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th>Kode Obat</th>
												<th>Nama Obat</th>
												<th>Satuan</th>
												<th>Stok</th>
												<th>Catatan</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
							</div>

						</div>
					</div>
				</div>
			</div>

		<!-- END Obat Racikan -->
		<!-- List Obat Racikan -->
			<div class="row">
				<div class="col-sm-12">
					<div class="progress progress-mini">
						<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
					</div>
					<h3 style="margin-bottom: 10px;">RACIKAN</h5>
					<div class="control-group">
						<div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0;">
							<table id="table_racikan_detail" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 20%;">Nama</th>
										<th style="width: 12%;">Jenis</th>
										<th style="width: 7%;">Cara Pakai</th>
										<th style="width: 9%;">Aturan</th>
										<th style="width: 5%;">Qty Racikan</th>
										<th style="width: 7%;">Tuslah</th>
										<th style="width: 10%;">Expire</th>
										<th style="width: 7%;">Harga</th>
										<th style="width: 8%;">Jumlah</th>
										<th style="width: 10%;">Actions</th>
									</tr>
									<tr>
										<td>
											<div id="" class="input-group">
												<input class="form-control input-sm nama_racikan" tabindex="9" type="text" id="rac_nama" />
												<span class="input-group-btn">
													<button class="btn btn-info input-sm btn_list_obat_racikan" tabindex="10"  id="btn_list_obat_racikan" data-toggle="modal"   type="button"><span id="lbl_rows_obat">List Obat (0)</span></button>
												</span>
											</div>
										</td>
										<td>
											<select style="width: 100%" tabindex="11"  class="form-control input-sm" id="rac_jenis">
											  <option value="0">-- Pilih Jenis --</option>
											  <option value="1">Kapsul</option>
											  <option value="2">Sirup</option>
											  <option value="3">Salep</option>
											  <option value="99">Lainnya</option>
											</select>
										</td>
										<td>
											<input class="form-control input-sm" tabindex="12" type="text" id="rac_cara_pakai" onkeypress="return hanyaAngka(event)" onfocus="inMask(this)" />
										</td>
										<td>
											<select id="rac_aturan"class="form-control input-sm" >
												<option value="SBM">SBM</option>
												<option value="SSM">SSM</option>
												<option value="BM">BM</option>
											</select>
										</td>


										<td>
											<input class="form-control input-sm " tabindex="13" type="text" id="rac_qty" onkeypress="return hanyaAngka(event)" />

										</td>
										<td>
											<input class="form-control input-sm" tabindex="14" type="text" id="rac_tuslah" onkeypress="return hanyaAngka(event)" />
										</td>

										<td><input class="js-datepicker form-control input-sm" type="text" tabindex="15" id="rac_expire" name="rac_expire" data-date-format="dd-mm-yyyy" value="-"/></td>

										<td>
											<span id="rac_harga_label">0</span>
											<input class="form-control input-sm" type="hidden"  id="rac_harga" onkeypress="return hanyaAngka(event)" />
										</td>
										<td>
											<span id="rac_jumlah_label">0</span>
											<input type="hidden" id="rac_jumlah" value="0">
										</td>
										<td>
											<input type="hidden" id="statusverifikasi_rac" value="0">
											<input type="hidden" id="rac_jml_racikan" value="0">
											<input type="hidden" id="no_urut" value="{no_urut}">
											<input type="hidden" id="no_urut_akhir" value="{no_urut_akhir}">
											<input type="hidden" id="st_edit" value="0">
											<input type="hidden" id="rowindex_rac">
											<input type="hidden" id="nomor_rac">
											<button type="button" class="btn btn-sm  btn-primary" tabindex="16" id="rac_addjual" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
											<button type='button' class='btn btn-sm btn-danger' tabindex="17" id="rac_canceljual" title="Refresh"><i class='glyphicon glyphicon-remove'></i></button>
										</td>

								</thead>
								<tbody>
									<?php if (count($list_racikan)>0){
										foreach($list_racikan as $row){
											if ($row->id != null){
										$nama='<div class="input-group"><input class="form-control input-sm" type="text" readonly id="label_racikan" value="'.$row->namaracikan.'" /><span class="input-group-btn"><button class="btn btn-success input-sm btn_detail_racikan" data-toggle="modal"  type="button"><span id="lbl_rows_obat">List Obat ('.$row->total_row.')</span></button></span></div>';
									?>
										<tr>
											<td style='display:none'><?=$row->id?></td>
											<td style='display:none'><?=$row->namaracikan?></td>
											<td><?=$nama?></td>
											<td><?=jenis_racikan($row->jenisracikan)?></td>
											<td><?=($row->carapakai)?></td>
											<td><?=($row->jenispenggunaan)?></td>
											<td align="right"><?=$row->kuantitas?></td>
											<td align="right"><?=$row->tuslah?></td>
											<td align="center"><?=HumanDateShort($row->expire_date)?></td>
											<td align="right"><?=number_format($row->harga,0)?></td>
											<td align="right"><?=number_format($row->totalharga,0)?></td>
											<td style='display:none'><?=$row->totalharga?></td>
											<td >
												<?php if($row->statusverifikasi == 0){ ?>
													<button type='button' class='btn btn-sm btn-info rac_edit'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger rac_hapus'><i class='glyphicon glyphicon-remove'></i></button>
												<?php }else{ ?>
													<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>
												<?php } ?>
											</td>
											<td style='display:none'><?=$row->id?></td>
											<td style='display:none'><?='tabel'.$row->id?></td>
											<td style='display:none'><?=$row->harga?></td>
											<td style='display:none'><?=$row->total_row?></td>
											<td style='display:none'><?=$row->jenisracikan?></td>
											<td style='display:none'><?=$row->statusverifikasi?></td>

										</tr>
											<?php }}
									}?>
								</tbody>
								<tfoot>
									<tr>
										<th colspan="8" class="hidden-phone">

										<label class="pull-right"><b>TOTAL</b></label></th>
										<th colspan="2"><b>
											<input type="hidden" id="total_racikan" name="total_racikan" value="{total_harga_racikan}" />
											<label id="total_racikan_label"><?=number_format($total_harga_racikan,0)?></label></b>
										</th>
									</tr>
									<tr class="bg-light dker">
										<th colspan="8"><label class="pull-right"><b>GRAND TOTAL</b></label></th>
										<th colspan="2"><b>
											<input type="hidden" id="gt_all" name="gt_all" value="{totalall}" />
											<span id="gt_all_label"><b><h4><?=number_format($totalall,0)?></h1></b></span></b>
										</th>
									</tr>
								</tfoot>
							</table>

					<input type="hidden" id="counter" name="counter" value="0" />
					<input type="hidden" id="counter_racikan" name="counter_racikan" value="0" />
						</div>
					</div>
				</div>
			</div>
			<!-- Modal Obat Racikan -->
			<div class="modal fade in black-overlay" id="list_obat_racikan-modal" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg modal-dialog-popout">
					<div class="modal-content">
						<div class="block block-themed block-transparent remove-margin-b">
							<div class="block-header bg-success">
								<ul class="block-options">
									<li>
										<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
									</li>
								</ul>
								<h3 class="block-title">List Obat Racikan  UNTUK [<span id="nama_obat_racikan"></span>]</h3>
							</div>
							<div class="block-content">
								<table id="manage-tabel-Racikan" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 20%;">Nama Obat</th>
											<th style="width: 10%;">Harga</th>
											<th style="width: 5%;">Satuan</th>
											<th style="width: 7%;">Qty</th>
											<th style="width: 9%;">Disc(%)</th>
											<th style="width: 12%;">Jumlah</th>
											<th style="width: 12%;">Actions</th>
										</tr>
										<tr>
											<td><div id="" class="input-group">
												<select name="lbl_obat_id2" tabindex="16"  style="width: 100%" id="lbl_obat_id2" data-placeholder="Cari Obat" class="form-control input-sm"></select>
												<span class="input-group-btn">
													<button data-toggle="modal" data-target="#obat_racikan-modal" tabindex="17" class="btn btn-sm btn-info" type="button" id="search_obat_2"><i class="fa fa-search"></i></button>
												</span>
											</div></td>
											<td><label class="datatrans" id="lbltarif2"> - </label></td>
											<td><label class="datatrans" id="lblsatuan2"> - </label></td>

											<td><input type="text" class="form-control input-sm" tabindex="18" id="lbl_qty2"  onkeypress="return hanyaAngka(event)"/></td>
											<td><input type="text" class="form-control input-sm" tabindex="19" id="lbl_disc2"  onkeypress="return hanyaAngka(event)" /></td>
											<td><label class="datatrans" id="lbl_jumlah2"> - </label></td>
											<td>

												<input type="hidden" id="idtipe2" >
												<input type="hidden" id="tstok2" >
												<input type="hidden" id="nomor2" >
												<input type="hidden" id="rowindex2" >
												<input type="hidden" class="form-control" id="lbl_nama_obat2"/>
												<input type="hidden" class="form-control" id="ttarif2"/>
												<input type="hidden" class="form-control" id="tjumlah2"/>
												<button type="button" class="btn btn-sm  btn-primary" tabindex="20" id="addjual2" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
												<button type='button' class='btn btn-sm btn-danger' tabindex="21" id="canceljual2" title="Refresh"><i class='glyphicon glyphicon-remove'></i></button>
										</td>
										</tr>
									</thead>
									<tbody></tbody>
									<tfoot id="foot-total-nonracikan">
										<tr>
											<th colspan="5" class="hidden-phone">

												<span class="pull-right"><b>TOTAL</b></span></th>
												<input type="hidden" id="tempgrandtotal2" name="tempgrandtotal">
											<th colspan="2"><b><span id="lblgrandtotal2">0</span></b></th>
										</tr>
									</tfoot>
								</table>
							</div>
							<div class="modal-footer">
								<button class="btn btn-sm btn-primary" disabled id="btn_ok_racikan" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
								<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade in black-overlay" id="obat_racikan-modal" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg modal-dialog-popout" style="width:60%; margin-top:100px">
					<div class="modal-content">
						<div class="block block-themed block-transparent remove-margin-b">
							<div class="block-header bg-success">
								<ul class="block-options">
									<li>
										<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
									</li>
								</ul>
								<h3 class="block-title">Daftar Obat</h3>
							</div>
							<div class="block-content">

									<table width="100%" id="table-Rujukan2" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th>Kode Obat</th>
												<th>Nama Obat</th>
												<th>Satuan</th>
												<th>Stok</th>
												<th>Catatan</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
							</div>

						</div>
					</div>
				</div>
			</div>
		<!-- END Obat Racikan -->
			<div class="progress progress-mini">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<hr>
			<div class="text-right bg-light lter">
				<button class="btn btn-info" type="submit" id="btnSubmit-Penjualan" name="btnSubmit-Penjualan">Simpan <img style="display: none;" id="loading-button-ajax" src="{ajax}img/ajax-loader.gif"></button>
				<button class="btn btn-default" type="button" id="btnCancel-Penjualan" name="btnCancel-Penjualan">Batal</button>
			</div>
			<br>

<!-- Modal List nonRacikan -->




<!-- Modal Data Pasien -->
<div class="modal in" id="cari-pasien-modal" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:70%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Data Pasien</h3>
				</div>
				<div class="block-content">
					<hr>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snomedrec">No. Medrec</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snomedrec" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snamapasien">Nama Pasien</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snamapasien" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="sjeniskelamin">Jenis Kelamin</label>
									<div class="col-md-8">
										<label class="css-input css-radio css-radio-sm css-radio-default push-10-r">
											<input type="radio" class="sjeniskelamin" name="sjeniskelamin" value="1"><span></span> Laki-laki
										</label>
										<label class="css-input css-radio css-radio-sm css-radio-default">
											<input type="radio" class="sjeniskelamin" name="sjeniskelamin" value="2"><span></span> Perempuan
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="salamat">Alamat</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="salamat" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="stanggallahir">Tanggal Lahir</label>
									<div class="col-md-8">
										<input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="stanggallahir" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snotelepon">Telepon</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snotelepon" value="">
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>

					<table width="100%" class="table table-bordered table-striped" id="datatable-pasien">
						<thead>
							<tr>
								<th>No. Medrec</th>
								<th>Nama</th>
								<th>Jenis Kelamin</th>
								<th>Tanggal Lahir</th>
								<th>Alamat</th>
								<th>Telepon</th>
								<th>Status Kawin</th>
							</tr>
						</thead>
						<tbody id="pasienmodaltabel">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="waktu_modal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">WAKTU Minum Obat</h3>
				</div>
				<div class="block-content">
					<table id="manage-tabel-Waktu" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 20%;">Waktu Minum</th>
								<th style="width: 10%;">Jam Minum</th>								
								<th style="width: 12%;">Actions</th>
							</tr>
							<tr>
								<td>
									<div id="" class="input-group">
									<select name="lbl_" tabindex="16"  style="width: 100%" id="lbl_obat_id2" data-placeholder="Cari Obat" class="form-control input-sm"></select>
									<span class="input-group-btn">
										<button data-toggle="modal" data-target="#obat_racikan-modal" tabindex="17" class="btn btn-sm btn-info" type="button" id="search_obat_2"><i class="fa fa-search"></i></button>
									</span>
									</div>
								</td>
								<td><label class="datatrans" id="jam_id"> - </label></td>
								
								<td>

									<input type="hidden" id="idtipe2" >
									<input type="hidden" id="tstok2" >
									<input type="hidden" id="nomor2" >
									<input type="hidden" id="rowindex2" >
									<input type="hidden" class="form-control" id="lbl_nama_obat2"/>
									<input type="hidden" class="form-control" id="ttarif2"/>
									<input type="hidden" class="form-control" id="tjumlah2"/>
									<button type="button" class="btn btn-sm  btn-primary" tabindex="20" id="addjual2" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
									<button type='button' class='btn btn-sm btn-danger' tabindex="21" id="canceljual2" title="Refresh"><i class='glyphicon glyphicon-remove'></i></button>
							</td>
							</tr>
						</thead>
						<tbody></tbody>
						<tfoot id="foot-total-nonracikan">
							<tr>
								<th colspan="5" class="hidden-phone">

									<span class="pull-right"><b>TOTAL</b></span></th>
									<input type="hidden" id="tempgrandtotal2" name="tempgrandtotal">
								<th colspan="2"><b><span id="lblgrandtotal2">0</span></b></th>
							</tr>
						</tfoot>
					</table>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-primary" disabled id="btn_ok_racikan" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Modal Data Pasien -->
<div id="data-obat-racikan" hidden>
	<?php if ($list_racikan_tabel){
		$idracikan='';
		$no=0;
		foreach($list_racikan_tabel as $row){
			if ($idracikan <> $row->idracikan){
				if ($idracikan <> ''){
					echo '</table>';
				}
				$no=0;
				$namatabel="tabel".$row->idracikan;
				echo '<input type="text" class="form-control  input-sm" name="pos_tabel'.$row->idracikan.'" id="pos_tabel'.$row->idracikan.'">';
				$idracikan=$row->idracikan;
				echo '<table id="'.$namatabel.'">';
			}
			$no=$no+1;
			$tjumlah=($row->harga_detail*$row->kuantitas_detail);
			$tjumlah2=$tjumlah - (($tjumlah*$row->diskon_detail)/100);
			echo '<tr>';
			echo '<td>'.$row->nama.'</td>';
			echo '<td>'.$row->idbarang.'</td>';
			echo '<td>'.$row->harga_detail.'</td>';
			echo '<td align="right">'.number_format($row->harga_detail).'</td>';
			echo '<td>'.$row->singkatan.'</td>';
			echo '<td align="right">'.$row->kuantitas_detail.'</td>';
			echo '<td align="right">'.$row->diskon_detail.'</td>';
			echo '<td>'.$tjumlah2.'</td>';
			echo '<td  align="right">'.number_format($tjumlah2).'</td>';
			echo "<td ><button type='button' class='btn btn-sm btn-info edit2'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus2'><i class='glyphicon glyphicon-remove'></i></button></td>";
			echo '<td>'.$no.'</td>';
			echo '<td>'.$row->idtipe.'</td>';
			echo '</tr>';
		}
		echo '</table>';
	}



	?>
</div>

<input type="hidden" name="idruangan"  id="idruangan" value="{idruangan}">
<input type="hidden" name="tab"  id="tab" value="{tab}">
<input type="hidden" name="st_browse"  id="st_browse" value="0">
<input type="hidden" name="totalbarang"  id="totalbarang" value="{totalbarang}">
<input type="hidden" name="idpegawaiapoteker"  id="idpegawaiapoteker" value="{idpegawaiapoteker}">
<input type="hidden" name="tgl_lahir"  id="tgl_lahir" value="{tgl_lahir}">
<input type="hidden" name="wakturujukan"  id="wakturujukan" value="{wakturujukan}">
<input type="hidden" name="waktupenyerahan"  id="waktupenyerahan" value="{waktupenyerahan}">
<input type="hidden" name="pos_tabel_non_racikan"  id="pos_tabel_non_racikan" >
<input type="hidden" name="pos_tabel_racikan"  id="pos_tabel_racikan" >
<input type="hidden" name="nopenjualan"  id="nopenjualan" value="{nopenjualan}">
<input type="hidden" name="status"  id="status" value="{status}">
<input type="hidden" name="form_edit"  id="nopenjualan" value="{form_edit}">
<input type="hidden" name="st_rujukan_non_rujukan"  id="st_rujukan_non_rujukan" value="{st_rujukan_non_rujukan}">

</form>
</div>
</div>
<script type="text/javascript" src="{ajax}tpasien-penjualan/js-clone.js"></script>
<!-- <script type="text/javascript" src="{ajax}tpasien-penjualan/manage.js"></script> -->
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
function stopRKey(evt) {
  var evt = (evt) ? evt : ((event) ? event : null);
  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
}

document.onkeypress = stopRKey;

jQuery(function(){ BaseTableDatatables.init();
$(document).ready(function() {
		// lbl_expire
		$("#lbl_expire" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '1920:2010',
			dateFormat : 'dd-mm-yy',
			defaultDate: '-'
		});
		if ($("#idkategori").val() != '0'){
				if ($("#idkategori").val() == '1'){//Jika  Pasien
					$("#data_pasien").attr('hidden',false);
					$("#data_non_pasien").attr('hidden',true);

				}else{
					$("#data_pasien").attr('hidden',true);
					$("#data_non_pasien").attr('hidden',false);
				}

			$("#tr_cari_data").attr('hidden',false);
			$idpasien=$("#idpasien").val();
			// $("#dokter_pegawai_id").append('<option value="'+$("#dokter_pegawai_id").val()+'">'+$("#dokter_pegawai_id").val()+'</option>');
			// $("#dokter_pegawai_id").select2('val',$("#dokter_pegawai_id").val());
			var newOption = new Option($("#nama").val(), $idpasien, false, false);
			$('#dokter_pegawai_id').append(newOption).trigger('change');
			$("#idpasien").val($idpasien);
			$("#nama").attr('readonly',true);
			$("#alamat").attr('readonly',true);
			$("#telprumah").attr('readonly',true);
			$("#btn_cari_data").attr('disabled',false);
		}else{
			$("#tr_cari_data").attr('hidden',true);

			$("#btn_cari_data").attr('disabled',true);
			$("#nama").attr('readonly',false);
			$("#alamat").attr('readonly',false);
			$("#telprumah").attr('readonly',false);
		}

	validasi_save();
	// $('.select2').select2();

	loadDataPasien();

	// DataTable
	function loadDataPasien(snomedrec, snamapasien, sjeniskelamin, salamat, stanggallahir, snotelepon){
		var tablePasien = $('#datatable-pasien').DataTable({
			"pageLength": 8,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}tpoliklinik_pendaftaran/getPasien',
				type: "POST",
				dataType: 'json',
				data:{
					snomedrec:snomedrec,
					snamapasien:snamapasien,
					sjeniskelamin:sjeniskelamin,
					salamat:salamat,
					stanggallahir:stanggallahir,
					snotelepon:snotelepon,
				}
			},
			"columnDefs": [{
				"targets": [0],
				"orderable": true
			}]
		});
	}
	var delay = (function(){
		  var timer = 0;
		  return function(callback, ms){
		  clearTimeout (timer);
		  timer = setTimeout(callback, ms);
		 };
		})();

	 // Apply the search
		$("#snomedrec").keyup(function() {
			var snomedrec = $(this).val();
			var snamapasien = $("#snamapasien").val();
			var sjeniskelamin = $("#sjeniskelamin").val();
			var salamat = $("#salamat").val();
			var stanggallahir = $("#stanggallahir").val();
			var snotelepon = $("#snotelepon").val();

			delay(function(){
				$('#datatable-pasien').DataTable().destroy();
				loadDataPasien(snomedrec, snamapasien, sjeniskelamin, salamat, stanggallahir, snotelepon);
		  }, 1000 );
    });
		$("#snamapasien").keyup(function() {
			var snomedrec = $("#snomedrec").val();
			var snamapasien = $(this).val();
			var sjeniskelamin = $("#sjeniskelamin").val();
			var salamat = $("#salamat").val();
			var stanggallahir = $("#stanggallahir").val();
			var snotelepon = $("#snotelepon").val();

			delay(function(){
				$('#datatable-pasien').DataTable().destroy();
				loadDataPasien(snomedrec, snamapasien, sjeniskelamin, salamat, stanggallahir, snotelepon);
		  }, 1000 );
    });
		$(".sjeniskelamin").click(function() {
			var snomedrec = $("#snomedrec").val();
			var snamapasien = $("#snamapasien").val();
			var sjeniskelamin = $(this).val();
			var salamat = $("#salamat").val();
			var stanggallahir = $("#stanggallahir").val();
			var snotelepon = $("#snotelepon").val();

			delay(function(){
				$('#datatable-pasien').DataTable().destroy();
				loadDataPasien(snomedrec, snamapasien, sjeniskelamin, salamat, stanggallahir, snotelepon);
		  }, 1000 );
    });
		$("#salamat").keyup(function() {
			var snomedrec = $("#snomedrec").val();
			var snamapasien = $("#snamapasien").val();
			var sjeniskelamin = $("#sjeniskelamin").val();
			var salamat = $(this).val();
			var stanggallahir = $("#stanggallahir").val();
			var snotelepon = $("#snotelepon").val();

			delay(function(){
				$('#datatable-pasien').DataTable().destroy();
				loadDataPasien(snomedrec, snamapasien, sjeniskelamin, salamat, stanggallahir, snotelepon);
		  }, 1000 );
    });
		$("#stanggallahir").change(function() {
			var snomedrec = $("#snomedrec").val();
			var snamapasien = $("#snamapasien").val();
			var sjeniskelamin = $("#sjeniskelamin").val();
			var salamat = $("#salamat").val();
			var stanggallahir = $(this).val();
			var snotelepon = $("#snotelepon").val();

			delay(function(){
				$('#datatable-pasien').DataTable().destroy();
				loadDataPasien(snomedrec, snamapasien, sjeniskelamin, salamat, stanggallahir, snotelepon);
		  }, 1000 );
    });
		$("#snotelepon").keyup(function() {
			var snomedrec = $("#snomedrec").val();
			var snamapasien = $("#snamapasien").val();
			var sjeniskelamin = $("#sjeniskelamin").val();
			var salamat = $("#salamat").val();
			var stanggallahir = $("#stanggallahir").val();
			var snotelepon = $(this).val();

			delay(function(){
				$('#datatable-pasien').DataTable().destroy();
				loadDataPasien(snomedrec, snamapasien, sjeniskelamin, salamat, stanggallahir, snotelepon);
		  }, 1000 );
    });
	$("#idkategori").change(function(){
		$("#dokter_pegawai_id").select2('val','');
		$('#dokter_pegawai_id').html('');
		if ($("#idkategori").val() != '0'){
				if ($("#idkategori").val() == '1'){//Jika  Pasien
					$("#data_pasien").attr('hidden',false);
					$("#data_non_pasien").attr('hidden',true);

				}else{
					$("#data_pasien").attr('hidden',true);
					$("#data_non_pasien").attr('hidden',false);
				}
			$("#tr_cari_data").attr('hidden',false);
			$("#idpasien").val('');
			$("#no_medrec").val('');
			$("#nama").val('');
			$("#alamat").val('');
			$("#telprumah").val('');
			$("#nama").attr('readonly',true);
			$("#alamat").attr('readonly',true);
			$("#telprumah").attr('readonly',true);
			$("#btn_cari_data").attr('disabled',false);
		}else{
			$("#tr_cari_data").attr('hidden',true);
			$("#idpasien").val('');
			$("#no_medrec").val('');
			$("#nama").val('');
			$("#alamat").val('');
			$("#telprumah").val('');
			$("#btn_cari_data").attr('disabled',true);
			$("#nama").attr('readonly',false);
			$("#alamat").attr('readonly',false);
			$("#telprumah").attr('readonly',false);
		}
		validasi_save();
	});
	$(document).on("click", ".selectPasien", function() {
	      var idpasien = ($(this).data('idpasien'));
	      $.ajax({
	        url:"{site_url}tpoliklinik_pendaftaran/getDataPasien/"+idpasien,
	        method:"POST",
	        dataType:"json",
	        success:function(data){
	          $('#idpasien').val(idpasien);
	          $('#no_medrec').val(data[0].no_medrec);
	          $('#nama').val(data[0].nama);
	          $('#alamat').val(data[0].alamat_jalan);
	          $('#telprumah').val(data[0].telepon);
	          $('#tgl_lahir').val(data[0].tanggal_lahir);
				validasi_save();
	        }
	      });
	      return false;
	  });

	$("#lbl_qty").bind("keyup", function(e) {
		 if (e.keyCode == 13){
			$("#addjual").trigger("click");
		 }
	});

	$("#search_obat_1").click(function() {

		$("#table-Rujukan").dataTable().fnDestroy();
		$('#table-Rujukan').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				// "bDestroy": true
				"ajax": {
				url: '{site_url}tpasien_penjualan/get_obat_rujukan/',
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": true },
					{ "width": "10%", "targets": 1, "orderable": true },
					{ "width": "30%", "targets": 2, "orderable": true },
					{ "width": "15%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 4, "orderable": true },
					{ "width": "20%", "targets": 5, "orderable": true },
				]

			});
	});
	$("#search_obat_2").click(function() {
		// $("#obat_racikan-modal").modal('show');
		// $("#list_obat_racikan-modal").modal('hide');

		$("#table-Rujukan2").dataTable().fnDestroy();
		$('#table-Rujukan2').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				// "bDestroy": true
				"ajax": {
				url: '{site_url}tpasien_penjualan/get_obat_rujukan2/',
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": true },
					{ "width": "10%", "targets": 1, "orderable": true },
					{ "width": "30%", "targets": 2, "orderable": true },
					{ "width": "15%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 4, "orderable": true },
					{ "width": "20%", "targets": 5, "orderable": true },
				]

			});
	});
	$("#btn_ok_racikan").click(function() {
		var content;
		var index;
		index=$("#no_urut").val();
		var var_input;//Untuk Agar bisa di POST
		var_input='<input type="text" class="form-control  input-sm" name="pos_tabel'+index+'" id="pos_tabel'+index+'"/>';
		content ='<table id="tabel'+index+'">';
		var rows=0;

		$('#manage-tabel-Racikan tbody tr').each(function() {
			content +='<tr>';
			content += "<td>" + $(this).find('td:eq(0)').text() + "</td>";//0 Nama Obat
			content += "<td style='display:none'>" + $(this).find('td:eq(1)').text() + "</td>"; //1 Kode barang ID Hidden
			content += "<td style='display:none'>" + $(this).find('td:eq(2)').text(); + "</td>";//2 tarif Hidden
			content += "<td>" + $(this).find('td:eq(3)').text(); + "</td>";//3
			content += "<td>" + $(this).find('td:eq(4)').text(); + "</td>";//4
			content += "<td align='right'>" + $(this).find('td:eq(5)').text(); + "</td>";//5
			content += "<td align='right'>" + $(this).find('td:eq(6)').text(); + "</td>";//6 Diskon
			content += "<td style='display:none'>" + $(this).find('td:eq(7)').text(); + "</td>";//7 Jumlah
			content += "<td>" + $(this).find('td:eq(8)').text(); + "</td>";//8 Jumlah
			content += "<td ><button type='button' class='btn btn-sm btn-info edit2'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus2'><i class='glyphicon glyphicon-remove'></i></button></td>";
			content += "<td style='display:none'>" + $(this).find('td:eq(10)').text() + "</td>";//10 nomor
			content += "<td>" + $(this).find('td:eq(11)').text() + "</td>";//11 nomor
			content +='</tr>';
			rows+=1;
		});
		content +='</table>';
		$('#pos_tabel'+index).remove();
		$('#tabel'+index).remove();
		$('#data-obat-racikan').append(content);
		// alert(var_input);
		$('#data-obat-racikan').append(var_input);
		// $("#lbl_rows_obat").text('List Obat ('+$("#tempgrandtotal2").val()+')');
		$("#lbl_rows_obat").text('List Obat ('+rows+')');
		$("#rac_harga").val($("#tempgrandtotal2").val());
		$("#rac_jml_racikan").val(rows);

		$("#rac_harga_label").text(formatNumber($("#tempgrandtotal2").val()));
		gen_jumlah_rac();
	});

});
	function formatNumber (num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}
	function gen_jumlah(){
		var total;
		if ($("#lbl_qty").val() != ''){
			total=parseFloat($("#ttarif").val())*parseFloat($("#lbl_qty").val());
		}else{
			total=0;
		}

		if ($("#lbl_disc").val() != ''){
			total=parseFloat(total) - parseFloat((total * $("#lbl_disc").val() /100));
		}
		if ($("#lbl_tuslah").val() != ''){
			total=parseFloat(total) + parseFloat(($("#lbl_tuslah").val()));
		}
		$("#tjumlah").val(total);
		$("#lbl_jumlah").text(formatNumber($("#tjumlah").val()));
	}
	function gen_jumlah2(){
		var total;
		if ($("#lbl_qty2").val() != ''){
			total=parseFloat($("#ttarif2").val())*parseFloat($("#lbl_qty2").val());
		}else{
			total=0;
		}

		if ($("#lbl_disc2").val() != ''){
			total=parseFloat(total) - parseFloat((total * $("#lbl_disc2").val() /100));
		}

		$("#tjumlah2").val(total);
		$("#lbl_jumlah2").text(formatNumber($("#tjumlah2").val()));
	}
	function gen_jumlah_rac(){
		var total;
		if ($("#rac_tuslah").val() != ''){

		}else{
			$("#rac_tuslah").val(0);
		}
		total=parseFloat($("#rac_harga").val())+parseFloat($("#rac_tuslah").val());


		$("#rac_jumlah").val(total);
		$("#rac_jumlah_label").text(formatNumber($("#rac_jumlah").val()));
	}
	function clear_detail(){
		$("#rowindex").val('');
		$("#nomor").val('');
		$("#lbl_obat_id").val("");
		$("#lbl_obat_id").select2('val','');
		$('#lbl_obat_id').html('');

		$("#pemakaian").val("SBM");
		$("#ttarif").val("0");
		$("#lbltarif").text("");
		$("#lblsatuan").text("");
		$("#lbl_cara_pakai").val("");
		$("#lbl_qty").val("0");
		$("#lbl_disc").val("0");
		$("#tstatusverifikasi").val("0");
		$("#lbl_tuslah").val("0");
		$("#lbl_jumlah").text("");
		$("#tjumlah").val("");
		$("#search_obat_1").attr('disabled',false);

	}
	function clear_detail_racikan(){
		$("#rac_aturan").val('SBM');
		$("#rowindex_rac").val('');
		$("#rac_cara_pakai").val('');
		$("#nomor_rac").val('');
		$("#rac_nama").val("");
		$("#rac_jenis").val("0");
		$("#rac_harga").val("0");
		$("#statusverifikasi_rac").val("0");
		$("#rac_harga_label").text("0");
		$("#rac_qty").val("0");
		$("#rac_jumlah_label").text("0");
		$("#rac_jumlah").val("0");
		$("#st_edit").val("0");
		$("#rac_tuslah").val("0");
		$("#lbl_rows_obat").text('List Obat (0)');

	}
	function clear_detail2(){
		$("#rowindex2").val('');
		$("#nomor2").val('');
		$("#lbl_obat_id2").val("");
		$("#lbl_obat_id2").select2('val','');
		$('#lbl_obat_id2').html('');

		$("#ttarif2").val("0");
		$("#lbltarif2").text("");
		$("#lblsatuan2").text("");
		$("#lbl_qty2").val("0");
		$("#lbl_disc2").val("0");
		$("#lbl_jumlah2").text("");
		$("#tjumlah2").val("");
		$("#search_obat_2").attr('disabled',false);
		var total_rows = 0;
		$('#manage-tabel-Racikan tbody tr').each(function() {
			total_rows +=1;
		});
		if (total_rows>0){
			$("#btn_ok_racikan").attr('disabled',false);
		}else{
			$("#btn_ok_racikan").attr('disabled',true);
		}
	}
	function grand_total()
	{
		var total_grand = 0;
		$('#manage-tabel-nonRacikan tbody tr').each(function() {
			total_grand +=parseFloat($(this).find('td:eq(12)').text());
		});


		$("#tempgrandtotal").val(total_grand);
		$("#lblgrandtotal").text(formatNumber(total_grand));
		$("#gt_all").val(parseFloat($("#tempgrandtotal").val())+parseFloat($("#total_racikan").val()));
		$("#gt_all_label").text(formatNumber($("#gt_all").val()));
		validasi_save();

	}
	function validasi_save(){
		if (parseFloat($("#gt_all").val()) < 1){
			$("#btnSubmit-Penjualan").attr('disabled',true);
		}else{
			if ($("#id_penjualan").val() =='' || $("#nama").val()==''){
				$("#btnSubmit-Penjualan").attr('disabled',true);
			}else{
				$("#btnSubmit-Penjualan").attr('disabled',false);
			}
		}
	}
	function grand_total2()
	{
		var total_grand = 0;
		$('#manage-tabel-Racikan tbody tr').each(function() {
			total_grand +=parseFloat($(this).find('td:eq(7)').text());
		});


		$("#tempgrandtotal2").val(total_grand);
		$("#lblgrandtotal2").text(formatNumber(total_grand));
	}
	function grand_total_rac()
	{
		var total_grand = 0;
		$('#table_racikan_detail tbody tr').each(function() {
			total_grand +=parseFloat($(this).find('td:eq(11)').text());
			console.log($(this).find('td:eq(11)').text());
		});

		console.log(parseFloat($("#tempgrandtotal").val()));
		$("#total_racikan").val(total_grand);
		$("#total_racikan_label").text(formatNumber(total_grand));
		$("#gt_all").val(parseFloat($("#tempgrandtotal").val())+parseFloat($("#total_racikan").val()));
		$("#gt_all_label").text(formatNumber($("#gt_all").val()));

		validasi_save();
	}




	$("#addjual").click(function() {
			if(!validate_detail()) return false;
			var duplicate = false;
			var content = "";

			if($("#nomor").val() != ''){
					var no = $("#nomor").val();
			}else{
				var no = $('#manage-tabel-nonRacikan tr').length - 2;
				var content = "<tr>";
				$('#manage-tabel-nonRacikan tbody tr').filter(function (){
					var $cells = $(this).children('td');
					if($cells.eq(1).text() === $("#lbl_obat_id").val() && $cells.eq(16).text() === $("#idtipe").val()){
						// big_notification("Barang sudah ada didalam daftar","error");
						duplicate = true;
						sweetAlert("Duplicate...", "Data Obat Duplicate!", "error");
					}
				});
			}
			if(duplicate) return false;
			content += "<td>" + $("#lbl_nama_obat").val() + "</td>";//0 Nama Obat
			content += "<td style='display:none'>" + $("#lbl_obat_id").val() + "</td>"; //1 Kode barang ID Hidden
			content += "<td style='display:none'>" + $("#ttarif").val(); + "</td>";//2 tarif Hidden
			content += "<td>" + $("#lbltarif").text(); + "</td>";//3
			content += "<td>" + $("#lblsatuan").text(); + "</td>";//4
			content += "<td>" + $("#lbl_cara_pakai").val(); + "</td>";//5
			content += "<td>" + $("#pemakaian").val(); + "</td>";//6
			content += "<td align='right'>" + $("#lbl_qty").val(); + "</td>";//7
			content += "<td align='right'>" + $("#lbl_disc").val(); + "</td>";//8 Diskon
			content += "<td align='right'>" + $("#lbl_tuslah").val(); + "</td>";//9 Tuslah
			if ($("#lbl_expire").val()==''){
				content += "<td align='center'>-</td>";//10 Tuslah asal 9				
			}else{
				content += "<td align='center'>" + $("#lbl_expire").val(); + "</td>";//10 Tuslah asal 9				
				
			}
			content += "<td align='right'>" + $("#lbl_jumlah").text(); + "</td>";//11 Jumlah asa 10
			content += "<td style='display:none'>" + $("#tjumlah").val(); + "</td>";//12 Jumlah asal 11
			content += "<td ><button type='button' class='btn btn-sm btn-info edit'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus'><i class='glyphicon glyphicon-remove'></i></button></td>";
			content += "<td style='display:none'>" + no + "</td>";//14 nomor
			content += "<td style='display:none'>" + $("#tstatusverifikasi").val() + "</td>";//15 nomor
			content += "<td style='display:none'>" + $("#idtipe").val() + "</td>";//16 nomor
			if($("#rowindex").val() != ''){
				$('#manage-tabel-nonRacikan tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
			}else{
				content += "</tr>";
				$('#manage-tabel-nonRacikan tbody').append(content);
			}

			$(".edit").attr('disabled',false);
			$(".hapus").attr('disabled',false);

			clear_detail();
			grand_total();
			$('#lbl_obat_id').select2('enable');
			$('#lbl_obat_id').select2('focus');

			// $("#lbl_obat_id").select2('enable',true);

			// console.log(no);

	});
	$("#addjual2").click(function() {
			if(!validate_detail2()) return false;
			var duplicate = false;
			var content = "";

			if($("#nomor2").val() != ''){
					var no = $("#nomor2").val();
			}else{
				var no = $('#manage-tabel-Racikan tr').length - 2;
				var content = "<tr>";
				$('#manage-tabel-Racikan tbody tr').filter(function (){
					var $cells = $(this).children('td');
					if($cells.eq(1).text() === $("#lbl_obat_id2").val() && $cells.eq(11).text() === $("#idtipe2").val()){
						// big_notification("Barang sudah ada didalam daftar","error");
						duplicate = true;
						sweetAlert("Duplicate...", "Data Obat Duplicate!", "error");
					}
				});
			}
			if(duplicate) return false;
			content += "<td>" + $("#lbl_nama_obat2").val() + "</td>";//0 Nama Obat
			content += "<td style='display:none'>" + $("#lbl_obat_id2").val() + "</td>"; //1 Kode barang ID Hidden
			content += "<td style='display:none'>" + $("#ttarif2").val(); + "</td>";//2 tarif Hidden
			content += "<td>" + $("#lbltarif2").text(); + "</td>";//3
			content += "<td>" + $("#lblsatuan2").text(); + "</td>";//4
			content += "<td align='right'>" + $("#lbl_qty2").val(); + "</td>";//5
			content += "<td align='right'>" + $("#lbl_disc2").val(); + "</td>";//6 Diskon
			content += "<td style='display:none'>" + $("#tjumlah2").val(); + "</td>";//7 Jumlah
			content += "<td>" + $("#lbl_jumlah2").text(); + "</td>";//8 Jumlah
			content += "<td ><button type='button' class='btn btn-sm btn-info edit2'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus2'><i class='glyphicon glyphicon-remove'></i></button></td>";
			content += "<td style='display:none'>" + no + "</td>";//10 nomor
			content += "<td style='display:none'>" + $("#idtipe2").val() + "</td>";//11 nomor
			if($("#rowindex2").val() != ''){
				$('#manage-tabel-Racikan tbody tr:eq(' + $("#rowindex2").val() + ')').html(content);
			}else{
				content += "</tr>";
				$('#manage-tabel-Racikan tbody').append(content);
			}

			$(".edit2").attr('disabled',false);
			$(".hapus2").attr('disabled',false);

			clear_detail2();
			grand_total2();
			$('#lbl_obat_id2').select2('enable');
			$('#lbl_obat_id2').select2('focus');

			// $("#lbl_obat_id").select2('enable',true);

			// console.log(no);

	});

	$("#rac_addjual").click(function() {
			if(!validate_detail_racikan()) return false;
			var duplicate = false;
			var content = "";

			if($("#nomor_rac").val() != ''){
					var no = $("#nomor_rac").val();
			}else{
				var no = $('#table_racikan_detail tr').length - 2;
				var content = "<tr>";
				$('#manage-tabel-Racikan tbody tr').filter(function (){
					var $cells = $(this).children('td');
					if($cells.eq(0).text() === $("#no_urut").val()){
						// big_notification("Barang sudah ada didalam daftar","error");
						duplicate = true;
						sweetAlert("Duplicate...", "Data Obat Duplicate!", "error");
					}
				});
			}

			if(duplicate) return false;
			var nama='<div class="input-group"><input class="form-control input-sm" type="text" readonly id="label_racikan" value="'+$("#rac_nama").val()+'" /><span class="input-group-btn"><button class="btn btn-success input-sm btn_detail_racikan" data-toggle="modal"  type="button"><span id="lbl_rows_obat">'+$("#lbl_rows_obat").text()+'</span></button></span></div>';
			content += "<td style='display:none'>" + $("#no_urut").val() + "</td>";//0 No Urut
			content += "<td style='display:none'>" + $("#rac_nama").val() + "</td>";//1 Nama Obat
			content += "<td>" + nama + "</td>";//2 Nama Obat
			content += "<td>" + $('#rac_jenis option:selected').text() + "</td>";//3
			content += "<td>" + $("#rac_cara_pakai").val() + "</td>";//4
			content += "<td>" + $("#rac_aturan").val() + "</td>";//5
			content += "<td align='right'>" + $("#rac_qty").val() + "</td>";//6
			content += "<td align='right'>" + $("#rac_tuslah").val() + "</td>";//7
			content += "<td align='center'>" + $("#rac_expire").val() + "</td>";//8
			content += "<td align='right'>" + $("#rac_harga_label").text() + "</td>";//9
			content += "<td align='right'>" + $("#rac_jumlah_label").text() + "</td>";//10 rac_jumlah
			content += "<td style='display:none'>" + $("#rac_jumlah").val() + "</td>";//11
			content += "<td ><button type='button' class='btn btn-sm btn-info rac_edit'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger rac_hapus'><i class='glyphicon glyphicon-remove'></i></button></td>";
			content += "<td style='display:none'>" + no + "</td>";//13 nomor
			content += "<td style='display:none'>tabel" + $("#no_urut").val() + "</td>";//14 nomor
			content += "<td style='display:none'>" + $("#rac_harga").val() + "</td>";//15 nomor
			content += "<td style='display:none'>" + $("#rac_jml_racikan").val() + "</td>";//16 Jumlah Racikan
			content += "<td style='display:none'>" + $("#rac_jenis").val() + "</td>";//17  Racikan Jenis
			content += "<td style='display:none'>" + $("#statusverifikasi_rac").val() + "</td>";//18  Status Verifikasi
			if($("#rowindex_rac").val() != ''){
				$('#table_racikan_detail tbody tr:eq(' + $("#rowindex_rac").val() + ')').html(content);
			}else{
				content += "</tr>";
				$('#table_racikan_detail tbody').append(content);
			}

			$(".rac_edit").attr('disabled',false);
			$(".rac_hapus").attr('disabled',false);
			// $("#no_urut").val(parseFloat($("#no_urut").val())+1);
			$("#no_urut_akhir").val(parseFloat($("#no_urut_akhir").val())+1);
			clear_detail_racikan();
			$('#manage-tabel-Racikan tbody tr').remove();
			grand_total_rac();

			// $("#lbl_obat_id").select2('enable',true);

			// console.log(no);

	});
	$("#canceljual").click(function() {
		clear_detail();
		$('#lbl_obat_id').select2('enable');
		$(".edit").attr('disabled',false);
		$(".hapus").attr('disabled',false);
	});
	$("#rac_canceljual").click(function() {
		clear_detail_racikan();
		$(".rac_edit").attr('disabled',false);
		$(".rac_hapus").attr('disabled',false);
	});
	$("#canceljual2").click(function() {

		clear_detail2();
		$('#lbl_obat_id2').select2('enable');
		$(".edit2").attr('disabled',false);
		$(".hapus2").attr('disabled',false);
	});
	$("#btn_list_obat_racikan").click(function() {
		$("#nama_obat_racikan").text($("#rac_nama").val());
		if ($("#lbl_rows_obat").text()=='List Obat (0)'){
			$("#tempgrandtotal2").val(0);
			$("#lblgrandtotal2").text(0);
			$('#manage-tabel-Racikan tbody tr').remove();
		}
		if ($("#st_edit").val()=='1'){//Jika Edit

			$('#manage-tabel-Racikan tbody tr').remove();
			$no_urut = $("#no_urut").val();
			var idtabel='tabel'+$no_urut;
			$("#list_obat_racikan-modal").modal('show');
			var content;

			content ='';
			var rows=0;
			var total=0
			$('#'+idtabel+' tbody tr').each(function() {
				content +='<tr>';
				content += "<td>" + $(this).find('td:eq(0)').text() + "</td>";//0 Nama Obat
				content += "<td style='display:none'>" + $(this).find('td:eq(1)').text() + "</td>"; //1 Kode barang ID Hidden
				content += "<td style='display:none'>" + $(this).find('td:eq(2)').text(); + "</td>";//2 tarif Hidden
				content += "<td>" + $(this).find('td:eq(3)').text(); + "</td>";//3
				content += "<td>" + $(this).find('td:eq(4)').text(); + "</td>";//4
				content += "<td align='right'>" + $(this).find('td:eq(5)').text(); + "</td>";//5
				content += "<td align='right'>" + $(this).find('td:eq(6)').text(); + "</td>";//6 Diskon
				content += "<td style='display:none'>" + $(this).find('td:eq(7)').text(); + "</td>";//7 Jumlah
				content += "<td>" + $(this).find('td:eq(8)').text(); + "</td>";//8 Jumlah
				content += "<td ><button type='button' class='btn btn-sm btn-info edit2'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus2'><i class='glyphicon glyphicon-remove'></i></button></td>";
				content += "<td style='display:none'>" + $(this).find('td:eq(10)').text() + "</td>";//10 nomor
				content += "<td style='display:none'>" + $(this).find('td:eq(11)').text() + "</td>";//11 idtipe
				content +='</tr>';
				total +=parseFloat($(this).find('td:eq(7)').text());
				rows+=1;
			});
			$("#tempgrandtotal2").val(total);
			$("#lblgrandtotal2").text(formatNumber(total));
			$('#manage-tabel-Racikan tbody').append(content);
		}else{
			$("#no_urut").val($("#no_urut_akhir").val());
		}

		$("#list_obat_racikan-modal").modal('show');
		$("#btn_ok_racikan").attr('disabled',true);
		$("#lbl_obat_id2").attr('disabled',false);
		$("#search_obat_2").attr('disabled',false);
		$("#addjual2").attr('disabled',false);
		$("#canceljual2").attr('disabled',false);


	});
	$(document).on("click",".btn_detail_racikan",function(){
		$("#nama_obat_racikan").text($(this).closest('tr').find("td:eq(1)").html());
		$('#manage-tabel-Racikan tbody tr').remove();
		$no_urut = $(this).closest('tr').find("td:eq(0)").html();
		var idtabel=$(this).closest('tr').find("td:eq(14)").html();
		$("#list_obat_racikan-modal").modal('show');

		var content;

		content ='';
		var rows=0;
		var total=0
		$('#'+idtabel+' tbody tr').each(function() {
			content +='<tr>';
			content += "<td>" + $(this).find('td:eq(0)').text() + "</td>";//0 Nama Obat
			content += "<td style='display:none'>" + $(this).find('td:eq(1)').text() + "</td>"; //1 Kode barang ID Hidden
			content += "<td style='display:none'>" + $(this).find('td:eq(2)').text(); + "</td>";//2 tarif Hidden
			content += "<td>" + $(this).find('td:eq(3)').text(); + "</td>";//3
			content += "<td>" + $(this).find('td:eq(4)').text(); + "</td>";//4
			content += "<td align='right'>" + $(this).find('td:eq(5)').text(); + "</td>";//5
			content += "<td align='right'>" + $(this).find('td:eq(6)').text(); + "</td>";//6 Diskon
			content += "<td style='display:none'>" + $(this).find('td:eq(7)').text(); + "</td>";//7 Jumlah
			content += "<td>" + $(this).find('td:eq(8)').text(); + "</td>";//8 Jumlah
			content += "<td ><button type='button' class='btn btn-sm btn-info edit2'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus2'><i class='glyphicon glyphicon-remove'></i></button></td>";
			content += "<td style='display:none'>" + $(this).find('td:eq(10)').text() + "</td>";//10 nomor
			content += "<td style='display:none'>" + $(this).find('td:eq(11)').text() + "</td>";//11 idtipe
			content +='</tr>';
			total +=parseFloat($(this).find('td:eq(7)').text());
			rows+=1;
		});
		$("#tempgrandtotal2").val(total);
		$("#lblgrandtotal2").text(formatNumber(total));
		$('#manage-tabel-Racikan tbody').append(content);
		disable_edit();
	});
	function disable_edit(){
		clear_detail2();
		$(".edit2").attr('disabled',true);
		$(".hapus2").attr('disabled',true);
		$("#btn_ok_racikan").attr('disabled',true);
		$("#lbl_obat_id2").attr('disabled',true);
		$("#search_obat_2").attr('disabled',true);
		$("#addjual2").attr('disabled',true);
		$("#canceljual2").attr('disabled',true);

	}
	$(document).on("click",".edit",function(){
		$(".edit").attr('disabled',true);
		$(".hapus").attr('disabled',true);
		$("#search_obat_1").attr('disabled',true);

		$kodebarang = $(this).closest('tr').find("td:eq(1)").html();
		$namabarang = $(this).closest('tr').find("td:eq(0)").html();
		$("#lbl_obat_id").append('<option value="'+$kodebarang+'">'+$namabarang+'</option>');
		// $("#lbl_obat_id").select2('val',$(this).closest('tr').find("td:eq(1)").html());
		$('#lbl_obat_id').val($kodebarang).trigger('change');
		$("#nomor").val($(this).closest('tr').find("td:eq(12)").html());
		$("#lbltarif").text($(this).closest('tr').find("td:eq(3)").html());
		$("#ttarif").val($(this).closest('tr').find("td:eq(2)").html());
		$("#lblsatuan").text($(this).closest('tr').find("td:eq(4)").html());
		$("#lbl_cara_pakai").val($(this).closest('tr').find("td:eq(5)").html());
		$('#pemakaian').val($(this).closest('tr').find("td:eq(6)").html());

		$("#lbl_qty").val($(this).closest('tr').find("td:eq(7)").html());
		$("#lbl_disc").val($(this).closest('tr').find("td:eq(8)").html());
		$("#lbl_tuslah").val($(this).closest('tr').find("td:eq(9)").html());
		$("#lbl_expire").val($(this).closest('tr').find("td:eq(10)").html());
		$("#lbl_jumlah").text($(this).closest('tr').find("td:eq(11)").html());
		$("#tjumlah").val($(this).closest('tr').find("td:eq(12)").html());
		$("#tstatusverifikasi").val($(this).closest('tr').find("td:eq(15)").html());
		$("#idtipe").val($(this).closest('tr').find("td:eq(16)").html());
		$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
		// $("#lbl_obat_id").select2('enable',false);
		// console.log($("#rowindex").val());
		$("#lbl_obat_id").select2('enable',false);
		$("#lbl_qty").focus();
	});
	$(document).on("click",".edit2",function(){
		$(".edit2").attr('disabled',true);
		$(".hapus2").attr('disabled',true);
		$("#addjual2").attr('disabled',false);
		$("#canceljual2").attr('disabled',false);
		$("#search_obat_2").attr('disabled',true);
		$("#st_browse").val('1');
		$kodebarang = $(this).closest('tr').find("td:eq(1)").html();
		$namabarang = $(this).closest('tr').find("td:eq(0)").html();
		$("#lbl_obat_id2").append('<option value="'+$kodebarang+'">'+$namabarang+'</option>');
		// $("#lbl_obat_id2").select2('val',$(this).closest('tr').find("td:eq(1)").html());
		$('#lbl_obat_id2').val($kodebarang).trigger('change');
		// var newOption = new Option($namabarang, $kodebarang,true, true);
		// $('#lbl_obat_id').append(newOption).trigger('change');

		$("#nomor2").val($(this).closest('tr').find("td:eq(10)").html());
		$("#lbltarif2").text($(this).closest('tr').find("td:eq(3)").html());
		$("#ttarif2").val($(this).closest('tr').find("td:eq(2)").html());
		$("#lblsatuan2").text($(this).closest('tr').find("td:eq(4)").html());
		$("#lbl_qty2").val($(this).closest('tr').find("td:eq(5)").html());
		$("#lbl_disc2").val($(this).closest('tr').find("td:eq(6)").html());
		$("#lbl_jumlah2").text($(this).closest('tr').find("td:eq(8)").html());
		$("#tjumlah2").val($(this).closest('tr').find("td:eq(7)").html());
		$("#idtipe2").val($(this).closest('tr').find("td:eq(11)").html());
		$("#rowindex2").val($(this).closest('tr')[0].sectionRowIndex);
		// $("#lbl_obat_id").select2('enable',false);
		// console.log($("#rowindex").val());
		$("#lbl_obat_id2").select2('enable',false);
		$("#lbl_qty2").focus();
	});
	$(document).on("click",".rac_edit",function(){
		$(".rac_edit").attr('disabled',true);
		$(".rac_hapus").attr('disabled',true);
		$("#lbl_rows_obat").text('List Obat ('+$(this).closest('tr').find("td:eq(16)").html()+')');
		var nama='<div class="input-group"><input class="form-control input-sm" type="text" readonly id="label_racikan" value="'+$("#rac_nama").val()+'" /><span class="input-group-btn"><button class="btn btn-success input-sm btn_detail_racikan" data-toggle="modal"  type="button"><span id="lbl_rows_obat">'+$("#lbl_rows_obat").text()+'</span></button></span></div>';
		$("#no_urut").val($(this).closest('tr').find("td:eq(0)").html());
		$("#rac_nama").val($(this).closest('tr').find("td:eq(1)").html());
		$("#rac_jenis").val($(this).closest('tr').find("td:eq(17)").html());
		$("#rac_cara_pakai").val($(this).closest('tr').find("td:eq(4)").html());
		$("#rac_aturan").val($(this).closest('tr').find("td:eq(5)").html());
		$("#rac_qty").val($(this).closest('tr').find("td:eq(6)").html());
		$("#rac_tuslah").val($(this).closest('tr').find("td:eq(7)").html());
		$("#rac_expire").val($(this).closest('tr').find("td:eq(8)").html());
		$("#rac_harga_label").text($(this).closest('tr').find("td:eq(9)").html());
		$("#rac_jumlah_label").text($(this).closest('tr').find("td:eq(10)").html());
		// $("#rac_jumlah").val($(this).closest('tr').find("td:eq(11)").html());
		$("#rac_jumlah").val($(this).closest('tr').find("td:eq(11)").html());
		$("#nomor_rac").val($(this).closest('tr').find("td:eq(13)").html());
		// $("#no_urut").val($(this).closest('tr').find("td:eq(13)").html());
		$("#rac_harga").val($(this).closest('tr').find("td:eq(15)").html());
		$("#lbl_rows_obat").text('List Obat ('+$(this).closest('tr').find("td:eq(16)").html()+')');
		$("#rac_jml_racikan").val($(this).closest('tr').find("td:eq(16)").html());
		$("#statusverifikasi_rac").val($(this).closest('tr').find("td:eq(18)").html());
		$("#st_edit").val(1);
		$("#rowindex_rac").val($(this).closest('tr')[0].sectionRowIndex);
		// alert($(this).closest('tr').find("td:eq(10)").html());
		$("#rac_qty").focus();
	});
	$(document).on("click",".hapus",function(){
		if(confirm("Hapus Data ?") == true){

			$(this).closest('td').parent().remove();
			$("#lbl_obat_id").focus();
		}
		grand_total();
		clear_detail();
	});
	$(document).on("click",".hapus2",function(){
		if(confirm("Hapus Data ?") == true){

			$(this).closest('td').parent().remove();
			$("#lbl_obat_id2").focus();
		}
		grand_total2();
		clear_detail2();
	});
	$(document).on("click",".rac_hapus",function(){

		if(confirm("Hapus Data ?") == true){

			$(this).closest('td').parent().remove();

			var index=$(this).closest('tr').find("td:eq(0)").html();
			var idtabel=$(this).closest('tr').find("td:eq(13)").html();
			$('#'+idtabel).remove();
			$('#pos_tabel'+index).remove();
			$("#rac_nama").focus();
		}
		// grand_total2();
		// clear_detail2();
	});

	function validate_detail()
	{
		if($("#lbl_obat_id").val() == ""){
			sweetAlert("Maaf...", "Data Obat Harus Diisi!", "error");
			$("#lbl_obat_id").focus();
			return false;
		}
		if($("#lbl_obat_id").val() == null){
			sweetAlert("Maaf...", "Data Obat Harus Diisi!", "error");
			$("#lbl_obat_id").focus();
			return false;
		}
		if($("#lbl_cara_pakai").val() == ""){
			sweetAlert("Maaf...", "Cara Pakai Obat Harus Diisi!", "error");
			$("#lbl_cara_pakai").focus();
			return false;
		}
		if($("#lbl_qty").val() == ""){
			sweetAlert("Maaf...", "QTY Harus Diisi!", "error");
			$("#lbl_qty").focus();
			return false;
		}

		if($("#lbl_qty").val() < 1){
			sweetAlert("Maaf...", "QTY Harus Diisi!", "error");
			$("#lbl_qty").focus();
			return false;
		}

		return true;
	}
	function validate_detail_racikan()
	{
		if($("#rac_nama").val() == ""){
			sweetAlert("Maaf...", "Nama Racikan Harus Diisi!", "error");
			$("#rac_nama").focus();
			return false;
		}
		if ($("#lbl_rows_obat").text()=='List Obat (0)'){
			sweetAlert("Maaf...", "Data Racikan Harus Diisi!", "error");
			return false;
		}
		if($("#rac_cara_pakai").val() == ""){
			$("#rac_cara_pakai").focus();
			sweetAlert("Maaf...", "Cara Pakai Obat Harus Diisi!", "error");
			return false;
		}
		if($("#rac_qty").val() == ""){
			sweetAlert("Maaf...", "QTY Harus Diisi!", "error");
			$("#rac_qty").focus();
			return false;
		}

		if($("#rac_qty").val() < 1){
			sweetAlert("Maaf...", "QTY Harus Diisi!", "error");
			$("#rac_qty").focus();
			return false;
		}

		return true;
	}
	function validate_detail2()
	{
		if($("#lbl_obat_id2").val() == ""){
			sweetAlert("Maaf...", "Data Obat Harus Diisi!", "error");
			$("#lbl_obat_id2").focus();
			return false;
		}
		if($("#lbl_obat_id2").val() == null){
			sweetAlert("Maaf...", "Data Obat Harus Diisi!", "error");
			$("#lbl_obat_id2").focus();
			return false;
		}

		if($("#lbl_qty2").val() == ""){
			sweetAlert("Maaf...", "QTY Harus Diisi!", "error");
			$("#lbl_qty2").focus();
			return false;
		}

		if($("#lbl_qty2").val() < 1){
			sweetAlert("Maaf...", "QTY Harus Diisi!", "error");
			$("#lbl_qty2").focus();
			return false;
		}

		return true;
	}
	$("#lbl_obat_id").select2({
        minimumInputLength: 2,
		noResults: 'Obat Tidak Ditemukan.',
		// allowClear: true
        // tags: [],
        ajax: {
            url: '{site_url}tpasien_penjualan/get_obat/',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,

		 data: function (params) {
              var query = {
                search: params.term,
              }
              return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nama,
                            id: item.id,
							idtipe : item.idtipe
                        }
                    })
                };
            }
        }
    });
	$("#lbl_obat_id").change(function(){
		data=$("#lbl_obat_id").select2('data')[0];
		if ($("#st_browse").val()==0){
			$("#idtipe").val(data.idtipe);
		}
		$("#st_browse").val("0");
		$("#lbl_nama_obat").val($("#lbl_obat_id option:selected").text());
		$("#lbl_qty").val("0");
		$("#lbl_disc").val("0");
		$("#lbl_tuslah").val("0");

		// alert($("#idtipe").val());
		if ($(this).val()!=''){
			$.ajax({
			  url: '{site_url}tpasien_penjualan/get_obat_detail/'+$(this).val()+'/'+$("#idkelompokpasien").val()+'/'+$("#idtipe").val(),
			  dataType: "json",
			  success: function(data) {
				 if (data==null){
					sweetAlert("Maaf...", "Data Obat Tidak ditemukan!", "error");
					return false;
				 }
				$("#ttarif").val(parseFloat(data.margin*data.hargadasar/100)+parseFloat(data.hargadasar));
				$("#lbltarif").text(formatNumber($("#ttarif").val()));
				$("#lblsatuan").text(data.singkatan);
				$("#tstok").val(data.stok);
				$("#idtipe").val(data.idtipe);
				$("#lbl_cara_pakai").focus();

			  }
			});
		}else{

		}
	});
	$(document).on("click",".selectObat",function(){
		clear_detail();
		var idobat = ($(this).data('idobat'));
		var idtipe = ($(this).data('idtipe'));
		// alert(idtipe);
		$("#st_browse").val("1");
		$("#lbl_qty").val("0");
		$("#lbl_disc").val("0");
		$("#lbl_tuslah").val("0");
		$("#idtipe").val(idtipe);
		$.ajax({
		  url: '{site_url}tpasien_penjualan/get_obat_detail/'+idobat+'/'+$("#idkelompokpasien").val()+'/'+idtipe,
		  dataType: "json",
		  success: function(data) {
			$("#ttarif").val(parseFloat(data.margin*data.hargadasar/100)+parseFloat(data.hargadasar));
			$("#lbltarif").text(formatNumber($("#ttarif").val()));
			$("#lblsatuan").text(data.singkatan);
			$("#tstok").val(data.stok);
			$("#lbl_nama_obat").val(data.singkatan);
			$("#idtipe").val(data.idtipe);

			var isi = {
				id: idobat,
				text: data.nama,
				idtipe: data.idtipe
				};
			// alert(isi.idtipe);
				var newOption = new Option(isi.text, isi.id,true, true);
				$('#lbl_obat_id').append(newOption).trigger('change');
				// $('#lbl_obat_id').append($('<option>', {
					// value: idobat,
					// text : data.nama,
					// idtipe: data.idtipe
				// })).trigger('change');
			$("#lbl_cara_pakai").focus();
		  }

		});

	});
	$("#lbl_obat_id2").select2({
        minimumInputLength: 2,
		// allowClear: true
        // tags: [],
        ajax: {
            url: '{site_url}tpasien_penjualan/get_obat/',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
            data: function (params) {
              var query = {
                search: params.term,
              }
              return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nama,
                            id: item.id,
							idtipe : item.idtipe
                        }
                    })
                };
            }
        }
    });
	$("#lbl_obat_id2").change(function(){
		// console.log('data sini masuk');
		data=$("#lbl_obat_id2").select2('data')[0];
		if ($("#st_browse").val()==0){
			$("#idtipe2").val(data.idtipe);
		}
		$("#st_browse").val("0");

		$("#lbl_nama_obat2").val($("#lbl_obat_id2 option:selected").text());
		$("#lbl_qty2").val("0");
		$("#lbl_disc2").val("0");

		$.ajax({
		  url: '{site_url}tpasien_penjualan/get_obat_detail/'+$(this).val()+'/'+$("#idkelompokpasien").val()+'/'+$("#idtipe2").val(),
		  dataType: "json",
		  success: function(data) {
			$("#ttarif2").val(parseFloat(data.margin*data.hargadasar/100)+parseFloat(data.hargadasar));
			$("#lbltarif2").text(formatNumber($("#ttarif2").val()));
			$("#lblsatuan2").text(data.singkatan);
			$("#tstok2").val(data.stok);
			$("#lbl_qty2").focus();

		  }
		});
	});
	$("#dokter_pegawai_id").select2({
        minimumInputLength: 2,
		// allowClear: true
        tags: [],
        ajax: {
            url: '{site_url}tpasien_penjualan/get_dokter_pegawai/',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
            data: function (params) {
              var query = {
                search: params.term,
                kategori: $("#idkategori").val(),
              }
              return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nip + ' - ' +item.nama,
                            id: item.id
                        }
                    })
                };
            }
        }
    });

	$("#lbl_qty").keyup(function(){
		if (parseFloat($("#lbl_qty").val()) > parseFloat($("#tstok").val())){
			sweetAlert("Maaf...", "Stok Tidak Mencukupi!", "error");
			$("#lbl_qty").val($("#tstok").val());
		}
		gen_jumlah();
	});
	$("#nama").keyup(function(){
		validasi_save();
	});

	$("#lbl_qty2").keyup(function(){
		if (parseFloat($("#lbl_qty2").val()) > parseFloat($("#tstok2").val())){
			sweetAlert("Maaf...", "Stok Tidak Mencukupi!", "error");
			$("#lbl_qty2").val($("#tstok2").val());
		}
		gen_jumlah2();
	});
	$("#lbl_disc").keyup(function(){
		if (parseFloat($(this).val()) > 100){
			$(this).val(100);
		}
		gen_jumlah();
	});
	$("#rac_harga").change(function(){

		$("#rac_harga_label").text(formatNumber($("#rac_harga").val()));
		gen_jumlah_rac();
	});
	$("#rac_tuslah").keyup(function(){
		gen_jumlah_rac();
	});
	$("#lbl_disc2").keyup(function(){
		if (parseFloat($(this).val()) > 100){
			$(this).val(100);
		}
		gen_jumlah2();
	});
	$("#lbl_tuslah").keyup(function(){
		gen_jumlah();
	});

	$(document).on("click",".selectObat2",function(){
		clear_detail2();
		var idobat = ($(this).data('idobat'));
		var idtipe = ($(this).data('idtipe'));
		$("#lbl_qty2").val("0");
		$("#lbl_disc2").val("0");
		$("#lbl_tuslah2").val("0");
		$("#idtipe2").val(idtipe);
		$("#st_browse").val("1");
		$.ajax({
		  url: '{site_url}tpasien_penjualan/get_obat_detail/'+idobat+'/'+$("#idkelompokpasien").val()+'/'+$("#idtipe2").val(),
		  dataType: "json",
		  success: function(data) {
			$("#ttarif2").val(parseFloat(data.margin*data.hargadasar/100)+parseFloat(data.hargadasar));
			$("#lbltarif2").text(formatNumber($("#ttarif2").val()));
			$("#lblsatuan2").text(data.singkatan);
			$("#tstok2").val(data.stok);
			$("#lbl_nama_obat2").val(data.singkatan);

			var data = {
				id: idobat,
				text: data.nama
				};

				var newOption = new Option(data.text, data.id, false, false);
				$('#lbl_obat_id2').append(newOption).trigger('change');

				// setTimeout(function () {

				// }, 10);

		  }

		});
		$("#lbl_qty2").focus();
	});

	$("#dokter_pegawai_id").change(function(){
		// console.log('data sini masuk');
		// $("#nama").val($("#dokter_pegawai_id option:selected").text());
		$("#idpasien").val($("#dokter_pegawai_id").val());

		$.ajax({
		  url: '{site_url}tpasien_penjualan/get_dokter_pegawai_detail/'+$(this).val()+'/'+$("#idkategori").val(),
		  dataType: "json",
		  success: function(data) {
			  // alert(data.alamat);
			$("#nama").val(data.nama);
			$("#alamat").val(data.alamat);
			$("#tgl_lahir").val(data.tanggallahir);
			$("#telprumah").val(data.telepon);
			validasi_save();
		  }
		});
	});

	$("#idkelas").change(function() {
			var idruangan = $("#idruangan").val();
			var idkelas = $("#idkelas").val();
			var id = $("#getIDtindakan").val();

			$.ajax({
				url: "{site_url}tpasien_penjualan/getBed/" + idruangan + "/" + idkelas + "/" + id,
				dataType: "json",
				success: function(data) {
					$("#idbed").empty();
					$("#idbed").append("<option value=''>Pilih Opsi</option>");
					for (var i = 0; i < data.length; i++) {
						$("#idbed").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
					}
					$("#idbed").selectpicker('refresh');
				}
			});
		});

});
$(document).on("click","#btnSubmit-Penjualan",function(){
	// alert('Simpan');exit();
	// Tabel Non Racikan
	var tbl_non_racikan = $('table#manage-tabel-nonRacikan tbody tr').get().map(function(row) {
	  return $(row).find('td').get().map(function(cell) {
		return $(cell).html();
	  });
	});
	// Tabel Racikan
	var tbl_racikan = $('table#table_racikan_detail tbody tr').get().map(function(row) {
	  return $(row).find('td').get().map(function(cell) {
		return $(cell).html();
	  });
	});
	$("#pos_tabel_non_racikan").val(JSON.stringify(tbl_non_racikan));
	$("#pos_tabel_racikan").val(JSON.stringify(tbl_racikan));
	var tbl_index;
	$('#table_racikan_detail tbody tr').each(function() {
		  tbl_index = $('table#tabel'+$(this).find('td:eq(0)').text()+' tbody tr').get().map(function(row) {
		  return $(row).find('td').get().map(function(cell) {
			return $(cell).html();
		  });
		});
		$("#pos_tabel"+$(this).find('td:eq(0)').text()).val(JSON.stringify(tbl_index));
	});

	$("#form1").submit();
});
</script>
<script type="text/javascript" src="{ajax}basic-ajax.js"></script>

<!-- Load Lib Validation -->
<script src="{plugins_path}jquery-validation/jquery.validate.min.js"></script>
<script src="{plugins_path}jquery-validation/additional-methods.min.js"></script>
<script src="{plugins_path}js-validation/validation-tpasien-penjualan.js"></script>
<link rel="stylesheet" id="css-main" href="{toastr_css}">
<script src="{toastr_js}"></script>
