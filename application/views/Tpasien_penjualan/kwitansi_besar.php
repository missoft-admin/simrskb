<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>KWITANSI PENJUALAN </title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
    }
	@page {
           margin-top: 1,8em;
            margin-left: 0.8em;
            margin-right: 1.5em;
            margin-bottom: 2.5em;
        }
   

    @media screen {
      table {
		font-family: "Courier", Verdana, sans-serif;
        font-size: 20px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;

      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
      }
      .content-2 td {
        margin: 3px;
		border: 0px solid #6033FF;
      }

      /* border-normal */
      .border-full {
        border: 0px solid #000 !important;

      }
	  .text-header{
		  font-family: "Courier", Verdana, sans-serif;
		font-size: 23px !important;
      }
      .text-judul{
		  font-family: "Courier", Verdana, sans-serif;
		font-size: 29px !important;
      }
      .text-section{
		  font-family: "Courier", Verdana, sans-serif;
		font-size: 24px !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-bottom-top-left {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	 
    }
    </style>
    <script type="text/javascript">

    </script>
  </head>
  <body>
    <table class="content">
      <tr>
        <td class="text-header" rowspan="8" width="15%">
			<img src="<?=base_url()?>assets/upload/logo/logo.png" alt="" style="width: 150px;height: 150px;">
		</td>
		<td class="text-header" rowspan="8" width="85%">
          &nbsp;RSKB HALMAHERA SIAGA<br>
          &nbsp;JL. LLRE. Martadinata No. 28<br>
          &nbsp;Telp. +6222-4206061<br>
          &nbsp;Bandung

		</td>
      </tr>

    </table>
	<br>
	<label class="text-judul">KWITANSI PENJUALAN</label>
	<table class="content-2">

      <tr>
        <td width="15%"><strong></strong></td>
        <td width="1%"></td>
        <td width="20%"></td>
      </tr>
	  <tr>
        <td width="25%">NO FAKTUR</td>
        <td width="1%">:</td>
        <td width="30%"><?=$nopenjualan?></td>
        <td width="10%"></td>
		<td width="15%">UMUR</td>
        <td width="1%">:</td>
        <td width="25%"><?=(($umur)? $umur.' th ':'').(($umur_bulan)? $umur_bulan.' bln ':'').(($umur_hari)? $umur_hari.' hr ':'')?></td>
      </tr>
	  <tr>
        <td width="25%">NO. MEDREC</td>
        <td width="1%">:</td>
        <td width="30%"><?=$nomedrec?></td>
		<td width="10%"></td>
		<td width="15%">ASAL PASIEN</td>
        <td width="1%">:</td>
        <td width="25%"><?=$asalrujukan?></td>
      </tr>
	  <tr>
        <td width="25%">NAMA PASIEN</td>
        <td width="1%">:</td>
        <td width="30%"><?=$nama?></td>
		<td width="10%"></td>
		<td width="15%">RESEP</td>
        <td width="1%">:</td>
        <td width="25%"><?=($resep)?'ADA':'TIDAK ADA'?></td>

      </tr>

    </table>
	<br>
	<label class="text-section">NOTA RINCIAN</label>
    <table class="content-2">
      <tr>
        <td class="border-bottom-top text-left" width="5%">NO</td>
        <td class="border-bottom-top text-left" width="35%">KETERANGAN</td>
        <td class="border-bottom-top text-right" width="15%">HARGA SATUAN</td>
        <td class="border-bottom-top text-center" width="15%">QTY</td>
        <td class="border-bottom-top text-center" width="5%">DISC</td>
        <td class="border-bottom-top text-right" width="15%">TUSLAH</td>
        <td class="border-bottom-top text-right" width="20%">TOTAL</td>
      </tr>
      <?php $number = 0; ?>
      <?php foreach  ($list_data as $row){ ?>
        <?php $number = $number + 1; ?>
        <tr>
          <td class="border-full text-left"> <?=strtoupper(($number))?></td>
          <td class="border-full text-left"> <?=strtoupper(($row->nama))?></td>
          <td class="border-full text-right"><?= number_format($row->harga,0)?></td>
          <td class="border-full text-center"><?= number_format($row->kuantitas,0)?></td>
          <td class="border-full text-center"><?= number_format($row->diskon,0)?> %</td>
          <td class="border-full text-right"><?= number_format($row->tuslah,0)?></td>
          <td class="border-full text-right"><?= number_format($row->totalharga,0) ?></td>
        </tr>
      <?php } ?>
	  <?php foreach  ($list_data_racikan as $row){ ?>
        <?php $number = $number + 1; ?>
        <tr>
          <td class="border-full text-left"> <?=strtoupper(($number))?></td>
          <td class="border-full text-left"> <?=strtoupper(($row->namaracikan))?></td>
          <td class="border-full text-right"><?= number_format($row->harga,0)?></td>
          <td class="border-full text-center"><?= number_format($row->kuantitas,0)?></td>
          <td class="border-full text-center"><?= number_format(0,0)?> %</td>
          <td class="border-full text-right"><?= number_format($row->tuslah,0)?></td>
          <td class="border-full text-right"><?= number_format($row->totalharga,0) ?></td>
        </tr>
      <?php } ?>
      <tr>
        <td class="border-bottom-top text-left"></td>
        <td class="border-bottom-top text-left"><span style="float:right"></span></td>
        <td class="border-bottom-top">&nbsp;&nbsp;</td>
		<td class="border-bottom-top text-left"></td>
		<td class="border-bottom-top text-left"></td>
		<td class="border-bottom-top text-left"></td>
        <td class="border-bottom-top text-right"><span style="float:right">Rp. <?=number_format($totalharga,0)?></span></td>
      </tr>
    </table>
	<br>
	<table class="content-2">

      <tr>
        <td class="text-header" width="20%">TOTAL</td>
        <td width="1%">:</td>
        <td class="text-header" width="20%">Rp. <?=number_format($totalharga,0)?></td>

      </tr>
	  <tr>
        <td class="border-bottom" width="10%">TERBILANG</td>
        <td class="border-bottom" width="1%">:</td>
        <td class="border-bottom" width="80%"><?=terbilang($totalharga,4)?> Rupiah</td>

      </tr>


    </table>
	
	<br>
    <table>
      <tr>
        <td style="width:25%">Petugas,</td>
        <td style="width:75%">&nbsp;</td>
      </tr>

	  <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
	  <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>(<?=($created_nama!=''?$created_nama:$edited_nama)?>)</td>
        <td></td>
      </tr>
    </table>
  </body>
</html>
