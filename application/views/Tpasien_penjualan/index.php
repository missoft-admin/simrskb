<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('1044','1053'))){
if (UserAccesForm($user_acces_form,array('1044'))==false){
	$tab='1';
}
	?>
<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1044'))){ ?>
		<li class="<?=($tab==0)?'active':''?>">
			<a href="#tab1home">Rujukan</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1053'))){ ?>
		<li class="<?=($tab==1)?'active':''?>">
			<a href="#tab2NR">Non-Rujukan</a>
		</li>
		<?}?>
	</ul>
	<div class="block-content tab-content">
		<?php if (UserAccesForm($user_acces_form,array('1044'))){ ?>
		<div class="tab-pane fade <?=($tab==0)?'in active':''?>" id="tab1home">

			<hr style="margin-top:0px">

			<div class="row">
				<?php echo form_open('tpasien_penjualan/filter','class="form-horizontal" id="form-work"') ?>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">No Penjualan</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="nopenjualan" value="{nopenjualan}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">No Medrec</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="nomedrec" value="{nomedrec}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Nama Pasien</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="namapasien" value="{namapasien}">
						</div>
					</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="">Asal Pasien</label>
							<div class="col-md-8">
								<select name="idasalpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="0" selected>Semua Asal Pasien</option>
									<option value="1" <?=($idasalpasien == '1' ? 'selected' : '')?>>Poliklinik</option>
									<option value="2" <?=($idasalpasien == '2' ? 'selected' : '')?>>Instalasi Gawat Darurat</option>
									<option value="3" <?=($idasalpasien == '3' ? 'selected' : '')?>>Rawat Inap</option>
								</select>
							</div>
						</div>
					<div class="form-group">
		          <label class="col-md-4 control-label" for="example-daterange1">Tanggal</label>
		          <div class="col-md-8">
		            <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
		                <input class="form-control" type="text" name="tanggaldari" placeholder="From" value="{tanggaldari}">
		                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
		                <input class="form-control" type="text" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
		            </div>
		          </div>
		      </div>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Kelompok Pasien</label>
						<div class="col-md-8">
							<select name="idkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                <option value="#" <?=($idkelompokpasien == '#' ? 'selected="selected"':'')?>>Semua Kelompok Pasien</option>
                <?php foreach (get_all('mpasien_kelompok') as $row){?>
                  <option value="<?=$row->id ?>" <?=($idkelompokpasien == $row->id ? 'selected="selected"':'')?>><?=$row->nama ?></option>
                <?php } ?>
              </select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Status</label>
						<div class="col-md-8">
							 <select name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="0">Semua Status</option>
								<option value="1" <?=($status == 1 ? 'selected="selected"':'')?>>Belum Ditindak</option>
								<option value="2" <?=($status == 2 ? 'selected="selected"':'')?>>Sudah Ditindak</option>
								<option value="3" <?=($status == 3 ? 'selected="selected"':'')?>>Sudah Diserahkan</option>
							  </select>
						</div>
					</div>
					<?php if (UserAccesForm($user_acces_form,array('1045'))){ ?>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for=""></label>
						<div class="col-md-8">							
							<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
							
						</div>
					</div>
					<?}?>
				</div>
				<?php echo form_close() ?>
			</div>
			<hr style="margin-top:10px">

			<!-- <h4 class="font-w300 push-15">Home Tab</h4> -->
			<table width="100%" id="table-Rujukan" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Tanggal/Jam</th>
						<th>No. Jual</th>
						<th>No. Medrec</th>
						<th>Nama Pasien</th>
						<th>Asal Pasien</th>
						<th>Status</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1053'))){ ?>
		<div class="tab-pane fade <?=($tab==1)?'in active':''?>" id="tab2NR">

			<hr style="margin-top:0px">

			<div class="row">
				<?php echo form_open('tpasien_penjualan/filternonrujukan','class="form-horizontal" id="form-work"') ?>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">No Penjualan</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="nopenjualan" value="{nopenjualan}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">No Medrec</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="nomedrec" value="{nomedrec}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Nama Pasien</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="namapasien" value="{namapasien}">
						</div>
					</div>
					<div class="form-group">
		          <label class="col-md-4 control-label" for="example-daterange1">Tanggal</label>
		          <div class="col-md-8">
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
		                <input class="form-control" type="text" name="tanggaldari" placeholder="From" value="{tanggaldari}">
		                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
		                <input class="form-control" type="text" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
		            </div>
		          </div>
		      </div>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Status</label>
						<div class="col-md-8">
							<select name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                <option value="0" selected>Semua Status</option>
                <option value="1" <?=($status == 1 ? 'selected="selected"':'')?>>Belum Ditindak</option>
                <option value="2" <?=($status == 2 ? 'selected="selected"':'')?>>Sudah Ditindak</option>
                <option value="3" <?=($status == 3 ? 'selected="selected"':'')?>>Sudah Diserahkan</option>
              </select>
						</div>
					</div>
					<?php if (UserAccesForm($user_acces_form,array('1054'))){ ?>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for=""></label>
						<div class="col-md-8">
							<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						</div>
					</div>
					<?}?>
				</div>
				<?php echo form_close() ?>
			</div>

			<hr style="margin-top:10px">
			<?php if (UserAccesForm($user_acces_form,array('1055'))){ ?>
			<div class="text-right lter">
				<a href="{base_url}tpasien_penjualan/create/" id="link-tambah" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> Tambah</a>
				<?=br(2)?>
			</div>
			<hr style="margin-top:10px">
			<?}?>

			<!-- <h4 class="font-w300 push-15">Profile Tab</h4> -->
			<table width="100%" id="table-nonRujukan" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th width="5%">#</th>
						<th width="15%">Tanggal/Jam</th>
						<th width="10%">Kategori</th>
						<th width="15%">No. Jual</th>
						<th width="15%">Nama Pasien</th>
						<th width="15%">Jumlah</th>
						<th width="15%">Status Pasien</th>
						<th width="10%">Action</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<?}?>
	</div>
</div>
<?}?>
<link rel="stylesheet" id="css-main" href="{toastr_css}">
<script src="{toastr_js}"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript" src="{ajax}basic-ajax.js"></script>
<script type="text/javascript">
	jQuery(function() {
		BaseTableDatatables.init();
		$('#table-Rujukan').DataTable({
			"autoWidth": false,
			"pageLength": 10,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}tpasien_penjualan/get_rujukan/' + '<?=$this->uri->segment(2);?>',
				type: "POST",
				dataType: 'json'
			},
			"columnDefs": [{
					"width": "5%",
					"targets": 0,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 1,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 2,
					"orderable": true
				},
				{
					"width": "15%",
					"targets": 3,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 4,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 5,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 6,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 7,
					"orderable": true
				}
			]
		});

		$('#table-nonRujukan').DataTable({
			"autoWidth": false,
			"pageLength": 10,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}tpasien_penjualan/get_non_rujukan/' + '<?=$this->uri->segment(2);?>',
				type: "POST",
				dataType: 'json'
			},
			"columnDefs": [{
					"width": "5%",
					"targets": 0,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 1,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 2,
					"orderable": true
				},
				{
					"width": "15%",
					"targets": 3,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 4,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 5,
					"orderable": true
				}
			]
		});
	});
</script>
