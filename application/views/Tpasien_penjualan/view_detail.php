<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-rounded">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="../tpasien_penjualan" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
        <hr style="margin-bottom: 0px;">
    </div>
    <div class="block-content">
        <form id="form1" class=" form-horizontal push-10-t" action="../tpasien_penjualan/save" method="post">
<input type="hidden"  name="json-detail-nonracikan" id="json-detail-nonracikan" value=""/>
<input type="hidden"  name="json-detail-racikan" id="json-detail-racikan" value=""/>
<input type="hidden"  name="json-detail-obat-racikan" id="json-detail-obat-racikan" value=""/>
<input type="hidden"  name="post-totalbarang" id="post-totalbarang" value=""/>
<input type="hidden"  name="post-Trow" id="post-Trow" value=""/>
<input type="hidden"  name="post-Trow2" id="post-Trow2" value=""/>
<input type="hidden" name="getNOpenjualan" id="getNOpenjualan" <?=(!empty($_GET['id']))?'value="'.getwhere('id',decriptURL($_GET['id']),'tpasien_penjualan')->row()->nopenjualan.'"':''?>/>
<input type="hidden" name="get-IDpenjualan" id="get-IDpenjualan" <?=(!empty($_GET['id']))?'value="'.decriptURL($_GET['id']).'"':''?>/>
<!-- Start Input Data Pasien -->
<?php if(empty($_GET['id'])){?>
    <div class="col-sm-6 col-lg-6">
        <?php }else{?>
    <div class="col-sm-12 col-lg-12">
        <?php }?>
        <div class="block block-rounded block-bordered">
                <div class="block-header">
                    <h3 class="block-title">DETAIL PASIEN</h3>
                </div>
                <div class="block-content">
    <!-- <div class="col-sm-6 col-lg-12"> -->
        <!-- <div class="block block-rounded block-bordered"> -->
                <!-- <div class="block-content"> -->
                    <table class="table">
                        <tr width="30%">
                            <th width="20%">Tanggal <span class="text-danger">*</span></th>
                            <td width="35%">
                        <div class="form-group">
                                <div class="col-sm-12">
                                        <input disabled class="js-datepicker form-control input-sm" type="text" id="manage-input-tanggal" name="manage-input-tanggal" data-date-format="dd-mm-yyyy" value="<?=date('d-m-Y')?>"/>
                                </div>
                        </div>
                            </td>
                            <?=(!empty($_GET['id']))? '<th>&nbsp;</th><td>&nbsp;</td>':'';?>
                        </tr>
                        <?php
                            foreach($listpasien1 as $listpasien){
$mpasien=getwhere('id',$listpasien->idasalpasien,'mpasien_asal')->row();
                                if(count($mpasien)>1){
$asalpasien=$mpasien->nama;
                                }else{
$asalpasien='';
                                }
                            ?>
                            <input type="hidden" name="getIDtindakan" id="getIDtindakan" value="<?=(!empty($_GET['id']))?$listpasien->idtindakan:''?>">
                            <input type="hidden" name="getIDpasien" id="getIDpasien" value="<?=(!empty($_GET['id']))?$listpasien->idpasien:''?>">
                            <!-- <input type="hidden" name="getIDbatal" id="getIDbatal" value="<?=(!empty($_GET['id']))?$listpasien->no_medrec:''?>"> -->
                            <!-- <input type="hidden" name="getIDbatal" id="getIDbatal" value="{getIDbatal}"> -->
                        <tr>
                        <th>Status Pasien <span class="text-danger">*</span></th>
                        <td>
                        <div class="form-group">
                                <div class="col-sm-12">
                                    <label class="css-input input-disabled css-radio css-radio-sm css-radio-primary push-10-r">
                                        <input disabled type="radio" id="manage-input-status-pasien" name="manage-input-status-pasien" <?=($listpasien->statuspasien==1)?'checked':''?> value="1" /><span></span> Pasien RS
                                    </label>
                                    <label class="css-input input-disabled css-radio css-radio-sm css-radio-primary push-10-r">
                                        <input disabled type="radio" id="manage-input-status-pasien" name="manage-input-status-pasien" <?=($listpasien->statuspasien==0)?'checked':''?> value="0" /><span></span> Pasien non-RS
                                    </label>
                                </div>
                        </div>
                            </td>
                            <th>Asal Pasien</th><td>
                        <div class="form-group">
                                <div class="col-sm-12">
                                    <input readonly type="text" class="form-control input-sm" id="manage-input-asal-pasien" name="manage-input-asal-pasien" value="<?=($listpasien->idtipe==1)?'IGD':'UGD'?>" />
                                </div>
                        </div>
                    </td>
                        </tr>
                        <tr>
                            <th>Resep <span class="text-danger">*</span></th>
                            <td>
                        <div class="form-group">
                                <div class="col-sm-12">
                                    <select disabled class="js-select2 form-control input-sm" id="manage-select-resep" name="manage-select-resep">
                                      <option >-- Pilih Opsi --</option>
                                      <option <?=($listpasien->statusresep==0)?'selected':''?> value="0">Tidak Ada</option>
                                      <option <?=($listpasien->statusresep==1)?'selected':''?> value="1">Ada</option>
                                    </select>
                                </div>
                        </div>
                            </td>
                            <th>Rujukan</th><td>
                        <div class="form-group">
                                <div class="col-sm-12">
                                    <input readonly type="text" class="form-control input-sm" id="manage-input-rujukan-pasien" name="manage-input-rujukan-pasien"  value="<?=(!empty($_GET['id']))?$asalpasien:''?>"/>
                                </div>
                        </div>
                    </td>
                        </tr>
                        <tr>
                            <th>No. Medrec <span class="text-danger">*</span></th>
                            <td>
                        <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" id="manage-input-medrec-pasien" name="manage-input-medrec-pasien" readonly value="<?=(!empty($_GET['id']))?$listpasien->no_medrec:''?>" />
                                </div>
                        </div>
                            </td>
                            <th>Kelompok Pasien</th><td>
                        <div class="form-group">
                                <div class="col-sm-12">
                                    <input readonly type="text" class="form-control input-sm" id="manage-input-nama-kelompok" name="manage-input-nama-kelompok" value="<?=(!empty($_GET['id']))?$listpasien->namakelompok:''?>" />
                                </div>
                        </div>
                    </td>
                        </tr>
                        <tr>
                            <th>Nama Pasien <span class="text-danger">*</span></th>
                            <td>
                        <div class="form-group">
                                <div class="col-sm-12">
                                    <input readonly type="text" class="form-control input-sm" id="manage-input-nama-pasien" name="manage-input-nama-pasien" value="<?=(!empty($_GET['id']))?$listpasien->namapasien:''?>" />
                                </div>
                        </div>
                            </td>
                            <th>Poliklinik</th><td>
                        <div class="form-group">
                                <div class="col-sm-12">
                                    <input readonly type="text" class="form-control input-sm" id="manage-input-nama-poliklinik" name="manage-input-nama-poliklinik" value="<?=(!empty($_GET['id']))?$listpasien->namapoliklinik:''?>" />
                                </div>
                        </div>
                    </td>
                        </tr>
                        <tr>
                            <th>Alamat <span class="text-danger">*</span></th>
                            <td>
                        <div class="form-group">
                                <div class="col-sm-12">
                                    <textarea readonly class="form-control input-sm" id="manage-textarea-alamat-pasien" name="manage-textarea-alamat-pasien"><?=(!empty($_GET['id']))?$listpasien->alamat_jalan:''?></textarea>
                                </div>
                        </div>
                            </td>
                            <th>Alergi Obat</th><td>
                        <div class="form-group">
                                <div class="col-sm-12">
                                    <input readonly type="text" class="form-control input-sm" id="manage-input-alergi-pasien" name="manage-input-alergi-pasien" value="<?=(!empty($_GET['id']))?$listpasien->alergi_obat:''?>" />
                                </div>
                        </div>
                    </td>
                        </tr>
                        <tr>
                            <th>No. Telp</th>
                            <td>
                        <div class="form-group">
                                <div class="col-sm-12">
                                    <input readonly type="text" class="form-control input-sm" name="manage-input-notelp-pasien" id="manage-input-notelp-pasien" value="<?=(!empty($_GET['id']))?$listpasien->telepon:''?>" />
                                </div>
                            </div>
                            </td>
                            <th>&nbsp;</th><td>&nbsp;
                    </td>
                        </tr>
                        <?php 
                    }
                    ?>
                    </table>
                <!-- </div>
            </div>
        </div> -->
    </div>
</div>
</div>
        <!-- End Input Data Pasien -->

<input type="hidden" value="<?=$_GET['id']?>" id="hide-id" />
<input type="hidden" value="<?=$_GET['tipe']?>" id="hide-tipe" />
<input type="hidden" value="<?=$_GET['status-pasien']?>" id="hide-statuspasien" />
                            <!-- List Obat Non-Racikan -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="progress progress-mini">
                        <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                    </div>
                    <h5 style="margin-bottom: 10px;">Non Racikan</h5>
                    <div class="control-group">
                        <div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0;">
                            <table id="manage-tabel-nonRacikan" class="table table-striped table-bordered" style="margin-bottom: 0;">
                                <thead>
                                    <tr>
                                        <th style="width: 15%;">Nama Obat</th>
                                        <th style="width: 15%;">Tarif</th>
                                        <th style="width: 7%;">Satuan</th>
                                        <th style="width: 10%;">Cara Pakai</th>
                                        <th style="width: 10%;">Qty</th>
                                        <th style="width: 9%;">Disc(%)</th>
                                        <th style="width: 15%;">Tuslah R.</th>
                                        <th style="width: 15%;">Jumlah</th>
                                        <th style="width: 17%;">Actions</th>
                                    </tr>
                                </thead>
                                <tbody id="manage-tbody-nonRacikan"></tbody>
                                <tfoot id="foot-total-nonracikan">
                                    <tr>
                                        <td colspan="7" class="hidden-phone">
                                            <span class="pull-right"><b>TOTAL</b></span></td>
                                        <td colspan="2"><b>Rp<span id="manage-fullTotal-nonRacikan">0</span></b></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        <!-- List Obat Racikan -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="progress progress-mini">
                        <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                    </div>
                    <h5 style="margin-bottom: 10px;">Racikan</h5>
                    <div class="control-group">
                        <div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0;">
                            <table id="table_racikan_detail" class="table table-striped table-bordered" style="margin-bottom: 0;">
                                <thead>
                                    <tr>
                                        <th style="width: 20%;">Nama</th>
                                        <th style="width: 11%;">Jenis</th>
                                        <th style="width: 7%;">Cara Pakai</th>
                                        <th style="width: 7%;">Qty Racikan</th>
                                        <th style="width: 7%;">Harga</th>
                                        <th style="width: 7%;">Tuslah R.</th>
                                        <th style="width: 7%;">Jumlah</th>
                                        <th style="width: 5%;">Actions</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_racikan">
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="6" class="hidden-phone">                                        
                                        <span class="pull-right"><b>TOTAL</b></span></td>
                                        <td colspan="2"><b>Rp
                                            <label id="disp_total_racikan">0</label></b>
                                            <input type="hidden" id="total_racikan" name="total_racikan" value="0" />
                                        </td>
                                    </tr>
                                    <tr class="bg-light dker">
                                        <th colspan="6"><label class="pull-right"><b>GRAND TOTAL</b></label></th>
                                        <td colspan="2"><b>Rp
                                            <label id="disp_total">0</label></b>
                                            <input type="hidden" id="total" name="grandtotal" value="0" />
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                            <table style="display: none;">
                                <tbody id="row_racikan_detail_clone">
                                    <tr id="tr_racikan" class="c_tr_racikan">
                                        <td>
                                            <input type="hidden" id="clone_racikan_detail_id" />
                                            <input type="hidden" id="clone_tmp_counter_racikan" />
                                            <input type="hidden" id="clone_racikan" />
                                            <input type="hidden" id="clone_idracikan" />
                                            <input type="hidden" id="clone_list_obat" />
                                            <div id="clone_div_racikan_obat" class="input-group">
                            <input class="form-control input-sm nama_racikan" readonly type="text" id="clone_nama_racikan" />
                            <span class="input-group-btn">
                                <button class="btn btn-info input-sm list_obat_racikan" type="button">List Obat (0)</button>
                            </span>
                        </div>
                    </td>
<td>
    <span id="clone_span_jenis_racikan"></span></td>
<td>
    <span id="clone_span_cara_pakai_racikan"></span></td>
<td>
    <span id="clone_disp_quantity_racikan"></span></td>
<td>
    Rp<span id="clone_disp_racikan_jmlharga">0</span></td>
<td>
    Rp<span id="clone_disp_racikan_tuslah_r">0</span></td>
<td>
    Rp<span id="clone_disp_subtotal_racikan">0</span>
    <input type="hidden" class="hide-subtotal-racik" id="clone_hide-subtotal-racik"></td>
<td>-</td>
                                                </tr>
                                    <tr id="tr_racikan_obat" class="c_tr_racikan_obat" style="display: none;"></tr>
                                </tbody>
                            </table>
                    <input type="hidden" id="counter" name="counter" value="0" />
                    <input type="hidden" id="counter_racikan" name="counter_racikan" value="0" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="progress progress-mini">
                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
            </div>
            <hr>
            <div class="block-footer text-right bg-light lter">
                <button class="btn btn-info" type="submit" id="btnSubmit-Penjualan" disabled name="btnSubmit-Penjualan">Simpan <img style="display: none;" id="loading-button-ajax" src="{ajax}img/ajax-loader.gif"></button>
                <button class="btn btn-default" type="button" id="btnCancel-Penjualan" name="btnCancel-Penjualan">Batal</button>
        <br><br>
            </div>
                        </div>
                    </form>
                    </div>
                </div>
            <!-- </div> -->
        <input type="hidden" id="detail_value" name="detail_value" value="[]">
    <!-- </div> -->
<!-- </div> -->
<button hidden id="success_msg" class="js-notify" data-notify-type="success" data-notify-message="Success!">Success</button>
<button hidden id="error_msg" class="js-notify" data-notify-type="danger" data-notify-message="Error!">Danger</button>
           
<!-- Modal Data Pasien -->
<div class="modal fade in black-overlay" data-backdrop="false" id="modal-data-ListRacikanObat" tabindex="-1" role="dialog" aria-hidden="true" style="display: none; padding-right: 30px;">
            <div class="modal-dialog modal-lg modal-dialog-popout">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-header bg-success">
                            <ul class="block-options">
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>
                            <h3 class="block-title">List Data Racikan Obat</h3>
                        </div>
                        <div class="block-content">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <blockquote>
<footer id="list_racikan_modal_nama"></footer>
                                    </blockquote>
                                    </div>
                                </div>
                            </div>
            <div class="progress progress-mini">
                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
            </div>
<table id="manage-tabel-list-Racikan" class="table table-striped table-bordered" style="margin-bottom: 0;">
    <thead>
        <tr>
            <th style="width: 15%;">Nama Obat</th>
            <th style="width: 15%;">Tarif</th>
            <th style="width: 10%;">Satuan</th>
            <th style="width: 10%;">Qty</th>
            <th style="width: 9%;">Disc(%)</th>
            <th style="width: 15%;">Jumlah</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                        <!-- <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Ok</button> -->
                    </div>
                </div>
            </div>
        </div>
<!-- Modal Data Pasien -->
<div class="modal fade in black-overlay" data-backdrop="false" id="modal-data-penjualan" tabindex="-1" role="dialog" aria-hidden="true" style="display: none; padding-right: 30px;">
            <div class="modal-dialog modal-lg modal-dialog-popout">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-header bg-success">
                            <ul class="block-options">
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>
                            <h3 class="block-title">List Data Penjualan</h3>
                        </div>
                        <div class="block-content">
                            <table width="100%" id="tabel-data-penjualan" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tanggal</th>
                                        <th>No. Penjualan</th>
                                        <th>No. Medrec</th>
                                        <th>Nama</th>
                                        <th>Total Barang</th>
                                        <th>Total Harga</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody-data-penjualan"></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                        <!-- <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Ok</button> -->
                    </div>
                </div>
            </div>
        </div>


<!-- End Modal Data Pasien -->
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript" src="{ajax}tpasien-penjualan/view-js.js"></script>
<script type="text/javascript" src="{ajax}basic-ajax.js"></script>
        <link rel="stylesheet" id="css-main" href="{toastr_css}">
        <script src="{toastr_js}"></script>

<script type="text/javascript">
    $(function () {// Setup AJAX
var kuantitas,harga,sisaobat,total,det_td,det_td2,det_td3,labelTHracikan;
$.ajaxSetup({
type:"post",
cache:false,
dataType: "json"
});

// getDataSSP('#tabel-data-penjualan','../get/datapenjualan')
$(document).on("focus","tr td input",function() { $(this).select();} );

$(document).ready(function(){
    loadDetail()

$(document).on("click", ".list_obat_racikan", function() {
                det_th=$('table#manage-tabel-list-Racikan thead');
                det_td3=$('table#manage-tabel-list-Racikan tbody');
                det_td3.empty();
        $.ajax({
            before:function(){
                // $('#loading-button-ajax').show()
            },
            url:"showRacikanObat",
            // type:"json",
            data:{"idracikan":$(this).attr('data-id'),"namaracikan":$(this).attr('data-NRac')},
            success:function(data){
                var show=data.dataobatracikan;
    $('#list_racikan_modal_nama').html('<b>Nama Racikan: '+show[0].namaracikan+'</b>')
            for(var t=0;t<show.length;t++){
                content = "<tr class='tr_list_obat'>";
                content += '<td>'+show[t].namaobat+'<input type="hidden" value="'+show[t].namaobat+'" name="det-racikan-namaobat[]"></td>';
                content += '<td>Rp'+show[t].hargabeli+'<input type="hidden" value="'+show[t].hargabeli+'" name="det-racikan-harga[]"></td>';
                content += '<td>'+show[t].singkatan+'<input type="hidden" value="'+show[t].singkatan+'" name="det-racikan-satuan[]"></td>';
                content += '<td>'+show[t].kuantitas+'<input type="hidden" value="'+show[t].kuantitas+'" name="det-racikan-qty[]"></td>';
                content += '<td>'+show[t].diskon+'<input type="hidden" value="'+show[t].diskon+'" name="det-racikan-diskon[]"></td>';
                content += '<td>Rp'+show[t].totalharga+'<input type="hidden" value="'+show[t].totalharga+'" name="det-racikan-jumlah[]"></td>';
                content += '<td style="display:none">'+show[t].idobat+'<input type="hidden" value="'+show[t].idobat+'" name="det-racikan-idobat[]"></td>';
                content += '</tr>';
                det_td3.append(content);
                }
    $('#modal-data-ListRacikanObat').modal('show')
}
})
})
 

$('#manage-cari-pengembalian').click(function(){
            $('#modal-data-penjualan').modal('show')
    })

})
//** End document ready  

// fungsi tombol open modal Non-Racikan 
        $('#manage-tambah-pengembalian').click(function(){
            $('#manage-modal-list-pengembalian').modal('show')
        })

        // tombol hapusdet-obat
        $(document).on('click','#manage-hapusdet-pengembalian',function(){
                var row = $(this).closest('td').parent();
                var totalFull=$('#manage-fullTotal-pengembalian');
                alertDelete(row,'manage-tbody-pengembalian',totalFull,7);
        })

    })

    function checkTotal() {
        var totalhargaretur = 0;
        $('#manage-tabel-nonRacikan tbody tr').each(function() {
            totalhargaretur += parseFloat($(this).find('.label-totalharga').text().replace(/\,/g, ""));
        });

        $("#manage-fullTotal-nonRacikan").html(totalhargaretur.toLocaleString(undefined, {
            minimumFractionDigits: 0
        }));


        // SUM GRAND TOTAL
        // var totalNonRacikan = totalhargaretur;
        // var totalRacikan = $("#disp_total_racikan").html().replace(/\,/g, "");
        // var grandTotal = (parseFloat(totalNonRacikan) + parseFloat(totalRacikan));
        //  $("#disp_total").html(grandTotal.toLocaleString(undefined, {
        //     minimumFractionDigits: 0
        // }));
    }

function loadDetail(){
    // $.ajax({
            // url:"decript",
            // data:{"id":$('#hide-id'),"tipe":$('#hide-tipe'),"statuspasien":$('#hide-statuspasien')},
                    // processData:false,
            // success:function(data){
        $("#counter_racikan").val(0);
                det_td=$('table#manage-tabel-nonRacikan tbody#manage-tbody-nonRacikan');
                det_td2=$('table#table_racikan_detail tbody');
                det_td.find('tr').remove();
                det_td2.find('tr').remove();
        $.ajax({
            before:function(){

            },
            url:"showNonRacikan",
            // type:"json",
            data:{"idpenjualan":$('#hide-id').val()},
            success:function(data){
                var show =data.dataobatnonracikan;
                $('#post-TrowNonracikan').val(show.length);
            for(var i = 0;i < show.length; i++){
                content = "<tr id='rowke-"+(i+1)+"'>";
                content += '<td>'+show[i].namaobat+'<input type="hidden" name="hide-namaobat[]" value="'+show[i].namaobat+'"></td>';
                content += '<td>Rp<span class="label-hargabeli" id="label-hargabeli_'+i+'">'+show[i].hargabeli
                +'</span><input type="hidden" name="hide-hargabeli[]" value="'+show[i].hargabeli+'"></td>';
                content += '<td>'+show[i].singkatan+'<input type="hidden" name="hide-singkatan[]" value="'+show[i].singkatan+'"></td>';
                content += '<td>'+show[i].carapakai+'<input type="hidden" name="hide-carapakai[]" value="'+show[i].carapakai+'"></td>';
                content += '<td>'+show[i].kuantitas+'<input type="hidden" name="hide-kuantitas[]" value="'+show[i].kuantitas+'"></td>';
                content += '<td><span class="label-diskon" id="label-diskon_'+i+'">'+show[i].diskon
                +'</span><input type="hidden" name="hide-diskon[]" value="'+show[i].diskon+'"></td>';
                content += '<td>Rp<span class="label-tuslah" id="label-tuslah_'+i+'">'+show[i].tuslah
                +'</span><input type="hidden" name="hide-tuslah[]" value="'+show[i].tuslah+'"></td>';
                content += '<td>Rp<span class="label-totalharga" id="label-totalharga_'+i+'">'+show[i].totalharga
                +'</span><input type="hidden" name="hide-totalharga[]" value="'+show[i].totalharga+'"></td>';
                content += '<td>-</td>';
                content += '</tr>';
                content += '<input type="hidden" name="hide-idobat[]" value="'+show[i].idobat+'">';
                det_td.append(content);
}
                }
            });
$.ajax({
before:function(){
    // $('#loading-button-ajax').show()
},
url:"showRacikan",
// type:"json",
data:{"idpenjualan":$('#hide-id').val()},
success:function(data){
var show=data.dataracikan;
    for(var y=0;y<show.length;y++){
cloneTabel2();
var Y=(y+1);

$('#tr_racikan_'+Y+' .list_obat_racikan').text('List Obat (' + show[y].totalobat +')');
$('#tr_racikan_'+Y+' .list_obat_racikan').attr('data-id',show[y].idracikan)
$('#tr_racikan_'+Y+' .list_obat_racikan').attr('data-NRac',show[y].namaracikan)
$('#hide-idracikan_'+Y).val(show[y].idracikan)

$('#nama_racikan_'+Y).val(show[y].namaracikan)
$('#span_jenis_racikan_'+Y).text(show[y].jenisracikan)
$('#span_cara_pakai_racikan_'+Y).text(show[y].carapakai)
$('#disp_quantity_racikan_'+Y).text(show[y].kuantitas)
$('#span_sisa_obat_'+Y).text(show[y].sisaobat)
$('#disp_racikan_tuslah_r_'+Y).text(show[y].tuslah)
$('#disp_racikan_jmlharga_'+Y).text(show[y].harga)
$('#disp_subtotal_racikan_'+Y).text(show[y].subtotal)
$('#disp_total_racikan').text(show[y].totalharga)
}
            var ctr =$('#tbody_racikan tr.c_tr_racikan').length;
                $('#post-Trow').val(ctr)
            checkTotal();
            getTotal();
}
});
        // }
        // });
}
</script>