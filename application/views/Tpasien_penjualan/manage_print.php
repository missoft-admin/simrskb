<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<!-- Developer : @RendyIchtiarSaputra & @GunaliRezqiMauludi -->
<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tpoliklinik_pendaftaran/create" class="btn"><i class="fa fa-plus"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
<?php echo form_open_multipart('tpasien_penjualan/print_e_ticket','class="form-horizontal" id="form-work"') ?>
	<div class="block-content">
		<hr style="margin-top:0px">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="nomedrec">Start Label Ke -</label>
					<div class="col-md-4">
					<select id="start_awal" name="start_awal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
						<?php for ($i=1; $i <= 10; $i++) {?>
							<option value="<?=$i?>" <?=($start_awal == $i ? 'selected' : '')?>><?=$i?></option>
						<?php } ?>
					</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="nomedrec">Jenis</label>
					<div class="col-md-4">
					<select id="type_etiket" name="type_etiket" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
							<option value="0" <?=($type_etiket =='0' ? 'selected' : '')?>>Obat Syrup</option>
							<option value="1" <?=($type_etiket =='1' ? 'selected' : '')?>>Lainnya</option>
					</select>
					</div>
				</div>
				<input type="hidden" id="counter" name="counter" value="0">
				<input type="hidden" id="sisa" name="sisa" value="10">
				<input type="hidden" name="idpenjualan" value="{idpenjualan}">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-4">
						<button class="btn btn-success text-uppercase" disabled type="submit" id="button" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-print"></i> Cetak E-Ticket</button>
					</div>
				</div>

			</div>


		</div>
		<hr>
		<? $no=0;?>
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-obat">
			<thead>
				<tr>
					<th class="text-center" style="width: 5%;">
						<label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
							X
						</label>
					</th>
					<th style="width: 20%;">Non Racikan</th>
					<th style="width: 7%;">Cara Pakai</th>
					<th style="width: 10%;">Aturan</th>
					<th style="width: 7%;">Qty</th>
					<th style="width: 5%;">#</th>
				</tr>
			</thead>
			<tbody>
				<?
					foreach($list_data as $row){
					$no +=1;
				?>
					<tr>
						<input type="hidden" id="chck_obat[<?=$no?>]" name="chck_obat[<?=$no?>]" value="0">
						<input type="hidden" id="idjual_obat[<?=$no?>]" name="idjual_obat[<?=$no?>]" value="<?=$row->id?>">
						<td class="text-center">
							<label class="css-input css-checkbox css-checkbox-primary">
								<input type="checkbox" class="chck" id="chck_obat[<?=$no?>]" name="chck_obat[<?=$no?>]"  value="1"><span></span>
							</label>
						</td>
						<td><?=$row->nama?></td>
						<td style='display:none'><?=$row->idbarang?></td>
						<td><?=$row->carapakai?></td>
						<td><?=cara_minum($row->jenispenggunaan)?></td>
						<td align="right"><?=$row->kuantitas?></td>
						<td><?=$no?></td>
						<td style='display:none'><?=$row->id?></td>
					</tr>
					<?
				}?>
			</tbody>

		</table>
		</div>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable_racikan">
			<thead>
				<tr>
					<th class="text-center" style="width: 5%;">
						<label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
							X
						</label>
					</th>
					<th style="width: 20%;">Racikan</th>
					<th style="width: 7%;">Cara Pakai</th>
					<th style="width: 10%;">Aturan</th>
					<th style="width: 7%;">Qty</th>
					<th style="width: 5%;">#</th>
				</tr>
			</thead>
			<tbody>
				<?
					foreach($list_data_racikan as $row){
					$no +=1;
				?>
					<tr>
						<input type="hidden" id="chck_racikan[<?=$no?>]" name="chck_racikan[<?=$no?>]" value="0">
						<input type="hidden" id="idjual_racikan[<?=$no?>]" name="idjual_racikan[<?=$no?>]" value="<?=$row->id?>">
						<td class="text-center">
							<label class="css-input css-checkbox css-checkbox-primary">
								<input type="checkbox" class="chck" id="chck_racikan[<?=$no?>]" name="chck_racikan[<?=$no?>]"   value="1"><span></span>
							</label>
						</td>
						<td><?=$row->namaracikan?></td>
						<td style='display:none'><?=$row->id?></td>
						<td><?=$row->carapakai?></td>
						<td><?=cara_minum($row->jenispenggunaan)?></td>
						<td align="right"><?=$row->kuantitas?></td>
						<td style='display:none'><?=$row->id?></td>
						<td><?=$no?></td>
					</tr>
					<?
				}?>
			</tbody>

		</table>
	</div>
	</div>
	<?php echo form_close() ?>
</div>


<!-- End Modal Action END PRINT -->

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
		$('input[type="checkbox"]').change(function(){
			   var checkedValue = $('input:checkbox:checked').map(function(){
						return this.value;
						// alert(this.value);
					}).get();
					$("#counter").val(checkedValue.length);   //display selected checkbox value
					hitung_sisa();
					// alert($("#start_awal").val());
		 })

	});
	$("#start_awal").change(function(){
		hitung_sisa();
	});
	function hitung_sisa(){
			var total=10;
			total=total-parseFloat($("#start_awal").val())-parseFloat($("#counter").val())+1;
			$("#sisa").val(total);
			if ($("#sisa").val() < 0){
				$("#button").attr('disabled',true);
				// $(".chck").attr('disabled',true);
				swal({
						title: 'Warning!',
						text: 'Print Tidak bisa dilanjutkan!. Karena Label Tidak akan mencukupi',
						type: 'error',
						timer: 2000,
					  showCancelButton: false,
					  showConfirmButton: false
					});

			}else{
				if (parseFloat($("#counter").val())==0){
					$("#button").attr('disabled',true);
				}else{
					$("#button").attr('disabled',false);
				}
			}


		}
</script>
