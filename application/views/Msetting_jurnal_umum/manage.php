<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}msetting_jurnal_umum" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('msetting_jurnal_umum/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>	
			
			
			<div class="form-group">
				<label class="col-md-3 control-label" ><?=text_primary('Batas Waktu Batal Jurnal Depresiasi')?></label>
				
				<div class="col-md-4">
					<input type="text" style="width: 100%"  class="form-control number" id="batas_batal_dep" placeholder="0" name="batas_batal_dep" value="<?=$batas_batal_dep?>">
				</div>
				
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" ><?=text_primary('Batas Waktu Batal Jurnal Penyesesuaian')?></label>
				
				<div class="col-md-4">
					<input type="text" style="width: 100%"  class="form-control number" id="batas_batal_pen" placeholder="0" name="batas_batal_pen" value="<?=$batas_batal_pen?>">
				</div>
				<div class="col-md-2">
					<button type="button" class="btn btn-sm btn-success" tabindex="8" id="btn_update" title="Update"><i class="fa fa-save"></i> UPDATE</button>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-1 control-label"><h4><?=text_success('Batas Waktu Batal Jurnal Rekonsiliasi')?></h4></label>
				
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<table id="tabel_rekon" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 0%;">#</th>															
								<th style="width: 20%;">Jenis Transaksi</th>															
								<th style="width: 25%;">Batas Waktu </th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td></td>								
								<td>
									<input type="text" style="width: 100%"  class="form-control" id="xnama" placeholder="Jenis" name="xnama" value="">										
								</td>								
								
								<td>
									<input type="text" style="width: 100%"  class="form-control number" id="xbatas" placeholder="Batas Waktu Batal" name="xbatas" value="">
									<input type="hidden" style="width: 100%" readonly class="form-control number" id="iddet" placeholder="Batas Waktu Batal" name="iddet" value="">
								</td>	
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_rekon" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_rekon" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						
				</table>
				</div>
			</div>
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		// load_unit();
		load_rekon();
		clear_input_rekon();

	});	
	$(document).on("click","#btn_update",function(){
		if (validate_update()==false)return false;
		var batas_batal_dep=$("#batas_batal_dep").val();
		var batas_batal_pen=$("#batas_batal_pen").val();
		$("#tmp_batas_batal_pen").val(batas_batal_pen)
		$.ajax({
			url: '{site_url}msetting_jurnal_umum/update',
			type: 'POST',
			data: {
				batas_batal_dep:batas_batal_dep,batas_batal_pen:batas_batal_pen
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					// $("#batas_batal_pen").val($("#tmp_batas_batal_pen").val()).trigger('change');
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function validate_update()
	{
		
		if ($("#batas_batal_dep").val()=='' || $("#batas_batal_dep").val()=='0'){
			sweetAlert("Maaf...", "Batas Waktu Depresiasi Harus diisi", "error");
			return false;
		}
		if ($("#batas_batal_pen").val()=='' || $("#batas_batal_pen").val()=='0'){
			sweetAlert("Maaf...", "Batas Waktu Batal Penyesesuaian Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	
	
	function validate_add_rekon()
	{
		
		
		if ($("#xnama").val()==''){
			sweetAlert("Maaf...", "Jenis Harus diisi", "error");
			return false;
		}
		if ($("#xbatas").val()=='0' || $("#xbatas").val()==''){
			sweetAlert("Maaf...", "Batas Waktu Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_rekon(){
		$("#xnama").val('');		
		$("#xbatas").val('');		
		$("#iddet").val('');		
	}
	$(document).on("click","#simpan_rekon",function(){
		if (validate_add_rekon()==false)return false;
		var iddet=$("#iddet").val();
		var xnama=$("#xnama").val();
		var xbatas=$("#xbatas").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_umum/simpan_rekon',
			type: 'POST',
			data: {
				iddet:iddet,xnama:xnama,xbatas: xbatas
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_rekon').DataTable().ajax.reload( null, false );
					clear_input_rekon();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	function hapus_rekon($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_umum/hapus_rekon/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_rekon').DataTable().ajax.reload( null, false );
						clear_input_rekon();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_rekon",function(){
		
		clear_input_rekon();
	});
	$(document).on("click",".edit",function(){
		var table = $('#tabel_rekon').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		$("#iddet").val(table.cell(tr,0).data());
		$("#xnama").val(table.cell(tr,1).data());
		$("#xbatas").val(table.cell(tr,2).data());

	});
	function load_rekon(){
		var idlogic=$("#id").val();
		$('#tabel_rekon').DataTable().destroy();
		var table = $('#tabel_rekon').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_umum/load_rekon/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	
	
</script>
