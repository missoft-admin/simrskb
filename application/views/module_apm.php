<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>{title_atas}</title>

  <meta name="description" content="Sistem Rumah Sakit RSKB Halmahera">
  <meta name="author" content="Sinergi SIMRS">
  <meta name="robots" content="noindex, nofollow">

  <link rel="apple-touch-icon" sizes="57x57" href="{img_path}favicons/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="{img_path}favicons/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="{img_path}favicons/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="{img_path}favicons/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="{img_path}favicons/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="{img_path}favicons/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="{img_path}favicons/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="{img_path}favicons/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="{img_path}favicons/apple-icon-180x180.png">

  <link rel="icon" type="image/png" sizes="192x192" href="{img_path}favicons/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="{img_path}favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="{img_path}favicons/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="{img_path}favicons/favicon-16x16.png">

  <link rel="manifest" href="{img_path}favicons/manifest.json">

  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="{img_path}favicons/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <link rel="stylesheet" href="{plugins_path}bootstrap-datepicker/bootstrap-datepicker3.min.css">
  <link rel="stylesheet" href="{plugins_path}bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
  <link rel="stylesheet" href="{plugins_path}bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="{plugins_path}select2/select2.min.css">
  <link rel="stylesheet" href="{plugins_path}select2/select2-bootstrap.min.css">
  <link rel="stylesheet" href="{plugins_path}jquery-auto-complete/jquery.auto-complete.min.css">
  <link rel="stylesheet" href="{plugins_path}ion-rangeslider/css/ion.rangeSlider.min.css">
  <link rel="stylesheet" href="{plugins_path}ion-rangeslider/css/ion.rangeSlider.skinHTML5.min.css">
  <link rel="stylesheet" href="{plugins_path}dropzonejs/dropzone.min.css">
  <link rel="stylesheet" href="{plugins_path}jquery-tags-input/jquery.tagsinput.min.css">

  <link rel="stylesheet" href="{js_path}plugins/datatables/jquery.dataTables.min.css">
  <link rel="stylesheet" href="{js_path}plugins/slick/slick.min.css">
  <link rel="stylesheet" href="{js_path}plugins/slick/slick-theme.min.css">
  <link rel="stylesheet" href="{plugins_path}jquery-auto-complete/jquery.auto-complete.min.css">
  <script type="text/javascript" src="{js_path}core/jquery.min.js"></script>

  <link rel="stylesheet" href="{js_path}plugins/sweetalert2/sweetalert2.min.css">

  <link rel="stylesheet" href="{ajax}datatables.net-bs4/css/dataTables.bootstrap4.css">
  <script src="{ajax}datatables.net-bs4/jquery.dataTables.min.js"></script>
  <script src="{ajax}datatable.js"></script>

  <link rel="stylesheet" href="{css_path}bootstrap.min.css">
  <link rel="stylesheet" href="{css_path}fonts.googleapis.com.css">
  <link rel="stylesheet" id="css-main" href="{css_path}oneui.css">
  <link rel="stylesheet" href="{css_path}ionicons.min.css">

  <script src="{plugins_path}bootstrap-select/bootstrap-select.min.js"></script>
  <link rel="stylesheet" href="{plugins_path}bootstrap-select/bootstrap-select.css">

  <link rel="stylesheet" href="{js_path}plugins/summernote/summernote.min.css">
  <link rel="stylesheet" href="{js_path}plugins/simplemde/simplemde.min.css">

  <link rel="stylesheet" id="css-theme" href="{css_path}themes/modern.min.css">
  <link rel='stylesheet' href='<?=$assets_path?>css/style-custom.css' type='text/css' media='all' />
  <script src="{plugins_path}autoNumeric/autoNumeric.min.js"></script>
  <link rel='stylesheet' href='<?=$css_path?>video/video-js.css' type='text/css' media='all' />
    <link rel='stylesheet' href='<?=$css_path?>video/video-js-fantasy.css' type='text/css' media='all' />
    
  <style>
	.rata-tengah {
	  vertical-align: middle;
	}
  </style>
</head>
<script>
	var site_pat='localhost';
</script>
<body>
  <?php $menu = $this->uri->segment(1); ?>
    <main id="main-container">
		<div id="cover-spin"></div>
        <?php $this->load->view($content);?>
    </main>

<? if (count($list_running)>0){ ?>
		<div class="acme-news-ticker" style="z-index: 100000;">
			<div class="acme-news-ticker-label">Flash Info :</div>
			<div class="acme-news-ticker-box">
				<ul class="my-news-ticker">
					<?foreach($list_running as $row){?>
						<li><i class="si si-info fa-2x text-danger"></i></li>
						<li><a href="javascript:void(0)" class="h3 "><?=$row->isi?></a></li>
					<?}?>
				</ul>
			</div>
			<!--
			<div class="acme-news-ticker-controls acme-news-ticker-vertical-controls">
			
				<button class="acme-news-ticker-arrow acme-news-ticker-prev"></button>
				<button class="acme-news-ticker-arrow acme-news-ticker-next"></button>
			</div> -->
		</div>
<?}?>
  <link rel="stylesheet" href="{css_path}running/ticker_style/style.css">
  <script type="text/javascript" src="{js_path}running/acmeticker.js"></script>
  <script type="text/javascript" src="{js_path}video/video.min.js"></script>
	<script type="text/javascript" src="{js_path}video/youtube.min.js"></script>
  <script>
	jQuery(document).ready(function ($) {
        var options = {};
		$('.my-news-ticker').AcmeTicker({
			type:'marquee',/*vertical/horizontal/Marquee/type*/
			direction: 'left',/*up/down/left/right*/
			speed:0.05,/*true/false/number*/ /*For vertical/horizontal 600*//*For marquee 0.05*//*For typewriter 50*/
			controls: {
				// prev: $('.acme-news-ticker-prev'),/*Can be used for vertical/horizontal/typewriter*//*not work for marquee*/
				// next: $('.acme-news-ticker-next'),/*Can be used for vertical/horizontal/typewriter*//*not work for marquee*/
				// toggle: $('.acme-news-ticker-pause')/*Can be used for vertical/horizontal/marquee/typewriter*/
			}
		});
        
            
        // setTimeout(function() {$( ".telpfixed" ).effect( 'shake', options, 2000,callback  );},3000);
        
        // function callback() {
          // setTimeout(function() {
             // $( ".telpfixed" ).effect( 'shake', options, 2000,callback  );
          // }, 60000 );
        // };
    
    })
	
	</script> 
	
  <script src="{js_path}core/bootstrap.min.js"></script>
  <script src="{js_path}core/jquery.slimscroll.min.js"></script>
  <script src="{js_path}core/jquery.scrollLock.min.js"></script>
  <script src="{js_path}core/jquery.appear.min.js"></script>
  <script src="{js_path}core/jquery.countTo.min.js"></script>
  <script src="{js_path}core/jquery.placeholder.min.js"></script>
  <script src="{js_path}core/js.cookie.min.js"></script>
  <script src="{js_path}app.js"></script>

  <script src="{plugins_path}bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  <script src="{plugins_path}bootstrap-datetimepicker/moment.min.js"></script>
  <script src="{plugins_path}bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
  <script src="{plugins_path}bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
  <script src="{plugins_path}bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
  <script src="{plugins_path}select2/select2.full.min.js"></script>
  <script src="{plugins_path}masked-inputs/jquery.maskedinput.min.js"></script>
  <script src="{plugins_path}jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
  <script src="{plugins_path}jquery-auto-complete/jquery.auto-complete.min.js"></script>
  <script src="{plugins_path}ion-rangeslider/js/ion.rangeSlider.min.js"></script>
  <script src="{plugins_path}dropzonejs/dropzone.min.js"></script>
  <script src="{plugins_path}jquery-tags-input/jquery.tagsinput.min.js"></script>
  <script src="{plugins_path}datatables/jquery.dataTables.min.js"></script>
  <script src="{plugins_path}slick/slick.min.js"></script>
  <script src="{plugins_path}chartjs/Chart.min.js"></script>
  <script src="{plugins_path}custom-file-input/custom-file-input.js"></script>

  <script src="{plugins_path}bootstrap-notify/bootstrap-notify.min.js"></script>
  <script src="{plugins_path}sweetalert2/es6-promise.auto.min.js"></script>
  <script src="{plugins_path}sweetalert2/sweetalert2.min.js"></script>

  <script src="{js_path}plugins/summernote/summernote.min.js"></script>
  <script src="{js_path}plugins/ckeditor/ckeditor.js"></script>
  <script src="{js_path}plugins/simplemde/simplemde.min.js"></script>
  <script src="{js_path}pages/base_forms_pickers_more.js"></script>
  <script src="{js_path}pages/base_tables_datatables.js"></script>
 
</body>

</html>