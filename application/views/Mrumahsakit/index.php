<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
	<?php if (UserAccesForm($user_acces_form,array('29'))){ ?>
		<ul class="block-options">
			<li>
				<a href="{base_url}mrumahsakit/create" class="btn"><i class="fa fa-plus"></i></a>
			</li>
		</ul>
		<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
	<?php if (UserAccesForm($user_acces_form,array('28'))){ ?>
		<hr style="margin-top:0px">
		<div class="row">
			<?php echo form_open('mrumahsakit/filter','class="form-horizontal" id="form-work"') ?>
			<div class="col-md-4">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="idruangan">Tipe</label>
					<div class="col-md-8">
						<select name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#" <?=($idtipe == '#' ? 'selected' : '')?>>Semua Tipe</option>
							<option value="1" <?=($idtipe == 1 ? 'selected' : '')?>>Klinik</option>
							<option value="2" <?=($idtipe == 2 ? 'selected' : '')?>>Rumah Sakit</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="idruangan">Kategori</label>
					<div class="col-md-8">
						<select name="idkategori" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#" <?=($idkategori == '#' ? 'selected' : '')?>>Semua Kategori</option>
							<option value="1" <?=($idkategori == 1 ? 'selected' : '')?>>Berekanan</option>
							<option value="2" <?=($idkategori == 2 ? 'selected' : '')?>>Tidak Berekanan</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
		<hr style="margin-top:10px">
		<?}?>
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Tipe</th>
					<th>Kategori</th>
					<th>Persentase</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function() {
		BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
			"autoWidth": false,
			"pageLength": 10,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}mrumahsakit/getIndex/' + "<?=$this->uri->segment(2)?>",
				type: "POST",
				dataType: 'json'
			},
			"columnDefs": [{
					"width": "10%",
					"targets": 0,
					"orderable": true
				},
				{
					"width": "30%",
					"targets": 1,
					"orderable": true
				},
				{
					"width": "20%",
					"targets": 2,
					"orderable": true
				},
				{
					"width": "15%",
					"targets": 3,
					"orderable": true
				},
				{
					"width": "5%",
					"targets": 4,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 4,
					"orderable": true
				}
			]
		});
	});
</script>
