<button type="button" data-toggle="modal" data-target="#modal_tarif_laboratorium" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Cari Tarif</button><br><br>
<div class="table-responsive">
<table class="table table-bordered table-striped" id="mtarif_laboratorium" style="width: 100%;">
    <thead>
        <tr>
            <th style="width: 15%;">Nama Tarif</th>	
            <th style="width: 10%;">Jasa Sarana</th>								
            <th style="width: 10%;">Jasa Pelayanan</th>	
            <th style="width: 10%;">Jasa BHP</th>	
            <th style="width: 10%;">Biaya Perawatan</th>	
            <th style="width: 10%;">Aksi</th>
        </tr>
    </thead>
    <tbody></tbody>							
</table>
</div>