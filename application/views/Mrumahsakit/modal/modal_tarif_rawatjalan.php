<div class="modal fade in black-overlay" id="modal_tarif_rawatjalan" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 50%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">PENCARIAN TARIF RAJAL</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="idpath_rajal">Header</label>
									<div class="col-md-9">
										<select name="idpath_rajal" id="idpath_rajal" class="js-select2 form-control" style="width: 100%;" data-placeholder=" - All Kategori - ">
											<option value="#">- All -</option>
											<?php foreach ($list_level0 as $row) { ?>
												<option value="<?=$row->path; ?>"><?=TreeView($row->level, $row->nama)?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="idpath_rajal">Jenis</label>
									<div class="col-md-9">
										<input type="text" style="width: 100%"  class="form-control" id="nama_rajal" placeholder="Nama Layanan" name="nama_rajal" value="">									
									</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="btn_cari_rajal"></label>
									<div class="col-md-9">
										<button class="btn btn-sm btn-success" id="btn_cari_rajal" type="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-search"></i> Cari</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<table id="tableTarifRawatJalan" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 30%;">NAMA TARIF</th>
								<th style="width: 10%;">JASA SARANA</th>
								<th style="width: 10%;">JASA PELAYANAN</th>
								<th style="width: 10%;">BHP</th>
								<th style="width: 10%;">BIAYA PERAWATAN</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<br>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).on('click', '#btn_cari_rajal', function() {
		loadTarifRawatJalan();
	});

	function loadTarifRawatJalan(){
		var idpath = $("#idpath_rajal").val();
		var namatarif = $("#nama_rajal").val();

		$('#tableTarifRawatJalan').DataTable().destroy();
		$('#tableTarifRawatJalan').DataTable({
            "autoWidth": false,
            "searching": false,
            "lengthChange": false,
            "serverSide": true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,		
			"columnDefs": [
				{ "targets": [0], className: "text-left" },
				{ "targets": [1, 2, 3, 4], className: "text-center" },
				{ "targets": [5, 6], className: "hidden" },
				{ "width": "30%", "targets": 0 },
				{ "width": "10%", "targets": 1 },
				{ "width": "10%", "targets": 2 },
				{ "width": "10%", "targets": 3 },
				{ "width": "10%", "targets": 4 },
				{ "width": "10%", "targets": 5 },
				{ "width": "10%", "targets": 6 },
			],
            ajax: { 
                url: '{site_url}mrumahsakit/getTarifRawatJalan',
                type: "POST",
                dataType: 'json',
				data : {
					namatarif: namatarif,
					idpath: idpath
				}
            }
        });
	}
</script>
