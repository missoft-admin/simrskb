<div class="modal fade in black-overlay" id="modal_tarif_radiologi" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 50%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">PENCARIAN TARIF RADIOLOGI</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_rad">Tipe</label>
									<div class="col-md-9">
										<select name="idtipe_rad" id="idtipe_rad" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
											<option value="#">Pilih Opsi</option>
											<option value="1">X-Ray</option>
											<option value="2">USG</option>
											<option value="3">CT Scan</option>
											<option value="4">MRI</option>
											<option value="5">BMD</option>
										</select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_rad">Nama</label>
									<div class="col-md-9">
										<input type="text" style="width: 100%"  class="form-control" id="nama_rad" placeholder="Nama Tarif" name="nama_rad" value="">									
									</select>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 10px;">
									<label class="col-md-3 control-label" for="jenis_rad">Header</label>
									<div class="col-md-9">
										<select name="idpath_rad" id="idpath_rad" class="js-select2 form-control" style="width: 100%;" data-placeholder=" - All Kategori - ">
											<option value="#" selected>- All -</option>
											
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="btn_cari_rad"></label>
									<div class="col-md-9">
										<button class="btn btn-sm btn-success" id="btn_cari_rad" type="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-search"></i> Cari</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<table id="tableTarifRadiologi" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 30%;">NAMA TARIF</th>
								<th style="width: 10%;">JASA SARANA</th>
								<th style="width: 10%;">JASA PELAYANAN</th>
								<th style="width: 10%;">BHP</th>
								<th style="width: 10%;">BIAYA PERAWATAN</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<br>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#idtipe_rad").change(function(){
		if ($(this).val() != ''){
			$.ajax({
				url: '{site_url}mtarif_radiologi/find_headparent/' + $(this).val(),
				dataType: "json",
				success: function(data) {
					$('#idpath_rad').find('option').remove().end().append('<option value="#" selected>- All -</option>').val('#').trigger("liszt:updated");
					$('#idpath_rad').append(data.detail);
				}
			});
		}
	});

	$(document).on('click', '#btn_cari_rad', function() {
		loadTarifRadiologi();
	});
	
	function loadTarifRadiologi(){
		var idtipe = $("#idtipe_rad").val();
		var namatarif = $("#nama_rad").val();
		var idpath = $("#idpath_rad").val();
		
		$('#tableTarifRadiologi').DataTable().destroy();
		$('#tableTarifRadiologi').DataTable({
            "autoWidth": false,
            "searching": false,
            "lengthChange": false,
            "serverSide": true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,		
			"columnDefs": [
				{ "targets": [0], className: "text-left" },
				{ "targets": [1, 2, 3, 4], className: "text-center" },
				{ "targets": [5, 6], className: "hidden" },
				{ "width": "30%", "targets": 0 },
				{ "width": "10%", "targets": 1 },
				{ "width": "10%", "targets": 2 },
				{ "width": "10%", "targets": 3 },
				{ "width": "10%", "targets": 4 },
				{ "width": "10%", "targets": 5 },
				{ "width": "10%", "targets": 6 },
			],
            ajax: { 
                url: '{site_url}mrumahsakit/getTarifRadiologi',
                type: "POST",
                dataType: 'json',
				data : {
					idtipe: idtipe,
					namatarif: namatarif,
					idpath: idpath
				}
            }
        });
	}
</script>
