<div class="modal fade in black-overlay" id="modal_tarif_fisioterapi" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 50%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">PENCARIAN TARIF FISIOTERAPI</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="jenis_fis">Header</label>
									<div class="col-md-9">
										<select name="idpath_fis" id="idpath_fis" class="js-select2 form-control" style="width: 100%;" data-placeholder=" - All Kategori - ">
											<option value="#" selected>- All -</option>
											<?php foreach ($list_level0_fis as $row) { ?>
												<option value="<?=$row->path; ?>"><?=TreeView($row->level, $row->nama)?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_fis">Nama</label>
									<div class="col-md-9">
										<input type="text" style="width: 100%"  class="form-control" id="nama_fis" placeholder="Nama Tarif" name="nama_fis" value="">									
									</select>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 10px;">
									<label class="col-md-3 control-label" for="jenis_fis"></label>
									<div class="col-md-9">
										
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="btn_cari_fis"></label>
									<div class="col-md-9">
										<button class="btn btn-sm btn-success" id="btn_cari_fis" type="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-search"></i> Cari</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<table id="tableTarifFisioterapi" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 30%;">NAMA TARIF</th>
								<th style="width: 10%;">JASA SARANA</th>
								<th style="width: 10%;">JASA PELAYANAN</th>
								<th style="width: 10%;">BHP</th>
								<th style="width: 10%;">BIAYA PERAWATAN</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<br>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).on('click', '#btn_cari_fis', function() {
		loadTarifFisioterapi();
	});
	
	function loadTarifFisioterapi(){
		var namatarif = $("#nama_fis").val();
		var idpath = $("#idpath_fis").val();

		$('#tableTarifFisioterapi').DataTable().destroy();
		$('#tableTarifFisioterapi').DataTable({
            "autoWidth": false,
            "searching": false,
            "lengthChange": false,
            "serverSide": true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,		
			"columnDefs": [
				{ "targets": [0], className: "text-left" },
				{ "targets": [1, 2, 3, 4], className: "text-center" },
				{ "targets": [5, 6], className: "hidden" },
				{ "width": "30%", "targets": 0 },
				{ "width": "10%", "targets": 1 },
				{ "width": "10%", "targets": 2 },
				{ "width": "10%", "targets": 3 },
				{ "width": "10%", "targets": 4 },
				{ "width": "10%", "targets": 5 },
				{ "width": "10%", "targets": 6 },
			],
            ajax: { 
                url: '{site_url}mrumahsakit/getTarifFisioterapi',
                type: "POST",
                dataType: 'json',
				data : {
					namatarif: namatarif,
					idpath: idpath
				}
            }
        });
	}
</script>
