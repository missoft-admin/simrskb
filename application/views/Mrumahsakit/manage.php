<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mrumahsakit/index" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?php echo form_open('mrumahsakit/save', 'class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="idtipe">Tipe</label>
				<div class="col-md-8">
					<select class="js-select2 form-control" id="idtipe" name="idtipe" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($idtipe == 1 ? 'selected="selected"' : '')?>>Klinik</option>
						<option value="2" <?=($idtipe == 2 ? 'selected="selected"' : '')?>>Rumah Sakit</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Nama</label>
				<div class="col-md-8">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="idkategori">Kategori</label>
				<div class="col-md-8">
					<select class="js-select2 form-control" id="idkategori" name="idkategori" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($idkategori == 1 ? 'selected="selected"' : '')?>>Berekanan</option>
						<option value="2" <?=($idkategori == 2 ? 'selected="selected"' : '')?>>Tidak Berekanan</option>
					</select>
				</div>
			</div>
			<div class="form-group" id="form_jenis_berekanan" style="display:none">
				<label class="col-md-2 control-label" for="jenis_berekanan">Jenis Berekanan</label>
				<div class="col-md-8">
					<select class="js-select2 form-control" id="jenis_berekanan" name="jenis_berekanan" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($jenis_berekanan == 1 ? 'selected="selected"' : '')?>>Persentase</option>
						<option value="2" <?=($jenis_berekanan == 2 ? 'selected="selected"' : '')?>>Flat Rate</option>
					</select>
				</div>
			</div>
			<div class="form-group" id="form_persentase" style="display:none">
				<label class="col-md-2 control-label" for="persentase">Persentase</label>
				<div class="col-md-8">
					<input type="text" class="form-control" id="persentase" placeholder="Persentase" name="persentase" value="{persentase}">
				</div>
			</div>
			<div class="form-group" id="form_rate_tarif" style="display:none">
				<label class="col-md-2 control-label" for="ratetarif">Rate Tarif</label>
				<div class="col-md-8">
					<input type="text" class="form-control number" id="ratetarif" placeholder="Rate Tarif" name="ratetarif" value="{ratetarif}">
				</div>
			</div>
			<div class="form-group" id="form_berdasarkan" style="display:none">
				<label class="col-md-2 control-label" for="berdasarkan">Berdasarkan</label>
				<div class="col-md-8">
					<select class="js-select2 form-control" id="berdasarkan" name="berdasarkan" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($berdasarkan == 1 ? 'selected="selected"' : '')?>>Total Tarif</option>
						<option value="2" <?=($berdasarkan == 2 ? 'selected="selected"' : '')?>>Tarif Tertentu</option>
					</select>
				</div>
			</div>
			<div class="form-group" id="form_tentukan_tarif" style="display:none">
				<label class="col-md-2 control-label" for="tentukantarif">Tentukan Tarif</label>
				<div class="col-md-8">
					<select class="js-select2 form-control" id="tentukantarif" name="tentukantarif" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($tentukantarif == '1' ? 'selected="selected"' : '')?>>Ya</option>
						<option value="0" <?=($tentukantarif == '0' ? 'selected="selected"' : '')?>>Tidak</option>
					</select>
				</div>
			</div>
			<div id="form_tarif_tertentu" style="display:none">
				<hr>
				<div class="form-group">
					<label class="col-md-2 control-label" for="">Alkes Poli/IGD</label>
					<div class="col-md-8">
						<select class="js-select2 form-control" name="alkes_poli" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1" <?=($alkes_poli == '1' ? 'selected="selected"' : '')?>>Ya</option>
							<option value="0" <?=($alkes_poli == '0' ? 'selected="selected"' : '')?>>Tidak</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-2 control-label" for="">Obat Poli/IGD</label>
					<div class="col-md-8">
						<select class="js-select2 form-control" name="obat_poli" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1" <?=($obat_poli == '1' ? 'selected="selected"' : '')?>>Ya</option>
							<option value="0" <?=($obat_poli == '0' ? 'selected="selected"' : '')?>>Tidak</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-2 control-label" for="">Alkes Farmasi</label>
					<div class="col-md-8">
						<select class="js-select2 form-control" name="alkes_farmasi" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1" <?=($alkes_farmasi == '1' ? 'selected="selected"' : '')?>>Ya</option>
							<option value="0" <?=($alkes_farmasi == '0' ? 'selected="selected"' : '')?>>Tidak</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-2 control-label" for="">Obat Farmasi</label>
					<div class="col-md-8">
						<select class="js-select2 form-control" name="obat_farmasi" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1" <?=($obat_farmasi == '1' ? 'selected="selected"' : '')?>>Ya</option>
							<option value="0" <?=($obat_farmasi == '0' ? 'selected="selected"' : '')?>>Tidak</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-2 control-label" for="">Tarif Laboratorium</label>
					<div class="col-md-8">
						<?php $this->load->view('Mrumahsakit/detail/tarif_laboratorium'); ?>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-2 control-label" for="">Tarif Radiologi</label>
					<div class="col-md-8">
						<?php $this->load->view('Mrumahsakit/detail/tarif_radiologi'); ?>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-2 control-label" for="">Tarif Fisio</label>
					<div class="col-md-8">
						<?php $this->load->view('Mrumahsakit/detail/tarif_fisioterapi'); ?>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-2 control-label" for="">Tarif Rawat Jalan</label>
					<div class="col-md-8">
						<?php $this->load->view('Mrumahsakit/detail/tarif_rawatjalan'); ?>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mrumahsakit" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_hidden('idtemp', $idtemp); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
        $(".number").number(true, 0, '.', ',');
        $("#persentase").number(true, 2, '.', ',');

		toggleForm();
		getTarifTertentuRS();

		$("#idkategori, #jenis_berekanan, #berdasarkan, #tentukantarif").change(function(){
			toggleForm();
		});

		$("#persentase").keyup(function(){
			if ($(this).val() < 0) {
				$("#persentase").val(0);
			} else if ($(this).val() > 100) {
				$("#persentase").val(100);
			} else {
				if ($(this).val() == '00') {
					$("#persentase").val(0);
				}
			}
		});
	});

	function toggleForm() {
		const BEREKANAN = 1;
		const PERSENTASE = 1;
		const FLAT_RATE = 2;
		const TARIF_TERTENTU = 2;
		const YA = 1;

		const idkategori = $("#idkategori").val();
		const jenis_berekanan = $("#jenis_berekanan").val();
		const berdasarkan = $("#berdasarkan").val();
		const tentukantarif = $("#tentukantarif").val();

		if (idkategori == BEREKANAN) {
			$("#form_jenis_berekanan").show();

			if (jenis_berekanan == PERSENTASE) {
				$("#form_persentase").show();
				$("#form_berdasarkan").show();
				$("#form_tentukan_tarif").show();

				if (berdasarkan == TARIF_TERTENTU && tentukantarif == YA) {
					$("#form_tarif_tertentu").show();
				} else {
					$("#form_tarif_tertentu").hide();
				}
			} else if (jenis_berekanan == FLAT_RATE) {
				$("#form_persentase").hide();
				$("#form_berdasarkan").hide();
				$("#form_rate_tarif").show();
				$("#form_tentukan_tarif").show();

				if (tentukantarif == YA) {
					$("#form_tarif_tertentu").show();
				} else {
					$("#form_tarif_tertentu").hide();
				}
			}
		} else {
			$("#form_jenis_berekanan").hide();
			$("#form_persentase").hide();
			$("#form_rate_tarif").hide();
			$("#form_berdasarkan").hide();
			$("#form_tentukan_tarif").hide();
			$("#form_tarif_tertentu").hide();
		}
	}

	$(document).on('click', '.jasasarana, .jasapelayanan, .bhp, .biayaperawatan', function() {
		const idrumahsakit = '{idtemp}';
		const idtarif = $(this).closest('tr').find('td:eq(5)').html();
		const namatarif = $(this).closest('tr').find('td:eq(0)').html();
		const reference_table = $(this).closest('tr').find('td:eq(6)').html();
		const jasasarana = $(this).closest('tr').find('td:eq(1) label input')[0].checked;
		const jasapelayanan = $(this).closest('tr').find('td:eq(2) label input')[0].checked;
		const bhp = $(this).closest('tr').find('td:eq(3) label input')[0].checked;
		const biayaperawatan = $(this).closest('tr').find('td:eq(4) label input')[0].checked;

		$.ajax({
			url: '{site_url}mrumahsakit/saveTarifTertentu',
			method: "POST",
			data: {
				idrumahsakit: idrumahsakit,
				reference_table: reference_table,
				idtarif: idtarif,
				namatarif: namatarif,
				jasasarana: (jasasarana ? 1 : 0),
				jasapelayanan: (jasapelayanan ? 1 : 0),
				bhp: (bhp ? 1 : 0),
				biayaperawatan: (biayaperawatan ? 1 : 0),
			},
			success: function(data) {
				getTarifTertentuRS();
			}
		});
	});

	function getTarifTertentuRS() {
		$('#mtarif_laboratorium tbody').empty();
		$('#mtarif_radiologi tbody').empty();
		$('#mtarif_fisioterapi tbody').empty();
		$('#mtarif_rawatjalan tbody').empty();

		$.ajax({
			url: '{site_url}mrumahsakit/getTarifTertentu/' + '{idtemp}',
			dataType: 'json',
			success: function(data) {
				data.map((item) => {
					$(`#${item.reference_table} tbody`).append(`
						<tr>
							<td class=" text-left">${item.namatarif}</td>
							<td class=" text-center">
								<label class="css-input css-checkbox css-checkbox-primary">
									<input type="checkbox" ${item.jasasarana == '1' ? 'checked' : ''} class="jasasarana">
									<span></span>
								</label>
							</td>
							<td class=" text-center">
								<label class="css-input css-checkbox css-checkbox-primary">
									<input type="checkbox" ${item.jasapelayanan == '1' ? 'checked' : ''} class="jasapelayanan">
									<span></span>
								</label>
							</td>
							<td class=" text-center">
								<label class="css-input css-checkbox css-checkbox-primary">
									<input type="checkbox" ${item.bhp == '1' ? 'checked' : ''} class="bhp">
									<span></span>
								</label>
							</td>
							<td class=" text-center">
								<label class="css-input css-checkbox css-checkbox-primary">
									<input type="checkbox" ${item.biayaperawatan == '1' ? 'checked' : ''} class="biayaperawatan">
									<span></span>
								</label>
							</td>
							<td hidden>${item.idtarif}</td>
							<td hidden>${item.reference_table}</td>
							<td class=" text-center">
								<div class="btn-group">
									<button class="btn btn-xs btn-danger" type="button" title="Hapus" onclick="removeTarif(${item.id})">
										<i class="fa fa-trash-o"></i>
									</button>
								</div>
							</td>
						</tr>
					`);
				});
			}
		});
	}

	function removeTarif(id) {
		$.ajax({
			url: '{site_url}mrumahsakit/removeTarifTertentu/' + id,
			success: function(data) {
				getTarifTertentuRS();
			}
		});
	}
</script>

<?php $this->load->view('Mrumahsakit/modal/modal_tarif_laboratorium'); ?>
<?php $this->load->view('Mrumahsakit/modal/modal_tarif_radiologi'); ?>
<?php $this->load->view('Mrumahsakit/modal/modal_tarif_fisioterapi'); ?>
<?php $this->load->view('Mrumahsakit/modal/modal_tarif_rawatjalan');
