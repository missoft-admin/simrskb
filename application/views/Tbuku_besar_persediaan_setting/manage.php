<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}tbuku_besar_persediaan_setting" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('tbuku_besar_persediaan_setting/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>	
			
			<div class="form-group">
				<div class="col-md-12">
					<div class="table-responsive">
						<table id="tabel_beli" class="table table-striped table-bordered" style="margin-bottom: 0;">
							<thead>
								<tr>
									<th style="width: 0%;">#</th>															
									<th style="width: 20%;">Tipe </th>															
									<th style="width: 20%;">Kategori</th>								
									<th style="width: 25%;">Barang</th>								
									<th style="width: 25%;">Unit</th>
									<th style="width: 10%;">Actions</th>
								</tr>
								<tr>								
									<td></td>
									<td>
										<select name="idtipe" id="idtipe" style="width: 100%" id="step" class="js-select2 form-control input-sm">										
											<option value="#" selected>- Pilih Tipe -</option>																			
											<?foreach(get_all('mdata_tipebarang',array(),'id','ASC') as $row){?>
												<option value="<?=$row->id?>"><?=$row->nama_tipe?></option>																				
											<?}?>
										</select>										
									</td>								
									<td>
										<select name="idkategori" id="idkategori" style="width: 100%" class="js-select2 form-control input-sm">										
											<option value="0" selected>- Semua Kategori -</option>																			
										</select>										
									</td>
									<td>
										<select name="idbarang" id="idbarang" style="width: 100%" class="form-control input-sm">										
											<option value="0" selected>- Semua Barang -</option>																			
										</select>										
									</td>
									
									<td>
										<select name="idunit" id="idunit" style="width: 100%" class="js-select2 form-control input-sm idunit">										
											
										</select>
										<input type="hidden" class="form-control" id="id_edit_beli" placeholder="0" name="id_edit_beli" value="">
									</td>	
									
									<td>									
										<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_beli" title="Masukan Item"><i class="fa fa-save"></i></button>
										<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_beli" title="Batalkan"><i class="fa fa-refresh"></i></button>
									</td>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
			
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		// load_unit();
		list_unit();
		load_setting();
		
		clear_input_beli();
		
		list_barang_beli();
		setTimeout(function() {
			$("#idunit_kredit").val($("#tmp_idunit_kredit").val()).trigger('change');
		}, 500);
		// load_setting();
	});	
	
	function list_unit(){
		$.ajax({
			url: '{site_url}tbuku_besar_persediaan_setting/list_unit',
			dataType: "json",
			success: function(data) {
				$(".idunit").empty();
				$('.idunit').append('<option value="#" selected>- Pilih Unit -</option>');
				$('.idunit').append(data.detail);
				
			}
		});		
	}
	
	
	function list_barang_beli(){
		var newOption = new Option('- Semua Barang -', '0', true, true);					
		$("#idbarang").append(newOption);
		$("#idbarang").val("0").trigger('change');
		
		var idtipe=$("#idtipe").val();
		var idkategori=$("#idkategori").val();
		$("#idbarang").select2({
			minimumInputLength: 0,
			noResults: 'Obat Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}tbuku_besar_persediaan_setting/list_barang/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,idtipe: idtipe,idkategori:idkategori,
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.kode+' - '+item.nama,
								id: item.id,
								idtipe : item.idtipe
							}
						})
					};
				}
			}
		});

	}
	$(document).on("change","#idtipe",function(){
		list_kategori_beli($(this).val());
		list_barang_beli();
	});
	$(document).on("change","#idkategori",function(){
		list_barang_beli();
	});
	function list_kategori_beli($id){
		if ($id!='#'){
			$.ajax({
				url: '{site_url}tbuku_besar_persediaan_setting/list_kategori/'+$id,
				dataType: "json",
				success: function(data) {
					$("#idkategori").empty();
					$('#idkategori').append('<option value="0" selected>- Semua Kategori -</option>');
					$('#idkategori').append(data.detail);
				}
			});
		}else{
			$("#idkategori").empty();
			$('#idkategori').append('<option value="0" selected>- Semua Kategori -</option>');
		}		
	}
	function validate_add_beli()
	{
		
		if ($("#idtipe").val()=='#'){
			sweetAlert("Maaf...", "Tipe Harus diisi", "error");
			return false;
		}
		if ($("#idunit").val()=='#'){
			sweetAlert("Maaf...", "No AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_beli(){
		$("#idtipe").val('#').trigger('change');	
		var newOption = new Option('- Semua Barang -', '0', true, true);
					
		$("#idbarang").append(newOption);
		$("#idbarang").val("0").trigger('change');


		// $("#idbarang").val('#').trigger('change');		
		$("#id_edit_beli").val('');		
		$("#idunit").val('#').trigger('change');
		// alert($("#idunit").val());
	}
	$(document).on("click","#simpan_beli",function(){
		if (validate_add_beli()==false)return false;
		var id_edit=$("#id_edit_beli").val();
		var idtipe=$("#idtipe").val();
		var idkategori=$("#idkategori").val();
		var idbarang=$("#idbarang").val();
		var idunit=$("#idunit").val();
		
		$.ajax({
			url: '{site_url}tbuku_besar_persediaan_setting/simpan_beli',
			type: 'POST',
			data: {
				id_edit:id_edit,idtipe:idtipe,
				idkategori: idkategori,idbarang:idbarang,idunit:idunit,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_beli').DataTable().ajax.reload( null, false );
					clear_input_beli();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	function hapus_beli($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}tbuku_besar_persediaan_setting/hapus_beli/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_beli').DataTable().ajax.reload( null, false );
						clear_input_beli();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_beli",function(){
		
		clear_input_beli();
	});
	function load_setting(){
		var idlogic=$("#id").val();
		
		$('#tabel_beli').DataTable().destroy();
		var table = $('#tabel_beli').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}tbuku_besar_persediaan_setting/load_setting/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	
</script>
