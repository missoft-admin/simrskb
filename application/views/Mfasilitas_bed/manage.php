<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mfasilitas_bed" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('mfasilitas_bed/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="row">
					<div class="form-group">
						<label class="col-md-2 control-label" for="nama">Nama Fasilitas Bed</label>
						<div class="col-md-10">
							<input type="text" class="form-control" id="nama" placeholder="Nama Fasilitas Bed" name="nama" value="{nama}">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-2 control-label" for="nama">Bed</label>
						<div class="col-md-10">
							<select id="mbed_id" name="mbed_id[]" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
								<?foreach($list_bed as $r){?>
								<option value="<?=$r->id?>" <?=$r->pilih?>><?=$r->nama?></option>
								<?}?>
								
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-md-2 col-md-offset-2">
							<button class="btn btn-success" type="submit">Simpan</button>
							<a href="{base_url}mfasilitas_bed" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
						</div>
					</div>
					
				
				
			</div>
			<input type="hidden" value="{id}" id="id" name="id" readonly>
			<?php echo form_close() ?>
			<?if ($id){?>
			<div class="row">
				<div class="form-group">
					<label class="col-md-2 control-label" for="nama"></label>
					<div class="col-md-10">
						<form action="{base_url}mfasilitas_bed/upload" enctype="multipart/form-data" class="dropzone" id="image-upload">
							<input type="hidden" value="{id}" id="mfasilitas_id" name="mfasilitas_id" readonly>
							<input type="hidden" value="{nama}" id="keterangan" name="keterangan" readonly>
							<div>
							  <h5>Upload Foto</h5>
							</div>
						</form>
					</div>
				</div>
			</div>
			<?php echo form_open_multipart('mfasilitas_bed/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="row">
				<div class="form-group">
					<label class="col-md-2 control-label" for="nama"></label>
					<div class="col-md-10">
						 <table class="table table-bordered" id="list_file">
							<thead>
								<tr>
									<th width="40%" class="text-center">File</th>
									<th width="25%" class="text-center">Size</th>
									<th width="25%" class="text-center">User</th>
									<th width="10%" class="text-center">X</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
			<?}?>
	</div>

</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var myDropzone ;
	$(document).ready(function(){
		if ($("#id").val()!=''){
			Dropzone.autoDiscover = false;
			myDropzone = new Dropzone(".dropzone", { 
			   autoProcessQueue: true,
			   maxFilesize: 30,
			   acceptedFiles:'image/*' 
			});
			myDropzone.on("complete", function(file) {
			  
			  refresh_image();
			  myDropzone.removeFile(file);
			  
			});
		   Dropzone.options.dropzone = {
				acceptedFiles:'image/*'       
			};
			refresh_image();
		}
	});
	$(document).on("click",".hapus_file",function(){	
		var currentRow=$(this).closest("tr");          
         var id=currentRow.find("td:eq(4)").text();
		 
		 $("#cover-spin").show();
		 $.ajax({
			url: '{site_url}mfasilitas_bed/hapus_file',
			type: 'POST',
			data: {id: id},
			success : function(data) {				
				$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
				currentRow.remove();
				$("#cover-spin").hide();
				// table.ajax.reload( null, false );				
			}
		});
	});
	function refresh_image(){
		// alert($("#mfasilitas_id").val());
		var id=$("#mfasilitas_id").val();
		$('#list_file tbody').empty();
		// alert(id);
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}mfasilitas_bed/refresh_image/'+id,
			dataType: "json",
			success: function(data) {
				$("#cover-spin").hide();
				if (data.detail!=''){
					$('#list_file tbody').empty();
					$("#list_file tbody").append(data.detail);
				}
				// console.log();
				
			}
		});
	}
	
</script>