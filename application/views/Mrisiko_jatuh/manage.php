<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mrisiko_jatuh" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mrisiko_jatuh/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group" style="margin-bottom:10px">
				<label class="col-md-2 control-label" for="nama">Nama</label>
				<div class="col-md-10">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="mrisiko_id" placeholder="mrisiko_id" name="mrisiko_id" value="{id}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group" style="margin-bottom:5px">
				<label class="col-md-2 control-label" for="isi_header">Header</label>
				<div class="col-md-10">
					<textarea class="form-control js-summernote" name="isi_header" id="isi_header" required><?=$isi_header?></textarea>
				</div>
			</div>
			<div class="form-group" style="margin-bottom:5px">
				<label class="col-md-2 control-label" for="isi_footer">Footer</label>
				<div class="col-md-10">
					<textarea class="form-control js-summernote" name="isi_footer" id="isi_footer" required><?=$isi_footer?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="idtipe">Tipe Layanan</label>
				<div class="col-md-10">
					<select name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($idtipe == 1 ? 'selected="selected"':'')?>>Poliklinik</option>
						<option value="2" <?=($idtipe == 2 ? 'selected="selected"':'')?>>IGG</option>
					</select>
				</div>
			</div>
			<?if ($id){?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="">Penilaian</label>
				<div class="col-md-10">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="index_parameter" style=" vertical-align: baseline;">
							<thead>
								<tr>
									<th width="5%">No</th>
									<th width="80%">Deskripsi Parameter</th>
									<th width="15%">Action</th>										   
								</tr>
								<tr>
									<th  style=" vertical-align: top;">#<input class="form-control" type="hidden" id="parameter_id" name="parameter_id" value=""></th>
									<th  style=" vertical-align: top;">
										<textarea class="form-control js-summernote" name="parameter_nama" id="parameter_nama" ></textarea>
									</th>
									
									<th style=" vertical-align: top;">
										<div class="btn-group">
										<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_parameter" name="btn_tambah_parameter"><i class="fa fa-save"></i> Simpan</button>
										<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_parameter()"><i class="fa fa-refresh"></i> Clear</button>
										</div>
									</th>										   
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>					
				</div>
			</div>
			<?}?>
			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mrisiko_jatuh" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>
<div class="modal in" id="modal_tambah" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tambah value</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-horizontal">
									<label for="parameter_add">Parameter :</label><br>
									<label id="parameter_add"></label>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_jawaban" style=" vertical-align: baseline;">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Skor</th>
												<th width="70%">Deskripsi Parameter</th>
												<th width="15%">Action</th>										   
											</tr>
											<tr>
												<th  style=" vertical-align: top;">#
													<input class="form-control" type="hidden" id="parameter_id2" name="parameter_id2" value="">
													<input class="form-control" type="hidden" id="skor_id" name="skor_id" value="">
												</th>
												<th  style=" vertical-align: top;">
													<input type="text" class="form-control number" style="width:100%" id="skor" placeholder="Value Skor" name="skor" value="" >
												</th>
												<th  style=" vertical-align: top;">
													<input type="text" class="form-control" id="deskripsi_nama"  style="width:100%"  placeholder="Deskripsi Parameter" name="deskripsi_nama" value="" >
												</th>
												
												<th style=" vertical-align: top;">
													<div class="btn-group">
													<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_jawaban" name="btn_tambah_jawaban"><i class="fa fa-save"></i> Simpan</button>
													<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_jawaban()"><i class="fa fa-refresh"></i> Clear</button>
													</div>
												</th>										   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>	
								
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){	
	$(".number").number(true,0,'.',',');
	$('.js-summernote').summernote({
		  height: 60,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
	let mrisiko_id=$("#mrisiko_id").val();
	if (mrisiko_id){
		load_parameter();
	}
	
})	
function clear_parameter(){
	$("#parameter_id").val('');
	$('#parameter_nama').summernote('code','');
	
	$("#btn_tambah_parameter").html('<i class="fa fa-save"></i> Save');
	
}
function load_parameter(){
	let mrisiko_id=$("#mrisiko_id").val();
	$('#index_parameter').DataTable().destroy();	
	table = $('#index_parameter').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mrisiko_jatuh/load_parameter', 
                type: "POST" ,
                dataType: 'json',
				data : {
							mrisiko_id:mrisiko_id
					   }
            }
        });
}		
function edit_parameter(parameter_id){
	$("#parameter_id").val(parameter_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mrisiko_jatuh/find_parameter',
		type: 'POST',
		dataType: "JSON",
		data: {id: parameter_id},
		success: function(data) {
			$("#btn_tambah_parameter").html('<i class="fa fa-save"></i> Edit');
			$("#parameter_id").val(data.id);
			$('#parameter_nama').summernote('code',data.parameter_nama);
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_parameter(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Parameter?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mrisiko_jatuh/hapus_parameter',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_parameter').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

$("#btn_tambah_parameter").click(function() {
	let mrisiko_id=$("#mrisiko_id").val();
	let parameter_id=$("#parameter_id").val();
	let parameter_nama=$("#parameter_nama").val();
	
	if (parameter_nama==''){
		sweetAlert("Maaf...", "Tentukan Range Parameter", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mrisiko_jatuh/simpan_parameter', 
		dataType: "JSON",
		method: "POST",
		data : {
				mrisiko_id:mrisiko_id,
				parameter_id:parameter_id,
				parameter_nama:parameter_nama,
				
			},
		complete: function(data) {
			clear_parameter();
			$('#index_parameter').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
$("#btn_tambah_jawaban").click(function() {
	let parameter_id=$("#parameter_id2").val();
	let skor=$("#skor").val();
	let skor_id=$("#skor_id").val();
	let deskripsi_nama=$("#deskripsi_nama").val();
	
	if (skor==''){
		sweetAlert("Maaf...", "Tentukan Skor", "error");
		return false;
	}
	if (deskripsi_nama==''){
		sweetAlert("Maaf...", "Tentukan Deskripsi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mrisiko_jatuh/simpan_jawaban', 
		dataType: "JSON",
		method: "POST",
		data : {
				parameter_id:parameter_id,
				skor_id:skor_id,
				deskripsi_nama:deskripsi_nama,
				skor:skor,
				
			},
		complete: function(data) {
			clear_jawaban();
			load_parameter(); 
			$('#index_jawaban').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
function add_jawaban(parameter_id){
	$("#parameter_id2").val(parameter_id);
	$("#modal_tambah").modal('show');
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mrisiko_jatuh/find_parameter',
		type: 'POST',
		dataType: "JSON",
		data: {id: parameter_id},
		success: function(data) {
			// $("#btn_tambah_parameter").html('<i class="fa fa-save"></i> Edit');
			// $("#parameter_id").val(data.id);
			$("#parameter_add").html(data.parameter_nama);
			// $('#parameter_add').summernote('code',data.parameter_nama);
			$("#cover-spin").hide();
			load_jawaban();
		}
	});
}
function clear_jawaban(){
	$("#skor_id").val('');
	$('#deskripsi_nama').val('');
	$('#skor').val('');
	
	$("#btn_tambah_jawaban").html('<i class="fa fa-save"></i> Save');
	
}
function load_jawaban(){
	let parameter_id=$("#parameter_id2").val();
	$('#index_jawaban').DataTable().destroy();	
	table = $('#index_jawaban').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mrisiko_jatuh/load_jawaban', 
                type: "POST" ,
                dataType: 'json',
				data : {
							parameter_id:parameter_id
					   }
            }
        });
}	
function edit_jawaban(skor_id){
	$("#skor_id").val(skor_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mrisiko_jatuh/find_jawaban',
		type: 'POST',
		dataType: "JSON",
		data: {id: skor_id},
		success: function(data) {
			$("#modal_tambah").modal('show');
			$("#btn_tambah_jawaban").html('<i class="fa fa-save"></i> Edit');
			$("#skor").val(data.skor);
			$("#skor_id").val(data.id);
			$("#deskripsi_nama").val(data.deskripsi_nama);
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_jawaban(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Skor?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mrisiko_jatuh/hapus_jawaban',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_jawaban').DataTable().ajax.reload( null, false ); 
					load_parameter();
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
</script>