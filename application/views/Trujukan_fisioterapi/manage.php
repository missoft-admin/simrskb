<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}trujukan_fisioterapi/index" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?php echo form_open('trujukan_fisioterapi/save','class="form-horizontal" id="form-work" onsubmit="return validate_final()"') ?>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <label class="col-md-2" for="tglpendaftaran" style="margin-top: 5px;">Tanggal</label>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-6">
                            <input tabindex="2" type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" name="tanggalrujukan" value="{tanggalrujukan}" required>
                        </div>
                        <div class="col-md-6">
                            <input tabindex="3" type="text" class="time-datepicker form-control" name="wakturujukan" value="{wakturujukan}" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <label class="col-md-4">No. Rujukan</label>
                <div class="col-md-8">
                    <label><i><?=$norujukan?></i></label>
                </div>
            </div>
        </div>

        <br><br>

        <hr>

        <div class="col-sm-6 col-lg-6">
            <div class="block block-rounded block-bordered">
                <div class="block-header">
                    <h3 class="block-title">Data Pasien</h3>
                </div>
                <div class="block-content">
                    <table class="table">
                        <tr>
                            <th width="30%">No. Medrec</th>
                            <td>
                                <div class="col-sm-12">
                                    <input class="form-control input-sm" type="text" value="<?=$nomedrec?>" readonly="readonly">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Nama Pasien</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="<?=$namapasien?>">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Jenis Kelamin</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="<?=$jeniskelamin?>">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Alamat</th>
                            <td>
                                <div class="col-sm-12">
                                    <textarea class="form-control input-sm" readonly="readonly"><?=$alamat?></textarea>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tanggal Lahir</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="<?=$tanggallahir?>">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Umur</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="<?=$umur_tahun.' Tahun '.$umur_bulan.' Bulan '.$umur_hari.' Hari'?>">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Dokter Perujuk</th>
                            <td>
                                <div class="col-sm-12">
                                    <select id="iddokterperujuk" <?=(UserAccesForm($user_acces_form,array('1034'))?'':'disabled')?> name="iddokterperujuk" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                        <option value="#">Pilih Opsi</option>
                                        <?php foreach ($list_dokterperujuk as $row) { ?>
                                        <option value="<?php echo $row->id; ?>" <?=($iddokterperujuk == $row->id ? 'selected' : '')?>><?php echo $row->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-6">
            <div class="block block-rounded block-bordered">
                <div class="block-content">
                    <table class="table">
                        <tr>
                            <th>Kelompok Pasien</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly value="<?=$namakelompok?>">
                                </div>
                            </td>
                        </tr>
                        <?php if($idkelompokpasien == 2){?>
                        <tr>
                            <th>Rekanan Asuransi</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly value="<?=$namarekanan?>">
                                </div>
                            </td>
                        </tr>
                        <?php }?>
                        <tr>
                            <th>Pegawai Fisioterapi</th>
                            <td>
                                <div class="col-sm-12">
                                    <select name="idpegawai" <?=(UserAccesForm($user_acces_form,array('1035'))?'':'disabled')?> id="idpegawai" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                        <option value="#">Pilih Opsi</option>
                                        <?php foreach($list_pegawai as $row){ ?>
                                        <option value="<? echo $row->id ?>" <?=($idpegawai == $row->id ? 'selected' : '')?>><?php echo $row->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="control-group">
                    <div class="col-md-12">
                        <div class="progress progress-mini">
                            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                        </div>
                        <table class="table table-striped table-bordered" id="tindakan-list" style="margin-bottom:0px">
                            <thead>
                                <tr>
                                    <th>Tindakan</th>
                                    <th>Tarif</th>
                                    <th>Kuantitas</th>
                                    <th>Diskon (%)</th>
                                    <th>Jumlah</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($list_detail as $row) { ?>
                                <tr>
                                    <?php $total = $row->total * $row->kuantitas?>
                                    <td width="10%"><?=TreeView($row->level, $row->nama);?></td>
                                    <td width="10%" style="display:none"><?=number_format($row->jasasarana)?></td>
                                    <td width="10%" style="display:none"><?=number_format($row->jasapelayanan)?></td>
                                    <td width="10%" style="display:none"><?=number_format($row->bhp)?></td>
                                    <td width="10%" style="display:none"><?=number_format($row->biayaperawatan)?></td>
                                    <td width="10%"><?=number_format($row->total)?></td>
                                    <td width="10%"><?=number_format($row->kuantitas)?></td>
                                    <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                                    <td width="10%" hidden><?=number_format($row->diskon)?></td>
                                    <td width="10%"><?=number_format($row->totalkeseluruhan)?></td>
                                    <td style="display:none"><?=$row->idfisioterapi?></td>
                                    <td style="display:none"><?=$row->statusverifikasi?></td>
                                    <td width="10%">
                                        <?php if($row->statusverifikasi == 0) { ?>
                                        <div class='btn btn-group'>
                                            <?php if (UserAccesForm($user_acces_form,array('1037'))) { ?>
                                            <a href='#' class='btn btn-primary btn-sm tindakan-edit' data-toggle='modal' data-target='#tindakan-modal' id='tindakan-modal-show' data-toggle='tooltip' title='Ubah'><i class='fa fa-pencil'></i></a>
                                            <? } ?>
                                            <?php if (UserAccesForm($user_acces_form,array('1038'))) { ?>
                                            <a href='#' class='btn btn-danger btn-sm tindakan-delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a>
                                            <? } ?>
                                        </div>
                                        <?php } else { ?>
                                        <div class='btn btn-group'>
                                            <button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>
                                        </div>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <table class="table table-striped table-bordered">
                            <tbody>
                                <tr>
                                    <td colspan="2" class="hidden-phone">
                                        <?php if (UserAccesForm($user_acces_form,array('1036'))){ ?>
                                        <button data-toggle="modal" data-target="#tindakan-modal" id="tindakan-modal-show" class="btn btn-info btn-xs" type="button">Tambah Tindakan</button>
                                        <?}?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="text-right bg-light lter">
            <button class="btn btn-info" type="submit">Simpan</button>
            <button class="btn btn-default" type="button">Batal</button>
        </div>
        <input type="hidden" id="tindakan-value" name="tindakan-value">
        <br><br>
        <?php echo form_hidden('id', $id); ?>
        <?php echo form_hidden('idpendaftaran', $idpendaftaran); ?>
        <?php echo form_close() ?>
    </div>
</div>

<!-- Modal Tindakan -->
<div class="modal fade in" id="tindakan-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none; left:30px;">
    <div class="modal-dialog modal-lg modal-dialog-popout">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Daftar Tindakan</h3>
                </div>
                <div class="block-content">
                    <div id="tindakan-step-1">

                        <div class="row" style="margin-bottom: 20px">
                            <div class="col-md-12 form-horizontal">
                                <div class="form-val">
                                    <label class="col-sm-4" for="idheadparent">Head Parent</label>
                                    <div class="col-sm-8">
                                        <select name="idtipe" id="idheadparent" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                            <option value="" selected>Pilih Opsi</option>
                                            <?=$list_parent?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <table width="100%" class="table table-striped table-striped table-hover table-bordered" id="tindakan-table-step-1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Jasa Sarana</th>
                                    <th>Jasa Pelayanan</th>
                                    <th>BHP</th>
                                    <th>Biaya Perawatan</th>
                                    <th>Total Tarif</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div style="display: none;" id="tindakan-step-2">
                        <input type="hidden" class="form-control" id="rowindex">
                        <table class="table table-striped table-bordered table-hover" id="tindakan-table-step-2">
                            <thead>
                                <tr>
                                    <th>Tindakan</th>
                                    <th>Tarif</th>
                                    <th>Kuantitas</th>
                                    <th>Diskon (%)</th>
                                    <th>Diskon (Rp)</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="tindakan-input">
                                    <td id="namatarif"></td>
                                    <td style="display: none" id="jasasarana"></td>
                                    <td style="display: none" id="jasapelayanan"></td>
                                    <td style="display: none" id="bhp"></td>
                                    <td style="display: none" id="biayaperawatan"></td>
                                    <td id="totaltarif"></td>
                                    <td><input class="form-control input-sm number" type="text" id="kuantitas" value="1" autocomplete="off" /></td>
                                    <td><input class="form-control input-sm number-diskon" type="text" id="diskon" value="0" autocomplete="off" /></td>
                                    <td><input class="form-control input-sm number" type="text" id="diskonRp" value="0" autocomplete="off" /></td>
                                    <td><input class="form-control input-sm number" type="text" id="totalkeseluruhan" value="" autocomplete="off" readonly /></td>
                                    <td style="display: none" id="idtarif"></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="modal-footer">
                            <button class="btn btn-sm btn-default" id="tindakan-back" type="button">Batalkan</button>
                            <button class="btn btn-sm btn-primary" type="button" id="tindakan-add">Tambahkan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
jQuery(function() {
    BaseTableDatatables.init();
    var idkelas = '<?=$idkelas?>';
    var idkelompokpasien = '<?=$idkelompokpasien?>';
    var idrekanan = '<?=$idrekanan?>';
    var idpath = 1;

    $('#tindakan-table-step-1').DataTable({
        "bSort": false,
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}trujukan_fisioterapi/getTindakan/' + idkelas + '/' + idkelompokpasien + '/' + idrekanan + '/' + idpath,
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": true
            },
            {
                "width": "20%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 2,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 3,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 4,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 5,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 6,
                "orderable": true
            }
        ]
    });
});

$(document).ready(function() {
    $(".time-datepicker").datetimepicker({
        format: "HH:mm:ss"
    });

    $(".number").number(true, 0, '.', ',');
    $(".number-diskon").number(true, 2, '.', ',');

    $("#idheadparent").change(function() {
        var idkelas = '<?=$idkelas?>';
        var idkelompokpasien = '<?=$idkelompokpasien?>';
        var idrekanan = '<?=$idrekanan?>';

        $('#tindakan-table-step-1').DataTable().destroy();
        $('#tindakan-table-step-1').DataTable({
            "bSort": false,
            "autoWidth": false,
            "pageLength": 10,
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                url: '{site_url}trujukan_fisioterapi/getTindakan/' + idkelas + '/' + idkelompokpasien + '/' + idrekanan + '/' + $(this).val(),
                type: "POST",
                dataType: 'json'
            },
            "columnDefs": [{
                    "width": "5%",
                    "targets": 0,
                    "orderable": true
                },
                {
                    "width": "20%",
                    "targets": 1,
                    "orderable": true
                },
                {
                    "width": "10%",
                    "targets": 2,
                    "orderable": true
                },
                {
                    "width": "10%",
                    "targets": 3,
                    "orderable": true
                },
                {
                    "width": "10%",
                    "targets": 4,
                    "orderable": true
                },
                {
                    "width": "10%",
                    "targets": 5,
                    "orderable": true
                },
                {
                    "width": "10%",
                    "targets": 6,
                    "orderable": true
                }
            ]
        });
    })


    $("#tindakan-modal-show").click(function() {
        $('#tindakan-step-1').fadeIn('slow');
        $('#tindakan-step-2').hide();
    })

    $("#tindakan-back").click(function() {
        $('#tindakan-step-1').fadeIn('slow');
        $('#tindakan-step-2').fadeOut();
        $("#kuantitas").val('');
        $("#diskon").val('');
        $("#diskonRp").val('');
    })

    $(document).on("click", "#tindakan-select", function() {
        $('#tindakan-step-1').fadeOut();
        $('#tindakan-step-2').fadeIn('slow');
        $('table#tindakan-table-step-2 tbody').empty();

        content = '<tr id="tindakan-input">';
        content += '<td id="namatarif">' + $(this).closest('tr').find("td:eq(1) a").html() + '</td>';
        content += '<td style="display: none" id="jasasarana">' + $(this).closest('tr').find("td:eq(2)").html() + '</td>';
        content += '<td style="display: none" id="jasapelayanan">' + $(this).closest('tr').find("td:eq(3)").html() + '</td>';
        content += '<td style="display: none" id="bhp">' + $(this).closest('tr').find("td:eq(4)").html() + '</td>';
        content += '<td style="display: none" id="biayaperawatan">' + $(this).closest('tr').find("td:eq(5)").html() + '</td>';
        content += '<td id="totaltarif">' + $(this).closest('tr').find("td:eq(6)").html() + '</td>';
        content += '<td><input class="form-control input-sm number" type="text" id="kuantitas" value="1" autocomplete="off"/></td>';
        content += '<td><input class="form-control input-sm number-diskon" type="text" id="diskon" value="0" autocomplete="off"/></td>';
        content += '<td><input class="form-control input-sm number" type="text" id="diskonRp" value="0" autocomplete="off"/></td>';
        content += '<td><input class="form-control input-sm number" type="text" id="totalkeseluruhan" value="' + $(this).closest('tr').find("td:eq(6)").html() + '" autocomplete="off" readonly/></td>';
        content += '<td style="display: none" id="idtarif">' + $(this).data('idtindakan') + '</td>';
        content += '</tr>';

        $("table#tindakan-table-step-2 tbody").html(content);

        $(".number").number(true, 0, '.', ',');
        $(".number-diskon").number(true, 2, '.', ',');

        $("#kuantitas").focus();
    });

    $(document).on("keyup", "#kuantitas", function() {
        var totaltarif = $("#totaltarif").text().replace(/,/g, '');
        var kuantitas = $(this).val().replace(/,/g, '');
        var subtotal = parseFloat(totaltarif) * parseFloat(kuantitas);

        var diskonRupiah = parseFloat(subtotal * parseFloat($('#diskon').val()) / 100);
        $("#diskonRp").val(diskonRupiah);

        var totalkeseluruhan = parseFloat(subtotal) - parseFloat(diskonRupiah);
        $("#totalkeseluruhan").val(totalkeseluruhan);
    });

    $(document).on("keyup", "#diskon", function() {
        var totaltarif = $("#totaltarif").html().replace(/,/g, '');
        var kuantitas = $("#kuantitas").val().replace(/,/g, '');
        var subtotal = (parseFloat(totaltarif) * parseFloat(kuantitas));

        if ($("#diskon").val() == '') {
            $("#diskon").val(0)
        }
        if (parseFloat($(this).val()) > 100) {
            $(this).val(100);
        }

        var diskonRupiah = parseFloat(subtotal * parseFloat($(this).val()) / 100);
        $("#diskonRp").val(diskonRupiah);
        
        var totalkeseluruhan = parseFloat(subtotal) - parseFloat(diskonRupiah);
        $("#totalkeseluruhan").val(totalkeseluruhan);
    });

    $(document).on("keyup", "#diskonRp", function() {
        var totaltarif = $("#totaltarif").html().replace(/,/g, '');
        var kuantitas = $("#kuantitas").val().replace(/,/g, '');
        var subtotal = (parseFloat(totaltarif) * parseFloat(kuantitas));

        if ($("#diskonRp").val() == '') {
            $("#diskonRp").val(0)
        }
        if (parseFloat($(this).val()) > subtotal) {
            $(this).val(subtotal);
        }

        var diskonPersen = parseFloat((parseFloat($(this).val() * 100)) / subtotal);
        $("#diskon").val(diskonPersen);

        var totalkeseluruhan = parseFloat(subtotal) - parseFloat($(this).val());
        $("#totalkeseluruhan").val(totalkeseluruhan);
    });

    $(document).on("click", "#tindakan-add", function() {
        var valid = validateDetail();
        if (!valid) return false;

        var rowindex;
        var duplicate = false;

        if ($("#rowindex").val() != '') {
            var content = "";
            rowindex = $("#rowindex").val();
        } else {
            var content = "<tr>";
            $('#tindakan-list tbody tr').filter(function() {
                var $cells = $(this).children('td');
                if ($cells.eq(0).text() === $("#namatarif").text()) {
                    sweetAlert("Maaf...", "Tindakan " + $("#namatarif").text() + " sudah ditambahkan.", "error");
                    duplicate = true;
                    return false;
                }
            });
        }

        if (duplicate == false) {
            content += "<td width='10%'>" + $("#namatarif").text(); + "</td>";
            content += "<td style='display: none' width='10%'>" + $("#jasasarana").text(); + "</td>";
            content += "<td style='display: none' width='10%'>" + $("#jasapelayanan").text(); + "</td>";
            content += "<td style='display: none' width='10%'>" + $("#bhp").text(); + "</td>";
            content += "<td style='display: none' width='10%'>" + $("#biayaperawatan").text(); + "</td>";
            content += "<td width='10%'>" + $("#totaltarif").text(); + "</td>";
            content += "<td width='10%'>" + $("#kuantitas").val(); + "</td>";
            content += "<td width='10%'>" + $("#diskon").val(); + "</td>";
            content += "<td width='10%' hidden>" + $("#diskonRp").val(); + "</td>";
            content += "<td width='10%'>" + $.number($("#totalkeseluruhan").val()); + "</td>";
            content += "<td style='display:none'>" + $("#idtarif").text(); + "</td>";
            content += "<td style='display:none'>0</td>";
            content += "<td width='10%'>";
            content += "<div class='btn btn-group'>";
            content += "<a href='#' class='btn btn-primary btn-sm tindakan-edit' data-toggle='modal' data-target='#tindakan-modal' id='tindakan-modal-show' data-toggle='tooltip' title='Ubah'><i class='fa fa-pencil'></i></a>";
            content += "<a href='#' class='btn btn-danger btn-sm tindakan-delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
            content += "</div>";
            content += "</td>";

            if ($("#rowindex").val() != '') {
                $('#tindakan-list tbody tr:eq(' + rowindex + ')').html(content);
            } else {
                content += "</tr>";
                $('#tindakan-list tbody').append(content);
            }

            $("#tindakan-back").click();
            detailClear();

            swal({
                type: 'success',
                title: 'Berhasil!',
                text: 'Tindakan telah berhasil ditambahkan.',
                timer: 1000,
                showCancelButton: false,
                showConfirmButton: false
            });
        }
    });

    $(document).on("click", ".tindakan-edit", function() {
        $('#tindakan-step-1').fadeOut();
        $('#tindakan-step-2').fadeIn('slow');

        $("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);

        $("#namatarif").text($(this).closest('tr').find("td:eq(0)").html());
        $("#jasasarana").text($(this).closest('tr').find("td:eq(1)").html());
        $("#jasapelayanan").text($(this).closest('tr').find("td:eq(2)").html());
        $("#bhp").text($(this).closest('tr').find("td:eq(3)").html());
        $("#biayaperawatan").text($(this).closest('tr').find("td:eq(4)").html());
        $("#totaltarif").text($(this).closest('tr').find("td:eq(5)").html());
        $("#kuantitas").val($(this).closest('tr').find("td:eq(6)").html());
        $("#diskon").val($(this).closest('tr').find("td:eq(7)").html());
        $("#diskonRp").val($(this).closest('tr').find("td:eq(8)").html());
        $("#totalkeseluruhan").val($(this).closest('tr').find("td:eq(9)").html());
        $("#idtarif").text($(this).closest('tr').find("td:eq(10)").html());
        $("#statusverifikasi").text($(this).closest('tr').find("td:eq(11)").html());

        return false;
    });

    $(document).on("click", ".tindakan-delete", function() {
        var row = $(this).closest('td').parent();
        swal({
            title: '',
            text: "Apakah anda yakin akan menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false,
            closeOnCancel: true
        }).then(function() {
            return row.remove();
        })
        return false;
    });

    $("#form-work").submit(function(e) {
        var form = this;

        if ($("#idpegawai").val() == "#") {
            e.preventDefault();
            sweetAlert("Maaf...", "Pegawai Fisioterapi Belum Dipilih!", "error");
            return false;
        }

        var tindakan_tbl = $('table#tindakan-list tbody tr').get().map(function(row) {
            return $(row).find('td').get().map(function(cell) {
                return $(cell).html();
            });
        });

        $("#tindakan-value").val(JSON.stringify(tindakan_tbl));

        swal({
            title: "Berhasil!",
            text: "Proses penyimpanan data.",
            type: "success",
            timer: 1500,
            showConfirmButton: false
        });
    });
});

function detailClear() {
    $("#rowindex").val('');
    $(".detail").val('');
}

function validateDetail() {
    var kuantitas = $("#kuantitas").val();
    if (kuantitas == "" || kuantitas == "0") {
        sweetAlert("Maaf...", "Kuantitas tidak boleh kosong!", "error");
        return false;
    }
    return true;
}

function validate_final() {
    $("*[disabled]").not(true).removeAttr("disabled");
    return true;
}

</script>
