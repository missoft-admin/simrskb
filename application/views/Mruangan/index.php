<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('183'))){ ?>
<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('184'))){ ?>
		<ul class="block-options">
        <li>
            <a href="{base_url}mruangan/create" class="btn"><i class="fa fa-plus"></i></a>
        </li>
    </ul>
		<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Tipe</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"bSort": false,
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
				url: '{site_url}mruangan/getIndex',
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "10%", "targets": 0, "orderable": true },
					{ "width": "50%", "targets": 1, "orderable": true },
					{ "width": "20%", "targets": 2, "orderable": true },
					{ "width": "20%", "targets": 3, "orderable": true }
				]
			});
	});
	function set_tampil(id,st_tampil){
		if (st_tampil=='1'){
			swal({
				title: "Apakah Anda Yakin ?",
				text : "Menampilkan Ruangan ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				update_tampil(id,st_tampil)
			});
		}else{
			swal({
				title: "Apakah Anda Yakin ?",
				text : "Tidak Akan Menampilkan Ruangan ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				update_tampil(id,st_tampil)
			});
		}
		
			
	}
	function update_tampil(id,st_tampil){
		$("#cover-spin").show();
			$.ajax({
				url: '{site_url}mruangan/update_tampil', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						st_tampil:st_tampil,
					   },
				success: function(data) {
					$("#cover-spin").hide();
					$('#datatable-simrs').DataTable().ajax.reload( null, false );
				}
			});
		
	}
</script>
