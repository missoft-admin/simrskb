<?= ErrorSuccess($this->session) ?>
<?php if ($error != '') echo ErrorMessage($error) ?>
<?php if (UserAccesForm($user_acces_form,array('1429'))){?>
<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('1429'))){?>
		<ul class="block-options">
			<li>
				<a href="{site_url}trefund/manage" class="btn"><i class="fa fa-plus"></i></a>
			</li>
		</ul>
		<?}?>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-bottom: 0px;">
	</div>
	<div class="block-content">
		<form class="form-horizontal" action="{site_url}trefund/filter" method="post">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label">No Medrec</label>
						<div class="col-md-9">
							<input class="form-control" type="text" id="nomedrec" name="nomedrec" value="<?= $nomedrec ?>">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label">Nama Pasien</label>
						<div class="col-md-9">
							<input class="form-control" type="text" id="namapasien" name="namapasien" value="<?= $namapasien ?>">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label">Tipe Refund</label>
						<div class="col-md-9">
							<select id="tiperefund" name="tiperefund" class="js-select2 form-control" style="width: 100%;">
								<option value="#" selected>Semua Tipe</option>
								<option value="0" <?= ($tiperefund == '0' ? 'selected' : '') ?>>Deposit</option>
								<option value="1" <?= ($tiperefund == '1' ? 'selected' : '') ?>>Pengembalian Obat</option>
								<option value="2" <?= ($tiperefund == '2' ? 'selected' : '') ?>>Transaksi</option>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label">Tanggal</label>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-6">
									<input class="form-control datepicker" type="text" id="tanggal_refund_awal" name="tanggal_refund_awal" value="{tanggalawal}">
								</div>
								<div class="col-md-6">
									<input class="form-control datepicker" type="text" id="tanggal_refund_akhir" name="tanggal_refund_akhir" value="{tanggalakhir}">
								</div>
							</div>
						</div>
					</div>
					<?php if (UserAccesForm($user_acces_form,array('1091'))){?>
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="button" id="btn_filter" name="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						</div>
					</div>
					<?}?>
				</div>
			</div>
		</form>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<table id="table_index" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>id</th>
							<th>No</th>
							<th>Tanggal & Waktu</th>
							<th>Tipe</th>
							<th>No Refund</th>
							<th>No Medrec</th>
							<th>Nama Pasien</th>
							<th>Nominal</th>
							<th>Alasan</th>
							<th>Metode</th>
							<th>Bank</th>
							<th>No Rekening</th>
							<th>Status</th>
							<th width="9%">Aksi</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<?}?>
<div class="modal fade in" id="modal-alasan-refund" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close">
								<i class="fa fa-close" style="display:block;font-size:15px;"></i>
							</button>
						</li>
					</ul>
					<h3 class="block-title">Hapus Transaksi</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal">
						<div class="form-group" style="margin-bottom: 5px;">
							<div class="col-md-12">
								<label class="control-label-custom text-uppercase" for="">Alasan</label>
								<textarea id="alasan" class="form-control" name="name" rows="7" cols="80"></textarea>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<div class="col-md-12">
								<button class="btn btn-sm btn-success text-uppercase" id="delete-refund" type="button" style="margin:10px 0;" data-dismiss="modal">Simpan
								</button>
							</div>
						</div>
						<input id="idrefund" type="hidden" readonly>
						
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('Trefund_kas/modal/modal_edit_transaksi'); ?>
<div class="modal fade in black-overlay" id="modal_approval" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<input type="hidden" class="form-control" id="tipe_refund" placeholder="" name="tipe_refund" value="">
						<input type="hidden" class="form-control" id="nominal_trx" placeholder="" name="nominal_trx" value="">
						<input type="hidden" class="form-control" id="idtrx" placeholder="" name="idtrx" value="">
						<div class="col-md-12">
							<table width="100%" id="tabel_user" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Step</th>
										<th>User</th>
										<th>Jika Setuju</th>
										<th>Jika Menolak</th>								
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						
					</div>
					<?php echo form_close() ?>
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-primary" id="btn_simpan_approval" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Proses Persetujuan</button>
			</div>
			
		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_user" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<input type="hidden" class="form-control" id="idrka" placeholder="" name="idrka" value="">
						<div class="col-md-12">
							<table width="100%" id="tabel_user_proses" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Step</th>
										<th>User</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						
					</div>
					<?php echo form_close() ?>
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
			
		</div>
	</div>
</div>
<div class="modal fade" id="modal_tolak" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Alasan Tolak</h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Alasan</label>
								<div class="col-md-8">
									<textarea class="form-control" rows="2" name="alasan_tolak" id="alasan_tolak"></textarea>
									<input type="hidden" readonly class="form-control" id="id_approval"  placeholder="id_approval" name="id_approval" value="">
								</div>
							</div>
						</div>							
					</div>
					
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-danger text-uppercase" type="button" name="btn_tolak" id="btn_tolak"><i class="fa fa-save"></i> SIMPAN TOLAK</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.datepicker').datepicker({
			format: "dd/mm/yyyy",
			todayHighlight: true,
		});
		load_index();
		
		$(document).on('click', '.show-refund', function() {
			var id = $(this).data('idtransaksi');

			$('#modal-alasan-refund').modal('show');
			$('#idrefund').val(id);
		});
		$(document).on('click', '#btn_filter', function() {
			load_index();
		});
		$(document).on('click', '.approval', function() {
			$('#modal_approval').modal();
			let tipe_refund = $(this).data('tipe_refund');
			let nominal_trx = $(this).data('nominal');
			let idtrx = $(this).data('id');
			// alert(tipe_refund + ' '+ nominal_trx);return false;
			$('#tipe_refund').val(tipe_refund);
			$('#nominal_trx').val(nominal_trx);
			$('#idtrx').val(idtrx);
			
			load_user_approval();
			
		});
		function load_user_approval(){
			var tipe_refund=$("#tipe_refund").val();
			var nominal_trx=$("#nominal_trx").val();
			$('#tabel_user').DataTable().destroy();
			table=$('#tabel_user').DataTable({
					"autoWidth": false,
					"pageLength": 10,
					"ordering": false,
					"processing": true,
					"serverSide": true,
					"order": [],
					"ajax": {
						url: '{site_url}trefund_kas/load_user_approval',
						type: "POST",
						dataType: 'json',
						data : {
							tipe_refund:tipe_refund,
							nominal_trx:nominal_trx,
													
						   }
					},
					"columnDefs": [
						 {  className: "text-right", targets:[0] },
						 {  className: "text-center", targets:[1,2,3] },
						 { "width": "10%", "targets": [0] },
						 { "width": "40%", "targets": [1] },
						 { "width": "25%", "targets": [2,3] },

					]
				});
		}
		$(document).on("click","#btn_simpan_approval",function(){		
			swal({
				title: "Apakah Anda Yakin ?",
				text : "Melanjutkan Step Persetujuan?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#d26a5c",
				cancelButtonText: "Tidak Jadi",
			}).then(function() {
				$.ajax({
					url: '{site_url}trefund_kas/simpan_proses_peretujuan/'+$("#idtrx").val(),
					type: 'POST',
					complete: function() {
						swal({
							title: "Berhasil!",
							text: "Proses Approval Berhasil.",
							type: "success",
							timer: 1500,
							showConfirmButton: false
						});
						$('#table_index').DataTable().ajax.reload( null, false );
					}
				});
			});
			
			return false;
		});
		$(document).on("click",".user",function(){
			var table = $('#table_index').DataTable()
			var tr = $(this).parents('tr')
			var id = table.cell(tr,0).data()
			$("#idtrx").val(id);
			load_user();
		});
		$(document).on("click",".verif",function(){
			var table = $('#table_index').DataTable()
			var tr = $(this).parents('tr')
			var id = table.cell(tr,0).data()
			var id_approval=$(this).data('id');
			swal({
				title: "Apakah Anda Yakin  ?",
				text : "Verifikasi Refund ini?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#d26a5c",
				cancelButtonText: "Tidak Jadi",
			}).then(function() {
				$.ajax({
					url: '{site_url}Trefund_kas/verifikasi/'+id,
					type: 'POST',
					success: function(data) {
						// console.log(data);
						// alert(data);
						$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi berhasil'});
						$('#table_index').DataTable().ajax.reload( null, false );
						// if (data=='"1"'){
						// window.location.href = "{site_url}mrka_approval/next_bendahara/"+id;
						// }
					}
				});
			});
			
			return false;
			
		});
		function load_user(){
			var idtrx=$("#idtrx").val();
			$("#modal_user").modal('show');
			
			// alert(idrka);
			$.ajax({
				url: '{site_url}trefund_kas/list_user/'+idtrx,
				dataType: "json",
				success: function(data) {
					$("#tabel_user_proses tbody").empty();
					$("#tabel_user_proses tbody").append(data.detail);
				}
			});
		}
		$(document).on('click', '#delete-refund', function() {
			var id = $('#idrefund').val();
			var alasan = $('#alasan').val();

			$.ajax({
				type: 'POST',
				url: "{site_url}Trefund_kas/removeRefund/" + id,
				dataType: 'json',
				data: {
					alasan: alasan
				},
				success: function(data) {
					swal({
						title: "Berhasil!",
						text: "Proses penyimpanan data.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					reload();
				}
			});
		});
		$(document).on("click",".setuju",function(){
			var table = $('#table_index').DataTable()
			var tr = $(this).parents('tr')
			var id = table.cell(tr,0).data()
			var id_approval=$(this).data('id');
			swal({
				title: "Apakah Anda Yakin  ?",
				text : "Menyetujui Pengajuan ini?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#d26a5c",
				cancelButtonText: "Tidak Jadi",
			}).then(function() {
				$.ajax({
					url: '{site_url}Trefund_kas/setuju_batal/'+id_approval+'/1'+'/'+id,
					type: 'POST',
					success: function(data) {
						// console.log(data);
						// alert(data);
						// $.toaster({priority : 'success', title : 'Succes!', message : ' Pengajuan berhasil disetujui'});
						$('#table_index').DataTable().ajax.reload( null, false );
						// if (data=='"1"'){
						// window.location.href = "{site_url}mrka_approval/next_bendahara/"+id;
						// }
					}
				});
			});
			
			return false;
			
		});
		$(document).on("click",".batal",function(){
			var table = $('#table_index').DataTable()
			var tr = $(this).parents('tr')
			var id = table.cell(tr,0).data()
			var id_approval=$(this).data('id');
			swal({
				title: "Apakah Anda Yakin  ?",
				text : "Mengembalikan Status Konfirmasi Pengajuan ini?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#d26a5c",
				cancelButtonText: "Tidak Jadi",
			}).then(function() {
				$.ajax({
					url: '{site_url}Trefund_kas/setuju_batal/'+id_approval+'/0',
					type: 'POST',
					complete: function() {
						$.toaster({priority : 'success', title : 'Succes!', message : ' Pengembalian berhasil disetujui'});
						$('#table_index').DataTable().ajax.reload( null, false );
					}
				});
			});
			
			return false;
			
		});
		$(document).on("click","#btn_tolak",function(){
		
			var id_approval=$("#id_approval").val();
			var alasan_tolak=$("#alasan_tolak").val();
			swal({
				title: "Apakah Anda Yakin  ?",
				text : "Menolak Pengajuan ini?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#d26a5c",
				cancelButtonText: "Tidak Jadi",
			}).then(function() {
				$.ajax({
					url: '{site_url}Trefund_kas/tolak/',
					type: 'POST',
					data: {
						id_approval:id_approval,
						alasan_tolak:alasan_tolak,
					},
					complete: function() {
						$('#modal_tolak').modal('hide');
						$.toaster({priority : 'success', title : 'Succes!', message : ' Pengajuan berhasil ditolak'});
						$('#table_index').DataTable().ajax.reload( null, false );
						
					}
				});
			});
			
			return false;
			
		});
		$(document).on("click",".tolak",function(){
			var table = $('#table_index').DataTable()
			var tr = $(this).parents('tr')
			var id = table.cell(tr,0).data()
			var id_approval=$(this).data('id');
			$("#id_approval").val(id_approval)
			$('#modal_tolak').modal('show');
			// modal_tolak
			
		});
		$(document).on('click', '.edit-refund', function() {
			let id = $(this).data('idtransaksi');
			$('#idTransaksi').val(id);

			$('#modalEditRefund').modal('show');
			$('#updatePembayaranRefund').attr('disabled', false);
			$.ajax({
				url: "{base_url}Trefund_kas/getInfoRefund/" + id,
				dataType: 'json',
				success: function(data) {
					if (data.idmetode == '1') {
		        $('.formNonTunai').hide();
		        $('#bankRefund').attr('readonly', true);
		        $('#norekeningRefund').attr('readonly', true);
		      } else {
		        $('.formNonTunai').show();
		        $('#bankRefund').val('').removeAttr('readonly');
		        $('#norekeningRefund').val('').removeAttr('readonly');
		      }

					$('#norefund').val(data.norefund);
					$('#notransaksi').val(data.notransaksi);
					$('#nominalRefund').val($.number(data.totalrefund));
					$('#metodeRefund').select2('val', data.idmetode);
					$('#bankRefund').val(data.bank);
					$('#norekeningRefund').val(data.norekening);
					$('#alasanRefund').val(data.alasan);
				}
			});
		});
		$(document).on('click', '.lihat-refund', function() {
			let id = $(this).data('idtransaksi');
			$('#idTransaksi').val(id);

			$('#modalEditRefund').modal('show');

			$.ajax({
				url: "{base_url}Trefund_kas/getInfoRefund/" + id,
				dataType: 'json',
				success: function(data) {
					if (data.idmetode == '1') {
		        $('.formNonTunai').hide();
		        $('#bankRefund').attr('readonly', true);
		        $('#norekeningRefund').attr('readonly', true);
		      } else {
		        $('.formNonTunai').show();
		        $('#bankRefund').val('').removeAttr('readonly');
		        $('#norekeningRefund').val('').removeAttr('readonly');
		      }

					$('#norefund').val(data.norefund);
					$('#notransaksi').val(data.notransaksi);
					$('#nominalRefund').val($.number(data.totalrefund));
					$('#metodeRefund').select2('val', data.idmetode);
					$('#bankRefund').val(data.bank);
					$('#norekeningRefund').val(data.norekening);
					$('#alasanRefund').val(data.alasan);
				}
			});
					$('#updatePembayaranRefund').attr('disabled', true);
			
		});
	});
	function load_index(){
		var nomedrec=$("#nomedrec").val();
		var namapasien=$("#namapasien").val();		
		var tiperefund=$("#tiperefund").val();
		var tanggal_refund_awal=$("#tanggal_refund_awal").val();
		var tanggal_refund_akhir=$("#tanggal_refund_akhir").val();
		$('#table_index').DataTable().destroy();
		table=$('#table_index').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}trefund_kas/getIndex',
					type: "POST",
					dataType: 'json',
					data : {
						nomedrec:nomedrec,
						namapasien:namapasien,
						tiperefund:tiperefund,
						tanggal_refund_awal:tanggal_refund_awal,
						tanggal_refund_akhir:tanggal_refund_akhir,
					   }
				},
				"columnDefs": [
					{ "width": "0%", "targets": 0, "visible": false },
					{ "width": "3%", "targets": 1, "visible": true,"class":"text-right" },
					{ "width": "8%", "targets": [2,3,4,5], "visible": true,"class":"text-center" },
					{ "width": "12%", "targets": [13], "visible": true,"class":"text-left" },
					// { "width": "8%", "targets": 2, "visible": true,"class":"text-left" },
					// { "width": "10%", "targets": 3, "visible": true,"class":"text-left" },
					// { "width": "10%", "targets": 4, "visible": true,"class":"text-left" },
					// { "width": "10%", "targets": 5, "visible": true,"class":"text-right" },
					// { "width": "10%", "targets": 6, "visible": true ,"class":"text-left"},
				]
			});
	}
</script>
