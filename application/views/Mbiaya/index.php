<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
		<ul class="block-options">
        <li>
            <a href="{base_url}mbiaya/create" class="btn"><i class="fa fa-plus"></i></a>
        </li>
    </ul>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<table class="table table-bordered table-striped js-dataTable-full">
			<thead>
				<tr>
					<th>No.</th>
					<th>Keterangan</th>
					<th>No. Akun Debit</th>
					<th>Aksi</th>
					<th style="display:none">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php $no=0; ?>
				<?php foreach($list_index as $row){ ?>
				<?php $no=$no+1; ?>
				<tr>
					<td><?php echo $no; ?></td>
					<td><?php echo $row->keterangan; ?></td>
					<td><i><?php echo $row->noakun.' '.$row->namaakun; ?></i></td>
					<td>
						<div class="btn-group">
							<a href="{base_url}mbiaya/edit/<?php echo $row->id; ?>" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
							<a href="{base_url}mbiaya/delete/<?=$row->id?>" data-toggle="tooltip" title="Delete" onclick="remove_row(this); return false;"><i class="fa fa-trash-o"></i></a>
						</div>
					</td>
					<td style="display:none">&nbsp;</td>
				</tr>
				<? } ?>
			</tbody>
		</table>
	</div>
</div>
