<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mbiaya" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mbiaya/save','class="form-horizontal"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="keterangan">Keterangan</label>
				<div class="col-md-7">
					<input type="text" class="form-control" name="keterangan" value="{keterangan}" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="noakun">No. Akun Debit</label>
				<div class="col-md-7">
					<select class="js-select2 form-control" id="noakun" name="noakun" style="width: 100%;" data-placeholder="No. Akun Debit">
						<option value="#">Pilih Opsi</option>
						<?php foreach($list_noakun as $row){ ?>
							<option value="<? echo $row->noakun ?>" <?=($noakun==$row->noakun) ? "selected" : "" ?>><?php echo $row->noakun.' '.$row->namaakun; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Submit</button>
					<button class="btn btn-default" type="reset"><i class="pg-close"></i> Clear</button>
				</div>
			</div>
		<?php echo form_hidden('id', $id); ?>
		<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.number').number(true, 0, '.', ',');
	});
</script>
