<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1621'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_berat()"><i class="fa fa-thermometer-0"></i> {title}</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('1621'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?>" id="tab_info">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
					
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_berat">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="40%">Range BB / TB</th>
										<th width="25%">Kategori</th>
										<th width="15%">Warna</th>
										<th width="15%">Action</th>										   
									</tr>
									<tr>
										<th>#<input class="form-control" type="hidden" id="berat_id" name="berat_id" value="" placeholder="id"></th>
										<th>
											<div class="input-group" style="width:100%">
												<input class="form-control decimal" type="text" id="berat_1" name="berat_1" placeholder="BB / TB" value="" >
												<span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
												<input class="form-control decimal" type="text" id="berat_2" name="berat_2" placeholder="BB / TB" value="" >
												
												
											</div>
										</th>
										<th>
											<input class="form-control" type="text" style="width:100%" id="kategori_berat" name="kategori_berat" placeholder="Kategori" value="" >
										</th>
										<th>
											
											<div class="js-colorpicker input-group colorpicker-element">
												<input class="form-control" type="text" id="warna" name="warna" value="#0000">
												<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
											</div>
										
										</th>
										<th>
											<div class="btn-group">
											<?php if (UserAccesForm($user_acces_form,array('1622'))){ ?>
											<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_berat" name="btn_tambah_berat"><i class="fa fa-save"></i> Simpan</button>
											<?}?>
											<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_berat()"><i class="fa fa-refresh"></i> Clear</button>
											</div>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
					</div>
				</div>
				</div>
			
			
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
	$(".decimal").number(true,1,'.',',');	
	load_berat();		
	
})	
function clear_berat(){
	$("#berat_id").val('');
	$("#berat_1").val('');
	$("#berat_2").val('');
	$("#kategori_berat").val('');
	$("#warna").val('#000');
	$("#btn_tambah_berat").html('<i class="fa fa-save"></i> Save');
	
}
function load_berat(){
	$('#index_berat').DataTable().destroy();	
	table = $('#index_berat').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mberat/load_berat', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}

$("#btn_tambah_berat").click(function() {
	let berat_id=$("#berat_id").val();
	let berat_1=$("#berat_1").val();
	let berat_2=$("#berat_2").val();
	let kategori_berat=$("#kategori_berat").val();
	let warna=$("#warna").val();
	
	if (berat_1==''){
		sweetAlert("Maaf...", "Tentukan Range BB / TB", "error");
		return false;
	}
	if (berat_2==''){
		sweetAlert("Maaf...", "Tentukan Range BB / TB", "error");
		return false;
	}
	if (kategori_berat==''){
		sweetAlert("Maaf...", "Tentukan Kategori", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mberat/simpan_berat', 
		dataType: "JSON",
		method: "POST",
		data : {
				berat_id:berat_id,
				berat_1:berat_1,
				berat_2:berat_2,
				kategori_berat:kategori_berat,
				warna:warna,
				
			},
		complete: function(data) {
			clear_berat();
			$('#index_berat').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
function edit_berat(berat_id){
	$("#berat_id").val(berat_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mberat/find_berat',
		type: 'POST',
		dataType: "JSON",
		data: {id: berat_id},
		success: function(data) {
			$("#btn_tambah_berat").html('<i class="fa fa-save"></i> Edit');
			$("#berat_id").val(data.id);
			$("#berat_1").val(data.berat_1);
			$("#berat_2").val(data.berat_2);
			$("#warna").val(data.warna).trigger('change');
			$("#kategori_berat").val(data.kategori_berat);
			
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_berat(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus BB / TB?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mberat/hapus_berat',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_berat').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

</script>