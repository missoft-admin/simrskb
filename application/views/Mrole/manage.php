<?=ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<?php if (button_roles('mrole/save')): ?>
			<ul class="block-options"> 
				<li> 
					<a href="{base_url}mrole" class="btn"><i class="fa fa-reply"></i></a>
				</li> 
			</ul>
		<?php endif ?>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:25px">
		<div class="row">
            <?php echo form_open('mresources/filter', 'class="form-horizontal" id="form-work"'); ?>
            
			<div class="col-md-12">               
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-2 control-label" for="nama">Nama Role</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" readonly id="action" placeholder="nama" name="nama" value="{nama}">
                        <input type="hidden" class="form-control" readonly id="id" placeholder="id" name="id" value="{id}">
                    </div>
					<div class="col-sm-4" id="opsi_copy">
						<div class="btn-group">
							<button class="btn btn-default" type="button"><i class="fa fa-paste"></i> Copy dari..</button>
							<div class="btn-group">
								<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li class="dropdown-header">Role</li>
									<?foreach($list_role as $r){?>
										<? if ($r->jml > 0){?>
										<li>
											<a href="#" onClick="copy_to(<?=$r->id?>);"><span class="badge pull-right"><?=$r->jml?></span> <?=$r->nama?></a>
										</li>
										<?}?>
									<?}?>
									
									
								</ul>
							</div>
						</div>
					</div>
                </div>
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="distributor">Menu Utama </label>
                    <div class="col-md-4">
                        <select id="menu_id" name="menu_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#" <?=($menu_id == '#' ? 'selected' : ''); ?>>- Semua Menu Utama -</option>
                            <?php foreach ($list_menu as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?=($menu_id == $row->id ? 'selected' : ''); ?>><?php echo $row->menu; ?></option>
                            <?php } ?>
                        </select>
                    </div>
					<div class="col-sm-4" id="opsi_utama" hidden>
						<div class="btn-group">
							<button class="btn btn-success" type="button">Menu Utama Opsi</button>
							<div class="btn-group">
								<button class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li class="dropdown-header">Opsi</li>
									<li>
										<a id="utama_c_all" href="#">checked all</a>
									</li>
									<li>
										<a id="utama_u_all" href="javascript:void(0)">unchecked all</a>
									</li>
									
								</ul>
							</div>
						</div>
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="status">Sub Menu</label>
                    <div class="col-md-4">
                        <select id="sub_id" name="sub_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#" <?=($sub_id == '#' ? 'selected' : ''); ?>>- Semua Sub Menu -</option>
                            <?php foreach ($list_sub as $row) { ?>
                                    <option value="<?php echo $row->id2; ?>" <?=($sub_id == $row->id2 ? 'selected' : ''); ?>><?php echo $row->menu2; ?></option>
                            <?php } ?>
                        </select>
                    </div>
					<div class="col-sm-4" id="opsi_sub" hidden>
						<div class="btn-group">
							<button class="btn btn-success" type="button">Sub Menu Opsi</button>
							<div class="btn-group">
								<button class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li class="dropdown-header">Opsi</li>
									<li>
										<a id="sub_c_all" href="#">checked all</a>
									</li>
									<li>
										<a id="sub_u_all" href="javascript:void(0)">unchecked all</a>
									</li>
									
								</ul>
							</div>
						</div>
					</div>
                </div>                
            </div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<table id="mrole" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th style="width:10%">No</th>
					<th style="width:10%">ID</th>
					<th style="width:10%">Menu</th>
					<th style="width:10%">Form</th>
					<th style="width:20%">Aksi</th>
					<th style="width:10%">X</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">

  $(document).ready(function () {
		
		var delay = (function() {
			var timer = 0;
			return function(callback, ms) {
				clearTimeout(timer);
				timer = setTimeout(callback, ms);
			};
		})();
		
		loadDataAkses($("#id").val(),$("#menu_id").val(),$("#sub_id").val());
		
		$(document).on("click", "#utama_c_all", function() {
			swal({
				title: "Anda Yakin ?",
				text : "Ingin Checked semua Aksi Untuk Menu Utama "+$("#menu_id option:selected").text()+"?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				utama_c_all('1');
			});
		});
		$(document).on("click", "#utama_u_all", function() {
			swal({
				title: "Anda Yakin ?",
				text : "Ingin Unchecked semua Aksi Untuk Menu Utama "+$("#menu_id option:selected").text()+"?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				utama_c_all('0');
			});
		});
		function utama_c_all(pilih){
			var role_id=$("#id").val();
			var menu_id=$("#menu_id").val();
			$('#mrole').DataTable().destroy();
				$.ajax({
					url: '{site_url}mrole/utama_c_all',
					type: 'POST',
					data: {role_id: role_id, menu_id: menu_id, pilih: pilih},
					complete: function() {
						// delay(function() {
							$('#mrole').DataTable().destroy();
							loadDataAkses($("#id").val(),$("#menu_id").val(),$("#sub_id").val());
							$.toaster({priority : 'success', title : 'Succes!', message : 'Data Berhasil diubah'});
						// }, 1000);
					}
				});
		}
		$(document).on("click", "#sub_c_all", function() {
			swal({
				title: "Anda Yakin ?",
				text : "Ingin Checked semua Aksi Untuk Sub Menu "+$("#sub_id option:selected").text()+"?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				sub_c_all('1');
			});
		});
		$(document).on("click", "#sub_u_all", function() {
			swal({
				title: "Anda Yakin ?",
				text : "Ingin Unchecked semua Aksi Untuk Sub Menu "+$("#sub_id option:selected").text()+"?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				sub_c_all('0');
			});
		});
		function sub_c_all(pilih){
			var role_id=$("#id").val();
			var sub_id=$("#sub_id").val();
			$('#mrole').DataTable().destroy();
				$.ajax({
					url: '{site_url}mrole/sub_c_all',
					type: 'POST',
					data: {role_id: role_id, sub_id: sub_id, pilih: pilih},
					complete: function() {
						// delay(function() {
							$('#mrole').DataTable().destroy();
							loadDataAkses($("#id").val(),$("#menu_id").val(),$("#sub_id").val());
							$.toaster({priority : 'success', title : 'Succes!', message : 'Data Berhasil diubah'});
						// }, 1000);
					}
				});
		}
		
	
	$("#menu_id").change(function(){
		if ($(this).val()=='#'){
			$("#opsi_utama").hide();
		}else{
			$("#opsi_utama").show();			
		}
		// console.log($(this).val());
		$('#sub_id')
					.find('option')
					.remove()			
					.end()
					.append('<option value="#">- Semua Sub Menu -</option>')
					.val('#').trigger("change");
		if ($(this).val()!=''){
			$.ajax({
			  url: '{site_url}mresources/get_sub/'+$(this).val(),
			  dataType: "json",
			  success: function(data) {
				  
				$.each(data, function (i,row) {
					$('#sub_id').append('<option value="' + row.id2 + '">' + row.menu2 + '</option>');
				});	
				// delay(function() {
					$('#mrole').DataTable().destroy();
					loadDataAkses($("#id").val(),$("#menu_id").val(),$("#sub_id").val());
				// }, 1000);
			  }
			});
		}
		
		
	});
	$("#sub_id").change(function(){
		// opsi_sub
		if ($(this).val()=='#'){
			$("#opsi_sub").hide();
		}else{
			$("#opsi_sub").show();			
		}
		if ($(this).val() != ''){
			delay(function() {
				$('#mrole').DataTable().destroy();
				loadDataAkses($("#id").val(),$("#menu_id").val(),$("#sub_id").val());
			}, 1000);
		}
		
	});
	
		
  });
  function handleClick(cb,id3) {
		// alert(cb.checked);
	var dt = $('#mrole').DataTable();
	var pilih;
	var role_id = $('#id').val();
	if (cb.checked==true){
		pilih='1';
	}else{
		pilih='0';
	}
	$.ajax({
			url: '{site_url}mrole/savePilih',
			type: 'POST',
			data: {role_id: role_id, id3: id3, pilih: pilih},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : 'Data Berhasil diubah'});
				// alert('Berhasil');
				// swal("Berhasil!", "Mengatur Rule", "success").then(function() {
					// window.location.assign('{site_url}mrole');
				// });
			}
		});
	}
	function copy_to(role_id)
	{
		var id=$("#id").val();
		// alert(id+' : '+role_id);return false;
		$('#mrole').DataTable().destroy();
			$.ajax({
				url: '{site_url}mrole/copy_to',
				type: 'POST',
				data: {role_id: role_id, id: id},
				complete: function() {
					// delay(function() {
						loadDataAkses($("#id").val(),$("#menu_id").val(),$("#sub_id").val());
					// }, 1000);
					 $.toaster({priority : 'success', title : 'Succes!', message : 'Data Berhasil dicopy'});
				}
			});
	   // alert(role_id);
	}
	function loadDataAkses(role_id, menu_id, sub_id) {
			// alert(role_id+' : '+menu_id+' : '+sub_id);
			var tablePasien = $('#mrole').DataTable({
			"pageLength": 10,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}mrole/getAkses',
				type: "POST",
				dataType: 'json',
				data: {
					role_id: role_id,
					menu_id: menu_id,
					sub_id: sub_id,
				}
			},
			columnDefs: [{"targets": [1], "visible": true }	]
			});
	}
</script>
