<?=ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('244'))){ ?>
<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('247'))){ ?>
			<ul class="block-options"> 
				<li> 
					<button class="btn" data-toggle="modal" data-target="#add-roles"><i class="fa fa-plus"></i></button> 
				</li> 
			</ul>
		<?php } ?>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-bottom: 0px;">
	</div>
	<div class="block-content">
		<table id="mrole" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th width="3%">No</th>
					<th>Nama</th>
          <th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<!-- {{ @action }} -->
			</tbody>
		</table>
	</div>
</div>

<!-- @ -->
<div class="modal in" id="add-roles" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tambah Role</h3>
				</div>
				<div class="block-content">
          <!-- @ -->

          <div class="row">
            <div class="col-md-12">

              <form id="form-roles" class="form-horizontal push-10-t">
								<div class="col-md-12">

									<div class="form-group">
										<label for="" class="control-label col-md-3">Nama</label>
										<div class="col-md-8">
											<input id="nama-roles" class="form-control" type="text" name="nama-roles">
										</div>
									</div>

									<div class="form-group">
										<label for="" class="control-label col-md-3">Jenis</label>
										<div class="col-md-8">
											<label class="radio-inline">
												<input type="radio" name="jenis-roles" value="0">Kelompok
											</label>
											<label class="radio-inline">
												<input type="radio" name="jenis-roles" value="1">Rincian
											</label>
										</div>
									</div>

									<div class="form-group">
										<label for="" class="control-label col-md-3">Parent Id</label>
										<div class="col-md-8">
											<select id="parent-id" class="form-control js-select2" type="text" name="parent-id" style="width:100%;">
												<?= $list_parent ?>
											</select>
										</div>
									</div>

									<div class="form-group">
										<div class="col-md-3"></div>
										<div class="col-md-8">
											<button id="save-roles" class="btn btn-sm btn-primary" type="button" name="button">Save</button>
										</div>
									</div>

								</div>

								<input type="hidden" id="path" name="path" value="1" readonly>
								<input type="hidden" id="level" name="level" value="0" readonly>

              </form>

            </div>
          </div>

          <!-- @ -->
				</div>
				<div class="modal-footer">
					<div style="float:right;">
						<button type="button" class="btn btn-sm btn-default text-uppercase" data-dismiss="modal" style="width:100px;font-size:11px;">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- @ -->

<!-- @ -->
<div class="modal in" id="add-rules" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Rules</h3>
				</div>
				<div class="block-content">
          <!-- @ -->

          <div class="row">
            <div class="col-md-12">

							<table id="mroles_detail" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th width="3%">No</th>
										<th>Resource</th>
										<th width="10%">Allow</th>
										<th width="10%">Deny</th>
										<th width="10%">Inherited</th>
										<th hidden>Permission</th>
										<th hidden>Id</th>
									</tr>
								</thead>
								<tbody>
									<!-- @action -->
								</tbody>
							</table>

							<input type="hidden" id="idrole" value="">
							<input type="hidden" id="detailrole" value="">
            </div>
          </div>

          <!-- @ -->
				</div>
				<div class="modal-footer">
					<div style="float:right;">
						<button id="save-rules" type="button" class="btn btn-sm btn-success text-uppercase" style="width:100px;font-size:11px;">Save</button>
						<button type="button" class="btn btn-sm btn-default text-uppercase" data-dismiss="modal" style="width:100px;font-size:11px;">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- @ -->
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">

  $(document).ready(function () {
		$.ajax({
			url: '{base_url}mrole/get_child_level/' + 0,
			method: 'GET',
			dataType: 'json',
			success: function(data) {
				if ($('#parent-id option:selected').text() === 'Root') {
					$("#path").val($('#parent-id option:selected').val());
				} else {
					$("#path").val(data.path);
				}
				$("#level").val(data.level);
			}
		});

		$(document).on("change", "#parent-id", function() {
			$.ajax({
				url: '{base_url}mrole/get_child_level/' + $(this).val(),
				method: 'GET',
				dataType: 'json',
				success: function(data) {
					if ($('#parent-id option:selected').text() === 'Root') {
						$("#path").val($('#parent-id option:selected').val());
					} else {
						$("#path").val(data.path);
					}
					$("#level").val(data.level);
				}
			});
		});

		var showRole	= $('#mrole').DataTable({
			"oLanguage":{
				"sEmptyTable": "<i class='text-uppercase'>NO DATA AVAILABLE IN TABLE</i>",
			},
			"ajax": {
				"url":"{site_url}mrole/showRoles",
			},
			"autoWidth": false,
			"pageLength": 50,
			"lengthChange": true,
			"ordering": true,
			"processing": true,
			"order": []
		});

		$(document).on("click", "#save-roles", function() {
			$.ajax({
				url: '{site_url}mrole/save',
				type: 'POST',
				data: $('#form-roles').serialize(),
				complete: function() {
					swal("Berhasil!", "Menambahkan Role", "success").then(function() {
						window.location.assign('{site_url}mrole');
					});
					showRole.ajax.reload();
				}
			});
		});

		function getDetailTable() {
			var detail_table = $('#mroles_detail tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					return $(cell).html();
				});
			});

			$("#detailrole").val(JSON.stringify(detail_table));
		}

		$(document).on('click', '.delete-rules', function() {
			var id = $(this).attr('data-value');

			swal({
				title: "Anda Yakin ?",
				text : "Ingin menghapus data ini ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				$.ajax({
					url: '{site_url}mrole/deleteRole',
					type: 'POST',
					dataType: 'json',
					data: {id:id},
					complete: function(data) {
						showRole.ajax.reload();
					}
				});
			});
		});

		$(document).on('click', '.add-resource', function() {
			var id = $(this).attr('data-value');
			$('#idrole').val(id);

			$('#mroles_detail').DataTable().destroy();

			$('#mroles_detail').DataTable({
				"oLanguage":{
					"sEmptyTable": "<i class='text-uppercase'>NO DATA AVAILABLE IN TABLE</i>",
				},
				"ajax": {
					"url":"{site_url}mrole/showRules/"+id,
				},
				"scrollY": "200px",
				"searching": false,
				"autoWidth": false,
				"lengthChange": false,
				"ordering": false,
				"processing": true,
				"bInfo": false,
				"bPaginate": false,
				"order": [],
				"rowCallback": function(row, data, index) {
					$('td:eq(5)', row).attr('hidden', '');
					$('td:eq(6)', row).attr('hidden', '');
				},
				"initComplete": function(settings, json) {
					getDetailTable();
				}
			});


		});

		$(document).on('click', '.permission', function() {
			var permission = $(this).val();

			$(event.target).closest('tr').each(function() {
				$(this).find('td:eq(5)').text('').append(permission);
			});

			getDetailTable();
		});

		$(document).on('click', '#save-rules', function() {
			var id 		 = $('#idrole').val(),
					detail = $('#detailrole').val();

			$.ajax({
				url: '{site_url}mrole/saveDetail',
				type: 'POST',
				data: {id: id, detail: detail},
				complete: function() {
					swal("Berhasil!", "Mengatur Rule", "success").then(function() {
						window.location.assign('{site_url}mrole');
					});
				}
			});

		});
  });

</script>
