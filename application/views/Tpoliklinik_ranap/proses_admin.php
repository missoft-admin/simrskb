<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>
<?php $user_id = $this->session->userdata('user_id'); ?>
<style>
.text-muted {
  color: #919191;
  font-weight: normal;
  font-style: italic;
  
}
#sig canvas{
	width: 100% !important;
	height: auto;
}
.edited_final{
	border-color:#d26a5c;
	background-color:#fff2f1!important;
	font-weight: bold;
}
</style>
<?
	$dengan_diagnosa=trim(strip_tags($dengan_diagnosa));
	$rencana_tindakan=trim(strip_tags($rencana_tindakan));
	$login_ppa_id=$this->session->userdata('login_ppa_id');
	$login_nama_ppa=$this->session->userdata('login_nama_ppa');
?>
<div class="block push-10">
    <div class="block-content">
        <input type="hidden" id="assesmen_id" name="assesmen_id" value="{assesmen_id}">
        <input type="hidden" id="status_proses" name="status_proses" value="{status_proses}">
        <input type="hidden" id="status_persetujuan_pasien" name="status_persetujuan_pasien" value="{status_persetujuan_pasien}">
        <input type="hidden" id="disabel" name="disabel" value="{disabel}">
        <input type="hidden" id="pasien_ttd" name="pasien_ttd" value="{pasien_ttd}">
        <?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
        <div class="row pull-10">
			<div class="form-group">
				<div class="col-md-12">
					<h4 class="font-w700 push-5 text-center text-primary">{judul_per_ina}</h4>
					<h5 class="push-5 text-center"><i>{judul_per_eng}</i></h5>
					<h5 class="push-5 text-center text-muted">{nopermintaan}</h5>
				</div>
				
			</div>
			<div class="form-group">
				<div class="col-md-6 ">
					<div class="col-md-12 ">
						<label for="diagnosa">{nama_pasien_ina} / <i class="text-muted">{nama_pasien_eng}</i></label>
						<input tabindex="17" disabled type="text" class="form-control auto_blur " value="<?= $nomedrec_pasien.' - '.$nama_pasien.' - '.$jk_pasien ?>" required>
					</div>
					
				</div>
				<div class="col-md-6 ">
					<div class="col-md-12 ">
						<label for="diagnosa">{ttl_ina} / <i class="text-muted">{ttl_eng}</i></label>
						<input tabindex="17" disabled type="text" class="form-control auto_blur " value="<?=HumanDateShort($ttl_pasien).' - '.$umur_pasien ?> Tahun" required>
					</div>
					
				</div>
			</div>
			<div class="form-group">
					<div class="col-md-6 ">
						<div class="col-md-12 ">
							<label for="dengan_diagnosa">{diagnosa_per_ina} / <i class="text-muted">{diagnosa_per_eng}</i></label>
							<textarea class="form-control " disabled id="dengan_diagnosa" width="100%"  rows="4" placeholder="Dengan Diagnosa"> <?=$dengan_diagnosa?></textarea>
						</div>
					</div>
					<div class="col-md-6 ">
						<div class="col-md-12 ">
							<label for="fisio">{nama_tindakan_ina} / <i class="text-muted">{nama_tindakan_eng}</i></label>
							<textarea class="form-control " disabled id="rencana_tindakan"  width="100%"  rows="4" placeholder="Catatan"> <?=$rencana_tindakan?></textarea>
						</div>
					</div>
			</div>
			<div class="form-group" style="margin-bottom: 5px!important">
				<div class="col-md-6 ">
					<div class="col-md-4 ">
						<label for="diagnosa">{tipe_ina} <br><i class="text-muted">{tipe_eng}</i></label>
						<select tabindex="13" id="tipe" disabled class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
							<option value="" <?=($tipe == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
							<option value="3" <?=($tipe == 3 ? 'selected="selected"' : '')?>>RAWAT INAP</option>
							<option value="4" <?=($tipe == 4 ? 'selected="selected"' : '')?>>ODS</option>
							
						</select>
						
					</div>
					<div class="col-md-4 ">
						<label for="icu">{icu_ina} <br><i class="text-muted">{icu_eng}</i></label>
						<select tabindex="8" id="icu" disabled name="icu" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
							<?foreach(list_variable_ref(122) as $row){?>
							<option value="<?=$row->id?>" <?=($icu == $row->id ? 'selected="selected"' : '')?> <?=($icu == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
							<?}?>
						</select>
					</div>
					<div class="col-md-4 ">
						<label for="cito">{cito_ina} <br><i class="text-muted">{cito_eng}</i></label>
						<select tabindex="8" id="cito" disabled name="cito" class="js-select2 form-control opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi">
							<?foreach(list_variable_ref(123) as $row){?>
							<option value="<?=$row->id?>" <?=($icu == $row->id ? 'selected="selected"' : '')?> <?=($icu == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
							<?}?>
						</select>
					</div>
				</div>
				<div class="col-md-6 ">
					<div class="col-md-12 ">
						<label for="example-input-normal">{dokter_ina}<br><i class="text-muted">{dokter_eng}</i></label>
						<select id="dpjp" disabled class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
							<option value="" <?=($dpjp == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
							<?foreach(get_all('mdokter',array('status'=>1)) as $row){?>
							<option value="<?=$row->id?>" <?=($dpjp==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
							
						</select>
					</div>
				</div>
				
			</div>
			<div class="form-group"  style="margin-bottom: 25px!important">
				<div class="col-md-12 ">
					<div class="col-md-12 ">
						<label for="example-input-normal">{catatan_ina}<br><i class="text-muted">{catatan_eng}</i></label>
						<input tabindex="17" type="text" disabled id="catatan" class="form-control auto_blur " value="<?=$catatan?>" required>
					</div>
					
				</div>
			
			</div>
			<div class="form-group"  style="margin-bottom: 5px!important">
				<div class="col-md-12 ">
					<div class="col-md-12 ">
						<label for="example-input-normal">{rencana_implant_ina}<br><i class="text-muted">{rencana_implant_eng}</i></label>
					</div>
				</div>
			</div>
			<div class="form-group"  style="margin-bottom: 5px!important">
				<div class="col-md-12 ">
					<div class="col-md-12 ">
						<div class="table-responsive">
							<table class="table table-condensed table-bordered" id="index_implant">
								<thead>
									<tr>
										<th width="5%" class="text-center">{table_no_ina}<br><i class="text-muted">{table_no_eng}</i></th>
										<th width="25%" class="text-center">{table_alat_ina}<br><i class="text-muted">{table_alat_eng}</i></th>
										<th width="10%" class="text-center">{table_ukuran_ina}<br><i class="text-muted">{table_ukuran_eng}</i></th>
										<th width="10%" class="text-center">{table_merk_ina}<br><i class="text-muted">{table_merk_eng}</i></th>
										<th width="20%" class="text-center">{table_catatan_ina}<br><i class="text-muted">{table_catatan_eng}</i></th>
										<th width="8%" class="text-center">{table_jumlah_ina}<br><i class="text-muted">{table_jumlah_eng}</i></th>
										<th width="10%" class="text-center">{table_biaya_ina}<br><i class="text-muted">{table_biaya_eng}</i></th>
										<th width="15%" class="text-center">{table_aksi_ina}<br><i class="text-muted">{table_aksi_eng}</i></th>										   
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>	
					</div>
				</div>
			</div>
			<div class="form-group"  style="margin-bottom: 5px!important">
				<div class="col-md-12 ">
					<div class="col-md-12 ">
						<label for="example-input-normal">{table_footer_ina}<br><i class="text-muted">{table_footer_eng}</i></label>
					</div>
				</div>
			</div>
	   </div>

        <?php echo form_close() ?>
    </div>
</div>

        <?php echo form_open('#','class="form-horizontal" id="form-work2" target="_blank"') ?>
<div class="block push-10">
    <div class="block-content">
        <input type="hidden" id="tab" name="tab" value="{tab}">
         <div class="row pull-10">
			<div class="form-group" style="margin-bottom: 10px!important">
				<div class="col-md-12">
					<div class="col-md-3 ">
						<label for="kelompok_operasi_id">{kel_op_ina} <br><i class="text-muted">{kel_op_eng}</i> </label>
						<select id="kelompok_operasi_id" class="js-select2 form-control  opsi_change_header" style="width: 100%;" data-placeholder="Pilih Opsi" required>
							<option value="" <?=($kelompok_operasi_id == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
							<?foreach(get_all('mkelompok_operasi',array('status'=>1)) as $row){?>
							<option value="<?=$row->id?>" <?=($kelompok_operasi_id==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
							
						</select>
					</div>
					<div class="col-md-3">
						<label for="jenis_operasi_id">{jenis_ina} <br><i class="text-muted">{jenis_eng}</i></label>
						<select id="jenis_operasi_id" class="js-select2 form-control  opsi_change_header" style="width: 100%;" data-placeholder="Pilih Opsi" required>
							<option value="" <?=($jenis_operasi_id == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
							<?foreach(get_all('erm_jenis_operasi',array('status'=>1)) as $row){?>
							<option value="<?=$row->id?>" <?=($jenis_operasi_id==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
							
						</select>
					</div>
					<div class="col-md-2 ">
						<label for="icu">{lama_ina} <br><i class="text-muted">{lama_eng}</i></label>
						<select id="lama" class="js-select2 form-control  opsi_change_header" style="width: 100%;" data-placeholder="Pilih Opsi" required>
							<option value="" <?=($lama == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
							<?for($i=1;$i<100;$i++){?>
							<option value="<?=$i?>" <?=($lama==$i?'selected':'')?>><?=$i?></option>
							<?}?>
							
						</select>
					</div>
					<div class="col-md-2 ">
						<label for="icu_pilih">{icu_ina} <br><i class="text-muted">{icu_eng}</i></label>
						<select tabindex="8" id="icu_pilih" name="cito" class="js-select2 form-control opsi_change_header" style="width: 100%;" data-placeholder="Pilih Opsi">
							<?foreach(list_variable_ref(122) as $row){?>
							<option value="<?=$row->id?>" <?=($icu_pilih == $row->id ? 'selected="selected"' : '')?> <?=($icu == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
							<?}?>
						</select>
					</div>
					<div class="col-md-2 ">
						<label for="cito_pilih">{cito_ina} <br><i class="text-muted">{cito_eng}</i></label>
						<select tabindex="8" id="cito_pilih" name="cito" class="js-select2 form-control opsi_change_header" style="width: 100%;" data-placeholder="Pilih Opsi">
							<?foreach(list_variable_ref(123) as $row){?>
							<option value="<?=$row->id?>" <?=($cito_pilih == $row->id ? 'selected="selected"' : '')?> <?=($icu == '0' && $row->st_default=='1' ? 'selected="selected"' : '')?> ><?=$row->nama?></option>
							<?}?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 15px!important">
				<div class="col-md-12">
					<div class="col-md-3 ">
						<label for="kelompok_pasien_id">{kel_pasien_ina} <br><i class="text-muted">{kel_pasien_eng}</i></label>
						<select id="kelompok_pasien_id" class="js-select2 form-control  opsi_change_header" style="width: 100%;" data-placeholder="Pilih Opsi" required>
							<option value="" <?=($kelompok_pasien_id == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
							<?foreach(get_all('mpasien_kelompok',array('status'=>1)) as $row){?>
							<option value="<?=$row->id?>" <?=($kelompok_pasien_id==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
							
						</select>
					</div>
					<div class="col-md-3 ">
						<label for="idrekanan">{asuransi_ina} <br><i class="text-muted">{asuransi_eng}</i></label>
						<select id="idrekanan" class="js-select2 form-control  opsi_change_header" style="width: 100%;" data-placeholder="Pilih Opsi" required>
							<option value="0" <?=($idrekanan == '0' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
							<?if ($kelompok_pasien_id=='1'){?>
							<?foreach(get_all('mrekanan',array('status'=>1)) as $row){?>
							<option value="<?=$row->id?>" <?=($idrekanan==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
							<?}?>
							
						</select>
					</div>
					
					<div class="col-md-1 ">
						<label for="btn_estimasi">&nbsp;<br>&nbsp;&nbsp;</label>
						<button class="btn btn-success push-5-r push-10" id="btn_estimasi" onclick="get_tarif()" title="Hitung Estimasi" type="button"><i class="si si-calculator"></i> ESTIMASI</button>
					</div>
				</div>
			</div>
		</div>
        
	</div>
</div>

<div class="block push-10">
    <ul class="nav nav-pills">
		 <li id="div_3" class="<?=($tab=='3'?'active':'')?>">
            <a href="javascript:void(0)" onclick="set_tab(3)"> Kelas III</a>
        </li>
		 <li id="div_2" class="<?=($tab=='2'?'active':'')?>">
            <a href="javascript:void(0)"  onclick="set_tab(2)"> Kelas II</a>
        </li>
       
        <li id="div_1" class="<?=($tab=='1'?'active':'')?>">
            <a href="javascript:void(0)" onclick="set_tab(1)"> Kelas I</a>
        </li>
       
		<li id="div_4" class="<?=($tab=='4'?'active':'')?>">
            <a href="javascript:void(0)" onclick="set_tab(4)"> Kelas Utama</a>
        </li>
		<li id="div_0" class="<?=($tab=='0'?'active':'')?>">
            <a href="javascript:void(0)" onclick="set_tab(0)"> ODS</a>
        </li>
		
    </ul>
    <div class="block-content push-10">
        <input type="hidden" id="tab" name="tab" value="{tab}">
        <div class="row pull-10 ">
            <div class="form-group" style="margin-bottom: 25px!important">
				<div class="col-md-12">
					<div class="col-md-12 ">
						<label class="h4 text-primary">{section_biaya_ina}  / <i class="text-muted">{section_biaya_eng}</i></label>
						
					</div>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 5px!important">
				<div class="col-md-12">
					<div class="col-md-3 ">
						<label>{kamar_ina} <br><i class="text-muted">{kamar_eng}</i></label>
						<input type="text" readonly class="form-control input-sm number " id="harga_kamar_ko" value="{harga_kamar_ko}">
					</div>
					<div class="col-md-3 ">
						<label>{pakai_obat_ina} <br><i class="text-muted">{pakai_obat_eng}</i></label>
						<input type="text"  class="form-control input-sm number " id="harga_obat_ko" value="{harga_obat_ko}">
					</div>
					<div class="col-md-3 ">
						<label>{biaya_implan_ina} <br><i class="text-muted">{biaya_implan_eng}</i></label>
						<input type="text" readonly class="form-control input-sm number" id="harga_implan" value="{harga_implan}">
					</div>
					<div class="col-md-3 ">
						<label>{jasa_dokter_operator_ina} <br><i class="text-muted">{jasa_dokter_operator_eng}</i></label>
						<input type="text" class="form-control input-sm number " id="harga_do" value="{harga_do}">
					</div>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 15px!important">
				<div class="col-md-12">
					<div class="col-md-3 ">
						<label>{jasa_dokter_anes_ina} <br><i class="text-muted">{jasa_dokter_anes_eng}</i></label>
						<input type="text"  class="form-control input-sm number " id="harga_da" value="{harga_da}">
					</div>
					<div class="col-md-3 ">
						<label>{jasa_asisten_ina} <br><i class="text-muted">{jasa_asisten_eng}</i></label>
						<input type="text"  class="form-control input-sm number " id="harga_daa" value="{harga_daa}">
					</div>
					<div class="col-md-3 ">
						<label class="text-danger">{total_biaya_ina} <br><i class="text-muted">{total_biaya_eng}</i></label>
						<input type="text" disabled class="form-control input-sm number edited_final" id="total_biaya_bedah" value="{total_biaya_bedah}">
					</div>
					
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 25px!important">
				<div class="col-md-12">
					<div class="col-md-12 ">
						<label class="h4 text-primary">{section_biaya_ranap_ina}  / <i class="text-muted">{section_biaya_ranap_eng}</i></label>
						
					</div>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 5px!important">
				<div class="col-md-12">
					<div class="col-md-3 ">
						<label>{biaya_ruang_ina} <br><i class="text-muted">{biaya_ruang_eng}</i></label>
						<input type="text" readonly class="form-control input-sm number " id="harga_ruang" value="{harga_ruang}">
					</div>
					<div class="col-md-3 ">
						<label>{biaya_obat_ina} <br><i class="text-muted">{biaya_obat_eng}</i></label>
						<input type="text"  class="form-control input-sm number" id="harga_obat_ruang" value="{harga_obat_ruang}">
					</div>
					<div class="col-md-4 ">
						<label>{biaya_penunjang_ina} <br><i class="text-muted">{biaya_penunjang_eng}</i></label>
						<input type="text"  class="form-control input-sm number " id="harga_penunjang_ruang" value="{harga_penunjang_ruang}">
					</div>
					<div class="col-md-2 ">
						<label>{biaya_visit_ina} <br><i class="text-muted">{biaya_visit_eng}</i></label>
						<input type="text"  class="form-control input-sm number " id="harga_visit_ruang" value="{harga_visit_ruang}">
					</div>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 15px!important">
				<div class="col-md-12">
					<div class="col-md-4 ">
						<label>{biaya_icu_ina} <br><i class="text-muted">{biaya_icu_eng}</i></label>
						<div class="input-group">
							<input type="text" class="form-control input-sm number" id="jumlah_icu" value="{jumlah_icu}">
							<span class="input-group-addon">Rp.</span>
							<input type="text"  class="form-control input-sm number" id="harga_icu" value="{harga_icu}">
						</div>
						
					</div>
					<div class="col-md-2 ">
					</div>
					<div class="col-md-3 ">
						<label class="text-danger">{total_biaya_ranap_ina} <br><i class="text-muted">{total_biaya_ranap_eng}</i></label>
						<input type="text" disabled class="form-control input-sm number edited_final" id="total_biaya_ranap" value="{total_biaya_ranap}">
					</div>
					<div class="col-md-3 ">
						<label class="text-danger">{total_estimasi_biaya_ina} <br><i class="text-muted">{total_estimasi_biaya_eng}</i></label>
						<input type="text" disabled class="form-control input-sm number edited_final" id="total_estimasi_all" value="{total_estimasi_all}">
					</div>
					
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 15px!important">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-condensed table-bordered" id="index_keterangan">
								<thead>
									<tr>
										<th width="5%" class="text-center">{ket_no_ina}<br><i class="text-muted">{ket_no_eng}</i></th>
										<th width="70%" class="text-center">{ket_penjelasan_ina}<br><i class="text-muted">{ket_penjelasan_eng}</i></th>
										<th width="20%" class="text-center">{ket_paraf_ina}<br><i class="text-muted">{ket_paraf_eng}</i></th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>	
					</div>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 15px!important">
				<div class="col-md-12">
					<div class="col-md-12">
						<label class="">{yang_menjelaskan_ina} <br><i class="text-muted">{yang_menjelaskan_eng}</i></label>
					</div>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 15px!important">
				<div class="col-md-12">
					<div class="col-md-2 text-center">
						<label class="text-center">{petugas_biling_ina} <br><i class="text-muted">{petugas_biling_eng}</i></label>
					</div>
					<div class="col-md-2 text-center">
						<label class="text-center">{dpjp_ina} <br><i class="text-muted">{dpjp_eng}</i></label>
					</div>
					<div class="col-md-5 text-center">
						<label class="text-center">&nbsp;</label>
					</div>
					<div class="col-md-2 text-center">
						<label class="text-center">{ttd_pasien_ina} <br><i class="text-muted">{ttd_pasien_eng}</i></label>
					</div>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 15px!important">
				<div class="col-md-12">
					<div class="col-md-2 text-center">
						<img class="" style="width:100px;height:100px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($petugas_billilng_ppa_id?$petugas_billilng_ppa_id:$login_ppa_id)?>" alt="" title="">
					</div>
					<div class="col-md-2 text-center">
						<img class="" style="width:100px;height:100px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ttd_dokter/<?=$dpjp?>" alt="" title="">
					</div>
					<div class="col-md-5 text-center">
						<label class="text-center">&nbsp;</label>
					</div>
					<div class="col-md-2 text-center">
						<?if ($pasien_ttd){?>
							<div class="img-container fx-img-rotate-r">
								<img class="img-responsive" src="<?=$pasien_ttd?>" alt="">
								<div class="img-options">
									<div class="img-options-content">
										<div class="btn-group btn-group-sm">
											<a class="btn btn-default" onclick="modal_ttd()" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
											<a class="btn btn-default btn-danger" onclick="hapus_ttd()" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
										</div>
									</div>
								</div>
							</div>
						<?}else{?>
							<button class="btn btn-sm btn-success" onclick="modal_ttd()" id="btn_ttd_final" type="button"><i class="fa fa-paint-brush"></i> SIGNATURE</button>
						<?}?>
						
						
					</div>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 15px!important">
				<div class="col-md-12">
					<div class="col-md-2 text-center">
						<label class="text-center"><?=($petugas_billing_ttd?$petugas_billing_ttd:$login_nama_ppa)?></label>
					</div>
					<div class="col-md-2 text-center">
						<label class="text-center"><?=$nama_dpjp?></label>
					</div>
					<div class="col-md-5 text-center">
						<label class="text-center">&nbsp;</label>
					</div>
					<div class="col-md-2 text-center">
						<label class="text-center"><?=($nama_pasien_ttd?$nama_pasien_ttd:'')?></label>
					</div>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 15px!important">
				<div class="col-md-12">
					<div class="col-md-4 ">
						<div class="col-md-12 ">
							<label for="example-input-normal">{kelas_diambil_ina}<br><i class="text-muted">{kelas_diambil_eng}</i></label>
							<select id="kelas" class="js-select2 form-control  opsi_change_header" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="" <?=($kelas == '' ? 'selected="selected"' : '')?>>Pilih Opsi</option>
								<?foreach(get_all('mkelas',array('status'=>1)) as $row){?>
								<option value="<?=$row->id?>" <?=($kelas==$row->id?'selected':'')?>><?=$row->nama?></option>
								<?}?>
								<option value="0" <?=($kelas == '0' ? 'selected="selected"' : '')?>>ODS</option>
							</select>
						</div>
					</div>
					
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 15px!important">
				<div class="col-md-12">
					
					<div class="col-md-12 ">
						<div class="col-md-12  text-right">
							<label for="example-input-normal">&nbsp;<br>&nbsp;&nbsp;</label>
							<?if ($disabel==''){?>
							<?if ($status_persetujuan_pasien<2){?>
							<button class="btn btn-success btn_setuju btn_aksi" onclick="simpan_setuju(2)" type="button"><i class="si si-check"></i> PASIEN SETUJU</button>
							<button class="btn btn-danger btn_aksi" onclick="show_menolak()" type="button"><i class="si si-ban"></i> PASIEN MENOLAK</button>
							<?}?>
							<?if ($status_persetujuan_pasien=='0'){?>
							<button class="btn btn-warning btn_aksi" onclick="simpan_setuju(1)" type="button"><i class="fa fa-spinner"></i> PASIEN BERUNDING</button>
							<button class="btn btn-primary" onclick="simpan_proses(1)" type="button"><i class="fa fa-save"></i> UPDATE</button>
							<button class="btn btn-info" onclick="simpan_proses(2)" type="button"><i class="fa fa-send-o"></i> UPDATE & KELUAR</button>
							<?}else{?>
							<a class="btn btn-default" onclick="goBack()" type="button"><i class="si si-action-undo"></i> KEMBALI</a>
							<?}?>
							<?}else{?>
							<a class="btn btn-default" onclick="goBack()" type="button"><i class="si si-action-undo"></i> KEMBALI</a>
							<?}?>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>
<?php echo form_close() ?>
<div class="modal in" id="modal_menolak" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Alasan Menolak</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12">
										<label class="text-center">Alasan Menolak</label>
										<input type="text" class="form-control input-sm" id="alasan_menolak" value="{alasan_menolak}">
									</div>
								</div>
							</div>
							
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-success" type="button" onclick="simpan_setuju(3)"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_ttd" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Tanda Tangan Pasien / Penanggung Jawab </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig_2" ></div>
									</div>
								</div>
								<textarea id="signature64_2" name="signed_2" style="display: none"><?=$pasien_ttd?></textarea>
							</div>
							<input type="hidden" readonly id="ttd_id_2" name="ttd_id_2" value="{id}">
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<label class="text-center">{ttd_pasien_ina} <br><i class="text-muted">{ttd_pasien_eng}</i></label>
										<input type="text" class="form-control input-sm" id="nama_pasien_ttd" value="{nama_pasien_ttd}">
									</div>
								</div>
							</div>
							
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear_2"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" id="btn_save_ttd"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_paraf" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Paraf Pasien</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12 text-center">
										<div id="sig" ></div>
									</div>
								</div>
								<input type="hidden" readonly id="ttd_id" name="ttd_id" value="">
								<input type="hidden" readonly id="nama_tabel" name="nama_tabel" value="">
								<textarea id="signature64" name="signed" style="display: none"></textarea>
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" id="clear"><i class="fa fa-refresh"></i> Hapus</button>
							<button class="btn btn-sm btn-success" type="button" id="btn_save_paraf"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>

<script type="text/javascript" src="{js_path}ttd/jquery.signature.min.js"></script>
<script type="text/javascript" src="{js_path}ttd/jquery.ui.touch-punch.min.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}ttd/jquery.signature.css">
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
var table;
var tab;
var total_implan=0;
var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
$('#clear').click(function(e) {
	e.preventDefault();
	sig.signature('clear');
	$("#signature64").val('');
});
var sig_2 = $('#sig_2').signature({syncField: '#signature64_2', syncFormat: 'PNG'});
$('#clear_2').click(function(e) {
	e.preventDefault();
	sig_2.signature('clear');
	$("#signature64_2").val('');
});
function goBack() {
  window.history.back();
}


$(document).ready(function() {
	load_implant();
	load_keterangan();
	disable_edit();
});
function show_menolak(){
	$("#modal_menolak").modal('show');
}
function modal_ttd(){
	$("#modal_ttd").modal('show');
}
$(".opsi_change_header").change(function(){
	simpan_auto_update();
});
$(document).on("click","#btn_save_ttd",function(){
	
	var nama_pasien_ttd=$("#nama_pasien_ttd").val();
	var signature64=$("#signature64_2").val();
	if (nama_pasien_ttd==''){
		swal({
			title: "Konfirmasi!",
			text: "Silahkan Isi Nama Pasien.",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	if (signature64==''){
		swal({
			title: "Konfirmasi!",
			text: "Silahkan Tandatangani Document",
			type: "error",
			timer: 1500,
			showConfirmButton: false
		});
		return false;
	}
	var assesmen_id=$("#assesmen_id").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpoliklinik_ranap/save_ttd_2/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				assesmen_id:assesmen_id,signature64:signature64,nama_pasien_ttd:nama_pasien_ttd
		  },success: function(data) {
				location.reload();
			}
		});
	
});
function hapus_ttd(){
	var assesmen_id=$("#assesmen_id").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Batal Tandatangan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		$.ajax({
			  url: '{site_url}Tpoliklinik_ranap/hapus_ttd/',
			  dataType: "json",
			  type: 'POST',
			  data: {
					assesmen_id:assesmen_id,
					// nama_tabel:nama_tabel,
			  },success: function(data) {
					location.reload();
				}
		});

	});
	
}
$('#index_implant tbody').on('click', '.input_edit', function() {
	$(this).select();
	$(this).focus();
});
$('#index_implant tbody').on('blur', '.input_edit', function() {
	console.log($(this));
	sum_total_implan();
	update_record($(this));
});
$('#index_implant tbody').on('click', '.chck_pilih', function() {
	let id = $(this).data("id");
	let check = $(this).is(":checked");
	if (check==false){
		 $(this).closest('tr').find(".pilih").val(0);
		 $(this).closest('tr').find(".total_biaya_implant").prop("disabled", true);
	}else{
		 $(this).closest('tr').find(".pilih").val(1);
		 $(this).closest('tr').find(".total_biaya_implant").removeAttr('disabled');
		
	}
	update_record($(this));
	sum_total_implan();
	// alert(check);
});
function sum_total_implan(){
	total_implan=0;
	$('#index_implant tbody tr').each(function() {
		let total_biaya_implant=$(this).find(".total_biaya_implant").val();
		let pilih=$(this).find(".pilih").val();
		if (pilih=='1'){
			total_implan=parseFloat(total_implan)+parseFloat(total_biaya_implant);
		}
	});
	$("#harga_implan").val(total_implan);
	
	
	let total_biaya_bedah=0;
	let harga_kamar_ko=parseFloat($("#harga_kamar_ko").val());
	let harga_obat_ko=parseFloat($("#harga_obat_ko").val());
	let harga_implan=parseFloat($("#harga_implan").val());
	let harga_do=parseFloat($("#harga_do").val());
	let harga_da=parseFloat($("#harga_da").val());
	let harga_daa=parseFloat($("#harga_daa").val());
	total_biaya_bedah=harga_kamar_ko+harga_obat_ko+harga_implan+harga_do+harga_da+harga_daa;
	$("#total_biaya_bedah").val(total_biaya_bedah);
	
	let total_biaya_ranap=0;
	total_biaya_ranap=parseFloat($("#harga_ruang").val()) + parseFloat($("#harga_obat_ruang").val()) + parseFloat($("#harga_penunjang_ruang").val()) + parseFloat($("#harga_visit_ruang").val()) + parseFloat($("#harga_icu").val())
	$("#total_biaya_ranap").val(total_biaya_ranap);
	$("#total_estimasi_all").val(parseFloat(total_biaya_ranap) + parseFloat(total_biaya_bedah));
	disable_edit();
	$('#cover-spin').hide();
}
function disable_edit(){
	let disabel=$("#disabel").val();
	let pasien_ttd=$("#pasien_ttd").val();
	let total_estimasi_all=$("#total_estimasi_all").val();
	// alert(pasien_ttd);
	if (disabel=='disabled'){
		$("#form-work :input").prop("disabled", true);
		$("#form-work2 :input").prop("disabled", true);
		$(".btn_ttd_sc").prop("disabled", true);
		
		// $('.js-summernote').removeClass('js-summernote');
		// $(".btn_kolom").prop("disabled", true);
		// // $('#dengan_diagnosa').summernote('disable');
		// $(".btn_cetak").removeAttr('disabled');
	}else{
		
		console.log(total_estimasi_all);
		if (total_estimasi_all=='0'){
			$(".btn_aksi").prop("disabled", true);
		}else{
			$(".btn_aksi").removeAttr('disabled');
		}
		if (pasien_ttd==''){
			$(".btn_setuju").prop("disabled", true);
		}
	}
	
}
function modal_faraf($pendaftaran_id,$nama_tabel){
	$("#ttd_id").val($pendaftaran_id);
	$("#nama_tabel").val($nama_tabel);
	$("#modal_paraf").modal('show');
}
$(document).on("change",".nilai_sc",function(){
	// alert($(this));
	var tr=$(this).closest('tr');
	var sc_id=tr.find(".sc_id").val();
	var nilai_id=$(this).val();
	$.ajax({
		  url: '{site_url}Tpoliklinik_ranap/update_nilai_sc/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				sc_id:sc_id,nilai_id:nilai_id,
		  },success: function(data) {
				// validate_sc();
			}
		});
	
});
function simpan_auto_update(){
	$.ajax({
		url: '{site_url}Tpoliklinik_ranap/simpan_header_estimasi', 
		dataType: "JSON",
		method: "POST",
		data : {
				kelompok_operasi_id : $("#kelompok_operasi_id").val(),
				jenis_operasi_id : $("#jenis_operasi_id").val(),
				lama : $("#lama").val(),
				icu_pilih : $("#icu_pilih").val(),
				cito_pilih : $("#cito_pilih").val(),
				kelompok_pasien_id : $("#kelompok_pasien_id").val(),
				idrekanan : $("#idrekanan").val(),
				kelas : $("#kelas").val(),
				assesmen_id : $("#assesmen_id").val(),
				jumlah_icu : $("#jumlah_icu").val(),
			},
		success: function(data) {
					console.log(data);
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan Assesmen.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
				
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save Draft'});
				
			}
		}
	});
}
function simpan_proses(st_close){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_ranap/simpan_proses', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id : $("#assesmen_id").val(),
				kelompok_operasi_id : $("#kelompok_operasi_id").val(),
				jenis_operasi_id : $("#jenis_operasi_id").val(),
				lama : $("#lama").val(),
				icu_pilih : $("#icu_pilih").val(),
				cito_pilih : $("#cito_pilih").val(),
				kelompok_pasien_id : $("#kelompok_pasien_id").val(),
				idrekanan : $("#idrekanan").val(),
				kelas : $("#kelas").val(),
				harga_kamar_ko : $("#harga_kamar_ko").val(),
				harga_obat_ko : $("#harga_obat_ko").val(),
				harga_implan : $("#harga_implan").val(),
				harga_do : $("#harga_do").val(),
				harga_da : $("#harga_da").val(),
				harga_daa : $("#harga_daa").val(),
				total_biaya_bedah : $("#total_biaya_bedah").val(),
				harga_ruang : $("#harga_ruang").val(),
				harga_obat_ruang : $("#harga_obat_ruang").val(),
				harga_penunjang_ruang : $("#harga_penunjang_ruang").val(),
				harga_visit_ruang : $("#harga_visit_ruang").val(),
				jumlah_icu : $("#jumlah_icu").val(),
				harga_icu : $("#harga_icu").val(),
				total_biaya_ranap : $("#total_biaya_ranap").val(),
				total_estimasi_all : $("#total_estimasi_all").val(),
				status_proses : $("#status_proses").val(),
			},
		success: function(data) {
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Proses Berhasil Disimpan'});
			if (st_close=='2'){
				$("#cover-spin").show();
				window.location.href = "<?php echo site_url('tpoliklinik_ranap/estimasi_admin'); ?>";
			}
			
		}
	});
}
function simpan_setuju(status_persetujuan_pasien){
	$("#status_persetujuan_pasien").val(status_persetujuan_pasien);
	if (status_persetujuan_pasien!='3'){
		$("#alasan_menolak").val('');
	}
	$("#modal_menolak").modal('hide');
	let label_nama='';
	if (status_persetujuan_pasien=='1'){
		label_nama='Pasien Memilih Berunding ?';
	}
	if (status_persetujuan_pasien=='2'){
		label_nama='Pasien Memilih Setuju ?';
	}
	if (status_persetujuan_pasien=='3'){
		label_nama='Pasien Memilih Menolak ?';
	}
	swal({
		title: "Anda Yakin ?",
		text : label_nama,
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		simpan_setuju_direct();
	});
}
function simpan_setuju_direct(){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_ranap/simpan_setuju', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id : $("#assesmen_id").val(),
				jenis_operasi_id : $("#jenis_operasi_id").val(),
				lama : $("#lama").val(),
				icu_pilih : $("#icu_pilih").val(),
				cito_pilih : $("#cito_pilih").val(),
				kelompok_pasien_id : $("#kelompok_pasien_id").val(),
				idrekanan : $("#idrekanan").val(),
				kelas : $("#kelas").val(),
				harga_kamar_ko : $("#harga_kamar_ko").val(),
				harga_obat_ko : $("#harga_obat_ko").val(),
				harga_implan : $("#harga_implan").val(),
				harga_do : $("#harga_do").val(),
				harga_da : $("#harga_da").val(),
				harga_daa : $("#harga_daa").val(),
				total_biaya_bedah : $("#total_biaya_bedah").val(),
				harga_ruang : $("#harga_ruang").val(),
				harga_obat_ruang : $("#harga_obat_ruang").val(),
				harga_penunjang_ruang : $("#harga_penunjang_ruang").val(),
				harga_visit_ruang : $("#harga_visit_ruang").val(),
				jumlah_icu : $("#jumlah_icu").val(),
				harga_icu : $("#harga_icu").val(),
				total_biaya_ranap : $("#total_biaya_ranap").val(),
				total_estimasi_all : $("#total_estimasi_all").val(),
				status_persetujuan_pasien : $("#status_persetujuan_pasien").val(),
				alasan_menolak : $("#alasan_menolak").val(),
			},
		success: function(data) {
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Proses Berhasil Disimpan'});
			
			$("#cover-spin").show();
			window.location.href = "<?php echo site_url('tpoliklinik_ranap/estimasi_admin'); ?>";
		
		}
	});
}
function validate_icu(){
	if ($("#icu_pilih").val()=='1'){
		if (parseFloat($("#jumlah_icu").val())==0){
			$("#jumlah_icu").val(1);
			
		}
		$("#jumlah_icu").removeAttr('disabled');
		$("#harga_icu").removeAttr('disabled');
	}else{
		$("#jumlah_icu").val(0);
		$("#jumlah_icu").prop("disabled", true);
		$("#harga_icu").prop("disabled", true);
	}
}
function get_tarif(){
	validate_icu();
	$('#cover-spin').show();
	$.ajax({
		url: '{site_url}Tpoliklinik_ranap/get_tarif', 
		dataType: "JSON",
		method: "POST",
		data : {
				kelompok_operasi_id : $("#kelompok_operasi_id").val(),
				jenis_operasi_id : $("#jenis_operasi_id").val(),
				lama : $("#lama").val(),
				icu_pilih : $("#icu_pilih").val(),
				cito_pilih : $("#cito_pilih").val(),
				kelompok_pasien_id : $("#kelompok_pasien_id").val(),
				idrekanan : $("#idrekanan").val(),
				kelas : $("#kelas").val(),
				assesmen_id : $("#assesmen_id").val(),
				jumlah_icu : $("#jumlah_icu").val(),
				lama : $("#lama").val(),
			},
		success: function(data) {
			console.log(data);
			$("#harga_kamar_ko").val(data.harga_kamar_ko);
			$("#harga_obat_ko").val(data.harga_obat_ko);
			$("#harga_do").val(data.harga_do);
			$("#harga_da").val(data.harga_da);
			$("#harga_daa").val(data.harga_daa);
			$("#harga_ruang").val(data.harga_ruang);
			$("#harga_obat_ruang").val(data.harga_obat_ruang);
			$("#harga_penunjang_ruang").val(data.harga_penunjang_ruang);
			$("#harga_visit_ruang").val(data.harga_visit_ruang);
			$("#harga_icu").val(data.harga_icu);
			sum_total_implan();
		}
	});
}
function hapus_paraf($pendaftaran_id,$nama_tabel){
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpoliklinik_ranap/hapus_paraf/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				pendaftaran_id:$pendaftaran_id,
				nama_tabel:$nama_tabel,
		  },success: function(data) {
				$("#modal_paraf").modal('hide');
				load_keterangan();
				$("#cover-spin").hide();
			}
		});
}

$(document).on("click","#btn_save_paraf",function(){
	var pendaftaran_id=$("#ttd_id").val();
	var signature64=$("#signature64").val();
	var nama_tabel=$("#nama_tabel").val();
	$nama_tabel=$("#nama_tabel").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpoliklinik_ranap/save_ttd/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				pendaftaran_id:pendaftaran_id,signature64:signature64,nama_tabel:nama_tabel,
		  },success: function(data) {
				$("#modal_paraf").modal('hide');
				if ($nama_tabel=='tpoliklinik_ranap_rencana_biaya_keterangan'){
					load_keterangan();
				}
				
				$("#cover-spin").hide();
			}
		});
	
});
$(document).on("change","#kelas",function(){
	set_tab($(this).val());
});
$(document).on("click","#btn_save_paraf",function(){
	var pendaftaran_id=$("#ttd_id").val();
	var signature64=$("#signature64").val();
	var nama_tabel=$("#nama_tabel").val();
	$nama_tabel=$("#nama_tabel").val();
	$("#cover-spin").show();
	$.ajax({
		  url: '{site_url}Tpoliklinik_ranap/save_ttd/',
		  dataType: "json",
		  type: 'POST',
		  data: {
				pendaftaran_id:pendaftaran_id,signature64:signature64,nama_tabel:nama_tabel,
		  },success: function(data) {
				$("#modal_paraf").modal('hide');
				if ($nama_tabel=='tpoliklinik_ranap_rencana_biaya_keterangan'){
					load_keterangan();
				}
				
				$("#cover-spin").hide();
			}
		});
	
});
function set_tab($tab) {
    tab = $tab;
	$("#kelas").val(tab).trigger('change.select2');
    document.getElementById("div_0").classList.remove("active");
    document.getElementById("div_1").classList.remove("active");
    document.getElementById("div_2").classList.remove("active");
    document.getElementById("div_3").classList.remove("active");
    document.getElementById("div_4").classList.remove("active");
    if (tab == '1') {
        document.getElementById("div_1").classList.add("active");
    }
    if (tab == '2') {
        document.getElementById("div_2").classList.add("active");
    }
    if (tab == '3') {
        document.getElementById("div_3").classList.add("active");
    }
	if (tab == '4') {
        document.getElementById("div_4").classList.add("active");
    }
	if (tab == '0') {
        document.getElementById("div_0").classList.add("active");
    }

    get_tarif();
}
function update_record(tabel){
	let transaksi_id=tabel.closest('tr').find(".trx_id").val();
	let jumlah_implant=tabel.closest('tr').find(".jumlah_implant").val();
	let total_biaya_implant=tabel.closest('tr').find(".total_biaya_implant").val();
	let pilih=tabel.closest('tr').find(".pilih").val();
	
	$.ajax({
		url: '{site_url}Tpoliklinik_ranap/update_record', 
		dataType: "JSON",
		method: "POST",
		data : {
				transaksi_id : transaksi_id,
				jumlah_implant : jumlah_implant,
				total_biaya_implant : total_biaya_implant,
				pilih : pilih,
			},
		success: function(data) {
			
			if (data==null){
				swal({
					title: "Gagal!",
					text: "Simpan Racikan E-Resep.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
					$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
				}
		}
	});
}

function load_implant(){
	$('#index_implant').DataTable().destroy();	
	table = $('#index_implant').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			"columnDefs": [
					{ "width": "10%", "targets": [7],  className: "text-center" },
				],
            ajax: { 
                url: '{site_url}Tpoliklinik_ranap/load_implant_edit', 
                type: "POST" ,
                dataType: 'json',
				data : {
						assesmen_id: $("#assesmen_id").val(),
			   },
				
            },
			"drawCallback": function( settings ) {
			 	$(".number").number(true,0,'.',',');
				sum_total_implan();
				disable_edit();
		 }  
        });
}
function load_keterangan(){
	let assesmen_id=$("#assesmen_id").val();
	$.ajax({
			url: '{site_url}Tpoliklinik_ranap/load_index_keterangan', 
			dataType: "JSON",
			method: "POST",
			data : {
					assesmen_id:assesmen_id,
				},
			success: function(data) {
				$("#index_keterangan tbody").empty();
				$("#index_keterangan tbody").append(data.tabel);
				$(".nilai_sc").select2();
			}
		});
	$('#index_implant').DataTable().destroy();	
	table = $('#index_implant').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			"columnDefs": [
					{ "width": "10%", "targets": [7],  className: "text-center" },
				],
            ajax: { 
                url: '{site_url}Tpoliklinik_ranap/load_implant_edit', 
                type: "POST" ,
                dataType: 'json',
				data : {
						assesmen_id: $("#assesmen_id").val(),
			   },
				
            },
			"drawCallback": function( settings ) {
			 	$(".number").number(true,0,'.',',');
				sum_total_implan();
				disable_edit();
		 }  
        });
}
</script>
