<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title><?=$title?></title>
	<style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
		@page {
            margin-top: 1,8em;
            margin-left: 2,3em;
            margin-right: 2em;
            margin-bottom: 2.5em;
        }
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }

      th {
        padding: 5px;
      }
	  td {
        padding: 5px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }

      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;

      }
	  td {
        padding: 3px;

      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
		vertical-align:top;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .text-normal{
		font-size: 14px !important;
      }
	  .text-header{
		font-size: 13px !important;
      }
	  .text-judul{
        font-size: 18px  !important;
        font-weight: bold;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 20px 0;
		}
		fieldset {
			border:1px solid #000;
			border-radius:4px;
			box-shadow:0 0 0px #000;
		}
		legend {
			background:#fff;
		}
    }


  </style>
</head>

<body>
	<header>
		
	</header>
	<footer>
		<p style="position:fixed; bottom:40px; font-size: 18px; color: #3abad8;">
        <?=$alamat_rs1?><br>T. <?=$telepon_rs?>F. <?=$fax_rs?> E. <?=$email_rs?> ; <?=$web_rs?>
      </p>
	</footer>
	<main>
	<table class="content">
		<tr>
			<td width="100%" class="text-center"><img src="<?=base_url().$logo2_rs?>" alt="" width="100" height="100"></td>
		</tr>
		<tr>
			<td width="100%" class="text-center text-normal"><?=$alamat_rs1?><br><?=$telepon_rs?> <?=$fax_rs?><br><?=$web_rs?> Email : <?=$email_rs?></td>
		</tr>
		
		
	</table>
	<br>
	<br>
	<br>
	<table class="content">
		<tr>
			<td width="100%" class="text-center text-judul"><?=$judul_per_ina?><br><i><?=$judul_per_eng?></i></td>
		</tr>
		<tr>
			<td width="100%" class="text-center text-judul"><?=$nopermintaan?><br></td>
		</tr>
		
	</table>
	<br>
	<br>
	<table class="content">
		<tr>
			<td colspan="3" class="text-left text-normal"><strong><?=strip_tags($paragraph_1_ina)?></strong><br><i><?=strip_tags($paragraph_1_eng)?></i></td>
		</tr>
		<tr>
			<td colspan="3" class="text-left text-normal"><strong><?=strip_tags($paragraph_2_ina)?></strong><br><i><?=strip_tags($paragraph_2_eng)?></i></td>
		</tr>
		<tr>
			<td class="text-left text-normal" width="47%"><strong><?=strip_tags($nama_pasien_ina)?></strong> / <i><?=strip_tags($nama_pasien_eng)?></i></td>
			<td class="text-left text-normal" width="5%"></td>
			<td class="text-left text-normal" width="48%"><strong><?=strip_tags($ttl_ina)?></strong> / <i><?=strip_tags($ttl_eng)?></i></td>
		</tr>
		<tr>
			<td class="text-left text-normal" width="47%">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<strong><?=$nama_pasien?> - <?=$nomedrec_pasien?> - <?=$jenis_kelamin?></strong>
				</fieldset>
			</td>
			<td class="text-left text-normal" width="5%"></td>
			<td class="text-left text-normal" width="48%">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<strong><?=HumanDateShort($ttl_pasien)?> - <?=$umur_pasien?> tahun</strong>
				</fieldset>
			
			</td>
		</tr>
		<tr>
			<td class="text-left text-normal" width="47%"><strong><?=strip_tags($diagnosa_per_ina)?></strong> / <i><?=strip_tags($diagnosa_per_eng)?></i></td>
			<td class="text-left text-normal" width="5%"></td>
			<td class="text-left text-normal" width="48%"><strong><?=strip_tags($catatan_ina)?></strong> / <i><?=strip_tags($catatan_eng)?></i></td>
		</tr>
		<tr>
			<td class="text-left text-normal" width="47%">
				<fieldset>
				<legend style="font-weight:bold;min-height: 200px;"></legend>
				<?=strip_tags($dengan_diagnosa)?>
				</fieldset>
			</td>
			<td class="text-left text-normal" width="5%"></td>
			<td class="text-left text-normal" width="48%">
				<fieldset style="min-height:70px !important;">
				<legend style="font-weight:bold;;"></legend>
				<?=strip_tags($catatan)?> 
				</fieldset>
			
			</td>
		</tr>
	
		
	</table>
	<table class="content">
		<tr>
			<td class="text-left text-normal" style="width:20%"><strong><?=strip_tags($tipe_ina)?></strong> <br> <i><?=strip_tags($tipe_eng)?></i></td>
			<td class="text-left text-normal" style="width:2%"></td>
			<td class="text-left text-normal" style="width:26%"><strong><?=strip_tags($dilakukan_pemeriksaan_ina)?></strong> <br> <i><?=strip_tags($dilakukan_pemeriksaan_eng)?></i></td>
			<td class="text-left text-normal" style="width:2%"></td>
			<td class="text-left text-normal" style="width:26%"><strong><?=strip_tags($rencana_pelayanan_ina)?></strong> <br><i><?=strip_tags($rencana_pelayanan_eng)?></i></td>
			<td class="text-left text-normal" style="width:2%"></td>
			<td class="text-left text-normal" style="width:20%"><strong><?=strip_tags($dpjp_ina)?></strong> <br> <i><?=strip_tags($dpjp_eng)?></i></td>
		</tr>
		<tr>
			<td class="text-left text-normal" width="20%">
				<fieldset style="min-height:70px !important;">
				<legend style="font-weight:bold;;"></legend>
				<?=($tipe=='3'?'RAWAT INAP':'ODS')?>
				</fieldset>
			</td>
			<td class="text-left text-normal" width="5%"></td>
			<td class="text-left text-normal" width="25%">
				<fieldset style="min-height:70px !important;">
				<legend style="font-weight:bold;"></legend>
				<?=($rencana_pemeriksaan_str?$rencana_pemeriksaan_str:'&nbsp;&nbsp;&nbsp;')?>
				</fieldset>
			</td>
			<td class="text-left text-normal" width="5%"></td>
			<td class="text-left text-normal " width="25%">
				<fieldset style="min-height:70px !important;">
				<legend style="font-weight:bold;"></legend>
				<?=($rencana_pelayanan_str?$rencana_pelayanan_str:'&nbsp;&nbsp;')?>
				</fieldset>
			</td>
			<td class="text-left text-normal " width="5%"></td>
			<td class="text-left text-normal " width="20%">
				<fieldset style="min-height:70px !important;">
				<legend style="font-weight:bold;"></legend>
				<?=($nama_dokter)?>
				</fieldset>
			</td>
		</tr>
		
	</table>
	<br>
	<br>
	<br>
	<table class="content">
		<tr>
			<td colspan="3" class="text-left text-normal"><strong><?=strip_tags($paragraph_3_ina)?></strong><br><i><?=strip_tags($paragraph_3_eng)?></i></td>
		</tr>
		<tr>
			<td colspan="3" class="text-left text-normal">Jam Input : <?=HumanDateLong($created_date)?> | Jam Cetak : <?=date('d-m-Y H:i:s')?><br><?=strip_tags($footer_ina)?></td>
		</tr>
		<tr>
			<td style="width:30%" class="text-left text-normal"></td>
			<td style="width:30%" class="text-left text-normal"></td>
			<td style="width:40%" class="text-center text-normal">Dokter Pengirim</strong>
			<br><img class="" style="width:100px;height:100px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ttd_dokter/<?=$iddokter_ttd?>" alt="" title=""><br>( <?=$nama_dokter_ttd?> )</td>
		</tr>
		
	</table>
	<br>
	</main>
</body>

</html>
