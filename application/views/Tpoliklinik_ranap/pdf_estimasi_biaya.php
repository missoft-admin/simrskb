<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title><?=$title?></title>
	<style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
	@page {
		margin-top: 1em;
		margin-left: 2,3em;
		margin-right: 2em;
		margin-bottom: 1em;
	}
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }

      th {
        padding: 5px;
      }
	  td {
        padding: 5px;
		
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }

      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		
      }
      th {
        padding: 5px;

      }
	  td {
        padding: 3px;

      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
		vertical-align:top;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .text-normal{
		font-size: 14px !important;
      }
	  .text-normal-kecil{
		font-size: 12px !important;
      }
	  .text-header{
		font-size: 16px !important;
		font-weight:bold;
      }
	  .text-judul{
        font-size: 16px  !important;
        
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 20px 0;
		}
		fieldset {
			border:1px solid #000;
			border-radius:4px;
			box-shadow:0 0 0px #000;
			margin-top: -4px !important;
		}
		legend {
			background:#fff;
		}
    }

	.text-muted {
	  color: #000;
	  font-weight: normal;
	  font-style: italic;
	  
	}
  </style>
</head>

<body>
	<header>
		
	</header>
	<footer>
		<p style="position:fixed; bottom:40px; font-size: 18px; color: #3abad8;">
        <?=$alamat_rs1?><br>T. <?=$telepon_rs?>F. <?=$fax_rs?> E. <?=$email_rs?> ; <?=$web_rs?>
      </p>
	</footer>
	<main>
	<table class="content">
		<tr>
			<td width="100%" class="text-center"><img src="<?=base_url().$logo2_rs?>" alt="" width="100" height="100"></td>
		</tr>
		<tr>
			<td width="100%" class="text-center text-normal"><?=$alamat_rs1?><br><?=$telepon_rs?> <?=$fax_rs?><br><?=$web_rs?> Email : <?=$email_rs?></td>
		</tr>
	</table>
	<table class="content">
		<tr>
			<td width="100%" class="text-center text-judul"><strong><?=$judul_per_ina?></strong><br><i><?=$judul_per_eng?></i><br><i><?=$nopermintaan?></i></td>
		</tr>
		
	</table>
	<table class="content ">
		<tr>
			<td style="width:49%;" class="text-normal"><strong><?=$nama_pasien_ina?></strong> / <i><?=$nama_pasien_eng?></i></td>
			<td style="width:2%"></td>
			<td style="width:49%;" class="text-normal "><strong><?=$ttl_ina?></strong> / <i><?=$ttl_eng?></i></td>
		</tr>
		<tr>
			<td style="width:49%;" class="text-normal ">
				<fieldset>
				<legend ></legend>
				<strong><?=$nama_pasien?> - <?=$nomedrec_pasien?> - <?=$jk_pasien?></strong>
				</fieldset>
			</td>
			<td style="width:2%"></td>
			<td style="width:49%" class="text-normal">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<strong><?=HumanDateShort($ttl_pasien)?> - <?=$umur_pasien?> tahun</strong>
				</fieldset>
				
			</td>
		</tr>
		<tr>
			<td style="width:49%;" class="text-normal"><strong><?=strip_tags($diagnosa_per_ina)?></strong> / <i><?=strip_tags($diagnosa_per_eng)?></i></td>
			<td style="width:2%"></td>
			<td style="width:49%;" class="text-normal "><strong><?=strip_tags($nama_tindakan_ina)?></strong> / <i><?=strip_tags($nama_tindakan_eng)?></i></td>
		</tr>
		<tr>
			<td style="width:49%;" class="text-normal ">
				<fieldset style="height: 30px!important">
				<legend style="font-weight:bold;;!important"></legend>
				<?=strip_tags($dengan_diagnosa)?>
				</fieldset>
			</td>
			<td style="width:2%"></td>
			<td style="width:49%" class="text-normal">
				<fieldset style="height: 30px!important">
				<legend style="font-weight:bold;;"></legend>
				<?=strip_tags($rencana_tindakan)?> 
				</fieldset>
				
			</td>
		</tr>
	</table>
	<table class="content ">
		<tr>
			<td style="width:21%;" class="text-normal"><strong><?=$tipe_ina?></strong> / <i><?=$tipe_eng?></i></td>
			<td style="width:16%;" class="text-normal "><strong><?=$icu_ina?></strong> / <i><?=$icu_eng?></i></td>
			<td style="width:12%;" class="text-normal "><strong><?=$cito_ina?></strong> / <i><?=$cito_eng?></i></td>
			<td style="width:2%"></td>
			<td style="width:25%;" class="text-normal "><strong><?=$dokter_ina?></strong> / <i><?=$dokter_eng?></i></td>
			<td style="width:24%;" class="text-normal "><strong><?=$catatan_ina?></strong> / <i><?=$catatan_eng?></i></td>
		</tr>
		<tr>
			<td style="width:21%;" class="text-normal">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=($tipe==3?'RAWAT INAP':'ODS')?>
				</fieldset>
			</td>
			<td style="width:16%;" class="text-normal ">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=($nama_icu)?>
				</fieldset>
			</td>
			<td style="width:12%;" class="text-normal ">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=($nama_cito)?>
				</fieldset>
			</td>
			<td style="width:2%"></td>
			<td style="width:25%;" class="text-normal ">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=($nama_dpjp)?>
				</fieldset>
			</td>
			<td style="width:24%;" class="text-normal ">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=($catatan?$catatan:'-')?>
				</fieldset>
			</td>
		</tr>
		<tr>
			<td colspan="6" class="text-normal"><strong><?=$rencana_implant_ina?></strong> / <i><?=$rencana_implant_eng?></i></td>
		</tr>
	</table>
	<table class="content">
		<tr>
			<td>
				<table class="content">
					<tr>
						<td widtd="5%" class="text-center border-full"><?=$table_no_ina?><br><i class="text-muted"><?=$table_no_eng?></i></td>
						<td widtd="25%" class="text-center border-full"><?=$table_alat_ina?><br><i class="text-muted"><?=$table_alat_eng?></i></td>
						<td widtd="10%" class="text-center border-full"><?=$table_ukuran_ina?><br><i class="text-muted"><?=$table_ukuran_eng?></i></td>
						<td widtd="10%" class="text-center border-full"><?=$table_merk_ina?><br><i class="text-muted"><?=$table_merk_eng?></i></td>
						<td widtd="20%" class="text-center border-full"><?=$table_catatan_ina?><br><i class="text-muted"><?=$table_catatan_eng?></i></td>
						<td widtd="8%" class="text-center border-full"><?=$table_jumlah_ina?><br><i class="text-muted"><?=$table_jumlah_eng?></i></td>
						<td widtd="10%" class="text-center border-full"><?=$table_biaya_ina?><br><i class="text-muted"><?=$table_biaya_eng?></i></td>
						<td widtd="15%" class="text-center border-full">Status</td>										   
					</tr>
					<?
						 $q="SELECT H.*,HH.status_assemen 
									FROM tpoliklinik_ranap_rencana_biaya_implant H
									INNER JOIN tpoliklinik_ranap_rencana_biaya HH ON HH.assesmen_id=H.assesmen_id
									WHERE H.assesmen_id='$assesmen_id'
									ORDER BY H.id ASC
								 ";
				  $list_data=$this->db->query($q)->result();
				  $no=1;
					?>
					<?foreach($list_data as $r){?>
						<tr>
							<td class="border-full text-center"><?=$no?></td>
							<td class="border-full"><?=$r->nama_implant?></td>
							<td class="border-full text-center"><?=$r->ukuran_implant?></td>
							<td class="border-full text-center"><?=$r->merk_implant?></td>
							<td class="border-full text-center"><?=$r->catatan_implant?></td>
							<td class="border-full text-center"><?=$r->jumlah_implant?></td>
							<td class="border-full text-right"><?=number_format($r->total_biaya_implant,0)?></td>
							<td class="border-full"><?=($r->pilih=='1'?'DIGUNAKAN':'')?></td>
						</tr>
					<?
					$no++;
					}?>
				</table>
			
			</td>
		</tr>
		<tr>
			<td class="text-normal"><strong><?=$table_footer_ina?></strong> / <i><?=$table_footer_eng?></i></td>
		</tr>
	</table>
	<table class="content ">
		<tr>
			<td style="width:16%;" class="text-normal"><strong><?=$kel_op_ina?></strong> <br><i><?=$kel_op_eng?></i></td>
			<td style="width:12.5%;" class="text-normal"><strong><?=$jenis_ina?></strong> <br> <i><?=$jenis_eng?></i></td>
			<td style="width:12.5%;" class="text-normal"><strong><?=$lama_ina?></strong> <br> <i><?=$lama_eng?></i></td>
			<td style="width:10%;" class="text-normal"><strong><?=$icu_ina?></strong> <br> <i><?=$icu_eng?></i></td>
			<td style="width:11.5%;" class="text-normal"><strong><?=$cito_ina?></strong> <br> <i><?=$cito_eng?></i></td>
			<td style="width:12.5%;" class="text-normal"><strong><?=$kel_pasien_ina?></strong> <br> <i><?=$kel_pasien_ina?></i></td>
			<td style="width:12.5%;" class="text-normal"><strong><?=$asuransi_ina?></strong> <br> <i><?=$asuransi_eng?></i></td>
			<td style="width:12.5%;" class="text-normal"><strong><?=$kelas_diambil_ina?></strong> <br> <i><?=$kelas_diambil_eng?></i></td>
			
		</tr>
		<tr>
			<td class="text-normal-kecil">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=($nama_ko?$nama_ko:'&nbsp;')?>
				</fieldset>
			</td>
			<td class="text-normal-kecil">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=($nama_jo?$nama_jo:'&nbsp;')?>
				</fieldset>
			</td>
			<td class="text-normal-kecil">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=($lama)?>
				</fieldset>
			</td>
			<td class="text-normal-kecil">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=($nama_icu_pilih)?>
				</fieldset>
			</td>
			<td class="text-normal-kecil">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=($nama_cito_pilih)?>
				</fieldset>
			</td>
			<td class="text-normal-kecil">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=($nama_KP)?>
				</fieldset>
			</td>
			<td class="text-normal-kecil">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=($asuransi?$asuransi:$nama_KP)?>
				</fieldset>
			</td>
			<td class="text-normal-kecil">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=($kelas=='0'?'ODS':$nama_kelas)?>
				</fieldset>
			</td>
			
		</tr>
		
	</table>
	<table class="content">
		<tr>
			<td colspan="4" class="text-header  border-bottom"><?=$section_biaya_ina?> / <i><?=$section_biaya_eng?></i></td>
		</tr>
		<tr>
			<td style="width:18%;" class="text-normal"><strong><?=$kamar_ina?></strong> <br><i><?=$kamar_eng?></i></td>
			<td style="width:35%;" class="text-normal"><strong><?=$pakai_obat_ina?></strong> <br> <i><?=$pakai_obat_eng?></i></td>
			<td style="width:29%;" class="text-normal"><strong><?=$biaya_implan_ina?></strong> <br> <i><?=$biaya_implan_eng?></i></td>
			<td style="width:18%;" class="text-normal"><strong><?=$jasa_dokter_operator_ina?></strong> <br> <i><?=$jasa_dokter_operator_eng?></i></td>
		</tr>
		<tr>
			<td class="text-normal">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=number_format($harga_kamar_ko)?>
				</fieldset>
			</td>
			<td class="text-normal">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=number_format($harga_obat_ko)?>
				</fieldset>
			</td>
			<td class="text-normal">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=number_format($harga_implan)?>
				</fieldset>
			</td>
			<td class="text-normal">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=number_format($harga_do)?>
				</fieldset>
			</td>
		</tr>
		<tr>
			<td class="text-normal"><strong><?=$jasa_dokter_anes_ina?></strong> <br><i><?=$jasa_dokter_anes_eng?></i></td>
			<td class="text-normal"><strong><?=$jasa_asisten_ina?></strong> <br> <i><?=$jasa_asisten_eng?></i></td>
			<td colspan="2" class="text-normal"><strong><?=$total_biaya_ina?></strong> <br> <i><?=$total_biaya_eng?></i></td>
		</tr>
		<tr>
			<td class="text-normal">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=number_format($harga_da)?>
				</fieldset>
			</td>
			<td class="text-normal">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=number_format($harga_daa)?>
				</fieldset>
			</td>
			<td class="text-normal">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<strong><?=number_format($total_biaya_bedah)?></strong>
				</fieldset>
			</td>
			<td class="text-normal">
				
			</td>
		</tr>
		<tr>
			<td colspan="4" class="text-header border-bottom"><?=$section_biaya_ranap_ina?> / <i><?=$section_biaya_ranap_eng?></i></td>
		</tr>
		<tr>
			<td class="text-normal"><strong><?=$biaya_ruang_ina?></strong> <br><i><?=$biaya_ruang_eng?></i></td>
			<td class="text-normal"><strong><?=$biaya_obat_ina?></strong> <br> <i><?=$biaya_obat_eng?></i></td>
			<td class="text-normal"><strong><?=$biaya_penunjang_ina?></strong> <br> <i><?=$biaya_penunjang_eng?></i></td>
			<td class="text-normal"><strong><?=$biaya_visit_ina?></strong> <br> <i><?=$biaya_visit_eng?></i></td>
		</tr>
		<tr>
			<td class="text-normal">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=number_format($harga_ruang)?>
				</fieldset>
			</td>
			<td class="text-normal">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=number_format($harga_obat_ruang)?>
				</fieldset>
			</td>
			<td class="text-normal">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=number_format($harga_penunjang_ruang)?>
				</fieldset>
			</td>
			<td class="text-normal">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=number_format($harga_visit_ruang)?>
				</fieldset>
			</td>
		</tr>
		<tr>
			<td class="text-normal"><strong><?=$biaya_icu_ina?></strong> <br><i><?=$biaya_icu_eng?></i></td>
			<td class="text-normal"><strong></td>
			<td class="text-normal"><strong><?=$total_biaya_ranap_ina?></strong> <br> <i><?=$total_biaya_ranap_eng?></i></td>
			<td class="text-normal"><strong><?=$total_estimasi_biaya_ina?></strong> <br> <i><?=$total_estimasi_biaya_eng?></i></td>
		</tr>
		<tr>
			<td class="text-normal">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=number_format($jumlah_icu)?>
				</fieldset>
			</td>
			<td class="text-normal">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<?=number_format($harga_icu)?>
				</fieldset>
			</td>
			<td class="text-normal">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<strong><?=number_format($total_biaya_ranap)?></strong>
				</fieldset>
			</td>
			<td class="text-normal">
				<fieldset>
				<legend style="font-weight:bold;"></legend>
				<strong><?=number_format($total_estimasi_all)?></strong>
				</fieldset>
			</td>
		</tr>
	</table>
	<table class="content">
		<tr><td>
		<table class="content">
			<thead>
				<tr>
					<th width="5%" class="text-center border-full"><?=$ket_no_ina?><br><i class="text-muted"><?=$ket_no_eng?></i></th>
					<th width="80%" class="text-center border-full"><?=$ket_penjelasan_ina?><br><i class="text-muted"><?=$ket_penjelasan_eng?></i></th>
					<th width="15%" class="text-center border-full"><?=$ket_paraf_ina?><br><i class="text-muted"><?=$ket_paraf_eng?></i></th>
				</tr>
			</thead>
			<?
				 $q="SELECT  H.*			,CASE 
						WHEN H.jenis_isi=1 THEN H.jawaban_id
						WHEN H.jenis_isi=2 THEN H.jawaban_freetext
						WHEN H.jenis_isi=3 THEN H.jawaban_ttd
					 END as jawaban
					 FROM tpoliklinik_ranap_rencana_biaya_keterangan H WHERE H.assesmen_id='$assesmen_id'
						ORDER BY H.no ASC
					 ";
					$list_data=$this->db->query($q)->result();
						$login_ppa_id=$this->session->userdata('login_ppa_id');
						$login_nama_ppa=$this->session->userdata('login_nama_ppa');
			?>
			<tbody>
				<?$no=0; foreach ($list_data as $r){$no++; ?>
					<tr>
						<td class="text-normal border-full text-center"><?=$no?></td>
						<td class="text-normal border-full"><?=$r->pertanyaan?></td>
						<td class="text-normal border-full text-center"><img  width="90" height="60" src="<?=$r->jawaban?>" alt=""></td>
					</tr>
				<? } ?>
			</tbody>
		</table>
		</td></tr>
	</table>
	<table class="content">
		<tr><td>
		<table class="content">
			<tr>
				<td colspan="4" class="text-left"><?=$yang_menjelaskan_ina?><br><i class="text-muted"><?=$yang_menjelaskan_eng?></i></td>
				
			</tr>
			<tr>
				<td widtd="20%" class="text-center"><?=$petugas_biling_ina?><br><i class="text-muted"><?=$petugas_biling_eng?></i></td>
				<td widtd="20%" class="text-center"><?=$dpjp_ina?><br><i class="text-muted"><?=$dpjp_eng?></i></td>
				<td widtd="40%" class="text-center"></td>
				<td widtd="20%" class="text-center"><?=$ttd_pasien_ina?><br><i class="text-muted"><?=$ttd_pasien_eng?></i></td>
			</tr>
			<tr>
				<td width="20%" class="text-center"><img class="" style="width:100px;height:100px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ppa/<?=($petugas_billilng_ppa_id?$petugas_billilng_ppa_id:$created_ppa)?>" alt="" title=""></i></td>
				<td width="20%" class="text-center"><img class="" style="width:100px;height:100px; text-align: center;" src="<?=base_url()?>qrcode/qr_code_ttd_dokter/<?=$dpjp?>" alt="" title=""></td>
				<td width="40%" class="text-center"></td>
				<td width="20%" class="text-center"><img  style="width:100px;height:100px;" src="<?=$pasien_ttd?>" alt=""></td>
			</tr>
			<tr>
				<td width="20%" class="text-center"><?=($petugas_billing_ttd?$petugas_billing_ttd:$user_created)?></td>
				<td width="20%" class="text-center"><?=$nama_dpjp?></td>
				<td width="40%" class="text-center"></td>
				<td width="20%" class="text-center"><?=($nama_pasien_ttd?$nama_pasien_ttd:'')?></td>
			</tr>
			<tr>
				<td colspan="4" class="text-left">Dicetak oleh : <?=$login_nama_ppa?> | Jam Cetak : <?=date('d-m-Y H:i:s')?>
				<br>*Dokument ini telah divalidasi dan dicetak otomatis oleh sistem, tidak digunakan tandatangan otentik.
				</td>
				
			</tr>
		</table>
		</td></tr>
	</table>
	<br>
	</main>
</body>

</html>
