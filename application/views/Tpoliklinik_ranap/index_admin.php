<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>
<?php $user_id = $this->session->userdata('user_id'); ?>

<div class="block push-10">
    <div class="block-content">
        <input type="hidden" id="tab" name="tab" value="{tab}">
        <input type="hidden" id="assesmen_id" name="assesmen_id" value="">
        <input type="hidden" id="status_persetujuan_pasien" name="status_persetujuan_pasien" value="">
        <?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
        <div class="row pull-10">
            <div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tanggal Pembuatan</label>
						<div class="col-md-9">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" id="tanggal_1" name="tanggal_1" placeholder="From" value="{tanggal_1}"/>
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" id="tanggal_2" name="tanggal_2" placeholder="To" value="{tanggal_2}"/>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Nomor. Estimasi</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="nopermintaan" placeholder="No. Estimasi" name="nopermintaan" value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
						<div class="col-md-9">
							<select tabindex="13" id="tipe" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
								<option value="#" selected>All</option>
								<option value="3">RAWAT INAP</option>
								<option value="4">ODS</option>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Dokter Bedah</label>
						<div class="col-md-9">
							<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Dokter -</option>
								<?foreach(get_all('mdokter',array('status'=>1)) as $r){?>
								<option value="<?=$r->id?>"><?=$r->nama?></option>
								<?}?>
								
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Pembuat Estimasi</label>
						<div class="col-md-9">
							<select id="created_ppa" name="created_ppa" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- All -</option>
								<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
								<option value="<?=$r->id?>"><?=$r->nama?></option>
								<?}?>
								
								
							</select>
						</div>
					</div>
					
					
					
				</div>
				<div class="col-md-6 push-10">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">User Proses</label>
						<div class="col-md-9">
							<select id="petugas_billilng_ppa_id" name="petugas_billilng_ppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- All -</option>
								<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
								<option value="<?=$r->id?>"><?=$r->nama?></option>
								<?}?>
								
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="notransaksi" placeholder="No Pendaftaran" name="notransaksi" value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">No Medrec</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="no_medrec" placeholder="No. Medrec" name="no_medrec" value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Nama Pasien</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="namapasien" placeholder="Nama Pasien" name="namapasien" value="">
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label" for="btn_filter_all"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="button" onclick="load_index_all()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						</div>
					</div>
						
				</div>
        </div>
        <?php echo form_close() ?>
    </div>
</div>

<div class="block">
    <ul class="nav nav-pills">
        <li id="div_1" class="<?=($tab=='1'?'active':'')?>">
            <a href="javascript:void(0)" onclick="set_tab(1)"><i class="si si-list"></i> Semua</a>
        </li>
        <li id="div_2" class="<?=($tab=='2'?'active':'')?>">
            <a href="javascript:void(0)"  onclick="set_tab(2)"><i class="si si-pin"></i> Menunggu Diproses </a>
        </li>
        <li id="div_3" class="<?=($tab=='3'?'active':'')?>">
            <a href="javascript:void(0)" onclick="set_tab(3)"><i class="fa fa-check-square-o"></i> Telah Diproses</a>
        </li>
		<li id="div_4" class="<?=($tab=='4'?'active':'')?>">
            <a href="javascript:void(0)" onclick="set_tab(4)"><i class="fa fa-check-square-o"></i> Setuju</a>
        </li>
		<li id="div_5" class="<?=($tab=='5'?'active':'')?>">
            <a href="javascript:void(0)" onclick="set_tab(5)"><i class="fa fa-check-square-o"></i> Menolak</a>
        </li>
		<li id="div_6" class="<?=($tab=='6'?'active':'')?>">
            <a href="javascript:void(0)" onclick="set_tab(6)"><i class="fa fa-check-square-o"></i> Berunding</a>
        </li>
    </ul>
    <div class="block-content">
        <div class="row   ">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered" id="index_all">
                        <thead>
                            <tr>
                                <th width="12%">Action</th>
                                <th width="10%">No Estimasi</th>
                                <th width="10%">Pasien</th>
                                <th width="5%">Tipe Pelayanan</th>
                                <th width="5%">ICU</th>
                                <th width="5%">CITO</th>
                                <th width="10%">Dengan Diagnosa</th>
                                <th width="10%">Nama Tindakan</th>
                                <th width="10%">Dokter</th>
                                <th width="10%">Status</th>
                                <th width="10%">Nominal Estimasi</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal in" id="modal_menolak" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Alasan Menolak</h3>
				</div>
				<div class="block-content">
					
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<div class="col-md-12">
										<label class="text-center">Alasan Menolak</label>
										<input type="text" class="form-control input-sm" id="alasan_menolak" value="">
									</div>
								</div>
							</div>
							
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-success" type="button" onclick="set_setuju_manual(3)"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>

<script type="text/javascript">
var table;
var tab;
var st_login;

$(document).ready(function() {
    tab=$("#tab").val();
    load_index_all();
    // st_login=$("#st_login").val();
    // // $("#div_1").classlist.remove("active");
    // cek_login();
    // set_tab(tab);
    // startWorker();
});
function set_modal_fasilitas(id){
	$("#modal_fasilitas").modal('show');
	$.ajax({
		url: '{site_url}tmonitoring_bed/get_fasilitas/',
		dataType: "json",
		type: "POST",
		data:{
			mfasilitas_id:id,
		},
		success: function(data) {
			$("#div_gambar").html(data.detail);
		}
	});
}
// $(document).on("change", "#idbed", function() {
    // get_bed();
// });
$(document).on("change", "#idkelas", function() {
    get_bed();
});
$(document).on("change", "#ruangan_id", function() {
	$("#idkelas").val("#").trigger('change');
    get_bed();
});
$(document).on("click", "#btn_cari", function() {
    load_index_all();
});

function set_tab($tab) {
    tab = $tab;
    // alert(tab);
    // $("#cover-spin").show();
    document.getElementById("div_1").classList.remove("active");
    document.getElementById("div_2").classList.remove("active");
    document.getElementById("div_3").classList.remove("active");
    document.getElementById("div_4").classList.remove("active");
    document.getElementById("div_5").classList.remove("active");
    document.getElementById("div_6").classList.remove("active");
    if (tab == '1') {
        document.getElementById("div_1").classList.add("active");
    }
    if (tab == '2') {
        document.getElementById("div_2").classList.add("active");
    }
    if (tab == '3') {
        document.getElementById("div_3").classList.add("active");
    }
	if (tab == '4') {
        document.getElementById("div_4").classList.add("active");
    }
	if (tab == '5') {
        document.getElementById("div_5").classList.add("active");
    }
	if (tab == '6') {
        document.getElementById("div_6").classList.add("active");
    }

    load_index_all();
}

function set_setuju(assesmen_id,status_persetujuan_pasien){
	$("#assesmen_id").val(assesmen_id);
	$("#status_persetujuan_pasien").val(status_persetujuan_pasien);
	if (status_persetujuan_pasien!='3'){
		$("#alasan_menolak").val('');
	}
	$("#modal_menolak").modal('hide');
	let label_nama='';
	if (status_persetujuan_pasien=='1'){
		label_nama='Pasien Memilih Berunding ?';
	}
	if (status_persetujuan_pasien=='2'){
		label_nama='Pasien Memilih Setuju ?';
	}
	if (status_persetujuan_pasien=='3'){
		label_nama='Pasien Memilih Menolak ?';
	}
	swal({
		title: "Anda Yakin ?",
		text : label_nama,
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		simpan_setuju_direct();
	});
}
function set_setuju_manual(status_persetujuan_pasien){
	// $("#assesmen_id").val(assesmen_id);
	$("#status_persetujuan_pasien").val(status_persetujuan_pasien);
	if (status_persetujuan_pasien!='3'){
		$("#alasan_menolak").val('');
	}
	$("#modal_menolak").modal('hide');
	let label_nama='';
	if (status_persetujuan_pasien=='1'){
		label_nama='Pasien Memilih Berunding ?';
	}
	if (status_persetujuan_pasien=='2'){
		label_nama='Pasien Memilih Setuju ?';
	}
	if (status_persetujuan_pasien=='3'){
		label_nama='Pasien Memilih Menolak ?';
	}
	swal({
		title: "Anda Yakin ?",
		text : label_nama,
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		simpan_setuju_direct();
	});
}
function show_menolak(assesmen_id){
	$("#assesmen_id").val(assesmen_id);
	$("#modal_menolak").modal('show');
}
function simpan_setuju_direct(){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_ranap/simpan_setuju_direct', 
		dataType: "JSON",
		method: "POST",
		data : {
				assesmen_id :  $("#assesmen_id").val(),
				status_persetujuan_pasien : $("#status_persetujuan_pasien").val(),
				alasan_menolak : $("#alasan_menolak").val(),
			},
		success: function(data) {
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Proses Berhasil Disimpan'});
			$('#index_all').DataTable().ajax.reload( null, false );
		
		}
	});
}
function load_index_all(){
	$('#index_all').DataTable().destroy();	
	// alert(tab);
	let tanggal_1=$("#tanggal_1").val();
	let tanggal_2=$("#tanggal_2").val();
	let nopermintaan=$("#nopermintaan").val();
	let tipe=$("#tipe").val();
	let iddokter=$("#iddokter").val();
	let created_ppa=$("#created_ppa").val();
	let petugas_billilng_ppa_id=$("#petugas_billilng_ppa_id").val();
	let notransaksi=$("#notransaksi").val();
	let no_medrec=$("#no_medrec").val();
	let namapasien=$("#namapasien").val();
	
	table = $('#index_all').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 10,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			 "columnDefs": [
						{  className: "text-right", targets:[0,10] },
						{  className: "text-center", targets:[9] },
						
					],
            ajax: { 
                url: '{site_url}Tpoliklinik_ranap/list_estimasi_admin',
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						nopermintaan:nopermintaan,
						iddokter:iddokter,
						created_ppa:created_ppa,
						petugas_billilng_ppa_id:petugas_billilng_ppa_id,
						notransaksi:notransaksi,
						tipe:tipe,
						tab:tab,
						namapasien:namapasien,
						no_medrec:no_medrec,
						
						
					   }
            },
			"drawCallback": function(settings) {
				$("#cover-spin").hide();
			}
        });
	$("#cover-spin").hide();
}
// function load_index_all() {
    // // let idkelas = $("#idkelas").val();
    // // let ruangan_id = $("#ruangan_id").val();
    // // let idbed = $("#idbed").val();
    // $("#cover-spin").show();
    // $('#index_all').DataTable().destroy();
    // // alert(ruangan_id);
    // table = $('#index_all').DataTable({
        // autoWidth: false,
        // searching: true,
        // serverSide: true,
        // "processing": true,
        // "order": [],
        // "pageLength": 10,
        // "ordering": false,
        
        // ajax: {
            // url: '{site_url}Tpoliklinik_ranap/list_estimasi_admin',
            // type: "POST",
            // dataType: 'json',
            // data: {
                // // idbed: idbed,
                // // idkelas: idkelas,
                // // idruangan: ruangan_id,

            // },
			// columnDefs: [
				// {  className: "text-right", targets:[0] },
				 // // { "width": "5%", "targets": [5,6,7] },
				 // // { "width": "10%", "targets": [0,2,3,8] },
				 // // { "width": "15%", "targets": [1] },
				 // // { "width": "20%", "targets": [4] }
			// ]


        // },
        // // "drawCallback": function(settings) {
            // // // $("#index_all thead").remove();\
			// // // console.log('namama');
            // // $("#cover-spin").hide();
        // // }
    // });
    // $("#cover-spin").hide();
// }

</script>
