<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('2466'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_general()"><i class="fa fa-bell"></i> General</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2468'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_event"  onclick="load_tab2()"><i class="fa fa-check-square-o"></i> Logic</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2471'))){ ?>
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab_label" ><i class="si si-note"></i> Label</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2473'))){ ?>
		<li class="<?=($tab=='4'?'active':'')?>">
			<a href="#tab_4" onclick="load_tab4()"><i class="fa fa-user-md"></i> User</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2474'))){ ?>
		<li class="<?=($tab=='5'?'active':'')?>">
			<a href="#tab_5" ><i class="fa fa-paint-brush"></i> Paraf</a>
		</li>
		<?}?>
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('2466'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_info">
			
			<?php echo form_open('setting_invasif/save_assesmen','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_header">Judul Header<span style="color:red;">*</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_header" name="judul_header" value="{judul_header}" placeholder="Header">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_footer">Judul Footer<span style="color:red;">*</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_footer" name="judul_footer" value="{judul_footer}" placeholder="Footer">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_header_eng">Judul Header <span style="color:red;"> ENG *</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_header_eng" name="judul_header_eng" value="{judul_header_eng}" placeholder="Header">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="footer_eng">Judul Footer<span style="color:red;">ENG *</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="footer_eng" name="footer_eng" value="{footer_eng}" placeholder="Footer">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_edit_catatan">Izin Edit Catatan</label>
						<div class="col-md-2">
							<select id="st_edit_catatan" name="st_edit_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_edit_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_edit_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_edit">Waktu Edit</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_edit" name="lama_edit" value="{lama_edit}" placeholder="Waktu Edit">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_edit">Edit Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_edit" name="orang_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_edit=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_edit=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_hapus_catatan">Izin Hapus Catatan</label>
						<div class="col-md-2">
							<select id="st_hapus_catatan" name="st_hapus_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_hapus_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_hapus_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_hapus">Waktu Hapus</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_hapus" name="lama_hapus" value="{lama_hapus}" placeholder="Waktu Hapus">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_hapus">Hapus Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_hapus" name="orang_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_hapus=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_hapus=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_duplikasi_catatan">Izin Duplikasi Catatan</label>
						<div class="col-md-2">
							<select id="st_duplikasi_catatan" name="st_duplikasi_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_duplikasi_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_duplikasi_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_duplikasi">Waktu Duplikasi</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_duplikasi" name="lama_duplikasi" value="{lama_duplikasi}" placeholder="Waktu Duplikasi">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_duplikasi">Duplikasi Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_duplikasi" name="orang_duplikasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_duplikasi=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_duplikasi=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				
				<?php if (UserAccesForm($user_acces_form,array('2467'))){ ?>
				<div class="col-md-12 push-10">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="statusstok"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_general" name="btn_save_general"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
				<?}?>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2471'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?> " id="tab_label">
			
			<?php echo form_open('setting_invasif/save_label','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="form-group">
					
					<div class="col-md-12">
						<div class="col-md-12">
							<img class="img-avatar"  id="output_img" src="{upload_path}app_setting/{logo}" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Logo  (100x100)</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="box">
								<input type="file" id="file-3" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;" name="logo" value="{logo}" />
								<label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label>Alamat</label>
							<input type="text" class="form-control" name="alamat_rs" value="{alamat_rs}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label>Phone</label>
							<input type="text" class="form-control" name="phone_rs" value="{phone_rs}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label>Web</label>
							<input type="text" class="form-control" name="web_rs" value="{web_rs}">
						</div>
					</div>
				</div>
				<hr>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Label Header Identitas Pasien</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="identitas_pasien_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="identitas_pasien_ina" name="identitas_pasien_ina" value="{identitas_pasien_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="identitas_pasien_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="identitas_pasien_eng" name="identitas_pasien_eng" value="{identitas_pasien_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">No. Registrasi</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="no_reg_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="no_reg_ina" name="no_reg_ina" value="{no_reg_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="no_reg_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="no_reg_eng" name="no_reg_eng" value="{no_reg_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">No Rekam Medis</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="norek_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="norek_ina" name="norek_ina" value="{norek_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="norek_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="norek_eng" name="norek_eng" value="{norek_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Nama Pasien</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_pasien_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_pasien_ina" name="nama_pasien_ina" value="{nama_pasien_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_pasien_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_pasien_eng" name="nama_pasien_eng" value="{nama_pasien_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Tanggal lahir</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="tanggal_lahir_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="tanggal_lahir_ina" name="tanggal_lahir_ina" value="{tanggal_lahir_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="tanggal_lahir_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="tanggal_lahir_eng" name="tanggal_lahir_eng" value="{tanggal_lahir_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">umur</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="umur_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="umur_ina" name="umur_ina" value="{umur_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="umur_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="umur_eng" name="umur_eng" value="{umur_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Jenis Kelamin</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="jenis_kelamin_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="jenis_kelamin_ina" name="jenis_kelamin_ina" value="{jenis_kelamin_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="jenis_kelamin_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="jenis_kelamin_eng" name="jenis_kelamin_eng" value="{jenis_kelamin_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Tanggal & Jam</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="tanggal_jam_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="tanggal_jam_ina" name="tanggal_jam_ina" value="{tanggal_jam_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="tanggal_jam_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="tanggal_jam_eng" name="tanggal_jam_eng" value="{tanggal_jam_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Nama Tindakan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_tindakan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_tindakan_ina" name="nama_tindakan_ina" value="{nama_tindakan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_tindakan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_tindakan_eng" name="nama_tindakan_eng" value="{nama_tindakan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Keterangan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_ina" name="ket_ina" value="{ket_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_eng" name="ket_eng" value="{ket_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Tanda Tangan  Petugas PPA</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ttd_petugas_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ttd_petugas_ina" name="ttd_petugas_ina" value="{ttd_petugas_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ttd_petugas_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ttd_petugas_eng" name="ttd_petugas_eng" value="{ttd_petugas_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Daftar Persetujuan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="daftar_persetujuan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="daftar_persetujuan_ina" name="daftar_persetujuan_ina" value="{daftar_persetujuan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="daftar_persetujuan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="daftar_persetujuan_eng" name="daftar_persetujuan_eng" value="{daftar_persetujuan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Tanda Tangan Pasien / Keluarga</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ttd_pasien_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ttd_pasien_ina" name="ttd_pasien_ina" value="{ttd_pasien_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ttd_pasien_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ttd_pasien_eng" name="ttd_pasien_eng" value="{ttd_pasien_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Persetujuan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="persetujuan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="persetujuan_ina" name="persetujuan_ina" value="{persetujuan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="persetujuan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="persetujuan_eng" name="persetujuan_eng" value="{persetujuan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-12 push-10">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="btn_save_label"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_label" name="btn_save_label"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2474'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='5'?'active in':'')?> " id="tab_5">
			
			<?php echo form_open('setting_invasif/save_paraf','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('TANDA TANGAN PETUGAS & PROFESI')?></h5>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Akses Tanda Tangan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ttd_petugas_st_auto">Akses Tanda Tangan</label>
								<div class="col-md-10">
									<select id="ttd_petugas_st_auto" name="ttd_petugas_st_auto" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="1" <?=($ttd_petugas_st_auto=='1'?'selected':'')?>>Ambil Otomatis</option>
										<option value="0" <?=($ttd_petugas_st_auto=='0'?'selected':'')?>>Tanda tangan sendiri</option>
										
									</select>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('PARAF ')?></h5>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Saya yang bertanda tangan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="paragraf_1_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="paragraf_1_ina" name="paragraf_1_ina" value="{paragraf_1_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="paragraf_1_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="paragraf_1_eng" name="paragraf_1_eng" value="{paragraf_1_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Yang bertandatangan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="yang_ttd_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="yang_ttd_ina" name="yang_ttd_ina" value="{yang_ttd_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="yang_ttd_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="yang_ttd_eng" name="yang_ttd_eng" value="{yang_ttd_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Nama</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_ina" name="nama_ina" value="{nama_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_eng" name="nama_eng" value="{nama_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Dengan ini memberikan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="memberikan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="memberikan_ina" name="memberikan_ina" value="{memberikan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="memberikan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="memberikan_eng" name="memberikan_eng" value="{memberikan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Untuk Tindakan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="untuk_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="untuk_ina" name="untuk_ina" value="{untuk_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="untuk_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="untuk_eng" name="untuk_eng" value="{untuk_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Terhadap</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="terhadap_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="terhadap_ina" name="terhadap_ina" value="{terhadap_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="terhadap_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="terhadap_eng" name="terhadap_eng" value="{terhadap_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Signature</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="sign_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="sign_ina" name="sign_ina" value="{sign_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="sign_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="sign_eng" name="sign_eng" value="{sign_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 push-10">
					<div class="form-group" style="margin-bottom: 5px;">
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_label" name="btn_save_label"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
				
				
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2468'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_event">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('HAK AKSES FORMULIR')?></h5>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_hak_akses">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Profesi</th>
										<th width="15%">Spesialisasi</th>
										<th width="15%">PPA</th>
										<th width="15%">Melihat</th>
										<th width="10%">Input</th>
										<th width="10%">Edit</th>
										<th width="10%">Hapus</th>
										<th width="10%">Cetak</th>
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="profesi_id" name="profesi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" <?=($profesi_id=='#'?'selected':'')?>>-Pilih Profesi-</option>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" <?=($profesi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="spesialisasi_id" name="spesialisasi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($spesialisasi_id=='#'?'selected':'')?>>-SEMUA-</option>
												<?foreach(list_variable_ref(22) as $r){?>
													<option value="<?=$r->id?>" <?=($spesialisasi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										<th>
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($mppa_id=='#'?'selected':'')?>>-SEMUA-</option>
												
											</select>
										</th>
										
										<th>
											<select id="st_lihat" name="st_lihat" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_lihat=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_lihat=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_input" name="st_input" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_input=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_input=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_edit" name="st_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_edit=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_edit=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_hapus" name="st_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_hapus=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_hapus=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_cetak" name="st_cetak" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_cetak=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_cetak=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<?php if (UserAccesForm($user_acces_form,array('2443'))){ ?>
											<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_hak_akses" name="btn_tambah_hak_akses"><i class="fa fa-plus"></i> Tambah</button>
											<?}?>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
					</div>
				</div>
				<div class="col-md-12" hidden>
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('STATUS TAMPIL FORMULIR')?></h5>
					</div>
				</div>
				<div class="col-md-12" hidden>
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_tampil">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Tujuan</th>
										<th width="15%">Poliklinik</th>
										<th width="15%">Status Pasien</th>
										<th width="15%">Kasus</th>
										<th width="10%">Lihat Rujukan Terakhir</th>
										<th width="10%">Lama Terakhir Kunjungan Tearkhir</th>
										<th width="10%">Status Formulir</th>
										<th width="10%">Action</th>										   
									</tr>
									<?
										$idtipe='#';
										$idpoli='#';
										$statuspasienbaru='#';
										$pertemuan_id='#';
										$st_tujuan_terakhir='0';
										$lama_terakhir_tujan='0';
										$st_formulir='0';
									?>
									<tr>
										<th>#</th>
										<th>
											<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" <?=($idtipe=='#'?'selected':'')?>>-Tujuan-</option>
												<option value="1" <?=($idtipe=='1'?'selected':'')?>>POLIKINIK</option>
												<option value="2" <?=($idtipe=='2'?'selected':'')?>>IGD</option>
												
											</select>
										</th>
										<th>
											<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($idpoli=='#'?'selected':'')?>>-Semua Poli-</option>
												<?foreach(get_all('mpoliklinik',array('status'=>1)) as $r){?>
													<option value="<?=$r->id?>" <?=($idpoli==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										
										<th>
											<select tabindex="4" id="statuspasienbaru" name="statuspasienbaru" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="2" selected>SEMUA</option>
												<option value="1" <?=($statuspasienbaru == 1 ? 'selected="selected"' : '')?>>PASIEN BARU</option>
												<option value="0" <?=($statuspasienbaru == 0 ? 'selected="selected"' : '')?>>PASIEN LAMA</option>
											</select>
										</th>
										<th>
											<select id="pertemuan_id" name="pertemuan_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($pertemuan_id==''?'selected':'')?>>- All Kasus -</option>
												<?foreach(list_variable_ref(15) as $r){?>
												<option value="<?=$r->id?>" <?=($pertemuan_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="st_tujuan_terakhir" name="st_tujuan_terakhir" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_tujuan_terakhir=='1'?'selected':'')?>>YA</option>
												<option value="0" <?=($st_tujuan_terakhir=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										
										<th>
											<select name="operand" style="width: 100%" id="operand" class="js-select2 form-control input-sm">					
										
												<option value="0" selected>TIDAK DITENTUKAN</option>										
												<option value=">">></option>										
												<option value=">=">>=</option>	
												<option value="<"><</option>
												<option value="<="><=</option>
												<option value="=">=</option>										
											</select>
											<input type="text" style="width: 100%"  class="form-control number" id="lama_terakhir_tujan" placeholder="0" name="lama_terakhir_tujan" value="0">
										</th>
										
										
										<th>
											<select id="st_formulir" name="st_formulir" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="0" <?=($st_formulir=='0'?'selected':'')?>>-PILIH-</option>
												<option value="1" <?=($st_formulir=='1'?'selected':'')?>>TAMPIL (Wajib diisi)</option>
												<option value="2" <?=($st_formulir=='2'?'selected':'')?>>TAMPIL (Tidak Wajib Diisi</option>
												<option value="3" <?=($st_formulir=='3'?'selected':'')?>>TIDAK DITAMPILKAN</option>
												
											</select>
										</th>
										
										<th>
											<?php if (UserAccesForm($user_acces_form,array('2444'))){ ?>
											<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_formulir" name="btn_tambah_formulir"><i class="fa fa-plus"></i> Tambah</button>
											<?}?>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2473'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='4'?'active in':'')?>" id="tab_4">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('PETUGAS RUMAH SAKIT')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-12">
							<label for="st_setting_dokter">Cara Pengaturan </label>
							<select id="st_setting_dokter" name="st_setting_dokter" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Choose one..">
												
								<option value="#" <?=($st_setting_dokter=='#'?'selected':'')?>>-Pilih Setting Dokter-</option>
								<option value="1" <?=($st_setting_dokter=='1'?'selected':'')?>>User Login</option>
								<option value="2" <?=($st_setting_dokter=='2'?'selected':'')?>>Diatur Spesifik</option>
								
							</select>
						</div>
						
					</div>
				</div>
				<div class="col-md-12" id="div_dokter">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_user_dokter">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Profesi</th>
										<th width="15%">Spesialisasi</th>
										<th width="15%">PPA</th>
										
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="profesi_id_dokter" name="profesi_id_dokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" selected>-Pilih Profesi-</option>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="spesialisasi_id_dokter" name="spesialisasi_id_dokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-SEMUA-</option>
												<?foreach(list_variable_ref(22) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										<th>
											<select id="mppa_id_dokter" name="mppa_id_dokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-SEMUA-</option>
												
											</select>
										</th>
										
										<th>
											<button class="btn btn-primary btn-sm" type="button" onclick="tambah_dokter()"><i class="fa fa-plus"></i> Tambah</button>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
						</div>					
					</div>
				</div>
				<div class="col-md-12" hidden>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_success('SETTING PEMBERI INFORMASI')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12" hidden>
					<div class="form-group" style="margin-bottom: 15px;">
						
						<div class="col-md-12">
							<label for="st_setting_pemberi_info">Pengaturan Pemberi Informasi</label>
							<select id="st_setting_pemberi_info" name="st_setting_pemberi_info" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Choose one..">
												
								<option value="#" <?=($st_setting_pemberi_info=='#'?'selected':'')?>>-Pilih Setting Dokter-</option>
								<option value="1" <?=($st_setting_pemberi_info=='1'?'selected':'')?>>User Login</option>
								<option value="2" <?=($st_setting_pemberi_info=='2'?'selected':'')?>>Diatur Spesifik</option>
								
							</select>
						</div>
						
					</div>
				</div>
				<div class="col-md-12"  id="div_pemberi" hidden>
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_user_pemberi">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Profesi</th>
										<th width="15%">Spesialisasi</th>
										<th width="15%">PPA</th>
										
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="profesi_id_pemberi" name="profesi_id_pemberi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" selected>-Pilih Profesi-</option>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="spesialisasi_id_pemberi" name="spesialisasi_id_pemberi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-SEMUA-</option>
												<?foreach(list_variable_ref(22) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										<th>
											<select id="mppa_id_pemberi" name="mppa_id_pemberi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-SEMUA-</option>
												
											</select>
										</th>
										
										<th>
											<button class="btn btn-primary btn-sm" type="button" onclick="tambah_pemberi()"><i class="fa fa-plus"></i> Tambah</button>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
						</div>					
					</div>
				</div>
				
				<div class="col-md-12" hidden>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_danger('SETTING PETUGAS YANG MENDAMPINGI')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12" hidden>
					<div class="form-group" style="margin-bottom: 15px;">
						
						
						<div class="col-md-12">
							<label for="st_setting_petugas_pendamping">Pengaturan Petugas yang Mendampingi</label>
							<select id="st_setting_petugas_pendamping" name="st_setting_petugas_pendamping" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Choose one..">
												
								<option value="#" <?=($st_setting_petugas_pendamping=='#'?'selected':'')?>>-Pilih Setting Dokter-</option>
								<option value="1" <?=($st_setting_petugas_pendamping=='1'?'selected':'')?>>User Login</option>
								<option value="2" <?=($st_setting_petugas_pendamping=='2'?'selected':'')?>>Diatur Spesifik</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12" id="div_pendamping" hidden>
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_user_pendamping">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Profesi</th>
										<th width="15%">Spesialisasi</th>
										<th width="15%">PPA</th>
										
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="profesi_id_pendamping" name="profesi_id_pendamping" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" selected>-Pilih Profesi-</option>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="spesialisasi_id_pendamping" name="spesialisasi_id_pendamping" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-SEMUA-</option>
												<?foreach(list_variable_ref(22) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										<th>
											<select id="mppa_id_pendamping" name="mppa_id_pendamping" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-SEMUA-</option>
												
											</select>
										</th>
										
										<th>
											<button class="btn btn-primary btn-sm" type="button" onclick="tambah_pendamping()"><i class="fa fa-plus"></i> Tambah</button>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
						</div>					
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
			
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
function loadFile_img(event){
	// alert('load');
	var output_img = document.getElementById('output_img');
	output_img.src = URL.createObjectURL(event.target.files[0]);
}
$(document).ready(function(){	
		$(".number").number(true,0,'.',',');
	var tab=$("#tab").val();
	if (tab=='1'){
		load_general();
	}
	if (tab=='2'){
		load_tab2();
	}
	if (tab=='4'){
		load_tab4();
	}
	$('.js-summernote').summernote({
		  height: 80,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})

})	
function load_tab4(){
	load_dokter();
	load_pemberi();
	load_pendamping();
	set_user();
}
function tambah_dokter(){
	let profesi_id=$("#profesi_id_dokter").val();
	let spesialisasi_id=$("#spesialisasi_id_dokter").val();
	let mppa_id=$("#mppa_id_dokter").val();
	let jenis_akses=1;
	
	if (profesi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Profesi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_invasif/simpan_dokter', 
		dataType: "JSON",
		method: "POST",
		data : {
				jenis_akses:jenis_akses,
				profesi_id:profesi_id,
				spesialisasi_id:spesialisasi_id,
				mppa_id:mppa_id,
				
			
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				$("#profesi_id_dokter").val('#').trigger('change');
				$("#spesialisasi_id_dokter").val('#').trigger('change');
				$("#mppa_id_dokter").val('#').trigger('change');
				
				$('#index_user_dokter').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});
}
function tambah_pemberi(){
	let profesi_id=$("#profesi_id_pemberi").val();
	let spesialisasi_id=$("#spesialisasi_id_pemberi").val();
	let mppa_id=$("#mppa_id_pemberi").val();
	let jenis_akses=2;
	
	if (profesi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Profesi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_invasif/simpan_dokter', 
		dataType: "JSON",
		method: "POST",
		data : {
				jenis_akses:jenis_akses,
				profesi_id:profesi_id,
				spesialisasi_id:spesialisasi_id,
				mppa_id:mppa_id,
				
			
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				$("#profesi_id_pemberi").val('#').trigger('change');
				$("#spesialisasi_id_pemberi").val('#').trigger('change');
				$("#mppa_id_pemberi").val('#').trigger('change');
				
				$('#index_user_pemberi').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});
}
function set_user(){
	if ($("#st_setting_dokter").val()=='1'){
		$("#div_dokter").hide();
	}else{
		$("#div_dokter").show();
		
	}
		$("#div_pemberi").hide();
		$("#div_pendamping").hide();
	// if ($("#st_setting_pemberi_info").val()=='1'){
	// }else{
		// $("#div_pemberi").show();
		
	// }
	// if ($("#st_setting_petugas_pendamping").val()=='1'){
	// }else{
		// $("#div_pendamping").show();
		
	// }
}
function tambah_pendamping(){
	let profesi_id=$("#profesi_id_pendamping").val();
	let spesialisasi_id=$("#spesialisasi_id_pendamping").val();
	let mppa_id=$("#mppa_id_pendamping").val();
	let jenis_akses=3;
	
	if (profesi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Profesi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_invasif/simpan_dokter', 
		dataType: "JSON",
		method: "POST",
		data : {
				jenis_akses:jenis_akses,
				profesi_id:profesi_id,
				spesialisasi_id:spesialisasi_id,
				mppa_id:mppa_id,
				
			
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				$("#profesi_id_pendamping").val('#').trigger('change');
				$("#spesialisasi_id_pendamping").val('#').trigger('change');
				$("#mppa_id_pendamping").val('#').trigger('change');
				
				$('#index_user_pendamping').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});
}
function hapus_user(id,jenis_akses){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus User?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_invasif/hapus_user',
				type: 'POST',
				data: {id: id},
				complete: function() {
					if (jenis_akses=='1'){
						$('#index_user_dokter').DataTable().ajax.reload( null, false ); 
					}
					if (jenis_akses=='2'){
						$('#index_user_pemberi').DataTable().ajax.reload( null, false ); 
					}
					if (jenis_akses=='3'){
						$('#index_user_pendamping').DataTable().ajax.reload( null, false ); 
					}
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function load_dokter(){
	let jenis_akses=1;
	$('#index_user_dokter').DataTable().destroy();	
	table = $('#index_user_dokter').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			
            ajax: { 
                url: '{site_url}setting_invasif/load_user', 
                type: "POST" ,
                dataType: 'json',
				data : {
						jenis_akses:jenis_akses,
					   }
            }
        });
}
function load_pemberi(){
	let jenis_akses=2;
	$('#index_user_pemberi').DataTable().destroy();	
	table = $('#index_user_pemberi').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			
            ajax: { 
                url: '{site_url}setting_invasif/load_user', 
                type: "POST" ,
                dataType: 'json',
				data : {
						jenis_akses:jenis_akses,
					   }
            }
        });
}
function load_pendamping(){
	let jenis_akses=3;
	$('#index_user_pendamping').DataTable().destroy();	
	table = $('#index_user_pendamping').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			
            ajax: { 
                url: '{site_url}setting_invasif/load_user', 
                type: "POST" ,
                dataType: 'json',
				data : {
						jenis_akses:jenis_akses,
					   }
            }
        });
}
$(".auto_blur").change(function(){
	set_user();
	simpan_user_head();
	
});
function simpan_user_head(){
	let st_setting_dokter=$("#st_setting_dokter").val();
	let st_setting_pemberi_info=$("#st_setting_pemberi_info").val();
	let st_setting_petugas_pendamping=$("#st_setting_petugas_pendamping").val();
	$.ajax({
		url: '{site_url}setting_invasif/simpan_user_head', 
		dataType: "JSON",
		method: "POST",
		data : {
				st_setting_dokter:st_setting_dokter,
				st_setting_pemberi_info:st_setting_pemberi_info,
				st_setting_petugas_pendamping:st_setting_petugas_pendamping,
			},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==true){
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});
}
$("#profesi_id_dokter").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_invasif/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id_dokter").empty();
				$("#mppa_id_dokter").append(data);
			}
		});
	}else{
		
				$("#mppa_id_dokter").empty();
	}

});
$("#profesi_id_pemberi").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_invasif/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id_pemberi").empty();
				$("#mppa_id_pemberi").append(data);
			}
		});
	}else{
		
				$("#mppa_id_pemberi").empty();
	}

});
$("#profesi_id_pendamping").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_invasif/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id_pendamping").empty();
				$("#mppa_id_pendamping").append(data);
			}
		});
	}else{
		
				$("#mppa_id_pendamping").empty();
	}

});
function load_general(){
	$('.js-summernote').summernote({
	  height: 50,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	});	

}
function load_tab2(){
	load_hak_akses();		
	// load_formulir();	
}

// LOGIC
function load_hak_akses(){
	$('#index_hak_akses').DataTable().destroy();	
	table = $('#index_hak_akses').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_invasif/load_hak_akses', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
$("#btn_tambah_hak_akses").click(function() {
	let profesi_id=$("#profesi_id").val();
	let spesialisasi_id=$("#spesialisasi_id").val();
	let mppa_id=$("#mppa_id").val();
	let st_lihat=$("#st_lihat").val();
	let st_input=$("#st_input").val();
	let st_edit=$("#st_edit").val();
	let st_hapus=$("#st_hapus").val();
	let st_cetak=$("#st_cetak").val();
	
	if (profesi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Profesi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_invasif/simpan_hak_akses', 
		dataType: "JSON",
		method: "POST",
		data : {
				profesi_id:profesi_id,
				spesialisasi_id:spesialisasi_id,
				mppa_id:mppa_id,
				st_lihat:st_lihat,
				st_input:st_input,
				st_edit:st_edit,
				st_hapus:st_hapus,
				st_cetak:st_cetak,
			
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				$("#profesi_id").val('#').trigger('change');
				$("#spesialisasi_id").val('#').trigger('change');
				$("#mppa_id").val('#').trigger('change');
				$("#st_cetak").val('0').trigger('change');
				$("#st_hapus").val('0').trigger('change');
				$("#st_edit").val('0').trigger('change');
				$("#st_input").val('0').trigger('change');
				$("#st_lihat").val('0').trigger('change');
				$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});

});
function hapus_hak_akses(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_invasif/hapus_hak_akses',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function hapus_default(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Default?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_invasif/hapus_default',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_tampil_default').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

$("#profesi_id").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_invasif/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id").empty();
				$("#mppa_id").append(data);
			}
		});
	}else{
		
				$("#mppa_id").empty();
	}

});
$("#operand_tahun").change(function(){
	if ($(this).val()=='0'){//Tidak Ditentukan
		$("#operand_bulan").val($(this).val()).trigger('change');
		$("#operand_hari").val($(this).val()).trigger('change');
		$("#umur_bulan").val($(this).val()).trigger('change');
		$("#umur_hari").val($(this).val()).trigger('change');
		$(".bulan").attr('disabled','disabled');
		$(".hari").attr('disabled','disabled');
	}else{
		$(".bulan").removeAttr('disabled');
		// $(".hari").removeAttr('disabled');
	}

});
$("#operand_bulan").change(function(){
	if ($(this).val()=='0'){//Tidak Ditentukan
		$("#operand_hari").val($(this).val()).trigger('change');
		$(".hari").attr('disabled','disabled');
		$("#umur_hari").val($(this).val()).trigger('change');
	}else{
		$(".hari").removeAttr('disabled');
	}

});
$("#st_spesifik_invasif").change(function(){
	if ($(this).val()=='1'){
		$(".setting_tidak").show();
	}else{
		$(".setting_tidak").hide();
	}

});
$("#btn_tambah_formulir").click(function() {
	let idtipe=$("#idtipe").val();
	let idpoli=$("#idpoli").val();
	let statuspasienbaru=$("#statuspasienbaru").val();
	let pertemuan_id=$("#pertemuan_id").val();
	let st_tujuan_terakhir=$("#st_tujuan_terakhir").val();
	let operand=$("#operand").val();
	// alert(operand);
	let lama_terakhir_tujan=$("#lama_terakhir_tujan").val();
	let st_formulir=$("#st_formulir").val();
	
	if (idtipe=='#'){
		sweetAlert("Maaf...", "Tentukan Tujuan", "error");
		return false;
	}
	
	if (st_formulir=='0'){
		sweetAlert("Maaf...", "Tentukan Status Formulir", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_invasif/simpan_formulir', 
		dataType: "JSON",
		method: "POST",
		data : {
				idtipe:idtipe,
				idpoli:idpoli,
				statuspasienbaru:statuspasienbaru,
				pertemuan_id:pertemuan_id,
				st_tujuan_terakhir:st_tujuan_terakhir,
				operand:operand,
				lama_terakhir_tujan:lama_terakhir_tujan,
				st_formulir:st_formulir,

			
			},
		success: function(data) {
			// alert(data);
				$("#cover-spin").hide();
			if (data==true){
				$("#idtipe").val('#').trigger('change');
				$("#idpoli").val('#').trigger('change');
				$("#statuspasienbaru").val('#').trigger('change');
				$("#pertemuan_id").val('#').trigger('change');
				$("#operand").val('0').trigger('change');
				$("#st_tujuan_terakhir").val('0').trigger('change');
				$("#lama_terakhir_tujan").val('0');
				$("#st_formulir").val('0').trigger('change');
				$('#index_tampil').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});

			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
		}
	});

});
$("#idtipe").change(function(){
		$.ajax({
			url: '{site_url}setting_invasif/find_poli/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#idpoli").empty();
				$("#idpoli").append(data);
			}
		});

});
$("#idtipe_default").change(function(){
		$.ajax({
			url: '{site_url}setting_invasif/find_poli/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#idpoli_default").empty();
				$("#idpoli_default").append(data);
			}
		});

});
function load_formulir(){
	$('#index_tampil').DataTable().destroy();	
	table = $('#index_tampil').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			"columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [3,2,4,5,6] },
					// { "width": "15%", "targets": [1]},
					// { "width": "8%", "targets": [8]},
					// { "width": "30%", "targets": 3,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}setting_invasif/load_formulir', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function hapus_formulir(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_invasif/hapus_formulir',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_tampil').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
</script>