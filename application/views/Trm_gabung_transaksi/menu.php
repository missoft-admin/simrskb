<div class="content-grid">
	<div class="row">
	<?php if (UserAccesForm($user_acces_form,array('1196'))){ ?>
		<div class="col-xs-6 col-sm-4 col-lg-2">
			<a class="block block-link-hover2 text-center" href="<?=($this->uri->segment(1)=='trm_gabung_transaksi'?'javascript:void(0)':'{site_url}trm_gabung_transaksi')?>">
				<div class="block-content block-content-full <?=($this->uri->segment(1)=='trm_gabung_transaksi'?'bg-primary':'')?>">
					<i class="fa fa-credit-card fa-4x text-<?=($this->uri->segment(1)=='trm_gabung_transaksi'?'white':'danger')?>"></i>
					<div class="font-w600 <?=($this->uri->segment(1)=='trm_gabung_transaksi'?'text-white-op':'')?> push-15-t">Gabung Transaksi</div>
				</div>
			</a>
		</div>
	<?}?>
	<?php if (UserAccesForm($user_acces_form,array('1194'))){ ?>
		<div class="col-xs-6 col-sm-4 col-lg-2">
			<a class="block block-link-hover3 text-center" href="<?=($this->uri->segment(1)=='trm_gabung_transaksi_history'?'javascript:void(0)':'{site_url}trm_gabung_transaksi_history')?>">
				<div class="block-content block-content-full <?=($this->uri->segment(1)=='trm_gabung_transaksi_history'?'bg-primary':'')?>">
					<i class="fa fa-history fa-4x text-<?=($this->uri->segment(1)=='trm_gabung_transaksi_history'?'white':'danger')?>"></i>
					<div class="font-w600 <?=($this->uri->segment(1)=='trm_gabung_transaksi_history'?'text-white-op':'')?> push-15-t">History</div>
				</div>
			</a>
		</div>
	<?}?>
	</div>
</div>
