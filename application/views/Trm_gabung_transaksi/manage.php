<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('1196'))){ ?>
<?
	$this->load->view('Trm_gabung_transaksi/menu');
?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}Trefund/index" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-bottom: 0px;">
	</div>
	<div class="block-content">
		<form class="form-horizontal" action="{base_url}trm_gabung_transaksi/save" method="POST">
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-danger alert-dismissable" id="alertInfo" hidden>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		        <p></p>
		      </div>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px; margin-left: 0;">
						<b><span class="label label-success" style="font-size:12px">Data Transaksi Yang Akan Digabung</span></b>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" style="text-align: left;">No. Medrec</label>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-8">
									<input type="hidden" id="idpasien" name="idpasien" readonly required value="">
									<input type="text" id="nomedrec" class="form-control" name="nomedrec" readonly value="">
								</div>
								<div class="col-md-4">
									<button class="btn btn-primary" id="btnFind" type="button" style="width:100%;" data-toggle="modal" data-target="#modalFindTransaksi">Cari Pasien</button>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" style="text-align: left;">Nama Pasien</label>
						<div class="col-md-9">
							<input type="text" id="namapasien" class="form-control" name="namapasien" readonly value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" style="text-align: left;">Tanggal Lahir</label>
						<div class="col-md-9">
							<input type="text" id="tanggallahir" class="form-control" name="tanggallahir" readonly value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" style="text-align: left;">Jenis Kelamin</label>
						<div class="col-md-9">
							<input type="text" id="jeniskelamin" class="form-control" name="jeniskelamin" readonly value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" style="text-align: left;">Alamat</label>
						<div class="col-md-9">
							<input type="text" id="alamatpasien" class="form-control" name="alamatpasien" readonly value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" style="text-align: left;">Tanggal Transaksi</label>
						<div class="col-md-9">
							<input type="hidden" id="idtransaksi" name="idtransaksi" readonly required value="">
							<input type="text" id="tanggaltransaksi" class="form-control" name="tanggaltransaksi" readonly value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" style="text-align: left;">Jenis Transaksi</label>
						<div class="col-md-9">
							<input type="text" id="jenistransaksi" class="form-control" name="jenistransaksi" readonly value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" style="text-align: left;">No Transaksi</label>
						<div class="col-md-9">
							<input type="text" id="notransaksi" class="form-control" name="notransaksi" readonly value="">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px; margin-left: 0;">
						<b><span class="label label-success" style="font-size:12px">Data Transaksi Yang Dituju</span></b>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" style="text-align: left;">No. Medrec</label>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-8">
									<input type="text" id="nomedrec_new" class="form-control" name="nomedrec_new" readonly value="">
								</div>
								<div class="col-md-4">
									<button class="btn btn-primary" id="btnFind_new" type="button" style="width:100%;" data-toggle="modal" data-target="#modalFindTransaksi">Cari Pasien</button>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" style="text-align: left;">Nama Pasien</label>
						<div class="col-md-9">
							<input type="hidden" id="idpasien_new" name="idpasien_new" readonly required value="">
							<input type="text" id="namapasien_new" class="form-control" name="namapasien_new" readonly value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" style="text-align: left;">Tanggal Lahir</label>
						<div class="col-md-9">
							<input type="text" id="tanggallahir_new" class="form-control" name="tanggallahir_new" readonly value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" style="text-align: left;">Jenis Kelamin</label>
						<div class="col-md-9">
							<input type="text" id="jeniskelamin_new" class="form-control" name="jeniskelamin_new" readonly value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" style="text-align: left;">Alamat</label>
						<div class="col-md-9">
							<input type="text" id="alamatpasien_new" class="form-control" name="alamatpasien_new" readonly value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" style="text-align: left;">Tanggal Transaksi</label>
						<div class="col-md-9">
							<input type="hidden" id="idtransaksi_new" name="idtransaksi_new" readonly required value="">
							<input type="text" id="tanggaltransaksi_new" class="form-control" name="tanggaltransaksi_new" readonly value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" style="text-align: left;">Jenis Transaksi</label>
						<div class="col-md-9">
							<input type="text" id="jenistransaksi_new" class="form-control" name="jenistransaksi_new" readonly value="">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" style="text-align: left;">No Transaksi</label>
						<div class="col-md-9">
							<input type="text" id="notransaksi_new" class="form-control" name="notransaksi_new" readonly value="">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<hr>
					<label class="control-label" style="text-align: left; margin-bottom: 10px;">Alasan Digabung :</label>
					<textarea name="alasan" class="form-control" rows="8" cols="80" required></textarea>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-12">
					<div style="float:right;padding:0 0 14px 0;">
						<button class="btn btn-success" type="submit" name="button" style="width:100px;font-size:12px;">Proses</button>
						<a href="{base_url}Trefund/index" class="btn btn-danger" type="button" name="button" style="width:100px;font-size:12px;">Batal</a>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<?}?>
<input type="hidden" id="tipepasien" value="">

<div class="modal in" id="modalFindTransaksi" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-popout" style="width:1200px;">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close">
								<i class="fa fa-close" style="display:block;font-size:15px;"></i>
							</button>
						</li>
					</ul>
					<h3 class="block-title">Cari Transaksi</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="col-md-8 form-horizontal">
							<div class="form-val">
								<label class="col-sm-2" for="">Tipe</label>
								<div class="col-sm-10">
									<select id="tIdTipeTransaksi" class="js-select2 form-control" style="width: 100%;">
										<option value="0">Semua Tipe</option>
										<option value="1">Rawat Jalan</option>
										<option value="2">Rawat Inap</option>
										<option value="3">One Day Surgery (ODS)</option>
									</select>
								</div>
							</div>
							<div class="form-val" style="margin-top:50px">
								<label class="col-sm-2" for="" style="margin-top: 8px;">Tanggal</label>
								<div class="col-sm-10">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input id="tTanggalAwal" class="form-control datepicker" type="text" placeholder="Tanggal Awal" value="<?= date("d/m/Y") ?>">
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input id="tTanggalAkhir" class="form-control datepicker" type="text" placeholder="Tanggal Akhir" value="<?= date("d/m/Y") ?>">
									</div>
								</div>
							</div>
							<div class="form-val" style="margin-top:50px">
								<label class="col-sm-2" for="" style="margin-top: 8px;"></label>
								<div class="col-sm-10" style="margin-top:15px">
									<button type="submit" id="tFilter" class="btn btn-success" style="font-size: 12px;">Filter</button>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div class="text-uppercase">
						<table id="search-list" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Tipe</th>
									<th>No Transaksi</th>
									<th>No Medrec</th>
									<th>Nama Pasien</th>
									<th>Kelompok Pasien</th>
									<th>Dokter Penanggung Jawab</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function() {

		let delay = (function() {
			let timer = 0;
			return function(callback, ms) {
				clearTimeout(timer);
				timer = setTimeout(callback, ms);
			};
		})();

		getTransaksi(null, null, 0);

		$("#btnFind").click(function() {
			$("#tipepasien").val('pasienlama');
		});

		$("#btnFind_new").click(function() {
			$("#tipepasien").val('pasienbaru');
		});

		$('#tFilter').click(function() {
			let tanggalAwal = $('#tTanggalAwal').val();
			let tanggalAkhir = $('#tTanggalAkhir').val();
			let idTipeTransaksi = $('#tIdTipeTransaksi').val();

			getTransaksi(tanggalAwal, tanggalAkhir, idTipeTransaksi);
		});

		$(document).on('click', '.selectTransaksi', function() {
			$(".separator").show();

			let data = JSON.parse(decodeURIComponent($(this).data("transaksi")));
			let tipepasien = $("#tipepasien").val();

			if (tipepasien == 'pasienlama') {
				$('#idpasien').val(data['idpasien']);
				$('#nomedrec').val(data['nomedrec']);
				$('#namapasien').val(data['namapasien']);
				$('#tanggallahir').val(data['tanggallahir']);
				$('#jeniskelamin').val(data['jeniskelamin']);
				$('#alamatpasien').val(data['alamatpasien']);
				$('#idtransaksi').val(data['idtransaksi']);
				$('#tanggaltransaksi').val(data['tanggaltransaksi']);
				$('#jenistransaksi').val(data['jenistransaksi']);
				$('#notransaksi').val(data['notransaksi']);

				$("#modalFindTransaksi .close").click();
			} else {
				if (checkMatchTransaksi(data['jenistransaksi'], data['notransaksi'])) {
					$('#idpasien_new').val(data['idpasien']);
					$('#nomedrec_new').val(data['nomedrec']);
					$('#namapasien_new').val(data['namapasien']);
					$('#tanggallahir_new').val(data['tanggallahir']);
					$('#jeniskelamin_new').val(data['jeniskelamin']);
					$('#alamatpasien_new').val(data['alamatpasien']);
					$('#idtransaksi_new').val(data['idtransaksi']);
					$('#tanggaltransaksi_new').val(data['tanggaltransaksi']);
					$('#jenistransaksi_new').val(data['jenistransaksi']);
					$('#notransaksi_new').val(data['notransaksi']);

					checkMatchPasien();

					$("#modalFindTransaksi .close").click();
				}
			}
		});
	});

	function getTransaksi(tanggalAwal, tanggalAkhir, idTipeTransaksi) {
		$('#search-list').DataTable().destroy();
		$('#search-list').DataTable({
			"oLanguage": {
				"sEmptyTable": "<i class='text-uppercase'>NO DATA AVAILABLE IN TABLE</i>",
			},
			"ajax": {
				"url": "{base_url}trm_gabung_transaksi/getTransaksi",
				"type": "POST",
				"data": {
					"tanggalawal": tanggalAwal,
					"tanggalakhir": tanggalAkhir,
					"idtipetransaksi": idTipeTransaksi,
				}
			},
			"autoWidth": false,
			"pageLength": 10,
			"lengthChange": false,
			"searching": true,
			"ordering": false,
			"processing": true,
			"bInfo": true,
			"bPaginate": true,
			"order": [],
			"initComplete": function(settings, json) {
			}
		});
	}

	function checkMatchPasien() {
		let namapasien = $('#namapasien').val();
		let tanggallahir = $('#tanggallahir').val();
		let jeniskelamin = $('#jeniskelamin').val();

		let namapasien_new = $('#namapasien_new').val();
		let tanggallahir_new = $('#tanggallahir_new').val();
		let jeniskelamin_new = $('#jeniskelamin_new').val();

		if (namapasien != namapasien_new || tanggallahir != tanggallahir_new || jeniskelamin != jeniskelamin_new) {
			$('#alertInfo').show();
			$('#alertInfo p').html('Data Pasien Berbeda, Apakah Anda Yakin Akan Menggabungkan?');
		} else {
			$('#alertInfo').hide();
			$('#alertInfo p').html('');
		}
	}

	function checkMatchTransaksi(jenisTransaksiNew, noTransaksiNew) {
		let jenisTransaksi = $('#jenistransaksi').val();
		let noTransaksi = $('#notransaksi').val();

		if (jenisTransaksi == 'Rawat Jalan' && jenisTransaksiNew == 'Rawat Jalan') {
			return true;
		} else if (jenisTransaksi == 'Rawat Jalan' && jenisTransaksiNew == 'Rawat Inap') {
			return true;
		} else if (jenisTransaksi == 'Rawat Inap' && jenisTransaksiNew == 'One Day Surgery (ODS)') {
			return true;
		} else if (jenisTransaksi == 'One Day Surgery (ODS)' && jenisTransaksiNew == 'Rawat Inap') {
			return true;
		} else {
			if (noTransaksiNew == noTransaksi) {
				swal('Gabung Transaksi Hanya Bisa Dilakukan Untuk Transaksi Berbeda', 'Silahkan Untuk Memilih Ulang Transaksi Lainnya','warning');	
			} else {
				swal('Gabung Transaksi Hanya Bisa Dilakukan Untuk Transaksi', '<ul style="text-align:left"><li>Rawat Jalan ke Rawat Jalan</li><li>Rawat Jalan ke Rawat Inap</li><li> One Day Surgery (ODS) ke Rawat Inap</li></ul>','warning');
			}
		}

		return false;
	}
</script>
