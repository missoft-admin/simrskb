<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('2088'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_general()"><i class="fa fa-bell"></i> General</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2090'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_event"  onclick="load_tab2()"><i class="fa fa-check-square-o"></i> Logic</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2093'))){ ?>
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab_label" ><i class="si si-note"></i> Label</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2095'))){ ?>
		<li class="<?=($tab=='4'?'active':'')?>">
			<a href="#tab_4" onclick="load_tab4()"><i class="fa fa-user-md"></i> User</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2096'))){ ?>
		<li class="<?=($tab=='5'?'active':'')?>">
			<a href="#tab_5" ><i class="fa fa-paint-brush"></i> Paraf</a>
		</li>
		<?}?>
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('2088'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_info">
			
			<?php echo form_open('setting_risiko_tinggi/save_assesmen','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_header">Judul Header<span style="color:red;">*</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_header" name="judul_header" value="{judul_header}" placeholder="Header">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_footer">Judul Footer<span style="color:red;">*</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_footer" name="judul_footer" value="{judul_footer}" placeholder="Footer">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_header_eng">Judul Header <span style="color:red;"> ENG *</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_header_eng" name="judul_header_eng" value="{judul_header_eng}" placeholder="Header">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="footer_eng">Judul Footer<span style="color:red;">ENG *</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="footer_eng" name="footer_eng" value="{footer_eng}" placeholder="Footer">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_edit_catatan">Izin Edit Catatan</label>
						<div class="col-md-2">
							<select id="st_edit_catatan" name="st_edit_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_edit_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_edit_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_edit">Waktu Edit</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_edit" name="lama_edit" value="{lama_edit}" placeholder="Waktu Edit">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_edit">Edit Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_edit" name="orang_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_edit=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_edit=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_hapus_catatan">Izin Hapus Catatan</label>
						<div class="col-md-2">
							<select id="st_hapus_catatan" name="st_hapus_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_hapus_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_hapus_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_hapus">Waktu Hapus</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_hapus" name="lama_hapus" value="{lama_hapus}" placeholder="Waktu Hapus">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_hapus">Hapus Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_hapus" name="orang_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_hapus=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_hapus=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_duplikasi_catatan">Izin Duplikasi Catatan</label>
						<div class="col-md-2">
							<select id="st_duplikasi_catatan" name="st_duplikasi_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_duplikasi_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_duplikasi_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_duplikasi">Waktu Duplikasi</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_duplikasi" name="lama_duplikasi" value="{lama_duplikasi}" placeholder="Waktu Duplikasi">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_duplikasi">Duplikasi Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_duplikasi" name="orang_duplikasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_duplikasi=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_duplikasi=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				
				<?php if (UserAccesForm($user_acces_form,array('2089'))){ ?>
				<div class="col-md-12 push-10">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="statusstok"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_general" name="btn_save_general"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
				<?}?>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2093'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?> " id="tab_label">
			
			<?php echo form_open('setting_risiko_tinggi/save_label','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">PEMBERI INFORMASI</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="label_pemberi_info_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="label_pemberi_info_ina" name="label_pemberi_info_ina" value="{label_pemberi_info_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="label_pemberi_info_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="label_pemberi_info_eng" name="label_pemberi_info_eng" value="{label_pemberi_info_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Dokter Pelaksana Tindakan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="dokter_pelaksan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="dokter_pelaksan_ina" name="dokter_pelaksan_ina" value="{dokter_pelaksan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="dokter_pelaksan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="dokter_pelaksan_eng" name="dokter_pelaksan_eng" value="{dokter_pelaksan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Waktu Pemberian Penjelasan Tindakan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="waktu_tindakan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="waktu_tindakan_ina" name="waktu_tindakan_ina" value="{waktu_tindakan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="waktu_tindakan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="waktu_tindakan_eng" name="waktu_tindakan_eng" value="{waktu_tindakan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Tanggal Pemberian Penjelasan Tindakan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="tanggal_tindakan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="tanggal_tindakan_ina" name="tanggal_tindakan_ina" value="{tanggal_tindakan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="tanggal_tindakan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="tanggal_tindakan_eng" name="tanggal_tindakan_eng" value="{tanggal_tindakan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Nama Pemberi Informasi</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_pemberi_info_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_pemberi_info_ina" name="nama_pemberi_info_ina" value="{nama_pemberi_info_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_pemberi_info_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_pemberi_info_eng" name="nama_pemberi_info_eng" value="{nama_pemberi_info_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Nama Petugas Yang Mendampingi</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_petugas_pendamping_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_petugas_pendamping_ina" name="nama_petugas_pendamping_ina" value="{nama_petugas_pendamping_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_petugas_pendamping_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_petugas_pendamping_eng" name="nama_petugas_pendamping_eng" value="{nama_petugas_pendamping_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Nama Penerima Informasi</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_penerima_info_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_penerima_info_ina" name="nama_penerima_info_ina" value="{nama_penerima_info_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_penerima_info_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_penerima_info_eng" name="nama_penerima_info_eng" value="{nama_penerima_info_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Hubungan Dengan pasien</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="hubungan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="hubungan_ina" name="hubungan_ina" value="{hubungan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="hubungan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="hubungan_eng" name="hubungan_eng" value="{hubungan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Jenis Informasi</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="jenis_info_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="jenis_info_ina" name="jenis_info_ina" value="{jenis_info_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="jenis_info_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="jenis_info_eng" name="jenis_info_eng" value="{jenis_info_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Isi Informasi</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="isi_info_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="isi_info_ina" name="isi_info_ina" value="{isi_info_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="isi_info_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="isi_info_eng" name="isi_info_eng" value="{isi_info_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Paraf</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="paraf_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="paraf_ina" name="paraf_ina" value="{paraf_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="paraf_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="paraf_eng" name="paraf_eng" value="{paraf_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Akses tanda tangan Dokter</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ttd_dokter_ina">Indonesia</label>
								<div class="col-md-10">
									<select id="st_auto_ttd_user" name="st_auto_ttd_user" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="1" <?=($st_auto_ttd_user=='1'?'selected':'')?>>Ambil Otomatis</option>
										<option value="0" <?=($st_auto_ttd_user=='0'?'selected':'')?>>Tanda tangan sendiri</option>
										
									</select>
								</div>
							</div>
						</div>
						
						
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Pop Up Gambar Tanda Tangan Dokter</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_gambar_ttd_dokter_ina">Indonesia</label>
								<div class="col-md-10">
									
									<input class="form-control " type="text" id="ket_gambar_ttd_dokter_ina" name="ket_gambar_ttd_dokter_ina" value="{ket_gambar_ttd_dokter_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_gambar_ttd_dokter_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_gambar_ttd_dokter_eng" name="ket_gambar_ttd_dokter_eng" value="{ket_gambar_ttd_dokter_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Pop Up Nama Tanda Tangan Dokter</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_ttd_dokter_ina">Indonesia</label>
								<div class="col-md-10">
									
									<input class="form-control " type="text" id="nama_ttd_dokter_ina" name="nama_ttd_dokter_ina" value="{nama_ttd_dokter_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_ttd_dokter_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_ttd_dokter_eng" name="nama_ttd_dokter_eng" value="{nama_ttd_dokter_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Tanda tangan Dokter</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ttd_dokter_ina">Indonesia</label>
								<div class="col-md-10">
									
									<input class="form-control " type="text" id="ttd_dokter_ina" name="ttd_dokter_ina" value="{ttd_dokter_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ttd_dokter_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ttd_dokter_eng" name="ttd_dokter_eng" value="{ttd_dokter_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Keterangan Tanda tangan Dokter</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_ttd_dokter_ina">Indonesia</label>
								<div class="col-md-10">
									<textarea class="form-control " name="ket_ttd_dokter_ina"  rows="2" placeholder="ket_ttd_dokter_ina"> <?=$ket_ttd_dokter_ina?></textarea>
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_ttd_dokter_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<textarea class="form-control " name="ket_ttd_dokter_eng"  rows="2" placeholder="ket_ttd_dokter_eng"> <?=$ket_ttd_dokter_eng?></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Tanda tangan Pasien</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ttd_pasien_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ttd_pasien_ina" name="ttd_pasien_ina" value="{ttd_pasien_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ttd_pasien_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ttd_pasien_eng" name="ttd_pasien_eng" value="{ttd_pasien_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Keterangan Tanda tangan Pasien</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_ttd_pasien_ina">Indonesia</label>
								<div class="col-md-10">
									<textarea class="form-control " name="ket_ttd_pasien_ina"  rows="2" placeholder="ket_ttd_pasien_ina"> <?=$ket_ttd_pasien_ina?></textarea>
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_ttd_pasien_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<textarea class="form-control " name="ket_ttd_pasien_eng"  rows="2" placeholder="ket_ttd_pasien_eng"> <?=$ket_ttd_pasien_eng?></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Pop Up Gambar Tanda Tangan Penerima Informasi</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_gambar_ttd_pasien_ina">Indonesia</label>
								<div class="col-md-10">
									
									<input class="form-control " type="text" id="ket_gambar_ttd_pasien_ina" name="ket_gambar_ttd_pasien_ina" value="{ket_gambar_ttd_pasien_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_gambar_ttd_pasien_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_gambar_ttd_pasien_eng" name="ket_gambar_ttd_pasien_eng" value="{ket_gambar_ttd_pasien_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Pop Up Nama Tanda Tangan Penerima Informasi</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_ttd_kel_ina">Indonesia</label>
								<div class="col-md-10">
									
									<input class="form-control " type="text" id="nama_ttd_kel_ina" name="nama_ttd_kel_ina" value="{nama_ttd_kel_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_ttd_kel_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_ttd_kel_eng" name="nama_ttd_kel_eng" value="{nama_ttd_kel_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">PERNYATAAN</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="label_pernyataan_ina">Indonesia</label>
								<div class="col-md-10">
									<textarea class="form-control " name="label_pernyataan_ina"  rows="2" placeholder="label_pernyataan_ina"> <?=$label_pernyataan_ina?></textarea>
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="label_pernyataan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<textarea class="form-control " name="label_pernyataan_eng"  rows="2" placeholder="label_pernyataan_eng"> <?=$label_pernyataan_eng?></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Info Pernyataan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="info_pernyataan_ina">Indonesia</label>
								<div class="col-md-10">
									<textarea class="form-control " name="info_pernyataan_ina"  rows="2" placeholder="info_pernyataan_ina"> <?=$info_pernyataan_ina?></textarea>
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="info_pernyataan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<textarea class="form-control " name="info_pernyataan_eng"  rows="2" placeholder="info_pernyataan_eng"> <?=$info_pernyataan_eng?></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Nama Pembuat Pernyataan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_pembuat_pernyataan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_pembuat_pernyataan_ina" name="nama_pembuat_pernyataan_ina" value="{nama_pembuat_pernyataan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_pembuat_pernyataan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_pembuat_pernyataan_eng" name="nama_pembuat_pernyataan_eng" value="{nama_pembuat_pernyataan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Umur Pembuat Pernyataan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="umur_pembuat_pernyataan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="umur_pembuat_pernyataan_ina" name="umur_pembuat_pernyataan_ina" value="{umur_pembuat_pernyataan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="umur_pembuat_pernyataan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="umur_pembuat_pernyataan_eng" name="umur_pembuat_pernyataan_eng" value="{umur_pembuat_pernyataan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Jenis Kelamin Pembuat Pernyataan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="jk_pembuat_pernyataan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="jk_pembuat_pernyataan_ina" name="jk_pembuat_pernyataan_ina" value="{jk_pembuat_pernyataan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="jk_pembuat_pernyataan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="jk_pembuat_pernyataan_eng" name="jk_pembuat_pernyataan_eng" value="{jk_pembuat_pernyataan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Alamat Pembuat Pernyataan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="alamat_pembuat_pernyataan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="alamat_pembuat_pernyataan_ina" name="alamat_pembuat_pernyataan_ina" value="{alamat_pembuat_pernyataan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="alamat_pembuat_pernyataan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="alamat_pembuat_pernyataan_eng" name="alamat_pembuat_pernyataan_eng" value="{alamat_pembuat_pernyataan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Bukti Diri Pembuat Pernyataan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="nik_pembuat_pernyataan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nik_pembuat_pernyataan_ina" name="nik_pembuat_pernyataan_ina" value="{nik_pembuat_pernyataan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="nik_pembuat_pernyataan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nik_pembuat_pernyataan_eng" name="nik_pembuat_pernyataan_eng" value="{nik_pembuat_pernyataan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Dengan Ini Memberikan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="dengan_ini_memberikan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="dengan_ini_memberikan_ina" name="dengan_ini_memberikan_ina" value="{dengan_ini_memberikan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="dengan_ini_memberikan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="dengan_ini_memberikan_eng" name="dengan_ini_memberikan_eng" value="{dengan_ini_memberikan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Untuk Tindakan Medis</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="untuk_tindakan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="untuk_tindakan_ina" name="untuk_tindakan_ina" value="{untuk_tindakan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="untuk_tindakan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="untuk_tindakan_eng" name="untuk_tindakan_eng" value="{untuk_tindakan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Terhadap</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="terhadap_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="terhadap_ina" name="terhadap_ina" value="{terhadap_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="terhadap_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="terhadap_eng" name="terhadap_eng" value="{terhadap_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">IDENTITAS PASIEN</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="label_identitas_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="label_identitas_ina" name="label_identitas_ina" value="{label_identitas_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="label_identitas_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="label_identitas_eng" name="label_identitas_eng" value="{label_identitas_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">No. Registrasi</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="no_reg_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="no_reg_ina" name="no_reg_ina" value="{no_reg_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="no_reg_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="no_reg_eng" name="no_reg_eng" value="{no_reg_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Nama Pasien</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_pasien_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_pasien_ina" name="nama_pasien_ina" value="{nama_pasien_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="nama_pasien_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="nama_pasien_eng" name="nama_pasien_eng" value="{nama_pasien_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Tanggal lahir</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="tanggal_lahir_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="tanggal_lahir_ina" name="tanggal_lahir_ina" value="{tanggal_lahir_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="tanggal_lahir_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="tanggal_lahir_eng" name="tanggal_lahir_eng" value="{tanggal_lahir_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Usia</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="usia_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="usia_ina" name="usia_ina" value="{usia_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="usia_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="usia_eng" name="usia_eng" value="{usia_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Jenis Kelamin</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="jenis_kelamin_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="jenis_kelamin_ina" name="jenis_kelamin_ina" value="{jenis_kelamin_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="jenis_kelamin_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="jenis_kelamin_eng" name="jenis_kelamin_eng" value="{jenis_kelamin_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">No Rekam Medis</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="norek_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="norek_ina" name="norek_ina" value="{norek_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="norek_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="norek_eng" name="norek_eng" value="{norek_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Waktu Pernyataan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="waktu_pernyataan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="waktu_pernyataan_ina" name="waktu_pernyataan_ina" value="{waktu_pernyataan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="waktu_pernyataan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="waktu_pernyataan_eng" name="waktu_pernyataan_eng" value="{waktu_pernyataan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Lokasi Pernyataan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="lokasi_pernyataan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="lokasi_pernyataan_ina" name="lokasi_pernyataan_ina" value="{lokasi_pernyataan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="lokasi_pernyataan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="lokasi_pernyataan_eng" name="lokasi_pernyataan_eng" value="{lokasi_pernyataan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Keterangan Pernyataan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_pernyataan_ina">Indonesia</label>
								<div class="col-md-10">
									<textarea class="form-control " name="ket_pernyataan_ina"  rows="2" placeholder="ket_pernyataan_ina"> <?=$ket_pernyataan_ina?></textarea>
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_pernyataan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<textarea class="form-control " name="ket_pernyataan_eng"  rows="2" placeholder="ket_pernyataan_eng"> <?=$ket_pernyataan_eng?></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php if (UserAccesForm($user_acces_form,array('2094'))){ ?>
				<div class="col-md-12 push-10">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="btn_save_label"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_label" name="btn_save_label"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
				<?}?>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2096'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='5'?'active in':'')?> " id="tab_5">
			
			<?php echo form_open('setting_risiko_tinggi/save_paraf','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('PARAF PENERIMA INFORMASI')?></h5>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Keterangan Gambar Tanda Tangan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_gambar_paraf_penerima_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_gambar_paraf_penerima_ina" name="ket_gambar_paraf_penerima_ina" value="{ket_gambar_paraf_penerima_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_gambar_paraf_penerima_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_gambar_paraf_penerima_eng" name="ket_gambar_paraf_penerima_eng" value="{ket_gambar_paraf_penerima_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Keterangan Nama Jelas</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_nama_paraf_penerima_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_nama_paraf_penerima_ina" name="ket_nama_paraf_penerima_ina" value="{ket_nama_paraf_penerima_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_nama_paraf_penerima_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_nama_paraf_penerima_eng" name="ket_nama_paraf_penerima_eng" value="{ket_nama_paraf_penerima_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('PARAF YANG MENYATAKAN')?></h5>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Keterangan Gambar Tanda Tangan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_gambar_paraf_menyatakan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_gambar_paraf_menyatakan_ina" name="ket_gambar_paraf_menyatakan_ina" value="{ket_gambar_paraf_menyatakan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_gambar_paraf_menyatakan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_gambar_paraf_menyatakan_eng" name="ket_gambar_paraf_menyatakan_eng" value="{ket_gambar_paraf_menyatakan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Keterangan Nama Jelas</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_nama_paraf_menyatakan_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_nama_paraf_menyatakan_ina" name="ket_nama_paraf_menyatakan_ina" value="{ket_nama_paraf_menyatakan_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_nama_paraf_menyatakan_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_nama_paraf_menyatakan_eng" name="ket_nama_paraf_menyatakan_eng" value="{ket_nama_paraf_menyatakan_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('PARAF SAKSI KELUARGA')?></h5>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Keterangan Gambar Tanda Tangan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_gambar_paraf_saksi_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_gambar_paraf_saksi_ina" name="ket_gambar_paraf_saksi_ina" value="{ket_gambar_paraf_saksi_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_gambar_paraf_saksi_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_gambar_paraf_saksi_eng" name="ket_gambar_paraf_saksi_eng" value="{ket_gambar_paraf_saksi_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Keterangan Nama Jelas</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_nama_paraf_saksi_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_nama_paraf_saksi_ina" name="ket_nama_paraf_saksi_ina" value="{ket_nama_paraf_saksi_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_nama_paraf_saksi_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_nama_paraf_saksi_eng" name="ket_nama_paraf_saksi_eng" value="{ket_nama_paraf_saksi_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('PARAF SAKSI RS')?></h5>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Keterangan Gambar Tanda Tangan</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_gambar_paraf_saksi_rs_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_gambar_paraf_saksi_rs_ina" name="ket_gambar_paraf_saksi_rs_ina" value="{ket_gambar_paraf_saksi_rs_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_gambar_paraf_saksi_rs_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_gambar_paraf_saksi_rs_eng" name="ket_gambar_paraf_saksi_rs_eng" value="{ket_gambar_paraf_saksi_rs_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="col-md-12 text-primary h5">Keterangan Nama Jelas</label>
						<div class="col-md-6"  style="margin-top: 15px;">
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_nama_paraf_saksi_rs_ina">Indonesia</label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_nama_paraf_saksi_rs_ina" name="ket_nama_paraf_saksi_rs_ina" value="{ket_nama_paraf_saksi_rs_ina}" >
								</div>
							</div>
						</div>
						
						<div class="col-md-6" style="margin-top: 15px;">	
							<div class="form-group">
								<label class="col-md-2 control-label" for="ket_nama_paraf_saksi_rs_eng">English<span style="color:red;">*</span></label>
								<div class="col-md-10">
									<input class="form-control " type="text" id="ket_nama_paraf_saksi_rs_eng" name="ket_nama_paraf_saksi_rs_eng" value="{ket_nama_paraf_saksi_rs_eng}">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
				<div class="col-md-12 push-10">
					<div class="form-group" style="margin-bottom: 5px;">
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_label" name="btn_save_label"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
				
				
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2090'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_event">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('HAK AKSES FORMULIR')?></h5>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_hak_akses">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Profesi</th>
										<th width="15%">Spesialisasi</th>
										<th width="15%">PPA</th>
										<th width="15%">Melihat</th>
										<th width="10%">Input</th>
										<th width="10%">Edit</th>
										<th width="10%">Hapus</th>
										<th width="10%">Cetak</th>
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="profesi_id" name="profesi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" <?=($profesi_id=='#'?'selected':'')?>>-Pilih Profesi-</option>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" <?=($profesi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="spesialisasi_id" name="spesialisasi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($spesialisasi_id=='#'?'selected':'')?>>-SEMUA-</option>
												<?foreach(list_variable_ref(22) as $r){?>
													<option value="<?=$r->id?>" <?=($spesialisasi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										<th>
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($mppa_id=='#'?'selected':'')?>>-SEMUA-</option>
												
											</select>
										</th>
										
										<th>
											<select id="st_lihat" name="st_lihat" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_lihat=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_lihat=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_input" name="st_input" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_input=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_input=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_edit" name="st_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_edit=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_edit=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_hapus" name="st_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_hapus=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_hapus=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_cetak" name="st_cetak" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_cetak=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_cetak=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<?php if (UserAccesForm($user_acces_form,array('2091'))){ ?>
											<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_hak_akses" name="btn_tambah_hak_akses"><i class="fa fa-plus"></i> Tambah</button>
											<?}?>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('STATUS TAMPIL FORMULIR')?></h5>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_tampil">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Tujuan</th>
										<th width="15%">Poliklinik</th>
										<th width="15%">Status Pasien</th>
										<th width="15%">Kasus</th>
										<th width="10%">Lihat Rujukan Terakhir</th>
										<th width="10%">Lama Terakhir Kunjungan Tearkhir</th>
										<th width="10%">Status Formulir</th>
										<th width="10%">Action</th>										   
									</tr>
									<?
										$idtipe='#';
										$idpoli='#';
										$statuspasienbaru='#';
										$pertemuan_id='#';
										$st_tujuan_terakhir='0';
										$lama_terakhir_tujan='0';
										$st_formulir='0';
									?>
									<tr>
										<th>#</th>
										<th>
											<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" <?=($idtipe=='#'?'selected':'')?>>-Tujuan-</option>
												<option value="1" <?=($idtipe=='1'?'selected':'')?>>POLIKINIK</option>
												<option value="2" <?=($idtipe=='2'?'selected':'')?>>IGD</option>
												
											</select>
										</th>
										<th>
											<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($idpoli=='#'?'selected':'')?>>-Semua Poli-</option>
												<?foreach(get_all('mpoliklinik',array('status'=>1)) as $r){?>
													<option value="<?=$r->id?>" <?=($idpoli==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										
										<th>
											<select tabindex="4" id="statuspasienbaru" name="statuspasienbaru" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="2" selected>SEMUA</option>
												<option value="1" <?=($statuspasienbaru == 1 ? 'selected="selected"' : '')?>>PASIEN BARU</option>
												<option value="0" <?=($statuspasienbaru == 0 ? 'selected="selected"' : '')?>>PASIEN LAMA</option>
											</select>
										</th>
										<th>
											<select id="pertemuan_id" name="pertemuan_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($pertemuan_id==''?'selected':'')?>>- All Kasus -</option>
												<?foreach(list_variable_ref(15) as $r){?>
												<option value="<?=$r->id?>" <?=($pertemuan_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="st_tujuan_terakhir" name="st_tujuan_terakhir" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_tujuan_terakhir=='1'?'selected':'')?>>YA</option>
												<option value="0" <?=($st_tujuan_terakhir=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										
										<th>
											<select name="operand" style="width: 100%" id="operand" class="js-select2 form-control input-sm">					
										
												<option value="0" selected>TIDAK DITENTUKAN</option>										
												<option value=">">></option>										
												<option value=">=">>=</option>	
												<option value="<"><</option>
												<option value="<="><=</option>
												<option value="=">=</option>										
											</select>
											<input type="text" style="width: 100%"  class="form-control number" id="lama_terakhir_tujan" placeholder="0" name="lama_terakhir_tujan" value="0">
										</th>
										
										
										<th>
											<select id="st_formulir" name="st_formulir" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="0" <?=($st_formulir=='0'?'selected':'')?>>-PILIH-</option>
												<option value="1" <?=($st_formulir=='1'?'selected':'')?>>TAMPIL (Wajib diisi)</option>
												<option value="2" <?=($st_formulir=='2'?'selected':'')?>>TAMPIL (Tidak Wajib Diisi</option>
												<option value="3" <?=($st_formulir=='3'?'selected':'')?>>TIDAK DITAMPILKAN</option>
												
											</select>
										</th>
										
										<th>
											<?php if (UserAccesForm($user_acces_form,array('2092'))){ ?>
											<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_formulir" name="btn_tambah_formulir"><i class="fa fa-plus"></i> Tambah</button>
											<?}?>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2095'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='4'?'active in':'')?>" id="tab_4">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('DOKTER PELAKSANA TINDAKAN')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-12">
							<label for="st_setting_dokter">Pengaturan Dokter Pelaksana</label>
							<select id="st_setting_dokter" name="st_setting_dokter" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Choose one..">
												
								<option value="#" <?=($st_setting_dokter=='#'?'selected':'')?>>-Pilih Setting Dokter-</option>
								<option value="1" <?=($st_setting_dokter=='1'?'selected':'')?>>User Login</option>
								<option value="2" <?=($st_setting_dokter=='2'?'selected':'')?>>Diatur Spesifik</option>
								
							</select>
						</div>
						
					</div>
				</div>
				<div class="col-md-12" id="div_dokter">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_user_dokter">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Profesi</th>
										<th width="15%">Spesialisasi</th>
										<th width="15%">PPA</th>
										
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="profesi_id_dokter" name="profesi_id_dokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" selected>-Pilih Profesi-</option>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="spesialisasi_id_dokter" name="spesialisasi_id_dokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-SEMUA-</option>
												<?foreach(list_variable_ref(22) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										<th>
											<select id="mppa_id_dokter" name="mppa_id_dokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-SEMUA-</option>
												
											</select>
										</th>
										
										<th>
											<button class="btn btn-primary btn-sm" type="button" onclick="tambah_dokter()"><i class="fa fa-plus"></i> Tambah</button>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
						</div>					
					</div>
				</div>
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_success('SETTING PEMBERI INFORMASI')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						
						<div class="col-md-12">
							<label for="st_setting_pemberi_info">Pengaturan Pemberi Informasi</label>
							<select id="st_setting_pemberi_info" name="st_setting_pemberi_info" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Choose one..">
												
								<option value="#" <?=($st_setting_pemberi_info=='#'?'selected':'')?>>-Pilih Setting Dokter-</option>
								<option value="1" <?=($st_setting_pemberi_info=='1'?'selected':'')?>>User Login</option>
								<option value="2" <?=($st_setting_pemberi_info=='2'?'selected':'')?>>Diatur Spesifik</option>
								
							</select>
						</div>
						
					</div>
				</div>
				<div class="col-md-12"  id="div_pemberi">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_user_pemberi">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Profesi</th>
										<th width="15%">Spesialisasi</th>
										<th width="15%">PPA</th>
										
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="profesi_id_pemberi" name="profesi_id_pemberi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" selected>-Pilih Profesi-</option>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="spesialisasi_id_pemberi" name="spesialisasi_id_pemberi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-SEMUA-</option>
												<?foreach(list_variable_ref(22) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										<th>
											<select id="mppa_id_pemberi" name="mppa_id_pemberi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-SEMUA-</option>
												
											</select>
										</th>
										
										<th>
											<button class="btn btn-primary btn-sm" type="button" onclick="tambah_pemberi()"><i class="fa fa-plus"></i> Tambah</button>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
						</div>					
					</div>
				</div>
				
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_danger('SETTING PETUGAS YANG MENDAMPINGI')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						
						
						<div class="col-md-12">
							<label for="st_setting_petugas_pendamping">Pengaturan Petugas yang Mendampingi</label>
							<select id="st_setting_petugas_pendamping" name="st_setting_petugas_pendamping" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Choose one..">
												
								<option value="#" <?=($st_setting_petugas_pendamping=='#'?'selected':'')?>>-Pilih Setting Dokter-</option>
								<option value="1" <?=($st_setting_petugas_pendamping=='1'?'selected':'')?>>User Login</option>
								<option value="2" <?=($st_setting_petugas_pendamping=='2'?'selected':'')?>>Diatur Spesifik</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12" id="div_pendamping">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_user_pendamping">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Profesi</th>
										<th width="15%">Spesialisasi</th>
										<th width="15%">PPA</th>
										
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="profesi_id_pendamping" name="profesi_id_pendamping" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" selected>-Pilih Profesi-</option>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="spesialisasi_id_pendamping" name="spesialisasi_id_pendamping" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-SEMUA-</option>
												<?foreach(list_variable_ref(22) as $r){?>
													<option value="<?=$r->id?>"><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										<th>
											<select id="mppa_id_pendamping" name="mppa_id_pendamping" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-SEMUA-</option>
												
											</select>
										</th>
										
										<th>
											<button class="btn btn-primary btn-sm" type="button" onclick="tambah_pendamping()"><i class="fa fa-plus"></i> Tambah</button>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
						</div>					
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
			
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
		$(".number").number(true,0,'.',',');
	var tab=$("#tab").val();
	if (tab=='1'){
		load_general();
	}
	if (tab=='2'){
		load_tab2();
	}
	if (tab=='4'){
		load_tab4();
	}
	$('.js-summernote').summernote({
		  height: 80,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})

})	
function load_tab4(){
	load_dokter();
	load_pemberi();
	load_pendamping();
	set_user();
}
function tambah_dokter(){
	let profesi_id=$("#profesi_id_dokter").val();
	let spesialisasi_id=$("#spesialisasi_id_dokter").val();
	let mppa_id=$("#mppa_id_dokter").val();
	let jenis_akses=1;
	
	if (profesi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Profesi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_risiko_tinggi/simpan_dokter', 
		dataType: "JSON",
		method: "POST",
		data : {
				jenis_akses:jenis_akses,
				profesi_id:profesi_id,
				spesialisasi_id:spesialisasi_id,
				mppa_id:mppa_id,
				
			
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				$("#profesi_id_dokter").val('#').trigger('change');
				$("#spesialisasi_id_dokter").val('#').trigger('change');
				$("#mppa_id_dokter").val('#').trigger('change');
				
				$('#index_user_dokter').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});
}
function tambah_pemberi(){
	let profesi_id=$("#profesi_id_pemberi").val();
	let spesialisasi_id=$("#spesialisasi_id_pemberi").val();
	let mppa_id=$("#mppa_id_pemberi").val();
	let jenis_akses=2;
	
	if (profesi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Profesi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_risiko_tinggi/simpan_dokter', 
		dataType: "JSON",
		method: "POST",
		data : {
				jenis_akses:jenis_akses,
				profesi_id:profesi_id,
				spesialisasi_id:spesialisasi_id,
				mppa_id:mppa_id,
				
			
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				$("#profesi_id_pemberi").val('#').trigger('change');
				$("#spesialisasi_id_pemberi").val('#').trigger('change');
				$("#mppa_id_pemberi").val('#').trigger('change');
				
				$('#index_user_pemberi').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});
}
function set_user(){
	if ($("#st_setting_dokter").val()=='1'){
		$("#div_dokter").hide();
	}else{
		$("#div_dokter").show();
		
	}
	if ($("#st_setting_pemberi_info").val()=='1'){
		$("#div_pemberi").hide();
	}else{
		$("#div_pemberi").show();
		
	}
	if ($("#st_setting_petugas_pendamping").val()=='1'){
		$("#div_pendamping").hide();
	}else{
		$("#div_pendamping").show();
		
	}
}
function tambah_pendamping(){
	let profesi_id=$("#profesi_id_pendamping").val();
	let spesialisasi_id=$("#spesialisasi_id_pendamping").val();
	let mppa_id=$("#mppa_id_pendamping").val();
	let jenis_akses=3;
	
	if (profesi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Profesi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_risiko_tinggi/simpan_dokter', 
		dataType: "JSON",
		method: "POST",
		data : {
				jenis_akses:jenis_akses,
				profesi_id:profesi_id,
				spesialisasi_id:spesialisasi_id,
				mppa_id:mppa_id,
				
			
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				$("#profesi_id_pendamping").val('#').trigger('change');
				$("#spesialisasi_id_pendamping").val('#').trigger('change');
				$("#mppa_id_pendamping").val('#').trigger('change');
				
				$('#index_user_pendamping').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});
}
function hapus_user(id,jenis_akses){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus User?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_risiko_tinggi/hapus_user',
				type: 'POST',
				data: {id: id},
				complete: function() {
					if (jenis_akses=='1'){
						$('#index_user_dokter').DataTable().ajax.reload( null, false ); 
					}
					if (jenis_akses=='2'){
						$('#index_user_pemberi').DataTable().ajax.reload( null, false ); 
					}
					if (jenis_akses=='3'){
						$('#index_user_pendamping').DataTable().ajax.reload( null, false ); 
					}
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function load_dokter(){
	let jenis_akses=1;
	$('#index_user_dokter').DataTable().destroy();	
	table = $('#index_user_dokter').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			
            ajax: { 
                url: '{site_url}setting_risiko_tinggi/load_user', 
                type: "POST" ,
                dataType: 'json',
				data : {
						jenis_akses:jenis_akses,
					   }
            }
        });
}
function load_pemberi(){
	let jenis_akses=2;
	$('#index_user_pemberi').DataTable().destroy();	
	table = $('#index_user_pemberi').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			
            ajax: { 
                url: '{site_url}setting_risiko_tinggi/load_user', 
                type: "POST" ,
                dataType: 'json',
				data : {
						jenis_akses:jenis_akses,
					   }
            }
        });
}
function load_pendamping(){
	let jenis_akses=3;
	$('#index_user_pendamping').DataTable().destroy();	
	table = $('#index_user_pendamping').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			
            ajax: { 
                url: '{site_url}setting_risiko_tinggi/load_user', 
                type: "POST" ,
                dataType: 'json',
				data : {
						jenis_akses:jenis_akses,
					   }
            }
        });
}
$(".auto_blur").change(function(){
	set_user();
	simpan_user_head();
	
});
function simpan_user_head(){
	let st_setting_dokter=$("#st_setting_dokter").val();
	let st_setting_pemberi_info=$("#st_setting_pemberi_info").val();
	let st_setting_petugas_pendamping=$("#st_setting_petugas_pendamping").val();
	$.ajax({
		url: '{site_url}setting_risiko_tinggi/simpan_user_head', 
		dataType: "JSON",
		method: "POST",
		data : {
				st_setting_dokter:st_setting_dokter,
				st_setting_pemberi_info:st_setting_pemberi_info,
				st_setting_petugas_pendamping:st_setting_petugas_pendamping,
			},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==true){
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});
}
$("#profesi_id_dokter").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_risiko_tinggi/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id_dokter").empty();
				$("#mppa_id_dokter").append(data);
			}
		});
	}else{
		
				$("#mppa_id_dokter").empty();
	}

});
$("#profesi_id_pemberi").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_risiko_tinggi/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id_pemberi").empty();
				$("#mppa_id_pemberi").append(data);
			}
		});
	}else{
		
				$("#mppa_id_pemberi").empty();
	}

});
$("#profesi_id_pendamping").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_risiko_tinggi/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id_pendamping").empty();
				$("#mppa_id_pendamping").append(data);
			}
		});
	}else{
		
				$("#mppa_id_pendamping").empty();
	}

});
function load_general(){
	$('.js-summernote').summernote({
	  height: 50,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	});	

}
function load_tab2(){
	load_hak_akses();		
	load_formulir();	
}

// LOGIC
function load_hak_akses(){
	$('#index_hak_akses').DataTable().destroy();	
	table = $('#index_hak_akses').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_risiko_tinggi/load_hak_akses', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
$("#btn_tambah_hak_akses").click(function() {
	let profesi_id=$("#profesi_id").val();
	let spesialisasi_id=$("#spesialisasi_id").val();
	let mppa_id=$("#mppa_id").val();
	let st_lihat=$("#st_lihat").val();
	let st_input=$("#st_input").val();
	let st_edit=$("#st_edit").val();
	let st_hapus=$("#st_hapus").val();
	let st_cetak=$("#st_cetak").val();
	
	if (profesi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Profesi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_risiko_tinggi/simpan_hak_akses', 
		dataType: "JSON",
		method: "POST",
		data : {
				profesi_id:profesi_id,
				spesialisasi_id:spesialisasi_id,
				mppa_id:mppa_id,
				st_lihat:st_lihat,
				st_input:st_input,
				st_edit:st_edit,
				st_hapus:st_hapus,
				st_cetak:st_cetak,
			
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				$("#profesi_id").val('#').trigger('change');
				$("#spesialisasi_id").val('#').trigger('change');
				$("#mppa_id").val('#').trigger('change');
				$("#st_cetak").val('0').trigger('change');
				$("#st_hapus").val('0').trigger('change');
				$("#st_edit").val('0').trigger('change');
				$("#st_input").val('0').trigger('change');
				$("#st_lihat").val('0').trigger('change');
				$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});

});
function hapus_hak_akses(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_risiko_tinggi/hapus_hak_akses',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function hapus_default(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Default?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_risiko_tinggi/hapus_default',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_tampil_default').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

$("#profesi_id").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_risiko_tinggi/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id").empty();
				$("#mppa_id").append(data);
			}
		});
	}else{
		
				$("#mppa_id").empty();
	}

});
$("#operand_tahun").change(function(){
	if ($(this).val()=='0'){//Tidak Ditentukan
		$("#operand_bulan").val($(this).val()).trigger('change');
		$("#operand_hari").val($(this).val()).trigger('change');
		$("#umur_bulan").val($(this).val()).trigger('change');
		$("#umur_hari").val($(this).val()).trigger('change');
		$(".bulan").attr('disabled','disabled');
		$(".hari").attr('disabled','disabled');
	}else{
		$(".bulan").removeAttr('disabled');
		// $(".hari").removeAttr('disabled');
	}

});
$("#operand_bulan").change(function(){
	if ($(this).val()=='0'){//Tidak Ditentukan
		$("#operand_hari").val($(this).val()).trigger('change');
		$(".hari").attr('disabled','disabled');
		$("#umur_hari").val($(this).val()).trigger('change');
	}else{
		$(".hari").removeAttr('disabled');
	}

});
$("#st_spesifik_risiko_tinggi").change(function(){
	if ($(this).val()=='1'){
		$(".setting_tidak").show();
	}else{
		$(".setting_tidak").hide();
	}

});
$("#btn_tambah_formulir").click(function() {
	let idtipe=$("#idtipe").val();
	let idpoli=$("#idpoli").val();
	let statuspasienbaru=$("#statuspasienbaru").val();
	let pertemuan_id=$("#pertemuan_id").val();
	let st_tujuan_terakhir=$("#st_tujuan_terakhir").val();
	let operand=$("#operand").val();
	// alert(operand);
	let lama_terakhir_tujan=$("#lama_terakhir_tujan").val();
	let st_formulir=$("#st_formulir").val();
	
	if (idtipe=='#'){
		sweetAlert("Maaf...", "Tentukan Tujuan", "error");
		return false;
	}
	
	if (st_formulir=='0'){
		sweetAlert("Maaf...", "Tentukan Status Formulir", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_risiko_tinggi/simpan_formulir', 
		dataType: "JSON",
		method: "POST",
		data : {
				idtipe:idtipe,
				idpoli:idpoli,
				statuspasienbaru:statuspasienbaru,
				pertemuan_id:pertemuan_id,
				st_tujuan_terakhir:st_tujuan_terakhir,
				operand:operand,
				lama_terakhir_tujan:lama_terakhir_tujan,
				st_formulir:st_formulir,

			
			},
		success: function(data) {
			// alert(data);
				$("#cover-spin").hide();
			if (data==true){
				$("#idtipe").val('#').trigger('change');
				$("#idpoli").val('#').trigger('change');
				$("#statuspasienbaru").val('#').trigger('change');
				$("#pertemuan_id").val('#').trigger('change');
				$("#operand").val('0').trigger('change');
				$("#st_tujuan_terakhir").val('0').trigger('change');
				$("#lama_terakhir_tujan").val('0');
				$("#st_formulir").val('0').trigger('change');
				$('#index_tampil').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});

			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
		}
	});

});
$("#idtipe").change(function(){
		$.ajax({
			url: '{site_url}setting_risiko_tinggi/find_poli/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#idpoli").empty();
				$("#idpoli").append(data);
			}
		});

});
$("#idtipe_default").change(function(){
		$.ajax({
			url: '{site_url}setting_risiko_tinggi/find_poli/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#idpoli_default").empty();
				$("#idpoli_default").append(data);
			}
		});

});
function load_formulir(){
	$('#index_tampil').DataTable().destroy();	
	table = $('#index_tampil').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			"columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [3,2,4,5,6] },
					// { "width": "15%", "targets": [1]},
					// { "width": "8%", "targets": [8]},
					// { "width": "30%", "targets": 3,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}setting_risiko_tinggi/load_formulir', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function hapus_formulir(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_risiko_tinggi/hapus_formulir',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_tampil').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
</script>