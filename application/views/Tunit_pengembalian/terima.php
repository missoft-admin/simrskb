<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block block-rounded block-bordered">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <form action="{site_url}tunit_pengembalian/save_terima" method="post" onsubmit="return validate_final()" class="form-horizontal">
        <div class="block-content">
            <input type="hidden" name="id" id="id" value="<?=$id?>"/>
            <div class="form-group">
                <label class="col-md-3 control-label">No Transaksi</label>
                <div class="col-md-2">
                    <input readonly type="text" class="form-control" id="nopengembalian" placeholder="nopengembalian" value="{nopengembalian}">
                </div>
				<label class="col-md-1 control-label">Tanggal</label>
                <div class="col-md-3">
                    <input readonly type="text" class="form-control" id="tanggal" placeholder="tanggal" value="<?=HumanDateLong($tanggal)?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Dari Unit</label>
                <div class="col-md-6">
                    <input readonly type="text" class="form-control" id="unit_dari" placeholder="Unit Dari" value="{unit_dari}">
                    <input  type="hidden" class="form-control" name="id_unit_dari" id="id_unit_dari" placeholder="Unit Dari" value="{id_unit_dari}">
                </div>
            </div>
            <div class="form-group c" id="f_idpermintaan">
                <label class="col-md-3 control-label">U. Pengembalian</label>
                <div class="col-md-6">
                    <input readonly type="text" class="form-control" id="unit_ke" placeholder="Unit Dari" value="{unit_ke}">
                    <input  type="hidden" class="form-control" name="id_unit_ke" id="id_unit_ke" placeholder="Unit Dari" value="{id_unit_ke}">
                </div>
            </div>
			
            <div class="form-group">
                <label class="col-md-3 control-label" for="alasan">Alasan</label>
                <div class="col-md-6">
					<textarea class="form-control" name="alasan"  id="alasan" rows="3" placeholder="Alasan"><?=$alasan?></textarea>
				</div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">User Pengembalian</label>
                <div class="col-md-2">
                    <input readonly type="text" class="form-control" id="nama_user" placeholder="nama_user" value="{nama_user}">
                </div>
				
            </div>

            <table class="table table-striped table-bordered" id="datapengembalian" style="margin-bottom:0px">
                <thead>
                    <tr>
                        <th>Tipe</th>
                        <th>Kode</th>
                        <th>Barang</th>
                        <th>No Batch</th>
                        <th>Stok Unit</th>
                        <th>Jumlah</th>
                        <th hidden>idtipe</th><!-- 6-->
                        <th hidden>idbarang</th><!-- 7-->
                        <th hidden>id_det</th><!-- 8-->
                        <th hidden>id_pengembalian</th><!-- 9-->
                        <th hidden>st_edit</th><!-- 10-->
                        <th hidden>st_hapus</th><!-- 11-->
                    </tr>
                </thead>
                <tbody>
					<?foreach ($List_barang_edit as $row){?>
					<tr>
						<td><?=$row->namatipe?></td>
						<td><?=$row->kode?></td>
						<td><?=$row->namabarang?></td>
						<td><?=$row->nobatch?></td>
						<td><?=$row->stok?></td>
						<td><?=$row->kuantitas?></td>
						
						<td hidden><input type="text" name="e_idtipe[]" value="<?=$row->idtipe?>"></td>
						<td hidden><input type="text" name="e_idbarang[]" value="<?=$row->idbarang?>"></td>
						<td hidden><input type="text" name="e_id_det[]" value="<?=$row->id?>"></td>
						<td hidden><input type="text" name="e_idpengembalian[]" value="<?=$row->idpengembalian?>"></td>
						<td hidden><input type="text" name="e_st_edit[]" value="0"></td>
						<td hidden><input type="text" name="e_st_hapus[]" value="0"></td><!-- 11-->
						<td hidden><input type="text" name="e_kuantitas[]" value="<?=$row->kuantitas?>"></td><!-- 12-->
						<td hidden><input type="text" name="e_namabarang[]" value="<?=$row->namabarang?>"></td><!-- 13-->
					</tr>
					<?}?>
				</tbody>
            </table>
            <div class="text-right">
                <br/>
                <a class="btn btn-sm btn-default" href="{site_url}tunit_pengembalian">Kembali</a>
                <button class="btn btn-sm btn-success" type="submit">Simpan</button>
                <br/>
                <br/>
            </div>
        </div>
    </form>
</div>

<form action="javascript:(0)" method="post" class="form-horizontal" id="form3">
    <div class="modal in" id="modal_edit" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">Edit Barang</h3>
                    </div>
                    <div class="block-content">
                        <input type="hidden" id="rowIndex" value="">
                        <input type="hidden" id="xtipebarang" name="xtipebarang">
                        <input type="hidden" id="xidbarang" name="xidbarang">
                        <input type="hidden" id="xid_det" name="xid_det">
                        <input type="hidden" id="xidpengembalian" name="xidpengembalian">


                        <div class="form-group">
                            <label class="control-label col-md-3">Tipe Barang</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="nama_tipebarang" name="nama_tipebarang" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Barang</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="xnamabarang"  name="xnamabarang" readonly>
                            </div>
                        </div>
						<div class="form-group">
							 <label class="control-label col-md-3">Nama Barang Baru</label>
							<div class="col-md-6">
								<div class="input-group">
									<select name="idbarang" id="idbarang" data-placeholder="Cari Barang" class="form-control" style="width:100%"></select>
									<div class="input-group-btn">
										<button class='btn btn-info modal_list_barang' data-toggle="modal" data-target="#modal_list_barang" type='button' id='btn_cari'>
											<i class="fa fa-search"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
                       <div class="form-group">
                            <label class="control-label col-md-3">Stok</label>
                            <div class="col-md-6">
                                <input type="text" readonly class="form-control angka" id="stok" name="stok" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Kuantitas</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control angka" id="x_kuantitas" name="x_kuantitas" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-sm btn-success" id="btn_update" type="submit">Update</button>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="modal in" id="modal_list_obat" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
            <div class="block-header bg-success">
                <h3 class="block-title">Pencarian Barang</h3>
            </div>
            <div class="block-content">
                <table class="table table-bordered" id="datatable-1">
                    <thead>
                        <th>ID</th>
                        <th>TIPE</th>
                        <th>KODE</th>
                        <th>NAMA</th>
                        <th>STOK</th>
                        <th>PILIH</th>
                        <th hidden>id</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
$(document).ready(function(){
	$("#btn_update").click(function() {
		$("#modal_edit").modal("hide");
		$('#datapengembalian tbody tr').each(function() {
			var tr = $(this).closest('tr');
			// tr[0].sectionRowIndex
			// alert(tr[0].sectionRowIndex);
			
			var $cells = $(this).children('td');
			if (tr[0].sectionRowIndex==$("#rowIndex").val()){
				tr.find("td:eq(7) input").val($("#idbarang").val());
				tr.find("td:eq(4)").text($("#x_kuantitas").val());
				tr.find("td:eq(12) input").val($("#x_kuantitas").val());
				tr.find("td:eq(2)").text($("#idbarang option:selected").text());
				tr.find("td:eq(10) input").val('1');
				tr.find("td:eq(13) input").val($("#idbarang option:selected").text());
			}
		});
	   // alert('update');
	});
	
    $("#idbarang").change(function(){
		var ke_unit=$("#id_unit_dari").val();
		var tipe_id=$("#xtipebarang").val();
		var idbarang=$(this).val();
		// alert(idbarang);
		$.ajax({
			url: '{site_url}tunit_permintaan/selected_barang/',
			dataType: "JSON",
			method: "POST",
			data : {idtipe: tipe_id, idunitpelayanan: ke_unit, idbarang: idbarang},
			success: function(data) {
				$("#stok").val(data);
			}
		});
		   

	});
	var table, tr;
	$(".edit").click(function() {
		var tr = $(this).closest('tr');
		$("#modal_edit").modal("show");
		$("#rowIndex").val(tr[0].sectionRowIndex);
		$("#xtipebarang").val(tr.find("td:eq(6) input").val());
		$("#xidbarang").val(tr.find("td:eq(7) input").val());
		$("#xid_det").val(tr.find("td:eq(8) input").val());
		$("#xidpengembalian").val(tr.find("td:eq(9) input").val());
		$("#xnamabarang").val(tr.find("td:eq(2)").text());
		$("#nama_tipebarang").val(tr.find("td:eq(0)").text());
		$("#x_kuantitas").val(tr.find("td:eq(4)").text());
		
		var n = tr.find("td:eq(6) input").val();
		var idunitpelayanan = $("#id_unit_dari").val();
		// alert(idunitpelayanan);
		$("#idbarang").select2({
			minimumInputLength: 0,
			noResults: 'Barang tidak ditemukan.',          
			allowClear: true,
			ajax: {
				url: '{site_url}tunit_permintaan/selectbarang_edit/', 
				dataType: 'json', 
				type: 'post', 
				quietMillis: 50, 
				data: function (params) {
					var query = { search: params.term, idtipe: n, idunitpelayanan: idunitpelayanan } 
					return query; 
				}, 
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							// alert(data);
							console.log(data);
							return { id: item.id, text: item.nama }
						})
					};
				}
			}
		}); 
		var data = { 
			id: $("#xidbarang").val(), 
			text: $("#xnamabarang").val()
		};
		var newOption = new Option(data.text, data.id, true, true);
		$('#idbarang').append(newOption).trigger('change');        
		// alert($('#idbarang').val());        
		// tr.find("td:eq(10) input").val('1');
	});
	$("#btn_cari").click(function() {
		// $("#modal_edit").modal("hide");
		$("#modal_list_obat").modal("show");
		var ke_unit=$("#id_unit_dari").val();
		var tipe_id=$("#xtipebarang").val();
		// var idbarang=$("#idbarang").val();
		table = $('#datatable-1').DataTable({
			destroy: true,
			serverSide: true,
			searching: true,
			sort: false,
			autoWidth: false,
			ajax: {
				url: '{site_url}tunit_permintaan/view_barang/',
				type: 'post',
				data: {idtipe: tipe_id, idunitpelayanan: ke_unit}
			},
			columns: [
				{data: 'id', searchable: false},
				{data: 'namatipe'},
				{data: 'kode'},
				{data: 'nama'},
				{data: 'stok'},
				{defaultContent: '<a href="#" class="btn btn-xs btn-success pilih">Pilih</a>', searchable: false },
				{data: 'id', visible: false},
			]
		})
		setTimeout(function() {
			$('div.dataTables_filter input').focus()
		}, 500);
	// }

	});
});

$('.angka').keydown(function(event){
		keys = event.keyCode;
		// Untuk: backspace, delete, tab, escape, and enter
		if (event.keyCode == 116 || event.keyCode == 46 || event.keyCode == 188 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
			 // Untuk: Ctrl+A
			(event.keyCode == 65 && event.ctrlKey === true) ||
			 // Untuk: home, end, left, right
			(event.keyCode >= 35 && event.keyCode <= 39) || event.keyCode == 190 || event.keyCode == 110|| event.keyCode == 188) {
				 // melanjutkan untuk memunculkan angka
				return;
		}
		else {
			// jika bukan angka maka tidak terjadi apa-apa
			if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault();
			}
		}

	});
$('#datatable-1 tbody').on('click', 'a', function(){
	// tr = table.row( $(this).parents('tr') ).index()
	var tr = $(this).closest('tr');
	// alert(tr.find("td:eq(3)").text());
	var data = { 
		id: tr.find("td:eq(0)").text(), 
		text: tr.find("td:eq(3)").text()
	};
	var newOption = new Option(data.text, data.id, true, true);
	$('#idbarang').append(newOption).trigger('change');        
	$('#modal_list_obat').modal('hide');
})
$(".hapus").click(function() {
	var tr = $(this).closest('tr');
	tr.find("td:eq(5)").html('<label class="label label-danger">Dihapus</label>')
	tr.find("td:eq(10) input").val('1')
	tr.find("td:eq(11) input").val('1')
   
});
function click_batal(){
    $('#iddetail').val("");
    $('#kuantitas').val("");
    $('#idbarang').val("");
    $('#namabarang').val("");
    $('#idtipe > option').remove();
}
function validate_final() {
	$("#cover-spin").show();
	return true;
}
</script>
