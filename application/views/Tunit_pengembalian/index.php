<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('1109'))){ ?>
<div class="block">
    <div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('1110'))){ ?>
        <ul class="block-options">
            <li><a href="{site_url}tunit_pengembalian/create" class="btn btn-default"><i class="fa fa-plus"></i> Tambah</a></li>
        </ul>
		<?}?>
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:25px">
        <div class="row">
            <?php echo form_open('tunit_pengembalian/filter', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Dari </label>
                    <div class="col-md-8">
                        <select id="iddariunit" name="iddariunit" class="form-control" style="width: 100%;" data-placeholder="Choose one..">
                        <option value="#" <?($iddariunit=='#')?'Selected':''?>>- Belum Dipilih -</option>
						<?php foreach($list_dari as $row){?>
                            <option value="<?=$row->id;?>" <?=($iddariunit==$row->id)?'Selected':''?>><?=$row->text;?></option>
                        <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Status</label>
                    <div class="col-md-8">
                        <select id="status" name="statuspenerimaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#">-Semua Status-</option>
                            <option value="1" <?=(1 == $statuspenerimaan ? 'selected' : ''); ?>>Proses Pengembalian</option>
                            <option value="2" <?=(2 == $statuspenerimaan ? 'selected' : ''); ?>>Selesai</option>
                            <option value="00" <?=('00' == $statuspenerimaan ? 'selected' : ''); ?>>Dibatalkan</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
                            <input class="form-control" type="text" name="tanggaldari" placeholder="From" value="{tanggaldari}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" name="tanggalsampai" placeholder="To" value="{tanggalsampai}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    
    <!-- start content -->
    <div class="block-content">
        <table class="table table-bordered table-striped" id="index_list">
            <thead>
                <tr>
                    <th>ID</th>
                    <th width="10%">Tanggal</th>
                    <th width="10%">No Pengembalian</th>
                    <th width="15%">Dari Unit</th>
                    <th width="15%">Ke Unit</th>
                    <th width="10%">T. Barang</th>
                    <th width="15%">Alasan</th>
                    <th width="10%">Status</th>
                    <th width="20%" align="center">Aksi</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <!-- end content -->

</div>
<?}?>
<div style="overflow-y: scroll;" class="modal in" id="modalDetailPengembalian" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form class="form-horizontal" id="form1">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">Tambah Detail Pemesanan Gudang</h3>
                    </div>
                    <div class="block-content">
                        <input type="hidden" name="detailValue_1">
                        <input type="hidden" id="idbarang">
                        <input type="hidden" id="tanggal" value="<?=date("Y-m-d")?>"/>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Dari Unit</label>
                            <div class="col-md-6">
                                <select id="idunit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.."></select>
                            </div>
                        </div>

                        <div class="form-group c" id="f_idtipe">
                            <label class="col-md-3 control-label">Tipe</label>
                            <div class="col-md-6">
                                <select id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.."></select>
                            </div>
                        </div>
                        <div class="form-group c" id="f_namabarang">
                            <label class="col-md-3 control-label" for="namabarang">Nama Barang</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input readonly type="text" class="form-control" id="namabarang" placeholder="Nama Barang" value="" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Stok</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input readonly type="text" class="form-control" id="stokbarang" placeholder="Stok Barang" value="" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="form-group c" id="f_idpermintaan">
                            <label class="col-md-3 control-label">U. Pengembalian</label>
                            <div class="col-md-6">
                                <select readonly id="idpermintaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.."></select>
                            </div>
                        </div>
                        <div class="form-group c" id="f_kuantitas">
                            <label class="col-md-3 control-label" for="kuantitas">Kuantitas</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                    <input type="text" class="form-control" id="kuantitas" placeholder="Kuantitas" value="" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label" for="alasan">Alasan</label>
                            <div class="col-xs-6">
                                <textarea class="form-control" id="alasan" rows="3" placeholder="Alasan"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-xs-3 control-label" for="alasan">&nbsp;</label>
                            <div class="col-xs-6">
                                <input type="button" class="btn btn-success" id="simpan-1" value="Simpan">
                            </div>
                        </div>

                        <table class="table table-striped table-bordered" id="datapengembalian" style="margin-bottom:0px">
                            <thead>
                                <tr>
                                    <th>Tipe</th>
                                    <th>Barang</th>
                                    <th>U. Penerima</th>
                                    <th>Kuantitas</th>
                                    <th>Actions</th>
                                    <th hidden>idtipe</th>
                                    <th hidden>idbarang</th>
                                    <th hidden>idpeminta</th>
                                    <th hidden>idunit</th>
                                    <th hidden>alasan</th>
                                    <th hidden>tanggal</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-sm btn-success" id="simpan-2" type="submit">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal in" id="modal_list_barang" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">List Barang</h3>
                </div>
                <div class="block-content">
                    <table class="table table-striped table-bordered" id="list-barang">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Kode</th>
                                <th>Nama</th>
                                <th>Stok</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" id="btnback" type="button" data-dismiss="modal">Back</button>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
var table;

$(document).ready(function(){
    $('#iddariunit').select2();
		table = $('#index_list').DataTable({
            autoWidth: false,
            pageLength: 50,
            serverSide: true,
            "order": [],
            "pageLength": 10,
            ajax: { 
                url: '{site_url}tunit_pengembalian/ajax_list', 
                type: "POST" ,
                dataType: 'json'
            }
        });
    
    
});

$('#datapengembalian').on( 'click', '.detail_remove', function(){
    var tr = table.row( $(this).parents('tr') ).index()
    table.row(tr).remove().draw()
    detailValue_1()
})

function detailValue_1() {
    var arr = $('#datapengembalian').DataTable().data().toArray()
    arr = JSON.stringify(arr)
    $('input[name=detailValue_1]').val( arr )
}
</script>
