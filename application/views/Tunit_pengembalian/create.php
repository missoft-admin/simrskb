<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block block-rounded block-bordered">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <?php echo form_open('tunit_pengembalian/save','class="form-horizontal" id="form-work"') ?>
    <div class="block-content">
        <input type="hidden" name="detailValue_1">
        <input type="hidden" id="idbarang">
        <input type="hidden" id="tanggal" value="<?=date("Y-m-d H:i:s")?>"/>
        <input type="hidden" id="hargabarang">
        <input type="hidden" id="marginbarang">
        <div class="form-group">
            <label class="col-md-3 control-label">Dari Unit</label>
            <div class="col-md-6">
                <select id="idunit" class="form-control" style="width: 100%;" data-placeholder="Choose one.."></select>
            </div>
        </div>
        <div class="form-group c" id="f_idpermintaan">
            <label class="col-md-3 control-label">Kembalikan Ke Unit</label>
            <div class="col-md-6">
                <select id="idpermintaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.."></select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="alasan">Alasan</label>
            <div class="col-md-6">
                <textarea class="form-control" id="alasan" rows="3" placeholder="Alasan"></textarea>
            </div>
        </div>
		<div class="form-group">
                <label class="col-md-3 control-label">User Pengembalian</label>
                <div class="col-md-2">
                    <input readonly type="text" class="form-control" id="nama_user" placeholder="nama_user" value="<?=$this->session->userdata("user_name")?>">
                </div>
				
            </div>
        <hr/>

        <div class="form-group c" id="f_idtipe">
            <label class="col-md-3 control-label">Tipe</label>
            <div class="col-md-6">
                <select id="idtipe" class="form-control" style="width: 100%;" data-placeholder="Choose one.."></select>
            </div>
        </div>
        <div class="form-group c" id="f_namabarang">
            <label class="col-md-3 control-label" for="namabarang">Nama Barang</label>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input readonly type="text" class="form-control" id="namabarang" placeholder="Nama Barang" value="" required="" aria-required="true">
                    <div class="input-group-btn">
                     <button class='btn btn-default' type='button' id='caribarang'><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Stok</label>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input readonly type="text" class="form-control" id="stokbarang" placeholder="Stok Barang" value="" required="" aria-required="true">
                </div>
            </div>
        </div>
        <div class="form-group c" id="f_kuantitas">
            <label class="col-md-3 control-label" for="kuantitas">Kuantitas</label>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                    <input type="text" class="form-control number" id="kuantitas" placeholder="Kuantitas" value="" >
                </div>
            </div>
        </div>
		<div class="form-group c" id="f_kuantitas">
            <label class="col-md-3 control-label" for="nobatch">No Batch</label>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                    <input type="text" class="form-control" id="nobatch" placeholder="nobatch" value="" >
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-xs-3 control-label" for="alasan">&nbsp;</label>
            <div class="col-xs-6">
                <input type="button" class="btn btn-success" id="simpan-1" value="Tambahkan Barang">
            </div>
        </div>

        <table class="table table-striped table-bordered" id="datapengembalian" style="margin-bottom:0px">
            <thead>
                <tr>
                    <th>Tipe</th>
                    <th>Barang</th>
                    <th>Ke Unit</th>
                    <th>Kuantitas</th>
                    <th>No Batch</th>
                    <th>Actions</th>
                    <th hidden>idtipe</th>
                    <th hidden>idbarang</th>
                    <th hidden>idpermintaan</th>
                    <th hidden>idunit</th>
                    <th hidden>alasan</th>
                    <th hidden>tanggal</th>
                    <th hidden>hargabarang</th>
                    <th hidden>marginbarang</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
        <div class="text-right">
            <br/>
            <a class="btn btn-sm btn-default" href="{site_url}tunit_pengembalian">Kembali</a>
            <button class="btn btn-sm btn-success" id="simpan-2" type="submit">Simpan</button>
            <br/>
            <br/>
        </div>
    </div>
</div>
<?php echo form_close(); ?>

<div class="modal in" id="modal_list_barang" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">List Barang</h3>
                </div>
                <div class="block-content">
                    <table class="table table-striped table-bordered" id="list-barang">
                        <thead>
                            <tr>
                                <th>ID</th>
								<th>TIPE</th>
								<th>KODE</th>
								<th>NAMA</th>
								<th>STOK</th>
								<th>PILIH</th>
								<th hidden>id</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>                    
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" id="btnback" type="button" data-dismiss="modal">Back</button>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
var table;
$(document).ready(function(){
    $('.number').number(true, 0);
    $('#idunit').select2({
        placeholder: 'Cari Data ... ',
        ajax: {
            url:'{site_url}tunit_pengembalian/get_unit',
            type: 'post',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {results:data}
            },
            cache: false,
        }
    });

    $('#idtipe').select2({
        placeholder: 'Cari Data ... ',
        ajax: {
            url: '{site_url}tunit_pengembalian/get_tipe',
            type: 'post',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {results:data}
            },
            cache: false,
        }
    });

    $('#idunit').change(change_idunit);
    $('#idtipe').change(change_idtipe);
    $('#simpan-1').click(click_simpan);
    // $('.detail_remove').click(click_detail_remove);
    // $('#form1').submit(submit_form);
    $('#list-barang').on('click', 'td', barang_click); 
    $('#caribarang').click(cari_barang);
	
	$('#simpan-2').on('click', function(){
		return validate_simpan();
	});
		
});
    // $('#simpan-2').click(change_idunit);
function cari_barang(){
	if ($("#idtipe").val()=='' || $("#idtipe").val()==null){
		alert('Silahkan Isi Tipe Barang Terlebih Dahulu');
	}else{
		change_idtipe();
	}
}
function change_idunit(){
    var val = $(this).val();
	$('#idtipe').val(null).trigger('change');
    $('#idtipe').select2({
        placeholder: 'Cari Data ... ',
        ajax: {
            url: '{site_url}tunit_pengembalian/get_tipe/',
            type: 'post',
            dataType: 'json',
            delay: 250,
            data: {
                idunit: val
            },
            processResults: function (data) {
                return {results:data}
            },
            cache: false,
        }
    }); 


    var gudangtipe;
    
    if($('#idunit').val() == 4) 
        gudangtipe = 4;
    else
        gudangtipe = 1;

    $('#idpermintaan').select2({
        placeholder: 'Cari Data ... ',
        ajax: {
            url: '{site_url}tunit_pengembalian/get_gudang',
            type: 'post',
            dataType: 'json',
            delay: 250,
            data: {
                idunit: $("#idunit").val(),
            },
            processResults: function (data) {
                return {results:data}
            },
            cache: false,
        }
    });
    $('#namabarang,#kuantitas').val('');
    $('#idpermintaan').val(null).trigger('change');
}

function change_idtipe(){
    var tipe_id = $('#idtipe').val();
    var ke_unit = $('#idunit').val();

    if(ke_unit !== null && tipe_id !== null) {
        table = $('#list-barang').DataTable({
           destroy: true,
                serverSide: true,
                searching: true,
                sort: false,
                autoWidth: false,
                ajax: {
                    url: '{site_url}tunit_permintaan/view_barang/',
                    type: 'post',
                    data: {idtipe: tipe_id, idunitpelayanan: ke_unit}
                },
                columns: [
                    {data: 'id', searchable: false},
                    {data: 'namatipe'},
                    {data: 'kode'},
                    {data: 'nama'},
                    {data: 'stok'},
                    {defaultContent: '<a href="#" class="btn btn-xs btn-success pilih">Pilih</a>', searchable: false },
                    {data: 'id', visible: false},
                ]
        })
		setTimeout(function() {
                $('div.dataTables_filter input').focus()
            }, 500);
        $('#modal_list_barang').modal('show');
    }
}

function barang_click(){
    table = $('#list-barang').DataTable();

    // $('#idtipe').select2({
        // placeholder: 'Cari Data ... ',
        // ajax: {
            // url: '{site_url}tunit_pengembalian/get_tipe',
            // type: 'post',
            // dataType: 'json',
            // delay: 250,
            // processResults: function (data) {
                // return {results:data}
            // },
            // cache: false,
        // }
    // })

    table.destroy()
    $('#idbarang').val(table.cell(this,0).data());
    $('#namabarang').val(table.cell(this,3).data());
    $('#stokbarang').val(table.cell(this,4).data());
    // $('#hargabarang').val(table.cell(this,4).data());
    // $('#marginbarang').val(table.cell(this,5).data());

    $('#modal_list_barang').modal('hide');

};
function validate() {
        if($('#idbarang').val() == '') {
           sweetAlert("Maaf...", "Barang harus diisi!", "error");
			$("#kuantitas").focus();
			return false;
        }
		
		if($('#kuantitas').val() <= 0) {
           sweetAlert("Maaf...", "kuantitas harus diisi!", "error");
			$("#kuantitas").focus();
			return false;
        }
		if(parseFloat($('#stokbarang').val()) < parseFloat($('#kuantitas').val())) {
           sweetAlert("Maaf...", "kuantitas Harus valid harus diisi!", "error");
			$("#kuantitas").focus();
			return false;
        }
       
       
        return true;
}
function validate_simpan() {
		var rowCount = $('#datapengembalian tr').length;
		
        if($('#alasan').val() == '') {
           sweetAlert("Maaf...", "Alasan harus diisi!", "error");
			$("#alasan").focus();
			return false;
        }
       if(rowCount =='1') {
           sweetAlert("Maaf...", "Belum ada data yang akan disubmit!", "error");
			$("#alasan").focus();
			return false;
        }
		$("#cover-spin").show();
        return true;
}
function click_simpan(){
	if(validate()) {
    table = $('#datapengembalian').DataTable({
        info: false,
        searching: false,
        paging: false,
        destroy: true,
        sort: false,
        columnDefs: [
            {targets: [6,7,8,9,10,11,12,13], visible: false}
        ]
    });
    table.row.add([
        $("#idtipe :selected").text(),
        $("#namabarang").val(),
        $('#idpermintaan :selected').text(),
        $("#kuantitas").val(),
        $("#nobatch").val(),
        '<button type="button" class="btn btn-sm btn-danger detail_remove" title="Hapus"><i class="glyphicon glyphicon-remove"></i></button>',
        // "<a href='#' class='on-default detail_remove' title='Remove'><i class='fa fa-trash-o'></i></a>",
        $("#idtipe :selected").val(),//6
        $("#idbarang").val(),//7
        $("#idpermintaan :selected").val(),//8
        $("#idunit :selected").val(),//9
        $("#alasan").val(),//10
        $("#tanggal").val(),//11
        $("#hargabarang").val(),//12
        $("#marginbarang").val(),//13
    ]).draw();
	$("#kuantitas").val('0');
	$("#namabarang").val('');
	$("#stokbarang").val('');
	$('#idbarang').val('').trigger('change');
    // $(".detail_remove").click(click_detail_remove);
    detailValue_1();
	}
}
$('#datapengembalian tbody').on( 'click', '.detail_remove', function () {
	if(confirm("Hapus Data ?") == true){
		var table = $('#datapengembalian').DataTable(); 
			var rows = table
				.row($(this).parents('tr'))
				.remove()
				.draw();
	 }
 
} );
// function click_detail_remove(){
    // // var tr = table.row( $(this).parents('tr') ).index()
    // // table.row(tr).remove().draw()
    // // detailValue_1()
	// if(confirm("Hapus Data ?") == true){
		// var table = $('#datapengembalian').DataTable(); 
			// var rows = table
				// .row($(this).parents('tr'))
				// .remove()
				// .draw();
	 // }
// }
// function submit_form(e){
	// if(validate_simpan()) {
		// e.preventDefault();
		// var formData = new FormData(this)
		// $.ajax({
			// method:'POST',
			// url: '{site_url}tunit_pengembalian/save/',
			// data: formData,
			// cache: false,
			// dataType: 'json',
			// contentType: false,
			// processData: false,
			// beforeSend: function() {
				// $('#simpan-2').html('<i class="fa fa-sun-o fa-spin"></i> Loading').attr('disabled',true);
			// },
			// success:function(res){
				// if(res.status == '200') {
					// swal('Sukses','Data berhasil disimpan!','success')
					// setTimeout(function() {
						// $('#index_list').DataTable().ajax.reload()
						// window.location = '{site_url}tunit_pengembalian'
					// }, 500);
				// } else {
					// swal('Kesalahan','Data gagal disimpan!','danger')
				// }
			// },
			// error: function() {
				// alert(1)
			// }
		// });
	// }	
// }

function detailValue_1() {
    var arr = $('#datapengembalian').DataTable().data().toArray()
    arr = JSON.stringify(arr)
    $('input[name=detailValue_1]').val( arr )
}
</script>
