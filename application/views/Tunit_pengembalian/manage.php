<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-rounded block-bordered">
    <div class="block-header">
        <ul class="block-options">
            <li><a href="{site_url}tunit_pengembalian" class="btn"><i class="fa fa-reply"></i></a></li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <?php echo form_open('tunit_pengembalian/save','class="form-horizontal" id="form-work"') ?>
    <div class="block-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="control-group">
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered" id="detail_list" style="margin-bottom:0px">
                            <thead>
                                <tr>
                                    <th>Tipe</th>
                                    <th>Barang</th>
                                    <th>U. Peminta</th>
                                    <th>Kuantitas</th>
                                    <th>Actions</th>
                                    <th hidden>idtipe</th>
                                    <th hidden>idbarang</th>
                                    <th hidden>idpeminta</th>
                                    <th hidden>alasan</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <table class="table table-striped table-bordered">
                            <tbody>
                                <tr>
                                    <td colspan="2" class="hidden-phone">
                                        <button class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalDetailPengembalian" data-backdrop="static" id="detail_pengembalian_add" type="button">Tambah Pengembalian</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="text-right bg-light lter">
                            <button class="btn btn-info" type="submit">Simpan</button>
                            <a href="{site_url}tgudang_pemesanan" class="btn btn-default">Batal</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="detail_value" name="detail_value" value="[]">
        <br><br>
        <?php echo form_hidden('id', $id); ?>
        <?php echo form_close() ?>
    </div>
</div>
<button hidden id="success_msg" class="js-notify" data-notify-type="success" data-notify-message="Success!">Success</button>
<button hidden id="error_msg" class="js-notify" data-notify-type="danger" data-notify-message="Error!">Danger</button>


<div class="modal in" id="modalDetailPengembalian" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Tambah Detail Pemesanan Gudang</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal" id="form_detail">
                        <input type="hidden" id="idbarang">

                        <div class="form-group c" id="f_idtipe">
                            <label class="col-md-3 control-label">Tipe</label>
                            <div class="col-md-6">
                                <select id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.."></select>
                            </div>
                        </div>
                        <div class="form-group c" id="f_namabarang">
                            <label class="col-md-3 control-label" for="namabarang">Nama Barang</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input readonly type="text" class="form-control" id="namabarang" placeholder="Nama Barang" name="namabarang" value="" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="form-group c" id="f_idpermintaan">
                            <label class="col-md-3 control-label">U. Permintaan</label>
                            <div class="col-md-6">
                                <select id="idpermintaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.."></select>
                            </div>
                        </div>
                        <div class="form-group c" id="f_kuantitas">
                            <label class="col-md-3 control-label" for="kuantitas">Kuantitas</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                    <input type="text" class="form-control number" id="kuantitas" placeholder="Kuantitas" name="kuantitas" value="" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label" for="alasan">Alasan</label>
                            <div class="col-xs-6">
                                <textarea class="form-control" id="alasan" name="alasan" rows="6" placeholder="Alasan">{alasan}</textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                <button class="btn btn-sm btn-primary" id="submit-form_detail" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal in" id="modal_list_barang" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">List Barang</h3>
                </div>
                <div class="block-content">
                    <table class="table table-striped table-bordered" id="list-barang">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Kode</th>
                                <th>Nama</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>                    
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" id="btnback" type="button" data-dismiss="modal">Back</button>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">

var table;

$(document).on('click','#detail_pengembalian_add', function(){
	$('.number').number(true, 0);
    table = $('#list-barang').DataTable().destroy()

    var option      = JSON.parse('{"1":"Alkes","2":"Implan","3":"Obat","4":"Logistik"}');
    var dataoption  = '<option value="">Pilih Opsi</option>';
    $.each(option, function(i,item){
        dataoption += '<option value="'+i+'">'+item+'</option>';
    })

    $('#namabarang,#kuantitas').val('')
    $('#idtipe').empty().append('#idtipe',dataoption).selectpicker('refresh')
    $('#idpermintaan').empty().selectpicker('refresh')
})

$(document).on('change','#idtipe', function(){

    var n = $('#idtipe').val()

    $('#modalDetailPengembalian').hide()
    table = $('#list-barang').DataTable({
        pageLength: 50,  
        serverSide: true,        
        autoWidth: false,        
        order: [],
        "ajax": { "url": '{site_url}tunit_pengembalian/getListBarang/'+n, "type": "POST" },
        "columns": [
            {data: "id", visible: false},
            {data: "kode", width: '10%', className: 'link-kodebarang'},
            {data: "nama", width: '10%'},
        ],
    })
    $('#modal_list_barang').fadeIn()

})

$('#list-barang').on( 'click', 'td', function(){
    table = $('#list-barang').DataTable()

    // nopermintaan
    $.ajax({
        url: '{site_url}tunit_pengembalian/getIdPermintaan',
        data: { idtipe: $('#idtipe').val(), idbarang: table.cell(this,0).data() },
        method: 'POST',
        dataType: 'json',
        success: function(data) {
            var dataoption  = '<option value="">Pilih Opsi</option>';
            $.each(data, function(i,item){
                dataoption += '<option value="'+item.id+'">'+item.nopermintaan+' - '+item.unitpeminta+'</option>';
            })
            $('#idpermintaan').empty().append(this,dataoption).selectpicker('refresh')
        }
    })

    table.destroy()
    $('#idbarang').val( table.cell(this,0).data() )
    $('#namabarang').val( table.cell(this,2).data() )
    $('#idpermintaan').empty().selectpicker('refresh')
    

    $('#modal_list_barang').hide()
    $('#modalDetailPengembalian').fadeIn()


})

$(document).on( 'click', '#submit-form_detail', function(){
    var table = "<tr>";
    table += "<td>"+$("#idtipe :selected").text()+"</td>";
    table += "<td>"+$("#namabarang").val()+"</td>";
    table += "<td>"+$('#idpermintaan :selected').text()+"</td>";
    table += "<td>"+$("#kuantitas").val()+"</td>";
    table += "<td><a href='#' class='on-default detail_remove' title='Remove'><i class='fa fa-trash-o'></i></a></td>";

    table += "<td hidden>"+$("#idtipe :selected").val()+"</td>"; //5
    table += "<td hidden>"+$("#idbarang").val()+"</td>";
    table += "<td hidden>"+$("#idpermintaan :selected").val()+"</td>";
    table += "<td hidden>"+$("#alasan").val()+"</td>";

    table += "</tr>";
    $('#detail_list tbody').append(table);
    detail_value()
    $.toaster({priority : 'success', title : 'Sukses!', message : 'Data berhasil ditambahkan ... '});
})

$(document).on("click",".detail_remove", function() {
    $(this).closest('tr').remove();
    detail_value()

    $.toaster({priority : 'success', title : 'Sukses!', message : 'Data berhasil dihapus'});
})

$(document).on('click','#btnback', function(){
    table = $('#list-barang').DataTable().destroy()
    $('#modalDetailPengembalian').fadeIn()
    $('#modal_list_barang').hide()

    var option      = JSON.parse('{"1":"Alkes","2":"Implan","3":"Obat","4":"Logistik"}');
    var dataoption  = '<option value="">Pilih Opsi</option>';
    $.each(option, function(i,item){
        dataoption += '<option value="'+i+'">'+item+'</option>';
    })

    $('#namabarang,#kuantitas').val('')
    $('#idtipe').empty().append('#idtipe',dataoption).selectpicker('refresh')
    $('#idpermintaan').empty().selectpicker('refresh')
})

function detail_value() {
    var detail_tbl = $('table#detail_list tbody tr').get().map(function(row) {
        return $(row).find('td').get().map(function(cell) {
            return $(cell).html();
        });
    });

    $("#detail_value").val(JSON.stringify(detail_tbl))
}
    

</script>
