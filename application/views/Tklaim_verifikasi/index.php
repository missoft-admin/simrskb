<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Registrasi</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_reg" placeholder="No Registrasi" name="no_reg" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Medrec</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_medrec" placeholder="No. Medrec" name="no_medrec" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="nama_pasien" placeholder="Nama Pasien" name="nama_pasien" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Kelompok Pasien</label>
                    <div class="col-md-8">
                        <select id="idkelompokpasien" name="idkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Kelompok Pasien -</option>
							<?foreach($list_kelompok_pasien as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Perusahaan</label>
                    <div class="col-md-8">
                        <select id="idrekanan" name="idrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Perusahaan -</option>
							<?foreach($list_rekanan as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
                 <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label text-left" id="lbl_warning" for=""></label>
                    <div class="col-md-8">
						<div class="alert alert-success alert-dismissable" id="label_warning">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<p><strong>Silahkan Tentukan Filter Pencarian selanjutnya  Klik Tombol Filter</strong></p>
						</div>
					</div>
                </div>                         
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tipe</label>
                    <div class="col-md-8">
						<select id="tipe" name="tipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Tipe -</option>
							<option value="1" <?=("1" == $tipe ? 'selected' : ''); ?>>Rawat Jalan</option>
							<option value="2" <?=("2" == $tipe ? 'selected' : ''); ?>>Ranap / ODS</option>
							
						</select>
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Status</label>
                    <div class="col-md-8">
                        <select id="status" name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Status -</option>
							<option value="0" <?=("0" == $status ? 'selected' : ''); ?>>Menunggu Proses</option>
							<option value="1" <?=("1" == $status ? 'selected' : ''); ?>>Telah Diverifikasi</option>
							<option value="2" <?=("2" == $status ? 'selected' : ''); ?>>Telah Diproses</option>
							
						</select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Transaksi</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tgl_trx1" name="tgl_trx1" placeholder="From" value="{tgl_trx1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tgl_trx2" name="tgl_trx2" placeholder="To" value="{tgl_trx2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Daftar</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggaldaftar1" name="tanggaldaftar1" placeholder="From" value="{tanggaldaftar1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggaldaftar2" name="tanggaldaftar2" placeholder="To" value="{tanggaldaftar2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-4" hidden>
                        <button class="btn btn-danger text-uppercase" type="button" name="btn_verif_filter" id="btn_verif_filter" style="font-size:13px;width:100%;"><i class="fa fa-check-square-o"></i> Verifikasi By Filter</button>
                    </div>
					<div class="col-md-8" hidden>
                        <button disabled class="btn btn-primary text-uppercase" type="button" name="btn_verif_page" id="btn_verif_page" style="font-size:13px;width:100%;"><i class="fa fa-check"></i> Verifikasi Per page</button>
                    </div>
                </div>
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th hidden width="5%">TIPE</th>
                    <th hidden width="2%">ID</th>
                    <th width="10%">NO</th>
                    <th width="10%">TGL KUNJUGAN</th>
                    <th width="10%">TGR TRX</th>
                    <th width="10%">JENIS</th>
                    <th width="10%">NO REG</th>
                    <th width="10%">MEDREC</th>
                    <th width="10%">PASIEN</th>
                    <th width="15%">KEL PASIEN</th>
                    <th width="15%">ASURANSI</th>
                    <th width="15%">STATUS</th>
                    <th width="15%">TOT TRX</th>
                    <th width="15%">ASURANSI</th>
                    <th width="15%">BAYAR LAINYA</th>
                    <th width="15%"> TGL PENAGIHAN</th>
                    <th width="15%" class="text-center">AKSI</th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
		</div>
	</div>
</div>
<div class="modal fade" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Pengalihan Jatuh Tempo </h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">
							<label class="col-md-4 control-label" for="status">Tanggal Asal</label>
							<div class="col-md-8">
							<div class="input-group">
								<input class="form-control" readonly type="text" id="tgl_asal" name="tgl_asal" value="">
								<input class="form-control" readonly type="hidden" id="xst_multiple" name="xst_multiple" value="">
								<input class="form-control" readonly type="hidden" id="xidpasien" name="xidpasien" value="">

								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
							</div>
						</div> 	
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Kelompok Pasien</label>
								<div class="col-md-8">
									<select id="xidkelompokpasien" name="xidkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach($list_kelompok_pasien as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>									
										<?}?>
										
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Nama Rekanan</label>
								<div class="col-md-8">
									<select id="xidrekanan" name="xidrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#">- Pilih Rekanan -</option>
										<?foreach($list_rekanan as $row){?>
											<option value="<?=$row->id?>" ><?=$row->nama?></option>									
										<?}?>
										
									</select>
								</div>
							</div>				
						</div>		
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Tanggal</label>
								<div class="col-md-8">
									<select id="xtgl_pilih" name="xtgl_pilih" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										
									</select>
								</div>
							</div>				
						</div>				
					</div>
					<div class="row">
							<div class="col-md-12">
							<label class="col-md-4 control-label" for="status"></label>	 
								<input type="hidden" class="form-control" id="xid" placeholder="No. PO" name="xid" value="">
								<input type="hidden" class="form-control" id="xtanggal_tagihan" placeholder="No. PO" name="xtanggal_tagihan" value="">
								<input type="hidden" class="form-control" id="xtipe" placeholder="No. PO" name="xtipe" value="">
							</div>
					</div>
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_ubah" id="btn_ubah"><i class="fa fa-save"></i> SIMPAN</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	// load_index();
   
});
function load_index(){
	var no_reg=$("#no_reg").val();
	var no_medrec=$("#no_medrec").val();
	var nama_pasien=$("#nama_pasien").val();
	var idkelompokpasien=$("#idkelompokpasien").val();
	var idrekanan=$("#idrekanan").val();
	var tipe=$("#tipe").val();
	var status=$("#status").val();
	var tgl_trx1=$("#tgl_trx1").val();
	var tgl_trx2=$("#tgl_trx2").val();
	var tanggaldaftar1=$("#tanggaldaftar1").val();
	var tanggaldaftar2=$("#tanggaldaftar2").val();
	$('#index_list').DataTable().destroy();
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0,1,17,18,19,20,21,22,23], "visible": false },
							{ "width": "3%", "targets": [2] },
							{ "width": "5%", "targets": [5,7] },
							{ "width": "5%", "targets": [3,4] },
							{ "width": "7%", "targets": [6,15,11,12,13,14] },
							{ "width": "8%", "targets": [8,,9,10] },
							{ "width": "15%", "targets": [16] },
						 {"targets": [8,16], className: "text-left" },
						 {"targets": [2,12,13,14,15], className: "text-right" },
						 {"targets": [3,4,5,6,7,9,10,11], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tklaim_verifikasi/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						no_reg:no_reg,no_medrec:no_medrec,nama_pasien:nama_pasien,idkelompokpasien:idkelompokpasien,
						idrekanan:idrekanan,tipe:tipe,status:status,tgl_trx1:tgl_trx1,
						tgl_trx2:tgl_trx2,tanggaldaftar1:tanggaldaftar1,tanggaldaftar2:tanggaldaftar2,
					   }
            }
        });
}
$("#btn_filter").click(function() {
	// alert('ID');
	// table.destroy();
	load_index();
});

$(document).on("click", ".verifikasi", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var tipe=table.cell(tr,0).data()
	var noreg=table.cell(tr,6).data()
	var tipekontraktor=table.cell(tr,20).data()
	var idkontraktor=table.cell(tr,21).data()
	var tanggal_jt=table.cell(tr,15).data()
	var st_multiple=table.cell(tr,22).data()
	var idpasien=table.cell(tr,23).data()
	  // $row[] = $r->st_multiple;//22
            // $row[] = $r->idpasien;//23
			
	// alert(tanggal_jt);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Verifikasi No. Registrasi "+noreg+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		verifikasi(id,tipe,tipekontraktor,idkontraktor,tanggal_jt,st_multiple,idpasien);
	});
});
function verifikasi($id,$tipe,$tipekontraktor,$idkontraktor,$tanggal_jt,$st_multiple,$idpasien){
	console.log($id);
	var id=$id;		
	var tipe=$tipe;		
	var tipekontraktor=$tipekontraktor;		
	var idkontraktor=$idkontraktor;		
	var tanggal_jt=$tanggal_jt;		
	var st_multiple=$st_multiple;		
	var idpasien=$idpasien;		
	$("#cover-spin").show();
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tklaim_verifikasi/verifikasi',
		type: 'POST',
		data: {id: id,tipe:tipe,tipekontraktor:tipekontraktor,idkontraktor:idkontraktor,tanggal_jt:tanggal_jt,st_multiple:st_multiple,idpasien:idpasien},
		success: function(data) {
			console.log(data);
			if (data=='true'){
				$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi'});
				table.ajax.reload( null, false ); 
				$("#cover-spin").hide();
			}else{
				$("#cover-spin").hide();
				sweetAlert("Maaf...", "Data Duplicate", "error");
			}
		}
	});
}

$(document).on("click", ".batal_verifikasi", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var nopenerimaan=table.cell(tr,4).data()
	// alert(id);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Membatalkan Verifikasi No. Penerimaan "+nopenerimaan+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		batal_verifikasi(id);
	});
});
function batal_verifikasi($id){
	// console.log($id);
	var id=$id;		
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tklaim_verifikasi/batal_verifikasi',
		type: 'POST',
		data: {id: id},
		complete: function() {
			$.toaster({priority : 'success', title : 'Succes!', message : ' Membatalkan Verifikasi'});
			table.ajax.reload( null, false ); 
		}
	});
}
$(document).on("click", ".ganti", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,17).data()
	var st_multiple=table.cell(tr,22).data()
	var idpasien=table.cell(tr,23).data()
	
	$("#xid").val(id);
	$("#xtipe").val(table.cell(tr,0).data()).trigger('change');
	$("#xidkelompokpasien").val(table.cell(tr,20).data()).trigger('change');
	$("#xidrekanan").val(table.cell(tr,21).data()).trigger('change');
	$("#tgl_asal").val(table.cell(tr,15).data()).trigger('change');
	$("#xtanggal_tagihan").val(table.cell(tr,15).data()).trigger('change');
	$("#xst_multiple").val(st_multiple);
	$("#xidpasien").val(idpasien);
	load_list_tanggal();
	$('#modal_edit').modal('show');
	
});
function load_list_tanggal(){
	var idkelompokpasien=$("#xidkelompokpasien").val();
	var idrekanan=$("#xidrekanan").val();
	var tipe=$("#xtipe").val();
	var st_multiple=$("#xst_multiple").val();
	var idpasien=$("#xidpasien").val();
	// alert(idkelompokpasien+' - '+idrekanan+' - '+tipe);
	if (idkelompokpasien && tipe){
		$.ajax({
			url: '{site_url}tklaim_rincian/list_tanggal',
			dataType: "json",
			type: 'POST',
			data: {idkelompokpasien: idkelompokpasien,idrekanan:idrekanan,tipe:tipe,st_multiple:st_multiple,idpasien:idpasien},
			success: function(data) {
				$("#xtgl_pilih").empty();
				$('#xtgl_pilih').append(data.detail);
			}
		});
	}
}
$(document).on("change", "#xidkelompokpasien", function() {
	var idkelompokpasien=$("#xidkelompokpasien").val();
	// alert(idkelompokpasien);
	$.ajax({
		url: '{site_url}tklaim_rincian/get_st_multiple',
		dataType: "json",
		type: 'POST',
		data: {idkelompokpasien: idkelompokpasien},
		success: function(data) {
			$("#xst_multiple").val(data.st_multiple);
			// alert('Load Tanggal');
			load_list_tanggal();
		}
	});
	if ($(this).val()=='1'){
		$("#xidrekanan").val("#").trigger('change');
		$('#div_rekanan').show();
	}else{
		$('#div_rekanan').hide();
	}
	
});
$(document).on("change", "#xidrekanan", function() {
	load_list_tanggal();
	
});
$(document).on("click", "#btn_ubah", function() {
		if ($("#xtgl_pilih").val()=='' || $("#xtgl_pilih").val()==null){
			sweetAlert("Maaf...", "Tanggal belum dipilih!", "error");
			return false;
		}
		// alert();
		if ($("#xidkelompokpasien").val()=='1' && $("#xidrekanan").val()=='#'){
			sweetAlert("Maaf...", "Rekanan Harus diisi!", "error");
			return false;
		}
		
		swal({
			title: "Anda Yakin ?",
			text : "Akan Memindahkan Faktur ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			edit_tanggal_satu();
		});
		
});
function edit_tanggal_satu(){
	var klaim_id=$("#xid").val();		
	var tanggal_tagihan=$("#xtgl_pilih").val();		
	var idkelompokpasien=$("#xidkelompokpasien").val();		
	var idrekanan=$("#xidrekanan").val();		
	var tipe=$("#xtipe").val();	
	var st_multiple=$("#xst_multiple").val();
	var idpasien=$("#xidpasien").val();
	// alert(klaim_id+' tg l'+tanggal_tagihan+' Kel '+idkelompokpasien+' rekan '+idrekanan+' tipe '+tipe);
	// return false;
	var table = $('#index_list').DataTable();
	$.ajax({
		url: '{site_url}tklaim_rincian/edit_tanggal_satu',
		type: 'POST',
		data: {klaim_id: klaim_id,tanggal_tagihan:tanggal_tagihan,idkelompokpasien:idkelompokpasien,idrekanan:idrekanan,tipe:tipe,st_multiple:st_multiple,idpasien:idpasien},
		complete: function() {

			$.toaster({priority : 'success', title : 'Succes!', message : ' Memindahkan Tanggal Jatuh Tempo'});
			$("#modal_edit").modal('hide');
			$('#index_list').DataTable().ajax.reload( null, false );
			$("#cover-spin").hide();
		}
	});
}
</script>