<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}mtarif_fisioterapi" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mtarif_fisioterapi/save','class="form-horizontal push-10-t" id="form-work"') ?>
		<div class="form-group">
			<label class="col-md-3 control-label" for="headerpath">Parent</label>
			<div class="col-md-7">
				<select name="headerpath" id="headerpath" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
					<option value="" selected>Pilih Opsi</option>
					<?=$list_parent?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="idkelompok">Jenis</label>
			<div class="col-md-7">
				<label class="radio-inline" for="kelompok">
            <input type="radio" id="kelompok" checked name="idkelompok" onclick="toggleJenis('kelompok')" <?=($idkelompok == 1 ? 'checked':'')?> value="1"> Kelompok
        </label>
				<label class="radio-inline" for="rincian">
            <input type="radio" id="rincian" name="idkelompok" onclick="toggleJenis('rincian')" <?=($idkelompok == 0 ? 'checked':'')?> value="0"> Rincian
        </label>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Nama</label>
			<div class="col-md-7">
				<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
			</div>
		</div>
	</div>
	<div class="block-content" id="form-detail" style="display:none">
		<hr>
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="totaltarif_list">
			<thead>
				<tr>
					<th>Kelas</th>
					<th>Jasa Sarana</th>
					<th>Jasa Pelayanan</th>
					<th>BHP</th>
					<th>Biaya Perawatan</th>
					<th>Total Tarif</th>
				</tr>
			</thead>
			<tbody>
				<?php if($this->uri->segment(2) == 'create'){ ?>
				<tr style="background-color:#e0dfda">
					<td style="text-align:center">POLIKLINIK / IGD</td>
					<td style="display:none"><input type="hidden" class="kelaskamar" value="0"></td>
					<td><input type="text" class="form-control number jasasarana" placeholder="Jasa Sarana" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasasarana" placeholder="Group Jasa Sarana" value=""></td>
					<td><input type="text" class="form-control number jasapelayanan" placeholder="Jasa Pelayanan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasapelayanan" placeholder="Group Jasa Pelayanan" value=""></td>
					<td><input type="text" class="form-control number bhp" placeholder="BHP" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_bhp" placeholder="Group BHP" value=""></td>
					<td><input type="text" class="form-control number biayaperawatan" placeholder="Biaya Perawatan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_biayaperawatan" placeholder="Group Biaya Perawatan" value=""></td>
					<td><input type="text" readonly class="form-control total" placeholder="Total" value="0"></td>
				</tr>
				<tr>
					<td style="text-align:center">I</td>
					<td style="display:none"><input type="hidden" class="kelaskamar" value="1"></td>
					<td><input type="text" class="form-control number jasasarana" placeholder="Jasa Sarana" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasasarana" placeholder="Group Jasa Sarana" value=""></td>
					<td><input type="text" class="form-control number jasapelayanan" placeholder="Jasa Pelayanan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasapelayanan" placeholder="Group Jasa Pelayanan" value=""></td>
					<td><input type="text" class="form-control number bhp" placeholder="BHP" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_bhp" placeholder="Group BHP" value=""></td>
					<td><input type="text" class="form-control number biayaperawatan" placeholder="Biaya Perawatan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_biayaperawatan" placeholder="Group Biaya Perawatan" value=""></td>
					<td><input type="text" readonly class="form-control total" placeholder="Total" value="0"></td>
				</tr>
				<tr>
					<td style="text-align:center">II</td>
					<td style="display:none"><input type="hidden" class="kelaskamar" value="2"></td>
					<td><input type="text" class="form-control number jasasarana" placeholder="Jasa Sarana" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasasarana" placeholder="Group Jasa Sarana" value=""></td>
					<td><input type="text" class="form-control number jasapelayanan" placeholder="Jasa Pelayanan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasapelayanan" placeholder="Group Jasa Pelayanan" value=""></td>
					<td><input type="text" class="form-control number bhp" placeholder="BHP" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_bhp" placeholder="Group BHP" value=""></td>
					<td><input type="text" class="form-control number biayaperawatan" placeholder="Biaya Perawatan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_biayaperawatan" placeholder="Group Biaya Perawatan" value=""></td>
					<td><input type="text" readonly class="form-control total" placeholder="Total" value="0"></td>
				</tr>
				<tr>
					<td style="text-align:center">III</td>
					<td style="display:none"><input type="hidden" class="kelaskamar" value="3"></td>
					<td><input type="text" class="form-control number jasasarana" placeholder="Jasa Sarana" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasasarana" placeholder="Group Jasa Sarana" value=""></td>
					<td><input type="text" class="form-control number jasapelayanan" placeholder="Jasa Pelayanan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasapelayanan" placeholder="Group Jasa Pelayanan" value=""></td>
					<td><input type="text" class="form-control number bhp" placeholder="BHP" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_bhp" placeholder="Group BHP" value=""></td>
					<td><input type="text" class="form-control number biayaperawatan" placeholder="Biaya Perawatan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_biayaperawatan" placeholder="Group Biaya Perawatan" value=""></td>
					<td><input type="text" readonly class="form-control total" placeholder="Total" value="0"></td>
				</tr>
				<tr>
					<td style="text-align:center">UTAMA</td>
					<td style="display:none"><input type="hidden" class="kelaskamar" value="4"></td>
					<td><input type="text" class="form-control number jasasarana" placeholder="Jasa Sarana" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasasarana" placeholder="Group Jasa Sarana" value=""></td>
					<td><input type="text" class="form-control number jasapelayanan" placeholder="Jasa Pelayanan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_jasapelayanan" placeholder="Group Jasa Pelayanan" value=""></td>
					<td><input type="text" class="form-control number bhp" placeholder="BHP" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_bhp" placeholder="Group BHP" value=""></td>
					<td><input type="text" class="form-control number biayaperawatan" placeholder="Biaya Perawatan" value="0"></td>
					<td style="display:none"><input type="text" class="form-control group_biayaperawatan" placeholder="Group Biaya Perawatan" value=""></td>
					<td><input type="text" readonly class="form-control total" placeholder="Total" value="0"></td>
				</tr>
				<?php }else{ ?>
					<?php foreach  ($list_tarif as $row) { ?>
					<tr style="background-color:<?=($row->kelas == '0' ? '#e0dfda' : '')?>">
						<td style="text-align:center"><?=GetKelas($row->kelas)?></td>
						<td style="display:none"><input type="hidden" class="kelaskamar" value="<?=$row->kelas?>"></td>
						<td><input type="text" class="form-control number jasasarana" placeholder="Jasa Sarana" value="<?=$row->jasasarana?>"></td>
						<td style="display:none"><input type="text" class="form-control group_jasasarana" placeholder="Group Jasa Sarana" value="<?=$row->group_jasasarana?>"></td>
						<td><input type="text" class="form-control number jasapelayanan" placeholder="Jasa Pelayanan" value="<?=$row->jasapelayanan?>"></td>
						<td style="display:none"><input type="text" class="form-control group_jasapelayanan" placeholder="Group Jasa Pelayanan" value="<?=$row->group_jasapelayanan?>"></td>
						<td><input type="text" class="form-control number bhp" placeholder="BHP" value="<?=$row->bhp?>"></td>
						<td style="display:none"><input type="text" class="form-control group_bhp" placeholder="Group BHP" value="<?=$row->group_bhp?>"></td>
						<td><input type="text" class="form-control number biayaperawatan" placeholder="Biaya Perawatan" value="<?=$row->biayaperawatan?>"></td>
						<td style="display:none"><input type="text" class="form-control group_biayaperawatan" placeholder="Group Biaya Perawatan" value="<?=$row->group_biayaperawatan?>"></td>
						<td><input type="text" readonly class="form-control number total" placeholder="Total" value="<?=$row->total?>"></td>
					</tr>
					<?php } ?>
				<?php } ?>
			</tbody>
		</table>
	</div>
	</div>

	<div class="row" style="margin-top:-25px">
		<div class="block-content block-content-narrow">
			<div class="form-group">
				<label class="col-md-3 control-label"></label>
				<div class="col-md-7">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mtarif_fisioterapi" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
		</div>
	</div>

	<input type="hidden" id="old_headerpath" name="old_headerpath" value="{headerpath}" readonly>
	<input type="hidden" id="path" name="path" value="{path}" readonly>
	<input type="hidden" id="level" name="level" value="{level}" readonly>
	<input type="hidden" id="totaltarif_value" name="totaltarif_value" />
	<br><br>
	<?php echo form_hidden('id', $id); ?>
	<?php echo form_close() ?>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	var idkelompok = <?=$idkelompok?>;

	$(document).ready(function() {
		$(".number").number(true, 0, '.', ',');

		if (idkelompok == 1) {
			$("#form-detail").hide();
		} else {
			$("#form-detail").show();
		}

		$(document).on("change", "#headerpath", function() {
			$.ajax({
				url: '{base_url}mtarif_fisioterapi/get_child_level/' + $(this).val(),
				method: 'GET',
				dataType: 'json',
				success: function(data) {
					if ($('#headerpath option:selected').text() === 'Root') {
						$("#path").val($('#headerpath option:selected').val());
					} else {
						$("#path").val(data.path);
					}
					$("#level").val(data.level);
				}
			});
		});

		$("#form-work").submit(function(e) {
			var form = this;

			var totaltarif_tbl = $('table#totaltarif_list tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					if ($(cell).find("input").length >= 1) {
						return $(cell).find("input").val();
					} else {
						return $(cell).html();
					}
				});
			});

			$("#totaltarif_value").val(JSON.stringify(totaltarif_tbl));

			swal({
				title: "Berhasil!",
				text: "Proses penyimpanan data.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});

		$(document).on("keyup", ".jasasarana", function() {
			var jasasarana = parseInt($(this).val().replace(/,/g, ''));
			var jasapelayanan = parseInt($(this).closest('tr').find(".jasapelayanan").val().replace(/,/g, ''));
			var bhp = parseInt($(this).closest('tr').find(".bhp").val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).closest('tr').find(".biayaperawatan").val().replace(/,/g, ''));
			var total = jasasarana + jasapelayanan + bhp + biayaperawatan;
			$(this).closest('tr').find(".total").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});
		$(document).on("keyup", ".jasapelayanan", function() {
			var jasasarana = parseInt($(this).closest('tr').find(".jasasarana").val().replace(/,/g, ''));
			var jasapelayanan = parseInt($(this).val().replace(/,/g, ''));
			var bhp = parseInt($(this).closest('tr').find(".bhp").val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).closest('tr').find(".biayaperawatan").val().replace(/,/g, ''));
			var total = jasasarana + jasapelayanan + bhp + biayaperawatan;
			$(this).closest('tr').find(".total").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});
		$(document).on("keyup", ".bhp", function() {
			var jasasarana = parseInt($(this).closest('tr').find(".jasasarana").val().replace(/,/g, ''));
			var jasapelayanan = parseInt($(this).closest('tr').find(".jasapelayanan").val().replace(/,/g, ''));
			var bhp = parseInt($(this).val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).closest('tr').find(".biayaperawatan").val().replace(/,/g, ''));
			var total = jasasarana + jasapelayanan + bhp + biayaperawatan;
			$(this).closest('tr').find(".total").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});
		$(document).on("keyup", ".biayaperawatan", function() {
			var jasasarana = parseInt($(this).closest('tr').find(".jasasarana").val().replace(/,/g, ''));
			var jasapelayanan = parseInt($(this).closest('tr').find(".jasapelayanan").val().replace(/,/g, ''));
			var bhp = parseInt($(this).closest('tr').find(".bhp").val().replace(/,/g, ''));
			var biayaperawatan = parseInt($(this).val().replace(/,/g, ''));
			var total = jasasarana + jasapelayanan + bhp + biayaperawatan;
			$(this).closest('tr').find(".total").val(total.toLocaleString(undefined, {
				minimumFractionDigits: 0
			}));
		});
	});

	function toggleJenis(label) {
		if (label == 'kelompok') {
			$("#form-detail").hide();
		} else {
			$("#form-detail").show();
		}
	}
</script>
