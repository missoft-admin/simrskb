<?php 'Naha'?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Laporan Penerimaan</title>
  <style type="text/css" media="all">


    body {
      -webkit-print-color-adjust: exact;
    }
    @page {
           margin-top: 2em;
            margin-left: 0.8em;
            margin-right: 1em;
            margin-bottom: 0,5em;
        }
    @media screen {


      table {

		font-family: "Courier", Arial,sans-serif;
        font-size: 18px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		table-layout:fixed;
		  overflow:hidden;
		  word-wrap:break-word;
      }
      th {
        padding: 2px;

      }
	  td {
        padding: 3px;

      }
      .content td {
        padding: 2px;
		border: 0px solid #6033FF;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .text-header{
		font-family: "Courier", Arial,sans-serif;
		font-size: 20px !important;
      }
	  .text-judul{
        font-size: 28px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
    }


  </style>
  <script type="text/javascript">
    // try {
      // this.print();
    // } catch (e) {
      // window.onload = window.print;
    // }
  </script>
</head>

<body>

  <table class="content">
      <tr>
        <td rowspan="2" width="20%" class="text-center"><img src="{logo1_rs}" alt="" width="100" height="100"></td>
        <td width="60%"  class="text-center text-judul"><u><b>LAPORAN PENERIMAAN <?=$user_pilih?></b></u></td>
		<td width="20%"></td>
      </tr>
	  <tr>
			<td class="text-center">PERIODE : <?=$nama_periode?></td>
			<td></td>
	  </tr>

  </table>
  <br/>
  <br/>
  <?

foreach ($user_list as $r_user){
	  $tot_user_tunai=0;
	  $tot_user_bank=0;
	  $tot_user_kontraktor=0;
	  $tot_user_tt=0;
?>
<fieldset style="border:1px solid #000 !important;border-radius:0px;">
		  <legend style="font-weight:bold;"></legend>
  <table class="content" width="100%">
    <thead>
      <tr>
        <td class=" text-left text-header" colspan="11"><b>USER : <?=strtoupper($r_user->name)?></b></td>
      </tr>
	  <tr>
        <td class=" text-left text-header" colspan="11"><b>PENJUALAN NON RUJUKAN (OBAT BEBAS)</b></td>
      </tr>
	  <tr>
        <td class="border-full text-center" rowspan="2" style="width:3%">NO</td>
        <td class="border-full text-center" rowspan="2" style="width:9%">NO KASIR</td>
        <td class="border-full text-center" rowspan="2" style="width:9%">MEDREC</td>
        <td class="border-full text-center" rowspan="2" style="width:12%">NAMA PASIEN</td>
        <td class="border-full text-center" rowspan="2" style="width:6%">TAG BAYAR</td>
        <td class="border-full text-center" colspan="3" style="width:30%">PEMBAYARAN</td>
        <td class="border-full text-center" rowspan="2" style="width:10%">PORSI DOKTER</td>
        <td class="border-full text-center" rowspan="2" style="width:10%">NETTO</td>
        <td class="border-full text-center" rowspan="2" style="width:10%">KET</td>
      </tr>
	  <tr>
		<td class="border-full text-center" style="width:10%">TUNAI</td>
		<td class="border-full text-center" style="width:10%">NON TUNAI</td>
		<td class="border-full text-center" style="width:10%">T TERTAGIH</td>

	  </tr>
    </thead>
    <tbody>
	<?
		$q_ob="SELECT T1.id,T1.tanggal,T1.no_bukti,T1.no_kasir,T1.nomedrec,T1.nama,GROUP_CONCAT(T1.tag separator '+') as tag
			,SUM(T1.tunai)-T1.kembalian as tunai,SUM(T1.non_tunai) as non_tunai,SUM(T1.kontraktor) as kontraktor,SUM(T1.t_t) as t_t
			,GROUP_CONCAT(T1.ket_bank) as ket_bank,GROUP_CONCAT(T1.nama_rekanan) as nama_rekanan, GROUP_CONCAT(T1.jaminan) as jaminan
			,GROUP_CONCAT(T1.nama_dokter) as nama_dokter,GROUP_CONCAT(T1.nama_pegawai) as nama_pegawai,T1.kembalian
			FROM (SELECT H.id,H.tanggal,P.nopenjualan as no_bukti,H.nokasir as no_kasir,P.nomedrec,P.nama,
			CASE
			WHEN B.idmetode='1' THEN 'T'
			WHEN B.idmetode='2' THEN 'DB'
			WHEN B.idmetode='3' THEN 'KR'
			WHEN B.idmetode='4' THEN 'TF'
			WHEN B.idmetode='5' AND B.idpegawai IS NOT NULL THEN 'TP'
			WHEN B.idmetode='5' AND B.iddokter IS NOT NULL THEN 'TD'
			WHEN B.idmetode='6' THEN 'TT'
			WHEN B.idmetode='7' THEN 'PA'
			END as tag
			,CASE WHEN B.idmetode='1' THEN B.nominal ELSE 0 END as tunai
			,CASE WHEN B.idmetode IN('2','3','4') THEN B.nominal ELSE 0 END as non_tunai
			,CASE WHEN B.idmetode IN('5','7') THEN B.nominal ELSE 0 END as kontraktor
			,CASE WHEN B.idmetode IN('6') THEN B.nominal ELSE 0 END as t_t,'' as porsi_dokter,
			CONCAT(mbank.noakun,'-',mbank.nama) as ket_bank,mrekanan.nama as nama_rekanan,B.jaminan,mdokter.nama as nama_dokter,mpegawai.nama as nama_pegawai
			,COALESCE(H.kembalian) as kembalian
			from tkasir H
			INNER JOIN tkasir_pembayaran B ON H.id=B.idkasir
			INNER JOIN tpasien_penjualan P ON P.id=H.idtindakan
			LEFT JOIN mbank ON mbank.id=B.idbank
			LEFT JOIN mrekanan ON mrekanan.id=B.idkontraktor AND B.tipekontraktor='1'
			LEFT JOIN mdokter ON mdokter.id = B.iddokter AND B.idmetode='5' AND B.tipepegawai='2'
			LEFT JOIN mpegawai ON mpegawai.id=B.idpegawai AND B.idmetode='5' AND B.tipepegawai='1'
			WHERE H.idtipe='3' AND H.`status`='2' AND H.edited_by=".$r_user->id." AND DATE_FORMAT(H.tanggal,'%Y-%m-%d') >= '$tanggaldari' AND DATE_FORMAT(H.tanggal,'%Y-%m-%d') <= '$tanggalsampai') T1
			GROUP BY T1.id
			ORDER BY T1.tanggal ASC";
			
		$list_ob=$this->db->query($q_ob)->result();
	?>
	<? $i=1; $tot_tunai=0;$tot_non_tunai=0;$tot_tt=0;$tot_neto=0;
	foreach($list_ob as $r){
	$neto=0;
	$ket='';
	$tot_tunai +=($r->tunai);
	$tot_non_tunai +=$r->non_tunai+$r->kontraktor;
	$tot_tt +=$r->t_t;
	$neto=$r->tunai+$r->non_tunai+$r->kontraktor+$r->t_t;
	$tot_neto +=$neto;

	$tot_user_tunai +=$r->tunai;
	$tot_user_bank +=$r->non_tunai;
	$tot_user_kontraktor +=$r->kontraktor;
	$tot_user_tt +=$r->t_t;

	$ket.=($r->ket_bank?$r->ket_bank:'');
	$ket.=($r->nama_rekanan !='' && $ket!=''?',':'').($r->nama_rekanan?$r->nama_rekanan:'');
	$ket.=($r->jaminan !=',' && $ket!=''?',':'').($r->jaminan !=','?$r->jaminan:'');
	$ket.=($r->nama_dokter !='' && $ket!=''?',':'').($r->nama_dokter?$r->nama_dokter:'');
	$ket.=($r->nama_pegawai !='' && $ket!=''?',':'').($r->nama_pegawai?$r->nama_pegawai:'');

	?>
	<tr>
     <td class="border-full text-center"><?=$i?></td>
     <td class="border-full text-center"><?=$r->no_kasir?></td>
     <td class="border-full text-center"><?=$r->nomedrec?></td>
     <td class="border-full text-left"><?=$r->nama?></td>
     <td class="border-full text-center"><?=$r->tag?></td>
     <td class="border-full text-right"><?=number($r->tunai)?></td>
     <td class="border-full text-right"><?=number($r->non_tunai+$r->kontraktor)?></td>
     <td class="border-full text-right"><?=number($r->t_t)?></td>
     <td class="border-full text-center"></td>
     <td class="border-full text-right"><?=number($neto)?></td>
     <td class="border-full text-left"><?=$ket?></td>
	 </tr>
	<?
	$i++;

	}?>
	<tr>
		<td class="border-full text-left" colspan="5">SUB TOTAL</td>
		<td class="border-full text-right"><?=number($tot_tunai)?></td>
		<td class="border-full text-right"><?=number($tot_non_tunai)?></td>
		<td class="border-full text-right"><?=number($tot_tt)?></td>
		<td class="border-full text-right"></td>
		<td class="border-full text-right"><?=number($tot_neto)?></td>
		<td class="border-full text-right"></td>
	</tr>
    </tbody>
  </table>
  <br/>

  <br/>
  <? $q_dokter="SELECT P.iddokter,mdokter.nama from tkasir H
			INNER JOIN tpoliklinik_tindakan T ON T.id=H.idtindakan
			INNER JOIN tpoliklinik_pendaftaran P ON P.id=T.idpendaftaran
			INNER JOIN mdokter ON mdokter.id=P.iddokter
			WHERE H.idtipe='1' AND H.`status`='2' AND H.edited_by=".$r_user->id." AND (DATE_FORMAT(H.tanggal,'%Y-%m-%d') >= '$tanggaldari' AND DATE_FORMAT(H.tanggal,'%Y-%m-%d') <= '$tanggalsampai')
			GROUP BY P.iddokter";
	$list_dokter=$this->db->query($q_dokter)->result();;
?>


 <label class="text-header"><b>TRANSAKSI RAWAT JALAN (TIPE POLIKLINIK)</b></label>
 <?
  foreach($list_dokter as $r_dokter){
  $q="SELECT T1.iddokter,T1.id,T1.idtindakan,T1.tanggal,T1.no_bukti,T1.no_kasir,T1.nomedrec,T1.nama,GROUP_CONCAT(T1.tag separator '+') as tag
	,SUM(T1.tunai)-T1.kembalian  as tunai,SUM(T1.non_tunai) as non_tunai,SUM(T1.kontraktor) as kontraktor,SUM(T1.t_t) as t_t
	,GROUP_CONCAT(T1.ket_bank) as ket_bank,GROUP_CONCAT(T1.nama_rekanan) as nama_rekanan, GROUP_CONCAT(T1.jaminan) as jaminan
	,GROUP_CONCAT(T1.nama_dokter) as nama_dokter,GROUP_CONCAT(T1.nama_pegawai) as nama_pegawai
	FROM (SELECT H.idtindakan,H.id,H.tanggal,P.nopendaftaran as no_bukti,H.nokasir as no_kasir,P.no_medrec as nomedrec,P.namapasien as nama,P.iddokter,
	CASE
	WHEN B.idmetode='1' THEN 'T'
	WHEN B.idmetode='2' THEN 'DB'
	WHEN B.idmetode='3' THEN 'KR'
	WHEN B.idmetode='4' THEN 'TF'
	WHEN B.idmetode='5' AND B.idpegawai IS NOT NULL THEN 'TP'
	WHEN B.idmetode='5' AND B.iddokter IS NOT NULL THEN 'TD'
	WHEN B.idmetode='6' THEN 'TT'
	WHEN B.idmetode='7' AND B.tipekontraktor='1' THEN 'PA'
	WHEN B.idmetode='7' AND B.tipekontraktor='2' THEN 'JR'
	WHEN B.idmetode='7' AND B.tipekontraktor='3' THEN 'BP.KS'
	WHEN B.idmetode='7' AND B.tipekontraktor='4' THEN 'BP.TK'
	END as tag
	,CASE WHEN B.idmetode='1' THEN B.nominal ELSE 0 END as tunai
	,CASE WHEN B.idmetode IN('2','3','4') THEN B.nominal ELSE 0 END as non_tunai
	,CASE WHEN B.idmetode IN('5','7') THEN B.nominal ELSE 0 END as kontraktor
	,CASE WHEN B.idmetode IN('6') THEN B.nominal ELSE 0 END as t_t,'' as porsi_dokter,
	CONCAT(mbank.noakun,'-',mbank.nama) as ket_bank,mrekanan.nama as nama_rekanan,B.jaminan,mdokter.nama as nama_dokter,mpegawai.nama as nama_pegawai,COALESCE(H.kembalian) as kembalian
	from tkasir H
	INNER JOIN tkasir_pembayaran B ON H.id=B.idkasir
	INNER JOIN tpoliklinik_tindakan T ON T.id=H.idtindakan
	INNER JOIN tpoliklinik_pendaftaran P ON P.id=T.idpendaftaran
	LEFT JOIN mbank ON mbank.id=B.idbank
	LEFT JOIN mrekanan ON mrekanan.id=B.idkontraktor AND B.tipekontraktor='1'
	LEFT JOIN mdokter ON mdokter.id = B.iddokter AND B.idmetode='5' AND B.tipepegawai='2'
	LEFT JOIN mpegawai ON mpegawai.id=B.idpegawai AND B.idmetode='5' AND B.tipepegawai='1'
	WHERE H.idtipe='1' AND H.`status`='2' AND P.iddokter='".$r_dokter->iddokter."' AND H.edited_by=".$r_user->id."  AND DATE_FORMAT(H.tanggal,'%Y-%m-%d')>='$tanggaldari' AND DATE_FORMAT(H.tanggal,'%Y-%m-%d')<='$tanggalsampai' ) T1
	GROUP BY T1.id
	ORDER BY T1.tanggal ASC";
	// print_r($q);exit;
	$rajal=$this->db->query($q)->result();


  ?>
  <table class="content" width="100%">
    <thead>
      <tr>
        <td class=" text-left text-header" colspan="11"><b><?=$r_dokter->nama?></b></td>
      </tr>

	  <tr>
        <td class="border-full text-center" rowspan="2" style="width:3%">NO</td>
        <td class="border-full text-center" rowspan="2" style="width:9%">NO KASIR</td>
        <td class="border-full text-center" rowspan="2" style="width:9%">MEDREC</td>
        <td class="border-full text-center" rowspan="2" style="width:12%">NAMA PASIEN</td>
        <td class="border-full text-center" rowspan="2" style="width:6%">TAG BAYAR</td>
        <td class="border-full text-center" colspan="3" style="width:30%">PEMBAYARAN</td>
        <td class="border-full text-center" rowspan="2" style="width:10%">PORSI DOKTER</td>
        <td class="border-full text-center" rowspan="2" style="width:10%">NETTO</td>
        <td class="border-full text-center" rowspan="2" style="width:10%">KET</td>
      </tr>
	  <tr>
		<td class="border-full text-center" style="width:10%">TUNAI</td>
		<td class="border-full text-center" style="width:10%">NON TUNAI</td>
		<td class="border-full text-center" style="width:10%">T TERTAGIH</td>

	  </tr>
    </thead>
    <tbody>
	<? $i=1; $tot_tunai=0;$tot_non_tunai=0;$tot_tt=0;$tot_neto=0;$tot_porsi=0;
	foreach($rajal as $r){
	$neto=0;
	$ket='';
	$tot_tunai +=$r->tunai;
	$tot_non_tunai +=$r->non_tunai+$r->kontraktor;
	$tot_tt +=$r->t_t;

	$tot_user_tunai +=$r->tunai;
	$tot_user_bank +=$r->non_tunai;
	$tot_user_kontraktor +=$r->kontraktor;
	$tot_user_tt +=$r->t_t;

	$ket.=($r->ket_bank?$r->ket_bank:'');
	$ket.=($r->nama_rekanan !='' && $ket!=''?',':'').($r->nama_rekanan?$r->nama_rekanan:'');
	$ket.=($r->jaminan !=',' && $ket!=''?',':'').($r->jaminan !=','?$r->jaminan:'');
	$ket.=($r->nama_dokter !='' && $ket!=''?',':'').($r->nama_dokter?$r->nama_dokter:'');
	$ket.=($r->nama_pegawai !='' && $ket!=''?',':'').($r->nama_pegawai?$r->nama_pegawai:'');

	$q_porsi="SELECT H.jasapelayanan,H.jasapelayanan_disc,H.pajak_dokter,H.pot_rs from tpoliklinik_pelayanan H
		WHERE H.idtindakan='".$r->idtindakan."'";
		$list_porsi=$this->db->query($q_porsi)->result();
		$var_porsi=0;$var_nilai_pajak=0;$var_nilai_pot=0;
		$var_jasa=0;
		foreach($list_porsi as $r_porsi){
			$var_jasa=($r_porsi->jasapelayanan)-($r_porsi->jasapelayanan_disc);
			$var_nilai_pajak=$var_jasa * ($r_porsi->pajak_dokter/100);
			$var_nilai_pot=$var_jasa * ($r_porsi->pot_rs/100);
			$var_porsi = $var_porsi + ($var_jasa-$var_nilai_pajak-$var_nilai_pot);
		}
		$tot_porsi +=$var_porsi;
		$neto=$r->tunai+$r->non_tunai+$r->kontraktor+$r->t_t - $var_porsi;
		$tot_neto +=$neto;
	?>
	<tr>
     <td class="border-full text-center"><?=$i?></td>
     <td class="border-full text-center"><?=$r->no_kasir?></td>
     <td class="border-full text-center"><?=$r->nomedrec?></td>
     <td class="border-full text-left"><?=$r->nama?></td>
     <td class="border-full text-center"><?=$r->tag?></td>
     <td class="border-full text-right"><?=number($r->tunai)?></td>
     <td class="border-full text-right"><?=number($r->non_tunai+$r->kontraktor)?></td>
     <td class="border-full text-right"><?=number($r->t_t)?></td>
     <td class="border-full text-right"><?=number($var_porsi)?></td>
     <td class="border-full text-right"><?=number($neto)?></td>
     <td class="border-full text-left"><?=$ket?></td>
	 </tr>
	<?
	$i++;

	}?>
	<tr>
		<td class="border-full text-left" colspan="5">SUB TOTAL</td>
		<td class="border-full text-right"><?=number($tot_tunai)?></td>
		<td class="border-full text-right"><?=number($tot_non_tunai)?></td>
		<td class="border-full text-right"><?=number($tot_tt)?></td>
		<td class="border-full text-right"><?=number($tot_porsi)?></td>
		<td class="border-full text-right"><?=number($tot_neto)?></td>
		<td class="border-full text-right"></td>
	</tr>
    </tbody>
  </table>
  <br/>
  <?}?>
  <br>
   <? $q_dokter="SELECT P.iddokter,mdokter.nama from tkasir H
			INNER JOIN tpoliklinik_tindakan T ON T.id=H.idtindakan
			INNER JOIN tpoliklinik_pendaftaran P ON P.id=T.idpendaftaran
			INNER JOIN mdokter ON mdokter.id=P.iddokter
			WHERE H.idtipe='2' AND H.edited_by=".$r_user->id." AND H.`status`='2' AND (DATE_FORMAT(H.tanggal,'%Y-%m-%d') >= '$tanggaldari' AND DATE_FORMAT(H.tanggal,'%Y-%m-%d') <= '$tanggalsampai')
			GROUP BY P.iddokter";
	$list_dokter=$this->db->query($q_dokter)->result();;
?>
  <label class="text-header"><b>TRANSAKSI IGD (TIPE IGD)</b></label>
 <?
  foreach($list_dokter as $r_dokter){
  $q="SELECT T1.iddokter,T1.id,T1.idtindakan,T1.tanggal,T1.no_bukti,T1.no_kasir,T1.nomedrec,T1.nama,GROUP_CONCAT(T1.tag separator '+') as tag
	,SUM(T1.tunai)-T1.kembalian as tunai,SUM(T1.non_tunai) as non_tunai,SUM(T1.kontraktor) as kontraktor,SUM(T1.t_t) as t_t
	,GROUP_CONCAT(T1.ket_bank) as ket_bank,GROUP_CONCAT(T1.nama_rekanan) as nama_rekanan, GROUP_CONCAT(T1.jaminan) as jaminan
	,GROUP_CONCAT(T1.nama_dokter) as nama_dokter,GROUP_CONCAT(T1.nama_pegawai) as nama_pegawai
	FROM (SELECT H.idtindakan,H.id,H.tanggal,P.nopendaftaran as no_bukti,H.nokasir as no_kasir,P.no_medrec as nomedrec,P.namapasien as nama,P.iddokter,
	CASE
	WHEN B.idmetode='1' THEN 'T'
	WHEN B.idmetode='2' THEN 'DB'
	WHEN B.idmetode='3' THEN 'KR'
	WHEN B.idmetode='4' THEN 'TF'
	WHEN B.idmetode='5' AND B.idpegawai IS NOT NULL THEN 'TP'
	WHEN B.idmetode='5' AND B.iddokter IS NOT NULL THEN 'TD'
	WHEN B.idmetode='6' THEN 'TT'
	WHEN B.idmetode='7' AND B.tipekontraktor='1' THEN 'PA'
	WHEN B.idmetode='7' AND B.tipekontraktor='2' THEN 'JR'
	WHEN B.idmetode='7' AND B.tipekontraktor='3' THEN 'BP.KS'
	WHEN B.idmetode='7' AND B.tipekontraktor='4' THEN 'BP.TK'
	END as tag
	,CASE WHEN B.idmetode='1' THEN B.nominal ELSE 0 END as tunai
	,CASE WHEN B.idmetode IN('2','3','4') THEN B.nominal ELSE 0 END as non_tunai
	,CASE WHEN B.idmetode IN('5','7') THEN B.nominal ELSE 0 END as kontraktor
	,CASE WHEN B.idmetode IN('6') THEN B.nominal ELSE 0 END as t_t,'' as porsi_dokter,
	CONCAT(mbank.noakun,'-',mbank.nama) as ket_bank,mrekanan.nama as nama_rekanan,B.jaminan,mdokter.nama as nama_dokter,mpegawai.nama as nama_pegawai,COALESCE(H.kembalian) as kembalian
	from tkasir H
	INNER JOIN tkasir_pembayaran B ON H.id=B.idkasir
	INNER JOIN tpoliklinik_tindakan T ON T.id=H.idtindakan
	INNER JOIN tpoliklinik_pendaftaran P ON P.id=T.idpendaftaran
	LEFT JOIN mbank ON mbank.id=B.idbank
	LEFT JOIN mrekanan ON mrekanan.id=B.idkontraktor AND B.tipekontraktor='1'
	LEFT JOIN mdokter ON mdokter.id = B.iddokter AND B.idmetode='5' AND B.tipepegawai='2'
	LEFT JOIN mpegawai ON mpegawai.id=B.idpegawai AND B.idmetode='5' AND B.tipepegawai='1'
	WHERE H.idtipe='2' AND H.`status`='2' AND P.iddokter='".$r_dokter->iddokter."' AND H.edited_by=".$r_user->id."  AND DATE_FORMAT(H.tanggal,'%Y-%m-%d')>='$tanggaldari' AND DATE_FORMAT(H.tanggal,'%Y-%m-%d')<='$tanggalsampai' ) T1
	GROUP BY T1.id
	ORDER BY T1.tanggal ASC";
	$rajal=$this->db->query($q)->result();


  ?>
  <table class="content" width="100%">
    <thead>
      <tr>
        <td class=" text-left text-header" colspan="11"><b><?=$r_dokter->nama?></b></td>
      </tr>

	  <tr>
        <td class="border-full text-center" rowspan="2" style="width:3%">NO</td>
        <td class="border-full text-center" rowspan="2" style="width:9%">NO KASIR</td>
        <td class="border-full text-center" rowspan="2" style="width:9%">MEDREC</td>
        <td class="border-full text-center" rowspan="2" style="width:12%">NAMA PASIEN</td>
        <td class="border-full text-center" rowspan="2" style="width:6%">TAG BAYAR</td>
        <td class="border-full text-center" colspan="3" style="width:30%">PEMBAYARAN</td>
        <td class="border-full text-center" rowspan="2" style="width:10%">PORSI DOKTER</td>
        <td class="border-full text-center" rowspan="2" style="width:10%">NETTO</td>
        <td class="border-full text-center" rowspan="2" style="width:10%">KET</td>
      </tr>
	  <tr>
		<td class="border-full text-center" style="width:10%">TUNAI</td>
		<td class="border-full text-center" style="width:10%">NON TUNAI</td>
		<td class="border-full text-center" style="width:10%">T TERTAGIH</td>

	  </tr>
    </thead>
    <tbody>
	<? $i=1; $tot_tunai=0;$tot_non_tunai=0;$tot_tt=0;$tot_neto=0;$tot_porsi=0;
	foreach($rajal as $r){
	$neto=0;
	$ket='';
	$tot_tunai +=$r->tunai;
	$tot_non_tunai +=$r->non_tunai+$r->kontraktor;
	$tot_tt +=$r->t_t;

	$tot_user_tunai +=$r->tunai;
	$tot_user_bank +=$r->non_tunai;
	$tot_user_kontraktor +=$r->kontraktor;
	$tot_user_tt +=$r->t_t;

	$ket.=($r->ket_bank?$r->ket_bank:'');
	$ket.=($r->nama_rekanan !='' && $ket!=''?',':'').($r->nama_rekanan?$r->nama_rekanan:'');
	$ket.=($r->jaminan !=',' && $ket!=''?',':'').($r->jaminan !=','?$r->jaminan:'');
	$ket.=($r->nama_dokter !='' && $ket!=''?',':'').($r->nama_dokter?$r->nama_dokter:'');
	$ket.=($r->nama_pegawai !='' && $ket!=''?',':'').($r->nama_pegawai?$r->nama_pegawai:'');

	$q_porsi="SELECT H.jasapelayanan,H.jasapelayanan_disc,H.pajak_dokter,H.pot_rs from tpoliklinik_pelayanan H
		WHERE H.idtindakan='".$r->idtindakan."'";
		$list_porsi=$this->db->query($q_porsi)->result();
		$var_porsi=0;$var_nilai_pajak=0;$var_nilai_pot=0;
		$var_jasa=0;
		foreach($list_porsi as $r_porsi){
			$var_jasa=($r_porsi->jasapelayanan)-($r_porsi->jasapelayanan_disc);
			$var_nilai_pajak=$var_jasa * ($r_porsi->pajak_dokter/100);
			$var_nilai_pot=$var_jasa * ($r_porsi->pot_rs/100);
			$var_porsi = $var_porsi + ($var_jasa-$var_nilai_pajak-$var_nilai_pot);
		}
		$tot_porsi +=$var_porsi;
		$neto=$r->tunai+$r->non_tunai+$r->kontraktor+$r->t_t - $var_porsi;
		$tot_neto +=$neto;
	?>
	<tr>
     <td class="border-full text-center"><?=$i?></td>
     <td class="border-full text-center"><?=$r->no_kasir?></td>
     <td class="border-full text-center"><?=$r->nomedrec?></td>
     <td class="border-full text-left"><?=$r->nama?></td>
     <td class="border-full text-center"><?=$r->tag?></td>
     <td class="border-full text-right"><?=number($r->tunai)?></td>
     <td class="border-full text-right"><?=number($r->non_tunai+$r->kontraktor)?></td>
     <td class="border-full text-right"><?=number($r->t_t)?></td>
     <td class="border-full text-right"><?=number($var_porsi)?></td>
     <td class="border-full text-right"><?=number($neto)?></td>
     <td class="border-full text-left"><?=$ket?></td>
	 </tr>
	<?
	$i++;

	}?>
	<tr>
		<td class="border-full text-left" colspan="5">SUB TOTAL</td>
		<td class="border-full text-right"><?=number($tot_tunai)?></td>
		<td class="border-full text-right"><?=number($tot_non_tunai)?></td>
		<td class="border-full text-right"><?=number($tot_tt)?></td>
		<td class="border-full text-right"><?=number($tot_porsi)?></td>
		<td class="border-full text-right"><?=number($tot_neto)?></td>
		<td class="border-full text-right"></td>
	</tr>
    </tbody>
  </table>
  <br/>
  
  <?}?>
  <br>
  <label class="text-header"><b>TRANSAKSI REFUND</b></label>
  <table class="content" width="100%">
    <thead>
      
	  <tr>
        <td class="border-full text-center" rowspan="2" style="width:3%">NO</td>
        <td class="border-full text-center" rowspan="2" style="width:9%">NO KASIR</td>
        <td class="border-full text-center" rowspan="2" style="width:9%">MEDREC</td>
        <td class="border-full text-center" rowspan="2" style="width:12%">NAMA PASIEN</td>
        <td class="border-full text-center" rowspan="2" style="width:6%">TAG BAYAR</td>
        <td class="border-full text-center" colspan="4" style="width:30%">PEMBAYARAN</td>
        <td class="border-full text-center" rowspan="2" style="width:10%">TOTAL REFUND</td>
        <td class="border-full text-center" rowspan="2" style="width:10%">KET</td>
      </tr>
	  <tr>
		<td class="border-full text-center" style="width:10%">TUNAI</td>
		<td class="border-full text-center" style="width:10%">NON TUNAI</td>
		<td class="border-full text-center" style="width:10%">T TERTAGIH</td>
		<td class="border-full text-center" style="width:10%">KONTRAKTOR</td>

	  </tr>
    </thead>
    <tbody>
	<?
	$q_refund="SELECT H.norefund,H.notransaksi,M.no_medrec as nomedrec,M.title,M.nama,
CASE 
WHEN H.metode='1' THEN 'T'
WHEN H.metode='2' THEN 'DB'
WHEN H.metode='3' THEN 'KR'
WHEN H.metode='4' THEN 'TF' END as tag,
CASE WHEN H.metode='1' THEN H.totalrefund ELSE 0 END tunai,
CASE WHEN H.metode IN (2,3,4) THEN H.totalrefund ELSE 0 END non_tunai,H.alasan

from trefund H
LEFT JOIN mfpasien M ON M.id=idpasien
WHERE DATE_FORMAT(H.tanggal,'%Y-%m-%d') >='$tanggaldari' AND DATE_FORMAT(H.tanggal,'%Y-%m-%d') <='$tanggalsampai'
AND (H.tipe='1' OR tipetransaksi='rawatjalan') AND H.created_by='".$r_user->id."'";
// print_r($q_refund);exit();
$list_refund=$this->db->query($q_refund)->result();
	?>
	<? 
	$tot_ref_tunai=0;
	$tot_ref_nt=0;
	foreach($list_refund as $rf){
	
	?>
		<tr>
		 <td class="border-full text-center"><?=$i?></td>
		 <td class="border-full text-center"><?=$rf->norefund?></td>
		 <td class="border-full text-center"><?=$rf->nomedrec?></td>
		 <td class="border-full text-left"><?=$rf->nama?></td>
		 <td class="border-full text-center"><?=$rf->tag?></td>
		 <td class="border-full text-right"><?=number($rf->tunai)?></td>
		 <td class="border-full text-right"><?=number($rf->non_tunai)?></td>
		 <td class="border-full text-right"><?=number(0)?></td>
		 <td class="border-full text-right"><?=number(0)?></td>
		 <td class="border-full text-right"><?=number($rf->tunai+$rf->non_tunai)?></td>
		 <td class="border-full text-left"><?=$rf->alasan?></td>
		 </tr>
	<?
	$tot_ref_tunai +=$rf->tunai;
	$tot_ref_nt +=$rf->non_tunai;
	$i++;

	}?>
	<tr>
		<td class="border-full text-left" colspan="5">SUB TOTAL</td>
		<td class="border-full text-right"><?=number($tot_ref_tunai)?></td>
		<td class="border-full text-right"><?=number($tot_ref_nt)?></td>
		<td class="border-full text-right"><?=number(0)?></td>
		<td class="border-full text-right"><?=number(0)?></td>
		<td class="border-full text-right"><?=number($tot_ref_tunai + $tot_ref_nt)?></td>
		<td class="border-full text-right"></td>
	</tr>
    </tbody>
  </table>
  <br/>
  </fieldset>
  <br>
  <br>
  <fieldset style="border:1px solid #000 !important;border-radius:0px;">
	<legend style="font-weight:bold;"></legend>
  <label class="text-header"><b>REKAPITULASI PENDAPATAN USER : <?=strtoupper($r_user->name)?></b></label>
  <div style="clear:both; position:relative;height:auto;">
		<div style="position:absolute; left:1pt; width:275pt;">
		  <label class="text-header">DETAIL PENDAPATAN</label>
		  <table class="content" width="50%">
			<thead>

			  <tr>
				<td class="border-full text-center"  style="width:10%">NO</td>
				<td class="border-full text-center"  style="width:50%">DESKRIPSI</td>
				<td class="border-full text-center"  style="width:40%">NOMINAL</td>
			  </tr>

			</thead>
			<tbody>
				<tr>
					<td class="border-full text-center">1</td>
					<td class="border-full text-left">TUNAI</td>
					<td class="border-full text-right"><?=number($tot_user_tunai)?></td>
				</tr>
				<tr>
					<td class="border-full text-center"  style="width:20%">2</td>
					<td class="border-full text-left">BANK</td>
					<td class="border-full text-right"><?=number($tot_user_bank)?></td>
				</tr>
				<tr>
					<td class="border-full text-center"  style="width:20%">3</td>
					<td class="border-full text-left">KONTRAKTOR</td>
					<td class="border-full text-right"><?=number($tot_user_kontraktor)?></td>
				</tr>
				<tr>
					<td class="border-full text-center"  style="width:20%">4</td>
					<td class="border-full text-left">KW MANUAL</td>
					<td class="border-full text-right">0</td>
				</tr>
				<tr>
					<td class="border-full text-center"  style="width:20%">5</td>
					<td class="border-full text-left">T TERTAGIH</td>
					<td class="border-full text-right"><?=number($tot_user_tt)?></td>
				</tr>
				<tr>
					<td class="border-full text-center" colspan="2">TOTAL PENDAPATAN</td>
					<td class="border-full text-right"><?=number($tot_user_tt+$tot_user_kontraktor + $tot_user_bank + $tot_user_tunai)?></td>
				</tr>
			</tbody>
		   </table>
		</div>
		<div style="margin-left:300pt;">
		<label class="text-header">DETAIL PENDAPATAN NON TUNAI</label>
		<?
			$q_bank="SELECT B.idbank,mbank.nama
			,SUM(CASE WHEN B.idmetode='2' THEN B.nominal ELSE 0 END) as debet
			,SUM(CASE WHEN B.idmetode='3' THEN B.nominal ELSE 0 END) as kredit
			,SUM(CASE WHEN B.idmetode='4' THEN B.nominal ELSE 0 END) as tf

			from tkasir H
			INNER JOIN tkasir_pembayaran B ON B.idkasir=H.id
			LEFT JOIN mbank ON mbank.id=B.idbank
			WHERE H.edited_by='".$r_user->id."' AND DATE_FORMAT(H.tanggal,'%Y-%m-%d')>='$tanggaldari' AND DATE_FORMAT(H.tanggal,'%Y-%m-%d')<='$tanggalsampai'
			AND B.idmetode IN (2,3,4)
			GROUP BY B.idbank";
			$list_bank=$this->db->query($q_bank)->result();
			$no_bank=0;
		?>
		<table class="content" width="50%">
			<thead>

			  <tr>
				<td class="border-full text-center"  style="width:30%">NAMA BANK</td>
				<td class="border-full text-center"  style="width:22%">DEBIT</td>
				<td class="border-full text-center"  style="width:22%">KREDIT</td>
				<td class="border-full text-center"  style="width:22%">TRANSFER</td>
			  </tr>

			</thead>
			<tbody>
				<?
				$tot_debet=0;$tot_kredit=0;$tot_tf=0;
				foreach($list_bank as $r_bank){
				$no_bank +=1;
				$tot_debet +=$r_bank->debet;
				$tot_kredit +=$r_bank->kredit;
				$tot_tf +=$r_bank->tf;
				?>
				<tr>
					<td class="border-full text-left"><?=$r_bank->nama?></td>
					<td class="border-full text-right"><?=number($r_bank->debet)?></td>
					<td class="border-full text-right"><?=number($r_bank->kredit)?></td>
					<td class="border-full text-right"><?=number($r_bank->tf)?></td>
			    </tr>

				<?}?>
				<?for($x=$no_bank;$x<5;$x++){ ?>
				  <tr>
					<td class="border-full text-center"  style="width:30%">&nbsp;</td>
					<td class="border-full text-center"  style="width:22%">&nbsp;</td>
					<td class="border-full text-center"  style="width:22%">&nbsp;</td>
					<td class="border-full text-center"  style="width:22%">&nbsp;</td>
				  </tr>
				<?}?>
				<tr>
					<td class="border-full text-center"  style="width:30%">TOTAL</td>
					<td class="border-full text-right"  style="width:22%"><?=number($tot_debet)?></td>
					<td class="border-full text-right"  style="width:22%"><?=number($tot_kredit)?></td>
					<td class="border-full text-right"  style="width:22%"><?=number($tot_tf)?></td>
				  </tr>
			</tbody>
		   </table>
		  </div>
	</div>

	<div style="clear:both; position:relative;height:auto;">
		<div style="position:absolute; left:1pt; width:275pt;">
		  <label class="text-header">SETORAN TUNAI</label>
		  <table class="content" width="50%">
			<thead>

			  <tr>
				<td class="border-full text-center"  style="width:10%">NO</td>
				<td class="border-full text-center"  style="width:50%">DESKRIPSI</td>
				<td class="border-full text-center"  style="width:40%">NOMINAL</td>
			  </tr>

			</thead>
			<tbody>
				<tr>
					<td class="border-full text-center">1</td>
					<td class="border-full text-left">TUNAI</td>
					<td class="border-full text-right"><?=number($tot_user_tunai)?></td>
				</tr>
				<tr>
					<td class="border-full text-center"  style="width:20%">2</td>
					<td class="border-full text-left">KW MANUAL</td>
					<td class="border-full text-right"><?=number(0)?></td>
				</tr>
				<tr>
					<td class="border-full text-center"  style="width:20%">3</td>
					<td class="border-full text-left">REFUND</td>
					<td class="border-full text-right"><?=number($tot_ref_tunai)?></td>
				</tr>

				<tr>
					<td class="border-full text-center" colspan="2">SETORAN TUNAI</td>
					<td class="border-full text-right"><?=number($tot_user_tunai-$tot_ref_tunai)?></td>
				</tr>
			</tbody>
		   </table>
		</div>
		<div style="margin-left:300pt;">
		<label class="text-header">DETAIL PENDAPATAN KONTRAKTOR</label>
		<?
			$q_kon="SELECT H.edited_by
				,SUM(CASE WHEN B.idmetode='5' AND B.tipepegawai='1' THEN B.nominal ELSE 0 END) as tag_pegawai
				,SUM(CASE WHEN B.idmetode='5' AND B.tipepegawai='2' THEN B.nominal ELSE 0 END) as tag_dokter
				,SUM(CASE WHEN B.idmetode='7' AND (B.tipekontraktor='1' OR B.tipekontraktor='2') THEN B.nominal ELSE 0 END) as tag_asuransi
				,SUM(CASE WHEN B.idmetode='7' AND (B.tipekontraktor='3') THEN B.nominal ELSE 0 END) as tag_bp_ks
				,SUM(CASE WHEN B.idmetode='7' AND (B.tipekontraktor='4') THEN B.nominal ELSE 0 END) as tag_bp_tk

				from tkasir H
				INNER JOIN tkasir_pembayaran B ON B.idkasir=H.id
				LEFT JOIN mbank ON mbank.id=B.idbank
				WHERE H.edited_by='".$r_user->id."' AND DATE_FORMAT(H.tanggal,'%Y-%m-%d')>='$tanggaldari' AND DATE_FORMAT(H.tanggal,'%Y-%m-%d')<='$tanggalsampai'
				AND B.idmetode IN (5,7)
				GROUP BY H.edited_by";
			$row_kont=$this->db->query($q_kon)->row();
			$no_bank=0;
		?>
		<table class="content" width="50%">
			<thead>

			  <tr>
				<td class="border-full text-center"  style="width:50%">KETERANGAN</td>
				<td class="border-full text-center"  style="width:52%">KREDIT</td>
			  </tr>

			</thead>
			<tbody>
				<?
				if ($row_kont){
					$tag_asuransi=$row_kont->tag_asuransi;
					$tag_bp_ks=$row_kont->tag_bp_ks;
					$tag_bp_tk=$row_kont->tag_bp_tk;
					$tag_pegawai=$row_kont->tag_pegawai;
					$tag_dokter=$row_kont->tag_dokter;
				}else{
					$tag_asuransi=0;
					$tag_bp_ks=0;
					$tag_bp_tk=0;
					$tag_pegawai=0;
					$tag_dokter=0;
				}
				?>
				<tr>
					<td class="border-full text-left">ASURANSI</td>
					<td class="border-full text-right"><?=number($tag_asuransi)?></td>
			    </tr>
				<tr>
					<td class="border-full text-left">BPJS TK</td>
					<td class="border-full text-right"><?=number($tag_bp_tk)?></td>
			    </tr>
				<tr>
					<td class="border-full text-left">BPJS KESEHATAN</td>
					<td class="border-full text-right"><?=number($tag_bp_ks)?></td>
			    </tr>
				<tr>
					<td class="border-full text-left">T. KARYAWAN</td>
					<td class="border-full text-right"><?=number($tag_pegawai)?></td>
			    </tr>
				<tr>
					<td class="border-full text-left">T. DOKTERR</td>
					<td class="border-full text-right"><?=number($tag_dokter)?></td>
			    </tr>

				<tr>
					<td class="border-full text-center"  style="width:30%">TOTAL</td>
					<td class="border-full text-right"  style="width:22%"><?=number($tag_asuransi +$tag_bp_tk + $tag_bp_ks + $tag_pegawai + $tag_dokter)?></td>
				  </tr>
			</tbody>
		   </table>
		  </div>
	</div>
	</fieldset>
  <?}//LOOPING USER?>
</body>

</html>
