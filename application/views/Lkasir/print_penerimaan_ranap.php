<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Laporan Penerimaan Kasir Rawat Inap</title>
	<style type="text/css" media="all">
		body {
			-webkit-print-color-adjust: exact;
		}

		@page {
			margin-top: 1, 8em;
			margin-left: 0.8em;
			margin-right: 1em;
			margin-bottom: 0,5em;
		}

		@media screen {
			table {
				font-family: "Courier", Arial, sans-serif;
				font-size: 18px !important;
				border-collapse: collapse !important;
				width: 100% !important;
				table-layout: fixed;
				overflow: hidden;
				word-wrap: break-word;
			}

			th {
				padding: 2px;

			}

			td {
				padding: 3px;

			}

			.content td {
				padding: 2px;
				border: 0px solid #6033FF;
			}

			.text-muted {
				font-size: 12px !important;
			}

			/* border-normal */
			.border-full {
				border: 1px solid #000 !important;

			}

			.text-header {
				font-family: "Courier", Arial, sans-serif;
				font-size: 20px !important;
			}

			.text-judul {
				font-size: 28px !important;
			}

			.border-bottom {
				border-bottom: 1px solid #000 !important;
			}

			.border-bottom-left {
				border-bottom: 1px solid #000 !important;
				border-left: 1px solid #000 !important;
			}

			.border-bottom-right {
				border-bottom: 1px solid #000 !important;
				border-right: 1px solid #000 !important;
			}

			.border-bottom-top {
				border-bottom: 1px solid #000 !important;
				border-top: 1px solid #000 !important;
			}

			.border-full {
				border-bottom: 1px solid #000 !important;
				border-top: 1px solid #000 !important;
				border-left: 1px solid #000 !important;
			}

			.border-bottom-top-right {
				border-bottom: 1px solid #000 !important;
				border-top: 1px solid #000 !important;
				border-right: 1px solid #000 !important;
			}

			.border-left {
				border-left: 1px solid #000 !important;
			}

			/* border-thick */
			.border-thick-top {
				border-top: 2px solid #000 !important;
			}

			.border-thick-bottom {
				border-bottom: 2px solid #000 !important;
			}

			.border-dotted {
				border-width: 1px;
				border-bottom-style: dotted;
			}

			/* text-position */
			.text-center {
				text-align: center !important;
			}

			.text-left {
				text-align: left !important;
			}

			.text-right {
				text-align: right !important;
			}

			/* text-style */
			.text-italic {
				font-style: italic;
			}

			.text-bold {
				font-weight: bold;
			}

			.text-top {
				font-size: 14px !important;
			}

			br {
				display: block;
				margin: 10px 0;
			}
		}
	</style>
</head>

<body>
	<table class="content">
		<tr>
			<td rowspan="2" width="20%" class="text-center"><img src="{logo1_rs}" alt="" width="100" height="100"></td>
			<td width="60%" class="text-center text-judul"><u><b>LAPORAN RAWAT INAP</b></u></td>
			<td width="20%"></td>
		</tr>
		<tr>
			<td class="text-center">PERIODE : <?=$nama_periode?></td>
			<td></td>
		</tr>
	</table>
	<br />
	<br />

	<?php foreach ($user_list as $user) { ?>
	<?php $total_user_tunai = 0; ?>
	<?php $total_user_bank = 0; ?>
	<?php $total_user_kontraktor = 0; ?>
	<?php $total_user_kwitansi_manual = 0; ?>
	<?php $total_user_refund = 0; ?>
	<?php $total_user_tidaktertagih = 0; ?>
	<fieldset style="border:1px solid #000 !important;border-radius:0px;">
		<legend style="font-weight:bold;"></legend>
		<table class="content" width="100%">
			<thead>
				<tr>
					<td class=" text-left text-header" colspan="15"><b>USER : <?=strtoupper($user->name)?></b></td>
				</tr>
				<tr>
					<td class=" text-left text-header" colspan="15"><b>PASIEN PULANG</b></td>
				</tr>
				<tr>
					<td class="border-full text-center" rowspan="2" style="width:3%">NO</td>
					<td class="border-full text-center" rowspan="2" style="width:9%">TANGGAL</td>
					<td class="border-full text-center" rowspan="2" style="width:9%">NO KASIR</td>
					<td class="border-full text-center" rowspan="2" style="width:9%">MEDREC</td>
					<td class="border-full text-center" rowspan="2" style="width:12%">NAMA PASIEN</td>
					<td class="border-full text-center" rowspan="2" style="width:6%">TAG KUNJ</td>
					<td class="border-full text-center" rowspan="2" style="width:6%">TAG BAYAR</td>
					<td class="border-full text-center" rowspan="2" colspan="3" style="width:30%">INFORMASI DEPOSIT</td>
					<td class="border-full text-center" colspan="4" style="width:10%">PELUNASAN</td>
					<td class="border-full text-center" rowspan="2" style="width:10%">TOTAL PELUNASAN</td>
					<td class="border-full text-center" rowspan="2" style="width:10%">TOTAL TAGIHAN</td>
					<td class="border-full text-center" rowspan="2" style="width:10%">KET</td>
				</tr>
				<tr>
					<td class="border-full text-center" style="width:10%">TUNAI</td>
					<td class="border-full text-center" style="width:10%">NON TUNAI</td>
					<td class="border-full text-center" style="width:10%">KONTRAKTOR</td>
					<td class="border-full text-center" style="width:10%">TIDAK TERTAGIH</td>
				</tr>
			</thead>
			<tbody>
				<?php $number = 1; ?>
				<?php 
				
				// foreach (get_all('view_laporan_kasir_rawat_inap', array('tanggal >=' => $tanggaldari, 'tanggal <=' => $tanggalsampai, 'iduser_input' => $user->id)) as $row) { 
				$q="SELECT
	`trawatinap_pendaftaran`.`id` AS `idrawatinap`,
	cast( `trawatinap_tindakan_pembayaran`.`tanggal` AS date ) AS `tanggal`,
	`trawatinap_pendaftaran`.`nopendaftaran` AS `nopendaftaran`,
	`trawatinap_pendaftaran`.`no_medrec` AS `nomedrec`,
	`trawatinap_pendaftaran`.`namapasien` AS `namapasien`,
IF
	( ( `trawatinap_pendaftaran`.`idtipe` = 1 ), 'RI', 'ODS' ) AS `tag_kunjungan`,
	group_concat( `GetMetodePembayaran` ( `trawatinap_tindakan_pembayaran_detail`.`idmetode`, `trawatinap_tindakan_pembayaran_detail`.`tipekontraktor` ) SEPARATOR ' + ' ) AS `tag_bayar`,
	sum( ( CASE WHEN ( `trawatinap_tindakan_pembayaran_detail`.`idmetode` = 1 ) THEN `trawatinap_tindakan_pembayaran_detail`.`nominal` END ) ) AS `pelunasan_tunai`,
	sum(
		(
		CASE
				
				WHEN ( ( `trawatinap_tindakan_pembayaran_detail`.`idmetode` = 2 ) OR ( `trawatinap_tindakan_pembayaran_detail`.`idmetode` = 3 ) OR ( `trawatinap_tindakan_pembayaran_detail`.`idmetode` = 4 ) ) THEN
				`trawatinap_tindakan_pembayaran_detail`.`nominal` 
			END 
			) 
		) AS `pelunasan_nontunai`,
		sum(
			(
			CASE
					
					WHEN ( ( `trawatinap_tindakan_pembayaran_detail`.`idmetode` = 5 ) OR ( `trawatinap_tindakan_pembayaran_detail`.`idmetode` = 6 ) OR ( `trawatinap_tindakan_pembayaran_detail`.`idmetode` = 8 ) ) THEN
					`trawatinap_tindakan_pembayaran_detail`.`nominal` 
				END 
				) 
			) AS `pelunasan_kontraktor`,
			sum( ( CASE WHEN ( `trawatinap_tindakan_pembayaran_detail`.`idmetode` = 7 ) THEN `trawatinap_tindakan_pembayaran_detail`.`nominal` END ) ) AS `pelunasan_tidaktertagih`,
			`trawatinap_tindakan_pembayaran`.`total` AS `total_tagihan`,
			(
			CASE
					
					WHEN ( ( `trawatinap_tindakan_pembayaran_detail`.`idmetode` = 2 ) OR ( `trawatinap_tindakan_pembayaran_detail`.`idmetode` = 4 ) ) THEN
					concat( CONVERT ( `trawatinap_tindakan_pembayaran_detail`.`tracenumber` USING utf8 ), ' - ', `mbank`.`nama` ) ELSE '' 
				END 
				) AS `keterangan`,
				`trawatinap_tindakan_pembayaran`.`iduser_input` AS `iduser_input` 
			FROM
				(
					(
						(
							`trawatinap_pendaftaran`
							LEFT JOIN `trawatinap_tindakan_pembayaran` ON ( ( ( `trawatinap_tindakan_pembayaran`.`idtindakan` = `trawatinap_pendaftaran`.`id` ) AND ( `trawatinap_tindakan_pembayaran`.`statusbatal` = 0 ) ) ) 
						)
						LEFT JOIN `trawatinap_tindakan_pembayaran_detail` ON ( ( `trawatinap_tindakan_pembayaran_detail`.`idtindakan` = `trawatinap_tindakan_pembayaran`.`id` ) ) 
					)
					LEFT JOIN `mbank` ON ( ( `mbank`.`id` = `trawatinap_tindakan_pembayaran_detail`.`idbank` ) ) 
				) 
			WHERE
				( `trawatinap_pendaftaran`.`statuspembayaran` = 1 )  
				AND DATE(trawatinap_tindakan_pembayaran.tanggal)='$tanggaldari' AND DATE(trawatinap_tindakan_pembayaran.tanggal)='$tanggalsampai' 
				AND 	trawatinap_tindakan_pembayaran.iduser_input ='".$user->id."'
		GROUP BY
	`trawatinap_pendaftaran`.`id`
	";
$hasil=$this->db->query($q)->result();
				// foreach (get_all('view_laporan_kasir_rawat_inap', array('tanggal >=' => $tanggaldari, 'tanggal <=' => $tanggalsampai, 'iduser_input' => $user->id)) as $row) { 
				foreach($hasil as $row){
				
				
				?>
				<?php $listDeposit = get_all('view_laporan_kasir_rawat_inap_deposit', array('idrawatinap' => $row->idrawatinap, 'iduser_input' => $user->id)); ?>
				<?php $countListDeposit = COUNT($listDeposit); ?>
				<?php $countDeposit = 0; ?>
				<?php $strDeposit = ''; ?>
				<tr>
					<td class="border-full text-center" rowspan="<?=$countListDeposit?>"><?=$number++?></td>
					<td class="border-full text-center" rowspan="<?=$countListDeposit?>"><?=DMYFormat($row->tanggal)?></td>
					<td class="border-full text-center" rowspan="<?=$countListDeposit?>"><?=$row->idrawatinap?> - <?=$row->nopendaftaran?></td>
					<td class="border-full text-center" rowspan="<?=$countListDeposit?>"><?=$row->nomedrec?></td>
					<td class="border-full text-center" rowspan="<?=$countListDeposit?>"><?=$row->namapasien?></td>
					<td class="border-full text-center" rowspan="<?=$countListDeposit?>"><?=($row->tag_kunjungan)?></td>
					<td class="border-full text-center" rowspan="<?=$countListDeposit?>"><?=($row->tag_bayar)?></td>

					<?php if ($countListDeposit > 0) { ?>
						<?php foreach ($listDeposit as $deposit) { ?>
							<?php if ($countDeposit > 0) { ?>
									<?php $strDeposit.= '<tr><td class="border-full text-center">'.DMYFormat($deposit->tanggal).'</td>
									<td class="border-full text-center">'.$deposit->metodepembayaran.'</td>
									<td class="border-full text-center">'.number_format($deposit->nominal).'</td></tr>'; ?>
							 <? } else { ?>
									<td class="border-full text-center"><?=DMYFormat($deposit->tanggal)?></td>
									<td class="border-full text-center"><?=$deposit->metodepembayaran?></td>
									<td class="border-full text-center"><?=number_format($deposit->nominal)?></td>
							<? } ?>
							<?php $countDeposit++; ?>
						<?php } ?>
				<?php } else { ?>
					<td class="border-full text-center"></td>
					<td class="border-full text-center"></td>
					<td class="border-full text-center"></td>
				<?php } ?>

					<td class="border-full text-center" rowspan="<?=$countListDeposit?>"><?=number_format($row->pelunasan_tunai)?></td>
					<td class="border-full text-center" rowspan="<?=$countListDeposit?>"><?=number_format($row->pelunasan_nontunai)?></td>
					<td class="border-full text-center" rowspan="<?=$countListDeposit?>"><?=number_format($row->pelunasan_kontraktor)?></td>
					<td class="border-full text-center" rowspan="<?=$countListDeposit?>"><?=number_format($row->pelunasan_tidaktertagih)?></td>
					<td class="border-full text-center" rowspan="<?=$countListDeposit?>"><?=number_format($row->pelunasan_tunai + $row->pelunasan_nontunai + $row->pelunasan_kontraktor + $row->pelunasan_tidaktertagih)?></td>
					<td class="border-full text-center" rowspan="<?=$countListDeposit?>"><?=number_format($row->total_tagihan)?></td>
					<td class="border-full text-center" rowspan="<?=$countListDeposit?>"><?=$row->keterangan?></td>
				</tr>

				<?=$strDeposit?>
				<?php } ?>
			</tbody>
		</table>
		<br>
		<br>
		<label class="text-header"><b>LAPORAN SETORAN</b></label>
		<table class="content" width="100%">
	    <thead>
		  	<tr>
	        <td class="border-full text-center" rowspan="2" style="width:3%">NO</td>
	        <td class="border-full text-center" rowspan="2" style="width:9%">TANGGAL</td>
	        <td class="border-full text-center" rowspan="2" style="width:9%">NO REGISTRASI</td>
	        <td class="border-full text-center" rowspan="2" style="width:9%">MEDREC</td>
	        <td class="border-full text-center" rowspan="2" style="width:12%">NAMA PASIEN</td>
	        <td class="border-full text-center" rowspan="2" style="width:6%">TAG KUNJ</td>
	        <td class="border-full text-center" rowspan="2" style="width:6%">TAG BAYAR</td>
	        <td class="border-full text-center" rowspan="2" style="width:6%">TIPE BAYAR</td>
	        <td class="border-full text-center" colspan="4" style="width:30%">PEMBAYARAN</td>
	        <td class="border-full text-center" rowspan="2" style="width:25%">KETERANGAN</td>
	      </tr>
			  <tr>
					<td class="border-full text-center" style="width:10%">TUNAI</td>
					<td class="border-full text-center" style="width:10%">NON TUNAI</td>
					<td class="border-full text-center" style="width:10%">KONTRAKTOR</td>
					<td class="border-full text-center" style="width:10%">TIDAK TERTAGIH</td>
			  </tr>
	    </thead>
	    <tbody>
				<?php $number = 1; ?>
				<?php $total_tunai = 0; ?>
				<?php $total_nontunai = 0; ?>
				<?php $total_kontraktor = 0; ?>
				<?php $total_kontraktor_pa = 0; ?>
				<?php $total_kontraktor_bpjstk = 0; ?>
				<?php $total_kontraktor_bpjskes = 0; ?>
				<?php $total_kontraktor_tkaryawan = 0; ?>
				<?php $total_kontraktor_tdokter = 0; ?>
				<?php $total_tidaktertagih = 0; ?>
				<?php $total_kwitansi_manual = $this->Lkasir_model->getTotalKwitansiManual($tanggaldari, $tanggalsampai); ?>
				<?php $total_kasbon = $this->Lkasir_model->getTotalKasbon($tanggaldari, $tanggalsampai, $user->id); ?>
				<?php $total_refund = 0; ?>

				<?php foreach ($this->Lkasir_model->getSetoranDepositRanap($tanggaldari, $tanggalsampai, $user->id) as $row) { ?>
				<tr>
			     <td class="border-full text-center"><?=$number++?></td>
			     <td class="border-full text-center"><?=DMYFormat($row->tanggal)?></td>
			     <td class="border-full text-center"><?=$row->notransaksi?></td>
			     <td class="border-full text-center"><?=$row->nomedrec?></td>
			     <td class="border-full text-center"><?=$row->namapasien?></td>
			     <td class="border-full text-center"><?=$row->tag_kunjungan?></td>
			     <td class="border-full text-center"><?=$row->tag_bayar?></td>
			     <td class="border-full text-center"><?=$row->tipe_bayar?></td>
			     <td class="border-full text-center"><?=number_format($row->pelunasan_tunai)?></td>
			     <td class="border-full text-center"><?=number_format($row->pelunasan_nontunai)?></td>
			     <td class="border-full text-center"><?=number_format($row->pelunasan_kontraktor)?></td>
			     <td class="border-full text-center"><?=number_format($row->pelunasan_tidaktertagih)?></td>
			     <td class="border-full text-center"><?=$row->keterangan?></td>
				 </tr>

				 <?php $total_tunai += $row->pelunasan_tunai; ?>
				 <?php $total_nontunai += $row->pelunasan_nontunai; ?>
				 <?php $total_kontraktor += $row->pelunasan_kontraktor; ?>
				 <?php $total_tidaktertagih += $row->pelunasan_tidaktertagih; ?>
			 	<? } ?>
				<?php foreach ($this->Lkasir_model->getSetoranPelunasanRanap($tanggaldari, $tanggalsampai, $user->id) as $row) { ?>
				<tr>
			     <td class="border-full text-center"><?=$number++?></td>
			     <td class="border-full text-center"><?=DMYFormat($row->tanggal)?></td>
			     <td class="border-full text-center"><?=$row->notransaksi?></td>
			     <td class="border-full text-center"><?=$row->nomedrec?></td>
			     <td class="border-full text-center"><?=$row->namapasien?></td>
			     <td class="border-full text-center"><?=$row->tag_kunjungan?></td>
			     <td class="border-full text-center"><?=$row->tag_bayar?></td>
			     <td class="border-full text-center"><?=$row->tipe_bayar?></td>
			     <td class="border-full text-center"><?=number_format($row->pelunasan_tunai)?></td>
			     <td class="border-full text-center"><?=number_format($row->pelunasan_nontunai)?></td>
			     <td class="border-full text-center"><?=number_format($row->pelunasan_kontraktor)?></td>
			     <td class="border-full text-center"><?=number_format($row->pelunasan_tidaktertagih)?></td>
			     <td class="border-full text-center"><?=$row->keterangan?></td>
				 </tr>

				 <?php $total_tunai += $row->pelunasan_tunai; ?>
				 <?php $total_nontunai += $row->pelunasan_nontunai; ?>
				 <?php $total_kontraktor += $row->pelunasan_kontraktor; ?>
				 <?php $total_tidaktertagih += $row->pelunasan_tidaktertagih; ?>

				 <?php if ($row->idmetode == 8 && $row->tipekontraktor == 1) { ?>
		 				<?php $total_kontraktor_pa = $row->pelunasan_kontraktor; ?>
				 <? } ?>
				 <?php if ($row->idmetode == 8 && $row->tipekontraktor == 2) { ?>
		 				<?php $total_kontraktor_bpjstk = $row->pelunasan_kontraktor; ?>
				 <? } ?>
				 <?php if ($row->idmetode == 5) { ?>
		 				<?php $total_kontraktor_tkaryawan = $row->pelunasan_kontraktor; ?>
				 <? } ?>
				 <?php if ($row->idmetode == 6) { ?>
		 				<?php $total_kontraktor_tdokter = $row->pelunasan_kontraktor; ?>
				 <? } ?>
			 	<? } ?>
				<?php foreach ($this->Lkasir_model->getSetoranRefundDepositRanap($tanggaldari, $tanggalsampai, $user->id) as $row) { ?>
					<tr>
				     <td class="border-full text-center"><?=$number++?></td>
				     <td class="border-full text-center"><?=DMYFormat($row->tanggal)?></td>
				     <td class="border-full text-center"><?=$row->notransaksi?></td>
				     <td class="border-full text-center"><?=$row->nomedrec?></td>
				     <td class="border-full text-center"><?=$row->namapasien?></td>
				     <td class="border-full text-center"><?=$row->tag_kunjungan?></td>
				     <td class="border-full text-center"><?=$row->tag_bayar?></td>
				     <td class="border-full text-center"><?=$row->tipe_bayar?></td>
				     <td class="border-full text-center"><?=number_format($row->pelunasan_tunai)?></td>
				     <td class="border-full text-center"><?=number_format($row->pelunasan_nontunai)?></td>
				     <td class="border-full text-center"><?=number_format($row->pelunasan_kontraktor)?></td>
				     <td class="border-full text-center"><?=number_format($row->pelunasan_tidaktertagih)?></td>
				     <td class="border-full text-center"><?=$row->keterangan?></td>
					 </tr>

				 	<?php $total_refund += $row->pelunasan_tunai; ?>
			 	<? } ?>
				<?php foreach ($this->Lkasir_model->getSetoranRefundTransaksiRanap($tanggaldari, $tanggalsampai, $user->id) as $row) { ?>
					<tr>
				     <td class="border-full text-center"><?=$number++?></td>
				     <td class="border-full text-center"><?=DMYFormat($row->tanggal)?></td>
				     <td class="border-full text-center"><?=$row->notransaksi?></td>
				     <td class="border-full text-center"><?=$row->nomedrec?></td>
				     <td class="border-full text-center"><?=$row->namapasien?></td>
				     <td class="border-full text-center"><?=$row->tag_kunjungan?></td>
				     <td class="border-full text-center"><?=$row->tag_bayar?></td>
				     <td class="border-full text-center"><?=$row->tipe_bayar?></td>
				     <td class="border-full text-center"><?=number_format($row->pelunasan_tunai)?></td>
				     <td class="border-full text-center"><?=number_format($row->pelunasan_nontunai)?></td>
				     <td class="border-full text-center"><?=number_format($row->pelunasan_kontraktor)?></td>
				     <td class="border-full text-center"><?=number_format($row->pelunasan_tidaktertagih)?></td>
				     <td class="border-full text-center"><?=$row->keterangan?></td>
					 </tr>
 				 	<?php $total_refund += $row->pelunasan_tunai; ?>
			 	<? } ?>
				<tr>
			     <td class="border-full text-center" colspan="8">TOTAL SETORAN</td>
			     <td class="border-full text-center"><?=number_format($total_tunai)?></td>
			     <td class="border-full text-center"><?=number_format($total_nontunai)?></td>
			     <td class="border-full text-center"><?=number_format($total_kontraktor)?></td>
			     <td class="border-full text-center"><?=number_format($total_tidaktertagih)?></td>
			     <td class="border-full text-center"></td>
				 </tr>
	    </tbody>
	  </table>
		<br>&nbsp;
	  <table class="content" style="width:100%">
	  <td>
		<label class="text-header"><b>REKAPITULASI PENDAPATAN USER : <?=strtoupper($user->name)?></b></label>
	  </td>
	  </table>
	  
	  <table class="content" style="width:100%">
		<td style="width:50%">
			<label class="text-header"><strong>DETAIL PENDAPATAN</strong></label><br/>
			<table class="content" width="100%">
				<thead>
				  <tr>
						<td class="border-full text-center"  style="width:10%">NO</td>
						<td class="border-full text-center"  style="width:50%">DESKRIPSI</td>
						<td class="border-full text-center"  style="width:40%">NOMINAL</td>
				  </tr>
				</thead>
				<tbody>
					<tr>
						<td class="border-full text-center">1</td>
						<td class="border-full text-left">TUNAI</td>
						<td class="border-full text-right"><?=number($total_tunai)?></td>
					</tr>
					<tr>
						<td class="border-full text-center"  style="width:20%">2</td>
						<td class="border-full text-left">BANK</td>
						<td class="border-full text-right"><?=number($total_nontunai)?></td>
					</tr>
					<tr>
						<td class="border-full text-center"  style="width:20%">3</td>
						<td class="border-full text-left">KONTRAKTOR</td>
						<td class="border-full text-right"><?=number($total_kontraktor)?></td>
					</tr>
					<tr>
						<td class="border-full text-center"  style="width:20%">4</td>
						<td class="border-full text-left">KW MANUAL</td>
						<td class="border-full text-right"><?=number($total_kwitansi_manual)?></td>
					</tr>
					<tr>
						<td class="border-full text-center"  style="width:20%">5</td>
						<td class="border-full text-left">KASBON</td>
						<td class="border-full text-right"><?=number($total_kasbon)?></td>
					</tr>
					<tr>
						<td class="border-full text-center"  style="width:20%">6</td>
						<td class="border-full text-left">T TERTAGIH</td>
						<td class="border-full text-right"><?=number($total_tidaktertagih)?></td>
					</tr>
					<tr>
						<td class="border-full text-center" colspan="2">TOTAL PENDAPATAN</td>
						<td class="border-full text-right"><?=number($total_tunai + $total_nontunai + $total_kontraktor + $total_kwitansi_manual + $total_tidaktertagih)?></td>
					</tr>
				</tbody>
		    </table>
		</td>
		<td style="width:2%"></td>
		<td style="width:48%">
			<label class="text-header"><strong>DETAIL PENDAPATAN NON TUNAI</strong></label>
			<table class="content" width="80%">
				<thead>
				  <tr>
						<td class="border-full text-center"  style="width:30%">NAMA BANK</td>
						<td class="border-full text-center"  style="width:22%">DEBIT</td>
						<td class="border-full text-center"  style="width:22%">KREDIT</td>
						<td class="border-full text-center"  style="width:22%">TRANSFER</td>
				  </tr>
				</thead>
				<tbody>
						<?php $totalNonTunaiDebit = 0; ?>
						<?php $totalNonTunaiKredit = 0; ?>
						<?php $totalNonTunaiTransfer = 0; ?>
						<?php foreach ($this->Lkasir_model->getPendapatanNonTunai($tanggaldari, $tanggalsampai, $user->id) as $row) { ?>
						<tr>
							<td class="border-full text-left"><?=$row->bank?></td>
							<td class="border-full text-right"><?=number($row->total_debit)?></td>
							<td class="border-full text-right"><?=number($row->total_kredit)?></td>
							<td class="border-full text-right"><?=number($row->total_transfer)?></td>
					</tr>

						<?php $totalNonTunaiDebit += $row->total_debit; ?>
						<?php $totalNonTunaiKredit += $row->total_kredit; ?>
						<?php $totalNonTunaiTransfer += $row->total_transfer; ?>
						<? } ?>
					  <tr>
							<td class="border-full text-center"  style="width:30%">&nbsp;</td>
							<td class="border-full text-center"  style="width:22%">&nbsp;</td>
							<td class="border-full text-center"  style="width:22%">&nbsp;</td>
							<td class="border-full text-center"  style="width:22%">&nbsp;</td>
					  </tr>
						<tr>
							<td class="border-full text-center"  style="width:30%">TOTAL</td>
							<td class="border-full text-right"  style="width:22%"><?=number($totalNonTunaiDebit)?></td>
							<td class="border-full text-right"  style="width:22%"><?=number($totalNonTunaiKredit)?></td>
							<td class="border-full text-right"  style="width:22%"><?=number($totalNonTunaiTransfer)?></td>
					  </tr>
				</tbody>
			   </table>
		</td>
	</table>
	<br />
	 <table class="content" style="width:100%">
		<td width="50%">
			 <label class="text-header"><b>SETORAN TUNAI</b></label>
			  <table class="content" width="100%">
					<thead>
					  <tr>
							<td class="border-full text-center"  style="width:10%">NO</td>
							<td class="border-full text-center"  style="width:50%">DESKRIPSI</td>
							<td class="border-full text-center"  style="width:40%">NOMINAL</td>
					  </tr>
					</thead>
					<tbody>
						<tr>
							<td class="border-full text-center">1</td>
							<td class="border-full text-left">TUNAI</td>
							<td class="border-full text-right"><?=number($total_tunai)?></td>
						</tr>
						<tr>
							<td class="border-full text-center"  style="width:20%">2</td>
							<td class="border-full text-left">KW MANUAL</td>
							<td class="border-full text-right"><?=number($total_kwitansi_manual)?></td>
						</tr>
						<tr>
							<td class="border-full text-center"  style="width:20%">4</td>
							<td class="border-full text-left">KASBON</td>
							<td class="border-full text-right"><?=number($total_kasbon)?></td>
						</tr>
						<tr>
							<td class="border-full text-center"  style="width:20%">3</td>
							<td class="border-full text-left">REFUND</td>
							<td class="border-full text-right">(<?=number($total_refund)?>)</td>
						</tr>
						<tr>
							<td class="border-full text-center" colspan="2">SETORAN TUNAI</td>
							<td class="border-full text-right"><?=number($total_tunai + $total_kwitansi_manual - $total_refund)?></td>
						</tr>
					</tbody>
			   </table>
		</td>
		<td style="width:2%"></td>
		<td  style="width:48%">
			<label class="text-header"><b>DETAIL PENDAPATAN KONTRAKTOR</b></label>
				<table class="content" width="100%">
					<thead>
					  <tr>
							<td class="border-full text-center"  style="width:50%">KETERANGAN</td>
							<td class="border-full text-center"  style="width:52%">KREDIT</td>
					  </tr>
					</thead>
					<tbody>
						<tr>
							<td class="border-full text-left">ASURANSI</td>
							<td class="border-full text-right"><?=number($total_kontraktor_pa)?></td>
					    </tr>
						<tr>
							<td class="border-full text-left">BPJS TK</td>
							<td class="border-full text-right"><?=number($total_kontraktor_bpjstk)?></td>
					    </tr>
						<tr>
							<td class="border-full text-left">BPJS KESEHATAN</td>
							<td class="border-full text-right"><?=number($total_kontraktor_bpjskes)?></td>
					    </tr>
						<tr>
							<td class="border-full text-left">T. KARYAWAN</td>
							<td class="border-full text-right"><?=number($total_kontraktor_tkaryawan)?></td>
					    </tr>
						<tr>
							<td class="border-full text-left">T. DOKTER</td>
							<td class="border-full text-right"><?=number($total_kontraktor_tdokter)?></td>
				    </tr>
						<tr>
							<td class="border-full text-center"  style="width:30%">TOTAL</td>
							<td class="border-full text-right"  style="width:22%"><?=number($total_kontraktor_pa + $total_kontraktor_bpjstk + $total_kontraktor_bpjskes + $total_kontraktor_tkaryawan + $total_kontraktor_tdokter)?></td>
						  </tr>
					</tbody>
		   	</table>
		</td>
	</table>
		
	</fieldset>
	<?php } //LOOPING USER ?>
</body>

</html>
