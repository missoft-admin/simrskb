<?php 'Naha'?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Laporan Penerimaan Dokter</title>
  <style type="text/css" media="all">
   
	
    body {
      -webkit-print-color-adjust: exact;
    }
    @page {
           margin-top: 1,8em;
            margin-left: 0.8em;
            margin-right: 1em;
            margin-bottom: 0,5em;
        }
    @media screen {
      
		
      table {
		 
		font-family: "Courier", Arial,sans-serif;
        font-size: 18px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		table-layout:fixed;
		  overflow:hidden;
		  word-wrap:break-word;
      }
      th {
        padding: 2px;
		 
      }
	  td {
        padding: 3px;
		 
      }
      .content td {
        padding: 2px;
		border: 0px solid #6033FF;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-family: "Courier", Arial,sans-serif;
		font-size: 20px !important;
      }
	  .text-judul{
        font-size: 28px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
    }
	
    
  </style>
  <script type="text/javascript">
    // try {
      // this.print();
    // } catch (e) {
      // window.onload = window.print;
    // }
  </script>
</head>

<body>

  <table class="content">
      <tr>
        <td rowspan="2" width="20%" class="text-center"><img src="{logo1_rs}" alt="" width="100" height="100"></td>
        <td width="60%"  class="text-center text-judul"><u><b>LAPORAN PENERIMAAN DOKTER</b></u></td>
		<td width="20%"></td>		
      </tr>
	  <tr>
			<td class="text-center">PERIODE : <?=$nama_periode?></td>
			<td></td>
	  </tr>
	  
  </table>
  <br/>
  <br/>
  <? 
  $tmp_dokter='';
  $gt_harga=0;
			$gt_disc=0;
			$gt_jml=0;
			$gt_pot_rs=0;
			$gt_pot_pajak=0;
			$gt_netto=0;
  foreach($dokter_list as $r_dokter) { ?>
  <?
		$q="SELECT iddokter,idtindakan,idpend,nopendaftaran,nokasir,no_medrec,namapasien,tag,nama_dokter,
			SUM(jasapelayanan) jasapelayanan, MAX(jasapelayanan_disc) jasapelayanan_disc, SUM(nominal_disc) nominal_disc,
			SUM(after_disc) after_disc, MAX(pot_rs) pot_rs, SUM(tot_potongan_rs) tot_potongan_rs, 
			MAX(pajak_dokter) pajak_dokter, SUM(tot_pajak_dokter) tot_pajak_dokter, SUM(total_bersih) as netto, nama_user

			FROM
			(
			SELECT tpl.iddokter,tpl.idtindakan,tpend.id idpend,tpend.nopendaftaran,tkasir.nokasir,tpend.no_medrec,tpend.namapasien,IF(tkasir.idtipe = 1,'RJ',IF(tkasir.idtipe = 2,'IGD','')) tag,
			mdokter.nama nama_dokter, (tpl.jasapelayanan*tpl.kuantitas) as jasapelayanan,
			tpl.jasapelayanan_disc, 
			tpl.jasapelayanan_disc nominal_disc, 
			(tpl.jasapelayanan*tpl.kuantitas) - (SELECT nominal_disc)after_disc, 
			(tpl.pot_rs) pot_rs , (tpl.pot_rs / 100) * (SELECT after_disc) tot_potongan_rs, 
			tpl.pajak_dokter, (tpl.pajak_dokter / 100) * (SELECT after_disc) tot_pajak_dokter, 

			(SELECT after_disc) - (SELECT tot_potongan_rs) - (SELECT tot_pajak_dokter) total_bersih,
			musers.name nama_user


			FROM tkasir
			INNER JOIN tpoliklinik_pelayanan tpl ON tkasir.idtindakan = tpl.idtindakan
			INNER JOIN tpoliklinik_tindakan tpd ON tpl.idtindakan = tpd.id
			INNER JOIN mdokter ON tpl.iddokter = mdokter.id
			INNER JOIN tpoliklinik_pendaftaran tpend ON tpend.id = tpd.idpendaftaran
			INNER JOIN musers ON  tkasir.edited_by = musers.id
			WHERE tkasir.`status` = 2 AND DATE_FORMAT( tkasir.tanggal, '%Y-%m-%d' ) BETWEEN '$tanggaldari' AND '$tanggalsampai' 
			AND tkasir.idtipe IN ( 1, 2) AND tpl.iddokter='".$r_dokter->id."'

			) xxx

			GROUP BY iddokter,idtindakan,idpend";
		$hasil=$this->db->query($q)->result();
		if ($hasil) {
			$no=1;
			$st_harga=0;
			$st_disc=0;
			$st_jml=0;
			$st_pot_rs=0;
			$st_pot_pajak=0;
			$st_netto=0;
			
  ?>
   <fieldset style="border:1px solid #000 !important;border-radius:0px;">
	<legend style="font-weight:bold;"></legend>
  <table class="content" width="100%">
    <thead>
	  <tr>
        <td class=" text-left text-header" colspan="10"><b>DOKTER : <?=$r_dokter->nama?></b></td>
      </tr>
	  <tr>
        <td class="border-full text-center" rowspan="2" style="width:3%">NO</td>
        <td class="border-full text-center" rowspan="2" style="width:11%">NO KASIR</td>
        <td class="border-full text-center" rowspan="2" style="width:8%">MEDREC</td>
        <td class="border-full text-center" rowspan="2" style="width:12%">NAMA PASIEN</td>
        <td class="border-full text-center" rowspan="2" style="width:6%">TAG</td>
        <td class="border-full text-center" rowspan="2" style="width:10%">HARGA</td>
        <td class="border-full text-center" colspan="2"  style="width:16%">POTONGAN</td>
        <td class="border-full text-center" rowspan="2" style="width:10%">NETTO</td>
        <td class="border-full text-center" rowspan="2" style="width:10%">USER</td>
      </tr>
	  <tr>
		<td class="border-full text-center" style="width:8%">RS</td>
		<td class="border-full text-center" style="width:8%">PAJAK</td>		
	  </tr>
    </thead>
    <tbody>
		
		<? foreach($hasil as $r){ 
			$st_harga +=$r->jasapelayanan;
			$st_disc +=$r->nominal_disc;
			$st_jml +=$r->after_disc;
			$st_pot_rs +=$r->tot_potongan_rs;
			$st_pot_pajak +=$r->tot_pajak_dokter;
			$st_netto +=$r->netto;
			
			$gt_harga +=$r->jasapelayanan;
			$gt_disc +=$r->nominal_disc;
			$gt_jml +=$r->after_disc;
			$gt_pot_rs +=$r->tot_potongan_rs;
			$gt_pot_pajak +=$r->tot_pajak_dokter;
			$gt_netto +=$r->netto;
		?>
			<tr>
				<td class="border-full text-center"><?=$no?></td>
				<td class="border-full text-center"><?=$r->nokasir?></td>
				<td class="border-full text-center"><?=$r->no_medrec?></td>
				<td class="border-full text-left"><?=$r->namapasien?></td>
				<td class="border-full text-center"><?=$r->tag?></td>
				<td class="border-full text-right"><?=number($r->after_disc)?></td>
				<td class="border-full text-right"><?=number($r->tot_potongan_rs)?></td>
				<td class="border-full text-right"><?=number($r->tot_pajak_dokter)?></td>
				<td class="border-full text-right"><?=number($r->netto)?></td>
				<td class="border-full text-left"><?=($r->nama_user)?></td>
			</tr>
		<?
		$no +=1;
		}?>
		<tr>
			<td class="border-full text-center" colspan="5">JUMLAH PENERIMAAN DOKTER : <?=$r_dokter->nama?></td>
			<td class="border-full text-right"><?=number($st_jml)?></td>
			<td class="border-full text-right"><?=number($st_pot_rs)?></td>
			<td class="border-full text-right"><?=number($st_pot_pajak)?></td>
			<td class="border-full text-right"><?=number($st_netto)?></td>
			<td class="border-full text-left"></td>
		</tr>
	</tbody>
  </table>
  </fieldset>
  <?}?>
   
  <?}?>
  <fieldset style="border:1px solid #000 !important;border-radius:0px;">
	<legend style="font-weight:bold;"></legend>
  <table class="content" width="100%">
    <thead>
	  
	  <tr>
        <td class="" rowspan="2" style="width:3%"></td>
        <td class="" rowspan="2" style="width:11%"></td>
        <td class="" rowspan="2" style="width:8%"></td>
        <td class="" rowspan="2" style="width:12%"></td>
        <td class="" rowspan="2" style="width:6%"></td>
        <td class="" rowspan="2" style="width:10%"></td>
        <td class="" colspan="2"  style="width:16%"></td>
        <td class="" rowspan="2" style="width:10%"></td>
        <td class="" rowspan="2" style="width:10%"></td>
      </tr>
	  <tr>
		<td class="" style="width:8%"></td>
		<td class="" style="width:8%"></td>		
	  </tr>
    </thead>
    <tbody>
		
		
		<tr>
			<td class="border-full text-center" colspan="5">GRAND TOTAL </td>
			<td class="border-full text-right"><?=number($gt_jml)?></td>
			<td class="border-full text-right"><?=number($gt_pot_rs)?></td>
			<td class="border-full text-right"><?=number($gt_pot_pajak)?></td>
			<td class="border-full text-right"><?=number($gt_netto)?></td>
			<td class="border-full text-left"></td>
		</tr>
	</tbody>
  </table>
  </fieldset>
</body>

</html>