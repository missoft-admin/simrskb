<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}antrian_monitoring" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('antrian_monitoring/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="row">
				
				<div class="form-group">
					<label class="col-md-2 control-label" for="nama_monitoring">Nama Display</label>
					<div class="col-md-8">
						<input type="text" class="form-control" <?=($id?'disabled':'')?> id="nama_monitoring" placeholder="Nama Display" name="nama_monitoring" value="{nama_monitoring}">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-2 control-label" for="nama_monitoring">Tipe Antrian</label>
					<div class="col-md-2">
						<select id="tipe_antrian"  <?=($id?'disabled':'')?> name="tipe_antrian" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="" <?=($tipe_antrian==''?'selected':'')?>>-Pilih Tipe-</option>
							<option value="1" <?=($tipe_antrian=='1'?'selected':'')?>>Pendaftaran</option>
							<option value="2" <?=($tipe_antrian=='2'?'selected':'')?>>Dokter / Penunjang</option>
						</select>
					</div>
				</div>
				
				<?if ($id){?>
			<div class="row push-20-t">
				
			</div>
			<div class="row">
				<div class="form-group">
					<label class="col-md-10 col-md-offset-2 text-danger"><?=($tipe_antrian=='1'?'SETTING PELAYANAN':'SETTING TUJUAN')?></label>
					<input type="hidden" class="form-control" id="antrian_id" placeholder="No Urut" name="antrian_id" value="{id}">
					<input type="hidden" class="form-control" id="arr" placeholder="No Urut" name="arr" value="">
				</div>
				<div class="form-group">
					<div class="col-md-10 col-md-offset-2">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_detail_1">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Urutan</th>
										<th width="65%">Detail</th>
										<th width="20%">Action</th>										   
									</tr>
									<input id="iddetail" type="hidden" value="">
									<tr>
										<th>#</th>
										<th>
											<input tabindex="0" type="text" class="form-control number" id="nourut" placeholder="No Urut" name="nourut">
										</th>
										<th>
											<?if ($tipe_antrian=='1'){?>
												<select id="detail_id"  name="detail_id" class="js-select2 form-control" style="width: 100%;">
													<option value="" selected>-Pilih Pelayanan-</option>
													<?foreach($list_pelayanan as $row){?>
													  <option value="<?=$row->id?>"><?=$row->nama_pelayanan?></option>
													<?}?>
												</select>
											<?}else{?>
												<select id="detail_id"  name="detail_id" class="js-select2 form-control" style="width: 100%;">
													<option value="" selected>-Pilih Tujuan-</option>
													<?foreach($list_tujuan as $row){?>
													  <option value="<?=$row->id?>"><?=$row->nama_tujuan?></option>
													<?}?>
												</select>
											<?}?>
										</th>
										
										<th>
											<div class="btn-group">
												<button class="btn btn-primary btn-sm" title="Simpan" type="button" id="btn_tambah"><i class="fa fa-save"></i></button>
												<button class="btn btn-warning btn-sm"  title="Clear" type="button" id="btn_refresh"><i class="fa fa-refresh"></i></button>
											</div>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<?}?>
				<div class="form-group">
					<div class="col-md-2 col-md-offset-2">
						<button class="btn btn-success" type="submit">Simpan</button>
						<a href="{base_url}antrian_monitoring" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
					</div>
				</div>
			</div>

			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>

</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">

	$(document).ready(function(){
		if ($("#antrian_id").val()){
			load_detail();
		}
	});

	$("#btn_tambah").click(function() {
		let antrian_id=$("#antrian_id").val();
		let tipe_antrian=$("#tipe_antrian").val();
		let iddetail=$("#iddetail").val();
		let nourut=$("#nourut").val();
		let detail_id=$("#detail_id").val();
		if ($("#nourut").val()==''){
			sweetAlert("Maaf...", "Tentukan No Urut", "error");
			return false;
		}
		if ($("#detail_id").val()==''){
			sweetAlert("Maaf...", "Tentukan Isi", "error");
			return false;
		}
		
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}antrian_monitoring/simpan_detail', 
			dataType: "JSON",
			method: "POST",
			data : {
				antrian_id:antrian_id,
				tipe_antrian:tipe_antrian,
				iddetail:iddetail,
				nourut:nourut,
				detail_id:detail_id,
				},
			complete: function(data) {
				// $("#idtipe").val('#').trigger('change');
				$('#index_detail_1').DataTable().ajax.reload( null, false ); 
				$("#cover-spin").hide();
				clear();
				
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}
		});

	});
	$("#btn_refresh_text").click(function() {
	clear();
	});
	
	function clear(){
		$("#iddetail").val('');
		$("#nourut").val('');
		$("#detail_id").val('#').trigger('change');
	}
	function load_detail(){
		var antrian_id=$("#antrian_id").val();
		var tipe_antrian=$("#tipe_antrian").val();
		$('#index_detail_1').DataTable().destroy();	
		table = $('#index_detail_1').DataTable({
			autoWidth: false,
			searching: false,
			serverSide: true,
			"processing": true,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}antrian_monitoring/load_detail', 
				type: "POST" ,
				dataType: 'json',
				data : {
						antrian_id:antrian_id,
						tipe_antrian:tipe_antrian
					   }
			}
		});
	}
	function hapus_detail($id){
	 var id=$id;
	swal({
			title: "Anda Yakin ?",
			text : "Untuk Hapus Detail?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}antrian_monitoring/hapus_detail',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
					$('#index_detail_1').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
				}
			});
		});
	}
	function edit_detail($id){
		let id=$id;
		$.ajax({
			url: '{site_url}antrian_monitoring/edit_detail', 
			dataType: "JSON",
			method: "POST",
			data : {id:id},
			success: function(data) {
				$("#nourut").val(data.nourut);
				$("#detail_id").val(data.detail_id).trigger('change');
				$("#iddetail").val(data.id);
			}
		});

	}
	
</script>