<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('163'))){ ?>
<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('165'))){ ?>
		<ul class="block-options">
        <li>
            <a href="{base_url}mtarif_bpjskesehatan/create" class="btn"><i class="fa fa-plus"></i></a>
        </li>
    </ul>
		<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
	
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">Kode</th>
					<th rowspan="2">Nama</th>
					<th rowspan="2">Tarif Rawat Jalan</th>
					<th colspan="3" class="text-center">Tarif Rawat Inap</th>
					<th rowspan="2">Aksi</th>
				</tr>
				<tr>
					<th>Kelas 1</th>
					<th>Kelas 2</th>
					<th>Kelas 3</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		</div>
	</div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"bSort": false,
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
				url: '{site_url}mtarif_bpjskesehatan/getIndex',
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": true },
					{ "width": "10%", "targets": 1, "orderable": true },
					{ "width": "20%", "targets": 2, "orderable": true },
					{ "width": "10%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 4, "orderable": true },
					{ "width": "10%", "targets": 5, "orderable": true },
					{ "width": "10%", "targets": 6, "orderable": true },
					{ "width": "10%", "targets": 7, "orderable": true }
				]
			});
	});
</script>
