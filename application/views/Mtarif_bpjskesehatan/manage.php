<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mtarif_bpjskesehatan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mtarif_bpjskesehatan/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Kode</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="kode" placeholder="Kode" name="kode" value="{kode}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="idtipe">Tipe</label>
				<div class="col-md-7">
					<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required="" aria-required="true">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($idtipe == 1 ? 'selected="selected"':'')?>>Rawat Jalan</option>
						<option value="2" <?=($idtipe == 2 ? 'selected="selected"':'')?>>Rawat Inap</option>
					</select>
				</div>
			</div>
			<div class="form-group form-rawatjalan" hidden>
				<label class="col-md-3 control-label" for="tarifrawatjalan">Tarif</label>
				<div class="col-md-7">
					<input type="text" class="form-control number" id="tarifrawatjalan" placeholder="Tarif Rawat Jalan" name="tarifrawatjalan" value="{tarifrawatjalan}">
				</div>
			</div>
			<div class="form-group form-rawatinap" hidden>
				<label class="col-md-3 control-label" for="tarifkelas1">Tarif Kelas 1</label>
				<div class="col-md-7">
					<input type="text" class="form-control number" id="tarifkelas1" placeholder="Tarif Kelas 1" name="tarifkelas1" value="{tarifkelas1}">
				</div>
			</div>
			<div class="form-group form-rawatinap" hidden>
				<label class="col-md-3 control-label" for="tarifkelas2">Tarif Kelas 2</label>
				<div class="col-md-7">
					<input type="text" class="form-control number" id="tarifkelas2" placeholder="Tarif Kelas 2" name="tarifkelas2" value="{tarifkelas2}">
				</div>
			</div>
			<div class="form-group form-rawatinap" hidden>
				<label class="col-md-3 control-label" for="tarifkelas3">Tarif Kelas 3</label>
				<div class="col-md-7">
					<input type="text" class="form-control number" id="tarifkelas3" placeholder="Tarif Kelas 3" name="tarifkelas3" value="{tarifkelas3}">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mtarif_bpjskesehatan" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		<?php if($this->uri->segment(2) == 'update'){ ?>
		if(<?=$idtipe?> == 1){
			$(".form-rawatjalan").show();
			$(".form-rawatinap").hide();
		}else{
			$(".form-rawatjalan").hide();
			$(".form-rawatinap").show();
		}
		<?php } ?>

		$(".number").number(true, 0, '.', ',');
		$("#idtipe").change(function(){
			if($(this).val() == 1){
				$(".form-rawatjalan").show();
				$(".form-rawatinap").hide();
			}else{
				$(".form-rawatjalan").hide();
				$(".form-rawatinap").show();
			}
		});
	});
</script>
