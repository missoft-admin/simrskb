<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<?
$user_id=$this->session->userdata('user_id');
?>
<div class="block push-10">
	<ul class="nav nav-pills">
		<li id="div_11" class="<?=($tab_utama=='11'?'active':'')?>">
			<a href="javascript:void(0)" onclick="set_tab_utama(11)"><i class="si si-list"></i> Semua</a>
		</li>
		<li  id="div_22" class="<?=($tab_utama=='22'?'active':'')?>">
			<a href="javascript:void(0)"  onclick="set_tab_utama(22)"><i class="si si-paper-clip"></i> Order </a>
		</li>
		<li  id="div_33" class="<?=($tab_utama=='33'?'active':'')?>">
			<a href="javascript:void(0)"  onclick="set_tab_utama(33)"><i class="si si-plane"></i> Perencanaan</a>
		</li>
		
		
	</ul>
	<div class="block-content bg-primary push-10-t">
			<input type="hidden" id="tab" name="tab" value="{tab}">
			<input type="hidden" id="tab_utama" name="tab_utama" value="{tab_utama}">
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			
			<div class="row pull-10">
				<div class="col-md-2 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">Poliklinik</label>
						<div class="col-xs-12">
							<select id="idpoli" name="idpoli"  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" <?=($idpoli=='#'?'selected':'')?>>- Semua Poliklinik -</option>
								<?foreach($list_poli as $r){?>
								<option value="<?=$r->id?>" <?=($idpoli==$r->id?'selected':'')?>><?=$r->nama?></option>
								<?}?>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">Dokter Peminta</label>
						<div class="col-xs-12">
							<select id="iddokter" name="iddokter"  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Dokter -</option>
								<?foreach($list_dokter as $r){?>
								<option value="<?=$r->id?>" ><?=$r->nama?></option>
								<?}?>
								
								
							</select>
						</div>
					</div>
				</div>
				
				
				
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">Tanggal</label>
						<div class="col-xs-12">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                <input class="form-control" type="text" id="tanggal_1" placeholder="From" value="{tanggal_1}">
                                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                <input class="form-control" type="text" id="tanggal_2" placeholder="To" value="{tanggal_2}">
                            </div>
						</div>
					</div>
				</div>
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">Search</label>
						<div class="col-xs-12">
							<input type="text" class="form-control" id="pencarian" placeholder="Nama Pasien / No Medrec" value="">
						</div>
					</div>
				</div>
				
				<div class="col-md-1 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">&nbsp;&nbsp;</label>
						<div class="col-xs-12">
							<span class="input-group-btn ">
								<button class="btn btn-warning btn-block " id="btn_cari" type="button" title="Login"><i class="fa fa-search pull-left"></i> Cari</button>&nbsp;
							</span>
						</div>
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
	</div>
</div>

<div class="block">
	<ul class="nav nav-pills">
		<li id="div_1" class="<?=($tab=='1'?'active':'')?>">
			<a href="javascript:void(0)" onclick="set_tab(1)"><i class="si si-list"></i> Semua</a>
		</li>
		<li  id="div_2" class="<?=($tab=='2'?'active':'')?>">
			<a href="javascript:void(0)"  onclick="set_tab(2)"><i class="fa fa-calendar-plus-o"></i> Sudah Ada Kunjungan </a>
		</li>
		<li  id="div_3" class="<?=($tab=='3'?'active':'')?>">
			<a href="javascript:void(0)" onclick="set_tab(3)"><i class="fa fa-calendar-times-o"></i> Belum Ada Kunjungan</a>
		</li>
		
		
	</ul>
	<div class="block-content">
			
			<input type="hidden" id="list_poli_array" name="list_poli_array" value="{list_poli_array}">
			<div class="row   ">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table" id="index_all">
							<thead>
								<tr>
									<th width="10%"></th>
									<th width="10%"></th>
									<th width="10%"></th>
									<th width="10%"></th>
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		
	
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<? $this->load->view('Tpendaftaran_poli_ttv/modal_alergi')?>
<script type="text/javascript">
var table;
var tab;
var tab_utama;
var st_login;

$(document).ready(function(){	
	tab=$("#tab").val();
	tab_utama=$("#tab_utama").val();
	// load_index_all();
	set_tab(tab);
	
});

$(document).on("change","#idpoli",function(){
	load_index_all();
});
$(document).on("click","#btn_cari",function(){
	load_index_all();
});
function set_tab_utama($tab){
	// alert('sini');
	tab_utama=$tab;
	$(".div_index").show();
	$("#div_penjualan").hide();
	document.getElementById("div_11").classList.remove("active");
	document.getElementById("div_22").classList.remove("active");
	document.getElementById("div_33").classList.remove("active");
	
	if (tab_utama=='11'){
		document.getElementById("div_11").classList.add("active");
	}
	if (tab_utama=='22'){
		document.getElementById("div_22").classList.add("active");
	}
	if (tab_utama=='33'){
		document.getElementById("div_33").classList.add("active");
	}
	load_index_all();
}
function set_tab($tab){
	tab=$tab;
	// alert(tab);
	// $("#cover-spin").show();
	document.getElementById("div_1").classList.remove("active");
	document.getElementById("div_2").classList.remove("active");
	document.getElementById("div_3").classList.remove("active");
	if (tab=='1'){
		document.getElementById("div_1").classList.add("active");
	}
	if (tab=='2'){
		document.getElementById("div_2").classList.add("active");
	}
	if (tab=='3'){
		document.getElementById("div_3").classList.add("active");
	}
	
	load_index_all();
}

function load_index_all(){
	$('#index_all').DataTable().destroy();	
	let idpoli=$("#idpoli").val();
	let iddokter=$("#iddokter").val();
	let tanggal_1=$("#tanggal_1").val();
	let tanggal_2=$("#tanggal_2").val();
	let pencarian=$("#pencarian").val();
	
	// alert(tab_utama);
	table = $('#index_all').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			
			"processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "10%", "targets": 0},
					{ "width": "50%", "targets": 1},
					{ "width": "20%", "targets": 2},
					{ "width": "20%", "targets": 3},
					
				],
            ajax: { 
                url: '{site_url}Tpoliklinik_rm_order/getIndex_all', 
                type: "POST" ,
                dataType: 'json',
				data : {
						idpoli:idpoli,
						iddokter:iddokter,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						pencarian:pencarian,
						tab:tab,
						tab_utama:tab_utama,
						
					   }
            },
			"drawCallback": function( settings ) {
				 // $("#index_all thead").remove();
			 }  
        });
	$("#cover-spin").hide();
}
function startWorker() {
	if(typeof(Worker) !== "undefined") {
		if(typeof(w) == "undefined") {
			w = new Worker("{site_url}assets/js/worker.js");
	
		}
		w.postMessage({'cmd': 'start' ,'msg': 5000});
		w.onmessage = function(event) {
			$('#index_all').DataTable().ajax.reload( null, false );
		};
		w.onerror = function(event) {
			window.location.reload();
		};
	} else {
		alert("Sorry, your browser does not support Autosave Mode");
		$("#autosave_status").html('<button class="btn btn-warning"><i class="fa fa-warning"></i> Autosave not running!</button>');
	}
 }

</script>