<!-- Modal Ubah Kelas Tarif -->
<div class="modal fade" id="modal-ubah-kelas-tarif" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-danger">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Ubah Kelas Tarif</h3>
				</div>
                <div class="block-content">
                    <div class="form-group">
                        <label for="selectUbahKelasTarif">Kelas Tarif</label>
                        <select id="selectUbahKelasTarif" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <?php foreach (get_all('mkelas', ['status' => 1]) as $r) { ?>
                            <option value="<?= $r->id; ?>"><?= $r->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="inputTotalTarif">Total Tarif</label>
                        <input type="text" class="form-control format-number" id="inputTotalTarif" disabled>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="btn-ubah-kelas-tarif" class="btn btn-success" data-dismiss="modal">Ya</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $(".format-number").number(true, 0, '.', ',');

    $(document).on("click", "#ubah-kelas-tarif", function() {
        transaksiId = $(this).data('transaksi-id');
        totalTarif = $(this).data('total-tarif');
        $('#inputTotalTarif').val(totalTarif);
    });

    $(document).on("change", "#selectUbahKelasTarif", function() {
        let kelasTarif = $("#selectUbahKelasTarif option:selected").val();
        
        $("#cover-spin").show();
        
        $.ajax({
        url: '{site_url}term_radiologi_lainnya/get_tarif_kelas_pemeriksaan/' + transaksiId + '/' + kelasTarif,
            success: function (result) {
                $('#inputTotalTarif').val(result.total);

                $("#cover-spin").hide();
            },
            error: function (error) {
                // Handle error if deletion fails
                console.error('Error update:', error);
                alert('Error update. Please try again.');
            }
        });
    });

    $(document).on("click", "#btn-ubah-kelas-tarif", function() {
        let kelasTarif = $("#selectUbahKelasTarif option:selected").val();
        
        ubahKelasTarif(transaksiId, kelasTarif);
    });
});

function ubahKelasTarif(transaksiId, kelasTarif) {
    $("#cover-spin").show();

    $.ajax({
        url: '{site_url}term_radiologi_lainnya/ubah_kelas_tarif/' + transaksiId + '/' + kelasTarif,
        success: function (result) {
            $("#cover-spin").hide();

            $.toaster({
                priority: 'success',
                title: 'Berhasil!',
                message: result.message
            });
            
            loadDataTableTransaksiPermintaan();
        },
        error: function (error) {
            // Handle error if deletion fails
            console.error('Error update:', error);
            alert('Error update. Please try again.');
        }
    });
}
</script>
