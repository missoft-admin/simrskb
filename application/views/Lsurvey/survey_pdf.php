<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title><?=$title?></title>
	<style type="text/css" media="all">
    <?php include "assets/css/print.css"?>
	@page {
		margin-top: 1em;
		margin-left: 1.6em;
		margin-right: 1.5 em;
		margin-bottom: 1em;
	}
	@font-face {
        font-family: 'font';
        font-style: normal;
        font-weight: normal;
        src: url(dompdf/font/arial.ttf);
    }

    @font-face {
        font-family: 'font2';
        font-style: normal;
        font-weight: bold;
        src: url(dompdf/font/arialbd.ttf);
    }
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		font-family: "Segoe UI", Arial, sans-serif;
      }

      th {
        padding: 5px;
      }
	  td {
        padding: 0px;
      }
      .content th {
        padding: 2px;
      }
      .content td {
        padding: 2px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		 margin: 3px;
		 padding: 2px;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	  .text-header{
		font-size: 20px !important;
      }

      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		
      }
      th {
        padding: 5px;

      }
	  td {
        padding: 0px;

      }
      .content td {
        padding: 0px;
		word-wrap: break-word;
		border: 0px solid #6033FF;
		vertical-align:middle;
      }
	  .content-2 td {
        padding: 0px;
		border: 0px solid #6033FF;
		vertical-align:top;
      }
	  .text-muted {
		   font-size: 12px !important;
	  }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;

      }
	  .text-notes{
		font-size: 10px !important;
      }
	  .text-normal{
		font-size: 12px !important;
      }
	  .text-upnormal{
		font-size: 14px !important;
      }
	  .text-header{
		font-size: 16px !important;
		font-weight:bold;
      }
	  .text-judul{
        font-size: 18px  !important;
        
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 20px 0;
		}
		fieldset {
			border:1px solid #000;
			border-radius:4px;
			box-shadow:0 0 0px #000;
			margin-top: -4px !important;
		}
		legend {
			background:#fff;
		}
    }

	.text-muted {
	  color: #000;
	  font-weight: normal;
	  font-style: italic;
	  
	}
	.center {
	   display: block;
		  margin-left: auto;
		  margin-right: auto;
		  width: 50%;
	}
	ul {
	  margin:0;
	  padding-left: 12px;
	}
  </style>
</head>

<body>
	<header>
		
	</header>
	
	<main>
	<table class="content">
		<tr>
			<td width="100%" class="text-center"><img src="<?=base_url().'/assets/upload/app_setting/'.$logo?>" alt="" width="100" height="100"></td>
		</tr>
		<tr>
			<td width="100%" class="text-center text-normal"><?=$alamat_rs1?><br><?=$telepon_rs?> <?=$fax_rs?><br><?=$web_rs?> Email : <?=$email_rs?></td>
		</tr>
	</table>
	
	<table class="content">
		<tr>
			<td width="100%" class="text-center text-judul"><strong><?=$judul_header_ina?> <?=strtoupper($tipe_laporan)?></strong><br><i>PERIODE <?=($tanggal_1_trx)?> s/d <?=($tanggal_2_trx)?></i></td>
		</tr>
		
	</table>
	<br>
	<br>
	<br>
	<br>
	
	<table class="content">
		<thead>
			<tr>
				<th class="text-center border-full">NO</th>
				<th class="text-center border-full">NO PENDAFTARAN</th>
				<th class="text-center border-full">DATA PASIEN</th>
				<th class="text-center border-full">KORESPONDEN</th>
				<?foreach($list_header as $row){?>
					<th class="text-center border-full"><?=$row->inisial?></th>
				<?}?>
				<?if ($tipe_laporan=='internal'){?>
				<th class="border-full">TOTAL SKOR</th>
				<?}else{?>
				<th class="border-full text-center">NILAI PER UNSUR</th>
				<th class="border-full text-center">NRR PER UNSUR</th>
				<th class="border-full text-center">NRR TERTIMBANG PER UNSUR</th>
				<th class="border-full text-center">SATUAN KEPUASAN MASYARAKAT</th>
				<th class="border-full text-center">HASIL</th>
				<?}?>
			</tr>
		</thead>
		<tbody>
			<?
			$no=1;
			$total=0;
			foreach($list_data as $r){
				$assesmen_id=$r->assesmen_id;
				?>
			<tr>
				<td class="text-left border-full"><?=$no?></td>
				<td class="text-left border-full"><?=$r->nopendaftaran ?></td>
				<td class="text-left border-full"><?=$r->title.' '.$r->namapasien ?></td>
				<td class="text-left border-full"><?=$r->nama_profile ?></td>
				<?foreach($list_header as $row){?>
				<?
					 $param_id=$row->param_id;
					  $data_survey = array_filter($jawaban_arr, function($var) use ($assesmen_id,$param_id) { 
						return ($var['assesmen_id'] == $assesmen_id && $var['param_id'] == $param_id);
					  });		
						// print_r($arr_jadwal);exit;
					  $data_survey = reset($data_survey);
					  if ($data_survey){
						   $jawaban='<strong>'.strtoupper($data_survey['jawaban']).'</strong><br>('.$data_survey['jawaban_skor'].')';
						  $total=$total+$data_survey['jawaban_skor'];
					  }else{
						  $jawaban='';
					  }
				?>
					<td class="text-center border-full"><?=$jawaban?></td>
				<?}?>
				<?if ($tipe_laporan=='internal'){?>
					<td class="border-full text-center"><strong><?=number_format($total,0)?></strong></td>
				<?}else{?>
					<td class="border-full text-center"><strong><?=number_format($r->nilai_per_unsur,0)?></strong></td>
					<td class="border-full text-center"><strong><?=number_format($r->nrr_per_unsur,5)?></strong></td>
					<td class="border-full text-center"><strong><?=number_format($r->nrr_tertimbang,5)?></strong></td>
					<td class="border-full text-center"><strong><?=number_format($r->kepuasan_mas,0)?></strong></td>
					<td class="border-full text-center"><strong><?=$r->flag_nilai_mas.'<br>'.($r->hasil_flag)?></strong></td>
				<?}?>
				<?
				$no++;
				}?>
			</tr>
			<?if ($tipe_laporan=='internal'){?>
				<tr>
					<td colspan="4" class="border-full text-center"><strong>TOTAL</strong></td>
					<?
						$tabel='';
						foreach($list_header as $row){
						  $param_id=$row->param_id;
						  $data_survey = array_filter($jawaban_arr_total, function($var) use ($param_id) { 
							return ($var['param_id'] == $param_id);
						  });	
							// print_r($arr_jadwal);exit;
						  $data_survey = reset($data_survey);
						  if ($data_survey){
							  $jawaban='<strong>'.strtoupper($data_survey['jawaban_skor']).'</strong>';
							  $total=$total+$data_survey['jawaban_skor'];
						  }else{
							  $jawaban='';
						  }
					  
					?>
						<td class="text-center border-full"><?=$jawaban?></td>
					<?}?>
					<td class="border-full text-center"><strong><?=number_format($total,0)?></strong></td>
				</tr>
			<?}else{?>
				<tr>
					<td colspan="4" class="border-full text-center"><strong>NILAI PER UNSUR</strong></td>
					<?
						  foreach($list_header as $row){
							  $param_id=$row->param_id;
							  $data_survey = array_filter($jawaban_arr_total, function($var) use ($param_id) { 
								return ($var['param_id'] == $param_id);
							  });	
								// print_r($arr_jadwal);exit;
							  $data_survey = reset($data_survey);
							  if ($data_survey){
								  $jawaban='<strong>'.strtoupper($data_survey['nilai_per_unsur']).'</strong>';
							  }else{
								  $jawaban='';
							  }
							  ?>
						<td class="border-full text-center"><strong><?=($jawaban)?></strong></td>
							  
					<?
						  }
					?>
					<td colspan="5" class="border-full text-center"></td>
				</tr>
				<tr>
					<td colspan="4" class="border-full text-center"><strong>NRR PER UNSUR</strong></td>
					<?
						  foreach($list_header as $row){
							  $param_id=$row->param_id;
							  $data_survey = array_filter($jawaban_arr_total, function($var) use ($param_id) { 
								return ($var['param_id'] == $param_id);
							  });	
								// print_r($arr_jadwal);exit;
							  $data_survey = reset($data_survey);
							  if ($data_survey){
								  $jawaban='<strong>'.strtoupper($data_survey['nrr_per_unsur']).'</strong>';
							  }else{
								  $jawaban='';
							  }
							  ?>
						<td class="border-full text-center"><strong><?=($jawaban)?></strong></td>
							  
					<?
						  }
					?>
					<td colspan="5" class="border-full text-center"></td>
				</tr>
				<tr>
					<td colspan="4" class="border-full text-center"><strong>NRR TERTIMBANG PER UNSUR</strong></td>
					<?
						  foreach($list_header as $row){
							  $param_id=$row->param_id;
							  $data_survey = array_filter($jawaban_arr_total, function($var) use ($param_id) { 
								return ($var['param_id'] == $param_id);
							  });	
								// print_r($arr_jadwal);exit;
							  $data_survey = reset($data_survey);
							  if ($data_survey){
								  $jawaban='<strong>'.strtoupper($data_survey['nrr_tertimbang']).'</strong>';
							  }else{
								  $jawaban='';
							  }
							  ?>
						<td class="border-full text-center"><strong><?=($jawaban)?></strong></td>
							  
					<?
						  }
					?>
					<td colspan="5" class="border-full text-center"></td>
				</tr>
				<tr>
					<td colspan="4" class="border-full text-center"><strong>SATUAN KEPUASAN MASYARAKAT</strong></td>
					<?
						  foreach($list_header as $row){
							  $param_id=$row->param_id;
							  $data_survey = array_filter($jawaban_arr_total, function($var) use ($param_id) { 
								return ($var['param_id'] == $param_id);
							  });	
								// print_r($arr_jadwal);exit;
							  $data_survey = reset($data_survey);
							  if ($data_survey){
								  $jawaban='<strong>'.strtoupper(number_format($data_survey['satuan_kepuasan_mas'],0)).'</strong>';
							  }else{
								  $jawaban='';
							  }
							  ?>
						<td class="border-full text-center"><strong><?=($jawaban)?></strong></td>
							  
					<?
						  }
					?>
					<td colspan="5" class="border-full text-center"></td>
				</tr>
				<tr>
					<td colspan="4" class="border-full text-center"><strong>HASIL SURVEY</strong></td>
					<?
						  foreach($list_header as $row){
							  $param_id=$row->param_id;
							  $data_survey = array_filter($jawaban_arr_total, function($var) use ($param_id) { 
								return ($var['param_id'] == $param_id);
							  });	
								// print_r($arr_jadwal);exit;
							  $data_survey = reset($data_survey);
							  if ($data_survey){
								  $jawaban='<strong>'.strtoupper($data_survey['flag_nilai'].'<br>'.$data_survey['ref_nilai']).'</strong>';
							  }else{
								  $jawaban='';
							  }
							  ?>
						<td class="border-full text-center"><strong><?=($jawaban)?></strong></td>
							  
					<?
						  }
					?>
					<td colspan="5" class="border-full text-center"></td>
				</tr>
			<?}?>
		</tbody>
	</table>
	<table class="content">
		<tr>
			<td width="50%" class="text-left text-normal "><strong><?=($judul_footer_ina)?></strong></td>
			<td width="50%" class="text-right text-normal ">
				Printed By : 
				<?=get_nama_ppa($this->session->userdata('login_ppa_id')).' '.HumanDateLong(date('Y-m-d'))?>
			</td>
		</tr>
	</table>
	
	<br>
	</main>
</body>

</html>
