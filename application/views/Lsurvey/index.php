<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<hr style="margin-top:0px">
		<?php echo form_open('lsurvey/cetak','class="form-horizontal" id="form-work" target="_blank"') ?>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-12" for="msurvey_kepuasan_id">Nama Survey</label>
					<div class="col-md-12">
						<select name="msurvey_kepuasan_id" id="msurvey_kepuasan_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#" selected>-Pilih Survey-</option>
							<?php foreach  ($list_survey as $row) { ?>
								<option value="<?=$row->id?>" <?=($msurvey_kepuasan_id == $row->id) ? "selected" : "" ?>><?=$row->nama?></option>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-12" for="periode_tanggal">Periode Tanggal</label>
					<div class="col-md-12">
						<select name="periode_tanggal" id="periode_tanggal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="1" <?=($periode_tanggal=='1'?'selected':'')?>>Hari Ini</option>
							<option value="2" <?=($periode_tanggal=='2'?'selected':'')?>>Bulan Ini</option>
							<option value="3" <?=($periode_tanggal=='3'?'selected':'')?>>Tahun Ini</option>
							<option value="4" <?=($periode_tanggal=='4'?'selected':'')?>>Triwulan 1</option>
							<option value="5" <?=($periode_tanggal=='5'?'selected':'')?>>Triwulan 2</option>
							<option value="6" <?=($periode_tanggal=='6'?'selected':'')?>>Triwulan 3</option>
							<option value="7" <?=($periode_tanggal=='7'?'selected':'')?>>Triwulan 4</option>
							<option value="8" <?=($periode_tanggal=='8'?'selected':'')?>>Semester 1</option>
							<option value="9" <?=($periode_tanggal=='9'?'selected':'')?>>Semester 2</option>
							
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-12" for="periode_tanggal">&nbsp;&nbsp;</label>
					<div class="col-md-12">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
							<input class="form-control" type="text" id="tanggal_1_trx" name="tanggal_1_trx" placeholder="From" value="{tanggal_1_trx}"/>
							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
							<input class="form-control" type="text" id="tanggal_2_trx" name="tanggal_2_trx" placeholder="To" value="{tanggal_2_trx}"/>
						</div>
					</div>
				</div>
			</div>
						<input type="hidden" class="form-control"  id="tipe_laporan" name="tipe_laporan" placeholder="" value="{tipe_laporan}">
			<?if ($tipe_laporan=='external'){?>
			<div class="col-md-2">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-12" for="periode_tanggal">TOTAL SKOR SURVEY</label>
					<div class="col-md-12">
						<input type="text" class="form-control" disabled id="total_survey" placeholder="Total Survey" value="">
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-12" for="periode_tanggal">HASIL SURVEY</label>
					<div class="col-md-12">
						<input type="text" class="form-control" disabled id="hasil_survey" placeholder="Total Survey" value="">
					</div>
				</div>
			</div>
			<?}?>
		</div>
		<div class="row" style="margin-top:20px">
			<div class="col-md-12">
				<button class="btn btn-primary text-uppercase btn_aksi" type="button" id="btn_filter" name="btn_filter" style="font-size:13px;"><i class="fa fa-filter"></i> Tampilkan</button>
				<button  class="btn btn-danger text-uppercase btn_aksi" type="submit" id="btn_print" name="btn_print" value="1" style="font-size:13px;"><i class="fa fa-file-pdf-o"></i> Print PDF</button>
				<button  class="btn btn-success text-uppercase btn_aksi" type="submit" id="btn_excel" name="btn_print" value="2" style="font-size:13px;"><i class="fa fa-file-excel-o"></i> Excel</button>
			</div>
		</div>
			<?php echo form_close() ?>
		<hr>

		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
			<div class="table-responsive" id="div_tabel">
					
			</div>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">

$(document).ready(function(){
	hide_button();
	// clear_table();
	if ($("#tipe_laporan").val()=='internal'){
		loadSurvey();
	}else{
		loadSurveyex();
	}
})
$("#periode_tanggal").change(function(){
	let periode_tanggal=$("#periode_tanggal").val();
	$.ajax({
		url: '{site_url}lsurvey/list_tipe/'+periode_tanggal,
		dataType: "json",
		success: function(data) {
			$("#tanggal_1_trx").val(data.tanggal_1);
			$("#tanggal_2_trx").val(data.tanggal_2);
		}
	});

	
});
$("#msurvey_kepuasan_id").change(function(){
	hide_button();
	
});
function hide_button(){
	let msurvey_kepuasan_id=$("#msurvey_kepuasan_id").val();
	if (msurvey_kepuasan_id=='#'){
		$(".btn_aksi").prop("disabled", true);
	}else{
		$(".btn_aksi").removeAttr('disabled');
		
	}
}
// table table-bordered table-striped js-dataTable-full dataTable no-footer
function clear_table(){
	$tabel='<table class="table table-bordered table-striped js-dataTable-full dataTable table-header-bg" id="index_list">';
	$tabel +='<thead></thead><tbody></tbody></table>';
	$("#div_tabel").html($tabel);
}
$(document).on("click", "#btn_filter", function() {
	if ($("#tipe_laporan").val()=='internal'){
		loadSurvey();
	}else{
		loadSurveyex();
	}
});
function loadSurvey() {
	clear_table();
	var msurvey_kepuasan_id=$("#msurvey_kepuasan_id").val();
	var tanggal_1_trx=$("#tanggal_1_trx").val();
	var tanggal_2_trx=$("#tanggal_2_trx").val();
	var tipe=$("#tipe_laporan").val();
	
	$.ajax({
			url: '{site_url}lsurvey/generate_header/',
			dataType: "json",
			type: "POST",
			data: {
				msurvey_kepuasan_id: msurvey_kepuasan_id,tanggal_1_trx: tanggal_1_trx,
				tanggal_2_trx: tanggal_2_trx,tipe: tipe,
			},
			success: function(data) {
				
				var content='';
				content +='<tr>';
				content +='<th class="text-left">NO</th>';
				content +='<th class="text-left">NO PENDAFTARAN</th>';
				content +='<th class="text-left">DATA PASIEN</th>';
				content +='<th class="text-left">KORESPONDEN</th>';
				$('#index_list thead').empty();
				content +=data.detail;
				console.log(data.no)
				var kolom=[];
				for (let i = 0; i < data.no; i++) {
					kolom[i]=i+4;
				 
				}
				content +='</tr>';
				$('#index_list thead').append(content);
				$('#index_list tbody').empty();
				$('#index_list').DataTable().destroy();
				
				// $("#cover-spin").show();
				var table = $('#index_list').removeAttr('width').DataTable({
					 scrollY:        "450px",
					scrollX:        true,
					scrollCollapse: true,
					searching:         false,
					paging:         false,
					serverSide: true,
					"processing": true,
					"ordering": false,
					fixedColumns:   {
						left: 4,
						right: 1
					},
					"ajax": {
						url: '{site_url}lsurvey/loadInternal/',
						type: "POST",
						dataType: 'json',
						data: {
							msurvey_kepuasan_id: msurvey_kepuasan_id,tanggal_1_trx: tanggal_1_trx,
							tanggal_2_trx: tanggal_2_trx,tipe: tipe,
						}
					},
					fixedColumns:   {
						left: 3,
						right: 1
					},
					"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
						$("#cover-spin").hide();
						if (aData[0]==''){
							$('td', nRow).css('background-color', '#e9e9e9');					
						}
					},
					
					columnDefs: [				
						// { "visible": false, "targets": [0,1,2]},
						{ "width": "250px", "targets": [2]},
						{ "width": "200px", "targets": [3]},
						{ "width": "120px", "targets": kolom},
						{ className: "text-center","targets": kolom},
						
					]
					// columnDefs: [
						// { 
						// "targets" : [ 2 ],
								  // render : function (data, type, row) {
									   
									// if(row.status == 'yes' && row.quantity > 0){
									 
									// return '<button class="btn btn-secondary btn-block">view</button>
									 
									// }else if(row.status == 'yes' && row.quantity == 0){
									 
									// return '<button class="btn btn-secondary btn-block">Delete</button>';
									 
									// }
						 
						// }
						
					// ]
				});
				
				
				
			}
		});
}
function loadSurveyex() {
	clear_table();
	var msurvey_kepuasan_id=$("#msurvey_kepuasan_id").val();
	var tanggal_1_trx=$("#tanggal_1_trx").val();
	var tanggal_2_trx=$("#tanggal_2_trx").val();
	var tipe=$("#tipe_laporan").val();
	
	$.ajax({
			url: '{site_url}lsurvey/generate_header_ex/',
			dataType: "json",
			type: "POST",
			data: {
				msurvey_kepuasan_id: msurvey_kepuasan_id,tanggal_1_trx: tanggal_1_trx,
				tanggal_2_trx: tanggal_2_trx,tipe: tipe,
			},
			success: function(data) {
				$("#total_survey").val(data.kepuasan_mas)
				$("#hasil_survey").val(data.flag_nilai+' - '+data.ref_nilai)
				var content='';
				content +='<tr>';
				content +='<th class="text-left">NO</th>';
				content +='<th class="text-left">NO PENDAFTARAN</th>';
				content +='<th class="text-left">DATA PASIEN</th>';
				content +='<th class="text-left">KORESPONDEN</th>';
				$('#index_list thead').empty();
				content +=data.detail;
				console.log(data.no)
				var kolom=[];
				for (let i = 0; i < data.no; i++) {
					kolom[i]=i+4;
				 
				}
				content +='</tr>';
				$('#index_list thead').append(content);
				$('#index_list tbody').empty();
				$('#index_list').DataTable().destroy();
				
				// $("#cover-spin").show();
				var table = $('#index_list').removeAttr('width').DataTable({
					 scrollY:        "450px",
					scrollX:        true,
					scrollCollapse: true,
					searching:         false,
					paging:         false,
					serverSide: true,
					"processing": true,
					"ordering": false,
					fixedColumns:   {
						left: 4,
						right: 1
					},
					"ajax": {
						url: '{site_url}lsurvey/loadexternal/',
						type: "POST",
						dataType: 'json',
						data: {
							msurvey_kepuasan_id: msurvey_kepuasan_id,tanggal_1_trx: tanggal_1_trx,
							tanggal_2_trx: tanggal_2_trx,tipe: tipe,
						}
					},
					fixedColumns:   {
						left: 3,
						right: 1
					},
					"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
						$("#cover-spin").hide();
						if (aData[0]==''){
							$('td', nRow).css('background-color', '#e9e9e9');					
						}
					},
					
					columnDefs: [				
						// { "visible": false, "targets": [0,1,2]},
						{ "width": "250px", "targets": [2]},
						{ "width": "200px", "targets": [3]},
						{ "width": "120px", "targets": kolom},
						{ className: "text-center","targets": kolom},
						
					]
					// columnDefs: [
						// { 
						// "targets" : [ 2 ],
								  // render : function (data, type, row) {
									   
									// if(row.status == 'yes' && row.quantity > 0){
									 
									// return '<button class="btn btn-secondary btn-block">view</button>
									 
									// }else if(row.status == 'yes' && row.quantity == 0){
									 
									// return '<button class="btn btn-secondary btn-block">Delete</button>';
									 
									// }
						 
						// }
						
					// ]
				});
				
				
				
			}
		});
}
// $(document).on("click", "#btn_print", function() {
	
		// print_barang();
// });
// function print_barang() {
	// $.ajax({        
	   // url: '{site_url}lgudang_stok/print_barang/',
		// type: "POST",
		// data: {
			// idunit: idunit,
			// idtipe: idtipe,
			// idkategori: idkategori,
			// statusstok: statusstok,
			// order_by: order_by,
		// }
			   
// }); 
// }
function clear_data(){
	
	$('#index_list').DataTable().destroy();
	$('#index_list tbody').empty();
	var table = $('#index_list').DataTable( {
		"searching": true,
		columnDefs: []
	} );
}
function load_tipe(){
	clear_data();
	var idunit=$("#idunitpelayanan").val();
	$('#idtipe').find('option').remove();
	
	$('#idkategori').find('option').remove();
	$('#idkategori').append('<option value="#" selected>- Pilih Semua -</option>');
	var def='';
	if (idunit !='#'){
			$('#idtipe').append('<option value="#" selected>- Pilih Semua -</option>');
					
			
	}
}
$("#idtipe").change(function(){
	load_kategori();
});
function load_kategori(){
	var idtipe=$("#idtipe").val();
	$('#idkategori').find('option').remove();
	$('#idkategori').append('<option value="#" selected>- Pilih Semua -</option>');
	if (idtipe !='#'){
					
			$.ajax({
				url: '{site_url}tstockopname/list_kategori/'+idtipe,
				dataType: "json",
				success: function(data) {
					$.each(data, function (i,row) {
						
					$('#idkategori')
					.append('<option value="' + row.path + '">' + TreeView(row.level, row.nama) + '</option>');
					});
				}
			});

	}
}
function TreeView($level, $name)
{
	// console.log($name);
	$indent = '';
	for ($i = 0; $i < $level; $i++) {
		if ($i > 0) {
			$indent += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		} else {
			$indent += '';
		}
	}

	if ($i > 0) {
		$indent += '└─';
	} else {
		$indent += '';
	}

	return $indent + ' ' +$name;
}
</script>
