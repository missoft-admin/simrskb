<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mrka_belanja" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title} </h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mrka_belanja/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Nama Pemohon</label>
				<div class="col-md-4">
					<input type="hidden" class="form-control" id="id" placeholder="Nama RKA" name="id" value="{id}" required="" aria-required="true">
					<input type="text" readonly class="form-control" id="nama_pemohon"  disabled  placeholder="Nama Pemohon" name="nama_pemohon" value="{nama_pemohon}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="disabel" placeholder="Nama RKA" name="disabel" value="{disabel}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="idrka_kegiatan" placeholder="Nama RKA" name="idrka_kegiatan" value="{idrka_kegiatan}" required="" aria-required="true">
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="bulan" id="bulan" value="<?=$bulan?>" required="" aria-required="true">
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="idlogic" id="idlogic" value="<?=$idlogic?>">
				</div>
				<label class="col-md-2 control-label" for="nama">Unit Yang Mengajukan</label>
				<div class="col-md-4">
					<select id="idunit_pengaju"  disabled  <?=($tipe_rka=='1'?'disabled':'')?> name="idunit_pengaju" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Unit -</option>
						<? foreach($list_unit as $row){ ?>
							<option value="<?=$row->idunit?>" <?=($idunit_pengaju==$row->idunit?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Tanggal Pengajuan</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input"  disabled  type="text" id="tanggal_pengajuan" name="tanggal_pengajuan" value="<?=$tanggal_pengajuan?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="nama"></label>
				<label class="col-md-2 control-label" for="nama">Tanggal Dibutuhkan</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input"  disabled  type="text" id="tanggal_dibutuhkan" name="tanggal_dibutuhkan" value="<?=$tanggal_dibutuhkan?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Untuk  Unit</label>
				<div class="col-md-4">
					<select id="idunit" name="idunit"  disabled  class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Unit -</option>
						<? foreach($list_unit as $row){ ?>
							<option value="<?=$row->idunit?>" <?=($idunit==$row->idunit?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
				<label class="col-md-2 control-label" for="nama">Jenis Pengajuan</label>
				<div class="col-md-4">
					<select id="idjenis" name="idjenis"   disabled  class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Jenis -</option>
						<? foreach($list_jenis as $row){ ?>
							<option value="<?=$row->id?>" <?=($idjenis==$row->id?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Tipe Pengajuan</label>
				<div class="col-md-4">
					<select id="tipe_rka" disabled name="tipe_rka" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="1" <?=$tipe_rka=='1'?'selected':''?>>RKA</option>
						<option value="2" <?=$tipe_rka=='2'?'selected':''?>>NON RKA</option>						
					</select>
				</div>
				<label class="col-md-2 control-label" for="nama">Nama Kegiatan</label>
				<div class="col-md-4">
					<? if ($tipe_rka=='1'){ ?>
					<div class="input-group date">
						<input type="text" class="form-control"  disabled  readonly id="nama_kegiatan"  disabled  placeholder="Nama Kegiatan" name="nama_kegiatan" value="{nama_kegiatan}">
						<span class="input-group-btn">
							<button class="btn btn-primary"  disabled  type="button" id="btn_open_modal_kegiatan" title="Terapkan sama rata"><i class="fa fa-search"></i></button>
						</span>
					</div>
					<?}else{?>
					<input type="text" class="form-control"  disabled   disabled  placeholder="Nama Kegiatan" name="nama_kegiatan" value="{nama_kegiatan}">
					<?}?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Vendor</label>
				<div class="col-md-4">
					<div class="input-group">
						<select id="idvendor" name="idvendor" <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" <?=($idvendor=='#'?'selected':'')?>>- Pilih Vendor -</option>
							<option value="0" <?=($idvendor=='0'?'selected':'')?>>Tentukan Vendor Nanti</option>
							<? foreach($list_vendor as $row){ ?>
								<option value="<?=$row->id?>" <?=($idvendor==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
						<span class="input-group-btn">
							<button class="btn btn-success" <?=$disabel?> type="button" id="btn_refresh_vendor" title="Refresh"><i class="fa fa-refresh"></i></button>
							<a href="{base_url}mvendor" target="_blank" class="btn btn-primary" <?=$disabel?> type="button" id="btn_add_vendor" title="Tambah Vendor"><i class="fa fa-plus"></i></a>
						</span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="nama">Klasifikasi</label>
				<div class="col-md-4">
					<select id="idklasifikasi" name="idklasifikasi" <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" <?=($idklasifikasi=='#'?'selected':'')?>>- Pilih Klasifikasi -</option>
							<? foreach($list_klasifikasi as $row){ ?>
								<option value="<?=$row->id?>" <?=($idklasifikasi==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
					
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Catatan</label>
				<div class="col-md-10">
						<textarea class="form-control" rows="2" name="catatan" id="catatan"><?=$catatan?></textarea>
						
				</div>
				
			</div>
			<div class="progress progress-mini">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<h5 style="margin-bottom: 10px;">DETAIL PENGAJUAN</h5>
			
			<div class="form-group">
				<table id="tabel_add" class="table table-striped table-bordered" style="margin-bottom: 0;">
					<thead>
						<tr>
							<th style="width: 15%;">Nama Barang</th>
							<th style="width: 15%;">Merk Barang</th>
							<th style="width: 5%;">QTY</th>
							<th style="width: 5%;">Satuan</th>
							<th style="width: 10%;">Harga</th>
							<th style="width: 10%;">Total</th>
							<th style="width: 15%;">Keterangan</th>
							<th style="width: 8%;">Actions</th>
						</tr>
						<tr hidden>							
							<td><input class="form-control input-sm" tabindex="2" type="text" id="nama_barang" /></td>
							<td><input class="form-control input-sm" tabindex="3" type="text" id="merk_barang" /></td>
							<td><input class="form-control input-sm number" tabindex="4" type="text" id="kuantitas" /></td>
							<td><input class="form-control input-sm" tabindex="5" type="text" id="satuan" /></td>
							<td>
								<div class="input-group date">
								<input class="form-control input-sm decimal" readonly tabindex="6" type="text" id="harga_satuan" />
								<input class="form-control input-sm decimal" readonly tabindex="6" type="hidden" id="harga_pokok" />
								<input class="form-control input-sm decimal" readonly tabindex="6" type="hidden" id="harga_diskon" />
								<input class="form-control input-sm decimal" readonly tabindex="6" type="hidden" id="harga_ppn" />
								<input class="form-control input-sm " readonly tabindex="6" type="hidden" id="idklasifikasi_det" />
									<span class="input-group-btn">
										<button class="btn btn-primary btn-sm" <?=$disabel?> type="button" id="btn_open_modal_harga" title="Terapkan sama rata"><i class="fa fa-pencil"></i></button>
									</span>
								</div>
							</td>							
							<td><input class="form-control input-sm number" readonly  type="text" id="total_harga" /></td>
							<td><input class="form-control input-sm" tabindex="7" type="text" id="keterangan" /></td>
							<td>
								<button type="button"  disabled  class="btn btn-sm btn-primary kunci" tabindex="8" id="addjual" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
								<button type='button'  disabled  class='btn btn-sm btn-danger kunci' tabindex="9" id="canceljual" title="Refresh"><i class='fa fa-times'></i></button>
							</td>
						</tr>
					</thead>
					<tbody>
						<? 
						$gt=0;
						$item=0;
						foreach($list_detail as $row){ 
							$gt=$gt + $row->total_harga;
							$item=$item + 1;
						?>
							<tr>
								<td><?=$row->nama_barang?></td>
								<td><?=$row->merk_barang?></td>
								<td><?=$row->kuantitas?></td>
								<td><?=$row->satuan?></td>
								<td><?=number_format($row->harga_satuan).'<br>└─ Hpp. '.number_format($row->harga_pokok).'<br>└─ Disc. '.number_format($row->harga_diskon).'<br>└─ PPN. '.number_format($row->harga_ppn)?></td>
								<td><?=number_format($row->total_harga)?></td>
								<td><?=$row->keterangan?></td>
								<td><? echo "<div class='btn-group'><button type='button' ".$disabel." disabled class='btn btn-xs btn-info edit kunci'><i class='glyphicon glyphicon-pencil'></i></button><button type='button' ".$disabel." disabled class='btn btn-xs btn-danger hapus kunci'><i class='glyphicon glyphicon-remove'></i></button></div>"?></td>
								<td style='display:none'><input type='text' name='xiddet[]' value="<?=$row->id?>"></td>
								<td style='display:none'><input type='text' name='xnama_barang[]' value="<?=$row->nama_barang?>"></td>
								<td style='display:none'><input type='text' name='xmerk_barang[]' value="<?=$row->merk_barang?>"></td>
								<td style='display:none'><input type='text' name='xkuantitas[]' value="<?=$row->kuantitas?>"></td>
								<td style='display:none'><input type='text' name='xsatuan[]' value="<?=$row->satuan?>"></td>
								<td style='display:none'><input type='text' name='xharga_satuan[]' value="<?=$row->harga_satuan?>"></td>
								<td style='display:none'><input type='text' name='xtotal_harga[]' value="<?=$row->total_harga?>"></td>
								<td style='display:none'><input type='text' name='xketerangan[]' value="<?=$row->keterangan?>"></td>
								<td style='display:none'><input type='text' name='xstatus[]' value="<?=$row->status?>"></td>
								<td style='display:none'><input type='text' name='xharga_pokok[]' value="<?=$row->harga_pokok?>"></td>
								<td style='display:none'><input type='text' name='xharga_diskon[]' value="<?=$row->harga_diskon?>"></td>
								<td style='display:none'><input type='text' name='xharga_ppn[]' value="<?=$row->harga_ppn?>"></td>
								<td style='display:none'><input type='text' name='xidklasifikasi_det[]' value="<?=$row->idklasifikasi_det?>"></td>
							</tr>
						<?}?>
					</tbody>
					<tfoot id="foot-total-nonracikan">
						<tr>
							<th colspan="5" class="hidden-phone">

								<span class="pull-right"><b>TOTAL</b></span></th>
							<input type="hidden" id="rowindex" name="rowindex" value="">
							<input type="hidden" id="iddet" name="iddet" value="">
							<th><input class="form-control input-sm number" readonly type="text" id="grand_total" name="grand_total" value="<?=$gt?>"/></th>
							<th><input class="form-control input-sm number"  type="hidden" id="total_item" name="total_item" value="<?=$item?>"/></th>
							<th><input class="form-control input-sm number"  type="hidden" id="total_jenis_barang" name="total_jenis_barang" value="<?=$item?>"/></th>
							
						</tr>
					</tfoot>
				</table>
				
			</div>
			<div class="progress progress-mini">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama"><h4><span class="label label-primary">Cara Pembayaran</span></h4></label>
				<div class="col-md-4">
					<select id="cara_pembayaran" name="cara_pembayaran"  disabled  class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" <?=($cara_pembayaran=='#'?'selected':'')?>>- Pilih Cara Pembayaran -</option>
						<option value="0" <?=($cara_pembayaran=='0'?'selected':'')?>>Tentukan Cara Pembayaran Nanti</option>
						<option value="1" <?=($cara_pembayaran=='1'?'selected':'')?>>Satu Kali Bayar</option>
						<option value="2" <?=($cara_pembayaran=='2'?'selected':'')?>>Cicilan</option>
						<option value="3" <?=($cara_pembayaran=='3'?'selected':'')?>>By Termin</option>
						
					</select>						
				</div>				
			</div>
			<div class="form-group" style="display:block" id="div_satu">
				<label class="col-md-2 control-label" for="nama"><h4><span class="label label-primary">Jenis Pembayaran</span></h4></label>
				<div class="col-md-4">
					<select id="jenis_pembayaran" name="jenis_pembayaran"  disabled  class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" <?=($jenis_pembayaran=='#'?'selected':'')?>>- Pilih Jenis Pembayaran -</option>
						<option value="0" <?=($jenis_pembayaran=='0'?'selected':'')?>>Tentukan Jenis Pembayaran Nanti</option>
						<option value="1" <?=($jenis_pembayaran=='1'?'selected':'')?>>Tunai</option>
						<option value="2" <?=($jenis_pembayaran=='2'?'selected':'')?>>Transfer</option>
						<option value="3" <?=($jenis_pembayaran=='3'?'selected':'')?>>Kartu Kredit</option>
						<option value="4" <?=($jenis_pembayaran=='4'?'selected':'')?>>Kontrabon</option>
						
					</select>						
				</div>				
			</div>
			<div id="div_tf">
				<div class="form-group" style="display:block">
					<label class="col-md-2 control-label" for="nama">No Rekening</label>
					<div class="col-md-4">
						<input class="form-control input-sm "  disabled  type="text" id="norek" name="norek" value="{norek}"/>			
					</div>
					<label class="col-md-2 control-label" for="nama">Bank</label>
					<div class="col-md-4">
						<input class="form-control input-sm "  disabled  type="text" id="bank" name="bank" value="{bank}"/>			
					</div>				
				</div>
				<div class="form-group" style="display:block" style="margin-bottom: 10px;">
					<label class="col-md-2 control-label" for="nama">Atas Nama</label>
					<div class="col-md-4">
						<input class="form-control input-sm " disabled type="text" id="atasnama" name="atasnama" value="{atasnama}"/>			
					</div>
								
				</div>
				<div class="form-group" style="display:block">
					<label class="col-md-2 control-label" for="nama">Keterangan</label>
					<div class="col-md-10">
						<textarea class="form-control" rows="2"  disabled   name="keterangan_bank" id="keterangan_bank"><?=$keterangan_bank?></textarea>	
					</div>
								
				</div>
			</div>
			<div id="div_kbo">
				<div class="form-group" style="display:block">
					<label class="col-md-2 control-label" for="nama">Tanggal Kontrabon</label>
					<div class="col-md-4">
						<div class="input-group date">
							<input class="js-datepicker form-control input"  disabled  type="text" id="tanggal_kontrabon" name="tanggal_kontrabon" value="<?=($tanggal_kontrabon?HumanDateShort($tanggal_kontrabon):'')?>" data-date-format="dd-mm-yyyy"/>
							<span class="input-group-addon">
								<span class="fa fa-calendar"></span>
							</span>
						</div>
					</div>
								
				</div>
				
			</div>
			<div id="div_cicilan">
				<div class="form-group" style="display:block">
					<label class="col-md-2 control-label" for="nama"><h4><span class="label label-primary">Pembayaran Cicilan</span></h4></label>
					<div class="col-md-10">
						<table id="tabel_dp_cicilan" class="table table-striped table-bordered" style="margin-bottom: 0;">
							<thead>
								<tr>
									<th style="width: 30%;">Nominal DP</th>
									<th style="width: 30%;">Tanggal</th>
									<th style="width: 30%;">Keterangan</th>
									<th style="width: 10%;">Actions</th>
								</tr>
								<tr hidden>							
									<td><input class="form-control input-sm number" type="text" id="xnominal_cicilan" /></td>
									<td>
										<div class="input-group date">
											<input class="js-datepicker form-control input-sm"   disabled  type="text" id="xtanggal_cicilan" value="" data-date-format="dd-mm-yyyy"/>
											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
										</div>
									</td>
									<td><input class="form-control input-sm" type="text" id="xketerangan_cicilan" /></td>
									<td>
										<button type="button"  disabled  class="btn btn-sm btn-primary kunci" tabindex="5" id="add_dp_cicilan" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
										<button type='button'  disabled  class='btn btn-sm btn-danger kunci' tabindex="9" id="cancel_dp_cicilan" title="Refresh"><i class='fa fa-times'></i></button>
									</td>
								</tr>
							</thead>
							<tbody>
								<? foreach($list_dp_cicilan as $row){ ?>
									<tr>
									<td><input class="form-control input-sm number" readonly type="text" name="nominal_cicilan[]" value="<?=$row->nominal_cicilan?>"/></td>
									<td>
										<div class="input-group date">
											<input readonly class="js-datepicker form-control input-sm"  type="text" name="tanggal_cicilan[]" value="<?=HumanDateShort($row->tanggal_cicilan)?>" data-date-format="dd-mm-yyyy"/>
											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
										</div>
									</td>
									<td><input readonly class="form-control input-sm" type="text" value="<?=$row->keterangan_cicilan?>" name="keterangan_cicilan[]" /></td>
									<td>
										<button type="button"  disabled  class="btn btn-sm btn-danger hapus_dp_cicilan" tabindex="9" title="Refresh"><i class="fa fa-times"></i></button>
									</td>
									<td style="display:none"><input readonly class="form-control input-sm number" type="text" name="jenis_cicilan[]" value="<?=$row->jenis_cicilan?>"/></td>
									</tr>
								<?}?>
							</tbody>
							
						</table>
					</div>
								
				</div>
				<div class="form-group" style="display:block">
					<label class="col-md-2 control-label" for="nama">Total DP</label>
					<div class="col-md-4">
						<input readonly class="form-control input-sm number" type="text" id="total_dp_cicilan" name="total_dp_cicilan" value="{total_dp_cicilan}"/>
					</div>
					<label class="col-md-2 control-label" for="nama">Sisa Untuk dicicil</label>
					<div class="col-md-4">
						<input readonly class="form-control input-sm number" type="text" id="sisa_cicilan" name="sisa_cicilan" value="{sisa_cicilan}"/>
						<input readonly class="form-control input-sm" type="hidden" id="rowindex_dp_cicilan" />
					</div>
								
				</div>
				<div class="form-group" style="display:block">
					<label class="col-md-2 control-label" for="nama">Jumlah cicilan</label>
					<div class="col-md-4">
						<input class="form-control input-sm number" type="text" id="jml_kali_cicilan" name="jml_kali_cicilan" value="{jml_kali_cicilan}" />
					</div>
					<label class="col-md-2 control-label" for="nama">Cicilan Per bulan</label>
					<div class="col-md-4">
						<input readonly class="form-control input-sm number" type="text" id="per_bulan_cicilan" name="per_bulan_cicilan" value="{per_bulan_cicilan}"/>
					</div>
					
								
				</div>
				<div class="form-group" style="display:block">
					<label class="col-md-2 control-label" for="nama">Cicilan Ke 1</label>
					<div class="col-md-4">
						<div class="input-group date">
							<input class="js-datepicker form-control input-sm"   disabled  type="text" name="xtanggal_cicilan_pertama" id="xtanggal_cicilan_pertama" value="<?=($xtanggal_cicilan_pertama?HumanDateShort($xtanggal_cicilan_pertama):'')?>" data-date-format="dd-mm-yyyy"/>
							<span class="input-group-addon">
								<span class="fa fa-calendar"></span>
							</span>
						</div>
					</div>
					
					<div class="col-md-4">
						<button type="button"  disabled  class="btn btn-sm btn-success kunci" tabindex="5" id="generate_cicilan" title="Masukan Item"><i class="fa fa-level-down"></i> Generate Cicilan</button>
					</div>
				</div>
				<div class="form-group" style="display:block">
					<label class="col-md-2 control-label" for="nama"></label>
					<div class="col-md-8">
						<table id="tabel_cicilan" class="table table-striped table-bordered" style="margin-bottom: 0;">
							<thead>
								<tr>
									<th style="width: 30%;">Keterangan</th>
									<th style="width: 30%;">Tanggal</th>
									<th style="width: 30%;">Cicilan</th>
								</tr>								
							</thead>
							<tbody>
								<? foreach($list_cicilan as $row){ ?>
									<tr>
										<td><input readonly class="form-control input-sm" type="text" name="keterangan_cicilan[]" value="<?=$row->keterangan_cicilan?>"/></td>
										<td><input readonly class="form-control input-sm" type="text" name="tanggal_cicilan[]" value="<?=HumanDateShort($row->tanggal_cicilan)?>"/></td>
										<td><input readonly class="form-control input-sm number" type="text" name="nominal_cicilan[]" value="<?=$row->nominal_cicilan?>"/></td>
										<td style="display:none"><input readonly class="form-control input-sm number" type="text" name="jenis_cicilan[]" value="<?=$row->jenis_cicilan?>"/></td>
									</tr>
								<?}?>
							</tbody>
							
						</table>
					</div>
								
				</div>
			</div>
			<div id="div_termin">
				<div class="form-group" style="display:block">
					<label class="col-md-2 control-label" for="nama"><h4><span class="label label-primary">Jenis Termin Fix</span></h4></label>
					<div class="col-md-10">
						<table id="tabel_dp_termin" class="table table-striped table-bordered" style="margin-bottom: 0;">
							<thead>
								<tr>
									<th style="width: 15%;">Jenis</th>
									<th style="width: 20%;">Judul Termin</th>
									<th style="width: 20%;">Deskrpsi</th>
									<th style="width: 15%;">Nominal</th>
									<th style="width: 15%;">Tanggal</th>
									<th style="width: 10%;">Actions</th>
								</tr>
								<tr hidden>							
									<td>
										<select id="xjenis_termin" name="xjenis_termin" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
											<option value="#" selected>Pilih Jenis Pembayaran</option>
											<option value="1" >Down Payment</option>
											<option value="2" >Pembayaran</option>							
										</select>
									</td>
									<td><input class="form-control input-sm" type="text" id="xjudul_termin" /></td>
									<td><input class="form-control input-sm" type="text" id="xdeskripsi_termin" /></td>
									<td><input class="form-control input-sm number" type="text" id="xnominal_termin" /></td>
									<td>
										<div class="input-group date">
											<input class="js-datepicker form-control input-sm"   disabled  type="text" id="xtanggal_termin" value="" data-date-format="dd-mm-yyyy"/>
											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
										</div>
									</td>
									<td>
										<button type="button"  disabled  class="btn btn-sm btn-primary kunci" tabindex="5" id="add_dp_termin" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
										<button type='button'  disabled  class='btn btn-sm btn-danger kunci' tabindex="9" id="cancel_dp_termin" title="Refresh"><i class='fa fa-times'></i></button>
									</td>
								</tr>
							</thead>
							<tbody>
								<? foreach($list_dp_termin as $row){ ?>
									<tr>
									<td><input class="form-control input-sm" readonly type="text" value="<?=($row->jenis_termin=='1'?'Down Payment':'Pembayaran')?>"/></td>
									<td><input class="form-control input-sm" readonly type="text" name="judul_termin[]" value="<?=$row->judul_termin?>"/></td>
									<td><input class="form-control input-sm" readonly type="text" name="deskripsi_termin[]" value="<?=$row->deskripsi_termin?>"/></td>
									<td><input class="form-control input-sm number" readonly type="text" name="nominal_termin[]" value="<?=$row->nominal_termin?>"/></td>
									<td>
										<div class="input-group date">
											<input readonly class="js-datepicker form-control input-sm"  type="text" name="tanggal_termin[]" value="<?=HumanDateShort($row->tanggal_termin)?>" data-date-format="dd-mm-yyyy"/>
											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
										</div>
									</td>
									<td>
										<button type="button"  disabled  class="btn btn-sm btn-danger hapus_termin" tabindex="9" title="Refresh"><i class="fa fa-times"></i></button>
									</td>
									<td style="display:none"><input readonly class="form-control input-sm number" type="text" name="jenis_termin[]" value="<?=$row->jenis_termin?>"/></td>
									<td style="display:none"><input readonly class="form-control input-sm number" type="text" name="status_dibayar_termin[]" value="<?=$row->status_dibayar_termin?>"/></td>
									</tr>
								<?}?>
							</tbody>
							
						</table>
					</div>
								
				</div>
				<div class="form-group" style="display:block">
					<label class="col-md-2 control-label" for="nama">Total Pembayaran</label>
					<div class="col-md-4">
						<input readonly class="form-control input-sm number" type="text" id="total_dp_termin" name="total_dp_termin" value="{total_dp_termin}"/>
					</div>
					<label class="col-md-2 control-label" for="nama">Sisa Untuk dicicil</label>
					<div class="col-md-4">
						<input readonly class="form-control input-sm number" type="text" id="sisa_termin" name="sisa_termin" value="{sisa_termin}"/>
						<input readonly class="form-control input-sm" type="hidden" id="rowindex_dp_termin" />
					</div>
								
				</div>
				
			</div>
			<div class="progress progress-mini">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<!-- Static Labels block-opt-hidden -->
					<div class="block block-themed block-opt-hidden" >
						<div class="block-header bg-primary">
							<ul class="block-options">
								<li>
									<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="si si-user-following"></i>  User Approval</h3>
						</div>
						<div class="block-content block-content">
							

								<input class="form-control"  value="0" type="hidden" readonly id="st_load_berkas">
								<table width="100%" id="tabel_user_proses" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Step</th>
											<th>User</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							
						</div>
					</div>
					<!-- END Static Labels -->
				</div>

			</div>
			<div class="row">
				<div class="col-md-12">
					<!-- Static Labels block-opt-hidden -->
					<div class="block block-themed block-opt-hidden" >
						<div class="block-header bg-primary">
							<ul class="block-options">
								<li>
									<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="si si-docs"></i>  MEMO</h3>
						</div>
						<div class="block-content block-content">
							<textarea class="form-control js-summernote" name="memo_bendahara" id="memo_bendahara"><?=$memo_bendahara?></textarea>
						</div>
					</div>
					<!-- END Static Labels -->
				</div>

			</div>
			
			<div class="row">
				<div class="col-md-12">
					<!-- Static Labels block-opt-hidden -->
					<div class="block block-themed block-opt-hidden" >
						<div class="block-header bg-primary">
							<ul class="block-options">
								<li>
									<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="si si-info"></i>  Update Informasi</h3>
						</div>
						<div class="block-content block-content">
							<div class="form-group">
								<div class="col-md-12">
									<textarea class="form-control js-summernote" name="informasi" id="informasi"></textarea>
									
								</div>
								
							</div>
							<div class="form-group">
									<label class="col-md-2 control-label" for="nama">Reminder</label>
									<div class="col-md-4">
										<div class="input-group date">
										<input class="js-datepicker form-control input"  type="text" id="tgl_reminder" name="tgl_reminder" value="" data-date-format="dd-mm-yyyy"/>
										<span class="input-group-addon">
											<span class="fa fa-calendar"></span>
										</span>
										</div>									
									</div>
									
									<div class="col-md-4">
										<button class="btn btn-primary" type="button" id="btn_tambah_informasi"><i class="fa fa-plus"></i> Tambah</button>
										<button class="btn btn-danger" type="button" id="btn_clear_data"><i class="fa fa-refresh"></i> Clear Data</button>						
									</div>
									<div class="col-md-2">
										<input class="form-control input"  type="hidden" id="informasi_id" name="informasi_id" value=""/>
									</div>
								
							</div>
							<hr>
							<div class="form-group">
								<table width="100%" id="tabel_informasi" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>NO</th>
											<th>TANGGAL</th>
											<th>INFORMASI</th>
											<th>USER</th>
											<th>REMINDER</th>
											<th>ACTION</th>
											<th hidden>ID</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>								
							</div>
						</div>
					</div>
					<!-- END Static Labels -->
				</div>
			</div>
			<?php echo form_close() ?>
			<div class="row">
				<div class="col-md-12">
					<!-- Static Labels block-opt-hidden -->
					<div class="block block-themed block-opt-hidden" >
						<div class="block-header bg-primary">
							<ul class="block-options">
								<li>
									<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="si si-docs"></i>  UPLOAD DOC</h3>
						</div>
						<div class="block-content block-content">
							<div class="col-md-12" style="margin-top: 10px;">
								<div class="block" id="box_file2">					
									 <table class="table table-bordered" id="list_file">
										<thead>
											<tr>
												<th width="50%" class="text-center">File</th>
												<th width="15%" class="text-center">Size</th>
												<th width="35%" class="text-center">User</th>
												<th width="35%" class="text-center">X</th>
												
											</tr>
										</thead>
										<tbody>
											
										</tbody>
									</table>
								</div>
								<div class="block">
									<form action="{base_url}mrka_bendahara/upload" enctype="multipart/form-data" class="dropzone" id="image-upload">
										<input type="hidden" class="form-control" name="idrka" id="idrka" value="{id}" readonly>
										<div>
										  <h5>Bukti Upload</h5>
										</div>
									</form>
								</div>
								
							</div> 
						</div>
					</div>
					<!-- END Static Labels -->
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<!-- Static Labels block-opt-hidden -->
					<div class="block block-themed" >
						<div class="block-header bg-success">
							<ul class="block-options">
								<li>
									<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="glyphicon glyphicon-ok"></i>  PROSES SELESAI</h3>
						</div>
						<div class="block-content block-content">
							<div class="form-group">
								<label class="col-md-3 control-label" for="nama">No Pembayaran </label>
								<div class="col-md-7">
									<input  type="text" class="form-control" id="bukti_pembayaran" placeholder="No Pembayaran" name="bukti_pembayaran">
								</div>
							</div>
							
							<div class="col-md-12" style="margin-top: 10px;">
								<div class="block" id="box_file2">					
									 <table class="table table-bordered" id="list_file_bukti">
										<thead>
											<tr>
												<th width="50%" class="text-center">File</th>
												<th width="15%" class="text-center">Size</th>
												<th width="35%" class="text-center">User</th>
												<th width="35%" class="text-center">X</th>
												
											</tr>
										</thead>
										<tbody>
											
										</tbody>
									</table>
								</div>
								<div class="block">
									<form action="{base_url}mrka_bendahara/upload_bukti" enctype="multipart/form-data" class="dropzone" id="image_bukti">
										<input type="hidden" class="form-control" name="idrka" id="idrka" value="{id}" readonly>
										<div>
										  <h5>Upload Bukti Pembayaran</h5>
										</div>
									</form>
								</div>
								
							</div> 
						</div>
					</div>
					<!-- END Static Labels -->
				</div>
			</div>
			<? if ($cara_pembayaran=='1'){?>
			<div class="row" hidden>
				<div class="col-md-12">
					<!-- Static Labels block-opt-hidden -->
					<div class="block block-themed" >
						<div class="block-header bg-primary">
							<ul class="block-options">
								<li>
									<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="fa fa-money"></i>  PEMBAYARAN</h3>
						</div>
						<div class="block-content block-content">
								<div class="form-group">
									<label class="col-md-2 control-label" for="nama">Total Transaksi</label>
									<div class="col-md-3">
										<input class="form-control input number" readonly type="text" id="total_trx" name="total_trx" value="<?=$gt?>"/>										
									</div>									
									<div class="col-md-4">
										<button class="btn btn-success" type="button" id="btn_add_pembayaran"><i class="fa fa-plus"></i> Tentukan Cara Pembayaran</button>
									</div>
									<div class="col-md-2">
										<input class="form-control input"  type="hidden" id="informasi_id" name="informasi_id" value=""/>
									</div>
								</div>
								<hr>
								<div class="form-group">
									 <table class="table table-bordered" id="list_pembayaran">
										<thead>
											<tr>
												<th width="5%" class="text-center">NO</th>
												<th width="10%" class="text-center">Jenis Kas</th>
												<th width="15%" class="text-center">Sumber Kas</th>
												<th width="10%" class="text-center">Metode</th>
												<th width="10%" class="text-center">Nominal</th>
												<th width="10%" class="text-center">Tgl Pencairan</th>
												<th width="15%" class="text-center">Keterangan</th>
												<th width="15%" class="text-center">Actions</th>
												
											</tr>
										</thead>
										<tbody>											
										</tbody>
										<tfoot>
											<tr>
												<td colspan="5" class="text-right"> <strong>Total Bayar </strong></td>
												<td colspan="2"> <input class="form-control input number" readonly type="text" id="total_bayar" name="total_bayar" value="0"/></td>
											</tr>
											<tr>
												<td colspan="5" class="text-right"> <strong>Sisa</strong></td>
												<td colspan="2"><input class="form-control input number" readonly type="text" id="total_sisa" name="total_sisa" value="0"/> </td>
											</tr>
										</tfoot>
									</table>
								</div>
								
							</div> 
					</div>
					<!-- END Static Labels -->
				</div>
			</div>
			<?}?>
			<?if ($disabel==''){?>
			<div class="form-group">
				<div class="text-right bg-light">
					<a href="{base_url}mrka_belanja" class="btn btn-success" type="submit" id="btn_simpan_biasa" name="btn_simpan_biasa">Simpan</a>
					<? if ($st_validasi=='1' && $status=='4'){?>
					<?php if (UserAccesForm($user_acces_form,array('1416'))){ ?>
					<button class="btn btn-primary" type="submit" id="btn_simpan" name="btn_simpan">Simpan & Selesai </button>
					<?}?>
					<?}?>
				</div>
			</div>
			<?}?>
			
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_pembayaran" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Pembayaran</h3>
				</div>
				<div class="block-content">
				<?php echo form_open('#','class="form-horizontal push-10-t" ') ?>
				<div class="form-group">
					<label class="col-md-3 control-label" for="nama">Sisa Rp. </label>
					<div class="col-md-7">
						<input readonly type="text" class="form-control number" id="sisa_modal" placeholder="Sisa" name="sisa_modal" required="" aria-required="true">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="jenis_kas_id">Jenis Kas</label>
					<div class="col-md-7">
						<select name="jenis_kas_id" id="jenis_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#">- Pilih Opsi -</option>
							<? foreach($list_jenis_kas as $row){ ?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>
							<?}?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="sumber_kas_id">Sumber Kas</label>
					<div class="col-md-7">
						<select name="sumber_kas_id" id="sumber_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">							
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="idmetode">Metode</label>
					<div class="col-md-7">
						<select name="idmetode" id="idmetode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#">Pilih Opsi</option>							
							<option value="1">Cheq</option>	
							<option value="2">Tunai</option>	
							<option value="4">Transfer Antar Bank</option>
							<option value="3">Transfer Beda Bank</option>							
						</select>
					</div>
				</div>
				
				
				<div class="form-group">
					<label class="col-md-3 control-label" for="nama">Nominal Rp. </label>
					<div class="col-md-7">
						<input  type="hidden" class="form-control" id="idpembayaran" placeholder="idpembayaran" name="idpembayaran" >
						<input  type="text" class="form-control number" id="nominal_bayar" placeholder="Nominal" name="nominal_bayar" required="" aria-required="true">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="nama">Tanggal Pencairan</label>
					<div class="col-md-7">
						<div class="input-group date">
						<input class="js-datepicker form-control input"  type="text" id="tanggal_pencairan" name="tanggal_pencairan" value="" data-date-format="dd-mm-yyyy"/>
						<span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-3 control-label" for="nama">Keterangan </label>
					<div class="col-md-7">
						<textarea class="form-control js-summernote" name="ket_pembayaran" id="ket_pembayaran"></textarea>
					</div>
				</div>

				<div class="modal-footer">
					<button class="btn btn-sm btn-primary" id="btn_add_bayar" type="button"><i class="fa fa-check"></i> Add</button>
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Exit</button>
				</div>
				</form>
			</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<?$this->load->view('Mrka_pengajuan/modal_harga')?>
<script type="text/javascript">
	var table;
	var myDropzone 
	$(document).ready(function(){
		Dropzone.autoDiscover = false;
		myDropzone = new Dropzone(".dropzone", { 
		   autoProcessQueue: true,
		   maxFilesize: 30,
		});
		myDropzone.on("complete", function(file) {
		  
		  refresh_image();
		  myDropzone.removeFile(file);
		  
		});
		myDropzone2 = new Dropzone("#image_bukti", { 
		   autoProcessQueue: true,
		   maxFilesize: 30,
		});
		myDropzone2.on("complete", function(file) {
		  
		  refresh_image_bukti();
		  myDropzone2.removeFile(file);
		  
		});
		$(".decimal").number(true,2,'.',',');
		$('.number').number(true, 0, '.', ',');
		clear_input();
		show_hide_pembayaran();
		load_user();
		$('#informasi').summernote({
		  height: 70,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }
		});
		$('#memo_bendahara').summernote({
		  height: 100,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }
		});
		$('#ket_pembayaran').summernote({
		  height: 100,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }
		});
		clear_data_info();
		refresh_image();
		refresh_pembayaran();
		refresh_image_bukti();
	})	
	function refresh_pembayaran(){
		var id=$("#id").val();
		// alert(id);
		$.ajax({
			url: '{site_url}mrka_bendahara/refresh_pembayaran/'+id,
			dataType: "json",
			success: function(data) {
				// if (data.detail!=''){
					$('#total_bayar').val(data.nominal_bayar);
					$('#list_pembayaran tbody').empty();
					$("#list_pembayaran tbody").append(data.detail);
					$("#total_sisa").val($("#total_trx").val()-data.nominal_bayar);
					clear_input_pembayaran();
				// }
				// console.log();
				
			}
		});
	}
	function refresh_image(){
		var id=$("#id").val();
		
		$.ajax({
			url: '{site_url}mrka_bendahara/refresh_image/'+id,
			dataType: "json",
			success: function(data) {
				if (data.detail!=''){
					$('#list_file tbody').empty();
					$("#box_file2").attr("hidden",false);
					$("#list_file tbody").append(data.detail);
				}
				// console.log();
				
			}
		});
	}
	function refresh_image_bukti(){
		var id=$("#id").val();
		
		$.ajax({
			url: '{site_url}mrka_bendahara/refresh_image_bukti/'+id,
			dataType: "json",
			success: function(data) {
				if (data.detail!=''){
					$('#list_file_bukti tbody').empty();
					// $("#box_file2").attr("hidden",false);
					$("#list_file_bukti tbody").append(data.detail);
				}
				// console.log();
				
			}
		});
	}
	$(document).on("click",".edit_pembayaran",function(){
		$("#idpembayaran").val($(this).closest('tr').find("td:eq(8)").text());
		var id=$("#idpembayaran").val();
		var sisa=0;
		$("#modal_pembayaran").modal('show');
		// get_pembayaran
		$.ajax({
			url: '{site_url}mrka_bendahara/get_pembayaran/'+id,
			dataType: "json",
			success: function(data) {
				// alert($("#total_sisa").val());
				var row=data.detail;
				if (data.detail!=''){
					sisa=parseFloat(row.nominal_bayar) + parseFloat($("#total_sisa").val());
					// alert(sisa);
					$("#jenis_kas_id").val(row.jenis_kas_id).trigger('change');
					$("#sumber_kas_id").val(row.sumber_kas_id).trigger('change');
					$("#idmetode").val(row.idmetode).trigger('change');
					$("#nominal_bayar").val(row.nominal_bayar);
					$("#tanggal_pencairan").val(row.tanggal_pencairan);
					
					$("#sisa_modal").val(sisa);
					$("#ket_pembayaran").summernote('code',row.ket_pembayaran);
					
				}
				// console.log(data);
				
			}
		});
	});
	$(document).on("click",".hapus_pembayaran",function(){	
		var currentRow=$(this).closest("tr");          
         var id=currentRow.find("td:eq(8)").text();
		 $.ajax({
			url: '{site_url}mrka_bendahara/hapus_pembayaran/',
			type: 'POST',
			data: {id: id},
			success : function(data) {				
				$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
				refresh_pembayaran();
				// table.ajax.reload( null, false );				
			}
		});
	});
	$(document).on("click",".hapus_file",function(){	
		var currentRow=$(this).closest("tr");          
         var id=currentRow.find("td:eq(4)").text();
		 $.ajax({
			url: '{site_url}mrka_bendahara/hapus_file',
			type: 'POST',
			data: {id: id},
			success : function(data) {				
				$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
				currentRow.remove();
				// table.ajax.reload( null, false );				
			}
		});
	});
	$(document).on("click",".hapus_file_bukti",function(){	
		var currentRow=$(this).closest("tr");          
         var id=currentRow.find("td:eq(4)").text();
		 $.ajax({
			url: '{site_url}mrka_bendahara/hapus_file_bukti',
			type: 'POST',
			data: {id: id},
			success : function(data) {				
				$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
				currentRow.remove();
				// table.ajax.reload( null, false );				
			}
		});
	});
	$(document).on("click","#btn_clear_data",function(){
		clear_data_info();
	});
	$(document).on("click","#btn_simpan",function(){
		if ($("#bukti_pembayaran").val()==''){
			sweetAlert("Maaf...", "No Pembayaran harus Diisi!", "error");
			$("#bukti_pembayaran").focus();
			return false;
		}
		var id=$("#id").val();
		var bukti_pembayaran=$("#bukti_pembayaran").val();
		 $.ajax({
			url: '{site_url}mrka_belanja/selesai',
			type: 'POST',
			data: {id: id,bukti_pembayaran: bukti_pembayaran},
			success : function(data) {			
				 $.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Berhasil'});
				 window.location.href = "{site_url}mrka_belanja";		
			}
		});
	});
	$(document).on("click","#btn_generate",function(){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Generate Cicilan / Termin Fix ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			generate_rka();
		});

		
	});
	function generate_rka(){
		var id=$("#id").val();
		$("#cover-spin").show();
		 $.ajax({
			url: '{site_url}mrka_bendahara/generate_cicilan/'+id,
			dataType: "json",
			success : function(data) {				
				window.location.href = "{site_url}mrka_bendahara";		
			}
		});
	}
	$(document).on("keyup","#nominal_bayar",function(){
		console.log($("#nominal_bayar").val());
		if (parseFloat($("#nominal_bayar").val()) > parseFloat($("#sisa_modal").val())){
			sweetAlert("Maaf...", "Nominal Harus sesuai!", "error");
			$("#nominal_bayar").val($("#sisa_modal").val())
			return false;
		}
	});
	$(document).on("click","#btn_add_bayar",function(){
		add_pembayaran();
	});
	function clear_input_pembayaran(){
		$("#jenis_kas_id").val("#").trigger('change');
		$("#sumber_kas_id").val("#").trigger('change');
		$("#nominal_bayar").val(0);
		$("#sisa_modal").val(0);
		$("#idpembayaran").val('');
		$("#idmetode").val("#").trigger('change');
		$("#ket_pembayaran").summernote('code','');
	}
	function add_pembayaran(){
		var idrka=$("#id").val();
		var jenis_kas_id=$("#jenis_kas_id").val();
		var sumber_kas_id=$("#sumber_kas_id").val();
		var nominal_bayar=$("#nominal_bayar").val();
		var idmetode=$("#idmetode").val();
		var idpembayaran=$("#idpembayaran").val();
		var tanggal_pencairan=$("#tanggal_pencairan").val();
		var ket_pembayaran=$("#ket_pembayaran").summernote('code');
		
		if ($("#jenis_kas_id").val() == "#") {
			sweetAlert("Maaf...", "Jenis Kas Harus Diisi!", "error");
			$("#jenis_kas_id").focus();
			return false;
		}
		if ($("#sumber_kas_id").val() == "#") {
			sweetAlert("Maaf...", "Sumber Kas Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		if ($("#idmetode").val() == "#" || $("#idmetode").val() == "") {
			sweetAlert("Maaf...", "Metode  Harus Diisi!", "error");
			$("#idmetode").focus();
			return false;
		}
		if ($("#nominal_bayar").val() == "" || $("#nominal_bayar").val() == "0") {
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		
		
		$.ajax({
			url: '{site_url}mrka_bendahara/save_pembayaran/',
			dataType: "json",
			type: "POST",
			data: {
				idrka:idrka
				,jenis_kas_id:jenis_kas_id
				,sumber_kas_id:sumber_kas_id
				,nominal_bayar:nominal_bayar				
				,idmetode:idmetode				
				,ket_pembayaran:ket_pembayaran				
				,idpembayaran:idpembayaran				
				,tanggal_pencairan:tanggal_pencairan				
			},
			success: function(data) {
				sweetAlert("Success...", "Pembayaran Berhasil disimpan!", "info");
				refresh_pembayaran();
				$("#modal_pembayaran").modal('hide');
			}
		});
		
	}
	$(document).on("change","#jenis_kas_id",function(){
		var jenis_kas_id=$("#jenis_kas_id").val();
		$.ajax({
			url: '{site_url}mrka_bendahara/list_sumber_kas',
			type: 'POST',
			dataType: "json",
			data: {jenis_kas_id: jenis_kas_id},
			success : function(data) {				
				$("#sumber_kas_id").empty();	
				console.log(data.detail);
				// $("#sumber_kas_id").append(data.detail);	
				$('#sumber_kas_id').append(data.detail);
			}
		});
	});
	$(document).on("click","#btn_add_pembayaran",function(){
		$("#modal_pembayaran").modal('show');
		$("#sisa_modal").val($("#total_sisa").val());
		
	});
	$(document).on("click","#btn_tambah_informasi",function(){
		if ($("#tgl_reminder").val() == "") {
			sweetAlert("Maaf...", "Tanggal Reminder Harus Diisi!", "error");
			$("#tgl_reminder").focus();
			return false;
		}
		// alert($("#informasi").summernote('code'));
		if ($("#informasi").summernote('code') == "<p><br></p>" || $("#informasi").summernote('code') == "") {
			sweetAlert("Maaf...", "Informasi Harus Diisi!", "error");
			$("#informasi").focus();
			return false;
		}
		var idrka=$("#id").val();
		var informasi_id=$("#informasi_id").val();
		var tgl_reminder=$("#tgl_reminder").val();
		var informasi=$("#informasi").summernote('code');
		$.ajax({
			url: '{site_url}mrka_bendahara/save_informasi/',
			dataType: "json",
			type: "POST",
			data: {
				tgl_reminder:tgl_reminder,informasi:informasi,idrka:idrka,informasi_id:informasi_id				
			},
			success: function(data) {
				sweetAlert("Success", "Informasi Berhasil disimpan!", "info");
				clear_data_info();
			}
		});
	});
	function clear_data_info(){
		// $("#informasi").val('');
		$('#informasi').summernote('code','');
		$("#tgl_reminder").val('');
		$("#informasi_id").val('');
		load_informasi();
	}
	function load_informasi(){
		var idrka=$("#id").val();
		
		$.ajax({
			url: '{site_url}mrka_bendahara/load_informasi/'+idrka,
			dataType: "json",
			success: function(data) {
				$("#tabel_informasi tbody").empty();
				$("#tabel_informasi tbody").append(data.detail);
			}
		});
	}
	$(document).on("click",".edit_info",function(){
		$("#informasi_id").val($(this).closest('tr').find("td:eq(6)").text());
		$("#tgl_reminder").val($(this).closest('tr').find("td:eq(4)").text());
		$("#informasi").summernote('code',$(this).closest('tr').find("td:eq(2)").text());
	});
	$(document).on("click",".hapus_info",function(){
		var informasi_id=$(this).closest('tr').find("td:eq(6)").text();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus informasi?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka_bendahara/hapus_informasi/'+informasi_id,
				dataType: "json",
				success: function(data) {
					sweetAlert("Maaf...", "Informasi Berhasil dihapus!", "info");
					clear_data_info();
				}
			});
		});

		
	});
	function load_user(){
		var idrka=$("#id").val();
		$("#modal_user").modal('show');
		
		// alert(idrka);
		$.ajax({
			url: '{site_url}mrka_pengajuan/list_user/'+idrka,
			dataType: "json",
			success: function(data) {
				$("#tabel_user_proses tbody").empty();
				$("#tabel_user_proses tbody").append(data.detail);
			}
		});
	}
	function show_hide_pembayaran(){
		$('#div_cicilan').hide();
		$('#div_satu').hide();
		$('#div_kbo').hide();
		$('#div_termin').hide();
		$('#div_tf').hide();
		if ($("#cara_pembayaran").val()=='1'){//Satu Kali			
			$('#div_satu').show();
			if ($("#jenis_pembayaran").val()=='2' || $("#jenis_pembayaran").val()=='3'){//TF
				$('#div_tf').show();
			}else if ($("#jenis_pembayaran").val()=='4'){//Konta	
				$('#div_kbo').show();
			}
		}else if ($("#cara_pembayaran").val()=='2'){//Cicilan
			$('#div_cicilan').show();
			hitung_total_cicilan();
		}else if ($("#cara_pembayaran").val()=='3'){//Fix
			$('#div_termin').show();
			hitung_total_termin();
		}
	}
	function clear_input(){
		$("#iddet").val('')
		$("#nama_barang").val('')
		$("#merk_barang").val('')
		$("#kuantitas").val('0')
		$("#satuan").val('')
		$("#harga_satuan").val('0')
		$("#total_harga").val('')
		$("#keterangan").val('')
		$("#rowindex").val('')
		if ($("#disabel").val==''){
			$(".edit").attr('disabled', false);
			$(".hapus").attr('disabled', false);
		}
		// hitung_total();
	}
	$(document).on("change","#jenis_pembayaran,#cara_pembayaran",function(){
		// console.log('sini');
		show_hide_pembayaran();
		
	});
	$(document).on("keyup","#kuantitas,#harga_satuan",function(){
		// console.log('sini');
		perkalian();
		
	});
	function formatNumber(num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}

	function perkalian(){
		var total=0;
		total=parseFloat($("#kuantitas").val()) * parseFloat($("#harga_satuan").val());
		$("#total_harga").val(total)
	}
	$("#addjual").click(function() {
			var content = "";
			if (!validate_detail()) return false;
			if ($("#rowindex").val() == '') {
				content += "<tr>";
			}
			content += "<td>" + $("#nama_barang").val() + "</td>"; //0 No Urut
			content += "<td>" + $("#merk_barang").val() + "</td>"; //1 
			content += "<td>" + $("#kuantitas").val() + "</td>"; //2 
			content += "<td>" + $("#satuan").val() + "</td>"; //3 
			content += "<td>" + formatNumber($("#harga_satuan").val()) + "</td>"; //4 
			content += "<td>" + formatNumber($("#total_harga").val()) + "</td>"; //5 
			content += "<td>" + $("#keterangan").val() + "</td>"; //6			
			content +="<td ><div class='btn-group'><button type='button' class='btn btn-xs btn-info edit kunci'><i class='glyphicon glyphicon-pencil'></i></button><button type='button' class='btn btn-xs btn-danger hapus kunci'><i class='glyphicon glyphicon-remove'></i></button></div></td>";
			content += "<td style='display:none'><input type='text' name='xiddet[]' value='"+$("#iddet").val()+"'></td>"; //8 nomor
			content += "<td style='display:none'><input type='text' name='xnama_barang[]' value='"+$("#nama_barang").val()+"'></td>"; //9 
			content += "<td style='display:none'><input type='text' name='xmerk_barang[]' value='"+$("#merk_barang").val()+"'></td>"; //10
			content += "<td style='display:none'><input type='text' name='xkuantitas[]' value='"+$("#kuantitas").val()+"'></td>"; //11
			content += "<td style='display:none'><input type='text' name='xsatuan[]' value='"+$("#satuan").val()+"'></td>"; //12
			content += "<td style='display:none'><input type='text' name='xharga_satuan[]' value='"+$("#harga_satuan").val()+"'></td>"; //13
			content += "<td style='display:none'><input type='text' name='xtotal_harga[]' value='"+$("#total_harga").val()+"'></td>"; //14
			content += "<td style='display:none'><input type='text' name='xketerangan[]' value='"+$("#keterangan").val()+"'></td>"; //15
			content += "<td style='display:none'><input type='text' name='xstatus[]' value='1'></td>"; //16
			content += "<td style='display:none'><input type='text' name='xharga_pokok[]' value='"+$("#harga_pokok").val()+"'></td>"; //17
			content += "<td style='display:none'><input type='text' name='xharga_diskon[]' value='"+$("#harga_diskon").val()+"'></td>"; //18
			content += "<td style='display:none'><input type='text' name='xharga_ppn[]' value='"+$("#harga_ppn").val()+"'></td>"; //19
			content += "<td style='display:none'><input type='text' name='xidklasifikasi_det[]' value='"+$("#idklasifikasi_det").val()+"'></td>"; //20
			
			if ($("#rowindex").val() != '') {
				$('#tabel_add tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
			} else {
				content += "</tr>";
				$('#tabel_add tbody').append(content);
			}
			 hitung_total();
			clear_input();
	});
	function validate_detail() {
		if ($("#nama_barang").val() == "") {
			sweetAlert("Maaf...", "Nama barang Harus Diisi!", "error");
			$("#nama_barang").focus();
			return false;
		}
		if ($("#merk_barang").val() == "") {
			sweetAlert("Maaf...", "Merk barang Harus Diisi!", "error");
			$("#merk_barang").focus();
			return false;
		}	
		
		if ($("#kuantitas").val() < 1) {
			sweetAlert("Maaf...", "QTY Harus Diisi!", "error");
			$("#kuantitas").focus();
			return false;
		}
		if ($("#satuan").val() == "") {
			sweetAlert("Maaf...", "Satuan barang Harus Diisi!", "error");
			$("#satuan").focus();
			return false;
		}
		if ($("#harga_satuan").val() < 1) {
			sweetAlert("Maaf...", "Harga Harus Diisi!", "error");
			$("#harga_satuan").focus();
			return false;
		}
		if ($("#total_harga").val() < 1) {
			sweetAlert("Maaf...", "Total Harga Harus Diisi!", "error");
			$("#total_harga").focus();
			return false;
		}
		return true;
	}
	function validate_final() {
		var rowCount = $('#tabel_add tbody tr').length;
		if (rowCount < 1){
			sweetAlert("Maaf...", "Tidak ada barang yang diajukan!", "error");
			return false;
		}
		if ($("#idunit").val() == "#") {
			sweetAlert("Maaf...", "Unit Harus Diisi!", "error");
			$("#idunit").focus();
			return false;
		}
		if ($("#idunit_pengaju").val() == "#") {
			sweetAlert("Maaf...", "Unit Pengaju Harus Diisi!", "error");
			$("#idunit_pengaju").focus();
			return false;
		}
		if ($("#idjenis").val() == "#") {
			sweetAlert("Maaf...", "Jenis Pengajuan Harus Diisi!", "error");
			$("#idjenis").focus();
			return false;
		}
		if ($("#nama_kegiatan").val() == "") {
			sweetAlert("Maaf...", "Kegiatan Harus Diisi!", "error");
			$("#merk_barang").focus();
			return false;
		}	
		if ($("#idvendor").val() == "#") {
			sweetAlert("Maaf...", "Teruntukan Vendor!", "error");
			$("#cara_pembayaran").focus();
			return false;
		}
		if ($("#cara_pembayaran").val() == "#") {
			sweetAlert("Maaf...", "Cara Pembayaran Harus Diisi!", "error");
			$("#cara_pembayaran").focus();
			return false;
		}
		
		// if ($("#cara_pembayaran").val() != "#" && $("#cara_pembayaran").val() != "0") {
			// if ($("#jenis_pembayaran").val() == "#") {
				// sweetAlert("Maaf...", "Jenis Pembayaran Diisi!", "error");
				// $("#jenis_pembayaran").focus();
				// return false;
			// }	
		// }
		if ($("#cara_pembayaran").val() == "1") {//1 kali
				if ($("#jenis_pembayaran").val()=='2' || $("#jenis_pembayaran").val()=='3'){//TF
					if ($("#norek").val()=='' || $("#bank").val()==''){
						sweetAlert("Maaf...", "Data Bank harus Diisi!", "error");
						return false;
					}
				}else if ($("#jenis_pembayaran").val()=='4'){//Konta	
					if ($("#tanggal_kontrabon").val()==''){
						sweetAlert("Maaf...", "tanggal Kontrabon harus Diisi!", "error");
						return false;
					}
				}
		}
		// alert($("#cara_pembayaran").val() + ' : '+ $("#sisa_termin").val());
		if ($("#cara_pembayaran").val() == "2") {//CICILAN
			if ($("#total_dp_cicilan").val()=='0'){
				sweetAlert("Maaf...", "DP Belum diinput Harus Diisi!", "error");
				$("#cara_pembayaran").focus();
				return false;
			}else{
				var rowCount = $('#tabel_cicilan tbody tr').length;
				if (rowCount==0){
					sweetAlert("Maaf...", "Cicilan Belum diinput Harus Diisi!", "error");
					return false;
				}
			}
			
			// return false;
			
		}
		if ($("#cara_pembayaran").val() == "3" && $("#sisa_termin").val()>0) {
			sweetAlert("Maaf...", "Termin Belum selesai Harus Diisi!", "error");
			$("#cara_pembayaran").focus();
			return false;
		}
		if ($('input[name="st_user_unit"]').val()=='0' && $('input[name="st_user_unit_lain"]').val()=='0' && $('input[name="st_user_bendahara"]').val()=='0'){
			sweetAlert("Maaf...", "Tentukan Unit Proses!", "error");
			return false;
		}
		$("#cover-spin").show();
		$("*[disabled]").not(true).removeAttr("disabled");
		return true;
	}
	$(document).on("click", ".edit", function() {
			$(".edit").attr('disabled', true);
			$(".hapus").attr('disabled', true);
			$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);			
			$("#nama_barang").val($(this).closest('tr').find("td:eq(9) input").val());
			$("#merk_barang").val($(this).closest('tr').find("td:eq(10) input").val());
			$("#kuantitas").val($(this).closest('tr').find("td:eq(11) input").val());
			$("#satuan").val($(this).closest('tr').find("td:eq(12) input").val());
			$("#harga_satuan").val($(this).closest('tr').find("td:eq(13) input").val());
			$("#total_harga").val($(this).closest('tr').find("td:eq(14) input").val());
			$("#keterangan").val($(this).closest('tr').find("td:eq(15) input").val());
			$("#iddet").val($(this).closest('tr').find("td:eq(8) input").val());
			$("#harga_pokok").val($(this).closest('tr').find("td:eq(17) input").val());
			$("#harga_diskon").val($(this).closest('tr').find("td:eq(18) input").val());
			$("#harga_ppn").val($(this).closest('tr').find("td:eq(19) input").val());
			$("#idklasifikasi_det").val($(this).closest('tr').find("td:eq(20) input").val());
			$("#kuantitas").focus();
	});
	$(document).on("click", ".hapus", function() {
		var baris=$(this).closest('td').parent();
		
			if ($("#id").val()==''){
				// alert($("#id").val());
				baris.remove();
				hitung_total();
			}else{
				$(this).closest('tr').find("td:eq(16) input").val(0)
				$(this).closest('tr').find("td:eq(6)").html('<span class="label label-danger">DELETED</span>')
				$(this).closest('tr').find("td:eq(7)").html('<span class="label label-danger">DELETED</span>')
			}
		
		 hitung_total();
	});
	
	function hitung_total(){
		var total_grand = 0;
		var total_item = 0;
		var total_jenis_barang = 0;
		$('#tabel_add tbody tr').each(function() {
			if ($(this).closest('tr').find("td:eq(16) input").val()=='1'){
				total_jenis_barang +=1;
				total_grand += parseFloat($(this).find('td:eq(14) input').val());
				total_item += parseFloat($(this).find('td:eq(11) input').val());
			}
		});
		// alert(total_grand);
		$("#grand_total").val(total_grand);
		$("#total_item").val(total_item);
		$("#total_jenis_barang").val(total_jenis_barang);
	}
	$(document).on("click", "#canceljual", function() {
		clear_input();
	});
	$(document).on("change", "#idrka", function() {
		refresh_program();
	});
	$(document).on("click", "#btn_refresh_vendor", function() {
		refresh_vendor();
	});
	
	function refresh_vendor(){
		$.ajax({
			url: '{site_url}mrka_pengajuan/refresh_vendor/',
			dataType: "json",
			success: function(data) {
				$("#idvendor").empty();
				$('#idvendor').append('<option value="#">- Pilih Vendor -</option><option value="0">Tentukan Vendor Nanti</option>');
				$('#idvendor').append(data.detail);
			}
		});
	}
	function refresh_program(){
		var idrka=$("#idrka").val();
		$.ajax({
			url: '{site_url}mrka_pengajuan/refresh_program/'+idrka,
			dataType: "json",
			success: function(data) {
				$("#idprogram").empty();
				$('#idprogram').append('<option value="#">- Pilih Program -</option>');
				$('#idprogram').append(data.detail);
			}
		});
	}
	//Cicilan
	$("#add_dp_cicilan").click(function() {
			var content = "";
			
			if (!validate_detail_cicilan()) return false;
			if ($("#rowindex_dp_cicilan").val() == '') {
				content += "<tr>";
			}
			content+='<td><input class="form-control input-sm number" readonly type="text" name="nominal_cicilan[]" value="'+$("#xnominal_cicilan").val()+'"/></td>';
			content+='						<td>';
			content+='							<div class="input-group date">';
			content+='								<input readonly class="js-datepicker form-control input-sm"  type="text" name="tanggal_cicilan[]" value="'+$("#xtanggal_cicilan").val()+'" data-date-format="dd-mm-yyyy"/>';
			content+='								<span class="input-group-addon">';
			content+='									<span class="fa fa-calendar"></span>';
			content+='								</span>';
			content+='							</div>';
			content+='						</td>';
			content+='						<td><input readonly class="form-control input-sm" type="text" value="'+$("#xketerangan_cicilan").val()+'" name="keterangan_cicilan[]" /></td>';
			content+='						<td>';
			content+='							<button type="button"  disabled  class="btn btn-sm btn-danger hapus_dp_cicilan" tabindex="9" title="Refresh"><i class="fa fa-times"></i></button>';
			content+='						</td>';
			content+='<td style="display:none"><input readonly class="form-control input-sm number" type="text" name="jenis_cicilan[]" value="1"/></td>';
			
			if ($("#rowindex").val() != '') {
				$('#tabel_dp_cicilan tbody tr:eq(' + $("#rowindex_dp_cicilan").val() + ')').html(content);
			} else {
				content += "</tr>";
				$('#tabel_dp_cicilan tbody').append(content);
			}
			clear_input_cicilan();
			hitung_total_cicilan();
	});
	function clear_input_cicilan(){
		$("#xnominal_cicilan").val('0')
		$("#xtanggal_cicilan").val('')
		$("#xketerangan_cicilan").val('')
		$("#rowindex_dp_cicilan").val('')
		$('.number').number(true, 0, '.', ',');
	}
	function validate_detail_cicilan() {
		// alert('sini');
		if ($("#xnominal_cicilan").val() == "0") {
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			$("#xnominal_cicilan").focus();
			return false;
		}
		if ($("#xtanggal_cicilan").val() == "") {
			sweetAlert("Maaf...", "Tanggal Harus Diisi!", "error");
			$("#xnominal_cicilan").focus();
			return false;
		}
		var sisa=0;
		sisa=parseFloat($("#grand_total").val()) - (parseFloat($("#xnominal_cicilan").val()) + parseFloat($("#total_dp_cicilan").val()));
		if (sisa < 0){
			sweetAlert("Maaf...", "Pembayaran Melebihi dari tagihan!", "error");
			return false;
		}
		
		
		return true;
	}
	function hitung_total_cicilan(){
		var total_dp = 0;
		$('#tabel_dp_cicilan tbody tr').each(function() {
				console.log($(this).find('td:eq(0) input').val());
				total_dp += parseFloat($(this).find('td:eq(0) input').val());
		});
		$("#total_dp_cicilan").val(total_dp);
		$("#sisa_cicilan").val(parseFloat($("#grand_total").val()) - total_dp);
		
	}
	$(document).on("click", "#generate_cicilan", function() {
		generate_cicilan();
	});
	$(document).on("click", "#cancel_dp_cicilan", function() {
		clear_input_cicilan();
	});
	$(document).on("keyup", "#jml_kali_cicilan", function() {
		var perbulan=0;
		if ($("#jml_kali_cicilan").val()==''){
			$("#jml_kali_cicilan").val(0);
		}
		if ($("#sisa_cicilan").val()=='0' || $("#sisa_cicilan").val()=='0'){
			return false;
		}
		
		perbulan=parseFloat($("#sisa_cicilan").val()) / parseFloat($("#jml_kali_cicilan").val());
		$("#per_bulan_cicilan").val(perbulan)
	});
	function generate_cicilan(){
		if ($("#sisa_cicilan").val()=='0' || $("#sisa_cicilan").val()=='0'){
			sweetAlert("Maaf...", "Sisa cicilan harus ada!", "error");
			return false;
		}
		if ($("#xtanggal_cicilan_pertama").val()==null || $("#xtanggal_cicilan_pertama").val()==''){
			sweetAlert("Maaf...", "Tanggal pertama harus ada!", "error");
			return false;
		}
		if ($("#jml_kali_cicilan").val()=='0' || $("#jml_kali_cicilan").val()=='0'){
			sweetAlert("Maaf...", "Jumlah cicilan harus ada!", "error");
			return false;
		}
		if ($("#per_bulan_cicilan").val()=='0' || $("#per_bulan_cicilan").val()=='0'){
			sweetAlert("Maaf...", "Jumlah cicilan per bulan harus ada!", "error");
			return false;
		}
		$("tabel_cicilan tbody").empty();
		$.ajax({
			url: '{site_url}mrka_pengajuan/generate_cicilan/',
			type: "POST",
			dataType: "json",
			data : {
					xtanggal_cicilan:$("#xtanggal_cicilan_pertama").val(),
					jml_kali_cicilan:$("#jml_kali_cicilan").val(),
					per_bulan_cicilan:$("#per_bulan_cicilan").val(),					
					
				   },
			success: function(data) {
				$("#tabel_cicilan tbody").empty();
				$('#tabel_cicilan tbody').append(data.detail);
				$('.number').number(true, 0, '.', ',');
			}
		});
	}
	function reset_cicilan(){
		$("#tabel_cicilan tbody").empty();
		
	}
	$(document).on("click", ".hapus_dp_cicilan", function() {
		var baris=$(this).closest('td').parent();		
		baris.remove();
		hitung_total_cicilan();
		reset_cicilan();
	});
	//END CICILAN
	//Termin
	$("#add_dp_termin").click(function() {
			var content = "";
			
			if (!validate_detail_termin()) return false;
			if ($("#rowindex_dp_cicilan").val() == '') {
				content += "<tr>";
			}
		content+='<td><input class="form-control input-sm" readonly type="text" value="'+$("#xjenis_termin option:selected").text()+'"/></td>';	
		content+='<td><input class="form-control input-sm" readonly type="text" name="judul_termin[]" value="'+$("#xjudul_termin").val()+'"</td>';
		content+='<td><input class="form-control input-sm" readonly type="text" name="deskripsi_termin[]" value="'+$("#xdeskripsi_termin").val()+'"/></td>';
		content+='<td><input class="form-control input-sm number" readonly type="text" name="nominal_termin[]" value="'+$("#xnominal_termin").val()+'"/></td>';
		content+='<td>';
			content+='<div class="input-group date">';
				content+='<input class="js-datepicker form-control input-sm"  disabled type="text" name="tanggal_termin[]" value="'+$("#xtanggal_termin").val()+'" data-date-format="dd-mm-yyyy"/>';
				content+='<span class="input-group-addon">';
					content+='<span class="fa fa-calendar"></span>';
				content+='</span>';
			content+='</div>';
		content+='</td>';
		content+='<td><button type="button" class="btn btn-sm btn-danger hapus_termin" tabindex="9" id="cancel_dp_termin" title="Refresh"><i class="fa fa-times"></i></button></td>';
		content+='<td style="display:none"><input class="form-control input-sm" type="text" name="jenis_termin[]" value="'+$("#xjenis_termin").val()+'"/></td>';
		content+='<td style="display:none"><input class="form-control input-sm" type="text" name="status_dibayar_termin[]" value="0"/></td>';
			
			if ($("#rowindex").val() != '') {
				$('#tabel_dp_termin tbody tr:eq(' + $("#rowindex_dp_cicilan").val() + ')').html(content);
			} else {
				content += "</tr>";
				$('#tabel_dp_termin tbody').append(content);
			}
			hitung_total_termin();
			clear_input_termin();
	});
	function validate_detail_termin() {
		// alert('sini');
		if ($("#xjenis_termin").val() == "#") {
			sweetAlert("Maaf...", "Tipe Transaksi Harus Diisi!", "error");
			$("#xjenis_termin").focus();
			return false;
		}
		if ($("#xjudul_termin").val() == "") {
			sweetAlert("Maaf...", "Judul Harus Diisi!", "error");
			$("#xjudul_termin").focus();
			return false;
		}
		if ($("#xdeskripsi_termin").val() == "") {
			sweetAlert("Maaf...", "Deskripsi Harus Diisi!", "error");
			$("#xdeskripsi_termin").focus();
			return false;
		}
		if ($("#xnominal_termin").val() == "" || $("#xnominal_termin").val() == "0") {
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			$("#xnominal_termin").focus();
			return false;
		}
		if ($("#xtanggal_termin").val() == "") {
			sweetAlert("Maaf...", "Tanggal Harus Diisi!", "error");
			$("#xtanggal_termin").focus();
			return false;
		}
		var sisa=0;
		sisa=parseFloat($("#grand_total").val()) - (parseFloat($("#xnominal_termin").val()) + parseFloat($("#total_dp_termin").val()));
		if (sisa < 0){
			sweetAlert("Maaf...", "Pembayaran Melebihi dari tagihan!", "error");
			return false;
		}
		
		
		return true;
	}
	$(document).on("click", ".hapus_termin", function() {
		var baris=$(this).closest('td').parent();		
		baris.remove();
		hitung_total_termin();
		clear_input_termin();
	});
	
	
	function hitung_total_termin(){
		var total_dp = 0;
		$('#tabel_dp_termin tbody tr').each(function() {
				console.log($(this).find('td:eq(3) input').val());
				total_dp += parseFloat($(this).find('td:eq(3) input').val());
		});
		$("#total_dp_termin").val(total_dp);
		$("#sisa_termin").val(parseFloat($("#grand_total").val()) - total_dp);
		
	}
	function clear_input_termin(){
		$("#xjenis_termin").val('#').trigger('change')
		$("#xjudul_termin").val('')
		$("#xdeskripsi_termin").val('')
		$("#xnominal_termin").val('0')
		$("#xtanggal_termin").val('')
		$('.number').number(true, 0, '.', ',');
	}
	
	// END TERMIN
	$(document).on("click", "#btn_open_modal_kegiatan", function() {
		$("#modal_kegiatan").modal('show');
		
	});
	function load_kegiatan(){
		var idrka=$("#idrka").val();
		var idprespektif=$("#idprespektif").val();
		var idprogram=$("#idprogram").val();
		
		$('#tabel_kegiatan').DataTable().destroy();
		table=$('#tabel_kegiatan').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mrka_pengajuan/load_kegiatan_reminder',
					type: "POST",
					dataType: 'json',
					data : {
						idrka:idrka,
						idprespektif:idprespektif,
						idprogram:idprogram,
						
					   }
				},
				"columnDefs": [
					{"targets": [0,1,2,3,4], "visible": false },
					{"targets": [10], "class": 'text-center' },
					
				]
			});
	}
	$(document).on("click",".pilih",function(){
		var table = $('#tabel_kegiatan').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		var bulan = table.cell(tr,1).data()
		$('#tabel_add tbody').empty();
		hitung_total();
		$.ajax({
			url: '{site_url}mrka_pengajuan/get_pilih_kegiatan/'+id+'/'+bulan,
			dataType: "json",
			success: function(data) {
				$("#nama_kegiatan").val(data.nama);
				$("#bulan").val(bulan);
		// alert(id+' '+bulan);
				$("#idrka_kegiatan").val(id);
				$("#idunit_pengaju").val(data.idunit).trigger('change');
				$("#idunit").val(data.idunit).trigger('change');
				$("#modal_kegiatan").modal('hide');
			}
		});
		
		
	});
	$(document).on("click","#btn_filter_rka",function(){
		load_kegiatan();
		
	});
</script>