<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('1410'))){ ?>
<div class="block">
	<div class="block-header">
	
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:20px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Pengajuan</label>
                    <div class="col-md-8">
                        <input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="">
                        <input type="text" class="form-control" id="no_pengajuan" placeholder="No Pengajuan" name="no_pengajuan" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Pengajuan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="nama_kegiatan" placeholder="Nama Kegiatan / Pengajuan" name="nama_kegiatan" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Tipe Pengajuan</label>
                    <div class="col-md-8">
                       <select id="tipe_rka" name="tipe_rka" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua -</option>
							<option value="1" >RKA</option>
							<option value="2" >NON RKA</option>							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Unit Yang Mengajukan</label>
                    <div class="col-md-8">
                       <select id="idunit_pengaju" name="idunit_pengaju" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua -</option>
							<? foreach($list_unit as $row){ ?>
							<option value="<?=$row->idunit?>" ><?=$row->nama?></option>
							<?}?>
						</select>
                    </div>
                </div>
				
                                
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Jenis</label>
                    <div class="col-md-8">
                      <select id="idjenis" name="idjenis" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Jenis -</option>
						<? foreach($list_jenis as $row){ ?>
							<option value="<?=$row->id?>" <?=($idjenis==$row->id?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
                    </div>
                </div>	
				 <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Pengajuan</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggal_pengajuan1" name="tanggal_pengajuan1" placeholder="From" value="{tanggal_pengajuan1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_pengajuan2" name="tanggal_pengajuan2" placeholder="To" value="{tanggal_pengajuan2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Dibutuhkan</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggal_dibutuhkan1" name="tanggal_dibutuhkan1" placeholder="From" value="{tanggal_dibutuhkan1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_dibutuhkan2" name="tanggal_dibutuhkan2" placeholder="To" value="{tanggal_dibutuhkan2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
                
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_index">
			<thead>
				<tr>
					<th>#</th>
					<th>#</th>
					<th>#</th>
					<th>#</th>
					<th>No Pengajuan</th>
					<th>Tanggal Pengajuan</th>
					<th>Tipe</th>
					<th>Nama Pengajuan</th>
					<th>Unit Yang Mengajukan</th>
					<th>Tanggal Dibutuhkan</th>
					<th>Nominal</th>
					<th>Jenis</th>
					<th>Status</th>
					<th>Status Bendahara</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<?}?>
<div class="modal fade in black-overlay" id="modal_approval" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						
						<div class="col-md-12">
							<table width="100%" id="tabel_user" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Step</th>
										<th>User</th>
										<th>Jika Setuju</th>
										<th>Jika Menolak</th>								
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						
					</div>
					<?php echo form_close() ?>
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-primary" id="btn_simpan_approval" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Proses Persetujuan</button>
			</div>
			
		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_user" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12">
							<table width="100%" id="tabel_user_proses" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Step</th>
										<th>User</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						
					</div>
					<?php echo form_close() ?>
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
			
		</div>
	</div>
</div>
<div class="modal fade" id="modal_ganti" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Ganti Unit</h3>
                </div>
				<form class="form-horizontal push-10-t">
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">						
							<div class="form-group" >
								<label class="col-md-4 control-label" for="status">Unit Asal</label>
								<div class="col-md-8">
									 <select id="idunit_asal" name="idunit_asal" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- Pilih -</option>
										<? foreach($list_unit as $row){ ?>
										<option value="<?=$row->idunit?>" ><?=$row->nama?></option>
										<?}?>
									</select>
									<input type="hidden" readonly class="form-control" id="idpengajuan" name="idpengajuan" value="">
								</div>
								
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="status">Unit Pengganti</label>
								<div class="col-md-8">
									 <select id="idunit_ganti" name="idunit_ganti" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- Pilih -</option>
										<? foreach($list_unit_ganti as $row){ ?>
										<option value="<?=$row->idunit?>" ><?=$row->nama?></option>
										<?}?>
									</select>
									<input type="hidden" readonly class="form-control" id="idpengajuan" name="idpengajuan" value="">
								</div>
								
							</div>
							<div class="form-group" style="margin-bottom: 15px;">
								<label class="col-md-4 control-label" for="status"></label>
							</div>
							<br>
						</div>							
					</div>
					
				</div>
				</form>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-danger text-uppercase" type="button" name="btn_ganti" id="btn_ganti"><i class="fa fa-save"></i> GANTI UNIT</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade in black-overlay" id="modal_selesai" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Selesai </h3>
				</div>
				<div class="block-content">
				<?php echo form_open('#','class="form-horizontal push-10-t" ') ?>
				
				
				
				<div class="form-group">
					<label class="col-md-3 control-label" for="nama">No pembayaran </label>
					<div class="col-md-7">
						<input  type="text" class="form-control" id="bukti_pembayaran" placeholder="No Pembayaran" name="bukti_pembayaran">
					</div>
				</div>
				</form>
				<div class="col-md-12" style="margin-top: 10px;">
					<div class="block" id="box_file2">					
						 <table class="table table-bordered" id="list_file_bukti">
							<thead>
								<tr>
									<th width="50%" class="text-center">File</th>
									<th width="15%" class="text-center">Size</th>
									<th width="35%" class="text-center">User</th>
									<th width="35%" class="text-center">X</th>
									
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
					<div class="block">
						<form action="{base_url}mrka_bendahara/upload_bukti" enctype="multipart/form-data" class="dropzone" id="image_bukti">
							<input type="hidden" class="form-control" name="idrka" id="idrka" value="{id}" readonly>
							<div>
							  <h5>Upload Bukti Pembayaran</h5>
							</div>
						</form>
					</div>
					
				</div> 
				<div class="modal-footer">
					<button class="btn btn-sm btn-primary" id="btn_selesai" type="button"><i class="fa fa-check"></i> Selesai</button>
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
				</div>
				
			</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	var myDropzone3 
	$(document).ready(function(){
		// alert('sini');
		load_index();
		Dropzone.autoDiscover = false;
		myDropzone3 = new Dropzone("#image_bukti", { 
		   autoProcessQueue: true,
		   maxFilesize: 30,
		});
		myDropzone3.on("complete", function(file) {		  
		  refresh_image_bukti();
		  myDropzone3.removeFile(file);
		  
		});
	})	
	$(document).on("click",".hapus_file_bukti",function(){	
		var currentRow=$(this).closest("tr");          
         var id=currentRow.find("td:eq(4)").text();
		 $.ajax({
			url: '{site_url}mrka_bendahara/hapus_file_bukti',
			type: 'POST',
			data: {id: id},
			success : function(data) {				
				$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
				currentRow.remove();
				// table.ajax.reload( null, false );				
			}
		});
	});
	function refresh_image_bukti(){
		var id=$("#id").val();
		
		$.ajax({
			url: '{site_url}mrka_bendahara/refresh_image_bukti/'+id,
			dataType: "json",
			success: function(data) {
				if (data.detail!=''){
					$('#list_file_bukti tbody').empty();
					// $("#box_file2").attr("hidden",false);
					$("#list_file_bukti tbody").append(data.detail);
				}
				// console.log();
				
			}
		});
	}
	$("#btn_filter").click(function() {
		// alert('ID');
		table.destroy();
		load_index();
	});
	$(document).on("click",".selesai",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		$("#id").val(id);
		$("#idrka").val(id);
		refresh_image_bukti();
		$("#modal_selesai").modal('show');
		
	});
	$(document).on("click",".pencairan",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		var id_approval=$(this).data('id');
		swal({
			title: "Apakah Anda Yakin  ?",
			text : "Proses Pencairan Pengajuan ini?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka_bendahara/setuju_pencairan/'+id+'/1',
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Pencairan berhasil diganti'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
		
	});
	$(document).on("click",".batal",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		var id_approval=$(this).data('id');
		swal({
			title: "Apakah Anda Yakin  ?",
			text : "Mengembalikan Status Konfirmasi Pengajuan ini?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka_approval/setuju_batal/'+id_approval+'/0',
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Pengembalian berhasil disetujui'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
		
	});
	$(document).on("click","#btn_ganti",function(){
		if ($("#idunit_asal").val() == "#") {
			sweetAlert("Maaf...", "Unit Asal Harus Diisi!", "error");
			$("#idunit_asal").focus();
			return false;
		}
		if ($("#idunit_ganti").val() == "#") {
			sweetAlert("Maaf...", "Unit Ganti Harus Diisi!", "error");
			$("#idunit_ganti").focus();
			return false;
		}
		var idpengajuan=$("#idpengajuan").val();
		var idunit_ganti=$("#idunit_ganti").val();
		var idunit_asal=$("#idunit_asal").val();
		swal({
			title: "Apakah Anda Yakin  ?",
			text : "Mengganti Unit yang mengerjakan?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka_belanja/ganti_unit/',
				type: 'POST',
				data: {
					idpengajuan:idpengajuan,
					idunit_ganti:idunit_ganti,
					idunit_asal:idunit_asal,
				},
				success: function() {
					$('#modal_ganti').modal('hide');
					$.toaster({priority : 'success', title : 'Succes!', message : ' Unit berhasil diganti'});
					$('#table_index').DataTable().ajax.reload( null, false );
					
				}
			});
		});
		
		return false;
		
	});
	$(document).on("click",".ganti",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		$("#idpengajuan").val(id)
		$('#modal_ganti').modal('show');
		// modal_tolak
		
	});
	$(document).on("click",".user",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		$("#idrka").val(id);
		load_user();
	});
	
	$(document).on("click","#btn_selesai",function(){
		
		var id = $("#id").val();
		// alert(id)
		var bukti_pembayaran=$("#bukti_pembayaran").val();
		if ($("#bukti_pembayaran").val()==''){
			sweetAlert("Maaf...", "No Pembayaran harus Diisi!", "error");
			$("#bukti_pembayaran").focus();
			return false;
		}
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menyelesaikan Pengajuan ini?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka_belanja/selesai',
				type: 'POST',
				data: {id: id,bukti_pembayaran:bukti_pembayaran},
				success: function() {
					$("#modal_selesai").modal('hide');
					$.toaster({priority : 'success', title : 'Succes!', message : ' RKA Berhasil diselesaikan'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	function load_index(){
		var no_pengajuan=$("#no_pengajuan").val();
		var nama_kegiatan=$("#nama_kegiatan").val();
		var tipe_rka=$("#tipe_rka").val();
		var idunit_pengaju=$("#idunit_pengaju").val();
		var idjenis=$("#idjenis").val();
		var tanggal_pengajuan1=$("#tanggal_pengajuan1").val();
		var tanggal_pengajuan2=$("#tanggal_pengajuan2").val();
		var tanggal_dibutuhkan1=$("#tanggal_dibutuhkan1").val();
		var tanggal_dibutuhkan2=$("#tanggal_dibutuhkan2").val();
		// alert(tanggal_dibutuhkan1);
		table=$('#table_index').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mrka_belanja/getIndex',
					type: "POST",
					dataType: 'json',
					data : {
						no_pengajuan:no_pengajuan,
						nama_kegiatan:nama_kegiatan,
						tipe_rka:tipe_rka,
						idunit_pengaju:idunit_pengaju,
						idjenis:idjenis,
						tanggal_pengajuan1:tanggal_pengajuan1,
						tanggal_pengajuan2:tanggal_pengajuan2,
						tanggal_dibutuhkan1:tanggal_dibutuhkan1,
						tanggal_dibutuhkan2:tanggal_dibutuhkan2,
						
					   }
				},
				"columnDefs": [
					{"targets": [0,1,2,11], "visible": false },
					 {  className: "text-right", targets:[3,10] },
					 {  className: "text-center", targets:[4,5,,6,8,9,11,12,13] },
					 { "width": "5%", "targets": [3] },
					 { "width": "8%", "targets": [4,5,6,9] },
					 { "width": "10%", "targets": [7,8,11] },
					 { "width": "12%", "targets": [12,13] },
					 { "width": "15%", "targets": [14] },

				]
			});
	}
	function load_user_approval(){
		var idrka=$("#idrka").val();
		$('#tabel_user').DataTable().destroy();
		table=$('#tabel_user').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mrka_pengajuan/load_user_approval',
					type: "POST",
					dataType: 'json',
					data : {
						idrka:idrka,
												
					   }
				},
				"columnDefs": [
					 {  className: "text-right", targets:[0] },
					 {  className: "text-center", targets:[1,2,3] },
					 { "width": "10%", "targets": [0] },
					 { "width": "40%", "targets": [1] },
					 { "width": "25%", "targets": [2,3] },

				]
			});
	}
	function load_user(){
		var idrka=$("#idrka").val();
		$("#modal_user").modal('show');
		
		// alert(idrka);
		$.ajax({
			url: '{site_url}mrka_pengajuan/list_user/'+idrka,
			dataType: "json",
			success: function(data) {
				$("#tabel_user_proses tbody").empty();
				$("#tabel_user_proses tbody").append(data.detail);
			}
		});
	}
</script>
