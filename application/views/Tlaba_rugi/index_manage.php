<?= ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        <ul class="block-options">
			<li>
				<a href="{base_url}tlaba_rugi" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
        <h3 class="block-title">{title}</h3>
    </div>
	<div class="block-content">
		  <?php echo form_open('Tbuku_besar/export','class="form-horizontal" id="form-work" target="_blank"') ?>
		<input type="hidden" class="form-control"  id="template_id" name="template_id" value="{template_id}" />
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-2" for="">Nama Laporan</label>
				<div class="col-md-10">
					<input type="text" class="form-control" readonly placeholder="No. Bukti" id="nama" value="{nama}" />
				</div>
				
			</div>
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-2" for="">Tanggal</label>
				<div class="col-md-5">
					<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
						<input class="form-control" type="text" id="tanggal_trx1" name="tanggal_trx1" placeholder="From" value="<?=($tanggal_trx1==''?HumanDateShort($tanggal_awal_akuntansi):$tanggal_trx1)?>"/>
						<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
						<input class="form-control" type="text" id="tanggal_trx2" name="tanggal_trx2" placeholder="To" value="{tanggal_trx2}"/>
					</div>
				</div>				
			</div>
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-2" for="">Periode Tampil</label>
				<div class="col-md-10">
					<select name="periode[]" id="periode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Periode" multiple>
						
					</select>
				</div>
				
			</div>
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-2" for="">Tapilkan Anggaran</label>
				<div class="col-md-10">
					<select name="st_anggaran" id="st_anggaran" class="js-select2 form-control" style="width: 100%;" data-placeholder="">
							<option value="0" selected>TIDAK</option>						
							<option value="1" >YA</option>						
					</select>
				</div>				
			</div>
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-2" for="">Tapilkan No. Akun</label>
				<div class="col-md-10">
					<select name="st_akun" id="st_akun" class="js-select2 form-control" style="width: 100%;" data-placeholder="">
							<option value="0" >TIDAK</option>						
							<option value="1" selected>YA</option>						
					</select>
				</div>				
			</div>
			
			<div class="form-group" style="margin-top: 15px;">
				<label class="col-md-2" for=""></label>
				<div class="col-md-10">
					<button class="btn btn-info" type="button" id="btn_filter"><i class="fa fa-filter"></i> Filter</button>
					<button disabled class="btn btn-success" type="submit" name="btn_export" value="1"><i class="fa fa-file-excel-o"></i> Export Excel</button>
					<button disabled class="btn btn-danger" type="submit" name="btn_export" value="2"><i class="fa fa-file-pdf-o"></i> Export PDF</button>
					<button disabled class="btn btn-primary" type="submit" name="btn_export" value="3"><i class="fa fa-print"></i> Print</button>
				</div>
			</div>
			
			<?php echo form_close(); ?>
			
    </div>
</div>
<div class="block">
    <div class="block-header">        
        <h3 class="block-title">Laba Rugi</h3>
    </div>
	<div class="block-content">
		 <div class="form-horizontal">		
				<div class="table-responsive" id="div_tabel">
					
				</div>
		</div>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}pages/dataTables.fixedColumns.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var idakun;
var table;
var len_arr;
	$(document).ready(function(){
        load_periode();
    })
	
	$(document).on("click","#btn_filter",function(){			
		// load_index_detail();		
		LoadLabaRugi();		
	});
	function load_periode(){
		var tanggal_trx1=$("#tanggal_trx1").val();
		var tanggal_trx2=$("#tanggal_trx2").val();
		$("#periode").empty();
		if ($("#tahun").val() !='#'){
			$.ajax({
				url: '{site_url}tlaba_rugi/load_periode_manage/',
				dataType: "json",
				type: "POST",
				data: {
					tanggal_trx1: tanggal_trx1,tanggal_trx2: tanggal_trx2,
					
				},
				success: function(data) {					
					$('#periode').append(data.detail);
					// load_anggaran();
				}
			});
		}
		
	}
	$(document).on("change","#tanggal_trx1,#tanggal_trx2",function(){			
		load_periode();		
	});
	function compare(){
		
		if ($("#st_compare").val()=='1'){
			$("#div_compare").show();
		}else{
			$("#div_compare").hide();
			$("#div_tanggal").hide();
			$(".div_periode").hide();
		}
		if ($("#st_compare").val()=='1'){
			if ($("#compare_by").val()=='1'){
				$("#div_tanggal").show();
				$(".div_periode").hide();
			}else{
				$("#div_tanggal").hide();
				$(".div_periode").show();
			}
		}
	}
	function clear_table(){
		$tabel='<table class="table table-striped table-borderless table-header-bg" id="index_list">';
		$tabel +='<thead></thead><tbody></tbody></table>';
		$("#div_tabel").html($tabel);
	}
	function generate_periode(){
		var st_compare=$("#st_compare").val();
		var tanggal_trx1=$("#tanggal_trx1").val();
		var tanggal_trx2=$("#tanggal_trx2").val();
		var compare_by=$("#compare_by").val();
		var tanggal__compare_trx1=$("#tanggal__compare_trx1").val();
		var tanggal__compare_trx2=$("#tanggal__compare_trx2").val();
		
		var tipe_periode=$("#tipe_periode").val();
		var jml_periode=$("#jml_periode").val();
		$("#periode").empty();
		$.ajax({
			url: '{site_url}tlaba_rugi/generate_periode/',
			dataType: "json",
			type: "POST",
			data: {
				tanggal_trx1: tanggal_trx1,tanggal_trx2: tanggal_trx2,
				tanggal__compare_trx1: tanggal__compare_trx1,tanggal__compare_trx2: tanggal__compare_trx2,
				tipe_periode: tipe_periode,jml_periode: jml_periode,
				compare_by: compare_by,st_compare: st_compare,
			},
			success: function(data) {
				
				$("#periode").append(data.detail);
				$("#cover-spin").hide();
			}
		});
	}
	function validate_filter(){
		// alert($("#tanggal_trx1").val());
		if ($("#tanggal_trx1").val()=='' || $("#tanggal_trx2").val()==''){
			sweetAlert("Maaf...", "Tanggal Filter Harus dipilih!", "error");
			return false;
		}
		
		if ($("#st_compare").val()=='1'){//ISCOMPARED
			if ($("#compare_by").val()=='1'){//Tanggal
				if ($("#tanggal__compare_trx1").val()=='' || $("#tanggal__compare_trx2").val()==''){
					sweetAlert("Maaf...", "Tanggal Filter Pembanding Harus dipilih!", "error");
					return false;
				}
			}else{
				
			}
		}
		return true;
	}
	
	function LoadLabaRugi(){
		if (validate_filter()==false){return false;}
		clear_table();
		// var periode_header='';
		// generate_periode();
		
		var template_id=$("#template_id").val();
		var baris=0;
		var st_compare=$("#st_compare").val();
		var tanggal_trx1=$("#tanggal_trx1").val();
		var tanggal_trx2=$("#tanggal_trx2").val();
		var compare_by=$("#compare_by").val();
		var tanggal__compare_trx1=$("#tanggal__compare_trx1").val();
		var tanggal__compare_trx2=$("#tanggal__compare_trx2").val();
		var idsection=$("#idsection").val();
		var idkategori=$("#idkategori").val();
		var idakun=$("#idakun").val();
		
		var tipe_periode=$("#tipe_periode").val();
		var jml_periode=$("#jml_periode").val();
		$("#periode").empty();
		$.ajax({
			url: '{site_url}tlaba_rugi/generate_periode/',
			dataType: "json",
			type: "POST",
			data: {
				tanggal_trx1: tanggal_trx1,tanggal_trx2: tanggal_trx2,
				tanggal__compare_trx1: tanggal__compare_trx1,tanggal__compare_trx2: tanggal__compare_trx2,
				tipe_periode: tipe_periode,jml_periode: jml_periode,
				compare_by: compare_by,st_compare: st_compare,
			},
			success: function(data) {
				
				$("#periode").append(data.detail);
				var periode=$("#periode").val();
				var data = [];
				data=$('#periode').select2('data');
				
				var content='';
				content +='<tr>';
				content +='<th class="text-left"></th>';
				content +='<th class="text-left"></th>';
				content +='<th class="text-left"></th>';
				content +='<th class="text-left">PERIODE : </th>';
				$('#index_list thead').empty();
				
				var kolom=[];
				var kolom_rp=[];
				var kolom_persen=[];
				var kol=3;
				  for (i = 0; i < data.length; i++) {
					  // console.log(data[i].text+' '+data[i].id)
					  
					  if (i==0){	
						kol=kol+1;
						kolom[i]=kol;
						content +='<th class="text-right">'+data[i].text+'</th>'						  
					  }else{
						kol=kol+1;
						kolom_rp[i-1]=kol;
						kol=kol+1;
						kolom_persen[i-1]=kol;
						kol=kol+1;
						kolom[i]=kol;
						
						content +='<th class="text-right"></th>';		
						// kolom[i]=i+5;
						content +='<th class="text-right"></th>';						  
						content +='<th class="text-right">'+data[i].text+'</th>';						  
					  }
					}
				content +='</tr>';
				$('#index_list thead').append(content);
				$('#index_list tbody').empty();
				$('#index_list').DataTable().destroy();
				// alert(kolom_rp);
				// return false;
				$("#cover-spin").show();
				var table = $('#index_list').removeAttr('width').DataTable({
					 scrollY:        "450px",
					scrollX:        true,
					scrollCollapse: true,
					searching:         false,
					paging:         false,
					serverSide: true,
					"processing": true,
					"ordering": false,
					fixedColumns:   {
						left: 4,
						// right: 1
					},
					"ajax": {
						url: '{site_url}tlaba_rugi/LoadLabaRugi/',
						type: "POST",
						dataType: 'json',
						data: {
							tanggal_trx1: tanggal_trx1,tanggal_trx2: tanggal_trx2,template_id:template_id,st_compare:st_compare,periode:periode
							,idsection:idsection,idkategori:idkategori,idakun:idakun
						}
					},
					// fixedColumns:   {
						// left: 4,
						// // right: 1
					// },
					"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
						baris=baris+1;
						$("#cover-spin").hide();
						if (aData[0]=='0' && aData[2]=='0'){
							$('td', nRow).css('background-color', '#e9e9e9');					
						}
						if (aData[0]=='1'  && aData[1]==null){
							$('td', nRow).css('background-color', '#d1e2ef');					
							// $(".decimal").number(true,0,'.',',');
						}
						if (aData[1]!=null){
							if ((baris % 2)=='1'){//ganjil
								$(nRow).find('td:eq(0)').css('background-color', '#f9f9f9');
							}else{
								$(nRow).find('td:eq(0)').css('background-color', '#fff');
							}
							
						}
						if (aData[2]=='1'){
							$('td', nRow).css('background-color', '#fdf3e5');	
							
						}
						
						// console.log(iDisplayIndex);
					},
					
					columnDefs: [				
						{ "visible": false, "targets": [0,1,2]},
						{ "width": "450px", "targets": [3]},
						{ "width": "150px", "targets": kolom},
						{ className: "text-right","targets": kolom},
						{ "width": "100px", "targets": kolom_rp},
						{ className: "text-right","targets": kolom_rp},
						{ className: "text-right","targets": kolom_persen},
						{ "width": "60px", "targets": kolom_persen},
						
					]
					// columnDefs: [
						// { 
						// "targets" : [ 2 ],
								  // render : function (data, type, row) {
									   
									// if(row.status == 'yes' && row.quantity > 0){
									 
									// return '<button class="btn btn-secondary btn-block">view</button>
									 
									// }else if(row.status == 'yes' && row.quantity == 0){
									 
									// return '<button class="btn btn-secondary btn-block">Delete</button>';
									 
									// }
						 
						// }
						
					// ]
				});
				
			}
		});
		
		
		periode_header=$("#tanggal_trx1").val()+' - '+$("#tanggal_trx2").val();
		
		
	}
	function load_index_detail(){
		
		clear_table();
		$("#cover-spin").show();
		var periode_header='';
		var tanggal_trx1=$("#tanggal_trx1").val();
		var tanggal_trx2=$("#tanggal_trx2").val();
		var template_id=$("#template_id").val();
		periode_header=$("#tanggal_trx1").val()+' - '+$("#tanggal_trx2").val();
		var content='';
		content +='<tr>';
		content +='<th class="text-left">PERIODE</th>';
		$('#index_list thead').empty();
		
		  content +='<th class="text-right">'+periode_header+'</th>';
		content +='</tr>';
		$('#index_list thead').append(content);
		// $('#index_list tbody').empty();
		// $('#index_list').DataTable().destroy();
		$.ajax({
			url: '{site_url}tlaba_rugi/load_index_detail/',
			dataType: "json",
			type: "POST",
			data: {
				tanggal_trx1: tanggal_trx1,tanggal_trx2: tanggal_trx2,template_id:template_id
			},
			success: function(data) {
				// console.log(data.tabel);
				$('#index_list tbody').append(data.tabel);
				// $(".opsi").select2({dropdownAutoWidth: false});
				$("#cover-spin").hide();
				// $(".tgl").datepicker();
				// $(".simpan").hide();
			}
		});
		
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').removeAttr('width').DataTable({
			 scrollY:        "450px",
			scrollX:        true,
			scrollCollapse: true,
			paging:         false,
			serverSide: true,
			"processing": true,
			"ordering": false,
			fixedColumns:   {
				left: 1,
				// right: 1
			},
			
			columnDefs: [
				{ "width": "4%", "targets": [0,1],  className: "text-center"},
				// { "width": "450px", "targets": [2]},
				// { "width": "200px", "targets": [kolom]},
				// { "width": "100%", "targets": 2,  className: "text-left" },
			]
			});
		
		
	}
	
</script>