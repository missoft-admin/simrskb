<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block" >
	<div class="block-header">
		<div class="block-header">
			<ul class="block-options">
			<li><a href="{base_url}merm_general_ranap/preview" class="btn btn-xs btn-success "><i class="fa fa-file-o"></i> Preview</a></li>
			<li><a href="{base_url}merm_general_ranap/export" class="btn btn-xs btn-warning " target="_blank"><i class="fa fa-file-pdf-o"></i> Export PDF</a></li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('merm_general_ranap/save', 'class="form-horizontal push-10-t"') ?>
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="example-hf-email"></label>
            <div class="col-md-7">

                <img class="img-avatar"  id="output_img" src="{upload_path}app_setting/<?=($logo?$logo:'no_image.png')?>" />
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="example-hf-email">Logo  (100x100)</label>
            <div class="col-md-7">
                <div class="box">
                    <input type="file" id="file-3" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;" name="logo" value="{logo}" />
                    <label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                </div>
            </div>
        </div>
		
		<div class="form-group">
			<label class="col-md-2 control-label" for="judul">Judul <span style="color:red;">*</span></label>
			<div class="col-md-10">
				<input tabindex="0" type="text" class="form-control" id="judul" placeholder="Judul " name="judul" value="{judul}" required>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="sub_header">Sub Header<span style="color:red;">*</span></label>
			<div class="col-md-4">
				<input tabindex="0" type="text" class="form-control" id="sub_header" placeholder="Sub Header" name="sub_header" value="{sub_header}" required>
			</div>
			<label class="col-md-2 control-label" for="sub_header_side">Sub Header Side<span style="color:red;">*</span></label>
			<div class="col-md-4">
				<input tabindex="0" type="text" class="form-control" id="sub_header_side" placeholder="Sub Header Side" name="sub_header_side" value="{sub_header_side}" required>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="sub_header">Content<span style="color:red;">*</span></label>
			<div class="col-md-4">
				<button class="btn btn-danger btn-xs" type="button" id="add_content"><i class="fa fa-plus"></i> Content</button>
			</div>
			
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="sub_header"></label>
			<div class="col-md-10">
				<table class="table" id="index_content">
					<thead>
						<tr>
							<th class="text-center" style="width: 50px;">#</th>
							<th>Content</th>
							<th class="hidden-xs" style="width: 15%;">Opsi</th>
							<th class="hidden-xs" style="width: 15%;">Created</th>
							<th class="text-center" style="width: 100px;">Actions</th>
						</tr>
					</thead>
					<tbody>
						
						
					</tbody>
				</table>
			</div>
			
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="sub_header">Footer 1<span style="color:red;">*</span></label>
			<div class="col-md-10">
				<textarea class="form-control js-summernote footer" name="footer_1" id="footer_1"><?=$footer_1?></textarea>
			</div>
			
		</div>
		<div class="form-group">
			
			<label class="col-md-2 control-label" for="sub_header_side">Footer 2<span style="color:red;">*</span></label>
			<div class="col-md-10">
				<textarea class="form-control js-summernote footer" name="footer_2" id="footer_2"><?=$footer_2?></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="ttd_1">Label Tandan Tangan 1<span style="color:red;">*</span></label>
			<div class="col-md-4">
				<input tabindex="0" type="text" class="form-control" id="ttd_1" placeholder="Label Tandan Tangan 1" name="ttd_1" value="{ttd_1}" required>
			</div>
			<label class="col-md-2 control-label" for="ttd_2">Label Tandan Tangan 2<span style="color:red;">*</span></label>
			<div class="col-md-4">
				<input tabindex="0" type="text" class="form-control" id="ttd_2" placeholder="Label Tandan Tangan 2" name="ttd_2" value="{ttd_2}" required>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="footer_form_1">Footer Form 1<span style="color:red;">*</span></label>
			<div class="col-md-4">
				<input tabindex="0" type="text" class="form-control" id="footer_form_1" placeholder="Footer Form 1" name="footer_form_1" value="{footer_form_1}" required>
			</div>
			<label class="col-md-2 control-label" for="footer_form_2">Footer Form 2<span style="color:red;">*</span></label>
			<div class="col-md-4">
				<input tabindex="0" type="text" class="form-control" id="footer_form_2" placeholder="Footer Form 2" name="footer_form_2" value="{footer_form_2}" required>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12 col-md-offset-2">
				<button class="btn btn-success" type="submit">Simpan</button>
				<a href="{base_url}merm_general_ranap" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
			</div>
		</div>
		<?php echo form_hidden('id', $id); ?>
		<?php echo form_close() ?>
	</div>
</div>

<div class="modal" id="modal_content" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Content</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal push-10-t">
					<div class="row">
                        <input type="hidden" readonly id="idcontent" name="idcontent" value="">

						<div class="col-md-12">                               
							<div class="form-group">
								<label class="col-md-2 control-label">No Urut</label>
								<div class="col-md-9"> 
									<input tabindex="0" type="text" class="form-control number" maxlength="3" id="no" placeholder="No Urut" value="">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Content</label>
								<div class="col-md-9"> 
									<textarea class="form-control js-summernote footer" name="isi" id="isi"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Jenis Jawaban</label>
								<div class="col-md-9"> 
									<select tabindex="5" id="jenis_isi"   name="reservasi_cara" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="1"  <?=($jenis_isi == "1" ? 'selected="selected"' : '')?>>Option</option>
										<option value="2" <?=($jenis_isi == 2 ? 'selected="selected"' : '')?>>Free Text</option>
										<option value="3" <?=($jenis_isi == 3 ? 'selected="selected"' : '')?>>Tanda Tangan</option>
										
									</select>
								</div>
							</div>
							<div class="form-group div_opsi">
								<label class="col-md-2 control-label">Value</label>
								<div class="col-md-9"> 
									<select id="ref_id" name="ref_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
																		
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">                               
							<br>
						</div>
                    </div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-primary" type="submit" type="button" id="btn_simpan_content"><i class="fa fa-check"></i> Simpan</button>
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.number').number(true, 0);
		$('.footer').summernote({
		  height: 130,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})	
		load_content();
		
	})	
	$("#jenis_isi").change(function() {
		let jenis_isi=$("#jenis_isi").val();
		if (jenis_isi=='1'){
			$(".div_opsi").css("display", "block");
		}else{
			
			$(".div_opsi").css("display", "none");
		}
		
	});

	//add_content
	$("#btn_simpan_content").click(function() {
		if ($("#no").val()==''){
			sweetAlert("Maaf...", "Tentukan No Urut", "error");
			return false;
		}
		if ($("#no").val()=='0'){
			sweetAlert("Maaf...", "Tentukan No Urut", "error");
			return false;
		}

		if ($("#isi").val()==''){
			sweetAlert("Maaf...", "Isi Content ", "error");
			return false;
		}
		var idcontent=$("#idcontent").val();
		var no=$("#no").val();
		var isi=$("#isi").val();
		var jenis_isi=$("#jenis_isi").val();
		var ref_id=$("#ref_id").val();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}merm_general_ranap/simpan_content/',
			dataType: "json",
			type: "POST",
			data: {
				idcontent:idcontent,
				jenis_isi:jenis_isi,
				ref_id:ref_id,
				no: no,isi:isi
			},
			success: function(data) {
				$("#modal_content").modal('hide');
				$("#cover-spin").hide();
				load_content();
			}
		});

	});
	$("#add_content").click(function() {
		$("#modal_content").modal('show');
		$("#idcontent").val('');
		var rowCount = $('#index_content tr').length;
		$("#no").val(rowCount);
		$('#isi').summernote('code','');
		var id='';
		$.ajax({
				url: '{site_url}merm_general_ranap/load_jawaban',
				type: 'POST',
				dataType: "json",
				data: {id: id},
				success: function(data) {
					console.log(data);
					$("#ref_id").empty();
					$('#ref_id').append(data);
					$("#cover-spin").hide();
				}
		});
	});
	function edit_content(id){
		$("#idcontent").val(id);
		$("#modal_content").modal('show');
		$("#cover-spin").show();
			
		 $.ajax({
				url: '{site_url}merm_general_ranap/load_jawaban',
				type: 'POST',
				dataType: "json",
				data: {id: id},
				success: function(data) {
					console.log(data);
					$("#ref_id").empty();
					$('#ref_id').append(data);
					$("#cover-spin").hide();
				}
		});
		$("#cover-spin").show();
		$.ajax({
				url: '{site_url}merm_general_ranap/get_edit',
				type: 'POST',
				dataType: "json",
				data: {id: id},
				success: function(data) {
					console.log(data);
					$('#isi').summernote('code',data.isi);
					// $("#isi").html(data.isi);
					$("#jenis_isi").val(data.jenis_isi).trigger('change');
					$("#no").val(data.no);
					$("#cover-spin").hide();
					
				}
		});
	}
	function loadFile_img(event){
		// alert('load');
		var output_img = document.getElementById('output_img');
		output_img.src = URL.createObjectURL(event.target.files[0]);
	}
	function load_content(){
		$.ajax({
			url: '{site_url}merm_general_ranap/load_content/',
			dataType: "json",
			success: function(data) {
				$("#index_content tbody").empty();
				$('#index_content tbody').append(data);
			}
		});

	}
	function hapus(id){
		swal({
			title: "Anda Yakin ?",
			text : "Akan Hapus?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			
			 $.ajax({
					url: '{site_url}merm_general_ranap/hapus',
					type: 'POST',
					data: {id: id},
					complete: function() {
						load_content();
						$("#cover-spin").hide();
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						
					}
				});
		});
	}
</script>