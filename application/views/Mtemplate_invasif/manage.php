<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mtemplate_invasif" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mtemplate_invasif/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group" style="margin-bottom:10px">
				<label class="col-md-2 control-label" for="nama">Nama</label>
				<div class="col-md-10">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="template_id" placeholder="template_id" name="template_id" value="{id}" required="" aria-required="true">
				</div>
			</div>
			
			<?if ($id){?>
			
			<div class="form-group push-5-t">
					<label class="col-md-2 control-label" for="nama"><?=text_primary('INFORMASI TINDAKAN')?></label>
					<div class="col-md-10">
						<div class="table-responsive">
					
							<table class="table table-bordered" id="tabel_informasi">
								<thead>
									<tr>
										<th width="10%" class="text-center">No</th>
										<th width="37%" class="text-center">Informasi INA</th>
										<th width="37%" class="text-center">Informasi ENG</th>
										<th width="15%" class="text-center">Action</th>
									</tr>
									<tr>
										<th  style=" vertical-align: top;">
										<input class="form-control" type="hidden" id="informasi_id" name="informasi_id" value="">
											<select id="nourut" class="js-select2 form-control btn_ttd" style="width: 100%;" data-placeholder="Pilih Opsi" required>
												<option value="#" selected>Urutan</option>
												<?for($i=1;$i<=10;$i++){?>
												<option value="<?=$i?>"><?=$i?></option>
												<?}?>
												
											</select>
										</th>
										<th  style=" vertical-align: top;">
											<input type="text" class="form-control" id="info_ina" placeholder="Indonesia" value="" >
										</th>
										<th  style=" vertical-align: top;">
											<input type="text" class="form-control" id="info_eng" placeholder="English" value="" >
										</th>
										
										<th style=" vertical-align: top;">
											<div class="btn-group">
											<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_informasi" name="btn_tambah_informasi"><i class="fa fa-save"></i> Simpan</button>
											<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_informasi()"><i class="fa fa-refresh"></i> Clear</button>
											</div>
										</th>										   
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
			</div>
			<?}?>
			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mtemplate_invasif" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){	
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
	$('.js-summernote').summernote({
		  height: 60,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
	let template_id=$("#template_id").val();
	if (template_id){
		load_index_informasi();
	}
	
})	
function load_index_informasi(){
	$("#cover-spin").show();
	let template_id=$("#template_id").val();
	
	$.ajax({
		url: '{site_url}mtemplate_invasif/load_index_informasi_invasif/',
		dataType: "json",
		type: 'POST',
		  data: {
				id:template_id,
				
		  },
		success: function(data) {
			$("#tabel_informasi tbody").empty();
			$("#tabel_informasi tbody").append(data.tabel);
			$("#cover-spin").hide();
			$('.auto_blur_tabel').summernote({
				height: 100,
			    codemirror: { // codemirror options
					theme: 'monokai'
				  },	
			  callbacks: {
				onBlur: function(contents, $editable) {
						assesmen_id=$("#assesmen_id").val();
						var tr=$(this).closest('tr');
						var informasi_id=tr.find(".informasi_id").val();
						var isi=$(this).val();
						$.ajax({
							url: '{site_url}mtemplate_invasif/update_info_eng_invasif/',
							dataType: "json",
							type: 'POST',
							data: {
							informasi_id:informasi_id,isi:isi,assesmen_id:assesmen_id
							},success: function(data) {

							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
							}
						});
				}
			  }
			});
				
		}
	});
}

function edit_informasi(informasi_id){
	$("#informasi_id").val(informasi_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mtemplate_invasif/find_informasi',
		type: 'POST',
		dataType: "JSON",
		data: {id: informasi_id},
		success: function(data) {
			$("#btn_tambah_informasi").html('<i class="fa fa-save"></i> Edit');
			$("#informasi_id").val(data.id);
			$('#info_ina').val(data.info_ina);
			$('#info_eng').val(data.info_eng);
			$('#nourut').val(data.nourut).trigger('change');
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_informasi(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Informasi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mtemplate_invasif/hapus_informasi',
				type: 'POST',
				data: {id: id},
				complete: function() {
					load_index_informasi();
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function clear_informasi(){
	$("#informasi_id").val('');
	$('#info_ina').val('');
	$('#info_eng').val('');
	$('#nourut').val('#').trigger('change');
	
	$("#btn_tambah_informasi").html('<i class="fa fa-save"></i> Save');
	
}
$("#btn_tambah_informasi").click(function() {
	let template_id=$("#template_id").val();
	let informasi_id=$("#informasi_id").val();
	let info_ina=$("#info_ina").val();
	let info_eng=$("#info_eng").val();
	let nourut=$("#nourut").val();
	
	if (nourut=='#'){
		sweetAlert("Maaf...", "Isi No Urut", "error");
		return false;
	}
	if (info_ina==''){
		sweetAlert("Maaf...", "Isi Jenis Informasi", "error");
		return false;
	}
	if (info_eng==''){
		sweetAlert("Maaf...", "Isi Isi Informasi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mtemplate_invasif/simpan_informasi', 
		dataType: "JSON",
		method: "POST",
		data : {
				template_id:template_id,
				informasi_id:informasi_id,
				info_ina:info_ina,
				info_eng:info_eng,
				nourut:nourut,
				
			},
		complete: function(data) {
			clear_informasi();
			load_index_informasi();
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
</script>