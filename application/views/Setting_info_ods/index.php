<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('2409'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_1" onclick="load_general()"><i class="fa fa-bell"></i> General</a>
		</li>
		<?}?>
		
		<?php if (UserAccesForm($user_acces_form,array('2410'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_2"  onclick="load_tab2()"><i class="fa fa-check-square-o"></i> Logic</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2411'))){ ?>
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab_3" ><i class="si si-note"></i> Label</a>
		</li>
		<?}?>
		
		<?php if (UserAccesForm($user_acces_form,array('2412'))){ ?>
		<li class="<?=($tab=='4'?'active':'')?>">
			<a href="#tab_4" onclick="load_info()"><i class="si si-note"></i> Information ODS</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('2409'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_1">
			
			<?php echo form_open('setting_info_ods/save_assesmen','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_header">Judul Header<span style="color:red;">*</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_header" name="judul_header" value="{judul_header}" placeholder="Header">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="judul_header_eng">Judul Header <span style="color:red;"> ENG </span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="judul_header_eng" name="judul_header_eng" value="{judul_header_eng}" placeholder="Footer">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="footer_ina">Footer<span style="color:red;">*</span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="footer_ina" name="footer_ina" value="{footer_ina}" placeholder="Footer INA">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="footer_eng">Footer <span style="color:red;"> ENG </span></label>
						<div class="col-md-10">
							<input class="form-control " type="text" id="footer_eng" name="footer_eng" value="{footer_eng}" placeholder="Footer">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_edit_catatan">Izin Edit Catatan</label>
						<div class="col-md-2">
							<select id="st_edit_catatan" name="st_edit_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_edit_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_edit_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_edit">Waktu Edit</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_edit" name="lama_edit" value="{lama_edit}" placeholder="Waktu Edit">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_edit">Edit Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_edit" name="orang_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_edit=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_edit=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_hapus_catatan">Izin Hapus Catatan</label>
						<div class="col-md-2">
							<select id="st_hapus_catatan" name="st_hapus_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_hapus_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_hapus_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_hapus">Waktu Hapus</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_hapus" name="lama_hapus" value="{lama_hapus}" placeholder="Waktu Hapus">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_hapus">Hapus Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_hapus" name="orang_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_hapus=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_hapus=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<label class="col-md-2 control-label" for="st_duplikasi_catatan">Izin Duplikasi Catatan</label>
						<div class="col-md-2">
							<select id="st_duplikasi_catatan" name="st_duplikasi_catatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($st_duplikasi_catatan=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($st_duplikasi_catatan=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
						<label class="col-md-1 control-label div_catatan" for="lama_duplikasi">Waktu Duplikasi</label>
						<div class="col-md-2 div_catatan">
							<div class="input-group">
								<input class="form-control number" type="text" id="lama_duplikasi" name="lama_duplikasi" value="{lama_duplikasi}" placeholder="Waktu Duplikasi">
								<span class="input-group-addon">Hari</span>
							</div>
						</div>
						<label class="col-md-2 control-label div_catatan" for="orang_duplikasi">Duplikasi Oleh Siapapun</label>
						<div class="col-md-2 div_catatan">
							<select id="orang_duplikasi" name="orang_duplikasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" <?=($orang_duplikasi=='1'?'selected':'')?>>IZINKAN</option>
								<option value="0" <?=($orang_duplikasi=='0'?'selected':'')?>>TIDAK</option>
								
							</select>
						</div>
					</div>
				</div>
				
				<?php if (UserAccesForm($user_acces_form,array('1759'))){ ?>
				<div class="col-md-12 push-10">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-2 control-label" for="statusstok"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_general" name="btn_save_general"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
						
				</div>
				<?}?>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('2410'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_2">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('HAK AKSES FORMULIR')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_hak_akses">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Profesi</th>
										<th width="15%">Spesialisasi</th>
										<th width="15%">PPA</th>
										<th width="15%">Melihat</th>
										<th width="10%">Input</th>
										<th width="10%">Edit</th>
										<th width="10%">Hapus</th>
										<th width="10%">Cetak</th>
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="profesi_id" name="profesi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" <?=($profesi_id=='#'?'selected':'')?>>-Pilih Profesi-</option>
												<?foreach(list_variable_ref(21) as $r){?>
													<option value="<?=$r->id?>" <?=($profesi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<select id="spesialisasi_id" name="spesialisasi_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($spesialisasi_id=='#'?'selected':'')?>>-SEMUA-</option>
												<?foreach(list_variable_ref(22) as $r){?>
													<option value="<?=$r->id?>" <?=($spesialisasi_id==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
												
											</select>
										</th>
										<th>
											<select id="mppa_id" name="mppa_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" <?=($mppa_id=='#'?'selected':'')?>>-SEMUA-</option>
												
											</select>
										</th>
										
										<th>
											<select id="st_lihat" name="st_lihat" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_lihat=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_lihat=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_input" name="st_input" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_input=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_input=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_edit" name="st_edit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_edit=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_edit=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_hapus" name="st_hapus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_hapus=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_hapus=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<select id="st_cetak" name="st_cetak" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="1" <?=($st_cetak=='1'?'selected':'')?>>IZINKAN</option>
												<option value="0" <?=($st_cetak=='0'?'selected':'')?>>TIDAK</option>
												
											</select>
										</th>
										<th>
											<?php if (UserAccesForm($user_acces_form,array('1761'))){ ?>
											<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_hak_akses" name="btn_tambah_hak_akses"><i class="fa fa-plus"></i> Tambah</button>
											<?}?>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
						</div>					
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
		<?php if (UserAccesForm($user_acces_form,array('2411'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?> " id="tab_3">
			
			<?php echo form_open_multipart('setting_info_ods/save_info_ods_label','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				
				<div class="form-group">
					
					<div class="col-md-12">
						<div class="col-md-12">
							<img class="img-avatar"  id="output_img" src="{upload_path}app_setting/{logo}" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Logo Login (100x100)</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="box">
								<input type="file" id="file-3" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;" name="logo" value="{logo}" />
								<label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
							</div>
						</div>
					</div>
				</div>
				 
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label>Alamat</label>
							<input type="text" class="form-control" name="alamat_rs" value="{alamat_rs}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label>Phone</label>
							<input type="text" class="form-control" name="phone_rs" value="{phone_rs}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label>Web</label>
							<input type="text" class="form-control" name="web_rs" value="{web_rs}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Label Header Identtias</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="label_identitas_ina" value="{label_identitas_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="label_identitas_eng" value="{label_identitas_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Nomor Register</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="no_reg_ina" value="{no_reg_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="no_reg_eng" value="{no_reg_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Nomor Rekam Medis</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="no_rm_ina" value="{no_rm_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="no_rm_eng" value="{no_rm_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Nama Pasien</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="nama_pasien_ina" value="{nama_pasien_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="nama_pasien_eng" value="{nama_pasien_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tanggal Lahir</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="ttl_ina" value="{ttl_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="ttl_eng" value="{ttl_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Umur</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="umur_ina" value="{umur_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="umur_eng" value="{umur_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Jenis Kelamin</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="jk_ina" value="{jk_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="jk_eng" value="{jk_eng}">
						</div>
						
					</div>
				</div>
				
				
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Paragraf 1</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="paragraf_1_ina" value="{paragraf_1_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="paragraf_1_eng" value="{paragraf_1_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Dokter DPJP </label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="dpjp_ina" value="{dpjp_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="dpjp_eng" value="{dpjp_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Rencana Tindakan Pembedahan </label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="rencana_ina" value="{rencana_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="rencana_eng" value="{rencana_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Tanggal Tindakan </label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="tanggal_tindakan_ina" value="{tanggal_tindakan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="tanggal_tindakan_eng" value="{tanggal_tindakan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Waktu Tindakan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="waktu_tindakan_ina" value="{waktu_tindakan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="waktu_tindakan_eng" value="{waktu_tindakan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Jenis Pembiusan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="jenis_bius_ina" value="{jenis_bius_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="jenis_bius_eng" value="{jenis_bius_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Minum Pengencer</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="minum_ina" value="{minum_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="minum_eng" value="{minum_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Jika Ya Sebutkan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="jika_ya_ina" value="{jika_ya_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="jika_ya_eng" value="{jika_ya_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Information ODS</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="info_ina" value="{info_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="info_eng" value="{info_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Paragraph 2</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="paragraf_2_ina" value="{paragraf_2_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="paragraf_2_eng" value="{paragraf_2_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Paragraph 3</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="paragraf_3_ina" value="{paragraf_3_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="paragraf_3_eng" value="{paragraf_3_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Paragraph 4</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="paragraf_4_ina" value="{paragraf_4_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="paragraf_4_eng" value="{paragraf_4_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Label Pasien</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="label_pasien_ina" value="{label_pasien_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="label_pasien_eng" value="{label_pasien_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Label Informasi</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="label_info_ina" value="{label_info_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="label_info_eng" value="{label_info_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Label Mulai Puasa</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="label_puasa_ina" value="{label_puasa_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="label_puasa_eng" value="{label_puasa_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Paragraph 5 </label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="paragraf_5_ina" value="{paragraf_5_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="paragraf_5_eng" value="{paragraf_5_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Paragraph Nama Dokumen</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="paragraf_dok_ina" value="{paragraf_dok_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="paragraf_dok_eng" value="{paragraf_dok_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Label Status</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="label_status_ina" value="{label_status_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="label_status_eng" value="{label_status_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Label Keterangan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="label_ket_ina" value="{label_ket_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="label_ket_eng" value="{label_ket_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Paragraf 6</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="paragraf_6_ina" value="{paragraf_6_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="paragraf_6_eng" value="{paragraf_6_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Paragraf 7</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="paragraf_7_ina" value="{paragraf_7_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="paragraf_7_eng" value="{paragraf_7_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Label Catatan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="label_catatan_ina" value="{label_catatan_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="label_catatan_eng" value="{label_catatan_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Content Catatan</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<textarea class="form-control js-summernote" name="content_catatan_ina" id="content_catatan_ina"><?=$content_catatan_ina?></textarea>
						</div>
						<div class="col-md-6">
							<label>English</label>
							<textarea class="form-control js-summernote" name="content_catatan_eng" id="content_catatan_eng"><?=$content_catatan_eng?></textarea>
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Label Pasien dan Keluarga</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="label_keluarga_ina" value="{label_keluarga_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="label_keluarga_eng" value="{label_keluarga_eng}">
						</div>
						
					</div>
				</div>
				<div class="form-group" style="margin-bottom:5px">
					<div class="col-md-12">
						<div class="col-md-12">
							<label class="text-primary">Label Petugas</label>&nbsp;&nbsp;&nbsp;&nbsp;
							
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
							<label>Indonesia</label>
							<input type="text" class="form-control" name="label_petugas_ina" value="{label_petugas_ina}">
						</div>
						<div class="col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="label_petugas_eng" value="{label_petugas_eng}">
						</div>
						
					</div>
				</div>
				
				<div class="form-group" style="margin-top:15px">
				
					<div class="col-md-12">
						<div class="col-md-6">
							<button class="btn btn-success text-uppercase" type="submit" id="btn_save_label" name="btn_save_general"><i class="fa fa-save"></i> Simpan</button>
						</div>
						
					</div>
				</div>
			</div>
			
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
		<?php if (UserAccesForm($user_acces_form,array('2412'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='4'?'active in':'')?>" id="tab_4">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('SETTING INFORMATION ODS')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_info">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="30%">Jenis ODS</th>
										<th width="55%">Infomation</th>
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<select id="jenis_pasien" name="jenis_pasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												
												<option value="#" <?=($jenis_pasien=='#'?'selected':'')?>>-Pilih Jenis-</option>
												<?foreach(list_variable_ref(428) as $r){?>
													<option value="<?=$r->id?>" <?=($jenis_pasien==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
												
											</select>
										</th>
										<th>
											<input type="text" class="form-control" name="informasi" style="width:100%" id="informasi" value="{informasi}">
										</th>
										
										<th>
											<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_info" name="btn_tambah_info"><i class="fa fa-plus"></i> Tambah</button>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
						</div>					
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
	$(".number").number(true,0,'.',',');
	var tab=$("#tab").val();
	if (tab=='1'){
		load_general();
	}
	if (tab=='2'){
		load_tab2();
	}
	if (tab=='4'){
		load_info();
	}
	
	$('.js-summernote').summernote({
		  height: 220,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})

})	

$(".auto_blur").change(function(){
	set_user();
	simpan_user_head();
	
});

$("#profesi_id_dokter").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_info_ods/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id_dokter").empty();
				$("#mppa_id_dokter").append(data);
			}
		});
	}else{
		
				$("#mppa_id_dokter").empty();
	}

});
$("#profesi_id_pemberi").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_info_ods/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id_pemberi").empty();
				$("#mppa_id_pemberi").append(data);
			}
		});
	}else{
		
				$("#mppa_id_pemberi").empty();
	}

});
$("#profesi_id_pendamping").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_info_ods/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id_pendamping").empty();
				$("#mppa_id_pendamping").append(data);
			}
		});
	}else{
		
				$("#mppa_id_pendamping").empty();
	}

});
function load_general(){
	$('.js-summernote').summernote({
	  height: 50,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	});	

}
function load_tab2(){
	load_hak_akses();		
	// load_formulir();	
}

// LOGIC
function load_hak_akses(){
	$('#index_hak_akses').DataTable().destroy();	
	table = $('#index_hak_akses').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_info_ods/load_hak_akses', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
$("#btn_tambah_hak_akses").click(function() {
	let profesi_id=$("#profesi_id").val();
	let spesialisasi_id=$("#spesialisasi_id").val();
	let mppa_id=$("#mppa_id").val();
	let st_lihat=$("#st_lihat").val();
	let st_input=$("#st_input").val();
	let st_edit=$("#st_edit").val();
	let st_hapus=$("#st_hapus").val();
	let st_cetak=$("#st_cetak").val();
	
	if (profesi_id=='#'){
		sweetAlert("Maaf...", "Tentukan Profesi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_info_ods/simpan_hak_akses', 
		dataType: "JSON",
		method: "POST",
		data : {
				profesi_id:profesi_id,
				spesialisasi_id:spesialisasi_id,
				mppa_id:mppa_id,
				st_lihat:st_lihat,
				st_input:st_input,
				st_edit:st_edit,
				st_hapus:st_hapus,
				st_cetak:st_cetak,
			
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				$("#profesi_id").val('#').trigger('change');
				$("#spesialisasi_id").val('#').trigger('change');
				$("#mppa_id").val('#').trigger('change');
				$("#st_cetak").val('0').trigger('change');
				$("#st_hapus").val('0').trigger('change');
				$("#st_edit").val('0').trigger('change');
				$("#st_input").val('0').trigger('change');
				$("#st_lihat").val('0').trigger('change');
				$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});

});
function hapus_hak_akses(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_info_ods/hapus_hak_akses',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_hak_akses').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function hapus_default(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Default?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_info_ods/hapus_default',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_tampil_default').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

$("#profesi_id").change(function(){
	if ($(this).val()!='#'){
		
		$.ajax({
			url: '{site_url}setting_info_ods/find_mppa/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#mppa_id").empty();
				$("#mppa_id").append(data);
			}
		});
	}else{
		
				$("#mppa_id").empty();
	}

});
$("#operand_tahun").change(function(){
	if ($(this).val()=='0'){//Tidak Ditentukan
		$("#operand_bulan").val($(this).val()).trigger('change');
		$("#operand_hari").val($(this).val()).trigger('change');
		$("#umur_bulan").val($(this).val()).trigger('change');
		$("#umur_hari").val($(this).val()).trigger('change');
		$(".bulan").attr('disabled','disabled');
		$(".hari").attr('disabled','disabled');
	}else{
		$(".bulan").removeAttr('disabled');
		// $(".hari").removeAttr('disabled');
	}

});
$("#operand_bulan").change(function(){
	if ($(this).val()=='0'){//Tidak Ditentukan
		$("#operand_hari").val($(this).val()).trigger('change');
		$(".hari").attr('disabled','disabled');
		$("#umur_hari").val($(this).val()).trigger('change');
	}else{
		$(".hari").removeAttr('disabled');
	}

});
$("#st_spesifik_info_ods").change(function(){
	if ($(this).val()=='1'){
		$(".setting_tidak").show();
	}else{
		$(".setting_tidak").hide();
	}

});
$("#btn_tambah_formulir").click(function() {
	let idtipe=$("#idtipe").val();
	let idpoli=$("#idpoli").val();
	let statuspasienbaru=$("#statuspasienbaru").val();
	let pertemuan_id=$("#pertemuan_id").val();
	let st_tujuan_terakhir=$("#st_tujuan_terakhir").val();
	let operand=$("#operand").val();
	// alert(operand);
	let lama_terakhir_tujan=$("#lama_terakhir_tujan").val();
	let st_formulir=$("#st_formulir").val();
	
	if (idtipe=='#'){
		sweetAlert("Maaf...", "Tentukan Tujuan", "error");
		return false;
	}
	
	if (st_formulir=='0'){
		sweetAlert("Maaf...", "Tentukan Status Formulir", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_info_ods/simpan_formulir', 
		dataType: "JSON",
		method: "POST",
		data : {
				idtipe:idtipe,
				idpoli:idpoli,
				statuspasienbaru:statuspasienbaru,
				pertemuan_id:pertemuan_id,
				st_tujuan_terakhir:st_tujuan_terakhir,
				operand:operand,
				lama_terakhir_tujan:lama_terakhir_tujan,
				st_formulir:st_formulir,

			
			},
		success: function(data) {
			// alert(data);
				$("#cover-spin").hide();
			if (data==true){
				$("#idtipe").val('#').trigger('change');
				$("#idpoli").val('#').trigger('change');
				$("#statuspasienbaru").val('#').trigger('change');
				$("#pertemuan_id").val('#').trigger('change');
				$("#operand").val('0').trigger('change');
				$("#st_tujuan_terakhir").val('0').trigger('change');
				$("#lama_terakhir_tujan").val('0');
				$("#st_formulir").val('0').trigger('change');
				$('#index_tampil').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});

			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
		}
	});

});
$("#idtipe").change(function(){
		$.ajax({
			url: '{site_url}setting_info_ods/find_poli/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#idpoli").empty();
				$("#idpoli").append(data);
			}
		});

});
$("#idtipe_default").change(function(){
		$.ajax({
			url: '{site_url}setting_info_ods/find_poli/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data);
				$("#idpoli_default").empty();
				$("#idpoli_default").append(data);
			}
		});

});
function load_formulir(){
	$('#index_tampil').DataTable().destroy();	
	table = $('#index_tampil').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			"columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [3,2,4,5,6] },
					// { "width": "15%", "targets": [1]},
					// { "width": "8%", "targets": [8]},
					// { "width": "30%", "targets": 3,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}setting_info_ods/load_formulir', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function hapus_formulir(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_info_ods/hapus_formulir',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_tampil').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
$("#btn_tambah_info").click(function() {
	let jenis_pasien=$("#jenis_pasien").val();
	let informasi=$("#informasi").val();
	let mppa_id=$("#mppa_id").val();
	let st_lihat=$("#st_lihat").val();
	let st_input=$("#st_input").val();
	let st_edit=$("#st_edit").val();
	let st_hapus=$("#st_hapus").val();
	let st_cetak=$("#st_cetak").val();
	
	if (jenis_pasien=='#'){
		sweetAlert("Maaf...", "Tentukan pasien", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_info_ods/simpan_info', 
		dataType: "JSON",
		method: "POST",
		data : {
				jenis_pasien:jenis_pasien,
				informasi:informasi,
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				$("#jenis_pasien").val('#').trigger('change');
				$("#informasi").val('');
				$('#index_info').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});

});
function load_info(){
	$('#index_info').DataTable().destroy();	
	table = $('#index_info').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}setting_info_ods/load_info', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function hapus_info(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_info_ods/hapus_info',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_info').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
</script>