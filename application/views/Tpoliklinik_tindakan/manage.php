<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>


<div class="block block-rounded">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}tpoliklinik_tindakan/index" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
        <hr style="margin-bottom: 0px;">
    </div>
    <div class="block-content">
        <?php echo form_open('tpoliklinik_tindakan/save', 'class="form-horizontal push-10-t" id="form-work"') ?>
        <!-- Start List Pasien -->
        <div class="col-sm-6 col-lg-6">
            <div class="block block-rounded block-bordered">
                <div class="block-header">
                    <h3 class="block-title">Data Pasien</h3>
                </div>
                <div class="block-content">
                    <table class="table">
                        <tr>
                            <th width="30%">No. Medrec</th>
                            <td>
                                <div class="col-sm-12">
                                    <?php if (UserAccesForm($user_acces_form, ['340'])) { ?>
                                    <div class="input-group">
                                        <?}?>
                                        <input class="form-control input-md" type="text" readonly="readonly" value="{no_medrec}">
                                        <?php if (UserAccesForm($user_acces_form, ['340'])) { ?>
                                        <span class="input-group-btn">
                        <a href="{base_url}tpoliklinik_pendaftaran/print_document/{idpendaftaran}/3" target="_blank" data-toggle="tooltip" title="" data-original-title="Kartu Status" class="btn btn-warning" id="kartu_status">Kartu Status</a>
                    </span>
                                        <?}?>
                                        <?php if (UserAccesForm($user_acces_form, ['340'])) { ?>
                                        <?}?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tanggal Pendaftaran</th>
                            <td>
                                <div class="col-sm-12">
                                    <input class="form-control input-sm" type="text" readonly="readonly" value="<?=HumanDateLong($tanggaldaftar)?>">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>No. Pendaftaran</th>
                            <td>
                                <div class="col-sm-12">
                                    <input class="form-control input-sm" type="text" readonly="readonly" value="{nopendaftaran}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Nama Pasien</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{title_nama}. {namapasien}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Alamat</th>
                            <td>
                                <div class="col-sm-12">
                                    <textarea class="form-control input-sm" readonly="readonly">{alamat}</textarea>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tanggal Lahir</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="<?=DMYFormat($tanggal_lahir)?>">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Umur</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{umur_tahun} Tahun {umur_bulan} Bulan {umur_hari} Hari">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Jenis Kelamin</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="Perempuan">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Agama</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="Islam">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Pernikahan</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="<?=GetStatusKawin($status_kawin)?>">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>No. Handpone</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{hp}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Penanggung Jawab</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{nama_keluarga}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tipe</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="<?=GetAsalRujukan($idtipe)?>">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Poliklinik</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{namapoli}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Kelompok Pasien</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly value="{namakelompok}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tipe Pasien</th>
                            <td>
                                <div class="col-sm-12">
                                    <select name="idtipepasien" class="select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
                                        <option value="">Pilih Opsi</option>
                                        <option value="1" <?=($idtipepasien == 1 ? 'selected="selected"' : '')?>>Pasien RS</option>
                                        <option value="2" <?=($idtipepasien == 2 ? 'selected="selected"' : '')?>>Pasien Pribadi</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <?php if ($idkelompokpasien == 2) {?>
                        <tr>
                            <th>Perusahaan</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly value="{namaperusahaan}">
                                </div>
                            </td>
                        </tr>
                        <?php }?>

                        <tr>
                            <th>Dokter <?=($idtipe == 2 ? '1' : '')?></th>
                            <td>
                                <div class="col-sm-12">
                                    <select tabindex="45" name="iddokter1" <?=(UserAccesForm($user_acces_form, ['341']) == false) ? 'disabled' : ''?> id="iddokter1" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
                                        <option value="">Pilih Opsi</option>
                                        <?php foreach (get_all('mpoliklinik_dokter', ['idpoliklinik' => $idpoliklinik]) as $r) {
	foreach (get_all('mdokter', ['id' => $r->iddokter]) as $s) {?>
                                        <option value="<?= $s->id?>" <?=($iddokter1 == $s->id ? 'selected="selected"' : '')?>><?= $s->nama?></option>
                                        <?php } ?>
                                        <?php
}?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <?php if ($idtipe == 2) { ?>
                        <tr>
                            <th>Dokter 2</th>
                            <td>
                                <div class="col-sm-12">
                                    <select tabindex="45" name="iddokter2" id="iddokter2" <?=(UserAccesForm($user_acces_form, ['341']) == false) ? 'disabled' : ''?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                        <option value="">Pilih Opsi</option>
                                        <?php foreach (get_all('mpoliklinik_dokter', ['idpoliklinik' => $idpoliklinik]) as $r) {
		foreach (get_all('mdokter', ['id' => $r->iddokter]) as $s) {?>
                                        <option value="<?= $s->id?>" <?=($iddokter2 == $s->id ? 'selected="selected"' : '')?>><?= $s->nama?></option>
                                        <?php } ?>
                                        <?php
	}?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php if ($idtipe == 2) { ?>
                        <tr>
                            <th>Dokter 3</th>
                            <td>
                                <div class="col-sm-12">
                                    <select tabindex="45" name="iddokter3" id="iddokter3" <?=(UserAccesForm($user_acces_form, ['341']) == false) ? 'disabled' : ''?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                        <option value="">Pilih Opsi</option>
                                        <?php foreach (get_all('mpoliklinik_dokter', ['idpoliklinik' => $idpoliklinik]) as $r) {
		foreach (get_all('mdokter', ['id' => $r->iddokter]) as $s) {?>
                                        <option value="<?= $s->id?>" <?=($iddokter3 == $s->id ? 'selected="selected"' : '')?>><?= $s->nama?></option>
                                        <?php } ?>
                                        <?php
	}?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>

                    </table>
                </div>
            </div>
        </div>
        <!-- End List Pasien -->

        <!-- Start Input Tindakan -->
        <div class="col-sm-6 col-lg-6">
            <div class="block block-rounded block-bordered">
                <div class="block-header">
                    <h3 class="block-title">Tindakan</h3>
                </div>
                <div class="block-content">
                    <table class="table">
                        <tr>
                            <th width="33%">Keluhan</th>
                            <td>
                                <div class="form-val">
                                    <div class="col-sm-12">
                                        <textarea class="form-control input-sm" <?=(UserAccesForm($user_acces_form, ['342']) == false) ? 'readonly' : ''?> name="keluhan" rows="2" placeholder="Keluhan">{keluhan}</textarea>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tinggi Badan</th>
                            <td>
                                <div class="form-val">
                                    <div class="col-sm-12">
                                        <input class="form-control input-sm number" type="text" name="tinggibadan" value="{tinggibadan}" placeholder="0" data-v-min="0" data-v-max="999.99" autocomplete="off" style="display: inline-block;width:20%;text-align: right;">
                                        <span style="font-weight: 600;">cm<?=str_repeat('&nbsp;', 13)?></span>
                                        <span style="margin-top:10px;font-size:11px;font-weight: 600;">SUHU BADAN</span>
                                        <input class="form-control input-sm number" type="text" name="suhubadan" value="{suhubadan}" data-v-min="0" data-v-max="999.99" placeholder="0" autocomplete="off" style="display: inline-block;width:20%;text-align: right;">
                                        <span style="font-weight: 600;">&deg;C</span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Berat Badan</th>
                            <td>
                                <div class="form-val">
                                    <div class="col-sm-12">
                                        <input class="form-control input-sm number" type="text" name="beratbadan" value="{tinggibadan}" placeholder="0" data-v-min="0" data-v-max="999.99" autocomplete="off" style="display: inline-block; width: 20%; text-align: right;">
                                        <span style="font-weight: 600;">kg<?=str_repeat('&nbsp;', 8)?></span>
                                        <span style="margin-top:10px;font-size:11px;font-weight: 600;">LINGKAR KEPALA</span>
                                        <input class="form-control input-sm number" type="text" name="lingkarkepala" value="{lingkarkepala}" placeholder="0" data-v-min="0" data-v-max="999.99" autocomplete="off" style="display: inline-block; width: 20%; text-align: right;">
                                        <span style="font-weight: 600;">cm</span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tensi</th>
                            <td>
                                <div class="form-val">
                                    <div class="col-sm-12">
                                        <input class="form-control input-sm number" type="text" name="sistole" value="{sistole}" placeholder="0" data-v-min="0" data-v-max="999.99" autocomplete="off" style="display: inline-block; width: 20%; text-align: right;">
                                        <span style="font-weight: 600;"> / </span>
                                        <input class="form-control input-sm number" type="text" name="disastole" value="{disastole}" placeholder="0" data-v-min="0" data-v-max="999.99" autocomplete="off" style="display: inline-block; width: 20%; text-align: right;">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Keterangan Lain</th>
                            <td>
                                <div class="form-val">
                                    <div class="col-sm-12">
                                        <textarea class="form-control input-sm" name="keterangan" rows="2" placeholder="Keterangan">{keterangan}</textarea>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Kasus</th>
                            <td>
                                <div class="form-val">
                                    <div class="col-sm-12">
                                        <label class="css-input css-radio css-radio-sm css-radio-primary push-10-r">
                                            <input type="radio" name="statuskasus" <?=($statuskasus == 1 ? 'checked' : '')?> value="1"><span></span>Baru
                                        </label>
                                        <label class="css-input css-radio css-radio-sm css-radio-primary push-10-r">
                                            <input type="radio" name="statuskasus" <?=($statuskasus == 2 ? 'checked' : '')?> value="2"><span></span>Lama
                                        </label>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Diagnosa</th>
                            <td>
                                <div class="form-val">
                                    <div class="col-sm-12">
                                        <textarea class="form-control input-sm" name="diagnosa" rows="2" placeholder="Diagnosa">{diagnosa}</textarea>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Alergi obat</th>
                            <td>
                                <div class="form-val">
                                    <div class="col-sm-12">
                                        <textarea class="form-control input-sm" name="alergiobat" rows="2" placeholder="Alergi Obat">{alergiobat}</textarea>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tanggal Kontrol <span class="text-danger">*</span></th>
                            <td>
                                <div class="form-val">
                                    <div class="col-sm-12">
                                        <input class="js-datepicker form-control" type="text" name="tanggalkontrol" data-date-format="yyyy-mm-dd" value="{tanggalkontrol}">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tindak Lanjut</th>
                            <td>
                                <div class="form-val">
                                    <div class="col-sm-12">
                                        <select class="js-select2 form-control input-sm" id="tindaklanjut" name="tindaklanjut">
                                            <option value="0" selected disabled>Pilih Opsi</option>
                                            <option value="1" <?=($tindaklanjut == 1 ? 'selected' : '')?>>Pulang</option>
                                            <option value="2" <?=($tindaklanjut == 2 ? 'selected' : '')?>>Dirawat</option>
                                            <option value="3" <?=($tindaklanjut == 3 ? 'selected' : '')?>>Dirujuk</option>
                                            <option value="4" <?=($tindaklanjut == 4 ? 'selected' : '')?>>Menolak Dirawat</option>
                                            <option value="6" <?=($tindaklanjut == 5 ? 'selected' : '')?>>Pindah RS</option>
                                            <option value="5" <?=($tindaklanjut == 6 ? 'selected' : '')?>>Dibatalkan</option>
                                            <option value="7" <?=($tindaklanjut == 7 ? 'selected' : '')?>>Pulang Dengan SP</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Sebab Luar</th>
                            <td>
                                <div class="form-val">
                                    <div class="col-sm-12">
                                        <select class="js-select2 form-control input-sm" name="sebabluar">
                                            <option value="0" selected disabled>Pilih Opsi</option>
                                            <?php foreach ($list_sebab_luar as $row) {?>
                                            <option value="<?=$row->id?>" <?=($sebabluar == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Kelompok Diagnosa</th>
                            <td>
                                <div class="form-val">
                                    <div class="col-sm-12">
                                        <select class="form-control input-sm" id="kelompokDiagnosa" name="idkelompok_diagnosa[]" multiple>
                                            <?php foreach ($list_kelompok_diagnosa as $row) {?>
                                            <option value="<?=$row->id?>"><?=$row->nama?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr id="form-menolakdirawat" style="display: none;">
                            <th>Menolak Dirawat</th>
                            <td>
                                <div class="form-val">
                                    <div class="col-sm-12">
                                        <select style="width: 100%" class="js-select2 form-control input-sm" id="menolakdirawat" name="menolakdirawat">
                                            <option value="0" selected disabled>Pilih Opsi</option>
                                            <option value="1" <?=($menolakdirawat == 1 ? 'selected' : '')?>>Berunding</option>
                                            <option value="2" <?=($menolakdirawat == 2 ? 'selected' : '')?>>Keberatan Biaya</option>
                                            <option value="3" <?=($menolakdirawat == 3 ? 'selected' : '')?>>BPJS</option>
                                            <option value="4" <?=($menolakdirawat == 4 ? 'selected' : '')?>>Asuransi</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <!-- End Input Tindakan -->

        <!-- List Tindakan Pelayanan -->
        <div class="row">
            <div class="col-sm-12">
                <div class="progress progress-mini">
                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                </div>
                <h5 style="margin-bottom: 10px;">Tindakan</h5>
                <div class="control-group">
                    <div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0;">
                        <table class="table table-striped table-bordered" id="tindakan-list" style="margin-bottom:0px">
                            <thead>
                                <tr>
                                    <th style="<?=($idtipe == 2 ? '' : 'display:none')?>" width="10%">Dokter</th>
                                    <th width="25%">Nama Tindakan</th>
                                    <th width="15%">Tarif</th>
                                    <th width="10%">Kuantitas</th>
                                    <th width="10%">Diskon Jasa Pelayanan (%)</th>
                                    <th width="15%">Jumlah</th>
                                    <th width="15%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($this->uri->segment(2) == 'edit') { ?>
                                <?php foreach ($list_pelayanan as $row) { ?>
                                <tr>
                                    <td style='display: none' width='10%'><?=$row->iddokter?></td>
                                    <td style="<?=($idtipe == 2 ? '' : 'display:none')?>" width='10%'><?=$row->namadokter?></td>
                                    <td width='10%'><?=$row->namatarif?></td>
                                    <td style='display: none' width='10%'><?=number_format($row->jasasarana)?></td>
                                    <td style='display: none' width='10%'><?=number_format($row->jasapelayanan)?></td>
                                    <td style='display: none' width='10%'><?=number_format($row->bhp)?></td>
                                    <td style='display: none' width='10%'><?=number_format($row->biayaperawatan)?></td>
                                    <td width='10%'><?=number_format($row->total)?></td>
                                    <td width='10%'><?=number_format($row->kuantitas)?></td>
                                    <?php if ($row->jasapelayanan_disc > 0) { ?>
                                    <td width='10%'><?=number_format(($row->jasapelayanan_disc / $row->jasapelayanan) * 100, 2)?></td>
                                    <?php } else { ?>
                                    <td width='10%'><?=number_format($row->jasapelayanan_disc, 2)?></td>
                                    <?php } ?>
                                    <td width='10%' hidden><?=$row->jasapelayanan_disc?></td>
                                    <td width='10%'><?=number_format($row->totalkeseluruhan)?></td>
                                    <td style='display:none'><?=$row->idpelayanan?></td>
                                    <td style='display:none'><?=$row->statusverifikasi?></td>
                                    <td width='10%'>
                                        <?php if ($row->statusverifikasi == 0 && $statuskasir != 2) { ?>
                                        <div class='btn btn-group'>
                                            <?if (UserAccesForm($user_acces_form, ['344'])) { ?>
                                            <a href='#' class='btn btn-primary btn-sm tindakan-edit' data-toggle='modal' data-target='#tindakan-modal' id='tindakan-modal-show' data-toggle='tooltip' title='Ubah'><i class='fa fa-pencil'></i></a>
                                            <?}?>
                                            <?if (UserAccesForm($user_acces_form, ['345'])) { ?>
                                            <a href='#' class='btn btn-danger btn-sm tindakan-delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>
                                    <?}?>
                    </div>
                    <?php } else { ?>
                    <div class='btn btn-group'>
                        <button class="btn btn-md btn-danger"><i class="fa fa-lock"></i></button>
                    </div>
                    <?php } ?>
                    </td>
                    </tr>
                    <?php } ?>
                    <?php } ?>
                    </tbody>
                    </table>
                    <table class="table table-striped table-bordered">
                        <tbody>
                            <tr>
                                <td colspan="2" class="hidden-phone">

                                    <?if (UserAccesForm($user_acces_form, ['343'])) { ?>
                                    <button data-toggle="modal" data-target="#tindakan-modal" id="tindakan-modal-show" <?= ($statuskasir == 2 ? 'disabled' : '') ?> class="btn btn-info btn-xs" type="button">Tambah Tindakan</button>
                                    <?}?>

                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="progress progress-mini">
        <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
    </div>

    <!-- List obat -->
    <div class="row">
        <div class="col-sm-12">
            <h5 style="margin-bottom: 10px;">Obat</h5>
            <div class="control-group">
                <div class="span12" style="margin-left: 0;">
                    <table class="table table-striped table-bordered" id="obat-list" style="margin-bottom:0px">
                        <thead>
                            <tr>
                                <th width="30%">Nama Obat</th>
                                <th width="15%">Satuan</th>
                                <th width="15%">Tarif</th>
                                <th width="15%">Kuantitas</th>
                                <th width="15%">Jumlah</th>
                                <th width="15%" class="hidden-phone">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($this->uri->segment(2) == 'edit') { ?>
                            <?php foreach ($list_obat as $row) { ?>
                            <tr>
                                <td style='display: none'><?=$row->kodeobat?></td>
                                <td width='10%'><?=$row->namaobat?></td>
                                <td width='10%'><?=$row->namasatuan?></td>
                                <td style='display: none'><?=number_format($row->hargadasar)?></td>
                                <td style='display: none'><?=number_format($row->margin)?></td>
                                <td width='10%'><?=number_format($row->hargajual)?></td>
                                <td width='10%'><?=number_format($row->kuantitas)?></td>
                                <td width='10%'><?=number_format($row->totalkeseluruhan)?></td>
                                <td style='display: none'><?=$row->idobat?></td>
                                <td style='display: none'><?=$row->idunit?></td>
                                <td style='display: none'><?=$row->statusstok?></td>
                                <td style='display:none'><?=$row->statusverifikasi?></td>
                                <td width='10%'>
                                    <?php if ($row->statusverifikasi == 0 && $statuskasir != 2) { ?>
                                    <div class='btn btn-group'>
                                        <?if (UserAccesForm($user_acces_form, ['347'])) { ?>
                                        <a href='#' class='btn btn-primary btn-sm obat-edit' data-toggle='modal' data-target='#obat-modal' id='obat-modal-show' data-toggle='tooltip' title='Ubah'><i class='fa fa-pencil'></i></a>
                                        <?}?>
                                        <?if (UserAccesForm($user_acces_form, ['348'])) { ?>
                                        <a href='#' class='btn btn-danger btn-sm obat-delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>
                                <?}?>
                </div>
                <?php } else { ?>
                <div class='btn btn-group'>
                    <button class="btn btn-md btn-danger"><i class="fa fa-lock"></i></button>
                </div>
                <?php } ?>
                </td>
                </tr>
                <?php } ?>
                <?php } ?>
                </tbody>
                </table>
                <table class="table table-striped table-bordered">
                    <tbody>
                        <tr>
                            <td colspan="4" class="hidden-phone">
                                <?if (UserAccesForm($user_acces_form, ['346'])) { ?>
                                <button data-toggle="modal" data-target="#obat-modal" id="obat-modal-show" <?= ($statuskasir == 2 ? 'disabled' : '') ?> class="btn btn-info btn-xs" type="button">Tambah Obat</button>
                                <?}?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="progress progress-mini">
    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
</div>

<!-- List Alkes -->
<div class="row">
    <div class="col-sm-12">
        <h5 style="margin-bottom: 10px;">Alat Kesehatan</h5>
        <div class="control-group">
            <div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0;">
                <table class="table table-striped table-bordered" id="alkes-list" style="margin-bottom:0px">
                    <thead>
                        <tr>
                            <th width="30%">Nama Alat Kesehatan</th>
                            <th width="15%">Satuan</th>
                            <th width="15%">Tarif</th>
                            <th width="15%">Kuantitas</th>
                            <th width="15%">Jumlah</th>
                            <th width="15%" class="hidden-phone">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($this->uri->segment(2) == 'edit') { ?>
                        <?php foreach ($list_alkes as $row) { ?>
                        <tr>
                            <td style='display: none'><?=$row->kodealkes?></td>
                            <td width='10%'><?=$row->namaalkes?></td>
                            <td width='10%'><?=$row->namasatuan?></td>
                            <td style='display: none'><?=$row->hargadasar?></td>
                            <td style='display: none'><?=$row->margin?></td>
                            <td width='10%'><?=$row->hargajual?></td>
                            <td width='10%'><?=$row->kuantitas?></td>
                            <td width='10%'><?=$row->totalkeseluruhan?></td>
                            <td style='display: none'><?=$row->idalkes?></td>
                            <td style='display: none'><?=$row->idunit?></td>
                            <td style='display:none'><?=$row->statusverifikasi?></td>
                            <td width='10%'>
                                <?php if ($row->statusverifikasi == 0 && $statuskasir != 2) { ?>
                                <div class='btn btn-group'>
                                    <?if (UserAccesForm($user_acces_form, ['350'])) { ?>
                                    <a href='#' class='btn btn-primary btn-sm alkes-edit' data-toggle='modal' data-target='#alkes-modal' id='alkes-modal-show' data-toggle='tooltip' title='Ubah'><i class='fa fa-pencil'></i></a>
                                    <?}?>
                                    <?if (UserAccesForm($user_acces_form, ['351'])) { ?>
                                    <a href='#' class='btn btn-danger btn-sm alkes-delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>
                            <?}?>
            </div>
            <?php } else { ?>
            <div class='btn btn-group'>
                <button class="btn btn-md btn-danger"><i class="fa fa-lock"></i></button>
            </div>
            <?php } ?>
            </td>
            </tr>
            <?php } ?>
            <?php } ?>
            </tbody>
            </table>
            <table class="table table-striped table-bordered">
                <tbody>
                    <tr>
                        <td colspan="5" class="hidden-phone">
                            <?if (UserAccesForm($user_acces_form, ['349'])) { ?>
                            <button data-toggle="modal" data-target="#alkes-modal" id="alkes-modal-show" <?= ($statuskasir == 2 ? 'disabled' : '') ?> class="btn btn-info btn-xs" type="button">Tambah Alat Kesehatan</button>
                            <?}?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>

<div class="progress progress-mini">
    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
</div>

<div class="row">
    <div class="col-sm-12">
        <h5>Dirujuk</h5>
        <div class="control-group">
            <div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0;">
                <?php if ($statuskasir == 2 || !UserAccesForm($user_acces_form, ['354'])) { ?>
                <input type="hidden" name="rujukfarmasi" value="<?=$rujukfarmasi?>"><span></span>
                <div class="checkbox">
                    <label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                        <input type="checkbox" disabled <?=($rujukfarmasi == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
                        Farmasi
                    </label>
                </div>
                <?php } else { ?>
                <div class="checkbox">
                    <label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                        <input type="checkbox" name="rujukfarmasi" <?=($rujukfarmasi == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
                        Farmasi
                    </label>
                </div>
                <?php } ?>

                <?php if ($statuskasir == 2 || !UserAccesForm($user_acces_form, ['354'])) { ?>
                <input type="hidden" name="rujuklaboratorium" value="<?=$rujuklaboratorium?>"><span></span>
                <div class="checkbox">
                    <label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                        <input type="checkbox" disabled <?=($rujuklaboratorium == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
                        Laboratorium
                    </label>
                </div>
                <?php } else { ?>
                <div class="checkbox">
                    <label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                        <input type="checkbox" name="rujuklaboratorium" <?=($rujuklaboratorium == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
                        Laboratorium
                    </label>
                </div>
                <?php } ?>

                <?php if ($statuskasir == 2 || !UserAccesForm($user_acces_form, ['353'])) { ?>
                <input type="hidden" name="rujukradiologi" value="<?=$rujukradiologi?>"><span></span>
                <div class="checkbox">
                    <label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                        <input type="checkbox" disabled <?=($rujukradiologi == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
                        Radiologi
                    </label>
                </div>
                <?php } else { ?>
                <div class="checkbox">
                    <label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                        <input type="checkbox" name="rujukradiologi" <?=($rujukradiologi == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
                        Radiologi
                    </label>
                </div>
                <?php } ?>

                <?php if ($statuskasir == 2 || !UserAccesForm($user_acces_form, ['355'])) { ?>
                <input type="hidden" name="rujukfisioterapi" value="<?=$rujukfisioterapi?>"><span></span>
                <div class="checkbox">
                    <label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                        <input type="checkbox" disabled <?=($rujukfisioterapi == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
                        Fisioterapi
                    </label>
                </div>
                <?php } else { ?>
                <div class="checkbox">
                    <label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                        <input type="checkbox" name="rujukfisioterapi" <?=($rujukfisioterapi == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
                        Fisioterapi
                    </label>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<hr>
<input type="hidden" id="idtipe" name="idtipe" value="{idtipe}">
<div class="text-right bg-light lter">
    <button class="btn btn-info" type="submit">Simpan</button>
    <a href="{base_url}tpoliklinik_tindakan/index" class="btn btn-default" type="button">Batal</a>
</div>
<!-- End View Tpoliklinik Tindakan -->

<!-- Modal Tindakan Pelayanan -->
<div class="modal fade in black-overlay" id="tindakan-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width:80%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Daftar Tindakan</h3>
                </div>
                <div class="block-content">
                    <div id="tindakan-step-1">
                        <table width="100%" class="table table-striped table-striped table-hover table-bordered" id="tindakan-table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Jasa Sarana</th>
                                    <th>Jasa Pelayanan</th>
                                    <th>BHP</th>
                                    <th>Biaya Perawatan</th>
                                    <th>Total Tarif</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div style="display: none;" id="tindakan-step-2">
                        <input type="hidden" class="form-control" id="rowindex">
                        <table class="table table-striped table-bordered table-hover" id="tindakan-table-step-2">
                            <thead>
                                <tr>
                                    <th style="<?=($idtipe == 2 ? '' : 'display:none')?>">Dokter</th>
                                    <th>Tindakan</th>
                                    <th>Jasa Sarana</th>
                                    <th>Jasa Pelayanan</th>
                                    <th>Diskon Jasa Pelayanan (%)</th>
                                    <th>Diskon Jasa Pelayanan (Rp)</th>
                                    <th>BHP</th>
                                    <th>Biaya Perawatan</th>
                                    <th>Tarif</th>
                                    <th>Kuantitas</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="tindakan-input">
                                    <td style="<?=($idtipe == 2 ? '' : 'display:none')?>">
                                        <select id="iddoktertindakan" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                            <option value="">Pilih Opsi</option>
                                        </select>
                                    </td>
                                    <td id="namatarif"></td>
                                    <td id="jasasarana"></td>
                                    <td id="jasapelayanan"></td>
                                    <td><input class="form-control input-sm number-diskon" type="text" id="diskon" value="0" autocomplete="off" /></td>
                                    <td><input class="form-control input-sm number" type="text" id="diskonRp" value="0" autocomplete="off" /></td>
                                    <td id="bhp"></td>
                                    <td id="biayaperawatan"></td>
                                    <td id="totaltarif"></td>
                                    <td><input class="form-control input-sm number" type="text" id="kuantitas" value="1" autocomplete="off" /></td>
                                    <td><input class="form-control input-sm number" type="text" id="totalkeseluruhan" value="" autocomplete="off" readonly /></td>
                                    <td style="display: none" id="idtarif"></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="modal-footer">
                            <button class="btn btn-sm btn-default" id="tindakan-back" type="button">Batalkan</button>
                            <button class="btn btn-sm btn-primary" type="button" id="tindakan-add">Tambahkan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Pelayanan -->

<!-- Modal obat -->
<div class="modal fade in black-overlay" id="obat-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Daftar obat</h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 form-horizontal">
                            <div class="form-val">
                                <label class="col-sm-4" for="idunitpelayananobat">Unit Pelayanan</label>
                                <div class="col-sm-8">
                                    <select style="width: 100%" id="idunitpelayananobat" class="form-control input-sm">
                                        <?php foreach ($list_unitpelayanan as $row) { ?>
                                        <?php $idp = $this->Tpoliklinik_tindakan_model->getDefaultUnitPelayananUser() ?>
                                        <?php if ($idp == $row->id): ?>
                                        <option value="<?=$row->id?>" selected="selected"><?=$row->nama?></option>
                                        <?php else: ?>
                                        <option value="<?=$row->id?>"><?=$row->nama?></option>
                                        <?php endif ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div id="obat-step-1">
                        <table width="100%" class="table table-striped table-striped table-hover table-bordered" id="obat-table">
                            <thead>
                                <tr>
                                    <th>Kode obat</th>
                                    <th>Nama obat</th>
                                    <th>Satuan</th>
                                    <th>Tarif</th>
                                    <th>Stok</th>
                                    <th>Kartu Stok</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div style="display: none;" id="obat-step-2">
                        <input type="hidden" class="form-control" id="rowindexobat">
                        <table class="table table-striped table-bordered table-hover" id="obat-table-step-2">
                            <thead>
                                <tr>
                                    <th>Kode Obat</th>
                                    <th>Nama Obat</th>
                                    <th>Satuan</th>
                                    <th>Tarif</th>
                                    <th>Kuantitas</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="obat-input">
                                    <td id="kodeobat"></td>
                                    <td id="namaobat"></td>
                                    <td id="satuanobat"></td>
                                    <td style="display: none" id="hargadasarobat"></td>
                                    <td style="display: none" id="marginobat"></td>
                                    <td id="hargajualobat"></td>
                                    <td><input class="form-control input-sm number" type="text" id="kuantitasobat" value="1" autocomplete="off" /></td>
                                    <td><input class="form-control input-sm number" type="text" id="totalkeseluruhanobat" value="" autocomplete="off" readonly></td>
                                    <td style="display: none" id="idobat"></td>
                                    <td style="display: none" id="statusstok"></td>
                                    <td style="display: none" id="statusverifikasi"></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="modal-footer">
                            <button class="btn btn-sm btn-default" id="obat-back" type="button">Batalkan</button>
                            <button class="btn btn-sm btn-primary" type="button" id="obat-add">Tambahkan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal obat -->

<!-- Modal Alkes -->
<div class="modal fade in black-overlay" id="alkes-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Daftar Alat Kesehatan</h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 form-horizontal">
                            <div class="form-val">
                                <label class="col-sm-4" for="idunitpelayananalkes">Unit Pelayanan</label>
                                <div class="col-sm-8">
                                    <select style="width: 100%" id="idunitpelayananalkes" class="form-control input-sm">
                                        <?php foreach ($list_unitpelayanan as $row) { ?>
                                        <?php $idp = $this->Tpoliklinik_tindakan_model->getDefaultUnitPelayananUser() ?>
                                        <?php if ($idp == $row->id): ?>
                                        <option value="<?=$row->id?>" selected="selected"><?=$row->nama?></option>
                                        <?php else: ?>
                                        <option value="<?=$row->id?>"><?=$row->nama?></option>
                                        <?php endif ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div id="alkes-step-1">
                        <table width="100%" class="table table-striped table-striped table-hover table-bordered" id="alkes-table">
                            <thead>
                                <tr>
                                    <th>Kode Alkes</th>
                                    <th>Nama Alkes</th>
                                    <th>Satuan</th>
                                    <th>Tarif</th>
                                    <th>Stok</th>
                                    <th>Kartu Stok</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div style="display: none;" id="alkes-step-2">
                        <input type="hidden" class="form-control" id="rowindexalkes">
                        <table class="table table-striped table-bordered table-hover" id="alkes-table-step-2">
                            <thead>
                                <tr>
                                    <th>Kode Alkes</th>
                                    <th>Nama Alkes</th>
                                    <th>Satuan</th>
                                    <th>Tarif</th>
                                    <th>Kuantitas</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="alkes-input">
                                    <td id="kodealkes"></td>
                                    <td id="namaalkes"></td>
                                    <td id="satuanalkes"></td>
                                    <td style="display: none" id="hargadasaralkes"></td>
                                    <td style="display: none" id="marginalkes"></td>
                                    <td id="hargajualalkes"></td>
                                    <td><input class="form-control input-sm number" type="text" id="kuantitasalkes" value="1" autocomplete="off" /></td>
                                    <td><input class="form-control input-sm number" type="text" id="totalkeseluruhanalkes" value="" autocomplete="off" readonly></td>
                                    <td style="display: none" id="idalkes"></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="modal-footer">
                            <button class="btn btn-sm btn-default" id="alkes-back" type="button">Batalkan</button>
                            <button class="btn btn-sm btn-primary" type="button" id="alkes-add">Tambahkan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" name="tindakan-value" id="tindakan-value" value="">
<input type="hidden" name="obat-value" id="obat-value" value="">
<input type="hidden" name="alkes-value" id="alkes-value" value="">

<?php echo form_hidden('id', $id); ?>
<?php echo form_hidden('idpendaftaran', $idpendaftaran); ?>
<?php echo form_close() ?>
<br><br>
</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
function getAlkesUnit(idunit) {
    var idunit = idunit;
    var idkelompokpasien = '<?=$idkelompokpasien?>';

    $('#alkes-step-1').fadeIn('slow');
    $('#alkes-step-2').hide();
    $('#alkes-table tbody').empty();
    $('#alkes-table tbody').append('<tr><td colspan="5"><center><img src="../../assets/ajax/img/ajax-loader.gif"></center></td></tr>');

    $("#alkes-table tbody").empty();

    $('#alkes-table').DataTable().destroy();

    $('#alkes-table').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}tpoliklinik_tindakan/getAlkesUnit/' + idunit + '/' + idkelompokpasien,
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "10%",
                "targets": 0,
                "orderable": true
            },
            {
                "width": "30%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 2,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 3,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 4,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 5,
                "orderable": true
            },
        ]
    });
}

function getObatUnit(idunit) {
    var idunit = idunit;
    var idkelompokpasien = '<?=$idkelompokpasien?>';

    $('#obat-step-1').fadeIn('slow');
    $('#obat-step-2').hide();
    $('#obat-table tbody').empty();
    $('#obat-table tbody').append('<tr><td colspan="5"><center><img src="../../assets/ajax/img/ajax-loader.gif"></center></td></tr>');

    $("#obat-table tbody").empty();

    $('#obat-table').DataTable().destroy();

    $('#obat-table').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}tpoliklinik_tindakan/getObatUnit/' + idunit + '/' + idkelompokpasien,
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "10%",
                "targets": 0,
                "orderable": true
            },
            {
                "width": "30%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 2,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 3,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 4,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 5,
                "orderable": true
            },
        ]
    });
}
$(document).ready(function() {
    $(".number").number(true, 0, '.', ',');
    $(".number-diskon").number(true, 2, '.', ',');

    $("#kelompokDiagnosa").select2({
        tag: true
    });

    $("#kelompokDiagnosa").on("select2:select", function(evt) {
        var element = evt.params.data.element;
        var $element = $(element);

        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
    });

    $('#kelompokDiagnosa').val([ <?= $idKelompokDiagnosa ?> ]).trigger('change');

    getAlkesUnit('<?php echo $this->Tpoliklinik_tindakan_model->getDefaultUnitPelayananUser() ?>')
    getObatUnit('<?php echo $this->Tpoliklinik_tindakan_model->getDefaultUnitPelayananUser() ?>')

    $(document).on("focus", "tr td input", function() {
        $(this).select();
    });

    $(document).on("focus", "tr td textarea", function() {
        $(this).select();
    });

    $('textarea').keyup(function() {
        $(this).val($(this).val().toUpperCase());
    });

    $(document).on("change", "#tindaklanjut", function() {
        if ($(this).val() == 4) {
            $('#menolakdirawat').val(0);
            $('#form-menolakdirawat').show()
        } else {
            $('#form-menolakdirawat').hide();
        }
    });

    $("#form-work").submit(function(e) {
        var form = this;
        $("*[disabled]").not(true).removeAttr("disabled");

        var tindakan_tbl = $('table#tindakan-list tbody tr').get().map(function(row) {
            return $(row).find('td').get().map(function(cell) {
                return $(cell).html(); + '/' + idkelompokpasien
            });
        });

        var obat_tbl = $('table#obat-list tbody tr').get().map(function(row) {
            return $(row).find('td').get().map(function(cell) {
                return $(cell).html();
            });
        });

        var alkes_tbl = $('table#alkes-list tbody tr').get().map(function(row) {
            return $(row).find('td').get().map(function(cell) {
                return $(cell).html();
            });
        });

        $("#tindakan-value").val(JSON.stringify(tindakan_tbl));
        $("#obat-value").val(JSON.stringify(obat_tbl));
        $("#alkes-value").val(JSON.stringify(alkes_tbl));

        swal({
            title: "Berhasil!",
            text: "Proses penyimpanan data.",
            type: "success",
            timer: 1500,
            showConfirmButton: false
        });
    });

    // Tindakan Pelayanan
    $('#tindakan-table').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}tpoliklinik_tindakan/getTindakanRawatJalan/' + '<?=$idtipe?>' + '/' + '<?=$idkelompokpasien?>' + '/' + '<?=$idrekanan?>',
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": true
            },
            {
                "width": "25%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 2,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 3,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 4,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 5,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 6,
                "orderable": true
            }
        ]
    });

    $("#tindakan-modal-show").click(function() {
        $('#tindakan-step-1').fadeIn('slow');
        $('#tindakan-step-2').hide();
    })

    $("#tindakan-back").click(function() {
        $('#tindakan-step-1').fadeIn('slow');
        $('#tindakan-step-2').fadeOut();
        $("#kuantitas").val('');
        $("#diskon").val('');
        $("#diskonRp").val('');
    });

    $(document).on("keyup", "#diskon", function() {
        var jasapelayanan = $("#jasapelayanan").html().replace(/,/g, '');
        if ($("#diskon").val() == '') {
            $("#diskon").val(0)
        }
        if (parseFloat($(this).val()) > 100) {
            $(this).val(100);
        }

        var discount_rupiah = parseFloat(jasapelayanan * parseFloat($(this).val()) / 100);
        $("#diskonRp").val(discount_rupiah);
    });

    $(document).on("keyup", "#diskonRp", function() {
        var jasapelayanan = $("#jasapelayanan").html().replace(/,/g, '');
        if ($("#diskonRp").val() == '') {
            $("#diskonRp").val(0)
        }
        if (parseFloat($(this).val()) > jasapelayanan) {
            $(this).val(jasapelayanan);
        }

        var discount_percent = parseFloat((parseFloat($(this).val() * 100)) / jasapelayanan);
        $("#diskon").val(discount_percent);
    });

    $(document).on("click", "#tindakan-select", function() {
        $('#tindakan-step-1').fadeOut();
        $('#tindakan-step-2').fadeIn('slow');
        $('table#tindakan-table-step-2 tbody').empty();

        var idtipe = $("#idtipe").val();

        var optionSelect = '';
        optionSelect += '<option value="' + $("#iddokter1 option:selected").val() + '">' + $("#iddokter1 option:selected").text() + '</option>';
        if ($("#iddokter2 option:selected").val()) {
            optionSelect += '<option value="' + $("#iddokter2 option:selected").val() + '">' + $("#iddokter2 option:selected").text() + '</option>';
        }
        if ($("#iddokter3 option:selected").val()) {
            optionSelect += '<option value="' + $("#iddokter3 option:selected").val() + '">' + $("#iddokter3 option:selected").text() + '</option>';
        }

        content = '<tr id="tindakan-input">';
        if (idtipe == 2) {
            content += '<td><select id="iddoktertindakan" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">' + optionSelect + '</select></td>';
        } else {
            content += '<td style="display: none">AUTO_GET_DOKTER_POLIKLINIK</td>';
        }
        content += '<td id="namatarif">' + $(this).closest('tr').find("td:eq(1) a").html() + '</td>';
        content += '<td id="jasasarana">' + $(this).closest('tr').find("td:eq(2)").html() + '</td>';
        content += '<td id="jasapelayanan">' + $(this).closest('tr').find("td:eq(3)").html() + '</td>';
        content += '<td><input class="form-control input-sm number-diskon" type="text" id="diskon" value="0" autocomplete="off"/></td>';
        content += '<td><input class="form-control input-sm number" type="text" id="diskonRp" value="0" autocomplete="off"/></td>';
        content += '<td id="bhp">' + $(this).closest('tr').find("td:eq(4)").html() + '</td>';
        content += '<td id="biayaperawatan">' + $(this).closest('tr').find("td:eq(5)").html() + '</td>';
        content += '<td id="totaltarif">' + $(this).closest('tr').find("td:eq(6)").html() + '</td>';
        content += '<td><input class="form-control input-sm number" type="text" id="kuantitas" value="1" autocomplete="off"/></td>';
        content += '<td><input class="form-control input-sm number" type="text" id="totalkeseluruhan" value="' + $(this).closest('tr').find("td:eq(6)").html() + '" autocomplete="off" readonly/></td>';
        content += '<td style="display: none" id="idtarif">' + $(this).data("idtindakan") + '</td>';
        content += '</tr>';

        $("table#tindakan-table-step-2 tbody").html(content);

        $(".number").number(true, 0, '.', ',');
        $(".number-diskon").number(true, 2, '.', ',');
        $("#kuantitas").focus();
    });

    $(document).on("keyup", "tr#tindakan-input td input", function() {
        var jasasarana = $("#jasasarana").text().replace(/,/g, '');
        var jasapelayanan = $("#jasapelayanan").text().replace(/,/g, '');
        var bhp = $("#bhp").text().replace(/,/g, '');
        var biayaperawatan = $("#biayaperawatan").text().replace(/,/g, '');

        var diskon_jasapelayanan = $("#diskonRp").val().replace(/,/g, '');
        var totaltarif = parseFloat(jasasarana) + parseFloat(jasapelayanan) + parseFloat(bhp) + parseFloat(biayaperawatan) - parseFloat(diskon_jasapelayanan);

        $("#totaltarif").html($.number(totaltarif));

        var kuantitas = $("#kuantitas").val().replace(/,/g, '');
        totalTarif = (totaltarif * kuantitas);
        $("#totalkeseluruhan").val(totalTarif);
    });

    $(document).on("click", "#tindakan-add", function() {
        var idtipe = $("#idtipe").val();
        var valid = validateDetailTindakan();
        if (!valid) return false;

        var rowindex;
        var duplicate = false;

        if ($("#rowindex").val() != '') {
            var content = "";
            rowindex = $("#rowindex").val();
        } else {
            var content = "<tr>";
            $('#tindakan-list tbody tr').filter(function() {
                var $cells = $(this).children('td');
                if ($cells.eq(0).text() === $("#namatarif").text()) {
                    sweetAlert("Maaf...", "Tindakan " + $("#namatarif").text() + " sudah ditambahkan.", "error");
                    duplicate = true;
                    return false;
                }
            });
        }

        if (duplicate == false) {

            if (idtipe == 2) {
                content += "<td style='display: none' width='10%'>" + $("#iddoktertindakan option:selected").val(); + "</td>";
                content += "<td width='10%'>" + $("#iddoktertindakan option:selected").text(); + "</td>";
            } else {
                content += "<td style='display: none' width='10%'>AUTO_GET_ID_DOKTER_POLIKLINIK</td>";
                content += "<td style='display: none' width='10%'>AUTO_GET_NAMA_DOKTER_POLIKLINIK</td>";
            }

            content += "<td width='10%'>" + $("#namatarif").text(); + "</td>";
            content += "<td style='display: none' width='10%'>" + $("#jasasarana").text(); + "</td>";
            content += "<td style='display: none' width='10%'>" + $("#jasapelayanan").text(); + "</td>";
            content += "<td style='display: none' width='10%'>" + $("#bhp").text(); + "</td>";
            content += "<td style='display: none' width='10%'>" + $("#biayaperawatan").text(); + "</td>";
            content += "<td width='10%'>" + $("#totaltarif").text(); + "</td>";
            content += "<td width='10%'>" + $("#kuantitas").val(); + "</td>";
            content += "<td width='10%'>" + $("#diskon").val(); + "</td>";
            content += "<td width='10%' hidden>" + $("#diskonRp").val(); + "</td>";
            content += "<td width='10%'>" + $("#totalkeseluruhan").val(); + "</td>";
            content += "<td style='display:none'>" + $("#idtarif").text(); + "</td>";
            content += "<td style='display:none'>0</td>";
            content += "<td width='10%'>";
            content += "<div class='btn btn-group'>";
            content += "<a href='#' class='btn btn-primary btn-sm tindakan-edit' data-toggle='modal' data-target='#tindakan-modal' id='tindakan-modal-show' data-toggle='tooltip' title='Ubah'><i class='fa fa-pencil'></i></a>";
            content += "<a href='#' class='btn btn-danger btn-sm tindakan-delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
            content += "</div>";
            content += "</td>";

            if ($("#rowindex").val() != '') {
                $('#tindakan-list tbody tr:eq(' + rowindex + ')').html(content);
            } else {
                content += "</tr>";
                $('#tindakan-list tbody').append(content);
            }

            $("#tindakan-back").click();
            detailClear();
        }
    });

    $(document).on("click", ".tindakan-edit", function() {
        $('#tindakan-step-1').fadeOut();
        $('#tindakan-step-2').fadeIn('slow');

        $("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);

        var iddokter = $(this).closest('tr').find("td:eq(0)").html();
        var namadokter = $(this).closest('tr').find("td:eq(1)").html();

        var optionSelect = '';
        optionSelect += '<option value="' + $("#iddokter1 option:selected").val() + '">' + $("#iddokter1 option:selected").text() + '</option>';
        if ($("#iddokter2 option:selected").val()) {
            optionSelect += '<option value="' + $("#iddokter2 option:selected").val() + '">' + $("#iddokter2 option:selected").text() + '</option>';
        }
        if ($("#iddokter3 option:selected").val()) {
            optionSelect += '<option value="' + $("#iddokter3 option:selected").val() + '">' + $("#iddokter3 option:selected").text() + '</option>';
        }

        $('#iddoktertindakan').html(optionSelect);
        $('#iddoktertindakan').val(iddokter);

        $("#namatarif").text($(this).closest('tr').find("td:eq(2)").html());
        $("#jasasarana").text($(this).closest('tr').find("td:eq(3)").html());
        $("#jasapelayanan").text($(this).closest('tr').find("td:eq(4)").html());
        $("#bhp").text($(this).closest('tr').find("td:eq(5)").html());
        $("#biayaperawatan").text($(this).closest('tr').find("td:eq(6)").html());
        $("#totaltarif").text($(this).closest('tr').find("td:eq(7)").html());
        $("#kuantitas").val($(this).closest('tr').find("td:eq(8)").html());
        $("#diskon").val($(this).closest('tr').find("td:eq(9)").html());
        $("#diskonRp").val($(this).closest('tr').find("td:eq(10)").html());
        $("#totalkeseluruhan").val($(this).closest('tr').find("td:eq(11)").html());
        $("#idtarif").text($(this).closest('tr').find("td:eq(12)").html());

        return false;
    });

    $(document).on("click", ".tindakan-delete", function() {
        var row = $(this).closest('td').parent();
        swal({
            title: '',
            text: "Apakah anda yakin akan menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false,
            closeOnCancel: true
        }).then(function() {
            return row.remove();
        })
        return false;
    });


    // Tindakan obat
    $("#idunitpelayananobat").change(function() {
        var idunit = $(this).val();
        var idkelompokpasien = '<?=$idkelompokpasien?>';

        $('#obat-step-1').fadeIn('slow');
        $('#obat-step-2').hide();
        $('#obat-table tbody').empty();
        $('#obat-table tbody').append('<tr><td colspan="5"><center><img src="../../assets/ajax/img/ajax-loader.gif"></center></td></tr>');

        $("#obat-table tbody").empty();

        $('#obat-table').DataTable().destroy();

        $('#obat-table').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                url: '{site_url}tpoliklinik_tindakan/getObatUnit/' + idunit + '/' + idkelompokpasien,
                type: "POST",
                dataType: 'json'
            },
            "columnDefs": [{
                    "width": "10%",
                    "targets": 0,
                    "orderable": true
                },
                {
                    "width": "10%",
                    "targets": 1,
                    "orderable": true
                },
                {
                    "width": "10%",
                    "targets": 2,
                    "orderable": true
                },
                {
                    "width": "10%",
                    "targets": 3,
                    "orderable": true
                },
                {
                    "width": "10%",
                    "targets": 4,
                    "orderable": true
                },
            ]
        });
    });

    $("#obat-modal-show").click(function() {
        $('#obat-step-1').fadeIn('slow');
        $('#obat-step-2').hide();
    })

    $("#obat-back").click(function() {
        $('#obat-step-1').fadeIn('slow');
        $('#obat-step-2').fadeOut();
        $("#kuantitasobat").val('');
    })

    $(document).on("click", "#obat-select", function() {
        if (parseInt($(this).data("stok")) <= 0) {
            sweetAlert("Perhatian...", "Stok Minus!", "warning");
        }

        $('#obat-step-1').fadeOut();
        $('#obat-step-2').fadeIn('slow');
        $('table#obat-table-step-2 tbody').empty();

        content = '<tr id="obat-input">';
        content += '<td id="kodeobat">' + $(this).closest('tr').find("td:eq(0) a").html() + '</td>';
        content += '<td id="namaobat">' + $(this).closest('tr').find("td:eq(1)").html() + '</td>';
        content += '<td id="satuanobat">' + $(this).closest('tr').find("td:eq(2)").html() + '</td>';
        content += '<td style="display: none" id="hargadasarobat">' + $(this).data("hargadasar") + '</td>';
        content += '<td style="display: none" id="marginobat">' + $(this).data("margin") + '</td>';
        content += '<td id="hargajualobat">' + $(this).closest('tr').find("td:eq(3)").html() + '</td>';
        content += '<td><input class="form-control input-sm number" type="text" id="kuantitasobat" value="1" autocomplete="off"/></td>';
        content += '<td><input class="form-control input-sm number" type="text" id="totalkeseluruhanobat" value="' + $(this).closest('tr').find("td:eq(3)").html() + '" autocomplete="off" readonly></td>';
        content += '<td style="display: none" id="idobat">' + $(this).data("idobat") + '</td>';
        content += '<td style="display: none" id="statusstok">' + $(this).data("statusstok") + '</td>';
        content += '<td style="display: none" id="statusverifikasi">0</td>';
        content += '</tr>';

        $("table#obat-table-step-2 tbody").html(content);

        $(".number").number(true, 0, '.', ',');
        $(".number-diskon").number(true, 2, '.', ',');
        $("#kuantitasobat").focus();
    });

    $(document).on("keyup", "tr#obat-input td input", function() {
        var totaltarif = $("#hargajualobat").text().replace(/,/g, '');
        var kuantitas = $("#kuantitasobat").val().replace(/,/g, '');

        totalTarif = (totaltarif * kuantitas);
        $("#totalkeseluruhanobat").val(totalTarif);
    });

    $(document).on("click", "#obat-add", function() {
        var valid = validateDetailObat();
        if (!valid) return false;

        var rowindex;
        var duplicate = false;

        if ($("#rowindexobat").val() != '') {
            var content = "";
            rowindex = $("#rowindexobat").val();
        } else {
            var content = "<tr>";
            $('#obat-list tbody tr').filter(function() {
                var $cells = $(this).children('td');
                if ($cells.eq(0).text() === $("#kodeobat").text()) {
                    sweetAlert("Maaf...", "obat " + $("#namaobat").text() + " sudah ditambahkan.", "error");
                    duplicate = true;
                    return false;
                }
            });
        }

        if (duplicate == false) {
            content += "<td style='display: none'>" + $("#kodeobat").text(); + "</td>";
            content += "<td width='10%'>" + $("#namaobat").text(); + "</td>";
            content += "<td width='10%'>" + $("#satuanobat").text(); + "</td>";
            content += "<td style='display: none'>" + $("#hargadasarobat").text(); + "</td>";
            content += "<td style='display: none'>" + $("#marginobat").text(); + "</td>";
            content += "<td width='10%'>" + $("#hargajualobat").text(); + "</td>";
            content += "<td width='10%'>" + $.number($("#kuantitasobat").val(), 0, '.', ','); + "</td>";
            content += "<td width='10%'>" + $.number($("#totalkeseluruhanobat").val(), 0, '.', ','); + "</td>";
            content += "<td style='display: none'>" + $("#idobat").text(); + "</td>";
            content += "<td style='display: none'>" + $("#idunitpelayananobat option:selected").val(); + "</td>";
            content += "<td style='display: none'>" + $("#statusstok").text(); + "</td>";
            content += "<td style='display: none'>" + $("#statusverifikasi").text(); + "</td>";
            content += "<td width='10%'>";
            content += "<div class='btn btn-group'>";
            content += "<a href='#' class='btn btn-primary btn-sm obat-edit' data-toggle='modal' data-target='#obat-modal' id='obat-modal-show' data-toggle='tooltip' title='Ubah'><i class='fa fa-pencil'></i></a>";
            content += "<a href='#' class='btn btn-danger btn-sm obat-delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
            content += "</div>";
            content += "</td>";

            if ($("#rowindexobat").val() != '') {
                $('#obat-list tbody tr:eq(' + rowindex + ')').html(content);
            } else {
                content += "</tr>";
                $('#obat-list tbody').append(content);
            }

            $("#obat-back").click();
            detailClear();
        }
    });

    $(document).on("click", ".obat-edit", function() {
        $('#obat-step-1').fadeOut();
        $('#obat-step-2').fadeIn('slow');

        $("#rowindexobat").val($(this).closest('tr')[0].sectionRowIndex);

        $("#kodeobat").text($(this).closest('tr').find("td:eq(0)").html());
        $("#namaobat").text($(this).closest('tr').find("td:eq(1)").html());
        $("#satuanobat").text($(this).closest('tr').find("td:eq(2)").html());
        $("#hargadasarobat").text($(this).closest('tr').find("td:eq(3)").html());
        $("#marginobat").text($(this).closest('tr').find("td:eq(4)").html());
        $("#hargajualobat").text($(this).closest('tr').find("td:eq(5)").html());
        $("#kuantitasobat").val($(this).closest('tr').find("td:eq(6)").html());
        $("#totalkeseluruhanobat").val($(this).closest('tr').find("td:eq(7)").html());
        $("#idobat").text($(this).closest('tr').find("td:eq(8)").html());
        $("#idunitpelayananobat").val($(this).closest('tr').find("td:eq(9)").html());
        $("#statusstok").val($(this).closest('tr').find("td:eq(10)").html());
        $("#statusverifikasi").val($(this).closest('tr').find("td:eq(11)").html());

        return false;
    });

    $(document).on("click", ".obat-delete", function() {
        var row = $(this).closest('td').parent();
        swal({
            title: '',
            text: "Apakah anda yakin akan menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false,
            closeOnCancel: true
        }).then(function() {
            return row.remove();
        })
        return false;
    });

    // Tindakan Alat Kesehatan
    $("#idunitpelayananalkes").change(function() {
        var idunit = $(this).val();
        var idkelompokpasien = '<?=$idkelompokpasien?>';

        $('#alkes-step-1').fadeIn('slow');
        $('#alkes-step-2').hide();
        $('#alkes-table tbody').empty();
        $('#alkes-table tbody').append('<tr><td colspan="5"><center><img src="../../assets/ajax/img/ajax-loader.gif"></center></td></tr>');

        $("#alkes-table tbody").empty();

        $('#alkes-table').DataTable().destroy();

        $('#alkes-table').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                url: '{site_url}tpoliklinik_tindakan/getAlkesUnit/' + idunit + '/' + idkelompokpasien,
                type: "POST",
                dataType: 'json'
            },
            "columnDefs": [{
                    "width": "10%",
                    "targets": 0,
                    "orderable": true
                },
                {
                    "width": "30%",
                    "targets": 1,
                    "orderable": true
                },
                {
                    "width": "10%",
                    "targets": 2,
                    "orderable": true
                },
                {
                    "width": "10%",
                    "targets": 3,
                    "orderable": true
                },
                {
                    "width": "10%",
                    "targets": 4,
                    "orderable": true
                },
                {
                    "width": "10%",
                    "targets": 5,
                    "orderable": true
                },
            ]
        });
    });

    $("#alkes-modal-show").click(function() {
        $('#alkes-step-1').fadeIn('slow');
        $('#alkes-step-2').hide();
    })

    $("#alkes-back").click(function() {
        $('#alkes-step-1').fadeIn('slow');
        $('#alkes-step-2').fadeOut();
        $("#kuantitasalkes").val('');
    })

    $(document).on("click", "#alkes-select", function() {
        $('#alkes-step-1').fadeOut();
        $('#alkes-step-2').fadeIn('slow');
        $('table#alkes-table-step-2 tbody').empty();

        content = '<tr id="alkes-input">';
        content += '<td id="kodealkes">' + $(this).closest('tr').find("td:eq(0) a").html() + '</td>';
        content += '<td id="namaalkes">' + $(this).closest('tr').find("td:eq(1)").html() + '</td>';
        content += '<td id="satuanalkes">' + $(this).closest('tr').find("td:eq(2)").html() + '</td>';
        content += '<td style="display: none" id="hargadasaralkes">' + $(this).data("hargadasar") + '</td>';
        content += '<td style="display: none" id="marginalkes">' + $(this).data("margin") + '</td>';
        content += '<td id="hargajualalkes">' + $(this).closest('tr').find("td:eq(3)").html() + '</td>';
        content += '<td><input class="form-control input-sm number" type="text" id="kuantitasalkes" value="1" autocomplete="off"/></td>';
        content += '<td><input class="form-control input-sm number" type="text" id="totalkeseluruhanalkes" value="' + $(this).closest('tr').find("td:eq(3)").html() + '" autocomplete="off" readonly></td>';
        content += '<td style="display: none" id="idalkes">' + $(this).data("idalkes") + '</td>';
        content += '</tr>';

        $("table#alkes-table-step-2 tbody").html(content);

        $(".number").number(true, 0, '.', ',');
        $(".number-diskon").number(true, 2, '.', ',');
        $("#kuantitasalkes").focus();
    });

    $(document).on("keyup", "tr#alkes-input td input", function() {
        var totaltarif = $("#hargajualalkes").text().replace(/,/g, '');
        var kuantitas = $("#kuantitasalkes").val().replace(/,/g, '');

        totalTarif = (totaltarif * kuantitas);
        $("#totalkeseluruhanalkes").val(totalTarif);
    });

    $(document).on("click", "#alkes-add", function() {
        var valid = validateDetailAlkes();
        if (!valid) return false;

        var rowindex;
        var duplicate = false;

        if ($("#rowindexalkes").val() != '') {
            var content = "";
            rowindex = $("#rowindexalkes").val();
        } else {
            var content = "<tr>";
            $('#alkes-list tbody tr').filter(function() {
                var $cells = $(this).children('td');
                if ($cells.eq(0).text() === $("#kodealkes").text()) {
                    sweetAlert("Maaf...", "alkes " + $("#namaalkes").text() + " sudah ditambahkan.", "error");
                    duplicate = true;
                    return false;
                }
            });
        }

        if (duplicate == false) {
            content += "<td style='display: none'>" + $("#kodealkes").text(); + "</td>";
            content += "<td width='10%'>" + $("#namaalkes").text(); + "</td>";
            content += "<td width='10%'>" + $("#satuanalkes").text(); + "</td>";
            content += "<td style='display: none'>" + $("#hargadasaralkes").text(); + "</td>";
            content += "<td style='display: none'>" + $("#marginalkes").text(); + "</td>";
            content += "<td width='10%'>" + $("#hargajualalkes").text(); + "</td>";
            content += "<td width='10%'>" + $.number($("#kuantitasalkes").val(), 0, '.', ','); + "</td>";
            content += "<td width='10%'>" + $.number($("#totalkeseluruhanalkes").val(), 0, '.', ','); + "</td>";
            content += "<td style='display: none'>" + $("#idalkes").text(); + "</td>";
            content += "<td style='display: none'>" + $("#idunitpelayananalkes option:selected").val(); + "</td>";
            content += "<td width='10%'>";
            content += "<div class='btn btn-group'>";
            content += "<a href='#' class='btn btn-primary btn-sm alkes-edit' data-toggle='modal' data-target='#alkes-modal' id='alkes-modal-show' data-toggle='tooltip' title='Ubah'><i class='fa fa-pencil'></i></a>";
            content += "<a href='#' class='btn btn-danger btn-sm alkes-delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
            content += "</div>";
            content += "</td>";

            if ($("#rowindexalkes").val() != '') {
                $('#alkes-list tbody tr:eq(' + rowindex + ')').html(content);
            } else {
                content += "</tr>";
                $('#alkes-list tbody').append(content);
            }

            $("#alkes-back").click();
            detailClear();
        }
    });

    $(document).on("click", ".alkes-edit", function() {
        $('#alkes-step-1').fadeOut();
        $('#alkes-step-2').fadeIn('slow');

        $("#rowindexalkes").val($(this).closest('tr')[0].sectionRowIndex);

        $("#kodealkes").text($(this).closest('tr').find("td:eq(0)").html());
        $("#namaalkes").text($(this).closest('tr').find("td:eq(1)").html());
        $("#satuanalkes").text($(this).closest('tr').find("td:eq(2)").html());
        $("#hargadasaralkes").text($(this).closest('tr').find("td:eq(3)").html());
        $("#marginalkes").text($(this).closest('tr').find("td:eq(4)").html());
        $("#hargajualalkes").text($(this).closest('tr').find("td:eq(5)").html());
        $("#kuantitasalkes").val($(this).closest('tr').find("td:eq(6)").html());
        $("#totalkeseluruhanalkes").val($(this).closest('tr').find("td:eq(7)").html());
        $("#idalkes").text($(this).closest('tr').find("td:eq(8)").html());
        $("#idunitpelayananalkes").val($(this).closest('tr').find("td:eq(9)").html());

        return false;
    });

    $(document).on("click", ".alkes-delete", function() {
        var row = $(this).closest('td').parent();
        swal({
            title: '',
            text: "Apakah anda yakin akan menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false,
            closeOnCancel: true
        }).then(function() {
            return row.remove();
        })
        return false;
    });

});

function detailClear() {
    $("#rowindex").val('');
    $("#rowindexobat").val('');
    $("#rowindexalkes").val('');
    $(".detail").val('');
}

function validateDetailTindakan() {
    var kuantitas = $("#kuantitas").val();
    if (kuantitas == "" || kuantitas == "0") {
        sweetAlert("Maaf...", "Kuantitas tidak boleh kosong!", "error");
        return false;
    }

    return true;
}

function validateDetailObat() {
    var kuantitas = $("#kuantitasobat").val();
    if (kuantitas == "" || kuantitas == "0") {
        sweetAlert("Maaf...", "Kuantitas tidak boleh kosong!", "error");
        return false;
    }

    return true;
}

function validateDetailAlkes() {
    var kuantitas = $("#kuantitasalkes").val();
    if (kuantitas == "" || kuantitas == "0") {
        sweetAlert("Maaf...", "Kuantitas tidak boleh kosong!", "error");
        return false;
    }

    return true;
}

</script>
