<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('330'))){ ?>

<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <hr style="margin-top:0px">
        <div class="row">
            <?php echo form_open('tpoliklinik_tindakan/filter','class="form-horizontal" id="form-work"') ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="nomedrec">No. Medrec</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="nomedrec" value="{nomedrec}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="namapasien">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="namapasien" value="{namapasien}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="idkelompokpasien">Kelompok Pasien</label>
                    <div class="col-md-8">
                        <select id="idkelompokpasien" name="idkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
                            <option value="#" selected>Semua Kelompok Pasien</option>
                            <?php foreach  ($list_kelompok_pasien as $row) { ?>
                            <option value="<?=$row->id?>" <?=($idkelompokpasien == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group" id="formPerusahaan" style="margin-bottom: 5px;" <?= ($idkelompokpasien != 1 ? 'hidden':'') ?>>
                    <label class="col-md-4 control-label" for="idrekanan">Perusahaan</label>
                    <div class="col-md-8">
                        <select name="idrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
                            <option value="#" selected>Semua Perusahaan</option>
                            <?php foreach (get_all('mrekanan', array('status' => 1)) as $row){?>
                            <option value="<?= $row->id?>" <?=($idrekanan == $row->id ? 'selected':'')?>><?= $row->nama?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="iddokter">Dokter</label>
                    <div class="col-md-8">
                        <select name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
                            <option value="#" selected>Semua Dokter</option>
                            <?php foreach  ($list_dokter as $row) { ?>
                            <option value="<?=$row->id?>" <?=($iddokter == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="idtipe">Tipe</label>
                    <div class="col-md-8">
                        <select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
                            <?php if (UserAccesForm($user_acces_form,array('331'))){ ?>
                            <option value="#" <?=($idtipe==''?'selected':'')?>>Semua Tipe</option>
                            <?}?>
                            <?php if (UserAccesForm($user_acces_form,array('332'))){ ?>
                            <option value="1" <?=($idtipe == '1' ? 'selected' : '')?>>Poliklinik</option>
                            <?}?>
                            <?php if (UserAccesForm($user_acces_form,array('1332'))){ ?>
                            <option value="2" <?=($idtipe == '2' ? 'selected' : '')?>>Instalasi Gawat Darurat</option>
                            <?}?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="idpoliklinik">Poliklinik</label>
                    <div class="col-md-8">
                        <select name="idpoliklinik" id="idpoliklinik" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
                            <option value="#">Semua Poliklinik</option>
                            <?php foreach (get_all('mpoliklinik', array('status' => 1)) as $row){?>
                            <option value="<?= $row->id?>" <?=($idpoliklinik == $row->id ? 'selected':'')?>><?= $row->nama?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Status</label>
                    <div class="col-md-8">
                        <select name="status" id="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
                            <option value="9" <?=($status == 9 ? 'selected':'')?>>Semua Status</option>
                            <option value="0" <?=($status == 0 ? 'selected':'')?>>Belum ditindak</option>
                            <option value="1" <?=($status == 1 ? 'selected':'')?>>Sudah ditindak</option>
                            <option value="2" <?=($status == 2 ? 'selected':'')?>>Tindakan dibatalkan</option>
                            <option value="3" <?=($status == 3 ? 'selected':'')?>>Telah Ditransaksikan</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggaldaftar">Tanggal Daftar</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" name="tanggaldari" placeholder="From" value="{tanggaldari}">
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
            <?php echo form_close() ?>
        </div>

        <hr style="margin-top:10px">
		<div class="table-responsive">
        <table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
            <thead>
                <tr>
                    <th>No</th>
                    <th>No. Pendaftaran</th>
                    <th>Tanggal</th>
                    <th>No. Antrian</th>
                    <th>No. Medrec</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Poliklinik</th>
                    <th>Dokter</th>
                    <th>Rujukan</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    </div>
</div>

<?}?>
<!-- MODAL Rencana -->
<div id="rencana-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Rencana</h3>
                </div>
                <div class="modal-body">
                    <table class="table" style="margin-bottom: 0px;">
                        <tr>
                            <td style="width:30%">Tipe Rencana</td>
                            <td style="width:70%">
                                <div class="col-sm-12">
                                    <select class="js-select2 form-control" id="tiperencana" style="width: 100%;" data-placeholder="Pilih Opsi">
                                        <option value="">Pilih Opsi</option>
                                        <option value="1">Rawat Inap</option>
                                        <option value="2">One Day Surgery</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr class="form-ods" hidden>
                            <td>Tanggal Operasi</td>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" id="tanggaloperasi" class="js-datepicker form-control" placeholder="Tanggal" value="<?= date("Y-m-d") ?>" required="" aria-required="true">
                                </div>
                            </td>
                        </tr>
                        <tr class="form-ods" hidden>
                            <td>No. Medrec</td>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" id="nomedrec" class="form-control" value="" required="" aria-required="true" readonly>
                                </div>
                            </td>
                        </tr>
                        <tr class="form-ods" hidden>
                            <td>Nama Pasien</td>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" id="namapasienoperasi" class="form-control" value="" required="" aria-required="true" readonly>
                                </div>
                            </td>
                        </tr>
                        <tr class="form-ods" hidden>
                            <td>Tanggal Lahir</td>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" id="tanggallahir" class="form-control" value="" required="" aria-required="true" readonly>
                                </div>
                            </td>
                        </tr>
                        <tr class="form-ods" hidden>
                            <td>Umur</td>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" id="umur" class="form-control" value="" required="" aria-required="true" readonly>
                                </div>
                            </td>
                        </tr>
                        <tr class="form-ods" hidden>
                            <td>Jenis Kelamin</td>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" id="jeniskelamin" class="form-control" value="" required="" aria-required="true" readonly>
                                </div>
                            </td>
                        </tr>
                        <tr class="form-ods" hidden>
                            <th>Diagnosa</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" id="diagnosaoperasi" class="form-control" required="" aria-required="true">
                                </div>
                            </td>
                        </tr>
                        <tr class="form-ods" hidden>
                            <td>Operasi</td>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" id="operasi" class="form-control" placeholder="" value="" required="" aria-required="true">
                                </div>
                            </td>
                        </tr>
                        <tr class="form-ods" hidden>
                            <th>Jam Operasi</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" id="jamoperasi" class="form-control" placeholder="" value="" required="" aria-required="true">
                                </div>
                            </td>
                        </tr>
                        <tr class="form-ods" hidden>
                            <td>Nama Petugas</td>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" id="petugasoperasi" class="form-control" placeholder="" value="" required="" aria-required="true">
                                </div>
                            </td>
                        </tr>
                        <tr class="form-ods" hidden>
                            <td>Catatan</td>
                            <td>
                                <div class="col-sm-12">
                                    <textarea id="catatanoperasi" class="form-control" required></textarea>
                                </div>
                            </td>
                        </tr>
                        <input type="hidden" id="idpendaftaran">
                    </table>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <div class="buton">
                    <button type="button" class="btn btn-primary simpan-rencana" data-dismiss="modal">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
jQuery(function() {
    BaseTableDatatables.init();
    $('#datatable-simrs').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}tpoliklinik_tindakan/getIndex/' + '<?=$this->uri->segment(2)?>',
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 2,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 3,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 4,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 5,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 6,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 7,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 8,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 9,
                "orderable": true
            }
        ]
    });
});

$(document).ready(function() {
    $("#jamoperasi").datetimepicker({
        format: "HH:mm",
        stepping: 30
    });

    $('#tanggaloperasi').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
    });
});

$(document).on('change', '#tiperencana', function() {
    $('.form-ods').show();
});

$(document).on('change', '#idkelompokpasien', function() {
    if ($(this).val() == '1') {
        $('#formPerusahaan').show();
    } else {
        $('#formPerusahaan').hide();
    }
});

$("#idtipe").change(function() {
    var idtipe = $("#idtipe").val();

    $.ajax({
        url: "{site_url}tpoliklinik_pendaftaran/getPoli",
        method: "POST",
        dataType: "json",
        data: {
            "tipepoli": idtipe
        },
        success: function(data) {
            $("#idpoliklinik").empty();
            $("#idpoliklinik").append("<option value='#'>Semua Poliklinik</option>");
            for (var i = 0; i < data.length; i++) {
                $("#idpoliklinik").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
            }
            $("#idpoliklinik").selectpicker('refresh');
        }
    });
});

$(document).on('click', '.getDataRencana', function() {
    $("#nomedrec").val($(this).closest('tr').find("td:eq(4)").html());
    $("#namapasienoperasi").val($(this).closest('tr').find("td:eq(5)").html());
    $("#tanggallahir").val($(this).data('tanggallahir'));
    $("#umur").val($(this).data('umur'));
    $("#jeniskelamin").val($(this).data('jeniskelamin'));
    $("#idpendaftaran").val($(this).closest('tr').find("td:eq(1) span").data('idpendaftaran'));
});

$(document).on('click', '.simpan-rencana', function() {
    var tiperencana = $('#tiperencana option:selected').val();
    var idpendaftaran = $('#idpendaftaran').val();
    var tanggaloperasi = $('#tanggaloperasi').val();
    var namapasienoperasi = $('#namapasienoperasi').val();
    var diagnosaoperasi = $('#diagnosaoperasi').val();
    var operasi = $('#operasi').val();
    var jamoperasi = $('#jamoperasi').val();
    var petugasoperasi = $('#petugasoperasi').val();
    var catatanoperasi = $('#catatanoperasi').val();
    var iddokterdpjp = $('#iddokterdpjp').val();

    $.ajax({
        url: '{site_url}tpoliklinik_tindakan/daftarOperasi',
        method: 'POST',
        data: {
            idpendaftaran: idpendaftaran,
            iddokterdpjp: iddokterdpjp,
            tiperencana: tiperencana,
            tanggaloperasi: tanggaloperasi,
            namapasienoperasi: namapasienoperasi,
            diagnosaoperasi: diagnosaoperasi,
            operasi: operasi,
            jamoperasi: jamoperasi,
            petugasoperasi: petugasoperasi,
            catatanoperasi: catatanoperasi
        },
        success: function(data) {
            location.reload();
        }
    });
});
</script>
