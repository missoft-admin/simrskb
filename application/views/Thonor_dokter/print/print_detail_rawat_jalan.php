<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?= $title; ?></title>
    <style>
      body{
          font-family: "Courier", Arial,sans-serif;
      }
      @media print {
         table {
           font-size: 13px !important;
           border-collapse: collapse !important;
           width: 100% !important;
         }
         td {
           padding: 5px;
         }
         .content td {
           padding: 0px;
         }
         .border-full {
           border: 1px solid #000 !important;
         }
         .border-bottom {
           border-bottom:1px solid #000 !important;
         }
         .border-right {
           border-right:1px solid #000 !important;
         }
         .border-left {
           border-left:1px solid #000 !important;
         }
         .border-top {
           border-top:1px solid #000 !important;
         }
         .border-thick-top{
           border-top:2px solid #000 !important;
         }
         .border-thick-bottom{
           border-bottom:2px solid #000 !important;
         }
         .text-center{
           text-align: center !important;
         }
         .text-right{
           text-align: right !important;
         }
         .text-semi-bold {
           font-size: 15px !important;
           font-weight: 700 !important;
         }
         .text-bold {
           font-size: 15px !important;
           font-weight: 700 !important;
         }
         .text-footer {
           font-size: 12px !important;
         }
         .text-address {
           color: #3abad8 !important;
         }
      }

      @media screen {
         table {
           font-size: 13px !important;
           border-collapse: collapse !important;
           width: 100% !important;
         }
         td {
           padding: 5px;
         }
         .content td {
           padding: 0px;
         }
         .border-full {
           border: 1px solid #000 !important;
         }
         .border-bottom {
           border-bottom:1px solid #000 !important;
         }
         .border-right {
           border-right:1px solid #000 !important;
         }
         .border-left {
           border-left:1px solid #000 !important;
         }
         .border-top {
           border-top:1px solid #000 !important;
         }
         .border-thick-top{
           border-top:2px solid #000 !important;
         }
         .border-thick-bottom{
           border-bottom:2px solid #000 !important;
         }
         .text-center{
           text-align: center !important;
         }
         .text-right{
           text-align: right !important;
         }
         .text-semi-bold {
           font-size: 15px !important;
           font-weight: 700 !important;
         }
         .text-bold {
           font-size: 15px !important;
           font-weight: 700 !important;
         }
         .text-footer {
           font-size: 12px !important;
         }
         .text-address {
           color: #3abad8 !important;
         }
      }
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <table>
      <tr>
        <td style="width:20%" rowspan="2" class="text-center">
          <img style="width:125px;" src="<?=base_url()?>assets/upload/logo/logo.png">
        </td>
          <td class="text-center">
            <b style="font-size:22px !important; margin-left: -200px;"><?= $titleReport; ?></b>
          </td>
      </tr>
      <tr>
        <td class="text-center">
          <b style="font-size:18px !important; margin-left: -200px;"><?= $namaDokter; ?></b>
          <br>
          <b style="font-size:18px !important; margin-left: -200px;">PERIODE : <?= $tanggalPembayaran; ?></b>
        </td>
      </tr>
    </table>

    <table>
      <tr>
        <td colspan="9"></td>
      </tr>
      <tr>
          <td class="border-full text-bold text-center" width="5%">NO</td>
          <td class="border-full text-bold text-center" width="10%">TANGGAL PEMERIKSAAN</td>
          <td class="border-full text-bold text-center" width="10%">NO RM & NAMA</td>
          <td class="border-full text-bold text-center" width="10%">KELOMPOK PASIEN</td>
          <td class="border-full text-bold text-center" width="10%">REKANAN</td>
          <td class="border-full text-bold text-center" width="10%">NAMA PEMERIKSAAN</td>
          <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
          <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
          <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
          <td class="border-full text-bold text-center" width="10%">JUMLAH NETTO</td>
      </tr>
      <?php $total_jasa_medis = 0; ?>
      <?php $total_nominal_potongan_rs = 0; ?>
      <?php $total_nominal_pajak_dokter = 0; ?>
      <?php $total_jasamedis_netto = 0; ?>
      <?php $data = $this->model->getPrintOutRawatJalan($id); ?>
      <?php if ($data) { ?>
        <?php foreach ($data as $index => $row) { ?>
        <tr>
            <td class="border-full text-center"><?=$index+1?></td>
            <td class="border-full text-center"><?=$row->tanggal_pemeriksaan?></td>
            <td class="border-full text-center"><?=$row->nomedrec?> - <?=$row->namapasien?></td>
            <td class="border-full text-center"><?=$row->namakelompok?></td>
            <td class="border-full text-center"><?=($row->namarekanan ? $row->namarekanan : '-')?></td>
            <td class="border-full text-center"><?=$row->namapemeriksaan?></td>
            <td class="border-full text-right"><?=number_format($row->jasamedis)?></td>
            <td class="border-full text-right"><?=number_format($row->nominal_potongan_rs)?><br>(<?=number_format($row->potongan_rs, 2)?>%)</td>
            <td class="border-full text-right"><?=number_format($row->nominal_pajak_dokter)?><br>(<?=number_format($row->pajak_dokter, 2)?>%)</td>
            <td class="border-full text-right"><?=number_format($row->jasamedis_netto)?></td>
        </tr>
        <?php $total_jasa_medis = $total_jasa_medis + $row->jasamedis; ?>
        <?php $total_nominal_potongan_rs = $total_nominal_potongan_rs + $row->nominal_potongan_rs; ?>
        <?php $total_nominal_pajak_dokter = $total_nominal_pajak_dokter + $row->nominal_pajak_dokter; ?>
        <?php $total_jasamedis_netto = $total_jasamedis_netto + $row->jasamedis_netto; ?>
        <?php } ?>
      <?php } else { ?>
        <tr>
            <td class="border-full text-center" colspan="10">-</td>
        </tr>
      <?php } ?>
      <tr>
        <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
        <td class="border-full text-semi-bold text-right"><?=number_format($total_jasa_medis)?></td>
        <td class="border-full text-semi-bold text-right"><?=number_format($total_nominal_potongan_rs)?></td>
        <td class="border-full text-semi-bold text-right"><?=number_format($total_nominal_pajak_dokter)?></td>
        <td class="border-full text-semi-bold text-right"><?=number_format($total_jasamedis_netto)?></td>
      </tr>
    </table>

    <br>
    <br>

    <table>
      <tr>
        <td class="text-left"></td>
        <td></td>
      </tr>
      <tr>
        <td class="text-left text-footer text-address">
          Jl. L.L.RE. Martadinata No.28 Bandung 40115 Jawa Barat, Indonesia<br>
          T. (022) 4206717 F. (022) 4216436 E. rskb.halmahera@gmail.com ; www.halmaherasiaga.com
        </td>
        <td class="text-right text-footer">
          Cetakan Ke-1 | UC : <?= $userPrint; ?> | <?= $datePrint; ?>
        </td>
      </tr>
    </table>
  </body>
</html>
