<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Saat Ini',
    'jenis_pasien' => 'Pasien Pribadi',
    'idkelompok' => array(5),
    'status_hold' => $statusHold,
  );
?>
<?php $dataPasienUmumPribadi = $this->model->getPendapatanRawatJalan($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Saat Ini',
    'jenis_pasien' => 'Pasien RS',
    'idkelompok' => array(5),
    'status_hold' => $statusHold,
  );
?>
<?php $dataPasienUmumRS = $this->model->getPendapatanRawatJalan($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Saat Ini',
    'jenis_pasien' => 'Pasien Pribadi',
    'idkelompok' => array(1, 2, 3, 4),
    'status_hold' => $statusHold,
  );
?>
<?php $dataPasienAsuransiPribadi = $this->model->getPendapatanRawatJalan($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Saat Ini',
    'jenis_pasien' => 'Pasien RS',
    'idkelompok' => array(1, 2, 3, 4),
    'status_hold' => $statusHold,
  );
?>
<?php $dataPasienAsuransiRS = $this->model->getPendapatanRawatJalan($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Sebelumnya',
    'jenis_pasien' => 'Pasien Pribadi',
    'idkelompok' => array(1, 2, 3, 4, 5),
    'status_hold' => $statusHold,
  );
?>
<?php $dataPeriodeSebelumnyaPribadi = $this->model->getPendapatanRawatJalan($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Sebelumnya',
    'jenis_pasien' => 'Pasien RS',
    'idkelompok' => array(1, 2, 3, 4, 5),
    'status_hold' => $statusHold,
  );
?>
<?php $dataPeriodeSebelumnyaRS = $this->model->getPendapatanRawatJalan($params); ?>

<!-- Header -->
<?php if($dataPasienUmumPribadi || $dataPasienUmumRS || $dataPasienAsuransiPribadi || $dataPasienAsuransiRS || $dataPeriodeSebelumnyaPribadi || $dataPeriodeSebelumnyaRS) { ?>
<tr>
  <td colspan="12" style="padding-top: 15px; padding-bottom: 15px;" class="text-center">
    <b class="text-bold" style="font-size:18px !important;">PENDAPATAN RAWAT JALAN</b>
  </td>
</tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataPasienUmumPribadi) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PASIEN UMUM (KATEGORI PASIEN PRIBADI)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">KELOMPOK PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataPasienUmumPribadi as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="2"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full text-center" colspan="3"><?=$row->namakelompok?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
        <td class="border-full text-right" width="10%" colspan="3"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataPasienUmumRS) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PASIEN UMUM (KATEGORI PASIEN RUMAH SAKIT)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">KELOMPOK PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataPasienUmumRS as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="2"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full text-center" colspan="3"><?=$row->namakelompok?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
        <td class="border-full text-right" width="10%" colspan="3"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataPasienAsuransiPribadi) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PASIEN ASURANSI (KATEGORI PASIEN PRIBADI)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">KELOMPOK PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataPasienAsuransiPribadi as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="2"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full text-center" colspan="3"><?=$row->namakelompok?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
        <td class="border-full text-right" width="10%" colspan="3"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataPasienAsuransiRS) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PASIEN ASURANSI (KATEGORI PASIEN RUMAH SAKIT)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">KELOMPOK PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataPasienAsuransiRS as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="2"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full text-center" colspan="3"><?=$row->namakelompok?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
        <td class="border-full text-right" width="10%" colspan="3"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataPeriodeSebelumnyaPribadi) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PASIEN PERIODE SEBELUMNYA (KATEGORI PASIEN PRIBADI)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">KELOMPOK PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataPeriodeSebelumnyaPribadi as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="2"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full text-center" colspan="3"><?=$row->namakelompok?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
        <td class="border-full text-right" width="10%" colspan="3"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataPeriodeSebelumnyaRS) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PASIEN PERIODE SEBELUMNYA  (KATEGORI PASIEN RUMAH SAKIT)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">KELOMPOK PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataPeriodeSebelumnyaRS as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="2"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full text-center" colspan="3"><?=$row->namakelompok?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
        <td class="border-full text-right" width="10%" colspan="3"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>
