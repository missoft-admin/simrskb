<?php
  $params = array(
    'idhonor' => $id,
    'jenis_section' => 'Operasi',
    'status_hold' => $statusHold,
  );
?>
<?php $dataPendapatanOperatif = $this->model->getPendapatanRujukanRawatInap($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'jenis_section' => 'Konservatif',
    'status_hold' => $statusHold,
  );
?>
<?php $dataPendapatanKonservatif = $this->model->getPendapatanRujukanRawatInap($params); ?>

<!-- Header -->
<?php if ($dataPendapatanOperatif || $dataPendapatanKonservatif) { ?>
<tr>
  <td colspan="12" style="padding-top: 15px; padding-bottom: 15px;" class="text-center">
    <b class="text-bold" style="font-size:18px !important;">PENDAPATAN RUJUKAN RAWAT INAP</b>
  </td>
</tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataPendapatanOperatif) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN OPERATIF</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">NAMA PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">ASAL PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">POLIKLINIK</td>
    <td class="border-full text-bold text-center" width="10%">KELOMPOK PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataPendapatanOperatif as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full" colspan="2"><?=$row->namapasien?></td>
        <td class="border-full text-center"><?=$row->asalpasien?></td>
        <td class="border-full text-center"><?=$row->namapoliklinik?></td>
        <td class="border-full text-center"><?=$row->namakelompok?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?><br>(<?=number_format($row->potongan_rs, 2)?>%)</td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?><br>(<?=number_format($row->pajak_dokter, 2)?>%)</td>
        <td class="border-full text-right" colspan="2" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="7" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="2"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataPendapatanKonservatif) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN KONSERVATIF</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">NAMA PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">ASAL PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">POLIKLINIK</td>
    <td class="border-full text-bold text-center" width="10%">KELOMPOK PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataPendapatanKonservatif as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full" colspan="2"><?=$row->namapasien?></td>
        <td class="border-full text-center"><?=$row->asalpasien?></td>
        <td class="border-full text-center"><?=$row->namapoliklinik?></td>
        <td class="border-full text-center"><?=$row->namakelompok?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?><br>(<?=number_format($row->potongan_rs, 2)?>%)</td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?><br>(<?=number_format($row->pajak_dokter, 2)?>%)</td>
        <td class="border-full text-right" colspan="2" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="7" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="2"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>