<?php
  $params = array(
    'idhonor' => $id,
    'status_hold' => $statusHold,
  );
?>
<?php $dataPendapatanLainLain = $this->model->getPendapatanLainLain($params); ?>

<!-- Header -->
<?php if($dataPendapatanLainLain) { ?>
<tr>
  <td colspan="12" style="padding-top: 15px; padding-bottom: 15px;" class="text-center">
    <b class="text-bold" style="font-size:18px !important;">PENDAPATAN LAIN LAIN</b>
  </td>
</tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataPendapatanLainLain) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN LAIN LAIN</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" width="10%">TANGGAL</td>
    <td class="border-full text-bold text-center" colspan="4" width="10%">DESKRIPSI PENDAPATAN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
<?php foreach ($dataPendapatanLainLain as $index => $row) { ?>
  <tr>
      <td class="border-full text-center"><?=$index+1?></td>
      <td class="border-full text-center"><?=$row->tanggal_pemeriksaan?></td>
      <td class="border-full" colspan="4"><?=$row->namatarif?></td>
      <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
      <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
      <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
      <td class="border-full text-right" colspan="3" width="10%"><?=number_format($row->jasamedis_netto)?></td>
  </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
<?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>
