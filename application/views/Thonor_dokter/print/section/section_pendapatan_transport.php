<?php
  $params = array(
    'idhonor' => $id,
    'status_hold' => $statusHold,
  );
?>
<?php $dataPendapatanTransport = $this->model->getPendapatanTransport($params); ?>

<!-- Header -->
<?php if ($dataPendapatanTransport) { ?>
<tr>
  <td colspan="12" style="padding-top: 15px; padding-bottom: 15px;" class="text-center">
    <b class="text-bold" style="font-size:18px !important;">PENDAPATAN TRANSPORT</b>
  </td>
</tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataPendapatanTransport) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN TRANSPORT</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" width="10%">HARI KERJA</td>
    <td class="border-full text-bold text-center" width="10%">HARI LIBUR</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">TOTAL JAGA</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">TOTAL TRANSPORT</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataPendapatanTransport as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center"><?=number_format($row->hari_kerja)?></td>
        <td class="border-full text-center"><?=number_format($row->hari_libur)?></td>
        <td class="border-full text-center" colspan="2" width="10%"><?=number_format($row->total_jaga)?></td>
        <td class="border-full text-right" colspan="2" width="10%"><?=number_format($row->total_transport)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->total_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->total_pajak_dokter)?></td>
        <td class="border-full text-right" colspan="3" width="10%"><?=number_format($row->total_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->total_transport; ?>
  <?php $totalPotonganRs += $row->total_potongan_rs; ?>
  <?php $totalPajakDokter += $row->total_pajak_dokter; ?>
  <?php $totalNetto += $row->total_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="5" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right" colspan="2"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>
