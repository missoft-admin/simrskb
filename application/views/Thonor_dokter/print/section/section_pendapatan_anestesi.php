
<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Saat Ini',
    'jenis_pasien' => 'Pasien Pribadi',
    'status_hold' => $statusHold,
  );
?>
<?php $dataAnesthesiKategoriPribadi = $this->model->getPendapatanAnesthesi($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Saat Ini',
    'jenis_pasien' => 'Pasien RS',
    'status_hold' => $statusHold,
  );
?>
<?php $dataAnesthesiKategoriRS = $this->model->getPendapatanAnesthesi($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Sebelumnya',
    'jenis_pasien' => '',
    'status_hold' => $statusHold,
  );
?>
<?php $dataPeriodeSebelumnya = $this->model->getPendapatanAnesthesi($params); ?>

<!-- Header -->
<?php if ($dataAnesthesiKategoriPribadi || $dataAnesthesiKategoriRS || $dataPeriodeSebelumnya) { ?>
<tr>
  <td colspan="12" style="padding-top: 15px; padding-bottom: 15px;" class="text-center">
    <b class="text-bold" style="font-size:18px !important;">PENDAPATAN ANESTHESI</b>
  </td>
</tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataAnesthesiKategoriPribadi) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN ANESTHESI (KATEGORI PASIEN PRIBADI)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="1%">NO</td>
    <td class="border-full text-bold text-center" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" width="10%">NAMA PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">ASAL PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">POLIKLINIK</td>
    <td class="border-full text-bold text-center" width="10%">KELOMPOK PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">JENIS OPERASI & NAMA OPERASI</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataAnesthesiKategoriPribadi as $index => $row) { ?>
  <tr>
      <td class="border-full text-center"><?=$index+1?></td>
      <td class="border-full text-center"><?=$row->tanggal_pemeriksaan?></td>
      <td class="border-full"><?=$row->namapasien?></td>
      <td class="border-full text-center"><?=$row->asalpasien?></td>
      <td class="border-full text-center"><?=$row->namapoliklinik?></td>
      <td class="border-full text-center"><?=$row->namakelompok?></td>
      <td class="border-full"><?=$row->jenisoperasi?> & <?=$row->namaoperasi?></td>
      <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
      <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?><br>(<?=number_format($row->potongan_rs, 2)?>%)</td>
      <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?><br>(<?=number_format($row->pajak_dokter, 2)?>%)</td>
      <td class="border-full text-right" colspan="2" width="10%"><?=number_format($row->jasamedis_netto)?></td>
  </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="7" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="2"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataAnesthesiKategoriRS) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN ANESTHESI (KATEGORI PASIEN RUMAH SAKIT)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="1%">NO</td>
    <td class="border-full text-bold text-center" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" width="10%">NAMA PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">ASAL PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">POLIKLINIK</td>
    <td class="border-full text-bold text-center" width="10%">KELOMPOK PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">JENIS OPERASI & NAMA OPERASI</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataAnesthesiKategoriRS as $index => $row) { ?>
  <tr>
      <td class="border-full text-center" width="5%"><?=$index+1?></td>
      <td class="border-full text-center" width="10%"><?=$row->tanggal_pemeriksaan?></td>
      <td class="border-full" width="10%"><?=$row->namapasien?></td>
      <td class="border-full text-center" width="10%"><?=$row->asalpasien?></td>
      <td class="border-full text-center" width="10%"><?=$row->namapoliklinik?></td>
      <td class="border-full text-center" width="10%"><?=$row->namakelompok?></td>
      <td class="border-full" width="10%"><?=$row->jenisoperasi?> & <?=$row->namaoperasi?></td>
      <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
      <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?><br>(<?=number_format($row->potongan_rs, 2)?>%)</td>
      <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?><br>(<?=number_format($row->pajak_dokter, 2)?>%)</td>
      <td class="border-full text-right" colspan="2" width="10%"><?=number_format($row->jasamedis_netto)?></td>
  </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="7" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="2"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataPeriodeSebelumnya) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN ANESTHESI (KATEGORI PASIEN PERIODE SEBELUMNYA)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="1%">NO</td>
    <td class="border-full text-bold text-center" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" width="10%">NAMA PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">ASAL PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">POLIKLINIK</td>
    <td class="border-full text-bold text-center" width="10%">KELOMPOK PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">JENIS OPERASI & NAMA OPERASI</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataPeriodeSebelumnya as $index => $row) { ?>
  <tr>
      <td class="border-full text-center" width="5%"><?=$index+1?></td>
      <td class="border-full text-center" width="10%"><?=$row->tanggal_pemeriksaan?></td>
      <td class="border-full" width="10%"><?=$row->namapasien?></td>
      <td class="border-full text-center" width="10%"><?=$row->asalpasien?></td>
      <td class="border-full text-center" width="10%"><?=$row->namapoliklinik?></td>
      <td class="border-full text-center" width="10%"><?=$row->namakelompok?></td>
      <td class="border-full" width="10%"><?=$row->jenisoperasi?> & <?=$row->namaoperasi?></td>
      <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
      <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?><br>(<?=number_format($row->potongan_rs, 2)?>%)</td>
      <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?><br>(<?=number_format($row->pajak_dokter, 2)?>%)</td>
      <td class="border-full text-right" colspan="2" width="10%"><?=number_format($row->jasamedis_netto)?></td>
  </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="7" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="2"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>
