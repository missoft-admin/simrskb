<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Saat Ini',
    'jenis_pasien' => 'Pasien Pribadi',
    'section_operasi' => '1',
    'status_hold' => $statusHold,
  );
?>
<?php $dataPendapatanOperatifPribadi = $this->model->getPendapatanRawatInap($params); ?>

<?php
  $params = [
      'idhonor' => $id,
      'status_periode' => 'Periode Saat Ini',
      'jenis_pasien' => 'Pasien RS',
      'section_operasi' => '1',
      'status_hold' => $statusHold,
  ];
?>
<?php $dataPendapatanOperatifRS = $this->model->getPendapatanRawatInap($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Saat Ini',
    'jenis_pasien' => 'Pasien Pribadi',
    'section_operasi' => '0',
    'status_hold' => $statusHold,
  );
?>
<?php $dataPendapatanKonservatifPribadi = $this->model->getPendapatanRawatInap($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Saat Ini',
    'jenis_pasien' => 'Pasien RS',
    'section_operasi' => '0',
    'status_hold' => $statusHold,
  );
?>
<?php $dataPendapatanKonservatifRS = $this->model->getPendapatanRawatInap($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Sebelumnya',
    'jenis_pasien' => 'Pasien RS',
    'section_operasi' => '',
    'status_hold' => $statusHold,
  );
?>
<?php $dataPeriodeSebelumnyaRS = $this->model->getPendapatanRawatInap($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Sebelumnya',
    'jenis_pasien' => 'Pasien Pribadi',
    'section_operasi' => '',
    'status_hold' => $statusHold,
  );
?>
<?php $dataPeriodeSebelumnyaPribadi = $this->model->getPendapatanRawatInap($params); ?>

<!-- Header -->
<?php if ($dataPendapatanOperatifPribadi || $dataPendapatanOperatifRS || $dataPendapatanKonservatifPribadi || $dataPendapatanKonservatifRS || $dataPeriodeSebelumnyaRS || $dataPeriodeSebelumnyaPribadi) { ?>
<tr>
  <td colspan="12" style="" class="text-center">
    <b class="text-bold" style="font-size:18px !important;">PENDAPATAN RAWAT INAP</b>
  </td>
</tr>
<?php } ?>


<!-- Sub Header -->
<?php if ($dataPendapatanOperatifPribadi) { ?>
<tr>
  <td class="border-full" colspan="12" style="">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN OPERATIF (KATEGORI PASIEN PRIBADI)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">NAMA PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">ASAL PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">POLIKLINIK</td>
    <td class="border-full text-bold text-center" width="10%">KELOMPOK PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN PERUJUK</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" width="10%">JUMLAH NETTO</td>
</tr>
  <?php $totalJasaMedis = 0; ?>
  <?php $totalPotonganRs = 0; ?>
  <?php $totalPotonganPerujuk = 0; ?>
  <?php $totalPajakDokter = 0; ?>
  <?php $totalNetto = 0; ?>
  <?php foreach ($dataPendapatanOperatifPribadi as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full" colspan="2"><?=$row->namapasien?></td>
        <td class="border-full text-center"><?=$row->asalpasien?></td>
        <td class="border-full text-center"><?=$row->namapoliklinik?></td>
        <td class="border-full text-center"><?=$row->namakelompok?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?><br>(<?=number_format($row->potongan_rs, 2)?>%)</td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_perujuk)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?><br>(<?=number_format($row->pajak_dokter, 2)?>%)</td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPotonganPerujuk += $row->nominal_potongan_perujuk; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="7" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganPerujuk); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php if ($dataPendapatanOperatifRS) { ?>
<tr>
  <td class="border-full" colspan="12" style="">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN OPERATIF (KATEGORI PASIEN RUMAH SAKIT)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">NAMA PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">ASAL PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">POLIKLINIK</td>
    <td class="border-full text-bold text-center" width="10%">KELOMPOK PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN PERUJUK</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" width="10%">JUMLAH NETTO</td>
</tr>
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPotonganPerujuk = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
  <?php foreach ($dataPendapatanOperatifRS as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full" colspan="2"><?=$row->namapasien?></td>
        <td class="border-full text-center"><?=$row->asalpasien?></td>
        <td class="border-full text-center"><?=$row->namapoliklinik?></td>
        <td class="border-full text-center"><?=$row->namakelompok?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?><br>(<?=number_format($row->potongan_rs, 2)?>%)</td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_perujuk)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?><br>(<?=number_format($row->pajak_dokter, 2)?>%)</td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPotonganPerujuk += $row->nominal_potongan_perujuk; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
<tr>
  <td colspan="7" class="border-full text-center text-semi-bold">TOTAL</td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganPerujuk); ?></td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalNetto); ?></td>
</tr>
<?php } ?>


<!-- Sub Header -->
<?php if ($dataPendapatanKonservatifPribadi) { ?>
<tr>
  <td class="border-full" colspan="12" style="">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN KONSERVATIF (KATEGORI PASIEN PRIBADI)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">NAMA PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">ASAL PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">POLIKLINIK</td>
    <td class="border-full text-bold text-center" width="10%">KELOMPOK PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN PERUJUK</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" width="10%">JUMLAH NETTO</td>
</tr>
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPotonganPerujuk = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
  <?php foreach ($dataPendapatanKonservatifPribadi as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full" colspan="2"><?=$row->namapasien?></td>
        <td class="border-full text-center"><?=$row->asalpasien?></td>
        <td class="border-full text-center"><?=$row->namapoliklinik?></td>
        <td class="border-full text-center"><?=$row->namakelompok?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?><br>(<?=number_format($row->potongan_rs, 2)?>%)</td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_perujuk)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?><br>(<?=number_format($row->pajak_dokter, 2)?>%)</td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPotonganPerujuk += $row->nominal_potongan_perujuk; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
<tr>
  <td colspan="7" class="border-full text-center text-semi-bold">TOTAL</td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganPerujuk); ?></td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalNetto); ?></td>
</tr>
<?php } ?>


<!-- Sub Header -->
<?php if ($dataPendapatanKonservatifRS) { ?>
<tr>
  <td class="border-full" colspan="12" style="">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN KONSERVATIF (KATEGORI PASIEN RUMAH SAKIT)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">NAMA PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">ASAL PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">POLIKLINIK</td>
    <td class="border-full text-bold text-center" width="10%">KELOMPOK PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN PERUJUK</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" width="10%">JUMLAH NETTO</td>
</tr>
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPotonganPerujuk = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
  <?php foreach ($dataPendapatanKonservatifRS as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full" colspan="2"><?=$row->namapasien?></td>
        <td class="border-full text-center"><?=$row->asalpasien?></td>
        <td class="border-full text-center"><?=$row->namapoliklinik?></td>
        <td class="border-full text-center"><?=$row->namakelompok?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?><br>(<?=number_format($row->potongan_rs, 2)?>%)</td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_perujuk)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?><br>(<?=number_format($row->pajak_dokter, 2)?>%)</td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPotonganPerujuk += $row->nominal_potongan_perujuk; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
<tr>
  <td colspan="7" class="border-full text-center text-semi-bold">TOTAL</td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganPerujuk); ?></td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalNetto); ?></td>
</tr>
<?php } ?>


<!-- Sub Header -->
<?php if ($dataPeriodeSebelumnyaRS) { ?>
<tr>
  <td class="border-full" colspan="12" style="">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN PERIODE SEBELUMNYA (KATEGORI PASIEN RUMAH SAKIT)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">NAMA PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">ASAL PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">POLIKLINIK</td>
    <td class="border-full text-bold text-center" width="10%">KELOMPOK PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN PERUJUK</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" width="10%">JUMLAH NETTO</td>
</tr>
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPotonganPerujuk = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
  <?php foreach ($dataPeriodeSebelumnyaRS as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full" colspan="2"><?=$row->namapasien?></td>
        <td class="border-full text-center"><?=$row->asalpasien?></td>
        <td class="border-full text-center"><?=$row->namapoliklinik?></td>
        <td class="border-full text-center"><?=$row->namakelompok?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?><br>(<?=number_format($row->potongan_rs, 2)?>%)</td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_perujuk)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?><br>(<?=number_format($row->pajak_dokter, 2)?>%)</td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPotonganPerujuk += $row->nominal_potongan_perujuk; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="7" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganPerujuk); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php if ($dataPeriodeSebelumnyaPribadi) { ?>
<tr>
  <td class="border-full" colspan="12" style="">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN PERIODE SEBELUMNYA (KATEGORI PASIEN PRIBADI)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">NAMA PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">ASAL PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">POLIKLINIK</td>
    <td class="border-full text-bold text-center" width="10%">KELOMPOK PASIEN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN PERUJUK</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" width="10%">JUMLAH NETTO</td>
</tr>
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPotonganPerujuk = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
  <?php foreach ($dataPeriodeSebelumnyaPribadi as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full" colspan="2"><?=$row->namapasien?></td>
        <td class="border-full text-center"><?=$row->asalpasien?></td>
        <td class="border-full text-center"><?=$row->namapoliklinik?></td>
        <td class="border-full text-center"><?=$row->namakelompok?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?><br>(<?=number_format($row->potongan_rs, 2)?>%)</td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_perujuk)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?><br>(<?=number_format($row->pajak_dokter, 2)?>%)</td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPotonganPerujuk += $row->nominal_potongan_perujuk; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="7" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganPerujuk); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>
