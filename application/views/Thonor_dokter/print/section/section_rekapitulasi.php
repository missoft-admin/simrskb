<!-- Header -->
<tr>
    <td colspan="12" style="padding-top: 15px; padding-bottom: 15px;" class="text-center">
        <b class="text-bold" style="font-size:18px !important;">REKAPITULASI</b>
    </td>
</tr>
<!-- Sub Header -->
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="8" width="10%">DESKRIPSI</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">NOMINAL POTONGAN</td>
</tr>
<tr>
    <td class="border-full text-center">1</td>
    <td class="border-full text-center" colspan="8">TOTAL PENDAPATAN BRUTTO</td>
    <td class="border-full text-right" colspan="3" width="10%"><?= $total_pendapatan_brutto; ?></td>
</tr>
<tr>
    <td class="border-full text-center">2</td>
    <td class="border-full text-center" colspan="8">TOTAL POTONGAN RUMAH SAKIT</td>
    <td class="border-full text-right" colspan="3" width="10%"><?= $total_potongan_rs; ?></td>
</tr>
<tr>
    <td class="border-full text-center">3</td>
    <td class="border-full text-center" colspan="8">BEBAN JASA DOKTER</td>
    <td class="border-full text-right" colspan="3" width="10%"><?= $total_beban_jasa_dokter; ?></td>
</tr>
<tr>
    <td class="border-full text-center">4</td>
    <td class="border-full text-center" colspan="8">TOTAL PAJAK</td>
    <td class="border-full text-right" colspan="3" width="10%"><?= $total_pajak_dokter; ?></td>
</tr>
<tr>
    <td class="border-full text-center">5</td>
    <td class="border-full text-center" colspan="8">TOTAL POTONGAN PRIBADI</td>
    <td class="border-full text-right" colspan="3" width="10%"><?= $total_potongan_pribadi; ?></td>
</tr>
<tr>
    <td class="border-full text-center">5</td>
    <td class="border-full text-center" colspan="8">TOTAL POTONGAN PERUJUK</td>
    <td class="border-full text-right" colspan="3" width="10%"><?= $total_potongan_perujuk; ?></td>
</tr>
<tr>
    <td class="border-full text-center">6</td>
    <td class="border-full text-center" colspan="8">TOTAL PENDAPATAN NETTO</td>
    <td class="border-full text-right" colspan="3" width="10%"><?= $total_pendapatan_netto; ?></td>
</tr>
<tr>
    <td class="border-full text-center" colspan="12" width="10%"><b><?= $total_pendapatan_netto_terbilang; ?></b></td>
</tr>
