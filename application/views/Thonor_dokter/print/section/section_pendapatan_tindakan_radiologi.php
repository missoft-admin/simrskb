<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Saat Ini',
    'idtipe_radiologi' => '1',
    'status_expertise' => '1',
    'status_hold' => $statusHold,
  );
?>
<?php $dataXRAYLangsung = $this->model->getPendapatanRadiologi($params); ?>

<?php
  $params = [
      'idhonor' => $id,
      'status_periode' => 'Periode Saat Ini',
      'idtipe_radiologi' => '1',
      'status_expertise' => '2',
      'status_hold' => $statusHold,
  ];
?>
<?php $dataXRAYMenolak = $this->model->getPendapatanRadiologi($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Saat Ini',
    'idtipe_radiologi' => '1',
    'status_expertise' => '3',
    'status_hold' => $statusHold,
  );
?>
<?php $dataXRAYKembali = $this->model->getPendapatanRadiologi($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Sebelumnya',
    'idtipe_radiologi' => '',
    'status_expertise' => '',
    'status_hold' => $statusHold,
  );
?>
<?php $dataXRAYSebelumnya = $this->model->getPendapatanRadiologi($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Saat Ini',
    'idtipe_radiologi' => '2',
    'status_expertise' => '',
    'status_hold' => $statusHold,
  );
?>
<?php $dataUSGSaatIni = $this->model->getPendapatanRadiologi($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Sebelumnya',
    'idtipe_radiologi' => '2',
    'status_expertise' => '',
    'status_hold' => $statusHold,
  );
?>
<?php $dataUSGSebelumnya = $this->model->getPendapatanRadiologi($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Saat Ini',
    'idtipe_radiologi' => '3',
    'status_expertise' => '',
    'status_hold' => $statusHold,
  );
?>
<?php $dataCTSCANSaatIni = $this->model->getPendapatanRadiologi($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Sebelumnya',
    'idtipe_radiologi' => '3',
    'status_expertise' => '',
    'status_hold' => $statusHold,
  );
?>
<?php $dataCTSCANSebelumnya = $this->model->getPendapatanRadiologi($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Saat Ini',
    'idtipe_radiologi' => '4',
    'status_expertise' => '',
    'status_hold' => $statusHold,
  );
?>
<?php $dataMRISaatIni = $this->model->getPendapatanRadiologi($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Sebelumnya',
    'idtipe_radiologi' => '4',
    'status_expertise' => '',
    'status_hold' => $statusHold,
  );
?>
<?php $dataMRISebelumnya = $this->model->getPendapatanRadiologi($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Saat Ini',
    'idtipe_radiologi' => '5',
    'status_expertise' => '',
    'status_hold' => $statusHold,
  );
?>
<?php $dataBMDSaatIni = $this->model->getPendapatanRadiologi($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_periode' => 'Periode Sebelumnya',
    'idtipe_radiologi' => '5',
    'status_expertise' => '',
    'status_hold' => $statusHold,
  );
?>
<?php $dataBMDSebelumnya = $this->model->getPendapatanRadiologi($params); ?>

<!-- Header -->
<?php if ($dataXRAYLangsung || $dataXRAYMenolak || $dataXRAYKembali || $dataXRAYSebelumnya || $dataUSGSaatIni || $dataUSGSebelumnya || $dataCTSCANSaatIni || $dataCTSCANSebelumnya || $dataMRISaatIni || $dataMRISebelumnya || $dataBMDSaatIni || $dataBMDSebelumnya) { ?>
<tr>
  <td colspan="12" style="padding-top: 15px; padding-bottom: 15px;" class="text-center">
    <b class="text-bold" style="font-size:18px !important;">PENDAPATAN RADIOLOGI</b>
  </td>
</tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataXRAYLangsung) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN X-RAY (LANGSUNG)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">JUMLAH PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataXRAYLangsung as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="3"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full text-center" colspan="2"><?=number_format($row->jumlah_pemeriksaan)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
        <td class="border-full text-right" colspan="3" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataXRAYMenolak) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN X-RAY (MENOLAK)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">JUMLAH PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataXRAYMenolak as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="3"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full text-center" colspan="2"><?=number_format($row->jumlah_pemeriksaan)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
        <td class="border-full text-right" colspan="3" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataXRAYKembali) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN X-RAY (KEMBALI)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">JUMLAH PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataXRAYKembali as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="3"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full text-center" colspan="2"><?=number_format($row->jumlah_pemeriksaan)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
        <td class="border-full text-right" colspan="3" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataXRAYSebelumnya) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN X-RAY (PERIODE SEBELUMNYA)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">JUMLAH PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataXRAYSebelumnya as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="3"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full text-center" colspan="2"><?=number_format($row->jumlah_pemeriksaan)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
        <td class="border-full text-right" colspan="3" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataUSGSaatIni) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN USG (PERIODE SAAT INI)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">JUMLAH PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataUSGSaatIni as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="3"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full text-center" colspan="2"><?=number_format($row->jumlah_pemeriksaan)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
        <td class="border-full text-right" colspan="3" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataUSGSebelumnya) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN USG (PERIODE SEBELUMNYA)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">JUMLAH PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataUSGSebelumnya as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="3"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full text-center" colspan="2"><?=number_format($row->jumlah_pemeriksaan)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
        <td class="border-full text-right" colspan="3" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataCTSCANSaatIni) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN CT-SCAN (PERIODE SAAT INI)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">JUMLAH PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataCTSCANSaatIni as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="3"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full text-center" colspan="2"><?=number_format($row->jumlah_pemeriksaan)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
        <td class="border-full text-right" colspan="3" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataCTSCANSebelumnya) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN CT-SCAN (PERIODE SEBELUMNYA)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">JUMLAH PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataCTSCANSebelumnya as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="3"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full text-center" colspan="2"><?=number_format($row->jumlah_pemeriksaan)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
        <td class="border-full text-right" colspan="3" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataMRISaatIni) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN MRI (PERIODE SAAT INI)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">JUMLAH PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataMRISaatIni as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="3"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full text-center" colspan="2"><?=number_format($row->jumlah_pemeriksaan)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
        <td class="border-full text-right" colspan="3" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataMRISebelumnya) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN MRI (PERIODE SEBELUMNYA)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">JUMLAH PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataMRISebelumnya as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="3"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full text-center" colspan="2"><?=number_format($row->jumlah_pemeriksaan)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
        <td class="border-full text-right" colspan="3" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataBMDSaatIni) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN BMD (PERIODE SAAT INI)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">JUMLAH PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataBMDSaatIni as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="3"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full text-center" colspan="2"><?=number_format($row->jumlah_pemeriksaan)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
        <td class="border-full text-right" colspan="3" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
  <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
  <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
</tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php $totalPotonganRs = 0; ?>
<?php $totalPajakDokter = 0; ?>
<?php $totalNetto = 0; ?>
<?php if ($dataBMDSebelumnya) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENDAPATAN BMD (PERIODE SEBELUMNYA)</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">TANGGAL PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">JUMLAH PEMERIKSAAN</td>
    <td class="border-full text-bold text-center" width="10%">JASA MEDIS</td>
    <td class="border-full text-bold text-center" width="10%">POTONGAN RS</td>
    <td class="border-full text-bold text-center" width="10%">PAJAK (PPH 21)</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JUMLAH NETTO</td>
</tr>
  <?php foreach ($dataBMDSebelumnya as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="3"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full text-center" colspan="2"><?=number_format($row->jumlah_pemeriksaan)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->jasamedis)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_potongan_rs)?></td>
        <td class="border-full text-right" width="10%"><?=number_format($row->nominal_pajak_dokter)?></td>
        <td class="border-full text-right" colspan="3" width="10%"><?=number_format($row->jasamedis_netto)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php $totalPotonganRs += $row->nominal_potongan_rs; ?>
  <?php $totalPajakDokter += $row->nominal_pajak_dokter; ?>
  <?php $totalNetto += $row->jasamedis_netto; ?>
  <?php } ?>
  <tr>
    <td colspan="6" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalJasaMedis); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPotonganRs); ?></td>
    <td class="border-full text-semi-bold text-right"><?= number_format($totalPajakDokter); ?></td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalNetto); ?></td>
  </tr>
<?php } ?>
