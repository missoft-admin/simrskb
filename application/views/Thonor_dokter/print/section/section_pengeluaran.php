<?php
  $params = array(
    'idhonor' => $id,
    'status_hold' => $statusHold,
  );
?>
<?php $dataPengeluaranKasbon = $this->model->getPotonganKasbon($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_hold' => $statusHold,
  );
?>
<?php $dataPotonganBerobat = $this->model->getPotonganBerobat($params); ?>

<?php
  $params = array(
    'idhonor' => $id,
    'status_hold' => $statusHold,
  );
?>
<?php $dataPotonganLainLain = $this->model->getPotonganLainLain($params); ?>

<!-- Header -->
<?php if($dataPengeluaranKasbon || $dataPotonganBerobat || $dataPotonganLainLain) { ?>
<tr>
  <td colspan="12" style="padding-top: 15px; padding-bottom: 15px;" class="text-center">
    <b class="text-bold" style="font-size:18px !important;">PENGELUARAN</b>
  </td>
</tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php if ($dataPengeluaranKasbon) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENGELUARAN KASBON</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">TANGGAL</td>
    <td class="border-full text-bold text-center" colspan="6" width="10%">CATATAN</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">NOMINAL POTONGAN</td>
</tr>
  <?php foreach ($dataPengeluaranKasbon as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="2"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full" colspan="6"><?=$row->namatarif?></td>
        <td class="border-full text-right" colspan="3" width="10%"><?=number_format($row->jasamedis)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php } ?>
  <tr>
    <td colspan="9" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalJasaMedis); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php if ($dataPotonganBerobat) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENGELUARAN POTONGAN BEROBAT</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">TANGGAL</td>
    <td class="border-full text-bold text-center" colspan="6" width="10%">NOMOR TRANSAKSI</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">NOMINAL POTONGAN</td>
</tr>
  <?php foreach ($dataPotonganBerobat as $index => $row) { ?>
    <tr>
        <td class="border-full text-center"><?=$index+1?></td>
        <td class="border-full text-center" colspan="2"><?=$row->tanggal_pemeriksaan?></td>
        <td class="border-full" colspan="6"><?=$row->namatarif?></td>
        <td class="border-full text-right" colspan="3" width="10%"><?=number_format($row->jasamedis)?></td>
    </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php } ?>
  <tr>
    <td colspan="9" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalJasaMedis); ?></td>
  </tr>
<?php } ?>


<!-- Sub Header -->
<?php $totalJasaMedis = 0; ?>
<?php if ($dataPotonganLainLain) { ?>
<tr>
  <td class="border-full" colspan="12" style="padding-top: 15px; padding-bottom: 15px;">
    <b class="text-bold" style="font-size:16px !important;">PENGELUARAN POTONGAN LAIN LAIN</b>
  </td>
</tr>
<tr>
    <td class="border-full text-bold text-center" width="5%">NO</td>
    <td class="border-full text-bold text-center" colspan="2" width="10%">TANGGAL</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">JENIS POTONGAN</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">DESKRIPSI</td>
    <td class="border-full text-bold text-center" colspan="3" width="10%">NOMINAL TRANSAKSI</td>
</tr>
  <?php foreach ($dataPotonganLainLain as $index => $row) { ?>
  <tr>
      <td class="border-full text-center"><?=$index+1?></td>
      <td class="border-full text-center" colspan="2"><?=$row->tanggal_pemeriksaan?></td>
      <td class="border-full text-center" colspan="3"><?=$row->jenis_potongan?></td>
      <td class="border-full" colspan="3"><?=$row->namatarif?></td>
      <td class="border-full text-right" colspan="3" width="10%"><?=number_format($row->jasamedis)?></td>
  </tr>
  <?php $totalJasaMedis += $row->jasamedis; ?>
  <?php } ?>
  <tr>
    <td colspan="9" class="border-full text-center text-semi-bold">TOTAL</td>
    <td class="border-full text-semi-bold text-right" colspan="3"><?= number_format($totalJasaMedis); ?></td>
  </tr>
<?php } ?>
