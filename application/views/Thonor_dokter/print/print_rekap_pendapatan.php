<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?= $title; ?></title>
    <style>
      body{
          font-family: "Courier", Arial,sans-serif;
      }
      @media print {
         table {
           font-size: 18px !important;
           border-collapse: collapse !important;
           width: 100% !important;
         }
         td {
           padding: 5px;
         }
         .content td {
           padding: 0px;
         }
         .border-full {
           border: 1px solid #000 !important;
         }
         .border-bottom {
           border-bottom:1px solid #000 !important;
         }
         .border-right {
           border-right:1px solid #000 !important;
         }
         .border-left {
           border-left:1px solid #000 !important;
         }
         .border-top {
           border-top:1px solid #000 !important;
         }
         .border-thick-top{
           border-top:2px solid #000 !important;
         }
         .border-thick-bottom{
           border-bottom:2px solid #000 !important;
         }
         .text-center{
           text-align: center !important;
         }
         .text-right{
           text-align: right !important;
         }
         .text-semi-bold {
           font-size: 15px !important;
           font-weight: 700 !important;
         }
         .text-bold {
           font-size: 15px !important;
           font-weight: 700 !important;
         }
         .text-footer {
           font-size: 12px !important;
         }
         .text-address {
           color: #3abad8 !important;
         }
      }

      @media screen {
         table {
           font-size: 18px !important;
           border-collapse: collapse !important;
           width: 100% !important;
         }
         td {
           padding: 5px;
         }
         .content td {
           padding: 0px;
         }
         .border-full {
           border: 1px solid #000 !important;
         }
         .border-bottom {
           border-bottom:1px solid #000 !important;
         }
         .border-right {
           border-right:1px solid #000 !important;
         }
         .border-left {
           border-left:1px solid #000 !important;
         }
         .border-top {
           border-top:1px solid #000 !important;
         }
         .border-thick-top{
           border-top:2px solid #000 !important;
         }
         .border-thick-bottom{
           border-bottom:2px solid #000 !important;
         }
         .text-center{
           text-align: center !important;
         }
         .text-right{
           text-align: right !important;
         }
         .text-semi-bold {
           font-size: 15px !important;
           font-weight: 700 !important;
         }
         .text-bold {
           font-size: 15px !important;
           font-weight: 700 !important;
         }
         .text-footer {
           font-size: 12px !important;
         }
         .text-address {
           color: #3abad8 !important;
         }
      }
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <table>
      <tr>
        <td style="width:20%" rowspan="2" colspan="2" class="text-center">
          <img style="width:80px;" src="<?=base_url()?>assets/upload/logo/logo.png">
        </td>
        <td class="text-center" colspan="9">
          <b style="font-size:22px !important; margin-left: -200px;"><?= $titleReport; ?></b>
        </td>
      </tr>
      <tr>
        <td class="text-center" colspan="9">
          <b style="font-size:18px !important; margin-left: -200px;"><?= $namaDokter; ?></b>
          <br>
          <b style="font-size:18px !important; margin-left: -200px;">PERIODE : <?= $tanggalPembayaran; ?></b>
          <br>
          <b style="font-size:18px !important; margin-left: -200px;">NPWP : <?= $npwpDokter; ?></b>
        </td>
      </tr>
      <tr>
        <td colspan="11"></td>
      </tr>
      <tr>
        <td colspan="11"></td>
      </tr>
      <?php $this->load->view('Thonor_dokter/print/section/section_pendapatan_rawat_jalan', array('statusHold' => $statusHold)); ?>
      <?php $this->load->view('Thonor_dokter/print/section/section_pendapatan_tindakan_radiologi', array('statusHold' => $statusHold)); ?>
      <?php $this->load->view('Thonor_dokter/print/section/section_pendapatan_tindakan_laboratorium', array('statusHold' => $statusHold)); ?>
      <?php $this->load->view('Thonor_dokter/print/section/section_pendapatan_tindakan_fisioterapi', array('statusHold' => $statusHold)); ?>
      <?php $this->load->view('Thonor_dokter/print/section/section_pendapatan_rujukan_rawat_inap', array('statusHold' => $statusHold)); ?>
      <?php $this->load->view('Thonor_dokter/print/section/section_pendapatan_rawat_inap', array('statusHold' => $statusHold)); ?>
      <?php $this->load->view('Thonor_dokter/print/section/section_pendapatan_anestesi', array('statusHold' => $statusHold)); ?>
      <?php $this->load->view('Thonor_dokter/print/section/section_pendapatan_lainlain', array('statusHold' => $statusHold)); ?>
      <?php $this->load->view('Thonor_dokter/print/section/section_pendapatan_nonpelayanan', array('statusHold' => $statusHold)); ?>
      <?php $this->load->view('Thonor_dokter/print/section/section_pendapatan_transport', array('statusHold' => $statusHold)); ?>
      <?php $this->load->view('Thonor_dokter/print/section/section_pengeluaran', array('statusHold' => $statusHold)); ?>
      <?php $this->load->view('Thonor_dokter/print/section/section_rekapitulasi', array('statusHold' => $statusHold)); ?>
    </table>

    <br>
    <br>

    <table>
      <tr>
        <td class="text-left"></td>
        <td class="text-right">
          <b>BAGIAN KEUANGAN</b>
        </td>
      </tr>
      <tr>
        <td class="text-left"></td>
        <td class="text-right">
          <br>
          <br>
        </td>
      </tr>
      <tr>
        <td class="text-left"></td>
        <td class="text-right">
          <br>
          <br>
        </td>
      </tr>
      <tr>
        <td class="text-left"></td>
        <td class="text-right">
          <?= $userFinance; ?>&nbsp;&nbsp;&nbsp;
        </td>
      </tr>
      <tr>
        <td class="text-left"></td>
        <td></td>
      </tr>
      <tr>
        <td class="text-left text-footer text-address">
          Jl. L.L.RE. Martadinata No.28 Bandung 40115 Jawa Barat, Indonesia<br>
          T. (022) 4206717 F. (022) 4216436 E. rskb.halmahera@gmail.com ; www.halmaherasiaga.com
        </td>
        <td class="text-right text-footer">
          Cetakan Ke-1 | UC : <?= $userPrint; ?> | <?= $datePrint; ?>
        </td>
      </tr>
    </table>
  </body>
</html>
