<?=ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}
?>

<style media="screen">
	.text-bold {
		font-weight: bold;
	}
	.separator-header {
		margin-top: 450px;
	}
  .modal { overflow: auto !important; }
</style>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}thonor_dokter/index" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title} </h3>
	</div>
	<div class="block-content">
		<div class="row">
			<div class="col-md-6">
				<div class="input-group" style="width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">Nama Dokter</span>
					<input type="text" class="form-control" value="{namadokter}" readonly="true">
				</div>
				<div class="input-group" style="margin-top:5px;width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">Nama Bank</span>
					<input type="text" class="form-control" value="{namabank}" readonly="true">
				</div>
			</div>
			<div class="col-md-6">
				<div class="input-group" style="width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">Nomor NPWP</span>
					<input type="text" class="form-control" value="{nonpwp}" readonly="true">
				</div>
        <div class="input-group" style="margin-top:5px;width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">Nomor Rekening</span>
					<input type="text" class="form-control" value="{norekening}" readonly="true">
				</div>
			</div>
		</div>

		<div class="content-verification">

			<hr>

      <div class="row">
    		<div class="col-md-12">
    			<div class="block block-themed <?=($open_block ? '' : 'block-opt-hidden');?>">
    				<div class="block-header bg-danger">
    					<ul class="block-options">
    						<li>
    							<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
    						</li>
    					</ul>
    					<h3 class="block-title">PENDAPATAN YANG DIBERIKAN PERIODE INI</h3>
    				</div>
    				<div class="block-content" style="padding: 5px 5px 5px 5px;">

              <?php $statusHold = 0; ?>
              <?php $this->load->view('Thonor_dokter/section/section_pendapatan_rawat_jalan', array('statusHold' => $statusHold)); ?>
              <?php $this->load->view('Thonor_dokter/section/section_pendapatan_tindakan_radiologi', array('statusHold' => $statusHold)); ?>
              <?php $this->load->view('Thonor_dokter/section/section_pendapatan_tindakan_laboratorium', array('statusHold' => $statusHold)); ?>
              <?php $this->load->view('Thonor_dokter/section/section_pendapatan_tindakan_fisioterapi', array('statusHold' => $statusHold)); ?>
              <?php $this->load->view('Thonor_dokter/section/section_pendapatan_rujukan_rawat_inap', array('statusHold' => $statusHold)); ?>
              <?php $this->load->view('Thonor_dokter/section/section_pendapatan_rawat_inap', array('statusHold' => $statusHold)); ?>
              <?php $this->load->view('Thonor_dokter/section/section_pendapatan_anestesi', array('statusHold' => $statusHold)); ?>
              <?php $this->load->view('Thonor_dokter/section/section_pendapatan_lainlain', array('statusHold' => $statusHold)); ?>
              <?php $this->load->view('Thonor_dokter/section/section_pendapatan_transport', array('statusHold' => $statusHold)); ?>
              <?php $this->load->view('Thonor_dokter/section/section_pendapatan_nonpelayanan', array('statusHold' => $statusHold)); ?>
              <?php $this->load->view('Thonor_dokter/section/section_pendapatan_informasi_medis', array('statusHold' => $statusHold)); ?>
              <?php $this->load->view('Thonor_dokter/section/section_pengeluaran', array('statusHold' => $statusHold)); ?>

    				</div>
    			</div>
    		</div>
    	</div>

      <div class="row">
    		<div class="col-md-12">
    			<div class="block block-themed block-opt-hidden">
    				<div class="block-header bg-danger">
    					<ul class="block-options">
    						<li>
    							<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
    						</li>
    					</ul>
    					<h3 class="block-title">PENDAPATAN YANG DITAHAN</h3>
    				</div>
    				<div class="block-content">

              <?php $statusHold = 1; ?>
              <?php $this->load->view('Thonor_dokter/section/section_pendapatan_rawat_jalan', array('statusHold' => $statusHold)); ?>
              <?php $this->load->view('Thonor_dokter/section/section_pendapatan_tindakan_radiologi', array('statusHold' => $statusHold)); ?>
              <?php $this->load->view('Thonor_dokter/section/section_pendapatan_tindakan_laboratorium', array('statusHold' => $statusHold)); ?>
              <?php $this->load->view('Thonor_dokter/section/section_pendapatan_tindakan_fisioterapi', array('statusHold' => $statusHold)); ?>
              <?php $this->load->view('Thonor_dokter/section/section_pendapatan_rujukan_rawat_inap', array('statusHold' => $statusHold)); ?>
              <?php $this->load->view('Thonor_dokter/section/section_pendapatan_rawat_inap', array('statusHold' => $statusHold)); ?>
              <?php $this->load->view('Thonor_dokter/section/section_pendapatan_anestesi', array('statusHold' => $statusHold)); ?>

    				</div>
    			</div>
    		</div>
    	</div>

      <div class="row">
    		<div class="col-md-12">
    			<div class="block block-themed <?=($open_block ? '' : 'block-opt-hidden');?>">
    				<div class="block-header bg-danger">
    					<ul class="block-options">
    						<li>
    							<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
    						</li>
    					</ul>
    					<h3 class="block-title">REKAPITULASI</h3>
    				</div>
    				<div class="block-content">

              <?php $this->load->view('Thonor_dokter/section/section_rekapitulasi'); ?>

    				</div>
    			</div>
    		</div>
    	</div>

		</div>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
  sumTotalPendapatan();
  sumTotalPendapatanRanap();
  sumTotalPendapatanTransport();
  sumTotalPengeluaran();
});

function sumTotalPendapatan() {
  var table = $(".tablePendapatan");
  var count = table.length;

  table.each(function (index, section) {
    var totalJasaMedis = 0;
    var totalPotonganRS = 0;
    var totalPajakDokter = 0;
    var totalJasaMedisNetto = 0;

    var tableSection = $(section).find("tbody tr");
    var indexStart = parseInt($(section).data('start'));
    var countSection = tableSection.length;

    tableSection.each(function (i) {
      totalJasaMedis += parseFloat($(this).find('td:eq(' + (indexStart + 0) + ')').text().replace(/,/g, ''));
      totalPotonganRS += parseFloat($(this).find('td:eq(' + (indexStart + 1) + ')').text().replace(/,/g, ''));
      totalPajakDokter += parseFloat($(this).find('td:eq(' + (indexStart + 2) + ')').text().replace(/,/g, ''));
      totalJasaMedisNetto += parseFloat($(this).find('td:eq(' + (indexStart + 3) + ')').text().replace(/,/g, ''));

      $(".totalJasaMedis").eq(index).html($.number(totalJasaMedis));
      $(".totalPotonganRS").eq(index).html($.number(totalPotonganRS));
      $(".totalPajakDokter").eq(index).html($.number(totalPajakDokter));
      $(".totalJasaMedisNetto").eq(index).html($.number(totalJasaMedisNetto));
    });
  });
}

function sumTotalPendapatanRanap() {
  var table = $(".tablePendapatanRanap");
  var count = table.length;

  table.each(function (index, section) {
    var totalJasaMedis = 0;
    var totalPotonganRS = 0;
    var totalPotonganPajak = 0;
    var totalPajakDokter = 0;
    var totalJasaMedisNetto = 0;

    var tableSection = $(section).find("tbody tr");
    var indexStart = parseInt($(section).data('start'));
    var countSection = tableSection.length;

    tableSection.each(function (i) {
      totalJasaMedis += parseFloat($(this).find('td:eq(' + (indexStart + 0) + ')').text().replace(/,/g, ''));
      totalPotonganRS += parseFloat($(this).find('td:eq(' + (indexStart + 1) + ')').text().replace(/,/g, ''));
      totalPotonganPajak += parseFloat($(this).find('td:eq(' + (indexStart + 2) + ')').text().replace(/,/g, ''));
      totalPajakDokter += parseFloat($(this).find('td:eq(' + (indexStart + 3) + ')').text().replace(/,/g, ''));
      totalJasaMedisNetto += parseFloat($(this).find('td:eq(' + (indexStart + 4) + ')').text().replace(/,/g, ''));

      $(".totalJasaMedisRanap").eq(index).html($.number(totalJasaMedis));
      $(".totalPotonganRSRanap").eq(index).html($.number(totalPotonganRS));
      $(".totalPotonganPajakRanap").eq(index).html($.number(totalPotonganPajak));
      $(".totalPajakDokterRanap").eq(index).html($.number(totalPajakDokter));
      $(".totalJasaMedisNettoRanap").eq(index).html($.number(totalJasaMedisNetto));
    });
  });
}

function sumTotalPendapatanTransport() {
  var table = $(".tablePendapatanTransport");
  var count = table.length;

  table.each(function (index, section) {
    var totalJagaTransport = 0;
    var totalNominalTransport = 0;
    var totalPotonganRSTransport = 0;
    var totalPajakDokterTransport = 0;
    var totalNettoTransport = 0;

    var tableSection = $(section).find("tbody tr");
    var indexStart = parseInt($(section).data('start'));
    var countSection = tableSection.length;

    tableSection.each(function (i) {
      totalJagaTransport += parseFloat($(this).find('td:eq(' + (indexStart + 0) + ')').text().replace(/,/g, ''));
      totalNominalTransport += parseFloat($(this).find('td:eq(' + (indexStart + 1) + ')').text().replace(/,/g, ''));
      totalPotonganRSTransport += parseFloat($(this).find('td:eq(' + (indexStart + 1) + ')').text().replace(/,/g, ''));
      totalPajakDokterTransport += parseFloat($(this).find('td:eq(' + (indexStart + 3) + ')').text().replace(/,/g, ''));
      totalNettoTransport += parseFloat($(this).find('td:eq(' + (indexStart + 4) + ')').text().replace(/,/g, ''));

      $(".totalJagaTransport").eq(index).html($.number(totalJagaTransport));
      $(".totalNominalTransport").eq(index).html($.number(totalNominalTransport));
      $(".totalPotonganRSTransport").eq(index).html($.number(totalPotonganRSTransport));
      $(".totalPajakDokterTransport").eq(index).html($.number(totalPajakDokterTransport));
      $(".totalNettoTransport").eq(index).html($.number(totalNettoTransport));
    });
  });
}

function sumTotalPengeluaran() {
  var table = $(".tablePengeluaran");
  var count = table.length;

  table.each(function (index, section) {
    var totalNominal = 0;

    var tableSection = $(section).find("tbody tr");
    var indexStart = parseInt($(section).data('start'));
    var countSection = tableSection.length;

    tableSection.each(function (i) {
      totalNominal += parseFloat($(this).find('td:eq(' + (indexStart) + ')').text().replace(/,/g, ''));

      $(".totalNominal").eq(index).html($.number(totalNominal));
    });
  });
}
</script>

<?php $this->load->view('Tverifikasi_transaksi/modal/modal_jasa_dokter'); ?>
