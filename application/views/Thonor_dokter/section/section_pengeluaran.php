<div class="block block-themed <?=($open_block ? '' : 'block-opt-hidden');?>">
  <div class="block-header <?=($statusHold ? 'bg-warning':'bg-primary')?>" style="padding: 10px 20px;">
    <ul class="block-options">
      <li>
        <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
      </li>
    </ul>
    <h3 class="block-title">PENGELUARAN</h3>
  </div>
  <div class="block-content" style="padding: 5px 0 5px 0;">

    <br>

    <b><span class="label label-success" style="font-size:12px">KASBON</span></b>
    <table class="table table-bordered table-striped tablePengeluaran" data-start="3" style="margin-top: 10px;">
      <thead>
        <tr>
          <th style="width:5%">No.</th>
          <th style="width:10%">Tanggal</th>
          <th style="width:10%">Catatan</th>
          <th style="width:10%">Nominal Potongan</th>
          <th style="width:15%">Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php
          $params = array(
            'idhonor' => $id,
            'status_hold' => $statusHold,
          );
        ?>
        <?php $data = $this->model->getPotonganKasbon($params); ?>
        <?php foreach ($data as $index => $row) { ?>
          <tr>
            <td><?=$index+1?> <?= '(<label style="color: red;">' . $row->id . '</label>)'; ?></td>
            <td><?=$row->tanggal_pemeriksaan?></td>
            <td><?=$row->namatarif?></td>
            <td><?=number_format($row->jasamedis)?></td>
            <td>
              <div class="btn-group">
                <button type="button" class="btn btn-sm btn-danger" data-idrow="<?=$row->idtransaksi?>"><i class="fa fa-arrow-right"></i></button>
                <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
              </div>
            </td>
          </tr>
        <?php } ?>
      </tbody>
      <tfoot>
        <tr>
          <td align="center" colspan="3"><b>TOTAL</b></td>
          <td class="text-bold totalNominal">0</td>
          <td></td>
        </tr>
      </tfoot>
    </table>

    <hr>

    <b><span class="label label-success" style="font-size:12px">POTONGAN BEROBAT</span></b>
    <table class="table table-bordered table-striped tablePengeluaran" data-start="3" style="margin-top: 10px;">
      <thead>
        <tr>
          <th style="width:5%">No.</th>
          <th style="width:10%">Tanggal</th>
          <th style="width:10%">No. Transaksi</th>
          <th style="width:10%">Nominal Potongan</th>
          <th style="width:15%">Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php
          $params = array(
            'idhonor' => $id,
            'status_hold' => $statusHold,
          );
        ?>
        <?php $data = $this->model->getPotonganBerobat($params); ?>
        <?php foreach ($data as $index => $row) { ?>
          <tr>
            <td><?=$index+1?> <?= '(<label style="color: red;">' . $row->id . '</label>)'; ?></td>
            <td><?=$row->tanggal_pemeriksaan?></td>
            <td><?=$row->namatarif?></td>
            <td><?=number_format($row->jasamedis)?></td>
            <td>
              <div class="btn-group">
                <button type="button" class="btn btn-sm btn-danger" data-idrow="<?=$row->idtransaksi?>"><i class="fa fa-arrow-right"></i></button>
                <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
              </div>
            </td>
          </tr>
        <?php } ?>
      </tbody>
      <tfoot>
        <tr>
          <td align="center" colspan="3"><b>TOTAL</b></td>
          <td class="text-bold totalNominal">0</td>
          <td></td>
        </tr>
      </tfoot>
    </table>

    <hr>

    <b><span class="label label-success" style="font-size:12px">POTONGAN LAIN LAIN</span></b>
    <table class="table table-bordered table-striped tablePengeluaran" data-start="4" style="margin-top: 10px;">
      <thead>
        <tr>
          <th style="width:5%">No.</th>
          <th style="width:10%">Tanggal</th>
          <th style="width:10%">Jenis Potongan</th>
          <th style="width:10%">Deskripsi</th>
          <th style="width:10%">Nominal Potongan</th>
          <th style="width:10%">Status</th>
          <th style="width:15%">Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php
          $params = array(
            'idhonor' => $id,
            'status_hold' => $statusHold,
          );
        ?>
        <?php $data = $this->model->getPotonganLainLain($params); ?>
        <?php foreach ($data as $index => $row) { ?>
          <?php
            if ($row->status_potongan == 1) {
              $status_potongan = '<label class="label label-md label-success">Rutin</label>';
              if ($row->status_periode == 1) {
                $status_periode = 'Setiap Tanggal';
              } else {
                $status_periode = 'Setiap Bulan';
              }
            } else {
              $status_potongan = '<label class="label label-md label-warning">Non Rutin</label><br>';
              $status_periode = DMYFormat($row->tanggal_pembayaran);
            }
          ?>
          <tr>
            <td><?=$index+1?> <?= '(<label style="color: red;">' . $row->id . '</label>)'; ?></td>
            <td><?=$row->tanggal_pemeriksaan?></td>
            <td><?=$row->jenis_potongan?></td>
            <td><?=$row->namatarif?></td>
            <td><?=number_format($row->jasamedis)?></td>
            <td><?=$status_potongan?></td>
            <td>
              <div class="btn-group">
                <button type="button" class="btn btn-sm btn-danger deletePotonganDokter" data-idrow="<?=$row->idtransaksi?>"><i class="fa fa-trash"></i></button>
                <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
              </div>
            </td>
          </tr>
        <?php } ?>
      </tbody>
      <tfoot>
        <tr>
          <td align="center" colspan="4"><b>TOTAL</b></td>
          <td class="text-bold totalNominal">0</td>
          <td colspan="2"></td>
        </tr>
      </tfoot>
    </table>

  </div>
</div>

<?php if ($statusHold == 0) { ?>
  <?php $this->load->view('Tpotongan_dokter/modal'); ?>
<?php } ?>
