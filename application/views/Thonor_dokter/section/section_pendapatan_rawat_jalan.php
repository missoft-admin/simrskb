<div class="block block-themed <?=($open_block ? '' : 'block-opt-hidden');?>">
  <div class="block-header <?=($statusHold ? 'bg-warning':'bg-primary')?>" style="padding: 10px 20px;">
    <ul class="block-options">
      <li>
        <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
      </li>
    </ul>
    <h3 class="block-title">PENDAPATAN RAWAT JALAN</h3>
  </div>
  <div class="block-content" style="padding: 5px 0 5px 0;">

    <div class="block block-themed <?=($open_block ? '' : 'block-opt-hidden');?>">
      <div class="block-header bg-success" style="padding: 8px 20px;">
        <ul class="block-options">
          <li>
            <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
          </li>
        </ul>
        <h3 class="block-title">PASIEN RUMAH SAKIT</h3>
      </div>
      <div class="block-content">

        <b><span class="label label-success" style="font-size:12px">PASIEN UMUM</span></b>
        <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
          <thead>
            <tr>
              <th style="width:5%">No.</th>
              <th style="width:10%">Tanggal Pemeriksaan</th>
              <th style="width:10%">Kelompok Pasien</th>
              <th style="width:10%">Jasa Medis</th>
              <th style="width:10%">Potongan RS</th>
              <th style="width:10%">Pajak (PPH 21)</th>
              <th style="width:10%">Jumlah</th>
              <th style="width:15%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $params = array(
                'idhonor' => $id,
                'status_periode' => 'Periode Saat Ini',
                'jenis_pasien' => 'Pasien RS',
                'idkelompok' => array(5),
                'status_hold' => $statusHold,
              );
            ?>
            <?php $data = $this->model->getPendapatanRawatJalan($params); ?>
            <?php foreach ($data as $index => $row) { ?>

              <?php $dataDetail = json_encode(
                array(
                  'title' => 'PENDAPATAN RAWAT JALAN / PASIEN RUMAH SAKIT / PASIEN UMUM',
                  'status_hold' => $statusHold,
                  'list_id' => explode(',', $row->list_id)
                )
              ); ?>

              <tr>
                <td><?=$index+1?></td>
                <td><?=$row->tanggal_pemeriksaan?></td>
                <td><?=$row->namakelompok?></td>
                <td><?=number_format($row->jasamedis)?></td>
                <td><?=number_format($row->nominal_potongan_rs)?></td>
                <td><?=number_format($row->nominal_pajak_dokter)?></td>
                <td><?=number_format($row->jasamedis_netto)?></td>
                <td>
                  <div class="btn-group">
                    <a href="{base_url}thonor_dokter/detail_tindakan/<?= base64url_encode($dataDetail); ?>" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                    <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                    <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td align="center" colspan="3"><b>TOTAL</b></td>
              <td class="text-bold totalJasaMedis">0</td>
              <td class="text-bold totalPotonganRS">0</td>
              <td class="text-bold totalPajakDokter">0</td>
              <td class="text-bold totalJasaMedisNetto">0</td>
              <td></td>
            </tr>
          </tfoot>
        </table>

        <hr>

        <b><span class="label label-success" style="font-size:12px">PASIEN REKANAN</span></b>
        <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
          <thead>
            <tr>
              <th style="width:5%">No.</th>
              <th style="width:10%">Tanggal Pemeriksaan</th>
              <th style="width:10%">Kelompok Pasien</th>
              <th style="width:10%">Jasa Medis</th>
              <th style="width:10%">Potongan RS</th>
              <th style="width:10%">Pajak (PPH 21)</th>
              <th style="width:10%">Jumlah</th>
              <th style="width:15%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $params = array(
                'idhonor' => $id,
                'status_periode' => 'Periode Saat Ini',
                'jenis_pasien' => 'Pasien RS',
                'idkelompok' => array(1, 2, 3, 4),
                'status_hold' => $statusHold,
              );
            ?>
            <?php $data = $this->model->getPendapatanRawatJalan($params); ?>
            <?php foreach ($data as $index => $row) { ?>

              <?php $dataDetail = json_encode(
                array(
                  'title' => 'PENDAPATAN RAWAT JALAN / PASIEN RUMAH SAKIT / PASIEN REKANAN',
                  'status_hold' => $statusHold,
                  'list_id' => explode(',', $row->list_id)
                )
              ); ?>

              <tr>
                <td><?=$index+1?></td>
                <td><?=$row->tanggal_pemeriksaan?></td>
                <td><?=$row->namakelompok?></td>
                <td><?=number_format($row->jasamedis)?></td>
                <td><?=number_format($row->nominal_potongan_rs)?></td>
                <td><?=number_format($row->nominal_pajak_dokter)?></td>
                <td><?=number_format($row->jasamedis_netto)?></td>
                <td>
                  <div class="btn-group">
                    <a href="{base_url}thonor_dokter/detail_tindakan/<?= base64url_encode($dataDetail); ?>" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                    <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                    <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td align="center" colspan="3"><b>TOTAL</b></td>
              <td class="text-bold totalJasaMedis">0</td>
              <td class="text-bold totalPotonganRS">0</td>
              <td class="text-bold totalPajakDokter">0</td>
              <td class="text-bold totalJasaMedisNetto">0</td>
              <td></td>
            </tr>
          </tfoot>
        </table>

        <hr>

        <b><span class="label label-success" style="font-size:12px">PASIEN PERIODE SEBELUMNYA</span></b>
        <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
          <thead>
            <tr>
              <th style="width:5%">No.</th>
              <th style="width:10%">Tanggal Pemeriksaan</th>
              <th style="width:10%">Kelompok Pasien</th>
              <th style="width:10%">Jasa Medis</th>
              <th style="width:10%">Potongan RS</th>
              <th style="width:10%">Pajak (PPH 21)</th>
              <th style="width:10%">Jumlah</th>
              <th style="width:15%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $params = array(
                'idhonor' => $id,
                'status_periode' => 'Periode Sebelumnya',
                'jenis_pasien' => 'Pasien RS',
                'idkelompok' => array(1, 2, 3, 4, 5),
                'status_hold' => $statusHold,
              );
            ?>
            <?php $data = $this->model->getPendapatanRawatJalan($params); ?>
            <?php foreach ($data as $index => $row) { ?>

              <?php $dataDetail = json_encode(
                array(
                  'title' => 'PENDAPATAN RAWAT JALAN / PASIEN RUMAH SAKIT / PERIODE SEBELUMNYA',
                  'status_hold' => $statusHold,
                  'list_id' => explode(',', $row->list_id)
                )
              ); ?>

              <tr>
                <td><?=$index+1?></td>
                <td><?=$row->tanggal_pemeriksaan?></td>
                <td><?=$row->namakelompok?></td>
                <td><?=number_format($row->jasamedis)?></td>
                <td><?=number_format($row->nominal_potongan_rs)?></td>
                <td><?=number_format($row->nominal_pajak_dokter)?></td>
                <td><?=number_format($row->jasamedis_netto)?></td>
                <td>
                  <div class="btn-group">
                    <a href="{base_url}thonor_dokter/detail_tindakan/<?= base64url_encode($dataDetail); ?>" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                    <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                    <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td align="center" colspan="3"><b>TOTAL</b></td>
              <td class="text-bold totalJasaMedis">0</td>
              <td class="text-bold totalPotonganRS">0</td>
              <td class="text-bold totalPajakDokter">0</td>
              <td class="text-bold totalJasaMedisNetto">0</td>
              <td></td>
            </tr>
          </tfoot>
        </table>

      </div>
    </div>

    <div class="block block-themed <?=($open_block ? '' : 'block-opt-hidden');?>">
      <div class="block-header bg-success" style="padding: 8px 20px;">
        <ul class="block-options">
          <li>
            <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
          </li>
        </ul>
        <h3 class="block-title">PASIEN PRIBADI</h3>
      </div>
      <div class="block-content">

        <b><span class="label label-success" style="font-size:12px">PASIEN UMUM</span></b>
        <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
          <thead>
            <tr>
              <th style="width:5%">No.</th>
              <th style="width:10%">Tanggal Pemeriksaan</th>
              <th style="width:10%">Kelompok Pasien</th>
              <th style="width:10%">Jasa Medis</th>
              <th style="width:10%">Potongan RS</th>
              <th style="width:10%">Pajak (PPH 21)</th>
              <th style="width:10%">Jumlah</th>
              <th style="width:15%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $params = array(
                'idhonor' => $id,
                'status_periode' => 'Periode Saat Ini',
                'jenis_pasien' => 'Pasien Pribadi',
                'idkelompok' => array(5),
                'status_hold' => $statusHold,
              );
            ?>
            <?php $data = $this->model->getPendapatanRawatJalan($params); ?>
            <?php foreach ($data as $index => $row) { ?>

              <?php $dataDetail = json_encode(
                array(
                  'title' => 'PENDAPATAN RAWAT JALAN / PASIEN PRIBADI / PASIEN UMUM',
                  'status_hold' => $statusHold,
                  'list_id' => explode(',', $row->list_id)
                )
              ); ?>

              <tr>
                <td><?=$index+1?></td>
                <td><?=$row->tanggal_pemeriksaan?></td>
                <td><?=$row->namakelompok?></td>
                <td><?=number_format($row->jasamedis)?></td>
                <td><?=number_format($row->nominal_potongan_rs)?></td>
                <td><?=number_format($row->nominal_pajak_dokter)?></td>
                <td><?=number_format($row->jasamedis_netto)?></td>
                <td>
                  <div class="btn-group">
                    <a href="{base_url}thonor_dokter/detail_tindakan/<?= base64url_encode($dataDetail); ?>" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                    <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                    <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td align="center" colspan="3"><b>TOTAL</b></td>
              <td class="text-bold totalJasaMedis">0</td>
              <td class="text-bold totalPotonganRS">0</td>
              <td class="text-bold totalPajakDokter">0</td>
              <td class="text-bold totalJasaMedisNetto">0</td>
              <td></td>
            </tr>
          </tfoot>
        </table>

        <hr>

        <b><span class="label label-success" style="font-size:12px">PASIEN REKANAN</span></b>
        <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
          <thead>
            <tr>
              <th style="width:5%">No.</th>
              <th style="width:10%">Tanggal Pemeriksaan</th>
              <th style="width:10%">Kelompok Pasien</th>
              <th style="width:10%">Jasa Medis</th>
              <th style="width:10%">Potongan RS</th>
              <th style="width:10%">Pajak (PPH 21)</th>
              <th style="width:10%">Jumlah</th>
              <th style="width:15%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $params = array(
                'idhonor' => $id,
                'status_periode' => 'Periode Saat Ini',
                'jenis_pasien' => 'Pasien Pribadi',
                'idkelompok' => array(1, 2, 3, 4),
                'status_hold' => $statusHold,
              );
            ?>
            <?php $data = $this->model->getPendapatanRawatJalan($params); ?>
            <?php foreach ($data as $index => $row) { ?>

              <?php $dataDetail = json_encode(
                array(
                  'title' => 'PENDAPATAN RAWAT JALAN / PASIEN PRIBADI / PASIEN REKANAN',
                  'status_hold' => $statusHold,
                  'list_id' => explode(',', $row->list_id)
                )
              ); ?>

              <tr>
                <td><?=$index+1?></td>
                <td><?=$row->tanggal_pemeriksaan?></td>
                <td><?=$row->namakelompok?></td>
                <td><?=number_format($row->jasamedis)?></td>
                <td><?=number_format($row->nominal_potongan_rs)?></td>
                <td><?=number_format($row->nominal_pajak_dokter)?></td>
                <td><?=number_format($row->jasamedis_netto)?></td>
                <td>
                  <div class="btn-group">
                    <a href="{base_url}thonor_dokter/detail_tindakan/<?= base64url_encode($dataDetail); ?>" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                    <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                    <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td align="center" colspan="3"><b>TOTAL</b></td>
              <td class="text-bold totalJasaMedis">0</td>
              <td class="text-bold totalPotonganRS">0</td>
              <td class="text-bold totalPajakDokter">0</td>
              <td class="text-bold totalJasaMedisNetto">0</td>
              <td></td>
            </tr>
          </tfoot>
        </table>

        <hr>

        <b><span class="label label-success" style="font-size:12px">PASIEN PERIODE SEBELUMNYA</span></b>
        <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
          <thead>
            <tr>
              <th style="width:5%">No.</th>
              <th style="width:10%">Tanggal Pemeriksaan</th>
              <th style="width:10%">Kelompok Pasien</th>
              <th style="width:10%">Jasa Medis</th>
              <th style="width:10%">Potongan RS</th>
              <th style="width:10%">Pajak (PPH 21)</th>
              <th style="width:10%">Jumlah</th>
              <th style="width:15%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $params = array(
                'idhonor' => $id,
                'status_periode' => 'Periode Sebelumnya',
                'jenis_pasien' => 'Pasien Pribadi',
                'idkelompok' => array(1, 2, 3, 4, 5),
                'status_hold' => $statusHold,
              );
            ?>
            <?php $data = $this->model->getPendapatanRawatJalan($params); ?>
            <?php foreach ($data as $index => $row) { ?>

              <?php $dataDetail = json_encode(
                array(
                  'title' => 'PENDAPATAN RAWAT JALAN / PASIEN PRIBADI / PERIODE SEBELUMNYA',
                  'status_hold' => $statusHold,
                  'list_id' => explode(',', $row->list_id)
                )
              ); ?>

              <tr>
                <td><?=$index+1?></td>
                <td><?=$row->tanggal_pemeriksaan?></td>
                <td><?=$row->namakelompok?></td>
                <td><?=number_format($row->jasamedis)?></td>
                <td><?=number_format($row->nominal_potongan_rs)?></td>
                <td><?=number_format($row->nominal_pajak_dokter)?></td>
                <td><?=number_format($row->jasamedis_netto)?></td>
                <td>
                  <div class="btn-group">
                    <a href="{base_url}thonor_dokter/detail_tindakan/<?= base64url_encode($dataDetail); ?>" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                    <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                    <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td align="center" colspan="3"><b>TOTAL</b></td>
              <td class="text-bold totalJasaMedis">0</td>
              <td class="text-bold totalPotonganRS">0</td>
              <td class="text-bold totalPajakDokter">0</td>
              <td class="text-bold totalJasaMedisNetto">0</td>
              <td></td>
            </tr>
          </tfoot>
        </table>

      </div>
    </div>

  </div>
</div>
