<div class="block block-themed <?=($open_block ? '' : 'block-opt-hidden');?>">
  <div class="block-header <?=($statusHold ? 'bg-warning':'bg-primary')?>" style="padding: 10px 20px;">
    <ul class="block-options">
      <li>
        <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
      </li>
    </ul>
    <h3 class="block-title">PENDAPATAN TRANSPORT</h3>
  </div>
  <div class="block-content" style="padding: 5px 0 5px 0;">

    <br>

    <table class="table table-bordered table-striped tablePendapatanTransport" data-start="3" style="margin-top: 10px;">
      <thead>
        <tr>
          <th style="width:5%">No.</th>
          <th style="width:10%">Hari Kerja</th>
          <th style="width:10%">Hari Libur</th>
          <th style="width:10%">Total Jaga</th>
          <th style="width:10%">Total Transport</th>
          <th style="width:10%">Potongan RS</th>
          <th style="width:10%">Pajak (PPH 21)</th>
          <th style="width:10%">Jumlah</th>
          <th style="width:15%">Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php
          $params = array(
            'idhonor' => $id,
            'status_hold' => $statusHold,
          );
        ?>
        <?php $data = $this->model->getPendapatanTransport($params); ?>
        <?php foreach ($data as $index => $row) { ?>
          <tr>
            <td><?=$index+1?> <?= '(<label style="color: red;">' . $row->id . '</label>)'; ?></td>
            <td><?=number_format($row->hari_kerja)?></td>
            <td><?=number_format($row->hari_libur)?></td>
            <td><?=number_format($row->total_jaga)?></td>
            <td><?=number_format($row->total_transport)?></td>
            <td><?=number_format($row->total_potongan_rs)?></td>
            <td><?=number_format($row->total_pajak_dokter)?></td>
            <td><?=number_format($row->total_netto)?></td>
            <td>
              <div class="btn-group">
                <a href="#" title="Rincian Transport Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                <button type="button" class="btn btn-sm btn-danger" data-idrow="<?=$row->idtransaksi?>"><i class="fa fa-arrow-right"></i></button>
              </div>
            </td>
          </tr>
        <?php } ?>
      </tbody>
      <tfoot>
        <tr>
          <td align="center" colspan="3"><b>TOTAL</b></td>
          <td class="text-bold totalJagaTransport">0</td>
          <td class="text-bold totalNominalTransport">0</td>
          <td class="text-bold totalPotonganRSTransport">0</td>
          <td class="text-bold totalPajakDokterTransport">0</td>
          <td class="text-bold totalNettoTransport">0</td>
          <td></td>
        </tr>
      </tfoot>
    </table>

  </div>
</div>
