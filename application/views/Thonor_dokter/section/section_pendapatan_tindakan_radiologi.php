<div class="block block-themed <?=($open_block ? '' : 'block-opt-hidden');?>">
  <div class="block-header <?=($statusHold ? 'bg-warning':'bg-primary')?>" style="padding: 10px 20px;">
    <ul class="block-options">
      <li>
        <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
      </li>
    </ul>
    <h3 class="block-title">PENDAPATAN TINDAKAN RADIOLOGI</h3>
  </div>
  <div class="block-content" style="padding: 5px 0 5px 0;">

    <div class="block block-themed <?=($open_block ? '' : 'block-opt-hidden');?>">
      <div class="block-header bg-success" style="padding: 8px 20px;">
        <ul class="block-options">
          <li>
            <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
          </li>
        </ul>
        <h3 class="block-title">PENDAPATAN X-RAY</h3>
      </div>
      <div class="block-content">

        <b><span class="label label-success" style="font-size:12px">EXPERTISE LANGSUNG</span></b>
        <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
          <thead>
            <tr>
              <th style="width:5%">No.</th>
              <th style="width:10%">Tanggal Pemeriksaan</th>
              <th style="width:10%">Kelompok Pasien</th>
              <th style="width:10%">Jumlah Pemeriksaan</th>
              <th style="width:10%">Jasa Medis</th>
              <th style="width:10%">Potongan RS</th>
              <th style="width:10%">Pajak (PPH 21)</th>
              <th style="width:10%">Jumlah</th>
              <th style="width:15%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $params = array(
                'idhonor' => $id,
                'status_periode' => 'Periode Saat Ini',
                'idtipe_radiologi' => '1',
                'status_expertise' => '1',
                'status_hold' => $statusHold,
              );
            ?>
            <?php $data = $this->model->getPendapatanRadiologi($params); ?>
            <?php foreach ($data as $index => $row) { ?>

              <?php $dataDetail = json_encode(
                array(
                  'title' => 'PENDAPATAN X-RAY / EXPERTISE LANGSUNG',
                  'status_hold' => $statusHold,
                  'list_id' => explode(',', $row->list_id)
                )
              ); ?>

              <tr>
                <td><?=$index+1?></td>
                <td><?=$row->tanggal_pemeriksaan?></td>
                <td><?=$row->namakelompok?></td>
                <td><?=number_format($row->jumlah_pemeriksaan)?></td>
                <td><?=number_format($row->jasamedis)?></td>
                <td><?=number_format($row->nominal_potongan_rs)?></td>
                <td><?=number_format($row->nominal_pajak_dokter)?></td>
                <td><?=number_format($row->jasamedis_netto)?></td>
                <td>
                  <div class="btn-group">
                    <a href="{base_url}thonor_dokter/detail_tindakan/<?= base64url_encode($dataDetail); ?>" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                    <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                    <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td align="center" colspan="4"><b>TOTAL</b></td>
              <td class="text-bold totalJasaMedis">0</td>
              <td class="text-bold totalPotonganRS">0</td>
              <td class="text-bold totalPajakDokter">0</td>
              <td class="text-bold totalJasaMedisNetto">0</td>
              <td></td>
            </tr>
          </tfoot>
        </table>

        <hr>

        <b><span class="label label-success" style="font-size:12px">EXPERTISE MENOLAK</span></b>
        <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
          <thead>
            <tr>
              <th style="width:5%">No.</th>
              <th style="width:10%">Tanggal Pemeriksaan</th>
              <th style="width:10%">Kelompok Pasien</th>
              <th style="width:10%">Jumlah Pemeriksaan</th>
              <th style="width:10%">Jasa Medis</th>
              <th style="width:10%">Potongan RS</th>
              <th style="width:10%">Pajak (PPH 21)</th>
              <th style="width:10%">Jumlah</th>
              <th style="width:15%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $params = array(
                'idhonor' => $id,
                'status_periode' => 'Periode Saat Ini',
                'idtipe_radiologi' => '1',
                'status_expertise' => '2',
                'status_hold' => $statusHold,
              );
            ?>
            <?php $data = $this->model->getPendapatanRadiologi($params); ?>
            <?php foreach ($data as $index => $row) { ?>

              <?php $dataDetail = json_encode(
                array(
                  'title' => 'PENDAPATAN X-RAY / EXPERTISE MENOLAK',
                  'status_hold' => $statusHold,
                  'list_id' => explode(',', $row->list_id)
                )
              ); ?>

              <tr>
                <td><?=$index+1?></td>
                <td><?=$row->tanggal_pemeriksaan?></td>
                <td><?=$row->namakelompok?></td>
                <td><?=number_format($row->jumlah_pemeriksaan)?></td>
                <td><?=number_format($row->jasamedis)?></td>
                <td><?=number_format($row->nominal_potongan_rs)?></td>
                <td><?=number_format($row->nominal_pajak_dokter)?></td>
                <td><?=number_format($row->jasamedis_netto)?></td>
                <td>
                  <div class="btn-group">
                    <a href="{base_url}thonor_dokter/detail_tindakan/<?= base64url_encode($dataDetail); ?>" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                    <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                    <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td align="center" colspan="4"><b>TOTAL</b></td>
              <td class="text-bold totalJasaMedis">0</td>
              <td class="text-bold totalPotonganRS">0</td>
              <td class="text-bold totalPajakDokter">0</td>
              <td class="text-bold totalJasaMedisNetto">0</td>
              <td></td>
            </tr>
          </tfoot>
        </table>

        <hr>

        <b><span class="label label-success" style="font-size:12px">EXPERTISE KEMBALI</span></b>
        <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
          <thead>
            <tr>
              <th style="width:5%">No.</th>
              <th style="width:10%">Tanggal Pemeriksaan</th>
              <th style="width:10%">Kelompok Pasien</th>
              <th style="width:10%">Jumlah Pemeriksaan</th>
              <th style="width:10%">Jasa Medis</th>
              <th style="width:10%">Potongan RS</th>
              <th style="width:10%">Pajak (PPH 21)</th>
              <th style="width:10%">Jumlah</th>
              <th style="width:15%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $params = array(
                'idhonor' => $id,
                'status_periode' => 'Periode Saat Ini',
                'idtipe_radiologi' => '1',
                'status_expertise' => '3',
                'status_hold' => $statusHold,
              );
            ?>
            <?php $data = $this->model->getPendapatanRadiologi($params); ?>
            <?php foreach ($data as $index => $row) { ?>

              <?php $dataDetail = json_encode(
                array(
                  'title' => 'PENDAPATAN X-RAY / EXPERTISE KEMBALI',
                  'status_hold' => $statusHold,
                  'list_id' => explode(',', $row->list_id)
                )
              ); ?>

              <tr>
                <td><?=$index+1?></td>
                <td><?=$row->tanggal_pemeriksaan?></td>
                <td><?=$row->namakelompok?></td>
                <td><?=number_format($row->jumlah_pemeriksaan)?></td>
                <td><?=number_format($row->jasamedis)?></td>
                <td><?=number_format($row->nominal_potongan_rs)?></td>
                <td><?=number_format($row->nominal_pajak_dokter)?></td>
                <td><?=number_format($row->jasamedis_netto)?></td>
                <td>
                  <div class="btn-group">
                    <a href="{base_url}thonor_dokter/detail_tindakan/<?= base64url_encode($dataDetail); ?>" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                    <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                    <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td align="center" colspan="4"><b>TOTAL</b></td>
              <td class="text-bold totalJasaMedis">0</td>
              <td class="text-bold totalPotonganRS">0</td>
              <td class="text-bold totalPajakDokter">0</td>
              <td class="text-bold totalJasaMedisNetto">0</td>
              <td></td>
            </tr>
          </tfoot>
        </table>

        <hr>

        <b><span class="label label-success" style="font-size:12px">PEMERIKSAAN PERIODE SEBELUMNYA</span></b>
        <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
          <thead>
            <tr>
              <th style="width:5%">No.</th>
              <th style="width:10%">Tanggal Pemeriksaan</th>
              <th style="width:10%">Kelompok Pasien</th>
              <th style="width:10%">Jumlah Pemeriksaan</th>
              <th style="width:10%">Jasa Medis</th>
              <th style="width:10%">Potongan RS</th>
              <th style="width:10%">Pajak (PPH 21)</th>
              <th style="width:10%">Jumlah</th>
              <th style="width:15%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $params = array(
                'idhonor' => $id,
                'status_periode' => 'Periode Sebelumnya',
                'idtipe_radiologi' => '1',
                'status_expertise' => '',
                'status_hold' => $statusHold,
              );
            ?>
            <?php $data = $this->model->getPendapatanRadiologi($params); ?>
            <?php foreach ($data as $index => $row) { ?>

              <?php $dataDetail = json_encode(
                array(
                  'title' => 'PENDAPATAN X-RAY / PERIODE SEBELUMNYA',
                  'status_hold' => $statusHold,
                  'list_id' => explode(',', $row->list_id)
                )
              ); ?>

              <tr>
                <td><?=$index+1?></td>
                <td><?=$row->tanggal_pemeriksaan?></td>
                <td><?=$row->namakelompok?></td>
                <td><?=number_format($row->jumlah_pemeriksaan)?></td>
                <td><?=number_format($row->jasamedis)?></td>
                <td><?=number_format($row->nominal_potongan_rs)?></td>
                <td><?=number_format($row->nominal_pajak_dokter)?></td>
                <td><?=number_format($row->jasamedis_netto)?></td>
                <td>
                  <div class="btn-group">
                    <a href="{base_url}thonor_dokter/detail_tindakan/<?= base64url_encode($dataDetail); ?>" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                    <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                    <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td align="center" colspan="4"><b>TOTAL</b></td>
              <td class="text-bold totalJasaMedis">0</td>
              <td class="text-bold totalPotonganRS">0</td>
              <td class="text-bold totalPajakDokter">0</td>
              <td class="text-bold totalJasaMedisNetto">0</td>
              <td></td>
            </tr>
          </tfoot>
        </table>

      </div>
    </div>

    <div class="block block-themed <?=($open_block ? '' : 'block-opt-hidden');?>">
      <div class="block-header bg-success" style="padding: 8px 20px;">
        <ul class="block-options">
          <li>
            <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
          </li>
        </ul>
        <h3 class="block-title">PENDAPATAN USG</h3>
      </div>
      <div class="block-content">

        <b><span class="label label-success" style="font-size:12px">PEMERIKSAAN USG</span></b>
        <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
          <thead>
            <tr>
              <th style="width:5%">No.</th>
              <th style="width:10%">Tanggal Pemeriksaan</th>
              <th style="width:10%">Kelompok Pasien</th>
              <th style="width:10%">Jumlah Pemeriksaan</th>
              <th style="width:10%">Jasa Medis</th>
              <th style="width:10%">Potongan RS</th>
              <th style="width:10%">Pajak (PPH 21)</th>
              <th style="width:10%">Jumlah</th>
              <th style="width:15%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $params = array(
                'idhonor' => $id,
                'status_periode' => 'Periode Saat Ini',
                'idtipe_radiologi' => '2',
                'status_expertise' => '',
                'status_hold' => $statusHold,
              );
            ?>
            <?php $data = $this->model->getPendapatanRadiologi($params); ?>
            <?php foreach ($data as $index => $row) { ?>

              <?php $dataDetail = json_encode(
                array(
                  'title' => 'PENDAPATAN USG / PEMERIKSAAN USG',
                  'status_hold' => $statusHold,
                  'list_id' => explode(',', $row->list_id)
                )
              ); ?>

              <tr>
                <td><?=$index+1?></td>
                <td><?=$row->tanggal_pemeriksaan?></td>
                <td><?=$row->namakelompok?></td>
                <td><?=number_format($row->jumlah_pemeriksaan)?></td>
                <td><?=number_format($row->jasamedis)?></td>
                <td><?=number_format($row->nominal_potongan_rs)?></td>
                <td><?=number_format($row->nominal_pajak_dokter)?></td>
                <td><?=number_format($row->jasamedis_netto)?></td>
                <td>
                  <div class="btn-group">
                    <a href="{base_url}thonor_dokter/detail_tindakan/<?= base64url_encode($dataDetail); ?>" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                    <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                    <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td align="center" colspan="4"><b>TOTAL</b></td>
              <td class="text-bold totalJasaMedis">0</td>
              <td class="text-bold totalPotonganRS">0</td>
              <td class="text-bold totalPajakDokter">0</td>
              <td class="text-bold totalJasaMedisNetto">0</td>
              <td></td>
            </tr>
          </tfoot>
        </table>

        <hr>

        <b><span class="label label-success" style="font-size:12px">PEMERIKSAAN PERIODE SEBELUMNYA</span></b>
        <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
          <thead>
            <tr>
              <th style="width:5%">No.</th>
              <th style="width:10%">Tanggal Pemeriksaan</th>
              <th style="width:10%">Kelompok Pasien</th>
              <th style="width:10%">Jumlah Pemeriksaan</th>
              <th style="width:10%">Jasa Medis</th>
              <th style="width:10%">Potongan RS</th>
              <th style="width:10%">Pajak (PPH 21)</th>
              <th style="width:10%">Jumlah</th>
              <th style="width:15%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $params = array(
                'idhonor' => $id,
                'status_periode' => 'Periode Sebelumnya',
                'idtipe_radiologi' => '2',
                'status_expertise' => '',
                'status_hold' => $statusHold,
              );
            ?>
            <?php $data = $this->model->getPendapatanRadiologi($params); ?>
            <?php foreach ($data as $index => $row) { ?>

              <?php $dataDetail = json_encode(
                array(
                  'title' => 'PENDAPATAN USG / PERIODE SEBELUMNYA',
                  'status_hold' => $statusHold,
                  'list_id' => explode(',', $row->list_id)
                )
              ); ?>

              <tr>
                <td><?=$index+1?></td>
                <td><?=$row->tanggal_pemeriksaan?></td>
                <td><?=$row->namakelompok?></td>
                <td><?=number_format($row->jumlah_pemeriksaan)?></td>
                <td><?=number_format($row->jasamedis)?></td>
                <td><?=number_format($row->nominal_potongan_rs)?></td>
                <td><?=number_format($row->nominal_pajak_dokter)?></td>
                <td><?=number_format($row->jasamedis_netto)?></td>
                <td>
                  <div class="btn-group">
                    <a href="{base_url}thonor_dokter/detail_tindakan/<?= base64url_encode($dataDetail); ?>" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                    <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                    <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td align="center" colspan="4"><b>TOTAL</b></td>
              <td class="text-bold totalJasaMedis">0</td>
              <td class="text-bold totalPotonganRS">0</td>
              <td class="text-bold totalPajakDokter">0</td>
              <td class="text-bold totalJasaMedisNetto">0</td>
              <td></td>
            </tr>
          </tfoot>
        </table>

      </div>
    </div>

    <div class="block block-themed <?=($open_block ? '' : 'block-opt-hidden');?>">
      <div class="block-header bg-success" style="padding: 8px 20px;">
        <ul class="block-options">
          <li>
            <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
          </li>
        </ul>
        <h3 class="block-title">PENDAPATAN CT-SCAN</h3>
      </div>
      <div class="block-content">

        <b><span class="label label-success" style="font-size:12px">PEMERIKSAAN CT-SCAN</span></b>
        <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
          <thead>
            <tr>
              <th style="width:5%">No.</th>
              <th style="width:10%">Tanggal Pemeriksaan</th>
              <th style="width:10%">Kelompok Pasien</th>
              <th style="width:10%">Jumlah Pemeriksaan</th>
              <th style="width:10%">Jasa Medis</th>
              <th style="width:10%">Potongan RS</th>
              <th style="width:10%">Pajak (PPH 21)</th>
              <th style="width:10%">Jumlah</th>
              <th style="width:15%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $params = array(
                'idhonor' => $id,
                'status_periode' => 'Periode Saat Ini',
                'idtipe_radiologi' => '3',
                'status_expertise' => '',
                'status_hold' => $statusHold,
              );
            ?>
            <?php $data = $this->model->getPendapatanRadiologi($params); ?>
            <?php foreach ($data as $index => $row) { ?>

              <?php $dataDetail = json_encode(
                array(
                  'title' => 'PENDAPATAN CT-SCAN / PEMERIKSAAN CT-SCAN',
                  'status_hold' => $statusHold,
                  'list_id' => explode(',', $row->list_id)
                )
              ); ?>

              <tr>
                <td><?=$index+1?></td>
                <td><?=$row->tanggal_pemeriksaan?></td>
                <td><?=$row->namakelompok?></td>
                <td><?=number_format($row->jumlah_pemeriksaan)?></td>
                <td><?=number_format($row->jasamedis)?></td>
                <td><?=number_format($row->nominal_potongan_rs)?></td>
                <td><?=number_format($row->nominal_pajak_dokter)?></td>
                <td><?=number_format($row->jasamedis_netto)?></td>
                <td>
                  <div class="btn-group">
                    <a href="{base_url}thonor_dokter/detail_tindakan/<?= base64url_encode($dataDetail); ?>" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                    <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                    <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td align="center" colspan="4"><b>TOTAL</b></td>
              <td class="text-bold totalJasaMedis">0</td>
              <td class="text-bold totalPotonganRS">0</td>
              <td class="text-bold totalPajakDokter">0</td>
              <td class="text-bold totalJasaMedisNetto">0</td>
              <td></td>
            </tr>
          </tfoot>
        </table>

        <hr>

        <b><span class="label label-success" style="font-size:12px">PEMERIKSAAN PERIODE SEBELUMNYA</span></b>
        <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
          <thead>
            <tr>
              <th style="width:5%">No.</th>
              <th style="width:10%">Tanggal Pemeriksaan</th>
              <th style="width:10%">Kelompok Pasien</th>
              <th style="width:10%">Jumlah Pemeriksaan</th>
              <th style="width:10%">Jasa Medis</th>
              <th style="width:10%">Potongan RS</th>
              <th style="width:10%">Pajak (PPH 21)</th>
              <th style="width:10%">Jumlah</th>
              <th style="width:15%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $params = array(
                'idhonor' => $id,
                'status_periode' => 'Periode Sebelumnya',
                'idtipe_radiologi' => '3',
                'status_expertise' => '',
                'status_hold' => $statusHold,
              );
            ?>
            <?php $data = $this->model->getPendapatanRadiologi($params); ?>
            <?php foreach ($data as $index => $row) { ?>

              <?php $dataDetail = json_encode(
                array(
                  'title' => 'PENDAPATAN CT-SCAN / PERIODE SEBELUMNYA',
                  'status_hold' => $statusHold,
                  'list_id' => explode(',', $row->list_id)
                )
              ); ?>

              <tr>
                <td><?=$index+1?></td>
                <td><?=$row->tanggal_pemeriksaan?></td>
                <td><?=$row->namakelompok?></td>
                <td><?=number_format($row->jumlah_pemeriksaan)?></td>
                <td><?=number_format($row->jasamedis)?></td>
                <td><?=number_format($row->nominal_potongan_rs)?></td>
                <td><?=number_format($row->nominal_pajak_dokter)?></td>
                <td><?=number_format($row->jasamedis_netto)?></td>
                <td>
                  <div class="btn-group">
                    <a href="{base_url}thonor_dokter/detail_tindakan/<?= base64url_encode($dataDetail); ?>" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                    <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                    <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td align="center" colspan="4"><b>TOTAL</b></td>
              <td class="text-bold totalJasaMedis">0</td>
              <td class="text-bold totalPotonganRS">0</td>
              <td class="text-bold totalPajakDokter">0</td>
              <td class="text-bold totalJasaMedisNetto">0</td>
              <td></td>
            </tr>
          </tfoot>
        </table>

      </div>
    </div>

    <div class="block block-themed <?=($open_block ? '' : 'block-opt-hidden');?>">
      <div class="block-header bg-success" style="padding: 8px 20px;">
        <ul class="block-options">
          <li>
            <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
          </li>
        </ul>
        <h3 class="block-title">PENDAPATAN MRI</h3>
      </div>
      <div class="block-content">

        <b><span class="label label-success" style="font-size:12px">PEMERIKSAAN MRI</span></b>
        <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
          <thead>
            <tr>
              <th style="width:5%">No.</th>
              <th style="width:10%">Tanggal Pemeriksaan</th>
              <th style="width:10%">Kelompok Pasien</th>
              <th style="width:10%">Jumlah Pemeriksaan</th>
              <th style="width:10%">Jasa Medis</th>
              <th style="width:10%">Potongan RS</th>
              <th style="width:10%">Pajak (PPH 21)</th>
              <th style="width:10%">Jumlah</th>
              <th style="width:15%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $params = array(
                'idhonor' => $id,
                'status_periode' => 'Periode Saat Ini',
                'idtipe_radiologi' => '4',
                'status_expertise' => '',
                'status_hold' => $statusHold,
              );
            ?>
            <?php $data = $this->model->getPendapatanRadiologi($params); ?>
            <?php foreach ($data as $index => $row) { ?>

              <?php $dataDetail = json_encode(
                array(
                  'title' => 'PENDAPATAN MRI / PEMERIKSAAN MRI',
                  'status_hold' => $statusHold,
                  'list_id' => explode(',', $row->list_id)
                )
              ); ?>

              <tr>
                <td><?=$index+1?></td>
                <td><?=$row->tanggal_pemeriksaan?></td>
                <td><?=$row->namakelompok?></td>
                <td><?=number_format($row->jumlah_pemeriksaan)?></td>
                <td><?=number_format($row->jasamedis)?></td>
                <td><?=number_format($row->nominal_potongan_rs)?></td>
                <td><?=number_format($row->nominal_pajak_dokter)?></td>
                <td><?=number_format($row->jasamedis_netto)?></td>
                <td>
                  <div class="btn-group">
                    <a href="{base_url}thonor_dokter/detail_tindakan/<?= base64url_encode($dataDetail); ?>" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                    <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                    <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td align="center" colspan="4"><b>TOTAL</b></td>
              <td class="text-bold totalJasaMedis">0</td>
              <td class="text-bold totalPotonganRS">0</td>
              <td class="text-bold totalPajakDokter">0</td>
              <td class="text-bold totalJasaMedisNetto">0</td>
              <td></td>
            </tr>
          </tfoot>
        </table>

        <hr>

        <b><span class="label label-success" style="font-size:12px">PEMERIKSAAN PERIODE SEBELUMNYA</span></b>
        <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
          <thead>
            <tr>
              <th style="width:5%">No.</th>
              <th style="width:10%">Tanggal Pemeriksaan</th>
              <th style="width:10%">Kelompok Pasien</th>
              <th style="width:10%">Jumlah Pemeriksaan</th>
              <th style="width:10%">Jasa Medis</th>
              <th style="width:10%">Potongan RS</th>
              <th style="width:10%">Pajak (PPH 21)</th>
              <th style="width:10%">Jumlah</th>
              <th style="width:15%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $params = array(
                'idhonor' => $id,
                'status_periode' => 'Periode Sebelumnya',
                'idtipe_radiologi' => '4',
                'status_expertise' => '',
                'status_hold' => $statusHold,
              );
            ?>
            <?php $data = $this->model->getPendapatanRadiologi($params); ?>
            <?php foreach ($data as $index => $row) { ?>

              <?php $dataDetail = json_encode(
                array(
                  'title' => 'PENDAPATAN MRI / PERIODE SEBELUMNYA',
                  'status_hold' => $statusHold,
                  'list_id' => explode(',', $row->list_id)
                )
              ); ?>

              <tr>
                <td><?=$index+1?></td>
                <td><?=$row->tanggal_pemeriksaan?></td>
                <td><?=$row->namakelompok?></td>
                <td><?=number_format($row->jumlah_pemeriksaan)?></td>
                <td><?=number_format($row->jasamedis)?></td>
                <td><?=number_format($row->nominal_potongan_rs)?></td>
                <td><?=number_format($row->nominal_pajak_dokter)?></td>
                <td><?=number_format($row->jasamedis_netto)?></td>
                <td>
                  <div class="btn-group">
                    <a href="{base_url}thonor_dokter/detail_tindakan/<?= base64url_encode($dataDetail); ?>" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                    <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                    <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td align="center" colspan="4"><b>TOTAL</b></td>
              <td class="text-bold totalJasaMedis">0</td>
              <td class="text-bold totalPotonganRS">0</td>
              <td class="text-bold totalPajakDokter">0</td>
              <td class="text-bold totalJasaMedisNetto">0</td>
              <td></td>
            </tr>
          </tfoot>
        </table>

      </div>
    </div>

    <div class="block block-themed <?=($open_block ? '' : 'block-opt-hidden');?>">
      <div class="block-header bg-success" style="padding: 8px 20px;">
        <ul class="block-options">
          <li>
            <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
          </li>
        </ul>
        <h3 class="block-title">PENDAPATAN BMD</h3>
      </div>
      <div class="block-content">

        <b><span class="label label-success" style="font-size:12px">PEMERIKSAAN BMD</span></b>
        <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
          <thead>
            <tr>
              <th style="width:5%">No.</th>
              <th style="width:10%">Tanggal Pemeriksaan</th>
              <th style="width:10%">Kelompok Pasien</th>
              <th style="width:10%">Jumlah Pemeriksaan</th>
              <th style="width:10%">Jasa Medis</th>
              <th style="width:10%">Potongan RS</th>
              <th style="width:10%">Pajak (PPH 21)</th>
              <th style="width:10%">Jumlah</th>
              <th style="width:15%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $params = array(
                'idhonor' => $id,
                'status_periode' => 'Periode Saat Ini',
                'idtipe_radiologi' => '5',
                'status_expertise' => '',
                'status_hold' => $statusHold,
              );
            ?>
            <?php $data = $this->model->getPendapatanRadiologi($params); ?>
            <?php foreach ($data as $index => $row) { ?>

              <?php $dataDetail = json_encode(
                array(
                  'title' => 'PENDAPATAN BMD / PEMERIKSAAN BMD',
                  'status_hold' => $statusHold,
                  'list_id' => explode(',', $row->list_id)
                )
              ); ?>

              <tr>
                <td><?=$index+1?></td>
                <td><?=$row->tanggal_pemeriksaan?></td>
                <td><?=$row->namakelompok?></td>
                <td><?=number_format($row->jumlah_pemeriksaan)?></td>
                <td><?=number_format($row->jasamedis)?></td>
                <td><?=number_format($row->nominal_potongan_rs)?></td>
                <td><?=number_format($row->nominal_pajak_dokter)?></td>
                <td><?=number_format($row->jasamedis_netto)?></td>
                <td>
                  <div class="btn-group">
                    <a href="{base_url}thonor_dokter/detail_tindakan/<?= base64url_encode($dataDetail); ?>" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                    <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                    <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td align="center" colspan="4"><b>TOTAL</b></td>
              <td class="text-bold totalJasaMedis">0</td>
              <td class="text-bold totalPotonganRS">0</td>
              <td class="text-bold totalPajakDokter">0</td>
              <td class="text-bold totalJasaMedisNetto">0</td>
              <td></td>
            </tr>
          </tfoot>
        </table>

        <hr>

        <b><span class="label label-success" style="font-size:12px">PEMERIKSAAN PERIODE SEBELUMNYA</span></b>
        <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
          <thead>
            <tr>
              <th style="width:5%">No.</th>
              <th style="width:10%">Tanggal Pemeriksaan</th>
              <th style="width:10%">Kelompok Pasien</th>
              <th style="width:10%">Jumlah Pemeriksaan</th>
              <th style="width:10%">Jasa Medis</th>
              <th style="width:10%">Potongan RS</th>
              <th style="width:10%">Pajak (PPH 21)</th>
              <th style="width:10%">Jumlah</th>
              <th style="width:15%">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $params = array(
                'idhonor' => $id,
                'status_periode' => 'Periode Sebelumnya',
                'idtipe_radiologi' => '5',
                'status_expertise' => '',
                'status_hold' => $statusHold,
              );
            ?>
            <?php $data = $this->model->getPendapatanRadiologi($params); ?>
            <?php foreach ($data as $index => $row) { ?>

              <?php $dataDetail = json_encode(
                array(
                  'title' => 'PENDAPATAN BMD / PERIODE SEBELUMNYA',
                  'status_hold' => $statusHold,
                  'list_id' => explode(',', $row->list_id)
                )
              ); ?>

              <tr>
                <td><?=$index+1?></td>
                <td><?=$row->tanggal_pemeriksaan?></td>
                <td><?=$row->namakelompok?></td>
                <td><?=number_format($row->jumlah_pemeriksaan)?></td>
                <td><?=number_format($row->jasamedis)?></td>
                <td><?=number_format($row->nominal_potongan_rs)?></td>
                <td><?=number_format($row->nominal_pajak_dokter)?></td>
                <td><?=number_format($row->jasamedis_netto)?></td>
                <td>
                  <div class="btn-group">
                    <a href="{base_url}thonor_dokter/detail_tindakan/<?= base64url_encode($dataDetail); ?>" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                    <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                    <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td align="center" colspan="4"><b>TOTAL</b></td>
              <td class="text-bold totalJasaMedis">0</td>
              <td class="text-bold totalPotonganRS">0</td>
              <td class="text-bold totalPajakDokter">0</td>
              <td class="text-bold totalJasaMedisNetto">0</td>
              <td></td>
            </tr>
          </tfoot>
        </table>

      </div>
    </div>

  </div>
</div>
