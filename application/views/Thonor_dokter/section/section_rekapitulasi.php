<table class="table table-bordered table-striped" style="margin-top: 10px;">
  <thead>
    <tr>
      <th style="width:10%">Deskripsi</th>
      <th style="width:10%">Nominal</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>PENDAPATAN BRUTO</td>
      <td>{total_pendapatan_brutto}</td>
    </tr>
    <tr>
      <td>TOTAL POTONGAN RUMAH SAKIT</td>
      <td>{total_potongan_rs}</td>
    </tr>
    <tr>
      <td>BEBAN JASA DOKTER</td>
      <td>{total_beban_jasa_dokter}</td>
    </tr>
    <tr>
      <td>TOTAL PAJAK</td>
      <td>{total_pajak_dokter}</td>
    </tr>
    <tr>
      <td>TOTAL POTONGAN PRIBADI</td>
      <td>{total_potongan_pribadi}</td>
    </tr>
    <tr>
      <td>TOTAL POTONGAN PERUJUK</td>
      <td>{total_potongan_perujuk}</td>
    </tr>
    <tr>
      <td>TOTAL PENDAPATAN NETTO</td>
      <td>{total_pendapatan_netto}</td>
    </tr>
  </tbody>
</table>
