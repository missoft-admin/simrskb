<div class="block block-themed <?=($open_block ? '' : 'block-opt-hidden');?>">
  <div class="block-header <?=($statusHold ? 'bg-warning':'bg-primary')?>" style="padding: 10px 20px;">
    <ul class="block-options">
      <li>
        <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
      </li>
    </ul>
    <h3 class="block-title">PENDAPATAN NON PELAYANAN</h3>
  </div>
  <div class="block-content" style="padding: 5px 0 5px 0;">

    <br>

    <button data-toggle="modal" data-target="#EditPendapatanNonPelayananModal" class="btn btn-success addPendapatanLainLain" style="float:right;"><i class="fa fa-plus"></i> Tambah</button>
    <br><br>

    <table class="table table-bordered table-striped tablePendapatan" data-start="3" style="margin-top: 10px;">
      <thead>
        <tr>
          <th style="width:5%">No.</th>
          <th style="width:10%">Tanggal</th>
          <th style="width:10%">Deskripsi Pendapatan</th>
          <th style="width:10%">Jasa Medis</th>
          <th style="width:10%">Potongan RS</th>
          <th style="width:10%">Pajak (PPH 21)</th>
          <th style="width:10%">Jumlah</th>
          <th style="width:10%">Status</th>
          <th style="width:15%">Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php
          $params = array(
            'idhonor' => $id,
            'status_hold' => $statusHold,
          );
        ?>
        <?php $data = $this->model->getPendapatanNonPelayanan($params); ?>
        <?php foreach ($data as $index => $row) { ?>
          <?php
            if ($row->status_pendapatan == 1) {
              $status_pendapatan = '<label class="label label-md label-success">Rutin</label>';
              if ($row->status_periode == 1) {
                $status_periode = 'Setiap Tanggal';
              } else {
                $status_periode = 'Setiap Bulan';
              }
            } else {
              $status_pendapatan = '<label class="label label-md label-warning">Non Rutin</label><br>';
              $status_periode = DMYFormat($row->tanggal_pembayaran);
            }
          ?>
          <tr>
            <td><?=$index+1?> <?= '(<label style="color: red;">' . $row->id . '</label>)'; ?></td>
            <td><?=$row->tanggal_pemeriksaan?></td>
            <td><?=$row->namatarif?></td>
            <td><?=number_format($row->jasamedis)?></td>
            <td><?=number_format($row->nominal_potongan_rs)?></td>
            <td><?=number_format($row->nominal_pajak_dokter)?></td>
            <td><?=number_format($row->jasamedis_netto)?></td>
            <td><?=$status_pendapatan?></td>
            <td>
              <div class="btn-group">
                <button type="button" class="btn btn-sm btn-danger deletePendapatanNonPelayanan" data-idrow="<?=$row->idtransaksi?>"><i class="fa fa-trash"></i></button>
                <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-success btn-sm"><i class="fa fa-print"></i></a>
              </div>
            </td>
          </tr>
        <?php } ?>
      </tbody>
      <tfoot>
        <tr>
          <td align="center" colspan="3"><b>TOTAL</b></td>
          <td class="text-bold totalJasaMedis">0</td>
          <td class="text-bold totalPotonganRS">0</td>
          <td class="text-bold totalPajakDokter">0</td>
          <td class="text-bold totalJasaMedisNetto">0</td>
          <td></td>
          <td></td>
        </tr>
      </tfoot>
    </table>

  </div>
</div>

<?php if ($statusHold == 0) { ?>
  <?php $this->load->view('Tpendapatan_nonpelayanan/modal'); ?>

  <script type="text/javascript">
    var iddokter = "<?=$iddokter?>";
    loadPeriodePembayaranHonorDokter(iddokter);

    $('#tPendapatanNonPelayananIdDokter').val(iddokter);
    $('.formDokter').hide();
  </script>
<?php } ?>
