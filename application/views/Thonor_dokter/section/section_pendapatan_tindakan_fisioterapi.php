<div class="block block-themed <?=($open_block ? '' : 'block-opt-hidden');?>">
  <div class="block-header <?=($statusHold ? 'bg-warning':'bg-primary')?>" style="padding: 10px 20px;">
    <ul class="block-options">
      <li>
        <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
      </li>
    </ul>
    <h3 class="block-title">PENDAPATAN TINDAKAN FISIOTERAPI</h3>
  </div>
  <div class="block-content" style="padding: 5px 0 5px 0;">

    <br>

    <b><span class="label label-success" style="font-size:12px">TINDAKAN FISIOTERAPI</span></b>
    <table class="table table-bordered table-striped tablePendapatan" data-start="5" style="margin-top: 10px;">
      <thead>
        <tr>
          <th style="width:5%">No.</th>
          <th style="width:10%">Tanggal Pemeriksaan</th>
          <th style="width:10%">Nama Pasien</th>
          <th style="width:10%">Kelompok Pasien</th>
          <th style="width:10%">Nama Pemeriksaan</th>
          <th style="width:10%">Jasa Medis</th>
          <th style="width:10%">Potongan RS</th>
          <th style="width:10%">Pajak (PPH 21)</th>
          <th style="width:10%">Jumlah</th>
          <th style="width:15%">Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php
          $params = array(
            'idhonor' => $id,
            'status_periode' => 'Periode Saat Ini',
            'status_hold' => $statusHold,
          );
        ?>
        <?php $data = $this->model->getPendapatanFisioterapi($params); ?>
        <?php foreach ($data as $index => $row) { ?>

          <?php
            $dataModal = array(
              'table' => $row->reference_table,
              'idrawatinap' => $row->idtransaksi,
              'iddetail' => $row->iddetail,
              'idtarif' => $row->idtarif,
              'namatarif' => $row->namatarif,
              'iddokter' => $row->iddokter,
              'namadokter' => $row->namadokter,
              'jasamedis' => $row->jasamedis,
              'potongan_rs' => $row->potongan_rs,
              'pajak_dokter' => $row->pajak_dokter,
              'nominal_potongan_rs' => $row->nominal_potongan_rs,
              'nominal_pajak_dokter' => $row->nominal_pajak_dokter,
              'jasamedis_netto' => $row->jasamedis_netto,
              'periode_pembayaran' => DMYFormat($row->tanggal_pembayaran),
              'periode_jatuhtempo' => DMYFormat($row->tanggal_jatuhtempo),
              'status_expertise' => $row->status_expertise,
              'status_verifikasi' => 1,
            );
          ?>

          <tr>
            <td><?=$index+1?> <?= '(<label style="color: red;">' . $row->id . '</label>)'; ?></td>
            <td><?=$row->tanggal_pemeriksaan?></td>
            <td><?=$row->namapasien?></td>
            <td><?=$row->namakelompok?></td>
            <td><?=$row->namatarif?></td>
            <td><?=number_format($row->jasamedis)?></td>
            <td><?=number_format($row->nominal_potongan_rs)?></td>
            <td><?=number_format($row->nominal_pajak_dokter)?></td>
            <td><?=number_format($row->jasamedis_netto)?></td>
            <td>
              <div class="btn-group">
                <button class="btn btn-success btn-sm editJasaMedis" title="Edit Tindakan" data-toggle="modal" data-target="#EditJasaMedisModal" data-item="<?= encodeURIComponent(json_encode($dataModal)); ?>"><i class="fa fa-pencil"></i></button>
                <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-primary btn-sm"><i class="fa fa-print"></i></a>
              </div>
            </td>
          </tr>
        <?php } ?>
      </tbody>
      <tfoot>
        <tr>
          <td align="center" colspan="5"><b>TOTAL</b></td>
          <td class="text-bold totalJasaMedis">0</td>
          <td class="text-bold totalPotonganRS">0</td>
          <td class="text-bold totalPajakDokter">0</td>
          <td class="text-bold totalJasaMedisNetto">0</td>
          <td></td>
        </tr>
      </tfoot>
    </table>

    <hr>

    <b><span class="label label-success" style="font-size:12px">PEMERIKSAAN PERIODE SEBELUMNYA</span></b>
    <table class="table table-bordered table-striped tablePendapatan" data-start="5" style="margin-top: 10px;">
      <thead>
        <tr>
          <th style="width:5%">No.</th>
          <th style="width:10%">Tanggal Pemeriksaan</th>
          <th style="width:10%">Nama Pasien</th>
          <th style="width:10%">Kelompok Pasien</th>
          <th style="width:10%">Nama Pemeriksaan</th>
          <th style="width:10%">Jasa Medis</th>
          <th style="width:10%">Potongan RS</th>
          <th style="width:10%">Pajak (PPH 21)</th>
          <th style="width:10%">Jumlah</th>
          <th style="width:15%">Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php
          $params = array(
            'idhonor' => $id,
            'status_periode' => 'Periode Sebelumnya',
            'status_hold' => $statusHold,
          );
        ?>
        <?php $data = $this->model->getPendapatanFisioterapi($params); ?>
        <?php foreach ($data as $index => $row) { ?>

          <?php
            $dataModal = array(
              'table' => $row->reference_table,
              'idrawatinap' => $row->idtransaksi,
              'iddetail' => $row->iddetail,
              'idtarif' => $row->idtarif,
              'namatarif' => $row->namatarif,
              'iddokter' => $row->iddokter,
              'namadokter' => $row->namadokter,
              'jasamedis' => $row->jasamedis,
              'potongan_rs' => $row->potongan_rs,
              'pajak_dokter' => $row->pajak_dokter,
              'nominal_potongan_rs' => $row->nominal_potongan_rs,
              'nominal_pajak_dokter' => $row->nominal_pajak_dokter,
              'jasamedis_netto' => $row->jasamedis_netto,
              'periode_pembayaran' => DMYFormat($row->tanggal_pembayaran),
              'periode_jatuhtempo' => DMYFormat($row->tanggal_jatuhtempo),
              'status_expertise' => $row->status_expertise,
              'status_verifikasi' => 1,
            );
          ?>
          
          <tr>
            <td><?=$index+1?> <?= '(<label style="color: red;">' . $row->id . '</label>)'; ?></td>
            <td><?=$row->tanggal_pemeriksaan?></td>
            <td><?=$row->namapasien?></td>
            <td><?=$row->namakelompok?></td>
            <td><?=$row->namatarif?></td>
            <td><?=number_format($row->jasamedis)?></td>
            <td><?=number_format($row->nominal_potongan_rs)?></td>
            <td><?=number_format($row->nominal_pajak_dokter)?></td>
            <td><?=number_format($row->jasamedis_netto)?></td>
            <td>
              <div class="btn-group">
                <a href="{base_url}thonor_dokter/index" title="Rincian Tindakan Dokter" class="btn btn-primary btn-sm"><i class="fa fa-list"></i></a>
                <a href="{base_url}thonor_dokter/index" title="Perubahan Periode Pembayaran" class="btn btn-danger btn-sm"><i class="fa fa-arrow-right"></i></a>
                <a href="{base_url}thonor_dokter/print" title="Print" class="btn btn-primary btn-sm"><i class="fa fa-print"></i></a>
              </div>
            </td>
          </tr>
        <?php } ?>
      </tbody>
      <tfoot>
        <tr>
          <td align="center" colspan="3"><b>TOTAL</b></td>
          <td class="text-bold totalJasaMedis">0</td>
          <td class="text-bold totalPotonganRS">0</td>
          <td class="text-bold totalPajakDokter">0</td>
          <td class="text-bold totalJasaMedisNetto">0</td>
          <td></td>
        </tr>
      </tfoot>
    </table>

  </div>
</div>
