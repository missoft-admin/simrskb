<div class="modal fade in black-overlay" id="EditPendapatanLainLainModal" role="dialog" aria-hidden="true"
	style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 50%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i
									class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">PENDAPATAN LAIN LAIN</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<input type="hidden" id="tPendapatanLainLainRowId" readonly value="">
							<div class="col-md-12">
								<div class="form-group">
									<label class="col-md-3 control-label">Tanggal</label>
									<div class="col-md-9">
										<input type="text" class="js-datepicker form-control"
											data-date-format="dd/mm/yyyy" id="tPendapatanLainLainTanggal"
											placeholder="Tanggal" value="<?=date("d/m/Y")?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Deskripsi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm"
											id="tPendapatanLainLainDeskripsi" placeholder="Deskripsi" value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Jasa Medis</label>
									<div class="col-md-9">
										<input type="text" class="form-control number input-sm"
											id="tPendapatanLainLainJasaMedis" placeholder="Jasa Medis" value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Potongan RS</label>
									<div class="col-md-9">
										<input type="text" class="form-control number input-sm"
											id="tPendapatanLainLainNominalPotonganRS" placeholder="Potongan RS"
											value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Pajak Dokter</label>
									<div class="col-md-9">
										<input type="text" class="form-control number input-sm"
											id="tPendapatanLainLainNominalPajakDokter" placeholder="Pajak Dokter"
											value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Jasa Medis Netto</label>
									<div class="col-md-9">
										<input type="text" class="form-control number input-sm"
											id="tPendapatanLainLainJasaMedisNetto" placeholder="Jasa Medis Netto"
											readonly value="">
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-primary" id="saveDataPendapatanLainLain" type="button"
					data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
				<button class="btn btn-sm btn-default" id="cancelDataPendapatanLainLain" type="button"
					data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$('.number').number(true, 0, '.', ',');

		$(document).on('keyup',
			'#tPendapatanLainLainJasaMedis, #tPendapatanLainLainNominalPotonganRS, #tPendapatanLainLainNominalPajakDokter',
			function () {
				var jasamedis = parseFloat($("#tPendapatanLainLainJasaMedis").val());
				var nominal_potongan_rs = parseFloat($("#tPendapatanLainLainNominalPotonganRS").val());
				var nominal_pajak_dokter = parseFloat($("#tPendapatanLainLainNominalPajakDokter").val());
				var jasamedis_netto = (jasamedis ? jasamedis : 0) - (nominal_potongan_rs ?
					nominal_potongan_rs : 0) - (nominal_pajak_dokter ? nominal_pajak_dokter : 0);
				$("#tPendapatanLainLainJasaMedisNetto").val(jasamedis_netto);
			});

		$(document).on('click', '.addPendapatanLainLain', function () {
			$("#tPendapatanLainLainRowId").val('');
			$("#tPendapatanLainLainJasaMedis").val('');
			$("#tPendapatanLainLainNominalPotonganRS").val('');
			$("#tPendapatanLainLainNominalPajakDokter").val('');
			$("#tPendapatanLainLainJasaMedisNetto").val('');
		});

		$(document).on('click', '.editPendapatanLainLain', function () {
			var idrow = $(this).data('idrow');
			var tanggal, deskrpsi, jasamedis, nominal_potongan_rs, nominal_pajak_dokter, jasamedis_netto;

			tanggal = $(this).closest('tr').find("td:eq(1)").html();
			deskripsi = $(this).closest('tr').find("td:eq(2)").html();
			jasamedis = $(this).closest('tr').find("td:eq(3)").html();
			nominal_potongan_rs = $(this).closest('tr').find("td:eq(4)").html();
			nominal_pajak_dokter = $(this).closest('tr').find("td:eq(5)").html();
			jasamedis_netto = $(this).closest('tr').find("td:eq(6)").html();

			$("#tPendapatanLainLainRowId").val(idrow);
			$("#tPendapatanLainLainTanggal").val(tanggal);
			$("#tPendapatanLainLainDeskripsi").val(deskripsi);
			$("#tPendapatanLainLainJasaMedis").val(jasamedis);
			$("#tPendapatanLainLainNominalPotonganRS").val(nominal_potongan_rs);
			$("#tPendapatanLainLainNominalPajakDokter").val(nominal_pajak_dokter);
			$("#tPendapatanLainLainJasaMedisNetto").val(jasamedis_netto);
		});

		$(document).on('click', '.deletePendapatanLainLain', function () {
			var idrow = $(this).data('idrow');

			event.preventDefault();
			swal({
				title: "Apakah anda yakin ingin menghapus data ini?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya",
				cancelButtonText: "Tidak",
				closeOnConfirm: false,
				closeOnCancel: false
			}).then(function () {
				$.ajax({
					url: '{site_url}thonor_dokter/removeDataPendapatanLainLain',
					method: 'POST',
					data: {
						idrow: idrow
					},
					success: function (data) {
						swal({
							title: "Berhasil!",
							text: "Data Pendapatan Lain Lain Telah Dihapus.",
							type: "success",
							timer: 1500,
							showConfirmButton: false
						});

						location.reload();
					}
				});
			});
		});

		$(document).on('click', '#saveDataPendapatanLainLain', function () {
			var idrow = $('#tPendapatanLainLainRowId').val();
			var namatarif = $('#tPendapatanLainLainDeskripsi').val();
			var jasamedis = $('#tPendapatanLainLainJasaMedis').val();
			var nominal_potongan_rs = $('#tPendapatanLainLainNominalPotonganRS').val();
			var nominal_pajak_dokter = $('#tPendapatanLainLainNominalPajakDokter').val();
			var jasamedis_netto = $('#tPendapatanLainLainJasaMedisNetto').val();
			var tanggal_pemeriksaan = $('#tPendapatanLainLainTanggal').val();

			if (namatarif == '') {
				sweetAlert("Maaf...", "Deskripsi tidak boleh kosong !", "error").then((value) => {
					$('#tPendapatanLainLainDeskripsi').focus();
				});
				return false;
			}

			if (jasamedis == '') {
				sweetAlert("Maaf...", "Jasa Medis tidak boleh kosong !", "error").then((value) => {
					$("#tPendapatanLainLainJasaMedis").select2('open');
				});
				return false;
			}

			if (nominal_potongan_rs == '') {
				sweetAlert("Maaf...", "Nominal Potongan RS tidak boleh kosong !", "error").then((
				value) => {
					$("#tPendapatanLainLainNominalPotonganRS").select2('open');
				});
				return false;
			}

			if (nominal_pajak_dokter == '') {
				sweetAlert("Maaf...", "Nominal Pajak Dokter tidak boleh kosong !", "error").then((
				value) => {
					$("#tPendapatanLainLainNominalPajakDokter").select2('open');
				});
				return false;
			}

			$.ajax({
				url: '{site_url}thonor_dokter/saveDataPendapatanLainLain',
				method: 'POST',
				data: {
					idrow: idrow,
					idhonor: '{id}',
					jenis_transaksi: 'pendapatan',
					jenis_tindakan: 'PENDAPATAN LAIN LAIN',
					namatarif: namatarif,
					iddokter: '{iddokter}',
					namadokter: '{namadokter}',
					jasamedis: jasamedis,
					potongan_rs: 0,
					nominal_potongan_rs: nominal_potongan_rs,
					pajak_dokter: 0,
					nominal_pajak_dokter: nominal_pajak_dokter,
					jasamedis_netto: jasamedis_netto,
					tanggal_pemeriksaan: tanggal_pemeriksaan,
					tanggal_pembayaran: '{tanggal_pembayaran}',
					tanggal_jatuhtempo: '{tanggal_jatuhtempo}'
				},
				success: function (data) {
					swal({
						title: "Berhasil!",
						text: "Data Pendapatan Lain Lain Telah Tersimpan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					location.reload();
				}
			});
		});
	});
</script>