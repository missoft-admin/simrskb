<div class="modal fade in black-overlay" id="RincianPerujukModal" role="dialog" aria-hidden="true"
  style="z-index: 1041;">
  <div class="modal-dialog modal-lg modal-dialog-popout" style="width: 80%;">
    <div class="modal-content">
      <div class="block block-themed">
        <div class="block-header bg-success">
          <ul class="block-options">
            <li>
              <button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
            </li>
          </ul>
          <h3 class="block-title">RINCIAN PERUJUK</h3>
        </div>
        <div class="block-content">

          <b><span class="label label-success" style="font-size:12px">JASA DOKTER VISITE</span></b>
		  <div class="table-responsive">
          <table class="table table-bordered table-striped" style="margin-top: 10px;" id="tableRincianJasaDokterVisite">
            <thead>
              <tr>
                <th style="width:5%">Tanggal</th>
                <th style="width:10%">Tindakan</th>
                <th style="width:10%">Dokter</th>
                <th style="width:5%">Jasa Medis</th>
                <th style="width:5%">Persentase Perujuk</th>
                <th style="width:5%">Nominal Perujuk</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
		</div>
          <b><span class="label label-success" style="font-size:12px">JASA DOKTER OPERATOR</span></b>
		  <div class="table-responsive">
          <table class="table table-bordered table-striped" style="margin-top: 10px;" id="tableRincianJasaDokterOperator">
            <thead>
              <tr>
                <th style="width:5%">Tanggal</th>
                <th style="width:10%">Tindakan</th>
                <th style="width:10%">Dokter</th>
                <th style="width:5%">Jasa Medis</th>
                <th style="width:5%">Persentase Perujuk</th>
                <th style="width:5%">Nominal Perujuk</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>

        </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
  $(document).ready(function () {
    $(document).on('click', '.showDetailPerujuk', function () {
      var idtransaksi = $(this).data('idtransaksi');

      $("#tableRincianJasaDokterVisite tbody").html('');
      $("#tableRincianJasaDokterOperator tbody").html('');

      $.ajax({
        url: '{site_url}thonor_dokter/getRincianPerujuk/' + idtransaksi,
        dataType: "json",
        success: function (data) {

          data.map((item) => {
            if (item.jenis_tindakan == "JASA DOKTER VISITE") {
              $("#tableRincianJasaDokterVisite tbody").append(`
                <tr>
                  <td>${item.tanggal_pemeriksaan}</td>
                  <td>${item.namatarif}</td>
                  <td>${item.namadokter}</td>
                  <td>${$.number(item.jasamedis)}</td>
                  <td>${$.number(item.potongan_perujuk)}</td>
                  <td>${$.number(item.nominal_potongan_perujuk)}</td>
                </tr>`);
            } else {
              $("#tableRincianJasaDokterOperator tbody").append(`
                <tr>
                  <td>${item.tanggal_pemeriksaan}</td>
                  <td>${item.namatarif}</td>
                  <td>${item.namadokter}</td>
                  <td>${$.number(item.jasamedis)}</td>
                  <td>${$.number(item.potongan_perujuk)}</td>
                  <td>${$.number(item.nominal_potongan_perujuk)}</td>
                </tr>`);
            }
          });

        }
      });
    });
  });
</script>