<div class="modal fade in black-overlay" id="PeriodePembayaranModal" role="dialog" aria-hidden="true"
    style="z-index: 1041;">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width: 50%;">
        <form class="form-horizontal" method="post" action="{base_url}thonor_dokter/next_periode">
            <div class="modal-content">
                <div class="block block-themed">
                    <div class="block-header bg-danger">
                        <ul class="block-options">
                            <li>
                                <button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
                            </li>
                        </ul>
                        <h3 class="block-title">Perubahan Tanggal Pembayaran</h3>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <input type="hidden" id="idHonor" name="idhonor" value="">
                            <input type="hidden" id="idDokter" name="iddokter" value="">
                            <input type="hidden" id="periodePembayaran" name="periode_pembayaran" value="">
                            <input type="hidden" id="periodeJatuhTempo" name="periode_jatuhtempo" value="">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Dokter</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" id="namaDokter" name="namadokter" readonly value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tanggal Asal</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" id="tanggalPembayaranAsal" name="periode_sebelumnya" readonly value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tanggal Perubahan</label>
                                    <div class="col-md-9">
                                        <select id="tanggalPembayaranPerubahan" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                            <option value="">Pilih Opsi</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check"></i> Simpan</button>
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', '.actionNextPeriode', function () {
            var idhonor = $(this).data('idhonor');
            var iddokter = $(this).data('iddokter');
            var namadokter = $(this).data('namadokter');
            var periodePembayaran = $(this).data('periodepembayaran');

            $("#idHonor").val(idhonor);
            $("#idDokter").val(iddokter);
            $("#namaDokter").val(namadokter);
            $("#tanggalPembayaranAsal").val(periodePembayaran);
            getPeriodePembayaran(iddokter, periodePembayaran);
        });

        $(document).on('change', '#tanggalPembayaranPerubahan', function () {
            setPeriodeHonor();
        });
    });

    function getPeriodePembayaran(iddokter, periodePembayaran) {
        $("#tanggalPembayaranPerubahan").empty();

        $.ajax({
            url: '{base_url}tverifikasi_transaksi/GetPeriodePembayaranHonorDokter/' + iddokter + '/' + periodePembayaran,
            dataType: "json",
            success: function (data) {
                $('#tanggalPembayaranPerubahan').append(data.list_option);
            },
            complete: function (data) {
                setPeriodeHonor();
            }
        });
    }

    function setPeriodeHonor() {
        var periodePembayaran = $('#tanggalPembayaranPerubahan option:selected').val();
        var periodeJatuhTempo = $('#tanggalPembayaranPerubahan option:selected').data('jatuhtempo');
        $("#periodePembayaran").val(periodePembayaran);
        $("#periodeJatuhTempo").val(periodeJatuhTempo);
    }
</script>