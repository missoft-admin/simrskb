<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>Tanggal Pemeriksaan</th>
					<th>No. Medrec</th>
					<th>Nama Pasien</th>
					<th>Tujuan</th>
					<th>Poliklinik</th>
					<th>Uraian / Jasa</th>
					<th>Jasa Medis</th>
					<th>Potongan Perujuk (%)</th>
					<th>Potongan RS (%)</th>
					<th>Pajak (%)</th>
					<th>Potongan Perujuk (Rp)</th>
					<th>Potongan RS (Rp)</th>
					<th>Pajak (Rp)</th>
					<th>Jasa Medis Netto</th>
					<th>Periode Pembayaran</th>
					<th>Status Hold</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"autoWidth": true,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"scrollX": true,
				"order": [],
				"ajax": {
					url: '{site_url}thonor_dokter/getDetailTindakan/{data}',
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": false },
					{ "width": "5%", "targets": 1, "orderable": false },
					{ "width": "5%", "targets": 2, "orderable": false },
					{ "width": "5%", "targets": 3, "orderable": false },
					{ "width": "5%", "targets": 4, "orderable": false },
					{ "width": "5%", "targets": 5, "orderable": false },
					{ "width": "5%", "targets": 6, "orderable": false },
					{ "width": "5%", "targets": 7, "orderable": false },
					{ "width": "5%", "targets": 8, "orderable": false },
					{ "width": "5%", "targets": 9, "orderable": false },
					{ "width": "5%", "targets": 10, "orderable": false },
					{ "width": "5%", "targets": 11, "orderable": false },
					{ "width": "5%", "targets": 12, "orderable": false },
					{ "width": "5%", "targets": 13, "orderable": false },
					{ "width": "5%", "targets": 14, "orderable": false },
					{ "width": "5%", "targets": 15, "orderable": false },
					{ "width": "5%", "targets": 16, "orderable": false },
					{ "width": "5%", "targets": 17, "orderable": false },
				]
			});
	});
</script>

<?php $this->load->view('Tverifikasi_transaksi/modal/modal_jasa_dokter'); ?>
<?php $this->load->view('Thonor_dokter/modal/modal_rincian_perujuk'); ?>
