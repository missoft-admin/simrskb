<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}thonor_dokter" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title} </h3>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-md-6">
                <div class="input-group" style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No. Transaksi</span>
                    <input type="text" class="form-control" value="{notransaksi}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Nama Dokter</span>
                    <input type="text" class="form-control" value="{namadokter}" readonly="true">
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group" style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Tanggal Pembayaran</span>
                    <input type="text" class="form-control" value="{tanggal_pembayaran}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Tanggal Jatuh Tempo</span>
                    <input type="text" class="form-control" value="{tanggal_jatuhtempo}" readonly="true">
                </div>
            </div>
        </div>

        <br>

        <div class="">
            <div class="row">
                <div class="col-md-12">
                    <form class="dropzone" action="{base_url}thonor_dokter/upload_files" method="post" enctype="multipart/form-data">
                        <input name="idhonor" type="hidden" value="{idhonor}">
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <a href="{base_url}thonor_dokter/upload_document/{idhonor}" class="btn btn-success"><i class="fa fa-refresh"></i> Reload List</a>
                    <br><br>
                </div>
                <div class="col-lg-12">
					<div class="table-responsive">
                    <table class="table table-bordered table-striped table-responsive" id="listFile">
                        <thead>
                            <tr>
                                <th width="1%">No</th>
                                <th width="20%">File</th>
                                <th width="5%">Size</th>
                                <th width="5%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($listFiles as $index => $row) { ?>
                            <tr>
                                <td><?=$index+1?></td>
                                <td><?=$row->filename?></td>
                                <td><?=$row->size?></td>
                                <td>
                                    <a href="{base_url}assets/upload/honor_dokter/<?=$row->filename?>" target="_blank" data-toggle="tooltip" title="Preview" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                    <a href="#" data-urlindex="{base_url}thonor_dokter/upload_document/{idhonor}" data-urlremove="{base_url}thonor_dokter/delete_file/<?=$row->id?>" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeData"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                            <? } ?>
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$('#listFile tbody').on('click', 'a.removeData', function() {
    urlIndex = $(this).data('urlindex');
    urlRemove = $(this).data('urlremove');
    warningText = ($(this).data('messagewarning') ? $(this).data('messagewarning') : 'Hapus record tersebut?');
    successText = ($(this).data('messagesuccess') ? $(this).data('messagesuccess') : 'Data telah dihapus.');
    setTimeout(function() {
        swal({
            text: warningText,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d26a5c',
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal',
            html: false,
            preConfirm: function() {
                return new Promise(function(resolve) {
                    setTimeout(function() {
                        resolve();
                    }, 100);
                })
            }
        }).then(
            function(result) {
                if (successText == 'Data telah dihapus.') {
                    $.ajax({
                        url: urlRemove
                    });
                    swal('Berhasil!', successText, 'success');
                    setTimeout(function() {
                        window.location = urlIndex;
                    }, 100);
                } else {
                    window.location = urlRemove;
                }
            }
        )
    }, 100);
});

</script>
