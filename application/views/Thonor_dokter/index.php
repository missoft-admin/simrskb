<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">

		<hr style="margin-top:0px">

		<div class="row">
			<?php echo form_open('thonor_dokter/filter','class="form-horizontal" id="form-work"') ?>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">No. Transaksi</label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="notransaksi" value="{notransaksi}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="example-daterange1">Kategori Dokter</label>
					<div class="col-md-8">
						<select name="idkategori_dokter[]" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
							<?php foreach (get_all('mdokter_kategori', []) as $row){?>
							<option value="<?= $row->id?>" <?=(in_array($row->id, $idkategori_dokter) ? 'selected="selected"':'')?>><?= $row->nama?></option>
							<?php }?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Dokter</label>
					<div class="col-md-8">
						<select name="iddokter" class="js-select2 form-control" style="width: 100%;"
							data-placeholder="Pilih Opsi">
							<option value="0">Semua Dokter</option>
							<?php foreach (get_all('mdokter', array('status' => 1)) as $row){?>
							<option value="<?= $row->id?>" <?=($iddokter == $row->id ? 'selected="selected"':'')?>>
								<?= $row->nama?></option>
							<?php }?>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="example-daterange1">Tanggal Pembayaran</label>
					<div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
							<input class="form-control" type="text" name="tanggalpembayaran_dari" placeholder="From"
								value="{tanggalpembayaran_dari}">
							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
							<input class="form-control" type="text" name="tanggalpembayaran_sampai" placeholder="To"
								value="{tanggalpembayaran_sampai}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="example-daterange1">Tanggal Jatuh Tempo</label>
					<div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
							<input class="form-control" type="text" name="tanggaljatuhtempo_dari" placeholder="From"
								value="{tanggaljatuhtempo_dari}">
							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
							<input class="form-control" type="text" name="tanggaljatuhtempo_sampai" placeholder="To"
								value="{tanggaljatuhtempo_sampai}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Status</label>
					<div class="col-md-8">
						<select name="status" class="js-select2 form-control" style="width: 100%;"
							data-placeholder="Pilih Opsi">
							<option value="#" <?=($status == '#' ? 'selected="selected"':'')?>>Semua Status</option>
							<option value="0" <?=($status == '0' ? 'selected="selected"':'')?>>Belum Dibayarkan</option>
							<option value="1" <?=($status == '1' ? 'selected="selected"':'')?>>Telah Dibayarkan</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-8">
						<div class="row">
							<div class="col-md-6">
								<button class="btn btn-success text-uppercase" type="submit" name="button"
									style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i>
									Filter</button>
							</div>
							<div class="col-md-6">
								<a href="{base_url}thonor_approval/create" class="btn btn-primary text-uppercase"
									type="button" name="button" style="font-size:13px;width:100%;float:right;"><i
										class="fa fa-paper-plane"></i> Create Approval</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>

		<hr style="margin-top:10px">

		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>No. Transaksi</th>
					<th>Dokter</th>
					<th>Tanggal Pembayaran</th>
					<th>Tanggal Jatuh Tempo</th>
					<th>Nominal</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_approval" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<div class="form-horizontal">
						<div class="row">
							<input type="hidden" class="form-control" id="id_setuju" placeholder="" name="id_setuju" value="">
							<div class="col-md-12">
								<table width="100%" id="tabel_user" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Step</th>
											<th>User</th>
											<th>Jika Setuju</th>
											<th>Jika Menolak</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-primary" id="btn_simpan_approval" type="button" data-dismiss="modal"><i
						class="fa fa-check"></i> Proses Persetujuan</button>
			</div>

		</div>
	</div>
</div>
<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function () {
		BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
			"autoWidth": false,
			"pageLength": 100,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"scrollX": true,
			"order": [],
			"ajax": {
				url: '{site_url}thonor_dokter/getIndex/' + '<?= $this->uri->segment(2); ?>',
				type: "POST",
				dataType: 'json'
			},
			"columnDefs": [{
					"width": "5%",
					"targets": 0,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 1,
					"orderable": true
				},
				{
					"width": "15%",
					"targets": 2,
					"orderable": true
				},
				{
					"width": "15%",
					"targets": 3,
					"orderable": true
				},
				{
					"width": "15%",
					"targets": 4,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 5,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 6,
					"orderable": true
				},
				{
					"width": "20%",
					"targets": 7,
					"orderable": true
				}
			]
		});
	});

	$(document).on("click", "#btn_simpan_approval", function () {
		var id = $("#id_setuju").val();
		swal({
			title: "Anda Yakin ?",
			text: "Akan Kirim Ke User Persetujuan?",
			type: "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function () {
			table = $('#datatable-simrs').DataTable()
			$.ajax({
				url: '{site_url}Thonor_approval/simpan_proses_peretujuan/' + id,
				complete: function (data) {
					$.toaster({
						priority: 'success',
						title: 'Succes!',
						message: ' Kirim Data'
					});
					table.ajax.reload(null, false);
				}
			});
		});
	});

	function kirim($id) {
		var id = $id;
		$("#modal_approval").modal('show');
		$('#btn_simpan_approval').attr('disabled', true);
		load_user_approval(id);
	}

	function load_user_approval($id) {
		var id = $id;
		$("#id_setuju").val(id);
		$('#tabel_user').DataTable().destroy();
		table = $('#tabel_user').DataTable({
			"autoWidth": false,
			"pageLength": 10,
			"searching": false,
			"lengthChange": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}thonor_dokter/load_user_approval',
				type: "POST",
				dataType: 'json',
				data: {
					id: id,
				}
			},
			"columnDefs": [{
					className: "text-right",
					targets: [0]
				},
				{
					className: "text-center",
					targets: [1, 2, 3]
				},
				{
					"width": "10%",
					"targets": [0]
				},
				{
					"width": "40%",
					"targets": [1]
				},
				{
					"width": "25%",
					"targets": [2, 3]
				},

			],
			"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				if (aData[1]) {
					$('#btn_simpan_approval').attr('disabled', false);
				}
			},

		});
	}

	function actionStopPeriode(obj) {
		event.preventDefault();

		var namaDokter = $(obj).data('namadokter');
		var periodePembayaran = $(obj).data('periodepembayaran');

		swal({
			title: "Stop Periode Honor Dokter?",
			text: namaDokter + " - " + periodePembayaran,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Ya",
			cancelButtonText: "Tidak, batalkan",
			closeOnConfirm: false,
			closeOnCancel: false
		}).then(function () {
			window.location = obj;
		});
	}

	function actionUnStopPeriode(obj) {
		event.preventDefault();

		var namaDokter = $(obj).data('namadokter');
		var periodePembayaran = $(obj).data('periodepembayaran');

		swal({
			title: "UnStop Periode Honor Dokter?",
			text: namaDokter + " - " + periodePembayaran,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Ya",
			cancelButtonText: "Tidak, batalkan",
			closeOnConfirm: false,
			closeOnCancel: false
		}).then(function () {
			window.location = obj;
		});
	}
</script>

<?php $this->load->view('Thonor_dokter/modal/modal_periode_pembayaran'); ?>