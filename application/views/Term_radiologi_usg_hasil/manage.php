<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>

<div class="block">
    <div class="block-content">
        <?php echo form_open('term_radiologi_usg_hasil/save', 'id="form-work"') ?>
        <div class="row">
            <div class="col-md-3">
                <h4>UPLOAD HASIL PEMERIKSAAN</h4> <br>
            </div>
            <div class="col-md-9 text-right">
                <button type="submit" class="btn btn-primary" name="form_submit" value="form-submit"><i class="fa fa-save"></i> Simpan</button>
                <a href="{site_url}term_radiologi_usg_hasil" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
            </div>
        </div>
        <?php echo form_hidden('id', $id); ?>
        <?php echo form_close(); ?>

        <hr>

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nomorRekamMedis">Nomor Rekam Medis & Nama Pasien</label>
                            <input type="text" class="form-control" placeholder="Nomor Rekam Medis" disabled value="{nomor_medrec} {nama_pasien}">
                        </div>

                        <div class="form-group">
                            <label for="jenisKelamin">Jenis Kelamin</label>
                            <input type="text" class="form-control" placeholder="Jenis Kelamin" disabled value="{jenis_kelamin}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nik">Nomor Induk Kependudukan</label>
                            <input type="text" class="form-control" placeholder="NIK" disabled value="{nomor_ktp}">
                        </div>

                        <div class="form-group">
                            <label for="email">Nomor Registrasi</label>
                            <input type="text" class="form-control" placeholder="Nomor Registrasi" disabled value="{nomor_registrasi}">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <input type="text" class="form-control" placeholder="Alamat" disabled value="{alamat_pasien}">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="tglLahir">Tanggal Lahir</label>
                                    <input type="text" class="form-control" placeholder="Tanggal Lahir" disabled value="{tanggal_lahir}">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="umur">Umur</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Tahun" disabled value="{umur_tahun}">
                                                <div class="input-group-addon">
                                                    <span class="input-group-text">Tahun</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="umur">&nbsp;</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Bulan" disabled value="{umur_bulan}">
                                                <div class="input-group-addon">
                                                    <span class="input-group-text">Bulan</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="umur">&nbsp;</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Hari" disabled value="{umur_hari}">
                                                <div class="input-group-addon">
                                                    <span class="input-group-text">Hari</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kelompokPasien">Kelompok Pasien</label>
                            <input type="text" class="form-control" placeholder="Kelompok Pasien" disabled value="{kelompok_pasien}">
                        </div>

                        <div class="form-group">
                            <label for="namaAsuransi">Nama Asuransi</label>
                            <input type="text" class="form-control" placeholder="Nama Asuransi" disabled value="{nama_asuransi}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tipeKunjungan">Tipe Kunjungan</label>
                            <input type="text" class="form-control" placeholder="Tipe Kunjungan" disabled value="{tipe_kunjungan}">
                        </div>

                        <div class="form-group">
                            <label for="namaPoliklinik">Nama Poliklinik</label>
                            <input type="text" class="form-control" placeholder="Nama Poliklinik" disabled value="{nama_poliklinik}">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_tujuan_radiologi']); ?></label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="tujuan_radiologi" class="form-control" value="{tujuan_radiologi_label}">
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_dokter_peminta_pemeriksaan']); ?></label>
                    <div class="col-xs-12">
                        <select disabled id="dokter_peminta_id" class="js-select2 form-control" style="width: 100%;">
                            <?php foreach (get_all('mdokter', ['status' => '1']) as $r) { ?>
                            <option value="<?= $r->id; ?>" <?= $r->id == $dokter_peminta_id ? 'selected' : ''; ?>><?= $r->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_diagnosa']); ?></label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="diagnosa" class="form-control" value="{diagnosa}">
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_catatan_pemeriksaan']); ?></label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="catatan_permintaan" class="form-control" value="{catatan_permintaan}">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_waktu_pemeriksaan']); ?></label>
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group date">
                                    <input type="text" disabled class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="tanggal_permintaan" value="{tanggal_permintaan}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" disabled class="time-datepicker form-control" id="waktu_permintaan" value="{waktu_permintaan}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_prioritas']); ?></label>
                    <div class="col-xs-12">
                        <select disabled id="prioritas" class="js-select2 form-control" style="width: 100%;">
                            <?php foreach (list_variable_ref(249) as $row) { ?>
                            <option value="<?= $row->id; ?>" <?= $row->id == $prioritas ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_pasien_puasa']); ?></label>
                    <div class="col-xs-12">
                        <select disabled id="pasien_puasa" class="js-select2 form-control" style="width: 100%;">
                            <?php foreach (list_variable_ref(87) as $row) { ?>
                            <option value="<?= $row->id; ?>" <?= $row->id == $pasien_puasa ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_pengiriman_hasil']); ?></label>
                    <div class="col-xs-12">
                        <select disabled id="pengiriman_hasil" class="js-select2 form-control" style="width: 100%;">
                            <?php foreach (list_variable_ref(88) as $row) { ?>
                            <option value="<?= $row->id; ?>" <?= $row->id == $pengiriman_hasil ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_alergi_bahan_kontras']); ?></label>
                    <div class="col-xs-12">
                        <select disabled id="alergi_bahan_kontras" class="js-select2 form-control" style="width: 100%;">
                            <?php foreach (list_variable_ref(252) as $row) { ?>
                            <option value="<?= $row->id; ?>" <?= $row->id == $alergi_bahan_kontras ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for=""><?= strip_tags($pengaturan_form['label_pasien_hamil']); ?></label>
                    <div class="col-xs-12">
                        <select disabled id="pasien_hamil" class="js-select2 form-control" style="width: 100%;">
                            <?php foreach (list_variable_ref(253) as $row) { ?>
                            <option value="<?= $row->id; ?>" <?= $row->id == $pasien_hamil ? 'selected' : ''; ?>><?= $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <hr>

            <div class="col-md-4">
                <div class="form-group row">
                    <label class="col-xs-12" for="">Waktu Pemeriksaan</label>
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group date">
                                    <input type="text" disabled class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="tanggal_pemeriksaan" value="{tanggal_pemeriksaan}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" disabled class="time-datepicker form-control" id="waktu_pemeriksaan" value="{waktu_pemeriksaan}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for="">Petugas Pemeriksa</label>
                    <div class="col-xs-12">
                        <select disabled class="js-select2 form-control" id="petugas_pemeriksaan" style="width: 100%;">
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for="">Nomor Foto</label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="nomor_foto" class="form-control" value="{nomor_foto}">
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for="">Dokter Radiologi</label>
                    <div class="col-xs-12">
                        <select disabled class="js-select2 form-control" id="dokter_radiologi" style="width: 100%;">
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for="">Jumlah Expose</label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="jumlah_expose" class="form-control" value="{jumlah_expose}">
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for="">Jumlah Film</label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="jumlah_film" class="form-control" value="{jumlah_film}">
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for="">QP</label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="qp" class="form-control" value="{qp}">
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for="">MAS</label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="mas" class="form-control" value="{mas}">
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group row">
                    <label class="col-xs-12" for="">Posisi</label>
                    <div class="col-xs-12">
                        <input type="text" disabled id="posisi" class="form-control" value="{posisi}">
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <b><span class="label label-success" style="font-size:12px">UPLOAD FOTO RADIOLOGI</span></b>
        <br><br>
        <div class="row">
            <div class="col-md-4">
                <table class="table table-bordered table-striped" id="table-pemeriksaan">
                    <thead>
                        <tr>
                            <th class="text-center">No.</th>
                            <th class="text-center">Nama Pemeriksaan</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="col-md-8">
                <div id="pemeriksaan-message"></div>
                <div id="pemeriksaan-content" style="display: none;">
                    <input type="text" disabled id="pemeriksaan_nama" class="form-control" value="">
                    <br>
                    <form class="dropzone" id="dropzone-pemeriksaan" action="{base_url}term_radiologi_usg_hasil/upload_foto_radiologi_pemeriksaan" method="post" enctype="multipart/form-data">
                        <input name="pemeriksaan_id" id="pemeriksaan_id" type="hidden" value="">
                    </form>
                    <br>
                    <div id="gallery" class="gallery" itemscope itemtype="http://schema.org/ImageGallery">
                        <div id="pemeriksaan-foto-radiologi" class="row"></div>
                    </div>
                    <br><br><br>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- Background of PhotoSwipe. 
            It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>
    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">
        <!-- Container that holds slides. 
                PhotoSwipe keeps only 3 of them in the DOM to save memory.
                Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
        <div class="pswp__item"></div>
        <div class="pswp__item"></div>
        <div class="pswp__item"></div>
        </div>
        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">
        <div class="pswp__top-bar">
            <!--  Controls are self-explanatory. Order can be changed. -->
            <div class="pswp__counter"></div>
            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
            <button class="pswp__button pswp__button--share" title="Share"></button>
            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
            <!-- element will get class pswp__preloader--active when preloader is running -->
            <div class="pswp__preloader">
            <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                <div class="pswp__preloader__donut"></div>
                </div>
            </div>
            </div>
        </div>
        <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
        </div>
        <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
        </button>
        <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
        </button>
        <div class="pswp__caption">
            <div class="pswp__caption__center"></div>
        </div>
        </div>
    </div>
</div>

<?php $this->load->view('Term_radiologi_usg_hasil/services'); ?>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        loadDataDokterRadiologi('{tujuan_radiologi}', '{dokter_radiologi}');
        loadDataPetugasProsesPemeriksaan('{tujuan_radiologi}', '{petugas_pemeriksaan}');
        loadDataPemeriksaan('{id}');
        checkPemeriksaanId();

        $(document).on('click', '.btn-open', function() {
            var pemeriksaanId = $(this).data('pemeriksaan-id');
            var pemeriksaanNama = $(this).data('pemeriksaan-nama');
            var dropzoneSelector = "#dropzone-pemeriksaan";
        
            if (Dropzone.instances.length > 0) {
                Dropzone.instances.forEach(function(instance) {
                    instance.destroy();
                });
            }

            $("#pemeriksaan_id").val(pemeriksaanId);
            $("#pemeriksaan_nama").val(pemeriksaanNama);

            checkPemeriksaanId();
            initDropzone(dropzoneSelector, pemeriksaanId);
            loadFotoRadiologi(pemeriksaanId);
        });
    });

    function initDropzone(dropzoneSelector, pemeriksaanId) {
        Dropzone.autoDiscover = false;

        var myDropzone = new Dropzone(dropzoneSelector, {
            autoProcessQueue: true,
            maxFilesize: 30,
            url: '{base_url}term_radiologi_usg_hasil/upload_foto_radiologi_pemeriksaan',
            params: { pemeriksaan_id: pemeriksaanId },
            init: function() {
                this.on("complete", function(file) {
                    loadFotoRadiologi(pemeriksaanId);
                    loadDataPemeriksaan('{id}');
                });
            }
        });
    }

    function loadDataPemeriksaan(transaksiId) {
        $.ajax({
            url: '{site_url}term_radiologi_usg_hasil/get_data_pemeriksaan/' + transaksiId,
            type: 'GET',
            dataType: 'html',
            success: function(data) {
                $('#table-pemeriksaan tbody').html(data);
            },
            error: function(xhr, status, error) {
            }
        });
    }

    function checkPemeriksaanId() {
        var pemeriksaanId = $("#pemeriksaan_id").val();
        if (pemeriksaanId == '') {
            $("#pemeriksaan-message").show();
            $("#pemeriksaan-content").hide();
            $("#dropzone-pemeriksaan").hide();
            $("#pemeriksaan-message").html(`<div style="text-align: center; border: 1px dashed #999; padding: 10px; width: 100%; background-color: #f9f9f9;">
                Silahkan pilih pemeriksaan yang akan diunggah foto radiologinya.
            </div>`);
        } else {
            $("#pemeriksaan-message").hide();
            $("#pemeriksaan-content").show();
            $("#dropzone-pemeriksaan").show();
            $("#pemeriksaan-foto-radiologi").empty();
        }
    }

    function loadFotoRadiologi(pemeriksaanId) {
        var element = "#pemeriksaan-foto-radiologi";

        $(element).empty();

        $.ajax({
            url: '{site_url}term_radiologi_usg_hasil/get_foto_radiologi_pemeriksaan/' + pemeriksaanId,
            dataType: "json",
            success: function(data) {
                $(element).html(data.detail);
                photoSwipe();
            }
        });
    }

    function removeFotoRadiologi(fileId, pemeriksaanId) {
        swal({
            title: "Anda Yakin ?",
            text: "Untuk Hapus data?",
            type: "success",
            showCancelButton: true,
            confirmButtonText: "Ya",
            confirmButtonColor: "#34a263",
            cancelButtonText: "Batalkan",
        }).then(function() {
            $.ajax({
                url: '{site_url}term_radiologi_usg_hasil/remove_foto_radiologi_pemeriksaan/' + fileId + '/' + pemeriksaanId,
                complete: function() {
                    $.toaster({
                        priority: 'success',
                        title: 'Succes!',
                        message: ' Hapus Data'
                    });

                    loadFotoRadiologi(pemeriksaanId);
                    loadDataPemeriksaan('{id}');
                }
            });
        });
    }

    function photoSwipe() {
        var container = [];

        // Loop over gallery items and push them to the container array
        $('#gallery').find('figure').each(function() {
            var $link = $(this).find('a'),
            item = {
                src: $link.attr('href'),
                w: $link.data('width'),
                h: $link.data('height'),
                title: $link.data('caption')
            };
            container.push(item);
            
            // Define click event for each anchor element within a figure
            $link.click(function(event) {
                event.preventDefault();
                var index = $(this).closest('figure').index();
                openPhotoSwipe(index);
            });
        });

        // Function to open PhotoSwipe gallery
        function openPhotoSwipe(index) {
            var pswpElement = document.querySelectorAll('.pswp')[0];
            var options = {
                index: index,
                bgOpacity: 0.85,
                showHideOpacity: true
            };
            var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, container, options);
            gallery.init();
        }
    }
</script>