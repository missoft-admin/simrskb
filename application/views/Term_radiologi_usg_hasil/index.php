<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>
<?php $user_id = $this->session->userdata('user_id'); ?>

<div class="block push-10">
    <div class="block-content bg-primary" style="border-radius: 10px;">
        <div class="form-horizontal">
            <div class="row pull-10">
                <div class="col-md-2" style="width: 14.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Tujuan Radiologi</label>
                        <div class="col-xs-12">
                            <select id="tujuan_radiologi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="0" selected>Semua</option>
                                <?php foreach ($tujuan_radiologi as $r) { ?>
                                <option value="<?= $r->id; ?>"><?= $r->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="width: 14.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="dokter_peminta">Dokter Peminta</label>
                        <div class="col-xs-12">
                            <select id="dokter_peminta" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="0" selected>Semua</option>
                                <?php foreach (get_all('mdokter', ['status' => 1]) as $row) { ?>
                                <option value="<?= $row->id; ?>"><?= $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="width: 18.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Tanggal Permintaan</label>
                        <div class="col-xs-12">
                            <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                <input class="form-control" type="text" id="tanggal_dari" value="{tanggal}">
                                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                <input class="form-control" type="text" id="tanggal_sampai" value="{tanggal}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="width: 14.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="dokter_radiologi">Dokter Radiologi</label>
                        <div class="col-xs-12">
                            <select id="dokter_radiologi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="0" selected>Semua</option>
                                <?php foreach (get_all('mdokter', ['status' => 1]) as $row) { ?>
                                <option value="<?= $row->id; ?>"><?= $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="width: 14.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="">No. Medrec</label>
                        <div class="col-xs-12">
                            <input class="form-control" type="text" id="nomor_medrec" value="">
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="width: 14.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Nama Pasien</label>
                        <div class="col-xs-12">
                            <input class="form-control" type="text" id="nama_pasien" value="">
                        </div>
                    </div>
                </div>

                <div class="col-md-2" style="width: 10.2857142857%;">
                    <div class="form-group">
                        <label class="col-xs-12" for="">&nbsp;&nbsp;</label>
                        <div class="col-xs-12">
                            <button class="btn btn-success" type="button" title="Cari" id="btn-search"><i class="fa fa-filter"></i> Cari</button>
                            <button class="btn btn-warning" type="button" title="Filter" data-toggle="modal" data-target="#modal-filter"><i class="fa fa-expand"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="block">
    <div class="block-content">
        <ul class="nav nav-pills">
            <li id="tab-semua" class="<?= ($status_pemeriksaan == '#' ? 'active' : ''); ?>">
                <a href="javascript:void(0)" onclick="setTab('#')"><i class="si si-list"></i> Semua</a>
            </li>
            <li id="tab-menunggu-upload" class="<?= ($status_pemeriksaan == '4' ? 'active' : ''); ?>">
                <a href="javascript:void(0)"  onclick="setTab(4)"><i class="si si-pin"></i> Menunggu Upload </a>
            </li>
            <li id="tab-telah-upload" class="<?= ($status_pemeriksaan == '5' ? 'active' : ''); ?>">
                <a href="javascript:void(0)" onclick="setTab(5)"><i class="fa fa-check-square-o"></i> Telah Upload</a>
            </li>
            <li id="tab-selesai-expertise" class="<?= ($status_pemeriksaan == '6' ? 'active' : ''); ?>">
                <a href="javascript:void(0)"  onclick="setTab(6)"><i class="fa fa-flag"></i> Selesai Expertise</a>
            </li>
            <li id="tab-telah-dikirim" class="<?= ($status_pemeriksaan == '7' ? 'active' : ''); ?>">
                <a href="javascript:void(0)"  onclick="setTab(7)"><i class="fa fa-send"></i> Telah Dikirim</a>
            </li>
            <li id="tab-dibatalkan" class="<?= ($status_pemeriksaan == '0' ? 'active' : ''); ?>">
                <a href="javascript:void(0)"  onclick="setTab(0)"><i class="fa fa-trash"></i> Dibatalkan</a>
            </li>
        </ul>

        <br>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="table-transaksi">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Asal Pasien</th>
                                <th>Nomor Pendaftaran</th>
                                <th>Nomor Permintaan</th>
                                <th>Nomor Radiologi</th>
                                <th>Tanggal Permintaan</th>
                                <th>Urutan Antrian</th>
                                <th>Antrian Daftar</th>
                                <th>No. Medrec</th>
                                <th>Nama Pasien</th>
                                <th>Kelompok Pasien</th>
                                <th>Dokter Radiologi</th>
                                <th>Status Tindakan</th>
                                <th>Status Expertise</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="status_pemeriksaan" name="status_pemeriksaan" value="{status_pemeriksaan}">
<input type="hidden" id="transaksiId" value="">

<?php $this->load->view('Term_radiologi_usg_hasil/modal/modal_filter'); ?>
<?php $this->load->view('Term_radiologi_usg_hasil/modal/modal_ubah_kelas_tarif'); ?>
<?php $this->load->view('Term_radiologi_usg_expertise/modal/modal_kirim_hasil_pemeriksaan'); ?>
<?php $this->load->view('Term_radiologi_usg_expertise/modal/modal_kirim_ulang_hasil_pemeriksaan'); ?>
<?php $this->load->view('Term_radiologi_usg_expertise/modal/modal_kirim_hasil_upload'); ?>
<?php $this->load->view('Term_radiologi_usg_hasil/services'); ?>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        setTab($("#status_pemeriksaan").val());

        $(document).on("click", "#btn-search", function() {
            $("#cover-spin").show();

            loadListTransaksi();
        });
    });

    function setTab(tab) {
        $("#cover-spin").show();

        document.getElementById("tab-semua").classList.remove("active");
        document.getElementById("tab-menunggu-upload").classList.remove("active");
        document.getElementById("tab-telah-upload").classList.remove("active");
        document.getElementById("tab-selesai-expertise").classList.remove("active");
        document.getElementById("tab-telah-dikirim").classList.remove("active");
        document.getElementById("tab-dibatalkan").classList.remove("active");
        
        if (tab == '#') {
            document.getElementById("tab-semua").classList.add("active");
        }
        
        if (tab == '4') {
            document.getElementById("tab-menunggu-upload").classList.add("active");
        }
        
        if (tab == '5') {
            document.getElementById("tab-telah-upload").classList.add("active");
        }
        
        if (tab == '6') {
            document.getElementById("tab-selesai-expertise").classList.add("active");
        }
        
        if (tab == '7') {
            document.getElementById("tab-telah-dikirim").classList.add("active");
        }
        
        if (tab == '0') {
            document.getElementById("tab-dibatalkan").classList.add("active");
        }

        $("#status_pemeriksaan").val(tab);
        $("#selectStatusPemeriksaan").val(tab).trigger('change');

        loadListTransaksi();
    }

    function loadListTransaksi() {
        let tujuan_radiologi = $("#selectTujuanRad option:selected").val();
        let dokter_peminta = $("#selectDokterPeminta option:selected").val();
        let tanggal_dari = $("#inputTanggalPermintaanDari").val();
        let tanggal_sampai = $("#inputTanggalPermintaanSampai").val();
        let dokter_radiologi = $("#selectDokterRadiologi option:selected").val();
        let nomor_medrec = $("#inputNomorMedrec").val();
        let nama_pasien = $("#inputNamaPasien").val();
        let asal_pasien = $("#selectAsalPasien option:selected").val();
        let kelompok_pasien = $("#selectKelompokPasien option:selected").val();
        let nomor_pendaftaran = $("#inputNomorPendaftaran").val();
        let nomor_permintaan = $("#inputNomorPermintaan").val();
        let nomor_radiologi = $("#inputNomorRadiologi").val();
        let status_pemeriksaan = $("#selectStatusPemeriksaan option:selected").val();
        let status_cito_expertise = $("#selectStatusCitoExpertise option:selected").val();
        let status_expertise = $("#selectStatusExpertise option:selected").val();

        let requestData = {
            tujuan_radiologi: tujuan_radiologi,
            dokter_peminta: dokter_peminta,
            tanggal_dari: tanggal_dari,
            tanggal_sampai: tanggal_sampai,
            dokter_radiologi: dokter_radiologi,
            nomor_medrec: nomor_medrec,
            nama_pasien: nama_pasien,
            asal_pasien: asal_pasien,
            kelompok_pasien: kelompok_pasien,
            nomor_pendaftaran: nomor_pendaftaran,
            nomor_permintaan: nomor_permintaan,
            nomor_radiologi: nomor_radiologi,
            status_pemeriksaan: status_pemeriksaan,
            status_cito_expertise: status_cito_expertise,
            status_expertise: status_expertise,
        }

        $('#table-transaksi').DataTable().destroy();
        $('#table-transaksi').DataTable({
            "autoWidth": false,
            "searching": true,
            "pageLength": 50,
            "serverSide": true,
            "processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
            "columnDefs": [
                {
                    "width": "5%",
                    "targets": [0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13],
                    "className": "text-center"
                },
                {
                    "width": "5%",
                    "targets": [8],
                    "className": "text-left"
                },
                {
                    "width": "10%",
                    "targets": [14],
                    "className": "text-center"
                },
            ],
            ajax: {
                url: '{site_url}term_radiologi_usg_hasil/getIndex',
                type: "POST",
                dataType: 'json',
                data: requestData
            }
        });

        $("#cover-spin").hide();
    }
</script>
