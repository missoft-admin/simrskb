<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<hr style="margin-top:0px">
		<?php echo form_open('lpenerimaan_gudang/export_report','class="form-horizontal" id="form-work" target="_blank"') ?>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Tipe Gudang</label>
                        <div class="col-xs-12">
                            <select name="tipe_gudang[]" id="tipe_gudang" class="js-select2 form-control" multiple data-placeholder="Pilih Opsi">
								<?php foreach (get_all('merm_pengaturan_laporan_gudang', ['status' => 1]) as $row) { ?>
								<option value="<?=$row->jenis_gudang?>"><?=$row->label_name?></option>
								<?php } ?>
							</select>
                        </div>
                    </div>
                </div>
				<div class="col-md-2">
					<div class="form-group">
						<label class="col-md-12" for="">Periode Tanggal</label>
						<div class="col-md-12">
							<select name="periode_tanggal" id="periode_tanggal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="1" selected>Hari Ini</option>
								<option value="2">Bulan Ini</option>
								<option value="3">Tahun Ini</option>
								<option value="4">Triwulan 1</option>
								<option value="5">Triwulan 2</option>
								<option value="6">Triwulan 3</option>
								<option value="7">Triwulan 4</option>
								<option value="8">Semester 1</option>
								<option value="9">Semester 2</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label class="col-md-12" for="">&nbsp;&nbsp;</label>
						<div class="col-md-12">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" id="tanggal_dari" name="tanggal_dari" placeholder="From" value=""/>
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" id="tanggal_sampai" name="tanggal_sampai" placeholder="To" value=""/>
							</div>
						</div>
					</div>
				</div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Distributor</label>
                        <div class="col-xs-12">
                            <select name="distributor_id[]" id="distributor_id" class="js-select2 form-control" multiple data-placeholder="Pilih Opsi">
								<?php foreach (get_all('mdistributor', ['status' => 1]) as $row) { ?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>
								<?php } ?>
							</select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Tipe Barang</label>
                        <div class="col-xs-12">
                            <select name="tipe_barang[]" id="tipe_barang" class="js-select2 form-control" multiple data-placeholder="Pilih Opsi">
								<option value="1">ALKES</option>
                                <option value="2">OBAT</option>
                                <option value="3">IMPLAN</option>
                                <option value="4">LOGISTIK</option>
							</select>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Order By</label>
                        <div class="col-xs-12">
                            <select name="order_by[]" id="order_by" class="js-select2 form-control" style="width: 100%;" multiple data-placeholder="Pilih Opsi">
                                <option value="tgudang_penerimaan.tipepenerimaan">TIPE GUDANG</option>
                                <option value="mdistributor.nama">NAMA DISTRIBUTOR</option>
								<option value="tgudang_penerimaan.tgl_terima">TANGGAL TERIMA</option>
								<option value="tgudang_penerimaan.nopenerimaan">NOMOR PENERIMAAN</option>
								<option value="tgudang_penerimaan.nofakturexternal">NOMOR FAKTUR</option>
								<option value="tgudang_penerimaan.tanggaljatuhtempo">TANGGAL JATUH TEMPO</option>
								<option value="view_barang_all.kode">KODE</option>
								<option value="view_barang_all.nama">NAMA BARANG</option>
								<option value="tgudang_penerimaan_detail.namasatuan">NAMA SATUAN</option>
								<option value="tgudang_penerimaan_detail.harga_asli">HARGA SATUAN</option>
								<option value="tgudang_penerimaan_detail.kuantitas">QTY</option>
								<option value="(tgudang_penerimaan_detail.harga_asli * tgudang_penerimaan_detail.kuantitas)">JUMLAH HARGA</option>
								<option value="tgudang_penerimaan_detail.diskon">DISC</option>
								<option value="(tgudang_penerimaan_detail.nominaldiskon * tgudang_penerimaan_detail.kuantitas)">JUMLAH DISC</option>
								<option value="tgudang_penerimaan_detail.ppn">PPN</option>
								<option value="(tgudang_penerimaan_detail.nominalppn * tgudang_penerimaan_detail.kuantitas)">NOMINAL PPN</option>
								<option value="(tgudang_penerimaan_detail.harga_asli + tgudang_penerimaan_detail.nominalppn) * tgudang_penerimaan_detail.kuantitas">NILAI PERSEDIAAN</option>
								<option value="(tgudang_penerimaan_detail.harga_after_ppn * tgudang_penerimaan_detail.kuantitas)">NILAI FAKTUR</option>
								<option value="tgudang_penerimaan_detail.tanggalkadaluarsa">TANGGAL KADALUARSA</option>
								<option value="tgudang_penerimaan.tipe_bayar">BAYAR</option>
								<option value="tgudang_penerimaan.keterangan">KETERANGAN</option>
								<option value="tgudang_pemesanan.tanggal">TANGGAL TRANSAKSI</option>
								<option value="tgudang_penerimaan.userpenerimaa">USER</option>
								<option value="tgudang_pemesanan.nopemesanan">NOMOR PEMESANAN</option>
								<option value="tgudang_pemesanan.tgl_pemesanan">TANGGAL PEMESANAN</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pull-10">
				<div class="col-md-4">
					<div class="form-group">
                        <label class="col-xs-12" for="">&nbsp;</label>
                        <div class="col-xs-12">
							<button class="btn btn-sm btn-success" id="btn-search" type="button" title="Cari"><i class="fa fa-search"></i> Search</button>
							<button class="btn btn-sm btn-warning" id="btn-export-excel" type="submit" name="btn_export" value="excel" title="Cari"><i class="fa fa-file-excel-o"></i> Excel</button>
							<button class="btn btn-sm btn-danger" id="btn-export-pdf" type="submit" name="btn_export" value="pdf" title="Cari"><i class="fa fa-file-pdf-o"></i> PDF</button>
							<button class="btn btn-sm btn-primary" id="btn-send-email" type="button" title="Cari"><i class="fa fa-send"></i> Send Email</button>
                        </div>
                    </div>
                </div>
				<div class="col-md-2">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Total Faktur</label>
                        <div class="col-xs-12">
                            <input type="text" class="form-control" id="total_faktur" placeholder="Total Faktur" readonly value="">
                        </div>
                    </div>
                </div>
				<div class="col-md-2">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Total Nilai Persediaan</label>
                        <div class="col-xs-12">
                            <input type="text" class="form-control" id="total_nilai_persediaan" placeholder="Total Nilai Persediaan" readonly value="">
                        </div>
                    </div>
                </div>
				<div class="col-md-2">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Total Discount</label>
                        <div class="col-xs-12">
                            <input type="text" class="form-control" id="total_discount" placeholder="Total Discount" readonly value="">
                        </div>
                    </div>
                </div>
				<div class="col-md-2">
                    <div class="form-group">
                        <label class="col-xs-12" for="">Total PPN</label>
                        <div class="col-xs-12">
                            <input type="text" class="form-control" id="total_ppn" placeholder="Total PPN" readonly value="">
                        </div>
                    </div>
                </div>
            </div>
		<?php echo form_close() ?>

		<div class="table-responsive">
			<table class="table table-bordered table-striped" id="table-report">
				<thead>
					<tr>
						<th class="text-center">NO</th>
						<th class="text-center">TIPE GUDANG</th>
						<th class="text-center">NAMA DISTRIBUTOR</th>
						<th class="text-center">TANGGAL TERIMA</th>
						<th class="text-center">NOMOR PENERIMAAN</th>
						<th class="text-center">NOMOR FAKTUR</th>
						<th class="text-center">TANGGAL JATUH TEMPO</th>
						<th class="text-center">KODE</th>
						<th class="text-center">NAMA BARANG</th>
						<th class="text-center">NAMA SATUAN</th>
						<th class="text-center">HARGA SATUAN</th>
						<th class="text-center">QTY</th>
						<th class="text-center">JUMLAH HARGA</th>
						<th class="text-center">DISC</th>
						<th class="text-center">JUMLAH DISC</th>
						<th class="text-center">PPN</th>
						<th class="text-center">NOMINAL PPN</th>
						<th class="text-center">NILAI PERSEDIAAN</th>
						<th class="text-center">NILAI FAKTUR</th>
						<th class="text-center">TANGGAL KADALUARSA</th>
						<th class="text-center">BAYAR</th>
						<th class="text-center">KETERANGAN</th>
						<th class="text-center">TANGGAL TRANSAKSI</th>
						<th class="text-center">USER</th>
						<th class="text-center">NOMOR PEMESANAN</th>
						<th class="text-center">TANGGAL PEMESANAN</th>
						<th class="text-center">AKSI</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
				<tfoot></tfoot>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	getPeriodeTanggal();
	getIndexData();

	$('.table-responsive').on('show.bs.dropdown', function () {
		$('.table-responsive').css( "overflow", "auto" );
	});

	$("#periode_tanggal").change(function(){
		getPeriodeTanggal();
	});

	$('#btn-search').on('click', function () {
		getIndexData();
	});
});

// Fungsi format angka
function formatNumber(number) {
    return number ? new Intl.NumberFormat('id-ID').format(number) : '0';
}

function getIndexData() {
	const tipe_gudang = $("#tipe_gudang").val();
	const periode_tanggal = $("#periode_tanggal").val();
	const tanggal_dari = $("#tanggal_dari").val();
	const tanggal_sampai = $("#tanggal_sampai").val();
	const distributor_id = $("#distributor_id").val();
	const tipe_barang = $("#tipe_barang").val();
	const order_by = $("#order_by").val();
	
	$.ajax({
		url: '{site_url}lpenerimaan_gudang/get_index',
		method: 'POST',
		data: {
			tipe_gudang,
			periode_tanggal,
			tanggal_dari,
			tanggal_sampai,
			distributor_id,
			tipe_barang,
			order_by,
		},
		dataType: "json",
		success: function(data) {
			// Render data ke tabel
			const tbody = $("#table-report tbody");
			const tfoot = $("#table-report tfoot");

			tbody.empty(); // Hapus konten sebelumnya
			tfoot.empty(); // Hapus total sebelumnya

			let total_harga_satuan = 0;
			let total_qty = 0;
			let total_jumlah_harga = 0;
			let total_disc = 0;
			let total_jumlah_disc = 0;
			let total_ppn = 0;
			let total_nominal_ppn = 0;
			let total_nilai_persediaan = 0;
			let total_nilai_faktur = 0;

			if (data.length > 0) {
				data.forEach((row, index) => {
					total_harga_satuan += parseFloat(row.harga_satuan) || 0;
					total_qty += parseFloat(row.qty) || 0;
					total_jumlah_harga += parseFloat(row.jumlah_harga) || 0;
					total_disc += parseFloat(row.disc) || 0;
					total_jumlah_disc += parseFloat(row.jumlah_disc) || 0;
					total_ppn += parseFloat(row.ppn) || 0;
					total_nominal_ppn += parseFloat(row.nominal_ppn) || 0;
					total_nilai_persediaan += parseFloat(row.nilai_persediaan) || 0;
					total_nilai_faktur += parseFloat(row.nilai_faktur) || 0;

					tbody.append(`
						<tr>
							<td class="text-center">${index + 1}</td>
							<td>${row.tipe_gudang || ''}</td>
							<td>${row.nama_distributor || ''}</td>
							<td>${row.tanggal_terima || ''}</td>
							<td>${row.nomor_penerimaan || ''}</td>
							<td>${row.nomor_faktur || ''}</td>
							<td>${row.tanggal_jatuh_tempo || ''}</td>
							<td>${row.kode_barang || ''}</td>
							<td>${row.nama_barang || ''}</td>
							<td>${row.nama_satuan || ''}</td>
							<td class="text-right">${formatNumber(row.harga_satuan)}</td>
							<td class="text-right">${formatNumber(row.qty)}</td>
							<td class="text-right">${formatNumber(row.jumlah_harga)}</td>
							<td class="text-right">${formatNumber(row.disc)}</td>
							<td class="text-right">${formatNumber(row.jumlah_disc)}</td>
							<td class="text-right">${formatNumber(row.ppn)}</td>
							<td class="text-right">${formatNumber(row.nominal_ppn)}</td>
							<td class="text-right">${formatNumber(row.nilai_persediaan)}</td>
							<td class="text-right">${formatNumber(row.nilai_faktur)}</td>
							<td>${row.tanggal_kadaluarsa || ''}</td>
							<td>${row.bayar || ''}</td>
							<td>${row.keterangan || ''}</td>
							<td>${row.tanggal_transaksi || ''}</td>
							<td>${row.user_terima || ''}</td>
							<td>${row.nomor_pemesanan || ''}</td>
							<td>${row.tanggal_pemesanan || ''}</td>
							<td>
								<div class="btn-group dropdown">
									<button class="btn btn-primary btn-xs btn-block dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										<i class="fa fa-print"></i>
									</button>
									<ul class="dropdown-menu pull-left">
										<li><a href="{base_url}tgudang_penerimaan/print_data_bukti_pemesanan/${row.idpemesanan}/${row.id}" target="_blank">Pemesanan Barang</a></li>
										<li><a href="{base_url}tgudang_penerimaan/print_data/${row.id}/0" target="_blank">Penerimaan Barang</a></li>
									</ul>
								</div>
								<a href="{base_url}tgudang_penerimaan/detail/${row.id}" target="_blank" data-toggle="tooltip" title="Lihat Rincian" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>
							</td>
						</tr>
					`);
				});

				tfoot.append(`
					<tr>
						<th colspan="10" style="text-align: right;">TOTAL</th>
						<th class="text-right">${formatNumber(total_harga_satuan)}</th>
						<th class="text-right">${formatNumber(total_qty)}</th>
						<th class="text-right">${formatNumber(total_jumlah_harga)}</th>
						<th class="text-right">${formatNumber(total_disc)}</th>
						<th class="text-right">${formatNumber(total_jumlah_disc)}</th>
						<th class="text-right">${formatNumber(total_ppn)}</th>
						<th class="text-right">${formatNumber(total_nominal_ppn)}</th>
						<th class="text-right">${formatNumber(total_nilai_persediaan)}</th>
						<th class="text-right">${formatNumber(total_nilai_faktur)}</th>
					</tr>
				`);

				$("#total_faktur").val(formatNumber(data.length));
				$("#total_nilai_persediaan").val(formatNumber(total_nilai_persediaan));
				$("#total_discount").val(formatNumber(total_disc));
				$("#total_ppn").val(formatNumber(total_ppn));
			} else {
				tbody.append('<tr><td colspan="28" class="text-center">Tidak ada data</td></tr>');
			}
		},
		error: function(xhr, status, error) {
			console.error("Error:", error);
		}
	});
}

function getPeriodeTanggal() {
	const periode_tanggal = $("#periode_tanggal").val();

	$.ajax({
		url: '{site_url}lsurvey/list_tipe/' + periode_tanggal,
		dataType: "json",
		success: function(data) {
			$("#tanggal_dari").val(data.tanggal_1);
			$("#tanggal_sampai").val(data.tanggal_2);
		}
	});
}
</script>