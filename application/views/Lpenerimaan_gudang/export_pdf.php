<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title><?= $title; ?></title>
	<style type="text/css" media="all">
		<?php include "assets/css/print.css"?>
		@page {
			margin-top: 1em;
			margin-left: 1.6em;
			margin-right: 1.5 em;
			margin-bottom: 1em;
		}
		@font-face {
			font-family: 'font';
			font-style: normal;
			font-weight: normal;
			src: url(dompdf/font/arial.ttf);
		}
		@font-face {
			font-family: 'font2';
			font-style: normal;
			font-weight: bold;
			src: url(dompdf/font/arialbd.ttf);
		}
		body {
			-webkit-print-color-adjust: exact;
		}
		@media print {
			table {
				font-size: 11px !important;
				border-collapse: collapse !important;
				width: 100% !important;
				font-family: "Segoe UI", Arial, sans-serif;
			}

			th {
				padding: 5px;
			}
			td {
				padding: 0px;
			}
			.content th {
				padding: 2px;
			}
			.content td {
				padding: 2px;
			}
			.content-2 td {
				margin: 3px;
			}

			/* border-normal */
			.border-full {
				border: 1px solid #000 !important;
				margin: 3px;
				padding: 2px;
			}
			.border-bottom {
				border-bottom:1px solid #000 !important;
			}

			/* border-thick */
			.border-thick-top{
				border-top:2px solid #000 !important;
			}
			.border-thick-bottom{
				border-bottom:2px solid #000 !important;
			}

			.border-dotted{
				border-width: 1px;
				border-bottom-style: dotted;
			}

			/* text-position */
			.text-center{
				text-align: center !important;
			}
			.text-header{
				font-size: 20px !important;
			}

			.text-right{
				text-align: right !important;
			}

			/* text-style */
			.text-italic{
				font-style: italic;
			}
			.text-bold{
				font-weight: bold;
			}
		}

		@media screen {
			table {
				font-family: "Courier New", Verdana, sans-serif;
				font-size: 11px !important;
				border-collapse: collapse !important;
				width: 100% !important;
			}
			th {
				padding: 5px;
			}
			td {
				padding: 0px;
			}
			.content td {
				padding: 0px;
				word-wrap: break-word;
				border: 0px solid #6033FF;
				vertical-align:middle;
			}
			.content-2 td {
				padding: 0px;
				border: 0px solid #6033FF;
				vertical-align:top;
			}
			.text-muted {
				font-size: 12px !important;
			}

			/* border-normal */
			.border-full {
				border: 1px solid #000 !important;

			}
			.text-notes{
				font-size: 10px !important;
			}
			.text-normal{
				font-size: 12px !important;
			}
			.text-upnormal{
				font-size: 14px !important;
			}
			.text-header{
				font-size: 16px !important;
				font-weight:bold;
			}
			.text-judul{
				font-size: 18px  !important;
				
			}
			.border-bottom {
				border-bottom:1px solid #000 !important;
			}
			.border-bottom-left {
				border-bottom:1px solid #000 !important;
				border-left:1px solid #000 !important;
			}
			.border-bottom-right {
				border-bottom:1px solid #000 !important;
				border-right:1px solid #000 !important;
			}
			.border-bottom-top {
				border-bottom:1px solid #000 !important;
				border-top:1px solid #000 !important;
			}
			.border-full {
				border-bottom:1px solid #000 !important;
				border-top:1px solid #000 !important;
				border-left:1px solid #000 !important;
			}
			.border-bottom-top-right {
				border-bottom:1px solid #000 !important;
				border-top:1px solid #000 !important;
				border-right:1px solid #000 !important;
			}
			.border-left {
				border-left:1px solid #000 !important;
			}

			/* border-thick */
			.border-thick-top{
				border-top:2px solid #000 !important;
			}
			.border-thick-bottom{
				border-bottom:2px solid #000 !important;
			}

			.border-dotted{
				border-width: 1px;
				border-bottom-style: dotted;
			}

			/* text-position */
			.text-center{
				text-align: center !important;
			}
			.text-left{
				text-align: left !important;
			}
			.text-right{
				text-align: right !important;
			}

			/* text-style */
			.text-italic{
				font-style: italic;
			}
			.text-bold{
				font-weight: bold;
			}
			.text-top{
				font-size: 14px !important;
			}
			br {
				display: block;
				margin: 20px 0;
			}
			fieldset {
				border:1px solid #000;
				border-radius:4px;
				box-shadow:0 0 0px #000;
				margin-top: -4px !important;
			}
			legend {
				background:#fff;
			}
		}

		.text-muted {
			color: #000;
			font-weight: normal;
			font-style: italic;
		}

		.center {
			display: block;
			margin-left: auto;
			margin-right: auto;
			width: 50%;
		}

		ul {
			margin:0;
			padding-left: 12px;
		}
	</style>
</head>

<body>
	<main>
		<table class="content">
			<tr>
				<td width="100%" class="text-center"><img src="<?= base_url().'/assets/upload/app_setting/' . $logo; ?>" alt="" width="100" height="100"></td>
			</tr>
			<tr>
				<td width="100%" class="text-center text-normal"><?= $alamat_rs1; ?><br><?= $telepon_rs; ?> <?= $fax_rs; ?><br><?= $web_rs; ?> Email : <?= $email_rs; ?></td>
			</tr>
		</table>
		
		<table class="content">
			<tr>
				<td width="100%" class="text-center text-judul"><strong>LAPORAN PENERIMAAN GUDANG</strong><br><i>PERIODE <?= $filter['tanggal_dari']; ?> s/d <?= $filter['tanggal_sampai']; ?></i></td>
			</tr>
		</table>
		
		<br><br><br><br>
		
		<table class="content">
			<thead>
				<tr>
					<th class="text-center border-full">NO</th>
					<th class="text-center border-full">TIPE GUDANG</th>
					<th class="text-center border-full">NAMA DISTRIBUTOR</th>
					<th class="text-center border-full">TANGGAL TERIMA</th>
					<th class="text-center border-full">NOMOR PENERIMAAN</th>
					<th class="text-center border-full">NOMOR FAKTUR</th>
					<th class="text-center border-full">TANGGAL JATUH TEMPO</th>
					<th class="text-center border-full">KODE</th>
					<th class="text-center border-full">NAMA BARANG</th>
					<th class="text-center border-full">NAMA SATUAN</th>
					<th class="text-center border-full">HARGA SATUAN</th>
					<th class="text-center border-full">QTY</th>
					<th class="text-center border-full">JUMLAH HARGA</th>
					<th class="text-center border-full">DISC</th>
					<th class="text-center border-full">JUMLAH DISC</th>
					<th class="text-center border-full">PPN</th>
					<th class="text-center border-full">NOMINAL PPN</th>
					<th class="text-center border-full">NILAI PERSEDIAAN</th>
					<th class="text-center border-full">NILAI FAKTUR</th>
					<th class="text-center border-full">TANGGAL KADALUARSA</th>
					<th class="text-center border-full">BAYAR</th>
					<th class="text-center border-full">KETERANGAN</th>
					<th class="text-center border-full">TANGGAL TRANSAKSI</th>
					<th class="text-center border-full">USER</th>
					<th class="text-center border-full">NOMOR PEMESANAN</th>
					<th class="text-center border-full">TANGGAL PEMESANAN</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$total_harga_satuan = 0;
					$total_qty = 0;
					$total_jumlah_harga = 0;
					$total_disc = 0;
					$total_jumlah_disc = 0;
					$total_ppn = 0;
					$total_nominal_ppn = 0;
					$total_nilai_persediaan = 0;
					$total_nilai_faktur = 0;
				?>
				<?php foreach ($report as $index => $row) { ?>
					<tr>
						<td class="text-center border-full"><?= ($index + 1); ?></td>
						<td class="text-left border-full"><?= $row->tipe_gudang; ?></td>
						<td class="text-left border-full"><?= $row->nama_distributor; ?></td>
						<td class="text-left border-full"><?= $row->tanggal_terima; ?></td>
						<td class="text-left border-full"><?= $row->nomor_penerimaan; ?></td>
						<td class="text-left border-full"><?= $row->nomor_faktur; ?></td>
						<td class="text-left border-full"><?= $row->tanggal_jatuh_tempo; ?></td>
						<td class="text-left border-full"><?= $row->kode_barang; ?></td>
						<td class="text-left border-full"><?= $row->nama_barang; ?></td>
						<td class="text-left border-full"><?= $row->nama_satuan; ?></td>
						<td class="text-right border-full"><?= number_format($row->harga_satuan); ?></td>
						<td class="text-right border-full"><?= number_format($row->qty); ?></td>
						<td class="text-right border-full"><?= number_format($row->jumlah_harga); ?></td>
						<td class="text-right border-full"><?= number_format($row->disc); ?></td>
						<td class="text-right border-full"><?= number_format($row->jumlah_disc); ?></td>
						<td class="text-right border-full"><?= number_format($row->ppn); ?></td>
						<td class="text-right border-full"><?= number_format($row->nominal_ppn); ?></td>
						<td class="text-right border-full"><?= number_format($row->nilai_persediaan); ?></td>
						<td class="text-right border-full"><?= number_format($row->nilai_faktur); ?></td>
						<td class="text-left border-full"><?= $row->tanggal_kadaluarsa; ?></td>
						<td class="text-left border-full"><?= $row->bayar; ?></td>
						<td class="text-left border-full"><?= $row->keterangan; ?></td>
						<td class="text-left border-full"><?= $row->tanggal_transaksi; ?></td>
						<td class="text-left border-full"><?= $row->user_terima; ?></td>
						<td class="text-left border-full"><?= $row->nomor_pemesanan; ?></td>
						<td class="text-left border-full"><?= $row->tanggal_pemesanan; ?></td>
					</tr>
					<?php
						$total_harga_satuan += $row->harga_satuan;
						$total_qty += $row->qty;
						$total_jumlah_harga += $row->jumlah_harga;
						$total_disc += $row->disc;
						$total_jumlah_disc += $row->jumlah_disc;
						$total_ppn += $row->ppn;
						$total_nominal_ppn += $row->nominal_ppn;
						$total_nilai_persediaan += $row->nilai_persediaan;
						$total_nilai_faktur += $row->nilai_faktur;
					?>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<th colspan="10" class="text-right border-full">TOTAL</th>
					<th class="text-right border-full"><?= number_format($total_harga_satuan); ?></th>
					<th class="text-right border-full"><?= number_format($total_qty); ?></th>
					<th class="text-right border-full"><?= number_format($total_jumlah_harga); ?></th>
					<th class="text-right border-full"><?= number_format($total_disc); ?></th>
					<th class="text-right border-full"><?= number_format($total_jumlah_disc); ?></th>
					<th class="text-right border-full"><?= number_format($total_ppn); ?></th>
					<th class="text-right border-full"><?= number_format($total_nominal_ppn); ?></th>
					<th class="text-right border-full"><?= number_format($total_nilai_persediaan); ?></th>
					<th class="text-right border-full"><?= number_format($total_nilai_faktur); ?></th>
					<th colspan="7" class="text-right border-full"></th>
				</tr>
			</tfoot>
		</table>
	</main>
</body>

</html>
