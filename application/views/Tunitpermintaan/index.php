<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li><a href="{site_url}tunitpermintaan/add" class="btn btn-default tambah-permintaan"><i class="fa fa-plus"></i> Tambah</a></li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <table class="table table-bordered table-striped" id="datatable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Dari Unit</th>
                    <th>Ke Unit</th>
                    <th>T. Pesan</th>
                    <th>T. Kirim</th>
                    <th>T. Alih</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">

    var table;

    $(document).ready(function(){
        table = $('#datatable').DataTable({
            autoWidth: false,
            pageLength: 50,
            serverSide: true,
            order: [[8,'desc']],
            ajax: { url: '{site_url}tunitpermintaan/dtpermintaan', type: "POST" },
            columns: [
                {data: "nopermintaan", width: '10%'},
                {data: "dariunit", width: '20%'},
                {data: "keunit", width: '20%'},
                {data: "totalpesan", width: '10%', sorting: false},
                {data: "totalkirim", width: '10%', sorting: false},
                {data: "totalalih", width: '10%', sorting: false},
                {defaultContent: "-", width: '10%', sorting: false},
                {defaultContent: "-", sorting: false},
                {data: "id", visible: false},
            ],
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                var id      = aData['id'];
                var url     = '{site_url}tunitpermintaan/';
                var status;
                var aksi    = '<div class="btn-group">';

                if(aData['status'] == 4) {
                    status  = '<label class="label label-success">Selesai</label>';
                    aksi    += '<a href="'+url+'view_detail/'+id+'" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>';
                } else if(aData['status'] == 3) {
                    status  = '<label class="label label-warning">Penerimaan</label>';
                    aksi    += '<a href="'+url+'view_detail/'+id+'" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>';
                    aksi    += '<a href="'+url+'unit_penerimaan/'+id+'" class="btn btn-xs btn-warning"><i class="fa fa-arrow-down"></i></a>';
                } else if(aData['status'] == 2) {
                    status  = '<label class="label label-info">Kirim / Terima</label>';
                    aksi    += '<a href="'+url+'view_detail/'+id+'" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>';
                    aksi    += '<a href="'+url+'acc_pengiriman/'+id+'" class="btn btn-xs btn-success"><i class="fa fa-arrow-up"></i></a>';
                    aksi    += '<a href="'+url+'unit_penerimaan/'+id+'" class="btn btn-xs btn-warning"><i class="fa fa-arrow-down"></i></a>';
                } else {
                    status  = '<label class="label label-default">Pending</label>';
                    aksi    += '<a href="'+url+'view_detail/'+id+'" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>';
                    aksi    += '<a href="'+url+'acc_pengiriman/'+id+'" class="btn btn-xs btn-success"><i class="fa fa-arrow-up"></i></a>';
                    aksi    += '<a class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>';
                }
                aksi += '</div>';
                $('td:eq(6)', nRow).html(status)
                $('td:eq(7)', nRow).html(aksi)

                return nRow;
            }
        })
    })
</script>
