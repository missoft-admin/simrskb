
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<form action="{site_url}tunitpermintaan/save_acc_pengiriman/<?php echo $this->uri->segment(3) ?>" method="post" class="form-horizontal">
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">{title}</h3>
        </div>
        <div class="block-content">
            <table class="table table-bordered table-striped" id="datatable">
                <thead>
                    <tr>
                        <th>Tipe</th>
                        <th>Nama</th>
                        <th>Dari</th>
                        <th>Ke</th>
                        <th>Pesan</th>
                        <th>Dikirim</th>
                        <th>Sisa</th>
                        <th>Kirim</th>
                        <th>Aksi</th>
                        <th hidden>dariunit</th>
                        <th hidden>keunit</th>
                        <th hidden>kuantitaskirim</th>
                        <th hidden>idtipe</th>
                        <th hidden>idbarang</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
            <div class="form-group text-right">
                <div class="col-md-12">
                    <a href="{site_url}tunitpermintaan" class="btn btn-default" type="reset">Kembali</a>
                    <button class="btn btn-success" type="submit">Submit</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form action="javascript:(0)" method="post" class="form-horizontal" id="form2">
    <div class="modal in" id="modal_alihkan_permintaan" role="dialog" aria-hidden="true"> 
        <div class="modal-dialog">
            <div class="modal-content"> 
                <div class="block block-themed block-transparent remove-margin-b"> 
                    <div class="block-header bg-success"> 
                        <h3 class="block-title">Alihkan Permintaan</h3> 
                    </div> 
                    <div class="block-content">
                        <input type="hidden" name="iddaripermintaan" value="<?php echo $this->uri->segment(3) ?>">
                        <input type="hidden" name="iddaripermintaandetail">
                        <input type="hidden" name="keunit">
                        <input type="hidden" name="idtipe">
                        <input type="hidden" name="idbarang">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Barang</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="namabarang" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Dari Unit</label>
                            <div class="col-md-9">
                                <select class="form-control" name="dariunit" style="width:100%"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Ke Unit</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="keunit_text" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Kuantitas</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="kuantitasalih">
                            </div>
                        </div>
                    </div> 
                </div> 
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button> 
                    <button class="btn btn-sm btn-success" type="submit">Simpan</button> 
                </div> 
            </div> 
        </div>
    </div>
</form>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
    var table;

    $(document).ready(function(){
        table = $('#datatable').DataTable({
            autoWidth: false,
            info: false,
            searching: false,
            paging: false,
            sort: false,
            destroy: true,
            ajax: '{site_url}tunitpermintaan/dtpermintaan_detail/<?php echo $this->uri->segment(3) ?>',
            columns: [
                {data: "idtipe", width: '5%'},
                {data: "idbarang", width: '10%'},
                {data: "dariunit", width: '10%'},
                {data: "keunit", width: '10%'},
                {data: "kuantitas", width: '5%'},
                {data: "kuantitaskirim", width: '10%'},
                {data: "kuantitassisa", width: '5%'},
                {
                    data: "kuantitaskirim", width: '10%',
                    render: function(data,type,row) {
                        if(row.kuantitassisa <= 0) {
                            return '<input type="text" name="kuantitaskirim[]" value="'+ row.kuantitassisa +'" class="form-control form-xs" size="5" disabled><input type="hidden" name="id[]" value="'+row.id+'" disabled>';
                        } else {
                            return '<input type="text" name="kuantitaskirim[]" value="'+ row.kuantitassisa +'" class="form-control form-xs" size="5"><input type="hidden" name="id[]" value="'+row.id+'">';
                        }
                    }
                },
                {
                    defaultContent: '-', width: '10%',
                    render: function(data, type, row) {
                        if(row.dariunit == 0) {
                            if(row.kuantitassisa <= 0) {
                                return '<a href="#" class="btn btn-xs btn-success">Selesai</a>';
                            } else {
                                return '<a href="#" class="btn btn-xs btn-warning pesan_distributor">Ke Distributor</a>';
                            }
                        } else {
                            if(row.kuantitassisa <= 0) {
                                return '<a href="#" class="btn btn-xs btn-success">Selesai</a>';
                            } else {
                                return '<a href="#" class="btn btn-xs btn-warning alihkan">Alihkan</a>';
                            }
                        }
                    }
                },
                {data: "idunitpeminta", visible: false}, // 9
                {data: "idunitpenerima", visible: false},
                {data: "kuantitaskirim", visible: false},
                {data: "idtipe", visible: false},
                {data: "idbarang", visible: false},
                {data: "id", visible: false},
            ],
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $.ajax({
                    url: '{site_url}tunitpermintaan/ajax_namabarang',
                    dataType: 'json',
                    method: 'post',
                    data: {idtipe: aData['idtipe'], idbarang: aData['idbarang']},
                    success: function(res) {
                        $('td:eq(1)', nRow).html(res)
                    }
                })                
                return nRow;
            }
        })

    })

    $('#datatable tbody').on('click', '.alihkan', function(){
        $('#modal_alihkan_permintaan').modal('show')

        tr = $(this).closest('tr');
        trRow = table.row( $(this).parents('tr') ).index();
        
        $('input[name=namabarang]').val( tr.find('td:eq(1)').text() )
        $('input[name=keunit_text]').val( tr.find('td:eq(3)').text() )
        $('input[name=kuantitasalih]').val( tr.find('td:eq(6)').text() )
        $('input[name=keunit]').val( table.cell(trRow,9).data() )
        $('input[name=idtipe]').val( table.cell(trRow,12).data() )
        $('input[name=idbarang]').val( table.cell(trRow,13).data() )
        $('input[name=iddaripermintaandetail]').val( table.cell(trRow,14).data() )

        var unitid = table.cell(trRow,9).data();

        // ke unit
        $('select[name=dariunit]').select2({
            placeholder: 'Cari Data ... ',
            allowClear: true,
            ajax: {
                url: '{site_url}tunitpermintaan/selectdariunit_alihan/'+unitid,
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {results:data}
                },
                cache: false,
            }
        })        
    })

    $('#form2').on('submit', (function(e){
        e.preventDefault();
        var formData = new FormData(this)

        $.ajax({
            method:'POST',
            url: '{site_url}tunitpermintaan/save_pengalihan_permintaan',
            data: formData,
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            success:function(res){
                if(res.status == '200') {
                    swal('Sukses','Data berhasil disimpan!','success')
                    $('#modal_alihkan_permintaan').modal('hide')
                    setTimeout(function() {
                        $('#datatable').DataTable().ajax.reload()
                    }, 1000);
                } else {
                    swal('Kesalahan','Data gagal disimpan!','danger')
                }
            }
        })        
    }))

    function get_namabarang() {
        $.ajax({
            url: '{site_url}tunitpermintaan/ajax_namabarang',
            dataType: 'json',
            method: 'post',
            success: function(res) {
                return res;
            }
        })
    }
</script>