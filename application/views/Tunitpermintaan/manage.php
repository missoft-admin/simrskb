<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <form id="form1" class="form-horizontal" action="javascript:(0)">
            <input type="hidden" name="idbarang">
            <div class="form-group">
                <div class="col-md-6">
                    <label>Tipe Barang</label>
                    <select class="form-control" name="idtipe" style="width:100%"></select>
                </div>
                <div class="col-md-6">
                    <label>Dari Unit</label>
                    <select class="form-control" name="dariunit" style="width:100%"></select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <label>Kode Barang</label>
                    <input type="text" class="form-control" name="kodebarang" readonly>
                </div>
                <div class="col-md-6">
                    <label>Stok Unit</label>
                    <input type="text" class="form-control" name="stokunit" readonly>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <label>Nama Barang</label>
                    <input type="text" class="form-control" name="namabarang" readonly>
                </div>
                <div class="col-md-6">
                    <label>Ke Unit</label>
                    <select class="form-control" name="keunit" style="width:100%"></select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6"> </div>
                <div class="col-md-6">
                    <label>Kuantitas</label>
                    <input type="text" class="form-control" name="kuantitas">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 pull-right text-right">
                    <button class="btn btn-default btn-sm" id="tambahkan">Tambahkan</button>
                </div>
            </div>
        </form>
        <table class="table table-bordered" id="datatable-2">
            <thead>
                <th>Tipe</th>
                <th>Kode</th>
                <th>Nama</th>
                <th>Dari Unit</th>
                <th>Ke Unit</th>
                <th>Kuantitas</th>
                <th>Aksi</th>
                <th hidden>idbarang</th>
                <th hidden>idtipe</th>
                <th hidden>dariunit</th>
                <th hidden>keunit</th>
            </thead>
            <tbody></tbody>
        </table>
        <form id="form2" class="form-horizontal" action="{site_url}tunitpermintaan/save" method="post">
            <input type="hidden" name="detailvalue">
            <div style="margin-top:50px;margin-bottom:20px;" class="text-right">
                <a href="{site_url}tunitpermintaan" class="btn btn-default btn-md">Kembali</a>
                <button type="submit" class="btn btn-success btn-md">Simpan</button>
            </div>
        </form>
    </div>
</div>

<div class="modal in" id="modal_list_barang" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
            <div class="block-header bg-success">
                <h3 class="block-title">{title}</h3>
            </div>
            <div class="block-content">
                <table class="table table-bordered" id="datatable-1">
                    <thead>
                        <th>Tipe</th>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Aksi</th>
                        <th hidden>id</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>

<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">

    var table;

    $(document).ready(function(){
        $('select[name=idtipe]').select2({
            data: [
                {id: ''},
                {id: 1, text: 'Alkes'},
                {id: 2, text: 'Implan'},
                {id: 3, text: 'Obat'},
                {id: 4, text: 'Logistik'},
            ],
            allowClear: true,
            placeholder: 'Cari Data ..'
        }).focus()
    })

    $('select[name=idtipe]').on('change', function(){
        var n = $(this).val()
        if(n !== '') {
            $('#modal_list_barang').modal('show')
            table = $('#datatable-1').DataTable({
                destroy: true,
                serverSide: true,
                searching: true,
                sort: false,
                ajax: {
                    url: '{site_url}tunitpermintaan/dtbarang/'+n,
                    type: 'post'
                },
                columns: [
                    {data: 'tipe'},
                    {data: 'kode'},
                    {data: 'nama'},
                    {defaultContent: '<a href="#" class="btn btn-xs btn-default">Pilih Barang</a>', searchable: false, },
                    {data: 'id', visible: false},
                ]
            })
            setTimeout(function() {
                $('div.dataTables_filter input').focus()
            }, 100);
        }
    })
    $('#datatable-1 tbody').on('click', 'a', function(){
        tr = table.row( $(this).parents('tr') ).index()

        $('input[name=namabarang]').val( table.cell(tr,2).data() )
        $('input[name=kodebarang]').val( table.cell(tr,1).data() )
        $('input[name=idbarang]').val( table.cell(tr,4).data() )

        $('#modal_list_barang').modal('hide')

        // dari unit
        $('select[name=dariunit]').select2({
            placeholder: 'Cari Data ... ',
            allowClear: true,
            ajax: {
                url: '{site_url}tunitpermintaan/selectdariunit',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {results:data}
                },
                cache: false,
            }
        })
        setTimeout(function() {
            $('select[name=dariunit]').focus()
        }, 100);
    })
    $('select[name=dariunit]').on('change', function(){
        var n = $(this).val();
        var idtipe = $('select[name=idtipe]').val();
        var idbarang = $('input[name=idbarang]').val();

        $.ajax({
            url: '{site_url}tunitpermintaan/ajax_stokunit/',
            method: 'GET',
            dataType: 'json',
            data: { idunit: n, idtipe: idtipe, idbarang: idbarang },
            success: function(res) {
                $('input[name=stokunit]').val(res)
            }
        })

        // ke unit
        $('select[name=keunit]').select2({
            placeholder: 'Cari Data ... ',
            allowClear: true,
            ajax: {
                url: '{site_url}tunitpermintaan/selectkeunit/'+n,
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {results:data}
                },
                cache: false,
            }
        })
        setTimeout(function() {
            $('select[name=keunit]').focus()
        }, 100);
    })
    $('select[name=keunit]').on('change', function(){
        setTimeout(function() {
            $('input[name=kuantitas]').focus()
        }, 100);
    })

    $('#form1').on('submit', (function(e){
        e.preventDefault();
        if(validate()) {
            table = $('#datatable-2').DataTable({
                autoWidth: false,
                info: false,
                sort: false,
                paging: false,
                searching: false,
                destroy: true,
            }).columns([7,8,9,10]).visible(false);
            table.row.add([
                $('select[name=idtipe] :selected').text(),
                $('input[name=kodebarang]').val(),
                $('input[name=namabarang]').val(),
                $('select[name=dariunit] :selected').text(),
                $('select[name=keunit] :selected').text(),
                $('input[name=kuantitas]').val(),
                '<a href="#"><i class="fa fa-trash-o"></i></a>',
                $('select[name=idtipe]').val(),
                $('input[name=idbarang]').val(),
                $('select[name=dariunit]').val(),
                $('select[name=keunit]').val(),
            ]).draw()
            detailvalue()
            clearform()
            setTimeout(function() {
                $('select[name=idtipe]').focus()
            }, 100);
        }
    }))

    function detailvalue() {
        var arr = $('#datatable-2').DataTable().data().toArray();
        $('input[name=detailvalue]').val( JSON.stringify(arr) )
    }
    function validate() {
        if($('select[name=idtipe]').val() == 0) {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Tipe tidak boleh kosong'});
            return false;
        }
        if($('input[name=kodebarang]').val() == '') {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Kode Barang tidak boleh kosong'});
            return false;
        }
        if($('select[name=dariunit]').val() == null ) {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Dari Unit tidak boleh kosong'});
            return false;
        }
        if($('select[name=keunit]').val() == null) {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Ke unit tidak boleh kosong'});
            return false;
        }
        if($('input[name=kuantitas]').val() == '' || $('input[name=kuantitas]').val() < 0) {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Kuantitas tidak boleh kosong'});
            return false;
        }
        if(parseFloat($('input[name=kuantitas]').val()) > parseFloat($('input[name=stokunit]').val())) {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Kuantitas tidak boleh lebih besar dari stok unit'});
            $('input[name=kuantitas]').val('').focus()
            return false;
        }
        return true;
    }
    function clearform() {
        $('select[name=idtipe]').val('').trigger('change')
        $('select[name=dariunit], select[name=keunit]').empty()
        $('input[name=kodebarang], input[name=namabarang], input[name=idbarang], input[name=stokunit], input[name=kuantitas]').val('')
    }
</script>
