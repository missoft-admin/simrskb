<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<form action="{site_url}tunitpermintaan/save_penerimaan/<?php echo $this->uri->segment(3) ?>" method="post" class="form-horizontal">
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">{title}</h3>
        </div>
        <div class="block-content">
            <table class="table table-bordered table-striped" id="datatable">
                <thead>
                    <tr>
                        <th>Tipe</th>
                        <th>Nama</th>
                        <th>Dari</th>
                        <th>Ke</th>
                        <th>Pesan</th>
                        <th>Dikirim</th>
                        <th>Dialihkan</th>
                        <th>Sisa</th>
                        <th>Diterima</th>
                        <th>Sisa</th>
                        <th>Terima</th>
                        <th>Aksi</th>
                        <th hidden>dariunit</th>
                        <th hidden>keunit</th>
                        <th hidden>kuantitaskirim</th>
                        <th hidden>idtipe</th>
                        <th hidden>idbarang</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
            <div class="form-group text-right">
                <div class="col-md-12">
                    <a href="{site_url}tunitpermintaan" class="btn btn-default" type="reset">Kembali</a>
                    <button class="btn btn-success" type="submit">Submit</button>
                </div>
            </div>
        </div>
    </div>
</form>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
    var table;

    $(document).ready(function(){
        table = $('#datatable').DataTable({
            autoWidth: false,
            info: false,
            searching: false,
            paging: false,
            sort: false,
            destroy: true,
            ajax: '{site_url}tunitpermintaan/dtpermintaan_detail_penerimaan/<?php echo $this->uri->segment(3) ?>',
            columns: [
                {data: "idtipe", width: '5%'},
                {data: "idbarang", width: '10%'},
                {data: "dariunit", width: '10%'},
                {data: "keunit", width: '10%'},
                {data: "kuantitas", width: '5%'},
                {data: "kuantitaskirim", width: '5%'},
                {data: "kuantitasalih", width: '5%'},
                {data: "kuantitassisa", width: '5%'},
                {defaultContent: 0, width: '5%'},
                {defaultContent: 0, width: '5%'},
                {defaultContent: 0, width: '5%'},
                {
                    defaultContent: '-', width: '10%',
                    render: function(data, type, row) {
                        var totalkirim = parseFloat(row.kuantitaskirim)+parseFloat(row.kuantitasalih);
                        if(totalkirim == row.kuantitas) {
                            return '-';
                        } else {
                            return '<a href="#" class="btn btn-xs btn-warning alihkan">Alihkan</a>';
                        }
                    }
                },
                {data: "idunitpeminta", visible: false}, // 9
                {data: "idunitpenerima", visible: false},
                {data: "kuantitaskirim", visible: false},
                {data: "idtipe", visible: false},
                {data: "idbarang", visible: false},
                {data: "id", visible: false},
            ],
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $.ajax({
                    url: '{site_url}tunitpermintaan/ajax_namabarang',
                    dataType: 'json',
                    method: 'post',
                    data: {idtipe: aData['idtipe'], idbarang: aData['idbarang']},
                    success: function(res) {
                        $('td:eq(1)', nRow).html(res)
                    }
                })

                // pernah terima
                $.ajax({
                    url: '{site_url}tunitpermintaan/ajax_pernah_terima',
                    dataType: 'json',
                    method: 'post',
                    data: {
                        idtipe: aData['idtipe'], 
                        idbarang: aData['idbarang'],
                        idpermintaan: '<?php echo $this->uri->segment(3) ?>',
                    },
                    success: function(res) {
                        var kuantitasterima = aData['kuantitaskirim']-res;

                        if(kuantitasterima <= 0) {
                            $('td:eq(8)', nRow).html(res)
                            var kuantitassisa = parseFloat(aData['kuantitas']) - res;
                            $('td:eq(9)', nRow).html( kuantitassisa )


                            $('td:eq(10)', nRow).html('<input type="text" name="kuantitasterima[]" value="'+ kuantitasterima +'" class="form-control form-xs" size="5" disabled><input type="hidden" name="idbarang[]" value="'+aData['idbarang']+'" disabled><input type="hidden" name="idtipe[]" value="'+aData['idtipe']+'" disabled>');
                        } else {
                            $('td:eq(8)', nRow).html(res)
                            var kuantitassisa = parseFloat(aData['kuantitas']) - res;
                            $('td:eq(9)', nRow).html( kuantitassisa )


                            $('td:eq(10)', nRow).html('<input type="text" name="kuantitasterima[]" value="'+ kuantitasterima +'" class="form-control form-xs" size="5"><input type="hidden" name="idbarang[]" value="'+aData['idbarang']+'"><input type="hidden" name="idtipe[]" value="'+aData['idtipe']+'">');
                        }

                    }
                })
                return nRow;
            }
        })

    })

    $('#datatable tbody').on('click', '.alihkan', function(){
        $('#modal_alihkan_permintaan').modal('show')

        tr = $(this).closest('tr');
        trRow = table.row( $(this).parents('tr') ).index();
        
        $('input[name=namabarang]').val( tr.find('td:eq(1)').text() )
        $('input[name=dariunit_text]').val( tr.find('td:eq(2)').text() )
        $('input[name=kuantitasalih]').val( tr.find('td:eq(6)').text() )
        $('input[name=dariunit]').val( table.cell(trRow,10).data() )
        $('input[name=idtipe]').val( table.cell(trRow,12).data() )
        $('input[name=idbarang]').val( table.cell(trRow,13).data() )
        $('input[name=iddaripermintaandetail]').val( table.cell(trRow,14).data() )

        var keunit = table.cell(trRow,10).data();

        // ke unit
        $('select[name=keunit]').select2({
            placeholder: 'Cari Data ... ',
            allowClear: true,
            ajax: {
                url: '{site_url}tunitpermintaan/selectkeunit/'+keunit,
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {results:data}
                },
                cache: false,
            }
        })        
    })

    $('#form2').on('submit', (function(e){
        e.preventDefault();
        var formData = new FormData(this)

        $.ajax({
            method:'POST',
            url: '{site_url}tunitpermintaan/save_pengalihan_permintaan',
            data: formData,
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            success:function(res){
                if(res.status == '200') {
                    swal('Sukses','Data berhasil disimpan!','success')
                    $('#modal_alihkan_permintaan').modal('hide')
                    setTimeout(function() {
                        $('#datatable').DataTable().ajax.reload()
                    }, 1000);
                } else {
                    swal('Kesalahan','Data gagal disimpan!','danger')
                }
            }
        })        
    }))

    function get_namabarang() {
        $.ajax({
            url: '{site_url}tunitpermintaan/ajax_namabarang',
            dataType: 'json',
            method: 'post',
            success: function(res) {
                return res;
            }
        })
    }
</script>