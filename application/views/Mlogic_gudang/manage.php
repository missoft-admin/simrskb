<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mlogic_gudang" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mlogic_gudang/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>
	
			<div class="form-group">
				<label class="col-md-2 control-label" for="Lokasi Tubuh"><?=text_primary('Proses Approval')?></label>
				
				<div class="col-md-10">
					<table id="tabel_detail" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 10%;">Tipe Bayar</th>								
								<th style="width: 10%;">Tipe Gudang</th>								
								<th style="width: 5%;">Step</th>
															
								<th style="width: 10%;">Nominal Transaksi</th>								
								<th style="width: 15%;">User </th>
								<th style="width: 10%;">Jika Setuju</th>								
								<th style="width: 10%;">Jika Tolak</th>	
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td>
									<select name="idtipe" style="width: 100%" id="idtipe" class="js-select2 form-control input-sm">										
										<option value="#">- Pilih Tipe Bayar -</option>
										<option value="1">TUNAI</option>										
										<option value="2">KREDIT</option>										
									</select>										
								</td>
								<td>
									<select name="tipepemesanan" style="width: 100%" id="tipepemesanan" class="js-select2 form-control input-sm">										
										<option value="#">- Pilih Tipe Gudang -</option>
										<option value="1">NON LOGISTIK</option>										
										<option value="2">LOGISTIK</option>										
									</select>										
								</td>
								<td>
									<select name="step" style="width: 100%" id="step" class="js-select2 form-control input-sm">
										<?for($i=1;$i<10;$i++){?>
											<option value="<?=$i?>"><?=$i?></option>
										<?}?>
									</select>										
								</td>
								
								
								
								<td>
									<select name="operand" style="width: 100%" id="operand" class="js-select2 form-control input-sm">					
										
										<option value=">">></option>										
										<option value=">=">>=</option>	
										<option value="<"><</option>
										<option value="<="><=</option>
										<option value="=">=</option>										
									</select>
									<input type="text" style="width: 100%"  class="form-control number" id="nominal" placeholder="0" name="nominal" value="0">
									<input type="hidden" class="form-control" id="id_edit" placeholder="0" name="id_edit" value="">
								</td>	
								<td>
									<select name="iduser" tabindex="16"  style="width: 100%" id="iduser" data-placeholder="User" class="js-select2 form-control input-sm"></select>										
								</td>
								<td>
									<select name="proses_setuju" style="width: 100%" id="proses_setuju" class="js-select2 form-control input-sm">										
										<option value="1">Langsung</option>
										<option value="2">Menunggu User</option>										
									</select>										
								</td>
								<td>
									<select name="proses_tolak" style="width: 100%" id="proses_tolak" class="js-select2 form-control input-sm">										
										<option value="1">Langsung</option>
										<option value="2">Menunggu User</option>										
									</select>										
								</td>
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_detail" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_edit" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						
				</table>
				</div>
			</div>
			
			
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		// load_unit();
		clear_all();
		// list_user();
		load_detail();
		// load_user();
	});
	
	$(document).on("change","#step,#idtipe,#tipepemesanan",function(){
		list_user();
	});
	function list_user(){
		// alert($("#idjenis").val());
		$('#iduser')
			.find('option')
			.remove()
			.end()
			.append('<option value="">- Pilih User -</option>')
			.val('').trigger("liszt:updated");
		if ($("#idtipe").val()!='#' && $("#step").val()!='#' && $("#tipepemesanan").val()!='#'){
			// console.log($("#id").val()+'/'+$("#step").val()+'/'+$("#idtipe").val()+'/'+$("#idjenis").val());
			$.ajax({
				url: '{site_url}mlogic_gudang/list_user/'+$("#step").val()+'/'+$("#idtipe").val()+'/'+$("#tipepemesanan").val(),
				dataType: "json",
				success: function(data) {

				$.each(data.detail, function (i,unit) {
				$('#iduser')
				.append('<option value="' + unit.id + '">' + unit.name + '</option>');
				});
				}
			});
		}else{
			$('#iduser')
			.find('option')
			.remove()
			.end()
			.append('<option value="">- Pilih User -</option>')
		}

	}
	
	function load_detail(){
		var idlogic=$("#id").val();
		
		$('#tabel_detail').DataTable().destroy();
		var table = $('#tabel_detail').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}mlogic_gudang/load_detail/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [8], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	$(document).on("click","#simpan_user",function(){
		
		if (validate_add_user()==false)return false;
		
		simpan_user();
		
	});
	$(document).on("click","#clear_edit",function(){
		clear_all();
		$("#id_edit").val('');
		clear_input_detail();
		$("#iduser").val(null).trigger('change');
	});
	function clear_all(){
		// list_unit();
		// list_user_bendahara();
		$("#id_edit").val('');
		$("#simpan_unit").attr('disabled', false);
	}
	function clear_input_detail(){
		$("#step").val('1').trigger('change');
		$("#idtipe").val('#').trigger('change');
		$("#tipepemesanan").val('#').trigger('change');
		// $("#idjenis").val('#').trigger('change');
		$("#operand").val('>=').trigger('change');
		list_user();
		$("#nominal").val(0);
	}
	$(document).on("click","#simpan_detail",function(){
		if (validate_add_detail()==false)return false;
		var id_edit=$("#id_edit").val();
		var iduser=$("#iduser").val();
		var step=$("#step").val();
		var proses_setuju=$("#proses_setuju").val();
		var proses_tolak=$("#proses_tolak").val();
		var idtipe=$("#idtipe").val();
		var operand=$("#operand").val();
		var nominal=$("#nominal").val();
		var tipepemesanan=$("#tipepemesanan").val();
		$.ajax({
			url: '{site_url}mlogic_gudang/simpan_detail',
			type: 'POST',
			data: {
				iduser:iduser,step:step,
				proses_setuju: proses_setuju,proses_tolak:proses_tolak,idtipe:idtipe,
				operand:operand,nominal:nominal,id_edit:id_edit,tipepemesanan:tipepemesanan
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Logic Berhasil disimpan'});
					$('#tabel_detail').DataTable().ajax.reload( null, false );
					clear_input_detail();
					$("#id_edit").val('');
				}else{
					sweetAlert("Gagal...", "Penyimpanan!", "error");
					
				}
			}
		});
		
		
	});
	
	$(document).on("click",".hapus_det",function(){
		var table = $('#tabel_detail').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,8).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menghapus Logic Detail ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mlogic_gudang/hapus_det',
				type: 'POST',
				data: {
					id: id	
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Detail Berhasil dihapus'});
					$('#tabel_detail').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	
	$(document).on("click",".edit",function(){
		// alert('SINI');
		table = $('#tabel_detail').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,8).data()
		$.ajax({
			url: '{site_url}mlogic_gudang/get_edit',
			type: 'POST',
			dataType: "json",
			data: {
				id: id
			},
			success: function(data) {
				// var item=data.detail;
				$("#id_edit").val(data.id);
				$("#step").val(data.step).trigger('change');
				$("#proses_setuju").val(data.proses_setuju).trigger('change');
				$("#proses_tolak").val(data.proses_tolak).trigger('change');
				$("#idtipe").val(data.idtipe).trigger('change');
				$("#operand").val(data.operand).trigger('change');
				$("#nominal").val(data.nominal).trigger('change');
				$("#tipepemesanan").val(data.tipepemesanan).trigger('change');
				var newOption = new Option(data.user_nama, data.iduser, true, true);
					
				$("#iduser").append(newOption);
				$("#iduser").val(data.iduser).trigger('change');

				console.log(data.id);
			}
		});
	});
	
	
	
	function validate_add_detail()
	{
		if ($("#iduser").val()=='' || $("#iduser").val()==null || $("#iduser").val()=="#"){
			sweetAlert("Maaf...", "User Harus diisi", "error");
			return false;
		}
		if ($("#tipepemesanan").val()=='' || $("#tipepemesanan").val()==null || $("#tipepemesanan").val()=="#"){
			sweetAlert("Maaf...", "Tipe Gudang Harus diisi", "error");
			return false;
		}
		if ($("#idtipe").val()=='' || $("#idtipe").val()==null || $("#idtipe").val()=="#"){
			sweetAlert("Maaf...", "Tipe Harus diisi", "error");
			return false;
		}
		
		if ($("#nominal").val()=='0'){
			if (($("#operand").val()!='>=' && $("#operand").val()!='>')){
			sweetAlert("Maaf...", "Nominal Harus diisi", "error");
			return false;
			}
		}
		
		return true;
	}
</script>
