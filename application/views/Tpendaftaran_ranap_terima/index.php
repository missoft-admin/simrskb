<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>
<?php $user_id = $this->session->userdata('user_id'); ?>
<?
?>
<div class="block">
    
			<input type="hidden" id="tab_detail" name="tab_detail" value="{tab_detail}">
   
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div id="div_expand" class="block block-opt-hidden">
				<div class="block-header bg-primary">
					<div class="block-options-simple">
							<button class="btn btn-xs btn-warning" onclick="click_expand()"  type="button" ><i class="fa fa-expand"></i></button>
						
					</div>
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 10px;">
							<div class="col-md-2">
								<label class="control-label" for="idtipe">TIPE</label>
								<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua  -</option>
									<option value="1" >RAWAT INAP</option>
									<option value="2" >ODS</option>
								</select>
							</div>
							<div class="col-md-2">
								<label class="control-label" for="idtipe_asal">Asal</label>
								<select id="idtipe_asal" name="idtipe_asal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua  -</option>
									<option value="1" >POLIKLINIK</option>
									<option value="2" >IGD</option>
								</select>
							</div>
							<div class="col-md-2">
								<label class="control-label" for="iddokter">Dokter</label>
								<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua Dokter -</option>
									<?foreach($list_dokter as $r){?>
									<option value="<?=$r->id?>"><?=$r->nama?></option>
									<?}?>
									
									
								</select>
							</div>
							<div class="col-md-3">
								<label class="control-label" for="tanggal_1">Tanggal Pendaftaran</label>
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" id="tanggal_1" name="tanggal_1" placeholder="From" value="{tanggal_1}"/>
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" id="tanggal_2" name="tanggal_2" placeholder="To" value="{tanggal_2}"/>
								</div>
							</div>
							<div class="col-md-3">
								<label class="control-label" for="cari_pasien">Pasien</label>
								<div class="input-group">
									<input type="text" class="form-control" id="cari_pasien" placeholder="Nama Pasien | No. Medrec" name="cari_pasien" value="">
									<span class="input-group-btn">
										<button class="btn btn-default" onclick="load_index_all()"  type="button"><i class="fa fa-search"></i> Search</button>
									</span>
								</div>
							</div>
						</div>
				</div>
					
				</div>
				<div class="block-content">
					
					<div class="row pull-10">
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="nopendaftaran">No Pendaftaran</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="nopendaftaran_index" placeholder="No Pendaftaran" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="idruangan_index">Ruangan</label>
								<div class="col-md-9">
									<select id="idruangan_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- Semua Ruangan -</option>
										<?foreach(get_all('mruangan',array('status'=>1)) as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="idkelas_index">Kelas</label>
								<div class="col-md-9">
									<select id="idkelas_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- Semua Kelas -</option>
										<?foreach(get_all('mkelas',array('status'=>1)) as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="idbed">Bed</label>
								<div class="col-md-9">
									<select id="idbed_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- Semua Bed -</option>
									</select>
								</div>
							</div>
							
						</div>
						<div class="col-md-6 push-10">
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">No Rujukan</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="norujukan_index" placeholder="No Rujukan" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="iddokter_perujuk_index">Dokter Perujuk</label>
								<div class="col-md-9">
									<select id="iddokter_perujuk_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- Semua Dokter -</option>
										<?foreach($list_dokter as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
										
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="idkelompokpasien_index">Kelompok Pasien</label>
								<div class="col-md-9">
									<select id="idkelompokpasien_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- Semua -</option>
										<?foreach(get_all('mpasien_kelompok',array('status'=>1)) as $r){?>
											<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="idrekanan_index">Perusahaan</label>
								<div class="col-md-9">
									<select id="idrekanan_index" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- Semua -</option>
										<?foreach(get_all('mrekanan',array('status'=>1)) as $r){?>
											<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="btn_filter_all"></label>
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="button" onclick="load_index_all()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
								</div>
							</div>
								
						</div>
					</div>
				</div>
			</div>
			
			<?php echo form_close() ?>
			
			<div class="block">
				<ul class="nav nav-pills">
					<li id="div_utama_1" class="<?=($tab_utama=='1'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab_utama(1)"><i class="si si-list"></i> Semua</a>
					</li>
					<li  id="div_utama_2" class="<?=($tab_utama=='2'?'active':'')?>">
						<a href="javascript:void(0)"  onclick="set_tab_utama(2)"><i class="si si-pin"></i> Menunggu Dirawat </a>
					</li>
					<li  id="div_utama_3" class="<?=($tab_utama=='3'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab_utama(3)"><i class="fa fa-bed"></i> Dirawat</a>
					</li>
					<li  id="div_utama_4" class="<?=($tab_utama=='4'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab_utama(4)"><i class="fa fa-times"></i> Dibatalkan</a>
					</li>
					<li  id="div_utama_5" class="<?=($tab_utama=='5'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab_utama(5)"><i class="si si-logout"></i> Pulang</a>
					</li>
					
				</ul>
				<div class="block-content">
						<input type="hidden" id="tab_utama" name="tab_utama" value="{tab_utama}">
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_all">
										<thead>
											<tr>
												<th width="10%"></th>
												<th width="10%"></th>
												<th width="10%"></th>
												<th width="10%"></th>
												<th width="10%"></th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					
				
				</div>
			</div>
</div>
<div class="modal in" id="modal_batal" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">PENGHAPUSAN </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<select tabindex="8" id="alasan_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Alasan</option>
										<?foreach(get_all('malasan_batal',array('status'=>1)) as $row){?>
										<option value="<?=$row->id?>" ><?=$row->keterangan?></option>
										<?}?>
										
									</select>
									<label for="alasan_id">Alasan Batal</label>
									<input type="hidden" id="pendaftaran_id_hapus" value="{pendaftaran_id_hapus}">
								</div>
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<textarea class="form-control" id="keterangan_hapus"  rows="3" placeholder="Isi keterangan"></textarea>
									<label for="keterangan_hapus">Keterangan</label>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-danger" type="button" onclick="hapus_record_daftar()" id="btn_hapus"><i class="fa fa-refresh"></i> Simpan Pembatalan</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?
	$waktuterima=date('H:i:s');
	$tanggalterima=date('d-m-Y');
?>
<div class="modal in" id="modal_terima" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Penerimaan Pasien Rawat Inap </h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-material">
									<div class="form-group" style="margin-bottom: 10px;">
										<label class="col-md-3 col-xs-12 push-5 control-label" for="tanggaldaftar">Tanggal Daftar <span style="color:red;">*</span></label>
										<div class="col-md-4 col-xs-6">
											<div class="input-group date">
												<input tabindex="2" type="text" class="js-datepicker form-control auto_blur" data-date-format="dd/mm/yyyy" id="tanggalterima" placeholder="HH/BB/TTTT" value="<?= $tanggalterima ?>" required>
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
										<div class="col-md-4 col-xs-6">
											<div class="input-group">
												<input tabindex="3" type="text" class="time-datepicker form-control auto_blur" id="waktuterima" name="waktuterima" value="<?= $waktuterima ?>" required>
												<span class="input-group-addon"><i class="si si-clock"></i></span>
											</div>
										</div>
									</div>
									<input type="hidden" id="pendaftaran_id_terima" value="">
								</div>
							</div>
							
						</div>
					</div>
					
				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-danger" type="button" onclick="terima_ranap()" id="btn_terima"><i class="fa fa-refresh"></i> Simpan</button>
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<? $this->load->view('Tpendaftaran_poli_ttv/modal_alergi')?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
var tab;
var tab_detail=<?=$tab_detail?>;
var tab_utama=<?=$tab_utama?>;
var st_login;
var st_expand=false;
$(document).ready(function() {
	load_index_all();
	startWorker();
});
function modal_terima(pendaftaran_id_terima){
	$("#pendaftaran_id_terima").val(pendaftaran_id_terima);
	$("#modal_terima").modal('show');
}
function terima_ranap(){
	let id=$("#pendaftaran_id_terima").val();
	let tanggalterima=$("#tanggalterima").val();
	let waktuterima=$("#waktuterima").val();
	
	$("#modal_terima").modal('hide');
	swal({
		title: "Apakah Anda Yakin ?",
		text : "Akan Menerima Rawat Inap?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpendaftaran_ranap/terima_ranap', 
			dataType: "JSON",
			method: "POST",
			data : {
					id:id,
					tanggalterima:tanggalterima,
					waktuterima:waktuterima,
				   },
			success: function(data) {
				
				$("#cover-spin").hide();
				if (data==null){
					swal({
						title: "Gagal!",
						text: "Hapus TTV.",
						type: "error",
						timer: 1500,
						showConfirmButton: false
					});

				}else{
					$("#modal_terima").modal('hide');
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					load_index_all();
				}
			}
		});
	});

}
$(document).on("change", "#idruangan_index,#idkelas_index", function() {
    get_bed_index();
});
function get_bed_index(){
	let ruangan_id=$("#idruangan_index").val();
	let idkelas=$("#idkelas_index").val();
	$.ajax({
		url: '{site_url}treservasi_bed/get_bed/',
		dataType: "json",
		type: "POST",
		data:{
			idruangan:ruangan_id,
			idkelas:idkelas,
		},
		success: function(data) {
			$("#idbed_index").empty();
			$('#idbed_index').append('<option value="#" selected>- Semua Bed -</option>');
			$('#idbed_index').append(data.detail);
		}
	});

}
function load_index_all(){
	$('#index_all').DataTable().destroy();	
	let tanggal_1=$("#tanggal_1").val();
	let tanggal_2=$("#tanggal_2").val();
	
	
	// alert(ruangan_id);
	table = $('#index_all').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			
			"processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "10%", "targets": 0},
					{ "width": "50%", "targets": 1},
					{ "width": "30%", "targets": 2},
					{ "width": "10%", "targets": 3},
					
				],
            ajax: { 
                url: '{site_url}tpendaftaran_ranap/getIndex_all', 
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						tab:tab_utama,
						iddokter:$("#iddokter").val(),
						idtipe:$("#idtipe").val(),
						idtipe_asal:$("#idtipe_asal").val(),
						cari_pasien:$("#cari_pasien").val(),
						nopendaftaran:$("#nopendaftaran_index").val(),
						idruangan:$("#idruangan_index").val(),
						idkelas:$("#idkelas_index").val(),
						idbed:$("#idbed_index").val(),
						norujukan:$("#norujukan_index").val(),
						iddokter_perujuk:$("#iddokter_perujuk_index").val(),
						idkelompokpasien:$("#idkelompokpasien_index").val(),
						idrekanan:$("#idrekanan_index").val(),
						
					   }
            },
			"drawCallback": function( settings ) {
				 $("#index_all thead").remove();
			 }  
        });
	$("#cover-spin").hide();
}
function click_expand(){
	// 
	if (st_expand==false){
		$("#div_expand").removeClass("block-opt-hidden");
		st_expand=true;
	}else{
		$("#div_expand").addClass("block-opt-hidden");
		st_expand=false;
	}
}
function set_tab($tab) {
    tab_detail = $tab;
    // alert(tab);
    // $("#cover-spin").show();
    document.getElementById("div_1").classList.remove("active");
    document.getElementById("div_2").classList.remove("active");
    document.getElementById("div_3").classList.remove("active");
    document.getElementById("div_4").classList.remove("active");
    if (tab_detail == '1') {
        document.getElementById("div_1").classList.add("active");
    }
    if (tab_detail == '2') {
        document.getElementById("div_2").classList.add("active");
    }
    if (tab_detail == '3') {
        document.getElementById("div_3").classList.add("active");
    }
	if (tab_detail == '4') {
        document.getElementById("div_4").classList.add("active");
    }
	
    getIndex_reservasi();
}
function set_tab_utama($tab) {
    tab_utama = $tab;
    // alert(tab);
    // $("#cover-spin").show();
    document.getElementById("div_utama_1").classList.remove("active");
    document.getElementById("div_utama_2").classList.remove("active");
    document.getElementById("div_utama_3").classList.remove("active");
    document.getElementById("div_utama_4").classList.remove("active");
    document.getElementById("div_utama_5").classList.remove("active");
    if (tab_utama == '1') {
        document.getElementById("div_utama_1").classList.add("active");
    }
    if (tab_utama == '2') {
        document.getElementById("div_utama_2").classList.add("active");
    }
    if (tab_utama == '3') {
        document.getElementById("div_utama_3").classList.add("active");
    }
	if (tab_utama == '4') {
        document.getElementById("div_utama_4").classList.add("active");
    }
	if (tab_utama == '5') {
        document.getElementById("div_utama_5").classList.add("active");
    }
	
    load_index_all();
}

function batal(id){
	swal({
		title: "Anda Yakin ?",
		text : "Membatalkan Pendaftaran ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tpendaftaran_ranap/batal', 
			dataType: "JSON",
			method: "POST",
			data : {
					id :  id,
				},
			success: function(data) {
				$("#cover-spin").hide();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Proses Berhasil Disimpan'});
				$('#index_ranap').DataTable().ajax.reload( null, false );
			
			}
		});
	});
}
function show_modal_batal(id){
	$("#modal_batal").modal('show');
	$("#pendaftaran_id_hapus").val(id);
}
function hapus_record_daftar(){
		let id=$("#pendaftaran_id_hapus").val();
		let keterangan_hapus=$("#keterangan_hapus").val();
		let alasan_id=$("#alasan_id").val();
		
		
		if (alasan_id=='' || alasan_id==null){
			swal({
				title: "Gagal!",
				text: "Tentukan Alasan.",
				type: "error",
				timer: 1000,
				showConfirmButton: false
			});

			return false;
		}
		$("#modal_hapus").modal('hide');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Membatalkan Pendaftaran ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}tpendaftaran_ranap/hapus_record_daftar', 
				dataType: "JSON",
				method: "POST",
				data : {
						id:id,
						keterangan_hapus:keterangan_hapus,
						alasan_id:alasan_id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					if (data==null){
						swal({
							title: "Gagal!",
							text: "Hapus TTV.",
							type: "error",
							timer: 1500,
							showConfirmButton: false
						});

					}else{
						$("#modal_batal").modal('hide');
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						load_index_all();
					}
				}
			});
		});

	}
	function load_data_play(){
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_terima/get_notif', 
			dataType: "JSON",
			method: "POST",
			data : {},
			success: function(data) {
				if (data.sound_play!=null){
					play_sound_arr(data.sound_play);
					update_notif(data.id);
					$('#index_all').DataTable().ajax.reload( null, false );
				}
				// $("#arr_sound").val(data.file);
				
			}
		});

	}
	function update_notif(id){
		$.ajax({
			url: '{site_url}tpendaftaran_ranap_terima/update_notif', 
			dataType: "JSON",
			method: "POST",
			data : {id:id},
			success: function(data) {
				
			}
		});
	}
	function play_sound_arr(str_file){
		// let str_file=$("#arr_sound").val();
		var myArray = str_file.split(":");
		console.log(myArray[0]);
		queue_sounds(myArray);
	}
	function queue_sounds(sounds){
		// $config['upload_path'] = './assets/upload/sound_antrian/';
		let path_sound='{site_url}assets/upload/sound_antrian/';
		
		var index = 0;
		function recursive_play()
		{
			let file_sound = new Audio(path_sound+sounds[index]);
		  if(index+1 === sounds.length)
		  {
			play(file_sound);
		  }
		  else
		  {
			play(file_sound,function(){
				index++; recursive_play();
				});
		  }
		}

	recursive_play();   
	}
	 function play(audio, callback) {

		audio.play();
		if(callback)
		{
			audio.onended = callback;
		}
	}
	function startWorker() {
		if(typeof(Worker) !== "undefined") {
			if(typeof(w) == "undefined") {
				w = new Worker("{site_url}assets/js/worker.js");
		
			}
			w.postMessage({'cmd': 'start' ,'msg': 5000});
			w.onmessage = function(event) {
				load_data_play();
			};
			w.onerror = function(event) {
				window.location.reload();
			};
		} else {
			alert("Sorry, your browser does not support Autosave Mode");
			$("#autosave_status").html('<button class="btn btn-warning"><i class="fa fa-warning"></i> Autosave not running!</button>');
		}
	 }
</script>
