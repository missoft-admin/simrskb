<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mvendor" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mvendor/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama</label>
				<div class="col-md-7">
					<input type="hidden" class="form-control" id="idvendor" placeholder="idvendor" name="idvendor" value="{id}" required="" aria-required="true">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="alamat">Alamat</label>
				<div class="col-md-7">
					<textarea class="form-control" id="alamat" placeholder="Alamat" name="alamat" required="" aria-required="true">{alamat}</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="telepon">Telepon</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="telepon" placeholder="Telepon" name="telepon" value="{telepon}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group" <?=($id?'hidden':'')?>>
				<label class="col-md-3 control-label" for="bank">Bank</label>
				<div class="col-md-7">
					<select name="bank_id" id="bank_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="#" <?=($bank_id=="#"?'selected':'')?> >-Tidak Memiliki Rekening-</option>
						<? foreach(get_all('ref_bank') as $row){ ?>										
							<option <?=($bank_id==$row->id?'selected':'')?> value="<?=$row->id?>"><?=$row->bank?></option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group" <?=($id==''?'hidden':'')?>>
				<label class="col-md-3 control-label" for="bank">Bank</label>
				<div class="col-md-7">
					<input type="text"  <?=($id?'readonly':'')?>  class="form-control" id="bank" placeholder="Bank" name="bank" value="{bank}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="norekening">No. Rekening</label>
				<div class="col-md-7">
					<div class="input-group">
						<input type="text" <?=($id?'readonly':'')?> class="form-control" id="norekening" placeholder="No. Rekening" name="norekening" value="{norekening}" required="" aria-required="true">
						<span class="input-group-btn">
							<button class="btn btn-primary" <?=($id==''?'disabled':'')?>  type="button" id="btn_add_rekening" title="Rekening Vendor"><i class="fa fa-credit-card"></i></button>
						</span>
					</div>	
					
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="atasnama">Atas Nama</label>
				<div class="col-md-7">
					<input type="text" <?=($id?'readonly':'')?> class="form-control" id="atasnama" placeholder="Atas Nama" name="atasnama" value="{atasnama}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="atasnama">Cabang</label>
				<div class="col-md-7">
					<input type="text" <?=($id?'readonly':'')?> class="form-control" id="cabang" placeholder="Cabang" name="cabang" value="{cabang}" required="" aria-required="true">
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mvendor" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>
<?$this->load->view('Mvendor/modal_vendor')?>
<script type="text/javascript">
	$(document).on("change","#bank_id",function(){		
		if ($("#bank_id").val()!='#'){
			$("#bank").val($("#bank_id option:selected").text());
		}else{
			$("#bank").val('');
		}
		
	});
</script>