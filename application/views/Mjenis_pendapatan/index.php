<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('2566'))){ ?>
		<li class="active">
			<a href="#tab_info"><i class="si si-settings"></i> Settings</a>
		</li>
		<?}?>
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		
		<?php if (UserAccesForm($user_acces_form,array('2566'))){ ?>
		<div class="tab-pane fade fade-left active in" id="tab_info">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_primary('JENIS PENDAPATAN')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_pendapatan">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="30%">Nama Pendapatan</th>
										<th width="25%">Debit</th>
										<th width="25%">Kredit</th>
										<th width="15%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<input type="text" style="width: 100%"  class="form-control" id="nama_pendapatan" placeholder="Jenis Pendapatan" name="nama_pendapatan" value="">
											<input type="hidden" class="form-control" id="pendapatan_id" value="">
										</th>
										<th>
											<select id="idakun_pendapatan" name="idakun_pendapatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="" <?=($idakun_pendapatan==''? 'selected' : ''); ?>>Pilih No Akun</option>
												<?foreach($list_akun as $r){?>
													<option value="<?=$r->id?>" <?=($r->id == $idakun_pendapatan? 'selected' : ''); ?>><?=$r->noakun.' - '.$r->namaakun.' ('.$r->id.')'?></option>
												<?}?>
											</select>
											
										</th>
										<th>
											<select id="idakun_pendapatan_kredit" name="idakun_pendapatan_kredit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="" <?=($idakun_pendapatan_kredit==''? 'selected' : ''); ?>>Pilih No Akun</option>
												<?foreach($list_akun as $r){?>
													<option value="<?=$r->id?>" <?=($r->id == $idakun_pendapatan_kredit? 'selected' : ''); ?>><?=$r->noakun.' - '.$r->namaakun.' ('.$r->id.')'?></option>
												<?}?>
											</select>
											
										</th>
										
										<th>
											<?php if (UserAccesForm($user_acces_form,array('2567','2568'))){ ?>
											<button class="btn btn-primary btn-sm" onclick="add_pendapatan()" type="button" id="btn_tambah_pendapatan"><i class="fa fa-save"></i> Simpan</button>
											<button class="btn btn-warning btn-sm" onclick="clear_pendapatan()" type="button" id="btn_clear_pendapatan"><i class="fa fa-refresh"></i> Clear</button>
											<?}?>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
					</div>
				</div>
				</div>
			</div>
			<div class="row">
				
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
					<h5><?=text_danger('JENIS LOSS')?></h5>
					</div>
				</div>
				</div>
				<div class="col-md-12">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_loss">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="30%">Nama Loss</th>
										<th width="25%">Debit</th>
										<th width="25%">Kredit</th>
										<th width="15%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<input type="text" style="width: 100%"  class="form-control" id="nama_loss" placeholder="Jenis Loss" name="nama_loss" value="">
											<input type="hidden" class="form-control" id="loss_id" value="">
										</th>
										<th>
											<select id="idakun_loss" name="idakun_loss" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="" <?=($idakun_loss==''? 'selected' : ''); ?>>Pilih No Akun</option>
												<?foreach($list_akun as $r){?>
													<option value="<?=$r->id?>" <?=($r->id == $idakun_loss? 'selected' : ''); ?>><?=$r->noakun.' - '.$r->namaakun.' ('.$r->id.')'?></option>
												<?}?>
											</select>
										</th>
										<th>
											<select id="idakun_loss_kredit" name="idakun_loss_kredit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="" <?=($idakun_loss_kredit==''? 'selected' : ''); ?>>Pilih No Akun</option>
												<?foreach($list_akun as $r){?>
													<option value="<?=$r->id?>" <?=($r->id == $idakun_loss_kredit? 'selected' : ''); ?>><?=$r->noakun.' - '.$r->namaakun.' ('.$r->id.')'?></option>
												<?}?>
											</select>
										</th>
										
										<th>
											<?php if (UserAccesForm($user_acces_form,array('2567','2568'))){ ?>
											<button class="btn btn-primary btn-sm" onclick="add_loss()" type="button" id="btn_tambah_loss"><i class="fa fa-save"></i> Simpan</button>
											<button class="btn btn-warning btn-sm" onclick="clear_loss()" type="button" id="btn_clear_loss"><i class="fa fa-refresh"></i> Clear</button>
											<?}?>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>					
					</div>
				</div>
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
			
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
	$(".number").number(true,0,'.',',');
	load_pendapatan();
	load_loss();
})	
// LOGIC
function load_pendapatan(){
	$('#index_pendapatan').DataTable().destroy();	
	table = $('#index_pendapatan').DataTable({
            autoWidth: false,
		searching: true,
		pageLength: 100,
		serverSide: true,
		"processing": true,
		"order": [],
		"ordering": false,
		// "columnDefs": [
				// { "width": "5%", "targets": 0,  className: "text-right" },
				// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
				// { "width": "12%", "targets": [6,7],  className: "text-center" },
				// { "width": "30%", "targets": 3,  className: "text-left" },
			// ],
		ajax: { 
			url: '{site_url}mjenis_pendapatan/load_pendapatan', 
			type: "POST" ,
			dataType: 'json',
			data : {
				   }
		}
	});
}
function clear_pendapatan(){
	$("#pendapatan_id").val('');
	$("#nama_pendapatan").val('');
	$("#idakun_pendapatan").val('').trigger('change');
	$("#idakun_pendapatan_kredit").val('').trigger('change');
}
function add_pendapatan(){
	let nama_jenis=$("#nama_pendapatan").val();
	let idakun=$("#idakun_pendapatan").val();
	let idakun_kredit=$("#idakun_pendapatan_kredit").val();
	let pendapatan_id=$("#pendapatan_id").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mjenis_pendapatan/simpan_pendapatan', 
		dataType: "JSON",
		method: "POST",
		data : {
				pendapatan_id:pendapatan_id,
				nama_jenis:nama_jenis,
				idakun:idakun,
				idakun_kredit:idakun_kredit,
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				clear_pendapatan();
				$('#index_pendapatan').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Gagal Simpan Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});
}

function hapus_pendapatan(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Data?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mjenis_pendapatan/hapus_pendapatan',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_pendapatan').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function edit_pendapatan(id){
	
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mjenis_pendapatan/edit_pendapatan',
				type: 'POST',
				dataType: "JSON",
				data: {id: id},
				success: function(data) {
					// alert(data.id);
					$("#cover-spin").hide();
					$("#pendapatan_id").val(data.id);
					$("#nama_pendapatan").val(data.nama_jenis);
					$("#idakun_pendapatan").val(data.idakun).trigger('change');
				}
			});
}
function load_loss(){
	$('#index_loss').DataTable().destroy();	
	table = $('#index_loss').DataTable({
            autoWidth: false,
		searching: true,
		pageLength: 100,
		serverSide: true,
		"processing": true,
		"order": [],
		"ordering": false,
		// "columnDefs": [
				// { "width": "5%", "targets": 0,  className: "text-right" },
				// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
				// { "width": "12%", "targets": [6,7],  className: "text-center" },
				// { "width": "30%", "targets": 3,  className: "text-left" },
			// ],
		ajax: { 
			url: '{site_url}mjenis_pendapatan/load_loss', 
			type: "POST" ,
			dataType: 'json',
			data : {
				   }
		}
	});
}
function clear_loss(){
	$("#loss_id").val('');
	$("#nama_loss").val('');
	$("#idakun_loss").val('').trigger('change');
	$("#idakun_loss_kredit").val('').trigger('change');
}
function add_loss(){
	let nama_jenis=$("#nama_loss").val();
	let idakun=$("#idakun_loss").val();
	let idakun_kredit=$("#idakun_loss_kredit").val();
	let loss_id=$("#loss_id").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mjenis_pendapatan/simpan_loss', 
		dataType: "JSON",
		method: "POST",
		data : {
				loss_id:loss_id,
				nama_jenis:nama_jenis,
				idakun:idakun,
				idakun_kredit:idakun_kredit,
			},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==true){
				clear_loss();
				$('#index_loss').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}else{
				swal({
					title: "Data Error!",
					text: "Gagal Simpan Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}
		}
	});
}

function hapus_loss(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Data?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mjenis_pendapatan/hapus_loss',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_loss').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function edit_loss(id){
	
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mjenis_pendapatan/edit_loss',
				type: 'POST',
				dataType: "JSON",
				data: {id: id},
				success: function(data) {
					// alert(data.id);
					$("#cover-spin").hide();
					$("#loss_id").val(data.id);
					$("#nama_loss").val(data.nama_jenis);
					$("#idakun_loss").val(data.idakun).trigger('change');
				}
			});
}
</script>