<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}msetting_jurnal_refund" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('msetting_jurnal_refund/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>	
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_primary('Proses Posting')?></label>
				<input type="hidden" class="form-control" id="tmp_idakun_kredit" placeholder="0" name="tmp_idakun_kredit" value="{idakun_kredit}">
				<div class="col-md-4">
					<select id="st_auto_posting" name="st_auto_posting" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="1" <?=(1 == $st_auto_posting ? 'selected' : ''); ?>>AUTO POSTING</option>
                            <option value="2" <?=(2 == $st_auto_posting ? 'selected' : ''); ?>>MANUAL</option>
                        </select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_primary('Batas Waktu Batal')?></label>
				
				<div class="col-md-4">
					<input type="text" style="width: 100%"  class="form-control number" id="batas_batal" placeholder="0" name="batas_batal" value="<?=$batas_batal?>">
				</div>
				<div class="col-md-2">
					<button type="button" class="btn btn-sm btn-success" tabindex="8" id="btn_update" title="Update"><i class="fa fa-save"></i> UPDATE</button>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-1 control-label"><h4><?=text_success('SETTING REFUND DEPOSIT')?></h4></label>
				
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<table id="tabel_deposit" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 0%;">#</th>															
								<th style="width: 20%;">Metode </th>															
								<th style="width: 25%;">No Akun </th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td></td>
								<td>
									<select name="idmetode" id="idmetode" style="width: 100%" id="step" class="js-select2 form-control input-sm">										
										<option value="#" selected>- Pilih -</option>																			
										<option value="1" >Tunai</option>																			
										<option value="2" >Debit</option>																			
										<option value="3" >Kredit</option>																			
										<option value="4" >Transfer</option>														
										
									</select>										
								</td>								
								
								<td>
									<select name="idakun_deposit" id="idakun_deposit" style="width: 100%" class="js-select2 form-control input-sm idakun">										
										
									</select>
									<input type="hidden" class="form-control" id="id_edit_deposit" placeholder="0" name="id_edit_deposit" value="">
								</td>	
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_deposit" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_deposit" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						
				</table>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-1 control-label"><h4><?=text_primary('SETTING REFUND OBAT')?></h4></label>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<table id="tabel_obat" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 0%;">#</th>															
								<th style="width: 7%;">Metode </th>															
								<th style="width: 7%;">Tipe </th>															
								<th style="width: 10%;">Kategori</th>								
								<th style="width: 15%;">Barang</th>								
								<th style="width: 24%;">No Akun Penjualan </th>
								<th style="width: 24%;">No Akun Diskon</th>
								<th style="width: 7%;">Actions</th>
							</tr>
							<tr>								
								<td></td>
								<td>
									<select name="idmetode_obat" id="idmetode_obat" style="width: 100%" id="step" class="js-select2 form-control input-sm">										
										<option value="#" selected>- Pilih -</option>																			
										<option value="1" >Tunai</option>																			
										<option value="2" >Debit</option>																			
										<option value="3" >Kredit</option>																			
										<option value="4" >Transfer</option>														
										
									</select>										
								</td>
								<td>
									<select name="idtipe_obat" id="idtipe_obat" style="width: 100%" id="step" class="js-select2 form-control input-sm">										
										<option value="#" selected>- Tipe -</option>																			
										<?foreach(get_all('mdata_tipebarang',array(),'id','ASC') as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama_tipe?></option>																				
										<?}?>
									</select>										
								</td>								
								<td>
									<select name="idkategori_obat" id="idkategori_obat" style="width: 100%" class="js-select2 form-control input-sm">										
										<option value="0" selected>- Semua Kategori -</option>																			
									</select>										
								</td>
								<td>
									<select name="idbarang_obat" id="idbarang_obat" style="width: 100%" class="form-control input-sm">										
										<option value="0" selected>- Semua Barang -</option>																			
									</select>										
								</td>
								
								<td>
									<select name="idakun_obat" id="idakun_obat" style="width: 100%" class="js-select2 form-control input-sm idakun">										
										
									</select>
									<input type="hidden" class="form-control" id="id_edit_obat" placeholder="0" name="id_edit_obat" value="">
								</td>
								<td>
									<select name="idakun_diskon" id="idakun_diskon" style="width: 100%" class="js-select2 form-control input-sm idakun">										
										
									</select>
								</td>	
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_obat" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_obat" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody></tbody>			
					</table>
				</div>
			</div>
			
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		// load_unit();
		list_akun();
		load_deposit();
		load_obat();
		clear_input_deposit();
		clear_input_obat();
		setTimeout(function() {
			$("#idakun_kredit").val($("#tmp_idakun_kredit").val()).trigger('change');
		}, 500);
		// load_deposit();
	});	
	$(document).on("click","#btn_update",function(){
		if (validate_update()==false)return false;
		var st_auto_posting=$("#st_auto_posting").val();
		var idakun_kredit=$("#idakun_kredit").val();
		var batas_batal=$("#batas_batal").val();
		$("#tmp_idakun_kredit").val(idakun_kredit)
		$.ajax({
			url: '{site_url}msetting_jurnal_refund/update',
			type: 'POST',
			data: {
				st_auto_posting:st_auto_posting,idakun_kredit:idakun_kredit,batas_batal:batas_batal,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$("#idakun_kredit").val($("#tmp_idakun_kredit").val()).trigger('change');
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function validate_update()
	{
		
		if ($("#st_auto_posting").val()=='#'){
			sweetAlert("Maaf...", "Setting Harus diisi", "error");
			return false;
		}
		if ($("#batas_batal").val()==''){
			sweetAlert("Maaf...", "Batas Waktu Batal Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function list_akun(){
		$.ajax({
			url: '{site_url}msetting_jurnal_refund/list_akun',
			dataType: "json",
			success: function(data) {
				$(".idakun").empty();
				$('.idakun').append('<option value="#" selected>- Pilih Akun -</option>');
				$('.idakun').append(data.detail);
				
			}
		});		
	}
	
	
	function list_barang_deposit(){
		var newOption = new Option('- Semua Barang -', '0', true, true);					
		$("#idbarang_deposit").append(newOption);
		$("#idbarang_deposit").val("0").trigger('change');
		
		var idtipe=$("#idmetode").val();
		var idkategori=$("#idkategori_deposit").val();
		$("#idbarang_deposit").select2({
			minimumInputLength: 0,
			noResults: 'Obat Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}msetting_jurnal_refund/list_barang/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,idtipe: idtipe,idkategori:idkategori,
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.kode+' - '+item.nama,
								id: item.id,
								idtipe : item.idtipe
							}
						})
					};
				}
			}
		});

	}
	
	function validate_add_deposit()
	{
		
		if ($("#idmetode").val()=='#'){
			sweetAlert("Maaf...", "Metode Harus diisi", "error");
			return false;
		}
		if ($("#idakun_deposit").val()=='#'){
			sweetAlert("Maaf...", "No AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_deposit(){
		$("#idmetode").val('#').trigger('change');	
			
		$("#id_edit_deposit").val('');		
		$("#idakun_deposit").val('#').trigger('change');
		// alert($("#idakun").val());
	}
	$(document).on("click","#simpan_deposit",function(){
		if (validate_add_deposit()==false)return false;
		var id_edit=$("#id_edit_deposit").val();
		var idmetode=$("#idmetode").val();
		var idakun=$("#idakun_deposit").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_refund/simpan_deposit',
			type: 'POST',
			data: {
				id_edit:id_edit,idmetode:idmetode,
				idakun:idakun,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_deposit').DataTable().ajax.reload( null, false );
					clear_input_deposit();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	function hapus_deposit($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_refund/hapus_deposit/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_deposit').DataTable().ajax.reload( null, false );
						clear_input_deposit();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_deposit",function(){
		
		clear_input_deposit();
	});
	function load_deposit(){
		var idlogic=$("#id").val();
		
		$('#tabel_deposit').DataTable().destroy();
		var table = $('#tabel_deposit').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_refund/load_deposit/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	//PPN
	function list_barang_obat(){
		var newOption = new Option('- Semua Barang -', '0', true, true);					
		$("#idbarang_obat").append(newOption);
		$("#idbarang_obat").val("0").trigger('change');
		var idtipe=$("#idtipe_obat").val();
		var idkategori=$("#idkategori_obat").val();
		$("#idbarang_obat").select2({
			minimumInputLength: 0,
			noResults: 'Obat Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}msetting_jurnal_refund/list_barang/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,idtipe: idtipe,idkategori:idkategori,
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.kode+' - '+item.nama,
								id: item.id,
								idtipe : item.idtipe
							}
						})
					};
				}
			}
		});

	}
	$(document).on("change","#idtipe_obat",function(){
		list_kategori_obat($(this).val());
		list_barang_obat();
	});
	$(document).on("change","#idkategori_obat",function(){
		list_barang_obat();
	});
	function list_kategori_obat($id){
		if ($id!='#'){
			$.ajax({
				url: '{site_url}msetting_jurnal_refund/list_kategori/'+$id,
				dataType: "json",
				success: function(data) {
					$("#idkategori_obat").empty();
					$('#idkategori_obat').append('<option value="0" selected>- Semua Kategori -</option>');
					$('#idkategori_obat').append(data.detail);
				}
			});
		}else{
			$("#idkategori_obat").empty();
			$('#idkategori_obat').append('<option value="0" selected>- Semua Kategori -</option>');
		}		
	}
	function validate_add_obat()
	{
		
		if ($("#idmetode_obat").val()=='#'){
			sweetAlert("Maaf...", "Metode Harus diisi", "error");
			return false;
		}
		if ($("#idtipe_obat").val()=='#'){
			sweetAlert("Maaf...", "Tipe Harus diisi", "error");
			return false;
		}
		if ($("#idakun_obat").val()=='#'){
			sweetAlert("Maaf...", "No AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	
	function clear_input_obat(){
		$("#idmetode_obat").val('#').trigger('change');	
		$("#idtipe_obat").val('#').trigger('change');	
		var newOption = new Option('- Semua Barang -', '0', true, true);					
		$("#idbarang_obat").append(newOption);
		$("#idbarang_obat").val("0").trigger('change');


		// $("#idbarang_obat").val('#').trigger('change');		
		$("#id_edit_obat").val('');		
		// $("#idakun_obat").val('#').trigger('change');
		// alert($("#idakun").val());
	}
	$(document).on("click","#simpan_obat",function(){
		if (validate_add_obat()==false)return false;
		var id_edit=$("#id_edit_obat").val();
		var idmetode=$("#idmetode_obat").val();
		var idtipe=$("#idtipe_obat").val();
		var idkategori=$("#idkategori_obat").val();
		var idbarang=$("#idbarang_obat").val();
		var idakun=$("#idakun_obat").val();
		var idakun_diskon=$("#idakun_diskon").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_refund/simpan_obat',
			type: 'POST',
			data: {
				id_edit:id_edit,idmetode:idmetode,idtipe:idtipe,
				idkategori: idkategori,idbarang:idbarang,idakun:idakun,idakun_diskon:idakun_diskon,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_obat').DataTable().ajax.reload( null, false );
					clear_input_obat();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function hapus_obat($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_refund/hapus_obat/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_obat').DataTable().ajax.reload( null, false );
						clear_input_obat();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_obat",function(){
		
		clear_input_obat();
	});
	function load_obat(){
		var idlogic=$("#id").val();
		
		$('#tabel_obat').DataTable().destroy();
		var table = $('#tabel_obat').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_refund/load_obat/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}

	
</script>
