<style>
	.edited_final{
		border-color:#d26a5c;
		background-color:#fff2f1!important;
	}
	.block > .nav-tabs > li.active > a, .block > .nav-tabs > li.active > a:hover, .block > .nav-tabs > li.active > a:focus {
		color: #fff;
		background-color: #57c1d1;
		border-color: transparent;
	}
	  .select2-custom-container {
            display: flex;
            align-items: center;
        }

        .select2-custom-image {
            border-radius: 50%;
            margin-right: 10px;
        }

        .select2-custom-info {
            display: flex;
            flex-direction: column;
        }

        .select2-custom-info span {
            margin-bottom: 5px;
        }
</style>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php 
	$st_info='';$d_info='';
	$st_alok='';$d_alok='';
	$st_margin='';$d_margin='';
	$st_harga='';$d_harga='';
	if ($id==''){
		if (UserAccesForm($user_acces_form,array('109'))=='0'){
			$st_info='readonly';
			$d_info='disabled';
		}
		if (UserAccesForm($user_acces_form,array('110'))=='0'){
			$st_alok='readonly';
			$d_alok='disabled';
		}
		if (UserAccesForm($user_acces_form,array('111'))=='0'){
			$st_margin='readonly';
			$d_margin='disabled';
		}
		if (UserAccesForm($user_acces_form,array('112'))=='0'){
			$st_harga='readonly';
			$d_harga='disabled';
		}
	}else{
		if (UserAccesForm($user_acces_form,array('113'))=='0'){
			$st_info='readonly';
			$d_info='disabled';
			
		}
		if (UserAccesForm($user_acces_form,array('114'))=='0'){
			$st_alok='readonly';
			$d_alok='disabled';
		}
		if (UserAccesForm($user_acces_form,array('115'))=='0'){
			$st_margin='readonly';
			$d_margin='disabled';
		}
		if (UserAccesForm($user_acces_form,array('116'))=='0'){
			$st_harga='readonly';
			$d_harga='disabled';
		}
	}
	

?>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mdata_obat_new" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?php echo form_open('mdata_obat_new/save','class="form-horizontal push-10-t"') ?>
			<div class="block">
			<input type="hidden" id="tab" name="tab" value="{tab}">
				<ul class="nav nav-tabs" data-toggle="tabs">
					
					<li class="<?=($tab=='1'?'active':'')?>">
						<a href="#tab_1">Data Barang</a>
					</li>
					<li class="<?=($tab=='2'?'active':'')?>">
						<a href="#tab_2">Margin</a>
					</li>
					<?if ($id){?>
					<?php if (UserAccesForm($user_acces_form,array('1804'))){ ?>
					<li class="<?=($tab=='3'?'active':'')?>">
						<a href="#tab_3" onclick="load_history_harga()">History Harga</a>
					</li>
					<?}?>
					<?php if (UserAccesForm($user_acces_form,array('1805'))){ ?>
					<li class="<?=($tab=='4'?'active':'')?>">
						<a href="#tab_4">History Barang</a>
					</li>
					<?}?>
					<?php if (UserAccesForm($user_acces_form,array('1806'))){ ?>
					<li class="<?=($tab=='5'?'active':'')?>">
						<a href="#tab_5" onclick="load_pembelian()">History Pembelian</a>
					</li>
					<?}?>
					<?}?>
				</ul>
				<div class="block-content tab-content">
					<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_1">
						<div class="form-group" style="margin-bottom:10px">
							<div class="col-md-12">
								<label for="idkategori">Kategori</label>
								<select name="idkategori" <?=$d_info?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="">Pilih Opsi</option>
									<?php foreach  ($list_kategori as $row) { ?>
										<option value="<?=$row->id;?>" <?=($idkategori==$row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom:10px" >
							<div class="col-md-6">
								<label for="idkategori">Nama</label>
								<input type="text"  <?=$st_info?> class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
							</div>
							<div class="col-md-6">
								<label for="idkategori">Nama Generik</label>
								<div class="input-group">
									<select tabindex="8"  <?=$d_info?> id="nama_generik"  name="nama_generik" class="js-select2 form-control js_92" style="width: 100%;">
									<option value="" <?=($nama_generik==""?'selected':'')?>>Pilih Opsi</option>	
										<?foreach(list_variable_ref(92) as $row){?>
											<option value="<?=$row->id?>" <?=($nama_generik==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
									<span class="input-group-btn">
										<button onclick="add_referensi(92)" <?=$d_info?> title="Add Data" class="btn btn-primary" type="button"><i class="fa fa-plus"></i></button>
									</span>
								</div>
								
							</div>
						</div>
						<div class="form-group"  style="margin-bottom:10px">
							<div class="col-md-3">
								<label for="idkategori">Dosis</label>
								<input type="text" class="form-control number" <?=$st_info?>  id="dosis" name="dosis" placeholder="Dosis" value="{dosis}" required="" aria-required="true">
							</div>
							<div class="col-md-3">
								<label for="idkategori">Merk</label>
								<div class="input-group">
									<select tabindex="8" id="merk" name="merk" <?=$d_info?> class="js-select2 form-control js_93" style="width: 100%;">
										<option value="" <?=($merk==""?'selected':'')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(93) as $row){?>
											<option value="<?=$row->id?>" <?=($merk==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
									<span class="input-group-btn">
										<button onclick="add_referensi(93)" <?=$d_info?> title="Add Data" class="btn btn-primary" type="button"><i class="fa fa-plus"></i></button>
									</span>
								</div>
							</div>
							<div class="col-md-2">
								<label for="idkategori">High Alert</label>
									<select tabindex="8" id="high_alert" <?=$d_info?> name="high_alert" class="js-select2 form-control " style="width: 100%;">
										<option value="" <?=($high_alert==""?'selected':'')?>>Pilih Opsi</option>
										<option value="1" <?=($high_alert=="1"?'selected':'')?>>YA</option>
										<option value="0" <?=($high_alert=="0"?'selected':'')?>>TIDAK</option>
									</select>
									
							</div>
							<div class="col-md-4">
								<label for="idkategori">Nama Industri</label>
								<div class="input-group">
									<select tabindex="8" id="nama_industri" <?=$d_info?> name="nama_industri" class="js-select2 form-control js_97" style="width: 100%;">
										<option value="" <?=($nama_industri==""?'selected':'')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(97) as $row){?>
											<option value="<?=$row->id?>" <?=($nama_industri==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
									<span class="input-group-btn">
										<button onclick="add_referensi(97)" <?=$d_info?> title="Add Data" class="btn btn-primary" type="button"><i class="fa fa-plus"></i></button>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group"  style="margin-bottom:10px">
							<div class="col-md-6">
								<label class="control-label" for="komposisi">Komposisi</label>
								<textarea  <?=$st_info?> class="form-control" id="komposisi" placeholder="Komposisi" name="komposisi" required="" aria-required="true">{komposisi}</textarea>
							</div>
							<div class="col-md-6">
								<label class="control-label" for="alamat">Indikasi</label>
								<textarea  <?=$st_info?> class="form-control" id="indikasi" placeholder="Indikasi" name="indikasi" required="" aria-required="true">{indikasi}</textarea>
							</div>
							
						</div>
						<div class="form-group"  style="margin-bottom:10px">
							<div class="col-md-3">
								<label for="idkategori">Bentuk Sediaan</label>
								<div class="input-group">
									<select tabindex="8" <?=$d_info?> id="bentuk_sediaan" name="bentuk_sediaan" class="js-select2 form-control js_94" style="width: 100%;">
										<option value="" <?=($bentuk_sediaan==""?'selected':'')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(94) as $row){?>
											<option value="<?=$row->id?>" <?=($bentuk_sediaan==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
									<span class="input-group-btn">
										<button onclick="add_referensi(94)" <?=$d_info?> title="Add Data" class="btn btn-primary" type="button"><i class="fa fa-plus"></i></button>
									</span>
								</div>
							</div>
							<div class="col-md-3" >
								<label for="idkategori">Jenis Penggunaan</label>
								<div class="input-group">
									<select tabindex="8" <?=$d_info?> id="idtipe" name="idtipe"  class="js-select2 form-control js_95" style="width: 100%;">
										<option value="" <?=($idtipe==""?'selected':'')?>>TIDAK TERSEDIA</option>
										<?foreach(list_variable_ref(95) as $row){?>
											<option value="<?=$row->id?>" <?=($idtipe==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
									<span class="input-group-btn">
										<button onclick="add_referensi(95)" <?=$d_info?> title="Add Data" class="btn btn-primary" type="button"><i class="fa fa-plus"></i></button>
									</span>
								</div>
								
							</div>
							<div class="col-md-3">
							
								<label for="idkategori">Golongan Obat</label>
								<div class="input-group">
									<select tabindex="8" id="gol_obat" <?=$d_info?> name="gol_obat" class="js-select2 form-control js_98" style="width: 100%;">
										<option value="" <?=($gol_obat==""?'selected':'')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(98) as $row){?>
											<option value="<?=$row->id?>" <?=($gol_obat==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
									<span class="input-group-btn">
										<button onclick="add_referensi(98)" <?=$d_info?> title="Add Data" class="btn btn-primary" type="button"><i class="fa fa-plus"></i></button>
									</span>
								</div>
									
							</div>
							<div class="col-md-3">
								<label for="idkategori">Satuan Besar</label>
								<select name="idsatuanbesar"  <?=$d_info?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</option>
									<?php foreach  ($list_satuan as $row) { ?>
										<option value="<?=$row->id;?>" <?=($idsatuanbesar==$row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group"  style="margin-bottom:10px">
							<div class="col-md-2">
								<label for="idkategori">Harga Satuan Besar</label>
								<input type="text"  <?=$st_harga?> class="form-control number" id="hargasatuanbesar" placeholder="Harga Satuan Besar" name="hargasatuanbesar" value="{hargasatuanbesar}" required="" aria-required="true">
							</div>
							<div class="col-md-2">
								<label for="idkategori">Jumlah Satuan Besar</label>
								<input type="text"  <?=$st_info?> class="form-control number" id="jumlahsatuanbesar" placeholder="Jumlah Satuan Besar" name="jumlahsatuanbesar" value="{jumlahsatuanbesar}" required="" aria-required="true">
							</div>
							
							
							<div class="col-md-2">
								<label for="idkategori">Harga Dasar</label>
								<input type="text"  <?=$st_harga?> class="form-control number" readonly id="hargabeli" placeholder="Harga Beli" name="hargabeli" value="{hargabeli}" required="" aria-required="true">
							</div>
							
							<div class="col-md-2">
								<label for="idkategori">Satuan Kecil</label>
								<select name="idsatuankecil"  <?=$d_info?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</option>
									<?php foreach  ($list_satuan as $row) { ?>
										<option value="<?=$row->id;?>" <?=($idsatuankecil==$row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-2">
								<label for="idkategori">PPN</label>
								<input type="text"  <?=$st_harga?> class="form-control number" id="ppn" placeholder="PPN" name="ppn" value="{ppn}" required="" aria-required="true">
							</div>
							<div class="col-md-2">
								<label for="idkategori">Harga Beli</label>
								<input type="text"  <?=$st_harga?> class="form-control number" id="hargadasar" placeholder="Harga Dasar" name="hargadasar" value="{hargadasar}" readonly required="" aria-required="true">
							</div>
						</div>
						<div class="form-group"  style="margin-bottom:10px">
							<div class="col-md-2">
								<label for="idkategori">Jenis Generik</label>
								<div class="input-group">
									<select tabindex="8" id="jenis_generik" <?=$d_info?> name="jenis_generik" class="js-select2 form-control js_99" style="width: 100%;">
										<option value="" <?=($jenis_generik==""?'selected':'')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(99) as $row){?>
											<option value="<?=$row->id?>" <?=($jenis_generik==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
									<span class="input-group-btn">
										<button onclick="add_referensi(99)" <?=$d_info?> title="Add Data" class="btn btn-primary" type="button"><i class="fa fa-plus"></i></button>
									</span>
								</div>
							</div>
							<div class="col-md-2">
								<label for="idsatuanlabel">Satuan Label</label>
								<select name="idsatuanlabel"  <?=$d_info?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</option>
									<?php foreach  ($list_satuan as $row) { ?>
										<option value="<?=$row->id;?>" <?=($idsatuanlabel==$row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-2">
								<label for="idsatuandosis">Satuan Dosis</label>
								<select name="idsatuandosis"  <?=$d_info?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="">Pilih Opsi</option>
									<?php foreach  ($list_satuan as $row) { ?>
										<option value="<?=$row->id;?>" <?=($idsatuandosis==$row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-2">
								<label for="idkategori">Formularium</label>
								<div class="input-group">
									<select tabindex="8" <?=$d_info?> id="formularium" name="formularium" class="js-select2 form-control js_96" style="width: 100%;">
										<option value="" <?=($formularium==""?'selected':'')?>>Pilih Opsi</option>
										<?foreach(list_variable_ref(96) as $row){?>
											<option value="<?=$row->id?>" <?=($formularium==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
									<span class="input-group-btn">
										<button onclick="add_referensi(96)" <?=$d_info?> title="Add Data" class="btn btn-primary" type="button"><i class="fa fa-plus"></i></button>
									</span>
								</div>
								
							</div>
							<div class="col-md-2">
								<label class="control-label" for="komposisi">Rak Gudang</label>
								<select name="idrakgudang"  <?=$d_info?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="">Pilih Opsi</option>
									<?php foreach  ($list_rakgudang as $row) { ?>
										<option value="<?=$row->id;?>" <?=($idrakgudang==$row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-2">
								<label class="control-label" for="alamat">Rak Farmasi</label>
								<select name="idrakfarmasi" <?=$d_info?>  class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="">Pilih Opsi</option>
									<?php foreach  ($list_rakfarmasi as $row) { ?>
										<option value="<?=$row->id;?>" <?=($idrakfarmasi==$row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
									<?php } ?>
								</select>
							</div>
							
						</div>
						<div class="form-group"  style="margin-bottom:10px">
							
							<div class="col-md-12">
								<label class="control-label" for="alamat">Catatan</label>
								<textarea  <?=$st_info?> class="form-control" id="catatan" placeholder="Catatan" name="catatan">{catatan}</textarea>
							</div>
							
						</div>
						<div class="form-group" hidden style="margin-bottom:10px">
							
							<div class="col-md-12">
								<label class="control-label" for="alamat">SELECT 22</label>
								<select id="customSelect" style="width:100%">
									<option value="1" data-image="https://via.placeholder.com/50" data-info="<b>Nama:</b> John Doe<br><b>Jenis Kelamin:</b> Laki-laki<br><b>Umur:</b> 30<br><b>Alamat:</b> Jl. Contoh No. 123">John Doe</option>
									<option value="2" data-image="https://via.placeholder.com/50" data-info="<b>Nama:</b> Jane Doe<br><b>Jenis Kelamin:</b> Perempuan<br><b>Umur:</b> 25<br><b>Alamat:</b> Jl. Contoh No. 456">Jane Doe</option>
									<!-- Tambahkan opsi sesuai kebutuhan -->
								</select>
	
							</div>
							
						</div>
						
						<div class="form-group">
							<div class="col-md-9 col-md-offset-3 ">
								<div class="pull-right">
								<button class="btn btn-success"id="btn_simpan"  type="submit">Simpan</button>
								<a href="{base_url}mdata_obat_new" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
								</div>
							</div>
						</div>	
					</div>
					<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_2">
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-12 control-label"><h5 align="left" class="text-primary"><b>Stok</b></h5></label>
							<div class="col-md-12">
								<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="stokreorder">Stok Reorder</label>
							<div class="col-md-2">
								<input type="text"  <?=$st_info?> class="form-control number" id="stokreorder" placeholder="Stok Reorder" name="stokreorder" value="{stokreorder}" required="" aria-required="true">
							</div>
							<label class="col-md-2 control-label" for="stokminimum">Stok Minimum</label>
							<div class="col-md-2">
								<input type="text"  <?=$st_info?> class="form-control number" id="stokminimum" placeholder="Stok Minimum" name="stokminimum" value="{stokminimum}" required="" aria-required="true">
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-12 control-label"><h5 align="left"  class="text-primary"><b>Margin (%)</b></h5></label>
							<div class="col-md-12">
								<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="marginumum">Umum</label>
							<div class="col-md-2">
								<div class="input-group">
									<input type="text"  <?=$st_margin?> class="form-control number" id="marginumum" placeholder="Umum" name="marginumum" value="{marginumum}" required="" aria-required="true">
									<span class="input-group-addon">%</span>
								</div>
							</div>
							<label class="col-md-2 control-label" for="marginasuransi">Perusahaan Asuransi</label>
							<div class="col-md-2">
								<div class="input-group">
									<input type="text"  <?=$st_margin?> class="form-control number" id="marginasuransi" placeholder="Asuransi" name="marginasuransi" value="{marginasuransi}" required="" aria-required="true">
									<span class="input-group-addon">%</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="marginjasaraharja">Jasa Raharja</label>
							<div class="col-md-2">
								<div class="input-group">
									<input type="text" <?=$st_margin?>  class="form-control number" id="marginjasaraharja" placeholder=" Jasa Raharja" name="marginjasaraharja" value="{marginjasaraharja}" required="" aria-required="true">
									<span class="input-group-addon">%</span>
								</div>
							</div>
							<label class="col-md-2 control-label" for="marginasuransi">BPJS Kesehatan</label>
							<div class="col-md-2">
								<div class="input-group">
									<input type="text"  <?=$st_margin?> class="form-control number" id="marginbpjskesehatan" placeholder="BPJS Kesehatan" name="marginbpjskesehatan" value="{marginbpjskesehatan}" required="" aria-required="true">
									<span class="input-group-addon">%</span>
								</div>
							</div>
						</div>
					
						<div class="form-group">
							<label class="col-md-3 control-label" for="marginbpjstenagakerja">BPJS Ketenagakerjaan</label>
							<div class="col-md-2">
								<div class="input-group">
									<input type="text"  <?=$st_margin?> class="form-control number" id="marginbpjstenagakerja" placeholder="BPJS Ketenagakerjaan" name="marginbpjstenagakerja" value="{marginbpjstenagakerja}" required="" aria-required="true">
									<span class="input-group-addon">%</span>
								</div>
							</div>
						</div>
						
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-12 control-label"><h5 align="left"  class="text-primary"><b>Alokasi</b></h5></label>
							<div class="col-md-12">
								<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<div class="col-xs-2">
											<div class="checkbox">
												<label for="alokasiumum">
												<input type="checkbox" <?=$d_alok?>  <?=($alokasiumum == 1 ? 'checked':'')?> id="alokasiumum" name="alokasiumum" value="1"> Umum
												</label>
											</div>
										</div>
										<div class="col-xs-2">
											<div class="checkbox">
												<label for="alokasiasuransi">
												<input type="checkbox" <?=$d_alok?>  <?=($alokasiasuransi == 1 ? 'checked':'')?> id="alokasiasuransi" name="alokasiasuransi" value="1"> Perusahaan Asuransi
												</label>
											</div>
										</div>
										<div class="col-xs-2">
											<div class="checkbox">
												<label for="alokasijasaraharja">
												<input type="checkbox" <?=$d_alok?>  <?=($alokasijasaraharja == 1 ? 'checked':'')?> id="alokasijasaraharja" name="alokasijasaraharja" value="1"> Jasa Raharja
												</label>
											</div>
										</div>
										<div class="col-xs-2">
											<div class="checkbox">
												<label for="alokasibpjskesehatan">
												<input type="checkbox" <?=$d_alok?>  <?=($alokasibpjskesehatan == 1 ? 'checked':'')?> id="alokasibpjskesehatan" name="alokasibpjskesehatan" value="1"> BPJS Kesehatan
												</label>
											</div>
										</div>
										<div class="col-xs-2">
											<div class="checkbox">
												<label for="alokasibpjstenagakerja">
												<input type="checkbox" <?=$d_alok?>  <?=($alokasibpjstenagakerja == 1 ? 'checked':'')?> id="alokasibpjstenagakerja" name="alokasibpjstenagakerja" value="1"> BPJS Ketenagakerjaan
												</label>
											</div>
										</div>
									</div>
									
									
									
								</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-9 col-md-offset-3 ">
								<div class="pull-right">
								<button class="btn btn-success"id="btn_simpan"  type="submit">Simpan</button>
								<a href="{base_url}mdata_obat_new" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
								</div>
							</div>
						</div>	
					</div>
					<?if ($id){?>
					<?php if (UserAccesForm($user_acces_form,array('1804'))){ ?>
					<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?>" id="tab_3">
						<div class="col-md-6">
							
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="asalpasien">Tanggal Update</label>
								<div class="col-md-8">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggalupdate_1" placeholder="From" value=""/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggalupdate_2" placeholder="To" value=""/>
									</div>
								</div>
							</div>
							
						</div>
						<div class="col-md-6">
							
							<div class="form-group" style="margin-bottom: 25px;">
								<label class="col-md-4 control-label" for="asalpasien"></label>
								<div class="col-md-8">
									<button class="btn btn-success text-uppercase" type="button" onclick="load_history_harga()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group" style="margin-bottom: 5px;">
								<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_history_harga">
										<thead>
											<tr>
												<th width="5%" class="text-center">NO</th>
												<th width="10%" class="text-center">Jenis Update</th>
												<th width="10%" class="text-center">Satuan Besar</th>
												<th width="10%" class="text-center">Harga Satuan Besar</th>
												<th width="10%" class="text-center">Jumlah Satuan Besar</th>
												<th width="10%" class="text-center">Satuan Kecil</th>
												<th width="10%" class="text-center">Harga Dasar</th>
												<th width="10%" class="text-center">PPN</th>
												<th width="10%" class="text-center">Harga Beli</th>
												<th width="15%" class="text-center">User Edited</th>
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
								</div>
							</div>
						</div>
						
					</div>
					<?}?>
					<?php if (UserAccesForm($user_acces_form,array('1805'))){ ?>
					<div class="tab-pane fade fade-left <?=($tab=='4'?'active in':'')?>" id="tab_4">
						<?
							$no=0;
						foreach($list_data_history as $rs){
							if ($no==0){
								$idkategori_awal=$rs->idkategori;$nama_awal=$rs->nama;$komposisi_awal=$rs->komposisi;$indikasi_awal=$rs->indikasi;$idtipe_awal=$rs->idtipe;$idsatuanbesar_awal=$rs->idsatuanbesar;$hargasatuanbesar_awal=$rs->hargasatuanbesar;$jumlahsatuanbesar_awal=$rs->jumlahsatuanbesar;$idsatuankecil_awal=$rs->idsatuankecil;$ppn_awal=$rs->ppn;$hargabeli_awal=$rs->hargabeli;$hargadasar_awal=$rs->hargadasar;$idrakgudang_awal=$rs->idrakgudang;$idrakfarmasi_awal=$rs->idrakfarmasi;$catatan_awal=$rs->catatan;$nama_generik_awal=$rs->nama_generik;$dosis_awal=$rs->dosis;$merk_awal=$rs->merk;$high_alert_awal=$rs->high_alert;$nama_industri_awal=$rs->nama_industri;$bentuk_sediaan_awal=$rs->bentuk_sediaan;$gol_obat_awal=$rs->gol_obat;$jenis_generik_awal=$rs->jenis_generik;$formularium_awal=$rs->formularium;
								
							}
							?>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-12 control-label"><h5 align="left"  class="<?=($no=='0'?'text-primary':'text-danger')?>"><b><?=($rs->versi_edit=='0'?'ORIGINAL':'VERSI '.$rs->versi_edit)?></b>  |   <?=($no=='0'?'CREATED BY : '.$rs->user_created.' '.HumanDateLong($rs->created_date):'EDITED BY : '.$rs->user_edited.' '.HumanDateLong($rs->edited_date))?></h5> </label>
								<div class="col-md-12">
									<hr style="margin-top: 10px;margin-bottom: 5px;background: red;">
								</div>
							</div>
							<div class="form-group" style="margin-bottom:10px">
								<div class="col-md-12">
									<label for="idkategori">Kategori</label>
									<select class="<?=($idkategori_awal!=$rs->idkategori?'edited_final':'')?>  form-control" disabled style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="">Pilih Opsi</option>
										<?php foreach  ($list_kategori as $row) { ?>
											<option value="<?=$row->id;?>" <?=($rs->idkategori==$row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom:10px" >
								<div class="col-md-6">
									<label for="idkategori">Nama <?=$nama_awal?></label>
									<input type="text" class="form-control <?=($nama_awal!=$rs->nama?'edited_final':'')?> " placeholder="Nama" disabled value="<?=$rs->nama?>">
								</div>
								<div class="col-md-6">
									<label for="idkategori">Nama Generik</label>
									<div class="input-group">
										<select tabindex="8" class="<?=($nama_generik_awal!=$rs->nama_generik?'edited_final':'')?> form-control js_92" disabled style="width: 100%;">
										<option value="" <?=($nama_generik==""?'selected':'')?>>Pilih Opsi</option>	
											<?foreach(list_variable_ref(92) as $row){?>
												<option value="<?=$row->id?>" <?=($rs->nama_generik==$row->id?'selected':'')?>><?=$row->nama?></option>
											<?}?>
										</select>
										<span class="input-group-btn">
											<button onclick="add_referensi(92)" disabled title="Add Data" class="btn btn-primary" type="button"><i class="fa fa-plus"></i></button>
										</span>
									</div>
									
								</div>
							</div>
							<div class="form-group"  style="margin-bottom:10px">
								<div class="col-md-3">
									<label for="idkategori">Dosis</label>
									<input type="text" class="form-control <?=($dosis_awal!=$rs->dosis?'edited_final':'')?> " disabled placeholder="Dosis" value="<?=$rs->dosis?>" >
								</div>
								<div class="col-md-3">
									<label for="idkategori">Merk</label>
									<div class="input-group">
										<select tabindex="8" class="<?=($merk_awal!=$rs->merk?'edited_final':'')?> form-control js_93" disabled style="width: 100%;">
											<option value="" <?=($merk==""?'selected':'')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(93) as $row){?>
												<option value="<?=$row->id?>" <?=($rs->merk==$row->id?'selected':'')?>><?=$row->nama?></option>
											<?}?>
										</select>
										<span class="input-group-btn">
											<button onclick="add_referensi(93)" disabled title="Add Data" class="btn btn-primary" type="button"><i class="fa fa-plus"></i></button>
										</span>
									</div>
								</div>
								<div class="col-md-2">
									<label for="idkategori">High Alert</label>
										<select tabindex="8"  class="<?=($high_alert_awal!=$rs->high_alert?'edited_final':'')?> form-control " disabled style="width: 100%;">
											<option value="" <?=($rs->high_alert==""?'selected':'')?>>Pilih Opsi</option>
											<option value="1" <?=($rs->high_alert=="1"?'selected':'')?>>YA</option>
											<option value="0" <?=($rs->high_alert=="0"?'selected':'')?>>TIDAK</option>
										</select>
										
								</div>
								<div class="col-md-4">
									<label for="idkategori">Nama Industri</label>
									<div class="input-group">
										<select tabindex="8"  disabled class="<?=($nama_industri_awal!=$rs->nama_industri?'edited_final':'')?> form-control js_97" style="width: 100%;">
											<option value="" <?=($nama_industri==""?'selected':'')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(97) as $row){?>
												<option value="<?=$row->id?>" <?=($rs->nama_industri==$row->id?'selected':'')?>><?=$row->nama?></option>
											<?}?>
										</select>
										<span class="input-group-btn">
											<button onclick="add_referensi(97)" disabled title="Add Data" class="btn btn-primary" type="button"><i class="fa fa-plus"></i></button>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group"  style="margin-bottom:10px">
								<div class="col-md-6">
									<label class="control-label" for="komposisi">Komposisi</label>
									<textarea  class="form-control <?=($komposisi_awal!=$rs->komposisi?'edited_final':'')?> " disabled placeholder="Komposisi" ><?=$rs->komposisi?></textarea>
								</div>
								<div class="col-md-6">
									<label class="control-label" for="alamat">Indikasi</label>
									<textarea  class="form-control <?=($indikasi_awal!=$rs->indikasi?'edited_final':'')?> " disabled placeholder="Indikasi" ><?=$rs->indikasi?></textarea>
								</div>
								
							</div>
							<div class="form-group"  style="margin-bottom:10px">
								<div class="col-md-3">
									<label for="idkategori">Bentuk Sediaan</label>
									<div class="input-group">
										<select tabindex="8" class="<?=($bentuk_sediaan_awal!=$rs->bentuk_sediaan?'edited_final':'')?> form-control js_94" disabled style="width: 100%;">
											<option value="" <?=($bentuk_sediaan==""?'selected':'')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(94) as $row){?>
												<option value="<?=$row->id?>" <?=($rs->bentuk_sediaan==$row->id?'selected':'')?>><?=$row->nama?></option>
											<?}?>
										</select>
										<span class="input-group-btn">
											<button onclick="add_referensi(94)" title="Add Data" disabled class="btn btn-primary" type="button"><i class="fa fa-plus"></i></button>
										</span>
									</div>
								</div>
								<div class="col-md-3">
									<label for="idkategori">Jenis Penggunaan</label>
									<div class="input-group">
										<select tabindex="8" disabled  class="<?=($idtipe_awal!=$rs->idtipe?'edited_final':'')?> form-control js_95" style="width: 100%;">
											<option value="" <?=($idtipe==""?'selected':'')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(95) as $row){?>
												<option value="<?=$row->id?>" <?=($rs->idtipe==$row->id?'selected':'')?>><?=$row->nama?></option>
											<?}?>
										</select>
										<span class="input-group-btn">
											<button onclick="add_referensi(95)" disabled title="Add Data" class="btn btn-primary" type="button"><i class="fa fa-plus"></i></button>
										</span>
									</div>
									
								</div>
								<div class="col-md-3">
								
									<label for="idkategori">Golongan Obat</label>
									<div class="input-group">
										<select tabindex="8" disabled class="<?=($gol_obat_awal!=$rs->gol_obat?'edited_final':'')?> form-control js_98" style="width: 100%;">
											<option value="" <?=($gol_obat==""?'selected':'')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(98) as $row){?>
												<option value="<?=$row->id?>" <?=($rs->gol_obat==$row->id?'selected':'')?>><?=$row->nama?></option>
											<?}?>
										</select>
										<span class="input-group-btn">
											<button onclick="add_referensi(98)" disabled title="Add Data" class="btn btn-primary" type="button"><i class="fa fa-plus"></i></button>
										</span>
									</div>
										
								</div>
								<div class="col-md-3">
									<label for="idkategori">Satuan Besar</label>
									<select disabled class="<?=($idsatuanbesar_awal!=$rs->idsatuanbesar?'edited_final':'')?> form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?php foreach  ($list_satuan as $row) { ?>
											<option value="<?=$row->id;?>" <?=($rs->idsatuanbesar==$row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group"  style="margin-bottom:10px">
								<div class="col-md-2">
									<label for="idkategori">Harga Satuan Besar</label>
									<input type="text" disabled class="<?=($hargasatuanbesar_awal!=$rs->hargasatuanbesar?'edited_final':'')?> form-control number"  value="<?=$rs->hargasatuanbesar?>" >
								</div>
								<div class="col-md-2">
									<label for="idkategori">Jumlah Satuan Besar</label>
									<input type="text"  disabled  class="<?=($jumlahsatuanbesar_awal!=$rs->jumlahsatuanbesar?'edited_final':'')?>  form-control number" id="jumlahsatuanbesar" placeholder="Jumlah Satuan Besar" value="<?=$rs->jumlahsatuanbesar?>" >
								</div>
								
								
								<div class="col-md-2">
									<label for="idkategori">Harga Dasar</label>
									<input type="text"  disabled class="<?=($hargabeli_awal!=$rs->hargabeli?'edited_final':'')?> form-control number" readonly value="<?=$rs->hargabeli?>" >
								</div>
								
								<div class="col-md-2">
									<label for="idkategori">Satuan Kecil</label>
									<select disabled class="<?=($idsatuankecil_awal!=$rs->idsatuankecil?'edited_final':'')?> form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
										<option value="">Pilih Opsi</option>
										<?php foreach  ($list_satuan as $row) { ?>
											<option value="<?=$row->id;?>" <?=($rs->idsatuankecil==$row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
										<?php } ?>
									</select>
								</div>
								<div class="col-md-2">
									<label for="idkategori">PPN</label>
									<input type="text" disabled class="<?=($ppn_awal!=$rs->ppn?'edited_final':'')?> form-control number"  value="<?=$rs->ppn?>" >
								</div>
								<div class="col-md-2">
									<label for="idkategori">Harga Beli</label>
									<input type="text" disabled class="<?=($hargadasar_awal!=$rs->hargadasar?'edited_final':'')?> form-control number" placeholder="Harga Dasar"  value="<?=$rs->hargadasar?>" readonly >
								</div>
							</div>
							<div class="form-group"  style="margin-bottom:10px">
								<div class="col-md-3">
									<label for="idkategori">Jenis Generik</label>
									<div class="input-group">
										<select tabindex="8" disabled class="<?=($jenis_generik_awal!=$rs->jenis_generik?'edited_final':'')?>  form-control js_99" style="width: 100%;">
											<option value="" <?=($jenis_generik==""?'selected':'')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(99) as $row){?>
												<option value="<?=$row->id?>" <?=($rs->jenis_generik==$row->id?'selected':'')?>><?=$row->nama?></option>
											<?}?>
										</select>
										<span class="input-group-btn">
											<button onclick="add_referensi(99)" disabled title="Add Data" class="btn btn-primary" type="button"><i class="fa fa-plus"></i></button>
										</span>
									</div>
								</div>
								<div class="col-md-3">
									<label for="idkategori">Formularium</label>
									<div class="input-group">
										<select tabindex="8" id="formularium" disabled class="<?=($formularium_awal!=$rs->formularium?'edited_final':'')?> form-control js_96" style="width: 100%;">
											<option value="" <?=($formularium==""?'selected':'')?>>Pilih Opsi</option>
											<?foreach(list_variable_ref(96) as $row){?>
												<option value="<?=$row->id?>" <?=($rs->formularium==$row->id?'selected':'')?>><?=$row->nama?></option>
											<?}?>
										</select>
										<span class="input-group-btn">
											<button onclick="add_referensi(96)" disabled title="Add Data" class="btn btn-primary" type="button"><i class="fa fa-plus"></i></button>
										</span>
									</div>
									
								</div>
								
								
							</div>
							<div class="form-group"  style="margin-bottom:10px">
								
								<div class="col-md-12">
									<label class="control-label" for="alamat">Catatan</label>
									<textarea  disabled class="<?=($catatan_awal!=$rs->catatan?'edited_final':'')?> form-control"  placeholder="Catatan" ><?=$rs->catatan?></textarea>
								</div>
								
							</div>
							<hr style="margin-top: 50px;margin-bottom: 5px;background: red;">
						<?
						$no++;
						$idkategori_awal=$rs->idkategori;$nama_awal=$rs->nama;$komposisi_awal=$rs->komposisi;$indikasi_awal=$rs->indikasi;$idtipe_awal=$rs->idtipe;$idsatuanbesar_awal=$rs->idsatuanbesar;$hargasatuanbesar_awal=$rs->hargasatuanbesar;$jumlahsatuanbesar_awal=$rs->jumlahsatuanbesar;$idsatuankecil_awal=$rs->idsatuankecil;$ppn_awal=$rs->ppn;$hargabeli_awal=$rs->hargabeli;$hargadasar_awal=$rs->hargadasar;$idrakgudang_awal=$rs->idrakgudang;$idrakfarmasi_awal=$rs->idrakfarmasi;$catatan_awal=$rs->catatan;$nama_generik_awal=$rs->nama_generik;$dosis_awal=$rs->dosis;$merk_awal=$rs->merk;$high_alert_awal=$rs->high_alert;$nama_industri_awal=$rs->nama_industri;$bentuk_sediaan_awal=$rs->bentuk_sediaan;$gol_obat_awal=$rs->gol_obat;$jenis_generik_awal=$rs->jenis_generik;$formularium_awal=$rs->formularium;
						}?>
						
					</div>
					<?}?>
					<?php if (UserAccesForm($user_acces_form,array('1806'))){ ?>
					<div class="tab-pane fade fade-left <?=($tab=='5'?'active in':'')?>" id="tab_5">
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="asalpasien">No Pemesanan</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="nopemesanan" placeholder="No Pemesanan" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="asalpasien">No Penerimaan</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="nopenerimaan" placeholder="No Penerimaan" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="asalpasien">Tanggal Terima</label>
								<div class="col-md-8">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggalpenerimaan_1" placeholder="From" value=""/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggalpenerimaan_2" placeholder="To" value=""/>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="iddis">Distributor</label>
								<div class="col-md-8">
									<select tabindex="8" id="iddis" class="js-select2 form-control " style="width: 100%;" multiple>
										<?foreach(get_all('mdistributor',array('status'=>1)) as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
										
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="asalpasien">No Faktur</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="nofakturexternal" placeholder="No Faktur" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="asalpasien">No Batch</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="nobatch" placeholder="No Batch" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="asalpasien">Tanggal Kadaluarsa</label>
								<div class="col-md-8">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggalkadaluarsa_1" placeholder="From" value=""/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggalkadaluarsa_2" placeholder="To" value=""/>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 15px;">
								<label class="col-md-4 control-label" for="asalpasien">Total Pembelian</label>
								<div class="col-md-8">
									<input type="text" class="form-control" disabled id="total_pembelian" placeholder="Total Pembelian" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 25px;">
								<label class="col-md-4 control-label" for="asalpasien"></label>
								<div class="col-md-8">
									<button class="btn btn-success text-uppercase" type="button" onclick="load_pembelian()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group" style="margin-bottom: 5px;">
								<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_pembelian">
										<thead>
											<tr>
												<th width="5%" class="text-center">NO</th>
												<th width="10%" class="text-center">No Pemesanan</th>
												<th width="10%" class="text-center">No Penerimaan</th>
												<th width="15%" class="text-center">Distributor</th>
												<th width="10%" class="text-center">Harga</th>
												<th width="10%" class="text-center">Harga Final</th>
												<th width="8%" class="text-center">Qty</th>
												<th width="10%" class="text-center">Total</th>
												<th width="12%" class="text-center">Batch</th>
												<th width="12%" class="text-center">Expired</th>
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
								</div>
							</div>
						</div>
						
					</div>
					<?}?>
					<?}?>
				</div>
		
			
			
			<input class="form-control" type="hidden" id="id" name="id" value="{id}">
			<?php echo form_hidden('kode', $kode); ?>
			<?php echo form_close() ?>
		</div>
	</div>
</div>
<div class="modal fade in" id="modal_add_referensi" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title" id="judul_referensi"></h3>
				</div>
				<div class="block-content">
					<?php echo form_open('#','class="form-horizontal" id="form-work"') ?>
					<div class="row push-10-t">
						<div class="col-md-12">
							<div class="form-group" style="margin-bottom:10px">
								<div class="col-md-12">
									<label for="nama_ref">Nama</label>
									<input type="text" class="form-control" id="nama_ref" placeholder="Nama" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom:10px">
								<div class="col-md-12">
									<label for="nama_ref"></label>
									<button class="btn btn-sm btn-success" type="button" onclick="simpan_referensi()"><i class="fa fa-check"></i> Simpan</button>
								</div>
							</div>
							
						</div>
						<input class="form-control" disabled type="hidden" id="nilai_ref" value="">
						<input class="form-control" disabled type="hidden" id="ref_head_id" value="">
						<input class="form-control" disabled type="hidden" id="ref_id" value="">
					</div>
					<div class="row push-10-t">
						<div class="col-md-12">
							<table width="100%" id="tabel_referensi" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th style="width:5%">ID</th>
										<th style="width:70%">Nama</th>
										<th style="width:25%">Action</th>
										
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
					<div class="row push-10-b">
						
					</div>
					<?php echo form_close() ?>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal"> Close</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	// modal_add_referensi
	function load_pembelian(){
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}mdata_obat_new/load_pembelian/',
			dataType: "json",
			type: 'POST',
			data: {
				id:$("#id").val(),
				nopemesanan:$("#nopemesanan").val(),
				nopenerimaan:$("#nopenerimaan").val(),
				tanggalpenerimaan_1:$("#tanggalpenerimaan_1").val(),
				tanggalpenerimaan_2:$("#tanggalpenerimaan_2").val(),
				iddis:$("#iddis").val(),
				nofakturexternal:$("#nofakturexternal").val(),
				nobatch:$("#nobatch").val(),
				tanggalkadaluarsa_1:$("#tanggalkadaluarsa_1").val(),
				tanggalkadaluarsa_2:$("#tanggalkadaluarsa_2").val(),
				
			},
			success: function(data) {
				$("#cover-spin").hide();
				$("#index_pembelian tbody").empty();
				$("#index_pembelian tbody").append(data.tabel);
				$("#total_pembelian").val(data.total_pembelian);
				
			}
		});
	}
	function load_history_harga(){
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}mdata_obat_new/load_history_harga/',
			dataType: "json",
			type: 'POST',
			data: {
				id:$("#id").val(),
				tanggalupdate_1:$("#tanggalupdate_1").val(),
				tanggalupdate_2:$("#tanggalupdate_2").val(),
				
			},
			success: function(data) {
				$("#cover-spin").hide();
				$("#index_history_harga tbody").empty();
				$("#index_history_harga tbody").append(data.tabel);
				
			}
		});
	}
	function clear_ref(){
		$("#nilai_ref").val('');
		$("#ref_id").val('');
		$("#nama_ref").val('');
		load_data_ref();
	}
	function refresh_select2(){
		let ref_head_id=$("#ref_head_id").val();
		$(".js_"+ref_head_id).empty();
		$.ajax({
			url: '{site_url}mdata_obat_new/refresh_select2', 
			dataType: "JSON",
			method: "POST",
			data : {
					ref_head_id:ref_head_id,
				   },
			success: function(data) {
				$(".js_"+ref_head_id).append(data);
				
			}
		});
	}
	
	$(document).ready(function() {
		$('#customSelect').select2({
			templateResult: formatOption,
			escapeMarkup: function (markup) {
				return markup;
			}
		});
	});
	function formatOption(option) {
		if (!option.id) {
			return option.text;
		}

		var image = $(option.element).data('image');
		var info = $(option.element).data('info');
		var $option = $(
			'<div class="select2-custom-container">' +
				'<img class="select2-custom-image" src="' + image + '" alt="Pasien">' +
				'<div class="select2-custom-info">' +
					'<span>' + option.text + '</span>' +
					'<span>' + info + '</span>' +
				'</div>' +
			'</div>'
		);

		return $option;
	}
	function edit_ref($id,$nama){
		$("#ref_id").val($id);
		$("#nama_ref").val($nama);
	}
	function hapus_ref($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Data ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}mdata_obat_new/hapus_ref', 
				dataType: "JSON",
				method: "POST",
				data : {
						ref_id:$id,
					   },
				success: function(data) {
					
					$("#cover-spin").hide();
					list_referensi();
				}
			});
		});
	}
	function add_referensi($ref_head){
		$("#ref_head_id").val($ref_head);
		$("#modal_add_referensi").modal('show');
		clear_ref();
		list_referensi();
	}
	function load_data_ref(){
		let ref_head_id=$("#ref_head_id").val();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}mdata_obat_new/load_data_ref/',
			dataType: "json",
			type: 'POST',
			data: {
				ref_head_id:$("#ref_head_id").val(),
				
			},
			success: function(data) {
				$("#judul_referensi").text(data.referensi_nama);
				$("#nilai_ref").val(data.nilai);
				$("#cover-spin").hide();
			}
		});
	}
	function simpan_referensi(){
		if ($("#nama_ref").val()==''){
			swal({
				title: "Gagal",
				text: "Silahkan Nama "+$("#judul_referensi").text(),
				type: "error",
				timer: 1500,
				showConfirmButton: false
			});
			return false;
		}
		let ref_head_id=$("#ref_head_id").val();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}mdata_obat_new/simpan_referensi/',
			dataType: "json",
			type: 'POST',
			data: {
				ref_head_id:$("#ref_head_id").val(),
				nilai_ref:$("#nilai_ref").val(),
				ref_id:$("#ref_id").val(),
				nama_ref:$("#nama_ref").val(),
				
			},
			success: function(data) {
				$("#cover-spin").hide();
				swal({
					title: "Berhasil",
					text: "Menamabah "+$("#judul_referensi").text(),
					type: "success",
					timer: 1000,
					showConfirmButton: false
				});
				clear_ref();
				list_referensi();
			}
		});
	}
	function list_referensi(){
		
		let ref_head_id=$("#ref_head_id").val();
		$('#tabel_referensi').DataTable().destroy();	
		$("#cover-spin").show();
		table = $('#tabel_referensi').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": false,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [
						// {  className: "text-right", targets:[0] },
						// {  className: "text-center", targets:[1,2,3,6,4,0,7] },
						 { "width": "5%", "targets": [0] },
						 { "width": "70%", "targets": [1] },
						 { "width": "30%", "targets": [2] },
					],
				ajax: { 
					url: '{site_url}mdata_obat_new/list_referensi', 
					type: "POST" ,
					dataType: 'json',
					data : {
							ref_head_id:ref_head_id,
							
						   }
				},
				"drawCallback": function( settings ) {
					 $("#cover-spin").hide();
				 }  
			});
		refresh_select2();
	}
	$(".number").number(true, 0, '.', ',');
	$("#hargasatuanbesar,#jumlahsatuanbesar,#ppn").keyup(function(){
		set_harga_dasar();
		
	});
	function set_harga_dasar(){
		var hargabesar=$("#hargasatuanbesar").val();
		var jml_besar=$("#jumlahsatuanbesar").val();
		var hargabeli=hargabesar/jml_besar;
		$("#hargabeli").val(hargabeli);
		var ppn=$("#ppn").val();
		
		var hargadasar=hargabeli+(hargabeli*ppn/100);
		$("#hargadasar").val(hargadasar);
		
	}
	// $("#ppn").keyup(function(){
		// if($(this).val().replace(/,/g, '') > 100){
			// $(this).val(100);
		// }

		// var hargabeli = parseFloat($("#hargabeli").val().replace(/,/g, ''));
		// var ppn = $(this).val() / 100;
		// $("#hargadasar").val( hargabeli + (hargabeli * ppn) );
	// });

	$("#marginumum, #marginasuransi, #marginjasaraharja, #marginbpjskesehatan, #marginbpjstenagakerja").keyup(function(){
		if($(this).val().replace(/,/g, '') > 100){
			$(this).val(100);
		}
	});
	$(document.body).on('click', '#btn_simpan' ,function(){
		$('input').removeAttr("disabled")
		$('.js-select2').removeAttr("disabled")
		// alert('Simpan');return false;
		return true;
        // alert($("#st_email").val());
        
    });
</script>
