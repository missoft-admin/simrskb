<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">		
		<ul class="block-options">
			<li>
				<a href="{base_url}tbuku_besar_piutang_setting" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:10px">
		<div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
           <input type="hidden" class="form-control" readonly id="idkelompok" name="idkelompok" value="{idkelompok}" />
           <input type="hidden" class="form-control" readonly id="idrekanan" name="idrekanan" value="{idrekanan}" />
           <input type="hidden" class="form-control" readonly id="st_multiple" name="st_multiple" value="{st_multiple}" />
           <input type="hidden" class="form-control" readonly id="id" name="id" value="{id}" />
			<div class="col-md-6">	
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Kelompok Pasien</label>
                    <div class="col-md-8">
						<input type="text" class="form-control" readonly id="nama_kelompok" name="nama_kelompok" value="{nama_kelompok}" />
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Tagihan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_klaim" placeholder="No Tagihan" name="no_klaim" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Tagihan</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
                            <input class="form-control" type="text" id="tanggal_tagihan" name="tanggal_tagihan" placeholder="From" value=""/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_tagihan2" name="tanggal_tagihan2" placeholder="To" value=""/>
                        </div>
                    </div>
                </div>
			</div>
			<div class="col-md-6">	
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Nama Asuransi</label>
                    <div class="col-md-8">
						<input type="text" class="form-control" readonly id="nama" name="nama" value="{nama}" />
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-4 control-label" for="tanggal">Tipe</label>
                    <div class="col-md-8">
						<select id="index_tipe" name="index_tipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua Tipe -</option>
							<option value="1">Rawat Jalan</option>
							<option value="2">Ranap / ODS</option>
							
						</select>
					</div>
                </div>
				
				
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>                    
					<div class="col-md-4">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>                        
                    </div>
					<div class="col-md-4">
                        <button class="btn btn-danger text-uppercase" type="button" name="btn_add" id="btn_add" style="font-size:13px;width:100%;float:right;"><i class="fa fa-plus"></i> Create Tagihan</button>
                    </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th hidden width="5%">TIPE</th>
                    <th hidden width="2%">ID</th>
                    <th width="10%">NO</th>
                    <th width="10%">NO TAGIHAN</th>
                    <th width="10%"> NAMA REKANAN</th>
                    <th width="10%">JENIS LAYANAN</th>
                    <th width="10%">JML FAKTUR</th>
                    <th width="10%">TOT TAGIHAN</th>
                    <th width="10%">JATUH TEMPO PENAGIHAN</th>
                    <th width="15%">STATUS</th>
                    <th width="15%">STATUS PENGIRIMAN</th>
                    <th width="15%">JATUH TEMPO PEMBAYARAN</th>
                    <th width="15%">TGL KIRIM</th>
                    <th width="15%">NO RESI</th>
                    <th width="15%" class="text-center">AKSI</th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                    <th width="15%" class="text-center"></th>
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
	<div class="modal fade" id="modal_create" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="block block-themed block-transparent remove-margin-b">
					<div class="block-header bg-danger">
						<h3 class="block-title">Create Tagihan</h3>
					</div>
					<div class="block-content">
						<div class="row">
							<div class="col-md-12">						
								<div class="form-group" style="margin-top: 5px;">
									<label class="col-md-4 control-label" for="status">Kelompok Pasien</label>
									<div class="col-md-8">
										<input class="form-control" readonly type="text" id="xnama_kelompok" name="xnama_kelompok" value="{nama_kelompok}">
									</div>
								</div>
							</div>
							<?if ($idkelompok =='1'){?>
							<div class="col-md-12">						
								<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
									<label class="col-md-4 control-label" for="status">Nama Rekanan</label>
									<div class="col-md-8">
										<input class="form-control" readonly type="text" id="xnama_rekanan" name="xnama_rekanan" value="{nama}">
									</div>
								</div>				
							</div>	
							<?}?>					
							<div class="col-md-12">
								<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Tanggal Tagihan</label>
								<div class="col-md-8">
								<div class="input-group">
									<input class="js-datepicker form-control input" type="text" id="xtanggal_tagihan" name="xtanggal_tagihan" autocomplete="off" value="" data-date-format="dd-mm-yyyy"/>
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
								</div>
									<input class="form-control" readonly type="hidden" id="xidrekanan" name="xidrekanan" value="<?=($idkelompok!='1'?'0':$idrekanan)?>">
									<input class="form-control" readonly type="hidden" id="xidkelompokpasien" name="xidkelompokpasien" value="<?=$idkelompok?>">
							</div> 
							</div> 
							<div class="col-md-12">						
								<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
									<label class="col-md-4 control-label" for="status">Tipe Layanan</label>
									<div class="col-md-8">
										<select name="xtipe" id="xtipe" class="js-select2 form-control" style="width: 100%;">
											<option value="#" selected>-Pilih Tipe Layanan-</option>
											<option value="1">Rawat Jalan</option>
											<option value="2">Rawat Inap / ODS</option>											
										</select>
									</div>
								</div>				
							</div>	
							<div class="col-md-12">
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Tanggal Kirim</label>
								<div class="col-md-8">
								<div class="input-group">
									<input class="js-datepicker form-control input" type="text" id="xtanggal_kirim" name="xtanggal_kirim" autocomplete="off" value="" data-date-format="dd-mm-yyyy"/>
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
								</div>
								</div>
							</div> 	
							<div class="col-md-12">
							<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
								<label class="col-md-4 control-label" for="status">Tanggal Jatuh Tempo</label>
								<div class="col-md-8">
								<div class="input-group">
									<input class="js-datepicker form-control input" type="text" id="xjatuh_tempo_bayar" name="xjatuh_tempo_bayar" autocomplete="off" value="" data-date-format="dd-mm-yyyy"/>
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
								</div>
								</div>
							</div> 	
									
							
							
							<?if ($st_multiple =='2'){?>
							<div class="col-md-12">						
								<div class="form-group" style="margin-top: 5px;" id="div_rekanan">
									<label class="col-md-4 control-label" for="status">Pasien</label>
									<div class="col-md-8">
										<select id="xidpasien" name="xidpasien" class="form-control" style="width: 100%;" data-placeholder="Choose one..">											
										</select>
									</div>
								</div>				
							</div>	
							<?}?>
						</div>
						<div class="row">
								<div class="col-md-12">
								<label class="col-md-4 control-label" for="status"></label>	 
								</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-success text-uppercase" type="button" name="btn_simpan_create" id="btn_simpan_create"><i class="fa fa-save"></i> SIMPAN</button>
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
				</div>
			</div>
		</div>
	</div>
	
</div>
<?$this->load->view('Tbuku_besar_piutang_setting/mod_cari_transaksi')?>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/number_format.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
$(document).ready(function(){
	$(".decimal").number(true,2,'.',',');
	list_pasien();
	load_index();
})
function load_index(){
	var no_klaim=$("#no_klaim").val();
	var idkelompokpasien=$("#idkelompok").val();
	var idrekanan=$("#idrekanan").val();
	var status=1;
	var tipe=$("#index_tipe").val();
	var tanggal_tagihan=$("#tanggal_tagihan").val();
	var tanggal_tagihan2=$("#tanggal_tagihan2").val();
	$('#index_list').DataTable().destroy();
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0,1,15,16,17,18,19], "visible": false },
							{ "width": "3%", "targets": [2] },
							{ "width": "5%", "targets": [5,6] },
							{ "width": "8%", "targets": [9,7,8,10,11,12,13] },
							{ "width": "10%", "targets": [3,4] },
							{ "width": "15%", "targets": [14] },
						 {"targets": [4,13], className: "text-left" },
						 {"targets": [2,7], className: "text-right" },
						 {"targets": [3,5,6,8,9,10,11,12], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}Tbuku_besar_piutang_setting/list_tagihan', 
                type: "POST" ,
                dataType: 'json',
				data : {
						no_klaim:no_klaim,idkelompokpasien:idkelompokpasien,
						idrekanan:idrekanan,status:status,
						tipe:tipe,
						tanggal_tagihan2:tanggal_tagihan2,tanggal_tagihan:tanggal_tagihan,
					   }
            }
        });
}
function add_detail($id){
	$('#modal_trx').modal('show');
	$.ajax({
		url: '{site_url}Tbuku_besar_piutang_setting/get_tklaim/'+$id,
		dataType: "json",
		type: 'POST',
		// data: {idkelompokpasien: idkelompokpasien,idrekanan:idrekanan,tipe:tipe,st_multiple:st_multiple,idpasien:idpasien},
		success: function(data) {
			$("#cari_idpasien").val(data.idpasien);
			$("#klaim_id").val(data.klaim_id);
			$("#namapasien").val(data.namapasien);
			$("#cari_tipe").val(data.tipe).trigger('change');
			load_transaksi();
		}
	});
}
$(document).on("click","#btn_filter",function(){	
	load_index();
});
$(document).on("click","#btn_add",function(){	
	$('#modal_create').modal('show');
});
$(document).on("click","#btn_simpan_create",function(){	
	if (validate_create()==false)return false;
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Membuat Tagihan Secara Manual?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			simpan_create();
		});

	// $('#modal_create').modal('show');
});
function simpan_create(){
	var tanggal_tagihan=$("#xtanggal_tagihan").val();		
	var tanggal_kirim=$("#xtanggal_kirim").val();		
	var jatuh_tempo_bayar=$("#xjatuh_tempo_bayar").val();		
	var tipe=$("#xtipe").val();		
	var st_multiple=$("#st_multiple").val();		
	var idkelompokpasien=$("#xidkelompokpasien").val();		
	var idrekanan=$("#xidrekanan").val();		
	var idpasien=$("#xidpasien").val();
	// alert(klaim_id+' tg l'+tanggal_tagihan+' Kel '+idkelompokpasien+' rekan '+idrekanan+' tipe '+tipe);
	// return false;\
	$("#cover-spin").show();
	var table = $('#index_list').DataTable();
	$.ajax({
		url: '{site_url}Tbuku_besar_piutang_setting/simpan_create',
		type: 'POST',
		data: {tanggal_kirim: tanggal_kirim,jatuh_tempo_bayar: jatuh_tempo_bayar,tanggal_tagihan:tanggal_tagihan,idkelompokpasien:idkelompokpasien,idrekanan:idrekanan,tipe:tipe,st_multiple:st_multiple,idpasien:idpasien},
		complete: function() {

			swal("Berhasil!", "Create Tagihan Berhasil!", "success");
			$("#modal_create").modal('hide');
			$('#index_list').DataTable().ajax.reload( null, false );
			$("#cover-spin").hide();
		}
	});
}
function list_pasien(){
	$("#xidpasien").select2({
        minimumInputLength: 2,
		noResults: 'Pasien Tidak Ditemukan.',
		// allowClear: true
        // tags: [],
        ajax: {
            url: '{site_url}Tbuku_besar_piutang_setting/list_pasien/',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
		 data: function (params) {
              var query = {
                search: params.term,
              }
              return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nama,
                            id: item.id,
                        }
                    })
                };
            }
        }
    });

}
function validate_create()
{
	if ($("#xtanggal_tagihan").val()=='' || $("#xtanggal_tagihan").val()==null){
		sweetAlert("Maaf...", "Tentukan Tanggal Tagihan", "error");
		return false;
	}
	if ($("#xtipe").val()=='#'){
		sweetAlert("Maaf...", "Tentukan Tipe Layanan", "error");
		return false;
	}
	if ($("#st_multiple").val()=='2'){
		if ($("#xidpasien").val()=='' || $("#xidpasien").val()==null){
		sweetAlert("Maaf...", "Tentukan Pasien", "error");
		return false;
		}
	}
	if ($("#xtanggal_kirim").val()=='' || $("#xtanggal_kirim").val()==null){
		sweetAlert("Maaf...", "Tentukan Tanggal Kirim", "error");
		return false;
	}
	if ($("#xjatuh_tempo_bayar").val()=='' || $("#xjatuh_tempo_bayar").val()==null){
		sweetAlert("Maaf...", "Tentukan Tanggal Jatuh Tempo", "error");
		return false;
	}
	
	return true;
}
$(document).on("click", ".verifikasi", function() {
	var table = $('#tabel_cari_trx').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var tipe=table.cell(tr,0).data()
	var noreg=table.cell(tr,8).data()
	var tipekontraktor=table.cell(tr,20).data()
	var idkontraktor=table.cell(tr,21).data()
	var tanggal_jt=table.cell(tr,15).data()
	var st_multiple=table.cell(tr,22).data()
	var idpasien=table.cell(tr,23).data()
	  // $row[] = $r->st_multiple;//22
            // $row[] = $r->idpasien;//23
			
	// alert(tanggal_jt);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Verifikasi No. Registrasi "+noreg+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		verifikasi(id,tipe,tipekontraktor,idkontraktor,tanggal_jt,st_multiple,idpasien);
	});
});
function verifikasi($id,$tipe,$tipekontraktor,$idkontraktor,$tanggal_jt,$st_multiple,$idpasien){
	var klaim_id=$("#klaim_id").val();		
	var id=$id;		
	var tipe=$tipe;		
	var tipekontraktor=$tipekontraktor;		
	var idkontraktor=$idkontraktor;		
	var tanggal_jt=$tanggal_jt;		
	var st_multiple=$st_multiple;		
	var idpasien=$idpasien;		
	$("#cover-spin").show();
	table = $('#tabel_cari_trx').DataTable()	
	$.ajax({
		url: '{site_url}Tbuku_besar_piutang_setting/verifikasi',
		type: 'POST',
		data: {id: id,tipe:tipe,tipekontraktor:tipekontraktor,idkontraktor:idkontraktor,tanggal_jt:tanggal_jt,st_multiple:st_multiple,idpasien:idpasien,klaim_id:klaim_id},
		success: function(data) {
			if (data=='true'){
				console.log(data);
				$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi'});
				table.ajax.reload( null, false ); 
				$('#index_list').DataTable().ajax.reload( null, false );
				$("#cover-spin").hide();
			}else{
				$("#cover-spin").hide();
				sweetAlert("Maaf...", "Data Duplicate", "error");
			}
		}
	});
}

</script>
