<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">		
		<ul class="block-options">
			<li>
				<a href="{base_url}tbuku_besar_piutang_setting" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:10px">
		<div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
			<div class="col-md-6">	
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
                            <input autocomplete="off" <?=$disabel?> class="form-control" type="text" id="tanggal_tagihan" name="tanggal_tagihan" placeholder="From" value="{tanggal_tagihan}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input autocomplete="off" <?=$disabel?>  class="form-control" type="text" id="tanggal_tagihan2" name="tanggal_tagihan2" placeholder="To" value="{tanggal_tagihan2}"/>
                        </div>
						<div class="help-block text-left text-danger">Pastikan tanggal akhir adalah tanggal sebelum set saldo awal</div>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Saldo Akhir</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control decimal" id="total" placeholder="Total Hutang" name="total" value="{saldo}">
                        <input type="hidden" readonly class="form-control" id="st_verifikasi" placeholder="Total Hutang" name="st_verifikasi" value="{st_verifikasi}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Akun</label>
                    <div class="col-md-8">
						<?
							$q="SELECT * FROM makun_nomor H WHERE H.`status`='1' AND H.idkategori='1'";
							$rows=$this->db->query($q)->result();
						?>
                        <select id="idakun" <?=$disabel?> name="idakun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" <?=($idakun=='#'?'selected':'')?>>- Belum Dipilih -</option>
							<?foreach($rows as $row){?>
							<option value="<?=$row->id?>" <?=($idakun==$row->id?'selected':'')?>><?=$row->noakun.' - '.$row->namaakun?></option>
							<?}?>
							
						</select>
                    </div>
                </div>
			</div>
			<div class="col-md-6">					
                <div class="form-group" style="margin-bottom: 5px;">
					<div class="col-md-8">
						<?if ($st_verifikasi=='0'){?>
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:49%;float:left;"><i class="fa fa-search"></i> Hitung Saldo</button>   
                        <button class="btn btn-danger text-uppercase" type="button" <?=$disabel?>  name="btn_save_generate" id="btn_save_generate" style="font-size:13px;width:49%;float:right;"><i class="fa fa-save"></i> Simpan Saldo Awal</button>
						<?}else{?>
                      
						<?}?>						
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
					<div class="col-md-8">
						<?if ($st_verifikasi=='0'){?>
                        <h4><span class="label label-warning" style="vertical-align: text-bottom;"><i class="fa fa-info-circle"></i> Belum Ada Saldo Awal</span></h4>
						<?}else{?>
                        <h4><span class="label label-info" style="vertical-align: text-bottom;"><i class="fa fa-check"></i> Saldo Awal Sudah di create</span></h4>
						<?}?>
                    </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
			<table class="table table-bordered table-striped" id="index_list">
				<thead>
					<tr>
						<th hidden width="5%">ID</th>
						<th width="10%">NO</th>
						<th width="10%">NAMA ASURANSI</th>
						<th width="10%">DBET</th>
						<th width="10%">KREDIT</th>
						<th width="10%">SALDO</th>
						<th width="15%" class="text-center">AKSI</th>
					</tr>					
				</thead>
				<tbody></tbody>
				<tfoot>
					<tr>
						<td colspan="3" class="text-center"><strong>TOTAL<strong></td>
						<td><strong><strong><input type="hidden" readonly class="form-control decimal" id="total_debet" value="{debet}"><label id="lbl_debet"></label></td>
						<td><strong><strong><input type="hidden" readonly class="form-control decimal" id="total_kredit" value="{kredit}"><label id="lbl_kredit"></label></td>
						<td><strong><strong><input type="hidden" readonly class="form-control decimal" id="total_saldo" value="{saldo}"><label id="lbl_saldo"></label></td>
						<td><strong><strong></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	
</div>
<?$this->load->view('Tbuku_besar_piutang_setting/mod_cari_transaksi')?>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/number_format.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
$(document).ready(function(){
	$(".decimal").number(true,2,'.',',');
	if ($("#st_verifikasi").val()=='1'){
		load_saldo();
		$("#lbl_debet").text(formatNumber($("#total_debet").val()));
		$("#lbl_kredit").text(formatNumber($("#total_kredit").val()));
		$("#lbl_saldo").text(formatNumber($("#total_saldo").val()));
	}
	// load_index();
	// get_total();
})
function load_index(){
	var tanggal_tagihan=$("#tanggal_tagihan").val();
	var tanggal_tagihan2=$("#tanggal_tagihan2").val();
	
	$('#index_list').DataTable().destroy();
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 100,
            "ordering": false,
			columnDefs: [{ "targets": [0], "visible": false },							
							{ "width": "5%", "targets": [1] },
							{ "width": "30%", "targets": [2] },
							{ "width": "15%", "targets": [3,4,5] },
						 {"targets": [3,4,5], className: "text-right" },
						 {"targets": [5], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}Tbuku_besar_piutang_setting/rekap_piutang', 
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_tagihan2:tanggal_tagihan2,tanggal_tagihan:tanggal_tagihan,
					   }
            }
        });
}
function load_saldo(){
	var tanggal_tagihan=$("#tanggal_tagihan").val();
	var tanggal_tagihan2=$("#tanggal_tagihan2").val();
	
	$('#index_list').DataTable().destroy();
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 100,
            "ordering": false,
			columnDefs: [{ "targets": [0], "visible": false },							
							{ "width": "5%", "targets": [1] },
							{ "width": "30%", "targets": [2] },
							{ "width": "15%", "targets": [3,4,5] },
						 {"targets": [3,4,5], className: "text-right" },
						 {"targets": [5], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}Tbuku_besar_piutang_setting/rekap_saldo', 
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_tagihan2:tanggal_tagihan2,tanggal_tagihan:tanggal_tagihan,
					   }
            }
        });
}
function get_total(){
	var tanggal_tagihan=$("#tanggal_tagihan").val();
	var tanggal_tagihan2=$("#tanggal_tagihan2").val();
	// alert(tanggal_tagihan);
	// info_total
	$.ajax({
	 
			url: '{site_url}Tbuku_besar_piutang_setting/info_total', 
			type: "POST" ,
			dataType: 'json',
			data : {
					tanggal_tagihan2:tanggal_tagihan2,tanggal_tagihan:tanggal_tagihan,
				   },
			success: function(data) {
				$("#total_debet").val(data.debet);
				$("#total_kredit").val(data.kredit);
				$("#total_saldo").val(data.saldo);
				$("#total").val(data.saldo);
				$("#lbl_debet").text(formatNumber(data.debet));
				$("#lbl_kredit").text(formatNumber(data.kredit));
				$("#lbl_saldo").text(formatNumber(data.saldo));
				
			}
		
	});
}
function formatNumber (num) {
return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

$(document).on("click","#btn_filter",function(){	
	load_index();
	get_total();
});
$(document).on("click","#btn_save_generate",function(){	
	if (validate_create()==false)return false;
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Membuat Saldo Awal Piutang ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			save_generate();
		});

});
function save_generate(){
	var idakun=$("#idakun").val();
	var debet=$("#total_debet").val();
	var kredit=$("#total_kredit").val();
	var saldo=$("#total_saldo").val();
	var tanggal_tagihan=$("#tanggal_tagihan").val();
	var tanggal_tagihan2=$("#tanggal_tagihan2").val();
	// alert(tanggal_tagihan);
	// info_total
	$("#cover-spin").show();
	$.ajax({
	 
			url: '{site_url}Tbuku_besar_piutang_setting/save_generate', 
			type: "POST" ,
			dataType: 'json',
			data : {
					tanggal_tagihan2:tanggal_tagihan2,tanggal_tagihan:tanggal_tagihan,
					debet:debet,kredit:kredit,
					idakun:idakun,saldo:saldo,
				   },
			success: function(data) {
				if (data==true){
					swal("Berhasil!", "Input Data Transaksi!", "success");
					window.location.href = "<?php echo site_url('tbuku_besar_piutang_setting/generate'); ?>";
				}else{
					sweetAlert("Maaf...", "Gagal Menyimpan!", "error");
				}
				$("#cover-spin").hide();
			}
		
	});
}
function validate_create()
{
	if ($("#total").val() < 1 || $("#total").val()=='0'){
		sweetAlert("Maaf...", "Belum Generate Saldo", "error");
		return false;
	}
	if ($("#idakun").val() =='#'){
		sweetAlert("Maaf...", "Tentukan Akun Saldo", "error");
		return false;
	}
	
	
	return true;
}

</script>
