<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">		
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:10px">
		<div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
           
			<div class="col-md-10">				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="tanggal">Kelompok Pasien</label>
                    <div class="col-md-10">
						<select name="idkelompok[]" id="idkelompok" class="js-select2 form-control" style="width: 100%;" multiple>
							<?php foreach  ($list_KP as $row) { ?>
							<option value="'<?=$row->id?>'"><?=$row->nama?></option>
							<?php } ?>						
					</select>
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-2 control-label" for="tanggal">Nama</label>
                    <div class="col-md-10">
						<select name="idrekanan[]" id="idrekanan" class="js-select2 form-control" style="width: 100%;" multiple>
						<?php foreach  ($list_asuransi as $row) { ?>
							<option value="'<?=$row->id?>'"><?=$row->nama?></option>
						<?php } ?>
					</select>
					
					</div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for=""></label>
                    <div class="col-md-2">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
					<div class="col-md-2">
                        <a href="{base_url}tbuku_besar_piutang_setting/generate" class="btn btn-warning text-uppercase" type="button" name="btn_generate" id="btn_generate" style="font-size:13px;width:100%;float:right;"><i class="si si-size-actual"></i> Generate</a>
                    </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="index_list">
			<thead>
				<tr>
					<th>No</th>
					<th>Tipe</th>
					<th>Nama</th>
					<th>Debit</th>
					<th>Kredit</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".decimal").number(true,2,'.',',');
	load_detail();
})
function verifikasi($tipe,$id){
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Verifikasi Saldo ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			// verifikasi_saldo($tipe,$id);
		});
		

}
function verifikasi_saldo($tipe,$id){
	$.ajax({
		url: '{site_url}Tbuku_besar_piutang_setting/verifikasi/'+$tipe+'/'+$id,
			type: "POST" ,
			dataType: 'json',			
			success: function(data) {
				swal("Berhasil!", "Input Data Transaksi!", "success");
				$('#index_list').DataTable().ajax.reload( null, false );
				// clear_input();
			}
		});
}
$(document).on("click","#btn_filter",function(){	
	load_detail();		
});
function load_detail() {
	// alert('sini');
	var idrekanan=$("#idrekanan").val();
	var idkelompok=$("#idkelompok").val();
	
	$('#index_list').DataTable().destroy();
	var table = $('#index_list').DataTable({
	"pageLength": 10,
	"ordering": false,
	"processing": true,
	"serverSide": true,
	"autoWidth": false,
	"fixedHeader": true,
	"searching": true,
	"order": [],
	"ajax": {
		url: '{site_url}Tbuku_besar_piutang_setting/getIndex/',
		type: "POST",
		dataType: 'json',
		data: {
			idrekanan: idrekanan,
			idkelompok: idkelompok,
		}
	},
	columnDefs: [
				 // {"targets": [0], "visible": false },
				 {  className: "text-right", targets:[3,4] },
				 {  className: "text-center", targets:[0] },
				 { "width": "5%", "targets": [0] },
				 { "width": "20%", "targets": [1] },
				 { "width": "15%", "targets": [3,4] },
				 { "width": "25%", "targets": [2] },
				 // { "width": "15%", "targets": [2,13] }

				]
	});
}

</script>
