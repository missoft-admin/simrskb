<div class="modal fade in black-overlay" id="modal_trx" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Cari Transaksi</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="cari_tipe_adm">Kelompok Pasien</label>
									<div class="col-md-9">
										<input type="text" class="form-control" readonly id="nama_kelompok" name="nama_kelompok" value="{nama_kelompok}" />
										<input type="hidden" class="form-control" readonly id="klaim_id" name="klaim_id" value="" />
										<input type="hidden" class="form-control" readonly id="cari_idpasien" name="cari_idpasien" value="" />
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="cari_tipe_adm">TIPE</label>
									<div class="col-md-9">
										<select id="cari_tipe" disabled name="cari_tipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">											
											<option value="#">- Semua Tipe -</option>
											<option value="1">Rawat Jalan</option>
											<option value="2">Rawat Inap</option>
											
										</select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="xke_id">Tanggal Kunjungan</label>
									<div class="col-md-9">
										<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="form-control" type="text" id="tanggaldaftar1" name="tanggaldaftar1" placeholder="From" value=""/>
											<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
											<input class="form-control" type="text" id="tanggaldaftar2" name="tanggaldaftar2" placeholder="To" value=""/>
										</div>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="xke_id">Tanggal Transaksi</label>
									<div class="col-md-9">
										<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="form-control" type="text" id="tgl_trx1" name="tgl_trx1" placeholder="From" value=""/>
											<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
											<input class="form-control" type="text" id="tgl_trx2" name="tgl_trx2" placeholder="To" value=""/>
										</div>
									</div>
								</div>
								
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="cari_tipe_adm">Asuransi</label>
									<div class="col-md-9">
										<input type="text" class="form-control" readonly id="nama" name="nama" value="{nama}" />
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="cari_tipe_adm">Pasien</label>
									<div class="col-md-9">
										<input type="text" class="form-control" readonly id="namapasien" name="namapasien" value="" />
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 15px;">
									<label class="col-md-3 control-label" for="cari_tipe_adm">No Pendaftaran</label>
									<div class="col-md-9">
										<input type="text" style="width: 100%"  class="form-control" id="no_reg" placeholder="No Pendaftaran" name="no_reg" value="">									
									</select>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label" for="btn_cari_penerimaan"></label>
									<div class="col-md-9">
										<button class="btn btn-sm btn-success" id="btn_cari_penerimaan" type="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-search"></i> Cari</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<table id="tabel_cari_trx" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th hidden width="5%">TIPE</th>
								<th hidden width="2%">ID</th>
								<th width="10%">NO</th>
								<th width="10%">TGL KUNJUGAN</th>
								<th width="10%">TGR TRX</th>
								<th width="10%">JENIS</th>
								<th width="10%">NO REG</th>
								<th width="10%">MEDREC</th>
								<th width="10%">PASIEN</th>
								<th width="15%">KEL PASIEN</th>
								<th width="15%">ASURANSI</th>
								<th width="15%">STATUS</th>
								<th width="15%">TOT TRX</th>
								<th width="15%">ASURANSI</th>
								<th width="15%">BAYAR LAINYA</th>
								<th width="15%"> TGL PENAGIHAN</th>
								<th width="15%" class="text-center">AKSI</th>
								<th width="15%" class="text-center"></th>
								<th width="15%" class="text-center"></th>
								<th width="15%" class="text-center"></th>
								<th width="15%" class="text-center"></th>
								<th width="15%" class="text-center"></th>
								<th width="15%" class="text-center"></th>
								<th width="15%" class="text-center"></th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">				
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Keluar</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
var table_permintaan;
	$(document).ready(function() {
		// $('#modal_trx').modal('show');
		// load_transaksi();
	});
	$(document).on('click', '#btn_cari_penerimaan', function() {
		load_transaksi();
	});
	// $(document).on('click', '.pilih_permintaan', function() {
		
	// });
	
	function load_transaksi(){
		var no_reg=$("#no_reg").val();
		var idpasien=$("#cari_idpasien").val();
		var st_multiple=$("#st_multiple").val();
		var idkelompokpasien=$("#idkelompok").val();
		var idrekanan=$("#idrekanan").val();
		var tipe=$("#cari_tipe").val();
		var status=$("#status").val();
		var tgl_trx1=$("#tgl_trx1").val();
		var tgl_trx2=$("#tgl_trx2").val();
		var tanggaldaftar1=$("#tanggaldaftar1").val();
		var tanggaldaftar2=$("#tanggaldaftar2").val();
		// alert(idpasien);
		 $('#tabel_cari_trx').DataTable().destroy();
		table = $('#tabel_cari_trx').DataTable({
				autoWidth: false,
				searching: true,
				pageLength: 50,
				serverSide: true,
				"processing": true,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [{ "targets": [0,1,17,18,19,20,21,22,23,8,9,12,14], "visible": false },
								{ "width": "3%", "targets": [2] },
								{ "width": "5%", "targets": [5] },
								{ "width": "5%", "targets": [3] },
								{ "width": "7%", "targets": [4,6,15,11,12,13,14] },
								{ "width": "8%", "targets": [8,,9,10,16] },
								{ "width": "15%", "targets": [7] },
							 {"targets": [8], className: "text-left" },
							 {"targets": [2,12,13,14,15], className: "text-right" },
							 {"targets": [3,4,5,6,16,9,10,11], className: "text-center" }
							 ],
				ajax: { 
					url: '{site_url}Tbuku_besar_piutang_setting/load_transaksi', 
					type: "POST" ,
					dataType: 'json',
					data : {
							no_reg:no_reg,idkelompokpasien:idkelompokpasien,st_multiple:st_multiple,idpasien:idpasien,
							idrekanan:idrekanan,tipe:tipe,status:status,tgl_trx1:tgl_trx1,
							tgl_trx2:tgl_trx2,tanggaldaftar1:tanggaldaftar1,tanggaldaftar2:tanggaldaftar2,
						   }
				}
			});
	}
	
	
	

</script>
