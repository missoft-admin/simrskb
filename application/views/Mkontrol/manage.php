<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
		<?php if (UserAccesForm($user_acces_form,array('303'))){ ?>	
		<li>
            <a href="{base_url}mkontrol/create" class="btn"><i class="fa fa-plus"></i></a>
        </li>
		<?}?>
        <li>
            <a href="{base_url}mkontrol" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mkontrol/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Master Kontrol</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="{id}" required="" aria-required="true">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="Lokasi Tubuh">TIPE</label>
				<div class="col-md-7">
						<select name="tipe_id" id="tipe_id" class="js-select2 form-control" style="width: 100%;">
							<option value="2" <?=($tipe_id=='2'?'selected':'')?>>IGD</option>
							<option value="1" <?=($tipe_id=='1'?'selected':'')?>>Rawat Jalan</option>
						</select>
						
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="Lokasi Tubuh">Poliklinik</label>
				<div class="col-md-7">
					<div class="input-daterange input-group">						
						<select name="idpoliklinik" id="idpoliklinik" class="js-select2 form-control" style="width: 100%;">
							<?foreach ($poli_list as $row){?>
								<option value="<?=$row->id?>" <?=($idpoliklinik==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
						
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mkontrol" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<hr>
			<?if ($id !=''){?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="Lokasi Tubuh">ICD 10</label>
				<div class="col-md-9">
					<table id="tabel_icd" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<input type="hidden" class="form-control" id="tedit" placeholder="tedit" name="tedit" value="0" required="" aria-required="true">
							<tr>
								<th style="width: 40%;">ICD 10</th>
								<th style="width: 30%;">Jenis</th>								
								<th style="width: 20%;">Actions</th>
								<th style="width: 20%;">Actions</th>
							</tr>
							<tr>
								
								<td>
									<select name="icd_id" tabindex="16"  style="width: 100%" id="icd_id" data-placeholder="Cari ICD" class="form-control input-sm">
										<option value="#" selected>-Pilih ICD 10-</option>
										<?foreach ($dokterumum as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?}?>
									</select>										
								</td>
								<td>									
									<select name="jenis_id" tabindex="16"  style="width: 100%" id="jenis_id" data-placeholder="" class="js-select2 form-control input-sm">
										<option value="1" selected>Primary</option>
										<option value="2" >Secondary</option>										
									</select>
								</td>
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_icd" title="Masukan Item"><i class="fa fa-save"></i></button>&nbsp;&nbsp;
								</td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						
				</table>
				</div>
			</div>
			<?}?>
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		loadICD();
	});
	
	// Initialize when page loads
	$("#icd_id").select2({
			minimumInputLength: 2,
			noResults: 'Dokter Tidak Ditemukan.',
		// allowClear: true
		// tags: [],
		// var idunit=$("#idunit_obat").val();
		ajax: {
			url: '{site_url}mkontrol/js_icd/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,
		 data: function (params) {
			  var query = {
				search: params.term
			  }
			  return query;
			},
			processResults: function (data) {
				console.log($("#icd_id").val());
				return {
					results: $.map(data, function (item) {
						return {
							text: item.kode + ' - '+ item.deskripsi,
							id: item.id
						}
					})
				};
			}
		}
	});
	function loadICD(){
		var id=$("#id").val();
		
		$('#tabel_icd').DataTable().destroy();
		var table = $('#tabel_icd').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}mkontrol/load_icd/',
			type: "POST",
			dataType: 'json',
			data: {
				id: id
			}
		},
		columnDefs: [
		{"targets": [3,4,5], "visible": false },
		{ "width": "60%", "targets": [0] },
		 { "width": "30%", "targets": [1] },
		 { "width": "10%", "targets": [3] },
					]
		});
	}
	$("#tipe_id").change(function() {
			var tipepoli = $("#tipe_id").val();
			$.ajax({
				url: "{site_url}tpoliklinik_pendaftaran/getPoli",
				method: "POST",
				dataType: "json",
				data: {
					"tipepoli": tipepoli
				},
				success: function(data) {
					$("#idpoliklinik").empty();
					// $("#idpoliklinik").append("<option value=''>Pilih Opsi</option>");
					for (var i = 0; i < data.length; i++) {
						$("#idpoliklinik").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
					}
					// $("#idpoliklinik").selectpicker('refresh');


						if (tipepoli=='2'){
						$('#idpoliklinik').val('22').trigger('change');
					}else{
						$('#idpoliklinik').val('1').trigger('change');
					}



				}
			});
		});
	$(document).on("click","#simpan_icd",function(){
		if (validate_add()==false)return false;
		var duplicate=false;
			var table = $('#tabel_icd').DataTable();		
			table.rows().eq(0).each( function ( index ) {
			var row = table.row( index );		 
			var data = row.data();
			if (data[4]==$("#icd_id").val()){
				duplicate = true;
				sweetAlert("Duplicate...", "Data ICD 9 Duplicate!", "error");
			}
			if ($("#jenis_id").val()=='1' && data[5]=='1'){
				duplicate = true;
				sweetAlert("Duplicate...", "Primary Duplicate!", "error");
			}
			// console.log();
			// ... do something with data(), or row.node(), etc
		} );
		// return false;
		if(duplicate) return false;
		
		simpan();
		
	});
	$(document).on("click",".edit",function(){
		 table = $('#tabel_icd').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,3).data()
		var icd_id = table.cell(tr,4).data()
		// alert(icd_id);
		$("#jenis_id").val(table.cell(tr,5).data()).trigger('change');
		var data2 = {
			id: icd_id,
			text: table.cell(tr,0).data()
			
		};
		var newOption = new Option(data2.text, data2.id, true, true);
		$('#icd_id').append(newOption);
		$('#tedit').val(id);
		
		// $("#Eid").val(id);
		// alert('SINI');
		return false;
	});
	$(document).on("click",".hapus",function(){
		// alert('SINI');
		table = $('#tabel_icd').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,3).data()
		// var idunit = table.cell(tr,7).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Mengahpus ICD 10 ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mkontrol/hapus_icd',
				type: 'POST',
				data: {
					tedit: id	
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' ICD Berhasil dihapus'});
					$('#tabel_icd').DataTable().ajax.reload( null, false );
								
					// window.location.href = "{site_url}tko/index/"+idunit;
				}
			});
		});
		
		return false;
	});
	
	function simpan(){
		var id=$("#id").val();		
		var tedit=$("#tedit").val();		
		var jenis_id=$("#jenis_id").val();		
		var icd_id=$("#icd_id").val();
// alert($("#id").val());		
		if (tedit =='0'){
			// alert('AD');
			$.ajax({
				url: '{site_url}mkontrol/simpan_add',
				type: 'POST',
				data: {
					id: id,jenis_id: jenis_id,
					icd_id: icd_id		
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' ICD Berhasil Disimpan'});
					$('#tabel_icd').DataTable().ajax.reload( null, false );
					$("#icd_id").val(null).trigger('change');
								
					// window.location.href = "{site_url}tko/index/"+idunit;
				}
			});
		}else{
			$.ajax({
				url: '{site_url}mkontrol/simpan_edit',
				type: 'POST',
				data: {
					id: id,jenis_id: jenis_id,
					icd_id: icd_id,tedit: tedit		
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' ICD Berhasil Disimpan'});
					$('#tabel_icd').DataTable().ajax.reload( null, false );
					$("#icd_id").val(null).trigger('change');								
					// window.location.href = "{site_url}tko/index/"+idunit;
				}
			});
		}
		$('#tedit').val(0);	
	}
	function validate_final()
	{
		// alert($("#id_alat").val());
		if ($("#nama").val()==''){
			sweetAlert("Maaf...", "Nama Nama Harus diisi", "error");
			return false;
		}
		
		if ($("#tipe_id").val()=='#'){
			sweetAlert("Maaf...", "Tipe Harus diisi", "error");
			return false;
		}
		if ($("#idpoliklinik").val()=='#'){
			sweetAlert("Maaf...", "Tipe Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function validate_add()
	{
		if ($("#icd_id").val()=='' || $("#icd_id").val()==null || $("#icd_id").val()=="#"){
			sweetAlert("Maaf...", "ICD 10 Harus diisi", "error");
			return false;
		}
		
		return true;
	}
</script>
