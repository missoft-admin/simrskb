<input type="hidden" id="idtransaksi" value="">
<input type="hidden" id="tanggal" value="">

<div class="modal in" id="editModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<h3 class="block-title">Ubah Data Dokter</h3>
				</div>
				<div class="block-content">
					<div class="form-horizontal" id="form-work">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="" style="margin-top: 5px;">Dokter Jadwal Pagi</label>
							<div class="col-md-8">
								<select id="iddokter_pagi" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="">Pilih Opsi</option>
									<?php foreach ($list_dokter as $row){ ?>
										<option value="<? echo $row->id ?>"><?php echo $row->nama; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="" style="margin-top: 5px;">Nominal Transport Pagi</label>
							<div class="col-md-8">
								<input type="text" class="form-control number" id="transport_pagi" placeholder="Nominal Transport Pagi" value="">
							</div>
						</div>

            <input type="hidden" id="tanggal_pembayaran_pagi" value="">
            <input type="hidden" id="tanggal_jatuhtempo_pagi" value="">

						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="" style="margin-top: 5px;">Dokter Jadwal Siang</label>
							<div class="col-md-8">
								<select id="iddokter_siang" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="">Pilih Opsi</option>
									<?php foreach ($list_dokter as $row){ ?>
										<option value="<? echo $row->id ?>"><?php echo $row->nama; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="" style="margin-top: 5px;">Nominal Transport Siang</label>
							<div class="col-md-8">
								<input type="text" class="form-control number" id="transport_siang" placeholder="Nominal Transport Siang" value="">
							</div>
						</div>

            <input type="hidden" id="tanggal_pembayaran_siang" value="">
            <input type="hidden" id="tanggal_jatuhtempo_siang" value="">

						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="" style="margin-top: 5px;">Dokter Jadwal Malam</label>
							<div class="col-md-8">
								<select id="iddokter_malam" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="">Pilih Opsi</option>
									<?php foreach ($list_dokter as $row){ ?>
										<option value="<? echo $row->id ?>"><?php echo $row->nama; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="" style="margin-top: 5px;">Nominal Transport Malam</label>
							<div class="col-md-8">
								<input type="text" class="form-control number" id="transport_malam" placeholder="Nominal Malam" value="">
							</div>
						</div>

            <input type="hidden" id="tanggal_pembayaran_malam" value="">
            <input type="hidden" id="tanggal_jatuhtempo_malam" value="">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button id="rowUpdate" class="btn btn-sm btn-success" type="button" style="width: 20%;"><i class="fa fa-check"></i> Simpan</button>
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
		$(".number").number(true, 0, '.', ',');

		$(document).on("click", ".rowEdit", function() {
			$("#idtransaksi").val($(this).closest('tr').find("td:eq(0)").html());
			$("#tanggal").val($(this).closest('tr').find("td:eq(1)").html());

			$("#iddokter_pagi").val($(this).closest('tr').find("td:eq(3)").html())
			$("#transport_pagi").val($(this).closest('tr').find("td:eq(4)").html());
			$("#tanggal_pembayaran_pagi").val($(this).closest('tr').find("td:eq(5)").html());
			$("#tanggal_jatuhtempo_pagi").val($(this).closest('tr').find("td:eq(6)").html());

			$("#iddokter_siang").val($(this).closest('tr').find("td:eq(8)").html())
			$("#transport_siang").val($(this).closest('tr').find("td:eq(9)").html());
			$("#tanggal_pembayaran_siang").val($(this).closest('tr').find("td:eq(10)").html());
			$("#tanggal_jatuhtempo_siang").val($(this).closest('tr').find("td:eq(11)").html());

			$("#iddokter_malam").val($(this).closest('tr').find("td:eq(13)").html())
			$("#transport_malam").val($(this).closest('tr').find("td:eq(14)").html());
			$("#tanggal_pembayaran_malam").val($(this).closest('tr').find("td:eq(15)").html());
			$("#tanggal_jatuhtempo_malam").val($(this).closest('tr').find("td:eq(16)").html());
		});

		$(document).on("click", "#rowUpdate, .rowUnverifikasi", function() {
			updateTransaksi(0);
		});

		$(document).on("click", ".rowVerifikasi", function() {
			event.preventDefault();

			swal({
	      title: "Apakah anda yakin ingin menyetujui data ini ?",
	      type: "warning",
	      showCancelButton: true,
	      confirmButtonColor: "#DD6B55",
	      confirmButtonText: "Ya",
	      cancelButtonText: "Tidak",
	      closeOnConfirm: false,
	      closeOnCancel: false
	    }).then(function() {
				updateTransaksi(1);
			});
		});
	});

	function updateTransaksi(status_verifikasi)
	{
		var idtransaksi = $("#idtransaksi").val();
		var tanggal = $("#tanggal").val();

		var iddokter_pagi = $("#iddokter_pagi option:selected").val();
		var transport_pagi = $("#transport_pagi").val();
		var tanggal_pembayaran_pagi = $("#tanggal_pembayaran_pagi").val();
		var tanggal_jatuhtempo_pagi = $("#tanggal_jatuhtempo_pagi").val();

		var iddokter_siang = $("#iddokter_siang option:selected").val();
		var transport_siang = $("#transport_siang").val();
		var tanggal_pembayaran_siang = $("#tanggal_pembayaran_siang").val();
		var tanggal_jatuhtempo_siang = $("#tanggal_jatuhtempo_siang").val();

		var iddokter_malam = $("#iddokter_malam option:selected").val();
		var transport_malam = $("#transport_malam").val();
		var tanggal_pembayaran_malam = $("#tanggal_pembayaran_malam").val();
		var tanggal_jatuhtempo_malam = $("#tanggal_jatuhtempo_malam").val();

		$.ajax({
			url: '{site_url}Ttransport_dokter/updateTransaksi',
			method: 'POST',
			data: {
				idtransaksi: idtransaksi,
				tanggal: tanggal,
				iddokter_pagi: iddokter_pagi,
				transport_pagi: transport_pagi,
				tanggal_pembayaran_pagi: tanggal_pembayaran_pagi,
				tanggal_jatuhtempo_pagi: tanggal_jatuhtempo_pagi,
				iddokter_siang: iddokter_siang,
				transport_siang: transport_siang,
				tanggal_pembayaran_siang: tanggal_pembayaran_siang,
				tanggal_jatuhtempo_siang: tanggal_jatuhtempo_siang,
				iddokter_malam: iddokter_malam,
				transport_malam: transport_malam,
				tanggal_pembayaran_malam: tanggal_pembayaran_malam,
				tanggal_jatuhtempo_malam: tanggal_jatuhtempo_malam,
				status_verifikasi: status_verifikasi
			},
			success: function(data) {
				swal({
					title: "Berhasil!",
					text: "Data Telah Tersimpan.",
					type: "success",
					timer: 1500,
					showConfirmButton: false
				});

				window.location.reload();
			}
		});
	}
</script>
