<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}ttransport_dokter" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('ttransport_dokter/save','class="form-horizontal" id="form-work"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Periode</label>
				<div class="col-md-7">
					<input type="text" class="form-control" readonly value="<?=GetDayIndonesia($periode)?>">
					<input type="hidden" class="form-control" id="periode" placeholder="Periode" name="periode" value="{periode}" readonly required="" aria-required="true">
				</div>
			</div>
	</div>
	<div class="block-content">
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th style="width:15%">Tanggal</th>
						<th style="width:25%">Jadwal Pagi</th>
						<th style="width:25%">Jadwal Siang</th>
						<th style="width:25%">Jadwal Malam</th>
						<th style="width:25%">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php $nominalpagi = 0;?>
					<?php $nominalsiang = 0;?>
					<?php $nominalmalam = 0;?>
					<?php $iddokterMalamTemp = 0; ?>
					<?php foreach ($list_jadwal as $row) { ?>
						<?php
						if($row->nominal_transport_pagi){
							$nominalpagi = $row->nominal_transport_pagi;
						}else{
							if($iddokterMalamTemp != $row->iddokterpagi){
								$nominalpagi = $row->transportpagi;
							}else{
								$nominalpagi = 0;
							}
						}

						if($row->nominal_transport_siang){
							$nominalsiang = $row->nominal_transport_siang;
						}else{
							if($row->iddokterpagi != $row->iddoktersiang){
								$nominalsiang = $row->transportsiang;
							}else{
								$nominalsiang = 0;
							}
						}

						if($row->nominal_transport_malam){
							$nominalmalam = $row->nominal_transport_malam;
						}else{
							if($row->iddoktersiang != $row->iddoktermalam){
								$nominalmalam = $row->transportmalam;
							}else{
								$nominalmalam = 0;
							}
						}
						?>

						<tr>
							<td hidden><?=$row->id;?></td>
							<td hidden><?=$row->periode.'-'.str_pad($row->tanggal, 2, '0', STR_PAD_LEFT);?></i></td>
							<td><?=GetDayIndonesia($row->periode.'-'.$row->tanggal, true);?></td>

							<td hidden><?=$row->iddokterpagi;?></td>
							<td hidden><?=$nominalpagi;?></td>
							<td hidden><?=($row->tanggal_pembayaran_pagi != '' ? DMYFormat($row->tanggal_pembayaran_pagi) : getPembayaranHonorDokter($row->iddokterpagi))?></td>
							<td hidden><?=($row->tanggal_jatuhtempo_pagi != '' ? DMYFormat($row->tanggal_jatuhtempo_pagi) : getJatuhTempoHonorDokter($row->iddokterpagi))?></td>
							<td><?=$row->namadokterpagi;?><br><i>Nominal : Rp. <?=number_format($nominalpagi);?></i></td>

							<td hidden><?=$row->iddoktersiang;?></td>
							<td hidden><?=$nominalsiang;?></td>
							<td hidden><?=($row->tanggal_pembayaran_siang != '' ? DMYFormat($row->tanggal_pembayaran_siang) : getPembayaranHonorDokter($row->iddoktersiang))?></td>
							<td hidden><?=($row->tanggal_jatuhtempo_siang != '' ? DMYFormat($row->tanggal_jatuhtempo_siang) : getJatuhTempoHonorDokter($row->iddoktersiang))?></td>
							<td><?=$row->namadoktersiang;?><br><i>Nominal : Rp. <?=number_format($nominalsiang);?></i></td>

							<td hidden><?=$row->iddoktermalam;?></td>
							<td hidden><?=$nominalmalam;?></td>
							<td hidden><?=($row->tanggal_pembayaran_malam != '' ? DMYFormat($row->tanggal_pembayaran_malam) : getPembayaranHonorDokter($row->iddoktermalam))?></td>
							<td hidden><?=($row->tanggal_jatuhtempo_malam != '' ? DMYFormat($row->tanggal_jatuhtempo_malam) : getJatuhTempoHonorDokter($row->iddoktermalam))?></td>
							<td><?=$row->namadoktermalam;?><br><i>Nominal : Rp. <?=number_format($nominalmalam);?></i></td>

							<td>
								<div class="btn-group">
									<?php if($row->status_verifikasi == 0){ ?>
										<a href="#" class="btn btn-primary rowEdit" data-toggle="modal" data-target="#editModal"><i class="fa fa-pencil"></i></a>
										<a href="#" class="btn btn-success rowEdit rowVerifikasi"><i class="fa fa-check"></i></a>
									<?php }else{ ?>
										<a href="#" class="btn btn-danger rowEdit rowUnverifikasi"><i class="fa fa-times"></i></a>
									<?php } ?>
								</div>
							</td>
						</tr>
						<?php $iddokterMalamTemp = $row->iddoktermalam; ?>
					<?php } ?>
				</tbody>
			</table>
			<?php echo form_close() ?>
	</div>
</div>

<?php $this->load->view('Ttransport_dokter/modal/modal_edit_transport'); ?>
