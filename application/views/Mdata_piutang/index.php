<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('3'))): ?>
			<ul class="block-options">
		        <li>
		            <a href="{base_url}mdata_piutang/create" class="btn"><i class="fa fa-plus"></i></a>
		        </li>
		    </ul>
		<?php endif ?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<?php if (UserAccesForm($user_acces_form,array('249'))): ?>
		<hr style="margin-top:0px">
		 <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
		<div class="row">
			<div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien">Nama</label>
					<div class="col-md-8">
						<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="asalpasien">Status </label>
					<div class="col-md-8">
						<select name="status" id="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="1" <?=($status == 1 ? 'selected="selected"':'')?>>Aktif</option>
							<option value="0" <?=($status == 0 ? 'selected="selected"':'')?>>Tidak Aktif</option>
							<option value="#" <?=($status =='#' ? 'selected="selected"':'')?>>- All -</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien"></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="button" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				
			</div>
			
		</div>
		<?php echo form_close() ?>
		<hr>
		<?php endif ?>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="index_list">
			<thead>
				<tr>
					<th>#</th>
					<th>No</th>
					<th>NAMA</th>
					<th>AKUN</th>
					<th>STATUS</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
$(document).ready(function(){
	// 	
	load_index();
   $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
	  table.destroy();
		load_index();
    }
  });
});
$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_index();
});

function load_index(){
	var status=$("#status").val();
	var nama=$("#nama").val();
	
	
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "4%", "targets": 0, "orderable": true,"visible":false },
					{ "width": "5%", "targets": 1, "orderable": true },
					{ "width": "20%", "targets": 2, "orderable": true },
					{ "width": "40%", "targets": 3, "orderable": true },
					{ "width": "15%", "targets": 4, "orderable": true },
					{ "width": "10%", "targets": 5, "orderable": true }
				],
            ajax: { 
                url: '{site_url}mdata_piutang/getIndex', 
                type: "POST" ,
                dataType: 'json',
				data : {
						status:status,
						nama:nama
					   }
            }
        });
}

$(document).on("click", ".removeData", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,0).data()
	
	swal({
		title: "Anda Yakin ?",
		text : "Akan Menghapus ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		removeData(id);
	});
});
function removeData($id){
	console.log($id);
	var id=$id;		

	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}mdata_piutang/delete/'+id,
		complete: function(data) {
			$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
			table.ajax.reload( null, false ); 
		}
	});
}
$(document).on("click", ".aktifData", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,0).data()
	
	swal({
		title: "Anda Yakin ?",
		text : "Akan Mengaktifkan Kembali ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		aktifkan(id);
	});
});
function aktifkan($id){
	console.log($id);
	var id=$id;		

	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}mdata_piutang/aktifkan/'+id,
		complete: function(data) {
			$.toaster({priority : 'success', title : 'Succes!', message : ' Aktif Kembali'});
			table.ajax.reload( null, false ); 
		}
	});
}
</script>
