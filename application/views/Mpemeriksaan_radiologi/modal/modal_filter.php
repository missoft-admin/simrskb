<style>
.gray-background {
    background-color: #f2f2f2 !important;
}

.white-background {
    background-color: #ffffff !important;
}
</style>

<!-- Filter Modal -->
<div class="modal fade in" id="modal-filter" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h5 class="block-title">Tarif Radiologi</h5>
                </div>
            </div>
            <div class="block-content">
                <div class="form-horizontal">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tipe-radiologi" class="col-xs-12">Tipe</label>
                                <div class="col-xs-12">
                                    <select id="tipe-radiologi" class="js-select2 form-control" style="width: 100%;">
                                        <?php foreach (list_variable_ref(245) as $row) { ?>
                                            <option value="<?php echo $row->id; ?>" <?php echo $row->id === $tipe_pemeriksaan ? 'selected' : ''; ?> <?php echo $row->id === $tipe_pemeriksaan ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="head-parent" class="col-xs-12">Head Parent</label>
                                <div class="col-xs-12">
                                    <select id="head-parent" class="js-select2 form-control" style="width: 100%;">
                                        <option value="0">Semua</option>
                                        <?php foreach ($head_parent as $row) { ?>
                                            <option value="<?= $row->path; ?>"><?= TreeView($row->level, $row->nama); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="sub-parent" class="col-xs-12">Sub Parent</label>
                                <div class="col-xs-12">
                                    <select class="js-select2 form-control" id="sub-parent" multiple style="width: 100%;"></select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-xs-12">&nbsp;</label>
                                <div class="col-xs-12">
                                    <button type="button" class="btn btn-success" id="btn-filter" style="width: 100%;">
                                        Filter <i class="fa fa-filter"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr>

                <table class="table table-bordered table-striped" id="tarif-radiologi">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Tarif</th>
                        <th>Aksi</th>
                    </tr>
                </thead></table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var selected = {
    id: '{tarif_radiologi_id}',
    tipe: '{tarif_radiologi_tipe}',
    nama: '{tarif_radiologi_nama}',
}

$(document).ready(function() {
    getHeadParent('1');

    if (selected.id) {
        setTarifRadiologi(selected);
    }

    $(document).on('click', '#btn-filter', function() {
        let tipe = $("#tipe-radiologi option:selected").val();
        let headParent = $("#head-parent option:selected").val();
        let subParent = $("#sub-parent").val();

        getDaftarPemeriksaan(tipe, headParent, subParent);
    });

    $(document).on('change', '#tipe-radiologi', function() {
        const tipeRadiologi = $(this).val();

        if ($(this).val() != '') {
            getHeadParent(tipeRadiologi);
        }
    });

    $(document).on('change', '#head-parent', function() {
        const headParent = $(this).val();

        if ($(this).val() != '') {
            getSubParent(headParent);
        }
    });

    $(document).on('click', '.select-tarif', function() {
        selected = {
            id: $(this).data('id'),
            tipe: $(this).data('tipe'),
            nama: $(this).data('nama'),
        }

        setTarifRadiologi(selected);
    });
});

function setTarifRadiologi(selected) {
    $('#tarif-radiologi-selected-id').val(selected.id);
    $('#tarif-radiologi-selected-tipe').html(selected.tipe);
    $('#tarif-radiologi-selected-nama').html(selected.nama);

    updateSelectedButton(selected.id);
    getTarifPemeriksaan(selected.id);
}

function getDaftarPemeriksaan(tipe, headParent, subParent) {
    $('#tarif-radiologi').DataTable().destroy();
    $('#tarif-radiologi').DataTable({
        "bSort": false,
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}mpemeriksaan_radiologi/get_daftar_pemeriksaan',
            type: "POST",
            dataType: 'json',
            data: {
                tipe: tipe,
                head_parent: headParent,
                sub_parent: JSON.stringify(subParent),
            }
        },
        "columnDefs": [
            {
                "width": "10%",
                "targets": [0],
                "className": "text-center"
            },
            {
                "width": "50%",
                "targets": [1],
                "className": "text-left"
            },
            {
                "width": "40%",
                "targets": [2],
                "className": "text-center"
            },
        ],
        "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
        "rowCallback": function(row, data) {
            let isTarifKelompok = data[3];

            if (isTarifKelompok == 1) {
                $(row).addClass('gray-background');
            } else {
            $(row).addClass('white-background');
            }
        },
        "drawCallback": function(settings) {
            updateSelectedButton(selected.id);
        },
    });
}

function updateSelectedButton(selectedId) {
    $('#tarif-radiologi .select-tarif').each(function() {
        let id = $(this).data('id');
        if (selectedId == id) {
            $(this).prop('disabled', true); // Menjadikan tombol disabled
            $(this).html('<i class="fa fa-check"></i> TERPILIH'); // Mengubah teks tombol
            $(this).removeClass('btn-primary').addClass('btn-success'); // Mengubah kelas tombol
        } else {
            $(this).prop('disabled', false); // Mengembalikan tombol ke status enabled
            $(this).html('<i class="fa fa-level-down"></i> UBAH TARIF'); // Mengembalikan teks tombol
            $(this).removeClass('btn-success').addClass('btn-primary'); // Mengembalikan kelas tombol
        }
    });
}

function getTarifPemeriksaan(tarifId) {
    $.ajax({
        url: '{site_url}mpemeriksaan_radiologi/get_tarif_pemeriksaan/' + tarifId,
        dataType: 'json',
        success: function(data) {
            displayDetail(data);
        }
    });
}

function displayDetail(data) {
    $('#tarif-radiologi-detail-selected tbody').empty();
    if (data.length > 0) {
        data.forEach(function(detail) {
            var row = '<tr>' +
                '<td>' + detail.kelas + '</td>' +
                '<td>' + $.number(detail.jasasarana) + '</td>' +
                '<td>' + $.number(detail.jasapelayanan) + '</td>' +
                '<td>' + $.number(detail.bhp) + '</td>' +
                '<td>' + $.number(detail.biayaperawatan) + '</td>' +
                '<td>' + $.number(detail.total) + '</td>' +
                '</tr>';
            $('#tarif-radiologi-detail-selected tbody').append(row);
        });
    } else {
        $('#tarif-radiologi-detail-selected tbody').html('<tr><td colspan="6">No data available</td></tr>');
    }
}

function getHeadParent(tipeRadiologi) {
    $('#head-parent').html('');

    $.ajax({
        url: '{site_url}mtarif_radiologi/find_headparent/' + tipeRadiologi,
        dataType: "json",
        success: function(data) {
            $('#head-parent').append('<option value="">Semua</option>');
            $('#head-parent').append(data.detail);
        }
    });
}

function getSubParent(headParent) {
    $('#sub-parent').html('');
            
    $.ajax({
        url: '{site_url}mtarif_radiologi/find_index_subparent/' + headParent,
        dataType: "json",
        success: function(data) {
            $('#sub-parent').append(data);
        }
    });
}
</script>
