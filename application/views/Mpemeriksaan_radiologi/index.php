<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<?php if (UserAccesForm($user_acces_form,array('158'))) { ?>
<div class="block">
    <div class="block-header">
        <?php if (UserAccesForm($user_acces_form,array('160'))) { ?>
        <ul class="block-options">
            <li>
                <a href="{base_url}mpemeriksaan_radiologi/create" class="btn"><i class="fa fa-plus"></i></a>
            </li>
        </ul>
        <? } ?>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?php if (UserAccesForm($user_acces_form,array('159'))) { ?>
        <hr style="margin-top:0px">

        <div class="form-horizontal">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="tipe_pemeriksaan">Tipe Pemeriksaan</label>
                        <div class="col-md-8">
                            <select name="tipe_pemeriksaan" id="tipe_pemeriksaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
                                <?php foreach (list_variable_ref(245) as $row) { ?>
                                    <option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="jenis_pemeriksaan">Jenis Pemeriksaan</label>
                        <div class="col-md-8">
                            <select name="jenis_pemeriksaan" id="jenis_pemeriksaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
                                <?php foreach (list_variable_ref(246) as $row) { ?>
                                    <option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="anatomi_tubuh">Anatomi Tubuh</label>
                        <div class="col-md-8">
                            <select name="anatomi_tubuh" id="anatomi_tubuh" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
                                <?php foreach (list_variable_ref(248) as $row) { ?>
                                    <option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="posisi_pemeriksaan">Posisi Pemeriksaan</label>
                        <div class="col-md-8">
                            <select name="posisi_pemeriksaan" id="posisi_pemeriksaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
                                <?php foreach (list_variable_ref(247) as $row) { ?>
                                    <option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="">Nama Pemeriksaan</label>
                        <div class="col-md-8">
                            <input type="text" name="nama_pemeriksaan" id="nama_pemeriksaan" class="form-control" style="width: 100%;">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="status">Status</label>
                        <div class="col-md-8">
                            <select name="status" id="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="1">Aktif</option>
                                <option value="0">Tidak Aktif</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for=""></label>
                        <div class="col-md-8">
                            <button id="btn-filter" class="btn btn-success text-uppercase" type="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>
        <? } ?>

        <div class="table-responsive">
            <table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tipe Pemeriksaan</th>
                        <th>Kode</th>
                        <th>Nama Pemeriksaan</th>
                        <th>Harga</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<? } ?>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
jQuery(function() {
    BaseTableDatatables.init();
    loadDataTable();

    $("#btn-filter").click(function() {
        loadDataTable();
    });
});

function loadDataTable() {
    $('#datatable-simrs').DataTable().destroy();
    $('#datatable-simrs').DataTable({
        "bSort": false,
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": '{site_url}mpemeriksaan_radiologi/getIndex',
            "type": "POST",
            "dataType": "json",
            "data": {
                tipe_pemeriksaan: $('#tipe_pemeriksaan').val(),
                jenis_pemeriksaan: $('#jenis_pemeriksaan').val(),
                anatomi_tubuh: $('#anatomi_tubuh').val(),
                posisi_pemeriksaan: $('#posisi_pemeriksaan').val(),
                nama_pemeriksaan: $('#nama_pemeriksaan').val(),
                status: $('#status option:selected').val(),
            }
        },
        "columnDefs": [{
                "width": "10%",
                "targets": [0, 1, 2, 3, 4, 5, 6],
                "orderable": true
            },
        ]
    });
}
</script>
