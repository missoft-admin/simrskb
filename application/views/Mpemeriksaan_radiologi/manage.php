<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}mpemeriksaan_radiologi" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?php echo form_open('mpemeriksaan_radiologi/save','class="form-horizontal push-10-t" id="form-work"') ?>
				<div class="row">
            <div class="col-md-6">
							<div class="form-group">
									<label class="col-md-12 -label" for="tipe_pemeriksaan">Tipe Pemeriksaan</label>
									<div class="col-md-12">
										<div class="input-group" style="width: 100%;">
											<select name="tipe_pemeriksaan" id="tipe_pemeriksaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
												<?php foreach (list_variable_ref(245) as $row) { ?>
														<option value="<?php echo $row->id; ?>" <?php echo $row->st_default == 1 ? 'selected' : ''; ?> <?php echo $row->id == $tipe_pemeriksaan ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
												<?php } ?>
											</select>
											<span class="input-group-btn">
												<button type="button" id="refresh-tipe-pemeriksaan" class="btn btn-warning">
													<i class="fa fa-refresh"></i>
												</button>
												<a href="{base_url}merm_referensi/create/245" target="_blank" class="btn btn-success">
													<i class="fa fa-plus"></i>
												</a>
											</span>
										</div>
									</div>
							</div>
							<div class="form-group">
									<label class="col-md-12" for="anatomi_tubuh">Anatomi Tubuh</label>
									<div class="col-md-12">
										<div class="input-group" style="width: 100%;">
											<select name="anatomi_tubuh" id="anatomi_tubuh" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
													<?php foreach (list_variable_ref(248) as $row) { ?>
															<option value="<?php echo $row->id; ?>" <?php echo $row->st_default == 1 ? 'selected' : ''; ?> <?php echo $row->id == $anatomi_tubuh ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
													<?php } ?>
											</select>
											<span class="input-group-btn">
												<button type="button" id="refresh-anatomi-tubuh" class="btn btn-warning">
													<i class="fa fa-refresh"></i>
												</button>
												<a href="{base_url}merm_referensi/create/248" target="_blank" class="btn btn-success">
													<i class="fa fa-plus"></i>
												</a>
											</span>
										</div>
									</div>
							</div>
							<div class="form-group">
									<label class="col-md-12" for="nama_pemeriksaan">Nama Pemeriksaan</label>
									<div class="col-md-12">
											<input type="text" name="nama_pemeriksaan" id="nama_pemeriksaan" placeholder="Nama Pemeriksaan" class="form-control" style="width: 100%;" value="{nama_pemeriksaan}">
									</div>
							</div>
							<div class="form-group">
									<label class="col-md-12" for="posisi_pemeriksaan">Posisi Pemeriksaan</label>
									<div class="col-md-12">
										<div class="input-group" style="width: 100%;">
											<select name="posisi_pemeriksaan" id="posisi_pemeriksaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
													<?php foreach (list_variable_ref(247) as $row) { ?>
															<option value="<?php echo $row->id; ?>" <?php echo $row->st_default == 1 ? 'selected' : ''; ?> <?php echo $row->id == $posisi_pemeriksaan ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
													<?php } ?>
											</select>
											<span class="input-group-btn">
												<button type="button" id="refresh-posisi-pemeriksaan" class="btn btn-warning">
													<i class="fa fa-refresh"></i>
												</button>
												<a href="{base_url}merm_referensi/create/247" target="_blank" class="btn btn-success">
													<i class="fa fa-plus"></i>
												</a>
											</span>
										</div>
									</div>
							</div>
						</div>
            <div class="col-md-6">
							<div class="form-group">
									<label class="col-md-12" for="jenis_pemeriksaan">Jenis Pemeriksaan</label>
									<div class="col-md-12">
										<div class="input-group" style="width: 100%;">
											<select name="jenis_pemeriksaan" id="jenis_pemeriksaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
													<?php foreach (list_variable_ref(246) as $row) { ?>
															<option value="<?php echo $row->id; ?>" <?php echo $row->st_default == 1 ? 'selected' : ''; ?> <?php echo $row->id == $jenis_pemeriksaan ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
													<?php } ?>
											</select>
											<span class="input-group-btn">
												<button type="button" id="refresh-jenis-pemeriksaan" class="btn btn-warning">
													<i class="fa fa-refresh"></i>
												</button>
												<a href="{base_url}merm_referensi/create/246" target="_blank" class="btn btn-success">
													<i class="fa fa-plus"></i>
												</a>
											</span>
										</div>
									</div>
							</div>
							<div class="form-group">
									<label class="col-md-12" for="kode">Kode</label>
									<div class="col-md-12">
											<input type="text" name="kode" id="kode" placeholder="Kode" class="form-control" style="width: 100%;" value="{kode}">
									</div>
							</div>
							<div class="form-group">
									<label class="col-md-12" for="nama_pemeriksaan_eng">Nama Pemeriksaan (English Version)</label>
									<div class="col-md-12">
											<input type="text" name="nama_pemeriksaan_eng" id="nama_pemeriksaan_eng" placeholder="Nama Pemeriksaan (English Version)" class="form-control" style="width: 100%;" value="{nama_pemeriksaan_eng}">
									</div>
							</div>
							<div class="form-group">
									<label class="col-md-12" for="urutan_tampil">Urutan Tampil</label>
									<div class="col-md-12">
											<input type="text" name="urutan_tampil" id="urutan_tampil" placeholder="Urutan Tampil" class="form-control" style="width: 100%;" value="{urutan_tampil}">
									</div>
							</div>
						</div>
				</div>
    </div>

    <div class="block-content">
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="tarif-radiologi-selected">
                <thead>
                    <tr>
                        <th width="40%">Tipe</th>
                        <th width="40%">Nama Tarif</th>
                        <th width="20%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
									<tr>
										<td id="tarif-radiologi-selected-tipe"></td>
										<td id="tarif-radiologi-selected-nama"></td>
										<td>
											<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-filter">
												<i class="fa fa-cog"></i> Setting Tarif
											</button>
										</td>
									</tr>
								</tbody>
            </table>

            <table class="table table-bordered table-striped" id="tarif-radiologi-detail-selected">
                <thead>
                    <tr>
                        <th>Kelas Tarif</th>
                        <th>Jasa Sarana</th>
                        <th>Jasa Pelayanan</th>
                        <th>BHP</th>
                        <th>Biaya Perawatan</th>
                        <th>Total Tarif</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="block-content">
						<div class="col-md-12" style="text-align: right;">
								<button class="btn btn-success" type="submit">Simpan</button>
								<a href="{base_url}mpemeriksaan_radiologi" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
						</div>
        </div>
    </div>

		<input type="hidden" id="tarif-radiologi-selected-id" name="tarif_radiologi_id" value="{tarif_radiologi_id}">
    
		<br><br>

    <?php echo form_hidden('id', $id); ?>
    <?php echo form_close() ?>
</div>

<?php $this->load->view('Mpemeriksaan_radiologi/modal/modal_filter'); ?>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$(document).on('click', '#refresh-tipe-pemeriksaan', function() {
		$('#tipe_pemeriksaan').html('');

		$.ajax({
				url: '{site_url}mpemeriksaan_radiologi/get_data_referensi/245/{tipe_pemeriksaan}',
				dataType: "json",
				success: function(data) {
						$('#tipe_pemeriksaan').append(data);
				}
		});
	});

	$(document).on('click', '#refresh-anatomi-tubuh', function() {
		$('#anatomi_tubuh').html('');

		$.ajax({
				url: '{site_url}mpemeriksaan_radiologi/get_data_referensi/248/{anatomi_tubuh}',
				dataType: "json",
				success: function(data) {
						$('#anatomi_tubuh').append(data);
				}
		});
	});

	$(document).on('click', '#refresh-posisi-pemeriksaan', function() {
		$('#posisi_pemeriksaan').html('');

		$.ajax({
				url: '{site_url}mpemeriksaan_radiologi/get_data_referensi/247/{posisi_pemeriksaan}',
				dataType: "json",
				success: function(data) {
						$('#posisi_pemeriksaan').append(data);
				}
		});
	});

	$(document).on('click', '#refresh-jenis-pemeriksaan', function() {
		$('#jenis_pemeriksaan').html('');

		$.ajax({
				url: '{site_url}mpemeriksaan_radiologi/get_data_referensi/246/{jenis_pemeriksaan}',
				dataType: "json",
				success: function(data) {
						$('#jenis_pemeriksaan').append(data);
				}
		});
	});
});
</script>
