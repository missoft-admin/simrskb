<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('32'))){ ?>
<div class="block">
	<div class="block-header">
	<?php if (UserAccesForm($user_acces_form,array('33','34'))){ ?>
		<ul class="block-options">
			<li>
				<a href="{base_url}mrekanan/create" class="btn"><i class="fa fa-plus"></i></a>
			</li>
		</ul>

	<?}?>
		<h3 class="block-title">{title}</h3>
	</div>

	<div class="block-content">
		<?php echo form_open('mpegawai/filter','class="form-horizontal" id="form-work"') ?>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="tipe_rekanan">Kategori</label>
					<div class="col-md-8">
						<select id="tipe_rekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="0" selected>- All -</option>
							<option value="1" >ASURANSI</option>
							<option value="2" >REKANAN</option>
							
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="tipe_rekanan">Masa Berlaku</label>
					<div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
							<input class="form-control" type="text" id="tanggal_1" name="tanggal_1" placeholder="From" value="{tanggal_1}"/>
							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
							<input class="form-control" type="text" id="tanggal_2" name="tanggal_2" placeholder="To" value="{tanggal_2}"/>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-2">
				<div class="row">
					<div class="col-md-12">
						<button class="btn btn-success text-uppercase" type="button" onclick="load_index_all()" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close() ?>
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<br>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Nama Lengkap</th>
								<th>Alamat</th>
								<th>Telepon</th>
								<th>Masa Berlaku</th>
								<th>Diskon</th>
								<th>PIC</th>
								<th>Tanggal Penagihan</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
$(document).ready(function(){	
	load_index_all();
})	
function load_index_all(){
	$('#datatable-simrs').DataTable().destroy();	
	$('#datatable-simrs').DataTable({
			"autoWidth": false,
			"pageLength": 10,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}mrekanan/getIndex',
				type: "POST",
				dataType: 'json',
				data : {
						tipe_rekanan:$("#tipe_rekanan").val(),
						tanggal_1:$("#tanggal_1").val(),
						tanggal_2:$("#tanggal_2").val(),
						
					   }
			},
			"columnDefs": [
				{ "width": "5%", "targets": 0, "orderable": true },
				{ "width": "10%", "targets": 1, "orderable": true },
				{ "width": "10%", "targets": 2, "orderable": true },
				{ "width": "10%", "targets": 3, "orderable": true },
				{ "width": "10%", "targets": 4, "orderable": true },
				{ "width": "10%", "targets": 5, "orderable": true },
				{ "width": "10%", "targets": 6, "orderable": true },
				{ "width": "10%", "targets": 7, "orderable": true },
				{ "width": "10%", "targets": 8, "orderable": true },
				{ "width": "10%", "targets": 9, "orderable": true },
			]
		});
}
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		
	});
</script>
