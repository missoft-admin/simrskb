<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php
	$st_info='';
	$d_info='';
	$st_tarif='';
	$d_tarif='';
	if ($id==''){
		if (UserAccesForm($user_acces_form,array('33'))=='0'){
			$st_info='readonly';
			$d_info='disabled';
		}
		if (UserAccesForm($user_acces_form,array('34'))=='0'){
			$st_tarif='readonly';
			$d_tarif='disabled';
		}
	}else{
		if (UserAccesForm($user_acces_form,array('35'))=='0'){
			$st_info='readonly';
			$d_info='disabled';

		}
		if (UserAccesForm($user_acces_form,array('36'))=='0'){
			$st_tarif='readonly';
			$d_tarif='disabled';
		}
	}


?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mrekanan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mrekanan/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tipe_rekanan">Tipe</label>
				<div class="col-md-7">
					<select name="tipe_rekanan" <?=$d_info?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($tipe_rekanan == 1 ? 'selected="selected"':'')?>>ASURANSI</option>
						<option value="2" <?=($tipe_rekanan == 2 ? 'selected="selected"':'')?>>REKANAN</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama</label>
				<div class="col-md-7">
					<input type="text" <?=$st_info?> class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
					<input type="hidden" id="id" placeholder="Nama" name="id" value="{id}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama Lengkap</label>
				<div class="col-md-7">
					<input type="text" <?=$st_info?> class="form-control" id="namalengkap" placeholder="Nama Lengkap" name="namalengkap" value="{namalengkap}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="alamat">Alamat</label>
				<div class="col-md-7">
					<textarea class="form-control" <?=$st_info?>  id="alamat" placeholder="Alamat" name="alamat" required="" aria-required="true">{alamat}</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="telepon">Telepon</label>
				<div class="col-md-7">
					<input type="text" <?=$st_info?> class="form-control" id="telepon" placeholder="Telepon" name="telepon" value="{telepon}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="fax">Fax</label>
				<div class="col-md-7">
					<input type="text" <?=$st_info?> class="form-control" id="fax" placeholder="Fax" name="fax" value="{fax}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="email">Email</label>
				<div class="col-md-7">
					<input type="text" <?=$st_info?> class="form-control" id="email" placeholder="Email" name="email" value="{email}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="jenispks">Jenis PKS</label>
				<div class="col-md-7">
					<input type="text" <?=$st_info?> class="form-control" id="jenispks" placeholder="Jenis PKS" name="jenispks" value="{jenispks}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="prosedur">Prosedur</label>
				<div class="col-md-7">
					<textarea  <?=$d_info?> class="form-control js-summernote" id="prosedur" placeholder="Prosedur" name="prosedur" rows="10">{prosedur}</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="penggunaanformulir">Penggunaan Formulir</label>
				<div class="col-md-7">
					<select name="penggunaanformulir" <?=$d_info?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($penggunaanformulir == 1 ? 'selected="selected"':'')?>>Ya</option>
						<option value="2" <?=($penggunaanformulir == 2 ? 'selected="selected"':'')?>>Tidak</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="alokasi">Alokasi</label>
				<div class="col-md-7">
					<input type="text" <?=$st_info?>  class="form-control" id="alokasi" placeholder="Alokasi" name="alokasi" value="{alokasi}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="alokasi">Masa Berlaku</label>
				<div class="col-md-3">
					<div class="input-group date">
						<input type="text" <?=$st_info?>   class="js-datepicker form-control " data-date-format="dd-mm-yyyy" id="masa_berlaku" placeholder="HH/BB/TTTT" name="masa_berlaku" value="<?= $masa_berlaku ?>" >
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="alokasi">Besaran Diskon</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="text" <?=$st_info?>  class="form-control diskon" id="besaran_diskon" placeholder="Diskon" name="besaran_diskon" value="{besaran_diskon}" required="" aria-required="true">
						<span class="input-group-addon">%</span>
					</div>
					
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="pic">PIC</label>
				<div class="col-md-7">
					<input type="text" <?=$st_info?>  class="form-control" id="pic" placeholder="PIC" name="pic" value="{pic}" required="" aria-required="true">
				</div>
			</div>
			<?if ($id){?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="pic">PKS</label>
				<div class="col-md-7">
					<button class="btn btn-sm btn-primary" onclick="show_pks()" type="button">Upload PKS</button>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="pic"></label>
				<div class="col-md-7">
					<table class="table table-bordered" id="list_file_2">
						<thead>
							<tr>
								<th width="50%" class="text-center">PKS File</th>
								<th width="15%" class="text-center">Size</th>
								<th width="35%" class="text-center">User</th>
								<th width="35%" class="text-center">X</th>
								
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
			<?}?>
			<hr>
			<div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tadm_rawatjalan">Tarif ADM Rajal (Administrasi)</label>
				<div class="col-md-7">
					<select name="tadm_rawatjalan" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_administrasi',array('idtipe' => '1', 'idjenis' => '1', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tadm_rawatjalan == $row->id) ? "selected" : "" ?>><?=GetJenisAdministrasi($row->idjenis).' ( '.$row->nama.' )';?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tkartu_rawatjalan">Tarif ADM Rajal (Cetak Kartu)</label>
				<div class="col-md-7">
					<select name="tkartu_rawatjalan" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_administrasi',array('idtipe' => '1', 'idjenis' => '2', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tkartu_rawatjalan == $row->id) ? "selected" : "" ?>><?=GetJenisAdministrasi($row->idjenis).' ( '.$row->nama.' )';?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tadm_rawatinap">Tarif ADM Ranap (Administrasi)</label>
				<div class="col-md-7">
					<select name="tadm_rawatinap" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_administrasi',array('idtipe' => '2', 'idjenis' => '1', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tadm_rawatinap == $row->id) ? "selected" : "" ?>><?=GetJenisAdministrasi($row->idjenis).' ( '.$row->nama.' )';?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tkartu_rawatinap">Tarif ADM Ranap (Cetak Kartu)</label>
				<div class="col-md-7">
					<select name="tkartu_rawatinap" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_administrasi',array('idtipe' => '2', 'idjenis' => '2', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tkartu_rawatinap == $row->id) ? "selected" : "" ?>><?=GetJenisAdministrasi($row->idjenis).' ( '.$row->nama.' )';?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="toperasi_sewaalat">Tarif Sewa Alat Kamar Operasi</label>
				<div class="col-md-7">
					<select name="toperasi_sewaalat" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_operasi_sewaalat',array('level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($toperasi_sewaalat == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="toperasi_tindakan">Tarif Jasa Tindakan Operasi</label>
				<div class="col-md-7">
					<select name="toperasi_tindakan" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_operasi',array('idtipe' => '3', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($toperasi_tindakan == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="toperasi_jenis">Tarif Jenis Operasi</label>
				<div class="col-md-7">
					<select name="toperasi_jenis[]" <?=$d_tarif?> class="js-select2 form-control" multiple style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mjenis_operasi', array('status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=in_array($row->id, $toperasi_jenis) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tradiologi_xray">Tarif Radiologi (X-RAY)</label>
				<div class="col-md-7">
					<select name="tradiologi_xray" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_radiologi',array('idtipe' => '1', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tradiologi_xray == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tradiologi_usg">Tarif Radiologi (USG)</label>
				<div class="col-md-7">
					<select name="tradiologi_usg" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_radiologi',array('idtipe' => '2', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tradiologi_usg == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tradiologi_ctscan">Tarif Radiologi (CT-Scan)</label>
				<div class="col-md-7">
					<select name="tradiologi_ctscan" <?=$d_tarif?>  class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_radiologi',array('idtipe' => '3', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tradiologi_ctscan == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tradiologi_mri">Tarif Radiologi (MRI)</label>
				<div class="col-md-7">
					<select name="tradiologi_mri" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_radiologi',array('idtipe' => '4', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tradiologi_mri == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tradiologi_bmd">Tarif Radiologi (BMD)</label>
				<div class="col-md-7">
					<select name="tradiologi_bmd" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_radiologi',array('idtipe' => '5', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tradiologi_bmd == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tlaboratorium_umum">Tarif Laboratorium Umum</label>
				<div class="col-md-7">
					<select name="tlaboratorium_umum" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_laboratorium',array('idtipe' => '1', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tlaboratorium_umum == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tlaboratorium_pa">Tarif Laboratorium PA</label>
				<div class="col-md-7">
					<select name="tlaboratorium_pa" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_laboratorium',array('idtipe' => '2', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tlaboratorium_pa == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tlaboratorium_pmi">Tarif Laboratorium PMI</label>
				<div class="col-md-7">
					<select name="tlaboratorium_pmi" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_laboratorium',array('idtipe' => '3', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tlaboratorium_pmi == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tfisioterapi">Tarif Fisioterapi</label>
				<div class="col-md-7">
					<select name="tfisioterapi" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_fisioterapi',array('level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tfisioterapi == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatjalan">Tarif IGD</label>
				<div class="col-md-7">
					<select name="tigd" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatjalan',array('level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($tigd == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatjalan">Tarif Poliklinik</label>
				<div class="col-md-7">
					<select name="trawatjalan" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatjalan',array('level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatjalan == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_fullcare1">Tarif Ranap Full Care (Perawatan)</label>
				<div class="col-md-7">
					<select name="trawatinap_fullcare1" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '1', 'idtipe' => '1', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_fullcare1 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_ecg1">Tarif Ranap ECG (Perawatan)</label>
				<div class="col-md-7">
					<select name="trawatinap_ecg1" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '1', 'idtipe' => '2', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_ecg1 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_visite1">Tarif Visite Dokter (Perawatan)</label>
				<div class="col-md-7">
					<select name="trawatinap_visite1" <?=$d_tarif?>  class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_visitedokter',array('idruangan' => '1', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_visite1 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_sewaalat1">Tarif Ranap Sewa Alat (Perawatan)</label>
				<div class="col-md-7">
					<select name="trawatinap_sewaalat1" <?=$d_tarif?>  class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '1', 'idtipe' => '4', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_sewaalat1 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_ambulance1">Tarif Ranap Ambulance (Perawatan)</label>
				<div class="col-md-7">
					<select name="trawatinap_ambulance1" <?=$d_tarif?>  class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '1', 'idtipe' => '5', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_ambulance1 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_fullcare2">Tarif Ranap Full Care (HCU)</label>
				<div class="col-md-7">
					<select name="trawatinap_fullcare2" <?=$d_tarif?>  class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '2', 'idtipe' => '1', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_fullcare2 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_ecg2">Tarif Ranap ECG (HCU)</label>
				<div class="col-md-7">
					<select name="trawatinap_ecg2" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '2', 'idtipe' => '2', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_ecg2 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_visite2">Tarif Visite Dokter (HCU)</label>
				<div class="col-md-7">
					<select name="trawatinap_visite2" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_visitedokter',array('idruangan' => '2', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_visite2 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_sewaalat2">Tarif Ranap Sewa Alat (HCU)</label>
				<div class="col-md-7">
					<select name="trawatinap_sewaalat2" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '2', 'idtipe' => '4', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_sewaalat2 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_ambulance2">Tarif Ranap Ambulance (HCU)</label>
				<div class="col-md-7">
					<select name="trawatinap_ambulance2" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '2', 'idtipe' => '5', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_ambulance2 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_fullcare3">Tarif Ranap Full Care (ICU)</label>
				<div class="col-md-7">
					<select name="trawatinap_fullcare3" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '3', 'idtipe' => '1', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_fullcare3 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_ecg3">Tarif Ranap ECG (ICU)</label>
				<div class="col-md-7">
					<select name="trawatinap_ecg3" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '3', 'idtipe' => '2', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_ecg3 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_visite3">Tarif Visite Dokter (ICU)</label>
				<div class="col-md-7">
					<select name="trawatinap_visite3" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_visitedokter',array('idruangan' => '3', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_visite3 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_sewaalat3">Tarif Ranap Sewa Alat (ICU)</label>
				<div class="col-md-7">
					<select name="trawatinap_sewaalat3" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '3', 'idtipe' => '4', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_sewaalat3 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_ambulance3">Tarif Ranap Ambulance (ICU)</label>
				<div class="col-md-7">
					<select name="trawatinap_ambulance3" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '3', 'idtipe' => '5', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_ambulance3 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_fullcare4">Tarif Ranap Full Care (Isolasi)</label>
				<div class="col-md-7">
					<select name="trawatinap_fullcare4" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '4', 'idtipe' => '1', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_fullcare4 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_ecg4">Tarif Ranap ECG (Isolasi)</label>
				<div class="col-md-7">
					<select name="trawatinap_ecg4" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '4', 'idtipe' => '2', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_ecg4 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_visite4">Tarif Visite Dokter (Isolasi)</label>
				<div class="col-md-7">
					<select name="trawatinap_visite4" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_visitedokter',array('idruangan' => '4', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_visite4 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_sewaalat4">Tarif Ranap Sewa Alat (Isolasi)</label>
				<div class="col-md-7">
					<select name="trawatinap_sewaalat4" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '4', 'idtipe' => '4', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_sewaalat4 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="trawatinap_ambulance4">Tarif Ranap Ambulance (Isolasi)</label>
				<div class="col-md-7">
					<select name="trawatinap_ambulance4" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_rawatinap',array('idruangan' => '4', 'idtipe' => '5', 'level' => '0', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($trawatinap_ambulance4 == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="truang_perawatan">Tarif Ruang (Perawatan)</label>
				<div class="col-md-7">
					<select name="truang_perawatan" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_ruangperawatan',array('idtipe' => '1', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($truang_perawatan == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="truang_hcu">Tarif Ruang (HCU)</label>
				<div class="col-md-7">
					<select name="truang_hcu" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_ruangperawatan',array('idtipe' => '2', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($truang_hcu == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="truang_icu">Tarif Ruang (ICU)</label>
				<div class="col-md-7">
					<select name="truang_icu" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_ruangperawatan',array('idtipe' => '3', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($truang_icu == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="truang_isolasi">Tarif Ruang (Isolasi)</label>
				<div class="col-md-7">
					<select name="truang_isolasi" <?=$d_tarif?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_ruangperawatan',array('idtipe' => '4', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($truang_isolasi == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mrekanan" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			
			<?php echo form_close() ?>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_upload" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title"><i class="fa fa-file-image-o"></i> Upload Document </h3>
				</div>
				<div class="block-content">
					<div class="col-md-12" style="margin-top: 10px;">
						<div class="block" id="box_file2">					
							 <table class="table table-bordered" id="list_file">
								<thead>
									<tr>
										<th width="50%" class="text-center">File</th>
										<th width="15%" class="text-center">Size</th>
										<th width="35%" class="text-center">User</th>
										<th width="35%" class="text-center">X</th>
										
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
						<div class="block">
							<form action="{base_url}mrekanan/upload" enctype="multipart/form-data" class="dropzone" id="image-upload">
								<input type="hidden" class="form-control" name="idrka" id="idrka" value="{id}" readonly>
								<div>
								  <h5>Bukti Upload</h5>
								</div>
							</form>
						</div>
						
					</div> 
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
			
		</div>
	</div>
</div>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

	var myDropzone ;
		
		$(document).ready(function(){
			$(".diskon").number(true,2,'.',',');
			if ($("#id").val()!=''){
			refresh_image();
				
			}
			$('#prosedur').summernote({
				height: 100,   //set editable area's height
				dialogsInBody: true,
				codemirror: { // codemirror options
				theme: 'monokai'
			  }	
			})


			Dropzone.autoDiscover = false;
			myDropzone = new Dropzone(".dropzone", { 
			   autoProcessQueue: true,
			   maxFilesize: 30,
			});
			myDropzone.on("complete", function(file) {
			  
			  refresh_image();
			  myDropzone.removeFile(file);
			  
			});
		})	
		function show_pks(){
			$("#modal_upload").modal('show');
			refresh_image();
		}
		$(document).on("click",".hapus_file",function(){	
			var currentRow=$(this).closest("tr");          
			 var id=currentRow.find("td:eq(4)").text();
			 $("#cover-spin").show();
			 $.ajax({
				url: '{site_url}mrekanan/hapus_file',
				type: 'POST',
				data: {id: id},
				success : function(data) {				
					currentRow.remove();
					refresh_image();
					$("#cover-spin").hide();
					// table.ajax.reload( null, false );				
				}
			});
		});
		
		function refresh_image(){
			var id=$("#idrka").val();
			$('#list_file tbody').empty();
			$("#cover-spin").show();
			// alert(id);
			$.ajax({
				url: '{site_url}mrekanan/refresh_image/'+id,
				dataType: "json",
				success: function(data) {
					if (data.detail!=''){
						$('#list_file_2 tbody').empty();
						$('#list_file tbody').empty();
						$("#box_file2").attr("hidden",false);
						$("#list_file tbody").append(data.detail);
						$("#list_file_2 tbody").append(data.detail);
					}
						$("#cover-spin").hide();
					// console.log();
					
				}
			});
		}

</script>