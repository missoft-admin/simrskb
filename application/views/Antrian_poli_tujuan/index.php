<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('1553'))): ?>
			<ul class="block-options">
		        <li>
		            <a href="{base_url}antrian_poli_tujuan/create" title="Tambah" class="btn"><i class="fa fa-plus"></i> Tambah</a>
		        </li>
		    </ul>
		<?php endif ?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		
		<table class="table table-bordered table-striped table-responsive" id="index_list">
			<thead>
				<tr>
					<th>No</th>
					<th>NAMA COUNTER</th>
					<th>JENIS</th>
					<th>USER</th>
					<th>STATUS</th>
					<th>ACTION</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
$(document).ready(function(){
	// 	
	load_index();
   $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
	  table.destroy();
		load_index();
    }
  });
});
$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_index();
});

function load_index(){
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "4%", "targets": 0, "orderable": true},
					{ "width": "12%", "targets": 1, "orderable": true },
					{ "width": "10%", "targets": 2, "orderable": true },
					{ "width": "15%", "targets": 3, "orderable": true },
					{ "width": "15%", "targets": 4, "orderable": true },
					{ "width": "10%", "targets": 5, "orderable": true },
					// { "width": "10%", "targets": 6, "orderable": true },
					{  className: "text-center", targets:[4, 5] },
				],
            ajax: { 
                url: '{site_url}antrian_poli_tujuan/getIndex', 
                type: "POST" ,
                dataType: 'json',
				data : {
						// status:status,
						// ref_head_id:ref_head_id
					   }
            }
        });
}


function removeData($id){
	console.log($id);
	var id=$id;		
	swal({
		title: "Anda Yakin ?",
		text : "Akan Menghapus ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		$.ajax({
			url: '{site_url}antrian_poli_tujuan/delete/'+id,
			complete: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
				table.ajax.reload( null, false ); 
			}
		});
	});
	
	
}

function aktifkan($id){
	console.log($id);
	var id=$id;		
	swal({
		title: "Anda Yakin ?",
		text : "Akan Mengaktifkan Kembali ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		table = $('#index_list').DataTable()	
		$.ajax({
			url: '{site_url}antrian_poli_tujuan/aktifkan/'+id,
			complete: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Aktif Kembali'});
				table.ajax.reload( null, false ); 
			}
		});
	});
	
}
</script>
