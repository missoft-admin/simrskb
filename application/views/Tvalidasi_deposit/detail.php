<style>
table.fixed {
      table-layout: fixed;
      width: 100%;
    }
    table.fixed td {
      overflow: hidden;
    }
</style>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tvalidasi_deposit/save_detail','class="form-horizontal push-10-t"') ?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Medrec</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="nojurnal" placeholder="No. Jurnal" name="nojurnal" value="{no_medrec}">
                        <input type="hidden" readonly class="form-control" id="id" placeholder="No. Jurnal" name="id" value="{id}">
                        <input type="hidden" readonly class="form-control" id="st_posting" placeholder="No. Jurnal" name="st_posting" value="{st_posting}">
                        <input type="hidden" readonly class="form-control" id="disabel" placeholder="No. Jurnal" name="disabel" value="{disabel}">
                    </div>
                </div>
                
                
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Pasien</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="distributor" placeholder="No. Jurnal" name="distributor" value="{namapasien}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No. Deposit</label>
					<div class="col-md-8">
                    <input type="text"  readonly class="form-control" id="distributor" placeholder="No. Jurnal" name="distributor" value="{notransaksi}">
                   
					</div>
                </div>
                
            </div>
			<div class="col-md-6">
				
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Transaksi</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=HumanDateShort($tanggal_transaksi)?>">
                        
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Kelompok Pasien</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="no_terima" placeholder="No. Penerimaan" name="no_terima" value="{kel_pasien}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Status Posting</label>
                    <div class="col-md-8">
                       <h2 class="block-title"><?=($st_posting=='1'?text_primary('SUDAH DIPOSTING'):text_default('MENUNGGU DIPOSTING'))?></h2>
                    </div>
                </div>
               
			</div>
            
        </div>
	</div>
	<?
		$q="SELECT *from makun_nomor A  ORDER BY A.noakun ";
		$list=$this->db->query($q)->result();
		
		
	?>
	<div class="block-content">
		<div class="row">
			<label class="col-md-2 control-label" for="tanggal"></label>
            <div class="col-md-10">
					<table class="table table-bordered table-striped fixed" id="index_list" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:35%"  class="text-center">KETERANGAN </th>
								<th style="width:20%"  class="text-center">NOMINAL</th>
								<th style="width:45%"  class="text-center">NO AKUN</th>								
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><strong>Pendapatan Deposit</strong></td>
								<td><input type="text"  readonly class="form-control decimal" id="nominal_pendapatan" placeholder="No. Faktur" name="nominal_pendapatan" value="<?=$nominal_pendapatan?>"></td>
								<td>
									<select id="idakun_pendapatan" <?=$disabel?> name="idakun_pendapatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach($list as $r){?>
										<option value="<?=$r->id?>" <?=($idakun_pendapatan==$r->id?'selected':'')?>><?=$r->noakun.' - '.$r->namaakun?></option>
										
										<?}?>
										
									</select>
								</td>
							</tr>
							<tr>
								<td><strong>Akun Pembayaran </strong></td>
								<td><input type="text"  readonly class="form-control decimal" id="nominal_bayar" placeholder="No. Faktur" name="nominal_bayar" value="<?=$nominal_bayar?>"></td>
								<td>
									<select id="idakun_bayar"  <?=$disabel?> name="idakun_bayar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<?foreach($list as $r){?>
										<option value="<?=$r->id?>" <?=($idakun_bayar==$r->id?'selected':'')?>><?=$r->noakun.' - '.$r->namaakun?></option>
										
										<?}?>
										
									</select>
								</td>
							</tr>
							
						</tbody>
					</table>
			</div>
		</div>
	</div>
	
	<?if ($disabel==''){?>
	<div class="block-content">
		 <hr style="margin-top:10px">
		 <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<div class="col-xs-12 text-right">
						<a href="{base_url}tvalidasi_deposit" class="btn btn-default" type="reset"><i class="fa fa-reply"></i> Batal</a>
						<button class="btn btn-success" name="btn_simpan" type="submit" value="2"><i class="si si-arrow-right"></i> Update & Posting</button>
						<button class="btn btn-primary" name="btn_simpan" type="submit" value="1"><i class="fa fa-save"></i> Update Akun</button>
					</div>
				</div>
				
            </div>
        </div>
		<hr style="margin-bottom:10px">		
		
	</div>
	<?}?>
</div>
<?php echo form_close(); ?>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
   
});


</script>