<?php  $generator = new Picqer\Barcode\BarcodeGeneratorPNG(); ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Hasil Pemeriksaan Laboratorium</title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      * {
        font-size: 13px !important;
      }
      table {
        font-size: 13px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      * {
        font-size: 13px !important;
      }
      table {
        font-size: 13px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <table class="content">
      <tr>
        <td style="text-align:center">
          <img src="assets/upload/pengaturan_printout_laboratorium_hasil/<?= strip_tags($pengaturan_printout['logo']); ?>" width="100px"><br>
          &nbsp;<?= strip_tags($pengaturan_printout['alamat']); ?><br>
          &nbsp;<?= strip_tags($pengaturan_printout['phone']); ?><br>
          &nbsp;<?= strip_tags($pengaturan_printout['website']); ?>
        </td>
      </tr>
    </table>
    <br>

    <p class="text-center" style="margin:0">
      <b><?= strip_tags($pengaturan_printout['label_header']); ?></b>
      <br>
      <i><?= strip_tags($pengaturan_printout['label_header_eng']); ?></i>
    </p>
    
    <br>
    
    <table class="content-2">
      <tr>
        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_noregister']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_noregister_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$nomor_register;?></td>

        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_nomedrec']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_nomedrec_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$nomor_medrec;?></td>
      </tr>
      <tr>
        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_nama']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_nama_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$nama_pasien;?></td>

        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_kelompok_pasien']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_kelompok_pasien_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$kelompok_pasien;?></td>
      </tr>
      <tr>
        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_alamat']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_alamat_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$alamat_pasien;?></td>

        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_nama_asuransi']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_nama_asuransi_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=($rekanan_asuransi ? $rekanan_asuransi : '-');?></td>
      </tr>
      <tr>
        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_dokter_perujuk']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_dokter_perujuk_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$dokter_perujuk;?></td>

        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_tanggal_lahir']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_tanggal_lahir_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=date("Y-m-d", strtotime($tanggal_lahir));?></td>
      </tr>
      <tr>
        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_nomor_laboratorium']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_nomor_laboratorium_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= str_pad($nomor_laboratorium, 4, "0", STR_PAD_LEFT);?></td>

        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_umur']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_umur_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$umur_tahun;?> Tahun <?=$umur_bulan;?> Bulan <?=$umur_hari;?> Hari</td>
      </tr>
      <tr>
        <td colspan="3">
          <img width="300px" src="data:image/png;base64, <?= base64_encode($generator->getBarcode(str_pad($nomor_laboratorium, 4, "0", STR_PAD_LEFT), $generator::TYPE_EAN_13)); ?>">
        </td>

        <td style="width:150px">
          <?= strip_tags($pengaturan_printout['label_rujukan']); ?><br>
          <i><?= strip_tags($pengaturan_printout['label_rujukan_eng']); ?></i>
        </td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=GetAsalRujukan($asal_rujukan);?></td>
      </tr>
    </table>
    
    <br>

    <?php foreach ($daftar_pemeriksaan as $row) { ?>
      <?php if($row->idkelompok != 1){ ?>
        <table>
          <?php if ($pengaturan_printout['tampilkan_nama_pemeriksaan']) { ?>
          <tr>
            <td><b>Nama Pemeriksaan</b></td>
          </tr>
          <tr>
            <td>
              <div style="border: 1px solid #ccc; border-radius: 5px; padding: 10px;">
                <?php echo TreeView(1, strtoupper($row->nama)); ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <?php echo $pengaturan_printout['tampilkan_flag'] && $row->flag ? '<span style="color: '.$pengaturan_form['warna_flag'].'">'.$row->flag.'</span>' : ''; ?>
                <?php echo $pengaturan_printout['tampilkan_flag_tidak_normal'] ? '<span style="color: '.$pengaturan_form['warna_tidak_normal'].'">'.(2 == $row->flag_normal ? '*' : '').'</span>' : ''; ?>
                <?php echo $pengaturan_printout['tampilkan_flag_kritis'] ? '<span style="color: '.$pengaturan_form['warna_nilai_kritis'].'">'.(2 == $row->flag_kritis ? '!' : '').'</span>' : ''; ?>
              </div>
            </td>
          </tr>
          <?php } ?>
          <?php if ($pengaturan_printout['tampilkan_hasil']) { ?>
          <tr>
            <td><b>Hasil Pemeriksaan</b></td>
          </tr>
          <tr>
            <td>
              <div style="border: 1px solid #ccc; border-radius: 5px; padding: 10px;">
                <?php echo $row->hasil; ?>
              </div>
            </td>
          </tr>
          <?php } ?>
        </table>
        <br>
      <?php } ?>
    <?php } ?>

    <table>
      <tr>
        <td>
          <table>
            <tr>
              <td width="150px">
                <?= strip_tags($pengaturan_printout['label_penanggung_jawab']); ?><br>
                <i><?= strip_tags($pengaturan_printout['label_penanggung_jawab_eng']); ?></i>
              </td>
              <td width="10px">:</td>
              <td><?=$dokter_penanggung_jawab?></td>
            </tr>
            <tr>
              <td width="150px">
                <?= strip_tags($pengaturan_printout['label_catatan']); ?><br>
                <i><?= strip_tags($pengaturan_printout['label_catatan_eng']); ?></i>
              </td>
              <td width="10px">:</td>
              <td><?=$catatan?></td>
            </tr>
            <tr>
              <td width="150px">
                <?= strip_tags($pengaturan_printout['label_interpretasi_hasil']); ?><br>
                <i><?= strip_tags($pengaturan_printout['label_interpretasi_hasil_eng']); ?></i>
              </td>
              <td width="10px">:</td>
              <td><?=$interpretasi_hasil_pemeriksaan?></td>
            </tr>
            <tr>
              <td colspan="3" style="font-size:10px!important"><?= strip_tags($pengaturan_printout['label_waktu_input_order']); ?> : <?= $tanggal_input_pemeriksaan; ?> | <?= strip_tags($pengaturan_printout['label_waktu_sampling']); ?> : <?= $tanggal_input_sampling; ?> | <?= strip_tags($pengaturan_printout['label_waktu_cetak']); ?> : <?= $tanggal_cetak; ?></td>
            </tr>
            <tr>
              <td colspan="3" style="font-size:10px!important"><?= strip_tags($pengaturan_printout['label_jumlah_cetak']); ?> : <?= $cetakan_ke; ?> - <?= strip_tags($pengaturan_printout['label_user_input_order']); ?>: <?= $user_input_pemeriksaan; ?> / <?= strip_tags($pengaturan_printout['label_user_sampling']); ?>: <?= $user_input_sampling; ?> / <?= strip_tags($pengaturan_printout['label_user_input_hasil']); ?>: <?= $user_input_hasil; ?> / <?= strip_tags($pengaturan_printout['label_user_cetak']); ?>: <?= $user_cetak; ?></i></td>
            </tr>
            <tr>
              <td colspan="3" style="font-size:10px!important"><?= strip_tags($pengaturan_printout['footer_notes']); ?></td>
            </tr>
        </table>
        </td>
        <td>
          <table>
            <tr>
              <td style="width:30%" class="text-center text-bold"><?= strip_tags($pengaturan_printout['label_tanda_tangan']); ?> <br> <i><?= strip_tags($pengaturan_printout['label_tanda_tangan_eng']); ?></i></td>
            </tr>
            <tr>
              <td style="width:30%" class="text-center"><img src="<?= base_url(); ?>qrcode/qr_code_ttd/<?= $user_cetak_id; ?>" width="100px"></td>
            </tr>
            <tr>
              <td style="width:30%" class="text-center text-bold">( <?= $user_cetak; ?> )</td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>
