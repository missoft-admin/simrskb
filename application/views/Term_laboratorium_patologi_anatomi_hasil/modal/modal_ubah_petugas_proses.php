<!-- Modal Ubah Petugas Proses -->
<div class="modal fade" id="modal-ubah-petugas-proses" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-danger">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Petugas Proses</h3>
				</div>
                <div class="block-content">
                    <div class="form-group">
                        <select id="petugas-proses" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <?php foreach ($petugas_proses as $item) { ?>
                                <option value="<?= $item->id ?>" <?= $item->id == $this->session->userdata('login_ppa_id') ? 'selected' : ''; ?>><?= $item->nama ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal" id="btn-update-petugas-proses">Ya</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#btn-update-petugas-proses').click(function () {
            var selectedPetugasProses = $('#petugas-proses').val();

            updatePetugasProses(selectedPetugasProses);
        });
    });

    function updatePetugasProses(selectedPetugasProses) {
        $('#table-pemeriksaan tbody tr').each(function () {
            var selectElement = $(this).find('.pemeriksaan-petugas-proses');

            selectElement.val(selectedPetugasProses).trigger('change');
        });
    }
</script>