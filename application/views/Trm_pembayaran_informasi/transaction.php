<?php echo ErrorSuccess($this->session)?>
<?php
    if ($error != '') {
        echo ErrorMessage($error);
    }
?>

<style>
	.control-label {
		text-align: left !important;
	}
</style>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
		<ul class="block-options">
			<li>
				<a href="{base_url}trm_pembayaran_informasi/index" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
	</div>
	<div class="block-content">

		<hr style="margin-top:-10px">

		<?php echo form_open('trm_pembayaran_informasi/save_transaction', 'class="form-horizontal" id="form-work"') ?>
		
		<div class="col-12">
			<div class="form-group">
				<label class="col-md-2 control-label" for="">No. Pengajuan</label>
				<div class="col-md-10">
					<input class="form-control" type="text" readonly placeholder="No. Pengajuan" value="{no_pengajuan}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-2 control-label" for="">Tanggal Pengajuan</label>
				<div class="col-md-10">
					<input class="form-control" type="text" readonly placeholder="Tanggal Pengajuan" value="{tanggal_pengajuan}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-2 control-label" for="">Nama Pemohon</label>
				<div class="col-md-10">
					<input class="form-control" type="text" readonly placeholder="Nama Pemohon" value="{nama_pemohon}">
				</div>
			</div>
		
			<div class="form-group">
				<label class="col-md-2 control-label" for="">Jumlah Pengajuan</label>
				<div class="col-md-10">
					<input class="form-control" type="text" readonly placeholder="Jumlah Pengajuan" value="{jumlah_pengajuan}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-2 control-label" for="">Keterangan</label>
				<div class="col-md-10">
					<textarea class="form-control summernote js-summernote-custom-height" readonly placeholder="Keterangan">{keterangan}</textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-2 control-label" for="">Tanggal Pembayaran</label>
				<div class="col-md-10">
					<div class="input-group date">
						<input class="js-datepicker form-control input" type="text" name="tanggal_pembayaran" placeholder="Tanggal Pembayaran" value="{tanggal_pembayaran}" data-date-format="dd/mm/yyyy"/>
						<span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
					</div>
				</div>
			</div>
		</div>
		
		<hr>

		<div class="col-12">
			<table class="table table-bordered" id="detailPengajuan">
				<thead>
					<tr>
						<td width="5%">No. Medrec</td>
						<td width="10%">Nama Pasien</td>
						<td width="10%">Tanggal Kunjungan</td>
						<td width="10%">Jenis Kunjungan</td>
						<td width="10%">Dokter</td>
						<td width="8%">Jenis Pengajuan</td>
						<td width="8%">Estimasi Selesai</td>
						<td width="8%">Nominal</td>
						<td width="8%">Diskon</td>
						<td width="8%">Nominal Akhir</td>
						<td width="10%">Aksi</td>
					</tr>
				</thead>
				<tbody></tbody>
				<tfoot>
					<tr>
						<td colspan="8"><label class="pull-right"><b>Sub Total</b></label></td>
						<td colspan="3"><b>
							<input class="form-control input-sm" readonly type="text" id="sub_total" name="sub_total" value="{sub_total}">
						</td>
					</tr>
					<tr>
						<td colspan="8"><label class="pull-right"><b>Discount</b></label></td>
						<td colspan="3"><b>
							<div class="input-group">
								<span class="input-group-addon">Rp</span>
								<input class="form-control" type="text" id="diskon_rp" name="diskon_rp" placeholder="Discount Rp" value="{diskon_rp}">
							</div>
							<div class="input-group">
								<span class="input-group-addon"> %. </span>
								<input class="form-control" type="text" id="diskon_persen" name="diskon_persen" placeholder="Discount %"  value="">
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="8"><label class="pull-right"><b>Grand Total</b></label></td>
						<td colspan="3"><b>
							<input class="form-control input-sm " readonly type="text" id="grand_total" name="grand_total" value="{grand_total}">
						</td>
					</tr>
					<tr>
						<td colspan="8"></td>
						<td colspan="3">
							<button class="btn btn-success" id="btnModalPembayaran" data-toggle="modal"  data-target="#PembayaranModal"  type="button">Pilih Cara Pembayaran</button>
						</td>
					</tr>
				<tfoot>
			</table>
		</div>

		<hr>
	
		<div class="col-12">
			<table id="detailPembayaran" class="table table-striped table-bordered" style="margin-bottom: 0;">
				<thead>
					<tr>
						<th style="width: 5%;">#</th>
						<th style="width: 25%;">Jenis Pembayaran</th>
						<th style="width: 35%;">Keterangan</th>
						<th style="width: 10%;">Nominal</th>
						<th style="width: 10%;">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php $no = 1;?>
					<?php foreach ($detail_pembayaran as $row) {?>
						<tr>
							<td hidden><?=$row->idmetode?></td>
							<td><?=$no++?></td>
							<td><?=metodePembayaran($row->idmetode)?></td>
							<td><?=$row->keterangan?></td>
							<td hidden><?=$row->nominal?></td>
							<td><?=number_format($row->nominal)?></td>
							<td hidden>idkasir</td>
							<td hidden><?=$row->idmetode?></td>
							<td hidden><?=$row->idpegawai?></td>
							<td hidden><?=$row->idbank?></td>
							<td hidden><?=$row->ket_cc?></td>
							<td hidden><?=$row->idkontraktor?></td>
							<td hidden><?=$row->keterangan?></td>
							<td>
								<button type='button' class='btn btn-sm btn-info btnUbahPembayaran'>
									<i class='glyphicon glyphicon-pencil'></i>
								</button>
								&nbsp;&nbsp;
								<button type='button' class='btn btn-sm btn-danger btnHapusPembayaranHapus'>
									<i class='glyphicon glyphicon-remove'></i>
								</button>
							</td>
							<td hidden><?=$row->jaminan?></td>
							<td hidden><?=$row->tracenumber?></td>
							<?php if ($row->idmetode =='8') { ?>
								<td hidden><?=$row->tipekontraktor?></td>
							<?php } else { ?>
								<td hidden>0</td>
							<?php } ?>
						</tr>
					<?php }?>
				</tbody>
				<tfoot>
					<tr>
						<th colspan="3"><label class="pull-right"><b>Nominal Pembayaran</b></label></th>
						<th colspan="2">
							<input type="text" class="form-control input-sm" readonly id="nominal_pembayaran" name="nominal_pembayaran" value="{nominal_pembayaran}">
						</th>
					</tr>
					<tr>
						<th colspan="3"><label class="pull-right"><b>Sisa Pembayaran</b></label></th>
						<th colspan="2">
							<input type="text" class="form-control input-sm" readonly id="sisa_pembayaran" name="sisa_pembayaran" value="{sisa_pembayaran}">
						</th>
					</tr>
				</tfoot>
			</table>
		</div>

		<br><br>

		<button type="submit" class="btn btn-success" id="btnSubmit" style="float: right;">
			<i class="fa fa-save"></i> Simpan
		</button>

		<br><br><br>
		
		<input type="hidden" id="rowindex" value="">
		<input type="hidden" id="nomor" value="">
		<input type="hidden" id="detail_pembayaran" name="detail_pembayaran" value="">

		<?php echo form_hidden('idpengajuan', $idpengajuan) ?>
		<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script>
	const idtransaksi = '{idpengajuan}';
	
    $(document).ready(function() {
		$('.number').number(true, 0, '.', ',');
		$('.summernote').summernote('disable');
	
		loadDataKunjungan(idtransaksi);

		$(document).on("keyup", ".diskonRupiah", function() {
			const nominalKunjungan = parseFloat($(this).closest('tr').find('td:eq(7)').html().replace(/,/g, ''));
			const diskonRupihan = parseFloat($(this).val());
			const nominalAkhir = $(this).closest('tr').find('td:eq(9)');
			const actionUpdate = $(this).closest('tr').find('td:eq(10) span');
			const detailId = $(this).closest('tr').find('td:eq(11)').html();
			
			if (diskonRupihan > nominalKunjungan) {
				$(this).val(nominalKunjungan);
				nominalAkhir.html(0);
			} else {
				nominalAkhir.html($.number(nominalKunjungan - diskonRupihan));
			}

			actionUpdate.html(`<button type="button" data-toggle="tooltip" title="Simpan Perubahan" class="btn btn-primary btn-sm updateDataKunjungan" data-id="${detailId}">
				<i class="fa fa-floppy-o"></i>
			</button>`);
		});

		$(document).on("click", ".updateDataKunjungan", function() {
			const idkunjungan = $(this).data('id');
			const diskonRupiah = $(this).closest('tr').find('td:eq(8) input').val();
			const nominalAkhir = $(this).closest('tr').find('td:eq(9)').html();

			updateDataKunjungan(idkunjungan, diskonRupiah, nominalAkhir);
		});

		$(document).on("click", ".removeDataKunjungan", function() {
			const idkunjungan = $(this).data('id');

			swal({
				title: "Anda Yakin ?",
				text : "untuk menghapus data kunjungan ini ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				removeDataKunjungan(idkunjungan);
			});
		});
	});

	function loadDataKunjungan(idtransaksi) {
		$.ajax({
			url: '{site_url}trm_pengajuan_informasi/getDataKunjungan/' + idtransaksi,
			dataType: "json",
			success: function(data) {
				$('#detailPengajuan tbody').html('');

				renderingTableDataKunjungan(data);
			},
			error: function(data) {
				console.log(data);
			}
		});
	}

	function renderingTableDataKunjungan(data) {
		data.map((item) => {
			$('#detailPengajuan tbody').append(`
				<tr>
					<td>${item.no_medrec}</td>
					<td>${item.nama_pasien}</td>
					<td>${item.tanggal_kunjungan}</td>
					<td>${item.jenis_kunjungan}</td>
					<td>${item.nama_dokter}</td>
					<td>${item.jenis_pengajuan}</td>
					<td>${item.estimasi_selesai}</td>
					<td>${$.number(item.nominal)}</td>
					<td>
						<input type="text" class="form-control number input-sm diskonRupiah" value="${$.number(item.diskon_rp)}">
					</td>
					<td>${$.number(item.nominal_akhir)}</td>
					<td>
						<span></span>
						<button type="button" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm removeDataKunjungan" data-id="${item.id}">
							<i class="fa fa-trash"></i>
						</button>
					</td>
					<td hidden>${item.id}</td>
				</tr>
			`);

			$('.number').number(true, 0, '.', ',');
		});
	}

	function removeDataKunjungan(idkunjungan) {
		$.ajax({
			url: '{site_url}trm_pengajuan_informasi/removeDataKunjungan/' + idkunjungan,
			dataType: "json",
			success: function(data) {
				location.reload();
			},
			error: function(data) {
				console.log(data);
			}
		});
	}

	function updateDataKunjungan(idkunjungan, diskonRupiah, nominalAkhir) {
		$.ajax({
			url: '{site_url}trm_pengajuan_informasi/updateDataKunjungan/' + idkunjungan,
			method: "POST",
			dataType: "json",
			data: {
				diskonRupiah: diskonRupiah,
				nominalAkhir: nominalAkhir,
			},
			success: function(data) {
				location.reload();
			},
			error: function(data) {
				console.log(data);
			}
		});
	}

	$(document).on("click", "#btnSubmit", function (e) {
		var detailPembayaran = $('table#detailPembayaran tbody tr').get().map(function (row) {
			return $(row).find('td').get().map(function (cell) {
				return $(cell).html();
			})
		})

		$("#detail_pembayaran").val(JSON.stringify(detailPembayaran));
	});
</script>
<?php $this->load->view('Trm_pembayaran_informasi/modal_pembayaran'); ?>