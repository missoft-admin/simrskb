<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">

		<hr style="margin-top:0px">
		<div class="row">
			<?php echo form_open('trm_pembayaran_informasi/filter', 'class="form-horizontal" id="form-work"') ?>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">No. Pengajuan</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="no_pengajuan" placeholder="No. Pengajuan" value="{no_pengajuan}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Nama Pemohon</label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="nama_pemohon" placeholder="Nama Pemohon" value="{nama_pemohon}">
					</div>
				</div>
			</div>
			<div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Tanggal Pengajuan</label>
					<div class="col-md-8">
						<div class="input-group date">
							<input class="form-control js-datepicker" type="text" id="tanggal_dari" name="tanggal_dari" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="Tanggal Dari" value="{tanggal_dari}">
							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
							<input class="form-control js-datepicker" type="text" id="tanggal_sampai" name="tanggal_sampai" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="Tanggal Sampai" value="{tanggal_sampai}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Status</label>
					<div class="col-md-8">
						<select class="select2 form-control" name="status_pembayaran" style="width: 100%;" data-placeholder="">
							<option value="#">Semua Status</option>
							<option value="0" <?= ($status_pembayaran == '0' ? 'selected' : ''); ?>>Menunggu Diproses</option>
							<option value="1" <?= ($status_pembayaran == '1' ? 'selected' : ''); ?>>Telah Diproses</option>
							<option value="2" <?= ($status_pembayaran == '2' ? 'selected' : ''); ?>>Telah Diverifikasi</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
		<hr>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>Nomor Pengajuan</th>
					<th>Tanggal Pengajuan</th>
					<th>Nama Pemohon</th>
					<th>Keterangan</th>
					<th>Jumlah Pengajuan</th>
					<th>Nominal</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}trm_pembayaran_informasi/getIndex/' + '<?= $this->uri->segment(2); ?>',
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": false },
					{ "width": "5%", "targets": 1, "orderable": true },
					{ "width": "10%", "targets": 2, "orderable": false },
					{ "width": "10%", "targets": 3, "orderable": false },
					{ "width": "5%", "targets": 4, "orderable": false },
					{ "width": "10%", "targets": 5, "orderable": false },
					{ "width": "5%", "targets": 6, "orderable": false },
					{ "width": "10%", "targets": 7, "orderable": false },
				]
			});
	});

	$("#tanggal_dari").change(function() {
		const tanggal_dari = $("#tanggal_dari").val();
		const tanggal_sampai = $("#tanggal_sampai").val();

		if (!tanggal_sampai) {
			$("#tanggal_sampai").val(tanggal_dari);
		}
	});
</script>