<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mtujuan_farmasi" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('mtujuan_farmasi/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="row">
				
				<div class="form-group">
					<label class="col-md-2 control-label" for="nama_tujuan">Nama Tujuan</label>
					<div class="col-md-8">
						<input type="text" class="form-control" id="nama_tujuan" placeholder="Nama Tujuan" name="nama_tujuan" value="{nama_tujuan}">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-2 control-label" for="nama_tujuan">Unit Pelayanan </label>
					<div class="col-md-8">
						<select id="idunit" name="idunit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							
							<option value="#" <?=($idunit=='#'?'selected':'')?>>-Pilih Unit Pelayanan-</option>
							<?foreach(get_all('munitpelayanan',array('status'=>1)) as $r){?>
							<option value="<?=$r->id?>" <?=($idunit==$r->id?'selected':'')?>><?=$r->nama?></option>
							<?}?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="mtujuan_antrian_id">Tujuan Antrian </label>
					<div class="col-md-8">
						<select id="mtujuan_antrian_id" name="mtujuan_antrian_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							
							<option value="#" <?=($mtujuan_antrian_id=='#'?'selected':'')?>>-Pilih Tujuan Antrian-</option>
							<?foreach(get_all('mtujuan',array('jenis'=>1,'status'=>1)) as $r){?>
							<option value="<?=$r->id?>" <?=($mtujuan_antrian_id==$r->id?'selected':'')?>><?=$r->nama_tujuan?></option>
							<?}?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="apoteker_id">Apoteker Penanggung Jawab </label>
					<div class="col-md-8">
						<select id="apoteker_id" name="apoteker_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							
							<option value="#" <?=($apoteker_id=='#'?'selected':'')?>>-Pilih Apoteker-</option>
							<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
							<option value="<?=$r->id?>" <?=($apoteker_id==$r->id?'selected':'')?>><?=$r->nama?></option>
							<?}?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="nama_tujuan">User Akses</label>
					<div class="col-md-8">
						<select id="userid" name="userid[]" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." multiple>
							<?foreach($list_user as $r){?>
							<option value="<?=$r->id?>" <?=($r->pilih)?>><?=$r->nama_user?></option>
							<?}?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-2 col-md-offset-2">
						<button class="btn btn-success" type="submit">Simpan</button>
						<a href="{base_url}mtujuan_farmasi" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
					</div>
				</div>
				<?if ($id){?>
			<div class="row push-20-t">
				
			</div>
			<div class="row">
				<div class="form-group">
					<label class="col-md-10 col-md-offset-2 text-danger">SOUND</label>
					<input type="hidden" class="form-control" id="tujuan_id" placeholder="No Urut" name="tujuan_id" value="{id}">
					<input type="hidden" class="form-control" id="arr_sound" placeholder="No Urut" name="arr_sound" value="">
				</div>
				<div class="form-group">
					<div class="col-md-10 col-md-offset-2">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_sound">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Urutan</th>
										<th width="65%">Asset Sound</th>
										<th width="20%">Action</th>										   
									</tr>
									<input id="idsound" type="hidden" value="">
									
									<tr>
										<th>#</th>
										<th>
											<input tabindex="0" type="text" class="form-control number" id="nourut" placeholder="No Urut" name="nourut">
										</th>
										<th>
											<select id="sound_id"  name="sound_id" class="js-select2 form-control" style="width: 100%;">
												<option value="#" selected>-Pilih Asset Sound-</option>
												<?foreach($list_sound as $row){?>
												  <option value="<?=$row->id?>"><?=$row->nama_asset?></option>
												<?}?>
											</select>
										
										</th>
										
										<th>
											<div class="btn-group">
												<button class="btn btn-primary btn-sm" title="Simpan" type="button" id="btn_tambah_sound"><i class="fa fa-save"></i></button>
												<button class="btn btn-warning btn-sm"  title="Clear" type="button" id="btn_refresh_sound"><i class="fa fa-refresh"></i></button>
												<button class="btn btn-danger btn-sm"  title="Play All" type="button" id="btn_play_all"><i class="fa fa-play"></i></button>
											</div>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group">
					<label class="col-md-10 col-md-offset-2 text-danger">AKSES UNIT FARMASI</label>
				</div>
				<div class="form-group">
					<div class="col-md-10 col-md-offset-2">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_logic">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="15%">Jenis</th>
										<th width="20%">Asal Pasien</th>
										<th width="25%">Poliklinik / Kelas</th>
										<th width="25%">Dokter</th>
										<th width="10%">Action</th>										   
									</tr>
									<?
										$asal_pasien='0';
										$iddokter_ppa='0';
										$jenis_order='1';
									?>
									<tr>
										<input id="idlogic" type="hidden" value="">
										<th>#</th>
										<th>
											<select id="jenis_order" name="jenis_order" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="#" selected>-Pilih Jenis-</option>
												<?foreach(list_variable_ref(100) as $row){?>
													<option value="<?=$row->id?>"><?=$row->nama?></option>
												<?}?>
											</select>
										</th>
										<th>
											<select id="asal_pasien" name="asal_pasien" class="js-select2 form-control" style="width: 100%;"data-placeholder="Choose one..">
													
												<option value="0" <?=($asal_pasien=='0'?'selected':'')?>>-SEMUA-</option>
												<option value="1" <?=($asal_pasien=='1'?'selected':'')?>>POLIKINIK</option>
												<option value="2" <?=($asal_pasien=='2'?'selected':'')?>>IGD</option>
												<option value="3" <?=($asal_pasien=='3'?'selected':'')?>>RANAP</option>
												<option value="4" <?=($asal_pasien=='4'?'selected':'')?>>ODS</option>
												
											</select>
										</th>
										<th>
											<select id="idpoli_kelas" name="idpoli_kelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											</select>
										</th>
										<th>
											<select id="iddokter_ppa" name="iddokter_ppa" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
												<option value="0" <?=($iddokter_ppa=='0'?'selected':'')?>>-All Dokter-</option>
												<?foreach(get_all('mppa',array('staktif'=>1,'tipepegawai'=>'2')) as $r){?>
												<option value="<?=$r->id?>" <?=($iddokter_ppa==$r->id?'selected':'')?>><?=$r->nama?></option>
												<?}?>
											</select>
										
										</th>
										
										<th>
											<div class="btn-group">
												<button class="btn btn-primary btn-sm" title="Simpan" type="button" id="btn_tambah_logic"><i class="fa fa-save"></i></button>
												<button class="btn btn-warning btn-sm"  title="Clear" type="button" id="btn_refresh_logic"><i class="fa fa-refresh"></i></button>
											</div>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<?}?>
				
			</div>

			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>

</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var x = document.getElementById("myAudio");

function play_sound() {
  x.play();
}

function pauseAudio() {
  x.pause();
}
	$(document).ready(function(){
		$("#asal_pasien").val(0).trigger('change');
		if ($("#tujuan_id").val()!=''){
			load_sound();
			load_logic();
		}
	})	
	$(".chk_status").on("click", function(){
		let id_det;
		 check = $(this).is(":checked");
			if(check) {
				$(this).closest('tr').find(".isiField").removeAttr("disabled");
			} else {
				$(this).closest('tr').find(".isiField").attr('disabled', 'disabled');
			}
	}); 
	
	function validate_final(){
		
		
		if ($("#nama_tujuan").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Tujuan", "error");
			return false;
		}
		
		$("*[disabled]").not(true).removeAttr("disabled");
		$("#cover-spin").show();
		return true;

		
	}
	$("#asal_pasien").change(function() {
		set_poli();
	});
	function set_poli(){
			$.ajax({
				url: '{site_url}setting_cover_barang/get_poli/',
				dataType: "json",
				 type: "POST" ,
				dataType: 'json',
				data : {
						idtipe : $("#asal_pasien").val(),
					   },
				success: function(data) {
					// alert(data);
					$("#idpoli_kelas").empty();
					$("#idpoli_kelas").append(data);
				}
			});
	}
	$("#btn_tambah_sound").click(function() {
		let tujuan_id=$("#tujuan_id").val();
		let idsound=$("#idsound").val();
		let nourut=$("#nourut").val();
		let sound_id=$("#sound_id").val();
		if ($("#nourut").val()==''){
			sweetAlert("Maaf...", "Tentukan No Urut", "error");
			return false;
		}
		if ($("#sound_id").val()==''){
			sweetAlert("Maaf...", "Tentukan Isi", "error");
			return false;
		}
		
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}mtujuan_farmasi/simpan_sound', 
			dataType: "JSON",
			method: "POST",
			data : {
				tujuan_id:tujuan_id,
				idsound:idsound,
				nourut:nourut,
				sound_id:sound_id,
				},
			complete: function(data) {
				// $("#idtipe").val('#').trigger('change');
				$('#index_sound').DataTable().ajax.reload( null, false ); 
				$("#cover-spin").hide();
				clear_sound();
				
				load_data_sound();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}
		});

	});
	$("#btn_tambah_logic").click(function() {
		let tujuan_id=$("#tujuan_id").val();
		let jenis_order=$("#jenis_order").val();
		let asal_pasien=$("#asal_pasien").val();
		let idpoli_kelas=$("#idpoli_kelas").val();
		let iddokter_ppa=$("#iddokter_ppa").val();
		if ($("#jenis_order").val()=='#'){
			sweetAlert("Maaf...", "Tentukan Jenis", "error");
			return false;
		}
		
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}mtujuan_farmasi/simpan_logic', 
			dataType: "JSON",
			method: "POST",
			data : {
				tujuan_id:tujuan_id,
				jenis_order:jenis_order,
				asal_pasien:asal_pasien,
				idpoli_kelas:idpoli_kelas,
				iddokter_ppa:iddokter_ppa,
				},
			complete: function(data) {
				// $("#idtipe").val('#').trigger('change');
				$('#index_logic').DataTable().ajax.reload( null, false ); 
				$("#cover-spin").hide();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan Logic'});
			}
		});

	});
	$("#btn_refresh_sound_text").click(function() {
	clear_sound();
	});
	$("#btn_play_all").click(function() {
		play_sound_arr();
	});
	function clear_sound(){
		$("#idsound").val('');
		$("#nourut").val('');
		$("#sound_id").val('#').trigger('change');
	}
	function load_sound(){
		var tujuan_id=$("#tujuan_id").val();
		$('#index_sound').DataTable().destroy();	
		table = $('#index_sound').DataTable({
			autoWidth: false,
			searching: false,
			serverSide: true,
			"processing": true,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}mtujuan_farmasi/load_sound', 
				type: "POST" ,
				dataType: 'json',
				data : {
						tujuan_id:tujuan_id
					   }
			}
		});
		load_data_sound();
	}
	function load_logic(){
		var tujuan_id=$("#tujuan_id").val();
		$('#index_logic').DataTable().destroy();	
		table = $('#index_logic').DataTable({
			autoWidth: false,
			searching: false,
			serverSide: true,
			"processing": true,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}mtujuan_farmasi/load_logic', 
				type: "POST" ,
				dataType: 'json',
				data : {
						tujuan_id:tujuan_id
					   }
			}
		});
		load_data_sound();
	}
	function hapus_sound($id){
	 var id=$id;
	swal({
			title: "Anda Yakin ?",
			text : "Untuk Hapus Sound?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}mtujuan_farmasi/hapus_sound',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
					$('#index_sound').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					load_data_sound();
				}
			});
		});
	}
	function hapus_logic($id){
	 var id=$id;
	swal({
			title: "Anda Yakin ?",
			text : "Untuk Hapus Logic?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}mtujuan_farmasi/hapus_logic',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
					$('#index_logic').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
				}
			});
		});
	}
	function edit_sound($id){
		let id=$id;
		$.ajax({
			url: '{site_url}mtujuan_farmasi/edit_sound', 
			dataType: "JSON",
			method: "POST",
			data : {id:id},
			success: function(data) {
				$("#nourut").val(data.nourut);
				$("#sound_id").val(data.sound_id).trigger('change');
				$("#idsound").val(data.id);
			}
		});

	}
	function load_data_sound(){
		let tujuan_id=$("#tujuan_id").val();
		$.ajax({
			url: '{site_url}mtujuan_farmasi/load_data_sound', 
			dataType: "JSON",
			method: "POST",
			data : {tujuan_id:tujuan_id},
			success: function(data) {
				$("#arr_sound").val(data.file);
				
			}
		});

	}

	function play_sound_arr(){
		let str_file=$("#arr_sound").val();
		var myArray = str_file.split(":");
		console.log(myArray[0]);
		queue_sounds(myArray);
	}
	function queue_sounds(sounds){
		// $config['upload_path'] = './assets/upload/sound_antrian/';
		let path_sound='{site_url}assets/upload/sound_antrian/';
		
		var index = 0;
		function recursive_play()
		{
			let file_sound = new Audio(path_sound+sounds[index]);
		  if(index+1 === sounds.length)
		  {
			play(file_sound);
		  }
		  else
		  {
			play(file_sound,function(){
				index++; recursive_play();
				});
		  }
		}

	recursive_play();   
	}
	 function play(audio, callback) {

		audio.play();
		if(callback)
		{
			audio.onended = callback;
		}
	}
</script>