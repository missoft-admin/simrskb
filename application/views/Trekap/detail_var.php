<?= ErrorSuccess($this->session) ?>
<?php if ($error != '') echo ErrorMessage($error) ?>

<style media="screen">
	.highlight-refund {
		background-color: #8bc34a2b !important;
	}
</style>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}trekap/detail/<?=$idrekap?>" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-bottom: 0px;">
	</div>
	<div class="block-content">
		<?php echo form_open('trekap/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<input class="form-control" type="hidden" name="id" id="id" type="text"  value="<?=($id)?>">
			<input class="form-control" type="hidden" name="idmrekap" id="idmrekap" type="text"  value="<?=($idmrekap)?>">
			<input class="form-control" type="hidden" name="trekap_id" id="trekap_id" type="text"  value="<?=($idrekap)?>">
			<input class="form-control" type="hidden" name="st_pendapatan" id="st_pendapatan" type="text"  value="<?=($st_pendapatan)?>">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="col-md-3 control-label" for="nominal">Tipe Pendapatan</label>
						<div class="col-md-4">
							<?if ($st_pendapatan=='1'){?>
							<h3><span class="label label-success">Pendapatan</span></h3>
							<?}else{?>
							<h3><span class="label label-warning">Bukan Pendapatan</span></h3>
							
							<?}?>
							
						</div>
						
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="idpegawai">Nama</label>
						<div class="col-md-7">
							<input class="form-control" disabled name="nama" id="nama" type="text" readonly value="<?=($nama)?>">
							
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label" for="nominal">Nominal</label>
						<div class="col-md-4">
							<input type="text" disabled class="form-control number" id="nominal" placeholder="Nominal" name="nominal" value="{nominal}" required="" aria-required="true">
						</div>
						
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="nominal"><h3><span class="label label-primary">VARIABLE</span></h3></label>
						<div class="col-md-7">
							<div class="form-group">
								 <table class="table table-bordered" id="list_index">
									<thead>
										<tr>
											<th width="5%" class="text-center" hidden>id</th>
											<th width="10%" class="text-center">Jenis Gaji</th>
											<th width="15%" class="text-center">Variable</th>									
											<th width="10%" class="text-center">Nominal</th>
											<th width="15%" class="text-center">Actions</th>
											
											
										</tr>
									</thead>
									</thead>
									<tbody>	
										
									</tbody>
									
								</table>
							</div>
						</div>
						
					</div>
					
				</div>
			</div>
			<div class="row">
				
				<div class="block-content block-content">	
					<input type="hidden" class="form-control" id="rowindex">
					<input type="hidden" class="form-control" id="iddet">
					
					
				</div> 
			</div>
			
		</form>
	</div>
</div>

<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/number_format.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
	//PEMBAYARAN
	$(document).ready(function(){
		$('.number').number(true, 0, '.', ',');
		
		load_index();
	})
	
	function load_index(){
		var id=$("#id").val();
		
		$('#list_index').DataTable().destroy();
		var table = $('#list_index').DataTable({
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"autoWidth": false,
			"fixedHeader": true,
			"searching": true,
			"order": [],
			"ajax": {
				url: '{site_url}trekap/get_master_variable/',
					type: "POST",
					data: {
						id: id,
						},
					dataType: 'json'
				},
				"columnDefs": [
					{"targets": [0,4], "visible": false },
					{ "width": "10%", "targets": 1, "orderable": true,"class":"text-left" },
					{ "width": "10%", "targets": 2, "orderable": true,"class":"text-left" },
					{ "width": "10%", "targets": 3, "orderable": true ,"class":"text-right"},
					{ "width": "10%", "targets": 4, "orderable": true ,"class":"text-center"},
				]
			});
		
	}
	
</script>
