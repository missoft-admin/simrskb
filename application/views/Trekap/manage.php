<?= ErrorSuccess($this->session) ?>
<?php if ($error != '') echo ErrorMessage($error) ?>

<style media="screen">
	.highlight-refund {
		background-color: #8bc34a2b !important;
	}
</style>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}trekap/detail/<?=$idrekap?>" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-bottom: 0px;">
	</div>
	<div class="block-content">
		<?php echo form_open('trekap/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<input class="form-control" type="hidden" name="disabel" id="disabel" type="text"  value="<?=($disabel)?>">
			<input class="form-control" type="hidden" name="id" id="id" type="text"  value="<?=($id)?>">
			<input class="form-control" type="hidden" name="idmrekap" id="idmrekap" type="text"  value="<?=($idmrekap)?>">
			<input class="form-control" type="hidden" name="trekap_id" id="trekap_id" type="text"  value="<?=($idrekap)?>">
			<input class="form-control" type="hidden" name="st_pendapatan" id="st_pendapatan" type="text"  value="<?=($st_pendapatan)?>">
			<input class="form-control" type="hidden" readonly name="st_pendapatan" id="st_idtipe" type="text"  value="<?=($st_idtipe)?>">
			<input class="form-control" type="hidden" readonly name="tipe_bayar" id="tipe_bayar" type="text"  value="<?=($tipe_bayar)?>">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="col-md-3 control-label" for="nominal">Tipe Pendapatan</label>
						<div class="col-md-4">
							<?if ($st_pendapatan=='1'){?>
							<h3><span class="label label-success">Pendapatan</span></h3>
							<?}else{?>
							<h3><span class="label label-warning">Bukan Pendapatan</span></h3>
							
							<?}?>
							
						</div>
						
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="idpegawai">Nama</label>
						<div class="col-md-7">
							<input class="form-control" disabled name="nama" id="nama" type="text" readonly value="<?=($nama)?>">
							
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label" for="nominal">Nominal</label>
						<div class="col-md-4">
							<input type="text" disabled class="form-control number" id="nominal" placeholder="Nominal" name="nominal" value="{nominal}" required="" aria-required="true">
						</div>
						<?if ($tipe_bayar=='1'){?>
						<div class="col-md-3">
							<button  type="button" <?=$disabel?> class="btn btn-danger btn_hide" type="button" id="btn_add_pembayaran" title="Add"><i class="fa fa-plus"></i> Pembayaran</button>
						</div>
						<?}?>
					</div>
					
					
					<div class="form-group" style="margin-bottom: 5px;">
						
					</div>
				</div>
			</div>
			<?if ($tipe_bayar=='1'){?>
			<div class="row">
				<h3><span class="label label-primary">PEMBAYARAN</span></h3>
				<div class="block-content block-content">	
					<input type="hidden" class="form-control" id="rowindex">
					<input type="hidden" class="form-control" id="iddet">
					<div class="form-group">
						 <table class="table table-bordered" id="tabel_pembayaran">
							<thead>
								<tr>
									<th width="5%" class="text-center" hidden>id</th>
									<th width="5%" class="text-center" hidden>jenis_id</th>
									<th width="5%" class="text-center" hidden>sumber_kas_id</th>
									<th width="5%" class="text-center" hidden>idmetode</th>
									<th width="10%" class="text-center">Jenis Kas</th>
									<th width="15%" class="text-center">Sumber Kas</th>
									<th width="10%" class="text-center">Metode</th>
									<th width="10%" class="text-center">Tgl Pencairan</th>
									<th width="15%" class="text-center">Keterangan</th>
									<th width="15%" class="text-center">Rekening</th>
									<th width="10%" class="text-center">Nominal</th>
									<th width="15%" class="text-center">Actions</th>
									<th width="5%" class="text-center" hidden>idmetode</th>
									<th width="5%" class="text-center" hidden>idmetode</th>
									<th width="5%" class="text-center" hidden>idmetode</th>
									<th width="5%" class="text-center" hidden>idmetode</th>
									
								</tr>
							</thead>
							</thead>
							<tbody>	
								
							</tbody>
							<tfoot>
								<tr>
									<td colspan="10" class="text-right"> <strong>Total Bayar </strong></td>
									<td> <input class="form-control input number" readonly type="text" id="total_bayar" name="total_bayar" value="0" style="width:100%"/></td>
									<td></td>
								</tr>
								<tr>
									<td colspan="10" class="text-right"> <strong>Sisa</strong></td>
									<td><input class="form-control input number" readonly type="text" id="total_sisa" name="total_sisa" value="0"  style="width:100%" /></td>
									<td></td>
								</tr>
							</tfoot>
						</table>
					</div>
					
				</div> 
			</div>
			<?}?>
			<?if ($tipe_bayar=='3'){?>
			<div class="row">
				<h3><span class="label label-success">PROSES PENDAPATAN</span></h3><br>
				<button  type="button" <?=$disabel?> class="btn btn-danger btn_hide" type="button" id="btn_add_pendapatan" title="Add"><i class="fa fa-plus"></i> Tentukan Cara Bayar</button>
				<div class="block-content block-content">	
					<input type="hidden" class="form-control" id="rowindex">
					<input type="hidden" class="form-control" id="iddet2">
					<div class="form-group">
						 <table class="table table-bordered" id="tabel_pendapatan">
							<thead>
								<tr>
									<th width="5%" class="text-center" hidden>id</th>
									<th width="5%" class="text-center" hidden>jenis_id</th>
									<th width="5%" class="text-center" hidden>sumber_kas_id</th>
									<th width="5%" class="text-center" hidden>idmetode</th>
									<th width="10%" class="text-center">Jenis Kas</th>
									<th width="15%" class="text-center">Sumber Kas</th>
									<th width="10%" class="text-center">Metode</th>
									<th width="10%" class="text-center">Tgl Pencairan</th>
									<th width="15%" class="text-center">Keterangan</th>
									<th width="10%" class="text-center">Nominal</th>
									<th width="15%" class="text-center">Actions</th>
									
									
								</tr>
							</thead>
							</thead>
							<tbody>	
								
							</tbody>
							<tfoot>
								<tr>
									<td colspan="9" class="text-right"> <strong>Total Bayar </strong></td>
									<td> <input class="form-control input number" readonly type="text" id="total_pendapatan" name="total_pendapatan" value="0" style="width:100%"/></td>
									<td></td>
								</tr>
								<tr>
									<td colspan="9" class="text-right"> <strong>Sisa</strong></td>
									<td><input class="form-control input number" readonly type="text" id="total_sisa_pendapatan" name="total_sisa_pendapatan" value="0"  style="width:100%" /></td>
									<td></td>
								</tr>
							</tfoot>
						</table>
					</div>
					
				</div> 
			</div>
			<?}?>
			<div class="row">
				<div class="block-content block-content">	
					
					<? if ($disabel==''){ ?>
					<div class="form-group hiden btn_hide">
						<div class="text-right bg-light lter">
							<button class="btn btn-success" type="submit"  name="btn_simpan" value="2">Simpan & Verifikasi</button>
							<button class="btn btn-primary" type="submit"  name="btn_simpan" value="1">Simpan</button>
							<a href="{base_url}trekap/detail/<?=$idrekap?>" class="btn btn-default" type="button">Batal</a>
						</div>
					</div>
					<?}?>
					
				</div> 
			</div>
			<hr>
			
		</form>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_pembayaran" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Pembayaran</h3>
				</div>
				
				<div class="block-content">
					<form class="form-horizontal">
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Sisa Rp. </label>
							<div class="col-md-7">
								<input readonly type="text" class="form-control number" id="sisa_modal" placeholder="Sisa" name="sisa_modal" required="" aria-required="true">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="jenis_kas_id">Jenis Kas</label>
							<div class="col-md-7">
								<select name="jenis_kas_id" id="jenis_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="#">- Pilih Opsi -</option>
									<? foreach($list_jenis_kas as $row){ ?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>
									<?}?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="sumber_kas_id">Sumber Kas</label>
							<div class="col-md-7">
								<select name="sumber_kas_id" id="sumber_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">							
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="idmetode">Metode</label>
							<div class="col-md-7">
								<select name="idmetode" id="idmetode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="#">Pilih Opsi</option>							
									<option value="1">Cheq</option>	
									<option value="2">Tunai</option>	
									<option value="4">Transfer Antar Bank</option>
									<option value="3">Transfer Beda Bank</option>							
								</select>
							</div>
						</div>
						
						
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Nominal Rp. </label>
							<div class="col-md-7">
								<input  type="hidden" class="form-control" id="sumber_kas_id_tmp" placeholder="sumber_kas_id_tmp" name="sumber_kas_id_tmp" >
								<input  type="hidden" class="form-control" id="idpembayaran" placeholder="idpembayaran" name="idpembayaran" >
								<input  type="text" class="form-control number" id="nominal_bayar" placeholder="Nominal" name="nominal_bayar" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Tanggal Pencairan</label>
							<div class="col-md-7">
								<div class="input-group date">
								<input class="js-datepicker form-control input"  type="text" id="tanggal_pencairan" name="tanggal_pencairan" value="" data-date-format="dd-mm-yyyy"/>
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Keterangan </label>
							<div class="col-md-7">
								<textarea class="form-control" name="ket_pembayaran" id="ket_pembayaran"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Rekening</label>
							<div class="col-md-7">
								<div class="input-group">
									<input class="form-control" type="text" readonly id="nama_rekening" name="nama_rekening" placeholder="Rekening">
									<span class="input-group-btn">
										<button class="btn btn-info" type="button" id="btn_cari_rekening"><i class="fa fa-search"></i> </button>
									</span>
								</div>
								<input class="form-control" type="hidden" readonly id="norek" name="norek" placeholder="norek">
								<input class="form-control" type="hidden" readonly id="atas_nama" name="atas_nama" placeholder="atas_nama">
								<input class="form-control" type="hidden" readonly id="idbank" name="idbank" placeholder="idbank">
								<input class="form-control" type="hidden" readonly id="nama_bank" name="nama_bank" placeholder="nama_bank">
							</div>
						</div>
						<div class="modal-footer">
							<button class="btn btn-sm btn-primary" id="btn_add_bayar" type="button"><i class="fa fa-check"></i> Add</button>
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Exit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_pendapatan" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Pendapatan</h3>
				</div>
				
				<div class="block-content">
					<form class="form-horizontal">
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Sisa Rp. </label>
							<div class="col-md-7">
								<input readonly type="text" class="form-control number" id="sisa_modal2" placeholder="Sisa" name="sisa_modal2" required="" aria-required="true">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="jenis_kas_id">Jenis Kas</label>
							<div class="col-md-7">
								<select name="jenis_kas_id2" id="jenis_kas_id2" class="js-select2 form-control" style="width: 100%;">
									<option value="#">- Pilih Opsi -</option>
									<? foreach($list_jenis_kas as $row){ ?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>
									<?}?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="sumber_kas_id2">Sumber Kas</label>
							<div class="col-md-7">
								<select name="sumber_kas_id2" id="sumber_kas_id2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">							
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="idmetode2">Metode</label>
							<div class="col-md-7">
								<select name="idmetode2" id="idmetode2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="#">Pilih Opsi</option>							
									<option value="1">Cheq</option>	
									<option value="2">Tunai</option>	
									<option value="4">Transfer Antar Bank</option>
									<option value="3">Transfer Beda Bank</option>							
								</select>
							</div>
						</div>
						
						
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Nominal Rp. </label>
							<div class="col-md-7">
								<input  type="hidden" class="form-control" id="sumber_kas_id_tmp2" placeholder="sumber_kas_id_tmp2" name="sumber_kas_id_tmp2" >
								<input  type="text" class="form-control number" id="nominal_bayar2" placeholder="Nominal" name="nominal_bayar2" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Tanggal Pencairan</label>
							<div class="col-md-7">
								<div class="input-group date">
								<input class="js-datepicker form-control input"  type="text" id="tanggal_pencairan2" name="tanggal_pencairan2" value="" data-date-format="dd-mm-yyyy"/>
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Keterangan </label>
							<div class="col-md-7">
								<textarea class="form-control" name="ket_pembayaran2" id="ket_pembayaran2"></textarea>
							</div>
						</div>
						
						<div class="modal-footer">
							<button class="btn btn-sm btn-primary" id="btn_add_bayar2" type="button"><i class="fa fa-check"></i> Add</button>
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Exit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_rekening" role="dialog" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">List Rekening</h3>
				</div>
				<div class="block-content">
					
					<table class="table table-bordered table-striped" id="tabel_rekening" style="width: 100%;">
						<thead>
							<tr>
								<th style="display:none">X</th>
								<th>Bank</th>
								<th>No Rekening</th>
								<th>Atas Nama</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?foreach($list_rekening as $row){?>
								<td style="display:none"><?=$row->idbank?></td>
								<td><?=$row->bank?></td>
								<td><?=$row->norek?></td>
								<td><?=$row->atas_nama?></td>
								<td><button title="Pilih" class="btn btn-danger btn-xs pilih_rek"><i class="fa fa-check"></i> Pilih</button></td>
							<?}?>
						</tbody>
					</table>
				</div>
			</div>
			
		</div>
	</div>
</div>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/number_format.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
	//PEMBAYARAN
	$(document).ready(function(){
		$('.number').number(true, 0, '.', ',');
		$('#ket_pembayaran').summernote({
		  height: 80,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }
		})
		load_pendapatan();
		load_pembayaran();
		total_bayar_sisa();
	})
	$(document).on("click","#btn_cari_rekening",function(){		
		$("#modal_rekening").modal('show');		
	});
	$(document).on("click",".pilih_rek",function(){		
		var idbank =$(this).closest('tr').find("td:eq(0)").html();
		var nama_bank =$(this).closest('tr').find("td:eq(1)").html();
		var norek =$(this).closest('tr').find("td:eq(2)").html();
		var atas_nama =$(this).closest('tr').find("td:eq(3)").html();
		$("#idbank").val(idbank);
		$("#nama_bank").val(nama_bank);
		$("#norek").val(norek);
		$("#atas_nama").val(atas_nama);
		$("#atas_nama").val(atas_nama);
		$("#nama_rekening").val(nama_bank + ' | '+ norek+ ' | '+atas_nama);
		$("#modal_rekening").modal('hide');		
	});
	function save_pembayaran(){
		var iddet=$("#iddet").val();
		var trekap_master_id=$("#id").val();
		var jenis_kas_id=$("#jenis_kas_id").val();
		var sumber_kas_id=$("#sumber_kas_id").val();
		var idmetode=$("#idmetode").val();
		var nominal_bayar=$("#nominal_bayar").val();
		var ket_pembayaran=$("#ket_pembayaran").summernote('code');
		if ($("#idmetode").val()=='3' || $("#idmetode").val()=='4'){
			var idbank=$("#idbank").val();
			var nama_bank=$("#nama_bank").val();
			var norek=$("#norek").val();
			var atas_nama=$("#atas_nama").val();
			var nama_rekening=$("#nama_rekening").val();
		}else{
			var idbank='';
			var nama_bank='';
			var norek='';
			var atas_nama='';
			var nama_rekening='';
		}
		
		var tanggal_pencairan=$("#tanggal_pencairan").val();
		var iddet=$("#iddet").val();
		
		$.ajax({
			url: '{site_url}trekap/save_pembayaran',
			type: 'POST',
			dataType: "json",
			data: {
				trekap_master_id: trekap_master_id,
				jenis_kas_id: jenis_kas_id,
				sumber_kas_id: sumber_kas_id,
				idmetode: idmetode,
				nominal_bayar: nominal_bayar,
				ket_pembayaran: ket_pembayaran,
				idbank: idbank,
				nama_bank: nama_bank,
				norek: norek,
				atas_nama: atas_nama,
				nama_rekening: nama_rekening,
				tanggal_pencairan: tanggal_pencairan,
				iddet: iddet,
				},
			success : function(data) {						
				$("#modal_pembayaran").modal('hide');	
				swal({
					title: "Berhasil!",
					text: "Pembayaran Berhasil disimpan.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});
				$('#tabel_pembayaran').DataTable().ajax.reload( null, false );
				pembayaran_clear();
			}
		});
	}
	function save_pendapatan(){
		var trekap_master_id=$("#id").val();
		var jenis_kas_id=$("#jenis_kas_id2").val();
		var sumber_kas_id=$("#sumber_kas_id2").val();
		var idmetode=$("#idmetode2").val();
		var nominal_bayar=$("#nominal_bayar2").val();
		var ket_pembayaran=$("#ket_pembayaran2").summernote('code');
		
		var tanggal_pencairan=$("#tanggal_pencairan2").val();
		var iddet=$("#iddet2").val();
		
		$.ajax({
			url: '{site_url}trekap/save_pendapatan',
			type: 'POST',
			dataType: "json",
			data: {
				trekap_master_id: trekap_master_id,
				jenis_kas_id: jenis_kas_id,
				sumber_kas_id: sumber_kas_id,
				idmetode: idmetode,
				nominal_bayar: nominal_bayar,
				ket_pembayaran: ket_pembayaran,
				tanggal_pencairan: tanggal_pencairan,
				iddet: iddet,
				},
			success : function(data) {						
				$("#modal_pendapatan").modal('hide');	
				swal({
					title: "Berhasil!",
					text: "Pembayaran Berhasil disimpan.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});
				$('#tabel_pendapatan').DataTable().ajax.reload( null, false );
				pembayaran_clear();
			}
		});
	}
	function load_pembayaran(){
		var id=$("#id").val();
		var disabel=$("#disabel").val();
		
		$('#tabel_pembayaran').DataTable().destroy();
		var table = $('#tabel_pembayaran').DataTable({
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"autoWidth": false,
			"fixedHeader": true,
			"searching": true,
			"order": [],
			"ajax": {
				url: '{site_url}trekap/getPembayaran/',
					type: "POST",
					data: {
						id: id,
						disabel: disabel,
						},
					dataType: 'json'
				},
				"columnDefs": [
					{"targets": [0,1,2,3,12,13,14,15], "visible": false },
					{ "width": "10%", "targets": 4, "orderable": true,"class":"text-center" },
					{ "width": "10%", "targets": 5, "orderable": true,"class":"text-center" },
					{ "width": "10%", "targets": 6, "orderable": true ,"class":"text-center"},
					{ "width": "10%", "targets": 7, "orderable": true ,"class":"text-center"},
					{ "width": "15%", "targets": 8, "orderable": true ,"class":"text-center"},
					{ "width": "15%", "targets": 9, "orderable": true ,"class":"text-center"},
					{ "width": "10%", "targets": 10, "orderable": true ,"class":"text-right"},
					{ "width": "10%", "targets": 11, "orderable": true ,"class":"text-left"},
				]
			});
		
	}
	function load_pendapatan(){
		var id=$("#id").val();
		
		$('#tabel_pendapatan').DataTable().destroy();
		var table = $('#tabel_pendapatan').DataTable({
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"autoWidth": false,
			"fixedHeader": true,
			"searching": true,
			"order": [],
			"ajax": {
				url: '{site_url}trekap/getPendapatan/',
					type: "POST",
					data: {
						id: id,
						},
					dataType: 'json'
				},
				"columnDefs": [
					{"targets": [0,1,2,3], "visible": false },
					{ "width": "10%", "targets": 4, "orderable": true,"class":"text-center" },
					{ "width": "10%", "targets": 5, "orderable": true,"class":"text-center" },
					{ "width": "10%", "targets": 6, "orderable": true ,"class":"text-center"},
					{ "width": "10%", "targets": 7, "orderable": true ,"class":"text-center"},
					{ "width": "15%", "targets": 8, "orderable": true ,"class":"text-center"},
					{ "width": "15%", "targets": 9, "orderable": true ,"class":"text-right"},
					{ "width": "15%", "targets": 10, "orderable": true ,"class":"text-left"},
				]
			});
		
	}
	
	$(document).on("click","#btn_add_pembayaran",function(){
		
		$("#modal_pembayaran").modal('show');
		$("#total_sisa").val(parseFloat($("#nominal").val()) - parseFloat($("#total_bayar").val()));
		$("#sisa_modal").val($("#total_sisa").val());
		$("#nominal_bayar").val($("#total_sisa").val());
		
	});
	$(document).on("click","#btn_add_pendapatan",function(){
		
		$("#modal_pendapatan").modal('show');
		$("#total_sisa_pendapatan").val(parseFloat($("#nominal").val()) - parseFloat($("#total_pendapatan").val()));
		$("#sisa_modal2").val($("#total_sisa_pendapatan").val());
		$("#nominal_bayar2").val($("#total_sisa_pendapatan").val());
		
	});
	$(document).on("change","#jenis_kas_id",function(){
		// alert('soo');
		var jenis_kas_id=$("#jenis_kas_id").val();
		var sumber_kas_id=$("#sumber_kas_id_tmp").val();
		$.ajax({
			url: '{site_url}mrka_bendahara/list_sumber_kas',
			type: 'POST',
			dataType: "json",
			data: {jenis_kas_id: jenis_kas_id,sumber_kas_id: sumber_kas_id},
			success : function(data) {				
				$("#sumber_kas_id").empty();	
				console.log(data.detail);
				// $("#sumber_kas_id").append(data.detail);	
				$('#sumber_kas_id').append(data.detail);
			}
		});
	});
	$(document).on("change","#jenis_kas_id2",function(){
		// alert('soo');
		var jenis_kas_id=$("#jenis_kas_id2").val();
		var sumber_kas_id=$("#sumber_kas_id_tmp2").val();
		$.ajax({
			url: '{site_url}mrka_bendahara/list_sumber_kas',
			type: 'POST',
			dataType: "json",
			data: {jenis_kas_id: jenis_kas_id,sumber_kas_id: sumber_kas_id},
			success : function(data) {				
				$("#sumber_kas_id2").empty();	
				console.log(data.detail);
				$('#sumber_kas_id2').append(data.detail);
			}
		});
	});
	$(document).on("change","#idmetode",function(){
		if ($("#idmetode").val()=='3' || $("#idmetode").val()=='4'){
			$("#btn_cari_rekening").attr('disabled',false);
		}else{
			$("#btn_cari_rekening").attr('disabled',true);
		}
	});
	$(document).on("click","#btn_add_bayar",function(){
		add_pembayaran();
	});
	function add_pembayaran(){
			
		if ($("#jenis_kas_id").val() == "#") {
			sweetAlert("Maaf...", "Jenis Kas Harus Diisi!", "error");
			$("#jenis_kas_id").focus();
			return false;
		}
		if ($("#sumber_kas_id").val() == "#") {
			sweetAlert("Maaf...", "Sumber Kas Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		if ($("#idmetode").val() == "#" || $("#idmetode").val() == "") {
			sweetAlert("Maaf...", "Metode  Harus Diisi!", "error");
			$("#idmetode").focus();
			return false;
		}
		if ($("#nominal_bayar").val() == "" || $("#nominal_bayar").val() == "0") {
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		if ($("#tanggal_pencairan").val() == "") {
			sweetAlert("Maaf...", "Tanggal Pencairan Harus Diisi!", "error");
			$("#tanggal_pencairan").focus();
			return false;
		}
		if ($("#idmetode").val() == "3" || $("#idmetode").val() == "4") {
			if ($("#nama_rekening").val()==''){
				sweetAlert("Maaf...", "Rekening Harus Diisi!", "error");
				$("#nama_rekening").focus();
				return false;
			}
			
		}
		save_pembayaran();
		
	}
	$(document).on("click","#btn_add_bayar2",function(){
		add_pendapatan();
	});
	function add_pendapatan(){
			
		if ($("#jenis_kas_id2").val() == "#") {
			sweetAlert("Maaf...", "Jenis Kas Harus Diisi!", "error");
			$("#jenis_kas_id2").focus();
			return false;
		}
		if ($("#sumber_kas_id2").val() == "#") {
			sweetAlert("Maaf...", "Sumber Kas Harus Diisi!", "error");
			$("#sumber_kas_id2").focus();
			return false;
		}
		if ($("#idmetode2").val() == "#" || $("#idmetode2").val() == "") {
			sweetAlert("Maaf...", "Metode  Harus Diisi!", "error");
			$("#idmetode2").focus();
			return false;
		}
		if ($("#nominal_bayar2").val() == "" || $("#nominal_bayar2").val() == "0") {
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		if ($("#tanggal_pencairan2").val() == "") {
			sweetAlert("Maaf...", "Tanggal Pencairan Harus Diisi!", "error");
			$("#tanggal_pencairan2").focus();
			return false;
		}
		
		save_pendapatan();
		
	}
	$(document).on("click",".edit_pembayaran",function(){
		var table = $('#tabel_pembayaran').DataTable();
        tr = table.row($(this).parents('tr')).index()
		
		$("#modal_pembayaran").modal('show');
		$("#iddet").val(table.cell(tr,0).data());
		$("#sumber_kas_id_tmp").val(table.cell(tr,2).data());
		$("#jenis_kas_id").val(table.cell(tr,1).data()).trigger('change');
		$("#nominal_bayar").val(table.cell(tr,10).data());
		$("#tanggal_pencairan").val(table.cell(tr,7).data());
		$("#nama_rekening").val(table.cell(tr,9).data());
		$("#ket_pembayaran").summernote('code',table.cell(tr,8).data());
		$("#idmetode").val(table.cell(tr,3).data()).trigger('change');
		$("#sisa_modal").val(parseFloat($("#total_sisa").val()) + parseFloat($("#nominal_bayar").val()));
		$("#idbank").val(table.cell(tr,12).data());
		$("#nama_bank").val(table.cell(tr,13).data());
		$("#norek").val(table.cell(tr,14).data());
		$("#atas_nama").val(table.cell(tr,15).data());
		// sisa_modal($();
		// $("#pembayaran_id").val($(this).closest('tr').find("td:eq(0)").val());

		
		return false;
	});
	$(document).on("click",".edit_pendapatan",function(){
		var table = $('#tabel_pendapatan').DataTable();
        tr = table.row($(this).parents('tr')).index()
		
		$("#modal_pendapatan").modal('show');
		$("#iddet2").val(table.cell(tr,0).data());
		$("#sumber_kas_id_tmp2").val(table.cell(tr,2).data());
		$("#jenis_kas_id2").val(table.cell(tr,1).data()).trigger('change');
		$("#nominal_bayar2").val(table.cell(tr,9).data());
		$("#tanggal_pencairan2").val(table.cell(tr,7).data());
		$("#ket_pembayaran2").summernote('code',table.cell(tr,8).data());
		$("#idmetode2").val(table.cell(tr,3).data()).trigger('change');
		$("#sisa_modal2").val(parseFloat($("#total_sisa_pendapatan").val()) + parseFloat($("#nominal_bayar2").val()));
		return false;
	});
	$(document).on("keyup","#nominal_bayar",function(){
		var sisa=parseFloat($("#sisa_modal").val());
		var bayar=parseFloat($("#nominal_bayar").val());
		if (sisa < bayar){
			$("#nominal_bayar").val(sisa);
			return false;
		}
		
	});
	$(document).on("keyup","#nominal_bayar2",function(){
		var sisa=parseFloat($("#sisa_modal2").val());
		var bayar=parseFloat($("#nominal_bayar2").val());
		if (sisa < bayar){
			$("#nominal_bayar2").val(sisa);
			return false;
		}
		
	});
	$(document).on("click", ".hapus_pembayaran", function() {
		var table = $('#tabel_pembayaran').DataTable();
        tr = table.row($(this).parents('tr')).index()
		var id=table.cell(tr,0).data()
			swal({
				title: "Apakah Anda Yakin ?",
				text : "Akan Menghapus Pembayaran ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				$.ajax({
					url: '{site_url}trekap/hapus_pembayaran',
					type: 'POST',
					dataType: "json",
					data: {id: id},
					success : function(data) {				
						$('#tabel_pembayaran').DataTable().ajax.reload( null, false );
						pembayaran_clear();
					}
				});
			});

			// if (confirm("Hapus data ?") == true) {
				
			// }
		// return false;
	});
	$(document).on("click", ".hapus_pendapatan", function() {
		var table = $('#tabel_pendapatan').DataTable();
        tr = table.row($(this).parents('tr')).index()
		var id=table.cell(tr,0).data()
			swal({
				title: "Apakah Anda Yakin ?",
				text : "Akan Menghapus Pendapatan ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				$.ajax({
					url: '{site_url}trekap/hapus_pendapatan',
					type: 'POST',
					dataType: "json",
					data: {id: id},
					success : function(data) {				
						$('#tabel_pendapatan').DataTable().ajax.reload( null, false );
						pembayaran_clear();
					}
				});
			});

			// if (confirm("Hapus data ?") == true) {
				
			// }
		// return false;
	});
	function formatNumber (num) {
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}
	function checkTotal(){
		total_bayar_sisa();
	}
	function total_bayar_sisa(){
		// get_total_bayar
		var id=$("#id").val();
		// alert(id);
		$.ajax({
			url: '{site_url}trekap/get_total_bayar/'+id,
			dataType: "json",
			success: function(data) {
				$("#total_bayar").val(data.total)
				$("#total_sisa").val(parseFloat($("#nominal").val())-data.total)
				$("#total_pendapatan").val(data.total_pendapatan)
				$("#total_sisa_pendapatan").val(parseFloat($("#nominal").val())-data.total_pendapatan)
			}
		});

	}
	function pembayaran_clear(){
		$("#sumber_kas_id_tmp").val('');
		$("#number").val('');
		$("#rowindex").val('');
		$("#modal_pembayaran").modal('hide');
		clear_input_pembayaran();
	}
	function clear_input_pembayaran(){
		$("#jenis_kas_id").val("#").trigger('change');
		$("#sumber_kas_id").val("#").trigger('change');
		$("#nominal_bayar").val(0);
		$("#sisa_modal").val(0);
		$("#idpembayaran").val('');
		$("#idmetode").val("#").trigger('change');
		$("#ket_pembayaran").summernote('code','');
		$("#idbank").val('');
		$("#nama_bank").val('');
		$("#norek").val('');
		$("#atas_nama").val('');
		$("#nama_rekening").val('');
		$("#iddet").val('');
		
		// ("#jenis_kas_id2").val("#").trigger('change');
		$("#sumber_kas_id2").val("#").trigger('change');
		$("#nominal_bayar2").val(0);
		$("#sisa_modal2").val(0);
		$("#idmetode").val("#").trigger('change');
		$("#ket_pembayaran2").summernote('code','');
		
		$("#iddet2").val('');
		total_bayar_sisa();
	}
	function validate_final(){
		
		var tipe_bayar=$("#tipe_bayar").val();
		if (tipe_bayar=='1'){
			if ($("#total_bayar").val() == "0") {
				sweetAlert("Maaf...", "Pembayaran Belum Dipilih!", "error");
				return false;
			}
			if ($("#total_sisa").val() != "0") {
				sweetAlert("Maaf...", "Masih Ada Sisa!", "error");
				return false;
			}
		}
		if (tipe_bayar=='3'){
			if ($("#total_sisa_pendapatan").val() != "0") {
				sweetAlert("Maaf...", "Masih Ada Sisa Pendapatan!", "error");
				return false;
			}
		}
	}
</script>
