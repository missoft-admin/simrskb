<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
          <a href="{base_url}trekap" class="btn"><i class="fa fa-reply"></i></a>
			</li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">

		<hr style="margin-top:0px">
		<div class="row">
			<?php echo form_open('trekap/filter','class="form-horizontal" id="form-work"') ?>
			
			<div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="idbed">No. Transaksi</label>
					<div class="col-md-8">
					<input type="hidden" class="form-control" readonly id="id" placeholder="No. Transaksi" name="id" value="{id}">
					<input type="hidden" class="form-control" readonly id="disabel" placeholder="No. Transaksi" name="disabel" value="{disabel}">
					<input type="text" class="form-control" readonly id="notransaksi" placeholder="No. Transaksi" name="notransaksi" value="{notransaksi}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="idbed">Periode</label>
					<div class="col-md-4">
						<div class="input-group">
                            <input disabled class="form-control" type="text" id="periode" name="periode" value="<?=HumanDateShort($periode)?>"/>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
					</div>
				</div>
			</div>
			
			<?php echo form_close() ?>
		</div>

		<hr style="margin-top:10px">

		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="index_list">
			<thead>
				<tr>
					<th>#</th>
					<th>No</th>
					<th>Nama Rekapan</th>
					<th>Nominal</th>
					<th>Tipe Variable</th>
					<th>Pendapatan</th>
					<th>Pembayaran / Pendapatan</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>

<div id="cover-spin"></div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		load_index();	
	})
	$(document).on("click","#btn_filter",function(){	
		load_index();	
	});
	$(document).on("click",".verifikasi",function(){	
		var table = $('#index_list').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var idrekap=table.cell(tr,0).data();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Verifikasi Rekap?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			verifikasi(idrekap);
		});


		
	
	});
	function verifikasi($id){
		var idrekap=$id;
		$.ajax({
			url: '{site_url}trekap/verifikasi',
			type: 'POST',
			data: {idrekap: idrekap},
			complete: function(data) {		
				// console.log(data);
				swal({
					title: "Berhasil!",
					text: "Rekap Berhasil Verifikasi.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});
				
				$('#index_list').DataTable().ajax.reload(null,false)
				$("#cover-spin").hide();
			}
		});
		
		
	}
	$(document).on("click",".batalkan",function(){	
		var table = $('#index_list').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var idrekap=table.cell(tr,0).data();
		// alert(idrekap);
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Membatalkan Rekap?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			batalkan(idrekap);
		});


		
	
	});
	function batalkan($id){
		var idrekap=$id;
		$.ajax({
			url: '{site_url}trekap/batalkan',
			type: 'POST',
			data: {idrekap: idrekap},
			complete: function(data) {		
				// console.log(data);
				swal({
					title: "Berhasil!",
					text: "Gaji Berhasil Dihapus.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});
				
				$('#index_list').DataTable().ajax.reload(null,false)
			}
		});
		
		
	}
	function load_index(){
		var id=$("#id").val();
		var disabel=$("#disabel").val();
		
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
			"pageLength": 25,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"autoWidth": false,
			"fixedHeader": true,
			"searching": true,
			"order": [],
			"ajax": {
				url: '{site_url}trekap/getDetail/',
					type: "POST",
					data: {
						id: id,disabel: disabel,
						},
					dataType: 'json'
				},
				"columnDefs": [
					{"targets": [0], "visible": false },
					{ "width": "5%", "targets": 1, "orderable": true ,"class":"text-right"},
					{ "width": "15%", "targets": 2, "orderable": true ,"class":"text-left"},
					{ "width": "10%", "targets": 3, "orderable": true ,"class":"text-right"},
					{ "width": "10%", "targets": 4, "orderable": true,"class":"text-center" },
					{ "width": "10%", "targets": 5, "orderable": true,"class":"text-center" },
					{ "width": "15%", "targets": 6, "orderable": true,"class":"text-center" },
					{ "width": "15%", "targets": 7, "orderable": true ,"class":"text-left"},
				]
			});
	}
	
</script>
