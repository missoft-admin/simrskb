<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">

		<hr style="margin-top:0px">
		<div class="row">
			<?php echo form_open('trekap/filter','class="form-horizontal" id="form-work"') ?>
			
			<div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="idbed">Status</label>
					<div class="col-md-8">
						<select tabindex="1" id="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
							<option value="#" selected>- Semua -</option>
							<option value="1">BELUM DIPROSES</option>
							<option value="2">SUDAH DIPROSES</option>
							<option value="3">VERIFIKASI</option>
						</select>
					</div>
				</div>
				
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="idbed">Periode</label>
					<div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="periode1" name="periode1" placeholder="From" value=""/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="periode2" name="periode2" placeholder="To" value=""/>
                        </div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" id="btn_filter" type="button" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>

		<hr style="margin-top:10px">

		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="index_list">
			<thead>
				<tr>
					<th>#</th>
					<th>No</th>
					<th>No. Reg</th>
					<th>Periode</th>
					<th>Total Penggajian</th>
					<th>Total Rekap</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>

<div id="cover-spin"></div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		load_index();	
	})
	$(document).on("click","#btn_filter",function(){	
		load_index();	
	});
	$(document).on("click",".verifikasi",function(){	
		var table = $('#index_list').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var idrekap=table.cell(tr,0).data();
		// alert(idrekap);return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Verifikasi Rekap?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			verifikasi(idrekap);
		});


		
	
	});
	function verifikasi($id){
		var idrekap=$id;
		$.ajax({
			url: '{site_url}trekap/validasi',
			type: 'POST',
			data: {idrekap: idrekap},
			complete: function(data) {		
				// console.log(data);
				swal({
					title: "Berhasil!",
					text: "Rekap Berhasil Verifikasi.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});
				
				$('#index_list').DataTable().ajax.reload(null,false)
				$("#cover-spin").hide();
			}
		});
		
		
	}
	$(document).on("click",".start",function(){	
		var table = $('#index_list').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var idrekap=table.cell(tr,0).data();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghitung Rekap?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			start(idrekap);
		});


		
	
	});
	function start($id){
		var idrekap=$id;
		$.ajax({
			url: '{site_url}trekap/start',
			type: 'POST',
			data: {idrekap: idrekap},
			complete: function(data) {		
				// console.log(data);
				swal({
					title: "Berhasil!",
					text: "Rekap Berhasil Start.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});
				
				$('#index_list').DataTable().ajax.reload(null,false)
				$("#cover-spin").hide();
			}
		});
		
		
	}
	$(document).on("click",".batalkan",function(){	
		var table = $('#index_list').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var idrekap=table.cell(tr,0).data();
		// alert(idrekap);
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Membatalkan Rekap?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			batalkan(idrekap);
		});


		
	
	});
	function batalkan($id){
		var idrekap=$id;
		$.ajax({
			url: '{site_url}trekap/batalkan',
			type: 'POST',
			data: {idrekap: idrekap},
			complete: function(data) {		
				// console.log(data);
				swal({
					title: "Berhasil!",
					text: "Gaji Berhasil Dihapus.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});
				
				$('#index_list').DataTable().ajax.reload(null,false)
			}
		});
		
		
	}
	function load_index(){
		var periode1=$("#periode1").val();
		var periode2=$("#periode2").val();
		var status=$("#status").val();
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"autoWidth": false,
			"fixedHeader": true,
			"searching": true,
			"order": [],
			"ajax": {
				url: '{site_url}trekap/getIndex/' + '<?=$this->uri->segment(2)?>',
					type: "POST",
					data: {
						periode1: periode1,
						periode2: periode2,
						status: status,
						},
					dataType: 'json'
				},
				"columnDefs": [
					{"targets": [0], "visible": false },
					{ "width": "5%", "targets": 1, "orderable": true },
					{ "width": "10%", "targets": 2, "orderable": true },
					{ "width": "15%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 4, "orderable": true,"class":"text-right" },
					{ "width": "10%", "targets": 5, "orderable": true,"class":"text-right" },
					{ "width": "10%", "targets": 6, "orderable": true },
					{ "width": "15%", "targets": 7, "orderable": true },
				]
			});
	}
	
</script>
