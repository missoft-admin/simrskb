<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<style media="screen">
	.v-align-middle {
		vertical-align: middle;
	}
</style>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}msetting_analisa" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>

	<?php echo form_open('msetting_analisa/save','class="push-10-t" id="form-work"') ?>
	<div class="block-content">
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Nama Analisa <span class="text-danger">*</span></label>
			<div class="col-md-7">
				<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}">
			</div>
		</div>
	</div>
	<div class="block-content">
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Jenis Kunjungan <span class="text-danger">*</span></label>
			<div class="col-md-7">
				<select class="js-select2 form-control" id="jenis" name="jenis[]" style="width: 100%;" data-placeholder="Jenis Kunjungan" multiple>
					<?php foreach ($formData['jenis'] as $row) { ?>
						<option value="<?= $row['id']; ?>" <?= (in_array($row['id'], $jenis) ? 'selected':'') ?>><?= $row['nama']; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>
	<div class="block-content">
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Tipe</label>
			<div class="col-md-7">
				<select class="js-select2 form-control" id="tipe" name="tipe[]" style="width: 100%;" data-placeholder="Tipe" multiple>
					<?php if ($this->uri->segment('2') == 'update') { ?>
						<?php foreach ($formData['tipe'] as $row) { ?>
							<option value="<?= $row['id']; ?>" <?= (in_array($row['id'], $tipe) ? 'selected':'') ?> data-jenis="<?= $row['jenis']; ?>"><?= $row['nama']; ?></option>
						<?php } ?>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>
	<div class="block-content">
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Poliklinik / Kelas</label>
			<div class="col-md-7">
				<select class="js-select2 form-control" id="poliklinik" style="width: 100%;" data-placeholder="Poliklinik / Kelas" multiple>
					<?php if ($this->uri->segment('2') == 'update') { ?>
						<?php foreach ($formData['poliklinik'] as $row) { ?>
							<option value="<?= $row['id']; ?>" <?= (in_array($row['tipe'].'_'.$row['id'], $poliklinik) ? 'selected':'') ?> data-jenis="<?= $row['jenis']; ?>" data-tipe="<?= $row['tipe']; ?>"><?= $row['nama']; ?></option>
						<?php } ?>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>
	<div class="block-content">
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Dokter</label>
			<div class="col-md-7">
				<select class="js-select2 form-control" id="dokter" name="dokter[]" style="width: 100%;" data-placeholder="Dokter" multiple>
					<?php foreach ($formData['dokter'] as $row) { ?>
						<option value="<?= $row->id; ?>" <?= (in_array($row->id, $dokter) ? 'selected':'') ?>><?= $row->nama; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>
	<div class="block-content">
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Kelompok Diagnosa</label>
			<div class="col-md-7">
				<select class="js-select2 form-control" id="kelompok_diagnosa" name="kelompok_diagnosa[]" style="width: 100%;"
					data-placeholder="Kelompok Diagnosa" multiple>
					<?php foreach (get_all('mkelompok_diagnosa', array('status' => '1')) as $row) { ?>
						<option value="<?= $row->id; ?>" <?= (in_array($row->id, $kelompok_diagnosa) ? 'selected':'') ?>><?= $row->nama; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>
	<div class="block-content">
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Kelompok Tindakan</label>
			<div class="col-md-7">
				<select class="js-select2 form-control" id="kelompok_tindakan" name="kelompok_tindakan[]" style="width: 100%;"
					data-placeholder="Kelompok Tindakan" multiple>
					<?php foreach (get_all('mkelompok_tindakan', array('status' => '1')) as $row) { ?>
						<option value="<?= $row->id; ?>" <?= (in_array($row->id, $kelompok_tindakan) ? 'selected':'') ?>><?= $row->nama; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>
	<div class="block-content">
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Kelompok Tindakan (Operasi)</label>
			<div class="col-md-7">
				<select class="js-select2 form-control" id="kelompok_tindakan_operasi" name="kelompok_tindakan_operasi[]" style="width: 100%;"
					data-placeholder="Kelompok Tindakan (Operasi)" multiple>
					<?php foreach (get_all('mkelompok_operasi', array('status' => '1')) as $row) { ?>
						<option value="<?= $row->id; ?>" <?= (in_array($row->id, $kelompok_tindakan_operasi) ? 'selected':'') ?>><?= $row->nama; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>

	<div class="block-content" style="padding-left: 35px;">
		<b><span class="label label-success" style="font-size:12px">LEMBARAN RM</span></b>
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="tableLembaranRM" style="margin-top: 10px;">
			<thead>
				<tr>
					<th width="5%"></th>
					<th width="20%">Nama Lembaran</th>
					<th width="20%">Digunakan Untuk</th>
					<th width="20%">Deksiripsi</th>
					<th width="15%">Urutan Tampil</th>
					<th width="15%">Variable HOLD</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach(get_all('mlembaran_rm', array('status' => '1')) as $index => $row) { ?>
					<?php
						if (isset($lembaran[$index])) {
							$urutanTampil = $lembaran[$index]->urutan_tampil;
							$variableHold = $lembaran[$index]->variable_hold;
							$statusChecked = ($row->id == $lembaran[$index]->lembaran_id);
						} else {
							$urutanTampil = '';
							$statusChecked = false;
							$variableHold = 0;
						}

						if ($row->referensi) {
							$digunakanUntuk = str_replace('_', ' ', "<span class='label label-primary'>" . implode("</span>&nbsp;<span class='label label-primary'>", array_map('strtoupper', json_decode($row->referensi))) . '</span>');
						} else {
							$digunakanUntuk = '';
						}
					?>

					<tr>
						<td hidden><?=$row->id?></td>
						<td class="v-align-middle">
							<label class="css-input css-checkbox css-checkbox-success">
								<input type="checkbox" class="checkboxLembaran" value="<?= $row->id; ?>" <?= ($statusChecked ? 'checked':''); ?>>
								<span></span>
							</label>
						</td>
						<td class="v-align-middle">
							<?=$row->nama?>
						</td>
						<td class="v-align-middle">
							<?=$digunakanUntuk?></td>
						<td class="v-align-middle">
							<?=strip_tags($row->deskripsi)?>
						</td>
						<td class="v-align-middle">
							<input type="text" class="form-control" <?= ($statusChecked ? '':'disabled'); ?> placeholder="Urutan Tampil" value="<?= $urutanTampil ?>">
						</td>
						<td class="v-align-middle">
							<select class="js-select2 form-control" <?= ($statusChecked ? '':'disabled'); ?> data-placeholder="Variable Hold">
								<option value="1" <?= ($variableHold == 1 ? 'selected':''); ?>>Ya</option>
								<option value="0" <?= ($variableHold == 0 ? 'selected':''); ?>>Tidak</option>
							</select>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>

		<b><span class="label label-success" style="font-size:12px">Keterangan</span></b>
		<br><br>
		<textarea class="form-control js-summernote" id="keterangan" placeholder="Keterangan" name="keterangan" rows="5"><?=$keterangan ?></textarea>
	</div>

	<div class="row">
		<div class="block-content">
			<div class="form-group">
				<div class="col-md-12 text-right">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mvariable_rekapan" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
		</div>
	</div>

	<input type="hidden" id="rowindex" value="">
	<input type="hidden" id="tableLembaranValue" name="lembaran" value="">
	<input type="hidden" id="poliklinikValue" name="poliklinik" value="">

	<br><br>
	<?php echo form_hidden('id', $id); ?>
	<?php echo form_close() ?>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$(".number").number(true, 0, '.', ',');

		$("#jenis").change(function() {
			var formDataTipe = <?= json_encode($formData['tipe']); ?>;
			var jenisOptionSelected = $(this).val();
			var tipeOptionSelected = $("#tipe option:selected");
			var poliklinikOptionSelected = $("#poliklinik option:selected");

			var tipeOptionSelectedValue = tipeOptionSelected.get().map(function(item) {
				return item.id;
			});

			if (jenisOptionSelected) {
				// Remove Option Where JENIS SELECTED
				tipeOptionSelected.filter(function() {
			    return !jenisOptionSelected.includes($(this).data("jenis"));
			  }).remove();

				poliklinikOptionSelected.filter(function() {
			    return !jenisOptionSelected.includes($(this).data("jenis"));
			  }).remove();

				// Append New Option
				var option = formDataTipe.filter(function(item) {
					return jenisOptionSelected.includes(item.jenis)
				});

				$("#tipe").html('');
				option.map(function(item) {
					var selected = tipeOptionSelectedValue.includes(item.id) ? 'selected' : '';
					$("#tipe").append('<option value="' + item.id + '" ' + selected + ' data-jenis="' + item.jenis + '">' + item.nama + '</option>');
				});
			} else {
				// Reset All Option
				$("#tipe").html('');
				$("#poliklinik").html('');
			}
		});

		$("#tipe").change(function() {
			var tipe = $(this).val(); // tipe : poli, igd, ranap, ods
			var poliklinik = $("#poliklinik > option:selected");
			var poliklinikOptionSelectedValue = poliklinik.get().map(function(item) {
					return $(item).data('tipe') + '_' + item.id;
			});

			if (tipe) {
				// Remove Option Where TIPE SELECTED
				if (poliklinik) {
					poliklinik.filter(function() {
				    return !tipe.includes($(this).data('tipe'));
				  }).remove();
				} else {
					poliklinik.remove();
				}

				// Append New Option
				var formDataPoliklinik = <?= json_encode($formData['poliklinik']); ?>;
				var option = formDataPoliklinik.filter(function(item) {
					return tipe.includes(item.tipe)
				});

				$("#poliklinik").html('');
				option.map(function (item) {
					var selected = poliklinikOptionSelectedValue.includes(item.tipe + '_' + item.id) ? 'selected' : '';
					$("#poliklinik").append('<option value="' + item.id + '" ' + selected + ' data-jenis="' + item.jenis + '" data-tipe="' + item.tipe + '">' + item.nama + '</option>');
				});
			} else {
				// Reset All Option
				$("#poliklinik").html('');
			}
		});

		$(".checkboxLembaran").click(function() {
			const urutanTampil = $(this).closest('tr').find('td:eq(5) input');
			const variableHold = $(this).closest('tr').find('td:eq(6) select');

			if (this.checked) {
				urutanTampil.attr('disabled', false);
				variableHold.attr('disabled', false);
			} else {
				urutanTampil.attr('disabled', true);
				variableHold.attr('disabled', true);
			}
		});

		// Save
		$("#form-work").submit(function(e) {
			var form = this;

			var tableLembaranRM = $('table#tableLembaranRM tbody tr').get().map(function(row) {
				return $(row).find('td:eq(1) input:checked').closest('tr').get().map(function(cell, index) {
					return {
						'index': index,
            'lembaran_id': $(cell).find("td:eq(0)").html(),
            'urutan_tampil': $(cell).find("td:eq(5) input").val(),
            'variable_hold': $(cell).find("td:eq(6) select option:selected").val(),
					}
				})[0];
			}).filter(function (item) {
				return item
			});

			$("#tableLembaranValue").val(JSON.stringify(tableLembaranRM));

			var poliklinik = $("#poliklinik > option:selected");
			var poliklinikJSON = poliklinik.get().map(function(item) {
				return {
					'id': $(item).val(),
					'name': $(item).text(),
					'jenis': $(item).data('jenis'),
					'tipe': $(item).data('tipe'),
				}
			});

			$("#poliklinikValue").val(JSON.stringify(poliklinikJSON));

			swal({
				title: "Berhasil!",
				text: "Proses penyimpanan data.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});
	});
</script>
