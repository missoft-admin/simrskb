<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}trekap_penggajian" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<?php echo form_open('trekap_penggajian/updateReview','class="form-horizontal push-10-t" id="form-work"') ?>

	<div class="block-content">
		<table class="table table-bordered table-striped" id="detailList">
			<thead>
				<tr>
					<th>Nama Variable</th>
					<th>Nominal</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($list_detail as $row){ ?>
					<tr>
						<td style="display:none"><?=$row->id?></td>
						<td><?=($row->idsub != 0 ? '└─' : '').' '.$row->nama?></td>
						<td>
							<input type="text" class="form-control number <?= ($row->idsub == 0 ? "header_".$row->idvariable : "child child_".$row->idvariable) ?>" data-header="<?= "header_".$row->idvariable?>" data-child="<?= "child_".$row->idvariable ?>" <?= ($row->idsub == 0 && $row->subrekapan == 1 ? 'readonly' : '')?> placeholder="Nominal" value="<?=number_format($row->nominal)?>">
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>

	<div class="row" style="margin-top:-25px">
		<div class="block-content block-content-narrow">
			<div class="form-group">
				<label class="col-md-3 control-label"></label>
				<div class="col-md-7">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}trekap_penggajian" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
		</div>
	</div>

	<input type="hidden" id="rowindex" value="">
	<input type="hidden" id="detailValue" name="detail_value" value="">

	<br><br>
	<?php echo form_hidden('id', $id); ?>
	<?php echo form_close() ?>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$(".number").number(true, 0, '.', ',');

		$(document).on("click", "#showVariableModal", function() {
			getVariableRekapan(0);
		});

		$(document).on("change", "#idtipe", function() {
			getVariableRekapan($(this).val());
		});

		$(document).on("click", "#addVariable", function() {
      var html = '';
      var getSelectedRows = $("input:checked").parents("tr");

			$('#variableData').find('tr').each(function () {
	        var row = $(this);
	        if (row.find('input[type="checkbox"]').is(':checked')) {
						html += '<tr>';
		        html += '<td style="display:none">' + row.children('td').eq(1).html() + '</td>';
		        html += '<td>' + row.children('td').eq(2).html() + '</td>';
		        html += '<td>' + row.children('td').eq(3).html() + '</td>';
		        html += '<td><a href="#" class="btn btn-danger btn-sm removeVariable" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash-o"></i></a></td>';
		        html += '</tr>';
	        }
	    });

      $("#detailList tbody").append(html);

			swal({
				title: "Berhasil!",
				text: "Data telah ditambahkan.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});

		$(document).on("click", ".removeVariable", function() {
			if (confirm("Hapus data ?") == true) {
				$(this).closest('td').parent().remove();
			}
			return false;
		});

		$(document).on("keyup", ".child", function() {
			var var_childs = "." + $(this).data('child');
			var var_header= "." + $(this).data('header');
			var total = 0;
			$(var_childs).each(function() {
				total += parseFloat($(this).val());
			});
			console.log(var_header);
			$(var_header).val(total);
		});

		$("#form-work").submit(function(e) {
			var form = this;

			var detailValue_tbl = $('table#detailList tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function(cell) {
					if ($(cell).find("input").length >= 1) {
						return $(cell).find("input").val();
					} else {
						return $(cell).html();
					}
				});
			});

			$("#detailValue").val(JSON.stringify(detailValue_tbl));

			swal({
				title: "Berhasil!",
				text: "Proses penyimpanan data.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});
	});

	function getVariableRekapan(idtipe){
		$.ajax({
			url: '{site_url}trekap_penggajian/getVariableRekapan/' + idtipe,
			method: 'GET',
			success: function(data) {
				$("#variableData tbody").html(data);
			}
		});
	}


</script>
