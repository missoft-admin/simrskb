<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Registrasi</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_reg" placeholder="No Registrasi" name="no_reg" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Medrec</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_medrec" placeholder="No. Medrec" name="no_medrec" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="nama_pasien" placeholder="Nama Pasien" name="nama_pasien" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Kelompok Pasien</label>
                    <div class="col-md-8">
                        <select id="idkelompokpasien" name="idkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Kelompok Pasien -</option>
							<?foreach($list_kelompok_pasien as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Perusahaan</label>
                    <div class="col-md-8">
                        <select id="idrekanan" name="idrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Perusahaan -</option>
							<?foreach($list_rekanan as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
                                
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Jenis</label>
                    <div class="col-md-8">
						<select id="tipe" name="tipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Tipe -</option>
							<option value="1" <?=("1" == $tipe ? 'selected' : ''); ?>>Poliklinik</option>
							<option value="2" <?=("2" == $tipe ? 'selected' : ''); ?>>IGD</option>
							<option value="3" <?=("3" == $tipe ? 'selected' : ''); ?>>Rawat Inap</option>
							<option value="4" <?=("4" == $tipe ? 'selected' : ''); ?>>ODS</option>
							
						</select>
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Status</label>
                    <div class="col-md-8">
                        <select id="status" name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Status -</option>
							<option value="0" <?=("0" == $status ? 'selected' : ''); ?>>Menunggu Pembayaran</option>
							<option value="1" <?=("1" == $status ? 'selected' : ''); ?>>Selesai Pembayaran</option>
							
						</select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Transaksi</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tgl_trx1" name="tgl_trx1" placeholder="From" value="{tgl_trx1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tgl_trx2" name="tgl_trx2" placeholder="To" value="{tgl_trx2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Daftar</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggaldaftar1" name="tanggaldaftar1" placeholder="From" value="{tanggaldaftar1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggaldaftar2" name="tanggaldaftar2" placeholder="To" value="{tanggaldaftar2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
					<th width="0%" class="text-center"></th>
                    <th width="0%" class="text-center"></th>
                    <th width="0%" class="text-center"></th>
                    <th width="0%" class="text-center"></th>
                    <th width="10%">NO</th>
                    <th width="10%">TGL KUNJUGAN / NO. REG</th>
                    <th width="10%">TGR TRX</th>
                    <th width="10%">JENIS</th>
                    <th width="10%">MEDREC</th>
                    <th width="10%">PASIEN</th>
                    <th width="15%">KEL PASIEN</th>
                    <th width="15%">TOT TAGIHAN</th>
                    <th width="15%">TOT PEMBAYARAN</th>
                    <th width="15%">SISA</th>
                    <th width="15%">JAMINAN</th>
                    <th width="15%">KET</th>
                    <th width="15%">STATUS</th>
                    <th width="15%" class="text-center">AKSI</th>
                   
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
</div>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	load_index();
   
});
function load_index(){
	var no_reg=$("#no_reg").val();
	var no_medrec=$("#no_medrec").val();
	var nama_pasien=$("#nama_pasien").val();
	var idkelompokpasien=$("#idkelompokpasien").val();
	var idrekanan=$("#idrekanan").val();
	var tipe=$("#tipe").val();
	// alert(tipe);
	var status=$("#status").val();
	var tgl_trx1=$("#tgl_trx1").val();
	var tgl_trx2=$("#tgl_trx2").val();
	var tanggaldaftar1=$("#tanggaldaftar1").val();
	var tanggaldaftar2=$("#tanggaldaftar2").val();
	
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: false,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0,1,2,3,8], "visible": false },
							{ "width": "3%", "targets": [4] },
							{ "width": "6%", "targets": [5,6,7,10,14] },
							{ "width": "7%", "targets": [11,12,13,15,16] },
							{ "width": "10%", "targets": [9] },
							// { "width": "8%", "targets": [8,,9,10] },
							{ "width": "12%", "targets": [17] },
						 {"targets": [11,12,13], className: "text-right" },
						 {"targets": [5,6,7,8,10,14,15,16,17], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tpiutang_tt_pembayaran/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						no_reg:no_reg,no_medrec:no_medrec,nama_pasien:nama_pasien,idkelompokpasien:idkelompokpasien,
						idrekanan:idrekanan,tipe:tipe,status:status,tgl_trx1:tgl_trx1,
						tgl_trx2:tgl_trx2,tanggaldaftar1:tanggaldaftar1,tanggaldaftar2:tanggaldaftar2,
					   }
            }
        });
}
$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_index();
});
function verifikasi($id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Verifikasi  ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		table = $('#index_list').DataTable()	
			$.ajax({
				url: '{site_url}tpiutang_tt_pembayaran/verifikasi',
				type: 'POST',
				data: {id: $id},
				complete: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi'});
					table.ajax.reload( null, false ); 
				}
		});
	});
}
$(document).on("click", ".verifikasi", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var idtipe=table.cell(tr,0).data()
	var id=table.cell(tr,1).data()
	var tanggal_jt=table.cell(tr,16).data()
	var idpendaftaran=table.cell(tr,2).data()
	var noreg=table.cell(tr,5).data()
	
	
});
// function verifikasi($id,$idtipe,$tanggal_jt,$idpendaftaran){
	// console.log($id);
	// var id=$id;		
	// var idtipe=$idtipe;		
	// var idpendaftaran=$idtipe;		
	// var tanggal_jt=$tanggal_jt;		
	
	
// }

$(document).on("click", ".batal_verifikasi", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,1).data()
	var nopenerimaan=table.cell(tr,4).data()
	// alert(id);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Membatalkan Verifikasi No. Penerimaan "+nopenerimaan+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		// pause_so();
		batal_verifikasi(id);
	});
});
function batal_verifikasi($id){
	// console.log($id);
	var id=$id;		
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tpiutang_tt_pembayaran/batal_verifikasi',
		type: 'POST',
		data: {id: id},
		complete: function() {
			$.toaster({priority : 'success', title : 'Succes!', message : ' Membatalkan Verifikasi'});
			table.ajax.reload( null, false ); 
		}
	});
}
$(document).on("click", ".ganti", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,17).data()
	var st_multiple=table.cell(tr,22).data()
	var idpasien=table.cell(tr,23).data()
	
	$("#xid").val(id);
	$("#xtipe").val(table.cell(tr,0).data()).trigger('change');
	$("#xidkelompokpasien").val(table.cell(tr,20).data()).trigger('change');
	$("#xidrekanan").val(table.cell(tr,21).data()).trigger('change');
	$("#tgl_asal").val(table.cell(tr,15).data()).trigger('change');
	$("#xtanggal_tagihan").val(table.cell(tr,15).data()).trigger('change');
	$("#xst_multiple").val(st_multiple);
	$("#xidpasien").val(idpasien);
	load_list_tanggal();
	$('#modal_edit').modal('show');
	
});
function load_list_tanggal(){
	var idkelompokpasien=$("#xidkelompokpasien").val();
	var idrekanan=$("#xidrekanan").val();
	var tipe=$("#xtipe").val();
	var st_multiple=$("#xst_multiple").val();
	var idpasien=$("#xidpasien").val();
	// alert(idkelompokpasien+' - '+idrekanan+' - '+tipe);
	if (idkelompokpasien && tipe){
		$.ajax({
			url: '{site_url}tklaim_rincian/list_tanggal',
			dataType: "json",
			type: 'POST',
			data: {idkelompokpasien: idkelompokpasien,idrekanan:idrekanan,tipe:tipe,st_multiple:st_multiple,idpasien:idpasien},
			success: function(data) {
				$("#xtgl_pilih").empty();
				$('#xtgl_pilih').append(data.detail);
			}
		});
	}
}

$(document).on("click", "#btn_ubah", function() {
		if ($("#xtgl_pilih").val()=='' || $("#xtgl_pilih").val()==null){
			sweetAlert("Maaf...", "Tanggal belum dipilih!", "error");
			return false;
		}
		// alert();
		if ($("#xidkelompokpasien").val()=='1' && $("#xidrekanan").val()=='#'){
			sweetAlert("Maaf...", "Rekanan Harus diisi!", "error");
			return false;
		}
		
		swal({
			title: "Anda Yakin ?",
			text : "Akan Memindahkan Faktur ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			edit_tanggal_satu();
		});
		
});
function edit_tanggal_satu(){
	var klaim_id=$("#xid").val();		
	var tanggal_tagihan=$("#xtgl_pilih").val();		
	var idkelompokpasien=$("#xidkelompokpasien").val();		
	var idrekanan=$("#xidrekanan").val();		
	var tipe=$("#xtipe").val();	
	var st_multiple=$("#xst_multiple").val();
	var idpasien=$("#xidpasien").val();
	// alert(klaim_id+' tg l'+tanggal_tagihan+' Kel '+idkelompokpasien+' rekan '+idrekanan+' tipe '+tipe);
	// return false;
	var table = $('#index_list').DataTable();
	$.ajax({
		url: '{site_url}tklaim_rincian/edit_tanggal_satu',
		type: 'POST',
		data: {klaim_id: klaim_id,tanggal_tagihan:tanggal_tagihan,idkelompokpasien:idkelompokpasien,idrekanan:idrekanan,tipe:tipe,st_multiple:st_multiple,idpasien:idpasien},
		complete: function() {

			$.toaster({priority : 'success', title : 'Succes!', message : ' Memindahkan Tanggal Jatuh Tempo'});
			$("#modal_edit").modal('hide');
			$('#index_list').DataTable().ajax.reload( null, false );
			$("#cover-spin").hide();
		}
	});
}
</script>