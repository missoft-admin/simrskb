<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work-1"'); ?>
                <div class="col-md-6"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Nomor Register</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{nopendaftaran}" disabled>
                            <input class="form-control" type="hidden" name="disabel" id="disabel" value="{disabel}" >
                            <input class="form-control" type="hidden" name="id" id="id" value="{id}" >
                        </div> 
                    </div>
                      
                </div>
				<div class="col-md-6"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Tipe Kunjungan</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="<?=GetTipePasienPiutangBiasa($idtipe)?>" disabled>
                        </div> 
                    </div>
                      
                </div>
				<div class="col-md-6"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">No Medrec</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{no_medrec}" disabled>
                        </div> 
                    </div>
                      
                </div>
				<div class="col-md-6"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Tipe Pembayaran</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="<?=($cara_bayar=='1'?'SEKALI BAYAR':'CICILAN '.$cicilan_ke.' / '.$jml_cicilan)?>" disabled>
                        </div> 
                    </div>
                      
                </div>
				<div class="col-md-6"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Nama Pasien</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="{namapasien}" disabled>
                        </div> 
                    </div>
                      
                </div>
				
				
				<div class="col-md-6"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Jaminan</label> 
                        <div class="col-md-9"> 
                            <input class="form-control" value="<?=($jaminan)?>" disabled>
                        </div> 
                    </div>
                      
                </div>
				<div class="col-md-6" style="margin-bottom: 5px;"> 
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label class="col-md-3 control-label">Nominal Pembayaran</label> 
                        <div class="col-md-4"> 
                            <input class="form-control decimal" id="nominal_sisa" name="nominal_sisa" value="<?=($nominal_tagihan)?>" disabled>
                        </div>
						<div class="col-md-4"> 
                            <button class="btn btn-danger  btn_pembayaran" id="btn_pembayaran" <?=$disabel?>  type="button"><i class="fa fa-plus"></i> Add Pembayaran</button>
                        </div> 
                    </div>
                   
                    
                </div>
            <?php echo form_close(); ?>
        </div>
		<hr>
		<div class="row">
			<div class="col-sm-12">
				<div class="progress progress-mini">
					<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
				</div>
				<h5 style="margin-bottom: 10px;">Pembayaran</h5>
				<div class="control-group">
					<div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0;">
						<table id="manage_tabel_pembayaran" class="table table-striped table-bordered" style="margin-bottom: 0;">
							<thead>
								<tr>
									<th style="width: 5%;">#</th>
									<th style="width: 25%;">Jenis Pembayaran</th>
									<th style="width: 35%;">Keterangan</th>
									<th style="width: 10%;">Nominal</th>
									<th style="width: 10%;">Action</th>

								</tr>

							</thead>
							<input type="hidden" id="rowindex">
							<input type="hidden" id="nomor">
							<tbody>
								
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
		</div>
		
        <div class="form-group" style="margin-top: 20px;"></div>
    </div>
</div>
<form class="form-horizontal" action="base_forms_premade.html" method="post" onsubmit="return false;">
                          
 
<div class="modal fade in black-overlay" id="modal_pembayaran" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Pembayaran</h3>
				</div>
				<div class="block-content block-content-narrow">
				<div class="form-group">
					<label class="col-md-3 control-label" for="nama">Sisa Rp. </label>
					<div class="col-md-7">
						<input  readonly type="text" class="form-control decimal" id="sisa_modal" placeholder="Sisa" name="sisa_modal" required="" aria-required="true">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-3 control-label" for="idmetode">Metode</label>
					<div class="col-md-7">
						<select name="idmetode" id="idmetode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#">Pilih Metode</option>
							<option value="1">Tunai</option>
							<option value="2">Transfer</option>
						</select>
					</div>
				</div>
				<div class="form-group" id="div_bank">
					<label class="col-md-3 control-label" for="idbank">Bank</label>
					<div class="col-md-7">
						<select name="idbank" id="idbank" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#">Pilih Bank</option>
							<?php foreach  ($list_bank as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama.' - '.$row->atasnama?></option>
							<?php }?>

						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-3 control-label" for="nama">Nominal Rp. </label>
					<div class="col-md-7">
						<input  type="text" class="form-control decimal" id="nominal" placeholder="Nominal" name="nominal" required="" aria-required="true">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-3 control-label" for="nama">Keterangan </label>
					<div class="col-md-7">
						<input type="text" class="form-control" id="ket" placeholder="Keterangan" name="ket" required="" aria-required="true">
						<input type="hidden" class="form-control" readonly id="iddet" placeholder="iddet" name="iddet" >
					</div>
				</div>

				<div class="modal-footer">
					<button class="btn btn-sm btn-primary" id="btn_add_bayar" type="button"><i class="fa fa-check"></i> Add</button>
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Exit</button>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
</form>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">


    var table;
	var myDropzone 
    $(document).ready(function(){
			// $('.decimal').number(true, 2);
			$(".decimal").number(true,2,'.',',');
			$(".number").number(true,0,'.',',');
			// $('.number').number(true, 2);
		load_bayar();
    });
	//btn_pembayaran
	function load_bayar(){
		$("#cover-spin").show();
		var id=$("#id").val();
		var disabel=$("#disabel").val();
		$('#manage_tabel_pembayaran tbody').empty();
		$.ajax({
			url: '{site_url}tpiutang_tt_pembayaran/load_bayar/',
			dataType: "json",
			type: "POST",
			data: {id: id,disabel: disabel},
			success: function(data) {
				// console.log(data.tabel);
				$('#manage_tabel_pembayaran').append(data.tabel);
				$(".decimal").number(true,2,'.',',');
				$("#cover-spin").hide();
				hitung_sisa();
				$("#iddet").val('');
				// $(".tgl").datepicker();
				// $(".simpan").hide();
			}
		});
	}
	$(document).on("click","#btn_pembayaran",function(){
		var bayar=$("#bayar_rp").val();
		var nominal=$("#nominal_sisa").val();
		var sisa=nominal-bayar;
		$("#sisa_modal").val(sisa);
		$("#idmetode").val('#').trigger('change');
		$("#nominal").val(sisa);
		$("#modal_pembayaran").modal('show');
		
	});
	$(document).on("change","#idmetode",function(){
		if ($(this).val()=='1'){
			$("#div_bank").hide();
		}else{
			$("#div_bank").show();
			
		}
		
	});
	$(document).on("keyup","#nominal",function(){
		var sisa=$("#sisa_modal").val();
		var nominal=$("#nominal").val();
		if (nominal > sisa){
			$("#nominal").val(sisa);
		}
		
	});
	function hitung_sisa(){
		var bayar=$("#bayar_rp").val();
		var nominal=$("#nominal_sisa").val();
		var sisa=nominal-bayar;
		$("#sisa_rp").val(sisa);
	}
	$(document).on("click","#btn_add_bayar",function(){
		if (validate_add()==false){
			return false;
		}
		var iddet=$("#iddet").val();
		var piutang_id=$("#id").val();
		var idmetode=$("#idmetode").val();
		var idbank=$("#idbank").val();
		var nominal=$("#nominal").val();
		var ket=$("#ket").val();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}tpiutang_tt_pembayaran/save_bayar/',
			dataType: "json",
			type: 'POST',
			data: {
				piutang_id: piutang_id,
				idmetode: idmetode,
				idbank: idbank,
				nominal: nominal,
				ket: ket,
				iddet: iddet,
				},
			success: function(data) {
				console.log(data);
				$('#modal_pembayaran').modal('hide');
				$.toaster({priority : 'success', title : 'Succes!', message : ' Proses Pembayaran'});
				// table.ajax.reload( null, false ); 
				load_bayar();
				$("#cover-spin").hide();
			}
		});
	});
	
	function validate_add()
	{
		// alert(parseFloat($("#nominal").val()));
		if ($("#idmetode").val()=='#'){
			sweetAlert("Maaf...", "Cara Bayar Harus diisi", "error");
			return false;
		}
		
		if ($("#idbank").val()=='#' && $("#idmetode").val()=='2'){
			sweetAlert("Maaf...", "Bank harus diisi diisi", "error");
			return false;
		}
		if ($("#nominal").val()=='' || parseFloat($("#nominal").val())==0){
			sweetAlert("Maaf...", "Nominal harus diisi diisi", "error");
			return false;
		}
		
		return true;
	}
	$(document).on("click", ".hapus", function() {
		var id=$(this).closest('tr').find(".iddet").val();
		// alert(id);
		swal({
			title: "Anda Yakin ?",
			text : "Akan Menghapus Pembayaran ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			table = $('#index_list').DataTable()	
				$.ajax({
					url: '{site_url}tpiutang_tt_pembayaran/hapus',
					type: 'POST',
					data: {id: id},
					complete: function(data) {
						$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi'});
						load_bayar();
					}
			});
		});
	});
	$(document).on("click", ".edit", function() {
		var id=$(this).closest('tr').find(".iddet").val();
		$("#iddet").val(id);
		$("#modal_pembayaran").modal('show');
		var sisa=0;
		$.ajax({
				url: '{site_url}tpiutang_tt_pembayaran/edit',
				dataType: "json",
				type: 'POST',
				data: {id: id},
				success: function(data) {
					console.log(data);
					// alert(data.idmetode);
					$("#idmetode").val(data.idmetode).trigger('change');
					$("#idbank").val(data.idbank).trigger('change');
					$("#nominal").val(data.nominal);
					sisa=data.nominal + parseFloat($("#sisa_rp").val());
					$("#ket").val(data.ket);
					$("#sisa_modal").val(sisa);
					// $.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi'});
					// load_bayar();
				}
		});
	});
</script>