
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_1 Surat <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_1_ina" id="paragraf_1_ina"><?=$paragraf_1_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_1 Surat <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_1_eng" id="paragraf_1_eng"><?=$paragraf_1_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Nama Pasien <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="nama_pasien_ina" id="nama_pasien_ina"><?=$nama_pasien_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Nama Pasien <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="nama_pasien_eng" id="nama_pasien_eng"><?=$nama_pasien_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Alamat <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="alamat_ina" id="alamat_ina"><?=$alamat_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Alamat <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="alamat_eng" id="alamat_eng"><?=$alamat_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Pekerjaan <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="pekerjaan_ina" id="pekerjaan_ina"><?=$pekerjaan_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Pekerjaan <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="pekerjaan_eng" id="pekerjaan_eng"><?=$pekerjaan_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="tb_ina">Tinggi Badan <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="tb_ina" id="tb_ina"><?=$tb_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="tb_eng">Tinggi Badan <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="tb_eng" id="tb_eng"><?=$tb_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="satuan_tb_ina">Satuan Tinggi Badan <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="satuan_tb_ina" id="satuan_tb_ina"><?=$satuan_tb_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="satuan_tb_eng">Satuan Tinggi Badan <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="satuan_tb_eng" id="satuan_tb_eng"><?=$satuan_tb_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="bb_ina">Berat Badan <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="bb_ina" id="bb_ina"><?=$bb_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="bb_eng">Berat Badan <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="bb_eng" id="bb_eng"><?=$bb_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="satuan_bb_ina">Satuan Berat Badan <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="satuan_bb_ina" id="satuan_bb_ina"><?=$satuan_bb_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="satuan_bb_eng">Satuan Berat Badan <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="satuan_bb_eng" id="satuan_bb_eng"><?=$satuan_bb_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="tekanan_darah_ina">Tekanan Darah <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="tekanan_darah_ina" id="tekanan_darah_ina"><?=$tekanan_darah_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="tekanan_darah_eng">Tekanan Darah <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="tekanan_darah_eng" id="tekanan_darah_eng"><?=$tekanan_darah_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="satuan_tekanan_darah_ina">Satuan Tekanan Darah <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="satuan_tekanan_darah_ina" id="satuan_tekanan_darah_ina"><?=$satuan_tekanan_darah_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="satuan_tekanan_darah_eng">Satuan Tekanan Darah <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="satuan_tekanan_darah_eng" id="satuan_tekanan_darah_eng"><?=$satuan_tekanan_darah_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="buta_ina">Buta Warna <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="buta_ina" id="buta_ina"><?=$buta_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="buta_eng">Buta Warna <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="buta_eng" id="buta_eng"><?=$buta_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="keadaan_ina">Keadaan Pemeriksaan <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="keadaan_ina" id="keadaan_ina"><?=$keadaan_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="keadaan_eng">Keadaan Pemeriksaan <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="keadaan_eng" id="keadaan_eng"><?=$keadaan_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="informasi_lain_ina">Informasi Lainnya<span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="informasi_lain_ina" id="informasi_lain_ina"><?=$informasi_lain_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="informasi_lain_eng">Informasi Lainnya<span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="informasi_lain_eng" id="informasi_lain_eng"><?=$informasi_lain_eng?></textarea>
		</div>
	</div>
</div>


<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_2 Surat <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_2_ina" id="paragraf_2_ina"><?=$paragraf_2_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_2 Surat <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_2_eng" id="paragraf_2_eng"><?=$paragraf_2_eng?></textarea>
		</div>
	</div>
</div>
