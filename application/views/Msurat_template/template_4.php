
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Kepada Yth <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="kepada_ina" id="kepada_ina"><?=$kepada_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Kepada Yth <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="kepada_eng" id="kepada_eng"><?=$kepada_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="up">UP Dokter <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="up_ina" id="up_ina"><?=$up_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="up">UP Dokter <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="up_eng" id="up_eng"><?=$up_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="di">Di <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="di_ina" id="di_ina"><?=$di_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="di">Di <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="di_eng" id="di_eng"><?=$di_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_1 Surat <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_1_ina" id="paragraf_1_ina"><?=$paragraf_1_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_1 Surat <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_1_eng" id="paragraf_1_eng"><?=$paragraf_1_eng?></textarea>
		</div>
	</div>
</div>

<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_2 Surat <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_2_ina" id="paragraf_2_ina"><?=$paragraf_2_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_2 Surat <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_2_eng" id="paragraf_2_eng"><?=$paragraf_2_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Nama Pasien & No Rekam Medis<span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="nama_pasien_ina" id="nama_pasien_ina"><?=$nama_pasien_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Nama Pasien & No Rekam Medis <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="nama_pasien_eng" id="nama_pasien_eng"><?=$nama_pasien_eng?></textarea>
		</div>
	</div>
</div>

<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Tanggal Lahir & Umur <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="tl_ina" id="tl_ina"><?=$tl_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Tanggal Lahir & Umur <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="tl_eng" id="tl_eng"><?=$tl_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="diagnosa_ina">Dengan Diagnosa<span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="diagnosa_ina" id="diagnosa_ina"><?=$diagnosa_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="diagnosa_eng">Dengan Diagnosa<span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="diagnosa_eng" id="diagnosa_eng"><?=$diagnosa_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="tindakan_ina">Tindakan Yang sudah dilakukan <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="tindakan_ina" id="tindakan_ina"><?=$tindakan_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="tindakan_eng">Tindakan Yang sudah dilakukan <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="tindakan_eng" id="tindakan_eng"><?=$tindakan_eng?></textarea>
		</div>
	</div>
</div>

<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_3_ina">Paragraf 3 <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_3_ina" id="paragraf_3_ina"><?=$paragraf_3_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_3_eng">Paragraf 3 <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_3_eng" id="paragraf_3_eng"><?=$paragraf_3_eng?></textarea>
		</div>
	</div>
</div>

