

<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_1 Surat  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_1_ina" id="paragraf_1_ina"><?=$paragraf_1_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_1 Surat  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_1_eng" id="paragraf_1_eng"><?=$paragraf_1_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_2_ina">Paragraf 2  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_2_ina" id="paragraf_2_ina"><?=$paragraf_2_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_2_eng">Paragraf 2  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_2_eng" id="paragraf_2_eng"><?=$paragraf_2_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Nama Pasien & No Rekam Medis <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="nama_pasien_ina" id="nama_pasien_ina"><?=$nama_pasien_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Nama Pasien & No Rekam Medis  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="nama_pasien_eng" id="nama_pasien_eng"><?=$nama_pasien_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Alamat Pasien <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="alamat_pasien_ina" id="alamat_pasien_ina"><?=$alamat_pasien_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Alamat Pasien <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="alamat_pasien_eng" id="alamat_pasien_eng"><?=$alamat_pasien_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="pelayanan_ina">Telah Mendapatkan Pelayanan <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="pelayanan_ina" id="pelayanan_ina"><?=$pelayanan_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="pelayanan_eng">Diagnosa <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="pelayanan_eng" id="pelayanan_eng"><?=$pelayanan_eng?></textarea>
		</div>
	</div>
</div>

<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="tanggal_tindakan_ina">Tanggal Tindakan <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="tanggal_tindakan_ina" id="tanggal_tindakan_ina"><?=$tanggal_tindakan_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="tanggal_tindakan_eng">Tanggal Tindakan <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="tanggal_tindakan_eng" id="tanggal_tindakan_eng"><?=$tanggal_tindakan_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_3_ina">Dengan Ringkasan <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_3_ina" id="paragraf_3_ina"><?=$paragraf_3_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_3_eng">Dengan Ringkasan <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_3_eng" id="paragraf_3_eng"><?=$paragraf_3_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="riwayat_ina">Riwayat Penyakit <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="riwayat_ina" id="riwayat_ina"><?=$riwayat_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="riwayat_eng">Riwayat Penyakit <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="riwayat_eng" id="riwayat_eng"><?=$riwayat_eng?></textarea>
		</div>
	</div>
</div>


<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="penunjang_ina">Pemeriksaan Penunjang <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="penunjang_ina" id="penunjang_ina"><?=$penunjang_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="penunjang_eng">Pemeriksaan Penunjang <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="penunjang_eng" id="penunjang_eng"><?=$penunjang_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="dibawa_ina">Diagnosa Akhir <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="diagnosa_akhir_ina" id="diagnosa_akhir_ina"><?=$diagnosa_akhir_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="diagnosa_akhir_eng">Diagnosa Akhir <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="diagnosa_akhir_eng" id="diagnosa_akhir_eng"><?=$diagnosa_akhir_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="terapi_ina">Terapi <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="terapi_ina" id="terapi_ina"><?=$terapi_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="terapi_eng">Terapi <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="terapi_eng" id="terapi_eng"><?=$terapi_eng?></textarea>
		</div>
	</div>
</div>

<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="tindakan_ina">Tindakan <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="tindakan_ina" id="tindakan_ina"><?=$tindakan_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="tindakan_eng">Tindakan <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="tindakan_eng" id="tindakan_eng"><?=$tindakan_eng?></textarea>
		</div>
	</div>
</div>

<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="keadaan_ina">Keadaan Sekarang  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="keadaan_ina" id="keadaan_ina"><?=$keadaan_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="keadaan_eng">Keadaan Sekarang  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="keadaan_eng" id="keadaan_eng"><?=$keadaan_eng?></textarea>
		</div>
	</div>
</div>


<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="dokter_ina">Dokter <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summerdokter" name="dokter_ina" id="dokter_ina"><?=$dokter_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="dokter_eng">Dokter <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summerdokter" name="dokter_eng" id="dokter_eng"><?=$dokter_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_4_ina">Paragraf 4 <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_4_ina" id="paragraf_4_ina"><?=$paragraf_4_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_4_eng">Paragraf 4 <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_4_eng" id="paragraf_4_eng"><?=$paragraf_4_eng?></textarea>
		</div>
	</div>
</div>

