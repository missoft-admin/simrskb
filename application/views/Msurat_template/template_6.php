
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Kepada Yth <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="kepada_ina" id="kepada_ina"><?=$kepada_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Kepada Yth <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="kepada_eng" id="kepada_eng"><?=$kepada_eng?></textarea>
		</div>
	</div>
</div>

<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_1 Surat <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_1_ina" id="paragraf_1_ina"><?=$paragraf_1_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_1 Surat <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_1_eng" id="paragraf_1_eng"><?=$paragraf_1_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Nama Pasien & No Rekam Medis<span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="nama_pasien_ina" id="nama_pasien_ina"><?=$nama_pasien_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Nama Pasien & No Rekam Medis <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="nama_pasien_eng" id="nama_pasien_eng"><?=$nama_pasien_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="mohon_ina">Mohon Dibuatkan<span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="mohon_ina" id="mohon_ina"><?=$mohon_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="mohon_eng">Mohon Dibuatkan <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="mohon_eng" id="mohon_eng"><?=$mohon_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="detail_ina">Detail <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="detail_ina" id="detail_ina"><?=$detail_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="detail_eng">Detail <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="detail_eng" id="detail_eng"><?=$detail_eng?></textarea>
		</div>
	</div>
</div>

<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="diagnosa_ina">Dengan Diagnosa<span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="diagnosa_ina" id="diagnosa_ina"><?=$diagnosa_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="diagnosa_eng">Dengan Diagnosa<span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="diagnosa_eng" id="diagnosa_eng"><?=$diagnosa_eng?></textarea>
		</div>
	</div>
</div>

<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_2_ina">Paragraf 2 <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_2_ina" id="paragraf_2_ina"><?=$paragraf_2_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_2_eng">Paragraf 2 <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_2_eng" id="paragraf_2_eng"><?=$paragraf_2_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="dokter_ina">Dokter <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="dokter_ina" id="dokter_ina"><?=$dokter_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="dokter_eng">Dokter <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="dokter_eng" id="dokter_eng"><?=$dokter_eng?></textarea>
		</div>
	</div>
</div>
