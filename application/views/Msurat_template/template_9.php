

<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_1 Surat  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_1_ina" id="paragraf_1_ina"><?=$paragraf_1_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_1 Surat  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_1_eng" id="paragraf_1_eng"><?=$paragraf_1_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_2_ina">Paragraf 2  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_2_ina" id="paragraf_2_ina"><?=$paragraf_2_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_2_eng">Paragraf 2  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_2_eng" id="paragraf_2_eng"><?=$paragraf_2_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Nama Pasien & No Rekam Medis <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="nama_pasien_ina" id="nama_pasien_ina"><?=$nama_pasien_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Nama Pasien & No Rekam Medis  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="nama_pasien_eng" id="nama_pasien_eng"><?=$nama_pasien_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Alamat Pasien <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="alamat_pasien_ina" id="alamat_pasien_ina"><?=$alamat_pasien_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Alamat Pasien <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="alamat_pasien_eng" id="alamat_pasien_eng"><?=$alamat_pasien_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="pekerjaan_pasien">Pekerjaan Pasien <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="pekerjaan_pasien_ina" id="pekerjaan_pasien_ina"><?=$pekerjaan_pasien_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="pekerjaan_pasien">Pekerjaan Pasien <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="pekerjaan_pasien_eng" id="pekerjaan_pasien_eng"><?=$pekerjaan_pasien_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="oleh_ina">Oleh Karena Sakit <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="oleh_ina" id="oleh_ina"><?=$oleh_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="oleh_eng">Oleh Karena Sakit <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="oleh_eng" id="oleh_eng"><?=$oleh_eng?></textarea>
		</div>
	</div>
</div>

<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="tanggal_tindakan_ina">Sejak <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="tanggal_tindakan_ina" id="tanggal_tindakan_ina"><?=$tanggal_tindakan_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="tanggal_tindakan_eng">Sejak <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="tanggal_tindakan_eng" id="tanggal_tindakan_eng"><?=$tanggal_tindakan_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_3_ina">Keterangan Lainnya  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_3_ina" id="paragraf_3_ina"><?=$paragraf_3_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_3_eng">Keterangan Lainnya <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_3_eng" id="paragraf_3_eng"><?=$paragraf_3_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_3_ina">Keterangan Lainnya  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_3_ina" id="paragraf_3_ina"><?=$paragraf_3_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_3_eng">Keterangan Lainnya <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_3_eng" id="paragraf_3_eng"><?=$paragraf_3_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_4_ina">Paragraf Penutup <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_4_ina" id="paragraf_4_ina"><?=$paragraf_4_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_4_eng">Paragraf Penutup <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_4_eng" id="paragraf_4_eng"><?=$paragraf_4_eng?></textarea>
		</div>
	</div>
</div>

<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="dokter_ina">Dokter <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summerdokter" name="dokter_ina" id="dokter_ina"><?=$dokter_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="dokter_eng">Dokter <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summerdokter" name="dokter_eng" id="dokter_eng"><?=$dokter_eng?></textarea>
		</div>
	</div>
</div>

