
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_1 Surat <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_1_ina" id="paragraf_1_ina"><?=$paragraf_1_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_1 Surat <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_1_eng" id="paragraf_1_eng"><?=$paragraf_1_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Nama Pasien <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="nama_pasien_ina" id="nama_pasien_ina"><?=$nama_pasien_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Nama Pasien <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="nama_pasien_eng" id="nama_pasien_eng"><?=$nama_pasien_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Alamat <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="alamat_ina" id="alamat_ina"><?=$alamat_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Alamat <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="alamat_eng" id="alamat_eng"><?=$alamat_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Pekerjaan <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="pekerjaan_ina" id="pekerjaan_ina"><?=$pekerjaan_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Pekerjaan <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="pekerjaan_eng" id="pekerjaan_eng"><?=$pekerjaan_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Opsi <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="opsi_ina" id="opsi_ina"><?=$opsi_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Opsi <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="opsi_eng" id="opsi_eng"><?=$opsi_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Lama <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="lama_ina" id="lama_ina"><?=$lama_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Lama <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="lama_eng" id="lama_eng"><?=$lama_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">mulai <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="mulai_ina" id="mulai_ina"><?=$mulai_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">mulai <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="mulai_eng" id="mulai_eng"><?=$mulai_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">sampai <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="sampai_ina" id="sampai_ina"><?=$sampai_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">sampai <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="sampai_eng" id="sampai_eng"><?=$sampai_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_2 Surat <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_2_ina" id="paragraf_2_ina"><?=$paragraf_2_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_2 Surat <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_2_eng" id="paragraf_2_eng"><?=$paragraf_2_eng?></textarea>
		</div>
	</div>
</div>
