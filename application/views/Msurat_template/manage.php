<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}msurat_template" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content ">
		<?php echo form_open('msurat_template/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group" style="margin-bottom:10px">
				
				<input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="{id}" required="" aria-required="true">
				<div class="col-md-4">
					<div class="col-md-12">
					<label class=" control-label" for="jenis_surat">Jenis Surat</label>
						<select id="jenis_surat" name="jenis_surat" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="" <?=($jenis_surat==''?'selected':'')?>>>Pilih Opsi</option>
							<?foreach(list_variable_ref(411) as $row){?>
							<option value="<?=$row->id?>" <?=(($row->id==$jenis_surat)?'selected':'')?>><?=$row->nama?></option>
							<?}?>
							
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="col-md-12">
					<label class="control-label" for="isi_surat">Format Surat</label>
						<select id="isi_surat" name="isi_surat" class="js-select2 form-control opsi_change " style="width: 100%;" data-placeholder="Pilih Opsi" required>
							<option value="" <?=($isi_surat==''?'selected':'')?>>Silhkan Pilih</option>
							<option value="1" <?=($isi_surat=='1'?'selected':'')?>>Format 1</option>
							<option value="2" <?=($isi_surat=='2'?'selected':'')?>>Format 2</option>
							
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="col-md-12">
					<label class="control-label" for="isi_surat">Kode Surat</label>
					<input type="text" class="form-control" id="kode" placeholder="Kode" name="kode" value="{kode}" required="" aria-required="true">
					</div>
				</div>
			</div>
			<div class="form-group" style="margin-bottom:10px">
				<div class="col-md-6">
					<div class="col-md-12">
						<label class=" control-label" for="kode">Judul Surat (INA)</label>
						<input type="text" class="form-control" id="judul_ina" placeholder="Judul Indonesia" name="judul_ina" value="{judul_ina}"  aria-required="true">
					</div>
				</div>
				<div class="col-md-6">
				<div class="col-md-12">
				<label class=" control-label" for="kode">Judul Surat (ENGLISH)</label>
					<input type="text" class="form-control" id="judul_eng" placeholder="Title" name="judul_eng" value="{judul_eng}"  aria-required="true">
				</div>
				</div>
			</div>
			<?
			
			if ($id=='1'){
				$this->load->view('Msurat_template/template_1');
			}
			if ($id=='2'){
				$this->load->view('Msurat_template/template_2');
			}
			if ($id=='3'){
				$this->load->view('Msurat_template/template_3');
			}
			if ($id=='4'){
				$this->load->view('Msurat_template/template_4');
			}
			if ($id=='5'){
				$this->load->view('Msurat_template/template_5');
			}
			if ($id=='6'){
				$this->load->view('Msurat_template/template_6');
			}
			if ($id=='7'){
				$this->load->view('Msurat_template/template_7');
			}
			if ($id=='8'){
				$this->load->view('Msurat_template/template_8');
			}
			if ($id=='9'){
				$this->load->view('Msurat_template/template_9');
			}
			
			?>
			<div class="form-group" style="margin-bottom:10px">
				<div class="col-md-6">
					<div class="col-md-12">
					<label class="control-label" for="kode">Footer Surat (INA)</label>
						<textarea class="form-control summernote" name="footer_ina" id="footer_ina"><?=$footer_ina?></textarea>
					</div>
				</div>
				<div class="col-md-6">
				<div class="col-md-12">
				<label class="control-label" for="kode">Footer Surat (ENGLISH)</label>
					<textarea class="form-control summernote" name="footer_eng" id="footer_eng"><?=$footer_eng?></textarea>
				</div>
				</div>
			</div>
			
		
			<div class="form-group" style="margin-top:50px">
				<div class="col-md-12">
				<div class="col-md-12">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}msurat_template" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
				</div>
			</div>
			<?php echo form_close() ?>
			
			
			
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
var myDropzone 
$(document).ready(function(){	
	$(".number").number(true,0,'.',',');
	// $('.summernote').summernote({
		  // height: 100,   //set editable area's height
		  // codemirror: { // codemirror options
			// theme: 'monokai'
		  // }	
		// })

	 // CKEDITOR.disableAutoInline = true;


        // // Init full text editor
        // if (jQuery('#js-ckeditor').length) {
            // CKEDITOR.replace('js-ckeditor');
        // }
		 // jQuery('.js-summernote').summernote({
            // height: 950,
            // minHeight: null,
            // maxHeight: null
        // });
	
})	


</script>