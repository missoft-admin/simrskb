
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_1 Surat  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_1_ina" id="paragraf_1_ina"><?=$paragraf_1_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_1 Surat  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_1_eng" id="paragraf_1_eng"><?=$paragraf_1_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Nama Pasien  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="nama_pasien_ina" id="nama_pasien_ina"><?=$nama_pasien_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Nama Pasien  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="nama_pasien_eng" id="nama_pasien_eng"><?=$nama_pasien_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Alamat  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="alamat_ina" id="alamat_ina"><?=$alamat_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Alamat  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="alamat_eng" id="alamat_eng"><?=$alamat_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Tanggal Lahir <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="tl_ina" id="tl_ina"><?=$tl_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Tanggal Lahir <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="tl_eng" id="tl_eng"><?=$tl_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="jk_ina">Jenis Kelamin <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="jk_ina" id="jk_ina"><?=$jk_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="jk_eng">Jenis Kelamin <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="jk_eng" id="jk_eng"><?=$jk_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="pekerjaan_ina">Pekerjaan <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="pekerjaan_ina" id="pekerjaan_ina"><?=$pekerjaan_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="pekerjaan_eng">Pekerjaan <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="pekerjaan_eng" id="pekerjaan_eng"><?=$pekerjaan_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="telah_ina">Telah Mendapatkan <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="telah_ina" id="telah_ina"><?=$telah_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="telah_eng">Telah Mendapatkan <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="telah_eng" id="telah_eng"><?=$telah_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="diagnosa_ina">Dengan Diagnosa <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="diagnosa_ina" id="diagnosa_ina"><?=$diagnosa_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="diagnosa_eng">Dengan Diagnosa <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="diagnosa_eng" id="diagnosa_eng"><?=$diagnosa_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="diharuskan_ina">Adapun pasien Diharuskan  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="diharuskan_ina" id="diharuskan_ina"><?=$diharuskan_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="diharuskan_eng">Adapun pasien Diharuskan  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="diharuskan_eng" id="diharuskan_eng"><?=$diharuskan_eng?></textarea>
		</div>
	</div>
</div>

<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_2 Surat  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_2_ina" id="paragraf_2_ina"><?=$paragraf_2_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_2 Surat  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_2_eng" id="paragraf_2_eng"><?=$paragraf_2_eng?></textarea>
		</div>
	</div>
</div>
