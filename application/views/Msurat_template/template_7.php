

<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_1 Surat  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_1_ina" id="paragraf_1_ina"><?=$paragraf_1_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">paragraf_1 Surat  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_1_eng" id="paragraf_1_eng"><?=$paragraf_1_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Nama Pasien & No Rekam Medis <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="nama_pasien_ina" id="nama_pasien_ina"><?=$nama_pasien_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Nama Pasien & No Rekam Medis  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="nama_pasien_eng" id="nama_pasien_eng"><?=$nama_pasien_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Alamat Pasien <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="alamat_pasien_ina" id="alamat_pasien_ina"><?=$alamat_pasien_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="kode">Alamat Pasien <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="alamat_pasien_eng" id="alamat_pasien_eng"><?=$alamat_pasien_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_2_ina">Paragraf 2  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_2_ina" id="paragraf_2_ina"><?=$paragraf_2_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_2_eng">Paragraf 2  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_2_eng" id="paragraf_2_eng"><?=$paragraf_2_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="dari_ina">Dari Tanggal <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="dari_ina" id="dari_ina"><?=$dari_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="dari_eng">Dari Tanggal  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="dari_eng" id="dari_eng"><?=$dari_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="diagnosa_ina">Diagnosa <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="diagnosa_ina" id="diagnosa_ina"><?=$diagnosa_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="diagnosa_eng">Diagnosa <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="diagnosa_eng" id="diagnosa_eng"><?=$diagnosa_eng?></textarea>
		</div>
	</div>
</div>

<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="tindakan_ina">Tindakan <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="tindakan_ina" id="tindakan_ina"><?=$tindakan_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="tindakan_eng">Tindakan <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="tindakan_eng" id="tindakan_eng"><?=$tindakan_eng?></textarea>
		</div>
	</div>
</div>


<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="tanggal_tindakan_ina">Tanggal Tindakan <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="tanggal_tindakan_ina" id="tanggal_tindakan_ina"><?=$tanggal_tindakan_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="tanggal_tindakan_eng">Tanggal Tindakan <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="tanggal_tindakan_eng" id="tanggal_tindakan_eng"><?=$tanggal_tindakan_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_3_ina">Paragraf 3 <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_3_ina" id="paragraf_3_ina"><?=$paragraf_3_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_3_eng">Paragraf 3 <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_3_eng" id="paragraf_3_eng"><?=$paragraf_3_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="bagian_ina">Nama Bagian Tubuh <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="bagian_ina" id="bagian_ina"><?=$bagian_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="bagian_eng">Nama Bagian Tubuh <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="bagian_eng" id="bagian_eng"><?=$bagian_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="serta_ina">Serta Akan Dibawa <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="serta_ina" id="serta_ina"><?=$serta_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="serta_eng">Serta Akan Dibawa <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="serta_eng" id="serta_eng"><?=$serta_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="dibawa_ina">Dibawa Oleh <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="dibawa_ina" id="dibawa_ina"><?=$dibawa_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="dibawa_eng">Dibawa Oleh <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="dibawa_eng" id="dibawa_eng"><?=$dibawa_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="nama_ina">Nama  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="nama_ina" id="nama_ina"><?=$nama_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="nama_eng">Nama  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="nama_eng" id="nama_eng"><?=$nama_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="alamat_ina">Alamat  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="alamat_ina" id="alamat_ina"><?=$alamat_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="alamat_eng">Alamat  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="alamat_eng" id="alamat_eng"><?=$alamat_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="nik_ina">NIK  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="nik_ina" id="nik_ina"><?=$nik_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="nik_eng">NIK  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="nik_eng" id="nik_eng"><?=$nik_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="hubungan_ina">Hubungan <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="hubungan_ina" id="hubungan_ina"><?=$hubungan_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="hubungan_eng">Hubungan <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="hubungan_eng" id="hubungan_eng"><?=$hubungan_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_4_ina">Paragraf 4 <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="paragraf_4_ina" id="paragraf_4_ina"><?=$paragraf_4_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="paragraf_4_eng">Paragraf 4 <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="paragraf_4_eng" id="paragraf_4_eng"><?=$paragraf_4_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="note_ina">Note <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote" name="note_ina" id="note_ina"><?=$note_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="note_eng">Note <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="note_eng" id="note_eng"><?=$note_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="mengetahui_ina">Mengetahui  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote " name="mengetahui_ina" id="mengetahui_ina"><?=$mengetahui_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="mengetahui_eng">Mengetahui  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="mengetahui_eng" id="mengetahui_eng"><?=$mengetahui_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="pemohon_ina">Pemohon  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote " name="pemohon_ina" id="pemohon_ina"><?=$pemohon_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="pemohon_eng">Pemohon  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="pemohon_eng" id="pemohon_eng"><?=$pemohon_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="saksi_kel_ina">Saksi Keluarga  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote " name="saksi_kel_ina" id="saksi_kel_ina"><?=$saksi_kel_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="saksi_kel_eng">Saksi Keluarga  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="saksi_kel_eng" id="saksi_kel_eng"><?=$saksi_kel_eng?></textarea>
		</div>
	</div>
</div>
<div class="form-group" style="margin-bottom:10px">
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="saksi_rs_ina">Saksi RS  <span style="color:blue;">(INA)</span></label>
			<textarea class="form-control summernote " name="saksi_rs_ina" id="saksi_rs_ina"><?=$saksi_rs_ina?></textarea>
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-md-12">
		<label class="control-label" for="saksi_rs_eng">Saksi RS  <span style="color:red;">(ENGLISH)</span></label>
			<textarea class="form-control summernote" name="saksi_rs_eng" id="saksi_rs_eng"><?=$saksi_rs_eng?></textarea>
		</div>
	</div>
</div>
