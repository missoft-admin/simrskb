<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('129'))){ ?>
<div class="block">
	<div class="block-header">
	
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:20px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
			
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Jenis Transaksi</label>
                    <div class="col-md-8">
                       <select id="jenis_trx" name="jenis_trx" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua Jenis Transaksi -</option>
							<option value="DEPOSIT (RI)" >DEPOSIT (RI)</option>
							<option value="PELUNASAN (RI)" >PELUNASAN (RI)</option>							
							<option value="PENDAPATAN (RJ)" >PENDAPATAN (RJ)</option>							
							<option value="REFUND (RJ)" >REFUND (RJ)</option>							
							<option value="REFUND (RI)" >REFUND (RI)</option>							
							<option value="PENDAPATAN LAIN" >PENDAPATAN LAIN</option>							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Tipe</label>
                    <div class="col-md-8">
                       <select id="idmetode" name="idmetode" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua Tipe -</option>
							<option value="1">TUNAI</option>
							<option value="2">NON TUNAI</option>							
													
						</select>
                    </div>
                </div>
				
            </div>
			<div class="col-md-6">
				
				 <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Transaksi</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggal_trx1" name="tanggal_trx1" placeholder="From" value="{tanggal_trx1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_trx2" name="tanggal_trx2" placeholder="To" value="{tanggal_trx2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">User</label>
                    <div class="col-md-8">
                       <select id="user_id" name="user_id" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua User -</option>
							<?foreach($list_user as $row){?>
								<option value="<?=$row->id?>"><?=$row->name?></option>
							<?}?>
						</select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
                
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_index">
			<thead>
				<tr>
					<th>#</th>
					<th>Tanggal</th>					
					<th>User</th>
					<th>Jenis Transaksi</th>
					<th>Tipe</th>
					<th>Keterangan</th>
					<th>Penerimaan Tunai</th>
					<th>Pengeluaran</th>
					<th>Bank</th>
					<th>Keterangan</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<div class="modal in" id="RincianDepositModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:80%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title titleRincian">RINCIAN DEPOSIT</h3>
				</div>
				<div class="block-content" style="margin-bottom: 10px;padding-top: 10px;">
					<div class="table-responsive">
					<table id="historyDeposit" class="table table-bordered table-striped" style="margin-top: 10px;">
						<thead>
							<tr>
								<th>Tanggal</th>
								<th>Metode</th>
								<th>Nominal</th>
								<th>Terima Dari</th>
								<th>User Input</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
							<tr style="background-color:white;">
								<td colspan="4" style="text-align:right;text-transform:uppercase;"><b>Total</b></td>
								<td id="depositTotal" style="font-weight:bold;"></td>
								<td></td>
							</tr>
						</tfoot>
					</table>
				</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-close"></i> Tutup</button>
				<a href="" target="_blank" id="historyPrintAll" class="btn btn-sm btn-success" ><i class="fa fa-print"></i> Print All</a>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="RincianPelunasanModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:80%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title titleRincian">RINCIAN PELUNASAN</h3>
				</div>
				<div class="block-content" style="margin-bottom: 10px;padding-top: 10px;">
					<div class="table-responsive">
					<table id="historyPelunasan" class="table table-bordered table-striped" style="margin-top: 10px;">
						<thead>
							<tr>
								<th style="width:15%" class="text-center">Tanggal</th>
								<th style="width:15%" class="text-center">Metode</th>
								<th style="width:35%" class="text-center">Keterangan</th>
								<th style="width:20%" class="text-center">Nominal</th>
								<th style="width:15%" class="text-center">Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
							<tr style="background-color:white;">
								<td colspan="3" style="text-align:right;text-transform:uppercase;"  class="text-right"><b>Total</b></td>
								<td id="PelunasanTotal" style="font-weight:bold;" class="text-right"></td>
								<td></td>
							</tr>
						</tfoot>
					</table>
				</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-close"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>

<?}?>


<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		// alert('sini');
		load_index();
		
	})	
	$("#btn_filter").click(function() {
		// alert('ID');
		table.destroy();
		load_index();
	});
	function getHistoryDeposit(idpendaftaran) {
		$("#RincianDepositModal").modal('show');
		$("#historyPrintAll").attr('href', '{site_url}trawatinap_tindakan/print_rincian_deposit/' + idpendaftaran);
		$('#historyDeposit tbody').empty();
		$("#depositTotal").html(0);
		$.ajax({
			url: '{site_url}trawatinap_tindakan/getHistoryDeposit/' + idpendaftaran,
			dataType: 'json',
			success: function(data) {
				
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);
					var metode = '';
					if(data[i].idmetodepembayaran == 1){
						metode = data[i].metodepembayaran;
					}else{
						metode = data[i].metodepembayaran + ' ( ' + data[i].namabank+ ' )';
					}
					var action = '';
					action = '<div class="btn-group">';
					action += '<a href="{site_url}trawatinap_tindakan/print_kwitansi_deposit/' + data[i].id + '" target="_blank" class="btn btn-sm btn-success">Print <i class="fa fa-print"></i></a>';
					action += '</div>';

					$('#historyDeposit tbody').append('<tr><td>' + data[i].tanggal + '</td><td>' + metode + '</td><td>' + $.number(data[i].nominal) + '</td><td>' + (data[i].terimadari ? data[i].terimadari : "-")  + '</td><td>' + data[i].namapetugas + '</td><td>' + action + '</td></tr>');
				}

				getTotalHistoryDeposit();
			}
		});
	}
	function getHistoryPelunasan(idpendaftaran) {
		$("#RincianPelunasanModal").modal('show');
		// $("#historyPrintAllPelunasan").attr('href', '{site_url}trawatinap_tindakan/print_rincian_pelunasan/' + idpendaftaran);
		$('#historyPelunasan tbody').empty();
		$("#PelunasanTotal").html(0);
		$.ajax({
			url: '{site_url}tmonitoring/getHistoryPelunasan/' + idpendaftaran,
			dataType: 'json',
			success: function(data) {
				$("#PelunasanTotal").html(data.total);
				$('#historyPelunasan tbody').append(data.tabel);
			}
		});
	}
	function getTotalHistoryDeposit()
	{
		var totalDeposit = 0;
		$('#historyDeposit tbody tr').each(function() {
			totalDeposit +=parseFloat($(this).find('td:eq(2)').text().replace(/,/g, ''));
		});

		$("#depositTotal").html($.number(totalDeposit));
	}
	// $(document).on("click",".verif",function(){
		// var table = $('#table_index').DataTable()
		// var tr = $(this).parents('tr')
		// var id = table.cell(tr,0).data()
		// // alert(id); return false;
		// swal({
			// title: "Apakah Anda Yakin ?",
			// text : "Verifikasi Setoran Kas?",
			// type : "success",
			// showCancelButton: true,
			// confirmButtonText: "Ya",
			// confirmButtonColor: "#d26a5c",
			// cancelButtonText: "Tidak Jadi",
		// }).then(function() {
			// $.ajax({
				// url: '{site_url}tmonitoring/verif/'+id,
				// type: 'POST',
				// complete: function() {
					// $.toaster({priority : 'success', title : 'Succes!', message : ' Setoran diselesaikan diverifikasi'});
					// $('#table_index').DataTable().ajax.reload( null, false );
				// }
			// });
		// });
		
		// return false;
	// });
	// $(document).on("click",".hapus",function(){
		// var table = $('#table_index').DataTable()
		// var tr = $(this).parents('tr')
		// var id = table.cell(tr,0).data()
		// // alert(id); return false;
		// swal({
			// title: "Apakah Anda Yakin ?",
			// text : "Hapus Setoran?",
			// type : "success",
			// showCancelButton: true,
			// confirmButtonText: "Ya",
			// confirmButtonColor: "#d26a5c",
			// cancelButtonText: "Tidak Jadi",
		// }).then(function() {
			// $.ajax({
				// url: '{site_url}tmonitoring/hapus/'+id,
				// type: 'POST',
				// complete: function() {
					// $.toaster({priority : 'success', title : 'Succes!', message : ' Setoran diselesaikan diverifikasi'});
					// $('#table_index').DataTable().ajax.reload( null, false );
				// }
			// });
		// });
		
		// return false;
	// });
	function load_index(){
		var jenis_trx=$("#jenis_trx").val();
		var idmetode=$("#idmetode").val();		
		var tanggal_trx1=$("#tanggal_trx1").val();
		var tanggal_trx2=$("#tanggal_trx2").val();
		var user_id=$("#user_id").val();
		table=$('#table_index').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tmonitoring/getIndex',
					type: "POST",
					dataType: 'json',
					data : {
						jenis_trx:jenis_trx,
						idmetode:idmetode,
						tanggal_trx1:tanggal_trx1,
						tanggal_trx2:tanggal_trx2,
						user_id:user_id,
						
					   }
				},
				"columnDefs": [
					{ "width": "5%", "targets": 0, "visible": false },
					{ "width": "8%", "targets": 1, "visible": true },
					{ "width": "10%", "targets": 2, "visible": true },
					{ "width": "10%", "targets": 3, "visible": true },
					{ "width": "10%", "targets": 4, "visible": true,"class":"text-center" },
					{ "width": "15%", "targets": 5, "visible": true },
					{ "width": "8%", "targets": 6, "visible": true,"class":"text-right" },
					{ "width": "8%", "targets": 7, "visible": true,"class":"text-right" },
					{ "width": "8%", "targets": 8, "visible": true,"class":"text-right" },
					{ "width": "10%", "targets": 9, "visible": true },
					{ "width": "15", "targets": 10, "visible": true },
				]
			});
	}
	
</script>
