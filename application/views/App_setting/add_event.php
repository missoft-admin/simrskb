<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block" >
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}app_setting/info/2" type="button" title="Kembali"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('app_setting/save_event', 'class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
        
		<div class="form-group">
			<label class="col-md-2 control-label" for="judul">Subject<span style="color:red;">*</span></label>
			<div class="col-md-10">
				<input tabindex="0" type="text" class="form-control" id="judul" placeholder="Subject  / Judul" name="judul" value="{judul}" required>
			</div>
		</div>
		
		
        <div class="form-group">
            <label class="col-md-2 control-label" for="example-hf-email"></label>
            <div class="col-md-7">

                <img class="" height="150px" width="300px" id="output_img" src="{upload_path}app_setting/<?=($gambar?$gambar:'no_image.png')?>" />
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="example-hf-email">Gambar</label>
            <div class="col-md-7">
                <div class="box">
                    <input type="file" id="gambar" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;"  name="gambar" value="{gambar}" accept="image/png, image/gif, image/jpg, image/jpeg" />
                    <label for="gambar"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                </div>
            </div>
        </div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="judul">Content<span style="color:red;">*</span></label>
			<div class="col-md-10">
				<textarea class="form-control js-summernote" name="isi" id="isi"><?=$isi?></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="judul">Tanggal Informasi<span style="color:red;">*</span></label>
			<div class="col-md-2">
				<div class="input-group date">
					<input class="js-datepicker form-control input"  type="text" id="date_start" name="date_start" value="<?=($date_start!=''?HumanDateShort($date_start):'')?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
			</div>
			<label class="col-md-2 control-label" for="judul">Batas Publish<span style="color:red;">*</span></label>
			<div class="col-md-2">
				<div class="input-group date">
					<input class="js-datepicker form-control input"  type="text" id="date_end" name="date_end" value="<?=($date_end!=''?HumanDateShort($date_end):'')?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
			</div>
		</div>
        <hr>
		<div class="form-group">
			<div class="col-md-12 col-md-offset-2">
			<?if($status!='3'){?>
				<button class="btn btn-success" name="btn_simpan" value="1" type="submit">Simpan Draft</button>
				<?php if (UserAccesForm($user_acces_form,array('1449'))){ ?>
				<button class="btn btn-primary" name="btn_simpan" value="2" type="submit">Simpan Publish</button>
				<?}?>
			<?}?>
				<a href="{base_url}app_setting/info/2" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
			</div>
		</div>
		<input type="hidden" id="id" name="id" value="{id}"/>
		<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">

$(document).ready(function(){
	
})	
function validate_final()
{
	var id=$("#id").val();
	
	
	if ($("#judul").val()==''){
		sweetAlert("Maaf...", "Tentukan Judul", "error");
		return false;
	}
	if (id==''){
		
		var vidFileLength = $("#gambar")[0].files.length;
		if(vidFileLength === 0){
			sweetAlert("Maaf...", "Tentukan Gambar", "error");
			return false;
		}
	}
	if ($("#isi").val()==''){
		sweetAlert("Maaf...", "Isi Content Informasi", "error");
		return false;
	}
	
	return true;
}

	function loadFile_img(event){
		// alert('load');
		var output_img = document.getElementById('output_img');
		output_img.src = URL.createObjectURL(event.target.files[0]);
	}
	
</script>