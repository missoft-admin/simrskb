<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<div class="block-header">
		
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content tab-content">
		<div class="col-md-12">
			<div class="form-group" style="margin-bottom: 15px;">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="index_poli">
							<thead>
								<tr>
									<th width="5%">No</th>
									<th width="30%">Tipe</th>
									<th width="50%">Poliklinik</th>
									<th width="15%">Action</th>										   
								</tr>
								<tr>
									<th width="5%">#</th>
									<th width="40%">
										<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="#" <?=($idtipe=='#'?'selected':'')?>>-Pilih Tipe-</option>
											<option value="1" <?=($idtipe=='1'?'selected':'')?>>Poliklinik</option>
											<option value="2" <?=($idtipe=='2'?'selected':'')?>>Instalasi Gawat Darurat</option>
											
										</select>
									</th>
									<th width="40%">
										<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="#" <?=($idpoli=='#'?'selected':'')?>>-Pilih Poliklinik-</option>
										</select>
									</th>
									<th width="15%">
										<?php if (UserAccesForm($user_acces_form,array('1471'))){ ?>
										<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_poli" name="btn_tambah_poli"><i class="fa fa-plus"></i> Tambah</button>
										<?}?>
									</th>										   
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
	
<div class="modal" id="modal_dokter" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Dokter Poliklinik <label id="nama_poli"></label></h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="col-md-12">                               
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_dokter">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="85%">Dokter</th>
											<th width="10%">Pilih</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
						<div class="col-md-12">                               
							<br>
						</div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="modal_setting" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Setting <label id="nama_poli_user"></label></h3>
				</div>
				<div class="block-content">
					<input type="hidden" value="" id="idpoliklinik">
					<div class="row">
						<div class="col-md-12">                               
							<div class="form-group" style="margin-bottom:10px" >
								
								<div class="col-md-12">
									<label for="tujuan_antrian_id">Tujuan Antrian</label>
									<select tabindex="8" id="tujuan_antrian_id"  class="js-select2 form-control js_92" style="width: 100%;">
									<option value="" >Pilih Opsi</option>	
										<?foreach(get_all('mtujuan',array('status'=>1)) as $row){?>
											<option value="<?=$row->id?>" ><?=$row->nama_tujuan?></option>
										<?}?>
									</select>
								</div>
								
							</div>
						</div>
						
						<div class="col-md-12">                               
							<br>
						</div>
                    </div>
					<div class="row">
						<div class="col-md-12">                               
							<div class="form-group" style="margin-bottom:10px" >
								
								<div class="col-md-12">
									<label for="iddokter">Dokter Penanggung Jawab</label>
									<select tabindex="8" id="iddokter"  class="js-select2 form-control js_92" style="width: 100%;">
									<option value="" >Pilih Opsi</option>	
										<?foreach(get_all('mdokter',array('status'=>1)) as $row){?>
											<option value="<?=$row->id?>" ><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
								
							</div>
						</div>
						
						<div class="col-md-12">                               
							<br>
						</div>
                    </div>
					<div class="row">
						<div class="col-md-12">                               
							<div class="form-group" style="margin-bottom:10px" >
								
								<div class="col-md-12">
									<label for="iduser">Dokter Penanggung Jawab</label>
									<select tabindex="8" id="iduser" name="iduser[]"  class="js-select2 form-control js_92" style="width: 100%;" multiple>
									
									</select>
								</div>
								
							</div>
						</div>
						
						<div class="col-md-12">                               
							<br>
						</div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-success" type="button" onclick="simpan_setting()" data-dismiss="modal">Simpan</button>
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
$(document).ready(function(){	
	load_poliklinik();
	// $("#modal_setting").modal('show');
})	
function simpan_setting(){
	
}
//PENDAFTARAN
$("#idtipe").change(function() {
	let idtipe=$("#idtipe").val();
	$.ajax({
		url: '{site_url}app_setting/list_poli_tujuan', 
		dataType: "JSON",
		method: "POST",
		data : {idtipe:idtipe},
		success: function(data) {
			$("#idpoli").empty();
			$("#idpoli").append(data);
		}
	});

});
function load_setting(id,nama_poli){
	$("#modal_setting").modal('show');
	$("#idpoliklinik").val(id);
	$("#nama_poli_user").text(nama_poli);
	$("#cover-spin").show();
	 $.ajax({
			url: '{site_url}app_setting/get_setting',
			type: 'POST',
			dataType: 'json',
			data: {idpoli: id},
			success: function(data) {
				if (data.iddokter){
					$("#iddokter").val(data.iddokter).trigger('change');
					$("#tujuan_antrian_id").val(data.tujuan_antrian_id).trigger('change');
					
				}else{
					
					$("#iddokter").val('').trigger('change');
					$("#tujuan_antrian_id").val('').trigger('change');
				}
				$("#iduser").empty();
				$("#iduser").append(data.list_user);
				$("#cover-spin").hide();
				
			}
		});
}
function simpan_setting(){
	let iddokter=$("#iddokter").val();
	let iduser=$("#iduser").val();
	let idpoli=$("#idpoliklinik").val();
	let tujuan_antrian_id=$("#tujuan_antrian_id").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/simpan_setting', 
		dataType: "JSON",
		method: "POST",
		data : {
			iddokter:iddokter,
			iduser:iduser,
			idpoli:idpoli,
			tujuan_antrian_id:tujuan_antrian_id,
		
		},
		success: function(data) {
			$.toaster({priority : 'success', title : 'Succes!', message : 'data berhasil disimpan'});
			$("#cover-spin").hide();
		}
	});

};

$("#btn_tambah_poli").click(function() {
	let idtipe=$("#idtipe").val();
	let idpoli=$("#idpoli").val();
	if ($("#idtipe").val()=='#'){
		sweetAlert("Maaf...", "Tentukan Tipe", "error");
		return false;
	}
	if ($("#idpoli").val()=='#'){
		sweetAlert("Maaf...", "Tentukan Poliklinik", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/simpan_poli_tujuan', 
		dataType: "JSON",
		method: "POST",
		data : {idpoli:idpoli},
		complete: function(data) {
			$("#idtipe").val('#').trigger('change');
			$('#index_poli').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});

function load_poliklinik(){
	$('#index_poli').DataTable().destroy();	
	table = $('#index_poli').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}app_setting/load_poliklinik_tujuan', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function load_dokter(id,nama_poli){
	$('#index_dokter').DataTable().destroy();	
	$("#modal_dokter").modal('show');
	$("#nama_poli").text(nama_poli);
	table = $('#index_dokter').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}app_setting/load_dokter', 
                type: "POST" ,
                dataType: 'json',
				data : {
						id:id
					   }
            }
        });
}

function hapus_poli_tujuan(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Poliklinik Tujuan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}app_setting/hapus_poli_tujuan',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_poli').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
function check_save_dokter(idpoli,iddokter,pilih){
	// alert('id : '+id+' Pilih :'+pilih);
	$("#cover-spin").show();
		
	 $.ajax({
			url: '{site_url}app_setting/check_save_dokter',
			type: 'POST',
			data: {idpoli: idpoli,iddokter:iddokter,pilih:pilih},
			complete: function() {
				$("#cover-spin").hide();
				$('#index_dokter').DataTable().ajax.reload( null, false ); 
				$('#index_poli').DataTable().ajax.reload( null, false ); 
				$.toaster({priority : 'success', title : 'Succes!', message : ' Update'});
				
			}
	});
}

</script>