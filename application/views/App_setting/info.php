<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1446'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info"  onclick="load_index_info()"><i class="fa fa-info-circle"></i> Informasi</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1453'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_event"  onclick="load_index_event()"><i class="fa fa-instagram"></i> Event</a>
		</li>
		<?}?>
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('1446'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_info">
			<?php if (UserAccesForm($user_acces_form,array('1447'))): ?>
			<div class="row">
				<div class="col-xs-6">
					
				</div>
				<div class="col-xs-6 text-right">
					<a class="btn btn-xs btn-danger  push-10 " href="{base_url}app_setting/add_info" ><i class="fa fa-plus"></i> Tambah Informasi</a>
					
				</div>
			</div>
			<?endif;?>
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tanggal Input</label>
						<div class="col-md-9">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" id="created_date1_info" name="created_date1_info" placeholder="From" value="{created_date1_info}"/>
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" id="created_date2_info" name="created_date2_info" placeholder="To" value="{created_date2_info}"/>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Status</label>
						<div class="col-md-9">
							<select id="status_info" name="status_info" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Status -</option>
								<option value="1">DRAFT</option>
								<option value="2">PUBLISH</option>
								<option value="3">SELESAI</option>
								
							</select>
						</div>
					</div>
					
				</div>
				<div class="col-md-6 push-10">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tanggal Informasi</label>
						<div class="col-md-9">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" id="date_start1_info" name="date_start1_info" placeholder="From" value="{date_start1_info}"/>
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" id="date_start2_info" name="date_start2_info" placeholder="To" value="{date_start2_info}"/>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label" for="statusstok"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="button" id="btn_filter_info" name="btn_filter_info" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						</div>
					</div>
						
				</div>
			</div>
			<?php echo form_close() ?>
			<div class="row   ">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="index_list_info">
							<thead>
								<tr>
									<th width="10%">No</th>
									<th width="10%">Tanggal Input</th>
									<th width="10%">Tanggal Informasi</th>
									<th width="10%">Judul</th>
									<th width="10%">Status</th>
									<th width="10%">Batas Publish</th>
									<th width="10%">Publish By</th>
									<th width="15%">Action</th>
								   
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1453'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_event">
			<?php if (UserAccesForm($user_acces_form,array('1454'))): ?>
			<div class="row">
				<div class="col-xs-6">
					
				</div>
				<div class="col-xs-6 text-right">
					<a class="btn btn-xs btn-danger  push-10 " href="{base_url}app_setting/add_event" ><i class="fa fa-plus"></i> Tambah Event</a>
					
				</div>
			</div>
			<?endif;?>
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tanggal Input</label>
						<div class="col-md-9">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" id="created_date1_event" name="created_date1_event" placeholder="From" value="{created_date1_event}"/>
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" id="created_date2_event" name="created_date2_event" placeholder="To" value="{created_date2_event}"/>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Status</label>
						<div class="col-md-9">
							<select id="status_event" name="status_event" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Status -</option>
								<option value="1">DRAFT</option>
								<option value="2">PUBLISH</option>
								<option value="3">SELESAI</option>
								
							</select>
						</div>
					</div>
					
				</div>
				<div class="col-md-6 push-10">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tanggal Event</label>
						<div class="col-md-9">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" id="date_start1_event" name="date_start1_event" placeholder="From" value="{date_start1_event}"/>
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" id="date_start2_event" name="date_start2_event" placeholder="To" value="{date_start2_event}"/>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label" for="statusstok"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="button" id="btn_filter_event" name="btn_filter_event" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						</div>
					</div>
						
				</div>
			</div>
			<?php echo form_close() ?>
			<div class="row   ">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="index_list_event">
							<thead>
								<tr>
									<th width="10%">No</th>
									<th width="10%">Tanggal Input</th>
									<th width="10%">Tanggal Event</th>
									<th width="10%">Judul</th>
									<th width="10%">Status</th>
									<th width="10%">Batas Publish</th>
									<th width="10%">Publish By</th>
									<th width="15%">Action</th>
								   
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<?}?>
		
	</div>
</div>
<div class="modal" id="modal_aktifasi_info" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog modal-sm" style="width: 25%;">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Aktifkan Kembali</h3>
				</div>
				<div class="block-content">
					<div class="row">
                        <input type="hidden" id="idinfo" name="idinfo" value="">

						<div class="col-md-12">                               
							<div class="form-group">
								<label class="col-md-4 control-label">Batas Publish</label>
								<div class="col-md-8"> 
								<div class="input-group date">
									<input class="js-datepicker form-control input"  type="text" id="date_end" name="date_end" value="" data-date-format="dd-mm-yyyy"/>
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">                               
							<br>
						</div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-primary" type="submit" type="button" id="btn_aktif_info"><i class="fa fa-check"></i> Simpan</button>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="modal_aktifasi_event" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog modal-sm" style="width: 25%;">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Aktifkan Kembali</h3>
				</div>
				<div class="block-content">
					<div class="row">
                        <input type="hidden" id="idevent" name="idevent" value="">

						<div class="col-md-12">                               
							<div class="form-group">
								<label class="col-md-4 control-label">Batas Publish</label>
								<div class="col-md-8"> 
								<div class="input-group date">
									<input class="js-datepicker form-control input"  type="text" id="date_end_event" name="date_end_event" value="" data-date-format="dd-mm-yyyy"/>
									<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</span>
								</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">                               
							<br>
						</div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-primary" type="submit" type="button" id="btn_aktif_event"><i class="fa fa-check"></i> Simpan</button>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
$(document).ready(function(){	
	var tab=$("#tab").val();
	if (tab=='1'){
		load_index_info();
		
	}
	if (tab=='2'){
		load_index_event();		
	}

})	

$(document).on("click","#btn_filter_info",function(){
	load_index_info();
});

function load_index_info(){
	$('#index_list_info').DataTable().destroy();	
	var created_date_1=$("#created_date1_info").val();
	var created_date_2=$("#created_date2_info").val();
	var date_start1=$("#date_start1_info").val();
	var date_start2=$("#date_start2_info").val();
	var status_info=$("#status_info").val();
	table = $('#index_list_info').DataTable({
            autoWidth: false,
            searching: false,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "5%", "targets": 0,  className: "text-right" },
					{ "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					{ "width": "12%", "targets": [6,7],  className: "text-center" },
					{ "width": "30%", "targets": 3,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}app_setting/getIndex_info', 
                type: "POST" ,
                dataType: 'json',
				data : {
						created_date_1:created_date_1,
						created_date_2:created_date_2,
						date_start1:date_start1,
						date_start2:date_start2,
						status:status_info,
					   }
            }
        });
}
function publish_info(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Publish?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}app_setting/publish_info',
				type: 'POST',
				data: {id: id},
				complete: function() {
					table.ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Publish'});
					
				}
			});
	});
}
function unPublish_info(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan unPublish?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}app_setting/unPublish_info',
				type: 'POST',
				data: {id: id},
				complete: function() {
					table.ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' unPublish'});
					
				}
			});
	});
}
function modal_aktifasi_info(id){
	$("#idinfo").val(id);
	$("#modal_aktifasi_info").modal('show');
}
$(document).on("click","#btn_aktif_info",function(){
	if ($("#date_end").val()==''){
		sweetAlert("Maaf...", "Tentukan Tanggal", "error");
		return false;
	}
	
	
	var id=$("#idinfo").val();
	var date_end=$("#date_end").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Mengaktifkan Kembali?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$("#modal_aktifasi_info").modal('hide');
		 $.ajax({
				url: '{site_url}app_setting/aktifkan_info',
				type: 'POST',
				data: {id: id,date_end:date_end},
				complete: function() {
					table.ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Aktifkan'});
					
				}
			});
	});
});

// EVENT
$(document).on("click","#btn_filter_event",function(){
	load_index_event();
});

function load_index_event(){
	$('#index_list_event').DataTable().destroy();	
	var created_date_1=$("#created_date1_event").val();
	var created_date_2=$("#created_date2_event").val();
	var date_start1=$("#date_start1_event").val();
	var date_start2=$("#date_start2_event").val();
	var status_event=$("#status_event").val();
	table = $('#index_list_event').DataTable({
            autoWidth: false,
            searching: false,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "5%", "targets": 0,  className: "text-right" },
					{ "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					{ "width": "12%", "targets": [6,7],  className: "text-center" },
					{ "width": "30%", "targets": 3,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}app_setting/getIndex_event', 
                type: "POST" ,
                dataType: 'json',
				data : {
						created_date_1:created_date_1,
						created_date_2:created_date_2,
						date_start1:date_start1,
						date_start2:date_start2,
						status:status_event,
					   }
            }
        });
}
function publish_event(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Publish?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}app_setting/publish_event',
				type: 'POST',
				data: {id: id},
				complete: function() {
					table.ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Publish'});
					
				}
			});
	});
}
function unPublish_event(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan unPublish?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}app_setting/unPublish_event',
				type: 'POST',
				data: {id: id},
				complete: function() {
					table.ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' unPublish'});
					
				}
			});
	});
}
function modal_aktifasi_event(id){
	$("#idevent").val(id);
	$("#modal_aktifasi_event").modal('show');
}
$(document).on("click","#btn_aktif_event",function(){
	if ($("#date_end_event").val()==''){
		sweetAlert("Maaf...", "Tentukan Tanggal", "error");
		return false;
	}
	
	
	var id=$("#idevent").val();
	var date_end=$("#date_end_event").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Mengaktifkan Kembali?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$("#modal_aktifasi_event").modal('hide');
		 $.ajax({
				url: '{site_url}app_setting/aktifkan_event',
				type: 'POST',
				data: {id: id,date_end:date_end},
				complete: function() {
					table.ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Aktifkan'});
					
				}
			});
	});
});

</script>