<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<?php if (UserAccesForm($user_acces_form,array('1460'))){ ?>
<div class="block">
	<div class="block-header">
		
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('app_setting/save_about', 'class="form-horizontal push-10-t" onsubmit="return validate_final_about()"') ?>
			<div class="row">
				
				<div class="col-md-12">
					<div class="form-group">
						<label class="col-md-2 control-label" for="about_us_judul">Judul About Us<span style="color:red;">*</span></label>
						<div class="col-md-10">
							<input tabindex="0" type="text" class="form-control" id="about_us_judul" placeholder="Judul About Us" name="about_us_judul" value="{about_us_judul}" required>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label class="col-md-2 control-label" for="about_us_judul">Isi About Us<span style="color:red;">*</span></label>
						<div class="col-md-10">
							<textarea class="form-control js-summernote" name="about_us_isi" id="about_us_isi"><?=$about_us_isi?></textarea>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12 col-md-offset-2">
					
						<button class="btn btn-success" name="btn_simpan_about" value="1" type="submit">Update</button>						
						<a href="{base_url}app_setting/info/3" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
	</div>

<?}?>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">

$(document).ready(function(){
	$('#about_us_isi').summernote({
	  height: 400,   //set editable area's height
	  codemirror: { // codemirror options
		theme: 'monokai'
	  }	
	})	
})	
function validate_final_about()
{
	if ($("#about_us_judul").val()==''){
		sweetAlert("Maaf...", "Tentukan Judul About Us", "error");
		return false;
	}

	if ($("#about_us_isi").val()==''){
		sweetAlert("Maaf...", "Isi Content About Us", "error");
		return false;
	}

	return true;
}
</script>