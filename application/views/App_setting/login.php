<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block" style="text-transform:uppercase;">
	<div class="block-header">
		
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('app_setting/save_login', 'class="form-horizontal push-10-t"') ?>
        <?php if ($login_logo != '') { ?>
        <div class="form-group">
            <label class="col-md-2 control-label" for="example-hf-email"></label>
            <div class="col-md-7">

                <img class="img-avatar"  id="output_img" src="{upload_path}app_setting/{login_logo}" />
            </div>
        </div>
        <?php } ?>
        <div class="form-group">
            <label class="col-md-2 control-label" for="example-hf-email">Logo Login (100x100)</label>
            <div class="col-md-7">
                <div class="box">
                    <input type="file" id="file-3" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;" name="login_logo" value="{login_logo}" />
                    <label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                </div>
            </div>
        </div>
		
		<div class="form-group">
			<label class="col-md-2 control-label" for="login_judul">Judul Login<span style="color:red;">*</span></label>
			<div class="col-md-10">
				<input tabindex="0" type="text" class="form-control" id="login_judul" placeholder="Judul Login" name="login_judul" value="{login_judul}" required>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="login_judul">Pesan Salah password<span style="color:red;">*</span></label>
			<div class="col-md-10">
				<input tabindex="0" type="text" class="form-control" id="login_salah" placeholder="Pesan Salah Password" name="login_salah" value="{login_salah}" required>
			</div>
		</div>
		<?php if ($login_gambar_side != '') { ?>
        <div class="form-group">
            <label class="col-md-2 control-label" for="example-hf-email"></label>
            <div class="col-md-7">

                <img class=""  id="output_img_bg" width="175" height="232" src="{upload_path}app_setting/{login_gambar_side}" />
            </div>
        </div>
        <?php } ?>
		<div class="form-group">
            <label class="col-md-2 control-label" for="example-hf-email">BG Login (870 × 1160)</label>
            <div class="col-md-7">
                <div class="box">
                    <input type="file" id="file-4" class="inputfile inputfile-3" onchange="loadFile_img_bg2(event)" style="display:none;" name="login_gambar_side" value="{login_gambar_side}" accept="image/png, image/jpeg, image/jpg, image/bmp" />
                    <label for="file-4"><svg xmlns="http://www.w3.org/2000/svg" width="870" height="1160" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                </div>
            </div>
        </div>
		<div class="form-group">
			<div class="col-md-12 col-md-offset-2">
				<button class="btn btn-success" type="submit">Simpan</button>
				<a href="{base_url}app_setting/login" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
			</div>
		</div>
		<?php echo form_hidden('id', $id); ?>
		<?php echo form_close() ?>
	</div>
</div>


<script type="text/javascript">
	function loadFile_img(event){
		// alert('load');
		var output_img = document.getElementById('output_img');
		output_img.src = URL.createObjectURL(event.target.files[0]);
	}
	function loadFile_img_bg2(event){
		// alert('sini');
		var output_img_bg = document.getElementById('output_img_bg');
		output_img_bg.src = URL.createObjectURL(event.target.files[0]);
	}
</script>