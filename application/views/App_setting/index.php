<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block" style="text-transform:uppercase;">
	<div class="block-header">
		
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('app_setting/save_index', 'class="form-horizontal push-10-t"') ?>
        
		
		<div class="form-group">
			<label class="col-md-2 control-label" for="index_pesan">Pesan Login<span style="color:red;">*</span></label>
			<div class="col-md-10">
				<input tabindex="0" type="text" class="form-control" id="index_pesan" placeholder="Pesan Login" name="index_pesan" value="{index_pesan}" required>
			</div>
		</div>
		
        <div class="form-group">
            <label class="col-md-2 control-label" for="example-hf-email">Logo Login (100 x 100)</label>
            <div class="col-md-1">
                <img class=""  id="output_img" width="32" height="32" src="{upload_path}app_setting/<?=($index_logo!='' ? $index_logo : 'no_image.png')?>" />
            </div>
			<div class="col-md-3">
                <div class="box">
                    <input type="file" id="file-3" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;" name="index_logo" value="{index_logo}" />
                    <label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                </div>
            </div>
        </div>
        
		
        <div class="form-group">
            <label class="col-md-2 control-label" for="example-hf-email">Lihat Antrian (64 x 64)</label>
            <div class="col-md-1">

                <img class=""  id="output_icon_1" width="32" height="32" src="{upload_path}app_setting/<?=($index_icon_1!='' ? $index_icon_1 : 'no_image.png')?>" />
            </div>
			<div class="col-md-3">
                <div class="box">
                    <input type="file" id="file_icon_1" class="inputfile inputfile-3" onchange="loadFile_icon_1(event)" style="display:none;" name="index_icon_1" value="{index_icon_1}" accept="image/png, image/jpeg, image/jpg, image/bmp" />
                    <label for="file_icon_1"><svg xmlns="http://www.w3.org/2000/svg" width="870" height="1160" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                </div>
            </div>
			 <label class="col-md-2 control-label" for="example-hf-email">Daftar Online (64 x 64)</label>
            <div class="col-md-1">

                <img class=""  id="output_icon_2" width="32" height="32" src="{upload_path}app_setting/<?=($index_icon_2!='' ? $index_icon_2 : 'no_image.png')?>" />
            </div>
			<div class="col-md-3">
                <div class="box">
                    <input type="file" id="file_icon_2" class="inputfile inputfile-3" onchange="loadFile_icon_2(event)" style="display:none;" name="index_icon_2" value="{index_icon_2}" accept="image/png, image/jpeg, image/jpg, image/bmp" />
                    <label for="file_icon_2"><svg xmlns="http://www.w3.org/2000/svg" width="870" height="1160" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                </div>
            </div>
        </div>
       
		<div class="form-group">
            <label class="col-md-2 control-label" for="example-hf-email">Jadwal Dokter (64 x 64)</label>
            <div class="col-md-1">

                <img class=""  id="output_icon_3" width="32" height="32" src="{upload_path}app_setting/<?=($index_icon_3!='' ? $index_icon_3 : 'no_image.png')?>" />
            </div>
			<div class="col-md-3">
                <div class="box">
                    <input type="file" id="file_icon_3" class="inputfile inputfile-3" onchange="loadFile_icon_3(event)" style="display:none;" name="index_icon_3" value="{index_icon_3}" accept="image/png, image/jpeg, image/jpg, image/bmp" />
                    <label for="file_icon_3"><svg xmlns="http://www.w3.org/2000/svg" width="870" height="1160" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                </div>
            </div>
			<label class="col-md-2 control-label" for="example-hf-email">Riwayat Transaksi (64 x 64)</label>
            <div class="col-md-1">

                <img class=""  id="output_icon_4" width="32" height="32" src="{upload_path}app_setting/<?=($index_icon_4!='' ? $index_icon_4 : 'no_image.png')?>" />
            </div>
			<div class="col-md-3">
                <div class="box">
                    <input type="file" id="file_icon_4" class="inputfile inputfile-3" onchange="loadFile_icon_4(event)" style="display:none;" name="index_icon_4" value="{index_icon_4}" accept="image/png, image/jpeg, image/jpg, image/bmp" />
                    <label for="file_icon_4"><svg xmlns="http://www.w3.org/2000/svg" width="870" height="1160" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="example-hf-email">Rehabilitas Medis (64 x 64)</label>
            <div class="col-md-1">

                <img class=""  id="output_icon_5" width="32" height="32" src="{upload_path}app_setting/<?=($index_icon_5!='' ? $index_icon_5 : 'no_image.png')?>" />
            </div>
			<div class="col-md-3">
                <div class="box">
                    <input type="file" id="file_icon_5" class="inputfile inputfile-3" onchange="loadFile_icon_5(event)" style="display:none;" name="index_icon_5" value="{index_icon_5}" accept="image/png, image/jpeg, image/jpg, image/bmp" />
                    <label for="file_icon_5"><svg xmlns="http://www.w3.org/2000/svg" width="870" height="1160" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                </div>
            </div>
			<label class="col-md-2 control-label" for="example-hf-email">Reservasi (64 x 64)</label>
            <div class="col-md-1">

                <img class=""  id="output_icon_6" width="32" height="32" src="{upload_path}app_setting/<?=($index_icon_6!='' ? $index_icon_6 : 'no_image.png')?>" />
            </div>
			<div class="col-md-3">
                <div class="box">
                    <input type="file" id="file_icon_6" class="inputfile inputfile-3" onchange="loadFile_icon_6(event)" style="display:none;" name="index_icon_6" value="{index_icon_6}" accept="image/png, image/jpeg, image/jpg, image/bmp" />
                    <label for="file_icon_6"><svg xmlns="http://www.w3.org/2000/svg" width="870" height="1160" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                </div>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="example-hf-email">Informasi (64 x 64)</label>
            <div class="col-md-1">

                <img class=""  id="output_icon_7" width="32" height="32" src="{upload_path}app_setting/<?=($index_icon_7!='' ? $index_icon_7 : 'no_image.png')?>" />
            </div>
			<div class="col-md-3">
                <div class="box">
                    <input type="file" id="file_icon_7" class="inputfile inputfile-3" onchange="loadFile_icon_7(event)" style="display:none;" name="index_icon_7" value="{index_icon_7}" accept="image/png, image/jpeg, image/jpg, image/bmp" />
                    <label for="file_icon_7"><svg xmlns="http://www.w3.org/2000/svg" width="870" height="1160" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                </div>
            </div>
			<label class="col-md-2 control-label" for="example-hf-email">Data Pasien (64 x 64)</label>
            <div class="col-md-1">

                <img class=""  id="output_icon_8" width="32" height="32" src="{upload_path}app_setting/<?=($index_icon_8!='' ? $index_icon_8 : 'no_image.png')?>" />
            </div>
			<div class="col-md-3">
                <div class="box">
                    <input type="file" id="file_icon_8" class="inputfile inputfile-3" onchange="loadFile_icon_8(event)" style="display:none;" name="index_icon_8" value="{index_icon_8}" accept="image/png, image/jpeg, image/jpg, image/bmp" />
                    <label for="file_icon_8"><svg xmlns="http://www.w3.org/2000/svg" width="870" height="1160" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                </div>
            </div>
        </div>
        <hr>
		
		<div class="form-group">
			<div class="col-md-12 col-md-offset-2">
				<button class="btn btn-success" type="submit">Simpan</button>
				<a href="{base_url}app_setting/login" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
			</div>
		</div>
		<?php echo form_hidden('id', $id); ?>
		<?php echo form_close() ?>
	</div>
</div>
<div class="block block-rounded">
	<div class="block-header">
		
		<h3 class="block-title">Gallery Image</h3>
	</div>
	<div class="block-content">
		<div class="row">
			<div class="form-group">
				<div class="col-md-12">
					<form class="dropzone" action="{base_url}App_setting/upload_files" method="post" enctype="multipart/form-data">
						<input name="idtransaksi" type="hidden" value="{id}">
					  </form>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
		<form class="form-horizontal push-10-t">
		<div class="form-group">
			<div class="col-md-12">
					<div class="table-responsive">
					 <table class="table table-bordered table-striped table-responsive" id="listFile">
						<thead>
							<tr>
								<th width="1%">No</th>
								<th width="20%">File</th>
								<th width="15%">Upload By</th>
								<th width="5%">Size</th>
								<th width="5%">Aksi</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
		</form>
	</div>
	</div>
</div>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var myDropzone 
$(document).ready(function(){
		Dropzone.autoDiscover = false;
		myDropzone = new Dropzone(".dropzone", { 
		   autoProcessQueue: true,
		   maxFilesize: 30,
		});
		myDropzone.on("complete", function(file) {
		  
		  refresh_image();
		  myDropzone.removeFile(file);
		  
		});
		refresh_image();
	})	
	function refresh_image(){
		
		$('#listFile tbody').empty();
		$.ajax({
			url: '{site_url}app_setting/refresh_image/',
			dataType: "json",
			success: function(data) {
				// if (data.detail!=''){
					$('#listFile tbody').empty();
					$("#listFile tbody").append(data.detail);
				// }
				// console.log();
				
			}
		});
	}
	function removeFile($id){
		 var id=$id;
        swal({
				title: "Anda Yakin ?",
				text : "Untuk Hapus data?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				$.ajax({
					url: '{site_url}app_setting/removeFile',
					type: 'POST',
					data: {id: id},
					complete: function() {
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
						refresh_image();
						// $("#cover-spin").hide();
						// filter_form();
					}
				});
			});
	}
	function loadFile_img(event){
		// alert('load');
		var output_img = document.getElementById('output_img');
		output_img.src = URL.createObjectURL(event.target.files[0]);
	}
	function loadFile_icon_1(event){
		// alert('sini');
		var output_icon_1 = document.getElementById('output_icon_1');
		output_icon_1.src = URL.createObjectURL(event.target.files[0]);
	}
	function loadFile_icon_2(event){
		// alert('sini');
		var output_icon_2 = document.getElementById('output_icon_2');
		output_icon_2.src = URL.createObjectURL(event.target.files[0]);
	}
	function loadFile_icon_3(event){
		// alert('sini');
		var output_icon_3 = document.getElementById('output_icon_3');
		output_icon_3.src = URL.createObjectURL(event.target.files[0]);
	}
	function loadFile_icon_4(event){
		// alert('sini');
		var output_icon_4 = document.getElementById('output_icon_4');
		output_icon_4.src = URL.createObjectURL(event.target.files[0]);
	}
	function loadFile_icon_5(event){
		// alert('sini');
		var output_icon_5 = document.getElementById('output_icon_5');
		output_icon_5.src = URL.createObjectURL(event.target.files[0]);
	}
	function loadFile_icon_6(event){
		// alert('sini');
		var output_icon_6 = document.getElementById('output_icon_6');
		output_icon_6.src = URL.createObjectURL(event.target.files[0]);
	}
	function loadFile_icon_7(event){
		// alert('sini');
		var output_icon_7 = document.getElementById('output_icon_7');
		output_icon_7.src = URL.createObjectURL(event.target.files[0]);
	}
	function loadFile_icon_8(event){
		// alert('sini');
		var output_icon_8 = document.getElementById('output_icon_8');
		output_icon_8.src = URL.createObjectURL(event.target.files[0]);
	}
</script>