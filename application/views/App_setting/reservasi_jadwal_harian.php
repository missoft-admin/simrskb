<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<style>
.tableFix { /* Scrollable parent element */
  position: relative;
  overflow: auto;
  height: 500px;
}

.tableFix table{
  width: 100%;
  border-collapse: collapse;
}

.tableFix th,
.tableFix td{
  padding: 8px;
  text-align: left;
}

.tableFix thead th {
  position: sticky;  /* Edge, Chrome, FF */
  top: 0px;
  background: #fff;  /* Some background is needed */
}
</style>
<style>
	/* kalendar */
	table.kalendar {
        width: 100%;
		border-left: 1px solid #e6e9ea;
	}

	tr.kalendar-row {}

	td.kalendar-day {
		min-height: 80px;
		font-size: 11px;
		position: relative;
	}

	* html div.kalendar-day {
		height: 80px;
	}

	td.kalendar-day:hover {
		background: #eceff5;
	}
	td.kalendar-day-last {
		background: #fdfdfd;
		min-height: 80px;
	}

	td.kalendar-day-np {
		background: #fdfdfd;
		min-height: 80px;
	}

	* html div.kalendar-day-np {
		height: 80px;
	}

	td.kalendar-day-head {
        width: calc(100% / 7);
        border-right: 1px solid #2c7aca;
        padding: 5px;
        text-transform: uppercase;
        font-size: 12px;
        font-weight: bold;
        color: #818589;
        color: #fff;
        background-color: #448cd6;
	}

    td.kalendar-day-head:nth-child(7) {
        border: none;
    }

	div.day-number {
        padding: 8px;
        font-weight: bold;
        color: #7c878d;
        cursor: pointer;
        min-height: 100px;
	}

	div.day-number-mini {
        padding: 8px;
        font-weight: bold;
        color: #7c878d;
        cursor: pointer;
	}

	/* shared */
	td.kalendar-day,
	td.kalendar-day-np {
		width: 120px;
		padding: 5px;
		border-bottom: 1px solid #e6e9ea;
		border-right: 1px solid #e6e9ea;
	}

	bg-isi {
		background-color: #ffcac9;
	}
	.kalendar-event {
		margin-top: 10px;
	}

	.kalendar-event a {
        font-weight: 500;
        padding: 3px 6px;
        border-radius: 4px;
        background-color: #ce5151;
        color: #fff;
        word-wrap: break-word;
	}

	.kalendar-event a.FULL {
        background-color: #d90234;
	}

	.kalendar-event a.AVAILABLE {
        background-color: #1db573;
	}

	.kalendar-event a.PUBLISH {
        background-color: #518fce;
	}

	.kalendar-event a.SELESAI {
        background-color: #51ce57;
	}

	.kalendar-event a.DIBATALKAN {
        background-color: #ce5151;
	}

    div.scroll-content {
        overflow-x: auto;
        white-space: nowrap;
    }
    div.scroll-content [class*="col"]{
        display: inline-block;
        float: none;
    }
</style>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li><a href="{base_url}app_setting/reservasi" title="Kembali"><i class="fa fa-reply"></i></a></li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
		
	<div class="block-content">
		<div class="block">
		<ul class="nav nav-tabs" data-toggle="tabs" id="tabs">
			<li id="tab_kuota" <?=($tab=='1'?'class="active"':'')?>>
				<a href="#btabs-animated-slideleft-kuota" onclick="load_awal_tab1()"><i class="fa fa-calendar-o"></i> Calendar View</a>
			</li>
			<li id="tab_add" <?=($tab=='2'?'class="active"':'')?>>
				<a href="#btabs-animated-slideleft-note"  onclick="load_awal_tab2()"><i class="fa fa-calendar"></i> Schedule List</a>
			</li>
			
		</ul>
		<input class="form-control input-sm " readonly type="hidden" id="tab" name="tab" value="{tab}"/>	
		<div class="block-content tab-content">
			<div class="tab-pane fade fade-left <?=($tab=='1'?'in active':'')?>" id="btabs-animated-slideleft-kuota">
				<?php echo form_open('#','class="form-horizontal" id="form-work"') ?>
				<input class="form-control input-sm " type="hidden" id="present" name="present" value="{present}"/>	
				<input class="form-control input-sm " readonly type="hidden" id="tanggal" name="tanggal" value="{tanggal}"/>	
				<div class="row">
					<div class="col-md-3">
						<div class="form-group" style="margin-bottom: 5px;">
							<div class="col-md-12">
								<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<?foreach($list_poli as $r){?>
									<option value="<?=$r->idpoli?>" <?=($idpoli==$r->idpoli?'selected':'')?>><?=$r->nama?></option>
									<?}?>
								</select>
							</div>
						</div>
						
					</div>
					<div class="col-md-2">
						<div class="form-group" style="margin-bottom: 5px;">
							<div class="btn-group btn-group-justified">
								<div class="btn-group">
									<button class="btn btn-default" id="btn_back" type="button"><i class="fa fa-arrow-left"></i></button>
								</div>
								
								<div class="btn-group">
									<button class="btn btn-default" id="btn_next" type="button"><i class="fa fa-arrow-right"></i></button>
								</div>
								<div class="btn-group">
									<button class="btn btn-default" id="btn_present" type="button"> PRESENT</button>
								</div>
							</div>
						</div>
						
					</div>
					<div class="col-md-4">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="control-label" for="edit_vendor"></label>
						</div>
						
					</div>
					<div class="col-md-3">
						<div class="form-group" style="margin-bottom: 5px;">
							<div class="btn-group btn-group-justified">
								<div class="btn-group">
									<button class="btn btn-default present" id="present_1" type="button">MONTH</button>
								</div>
								
								<div class="btn-group">
									<button class="btn btn-default present" id="present_2"  type="button">WEEK</button>
								</div>
								<div class="btn-group">
									<button class="btn btn-default present" id="present_3"  type="button">DAY</button>
								</div>
							</div>
						</div>
						
					</div>
					
				</div>
				<div class="row push-20-t">
					<div class="col-md-12">
						<h4 class="text-center text-uppercase">RESERVASI REHABILITAS MEDIS</h4>
						<h4 class="text-center text-uppercase" id="nama_periode"></h4>
						<br>
						<div id="hasil"></div>
					</div>
				</div>
				<?php echo form_close() ?>
			</div>
			<div class="tab-pane fade fade-left <?=($tab=='2'?'in active':'')?>" id="btabs-animated-slideleft-note">
				<?php echo form_open('#','class="form-horizontal push-5-t" id="form-work"') ?>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label class="col-md-12" for="login1-username">Tujuan Kunjungan</label>
							<div class="col-md-12">
								<select id="idpoli2" name="idpoli2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<?foreach($list_poli as $r){?>
									<option value="<?=$r->idpoli?>" <?=($idpoli2==$r->idpoli?'selected':'')?>><?=$r->nama?></option>
									<?}?>
								</select>
							</div>
						</div>
						
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label class="col-md-12" for="login1-username">View</label>
							<div class="col-md-12">
								<select id="present2" name="present2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									
									<option value="1" <?=($present2=="1"?'selected':'')?>>MONTHLY</option>
									<option value="2" <?=($present2=="2"?'selected':'')?>>WEEKLY</option>
									<option value="3" <?=($present2=="3"?'selected':'')?>>DAILY</option>
									
								</select>
							</div>
						</div>
						
					</div>
					<div class="col-md-2 div_present">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-12" for="login1-username">&nbsp;</label>
							<div class="col-md-12">
							<div class="btn-group btn-group-justified">
								<div class="btn-group">
									<button class="btn btn-default" id="btn_back2" type="button"><i class="fa fa-arrow-left"></i></button>
								</div>
								
								<div class="btn-group">
									<button class="btn btn-default" id="btn_next2" type="button"><i class="fa fa-arrow-right"></i></button>
								</div>
								
							</div>
							</div>
						</div>
						
					</div>
					<div class="col-md-3 div_date">
						<div class="form-group">
							<label class="col-md-12" for="login1-username">Date</label>
							<div class="col-md-12">
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" id="tanggaldari" name="tanggaldari" placeholder="From" value="{tanggaldari}">
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" id="tanggalsampai" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
								</div>
							</div>
						</div>
						
					</div>
					<div class="col-md-1 div_date">
						<div class="form-group">
							<label class="col-md-12" for="login1-username">&nbsp;</label>
							<div class="col-md-12">
								<button class="btn btn-success present" id="btn_filter_ls" type="button"><i class="fa fa-search"></i> Filter</button>
							</div>
						</div>
						
					</div>
				</div>
				<div class="row push-20-t">
					<div class="col-md-12">
						<div class="tableFix2">
						<div id="div_ls"></div>
						</div>
					</div>
				</div>
				<?php echo form_close() ?>
			</div>
		
			
		</div>
	</div>
  </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
var present;
var st_plus_minus;
var tab;
var tanggaldari;
var tanggalsampai;
$(document).ready(function(){	
	tab=$("#tab").val();
	if (tab=='1'){
		load_awal_tab1();
	}
	if (tab=='2'){
		load_awal_tab2();
	}
	
})	
function refresh_present2(){
	present=$("#present2").val();
	if (present=='3'){
		$(".div_date").show();
		$(".div_present").hide();
	}else{
		$(".div_date").hide();
		$(".div_present").show();
		
	}
}
function load_awal_tab2(){
	refresh_present2();
	hasil_generate_ls();
}

function load_awal_tab1(){
	refresh_present();
	hasil_generate_jadwal();
}
function hasil_generate_ls(){
	tanggaldari=$("#tanggaldari").val();
	tanggalsampai=$("#tanggalsampai").val();
	let idpoli=$("#idpoli2").val();
	let present=$("#present2").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/hasil_generate_ls/',
		method: "POST",
		dataType: "JSON",
		data: {
			tanggaldari:tanggaldari,
			tanggalsampai:tanggalsampai,
			present:present,
			idpoli:idpoli,
		},
		success: function(data) {
			$("#div_ls").html(data.tabel);
			$("#cover-spin").hide();
			window.table = $('#index_ls').DataTable(
			  {
			   "autoWidth": false,
					// "pageLength": 50,
					autoWidth: true,
					scrollX: true,
					scrollY: 400,
					"paging": true,
					paging: false,
					ordering: false,
					scrollCollapse: true,
					"processing": true,
					
				ordering: [
				  [1, 'asc']
				],
				colReorder:
				{
				  fixedColumnsLeft: 3,
				  fixedColumnsRight: 1
				}
			  });
			$("#cover-spin").hide();
		}
	});
}
function hasil_generate_jadwal(){
	var tanggal=$("#tanggal").val();
	var idpoli=$("#idpoli").val();
	present=$("#present").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/hasil_generate_jadwal/',
		method: "POST",
		dataType: "JSON",
		data: {
			tanggal:tanggal,
			present:present,
			idpoli:idpoli,
			st_plus_minus:st_plus_minus,
		},
		success: function(data) {
			$("#hasil").html(data.tabel);
			$("#nama_periode").html(data.nama_periode);
			$("#cover-spin").hide();
		}
	});
}
function refresh_tanggal(){
	var tanggal=$("#tanggal").val();
	present=$("#present").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/refresh_tanggal/',
		method: "POST",
		dataType: "JSON",
		data: {
			tanggal:tanggal,
			present:present,
			st_plus_minus:st_plus_minus,
		},
		success: function(data) {
			$("#tanggal").val(data.tanggal);
			$("#cover-spin").hide();
			hasil_generate_jadwal();
		}
	});
}
function refresh_tanggal2(){
	var tanggaldari=$("#tanggaldari").val();
	var tanggalsampai=$("#tanggalsampai").val();
	present=$("#present2").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/refresh_tanggal2/',
		method: "POST",
		dataType: "JSON",
		data: {
			tanggaldari:tanggaldari,
			tanggalsampai:tanggalsampai,
			present:present,
			st_plus_minus:st_plus_minus,
		},
		success: function(data) {
			$("#tanggaldari").val(data.tanggaldari);
			$("#tanggalsampai").val(data.tanggalsampai);
			$("#cover-spin").hide();
			hasil_generate_ls();
		}
	});
}
$("#idpoli2").change(function() {
	hasil_generate_ls();
});
$("#present2").change(function() {
	refresh_present2();
	// if ($("#present2").val()!='3'){
		st_plus_minus=0;
		refresh_tanggal2();
	// }
});
$("#idpoli").change(function() {
	hasil_generate_jadwal();
});
$("#btn_present").click(function() {
	present=parseFloat($("#present").val())+1;
	if (present>3){
		present=1;
	}
	st_plus_minus=0;
	$("#present").val(present);
	refresh_present();
});
$("#btn_filter_ls").click(function() {
	hasil_generate_ls();
});
$("#present_1").click(function() {
	present=1;
	$("#present").val(present);
	refresh_present();
});
$("#present_2").click(function() {
	present=2;
	$("#present").val(present);
	refresh_present();
});
$("#present_3").click(function() {
	present=3;
	$("#present").val(present);
	refresh_present();
});
$("#btn_back").click(function() {
	st_plus_minus='-1'
	refresh_tanggal();
	
});
$("#btn_next").click(function() {
	st_plus_minus='1'
	refresh_tanggal();
	
});
$("#btn_back2").click(function() {
	st_plus_minus='-1'
	refresh_tanggal2();
	
});
$("#btn_next2").click(function() {
	st_plus_minus='1'
	refresh_tanggal2();
	
});

function refresh_present(){
	clear_present();
	present=$("#present").val();
	if (present=='1'){
		$("#present_1").addClass("btn-success");
	}
	if (present=='2'){
		$("#present_2").addClass("btn-success");
	}
	if (present=='3'){
		$("#present_3").addClass("btn-success");
	}
	hasil_generate_jadwal();
}
function clear_present(){
	 $(".present").removeClass("btn-success");
}
</script>