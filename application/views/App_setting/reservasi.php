<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<div class="block-header">
		
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content tab-content">
		<div class="col-md-12">
			<div class="form-group" style="margin-bottom: 15px;">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="index_jadwal">
							<thead>
								<tr>
									<th width="5%">No</th>
									<th width="12%">Poliklinik</th>
									<th width="10%">Senin</th>
									<th width="10%">Selasa</th>
									<th width="10%">Rabu</th>
									<th width="10%">Kamis</th>
									<th width="10%">Jumat</th>
									<th width="10%">Sabtu</th>
									<th width="10%">Minggu</th>
									<th width="10%">Action</th>										   
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
	
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
$(document).ready(function(){	
	load_index();
})	
function load_index(){
	$('#index_jadwal').DataTable().destroy();	
	table = $('#index_jadwal').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "10%", "targets": [2,3,4,5,6,7,8,9],  className: "text-center" },
					{ "width": "5%", "targets": [0],  className: "text-right" },
					{ "width": "12%", "targets": [1],  className: "text-left" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}app_setting/load_index_jadwal', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}

</script>