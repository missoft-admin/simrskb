<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li><a href="{base_url}app_setting/reservasi" title="Kembali"><i class="fa fa-reply"></i></a></li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content tab-content">
		<?php echo form_open('mjadwal_dokter/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group" style="margin-bottom: 15px;">
				<label class="col-md-2 control-label" for="">Nama Tujuan</label>
				<div class="col-md-6">
					<input class="form-control" type="text" readonly name="nama_poli"  id="nama_poli" placeholder="" value="{nama_poli}">
					<input class="form-control" type="hidden" name="idpoli"  id="idpoli" placeholder="Jam Dari" value="{idpoli}">
					
				</div>
				
			</div>
			<div class="form-group" style="margin-bottom: 15px;">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-condensed" id="index_jadwal">
							<thead>
								<tr>
									<th width="5%" class="text-center">No</th>
									<th width="10%" class="text-center">Time</th>
									<th width="10%" class="text-center">Senin</th>
									<th width="10%" class="text-center">Selasa</th>
									<th width="10%" class="text-center">Rabu</th>
									<th width="10%" class="text-center">Kamis</th>
									<th width="10%" class="text-center">Jumat</th>
									<th width="10%" class="text-center">Sabtu</th>
									<th width="10%" class="text-center">Minggu</th>
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
				
			</div>
		<?php echo form_close() ?>
	</div>
</div>
<div class="modal" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width:50%">
        <form class="form-horizontal" action="javascript:void(0)" id="frm1">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">Detail Slot</h3>
                    </div>
                    <div class="block-content">
						<div class="block">
						<ul class="nav nav-tabs" data-toggle="tabs" id="tabs">
							<li class="active" id="tab_kuota">
								<a href="#btabs-animated-slideleft-kuota"><i class="fa fa-pencil"></i> Pengaturan Slot</a>
							</li>
							<li id="tab_add">
								<a href="#btabs-animated-slideleft-note"><i class="fa fa-pencil"></i> Update Notes</a>
							</li>
							<li id="tab_add">
								<a href="#btabs-animated-slideleft-list"><i class="fa fa-list-ol"></i> History Notes</a>
							</li>
							
						</ul>
						<div class="block-content tab-content">
							<div class="tab-pane fade fade-left in active" id="btabs-animated-slideleft-kuota">
								<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Hari</label>
											<div class="col-md-10">
												<input class="form-control input-sm " readonly type="text" id="namahari" name="namahari" value=""/>	
												<input class="form-control input-sm " readonly type="hidden" id="kodehari" name="kodehari" value=""/>	
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Jam</label>
											<div class="col-md-10">
												<input class="form-control input-sm " readonly type="text" id="jam" name="jam" value=""/>	
												<input class="form-control input-sm " readonly type="hidden" id="jam_id" name="jam_id" value=""/>	
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="tipeid">Kuota</label>
											<div class="col-md-10">
												<select id="kuota" name="kuota" class="form-control js-select2" style="width: 100%;" data-placeholder="Choose one..">
													<option value="999">TANPA BATAS</option>
													<option value="0" selected>ISI PENUH</option>
													<?for ($x = 1; $x <= 100; $x++) {?>
														<option value="<?=$x?>"><?=$x?></option>
													<?}?>
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="tipeid">Reservasi Online</label>
											<div class="col-md-10">
												<select id="st_reservasi_online" name="st_reservasi_online" class="form-control js-select2" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1">RESERVASI ONLINE</option>
													<option value="0">HANYA OFFLINE</option>
													
												</select>
											</div>
										</div>
										
									</div>
									<div class="col-md-12">
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-2 control-label" for="idprespektif"></label>
											<div class="col-md-10">
												<button class="btn btn-success text-uppercase"  type="button" name="btn_save_kuota" id="btn_save_kuota"><i class="fa fa-save"></i> Simpan</button>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_close() ?>
							</div>
							<div class="tab-pane fade fade-left" id="btabs-animated-slideleft-note">
								<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
								<div class="row">
									<div class="col-md-12">
										
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Notes</label>
											<div class="col-md-10">
												<input class="form-control input-sm " type="text" id="catatan" name="catatan" value=""/>	
											</div>
										</div>
										
									</div>
									<div class="col-md-12">
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-2 control-label" for="idprespektif"></label>
											<div class="col-md-10">
												<button class="btn btn-success text-uppercase"  type="button" name="btn_save_catatan" id="btn_save_catatan"><i class="fa fa-save"></i> Simpan</button>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_close() ?>
							</div>
						
							<div class="tab-pane fade fade-left " id="btabs-animated-slideleft-list">
								<h5 class="font-w300 push-15" id="lbl_list_rekening">History Notes </h5>
								<table class="table table-bordered table-striped" id="index_Catatan">
									<thead>
										<th style="width:5%">No</th>
										<th style="width:60%">Notes</th>
										<th style="width:35%">Posted By</th>
									</thead>
									<tbody></tbody>
								  
								</table>
							</div>
							
							
						</div>
					</div>
                  </div>
                </div>
                <div class="modal-footer" style="margin-top:20px">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>	
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
$(document).ready(function(){	
	load_index();
})	
function load_index(){
	var idpoli=$("#idpoli").val();
	$("#index_jadwal tbody").empty();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/load_template_jadwal_poli/'+idpoli,
		dataType: "json",
		success: function(data) {
			$("#index_jadwal tbody").append(data.tabel);
			$("#cover-spin").hide();
		}
	});

}
function hapus_kuota($idpoli,$kodehari,$jam_id){
	// alert($kodehari);
	swal({
			title: "Apakah Anda Yakin ?",
			text : "Menghapus Jadwal ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}app_setting/hapus_kuota/',
				method: "POST",
				dataType: "JSON",
				data: {
					idpoli:$idpoli,
					kodehari:$kodehari,
					jam_id:$jam_id
				},
				success: function(data) {
					load_index();
					$("#cover-spin").hide();
					
				}
			});
		});

}

function add_kuota($idpoli,$kodehari,$jam_id){
	$("#kodehari").val($kodehari);
	$("#jam_id").val($jam_id);
	$("#modal_edit").modal('show');
	get_kuota($idpoli,$kodehari,$jam_id);
	
}
function get_kuota($idpoli,$kodehari,$jam_id){
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/get_kuota/',
		method: "POST",
		dataType: "JSON",
		data: {
			idpoli:$idpoli,
			kodehari:$kodehari,
			jam_id:$jam_id
		},
		success: function(data) {
			$("#jam").val(data.jam);
			$("#namahari").val(data.namahari);
			$("#catatan").val(data.catatan);
			$("#kuota").val(data.kuota).trigger('change');
			$("#st_reservasi_online").val(data.st_reservasi_online).trigger('change');
			$("#cover-spin").hide();
			load_catatan();
		}
	});
}
$("#btn_save_kuota").click(function() {
	$idpoli=$("#idpoli").val();
	$kodehari=$("#kodehari").val();
	$jam_id=$("#jam_id").val();
	$kuota=$("#kuota").val();
	$st_reservasi_online=$("#st_reservasi_online").val();
	// alert($idpoli+' '+$kodehari+' '+$jam_id);
	$.ajax({
		url: '{site_url}app_setting/save_kuota/',
		method: "POST",
		dataType: "JSON",
		data: {
			idpoli:$idpoli,
			kodehari:$kodehari,
			jam_id:$jam_id,
			kuota:$kuota,
			st_reservasi_online:$st_reservasi_online,
		},
		success: function(data) {
			$("#modal_edit").modal('hide');
			load_index();
		}
	});
});
$("#btn_save_catatan").click(function() {
	$idpoli=$("#idpoli").val();
	$kodehari=$("#kodehari").val();
	$jam_id=$("#jam_id").val();
	$catatan=$("#catatan").val();
	$.ajax({
		url: '{site_url}app_setting/save_catatan/',
		method: "POST",
		dataType: "JSON",
		data: {
			idpoli:$idpoli,
			kodehari:$kodehari,
			jam_id:$jam_id,
			catatan:$catatan
		},
		success: function(data) {
			$("#modal_edit").modal('hide');
			load_index();
		}
	});
});
function load_catatan(){
	$idpoli=$("#idpoli").val();
	$kodehari=$("#kodehari").val();
	$jam_id=$("#jam_id").val();
	$('#index_Catatan').DataTable().destroy();	
	table = $('#index_Catatan').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "5%", "targets": 0,  className: "text-right" },
					{ "width": "60%", "targets": [1],  className: "text-left" },
					{ "width": "35%", "targets": [2],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}app_setting/load_catatan', 
                type: "POST" ,
                dataType: 'json',
				data : {
					idpoli:$idpoli,
						kodehari:$kodehari,
						jam_id:$jam_id,
					   }
            }
        });
}
</script>