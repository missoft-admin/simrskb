<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<style>
	/* kalendar */
	table.kalendar {
        width: 100%;
		border-left: 1px solid #e6e9ea;
	}

	tr.kalendar-row {}

	td.kalendar-day {
		min-height: 80px;
		font-size: 11px;
		position: relative;
	}

	* html div.kalendar-day {
		height: 80px;
	}

	td.kalendar-day:hover {
		background: #eceff5;
	}

	td.kalendar-day-np {
		background: #fdfdfd;
		min-height: 80px;
	}

	* html div.kalendar-day-np {
		height: 80px;
	}

	td.kalendar-day-head {
        width: calc(100% / 7);
        border-right: 1px solid #2c7aca;
        padding: 5px;
        text-transform: uppercase;
        font-size: 12px;
        font-weight: bold;
        color: #818589;
        color: #fff;
        background-color: #448cd6;
	}

    td.kalendar-day-head:nth-child(7) {
        border: none;
    }

	div.day-number {
        padding: 8px;
        font-weight: bold;
        color: #7c878d;
        cursor: pointer;
        min-height: 100px;
	}

	div.day-number-mini {
        padding: 8px;
        font-weight: bold;
        color: #7c878d;
        cursor: pointer;
	}

	/* shared */
	td.kalendar-day,
	td.kalendar-day-np {
		width: 120px;
		padding: 5px;
		border-bottom: 1px solid #e6e9ea;
		border-right: 1px solid #e6e9ea;
	}

	bg-isi {
		background-color: #ffcac9;
	}
	.kalendar-event {
		margin-top: 10px;
	}

	.kalendar-event a {
        font-weight: 500;
        padding: 3px 6px;
        border-radius: 4px;
        background-color: #ce5151;
        color: #fff;
        word-wrap: break-word;
	}

	.kalendar-event a.FULL {
        background-color: #d90234;
	}

	.kalendar-event a.AVAILABLE {
        background-color: #1db573;
	}

	.kalendar-event a.PUBLISH {
        background-color: #518fce;
	}

	.kalendar-event a.SELESAI {
        background-color: #51ce57;
	}

	.kalendar-event a.DIBATALKAN {
        background-color: #ce5151;
	}

    div.scroll-content {
        overflow-x: auto;
        white-space: nowrap;
    }
    div.scroll-content [class*="col"]{
        display: inline-block;
        float: none;
    }
</style>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li><a href="{base_url}app_setting/reservasi" title="Kembali"><i class="fa fa-reply"></i></a></li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
		
		<input class="form-control input-sm " readonly type="hidden" id="xjam_id" name="xjam_id" value="{jam_id}"/>	
		<div class="block-content tab-content">
			<div class="tab-pane fade fade-left in active" id="btabs-animated-slideleft-kuota0">
				<?php echo form_open('#','class="form-horizontal" id="form-work"') ?>
				<input class="form-control input-sm " readonly type="hidden" id="tanggal" name="tanggal" value="{tanggal}"/>	
				<div class="row">
					<div class="col-md-3">
						<div class="form-group" style="margin-bottom: 5px;">
							<div class="col-md-12">
								<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<?foreach($list_poli as $r){?>
									<option value="<?=$r->idpoli?>" <?=($idpoli==$r->idpoli?'selected':'')?>><?=$r->nama?></option>
									<?}?>
								</select>
							</div>
						</div>
						
					</div>
					<div class="col-md-2">
						<div class="form-group" style="margin-bottom: 5px;">
							<div class="btn-group btn-group-justified">
								<div class="btn-group">
									<button class="btn btn-default" id="btn_back" type="button"><i class="fa fa-arrow-left"></i></button>
								</div>
								
								<div class="btn-group">
									<button class="btn btn-default" id="btn_next" type="button"><i class="fa fa-arrow-right"></i></button>
								</div>
								
							</div>
						</div>
						
					</div>
					<div class="col-md-4">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="control-label" for="edit_vendor"></label>
						</div>
						
					</div>
					<div class="col-md-3">
						<div class="form-group" style="margin-bottom: 5px;">
							
						</div>
						
					</div>
					
				</div>
				<div class="row push-20-t">
					<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<h4 class="text-center text-uppercase">DETAIL SCHEDULE BOOKING</h4>
						<h4 class="text-center text-uppercase" id="nama_periode"></h4>
						
					</div>
					</div>
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table table-bordered table-condensed" id="index_jadwal">
									<thead>
										<tr>
											<th width="5%" class="text-center">No</th>
											<th width="10%" class="text-center">Time</th>
											<th width="85%" class="text-left" id="nama_periode2"></th>
											
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
						
					</div>
				</div>
				<?php echo form_close() ?>
			</div>
			
			
		</div>
</div>
<div class="modal" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width:50%">
        <form class="form-horizontal" action="javascript:void(0)" id="frm1">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">Detail Slot</h3>
                    </div>
                    <div class="block-content">
						<div class="block">
						<ul class="nav nav-tabs" data-toggle="tabs" id="tabs">
							<li class="active" id="tab_kuota">
								<a href="#btabs-animated-slideleft-kuota"><i class="fa fa-pencil"></i> Pengaturan Slot</a>
							</li>
							<li id="tab_add">
								<a href="#btabs-animated-slideleft-note"><i class="fa fa-pencil"></i> Update Notes</a>
							</li>
							
						</ul>
						<div class="block-content tab-content">
							<div class="tab-pane fade fade-left in active" id="btabs-animated-slideleft-kuota">
								<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Hari</label>
											<div class="col-md-10">
												<input class="form-control input-sm " readonly type="text" id="namahari" name="namahari" value=""/>	
												<input class="form-control input-sm " readonly type="hidden" id="kodehari" name="kodehari" value=""/>	
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Jam</label>
											<div class="col-md-10">
												<input class="form-control input-sm " readonly type="text" id="jam" name="jam" value=""/>	
												<input class="form-control input-sm " readonly type="hidden" id="jam_id" name="jam_id" value=""/>	
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="tipeid">Kuota</label>
											<div class="col-md-10">
												<select id="kuota" name="kuota" class="form-control js-select2" style="width: 100%;" data-placeholder="Choose one..">
													<option value="999">TANPA BATAS</option>
													<option value="0" selected>ISI PENUH</option>
													<?for ($x = 1; $x <= 100; $x++) {?>
														<option value="<?=$x?>"><?=$x?></option>
													<?}?>
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="tipeid">Reservasi Online</label>
											<div class="col-md-10">
												<select id="st_reservasi_online" name="st_reservasi_online" class="form-control js-select2" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1">RESERVASI ONLINE</option>
													<option value="0">HANYA OFFLINE</option>
													
												</select>
											</div>
										</div>
										
									</div>
									<div class="col-md-12">
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-2 control-label" for="idprespektif"></label>
											<div class="col-md-10">
												<button class="btn btn-success text-uppercase"  type="button" name="btn_save_kuota" id="btn_save_kuota"><i class="fa fa-save"></i> Simpan</button>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_close() ?>
							</div>
							<div class="tab-pane fade fade-left" id="btabs-animated-slideleft-note">
								<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
								<div class="row">
									<div class="col-md-12">
										
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Notes</label>
											<div class="col-md-10">
												<input class="form-control input-sm " type="text" id="catatan" name="catatan" value=""/>	
											</div>
										</div>
										
									</div>
									<div class="col-md-12">
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-2 control-label" for="idprespektif"></label>
											<div class="col-md-10">
												<button class="btn btn-success text-uppercase"  type="button" name="btn_save_catatan" id="btn_save_catatan"><i class="fa fa-save"></i> Simpan</button>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_close() ?>
							</div>
						
							
							
							
						</div>
					</div>
                  </div>
                </div>
                <div class="modal-footer" style="margin-top:20px">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>	
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
var st_plus_minus;
$(document).ready(function(){	
	load_index();
	// hasil_generate_jadwal();
})	
function load_index(){
	var jam_id=$("#xjam_id").val();
	var idpoli=$("#idpoli").val();
	var tanggal=$("#tanggal").val();
	$("#index_jadwal tbody").empty();
	// alert(jam_id);
	// $("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/load_reservasi_detail/',
		method: "POST",
		dataType: "JSON",
		data: {
			tanggal:tanggal,
			idpoli:idpoli,
			jam_id:jam_id,
		},
		success: function(data) {
			$("#index_jadwal tbody").append(data.tabel);
			$("#nama_periode").html(data.nama_periode);
			$("#nama_periode2").html(data.nama_periode);
			$("#cover-spin").hide();
		}
	});

}

function refresh_tanggal(){
	var tanggal=$("#tanggal").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/refresh_tanggal_detail/',
		method: "POST",
		dataType: "JSON",
		data: {
			tanggal:tanggal,
			st_plus_minus:st_plus_minus,
		},
		success: function(data) {
			$("#tanggal").val(data.tanggal);
			$("#cover-spin").hide();
			load_index();
		}
	});
}
$("#idpoli").change(function() {
	load_index();
});

$("#btn_next").click(function() {
	st_plus_minus='1'
	refresh_tanggal();
	
});
$("#btn_back").click(function() {
	st_plus_minus='-1'
	refresh_tanggal();
	
});
function stop_kuota($idpoli,$kodehari,$jam_id){
	// alert($kodehari);
	$tanggal=$("#tanggal").val();
	$tgl=$("#nama_periode").html();
	$nama_poli=$("#idpoli option:selected").text();
	// alert($nama_poli);
	// return false;
	swal({
			title: "Akan Menghentikan Reservasi "+$nama_poli+ " " + $tgl + "?",
			text : "Stelah menekan Ya maka slot yang tersedia pada tanggal tersebut tidak dapat dipilih diseluruh platform!",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}app_setting/stop_kuota/',
				method: "POST",
				dataType: "JSON",
				data: {
					idpoli:$idpoli,
					tanggal:$tanggal,
					jam_id:$jam_id
				},
				success: function(data) {
					load_index();
					$("#cover-spin").hide();
					
				}
			});
		});

}
function add_kuota($idpoli,$kodehari,$jam_id){
	$("#kodehari").val($kodehari);
	$("#jam_id").val($jam_id);
	$("#modal_edit").modal('show');
	get_kuota($idpoli,$kodehari,$jam_id);
	
}
function get_kuota($idpoli,$kodehari,$jam_id){
	$tanggal=$("#tanggal").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/get_kuota_detail/',
		method: "POST",
		dataType: "JSON",
		data: {
			idpoli:$idpoli,
			tanggal:$tanggal,
			jam_id:$jam_id
		},
		success: function(data) {
			$("#jam").val(data.jam);
			$("#namahari").val(data.namahari);
			$("#catatan").val(data.catatan);
			$("#kuota").val(data.kuota).trigger('change');
			$("#st_reservasi_online").val(data.st_reservasi_online).trigger('change');
			$("#cover-spin").hide();
		}
	});
}
$("#btn_save_kuota").click(function() {
	$idpoli=$("#idpoli").val();
	$tanggal=$("#tanggal").val();
	$jam_id=$("#jam_id").val();
	$kuota=$("#kuota").val();
	$st_reservasi_online=$("#st_reservasi_online").val();
	// alert($idpoli+' '+$kodehari+' '+$jam_id);
	$.ajax({
		url: '{site_url}app_setting/save_kuota_detail/',
		method: "POST",
		dataType: "JSON",
		data: {
			idpoli:$idpoli,
			tanggal:$tanggal,
			jam_id:$jam_id,
			kuota:$kuota,
			st_reservasi_online:$st_reservasi_online,
		},
		success: function(data) {
			$("#modal_edit").modal('hide');
			load_index();
		}
	});
});
$("#btn_save_catatan").click(function() {
	$idpoli=$("#idpoli").val();
	$tanggal=$("#tanggal").val();
	$jam_id=$("#jam_id").val();
	$catatan=$("#catatan").val();
	$.ajax({
		url: '{site_url}app_setting/save_catatan_detail/',
		method: "POST",
		dataType: "JSON",
		data: {
			idpoli:$idpoli,
			tanggal:$tanggal,
			jam_id:$jam_id,
			catatan:$catatan
		},
		success: function(data) {
			$("#modal_edit").modal('hide');
			load_index();
		}
	});
});
</script>