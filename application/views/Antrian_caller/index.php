<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab1"   onclick="set_tab(1)""><i class="glyphicon glyphicon-volume-up"></i> Panggil Antrian</a>
		</li>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab2"   onclick="set_tab(2)""><i class="fa fa-cut"></i> Shortcut</a>
		</li>
		<?if ($st_login){?>
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab3"  onclick="set_tab(3)"><i class="fa fa-mail-forward"></i> Antrian Transfer &nbsp; <span id="info_tf"></span></a>
		</li>
		<?}?>
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab1">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				<input type="hidden" id="st_login" name="st_login" value="{st_login}">
				<input type="hidden" id="st_istirahat" name="st_istirahat" value="{st_istirahat}">
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-2" for="tanggal">Counter</label>
						<div class="col-md-10">
							<div class="input-group">
								<select id="counter_id" name="counter_id" <?=($st_login=='1'?'disabled':'')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" <?=($st_login=='0'?'selected':'')?>>- Pilih Counter -</option>
									<?foreach($list_counter as $row){?>
									<option value="<?=$row->id?>" <?=($user_id==$row->user_login?'selected':'')?>><?=$row->nama_counter?></option>
									<?}?>
								</select>
								<span class="input-group-btn ">
									<button class="btn btn-primary" id="btn_login" type="button" title="Login"><i class="si si-login"></i> LogIn</button>&nbsp;
									<button class="btn btn-danger" type="button" title="Logout"  id="btn_logout"><i class="si si-logout"></i> Logout</button>&nbsp;
									<button class="btn btn-success" type="button" title="Istirahat"  id="btn_istirahat"><i class="fa fa-hand-stop-o"></i> Istirahat</button>&nbsp;
									<button class="btn btn-success" type="button" title="Istirahat"  id="btn_buka_istirahat"><i class="si si-login"></i> Selesai Istirahat</button>&nbsp;
									<button class="btn btn-default" type="button"  id="btn_info" title="Informasi"><i class="si si-info"></i></button>
								</span>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px;">
						<div class="col-md-12">
							<?if ($st_login){?>
								<?if ($st_istirahat=='0'){?>
									<div class="alert alert-success alert-dismissable">
										<?=$this->session->userdata('user_name')?> Anda sedang Login Di <?=$nama_counter?>
										
									</div>
								<?}else{?>
									<div class="alert alert-warning alert-dismissable">
										<?=$this->session->userdata('user_name')?>,  <?=$nama_counter?> Sedang Istirahat
										
									</div>
								<?}?>
							<?}else{?>
								<div class="alert alert-danger alert-dismissable">
									<?=$this->session->userdata('user_name')?> Anda Belum Login, Silahkan Pilih Counter yang tersedia
								</div>
							<?}?>
						</div>
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			<div class="row   ">
				<div class="col-md-10">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="index_panggil">
							<thead>
								<tr>
									<th width="10%">NO</th>
									<th width="10%">NAMA PELAYANAN</th>
									<th width="10%">STATUS</th>
									<th width="10%">TOTAL ANTRIAN</th>
									<th width="10%">SISA ANTRIAN</th>
									<th width="10%">NO ANTRIAN DILAYANI</th>
									<th width="10%">NO ANTRIAN BERIKUTNYA</th>
									<th width="15%">ACTION</th>
								   
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
				<div class="col-md-2 div_counter">
					
				</div>
			</div>
		</div>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab2">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
			</div>
			<?php echo form_close() ?>
			
		</div>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?>" id="tab3">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				<div class="col-md-6">
					<select id="st_panggil" name="st_panggil" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#">- Semua -</option>
						<option value="1">Sudah Dilayani</option>
						<option value="0" selected>Belum Dilayani</option>
						
					</select>
					<input type="hidden" id="st_istirahat2" value="{st_istirahat}">
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px;">
						<div class="col-md-12">
							<?if ($st_login){?>
								<?if ($st_istirahat=='0'){?>
									<div class="alert alert-success alert-dismissable">
										<?=$this->session->userdata('user_name')?> Anda sedang Login Di <?=$nama_counter?>
										
									</div>
								<?}else{?>
									<div class="alert alert-warning alert-dismissable">
										<?=$this->session->userdata('user_name')?>,  <?=$nama_counter?> Sedang Istirahat
										
									</div>
								<?}?>
							<?}else{?>
								<div class="alert alert-danger alert-dismissable">
									<?=$this->session->userdata('user_name')?> Anda Belum Login, Silahkan Pilih Counter yang tersedia
								</div>
							<?}?>
						</div>
					</div>
				</div>
				
			</div>
			<div class="row">
				<div class="col-md-10">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="index_tf">
							<thead>
								<tr>
									<th width="10%">NO</th>
									<th width="10%">NO ANTRIAN</th>
									<th width="10%">TRANSFER DARI</th>
									<th width="10%">TRANSFER OLEH</th>
									<th width="10%">STATUS</th>
									<th width="10%">NOTES</th>
									<th width="15%">ACTION</th>
								   
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
				<div class="col-md-2 div_counter">
					
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		
	</div>
</div>
<div class="modal" id="modal_tf" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Transfer Antrian</h3>
				</div>
				<div class="block-content">
					<div class="row">
                        <input type="hidden" id="tf_antrian_harian_id" value="">

						<div class="col-md-12"> 
							<div class="row">
								<div class="form-group" style="margin-bottom: 40px;">
									<div class="col-md-12"> 
										<h2 class="text-center">TRANSFER ANTRIAN</h2>
									</div>
									<div class="col-md-12"> 
										<h2 class="text-center" id="tmp_antrian_dilayani">NO ANTRIAN : </h2>
									</div>
								</div>
							</div>
							
						</div>
						<div class="col-md-12"> 
							<div class="row">
								<div class="form-group">
									<label class="col-md-12 control-label">Tujuan Counter</label>
									<div class="col-md-12"> 
										<select id="counter_id_tujuan" name="counter_id_tujuan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="#">- Pilih Counter -</option>
											<?foreach($list_counter2 as $row){?>
											<option value="<?=$row->id?>" <?=($user_id==$row->user_login?'disabled':'')?>><?=$row->nama_counter?></option>
											<?}?>
										</select>
									</div>
									
								</div>
								<div class="form-group pull-t">
									
									<label class="col-md-12 control-label">Notes</label>
									<div class="col-md-12"> 
										<textarea id="tf_catatan" class="form-control" name="tf_catatan" rows="3"></textarea>
									</div>
								</div>
							</div>
							
						</div>
						<div class="col-md-12">                               
							<br>
						</div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-warning" type="button" onclick="simpan_tf()" id="btn-tf"><i class="fa fa-mail-forward"></i> Transfer</button>
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="modal_panggil" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog modal-lg" style="width:50%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">MENU SHORTCUT PELAYANAN</h3>
				</div>
				<div class="block-content">
					<div class="row">
                        <input type="hidden" id="tmp_antrian_harian_id" value="">
                        <input type="hidden" id="tmp_pelayanan_id" value="">
                        <input type="hidden" id="tmp_kodeantrian" value="">

						<div class="col-md-12"> 
							<div class="row">
								<div class="form-group" style="margin-bottom: 40px;">
									<div class="col-md-12"> 
										<h4 class="text-center">PELAYANAN PENDAFTARAN POLIKLINIK</h4>
									</div>
									<div class="col-md-12"> 
										<h4 class="text-center" id="tmp_noantrian">NO ANTRIAN : </h4>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group" style="margin-bottom: 40px;">
									
								</div>
							</div>
							<div class="row">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-4">
										<a class="block block-link-hover3 text-center " id="link_rajal" onclick="daftar_rajal()"  href="javascript:void(0)" target="_blank">
											<div class="block-content block-content-full bg-primary" style="border-bottom: 1px solid white">
												<img src="./assets/icon/01_rajal.png" title="Pend. Rawat Jalan" width="100" height="100">
											</div>
											<div class="block-content block-content-full block-content-mini bg-primary text-white font-w600">Pend. Rawat Jalan</div>
										</a>
									</div>
									<div class="col-md-4">
										<a class="block block-link-hover3 text-center " id="link_ranap" onclick="daftar_ranap()"  href="javascript:void(0)" >
											<div class="block-content block-content-full bg-primary" style="border-bottom: 1px solid white">
												<img src="./assets/icon/02_ranap.png" title="Pend. Rawat Inap" width="100" height="100">
											</div>
											<div class="block-content block-content-full block-content-mini bg-primary text-white font-w600">Pend. Rawat Inap</div>
										</a>
									</div>
									<div class="col-md-4">
										<a disabled class="block block-link-hover3 text-center " href="javascript:void(0)">
											<div class="block-content block-content-full bg-gray" style="border-bottom: 1px solid white">
												<img src="./assets/icon/25_laporan_stok.png" title="Informasi Medis" width="100" height="100">
											</div>
											<div class="block-content block-content-full block-content-mini bg-gray text-white font-w600">Informasi Medis</div>
										</a>
									</div>
								</div>
							</div>
							<div class="row text-center">
								
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="col-md-3">
										<a class="block block-link-hover3 text-center link_checkin" onclick="set_checkin()" href="javascript:void(0)">
											<div class="block-content block-content-full bg-primary" style="border-bottom: 1px solid white">
												<img src="./assets/icon/16_verif_barang.png" title="Checkin Dokter" width="100" height="100">
											</div>
											<div class="block-content block-content-full block-content-mini bg-primary text-white font-w600">Checkin Dokter</div>
										</a>
									</div>
									<div class="col-md-3">
										<a class="block block-link-hover3 text-center  link_checkin"  onclick="set_checkin()"  href="javascript:void(0)">
											<div class="block-content block-content-full bg-primary" style="border-bottom: 1px solid white">
												<img src="./assets/icon/05_fis.png" title="Checkin Rehabilitas" width="100" height="100">
											</div>
											<div class="block-content block-content-full block-content-mini bg-primary text-white font-w600">Checkin Rehabilitas</div>
										</a>
									</div>
									<div class="col-md-3">
										<a class="block block-link-hover3 text-center " href="{base_url}tbooking/add_reservasi" target="_blank">
											<div class="block-content block-content-full bg-primary" style="border-bottom: 1px solid white">
												<img src="./assets/icon/20_pinjam.png" title="Reservasi Dokter" width="100" height="100">
											</div>
											<div class="block-content block-content-full block-content-mini bg-primary text-white font-w600">Reservasi Dokter</div>
										</a>
									</div>
									<div class="col-md-3">
										<a class="block block-link-hover3 text-center " href="{base_url}tbooking_rehab/add_reservasi" target="_blank">
											<div class="block-content block-content-full bg-primary" style="border-bottom: 1px solid white">
												<img src="./assets/icon/08_ko.png" title="Reservasi Rehabilitas" width="100" height="100">
											</div>
											<div class="block-content block-content-full block-content-mini bg-primary text-white font-w600">Reservasi Rehabilitas</div>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12"> 
							<div class="row">
								<div class="form-group" style="margin-bottom: 40px;">
									
								</div>
							</div>
							<div class="row text-center">
								<div class="form-group" style="margin-bottom: 10px;">
									<div class="btn-group btn-group-lg" role="group">
										<button class="btn btn-success" onclick="call_antrian_manual()" title="Lanjut Panggil" type="button"><i class="fa fa-bell-o"></i></button>
										<button class="btn btn-primary" type="button" onclick="recall_manual()" title="Panggil Ulang"><i class="si si-refresh"></i></button>
										<button class="btn btn-default" type="button" onclick="lewati_manual()"  title="Lewati Antrian"><i class="fa fa-step-forward"></i></button>
										<button class="btn btn-warning" type="button" onclick="tf_manual()"  title="Transfer Antrian"><i class="fa fa-mail-forward"></i></button>
										<button class="btn btn-danger" type="button" onclick="hapus_manual()"  title="Hapus Antrian"><i class="fa fa-trash"></i></button>
									</div>
								</div>
							</div>
						
						</div>
						<div class="col-md-12">                               
							<br>
						</div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="modal_checkin" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button onclick="close_checkin()" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Checkin</h3>
				</div>
				
				<div class="block-content">
					<div class="row">
						<div class="col-md-12"> 
							<div class="row">
								<div class="form-group" style="margin-bottom: 40px;">
									<div class="col-md-12"> 
										<h2 class="text-center">CHECKIN</h2>
									</div>
									<div class="col-md-12"> 
										<h5 class="text-center" id="lbl_no_antrian_checkcin">NO ANTRIAN : </h5>
									</div>
								</div>
							</div>
							
						</div>
						<hr>
						<div class="col-md-12"> 
							<div class="row">
								<div class="form-group">
									<label class="col-md-12 control-label">Scan / Masukan Kode Reservasi</label>
									<div class="col-md-12"> 
										<div class="input-group">
											<input type="text" class="form-control" id="tmp_kode_reservasi" placeholder="Kode Reservasi" name="tmp_kode_reservasi" value="">
											<span class="input-group-btn">
												<button class="btn btn-warning" type="button" id="btn_cari_checkin"><i class="fa fa-search"></i> Cari</button>
												
											</span>
										</div>
									</div>
									
								</div>
								
							</div>
							
						</div>
						<div class="col-md-12">                               
							<br>
						</div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" onclick="close_checkin()" >Close</button>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
var tab_pilih=0;
var st_login=0;
var jumlah_tf=0;
$(document).ready(function(){	
	// $("#info_tf").html('SIAL');
	let tab=$("#tab").val();
	st_login=$("#st_login").val();
	set_tab(tab);
	cek_login();
		load_index_panggil();
		load_index_tf();
	
	startWorker();
	// $("#modal_panggil").modal('show');
	// set_checkin();
	

})	
function close_checkin(){
	$("#modal_panggil").modal('show');
	$("#modal_checkin").modal('hide');
	
}
$(document).on("click","#btn_cari_checkin",function(){
	cari_kode_booking();
});
$("#tmp_kode_reservasi").keyup(function (e) {
    if (e.which == 13) {
        cari_kode_booking();
    }
 });
function cari_kode_booking(){
	//PWV45Z4
	let kode_booking=$("#tmp_kode_reservasi").val();
	let antrian_id=$("#tmp_antrian_harian_id").val();
	 $.ajax({
		url: '{site_url}antrian_caller/cari_kode_booking',
		type: 'POST',
		dataType: "json",
		data: {
			kode_booking: kode_booking,
		
		},
		success: function(data) {
			if (data==null){
				// sweetAlert("Maaf...", "Kode Booking Tidak Ditemukan!", "error");
				swal({
					title: "Maaf!",
					text: "Kode Booking Tidak Ditemukan.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});

			}else{
				window.open('{site_url}tcheckin/detail/'+data.id+'/'+antrian_id,'_blank');

			}
		}
	});
}
function set_checkin(){
	$("#modal_panggil").modal('hide');
	$("#modal_checkin").modal({backdrop: 'static', keyboard: false}).show();
	$("#lbl_no_antrian_checkcin").text('NO ANTRIAN : '+$("#tmp_kodeantrian").val());
	$("#tmp_kode_reservasi").val('');
	$("#tmp_kode_reservasi").focus();
}
function daftar_rajal(){
	let antrian_id=$("#tmp_antrian_harian_id").val();
	 window.open('<?=base_url()?>tpendaftaran_poli/add_daftar/'+antrian_id, '_blank');
}
function daftar_ranap(){
	let antrian_id=$("#tmp_antrian_harian_id").val();
	 window.open('<?=base_url()?>tpendaftaran_ranap/add_daftar/'+antrian_id, '_blank');
}
function set_tab(tab){
	tab_pilih=tab;
}
function simpan_tf(){
	if ($("#counter_id_tujuan").val()=='#'){
		sweetAlert("Maaf...", "Tentukan Counter Tujuan!", "error");
		return false;
	}
	if ($("#tf_catatan").val()==''){
		sweetAlert("Maaf...", "Tentukan Notes!", "error");
		return false;
	}
	let counter_id=$("#counter_id_tujuan").val();
	let catatan=$("#tf_catatan").val();
	let antrian_harian_id=$("#tf_antrian_harian_id").val();
	
	 $.ajax({
			url: '{site_url}antrian_caller/simpan_tf',
			type: 'POST',
			dataType: "json",
			data: {
					counter_id: counter_id,
					catatan: catatan,
					antrian_harian_id: antrian_harian_id,
				},
			success: function(data) {
				$('#modal_tf').modal('hide');
				swal("Berhasil!", "Transfer Berhasil", "success");
			}
		});
}

function startWorker() {
	if(typeof(Worker) !== "undefined") {
		if(typeof(w) == "undefined") {
			w = new Worker("{site_url}assets/js/worker.js");
	
		}
		w.postMessage({'cmd': 'start' ,'msg': 3000});
		w.onmessage = function(event) {
			console.log(st_login);
			if (st_login=='1'){
				load_jumlah_transfer();
			}
			if (tab_pilih==1){
				$('#index_panggil').DataTable().ajax.reload( null, false );
				refres_div_counter();
			}
			if (tab_pilih==3){
				$('#index_tf').DataTable().ajax.reload( null, false );
				refres_div_counter();
			}
			
		};
		w.onerror = function(event) {
			window.location.reload();
		};
	} else {
		alert("Sorry, your browser does not support Autosave Mode");
		$("#autosave_status").html('<button class="btn btn-warning"><i class="fa fa-warning"></i> Autosave not running!</button>');
	}
}
function cek_login(){
	let st_login=$('#st_login').val();
	let st_istirahat=$('#st_istirahat').val();
	let counter_id=$('#counter_id').val();
	if (st_login=='1'){
		$('#btn_login').hide();
		if (st_istirahat=='1'){
			$('#btn_istirahat').hide();			
			$('#btn_logout').hide();			
			$('#btn_buka_istirahat').show();			
		}else{
			$('#btn_buka_istirahat').hide();			
			$('#btn_istirahat').show();			
			$('#btn_logout').show();			
		}
	}else{
		$('#btn_login').show();
		$('#btn_istirahat').hide();
		$('#btn_logout').hide();
		$('#btn_buka_istirahat').hide();			
		if ($("#counter_id").val()=='#'){
			$('#btn_login').attr('disabled', 'disabled');
			$('#btn_istirahat').attr('disabled', 'disabled');
		}else{
			$('#btn_login').removeAttr("disabled");
			$('#btn_istirahat').removeAttr("disabled");
			
		}
	}
	
}
$(document).on("change","#counter_id",function(){
	cek_login();
});
$(document).on("change","#st_panggil",function(){
	load_index_tf();
});
$(document).on("click","#btn_login",function(){
	let nama_counter=$("#counter_id option:selected").text();
	let counter_id=$("#counter_id").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Login Menggunakan "+nama_counter+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}antrian_caller/login',
				type: 'POST',
				dataType: "json",
				data: {counter_id: counter_id},
				success: function(data) {
					location.reload();
				}
			});
	});
});
$(document).on("click","#btn_istirahat",function(){
	set_istirahat();
});
function set_istirahat(){
	let nama_counter=$("#counter_id option:selected").text();
	let counter_id=$("#counter_id").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Istirahat "+nama_counter+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}antrian_caller/set_istirahat',
				type: 'POST',
				dataType: "json",
				data: {counter_id: counter_id},
				success: function(data) {
					location.reload();
				}
			});
	});
}
function buka_antrian(antrian_id){
	swal({
		title: "Anda Yakin ?",
		text : "Buka Tiket Antrian ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}antrian_caller/buka_antrian',
				type: 'POST',
				dataType: "json",
				data: {antrian_id: antrian_id},
				success: function(data) {
					location.reload();
				}
			});
	});
}
function stop_antrian(antrian_id){
	swal({
		title: "Anda Yakin ?",
		text : "Buka Tiket Antrian ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}antrian_caller/stop_antrian',
				type: 'POST',
				dataType: "json",
				data: {antrian_id: antrian_id},
				success: function(data) {
					location.reload();
				}
			});
	});
}
$(document).on("click","#btn_buka_istirahat",function(){
	let nama_counter=$("#counter_id option:selected").text();
	let counter_id=$("#counter_id").val();
	swal({
		title: "Anda Yakin ?",
		text : "Buka "+nama_counter+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}antrian_caller/buka_istirahat',
				type: 'POST',
				dataType: "json",
				data: {counter_id: counter_id},
				success: function(data) {
					location.reload();
				}
			});
	});
});
$(document).on("click","#btn_logout",function(){
	let nama_counter=$("#counter_id option:selected").text();
	let counter_id=$("#counter_id").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Logout "+nama_counter+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}antrian_caller/logout',
				type: 'POST',
				dataType: "json",
				data: {counter_id: counter_id},
				success: function(data) {
					location.reload();
				}
			});
	});
});
function call_antrian_manual(){
	let antrian_id=$("#tmp_pelayanan_id").val();
	let counter_id=$("#counter_id").val();
	call_antrian(antrian_id);
	// $("#cover-spin").show();
	 // $.ajax({
		// url: '{site_url}antrian_caller/call_antrian',
		// type: 'POST',
		// dataType: "json",
		// data: {
			// counter_id: counter_id,
			// antrian_id: antrian_id,
		
		// },
		// success: function(data) {
			// $("#cover-spin").hide();
			// $('#index_panggil').DataTable().ajax.reload( null, false );
		// }
	// });
}
function call_antrian(antrian_id){
	let counter_id=$("#counter_id").val();
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}antrian_caller/call_antrian',
		type: 'POST',
		dataType: "json",
		data: {
			counter_id: counter_id,
			antrian_id: antrian_id,
		
		},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==false){
				swal({
					title: "Maaf!",
					text: "No Antrian Tidak tersedia",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				// alert(data.antrian_harian_id);
				$("#tmp_antrian_harian_id").val(data.antrian_harian_id);
				$("#tmp_pelayanan_id").val(data.pelayanan_id);
				$("#tmp_kodeantrian").val(data.kodeantrian);
				$("#cover-spin").hide();
				$('#index_panggil').DataTable().ajax.reload( null, false );
				show_short_cut();
			}
		}
	});
}
function show_short_cut(){
	
	$("#modal_panggil").modal('show');
	$("#tmp_noantrian").text('NO ANTRIAN : '+$("#tmp_kodeantrian").val());
}
function show_short_cut2(pelayanan_id){
	let counter_id=$("#counter_id").val();
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}antrian_caller/show_short_cut2',
		type: 'POST',
		dataType: "json",
		data: {
			counter_id: counter_id,
			pelayanan_id: pelayanan_id,
			// antrian_harian_id: antrian_harian_id,
		
		},
		success: function(data) {
			$("#cover-spin").hide();
			if (data!=false){
				$("#tmp_antrian_harian_id").val(data.antrian_harian_id);
				$("#tmp_pelayanan_id").val(data.pelayanan_id);
				$("#tmp_kodeantrian").val(data.kodeantrian);
				show_short_cut();
			}else{
				
			}
		}
	});
	
}
function call_antrian_tf(antrian_harian_id){
	let counter_id=$("#counter_id").val();
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}antrian_caller/call_antrian_tf',
		type: 'POST',
		dataType: "json",
		data: {
			counter_id: counter_id,
			antrian_harian_id: antrian_harian_id,
		
		},
		success: function(data) {
			$("#cover-spin").hide();
			$('#index_panggil').DataTable().ajax.reload( null, false );
		}
	});
}
function recall_manual(){
	let antrian_harian_id=$("#tmp_antrian_harian_id").val();
	recall_antrian(antrian_harian_id);
}
function recall_antrian(antrian_harian_id){
	let counter_id=$("#counter_id").val();
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}antrian_caller/recall_antrian',
		type: 'POST',
		dataType: "json",
		data: {
			counter_id: counter_id,
			antrian_harian_id: antrian_harian_id,
			
		},
		success: function(data) {
			$("#tmp_antrian_harian_id").val(data.antrian_harian_id);
			$("#tmp_pelayanan_id").val(data.pelayanan_id);
			$("#tmp_kodeantrian").val(data.kodeantrian);
			$("#cover-spin").hide();
			$('#index_panggil').DataTable().ajax.reload( null, false );
			show_short_cut();
		}
	});
}
function lewati_manual(){
	let antrian_harian_id=$("#tmp_antrian_harian_id").val();
	let antrian_id=$("#tmp_pelayanan_id").val();
	skip_antrian(antrian_harian_id,antrian_id);
}
function skip_antrian(antrian_harian_id,antrian_id){
	// alert(antrian_id);return false;
	let counter_id=$("#counter_id").val();
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}antrian_caller/skip_antrian',
		type: 'POST',
		dataType: "json",
		data: {
			antrian_id: antrian_id,
			counter_id: counter_id,
			antrian_harian_id: antrian_harian_id,
		
		},
		success: function(data) {
				$("#cover-spin").hide();
			if (data==false){
				swal({
					title: "Maaf!",
					text: "No Antrian Tidak tersedia",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				$("#tmp_antrian_harian_id").val(data.antrian_harian_id);
				$("#tmp_pelayanan_id").val(data.pelayanan_id);
				$("#tmp_kodeantrian").val(data.kodeantrian);
				$('#index_panggil').DataTable().ajax.reload( null, false );
				show_short_cut();
			}
		}
	});
}
function tf_manual(){
	let antrian_harian_id=$("#tmp_antrian_harian_id").val();
	let antrian_dilayani=$("#tmp_kodeantrian").val();
	transfer_antrian(antrian_harian_id,antrian_dilayani);
	$("#modal_panggil").modal('hide');
	
}
function transfer_antrian(antrian_harian_id,antrian_dilayani){
	$("#counter_id_tujuan").val('#').trigger('change');
	$("#tf_catatan").val('');
	$("#tf_antrian_harian_id").val(antrian_harian_id);
	$("#tmp_antrian_dilayani").html(antrian_dilayani);
	$("#modal_tf").modal({backdrop: 'static', keyboard: false}).show();
	// alert(antrian_id);return false;
	let counter_id=$("#counter_id").val();
	
}
function load_index_panggil(){
	let counter_id=$("#counter_id").val();
	let st_login=$("#st_login").val();
	let st_istirahat=$("#st_istirahat").val();
	$('#index_panggil').DataTable().destroy();
	table = $('#index_panggil').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			
			"processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "4%", "targets": 0, "orderable": false},
					{ "width": "30%", "targets": 1, "orderable": true },
					{ "width": "10%", "targets": 2, "orderable": true },
					{ "width": "5%", "targets": 3, "orderable": true },
					{ "width": "5%", "targets": 4, "orderable": true },
					{ "width": "10%", "targets": 5, "orderable": true },
					{ "width": "10%", "targets": 6, "orderable": true },
					{  className: "text-center", targets:[2,3,4,5,6] },
				],
            ajax: { 
                url: '{site_url}antrian_caller/load_index_panggil', 
                type: "POST" ,
                dataType: 'json',
				data : {
						counter_id:counter_id,
						st_login:st_login,
						st_istirahat:st_istirahat
					   }
            }
        });
}
function load_index_tf(){
	let counter_id=$("#counter_id").val();
	let st_login=$("#st_login").val();
	let st_istirahat=$("#st_istirahat2").val();
	let st_panggil=$("#st_panggil").val();
	$('#index_tf').DataTable().destroy();
	
	// console.log(counter_id);
	table = $('#index_tf').DataTable({
            autoWidth: false,
            searching: true,
            serverSide: true,
			
			"processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "4%", "targets": 0, "orderable": false},
					{ "width": "10%", "targets": 1, "orderable": true },
					{ "width": "10%", "targets": 2, "orderable": true },
					{ "width": "15%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 4, "orderable": true },
					{ "width": "30%", "targets": 5, "orderable": true },
					// { "width": "10%", "targets": 6, "orderable": true },
					{  className: "text-center", targets:[2,3,4] },
				],
            ajax: { 
                url: '{site_url}antrian_caller/load_index_tf', 
                type: "POST" ,
                dataType: 'json',
				data : {
						counter_id:counter_id,
						st_login:st_login,
						st_istirahat:st_istirahat,
						st_panggil:st_panggil
					   }
            }
        });
}
function refres_div_counter(){
	let counter_id=$("#counter_id").val();
	let st_login=$("#st_login").val();
	$.ajax({
		url: '{site_url}antrian_caller/load_counter_layanan',
		type: 'POST',
		dataType: "json",
		data: {
			st_login:st_login,			
			counter_id:counter_id,			
		},
		success: function(data) {
			$(".div_counter").html(data);
			$("#cover-spin").hide();
		}
	});
}
function load_jumlah_transfer(){
	let counter_id=$("#counter_id").val();
	let st_login=$("#st_login").val();
	$.ajax({
		url: '{site_url}antrian_caller/load_jumlah_transfer',
		type: 'POST',
		dataType: "json",
		data: {
			st_login:st_login,			
			counter_id:counter_id,			
		},
		success: function(data) {
			jumlah_tf=data;
			if (jumlah_tf>0){
				$("#info_tf").html('<span class="badge badge-danger pull-right">'+jumlah_tf+'</span>');
				// console.log('sial');
				// $("#info_tf").html('SIAL');
				
			}else{
				$("#info_tf").html('');
				
			}
			console.log(data);
		}
	});
}


</script>