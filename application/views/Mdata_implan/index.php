<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('96'))){ ?>
<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('98','101'))){ ?>
		<ul class="block-options">
        <li>
            <a href="{base_url}mdata_implan/create" class="btn"><i class="fa fa-plus"></i></a>
        </li>
    </ul>
		<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?php if (UserAccesForm($user_acces_form,array('97'))){ ?>
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<hr style="margin-top:0px">
		<?php echo form_open('mdata_implan/filter','class="form-horizontal" id="form-work"') ?>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien">Kategori</label>
					<div class="col-md-8">
						<select name="idkategori" id="idkategori" class="js-select2 form-control" style="width: 100%;" data-placeholder=" - All Kategori - ">
							<option value="0" <?=($idkategori == '0') ? "selected" : "" ?>>- All Kategori -</option>
							<?php foreach  ($list_kategori as $row) { ?>
								<option value="<?=$row->path;?>" <?=($idkategori == $row->path) ? "selected" : "" ?>><?=TreeView($row->level, $row->nama)?></option>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close() ?>
		<hr>
		<?}?>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>Kode</th>
					<th>Nama</th>
					<th>Kategori</th>
					<th>Harga Besar</th>
					<th>Harga Kecil</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		</div>
	</div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		var idkategori=$("#idkategori").val();
		$('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
				url: '{site_url}mdata_implan/getIndex/',
					type: "POST",
					dataType: 'json',
					data: {
						idkategori: idkategori
					}
				},
				"columnDefs": [
					{ "width": "10%", "targets": 0, "orderable": true },
					{ "width": "10%", "targets": 1, "orderable": true },
					{ "width": "25%", "targets": 2, "orderable": true },
					{ "width": "10%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 3, "orderable": true },
					{ "width": "15%", "targets": 4, "orderable": true }
				]
			});
	});
</script>
