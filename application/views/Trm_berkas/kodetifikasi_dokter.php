<div class="row">
	<div class="col-md-12">
		<!-- Static Labels block-opt-hidden -->
		<div class="block block-themed block-opt-hidden" id="div_kodefikasi_dokter">
			<div class="block-header bg-primary">
				<ul class="block-options ">
					<li>
						<button id="button_up_dokter" class="disable read" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="si si-users"></i>  DATA Dokter</h3>
			</div>
			<div class="block-content block-content">
				<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
					<input class="form-control"  value="0" type="hidden" readonly id="st_load_dokter">
					<input class="form-control"  value="" type="hidden" readonly id="id_edit_dokter">
					<div class="form-group">
						<div class="col-sm-6">
							<div class="form-material">
								<select name="iddokter_perujuk" id="iddokter_perujuk" tabindex="16" style="width: 100%"  data-placeholder="Dokte Perujuk" class="js-select2 disable">
									<option value="#" selected>- Dokter Perujuk -</option>
									<?foreach($list_dokter as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?> </option>
									<?}?>
								</select>
								<label for="material-info" class="has-info">Dokter Perujuk</label>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-material">
								<select name="iddokter" id="iddokter" tabindex="16" style="width: 100%"  data-placeholder="Dokter" class="js-select2 disable">
									<option value="#" selected>- Dokter DPJP -</option>
									<?foreach($list_dokter as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?> </option>
									<?}?>
								</select>
								<label for="material-info">Dokter DPJP</label>
							</div>
						</div>
						<div class="col-sm-12" style="margin-bottom: 15px;">
							<h5><?=text_success('Dokter Konsulen')?></h5>
						</div>
						
						<div class="col-sm-12">
							<table class="table table-bordered table-striped" id="tabel_dokter" style="width: 100%;">
								<thead>
									<tr>
										<th style="width:50%" class="text-center">Dokter</th>
										<th style="width:30%" class="text-center">Detail</th>
										<th style="width:20%" class="text-center">Aksi</th>
										<th style="display:none" class="text-center"></th>								
										<th style="display:none" class="text-center"></th>								
									</tr>
									<tr>
										<td class="text-center">
											<div class="input-group">
												<select name="iddokter_add" id="iddokter_add" tabindex="16" style="width: 100%"  data-placeholder="Cari Dokter" class="js-select2 form-control input-sm disable">
													<option value="#" selected>- Dokter Konsulen -</option>
													<?foreach($list_dokter as $row){?>
														<option value="<?=$row->id?>"><?=$row->nama?> </option>
													<?}?>
												</select>
												<span class="input-group-btn">
													<button id="btn_cari_dokter" tabindex="17" class="btn btn-info push-5-r push-10 disable" type="button" ><i class="fa fa-search"></i></button>
												</span>
											</div>
										</td>
										<td class="text-center">
												
										</td>
										
										<td class="text-center">		
											<div class="btn-group" role="group">
											<button type="button" class="btn btn-sm btn-primary disable" tabindex="8" id="add_dokter" title="Masukan Item"><i class="fa fa-plus"></i></button>&nbsp;&nbsp;
											<button type="button" class="btn btn-sm btn-danger disable" tabindex="9" id="cancel_dokter" title="Refresh"><i class="fa fa-times"></i></button>
											</div>
										</td>	
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
						<hr>
						<div class="col-sm-12" style="margin-bottom: 15px;">
							<h5><?=text_success('Dokter Anesthesi')?></h5>
						</div>
						
						<div class="col-sm-12">
							<table class="table table-bordered table-striped" id="tabel_dokter_anestesi" style="width: 100%;">
								<thead>
									<tr>
										<th style="width:100%" class="text-center">Dokter Anesthesi</th>
									</tr>
									
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END Static Labels -->
	</div>
	
</div>


<script type="text/javascript">

    $(document).ready(function() { 
		// alert($("#tujuan_id").val())
    });
	$(document).on("click", "#button_up_dokter", function() {//Load Hanya Pas Open
		set_view();
	});
	$(document).on("click",".edit_dokter",function(){
		$(".edit_dokter").attr('disabled',true);
		$(".hapus_dokter").attr('disabled',true);
		$id = $(this).closest('tr').find("td:eq(3)").html();
		$iddokter = $(this).closest('tr').find("td:eq(4)").html();
		$("#iddokter_add").val($iddokter).trigger('change');
		$("#id_edit_dokter").val($id);		
		
	});
	$(document).on("click","#add_dokter",function(){
		if (validate_dokter()==false)return false;
		var id_edit_dokter=$("#id_edit_dokter").val();		
		var iddokter=$("#iddokter_add").val();		
		var idberkas=$("#idberkas").val();		
		// alert($("#id").val());
		if (id_edit_dokter ==''){
			$.ajax({
				url: '{site_url}trm_berkas/simpan_add_dokter/',
				type: 'POST',
				data: {
					id_edit_dokter: id_edit_dokter,iddokter: iddokter,idberkas: idberkas		
				},
				complete: function(data) {
					if (data){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Dokter Berhasil Disimpan'});
						clear_dokter();
						load_dokter();
					}
				}
			});
		}else{
			$.ajax({
				url: '{site_url}trm_berkas/simpan_edit_dokter/',
				type: 'POST',
				data: {
					id_edit_dokter: id_edit_dokter,iddokter: iddokter			
				},
				complete: function(data) {
					if (data){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Dokter Berhasil Disimpan'});
						clear_dokter();
						load_dokter();
					}
				}
			});
		}
	});
	$(document).on("click","#cancel_dokter",function(){
		clear_dokter();
	});
	$(document).on("click",".hapus_dokter",function(){
		var id = $(this).closest('tr').find("td:eq(3)").html();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menghapus Dokter ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}trm_berkas/hapus_dokter',
				type: 'POST',
				data: {
					id: id	
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Dokter Berhasil dihapus'});
					clear_dokter();
					load_dokter();
				}
			});
		});
		
		return false;
	});
	function clear_dokter(){
		$("#iddokter_add").val(null).trigger('change');
		$("#id_edit_dokter").val('');
		$(".edit_dokter").attr('disabled',false);
		$(".hapus_dokter").attr('disabled',false);
	}
	function validate_dokter()
	{
		var duplicate=false;
		if ($("#iddokter_add").val()=='' || $("#iddokter_add").val()==null || $("#iddokter_add").val()=="#"){
			sweetAlert("Maaf...", "Dokter Harus diisi", "error");
			return false;
		}
		if ($("#iddokter_add").val()==$("#iddokter").val()){
			sweetAlert("Maaf...", "Dokter sama dengan Dokter DPJP", "error");
			return false;
		}
		if ($("#iddokter_add").val()==$("#iddokter_perujuk").val()){
			sweetAlert("Maaf...", "Dokter sama dengan Dokter Perujuk", "error");
			return false;
		}
		$('#tabel_dokter tbody tr').filter(function (){
			var $cells = $(this).children('td');
			if($cells.eq(4).text() === $("#iddokter_add").val()){
				// big_notification("Barang sudah ada didalam daftar","error");
				// alert($cells.eq(3).text() +' : '+ $("#id_edit_dokter").val());
				if ($cells.eq(3).text() != $("#id_edit_dokter").val()){
					duplicate = true;
					sweetAlert("Duplicate...", "Data Dokter Duplicate!", "error");
				}
			}
		});
		if (duplicate==true)return false; 
		return true;
	}
</script>