<div class="row">
	<div class="col-lg-4">
		<div class="block block-bordered">
			<div class="block-header">
				<ul class="block-options">
					<li>
						<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title">Profile Pasien</h3>
			</div>
			<div class="block-content">
				<div class="pull-r-l pull-t push">
					<table class="block-table text-center bg-primary border-b">
						<tbody>
							<tr>
								<td class="border-r bg-success" style="width: 20%;">
									<div class="push-30 push-30-t">
										<i class="si si-user fa-3x text-white-op"></i>
									</div>
								</td>
								<td>
									<div class="h4 text-left text-white  font-w500"><?=$no_medrec?></div>
									<div class="h4 text-left text-white  font-w500"><?=$nama.', '.$title_pasien?></div>
									<div class="h4 text-left text-white  text-muted text-uppercase push-5-t"><?=HumanDateShort($tanggal_lahir)?></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="block block-bordered">
			<div class="block-header">
				<ul class="block-options">
					<li>
						<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title">Statistik Kunjungan</h3>
			</div>
			<div class="block-content">
				<div class="pull-r-l pull-t push">
					<table class="block-table text-center bg-danger border-b">
						<tbody>
							<tr>
								<td class="border-r bg-success" style="width: 20%;">
									<div class="push-30 push-30-t">
										<i class="fa fa-stethoscope fa-3x text-white-op"></i>
									</div>
								</td>
								<td class="border-r" style="width: 20%;">
									<div class="h4 text-white text-uppercase push-5-t">POLI</div>
									<div class="h4 text-white font-w500"></div>
									<div class="h2  text-white font-w700"><?=$jml_poli?></div>
								</td>
								<td class="border-r" style="width: 20%;">
									<div class="h4 text-white text-uppercase push-5-t">IGD</div>
									<div class="h4 text-white font-w500"></div>
									<div class="h2  text-white font-w700"><?=$jml_igd?></div>
								</td>
								<td class="border-r" style="width: 20%;">
									<div class="h4 text-white text-uppercase push-5-t">RANAP</div>
									<div class="h4 font-w500"></div>
									<div class="h2 text-white  font-w700"><?=$jml_ri?></div>
								</td>
								<td class="border-r" style="width: 20%;">
									<div class="h4 text-white text-uppercase push-5-t">ODS</div>
									<div class="h4 font-w500"></div>
									<div class="h2 text-white  font-w700"><?=$jml_ods?></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="block block-bordered">
			<div class="block-header">
				<ul class="block-options">
					<li>
						<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title">Statistik Kunjungan</h3>
			</div>
			<div class="block-content ">
				<div class="pull-r-l pull-t push ">
					<table class="block-table text-center bg-success border-b ">
						<tbody>
							<tr>
								<td class="border-r bg-danger" style="width: 10%;">

									<div class="push-30 push-30-t">
										<i class="glyphicon glyphicon-time fa-3x text-white-op"></i>
									</div>
								</td>
								<td class="border-r" style="width: 20%;">
									<div class="h4 text-white-op push-10-t font-w700">Last Visit</div>
									<div class="h5 text-white-op font-w200"><?=HumanDateShort($last_visit)?></div>
									<div class="h5 text-white-op"><?=$last_dokter?></div>
								</td>
								<td class="border-r" style="width: 20%;">
									<div class="h4 text-white-op push-10-t font-w700">In Active</div>
									<div class="h5 text-white-op font-w200"><?=HumanDateShort($est_in)?></div>
									<div class="h5 text-white-op font-w200">
										<??>
									</div>
									<div class="h5 font-w500"></div>
								</td>
								<td class="border-r" style="width: 20%;">
									<div class="h4 text-white-op push-10-t font-w700">Retensi</div>
									<div class="h5 text-white-op font-w200"><?=HumanDateShort($est_retensi)?></div>
									<div class="h5 text-white-op font-w200">
										<??>
									</div>
									<div class="h5 font-w500"></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('Trm_berkas/profile_detail'); ?>

<?php if (UserAccesForm($user_acces_form, array('1170'))) {?>
	<?php $this->load->view('Trm_berkas/riwayat_kunjungan'); ?>
<?}?>

<?php if (UserAccesForm($user_acces_form, array('1171'))) {?>
	<?php $this->load->view('Trm_berkas/riwayat_berkas'); ?>
<?}?>

<?php if (UserAccesForm($user_acces_form, array('1172'))) {?>
	<?php $this->load->view('Trm_berkas/status_berkas'); ?>
<?}?>

<?php if (UserAccesForm($user_acces_form,array('1225'))) {?>
	<?php $this->load->view('Trm_berkas/riwayat_transaksi'); ?>
<?}?>

<?php if (UserAccesForm($user_acces_form, array('1174'))) {?>
	<?php $this->load->view('Trm_berkas/riwayat_pemeriksaan'); ?>

	<?php $this->load->view('Trm_berkas/riwayat_therapy'); ?>

	<?php $this->load->view('Trm_berkas/riwayat_radiologi'); ?>

	<?php $this->load->view('Trm_berkas/riwayat_laboratorium'); ?>
<?}?>

<?php if (UserAccesForm($user_acces_form, array('1178'))) {?>	
	<?php $this->load->view('Trm_berkas/riwayat_icdx'); ?>
	<?php $this->load->view('Trm_berkas/riwayat_icd9'); ?>
<?}?>

<?php if (UserAccesForm($user_acces_form, array('1179'))) {?>
	<div class="row">
		<div class="col-md-12">
			<div class="block block-themed block-opt-hidden">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
						</li>
					</ul>
					<h3 class="block-title"><i class="si si-user"></i> Riwayat Pengajuan SKD</h3>
				</div>
				<div class="block-content">

				</div>
			</div>
		</div>
	</div>
<?}?>
