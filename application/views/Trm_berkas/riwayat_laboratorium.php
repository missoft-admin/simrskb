<div class="row">
	<div class="col-md-12">
		<div class="block block-themed block-opt-hidden" id="div_laboratorium">
			<div class="block-header bg-primary">
				<ul class="block-options">
					<li>
						<button id="button_up_laboratorium" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="si si-login"></i>  Riwayat Laboratorium</h3>
			</div>
			<div class="block-content block-content">
				<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
					<input class="form-control" value="0" type="hidden" readonly id="st_load_laboratorium">
					<table class="table table-bordered table-striped" id="tabel_riwayat_laboratorium" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:10%">Tanggal</th>
								<th style="width:10%">No. Rujukan</th>
								<th style="width:10%">Asal Pasien</th>
								<th style="width:15%">Dokter Perujuk</th>
								<th style="width:10%">Kelompok Pasien</th>
								<th style="width:10%">Dokter</th>
								<th style="width:10%">Petugas</th>
								<th style="width:10%">Jumlah</th>
								<th style="width:10%">Aksi</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(document).on("click", "#button_up_laboratorium", function() {
		if ($("#div_laboratorium").attr('class')=='block block-themed'){
			if ($("#st_load_laboratorium").val()=='0'){
				LoadIndexLaboratorium();
			}
			$("#st_load_laboratorium").val('1');
		}
	});

	function LoadIndexLaboratorium(){
		var idpasien = '<?=$idpasien?>';
		$('#tabel_riwayat_laboratorium').DataTable().destroy();
		var tableTherapy = $('#tabel_riwayat_laboratorium').DataTable({
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"autoWidth": false,
			"fixedHeader": true,
			"order": [],
			"ajax": {
				url: '{site_url}trm_berkas/get_riwayat_laboratorium/',
				type: "POST",
				dataType: 'json',
				data: {idpasien: idpasien}
			},
			"columns": [
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" }
			],
			"columnDefs": [
				{ "width": "10%", "targets": 0, "orderable": true },
				{ "width": "10%", "targets": 1, "orderable": true },
				{ "width": "10%", "targets": 2, "orderable": true },
				{ "width": "10%", "targets": 3, "orderable": true },
				{ "width": "10%", "targets": 4, "orderable": true },
				{ "width": "10%", "targets": 5, "orderable": true },
				{ "width": "10%", "targets": 6, "orderable": true },
				{ "width": "10%", "targets": 7, "orderable": true },
				{ "width": "10%", "targets": 8, "orderable": true }
			]
		});

		var detailRows = [];
		$('#tabel_riwayat_laboratorium tbody').on( 'click', '.btn-detail-laboratorium', function (){
				var tr = $(this).closest('tr');
				var row = tableTherapy.row( tr );
				var idx = $.inArray( tr.attr('id'), detailRows );

				if ( row.child.isShown() ) {
						tr.removeClass( 'details' );
						row.child.hide();

						// Remove from the 'open' array
						detailRows.splice( idx, 1 );
				} else {
						var idlaboratorium = tr.find("td:eq(8) button").data('idlaboratorium');

						$.ajax({
							url:"{site_url}trm_berkas/getDataLaboratorium/" + idlaboratorium,
							dataType:"json",
							success:function(data){

								tr.addClass('details');
								row.child( viewDetailLaboratorium(data) ).show();

								$('.js-summernote').summernote();

								// Add to the 'open' array
								if ( idx === -1 ) {
									detailRows.push( tr.attr('id') );
								}
							}
						});
				}
		});

		// On each draw, loop over the `detailRows` array and show any child rows
		tableTherapy.on('draw', function(){
				$.each( detailRows, function(i, id){
						$('#'+id+' td.details-control').trigger('click');
				});
		});
	}

	function viewDetailLaboratorium(data) {
		var rows = '';

		rows += '<div class="col-md-12">';
		rows += '		<table class="table table-striped table-bordered">';
		rows += '			<thead>';
		rows += '				<tr>';
		rows += '					<th>Nama Tindakan</th>';
		rows += '					<th>Hasil</th>';
		rows += '					<th>Satuan</th>';
		rows += '					<th>Nilai Normal</th>';
		rows += '					<th>Keterangan</th>';
		rows += '				</tr>';
		rows += '			</thead>';
		rows += '			<tbody>';
		data.laboratorium.forEach((item, i) => {
			if(item.idkelompok == 1) {
				rows += '<tr>';
				rows += '	<td colspan="5"><b>' + item.nama + '</b></td>';
				rows += '</tr>';
			}else{
				rows += '<tr>';
				rows += '	<td>' + item.nama + '</td>';
				rows += '	<td><input type="text" class="form-control" value="' + item.hasil + '"></td>';
				rows += '	<td>' + item.satuan + '</td>';
				rows += '	<td>' + item.nilainormal + '</td>';
				rows += '	<td><textarea class="form-control">' + item.keterangan + '</textarea></td>';
				rows += '	<td style="display: none;">' + item.id + '</td>';
				rows += '</tr>';
			}
		});
		rows += '			</tbody>';
		rows += '			<tfoot>';
		rows += '				<tr>';
		rows += '					<td>Catatan</td>';
		rows += '					<td colspan="4">';
		rows += '						<textarea class="form-control js-summernote">' + data.catatan + '</textarea>';
		rows += '					</td>';
		rows += '				</tr>';
		rows += '			</tfoot>';
		rows += '		</table>';
		rows += '</div>';

		return rows;
	}
</script>
