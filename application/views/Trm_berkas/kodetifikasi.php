<div class="content">

	<div class="row">
		<div class="col-lg-4">
			<!-- Email Center Widget -->
			<div class="block block-bordered">
				<div class="block-header">
					<ul class="block-options">
						<li>
							<a href="{site_url}trm_berkas" type="button"><i class="si si-action-undo"></i></a>
						</li>
						<li>
							<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Berkas Pasien</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal push-10-t" action="base_forms_elements.html" method="post" onsubmit="return false;">
					<div class="pull-r-l pull-t push">
						<table class="block-table text-center bg-primary border-b">
							<tbody>
								<tr>
									<td class="border-r bg-success" style="width: 20%;">

										<div class="push-30 push-30-t">
											<i class="si si-user fa-3x text-white-op"></i>
										</div>
									</td>
									<td>
										<div class="h4 text-left text-white  font-w500"><?=$no_medrec?></div>
										<div class="h4 text-left text-white  font-w500"><?=$nama.', '.$title?></div>
										<div class="h4 text-left text-white  text-muted text-uppercase push-5-t"><?=HumanDateShort($tanggal_lahir)?></div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="push-10-t">
						<div class="form-group" style="margin-bottom: 15px;margin-top: 5px;">
							<label class="col-md-2 control-label" for="filter_idjenis">Filter</label>
							<div class="col-md-10">
								<select id="filter_idjenis" name="filter_idjenis" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua Tujuan -</option>
									<option value="1" >Poliklinik</option>
									<option value="2" >IGD</option>
									<option value="3" >Rawat Inap</option>
									<option value="4" >ODS</option>
								</select>
							</div>
						</div>
						<table class="table table-bordered table-striped" id="tabel_berkas_index" style="width: 100%;">
							<thead>
								<tr>
									<th style="width:50%">Tanggal / Dokter</th>
									<th style="width:30%">Status</th>
									<th style="width:20%">Aksi</th>								
									<th style="display:none">id_trx</th>
									<th style="display:none">id_trx</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
					</form>
				</div>
			</div>
			<!-- END Email Center Widget -->
		</div>
		<div class="col-lg-8">
			<!-- Email Center Widget -->
			<div class="block block-bordered">
				<div class="block-header">
					<ul class="block-options">
						<?php if (UserAccesForm($user_acces_form,array('1184'))){?>
						<a href="{site_url}trm_berkas/profile/{idpasien}" target="_blank" class="btn btn-xs btn-primary" type="button"><i class="si si-user"></i> Profile</a>
						<?}?>
						<?php if (UserAccesForm($user_acces_form,array('1185'))){?>
						<button class="btn btn-xs btn-success" type="submit"><i class="si si-printer"></i> Cetak Dokumen</button>
						<?}?>
						<li>
							<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Informasi Detail Pasien</h3>
				</div>
				<div class="block-content">
					<h3 id="text_header"></h3>
					<input class="form-control"  value="0" type="hidden" readonly id="st_read_only">
					<? $this->load->view('Trm_berkas/kodetifikasi_header'); ?>
					<? $this->load->view('Trm_berkas/kodetifikasi_lab'); ?>
					<? $this->load->view('Trm_berkas/kodetifikasi_radiologi'); ?>
					<? $this->load->view('Trm_berkas/kodetifikasi_fisio'); ?>
					<? $this->load->view('Trm_berkas/kodetifikasi_dokter'); ?>
					<? $this->load->view('Trm_berkas/kodetifikasi_icd10'); ?>
					<? $this->load->view('Trm_berkas/kodetifikasi_operasi'); ?>
					<? $this->load->view('Trm_berkas/kodetifikasi_icd9'); ?>

				</div>
				<div class="modal-footer">
					<div class="buton">
					<?php if (UserAccesForm($user_acces_form,array('1187'))){?>
						<button type="button" id="btn_save_kodefikasi" class="btn btn-success" style="display: none;"><i class="fa fa-save"></i> Simpan Kodetifikasi</button>
					<?}?>
					<?php if (UserAccesForm($user_acces_form,array('1189'))){?>
						<button type="button" id="btn_save_verifikasi" class="btn btn-danger" style="display: none;"><i class="fa fa-check"></i> Simpan Verifikasi</button>
					<?}?>
						<a href="{base_url}trm_berkas/kodetifikasi/{idpasien}" type="button" class="btn btn-default"><i class="si si-refresh"></i> Refresh</a>
					</div>
				</div>

			</div>
			<!-- END Email Center Widget -->
		</div>

</div>
<script type="text/javascript">
    var table;
    $(document).ready(function() {
       LoadIndex();
    });
	$(document).on("click", "#btn_save_kodefikasi", function() {

		swal({
			title: "Anda Yakin ?",
			text : "Akan Menyelesaikan Kodetifikasi ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {

			// pause_so();
			save_kodefikasi();
		});
	});
	function save_kodefikasi(){
		var id=$("#idberkas").val();
		$.ajax({
			url: '{site_url}trm_berkas/save_kodefikasi',
			type: 'POST',
			data: {id: id},
			complete: function() {
				swal({
						title: "Berhasil!",
						text: "Proses penyimpanan Kodetifikasi.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});
				setTimeout(function() {
					location.reload(true);
				}, 2000);

			}
		});
	}
	$(document).on("click", ".kembali", function() {
		var table = $('#tabel_berkas_index').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,3).data()
		// alert();
		swal({
			title: "Anda Yakin ?",
			text : "Akan Mengembalikan Berkas ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			// pause_so();
			kembali(id);
		});
	});
	$(document).on("click", "#btn_save_verifikasi", function() {

		swal({
			title: "Anda Yakin ?",
			text : "Akan Menyelesaikan Verifikasi ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {

			// pause_so();
			save_verifikasi();
		});
	});
	function save_verifikasi(){
		var id=$("#idberkas").val();
		$.ajax({
			url: '{site_url}trm_berkas/save_verifikasi',
			type: 'POST',
			data: {id: id},
			complete: function() {
					swal({
					title: "Berhasil!",
					text: "Proses penyimpanan Verifikasi.",
					type: "success",
					timer: 1500,
					showConfirmButton: false
				});
				setTimeout(function() {
					location.reload(true);
				}, 2000);

			}
		});
	}
	$(document).on("click", ".kembali", function() {
		var table = $('#tabel_berkas_index').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,3).data()
		// alert();
		swal({
			title: "Anda Yakin ?",
			text : "Akan Mengembalikan Berkas ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			// pause_so();
			kembali(id);
		});
	});
	function kembali($id){
		var id=$id;
		table = $('#tabel_berkas_index').DataTable()
		$.ajax({
			url: '{site_url}trm_layanan_berkas/set_kembali',
			type: 'POST',
			data: {id: id},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Selesai Dikirim'});
				table.ajax.reload( null, false );
			}
		});
	}
	function LoadIndex(){

		var idpasien=$("#idpasien").val();
		var filter_idjenis=$("#filter_idjenis").val();
		$('#tabel_berkas_index').DataTable().destroy();
		var table = $('#tabel_berkas_index').DataTable({
		"pageLength": 10,
		"searching": false,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"order": [],
		"ajax": {
			url: '{site_url}trm_berkas/get_berkas_pasien/',
			type: "POST",
			dataType: 'json',
			data: {idpasien: idpasien,filter_idjenis: filter_idjenis}
		},
		columnDefs: [
			 { "width": "60%", "targets": [0] },
			 { "width": "30%", "targets": [1] },
			 { "width": "10%", "targets": [2] },
			 {  className: "text-center", targets:[0,1,2] },
			 // { "width": "20%", "targets": [5] },
			 { "visible": false, "targets": [3,4] }
				]
		});

	}
	$(document).on("change", "#filter_idjenis", function() {
		 LoadIndex();
		 clear_form();
	});
	$(document).on("click", ".kodetifikasi", function() {
		$('#tabel_berkas_index tbody tr').removeClass('success');
		$row = $(this).closest('tr');
		$row.addClass("success");

		var table = $('#tabel_berkas_index').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var idberkas=table.cell(tr,3).data();
		if (idberkas!=$("#idberkas").val()){
			 swal({
				title: "Apakah Anda Yakin ?",
				text : "Akan Mulai Kodetifikasi Berkas?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				start_kodefikasi(idberkas);
			});
		}else{

		}
	});
	$(document).on("click", ".lihat", function() {
		$('#tabel_berkas_index tbody tr').removeClass('success');
		$row = $(this).closest('tr');
		$row.addClass("success");

		var table = $('#tabel_berkas_index').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var idberkas=table.cell(tr,3).data();
		if (idberkas!=$("#idberkas").val()){
			 swal({
				title: "Apakah Anda Yakin ?",
				text : "Akan Melihat Hasil Kodetifikasi?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				start_lihat(idberkas);
			});
		}else{

		}
	});
	function start_lihat(idberkas){
		$("#text_header").html('<span class="label label-default">VIEW KODETIFIKASI</span>');
		$("#btn_save_kodefikasi").hide();
		$("#btn_save_verifikasi").hide();
		$("#idberkas").val(idberkas);
		$("#st_read_only").val('1');
		close_header()
		var idpasien=$("#idpasien").val();
		$.ajax({
		  url: '{site_url}trm_berkas/get_header_verifikasi/',
		  type: "POST",
		  dataType: 'json',
		  data: {idpasien: idpasien,idberkas:idberkas},
		  success: function(data) {
			  if (data){
				$("#kod_user_daftar").val(data.user_daftar);
				$("#kod_tanggalkunjungan").val(data.tanggal_trx);
				$("#kod_poli").val(data.namapoliklinik);
				$("#id_trx").val(data.id_trx);
				$("#kod_kunjungan").val(data.tujuan);
				$("#kod_dokter").val(data.namadokter);
				$("#kod_tanggalkirim").val(data.tanggal_kirim);
				$("#kod_user_kirim").val(data.user_kirim_nama);
				$("#kod_tanggalkembali").val(data.tanggal_kembali);
				$("#kod_lama_kirim").val(data.durasi_kirim);
				$("#kod_lama_kembali").val(data.durasi_kembali);
				$("#kod_status_pasien").val(data.status_pasien);
				$("#kod_kasus").val(data.statuskasus_nama);
				$("#tujuan_id").val(data.tujuan_id);
				if (data.tujuan_id < 3){
					$("#div_kodefikasi_dokter").hide();
					$("#div_kodefikasi_operasi").hide();
					// alert('sini');
				}else{
					$("#div_kodefikasi_dokter").show();
					$("#div_kodefikasi_operasi").show();
				}
				 set_view();
				 loadICD_1();
				 loadICD_9();
				 load_dokter();
				 load_dokter_anesthesi();
				 // auto_tambahan();
			  }

			 // location.reload(true);
		  }
		});

	}
	function set_view(){
		if ($("#st_read_only").val()=='1'){
			$(".disable").attr('disabled', true);
			$(".read").attr('disabled', false);
		}else{
			$(".disable").attr('disabled', false);
			$(".read").attr('disabled', false);
		}

	}
	function close_header(){
		$("#div_kodefikasi_fisio").removeClass("block-opt-hidden").addClass("block-opt-hidden");
		$("#st_load_fisio").val('0');
		$("#div_kodefikasi_rad").removeClass("block-opt-hidden").addClass("block-opt-hidden");
		$("#st_load_rad").val('0');
		$("#div_kodefikasi_lab").removeClass("block-opt-hidden").addClass("block-opt-hidden");
		$("#st_load_lab").val('0');
		$("#div_kodefikasi_dokter").removeClass("block-opt-hidden").addClass("block-opt-hidden");
		$("#st_load_dokter").val('0');
		$("#div_kodefikasi9").removeClass("block-opt-hidden").addClass("block-opt-hidden");
		$("#div_kodefikasi10").removeClass("block-opt-hidden").addClass("block-opt-hidden");
		$("#div_kodefikasi_operasi").removeClass("block-opt-hidden").addClass("block-opt-hidden");
		$("#st_load_operasi").val('0');
	}
	$(document).on("click", ".verifikasi", function() {
		$('#tabel_berkas_index tbody tr').removeClass('success').removeClass('info');
		$row = $(this).closest('tr');
		$row.addClass("info");

		var table = $('#tabel_berkas_index').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var idberkas=table.cell(tr,3).data();
		if (idberkas!=$("#idberkas").val()){
			 swal({
				title: "Apakah Anda Yakin ?",
				text : "Akan Mulai Verifikasi Berkas?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				start_verifikasi(idberkas);
			});
		}else{

		}
	});
	function start_verifikasi(idberkas){
		$("#text_header").html('<span class="label label-success">VERIFIKASI</span>');
		$("#btn_save_kodefikasi").hide();
		$("#btn_save_verifikasi").show();
		$("#idberkas").val(idberkas);
		$("#st_read_only").val('0');
		close_header()
		var idpasien=$("#idpasien").val();
		$.ajax({
		  url: '{site_url}trm_berkas/get_header_verifikasi/',
		  type: "POST",
		  dataType: 'json',
		  data: {idpasien: idpasien,idberkas:idberkas},
		  success: function(data) {
			  if (data){
				$("#kod_user_daftar").val(data.user_daftar);
				$("#kod_tanggalkunjungan").val(data.tanggal_trx);
				$("#kod_poli").val(data.namapoliklinik);
				$("#id_trx").val(data.id_trx);
				$("#kod_kunjungan").val(data.tujuan);
				$("#kod_dokter").val(data.namadokter);
				$("#kod_tanggalkirim").val(data.tanggal_kirim);
				$("#kod_user_kirim").val(data.user_kirim_nama);
				$("#kod_tanggalkembali").val(data.tanggal_kembali);
				$("#kod_lama_kirim").val(data.durasi_kirim);
				$("#kod_lama_kembali").val(data.durasi_kembali);
				$("#kod_status_pasien").val(data.status_pasien);
				$("#kod_kasus").val(data.statuskasus_nama);
				$("#tujuan_id").val(data.tujuan_id);
				if (data.tujuan_id < 3){
					$("#div_kodefikasi_dokter").hide();
					$("#div_kodefikasi_operasi").hide();
					// alert('sini');
				}else{
					$("#div_kodefikasi_dokter").show();
					$("#div_kodefikasi_operasi").show();
				}
				 $(".disable").attr('disabled', false);
				 loadICD_1();
				 loadICD_9();
				 load_dokter();
				 load_dokter_anesthesi();
				 // auto_tambahan();
			  }

			 // location.reload(true);
		  }
		});


	}
	function start_kodefikasi(idberkas){
		$("#text_header").html('<span class="label label-primary">KODETIFIKASI</span>');
		$("#btn_save_kodefikasi").show();
		$("#btn_save_verifikasi").hide();
		$("#idberkas").val(idberkas);
		$("#st_read_only").val('0');
		close_header()
		var idpasien=$("#idpasien").val();
		$.ajax({
		  url: '{site_url}trm_berkas/get_header_kodetifikasi/',
		  type: "POST",
		  dataType: 'json',
		  data: {idpasien: idpasien,idberkas:idberkas},
		  success: function(data) {
			  if (data){
				$("#kod_user_daftar").val(data.user_daftar);
				$("#kod_tanggalkunjungan").val(data.tanggal_trx);
				$("#kod_poli").val(data.namapoliklinik);
				$("#id_trx").val(data.id_trx);
				$("#kod_kunjungan").val(data.tujuan);
				$("#kod_dokter").val(data.namadokter);
				$("#kod_tanggalkirim").val(data.tanggal_kirim);
				$("#kod_user_kirim").val(data.user_kirim_nama);
				$("#kod_tanggalkembali").val(data.tanggal_kembali);
				$("#kod_lama_kirim").val(data.durasi_kirim);
				$("#kod_lama_kembali").val(data.durasi_kembali);
				$("#kod_status_pasien").val(data.status_pasien);
				$("#kod_kasus").val(data.statuskasus_nama);
				$("#tujuan_id").val(data.tujuan_id);
				if (data.tujuan_id < 3){
					$("#div_kodefikasi_dokter").hide();
					$("#div_kodefikasi_operasi").hide();
					// alert('sini');
				}else{
					$("#div_kodefikasi_dokter").show();
					$("#div_kodefikasi_operasi").show();
				}
				 $(".disable").attr('disabled', false);
				 loadICD_1();
				 loadICD_9();
				 set_dokter();
				 // auto_tambahan();
			  }

			 // location.reload(true);
		  }
		});
		setTimeout(function() {
			auto_tambahan();

		}, 1000);

	}
	function set_dokter(){
		if ($("#tujuan_id").val()>'2'){
			var idberkas=$("#idberkas").val();
			$.ajax({
			  url: '{site_url}trm_berkas/get_dokter/',
			  type: "POST",
			  dataType: 'json',
			  data: {idberkas:idberkas},
			  success: function(data) {
				  // alert(data);
				  // console.log(data);
				  if (data){
					$("#iddokter").val(data.iddokter).trigger('change');
					$("#iddokter_perujuk").val(data.iddokter_perujuk).trigger('change');
				  }

				 // location.reload(true);
			  }
			});
			setTimeout(function() {
				load_dokter();
				load_dokter_anesthesi();
			}, 1000);
		}

	}
	function load_dokter(){
		var idberkas=$("#idberkas").val();
		var iddokter=$("#iddokter").val();
		var iddokter_perujuk=$("#iddokter_perujuk").val();
		$('#tabel_dokter tbody').empty();
		var content='';
		$.ajax({
		  url: '{site_url}trm_berkas/load_konsulen/',
		  type: "POST",
		  dataType: 'json',
		  data: {
				idberkas: idberkas,iddokter_perujuk:iddokter_perujuk,iddokter:iddokter
			},
		  success: function(data) {
			 $.each(data, function (a,row) {
				 var det='';
				 if (row.tabel_asal=='Manual'){
					 det='<span class="label label-danger"> Manual </span>';
				 }else{
					 det='<span class="label label-primary"> Otomatis </span>';

				 }
				 content ="<tr>";
				 content +="<td><i class='fa fa-user-md fa-2x'></i>   "+row.nama+"</td>";
				 content +='<td class="text-center"><span class="label label-success">'+row.detail+'</span> '+det+'</td>';
				 content +='<td class="text-center"><div class="btn-group"><button class="btn btn-xs btn-primary edit_dokter disable" title="Edit"><i class="fa fa-pencil"></i></button><button class="btn btn-xs btn-danger hapus_dokter disable" title="Hapus"><i class="fa fa-close"></i></button></div></td>';
				 content +="<td style='display:none'>"+row.id+"</td>";
				 content +="<td style='display:none'>"+row.iddokter+"</td>";

				 content +="<\tr>";
				 $('#tabel_dokter tbody').append(content);
			 });
		  }
		});

	}
	function load_dokter_anesthesi(){
		var id_trx=$("#id_trx").val();

		$('#tabel_dokter_anestesi tbody').empty();
		var content='';
		$.ajax({
		  url: '{site_url}trm_berkas/load_anasthesi/',
		  type: "POST",
		  dataType: 'json',
		  data: {
				id_trx: id_trx
			},
		  success: function(data) {
			 $.each(data, function (a,row) {

				 content ="<tr>";
				 console.log(row);
				 content +="<td><i class='fa fa-user-md fa-2x'></i>   "+row.nama+"</td>";

				 content +="<\tr>";
				 $('#tabel_dokter_anestesi tbody').append(content);
			 });
		  }
		});

	}


</script>
