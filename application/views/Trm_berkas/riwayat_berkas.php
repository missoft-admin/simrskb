
<div class="row">
	<div class="col-md-12">
		<!-- Static Labels block-opt-hidden -->
		<div class="block block-themed block-opt-hidden" id="div_berkas">
			<div class="block-header bg-primary">
				<ul class="block-options">
					<li>
						<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="si si-doc"></i>  Riwayat Berkas</h3>
			</div>
			<div class="block-content block-content">
				<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">

					<input class="form-control"  value="0" type="hidden" readonly id="st_load_berkas">
					<table class="table table-bordered table-striped" id="tabel_berkas" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:5%">#</th>
								<th style="width:5%">Tanggal</th>
								<th style="width:15%">Jenis</th>
								<th style="width:5%">Status</th>
								<th style="width:5%">No. Medrec</th>
								<th style="width:5%">Pasien</th>
								<th style="width:5%">Tujuan</th>
								<th style="width:5%">Detail</th>
								<th style="width:5%">Status</th>
								<th style="width:15%">Durasi</th>
								<th style="width:10%">User</th>
								<th style="width:10%">User Kembali</th>
								<th style="width:15%" align="center">
									Aksi
								</th>
								<th style="width:0%;display:none">id_trx</th>
								<th style="width:0%;display:none">jenis</th>

							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</form>
			</div>
		</div>
		<!-- END Static Labels -->
	</div>

</div>


<script type="text/javascript">
    var table;
    $(document).ready(function() {
		// alert($("#idpasien").val());
       // LoadIndex();

    });
	$(document).on("click", "#button_up_berkas", function() {//Load Hanya Pas Open
		// alert(("#div_berkas").attr('class'));
		if ($("#div_berkas").attr('class')=='block block-themed'){
			if ($("#st_load_berkas").val()=='0'){

				LoadIndex_berkas();

			}
			$("#st_load_berkas").val('1');
		}
	});
	function LoadIndex_berkas(){
		var idpasien = '<?=$idpasien?>';
		// alert(idpasien);
		$('#tabel_berkas').DataTable().destroy();
		var table = $('#tabel_berkas').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"order": [],
		"ajax": {
			url: '{site_url}trm_berkas/get_riwayat_berkas/',
			type: "POST",
			dataType: 'json',
			data: {idpasien: idpasien}
		},
		columnDefs: [
			 { "width": "10%", "targets": [9] },
			 { "width": "3%", "targets": [0] },
			 { "width": "8%", "targets": [2,3,6] },
			 { "width": "8%", "targets": [1,4,12,8] },
			 { "width": "10%", "targets": [7,5] },
			 { "visible": false, "targets": [0,12,14,13] }
				]
		});

	}


</script>
