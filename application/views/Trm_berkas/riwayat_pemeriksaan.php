<div class="row">
	<div class="col-md-12">
		<div class="block block-themed block-opt-hidden" id="div_pemeriksaan">
			<div class="block-header bg-primary">
				<ul class="block-options">
					<li>
						<button id="button_up_pemeriksaan" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="si si-login"></i>  Riwayat Pemeriksaan</h3>
			</div>
			<div class="block-content block-content">
				<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
					<input class="form-control" value="0" type="hidden" readonly id="st_load_pemeriksaan">
					<table class="table table-bordered table-striped" id="tabel_riwayat_pemeriksaan" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:10%">Tanggal</th>
								<th style="width:10%">No. Transaksi</th>
								<th style="width:10%">Tujuan</th>
								<th style="width:10%">Klinik / Kelas</th>
								<th style="width:15%">Dokter</th>
								<th style="width:10%">User Daftar</th>
								<th style="width:10%">Tindak Lanjut</th>
								<th style="width:10%">Aksi</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
	$(document).on("click", "#button_up_pemeriksaan", function() {
		if ($("#div_pemeriksaan").attr('class')=='block block-themed'){
			if ($("#st_load_pemeriksaan").val()=='0'){
				LoadIndexPemeriksaan();
			}
			$("#st_load_pemeriksaan").val('1');
		}
	});

	function LoadIndexPemeriksaan() {
		var idpasien = '<?=$idpasien?>';
		$('#tabel_riwayat_pemeriksaan').DataTable().destroy();
		var tablePemeriksaan = $('#tabel_riwayat_pemeriksaan').DataTable({
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"autoWidth": false,
			"fixedHeader": true,
			"order": [],
			"ajax": {
				url: '{site_url}trm_berkas/get_riwayat_pemeriksaan/',
				type: "POST",
				dataType: 'json',
				data: {idpasien: idpasien}
			},
			"columns": [
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
			],
			"columnDefs": [
				{ "width": "10%", "targets": 0, "orderable": true },
				{ "width": "10%", "targets": 1, "orderable": true },
				{ "width": "10%", "targets": 2, "orderable": true },
				{ "width": "10%", "targets": 3, "orderable": true },
				{ "width": "10%", "targets": 4, "orderable": true },
				{ "width": "10%", "targets": 5, "orderable": true },
				{ "width": "10%", "targets": 6, "orderable": true },
				{ "width": "10%", "targets": 7, "orderable": true },
			]
		});

		var detailRows = [];
		$('#tabel_riwayat_pemeriksaan tbody').on( 'click', '.btn-detail-pemeriksaan', async function (){
				var tr = $(this).closest('tr');
				var row = tablePemeriksaan.row( tr );
				var idx = $.inArray( tr.attr('id'), detailRows );

				if ( row.child.isShown() ) {
						tr.removeClass( 'details' );
						row.child.hide();

						// Remove from the 'open' array
						detailRows.splice( idx, 1 );
				} else {
						var reference = tr.find("td:eq(7) button").data('reference');
						var idpendaftaran = tr.find("td:eq(7) button").data('idpendaftaran');
						var child = await viewDetailPemeriksaan(reference, idpendaftaran);

						tr.addClass('details');

						row.child( child ).show();

						// Add to the 'open' array
						if ( idx === -1 ) {
							detailRows.push( tr.attr('id') );
						}
				}
		});

		// On each draw, loop over the `detailRows` array and show any child rows
		tablePemeriksaan.on('draw', function(){
				$.each( detailRows, function(i, id){
						$('#'+id+' td.details-control').trigger('click');
				});
		});
	}

	async function viewDetailPemeriksaan(reference, idpendaftaran) {
		return `<div class="col-md-12">
			${await detailTindakan(reference, idpendaftaran)}
			<hr style="margin-top: 0px; margin-bottom: 30px;">
			<div class="row">
				<div class="col-md-12">
					<b><span class="label label-success" style="margin-left:10px; font-size:12px">Pemeriksaan Penunjang</span></b><br>
				</div>
				<div class="col-md-12">
					<br>
					<b><span class="label label-primary" style="margin-left:10px; font-size:12px">Radiologi</span></b><br><br>
					${await detailRadiologi(reference, idpendaftaran)}
				</div>
				<div class="col-md-12">
					<b><span class="label label-primary" style="margin-left:10px; font-size:12px">Laboratorium</span></b><br><br>
					${await detailLaboratorium(reference, idpendaftaran)}
				</div>
			</div>
			<hr style="margin-top: 0px; margin-bottom: 30px;">
			<div class="row">
				<div class="col-md-12">
					<b><span class="label label-success" style="margin-left:10px; font-size:12px">Theraphy</span></b><br>
				</div>
				<div class="col-md-12">
					<br>
					<b><span class="label label-primary" style="margin-left:10px; font-size:12px">Racikan</span></b><br><br>
					${await detailTheraphyRacikan(reference, idpendaftaran)}
				</div>
				<div class="col-md-12">
					<b><span class="label label-primary" style="margin-left:10px; font-size:12px">Non Racikan</span></b><br><br>
					${await detailTheraphyNonRacikan(reference, idpendaftaran)}
				</div>
			</div>
		</div>`;
	}

	async function detailTindakan(reference, idpendaftaran) {
		var data = await $.ajax({
			url: `{site_url}trm_berkas/getDataTindakan/${reference}/${idpendaftaran}`,
			dataType:"json",
			success: function(data) {
				return data;
			}
		});

		rows = '<br>';
		rows += '<div class="row">';
		rows += '	<div class="col-md-12">';
		rows += '		<b><span class="label label-success" style="margin-left:10px; font-size:12px">Tanda Tanda Vital</span></b><br>';
		rows += '	</div>';
		rows += '	<div class="col-md-6">';
		rows += '		<table class="table" style="background-color: transparent;">';
		rows += '			<thead>';
		rows += '				<tr>';
		rows += '					<td style="width:20%"><b>Tinggi Badan</b></td>';
		rows += '					<td>';
		rows += '						<span>' + data.tinggibadan + '</span>';
		rows += '						<span style="font-weight: 600;">cm</span>';
		rows += '					</td>';
		rows += '				</tr>';
		rows += '				<tr>';
		rows += '					<td style="width:20%"><b>Suhu Badan</b></td>';
		rows += '					<td>';
		rows += '						<span>' + data.suhubadan + '</span>';
		rows += '						<span style="font-weight: 600;">&deg;C</span>';
		rows += '					</td>';
		rows += '				</tr>';
		rows += '				<tr>';
		rows += '					<td style="width:20%"><b>Berat Badan</b></td>';
		rows += '					<td>';
		rows += '						<span>' + data.beratbadan + '</span>';
		rows += '						<span style="font-weight: 600;">kg</span>';
		rows += '					</td>';
		rows += '				</tr>';
		rows += '				<tr>';
		rows += '					<td style="width:20%"><b>Lingkar Kepala</b></td>';
		rows += '					<td>';
		rows += '						<span>' + data.lingkarkepala + '</span>';
		rows += '						<span style="font-weight: 600;">cm</span>';
		rows += '					</td>';
		rows += '				</tr>';
		rows += '			</thead>';
		rows += '		</table>';
		rows += '	</div>';
		rows += '	<div class="col-md-6">';
		rows += '		<table class="table" style="background-color: transparent;">';
		rows += '			<thead>';
		rows += '				<tr>';
		rows += '					<td style="width:20%"><b>Tensi</b></td>';
		rows += '					<td>';
		rows += '						<span>' + data.sistole + '</span>';
		rows += '						<span style="font-weight: 600;"> / </span>';
		rows += '						<span>' + data.disastole + '</span>';
		rows += '					</td>';
		rows += '				</tr>';
		rows += '				<tr>';
		rows += '					<td style="width:20%"><b>Alergi</b></td>';
		rows += '					<td>';
		rows += '						<span>' + data.alergiobat + '</span>';
		rows += '					</td>';
		rows += '				</tr>';
		rows += '				<tr>';
		rows += '					<td style="width:20%"><b>Keterangan</b></td>';
		rows += '					<td>';
		rows += '						<span>' + data.keterangan + '</span>';
		rows += '					</td>';
		rows += '				</tr>';
		rows += '			</thead>';
		rows += '		</table>';
		rows += '	</div>';
		rows += '</div>';

		rows += '<hr style="margin-top: 0px; margin-bottom: 30px;">';
		rows += '<div class="row">';
		rows += '	<div class="col-md-12">';
		rows += '		<b><span class="label label-success" style="margin-left:10px; font-size:12px">Hasil Pemeriksaan</span></b><br>';
		rows += '	</div>';
		rows += '	<div class="col-md-6">';
		rows += '		<table class="table" style="background-color: transparent;">';
		rows += '			<thead>';
		rows += '				<tr>';
		rows += '					<td style="width:20%"><b>Keluhan</b></td>';
		rows += '					<td>';
		rows += '						<span>' + data.keluhan + '</span>';
		rows += '					</td>';
		rows += '				</tr>';
		rows += '			</thead>';
		rows += '		</table>';
		rows += '	</div>';
		rows += '	<div class="col-md-6">';
		rows += '		<table class="table" style="background-color: transparent;">';
		rows += '			<thead>';
		rows += '				<tr>';
		rows += '					<td style="width:20%"><b>Diagnosa</b></td>';
		rows += '					<td>';
		rows += '						<span>' + data.diagnosa + '</span>';
		rows += '					</td>';
		rows += '				</tr>';
		rows += '			</thead>';
		rows += '		</table>';
		rows += '	</div>';
		rows += '</div>';

		rows += '<hr style="margin-top: 0px; margin-bottom: 30px;">';
		rows += '<div class="row">';
		rows += '	<div class="col-md-12">';
		rows += '		<b><span class="label label-success" style="margin-left:10px; font-size:12px">Tindak Lanjut</span></b><br>';
		rows += '	</div>';
		rows += '	<div class="col-md-6">';
		rows += '		<table class="table" style="background-color: transparent;">';
		rows += '			<thead>';
		rows += '				<tr>';
		rows += '					<td style="width:20%"><b>Kasus</b></td>';
		rows += '					<td>';
		rows += '						<span>' + (data.statuskasus == 1 ? "Baru" : "Lama") + '</span>';
		rows += '					</td>';
		rows += '				</tr>';
		rows += '				<tr>';
		rows += '					<td style="width:20%"><b>Tindak Lanjut</b></td>';
		rows += '					<td>';
		rows += '						<span>' + await GetInfoTindakLanjut(data.tindaklanjut) + '</span>';
		rows += '					</td>';
		rows += '				</tr>';
		rows += '			</thead>';
		rows += '		</table>';
		rows += '	</div>';
		rows += '	<div class="col-md-6">';
		rows += '		<table class="table" style="background-color: transparent;">';
		rows += '			<thead>';
		rows += '				<tr>';
		rows += '					<td style="width:20%"><b>Sebab Luar</b></td>';
		rows += '					<td>';
		rows += '						<span>' + await GetInfoSebabLuar(data.sebabluar) + '</span>';
		rows += '					</td>';
		rows += '				</tr>';
		rows += '				<tr>';
		rows += '					<td style="width:20%"><b>Tanggal Kontrol</b></td>';
		rows += '					<td>';
		rows += '						<span>' + data.tanggalkontrol + '</span>';
		rows += '					</td>';
		rows += '				</tr>';
		rows += '			</thead>';
		rows += '		</table>';
		rows += '	</div>';
		rows += '</div>';

		return rows;
	}

	async function detailRadiologi(reference, idpendaftaran) {
		return `<table class="table table-bordered table-striped" id="tabel_pemeriksaan_radiologi" style="width: calc(100% - 10px); margin-left:10px;">
			<thead>
				<tr>
					<th style="width:10%">Tanggal</th>
					<th style="width:10%">No. Rujukan</th>
					<th style="width:10%">Asal Pasien</th>
					<th style="width:15%">Dokter Perujuk</th>
					<th style="width:10%">Kelompok Pasien</th>
					<th style="width:10%">Dokter</th>
					<th style="width:10%">Petugas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:10%">Aksi</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>`;
	}

	async function detailLaboratorium(reference, idpendaftaran) {
		return `<table class="table table-bordered table-striped" id="tabel_pemeriksaan_laboratorium" style="width: calc(100% - 10px); margin-left:10px;">
			<thead>
				<tr>
					<th style="width:10%">Tanggal</th>
					<th style="width:10%">No. Rujukan</th>
					<th style="width:10%">Asal Pasien</th>
					<th style="width:15%">Dokter Perujuk</th>
					<th style="width:10%">Kelompok Pasien</th>
					<th style="width:10%">Dokter</th>
					<th style="width:10%">Petugas</th>
					<th style="width:10%">Jumlah</th>
					<th style="width:10%">Aksi</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>`;
	}

	async function detailTheraphyRacikan(reference, idpendaftaran) {
		return `<table class="table table-bordered table-striped" id="tabel_pemeriksaan_therapy_racikan" style="width: calc(100% - 10px); margin-left:10px;">
			<thead>
				<tr>
					<th style="width:10%">Tanggal</th>
					<th style="width:10%">No. Transaksi</th>
					<th style="width:10%">Tujuan</th>
					<th style="width:15%">Klinik / Kelas</th>
					<th style="width:10%">Dokter</th>
					<th style="width:10%">Kelompok Pasien</th>
					<th style="width:10%">Total Obat</th>
					<th style="width:10%">Total Alkes</th>
					<th style="width:10%">Total</th>
					<th style="width:10%">Aksi</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>`;
	}

	async function detailTheraphyNonRacikan(reference, idpendaftaran) {
		return `<table class="table table-bordered table-striped" id="tabel_pemeriksaan_therapy_non_racikan" style="width: calc(100% - 10px); margin-left:10px;">
			<thead>
				<tr>
					<th style="width:10%">Tanggal</th>
					<th style="width:10%">No. Transaksi</th>
					<th style="width:10%">Tujuan</th>
					<th style="width:15%">Klinik / Kelas</th>
					<th style="width:10%">Dokter</th>
					<th style="width:10%">Kelompok Pasien</th>
					<th style="width:10%">Total Obat</th>
					<th style="width:10%">Total Alkes</th>
					<th style="width:10%">Total</th>
					<th style="width:10%">Aksi</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>`;
	}

	async function GetInfoTindakLanjut(id) {
		if (id == 1) {
			return 'Pulang';
		} else if (id == 2) {
			return 'Dirawat';
		} else if (id == 3) {
			return 'Dirujuk';
		} else if (id == 4) {
			return 'Menolak Dirawat';
		} else if (id == 5) {
			return 'Pindah RS';
		} else if (id == 6) {
			return 'Dibatalkan';
		} else if (id == 7) {
			return 'Pulang Dengan SP';
		}
	}

	async function GetInfoSebabLuar(id) {
		var data = await $.ajax({
			url: '{site_url}trm_berkas/getInfoSebabLuar/' + id,
			success: function(data) {
				return data;
			}
		});

		return data;
	}
</script>
