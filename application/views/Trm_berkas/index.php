<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>

<?php if (UserAccesForm($user_acces_form,array('1167'))){?>

<div class="block" id="div_header">
    <div class="block-header">
		
        <h3 class="block-title">Berkas Pasien</h3>
		<hr style="margin-top:15px">
        <div class="row">
            <?php echo form_open('trm_berkas/filter', 'class="form-horizontal" id="form"'); ?>
            <div class="col-md-6">
                
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="nomedrec">Medrec</label>
                    <div class="col-md-8">
                        <input type="text" id="nomedrec" name="nomedrec" class="form-control" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="namapasien">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" id="namapasien" name="namapasien" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="nomedrec">Tanggal Lahir</label>
                    <div class="col-md-4">
                        <div class="input-group" >
							<input class="js-datepicker form-control" type="text" id="tanggal_lahir"  name="tanggal_lahir" placeholder="Tanggal Lahir" value="" data-date-format="dd/mm/yyyy"/>
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="nomedrec">Last Visit</label>
                    <div class="col-md-4">
                        <div class="input-group" >
							<input class="js-datepicker form-control" type="text" id="last_visit"  name="last_visit" placeholder="Last Visit" value="" data-date-format="dd/mm/yyyy"/>
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
                    </div>
                </div>
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="nomedrec">Est. In Active</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
							<input class="form-control" type="text" id="in_activedari"  name="in_activedari" placeholder="From" value=""/>
							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
							<input class="form-control" type="text" id="in_activesampai" name="in_activesampai" placeholder="To" value=""/>
						</div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="ret_dari">Est. Retensi</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
							<input class="form-control" type="text" id="ret_dari"  name="ret_dari" placeholder="From" value=""/>
							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
							<input class="form-control" type="text" id="ret_sampai" name="ret_sampai" placeholder="To" value=""/>
						</div>
                    </div>
                </div>
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="status_aktif">Status </label>
                    <div class="col-md-8">
                        <select id="status_aktif" name="status_aktif" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#"selected >-Semua Status -</option>
							<option value="1"  >Active</option>
							<option value="2" >In Active</option>
							<option value="3" >Retensi</option>
							<option value="4" >Not Active</option>
                        </select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-3 control-label" for="status_kodefikasi">Status Kodefikasi</label>
                    <div class="col-md-8">
                        <select id="status_kodefikasi" name="status_kodefikasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#"selected >-Semua Status -</option>
							<option value="1">Sudah</option>
							<option value="2" >Belum</option>
                        </select>
                    </div>
                </div>
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="namapasien"></label>
					<?php if (UserAccesForm($user_acces_form,array('1168'))){?>
                   <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" id="btn_filter" name="btn_filter" style="font-size:13px;width:100%;float:left;"><i class="fa fa-filter"></i> Filter </button>
                    </div>
					<?}?>
                </div>
                
            </div>
			
            <?php echo form_close(); ?>
        </div>
    </div>
	
    <div class="block-content" id="div_tabel">
        <table class="table table-bordered table-striped" id="index_list" style="width: 100%;">
            <thead>
                <tr>
                    <th style="width:5%;display:none">#</th>
                    <th style="width:8%">No. Medrec</th>
                    <th style="width:15%">Pasien</th>
                    <th style="width:8%">Tanggal Lahir</th>
                    <th style="width:8%">Lok. Berkas</th>
                    <th style="width:8%">Last Visit</th>
                    <th style="width:8%">Est .In</th>
                    <th style="width:8%">Est .Retensi</th>
                    <th style="width:10%">Status Berkas</th>
                    <th style="width:10%">Status</th>
                    <th style="width:20%">Aksi</th>                    
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
	

</div>
<!-- BATAS HAPUS -->
<?}?>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
    var table;
    $(document).ready(function() {        
        $(".number").number(true,0,'.',',');
		// LoadIndex();
    });
	
	function LoadIndex(){
		var nomedrec=$("#nomedrec").val();
		var namapasien=$("#namapasien").val();
		var tanggal_lahir=$("#tanggal_lahir").val();
		var last_visit=$("#last_visit").val();
		var in_activedari=$("#in_activedari").val();
		var in_activesampai=$("#in_activesampai").val();
		var ret_dari=$("#ret_dari").val();
		var ret_sampai=$("#ret_sampai").val();
		var status_aktif=$("#status_aktif").val();
		var status_kodefikasi=$("#status_kodefikasi").val();
		// alert(nomedrec+namapasien+tanggal_lahir+last_visit+in_activedari+in_activesampai+ret_dari+ret_sampai+status_aktif+status_kodefikasi);
		if (nomedrec=='' && namapasien=='' && tanggal_lahir=='' && last_visit=='' && in_activedari=='' && in_activesampai=='' && ret_dari=='' && ret_sampai=='' && status_aktif=='#' && status_kodefikasi=='#'){
			sweetAlert("Maaf...", "Untuk Mempercepat Pencarian Silahkan Isi Kriteria Pencarian!", "error");
			return false;
		}
		
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"order": [],
		"ajax": {
			url: '{site_url}trm_berkas/getIndex/',
			type: "POST",
			dataType: 'json',
			data: {
				nomedrec: nomedrec,
				namapasien: namapasien,
				tanggal_lahir: tanggal_lahir,
				last_visit: last_visit,
				in_activedari: in_activedari,
				in_activesampai: in_activesampai,
				ret_dari: ret_dari,
				ret_sampai: ret_sampai,
				status_aktif: status_aktif,
				status_kodefikasi: status_kodefikasi,
			}
		},
		columnDefs: [
			 { "width": "8%", "targets": [1,3,4,5,6,8] },
			 { "width": "15%", "targets": [2] },
			 { "width": "10%", "targets": [7,9] },
			 { "width": "20%", "targets": [10] },
			 { "visible": false, "targets": [0] }
				]
		});
		
	}
	$(document).on("click", "#btn_filter", function() {
		LoadIndex();
	});
	$(document).on("click", ".aktifkan", function() {
		var table = $('#index_list').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()
		var nomedrec=table.cell(tr,1).data()
		var nama=table.cell(tr,2).data()
		// alert(id);return false;
		swal({
			title: "Anda Yakin ?",
			text : "Akan Mengaktifkan Pasien "+nomedrec+' - '+nama+" ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			// pause_so();
			aktifkan(id);
		});
	});
	function aktifkan($id){
		var id=$id;		
		table = $('#index_list').DataTable()	
		$.ajax({
			url: '{site_url}trm_berkas/aktifkan',
			type: 'POST',
			data: {id: id},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Selesai Diaktifkan'});
				table.ajax.reload( null, false ); 
			}
		});
	}
	$(document).on("click", ".nonaktif", function() {
		var table = $('#index_list').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,0).data()
		var nomedrec=table.cell(tr,1).data()
		var nama=table.cell(tr,2).data()
		// alert(id);return false;
		swal({
			title: "Anda Yakin ?",
			text : "Akan Non Aktifkan Pasien "+nomedrec+' - '+nama+" ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			// pause_so();
			nonaktif(id);
		});
	});
	function nonaktif($id){
		var id=$id;		
		table = $('#index_list').DataTable()	
		$.ajax({
			url: '{site_url}trm_berkas/nonaktif',
			type: 'POST',
			data: {id: id},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Selesai di Non Aktifkan'});
				table.ajax.reload( null, false ); 
			}
		});
	}
	//BATAS HAPUS
	
</script>