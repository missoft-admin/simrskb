
<div class="block-content block-content">
	<form class="form-horizontal push-10-t" action="base_forms_elements.html" method="post" onsubmit="return false;">					
		<div class="form-group">
			<div class="col-sm-6">
				<input class="form-control"  value="<?=$idpasien?>" type="hidden" readonly id="idpasien">
				<input class="form-control clear"  value="" type="hidden" readonly id="idberkas">
				<input class="form-control clear"  value="" type="hidden" readonly id="id_trx">
				<input class="form-control clear"  value="" type="hidden" readonly id="tujuan_id">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_tanggalkunjungan" name="kod_tanggalkunjungan" disabled="">
					<label for="kod_ ">Tanggal Kunjungan</label>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_poli" name="kod_poli" disabled="">
					<label for="kod_ ">Poliklinik</label>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-6">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_status_pasien" name="kod_status_pasien" disabled="">
					<label for="kod_ ">Status Pasien</label>
				</div>
			</div>
			
			<div class="col-sm-6">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_kasus" name="kod_kasus" disabled="">
					<label for="kod_ ">Kasus</label>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-6">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_kunjungan" name="kod_kunjungan" disabled="">
					<label for="kod_ ">Status Kunjungan</label>
				</div>
			</div>
			
			<div class="col-sm-6">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_dokter" name="kod_dokter" disabled="">
					<label for="kod_ ">Dokter</label>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-6">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_user_daftar" name="kod_user_daftar" disabled="">
					<label for="kod_ ">Petugas Pendaftaran</label>
				</div>
			</div>
			
			<div class="col-sm-3">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_tanggalkirim" name="kod_tanggalkirim" disabled="">
					<label for="kod_ ">Tanggal Kirim</label>
				</div>
			</div>
			<div class="col-sm-3">
				
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-6">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_user_kirim" name="kod_user_kirim" disabled="">
					<label for="kod_ ">Petugas Kirim Berkas</label>
				</div>
			</div>
			
			<div class="col-sm-3">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_tanggalkembali" name="kod_tanggalkembali" disabled="">
					<label for="kod_ ">Tanggal Kembali</label>
				</div>
			</div>
			<div class="col-sm-3">
				
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-3">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_lama_kirim" name="kod_lama_kirim" disabled="">
					<label for="kod_ ">Lama Kirim Berkas</label>
				</div>
			</div>
			<div class="col-sm-3">
				
			</div>
			<div class="col-sm-3">
				<div class="form-material">
					<input class="form-control clear" value="" type="text" id="kod_lama_kembali" name="kod_lama_kembali" disabled="">
					<label for="kod_ ">Lama Kembali</label>
				</div>
			</div>
			<div class="col-sm-3">
				
			</div>
		</div>
		
		
		
	</form>
</div>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
    var table;
    $(document).ready(function() {        
       $(".disable").attr('disabled', true)
    });
	function clear_form(){
		$(".clear").val('');
	}
</script>