<div class="row">
	<div class="col-md-12">
		<div class="block block-themed block-opt-hidden" id="div_radiologi">
			<div class="block-header bg-primary">
				<ul class="block-options">
					<li>
						<button id="button_up_radiologi" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="si si-login"></i>  Riwayat Radiologi</h3>
			</div>
			<div class="block-content block-content">
				<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
					<input class="form-control" value="0" type="hidden" readonly id="st_load_radiologi">
					<table class="table table-bordered table-striped" id="tabel_riwayat_radiologi" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:10%">Tanggal</th>
								<th style="width:10%">No. Rujukan</th>
								<th style="width:10%">Asal Pasien</th>
								<th style="width:15%">Dokter Perujuk</th>
								<th style="width:10%">Kelompok Pasien</th>
								<th style="width:10%">Dokter</th>
								<th style="width:10%">Petugas</th>
								<th style="width:10%">Jumlah</th>
								<th style="width:10%">Aksi</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(document).on("click", "#button_up_radiologi", function() {
		if ($("#div_radiologi").attr('class')=='block block-themed'){
			if ($("#st_load_radiologi").val()=='0'){
				LoadIndexRadiologi();
			}
			$("#st_load_radiologi").val('1');
		}
	});

	function LoadIndexRadiologi(){
		var idpasien = '<?=$idpasien?>';
		$('#tabel_riwayat_radiologi').DataTable().destroy();
		var tableTherapy = $('#tabel_riwayat_radiologi').DataTable({
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"autoWidth": false,
			"fixedHeader": true,
			"order": [],
			"ajax": {
				url: '{site_url}trm_berkas/get_riwayat_radiologi/',
				type: "POST",
				dataType: 'json',
				data: {idpasien: idpasien}
			},
			"columns": [
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" }
			],
			"columnDefs": [
				{ "width": "10%", "targets": 0, "orderable": true },
				{ "width": "10%", "targets": 1, "orderable": true },
				{ "width": "10%", "targets": 2, "orderable": true },
				{ "width": "10%", "targets": 3, "orderable": true },
				{ "width": "10%", "targets": 4, "orderable": true },
				{ "width": "10%", "targets": 5, "orderable": true },
				{ "width": "10%", "targets": 6, "orderable": true },
				{ "width": "10%", "targets": 7, "orderable": true },
				{ "width": "10%", "targets": 8, "orderable": true }
			]
		});

		var detailRows = [];
		$('#tabel_riwayat_radiologi tbody').on( 'click', '.btn-detail-radiologi', function (){
				var tr = $(this).closest('tr');
				var row = tableTherapy.row( tr );
				var idx = $.inArray( tr.attr('id'), detailRows );

				if ( row.child.isShown() ) {
						tr.removeClass( 'details' );
						row.child.hide();

						// Remove from the 'open' array
						detailRows.splice( idx, 1 );
				} else {
						var idradiologi = tr.find("td:eq(8) button").data('idradiologi');

						$.ajax({
							url:"{site_url}trm_berkas/getDataRadiologi/" + idradiologi,
							dataType:"json",
							success:function(data){
								tr.addClass('details');
								row.child( viewDetailRadiologi(data) ).show();

								$('.js-summernote-custom-height').summernote({
										height: 50,
										minHeight: null,
										maxHeight: null
								});

								// Add to the 'open' array
								if ( idx === -1 ) {
									detailRows.push( tr.attr('id') );
								}
							}
						});

				}
		});

		// On each draw, loop over the `detailRows` array and show any child rows
		tableTherapy.on('draw', function(){
				$.each( detailRows, function(i, id){
						$('#'+id+' td.details-control').trigger('click');
				});
		});
	}

	function GetTipeRadiologi(id) {
		if (id != '') {
				if (id == '1') {
					return 'X-Ray';
				} else if (id == '2') {
					return 'USG';
				} else if (id == '3') {
					return 'CT Scan';
				} else if (id == '4') {
					return 'MRI';
				} else if (id == '5') {
					return 'BMD';
				}
		} else {
				return '';
		}
	}

	function viewDetailRadiologi(data) {
		var rows = '';

		rows += '<div class="col-md-12">';
		data.forEach((item, i) => {
			rows += '		<table class="table table-striped table-bordered">';
			rows += '			<tbody>';
			rows += '				<tr>';
			rows += '					<th width="20%"><b>Tipe Tindakan</b></th>';
			rows += '					<th width="80%">' + GetTipeRadiologi(item.idtipe) + '</th>';
			rows += '				</tr>';
			rows += '				<tr>';
			rows += '					<th width="20%"><b>Nama Tindakan</b></th>';
			rows += '					<th width="80%">' + item.namatindakan + '</th>';
			rows += '				</tr>';
			rows += '				<tr>';
			rows += '					<th width="20%"><b>Posisi</b></th>';
			rows += '					<th width="80%">';
			rows += '						<textarea class="form-control js-summernote-custom-height" disabled rows="1">' + item.tindakanyangdilakukan + '</textarea>';
			rows += '					</th>';
			rows += '				</tr>';
			rows += '				<tr>';
			rows += '					<th width="20%"><b>Hasil Baca Foto</b></th>';
			rows += '					<th width="80%">';
			rows += '						<textarea class="form-control js-summernote-custom-height" disabled rows="1">' + item.hasilbacafoto + '</textarea>';
			rows += '					</th>';
			rows += '				</tr>';
			rows += '			</tbody>';
			rows += '		</table>';
		});
		rows += '</div>';

		return rows;
	}
</script>
