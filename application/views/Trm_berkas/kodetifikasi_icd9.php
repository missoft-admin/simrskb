
<div class="row">
	<div class="col-md-12">
		<!-- Static Labels block-opt-hidden -->
		<div class="block block-themed block-opt-hidden " id="div_kodefikasi9">
			<div class="block-header bg-primary ">
				<ul class="block-options">
					<li>
						<button id="button_up_icd_9" class="disable read" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="si si-note"></i>  Input ICD 9CM</h3>
			</div>
			<div class="block-content block-content">
				<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
					<div class="pull-r-l pull-t push">
					<input class="form-control"  value="0" type="hidden" readonly id="st_load_icd9">
					<input class="form-control"  value="1" type="hidden" readonly id="cara_input_9">
							<input class="form-control"  value="0" type="hidden" readonly id="tedit_9">
							<input class="form-control"  value="0" type="hidden" readonly id="tipe_kt9">
					<div class="form-group" style="margin-bottom: 10px;">
						<div class="col-sm-6">
							<?=text_success('Pilih Tindakan Utama & Tambahan')?>
						</div>
						
					</div>
					
					<table class="table table-bordered table-striped" id="tabel_icd_inputan" style="width: 100%;table-layout: fixed;">
						<thead>
							
							<tr>
								<th style="width:35%" class="text-center">ICD 9CM</th>
								<th style="width:20%" class="text-center">Cari Data</th>
								<th style="width:15%" class="text-center">Jenis</th>
								<th style="width:15%" class="text-center">User</th>
								<th style="width:15%" class="text-center">Aksi</th>								
							</tr>
							<tr>
								<td class="text-center">
										<select name="icd_id_9" id="icd_id_9" tabindex="16" style="width: 100%"  data-placeholder="Cari ICD 9CM" class="js-example-responsive disable">
										</select>
										
								</td>
								<td class="text-center">
										<select name="kt_1" id="kt_1" tabindex="16" style="width: 100%"  data-placeholder="Tarif / Kel. Tindakan" class="js-example-responsive disable">
										</select>
										
								</td>
								<td class="text-center">
								
									<select name="jenis_9" id="jenis_9" tabindex="16" style="width: 100%"  data-placeholder="Cari Jenis" class="js-select2 form-control input-sm disable">
										<option value="1" >Utama</option>												
										<option value="2" selected>Tambahan</option>												
									</select>										
								</td>
								<td class="text-center"><label class="small" id="user_1"><?=$this->session->userdata('user_name').'<br>'.date('d-m-Y')?></label></td>
								<td class="text-center">		
									<div class="btn-group" role="group">
									<button type="button" class="btn btn-sm btn-primary disable" tabindex="8" id="add_9" title="Masukan Item"><i class="fa fa-plus"></i></button>&nbsp;&nbsp;
									<button type="button" class="btn btn-sm btn-danger disable" tabindex="9" id="cancel_9" title="Refresh"><i class="fa fa-times"></i></button>
									</div>
								</td>	
							</tr>
						</thead>
						<tbody></tbody>
					</table>
					
					
					<div class="form-group" style="margin-bottom: 10px;">
						<div class="col-sm-6">
							<?=text_danger('Hasil Inputan ICD 9CM')?>
						</div>
						
					</div>
					
					<table class="table table-bordered table-striped" id="tabel_icd9" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:30%" class="text-center">ICD 9CM</th>
								<th style="width:15%" class="text-center">Jenis</th>
								<th style="width:20%" class="text-center">Kel Tindakan</th>
								<th style="width:20%" class="text-center">Tarif ICD</th>
								<th style="width:20%" class="text-center">Cara Input</th>
								<th style="width:15%" class="text-center">User</th>
								<th style="width:10%" class="text-center">Aksi</th>								
								<th style="display:none" class="text-center"></th>								
								<th style="display:none" class="text-center"></th>								
								<th style="display:none" class="text-center"></th>								
								<th style="display:none" class="text-center"></th>								
								<th style="display:none" class="text-center"></th>								
								<th style="display:none" class="text-center"></th>								
								<th style="display:none" class="text-center"></th>								
							</tr>
							
						</thead>
						<tbody></tbody>
					</table>
					
					</div>
				</form>
			</div>
		</div>
		<!-- END Static Labels -->
	</div>
	
</div>
<div class="modal" id="modal_sugges9" role="dialog" aria-hidden="true" aria-labelledby="modalAdd" font size="15">
    <div class="modal-dialog modal-lg" style="width:40%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Edit ICD Berdasarkan Kelompok</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal">
                        <input type="hidden" id="sug9_kelompok_id" value="" />                        
                        <input type="hidden" id="sug9_icd_id" value="" />                        

                        <div class="form-group">
                            <label class="col-md-4 control-label">Tindakan</label>
                            <div class="col-md-8">
                                <input type="text" readonly class="form-control input-sm" id="sug9_nama_tindakan" placeholder="" value="" required="" aria-required="true" >
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-4 control-label" id="sug9_label"></label>
                            <div class="col-md-8">
                                <input type="text" readonly class="form-control input-sm" id="sug9_nama_kel" placeholder="" value="" required="" aria-required="true" >
                            </div>
                        </div>
						<table class="table table-bordered table-striped" id="tabel_sugges9" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:70%" class="text-center">ICD</th>
								<th style="width:20%" class="text-center">Jenis</th>
								<th style="width:10%" class="text-center">Status</th>
								<th style="width:10%" class="text-center">Aksi</th>								
								<th style="display:none" class="text-center"></th>								
							</tr>
							
						</thead>
						<tbody></tbody>
					</table>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" id="tutup_icd10" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var table;
    $(document).ready(function() {  
		// $("#icd_id_9").select2({ width: 'resolve' });  
       // loadICD_1(); icd_id_2
	   // auto_tambahan9();
    });
	$(document).on("click", "#button_up_icd_9", function() {//Load Hanya Pas Open
		set_view();
	});
	function loadICD_9(){
		var idberkas=$("#idberkas").val();
		
		$('#tabel_icd9').DataTable().destroy();
		var table = $('#tabel_icd9').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}trm_berkas/load_icd9',
			type: "POST",
			dataType: 'json',
			data: {
				idberkas: idberkas
			}
		},
		columnDefs: [
		{"targets": [7,8,9,10,11,12,13], "visible": false },
		 { "width": "20%", "targets": [0] },
		 { "width": "10%", "targets": [1,4,5,6] },
		 { "width": "15%", "targets": [2,3] },
		 {  className: "text-center", targets:[0,1,2,3,4,5,6] },
					]
		});
		// auto_tambahan9();
	}
	function load_suges9($tipe){
		var tipe=$tipe;
		var kel_id=$("#sug9_kelompok_id").val();
		var icd_id=$("#sug9_icd_id").val();
		// alert(tipe+':'+kel_id+':'+icd_id);
		$('#tabel_sugges9').DataTable().destroy();
		var table = $('#tabel_sugges9').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}trm_berkas/load_suges9/',
			type: "POST",
			dataType: 'json',
			data: {
				tipe: tipe,kel_id: kel_id,icd_id: icd_id,
			}
		},
		columnDefs: [
		{"targets": [4], "visible": false },
		 { "width": "70%", "targets": [0] },
		 { "width": "20%", "targets": [1] },
		 { "width": "10%", "targets": [2,3] },
		 {  className: "text-center", targets:[3] },
					]
		});
		// auto_tambahan();
	}
	$(document).on("click",".sugges_9",function(){
		table = $('#tabel_icd9').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,7).data()
		var cara = table.cell(tr,13).data()
		var icd_id = table.cell(tr,8).data()
		
		var label = ''
		$('#sug9_icd_id').val(icd_id);		
		
		$('#tedit_9').val(id);
		if (cara=='1'){
			var nama = table.cell(tr,2).data()
			var kd_id = table.cell(tr,9).data()
			label="Kelompok Tindakan";
			$('#sug9_kelompok_id').val(kd_id);
			$('#sug9_nama_tindakan').val('Kelompok Tindakan');
			load_suges9('1');
		}else if (cara=='2'){
			var kd_id = table.cell(tr,10).data()
			var nama = table.cell(tr,3).data()
			label="Tindakan To ICD";	
			$('#sug9_kelompok_id').val(kd_id);
			$('#sug9_nama_tindakan').val('Layanan To ICD');
			load_suges9('2');
		}
		$('#sug9_nama_kel').val(nama);
		$('#sug9_label').text(label);
		$('#modal_sugges9').modal('show')
	});
	$(document).on("click",".pilih_icd_9",function(){
		table = $('#tabel_sugges9').DataTable()
		var tr = $(this).parents('tr')
		var icd_id = table.cell(tr,4).data()
		var id = $("#tedit_9").val();
		
		
			
		// alert(icd_id+':'+id);
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Mengganti ICD 9 ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}trm_berkas/ganti_sugges_icd9',
				type: 'POST',
				data: {
					id: id,icd_id:icd_id	
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' ICD Berhasil diganti'});
					$('#tabel_icd9').DataTable().ajax.reload( null, false );
					$('#modal_sugges9').modal('hide')
								
				}
			});
		});
		
		return false;
		
	});
	$("#icd_id_9").select2({
			minimumInputLength: 2,
			noResults: 'ICD Tidak Ditemukan.',
		ajax: {
			url: '{site_url}mkelompok_tindakan/js_icd/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,
		 data: function (params) {
			  var query = {
				search: params.term
			  }
			  return query;
			},
			processResults: function (data) {
				console.log($("#icd_id_9").val());
				return {
					results: $.map(data, function (item) {
						return {
							text: item.kode + ' - '+ item.deskripsi,
							id: item.id
						}
					})
				};
			}
		}
	});
	
	$("#kt_1").select2({
			minimumInputLength: 2,
			noResults: 'ICD Tidak Ditemukan.',
			// width: 'element',
		ajax: {
			url: '{site_url}trm_berkas/js_kelompok_tindakan/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,
		 data: function (params) {
			  var query = {
				search: params.term
			  }
			  return query;
			},
			processResults: function (data) {
				console.log($("#kt_1").val());
				return {
					results: $.map(data, function (item) {
						return {
							text: item.nama,
							id: item.id,
							tipe: item.tipe
						}
					})
				};
			}
		}
	});
	
	$("#kt_1").change(function(){
		var data=$("#kt_1").select2('data')[0];
		$("#icd_id_9").attr('disabled',false);
		if (data){
			$("#tipe_kt9").val(data.tipe);
		}else{
			$("#tipe_kt9").val(0);
		}

		if ($(this).val()!=null){
			$.ajax({
			  url: '{site_url}trm_berkas/get_kt/'+$(this).val()+'/'+$("#tipe_kt9").val(),
			  dataType: "json",
			  success: function(data) {
				  console.log(data);
				  var data2 = {
						id: data.id,
						text: data.nama
					};
					var newOption = new Option(data2.text, data2.id, true, true);
					$('#icd_id_9').append(newOption);
					$("#icd_id_9").select2({ width: 'resolve' });  
					$("#cara_input_9").val('2');
				
			  }
			});
		}
	});
	
	$("#icd_id_9").change(function(){
		$("#cara_input_9").val('1');
	});
	
	$(document).on("click","#add_9",function(){
		// alert($("#jenis_9").val());
		if (validate_add_9()==false)return false;
			var duplicate=false;
			var table = $('#tabel_icd9').DataTable();		
			table.rows().eq(0).each( function ( index ) {
				var row = table.row( index );		 
				var data = row.data();
				console.log(data[11]);
				if (data[11]=='1' && $("#jenis_9").val()=='1' && $("#tedit_9").val()!=data[7]){
					duplicate = true;
					sweetAlert("Duplicate...", "Diagnosa Utama Harus satu!", "error");
				}
				// if ($("#jenis_id").val()=='1' && data[5]=='1'){
					// duplicate = true;
					// sweetAlert("Duplicate...", "Primary Duplicate!", "error");
				// }
			} );
		if(duplicate) return false;
		
		simpan_icd9();
		setTimeout(function() {
			auto_tambahan9();
		}, 1000);
		// auto_tambahan9(ketemu);
	});
	
	function simpan_icd9(){
		$("#icd_id_9").attr('disabled',false);
		var idberkas=$("#idberkas").val();		
		var tipe_tabel=$("#tipe_kt9").val();
		var icd_id=$("#icd_id_9").val();	
		var kelompok_tindakan_id=$("#kt_1").val();	
		var jenis_id=$("#jenis_9").val();
		var cara_input=$("#cara_input_9").val();		
		var tedit=$("#tedit_9").val();	
		
		// alert('Alert Simpan'); return false;
		if (tedit =='0'){
			$.ajax({
				url: '{site_url}trm_berkas/simpan_icd9_add/',
				type: 'POST',
				data: {
					idberkas: idberkas,jenis_id: jenis_id,cara_input: cara_input,
					icd_id: icd_id,kelompok_tindakan_id: kelompok_tindakan_id,tipe_tabel: tipe_tabel		
				},
				complete: function(data) {
					if (data){
					$.toaster({priority : 'success', title : 'Succes!', message : ' ICD Berhasil Disimpan_icd9'});
						$('#tabel_icd9').DataTable().ajax.reload( null, false );
						clear_9();
						
					}
								
					// window.location.href = "{site_url}tko/index/"+idunit;
				}
			});
		}else{
			$.ajax({
				url: '{site_url}trm_berkas/simpan_icd9_edit/',
				type: 'POST',
				data: {
					idberkas: idberkas,jenis_id: jenis_id,cara_input: cara_input,
					icd_id: icd_id,kelompok_tindakan_id: kelompok_tindakan_id,id:tedit,tipe_tabel: tipe_tabel		
				},
				complete: function(data) {
					if (data){
						$.toaster({priority : 'success', title : 'Succes!', message : ' ICD Berhasil Disimpan_icd9'});
						$('#tabel_icd9').DataTable().ajax.reload( null, false );
						clear_9();
					}
				}
			});
		}
		
	}
	function auto_tambahan9(){
		var ketemu=false;
		var table = $('#tabel_icd9').DataTable();		
		table.rows().eq(0).each( function ( index ) {
			var row = table.row( index );		 
			var data = row.data();
			console.log(data[9]);
			if (data[11]=='1'){
				ketemu = true;
				
			}
			
		} );
		if (ketemu==true){
			$("#jenis_9").val(2).trigger('change');
		}else{
			$("#jenis_9").val(1).trigger('change');
		}
	}
	$(document).on("click",".edit_9",function(){
		table = $('#tabel_icd9').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,7).data()
		var icd_nama = table.cell(tr,0).data()
		var icd_id = table.cell(tr,8).data()
		var kd_id = table.cell(tr,9).data()
		var kd_nama = table.cell(tr,2).data()
		var tindakan_id = table.cell(tr,10).data()
		var tindakan_nama = table.cell(tr,3).data()
		var tipe_tabel = table.cell(tr,13).data()
		
		var jenis_id = table.cell(tr,11).data()
		var jenis_nama = table.cell(tr,1).data()
		var cara_input = table.cell(tr,12).data()
		
		var data2 = {id: icd_id,text: icd_nama};
		var newOption = new Option(data2.text, data2.id, true, true);
		$('#icd_id_9').append(newOption);
				
		$('#jenis_9').val(jenis_id).trigger('change');
		$("#tipe_kt9").val(tipe_tabel);
		if (tipe_tabel !='0'){
			$("#icd_id_9").attr('disabled',true);
			
			// $(".disable").attr('disabled', false);
		}
		if (tipe_tabel=='1'){
			var data3 = {id: kd_id,text: kd_nama};
			var newOption2 = new Option(data3.text, data3.id, true, true);
			$('#kt_1').append(newOption2);
		}else if (tipe_tabel=='2'){
			var data3 = {id: tindakan_id,text: tindakan_nama};
			var newOption2 = new Option(data3.text, data3.id, true, true);
			$('#kt_1').append(newOption2);
			
		}else{
			
		}
		
		$('.edit_9').attr('disabled',true);
		$('.sugges_9').attr('disabled',true);
		$('.hapus_9').attr('disabled',true);
		$('#tedit_9').val(id);
		$('#cara_input_9').val(cara_input);
		
	});
	$(document).on("click",".hapus_9",function(){
		table = $('#tabel_icd9').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,7).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menghapus ICD 9 ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}trm_berkas/hapus_icd9',
				type: 'POST',
				data: {
					id: id	
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' ICD Berhasil dihapus'});
					$('#tabel_icd9').DataTable().ajax.reload( null, false );
								
					// window.location.href = "{site_url}tko/index/"+idunit;
				}
			});
		});
		
		return false;
	});
	$(document).on("click","#cancel_9",function(){
		clear_9();
		
	});
	
	function validate_add_9()
	{
		if ($("#icd_id_9").val()=='' || $("#icd_id_9").val()==null || $("#icd_id_9").val()=="#"){
			sweetAlert("Maaf...", "ICD 9 Harus diisi", "error");
			return false;
		}
		
		
		return true;
	}
	function clear_9(){
		$("#tipe_kt9").val('0');
		$("#tedit_9").val('0');
		$("#cara_input_9").val('1');
		$("#icd_id_9").val(null).trigger('change');
		$("#kt_1").val(null).trigger('change');
		$('.hapus_9').attr('disabled',false)
		$('.sugges_9').attr('disabled',false)
		$('.edit_9').attr('disabled',false)
		$("#icd_id_9").attr('disabled',false);
	}
</script>