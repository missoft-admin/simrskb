<div class="row">
	<div class="col-md-12">
		<div class="block block-themed block-opt-hidden" id="div_therapy">
			<div class="block-header bg-primary">
				<ul class="block-options">
					<li>
						<button id="button_up_therapy" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="si si-login"></i>  Riwayat Terapi</h3>
			</div>
			<div class="block-content block-content">
				<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
					<input class="form-control" value="0" type="hidden" readonly id="st_load_therapy">
					<table class="table table-bordered table-striped" id="tabel_riwayat_therapy" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:10%">Tanggal</th>
								<th style="width:10%">No. Transaksi</th>
								<th style="width:10%">Tujuan</th>
								<th style="width:15%">Klinik / Kelas</th>
								<th style="width:10%">Dokter</th>
								<th style="width:10%">Kelompok Pasien</th>
								<th style="width:10%">Total Obat</th>
								<th style="width:10%">Total Alkes</th>
								<th style="width:10%">Total</th>
								<th style="width:10%">Aksi</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(document).on("click", "#button_up_therapy", function() {
		if ($("#div_therapy").attr('class')=='block block-themed'){
			if ($("#st_load_therapy").val()=='0'){
				LoadIndexTheraphy();
			}
			$("#st_load_therapy").val('1');
		}
	});

	function LoadIndexTheraphy(){
		var idpasien = '<?=$idpasien?>';
		$('#tabel_riwayat_therapy').DataTable().destroy();
		var tableTherapy = $('#tabel_riwayat_therapy').DataTable({
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"autoWidth": false,
			"fixedHeader": true,
			"order": [],
			"ajax": {
				url: '{site_url}trm_berkas/get_riwayat_therapy/',
				type: "POST",
				dataType: 'json',
				data: {idpasien: idpasien}
			},
			"columns": [
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
			],
			"columnDefs": [
				{ "width": "10%", "targets": 0, "orderable": true },
				{ "width": "10%", "targets": 1, "orderable": true },
				{ "width": "10%", "targets": 2, "orderable": true },
				{ "width": "10%", "targets": 3, "orderable": true },
				{ "width": "10%", "targets": 4, "orderable": true },
				{ "width": "10%", "targets": 5, "orderable": true },
				{ "width": "10%", "targets": 6, "orderable": true },
				{ "width": "10%", "targets": 7, "orderable": true },
				{ "width": "10%", "targets": 8, "orderable": true },
				{ "width": "10%", "targets": 9, "orderable": true }
			]
		});

		var detailRows = [];
		$('#tabel_riwayat_therapy tbody').on( 'click', '.btn-detail-theraphy', function (){
				var tr = $(this).closest('tr');
				var row = tableTherapy.row( tr );
				var idx = $.inArray( tr.attr('id'), detailRows );

				if ( row.child.isShown() ) {
						tr.removeClass( 'details' );
						row.child.hide();

						// Remove from the 'open' array
						detailRows.splice( idx, 1 );
				} else {
						var idfarmasi = tr.find("td:eq(9) button").data('idfarmasi');

						$.ajax({
							url:"{site_url}trm_berkas/getDataFarmasi/" + idfarmasi,
							dataType:"json",
							success:function(data){
								tr.addClass('details');
								row.child( viewDetailTheraphy(data) ).show();

								// Add to the 'open' array
								if ( idx === -1 ) {
									detailRows.push( tr.attr('id') );
								}
							}
						});

				}
		});

		// On each draw, loop over the `detailRows` array and show any child rows
		tableTherapy.on('draw', function(){
				$.each( detailRows, function(i, id){
						$('#'+id+' td.details-control').trigger('click');
				});
		});
	}

	function viewDetailTheraphy(data) {
		var rows = '';

		rows += '<div class="col-md-12">';
		rows += '		<table class="table" style="background-color: transparent;">';
		rows += '			<thead>';
		rows += '				<tr>';
		rows += '					<th width="10%"><b>Tanggal</b></th>';
		rows += '					<th width="10%"><b>No Penjualan</b></th>';
		rows += '					<th width="10%"><b>Kategori</b></th>';
		rows += '					<th width="10%"><b>Nama Obat</b></th>';
		rows += '					<th width="10%"><b>Harga Satuan</b></th>';
		rows += '					<th width="10%"><b>Kuantitas</b></th>';
		rows += '					<th width="10%"><b>Jumlah</b></th>';
		rows += '				</tr>';
		rows += '			</thead>';
		rows += '			<tbody>';

		data.forEach((item, i) => {
			rows += '				<tr>';
			rows += '					<td>' + item.tanggal +'</td>';
			rows += '					<td>' + item.nopenjualan +'</td>';
			rows += '					<td>' + item.kategori +'</td>';
			rows += '					<td>' + item.namabarang +'</td>';
			rows += '					<td>' + $.number(item.harga) +'</td>';
			rows += '					<td>' + $.number(item.kuantitas) +'</td>';
			rows += '					<td>' + $.number(item.totalharga) +'</td>';
			rows += '				</tr>';
		});

		rows += '			</tbody>';
		rows += '	</table>';
		rows += '</div>';

		return rows;
	}
</script>
