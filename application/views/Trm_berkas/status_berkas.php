<div class="row">
	<div class="col-md-12">
		<div class="block block-themed">
			<div class="block-header bg-primary">
				<ul class="block-options">
					<li>
						<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="si si-user"></i> Status Berkas</h3>
			</div>
			<div class="block-content">
				<form class="form-horizontal push-10-t" action="base_forms_elements.html" method="post" onsubmit="return false;">
					<div class="form-group">
						<label class="col-md-2 control-label" for="val-username">Status<span class="text-danger">*</span></label>
						<div class="col-md-3">
							<select id="status_aktif" name="status_aktif" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>-Semua Status -</option>
								<option value="1" <?=($st_aktif=='1'?'selected':'')?>>Active</option>
								<option value="2" <?=($st_aktif=='2'?'selected':'')?>>In Active</option>
								<option value="3" <?=($st_aktif=='3'?'selected':'')?>>Retensi</option>
								<option value="4" <?=($st_aktif=='4'?'selected':'')?>>Not Active</option>
							</select>
						</div>
						<label class="col-md-2 control-label" for="val-username">Last Visit <span class="text-danger">*</span></label>
						<div class="col-md-3">
							<div class="input-group">
								<input class="js-datepicker form-control" type="text" id="last_visit" name="last_visit" placeholder="Last Visit" value="<?=HumanDateShort($last_visit)?>" data-date-format="dd/mm/yyyy" />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label" for="val-username">Est. In Active <span class="text-danger">*</span></label>
						<div class="col-md-3">
							<div class="input-group">
								<input class="js-datepicker form-control" type="text" id="est_in" name="est_in" placeholder="Est. In Active" value="<?=HumanDateShort($est_in)?>" data-date-format="dd/mm/yyyy" />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
						<label class="col-md-2 control-label" for="val-username">Est. In Retensi <span class="text-danger">*</span></label>
						<div class="col-md-3">
							<div class="input-group">
								<input class="js-datepicker form-control" type="text" id="est_retensi" name="est_retensi" placeholder="Est. In Retensi" value="<?=HumanDateShort($est_retensi)?>" data-date-format="dd/mm/yyyy" />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label" for="val-username">Lokasi Berkas <span class="text-danger">*</span></label>
						<div class="col-md-8">
							<textarea class="form-control" id="lokasi_berkas" name="lokasi_berkas" rows="3" placeholder="Lokasi berkas"><?=$lokasi_berkas?></textarea>
						</div>
					</div>
					<?php if (UserAccesForm($user_acces_form,array('1173'))) { ?>
						<hr>
						<div class="form-group">
							<label class="col-md-2 control-label" for="val-username"></label>
							<div class="col-md-10">
								<button class="btn btn-success" type="button" id="btn_save_status" name="btn_save_status" style="width:20%;float:left;"><i class="fa fa-save"></i> Update </button>
							</div>
						</div>
					<? } ?>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		// alert($("#idpasien").val());
		// LoadIndex();
	});

	$(document).on("change", "#last_visit", function() {
		$.ajax({
			url: '{site_url}trm_berkas/change_last_visit/',
			type: "POST",
			dataType: 'json',
			data: {
				last_visit: $(this).val()
			},
			success: function(data) {
				$("#est_in").val(data.est_in);
				$("#est_retensi").val(data.est_retensi);
			}
		});
	});

	$(document).on("change", "#est_in", function() {
		$.ajax({
			url: '{site_url}trm_berkas/change_est_in/',
			type: "POST",
			dataType: 'json',
			data: {
				est_in: $(this).val()
			},
			success: function(data) {
				$("#est_retensi").val(data.est_retensi);
			}
		});
	});

	$(document).on("click", "#btn_save_status", function() {
		swal({
			title: "Apakah Anda Yakin ?",
			text: "Akan Ubah Status Berkas?",
			type: "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			save_status();
		});
	});

	function save_status() {
		var idpasien = $("#idpasien").val();
		var st_aktif = $("#status_aktif").val();
		var last_visit = $("#last_visit").val();
		var est_in = $("#est_in").val();
		var est_retensi = $("#est_retensi").val();
		var lokasi_berkas = $("#lokasi_berkas").val();

		$.ajax({
			url: '{site_url}trm_berkas/save_status_berkas/',
			type: "POST",
			dataType: 'json',
			data: {
				idpasien: idpasien,
				st_aktif: st_aktif,
				last_visit: last_visit,
				est_in: est_in,
				est_retensi: est_retensi,
				lokasi_berkas: lokasi_berkas
			},
			success: function(data) {
				if (data) {
					$.toaster({
						priority: 'success',
						title: 'Succes!',
						message: 'data berhasil disimpan'
					});
					setTimeout(function() {
						window.location.reload(true);
					}, 1000);
				}
			}
		});
	}
</script>
