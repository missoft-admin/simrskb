<div class="row">
	<div class="col-md-12">
		<!-- Static Labels block-opt-hidden -->
		<div class="block block-themed block-opt-hidden" id="div_kodefikasi_lab">
			<div class="block-header bg-primary">
				<ul class="block-options ">
					<li>
						<button id="button_up_lab" class="disable read" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="si si-chemistry"></i> Lihat Hasil Laboratorium</h3>
			</div>
			<div class="block-content block-content">
				<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
					<input class="form-control"  value="0" type="hidden" readonly id="st_load_lab">
					<div class="form-group">
						
						<div class="col-sm-12">
							<table class="table table-bordered table-striped" id="tabel_lab" style="width: 100%;">
								<thead>
									<tr>
										<th style="width:40%" class="text-center">Tarif</th>
										<th style="width:10%" class="text-center">Hasil</th>
										<th style="width:10%" class="text-center">Satuan</th>
										<th style="width:10%" class="text-center">Nilai Normal</th>
										<th style="width:30%" class="text-center">Keterangan</th>
									</tr>
									
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END Static Labels -->
	</div>
	
</div>


<script type="text/javascript">

    $(document).ready(function() { 
		
    });
	
	$(document).on("click", "#button_up_lab", function() {//Load Hanya Pas Open
		set_view();
		if ($("#div_kodefikasi_lab").attr('class')=='block block-themed'){
			if ($("#st_load_lab").val()=='0'){
				LoadLab();
				
			}
			$("#st_load_lab").val('1');
		}
	});
	function LoadLab(){
		var idberkas=$("#idberkas").val();
		$('#tabel_lab').DataTable().destroy();
		var table = $('#tabel_lab').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}trm_berkas/load_lab/',
			type: "POST",
			dataType: 'json',
			data: {
				idberkas: idberkas
			}
		},
		columnDefs: [
		 { "width": "40%", "targets": [0] },
		 { "width": "10%", "targets": [1,2,3] },
		 { "width": "30%", "targets": [4] },
		 // {  className: "text-center", targets:[0,1,2,3,4] },
					]
		});
		// auto_tambahan();
	}
</script>