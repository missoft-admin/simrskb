
<div class="row">
	<div class="col-md-12">
		<!-- Static Labels block-opt-hidden -->
		<div class="block block-themed block-opt-hidden" id="div_kodefikasi10">
			<div class="block-header bg-primary">
				<ul class="block-options">
					<li>
						<button id="button_up_idc10" class="disable read" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="si si-note"></i>  Input ICD 10</h3>
			</div>
			<div class="block-content block-content">
				<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
					<div class="pull-r-l pull-t push">
					<input class="form-control"  value="0" type="hidden" readonly id="st_load_icd10">
					<input class="form-control"  value="1" type="hidden" readonly id="cara_input_1">
							<input class="form-control"  value="0" type="hidden" readonly id="tedit_1">
					<div class="form-group" style="margin-bottom: 10px;">
						<div class="col-sm-6">
							<?=text_success('Pilih Diagnosa Utama & Tambahan')?>
						</div>
						
					</div>
					
					<table class="table table-bordered table-striped" id="tabel_icd10_1"  style="width: 100%;table-layout: fixed;">
						<thead>
							
							<tr>
								<th style="width:30%" class="text-center">ICD 10</th>
								<th style="width:20%" class="text-center">Kelompok Diagnosa</th>
								<th style="width:15%" class="text-center">Jenis Diagnosa</th>
								<th style="width:15%" class="text-center">User</th>
								<th style="width:10%" class="text-center">Aksi</th>								
							</tr>
							<tr>
								<td class="text-center">
										<select name="icd_id_1" id="icd_id_1" tabindex="16" style="width: 100%"  data-placeholder="Cari ICD 10" class="form-control input-sm disable">
										</select>
										
								</td>
								<td class="text-center">
										<select name="kd_1" id="kd_1" tabindex="16" style="width: 100%"  data-placeholder="Cari Kelompok Diagnosa" class="js-example-responsive disable">
										</select>
										
								</td>
								<td class="text-center">
								
									<select name="jenis_1" id="jenis_1" tabindex="16" style="width: 100%"  data-placeholder="Cari Jenis" class="js-select2 form-control input-sm disable">
										<option value="1" >Utama</option>												
										<option value="2" selected>Tambahan</option>												
									</select>										
								</td>
								<td class="text-center"><label class="small" id="user_1"><?=$this->session->userdata('user_name').'<br>'.date('d-m-Y')?></label></td>
								<td class="text-center">		
									<div class="btn-group" role="group">
									<button type="button" class="btn btn-sm btn-primary disable" tabindex="8" id="add_1" title="Masukan Item"><i class="fa fa-plus"></i></button>&nbsp;&nbsp;
									<button type="button" class="btn btn-sm btn-danger disable" tabindex="9" id="cancel_1" title="Refresh"><i class="fa fa-times"></i></button>
									</div>
								</td>	
							</tr>
						</thead>
						<tbody></tbody>
					</table>
					
					<div class="form-group" style="margin-bottom: 10px;">
						<div class="col-sm-6">
							<?=text_success('Pilih Kode Kontrol')?>
						</div>
						
					</div>
					
					<table class="table table-bordered table-striped" id="tabel_icd10_2"  style="width: 100%;table-layout: fixed;">
						<thead>
							
							<tr>
								<th style="width:30%" class="text-center">ICD 10</th>
								<th style="width:20%" class="text-center">Keterangan Kontrol</th>
								<th style="width:15%" class="text-center">Jenis Diagnosa</th>
								<th style="width:15%" class="text-center">User</th>
								<th style="width:10%" class="text-center">Aksi</th>								
							</tr>
							<tr>
								<td class="text-center">
										<select name="icd_id_2" id="icd_id_2" tabindex="16" style="width: 100%"  data-placeholder="Cari ICD 10" class="form-control input-sm disable">
										</select>
										
								</td>
								<td class="text-center">
										<select name="kd_2" id="kd_2" tabindex="16" style="width: 100%"  data-placeholder="Cari Keterangan Kontrol" class="js-example-responsive disable">
										</select>
										
								</td>
								<td class="text-center">
								
									<select name="jenis_2" id="jenis_2" tabindex="16" style="width: 100%"  data-placeholder="Cari Jenis" class="js-select2 form-control input-sm disable">
										<option value="3" selected>Kontrol</option>												
									</select>										
								</td>
								<td class="text-center"><label class="small" id="user_1"><?=$this->session->userdata('user_name').'<br>'.date('d-m-Y')?></label></td>
								<td class="text-center">		
									<div class="btn-group" role="group">
									<button type="button" class="btn btn-sm btn-primary disable" tabindex="8" id="add_2" title="Masukan Item"><i class="fa fa-plus"></i></button>&nbsp;&nbsp;
									<button type="button" class="btn btn-sm btn-danger disable" tabindex="9" id="cancel_2" title="Refresh"><i class="fa fa-times"></i></button>
									</div>
								</td>	
							</tr>
						</thead>
						<tbody></tbody>
					</table>
					<div class="form-group" style="margin-bottom: 10px;">
						<div class="col-sm-6">
							<?=text_success('Pilih External Cause')?>
						</div>
						
					</div>
					
					<table class="table table-bordered table-striped" id="tabel_icd10_3"  style="width: 100%;table-layout: fixed;">
						<thead>
							
							<tr>
								<th style="width:30%" class="text-center">ICD 10</th>
								<th style="width:20%" class="text-center">Ket. Sebab Luar</th>
								<th style="width:15%" class="text-center">Jenis Diagnosa</th>
								<th style="width:15%" class="text-center">User</th>
								<th style="width:10%" class="text-center">Aksi</th>								
							</tr>
							<tr>
								<td class="text-center">
										<select name="icd_id_2" id="icd_id_3" tabindex="16" style="width: 100%"  data-placeholder="Cari ICD 10" class="form-control input-sm disable">
										</select>
										
								</td>
								<td class="text-center">
										<select name="kd_3" id="kd_3" tabindex="16" style="width: 100%"  data-placeholder="Keterangan Sebab Luar" class="js-example-responsive disable">
										</select>
										
								</td>
								<td class="text-center">
								
									<select name="jenis_3" id="jenis_3" tabindex="16" style="width: 100%"  data-placeholder="Cari Jenis" class="js-select2 form-control input-sm disable">
										<option value="4" selected>External Cause</option>												
									</select>										
								</td>
								<td class="text-center"><label class="small" id="user_1"><?=$this->session->userdata('user_name').'<br>'.date('d-m-Y')?></label></td>
								<td class="text-center">		
									<div class="btn-group" role="group">
									<button type="button" class="btn btn-sm btn-primary disable" tabindex="8" id="add_3" title="Masukan Item"><i class="fa fa-plus"></i></button>&nbsp;&nbsp;
									<button type="button" class="btn btn-sm btn-danger disable" tabindex="9" id="cancel_3" title="Refresh"><i class="fa fa-times"></i></button>
									</div>
								</td>	
							</tr>
						</thead>
						<tbody></tbody>
					</table>
					
					
					
					<div class="form-group" style="margin-bottom: 10px;">
						<div class="col-sm-6">
							<?=text_danger('Hasil Inputan ICD 10')?>
						</div>
						
					</div>
					
					<table class="table table-bordered table-striped" id="tabel_icd10" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:30%" class="text-center">ICD 10</th>
								<th style="width:15%" class="text-center">Jenis Diagnosa</th>
								<th style="width:20%" class="text-center">Keterangan</th>
								<th style="width:15%" class="text-center">Cara Input</th>
								<th style="width:15%" class="text-center">User</th>
								<th style="width:15%" class="text-center">Aksi</th>								
								<th style="display:none" class="text-center"></th>								
								<th style="display:none" class="text-center"></th>								
								<th style="display:none" class="text-center"></th>								
								<th style="display:none" class="text-center"></th>								
								<th style="display:none" class="text-center"></th>								
								<th style="display:none" class="text-center"></th>								
								<th style="display:none" class="text-center"></th>								
								<th style="display:none" class="text-center"></th>								
							</tr>
							
						</thead>
						<tbody></tbody>
					</table>
					
					</div>
				</form>
			</div>
		</div>
		<!-- END Static Labels -->
	</div>	
</div>
<div class="modal" id="modal_sugges10" role="dialog" aria-hidden="true" aria-labelledby="modalAdd" font size="15">
    <div class="modal-dialog modal-lg" style="width:40%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Edit ICD Berdasarkan Kelompok</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal">
                        <input type="hidden" id="sug10_kelompok_id" value="" />                        
                        <input type="hidden" id="sug10_kontrol_id" value="" />                        
                        <input type="hidden" id="sug10_sebab_id" value="" />                        
                        <input type="hidden" id="sug10_icd_id" value="" />                        

                        <div class="form-group">
                            <label class="col-md-4 control-label">Diagnosa</label>
                            <div class="col-md-8">
                                <input type="text" readonly class="form-control input-sm" id="sug10_nama_diagnosa" placeholder="" value="" required="" aria-required="true" >
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-4 control-label" id="sug10_label"></label>
                            <div class="col-md-8">
                                <input type="text" readonly class="form-control input-sm" id="sug10_nama_kel" placeholder="" value="" required="" aria-required="true" >
                            </div>
                        </div>
						<table class="table table-bordered table-striped" id="tabel_sugges10" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:70%" class="text-center">ICD</th>
								<th style="width:20%" class="text-center">Jenis</th>
								<th style="width:10%" class="text-center">Status</th>
								<th style="width:10%" class="text-center">Aksi</th>								
								<th style="display:none" class="text-center"></th>								
							</tr>
							
						</thead>
						<tbody></tbody>
					</table>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" id="tutup_icd10" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var table;
    $(document).ready(function() {        
       // loadICD_1();
	   // auto_tambahan();
    });
	$(document).on("click", "#button_up_idc10", function() {//Load Hanya Pas Open
		set_view();
	});
	function loadICD_1(){
		var idberkas=$("#idberkas").val();
		
		$('#tabel_icd10').DataTable().destroy();
		var table = $('#tabel_icd10').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}trm_berkas/load_icd/1',
			type: "POST",
			dataType: 'json',
			data: {
				idberkas: idberkas
			}
		},
		columnDefs: [
		{"targets": [6,7,8,9,10,11,12,13], "visible": false },
		 { "width": "30%", "targets": [0] },
		 { "width": "20%", "targets": [1] },
		 { "width": "15%", "targets": [3,4] },
		 { "width": "10%", "targets": [5] },
		 {  className: "text-center", targets:[0,1,2,3,4] },
					]
		});
		// auto_tambahan();
	}
	function load_suges10($tipe){
		var tipe=$tipe;
		var kel_id=$("#sug10_kelompok_id").val();
		var icd_id=$("#sug10_icd_id").val();
		// alert(tipe+':'+kel_id+':'+icd_id);
		$('#tabel_sugges10').DataTable().destroy();
		var table = $('#tabel_sugges10').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}trm_berkas/load_suges10/',
			type: "POST",
			dataType: 'json',
			data: {
				tipe: tipe,kel_id: kel_id,icd_id: icd_id,
			}
		},
		columnDefs: [
		{"targets": [4], "visible": false },
		 { "width": "70%", "targets": [0] },
		 { "width": "20%", "targets": [1] },
		 { "width": "10%", "targets": [2,3] },
		 {  className: "text-center", targets:[3] },
					]
		});
		// auto_tambahan();
	}
	$(document).on("click",".sugges",function(){
		table = $('#tabel_icd10').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,6).data()
		var kd_id = table.cell(tr,8).data()
		var kontrol_id = table.cell(tr,12).data()
		var sebab_id = table.cell(tr,13).data()
		var nama = table.cell(tr,2).data()
		var cara = table.cell(tr,9).data()
		var icd_id = table.cell(tr,7).data()
		
		var label = ''
		$('#sug10_icd_id').val(icd_id);		
		$('#sug10_nama_kel').val(nama);
		$('#tedit_1').val(id);
		if (cara=='1' || cara=='2'){
			label="Kelompok Diagnosa";
			$('#sug10_kelompok_id').val(kd_id);
			$('#sug10_nama_diagnosa').val('Diagnosa Utama & Tambahan');
			load_suges10('1');
		}else if (cara=='3'){
			label="Keterangan Kontrol";	
			$('#sug10_kelompok_id').val(kontrol_id);
			$('#sug10_nama_diagnosa').val('Diagnosa Kontrol');
			load_suges10('3');
		}else if (cara=='4'){
			label="Sebab Luar";		
			$('#sug10_nama_diagnosa').val('Diagnosa Sebab Luar');
			$('#sug10_kelompok_id').val(sebab_id);
			load_suges10('4');
		}
		
		$('#sug10_label').text(label);
		$('#modal_sugges10').modal('show')
	});
	$(document).on("click",".pilih_icd",function(){
		table = $('#tabel_sugges10').DataTable()
		var tr = $(this).parents('tr')
		var icd_id = table.cell(tr,4).data()
		var id = $("#tedit_1").val();
		
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Mengganti ICD 10 ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}trm_berkas/ganti_sugges_icd10',
				type: 'POST',
				data: {
					id: id,icd_id:icd_id	
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' ICD Berhasil diganti'});
					$('#tabel_icd10').DataTable().ajax.reload( null, false );
					$('#modal_sugges10').modal('hide')
								
					// window.location.href = "{site_url}tko/index/"+idunit;
				}
			});
		});
		
		return false;
		
	});
	$("#icd_id_1").select2({
			minimumInputLength: 2,
			noResults: 'ICD Tidak Ditemukan.',
			// width: 'element',
		ajax: {
			url: '{site_url}mkelompok_diagnosa/js_icd/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,
		 data: function (params) {
			  var query = {
				search: params.term
			  }
			  return query;
			},
			processResults: function (data) {
				console.log($("#icd_id_1").val());
				return {
					results: $.map(data, function (item) {
						return {
							text: item.kode + ' - '+ item.deskripsi,
							id: item.id
						}
					})
				};
			}
		}
	});
	$("#icd_id_2").select2({
			minimumInputLength: 2,
			noResults: 'ICD Tidak Ditemukan.',
			// width: 'element',
		ajax: {
			url: '{site_url}mkelompok_diagnosa/js_icd/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,
		 data: function (params) {
			  var query = {
				search: params.term
			  }
			  return query;
			},
			processResults: function (data) {
				console.log($("#icd_id_1").val());
				return {
					results: $.map(data, function (item) {
						return {
							text: item.kode + ' - '+ item.deskripsi,
							id: item.id
						}
					})
				};
			}
		}
	});
	$("#icd_id_3").select2({
			minimumInputLength: 2,
			noResults: 'ICD Tidak Ditemukan.',
			// width: 'element',
		ajax: {
			url: '{site_url}mkelompok_diagnosa/js_icd/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,
		 data: function (params) {
			  var query = {
				search: params.term
			  }
			  return query;
			},
			processResults: function (data) {
				console.log($("#icd_id_1").val());
				return {
					results: $.map(data, function (item) {
						return {
							text: item.kode + ' - '+ item.deskripsi,
							id: item.id
						}
					})
				};
			}
		}
	});
	
	$("#kd_1").select2({
			minimumInputLength: 2,
			noResults: 'ICD Tidak Ditemukan.',
			// width: 'element',
		ajax: {
			url: '{site_url}trm_berkas/js_kelompok_diagnosa/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,
		 data: function (params) {
			  var query = {
				search: params.term
			  }
			  return query;
			},
			processResults: function (data) {
				console.log($("#kd_1").val());
				return {
					results: $.map(data, function (item) {
						return {
							text: item.nama,
							id: item.id
						}
					})
				};
			}
		}
	});
	$("#kd_2").select2({
			minimumInputLength: 2,
			noResults: 'ICD Tidak Ditemukan.',
			// width: 'element',
		ajax: {
			url: '{site_url}trm_berkas/js_ket_kontrol/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,
		 data: function (params) {
			  var query = {
				search: params.term
			  }
			  return query;
			},
			processResults: function (data) {
				console.log(data);
				return {
					results: $.map(data, function (item) {
						return {
							text: item.nama,
							id: item.id
						}
					})
				};
			}
		}
	});
	$("#kd_3").select2({
			minimumInputLength: 2,
			noResults: 'ICD Tidak Ditemukan.',
			// width: 'element',
		ajax: {
			url: '{site_url}trm_berkas/js_sebab_luar/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,
		 data: function (params) {
			  var query = {
				search: params.term
			  }
			  return query;
			},
			processResults: function (data) {
				console.log(data);
				return {
					results: $.map(data, function (item) {
						return {
							text: item.nama,
							id: item.id
						}
					})
				};
			}
		}
	});
	$("#kd_1").change(function(){
		
		if ($(this).val()!=null){
			$.ajax({
			  url: '{site_url}trm_berkas/get_diagnosa/'+$(this).val(),
			  dataType: "json",
			  success: function(data) {
				  console.log(data);
				  var data2 = {
						id: data.id,
						text: data.nama
					};
					var newOption = new Option(data2.text, data2.id, true, true);
					$('#icd_id_1').append(newOption);
					$("#cara_input_1").val('2');
				
			  }
			});
		}else{

		}
	});
	$("#kd_2").change(function(){
		
		if ($(this).val()!=null){
			$.ajax({
			  url: '{site_url}trm_berkas/get_kontrol/'+$(this).val(),
			  dataType: "json",
			  success: function(data) {
				  console.log(data);
				  if (data){
				  var data2 = {
						id: data.id,
						text: data.nama
					};
					var newOption = new Option(data2.text, data2.id, true, true);
					$('#icd_id_2').append(newOption);
					$("#cara_input_1").val('2');
				  }else{
					  sweetAlert("Maaf...", "Data Tidak Ditemukan", "error");
					  $("#kd_2").val(null).trigger('change');
					  return false;
				  }
				
			  }
			});
		}else{

		}
	});
	$("#kd_3").change(function(){
		
		if ($(this).val()!=null){
			$.ajax({
			  url: '{site_url}trm_berkas/get_sebab/'+$(this).val(),
			  dataType: "json",
			  success: function(data) {
				  console.log(data);
				  if (data){
				  var data2 = {
						id: data.id,
						text: data.nama
					};
					var newOption = new Option(data2.text, data2.id, true, true);
					$('#icd_id_3').append(newOption);
					$("#cara_input_1").val('2');
				  }else{
					  sweetAlert("Maaf...", "Data Tidak Ditemukan", "error");
					  $("#kd_3").val(null).trigger('change');
					  return false;
				  }
				
			  }
			});
		}else{

		}
	});
	$("#icd_id_1").change(function(){
		$("#cara_input_1").val('1');
	});
	$("#icd_id_2").change(function(){
		$("#cara_input_1").val('1');
	});
	$("#icd_id_3").change(function(){
		$("#cara_input_1").val('1');
	});
	$(document).on("click","#add_1",function(){
		// alert($("#jenis_1").val());
		if (validate_add_1(1)==false)return false;
			var duplicate=false;
			var table = $('#tabel_icd10').DataTable();		
			table.rows().eq(0).each( function ( index ) {
				var row = table.row( index );		 
				var data = row.data();
				console.log(data[9]);
				if (data[9]=='1' && $("#jenis_1").val()=='1' && $("#tedit_1").val()=='0'){
					duplicate = true;
					sweetAlert("Duplicate...", "Diagnosa Utama Harus satu!", "error");
				}
				// if ($("#jenis_id").val()=='1' && data[5]=='1'){
					// duplicate = true;
					// sweetAlert("Duplicate...", "Primary Duplicate!", "error");
				// }
			} );
		if(duplicate) return false;
		
		simpan(1);
		setTimeout(function() {
			auto_tambahan();
		}, 1000);
		// auto_tambahan(ketemu);
	});
	$(document).on("click","#add_2",function(){
		if (validate_add_1(2)==false)return false;
			var duplicate=false;
			var table = $('#tabel_icd10').DataTable();		
			table.rows().eq(0).each( function ( index ) {
			var row = table.row( index );		 
			var data = row.data();
			if (data[11]=='2' && $("#tedit_1").val()=='0'){
				duplicate = true;
				sweetAlert("Duplicate...", "Hanya Boleh diisi satu!", "error");
			}
			
			} );
		if(duplicate) return false;
		simpan(2);
		
	});
	
	$(document).on("click","#add_3",function(){
		if (validate_add_1(3)==false)return false;
			var duplicate=false;
			var table = $('#tabel_icd10').DataTable();		
			table.rows().eq(0).each( function ( index ) {
			var row = table.row( index );		 
			var data = row.data();
			if (data[11]=='3' && $("#tedit_1").val()=='0'){
				duplicate = true;
				sweetAlert("Duplicate...", "Eksternal Cause Hanya Boleh diisi satu!", "error");
			}
			
			} );
		if(duplicate) return false;
		simpan(3);
		
	});
	
	
	function simpan($tipe_tabel){
		var idberkas=$("#idberkas").val();		
		var tipe_tabel=$tipe_tabel;	
		if (tipe_tabel=='1'){
			var icd_id=$("#icd_id_1").val();	
			var kelompok_diagnosa_id=$("#kd_1").val();	
			var jenis_id=$("#jenis_1").val();	
		}else if (tipe_tabel=='2'){
			var icd_id=$("#icd_id_2").val();	
			var kelompok_diagnosa_id=$("#kd_2").val();	
			var jenis_id=$("#jenis_2").val();			
		}else{
			var icd_id=$("#icd_id_3").val();	
			var kelompok_diagnosa_id=$("#kd_3").val();	
			var jenis_id=$("#jenis_3").val();	
		}
		// alert(kelompok_diagnosa_id);return false
		var cara_input=$("#cara_input_1").val();		
		var tedit=$("#tedit_1").val();		
		// alert($("#id").val());
		if (tedit =='0'){
			$.ajax({
				url: '{site_url}trm_berkas/simpan_add/'+$tipe_tabel,
				type: 'POST',
				data: {
					idberkas: idberkas,jenis_id: jenis_id,cara_input: cara_input,
					icd_id: icd_id,kelompok_diagnosa_id: kelompok_diagnosa_id,tipe_tabel: tipe_tabel		
				},
				complete: function(data) {
					if (data){
					$.toaster({priority : 'success', title : 'Succes!', message : ' ICD Berhasil Disimpan'});
						$('#tabel_icd10').DataTable().ajax.reload( null, false );
						clear($tipe_tabel);
					}
								
					// window.location.href = "{site_url}tko/index/"+idunit;
				}
			});
		}else{
			$.ajax({
				url: '{site_url}trm_berkas/simpan_edit/'+$tipe_tabel,
				type: 'POST',
				data: {
					idberkas: idberkas,jenis_id: jenis_id,cara_input: cara_input,
					icd_id: icd_id,kelompok_diagnosa_id: kelompok_diagnosa_id,id:tedit		
				},
				complete: function(data) {
					if (data){
						$.toaster({priority : 'success', title : 'Succes!', message : ' ICD Berhasil Disimpan'});
						$('#tabel_icd10').DataTable().ajax.reload( null, false );
						clear($tipe_tabel);
					}
				}
			});
		}
		$('#tedit').val(0);	
	}
	function auto_tambahan(){
		var ketemu=false;
		var table = $('#tabel_icd10').DataTable();		
		table.rows().eq(0).each( function ( index ) {
			var row = table.row( index );		 
			var data = row.data();
			console.log(data[9]);
			if (data[9]=='1'){
				ketemu = true;
				
			}
			
		} );
		if (ketemu==true){
			$("#jenis_1").val(2).trigger('change');
		}else{
			$("#jenis_1").val(1).trigger('change');
		}
	}
	$(document).on("click",".edit_1",function(){
		table = $('#tabel_icd10').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,6).data()
		var icd_nama = table.cell(tr,0).data()
		var icd_id = table.cell(tr,7).data()
		var kd_nama = table.cell(tr,2).data()
		var jenis_id = table.cell(tr,9).data()
		var jenis_nama = table.cell(tr,1).data()
		var cara_input = table.cell(tr,10).data()
		var tipe_tabel = table.cell(tr,11).data()
		if (jenis_id=='1' || jenis_id=='2'){
			var kd_id = table.cell(tr,8).data()			
		}else if (jenis_id=='3'){
			var kd_id = table.cell(tr,12).data()
		}else{
			var kd_id = table.cell(tr,13).data()
		}
		// alert(tipe_tabel +':'+ kd_id);
		if (tipe_tabel=='1'){
			var data2 = {id: icd_id,text: icd_nama};
			var newOption = new Option(data2.text, data2.id, true, true);
			$('#icd_id_1').append(newOption);
			var data3 = {id: kd_id,text: kd_nama};
			var newOption2 = new Option(data3.text, data3.id, true, true);
			$('#kd_1').append(newOption2);		
			$('#jenis_1').val(jenis_id).trigger('change');
		}else if (tipe_tabel=='2'){
			var data2 = {id: icd_id,text: icd_nama};
			var newOption = new Option(data2.text, data2.id, true, true);
			$('#icd_id_2').append(newOption);
			var data3 = {id: kd_id,text: kd_nama};
			var newOption2 = new Option(data3.text, data3.id, true, true);
			$('#kd_2').append(newOption2);		
			$('#jenis_2').val(jenis_id).trigger('change');
			
		}else{
			var data2 = {id: icd_id,text: icd_nama};
			var newOption = new Option(data2.text, data2.id, true, true);
			$('#icd_id_3').append(newOption);
			var data3 = {id: kd_id,text: kd_nama};
			var newOption2 = new Option(data3.text, data3.id, true, true);
			$('#kd_3').append(newOption2);		
			$('#jenis_3').val(jenis_id).trigger('change');
		}
		
		$('.edit_1').attr('disabled',true);
		$('.hapus_1').attr('disabled',true);
		$('.sugges').attr('disabled',true);
		$('#tedit_1').val(id);
		$('#cara_input_1').val(cara_input);
		
	});
	
	$(document).on("click",".hapus_1",function(){
		table = $('#tabel_icd10').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,6).data()
		// alert(id);
		// var idunit = table.cell(tr,7).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menghapus ICD 10 ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}trm_berkas/hapus_icd',
				type: 'POST',
				data: {
					id: id	
				},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' ICD Berhasil dihapus'});
					$('#tabel_icd10').DataTable().ajax.reload( null, false );
								
					// window.location.href = "{site_url}tko/index/"+idunit;
				}
			});
		});
		
		return false;
	});
	$(document).on("click","#cancel_1",function(){
		clear(1);
		
	});
	$(document).on("click","#cancel_2",function(){
		clear(2);
		
	});
	$(document).on("click","#cancel_3",function(){
		clear(3);
		
	});
	function validate_add_1($tipe_tabel)
	{
		if ($tipe_tabel=='1'){
			if ($("#icd_id_1").val()=='' || $("#icd_id_1").val()==null || $("#icd_id_1").val()=="#"){
				sweetAlert("Maaf...", "ICD 10 Harus diisi", "error");
				return false;
			}
		}else if ($tipe_tabel=='2'){
			if ($("#icd_id_2").val()=='' || $("#icd_id_2").val()==null || $("#icd_id_2").val()=="#"){
				sweetAlert("Maaf...", "ICD 10 Harus diisi", "error");
				return false;
			}
		}else{
			if ($("#icd_id_3").val()=='' || $("#icd_id_3").val()==null || $("#icd_id_3").val()=="#"){
				sweetAlert("Maaf...", "ICD 10 Harus diisi", "error");
				return false;
			}
		}
		
		
		return true;
	}
	function clear($tipe_tabel){
		$("#tedit_1").val('0');
		$("#cara_input_1").val('1');
		if ($tipe_tabel=='1'){
			$("#icd_id_1").val(null).trigger('change');
			$("#kd_1").val(null).trigger('change');
		}else if ($tipe_tabel=='2'){
			$("#icd_id_2").val(null).trigger('change');
			$("#kd_2").val(null).trigger('change');
		}else{
			$("#icd_id_3").val(null).trigger('change');
			$("#kd_3").val(null).trigger('change');
		}
		$('.sugges').attr('disabled',false)
		$('.hapus_1').attr('disabled',false)
		$('.edit_1').attr('disabled',false)
	}
</script>