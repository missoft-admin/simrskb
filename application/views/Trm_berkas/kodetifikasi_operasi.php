<div class="row">
	<div class="col-md-12">
		<!-- Static Labels block-opt-hidden -->
		<div class="block block-themed block-opt-hidden" id="div_kodefikasi_operasi">
			<div class="block-header bg-primary">
				<ul class="block-options ">
					<li>
						<button id="button_up_operasi" class="disable read" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="fa fa-bed"></i>  Informasi Operasi</h3>
			</div>
			<div class="block-content block-content">
				<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
					<input class="form-control"  value="0" type="hidden" readonly id="st_load_operasi">
					<div class="form-group">
						
						<div class="col-sm-12" style="margin-bottom: 15px;">
							<h5><?=text_success('History Operasi')?></h5>
						</div>
						
						<div class="col-sm-12">
							<table class="table table-bordered table-striped" id="tabel_operasi" style="width: 100%;">
								<thead>
									<tr>
										<th style="width:50%" class="text-center">Tanggal</th>
										<th style="width:30%" class="text-center">Jam</th>
										<th style="width:20%" class="text-center">Diagnosa</th>
										<th style="width:20%" class="text-center">Operasi</th>
										<th style="width:20%" class="text-center">Jenis Operasi</th>
										<th style="width:20%" class="text-center">Dokter Operator</th>
									</tr>
									
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END Static Labels -->
	</div>
	
</div>


<script type="text/javascript">

    $(document).ready(function() { 
		
    });
	
	$(document).on("click", "#button_up_operasi", function() {//Load Hanya Pas Open
		set_view();
		if ($("#div_kodefikasi_operasi").attr('class')=='block block-themed'){
			if ($("#st_load_operasi").val()=='0'){
				LoadOperasi();
				
			}
			$("#st_load_operasi").val('1');
		}
	});
	function LoadOperasi(){
		var idpasien=$("#idpasien").val();
		$('#tabel_operasi').DataTable().destroy();
		var table = $('#tabel_operasi').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}trm_berkas/load_operasi/',
			type: "POST",
			dataType: 'json',
			data: {
				idpasien: idpasien
			}
		},
		columnDefs: [
		 { "width": "10%", "targets": [0,1] },
		 { "width": "15%", "targets": [2,3,4] },
		 { "width": "20%", "targets": [5] },
		 {  className: "text-center", targets:[0,1,2,3,4,5] },
					]
		});
		// auto_tambahan();
	}
</script>