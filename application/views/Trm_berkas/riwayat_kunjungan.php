<div class="row">
	<div class="col-md-12">
		<div class="block block-themed block-opt-hidden" id="div_kunjungan">
			<div class="block-header bg-primary">
				<ul class="block-options">
					<li>
						<button id="button_up_kunjungan" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="si si-login"></i>  Riwayat Kunjungan</h3>
			</div>
			<div class="block-content block-content">
				<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
					<input class="form-control" value="0" type="hidden" readonly id="st_load_kunjungan">
					<table class="table table-bordered table-striped" id="tabel_riwayat_kunjungan" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:10%">Tanggal</th>
								<th style="width:10%">Tujuan</th>
								<th style="width:10%">Klinik / Kelas</th>
								<th style="width:15%">Dokter</th>
								<th style="width:10%">User Daftar</th>
								<th style="width:10%">Status</th>
								<th style="width:10%">Tindak Lanjut</th>
								<th style="width:10%">Aksi</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>

<script type="text/javascript">
	$(document).on("click", "#button_up_kunjungan", function() {
		if ($("#div_kunjungan").attr('class')=='block block-themed'){
			if ($("#st_load_kunjungan").val()=='0'){
				LoadIndexKunjungan();
			}
			$("#st_load_kunjungan").val('1');
		}
	});

	function LoadIndexKunjungan(){
		var idpasien = '<?=$idpasien?>';
		$('#tabel_riwayat_kunjungan').DataTable().destroy();
		$('#tabel_riwayat_kunjungan').DataTable({
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"autoWidth": false,
			"fixedHeader": true,
			"order": [],
			"ajax": {
				url: '{site_url}trm_berkas/get_riwayat_kunjungan/',
				type: "POST",
				dataType: 'json',
				data: {idpasien: idpasien}
			},
			columnDefs: [
				 { "width": "10%"},
				 { "width": "10%"},
				 { "width": "10%"},
				 { "width": "10%"},
				 { "width": "10%"},
				 { "width": "10%"},
				 { "width": "10%"},
			]
		});

	}
</script>
