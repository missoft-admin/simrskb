
<div class="row">
	<div class="col-md-12">
		<!-- Static Labels -->
		<div class="block">
			<div class="block-header">
				<ul class="block-options">
					<li>
						<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title">Profile </h3>
			</div>
			<div class="block-content block-content">
				<form class="form-horizontal push-10-t" action="base_forms_elements.html" method="post" onsubmit="return false;">
					
					<div class="form-group">
						<div class="col-sm-6">
							<div class="form-material">
								<input class="form-control" value="" type="text" id="material-disabled" name="material-disabled" disabled="">
								<label for="material-disabled ">Nama</label>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-material">
								<input class="form-control" value="" type="text" id="material-disabled" name="material-disabled" disabled="">
								<label for="material-disabled ">ID Identitas</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<div class="form-material">
								<input class="form-control" value="" type="text" id="material-disabled" name="material-disabled" disabled="">
								<label for="material-disabled ">Jenis Kelamin</label>
							</div>
						</div>
						<div class="col-sm-3">
						</div>
						<div class="col-sm-6">
							<div class="form-material">
								<input class="form-control" value="" type="text" id="material-disabled" name="material-disabled" disabled="">
								<label for="material-disabled ">No HP</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<div class="form-material">
								<input class="form-control" value="" type="text" id="material-disabled" name="material-disabled" disabled="">
								<label for="material-disabled ">Tanggal Lahir</label>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-material">
								<input class="form-control" value="<?=getUmur($tanggal_lahir) ?>" type="text" id="material-disabled" name="material-disabled" disabled="">
								<label for="material-disabled ">Umur</label>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-material">
								<input class="form-control" value="" type="text" id="material-disabled" name="material-disabled" disabled="">
								<label for="material-disabled ">Telepon Rumah</label>
							</div>
						</div>
					</div>
				
					
				</form>
			</div>
		</div>
		<!-- END Static Labels -->
	</div>
	
</div>
