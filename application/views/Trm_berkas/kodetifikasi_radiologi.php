<div class="row">
	<div class="col-md-12">
		<!-- Static Labels block-opt-hidden -->
		<div class="block block-themed block-opt-hidden" id="div_kodefikasi_rad">
			<div class="block-header bg-primary">
				<ul class="block-options ">
					<li>
						<button id="button_up_rad" class="disable read" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="si si-film"></i> Lihat Hasil Radiologi</h3>
			</div>
			<div class="block-content block-content">
				<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
					<input class="form-control"  value="0" type="hidden" readonly id="st_load_rad">
					<div class="form-group">
						
						<div class="col-sm-12">
							<table class="table table-bordered table-striped" id="tabel_rad" style="width: 100%;">
								<thead>
									<tr>
										<th style="width:10%" class="text-center">Tipe</th>
										<th style="width:30%" class="text-center">Tindakan</th>
										<th style="width:30%" class="text-center">Posisi</th>
										<th style="width:30%" class="text-center">Hasil Foto</th>
									</tr>
									
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END Static Labels -->
	</div>
	
</div>


<script type="text/javascript">

    $(document).ready(function() { 
		
    });
	
	$(document).on("click", "#button_up_rad", function() {//Load Hanya Pas Open
		set_view();
		if ($("#div_kodefikasi_rad").attr('class')=='block block-themed'){
			if ($("#st_load_rad").val()=='0'){
				LoadRad();
				
			}
			$("#st_load_rad").val('1');
		}
	});
	function LoadRad(){
		var idberkas=$("#idberkas").val();
		$('#tabel_rad').DataTable().destroy();
		var table = $('#tabel_rad').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}trm_berkas/load_rad/',
			type: "POST",
			dataType: 'json',
			data: {
				idberkas: idberkas
			}
		},
		columnDefs: [
		 { "width": "10%", "targets": [0] },
		 { "width": "20%", "targets": [1,2] },
		 { "width": "30%", "targets": [3] },
		 // {  className: "text-center", targets:[0,1,2,3,4] },
					]
		});
		// auto_tambahan();
	}
</script>