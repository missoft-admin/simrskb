<div class="row">
	<div class="col-md-12">
		<div class="block block-themed block-opt-hidden" id="div_icd">
			<div class="block-header bg-primary">
				<ul class="block-options">
					<li>
						<button id="button_up_icd" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="si si-login"></i>  Riwayat ICD 9</h3>
			</div>
			<div class="block-content block-content">
				<form class="form-horizontal push-10-t" action="#" method="post" onsubmit="return false;">
					<input class="form-control" value="0" type="hidden" readonly id="st_load_icd">
					<table class="table table-bordered table-striped" id="tabel_riwayat_icd" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:10%">Tanggal</th>
								<th style="width:10%">Dokter</th>
								<th style="width:10%">Tindakan</th>
								<th style="width:15%">ICD 9 CM</th>
								<th style="width:10%">Deskripsi</th>
								<th style="width:10%">Kel. Tindakan</th>
								<th style="width:10%">Jenis</th>
								<th style="width:10%">User</th>
								<th style="width:10%">Verif</th>
								<th style="width:10%">Aksi</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(document).on("click", "#button_up_icd", function() {
		if ($("#div_icd").attr('class')=='block block-themed'){
			if ($("#st_load_icd").val()=='0'){
				LoadIndexICD9();
			}
			$("#st_load_icd").val('1');
		}
	});

	function LoadIndexICD9(){
		var idpasien = '<?=$idpasien?>';
		$('#tabel_riwayat_icd').DataTable().destroy();
		var tableICD9 = $('#tabel_riwayat_icd').DataTable({
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"autoWidth": false,
			"fixedHeader": true,
			"order": [],
			"ajax": {
				url: '{site_url}trm_berkas/get_riwayat_icd9/',
				type: "POST",
				dataType: 'json',
				data: {idpasien: idpasien}
			},
			"columns": [
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" },
				{ "class": "details-control", "orderable": false, "defaultContent": "" }
			],
			"columnDefs": [
				{ "width": "10%", "targets": 0, "orderable": true },
				{ "width": "10%", "targets": 1, "orderable": true },
				{ "width": "10%", "targets": 2, "orderable": true },
				{ "width": "10%", "targets": 3, "orderable": true },
				{ "width": "10%", "targets": 4, "orderable": true },
				{ "width": "10%", "targets": 5, "orderable": true },
				{ "width": "10%", "targets": 6, "orderable": true },
				{ "width": "10%", "targets": 7, "orderable": true },
				{ "width": "10%", "targets": 8, "orderable": true },
				{ "width": "10%", "targets": 9, "orderable": true }
			]
		});

		var detailRows = [];
		$('#tabel_riwayat_icd tbody').on( 'click', '.btn-detail-icd9', function (){
				var tr = $(this).closest('tr');
				var row = tableICD9.row( tr );
				var idx = $.inArray( tr.attr('id'), detailRows );

				if ( row.child.isShown() ) {
						tr.removeClass( 'details' );
						row.child.hide();

						// Remove from the 'open' array
						detailRows.splice( idx, 1 );
				} else {
						var idheader = tr.find("td:eq(9) button").data('idheader');
						$.ajax({
							url:"{site_url}trm_berkas/getDataICD9/" + idheader,
							dataType:"json",
							success:function(data){
								console.log(data);
								tr.addClass('details');
								row.child( viewDetailICD9(data) ).show();

								$('.js-summernote-custom-height').summernote({
										height: 50,
										minHeight: null,
										maxHeight: null
								});

								// Add to the 'open' array
								if ( idx === -1 ) {
									detailRows.push( tr.attr('id') );
								}
							}
						});

				}
		});

		// On each draw, loop over the `detailRows` array and show any child rows
		tableICD9.on('draw', function(){
				$.each( detailRows, function(i, id){
						$('#'+id+' td.details-control').trigger('click');
				});
		});
	}

	function GetTipeRadiologi(id) {
		if (id != '') {
				if (id == '1') {
					return 'X-Ray';
				} else if (id == '2') {
					return 'USG';
				} else if (id == '3') {
					return 'CT Scan';
				} else if (id == '4') {
					return 'MRI';
				} else if (id == '5') {
					return 'BMD';
				}
		} else {
				return '';
		}
	}

	function viewDetailICD9(data) {
		var rows = '';

		rows += '<div class="col-md-12"><b><span class="label label-success" style="margin-left:10px; font-size:12px">TAMBAHAN</span></b></div><br><hr>';
		rows += '<div class="col-md-12">';
		rows += '<table class="table table-bordered table-striped" id="tabel_riwayat_icd" style="width: 100%;">';
						rows += '<thead>';
							rows += '<tr>';
								rows += '<th style="width:10%">Tanggal</th>';
								rows += '<th style="width:10%">Dokter</th>';
								rows += '<th style="width:10%">Tindakan</th>';
								rows += '<th style="width:5%">ICD 9 CM</th>';
								rows += '<th style="width:15%">Deskripsi</th>';
								rows += '<th style="width:10%">Kel. Tindakan</th>';
								rows += '<th style="width:10%">Jenis</th>';
								rows += '<th style="width:10%">User</th>';
								rows += '<th style="width:10%">Verif</th>';
							rows += '</tr>';
						rows += '</thead>';
						rows += '<tbody>';
		data.forEach((item, i) => {
			rows += '				<tr>';
			rows += '					<td>' + item.tanggal_trx + '</td>';
			rows += '					<td>' + item.nama_dokter + '</td>';
			rows += '					<td>' + item.kel_tindakan + '</td>';
			rows += '					<td>' + item.kode + '</td>';
			rows += '					<td>' + item.deskripsi + '</td>';
			rows += '					<td>' + item.tindakan_layanan + '</td>';
			rows += '					<td>' + item.jenis_nama + '</td>';
			rows += '					<td>' + item.user_trx + '</td>';
			rows += '					<td>' + item.user_nama_verifikasi + '</td>';
			rows += '				</tr>';
			
		});
		rows += '</tbody></table></div>';

		return rows;
	}
</script>
