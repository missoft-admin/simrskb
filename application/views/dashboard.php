<!-- CSS DataTables -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

<div class="">
	<div class="row">
		<!-- Widget: Permintaan Laboratorium -->
		<?php if (UserAccesForm($user_acces_form, array('2780','2781'))){ ?>
			<div class="block">
				<div class="block-content">
					<div class="row items-push">
						<?php if (UserAccesForm($user_acces_form, array('2780'))){ ?>
						<div class="col-md-6">
							<div class="col-xs-12">
								<!-- Mini Stats -->
								<a class="block block-link-hover3" href="#">
									<table class="block-table text-center">
										<tbody>
											<tr>
												<td class="bg-primary" style="width: 50%;">
													<div class="push-30 push-30-t">
														<i class="si si-check fa-3x text-white"></i>
													</div>
												</td>
												<td class="bg-gray-lighter" style="width: 50%;">
													<div class="h1 font-w700"><span class="h2 text-primary">+</span> <span id="jml_approval_kwitansi" class="h2 text-primary">-</span></div>
													<div class="h4 text-primary text-uppercase push-5-t">APPROVAL BATAL KWITANSI RANAP</div>
												</td>
											</tr>
										</tbody>
									</table>
								</a>
								
							</div>
							<div class="col-md-6">
								<h4 class="text-primary">DAFTAR APPROVAL</h4>
							</div>
							<div class="col-md-6">
								
							</div>
							<div class="col-md-12 push-10-t">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_approval_kwitansi">
										<thead>
											<tr>
												<th width="10%" class="text-center">NOMOR REGISTRASI</th>
												<th width="10%" class="text-center">TANGGAL</th>
												<th width="15%" class="text-center">PASIEN</th>
												<th width="12%" class="text-center">JENIS</th>
												<th width="10%" class="text-center">NOMINAL</th>
												<th width="12%" class="text-center">ALASAN</th>
												<th width="12%" class="text-center">PENGAJUAN</th>
												<th width="15%" class="text-center">STEP</th>
												<th width="15%" class="text-center">Action</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<?}?>
						<?php if (UserAccesForm($user_acces_form, array('2781'))){ ?>
						<div class="col-md-6">
							<div class="col-xs-12">
								<!-- Mini Stats -->
								<a class="block block-link-hover3" href="#">
									<table class="block-table text-center">
										<tbody>
											<tr>
												<td class="bg-primary" style="width: 50%;">
													<div class="push-30 push-30-t">
														<i class="si si-check fa-3x text-white"></i>
													</div>
												</td>
												<td class="bg-gray-lighter" style="width: 50%;">
													<div class="h1 font-w700"><span class="h2 text-primary">+</span> <span id="jml_list_trx_ranap" class="h2 text-primary">-</span></div>
													<div class="h4 text-primary text-uppercase push-5-t">TRANSAKSI RAWAT INAP SELESAI</div>
												</td>
											</tr>
										</tbody>
									</table>
								</a>
								
							</div>
							<div class="col-md-3">
								<h4 class="text-primary">DAFTAR TRANSAKSI</h4>
							</div>
							<div class="col-md-3">
								<select id="waktu_selesai" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="0" selected>Hari Ini</option>
									<option value="1" >Kemarin</option>
									<option value="7" >1 Minggu</option>
									<option value="30" >1 Bulan</option>
									
								</select>
							</div>
							<div class="col-md-6">
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" id="tanggal_selesai_1" placeholder="From" value="{tanggal_1}"/>
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" id="tanggal_selesai_2" placeholder="To" value="{tanggal_2}"/>
								</div>
							</div>
							<div class="col-md-12 push-10-t">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_trx_selesai">
										<thead>
											<tr>
												<th width="10%" class="text-center">NOMOR REGISTRASI</th>
												<th width="10%" class="text-center">TANGGAL</th>
												<th width="15%" class="text-center">PASIEN</th>
												<th width="10%" class="text-center">NOMINAL</th>
												<th width="12%" class="text-center">STATUS</th>
												<th width="12%" class="text-center">TANGGAL SELESAI</th>
												<th width="10%" class="text-center">TANGGAL VERIFIED</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<?}?>
						<?php if (UserAccesForm($user_acces_form, array('2782'))){ ?>
						<div class="col-md-6">
							<div class="col-xs-12">
								<!-- Mini Stats -->
								<a class="block block-link-hover3" href="#">
									<table class="block-table text-center">
										<tbody>
											<tr>
												<td class="bg-primary" style="width: 50%;">
													<div class="push-30 push-30-t">
														<i class="si si-check fa-3x text-white"></i>
													</div>
												</td>
												<td class="bg-gray-lighter" style="width: 50%;">
													<div class="h1 font-w700"><span class="h2 text-primary">+</span> <span id="jml_card" class="h2 text-primary">-</span></div>
													<div class="h4 text-primary text-uppercase push-5-t">CARD PASS</div>
												</td>
											</tr>
										</tbody>
									</table>
								</a>
								
							</div>
							<div class="col-md-3">
								<h4 class="text-primary">CARD PASS</h4>
							</div>
							<div class="col-md-3">
								<select id="waktu_card" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="0" selected>Hari Ini</option>
									<option value="1" >Kemarin</option>
									<option value="7" >1 Minggu</option>
									<option value="30" >1 Bulan</option>
									
								</select>
							</div>
							<div class="col-md-6">
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" id="tanggal_card_1" placeholder="From" value="{tanggal_1}"/>
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" id="tanggal_card_2" placeholder="To" value="{tanggal_2}"/>
								</div>
							</div>
							<div class="col-md-12 push-10-t">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_trx_card">
										<thead>
											<tr>
												<th width="10%" class="text-center">NOMOR REGISTRASI</th>
												<th width="10%" class="text-center">TANGGAL</th>
												<th width="15%" class="text-center">PASIEN</th>
												<th width="10%" class="text-center">NOMINAL</th>
												<th width="12%" class="text-center">STATUS</th>
												<th width="12%" class="text-center">TANGGAL CARD PASS</th>
												<th width="10%" class="text-center">DESKRIPSI</th>
												<th width="10%" class="text-center">AKSI</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<?}?>
					</div>
				</div>
			</div>
		<?php } ?>
		<?php if (UserAccesForm($user_acces_form, array('2611','2612'))){ ?>
			<div class="block">
				<div class="block-content">
					<div class="row items-push">
						<?php if (UserAccesForm($user_acces_form, array('2611'))){ ?>
						<div class="col-md-6">
							<div class="col-xs-12">
								<!-- Mini Stats -->
								<a class="block block-link-hover3" href="#">
									<table class="block-table text-center">
										<tbody>
											<tr>
												<td class="bg-primary" style="width: 50%;">
													<div class="push-30 push-30-t">
														<i class="si si-check fa-3x text-white"></i>
													</div>
												</td>
												<td class="bg-gray-lighter" style="width: 50%;">
													<div class="h1 font-w700"><span class="h2 text-primary">+</span> <span id="jml_approval_deposit" class="h2 text-primary">-</span></div>
													<div class="h4 text-primary text-uppercase push-5-t">APPROVAL BATAL DEPOSIT</div>
												</td>
											</tr>
										</tbody>
									</table>
								</a>
								
							</div>
							<div class="col-md-6">
								<h4 class="text-primary">DAFTAR APPROVAL</h4>
							</div>
							<div class="col-md-6">
								
							</div>
							<div class="col-md-12 push-10-t">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_approval_deposit">
										<thead>
											<tr>
												<th width="15%" class="text-center">NOMOR DEPOSIT</th>
												<th width="15%" class="text-center">TANGGAL</th>
												<th width="15%" class="text-center">PASIEN</th>
												<th width="15%" class="text-center">NOMINAL</th>
												<th width="15%" class="text-center">ALASAN</th>
												<th width="15%" class="text-center">PENGAJUAN</th>
												<th width="15%" class="text-center">STEP</th>
												<th width="10%" class="text-center">Action</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<?}?>
						<?php if (UserAccesForm($user_acces_form, array('2612'))){ ?>
						<div class="col-md-6">
							<div class="col-xs-12">
								<!-- Mini Stats -->
								<a class="block block-link-hover3" href="#">
									<table class="block-table text-center">
										<tbody>
											<tr>
												<td class="bg-warning" style="width: 50%;">
													<div class="push-30 push-30-t">
														<i class="si si-screen-smartphone fa-3x text-white"></i>
													</div>
												</td>
												<td class="bg-gray-lighter" style="width: 50%;">
													<div class="h1 font-w700"><span class="h2 text-primary">+</span> <span id="jml_evavluasi_mpp" class="h2 text-primary">-</span></div>
													<div class="h4 text-primary text-uppercase push-5-t">PERSETUJUAN TINDAKAN</div>
												</td>
											</tr>
										</tbody>
									</table>
								</a>
								
							</div>
							<div class="col-md-3">
								<h4 class="text-primary">DAFTAR PASIEN</h4>
							</div>
							
							<div class="col-md-3">
								<select id="waktu_tindakan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="0" selected>Hari Ini</option>
									<option value="1" >Kemarin</option>
									<option value="7" >1 Minggu</option>
									<option value="30" >1 Bulan</option>
									
								</select>
							</div>
							<div class="col-md-6">
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" id="tanggal_tindakan_1" placeholder="From" value="{tanggal_1}"/>
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" id="tanggal_tindakan_2" placeholder="To" value="{tanggal_2}"/>
								</div>
							</div>
							<div class="col-md-12 push-10-t">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_tindakan">
										<thead>
											<tr>
												<th width="15%" class="text-center">No Register</th>
												<th width="15%" class="text-center">Pasien</th>
												<th width="15%" class="text-center">Tanggal Setuju</th>
												<th width="15%" class="text-center">Nama Tindakan </th>
												<th width="15%" class="text-center">Aproved By </th>
												<th width="15%" class="text-center">Status</th>
												<th width="10%" class="text-center">Action</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<?}?>
					</div>
				</div>
			</div>
		<?php } ?>
		<?php if (UserAccesForm($user_acces_form, array('2610'))){ ?>
			<div class="block">
				<div class="block-content">
					<div class="row items-push">
						<?php if (UserAccesForm($user_acces_form, array('2610'))){ ?>
						<div class="col-md-12">
							<div class="col-xs-6">
								<!-- Mini Stats -->
								<a class="block block-link-hover3" href="#">
									<table class="block-table text-center">
										<tbody>
											<tr>
												<td class="bg-primary" style="width: 50%;">
													<div class="push-30 push-30-t">
														<i class="fa fa-money fa-3x text-white"></i>
													</div>
												</td>
												<td class="bg-gray-lighter" style="width: 50%;">
													<div class="h1 font-w700"><span class="h2 text-primary">+</span> <span id="jml_deposit" class="h2 text-primary">-</span> <span class="h2 text-primary"> DEPOSIT</span></div>
													<div class="h4 text-primary text-uppercase push-5-t"> <span id="total_deposit" class="h2 text-primary">-</span></div>
												</td>
											</tr>
										</tbody>
									</table>
								</a>
								
							</div>
							</div>
						<div class="col-md-12">
							<div class="col-md-4">
								<h4 class="text-primary">DAFTAR DEPOSIT</h4>
							</div>
							<div class="col-md-2">
								<select id="satus_deposit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="0" selected>Semua</option>
									<option value="1" >Menunggu Transaksi</option>
									<option value="2" >Telah Diproses</option>
									<option value="3" >Dibatalkan</option>
								</select>
							</div>
							<div class="col-md-2">
								<select id="waktu_deposit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="0" selected>Hari Ini</option>
									<option value="1" >Kemarin</option>
									<option value="7" >1 Minggu</option>
									<option value="30" >1 Bulan</option>
									
								</select>
							</div>
							
							<div class="col-md-3">
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" id="tanggal_deposit_1" placeholder="From" value="{tanggal_1}"/>
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" id="tanggal_deposit_2" placeholder="To" value="{tanggal_2}"/>
								</div>
							</div>
							<div class="col-md-1">
								<button class="btn btn-success text-uppercase" type="button" onclick="load_index_deposit()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
							</div>
							<div class="col-md-12 push-10-t">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_deposit">
										<thead>
											<tr>
												<th width="10%" class="text-center">Nomor Deposit</th>
												<th width="10%" class="text-center">Tanggal</th>
												<th width="15%" class="text-center">Pasien</th>
												<th width="10%" class="text-center">Nominal </th>
												<th width="15%" class="text-center">Metode</th>
												<th width="15%" class="text-center">Status</th>
												<th width="15%" class="text-center">Tanggal Input</th>
												<th width="10%" class="text-center">Action</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<?}?>
					</div>
				</div>
			</div>
		<?php } ?>
		<?php if (UserAccesForm($user_acces_form, array('2213','2214'))){ ?>
			<div class="block">
				<div class="block-content">
					<div class="row items-push">
						<?php if (UserAccesForm($user_acces_form, array('2213'))){ ?>
						<div class="col-md-6">
							<div class="col-xs-12">
								<!-- Mini Stats -->
								<a class="block block-link-hover3" href="#">
									<table class="block-table text-center">
										<tbody>
											<tr>
												<td class="bg-primary" style="width: 50%;">
													<div class="push-30 push-30-t">
														<i class="fa fa-universal-access fa-3x text-white"></i>
													</div>
												</td>
												<td class="bg-gray-lighter" style="width: 50%;">
													<div class="h1 font-w700"><span class="h2 text-primary">+</span> <span id="jml_mpp" class="h2 text-primary">-</span></div>
													<div class="h4 text-primary text-uppercase push-5-t">SKRINING MPP</div>
												</td>
											</tr>
										</tbody>
									</table>
								</a>
								
							</div>
							<div class="col-md-6">
								<h4 class="text-primary">DAFTAR PASIEN</h4>
							</div>
							<div class="col-md-6">
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" id="tanggal_mpp_1" placeholder="From" value="{tanggal_1}"/>
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" id="tanggal_mpp_2" placeholder="To" value="{tanggal_2}"/>
								</div>
							</div>
							<div class="col-md-12 push-10-t">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_mpp">
										<thead>
											<tr>
												<th width="15%" class="text-center">No Pendaftaran / Tanggal Input</th>
												<th width="15%" class="text-center">Asal Pasien</th>
												<th width="15%" class="text-center">Pasien</th>
												<th width="15%" class="text-center">Kelas </th>
												<th width="15%" class="text-center">Hasil</th>
												<th width="15%" class="text-center">Tindak Lanjut</th>
												<th width="10%" class="text-center">Action</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<?}?>
						<?php if (UserAccesForm($user_acces_form, array('2214'))){ ?>
						<div class="col-md-6">
							<div class="col-xs-12">
								<!-- Mini Stats -->
								<a class="block block-link-hover3" href="#">
									<table class="block-table text-center">
										<tbody>
											<tr>
												<td class="bg-warning" style="width: 50%;">
													<div class="push-30 push-30-t">
														<i class="si si-screen-smartphone fa-3x text-white"></i>
													</div>
												</td>
												<td class="bg-gray-lighter" style="width: 50%;">
													<div class="h1 font-w700"><span class="h2 text-primary">+</span> <span id="jml_evavluasi_mpp" class="h2 text-primary">-</span></div>
													<div class="h4 text-primary text-uppercase push-5-t">EVALUASI AWAL MPP</div>
												</td>
											</tr>
										</tbody>
									</table>
								</a>
								
							</div>
							<div class="col-md-6">
								<h4 class="text-primary">DAFTAR PASIEN</h4>
							</div>
							<div class="col-md-6">
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" id="tanggal_evaluasi_mpp_1" placeholder="From" value="{tanggal_1}"/>
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" id="tanggal_evaluasi_mpp_2" placeholder="To" value="{tanggal_2}"/>
								</div>
							</div>
							<div class="col-md-12 push-10-t">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="index_evaluasi_mpp">
										<thead>
											<tr>
												<th width="15%" class="text-center">No Pendaftaran / Tanggal Input</th>
												<th width="15%" class="text-center">Asal Pasien</th>
												<th width="15%" class="text-center">Pasien</th>
												<th width="15%" class="text-center">Kelas </th>
												<th width="10%" class="text-center">Action</th>
											   
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<?}?>
					</div>
				</div>
			</div>
		<?php } ?>
		<div class="col-md-12 col-lg-6">
			<?php if (UserAccesForm($user_acces_form, array('2130'))){ ?>
			<div class="block" style="padding: 10px;">
				<table class="table" id="table-widget-permintaan-laboratorium">
					<thead class="text-center">
						<tr>
							<td colspan="2" class="bg-primary">
								<div class="push-30 push-30-t">
									<i class="fa fa-flask fa-3x text-white-op"></i>
								</div>
							</td>
							<td colspan="4" style="vertical-align: middle;">
								<div class="h1 font-w700">
									<a href="{base_url}term_laboratorium_umum_permintaan" id="total_permintaan_laboratorium">+ <?php echo $widget_permintaan_laboratorium['total']; ?> </a>
								</div>
								<div class="h4 text-muted text-uppercase">
									<a href="{base_url}term_laboratorium_umum_permintaan">Laboratorium</a>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="4" class="text-left" style="text-transform: uppercase; font-weight: bold; vertical-align: middle;">Daftar Permintaan</td>
							<td colspan="4" class="text-right">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="form-control" type="text" id="permintaan_laboratorium_tanggal_dari" placeholder="Dari Tanggal" value="<?php echo $widget_permintaan_laboratorium['tanggal_dari']; ?>">
											<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
											<input class="form-control" type="text" id="permintaan_laboratorium_tanggal_sampai" placeholder="Sampai Tanggal" value="<?php echo $widget_permintaan_laboratorium['tanggal_sampai']; ?>">
									</div>
							</td>
						</tr>
						<tr>
							<td class="bg-primary">No. Pendaftaran</td>
							<td class="bg-primary">Tanggal Permintaan</td>
							<td class="bg-primary">Nomor Order</td>
							<td class="bg-primary">Asal Pasien</td>
							<td class="bg-primary">Pasien</td>
							<td class="bg-primary">Tujuan Pelayanan</td>
							<td class="bg-primary">Tujuan Laboratorium</td>
							<td class="bg-primary" width="30%">Aksi</td>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($widget_permintaan_laboratorium['items'] as $item) { ?>
						<tr>
							<td><?php echo $item->nomor_pendaftaran; ?></td>
							<td><?php echo DMYTimeFormat($item->tanggal_permintaan); ?></td>
							<td><?php echo $item->nomor_order; ?></td>
							<td><?php echo GetAsalPasien($item->asal_pasien); ?></td>
							<td><?php echo $item->nomor_medrec; ?> - <?php echo $item->nama_pasien; ?></td>
							<td><?php echo $item->poliklinik; ?> - <?php echo $item->dokter_perujuk; ?></td>
							<td><?php echo $item->tujuan_laboratorium; ?></td>
							<td>
								<div class="btn-group">
										<a href="#" data-toggle="tooltip" title="Proses Transaksi" class="btn btn-success btn-xs"onclick="processOrder(<?php echo $item->pendaftaran_id; ?>, <?php echo $item->id; ?>)"><i class="fa fa-check-circle"></i></a>
										<a href="#" data-toggle="tooltip" title="Ubah Transaksi" class="btn btn-primary btn-xs" onclick="editOrder(<?php echo $item->pendaftaran_id; ?>, <?php echo $item->id; ?>, <?php echo $item->jumlah_edit; ?>)"><i class="fa fa-pencil"></i></a>
								</div>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<?php } ?>
		</div>

		<!-- Widget: Permintaan Patologi Anatomi -->
		<div class="col-md-12 col-lg-6">
			<?php if (UserAccesForm($user_acces_form, array('2131'))){ ?>
			<div class="block" style="padding: 10px;">
				<table class="table" id="table-widget-permintaan-patologi-anatomi">
					<thead class="text-center">
						<tr>
							<td colspan="2" class="bg-warning">
								<div class="push-30 push-30-t">
									<i class="fa fa-stethoscope fa-3x text-white-op"></i>
								</div>
							</td>
							<td colspan="4" style="vertical-align: middle;">
								<div class="h1 font-w700">
									<a href="{base_url}term_laboratorium_pa_permintaan" id="total_permintaan_laboratorium_pa">+ <?php echo $widget_permintaan_laboratorium_pa['total']; ?> </a>
								</div>
								<div class="h4 text-muted text-uppercase">
									<a href="{base_url}term_laboratorium_pa_permintaan">Patologi Anatomi</a>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="4" class="text-left" style="text-transform: uppercase; font-weight: bold; vertical-align: middle;">Daftar Permintaan</td>
							<td colspan="4" class="text-right">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="form-control" type="text" id="permintaan_laboratorium_pa_tanggal_dari" placeholder="Dari Tanggal" value="<?php echo $widget_permintaan_laboratorium_pa['tanggal_dari']; ?>">
											<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
											<input class="form-control" type="text" id="permintaan_laboratorium_pa_tanggal_sampai" placeholder="Sampai Tanggal" value="<?php echo $widget_permintaan_laboratorium_pa['tanggal_sampai']; ?>">
									</div>
							</td>
						</tr>
						<tr>
							<td class="bg-warning text-white">No. Pendaftaran</td>
							<td class="bg-warning text-white">Tanggal Permintaan</td>
							<td class="bg-warning text-white">Nomor Order</td>
							<td class="bg-warning text-white">Asal Pasien</td>
							<td class="bg-warning text-white">Pasien</td>
							<td class="bg-warning text-white">Tujuan Pelayanan</td>
							<td class="bg-warning text-white">Tujuan Laboratorium</td>
							<td class="bg-warning text-white" width="30%">Aksi</td>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($widget_permintaan_laboratorium_pa['items'] as $item) { ?>
						<tr>
							<td><?php echo $item->nomor_pendaftaran; ?></td>
							<td><?php echo DMYTimeFormat($item->tanggal_permintaan); ?></td>
							<td><?php echo $item->nomor_order; ?></td>
							<td><?php echo GetAsalPasien($item->asal_pasien); ?></td>
							<td><?php echo $item->nomor_medrec; ?> - <?php echo $item->nama_pasien; ?></td>
							<td><?php echo $item->poliklinik; ?> - <?php echo $item->dokter_perujuk; ?></td>
							<td><?php echo $item->tujuan_laboratorium; ?></td>
							<td>
								<div class="btn-group">
										<a href="#" data-toggle="tooltip" title="Proses Transaksi" class="btn btn-success btn-xs"onclick="processOrder(<?php echo $item->pendaftaran_id; ?>, <?php echo $item->id; ?>)"><i class="fa fa-check-circle"></i></a>
										<a href="#" data-toggle="tooltip" title="Ubah Transaksi" class="btn btn-primary btn-xs" onclick="editOrder(<?php echo $item->pendaftaran_id; ?>, <?php echo $item->id; ?>, <?php echo $item->jumlah_edit; ?>)"><i class="fa fa-pencil"></i></a>
								</div>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<?php } ?>
		</div>
	</div>

	<div class="row">
		<!-- Widget: Permintaan Bank Darah -->
		<div class="col-md-12 col-lg-6">
			<?php if (UserAccesForm($user_acces_form, array('2132'))){ ?>
			<div class="block" style="padding: 10px;">
				<table class="table" id="table-widget-permintaan-laboratorium-bankdarah">
					<thead class="text-center">
						<tr>
							<td colspan="2" class="bg-danger">
								<div class="push-30 push-30-t">
									<i class="fa fa-medkit fa-3x text-white-op"></i>
								</div>
							</td>
							<td colspan="4" style="vertical-align: middle;">
								<div class="h1 font-w700">
									<a href="{base_url}term_laboratorium_bd_permintaan" id="total_permintaan_laboratorium_bd">+ <?php echo $widget_permintaan_laboratorium_bd['total']; ?> </a>
								</div>
								<div class="h4 text-muted text-uppercase">
									<a href="{base_url}term_laboratorium_bd_permintaan">Bank Darah</a>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="4" class="text-left" style="text-transform: uppercase; font-weight: bold; vertical-align: middle;">Daftar Permintaan</td>
							<td colspan="4" class="text-right">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="form-control" type="text" id="permintaan_laboratorium_bd_tanggal_dari" placeholder="Dari Tanggal" value="<?php echo $widget_permintaan_laboratorium_bd['tanggal_dari']; ?>">
											<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
											<input class="form-control" type="text" id="permintaan_laboratorium_bd_tanggal_sampai" placeholder="Sampai Tanggal" value="<?php echo $widget_permintaan_laboratorium_bd['tanggal_sampai']; ?>">
									</div>
							</td>
						</tr>
						<tr>
							<td class="bg-danger text-white">No. Pendaftaran</td>
							<td class="bg-danger text-white">Tanggal Permintaan</td>
							<td class="bg-danger text-white">Nomor Order</td>
							<td class="bg-danger text-white">Asal Pasien</td>
							<td class="bg-danger text-white">Pasien</td>
							<td class="bg-danger text-white">Tujuan Pelayanan</td>
							<td class="bg-danger text-white">Tujuan Laboratorium</td>
							<td class="bg-danger text-white" width="30%">Aksi</td>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($widget_permintaan_laboratorium_bd['items'] as $item) { ?>
						<tr>
							<td><?php echo $item->nomor_pendaftaran; ?></td>
							<td><?php echo DMYTimeFormat($item->tanggal_permintaan); ?></td>
							<td><?php echo $item->nomor_order; ?></td>
							<td><?php echo GetAsalPasien($item->asal_pasien); ?></td>
							<td><?php echo $item->nomor_medrec; ?> - <?php echo $item->nama_pasien; ?></td>
							<td><?php echo $item->poliklinik; ?> - <?php echo $item->dokter_perujuk; ?></td>
							<td><?php echo $item->tujuan_laboratorium; ?></td>
							<td>
								<div class="btn-group">
										<a href="#" data-toggle="tooltip" title="Proses Transaksi" class="btn btn-success btn-xs"onclick="processOrder(<?php echo $item->pendaftaran_id; ?>, <?php echo $item->id; ?>)"><i class="fa fa-check-circle"></i></a>
										<a href="#" data-toggle="tooltip" title="Ubah Transaksi" class="btn btn-primary btn-xs" onclick="editOrder(<?php echo $item->pendaftaran_id; ?>, <?php echo $item->id; ?>, <?php echo $item->jumlah_edit; ?>)"><i class="fa fa-pencil"></i></a>
								</div>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<?php } ?>
		</div>

		<!-- Widget: Permintaan X-Ray -->
		<div class="col-md-12 col-lg-6">
			<?php if (UserAccesForm($user_acces_form, array('2170'))){ ?>
			<div class="block" style="padding: 10px;">
				<table class="table" id="table-widget-permintaan-radiologi-xray">
					<thead class="text-center">
						<tr>
							<td colspan="2" class="bg-primary">
								<div class="push-30 push-30-t">
									<i class="fa fa-medkit fa-3x text-white-op"></i>
								</div>
							</td>
							<td colspan="4" style="vertical-align: middle;">
								<div class="h1 font-w700">
									<a href="{base_url}term_radiologi_xray_permintaan" id="total_permintaan_radiologi_xray">+ <?php echo $widget_permintaan_radiologi_xray['total']; ?> </a>
								</div>
								<div class="h4 text-muted text-uppercase">
									<a href="{base_url}term_radiologi_xray_permintaan">X-Ray</a>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="4" class="text-left" style="text-transform: uppercase; font-weight: bold; vertical-align: middle;">Daftar Permintaan</td>
							<td colspan="4" class="text-right">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="form-control" type="text" id="permintaan_radiologi_xray_tanggal_dari" placeholder="Dari Tanggal" value="<?php echo $widget_permintaan_radiologi_xray['tanggal_dari']; ?>">
											<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
											<input class="form-control" type="text" id="permintaan_radiologi_xray_tanggal_sampai" placeholder="Sampai Tanggal" value="<?php echo $widget_permintaan_radiologi_xray['tanggal_sampai']; ?>">
									</div>
							</td>
						</tr>
						<tr>
							<td class="bg-primary text-white">No. Pendaftaran</td>
							<td class="bg-primary text-white">Tanggal Permintaan</td>
							<td class="bg-primary text-white">Nomor Order</td>
							<td class="bg-primary text-white">Asal Pasien</td>
							<td class="bg-primary text-white">Pasien</td>
							<td class="bg-primary text-white">Tujuan Pelayanan</td>
							<td class="bg-primary text-white">Tujuan Radiologi</td>
							<td class="bg-primary text-white" width="30%">Aksi</td>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($widget_permintaan_radiologi_xray['items'] as $item) { ?>
						<tr>
							<td><?php echo $item->nomor_pendaftaran; ?></td>
							<td><?php echo DMYTimeFormat($item->tanggal_permintaan); ?></td>
							<td><?php echo $item->nomor_order; ?></td>
							<td><?php echo GetAsalPasien($item->asal_pasien); ?></td>
							<td><?php echo $item->nomor_medrec; ?> - <?php echo $item->nama_pasien; ?></td>
							<td><?php echo $item->poliklinik; ?> - <?php echo $item->dokter_perujuk; ?></td>
							<td><?php echo $item->tujuan_radiologi; ?></td>
							<td>
								<div class="btn-group">
										<a href="#" data-toggle="tooltip" title="Proses Transaksi" class="btn btn-success btn-xs"onclick="processOrder(<?php echo $item->pendaftaran_id; ?>, <?php echo $item->id; ?>)"><i class="fa fa-check-circle"></i></a>
										<a href="#" data-toggle="tooltip" title="Ubah Transaksi" class="btn btn-primary btn-xs" onclick="editOrder(<?php echo $item->pendaftaran_id; ?>, <?php echo $item->id; ?>, <?php echo $item->jumlah_edit; ?>)"><i class="fa fa-pencil"></i></a>
								</div>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<?php } ?>
		</div>
	</div>

	<div class="row">
		<!-- Widget: Permintaan USG -->
		<div class="col-md-12 col-lg-6">
			<?php if (UserAccesForm($user_acces_form, array('2171'))){ ?>
			<div class="block" style="padding: 10px;">
				<table class="table" id="table-widget-permintaan-radiologi-usg">
					<thead class="text-center">
						<tr>
							<td colspan="2" class="bg-warning">
								<div class="push-30 push-30-t">
									<i class="fa fa-medkit fa-3x text-white-op"></i>
								</div>
							</td>
							<td colspan="4" style="vertical-align: middle;">
								<div class="h1 font-w700">
									<a href="{base_url}term_radiologi_usg_permintaan" id="total_permintaan_radiologi_usg">+ <?php echo $widget_permintaan_radiologi_usg['total']; ?> </a>
								</div>
								<div class="h4 text-muted text-uppercase">
									<a href="{base_url}term_radiologi_usg_permintaan">USG</a>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="4" class="text-left" style="text-transform: uppercase; font-weight: bold; vertical-align: middle;">Daftar Permintaan</td>
							<td colspan="4" class="text-right">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="form-control" type="text" id="permintaan_radiologi_usg_tanggal_dari" placeholder="Dari Tanggal" value="<?php echo $widget_permintaan_radiologi_usg['tanggal_dari']; ?>">
											<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
											<input class="form-control" type="text" id="permintaan_radiologi_usg_tanggal_sampai" placeholder="Sampai Tanggal" value="<?php echo $widget_permintaan_radiologi_usg['tanggal_sampai']; ?>">
									</div>
							</td>
						</tr>
						<tr>
							<td class="bg-warning text-white">No. Pendaftaran</td>
							<td class="bg-warning text-white">Tanggal Permintaan</td>
							<td class="bg-warning text-white">Nomor Order</td>
							<td class="bg-warning text-white">Asal Pasien</td>
							<td class="bg-warning text-white">Pasien</td>
							<td class="bg-warning text-white">Tujuan Pelayanan</td>
							<td class="bg-warning text-white">Tujuan Radiologi</td>
							<td class="bg-warning text-white" width="30%">Aksi</td>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($widget_permintaan_radiologi_usg['items'] as $item) { ?>
						<tr>
							<td><?php echo $item->nomor_pendaftaran; ?></td>
							<td><?php echo DMYTimeFormat($item->tanggal_permintaan); ?></td>
							<td><?php echo $item->nomor_order; ?></td>
							<td><?php echo GetAsalPasien($item->asal_pasien); ?></td>
							<td><?php echo $item->nomor_medrec; ?> - <?php echo $item->nama_pasien; ?></td>
							<td><?php echo $item->poliklinik; ?> - <?php echo $item->dokter_perujuk; ?></td>
							<td><?php echo $item->tujuan_radiologi; ?></td>
							<td>
								<div class="btn-group">
										<a href="#" data-toggle="tooltip" title="Proses Transaksi" class="btn btn-success btn-xs"onclick="processOrder(<?php echo $item->pendaftaran_id; ?>, <?php echo $item->id; ?>)"><i class="fa fa-check-circle"></i></a>
										<a href="#" data-toggle="tooltip" title="Ubah Transaksi" class="btn btn-primary btn-xs" onclick="editOrder(<?php echo $item->pendaftaran_id; ?>, <?php echo $item->id; ?>, <?php echo $item->jumlah_edit; ?>)"><i class="fa fa-pencil"></i></a>
								</div>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<?php } ?>
		</div>

		<!-- Widget: Permintaan CT-Scan -->
		<div class="col-md-12 col-lg-6">
			<?php if (UserAccesForm($user_acces_form, array('2172'))){ ?>
			<div class="block" style="padding: 10px;">
				<table class="table" id="table-widget-permintaan-radiologi-ctscan">
					<thead class="text-center">
						<tr>
							<td colspan="2" class="bg-danger">
								<div class="push-30 push-30-t">
									<i class="fa fa-medkit fa-3x text-white-op"></i>
								</div>
							</td>
							<td colspan="4" style="vertical-align: middle;">
								<div class="h1 font-w700">
									<a href="{base_url}term_radiologi_ctscan_permintaan" id="total_permintaan_radiologi_ctscan">+ <?php echo $widget_permintaan_radiologi_ctscan['total']; ?> </a>
								</div>
								<div class="h4 text-muted text-uppercase">
									<a href="{base_url}term_radiologi_ctscan_permintaan">CT-Scan</a>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="4" class="text-left" style="text-transform: uppercase; font-weight: bold; vertical-align: middle;">Daftar Permintaan</td>
							<td colspan="4" class="text-right">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="form-control" type="text" id="permintaan_radiologi_ctscan_tanggal_dari" placeholder="Dari Tanggal" value="<?php echo $widget_permintaan_radiologi_ctscan['tanggal_dari']; ?>">
											<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
											<input class="form-control" type="text" id="permintaan_radiologi_ctscan_tanggal_sampai" placeholder="Sampai Tanggal" value="<?php echo $widget_permintaan_radiologi_ctscan['tanggal_sampai']; ?>">
									</div>
							</td>
						</tr>
						<tr>
							<td class="bg-danger text-white">No. Pendaftaran</td>
							<td class="bg-danger text-white">Tanggal Permintaan</td>
							<td class="bg-danger text-white">Nomor Order</td>
							<td class="bg-danger text-white">Asal Pasien</td>
							<td class="bg-danger text-white">Pasien</td>
							<td class="bg-danger text-white">Tujuan Pelayanan</td>
							<td class="bg-danger text-white">Tujuan Radiologi</td>
							<td class="bg-danger text-white" width="30%">Aksi</td>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($widget_permintaan_radiologi_ctscan['items'] as $item) { ?>
						<tr>
							<td><?php echo $item->nomor_pendaftaran; ?></td>
							<td><?php echo DMYTimeFormat($item->tanggal_permintaan); ?></td>
							<td><?php echo $item->nomor_order; ?></td>
							<td><?php echo GetAsalPasien($item->asal_pasien); ?></td>
							<td><?php echo $item->nomor_medrec; ?> - <?php echo $item->nama_pasien; ?></td>
							<td><?php echo $item->poliklinik; ?> - <?php echo $item->dokter_perujuk; ?></td>
							<td><?php echo $item->tujuan_radiologi; ?></td>
							<td>
								<div class="btn-group">
										<a href="#" data-toggle="tooltip" title="Proses Transaksi" class="btn btn-success btn-xs"onclick="processOrder(<?php echo $item->pendaftaran_id; ?>, <?php echo $item->id; ?>)"><i class="fa fa-check-circle"></i></a>
										<a href="#" data-toggle="tooltip" title="Ubah Transaksi" class="btn btn-primary btn-xs" onclick="editOrder(<?php echo $item->pendaftaran_id; ?>, <?php echo $item->id; ?>, <?php echo $item->jumlah_edit; ?>)"><i class="fa fa-pencil"></i></a>
								</div>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<?php } ?>
		</div>
	</div>

	<div class="row">
		<!-- Widget: Permintaan MRI -->
		<div class="col-md-12 col-lg-6">
			<?php if (UserAccesForm($user_acces_form, array('2173'))){ ?>
			<div class="block" style="padding: 10px;">
				<table class="table" id="table-widget-permintaan-radiologi-mri">
					<thead class="text-center">
						<tr>
							<td colspan="2" class="bg-success">
								<div class="push-30 push-30-t">
									<i class="fa fa-medkit fa-3x text-white-op"></i>
								</div>
							</td>
							<td colspan="4" style="vertical-align: middle;">
								<div class="h1 font-w700">
									<a href="{base_url}term_radiologi_mri_permintaan" id="total_permintaan_radiologi_mri">+ <?php echo $widget_permintaan_radiologi_mri['total']; ?> </a>
								</div>
								<div class="h4 text-muted text-uppercase">
									<a href="{base_url}term_radiologi_mri_permintaan">MRI</a>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="4" class="text-left" style="text-transform: uppercase; font-weight: bold; vertical-align: middle;">Daftar Permintaan</td>
							<td colspan="4" class="text-right">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="form-control" type="text" id="permintaan_radiologi_mri_tanggal_dari" placeholder="Dari Tanggal" value="<?php echo $widget_permintaan_radiologi_mri['tanggal_dari']; ?>">
											<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
											<input class="form-control" type="text" id="permintaan_radiologi_mri_tanggal_sampai" placeholder="Sampai Tanggal" value="<?php echo $widget_permintaan_radiologi_mri['tanggal_sampai']; ?>">
									</div>
							</td>
						</tr>
						<tr>
							<td class="bg-success text-white">No. Pendaftaran</td>
							<td class="bg-success text-white">Tanggal Permintaan</td>
							<td class="bg-success text-white">Nomor Order</td>
							<td class="bg-success text-white">Asal Pasien</td>
							<td class="bg-success text-white">Pasien</td>
							<td class="bg-success text-white">Tujuan Pelayanan</td>
							<td class="bg-success text-white">Tujuan Radiologi</td>
							<td class="bg-success text-white" width="30%">Aksi</td>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($widget_permintaan_radiologi_mri['items'] as $item) { ?>
						<tr>
							<td><?php echo $item->nomor_pendaftaran; ?></td>
							<td><?php echo DMYTimeFormat($item->tanggal_permintaan); ?></td>
							<td><?php echo $item->nomor_order; ?></td>
							<td><?php echo GetAsalPasien($item->asal_pasien); ?></td>
							<td><?php echo $item->nomor_medrec; ?> - <?php echo $item->nama_pasien; ?></td>
							<td><?php echo $item->poliklinik; ?> - <?php echo $item->dokter_perujuk; ?></td>
							<td><?php echo $item->tujuan_radiologi; ?></td>
							<td>
								<div class="btn-group">
										<a href="#" data-toggle="tooltip" title="Proses Transaksi" class="btn btn-success btn-xs"onclick="processOrder(<?php echo $item->pendaftaran_id; ?>, <?php echo $item->id; ?>)"><i class="fa fa-check-circle"></i></a>
										<a href="#" data-toggle="tooltip" title="Ubah Transaksi" class="btn btn-primary btn-xs" onclick="editOrder(<?php echo $item->pendaftaran_id; ?>, <?php echo $item->id; ?>, <?php echo $item->jumlah_edit; ?>)"><i class="fa fa-pencil"></i></a>
								</div>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<?php } ?>
		</div>

		<!-- Widget: Permintaan Lainnya -->
		<div class="col-md-12 col-lg-6">
			<?php if (UserAccesForm($user_acces_form, array('2174'))){ ?>
			<div class="block" style="padding: 10px;">
				<table class="table" id="table-widget-permintaan-radiologi-lainnya">
					<thead class="text-center">
						<tr>
							<td colspan="2" class="bg-warning">
								<div class="push-30 push-30-t">
									<i class="fa fa-medkit fa-3x text-white-op"></i>
								</div>
							</td>
							<td colspan="4" style="vertical-align: middle;">
								<div class="h1 font-w700">
									<a href="{base_url}term_radiologi_lainnya_permintaan" id="total_permintaan_radiologi_lainnya">+ <?php echo $widget_permintaan_radiologi_lainnya['total']; ?> </a>
								</div>
								<div class="h4 text-muted text-uppercase">
									<a href="{base_url}term_radiologi_lainnya_permintaan">Lain-Lain</a>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="4" class="text-left" style="text-transform: uppercase; font-weight: bold; vertical-align: middle;">Daftar Permintaan</td>
							<td colspan="4" class="text-right">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="form-control" type="text" id="permintaan_radiologi_lainnya_tanggal_dari" placeholder="Dari Tanggal" value="<?php echo $widget_permintaan_radiologi_lainnya['tanggal_dari']; ?>">
											<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
											<input class="form-control" type="text" id="permintaan_radiologi_lainnya_tanggal_sampai" placeholder="Sampai Tanggal" value="<?php echo $widget_permintaan_radiologi_lainnya['tanggal_sampai']; ?>">
									</div>
							</td>
						</tr>
						<tr>
							<td class="bg-warning text-white">No. Pendaftaran</td>
							<td class="bg-warning text-white">Tanggal Permintaan</td>
							<td class="bg-warning text-white">Nomor Order</td>
							<td class="bg-warning text-white">Asal Pasien</td>
							<td class="bg-warning text-white">Pasien</td>
							<td class="bg-warning text-white">Tujuan Pelayanan</td>
							<td class="bg-warning text-white">Tujuan Radiologi</td>
							<td class="bg-warning text-white" width="30%">Aksi</td>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($widget_permintaan_radiologi_lainnya['items'] as $item) { ?>
						<tr>
							<td><?php echo $item->nomor_pendaftaran; ?></td>
							<td><?php echo DMYTimeFormat($item->tanggal_permintaan); ?></td>
							<td><?php echo $item->nomor_order; ?></td>
							<td><?php echo GetAsalPasien($item->asal_pasien); ?></td>
							<td><?php echo $item->nomor_medrec; ?> - <?php echo $item->nama_pasien; ?></td>
							<td><?php echo $item->poliklinik; ?> - <?php echo $item->dokter_perujuk; ?></td>
							<td><?php echo $item->tujuan_radiologi; ?></td>
							<td>
								<div class="btn-group">
										<a href="#" data-toggle="tooltip" title="Proses Transaksi" class="btn btn-success btn-xs"onclick="processOrder(<?php echo $item->pendaftaran_id; ?>, <?php echo $item->id; ?>)"><i class="fa fa-check-circle"></i></a>
										<a href="#" data-toggle="tooltip" title="Ubah Transaksi" class="btn btn-primary btn-xs" onclick="editOrder(<?php echo $item->pendaftaran_id; ?>, <?php echo $item->id; ?>, <?php echo $item->jumlah_edit; ?>)"><i class="fa fa-pencil"></i></a>
								</div>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<?php } ?>
		</div>
	</div>

	<div class="row">
		<!-- Widget: Pembelian Barang -->
		<div class="col-md-12">
			<div class="block" style="padding: 10px;">
			<?php if (UserAccesForm($user_acces_form,array('1561'))){ ?>
				<div class="row">
					<div class="col-md-6">
						<h4 class="text-primary">PEMBELIAN BARANG</h4>
					</div>
					<div class="col-md-6">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
							<input class="form-control" type="text" id="tanggal_1" name="tanggal_1" placeholder="From" value="{tanggal_1}"/>
							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
							<input class="form-control" type="text" id="tanggal_2" name="tanggal_2" placeholder="To" value="{tanggal_2}"/>	
						</div>
					</div>
					<div class="col-md-12 push-10-t">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_beli">
								<thead>
									<tr>
										<th width="10%" class="text-right">NO</th>
										<th width="60%" class="text-left">BARANG</th>
										<th width="30%" class="text-right">TOTAL</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			<? } ?>
			</div>
		</div>
	</div>
			
	<div class="block">	
		<div class="block-content">
		<div class="row">
		<?if ($jml_pencairan){?>
			<div class="col-md-3">
				<div class="block">
					<table class="block-table text-center">
						<tbody>
							<tr>
								<td class="bg-danger" style="width: 50%;">
									<div class="push-30 push-30-t">
										<i class="fa fa-credit-card fa-3x text-white-op"></i>
									</div>
								</td>
								<td class="bg-gray-lighter " style="width: 50%;">
									<div class="h1 font-w700"><span class="h2 text-muted"></span><a href="{base_url}tpencairan/index/4">+ <?=$jml_pencairan?> </a></div>
									<div class="h5 text-muted text-uppercase push-5-t"><a href="{base_url}tpencairan/index/4">Pencairan</a></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		<?}?>
		<?if ($jml_approval_kasbon){?>
			<div class="col-md-3">
				<div class="block">
					<table class="block-table text-center">
						<tbody>
							<tr>
								<td class="bg-success" style="width: 50%;">
									<div class="push-30 push-30-t">
										<i class="fa fa-money fa-3x text-white-op"></i>
									</div>
								</td>
								<td class="bg-gray-lighter " style="width: 50%;">
									<div class="h1 font-w700"><span class="h2 text-muted"></span><a href="{base_url}tkasbon_kas/index/2">+ <?=$jml_approval_kasbon?> </a></div>
									<div class="h5 text-muted text-uppercase push-5-t"><a href="{base_url}tkasbon_kas/index/2">Approval Kasbon</a></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1404'))){ ?>
		<?if ($jml_bendahara_app){?>
			<div class="col-md-3">
				<div class="block">
					<table class="block-table text-center">
						<tbody>
							<tr>
								<td class="bg-warning" style="width: 50%;">
									<div class="push-30 push-30-t">
										<i class="fa fa-money fa-3x text-white-op"></i>
									</div>
								</td>
								<td class="bg-gray-lighter " style="width: 50%;">
									<div class="h1 font-w700"><span class="h2 text-muted"></span><a href="{base_url}mrka_bendahara">+ <?=$jml_bendahara_app?> </a></div>
									<div class="h5 text-muted text-uppercase push-5-t"><a href="{base_url}mrka_bendahara">Approval Bendahara</a></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		<?}?>
		
		<?}?>
		<?if ($jml_approval_refund){?>
			<div class="col-md-3">
				<div class="block">
					<table class="block-table text-center">
						<tbody>
							<tr>
								<td class="bg-danger" style="width: 50%;">
									<div class="push-30 push-30-t">
										<i class="fa fa-money fa-3x text-white-op"></i>
									</div>
								</td>
								<td class="bg-gray-lighter " style="width: 50%;">
									<div class="h1 font-w700"><span class="h2 text-muted"></span><a href="{base_url}trefund_kas">+ <?=$jml_approval_refund?> </a></div>
									<div class="h5 text-muted text-uppercase push-5-t"><a href="{base_url}trefund_kas">Approval Refund</a></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		<?}?>
		<?if ($jml_app_pengajuan){?>
			<div class="col-md-3">
				<div class="block">
					<table class="block-table text-center">
						<tbody>
							<tr>
								<td class="bg-warning" style="width: 50%;">
									<div class="push-30 push-30-t">
										<i class="si si-book-open fa-3x text-white-op"></i>
									</div>
								</td>
								<td class="bg-gray-lighter " style="width: 50%;">
									<div class="h1 font-w700"><span class="h2 text-muted"></span><a href="{base_url}mrka_approval">+ <?=$jml_app_pengajuan?> </a></div>
									<div class="h5 text-muted text-uppercase push-5-t"><a href="{base_url}mrka_approval">Approval Pengajuan</a></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		<?}?>
		<?if ($jml_app_pengajuan_lanjutan){?>
			<div class="col-md-3">
				<div class="block">
					<table class="block-table text-center">
						<tbody>
							<tr>
								<td class="bg-danger" style="width: 50%;">
									<div class="push-30 push-30-t">
										<i class="si si-refresh fa-3x text-white-op"></i>
									</div>
								</td>
								<td class="bg-gray-lighter " style="width: 50%;">
									<div class="h1 font-w700"><span class="h2 text-muted"></span><a href="{base_url}mrka_approval_lanjutan">+ <?=$jml_app_pengajuan_lanjutan?> </a></div>
									<div class="h5 text-muted text-uppercase push-5-t"><a href="{base_url}mrka_approval_lanjutan">Approval Pengajuan Lanjutan</a></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('219'))){ ?>
		<?if ($jml_keuangan){?>
			<div class="col-md-3">
				<div class="block">
					<table class="block-table text-center">
						<tbody>
							<tr>
								<td class="bg-danger" style="width: 50%;">
									<div class="push-30 push-30-t">
										<i class="si si-bell fa-3x text-white-op"></i>
									</div>
								</td>
								<td class="bg-gray-lighter " style="width: 50%;">
									<div class="h1 font-w700"><span class="h2 text-muted"></span><a href="{base_url}tgudang_approval">+ <?=$jml_keuangan?> </a></div>
									<div class="h5 text-muted text-uppercase push-5-t"><a href="{base_url}tgudang_approval">Approval Keuangan</a></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		<?}?>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1432'))){ ?>
		<?if ($jml_app_mutasi){?>
			<div class="col-md-3">
				<div class="block">
					<table class="block-table text-center">
						<tbody>
							<tr>
								<td class="bg-danger" style="width: 50%;">
									<div class="push-30 push-30-t">
										<i class="si si-shuffle fa-3x text-white-op"></i>
									</div>
								</td>
								<td class="bg-gray-lighter " style="width: 50%;">
									<div class="h1 font-w700"><span class="h2 text-muted"></span><a href="{base_url}tmutasi_kas_approval">+ <?=$jml_app_mutasi?> </a></div>
									<div class="h5 text-muted text-uppercase push-5-t"><a href="{base_url}tmutasi_kas_approval">Approval Mutasi</a></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		<?}?>
		<?}?>
		<?if ($jml_pesanan){?>
		<?php if (UserAccesForm($user_acces_form,array('222'))){ ?>
			<div class="col-md-3">
				<div class="block">
					<table class="block-table text-center">
						<tbody>
							<tr>
								<td class="bg-gray-lighter " style="width: 50%;">
									<div class="h1 font-w700"><span class="h2 text-muted"></span> <a href="{base_url}tgudang_verifikasi/index/2">+ <?=$jml_pesanan?></a></div>
									<div class="h5 text-muted text-uppercase push-5-t"><a href="{base_url}tgudang_verifikasi/index/3">Pemesanan Baru </a></div>
								</td>
								<td class="bg-success" style="width: 50%;">
									<div class="push-30 push-30-t">
										<i class="si si-basket fa-3x text-white-op"></i>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		<?}?>
		<?}?>
			<?if ($jml_appvore){?>
			<div class="col-md-3">
				<div class="block">
					<table class="block-table text-center">
						<tbody>
							<tr>
								<td class="bg-primary" style="width: 50%;">
									<div class="push-30 push-30-t">
										<i class="fa fa-money fa-3x text-white-op"></i>
									</div>
								</td>
								<td class="bg-gray-lighter " style="width: 50%;">
									<div class="h1 font-w700"><span class="h2 text-muted"></span> <a href="{base_url}tkontrabon/data_verifikasi">+ <?=$jml_appvore?></a></div>
									<div class="h5 text-muted text-uppercase push-5-t"><a href="{base_url}tkontrabon/data_verifikasi">Kontrabon Baru </a></div>
								</td>
								
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<?}?>
			
			<?if ($jml_BH_approval){?>
			<div class="col-md-3">
				<div class="block">
					<table class="block-table text-center">
						<tbody>
							<tr>
								<td class="bg-danger" style="width: 50%;">
									<div class="push-30 push-30-t">
										<i class="fa fa-share-alt fa-3x text-white-op"></i>
									</div>
								</td>
								<td class="bg-gray-lighter " style="width: 50%;">
									<div class="h1 font-w700"><span class="h2 text-muted"></span> <a href="{base_url}tbagi_hasil_approval">+ <?=$jml_BH_approval?></a></div>
									<div class="h5 text-muted text-uppercase push-5-t"><a href="{base_url}tbagi_hasil_approval">Approval Bagi Hasil </a></div>
								</td>
								
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<?}?>
			<?php if (UserAccesForm($user_acces_form,array('1441'))){ ?>
			<?if ($jml_approval_honor > 0){?>
			<div class="col-md-3">
				<div class="block">
					<table class="block-table text-center">
						<tbody>
							<tr>
								<td class="bg-danger" style="width: 50%;">
									<div class="push-30 push-30-t">
										<i class="si si-users fa-3x text-white-op"></i>
									</div>
								</td>
								<td class="bg-gray-lighter " style="width: 50%;">
									<div class="h1 font-w700"><span class="h2 text-muted"></span> <a href="{base_url}thonor_approval">+ <?=$jml_approval_honor?></a></div>
									<div class="h5 text-muted text-uppercase push-5-t"><a href="{base_url}thonor_approval">Approval Honor Dokter </a></div>
								</td>
								
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<?}?>
			<?}?>
			<?php if (UserAccesForm($user_acces_form,array('1442'))){ ?>
			<?if ($jml_pembayaran_hd > 0){?>
			<div class="col-md-3">
				<div class="block">
					<table class="block-table text-center">
						<tbody>
							<tr>
								<td class="bg-success" style="width: 50%;">
									<div class="push-30 push-30-t">
										<i class="fa fa-money fa-3x text-white-op"></i>
									</div>
								</td>
								<td class="bg-gray-lighter " style="width: 50%;">
									<div class="h1 font-w700"><span class="h2 text-muted"></span> <a href="{base_url}thonor_bayar">+ <?=$jml_pembayaran_hd?></a></div>
									<div class="h5 text-muted text-uppercase push-5-t"><a href="{base_url}thonor_bayar">Pembayaran Honor Dokter </a></div>
								</td>
								
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<?}?>
			<?}?>
			
			
		</div>
		</div>
		</div>
		<div class="row">
				<?php if (UserAccesForm($user_acces_form,array('317'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}tpoliklinik_pendaftaran/index" target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/01_rajal.png" title="Pendaftaran Rawat Jalan" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Pend. Rawat Jalan</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('359'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}trawatinap_pendaftaran/index"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/02_ranap.png" title="Pendaftaran Rawat Inap" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Pend. Rawat Inap</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('1013'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}trujukan_laboratorium/index"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/03_lab.png" title="Rujukan Laboratorium" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Laboratorium</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('994'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}trujukan_radiologi/index"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/04_rad.png" title="Rujukan Radiologi" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Radiologi</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('1031'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}trujukan_fisioterapi/index"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/05_fis.png" title="Rujukan Fisioterapi" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Fisioterapi</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('330'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}tpoliklinik_tindakan/index"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/06_tin_rajal_igd.png" title="Tindakan Rawat Inap" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Tind. Rajal & IGD</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('369','1200'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}trawatinap_tindakan/index"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/06_tin_ranap.png" title="Tindakan Rawat Inap" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Tind. Rawat Inap</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('1199'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}trawatinap_tindakan_ods/index"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/07_tin_ods.png" title="Tindakan ODS" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Tind. ODS</div>
					</a>
				</div>
				<?php endif ?>
				 <?php if (UserAccesForm($user_acces_form,array('282'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}tko"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/08_ko.png" title="Kamar Operasi" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Kamar Operasi</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('1044','1053'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}tpasien_penjualan/index"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/09_farmasi.png" title="Transaksi Farmasi" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Farmasi</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('250'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}tkasir"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/10_kasir_rajal.png" title="Kasir Rawat Jalan" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Kasir Rawat Jalan</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('1090'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}trefund/index"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/11_refund.png" title="Transaksi Refund" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Refund TRX</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('1102'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}tunit_permintaan"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/12_perm_unit.png" title="Permintaan Dari Unit Ke Unit lain / Gudang" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Permintaan Unit</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('1109'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}tunit_pengembalian"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/13_penge_unit.png" title="Pengembalian Unit" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Pengembalian Unit</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('1115'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}tgudang_pemesanan"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/14_pesan_barang.png" title="Proses Pemesanan Barang" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Pemesanan Barang</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('1138'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}tgudang_penerimaan"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/15_terima_barang.png" title="Proses Penerimaan Barang" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Penerimaan Barang</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('1125'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}tgudang_verifikasi/index/2"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/16_verif_barang.png" title="Verifikasi Pemesanan" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Verifikasi Pemesanan</div>
					</a>
				</div>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}tgudang_verifikasi/index/4"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/17_proses_belanja.png" title="Proses Belanja / Pembelian" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Proses Belanja</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('1144'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}tgudang_pengembalian"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/18_retur.png" title="Retur Pemesanan" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Retur Pemesanan</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('267'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}tstockopname/index"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/19_opname.png" title="Stock Opname" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Stock Opname</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('1165'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}trm_pinjam_berkas/index"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/20_pinjam.png" title="Peminjaman Berkas Rekam Medis" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Pinjam Berkas RM</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('1167'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}trm_berkas"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/21_berkas.png" title="Berkas Rekam Medis" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Berkas RM</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('1155','1161'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}trm_layanan_berkas/index"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/22_pelayanan_berkas.png" title="Pelayanan Berkas" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Pely. Berkas</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('1196','1194'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}trm_gabung_transaksi"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/23_gabung_rm.png" title="Gabung Transaksi" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Gabung TRX</div>
					</a>
				</div>
				<?php endif ?>
				 <?php if (UserAccesForm($user_acces_form,array('1193','1224'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}trm_gabung"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/24_gabung_trx.png" title="Gabung Rekam Medis" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Gabung RM</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('251'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}lgudang_stok/index/0"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/25_laporan_stok.png" title="Laporan Stok Gudang" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Laporan Stok Gudang</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('16'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}mdokter_jadwal"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/26_jadwal.png" title="Jadwal Dokter Installasi Gawat Darurat" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Jadwal Dokter IGD</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('19'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}mpasien"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/27_pasien.png" title="Master Data Pasien" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Data Pasien</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('248'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}musers"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/28_manage_user.png" title="Pengaturan User" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Manage User</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('287'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}mkelompok_diagnosa"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/29_icd10.png" title="Pengaturan ICD 10" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Setup ICD 10</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('292'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}mkelompok_tindakan"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/30_icd9.png" title="Pengaturan ICD 9" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Setup ICD 9</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('107'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}mdata_obat"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/31_obat.png" title="Master Data Obat" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Master Obat</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('85'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}mdata_alkes"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/32_alkes.png" title="Master Data Alkes" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Master Alkes</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('96'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}mdata_implan"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/33_implan.png" title="Master Data Implan" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Master Implan</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('118'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}mdata_logistik"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/34_logistik.png" title="Master Data Logistik" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Data Logistik</div>
					</a>
				</div>
				<?php endif ?>
				 <?php if (UserAccesForm($user_acces_form,array('48'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}mdata_kategori/index"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/35_kate.png" title="Pengaturan Kategori" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Setup Kategori</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('1347'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}tkwitansi_manual"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/36_kwitansi.png" title="Kwitansi Manual" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Kwitansi Manual</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('1359'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="{base_url}tkasbon/index"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/37_kasbon.png" title="Kasbon Dokter" width="100" height="100">
						</div>
						<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Kasbon Dokter</div>
					</a>
				</div>
				<?php endif ?>
				<?php if (UserAccesForm($user_acces_form,array('1340'))): ?>
				<div class="col-md-2">
					<a class="block block-link-hover3 text-center " href="#"   target="_blank">
						<div class="block-content block-content-full bg-primary">
							<img src="./assets/icon/38_verifikasi.png" title="Verifikasi Deposit" width="100" height="100">
						</div>
					<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Verifikasi Deposit</div>
				</a>
			</div>
			<?php endif ?>
			<?php if (UserAccesForm($user_acces_form,array('1368'))): ?>
			<div class="col-md-2">
				<a class="block block-link-hover3 text-center " href="{base_url}lkasir"   target="_blank">
					<div class="block-content block-content-full bg-primary">
						<img src="./assets/icon/39_lap_kasir.png" title="Laporan Kasir" width="100" height="100">
					</div>
					<div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Laporan Kasir</div>
				</a>
			</div>
			<?php endif ?>

	</div>
</div>
<div class="modal fade" id="modal_tolak" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Alasan Tolak</h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Alasan</label>
								<div class="col-md-8">
									<textarea class="form-control" rows="2" name="alasan_tolak" id="alasan_tolak"></textarea>
									<input type="hidden" readonly class="form-control" id="id_approval"  placeholder="id_approval" name="id_approval" value="">
								</div>
							</div>
						</div>
					</div>

				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-danger text-uppercase" type="button" name="btn_tolak" id="btn_tolak"><i class="fa fa-save"></i> SIMPAN TOLAK</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_tolak_kwitansi" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Alasan Tolak Kwitansi</h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Alasan</label>
								<div class="col-md-8">
									<textarea class="form-control" rows="2" name="alasan_tolak_kwitansi" id="alasan_tolak_kwitansi"></textarea>
									<input type="hidden" readonly class="form-control" id="id_approval_kwitansi"  placeholder="id_approval_kwitansi" name="id_approval_kwitansi" value="">
								</div>
							</div>
						</div>
					</div>
			<br>

				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-danger text-uppercase" type="button" name="btn_tolak_kwitansi" id="btn_tolak_kwitansi"><i class="fa fa-save"></i> SIMPAN TOLAK</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<? $this->load->view('Tverifikasi_transaksi/modal/modal_deposit_header')?>
<script type="text/javascript">
	$(document).ready(function(){	
		// load_index_poliklinik();
		load_mpp();
		index_approval_kwitansi();
		load_evaluasi_mpp();
		load_index_deposit();
		index_approval_deposit();
		load_index_tindakan();
		load_index_trx_selesai();
		load_index_trx_card();
		$('#table-widget-permintaan-laboratorium').DataTable({
				searching: false,
        sorting: false,  
        ordering: false, 
        paging: true,
				pageLength: 5,
        lengthChange: false,
				autoWidth: true
		});

		$('#table-widget-permintaan-patologi-anatomi').DataTable({
				searching: false,
        sorting: false,  
        ordering: false, 
        paging: true,
				pageLength: 5,
        lengthChange: false,
				autoWidth: true
		});
		
		$('#table-widget-permintaan-laboratorium-bankdarah').DataTable({
				searching: false,
        sorting: false,  
        ordering: false, 
        paging: true,
				pageLength: 5,
        lengthChange: false,
				autoWidth: true
		});

		$('#table-widget-permintaan-radiologi-xray').DataTable({
				searching: false,
        sorting: false,  
        ordering: false, 
        paging: true,
				pageLength: 5,
        lengthChange: false,
				autoWidth: true
		});

		$('#table-widget-permintaan-radiologi-usg').DataTable({
				searching: false,
        sorting: false,  
        ordering: false, 
        paging: true,
				pageLength: 5,
        lengthChange: false,
				autoWidth: true
		});
		
		$('#table-widget-permintaan-radiologi-ctscan').DataTable({
				searching: false,
        sorting: false,  
        ordering: false, 
        paging: true,
				pageLength: 5,
        lengthChange: false,
				autoWidth: true
		});

		$('#table-widget-permintaan-radiologi-mri').DataTable({
				searching: false,
        sorting: false,  
        ordering: false, 
        paging: true,
				pageLength: 5,
        lengthChange: false,
				autoWidth: true
		});

		$('#table-widget-permintaan-radiologi-lainnya').DataTable({
				searching: false,
        sorting: false,  
        ordering: false, 
        paging: true,
				pageLength: 5,
        lengthChange: false,
				autoWidth: true
		});
	})
	function load_index_tindakan(){
		$('#index_tindakan').DataTable().destroy();	
		let tanggal_1=$("#tanggal_tindakan_1").val();
		let tanggal_2=$("#tanggal_tindakan_2").val();
		// let satus_deposit=$("#satus_deposit").val();
		table = $('#index_tindakan').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": true,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				  "paging": true,
				"columnDefs": [
						{"targets": [0,1,3,4,2,5,6],  className: "text-center" },
						// {"targets": [3],  className: "text-right" },
						// { "width": "10%", "targets": [1,2,3,6,7,8],  className: "text-center" },
						// { "width": "12%", "targets": [4,10],  className: "text-center" },
						// { "width": "15%", "targets": 5,  className: "text-left" },
					],
				ajax: { 
					url: '{site_url}dashboard/load_index_tindakan', 
					type: "POST" ,
					dataType: 'json',
					data : {
							tanggal_1:tanggal_1,
							tanggal_2:tanggal_2,
							// satus_deposit:satus_deposit,
							
						   }
				}
			});
		// load_jumlah_approval_deposit();
	}
	function load_index_trx_selesai(){
		$('#index_trx_selesai').DataTable().destroy();	
		let tanggal_1=$("#tanggal_selesai_1").val();
		let tanggal_2=$("#tanggal_selesai_2").val();
		// let satus_deposit=$("#satus_deposit").val();
		table = $('#index_trx_selesai').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": true,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				  "paging": true,
				"columnDefs": [
						{"targets": [0,1,3,4,2,5,6],  className: "text-center" },
						// {"targets": [3],  className: "text-right" },
						// { "width": "10%", "targets": [1,2,3,6,7,8],  className: "text-center" },
						// { "width": "12%", "targets": [4,10],  className: "text-center" },
						// { "width": "15%", "targets": 5,  className: "text-left" },
					],
				ajax: { 
					url: '{site_url}dashboard/load_index_trx_selesai', 
					type: "POST" ,
					dataType: 'json',
					data : {
							tanggal_1:tanggal_1,
							tanggal_2:tanggal_2,
							// satus_deposit:satus_deposit,
							
						   }
				}
			});
		load_jumlah_selesai();
	}
	function load_index_trx_card(){
		$('#index_trx_card').DataTable().destroy();	
		let tanggal_1=$("#tanggal_card_1").val();
		let tanggal_2=$("#tanggal_card_2").val();
		// let satus_deposit=$("#satus_deposit").val();
		table = $('#index_trx_card').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": true,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				  "paging": true,
				"columnDefs": [
						{"targets": [0,1,3,4,2,5,6],  className: "text-center" },
						// {"targets": [3],  className: "text-right" },
						// { "width": "10%", "targets": [1,2,3,6,7,8],  className: "text-center" },
						// { "width": "12%", "targets": [4,10],  className: "text-center" },
						// { "width": "15%", "targets": 5,  className: "text-left" },
					],
				ajax: { 
					url: '{site_url}dashboard/load_index_trx_card', 
					type: "POST" ,
					dataType: 'json',
					data : {
							tanggal_1:tanggal_1,
							tanggal_2:tanggal_2,
							// satus_deposit:satus_deposit,
							
						   }
				}
			});
		load_jumlah_card();
	}
	function load_index_deposit(){
		$('#index_deposit').DataTable().destroy();	
		let tanggal_1=$("#tanggal_deposit_1").val();
		let tanggal_2=$("#tanggal_deposit_2").val();
		let satus_deposit=$("#satus_deposit").val();
		table = $('#index_deposit').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": true,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				  "paging": true,
				"columnDefs": [
						{"targets": [0,1,2,5,6,7],  className: "text-center" },
						{"targets": [3],  className: "text-right" },
						// { "width": "10%", "targets": [1,2,3,6,7,8],  className: "text-center" },
						// { "width": "12%", "targets": [4,10],  className: "text-center" },
						// { "width": "15%", "targets": 5,  className: "text-left" },
					],
				ajax: { 
					url: '{site_url}dashboard/load_index_deposit', 
					type: "POST" ,
					dataType: 'json',
					data : {
							tanggal_1:tanggal_1,
							tanggal_2:tanggal_2,
							satus_deposit:satus_deposit,
							
						   }
				}
			});
		load_jumlah_approval_deposit();
	}
	function index_approval_deposit(){
		$('#index_approval_deposit').DataTable().destroy();	
		table = $('#index_approval_deposit').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": true,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				  "paging": true,
				"columnDefs": [
						{"targets": [0,1,2,5,6,7],  className: "text-center" },
						{"targets": [3],  className: "text-right" },
						// { "width": "10%", "targets": [1,2,3,6,7,8],  className: "text-center" },
						// { "width": "12%", "targets": [4,10],  className: "text-center" },
						// { "width": "15%", "targets": 5,  className: "text-left" },
					],
				ajax: { 
					url: '{site_url}dashboard/load_index_approval_deposit', 
					type: "POST" ,
					dataType: 'json',
					data : {
							// tanggal_1:tanggal_1,
							// tanggal_2:tanggal_2,
						   }
				}
			});
		load_jumlah_approval_deposit();
	}
	function index_approval_kwitansi(){
		$('#index_approval_kwitansi').DataTable().destroy();	
		table = $('#index_approval_kwitansi').DataTable({
				autoWidth: false,
				searching: true,
				serverSide: true,
				"processing": true,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				  "paging": true,
				"columnDefs": [
						{"targets": [0,1,2,5,6,7],  className: "text-center" },
						{"targets": [4],  className: "text-right" },
						// { "width": "10%", "targets": [1,2,3,6,7,8],  className: "text-center" },
						// { "width": "12%", "targets": [4,10],  className: "text-center" },
						// { "width": "15%", "targets": 5,  className: "text-left" },
					],
				ajax: { 
					url: '{site_url}dashboard/load_index_approval_kwitansi', 
					type: "POST" ,
					dataType: 'json',
					data : {
							// tanggal_1:tanggal_1,
							// tanggal_2:tanggal_2,
						   }
				}
			});
		load_jumlah_approval_kwitansi();
	}
	$(document).on("click",".setuju",function(){
		var table = $('#index_approval_deposit').DataTable()
		var tr = $(this).parents('tr')
		// var id = table.cell(tr,0).data()
		var id_approval=$(this).data('id');
		swal({
			title: "Apakah Anda Yakin  ?",
			text : "Menyetujui Pembatalan Deposit ini?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}tdeposit_approval/setuju_batal/'+id_approval+'/1',
				type: 'POST',
				success: function(data) {
					// console.log(data);
					// alert(data);
					// $.toaster({priority : 'success', title : 'Succes!', message : ' Deposit berhasil disetujui'});
					$('#index_approval_deposit').DataTable().ajax.reload( null, false );
					// if (data=='"1"'){
					// window.location.href = "{site_url}mrka_approval/next_bendahara/"+id;
					// }
				}
			});
		});

		return false;

	});
	$(document).on("click",".setuju_kwitansi",function(){
		var table = $('#index_approval_kwitansi').DataTable()
		var tr = $(this).parents('tr')
		// var id = table.cell(tr,0).data()
		var id_approval=$(this).data('id');
		swal({
			title: "Apakah Anda Yakin  ?",
			text : "Menyetujui Pembatalan Kwitansi ini?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}dashboard/setuju_batal_kwitansi/'+id_approval+'/1',
				type: 'POST',
				success: function(data) {
					// console.log(data);
					// alert(data);
					// $.toaster({priority : 'success', title : 'Succes!', message : ' Deposit berhasil disetujui'});
					$('#index_approval_kwitansi').DataTable().ajax.reload( null, false );
					// if (data=='"1"'){
					// window.location.href = "{site_url}mrka_approval/next_bendahara/"+id;
					// }
				}
			});
		});

		return false;

	});
$(document).on("click","#btn_tolak",function(){

	var id_approval=$("#id_approval").val();
	var alasan_tolak=$("#alasan_tolak").val();
	swal({
		title: "Apakah Anda Yakin  ?",
		text : "Menolak Pembatalan Deposit ini?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#d26a5c",
		cancelButtonText: "Tidak Jadi",
	}).then(function() {
		$.ajax({
			url: '{site_url}tdeposit_approval/tolak/',
			type: 'POST',
			data: {
				id_approval:id_approval,
				alasan_tolak:alasan_tolak,
			},
			complete: function() {
				$('#modal_tolak').modal('hide');
				index_approval_deposit();

			}
		});
	});

	return false;

});
$(document).on("click","#btn_tolak_kwitansi",function(){
	// alert('sini');
	var id_approval=$("#id_approval_kwitansi").val();
	var alasan_tolak=$("#alasan_tolak_kwitansi").val();
	swal({
		title: "Apakah Anda Yakin  ?",
		text : "Menolak Pembatalan Kwitansi ini?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#d26a5c",
		cancelButtonText: "Tidak Jadi",
	}).then(function() {
		$.ajax({
			url: '{site_url}dashboard/tolak_kwitansi/',
			type: 'POST',
			data: {
				id_approval:id_approval,
				alasan_tolak:alasan_tolak,
			},
			complete: function() {
				$('#modal_tolak_kwitansi').modal('hide');
				index_approval_kwitansi();

			}
		});
	});

	return false;

});
$(document).on("click",".tolak_kwitansi",function(){
	var table = $('#index_approval_kwitansi').DataTable()
	var tr = $(this).parents('tr')
	var id_approval=$(this).data('id');
	$("#id_approval_kwitansi").val(id_approval)
	$('#modal_tolak_kwitansi').modal('show');
	// modal_tolak

});
$(document).on("click",".tolak",function(){
	var table = $('#index_approval_deposit').DataTable()
	var tr = $(this).parents('tr')
	var id_approval=$(this).data('id');
	$("#id_approval").val(id_approval)
	$('#modal_tolak').modal('show');
	// modal_tolak

});
	function load_jumlah_deposit(){
		$("#cover-spin").show();
		let tanggal_1=$("#tanggal_deposit_1").val();
		let tanggal_2=$("#tanggal_deposit_2").val();
		let satus_deposit=$("#satus_deposit").val();
		$.ajax({
			url: '{site_url}dashboard/load_jumlah_deposit',
			method: "POST",
			dataType: "json",
			data: {
				"tanggal_1": tanggal_1,
				"tanggal_2": tanggal_2,
				satus_deposit:satus_deposit,
			},
			success: function(data) {
				$("#jml_deposit").text(formatNumber(data.jml_deposit));
				$("#total_deposit").text(formatNumber(data.total_deposit));
				$("#cover-spin").hide();
			},
			error: function(data) {
				console.log(data);
			}
		});
	}
	function load_jumlah_approval_deposit(){
		$("#cover-spin").show();
		
		$.ajax({
			url: '{site_url}dashboard/load_jumlah_approval_deposit',
			method: "POST",
			dataType: "json",
			data: {
				
			},
			success: function(data) {
				$("#jml_approval_deposit").text(formatNumber(data.jml_deposit));
				$("#cover-spin").hide();
			},
			error: function(data) {
				console.log(data);
			}
		});
	}
	function load_jumlah_approval_kwitansi(){
		$("#cover-spin").show();
		
		$.ajax({
			url: '{site_url}dashboard/load_jumlah_approval_kwitansi',
			method: "POST",
			dataType: "json",
			data: {
				
			},
			success: function(data) {
				$("#jml_approval_kwitansi").text(formatNumber(data.jml_deposit));
				$("#cover-spin").hide();
			},
			error: function(data) {
				console.log(data);
			}
		});
	}
	function load_jumlah_selesai(){
		$("#cover-spin").show();
		let tanggal_1=$("#tanggal_selesai_1").val();
		let tanggal_2=$("#tanggal_selesai_2").val();
		$.ajax({
			url: '{site_url}dashboard/load_jumlah_selesai',
			method: "POST",
			dataType: "json",
			data: {
				"tanggal_1": tanggal_1,
				"tanggal_2": tanggal_2,
			},
			success: function(data) {
				$("#jml_list_trx_ranap").text(formatNumber(data.jml_selesai));
				$("#cover-spin").hide();
			},
			error: function(data) {
				console.log(data);
			}
		});
	}
	function load_jumlah_card(){
		$("#cover-spin").show();
		let tanggal_1=$("#tanggal_card_1").val();
		let tanggal_2=$("#tanggal_card_2").val();
		$.ajax({
			url: '{site_url}dashboard/load_jumlah_card',
			method: "POST",
			dataType: "json",
			data: {
				"tanggal_1": tanggal_1,
				"tanggal_2": tanggal_2,
			},
			success: function(data) {
				$("#jml_card").text(formatNumber(data.jml_card));
				$("#cover-spin").hide();
			},
			error: function(data) {
				console.log(data);
			}
		});
	}
	function formatNumber (num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}

	$("#tanggal_1,#tanggal_2").change(function(){		
		load_pembelian();
	});
	$("#tanggal_mpp_1,#tanggal_mpp_2").change(function(){		
		load_mpp();
	});
	$("#waktu_deposit").change(function(){	
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}dashboard/load_waktu',
			method: "POST",
			dataType: "json",
			data: {
				"waktu_deposit": $("#waktu_deposit").val(),
			},
			success: function(data) {
				$("#tanggal_deposit_1").val(data.tanggal_deposit_1);
				$("#tanggal_deposit_2").val(data.tanggal_deposit_2);
				$("#cover-spin").hide();
			},
			error: function(data) {
				console.log(data);
			}
		});
	});
	$("#waktu_tindakan").change(function(){	
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}dashboard/load_waktu',
			method: "POST",
			dataType: "json",
			data: {
				"waktu_deposit": $("#waktu_tindakan").val(),
			},
			success: function(data) {
				$("#tanggal_tindakan_1").val(data.tanggal_deposit_1);
				$("#tanggal_tindakan_2").val(data.tanggal_deposit_2);
				load_index_tindakan();
				$("#cover-spin").hide();
			},
			error: function(data) {
				console.log(data);
			}
		});
	});
	$("#waktu_selesai").change(function(){	
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}dashboard/load_waktu',
			method: "POST",
			dataType: "json",
			data: {
				"waktu_deposit": $("#waktu_selesai").val(),
			},
			success: function(data) {
				$("#tanggal_selesai_1").val(data.tanggal_deposit_1);
				$("#tanggal_selesai_2").val(data.tanggal_deposit_2);
				load_index_trx_selesai();
				$("#cover-spin").hide();
			},
			error: function(data) {
				console.log(data);
			}
		});
	});
	$("#waktu_card").change(function(){	
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}dashboard/load_waktu',
			method: "POST",
			dataType: "json",
			data: {
				"waktu_deposit": $("#waktu_card").val(),
			},
			success: function(data) {
				$("#tanggal_card_1").val(data.tanggal_deposit_1);
				$("#tanggal_card_2").val(data.tanggal_deposit_2);
				load_index_trx_card();
				$("#cover-spin").hide();
			},
			error: function(data) {
				console.log(data);
			}
		});
	});
	$("#tanggal_evaluasi_mpp_1,#tanggal_evaluasi_mpp_2").change(function(){		
		load_evaluasi_mpp();
	});

	function load_pembelian(){
		$("#cover-spin").show();
		let tanggal_1=$("#tanggal_1").val();
		let tanggal_2=$("#tanggal_2").val();
		$.ajax({
			url: '{site_url}dashboard/load_pembelian',
			method: "POST",
			dataType: "json",
			data: {
				"tanggal_1": tanggal_1,
				"tanggal_2": tanggal_2,
			},
			success: function(data) {
				$("#index_beli tbody").empty();
				$("#index_beli tbody").append(data);
				$("#cover-spin").hide();
			},
			error: function(data) {
				console.log(data);
			}
		});
	}
	function load_mpp(){
		$("#cover-spin").show();
		let tanggal_1=$("#tanggal_mpp_1").val();
		let tanggal_2=$("#tanggal_mpp_2").val();
		$.ajax({
			url: '{site_url}dashboard/load_mpp',
			method: "POST",
			dataType: "json",
			data: {
				"tanggal_1": tanggal_1,
				"tanggal_2": tanggal_2,
			},
			success: function(data) {
				$("#index_mpp tbody").empty();
				$("#index_mpp tbody").append(data.tabel);
				$("#jml_mpp").text(data.jml_mpp);
				$("#cover-spin").hide();
			},
			error: function(data) {
				console.log(data);
			}
		});
	}
	function load_evaluasi_mpp(){
		$("#cover-spin").show();
		let tanggal_1=$("#tanggal_evaluasi_mpp_1").val();
		let tanggal_2=$("#tanggal_evaluasi_mpp_2").val();
		$.ajax({
			url: '{site_url}dashboard/load_evaluasi_mpp',
			method: "POST",
			dataType: "json",
			data: {
				"tanggal_1": tanggal_1,
				"tanggal_2": tanggal_2,
			},
			success: function(data) {
				$("#index_evaluasi_mpp tbody").empty();
				$("#index_evaluasi_mpp tbody").append(data.tabel);
				$("#jml_evavluasi_mpp").text(data.jml_mpp);
				$("#cover-spin").hide();
			},
			error: function(data) {
				console.log(data);
			}
		});
	}
</script>