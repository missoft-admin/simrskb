<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tpencairan/save_verifikasi','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}tpencairan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">TRANSAKSI PENCAIRAN</h3>
	</div>
	<div class="block-content" style="margin-top: -20px;font-size:">
		<div class="row">
			<div class="col-md-12">
				
			<input type="hidden" class="form-control" id="id" placeholder="ID" name="id" value="{id}" required="" aria-required="true">
			<input type="hidden" class="form-control" id="disabel" placeholder="ID" name="disabel" value="{disabel}" required="" aria-required="true">
			
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Tanggal Create</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input" <?=$disabel?> <?=($id?'disabled':'')?> type="text" id="tanggal_trx" name="tanggal_trx" value="<?=HumanDateLong($tanggal_trx)?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
				
			</div>
			<div class="form-group">
				
				<label class="col-md-2 control-label" for="nama">Tanggal Pencairan</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input" disabled <?=$disabel?> type="text" id="tanggal_pencairan" name="tanggal_pencairan" value="<?=HumanDateShort($tanggal_pencairan)?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama"></label>
				<div class="col-md-2">
				<a href="{base_url}tpencairan/review/<?=$id?>" class="btn btn-danger" <?=$disabel?> type="button" id="btn_filter" style="font-size:13px;width:100%;float:right;" name="btn_filter"><i class="fa fa-refresh"></i> REFRESH</a>
				</div>
			</div>
	
			
			</div>
		</div>
		
	</div>
</div>
<div class="block">
	<div class="block-header">
		<h3 class="block-title">DETAIl PENCAIRAN</h3>
	</div>
	<div class="block-content" style="margin-top: -20px;font-size:">
		
		<div class="row hiden">
			<div class="col-md-12">
				<div class="block block-themed">
					<div class="block-header bg-primary">
						<ul class="block-options">

							<li>
								<button type="button" id="btn_rajal" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">PENGAJUAN</h3>
					</div>
					<div class="block-content">
						<table id="tabel_rajal" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<?
											if ($query_pengajuan){
												$index=0;
												foreach ($list_field_pengajuan as $field)
												{
													$index=$index+1;
												   echo '<th  '.($index<=2?'hidden':'').'>'.balik_spasi_header($field).'</th>'; 
													
												}
												echo '<th style="width:10%">Action</th>';
											}
										?>
										
									</tr>
									
								</thead>
								<tbody>
									<?php 
										if ($query_pengajuan){
											foreach($query_pengajuan as $row){ 
												$aksi='<div class="btn-group">';
												$aksi .= '<a href="'.site_url().'mrka_bendahara/proses_bendahara/'.$row->id.'" '.$disabel.' data-toggle="tooltip" title="Edit" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
												$aksi .= '<a href="'.site_url().'mrka_belanja/proses_belanja/'.$row->id.'/disabled" '.$disabel.' data-toggle="tooltip" target="_blank"  title="Print" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
												$aksi .= '<a href="'.site_url().'mrka_belanja/proses_belanja/'.$row->id.'" '.$disabel.' data-toggle="tooltip" target="_blank"  title="Verifikasi" class="btn btn-danger btn-xs"><i class="fa fa-check"></i></a>';
												$aksi .='</div>';
												$no=0;
												echo '<tr>';
												foreach ($list_field_pengajuan as $field)
												{
													$no=$no+1;
													if (is_numeric($row->$field)){
														echo '<td class="text-right" '.($no<=2?'hidden':'').'>'.(is_numeric($row->$field)?number_format($row->$field,0):$row->$field).'</td>';
													}else{
														echo '<td class="text-left" '.($no<=2?'hidden':'').'>'.$row->$field.'</td>';
														
													}
												}
											
												echo '<td>'.$aksi.'</td>';
												echo '<tr>';
											} 
										}?>
								</tbody>
								
								
						</table>
					</div>
					
				</div>
			</div>
		</div>
		<div class="row hiden">
			<div class="col-md-12">
				<div class="block block-themed">
					<div class="block-header bg-primary">
						<ul class="block-options">

							<li>
								<button type="button" id="btn_rajal" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">KASBON</h3>
					</div>
					<div class="block-content">
						<table id="tabel_rajal" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<?
											if ($query_kasbon){
												$index=0;
												foreach ($list_field_kasbon as $field)
												{
													$index=$index+1;
												   echo '<th  '.($index<=3?'hidden':'').'>'.balik_spasi_header($field).'</th>'; 
													
												}
												echo '<th style="width:10%">Action</th>';
											}
										?>
										
									</tr>
									
								</thead>
								<tbody>
									<?php 
										if ($query_kasbon){
											foreach($query_kasbon as $row){ 
												$aksi='<div class="btn-group">';
												if ($row->st_verifikasi=='0'){
												$aksi .= '<a href="'.site_url().'tkasbon_kas/pembayaran/'.$row->id.'" '.$disabel.' data-toggle="tooltip" title="Edit" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
												}else{
												$aksi .= '<a href="'.site_url().'tkasbon_kas/pembayaran/'.$row->id.'/disabled" '.$disabel.' data-toggle="tooltip" title="View" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a>';
													
												}
												$aksi .= '<a href="'.site_url().'tkasbon/printout/'.$row->id.'/disabled" '.$disabel.' data-toggle="tooltip" target="_blank"  title="Print" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
												if ($row->st_verifikasi=='0'){
												$aksi .= '<button  title="Verifikasi" type="button" '.$disabel.' class="btn btn-danger btn-xs verif_kasbon"><i class="fa fa-check"></i></button>';
												}
												$aksi .='</div>';
												$no=0;
												echo '<tr>';
												foreach ($list_field_kasbon as $field)
												{
													$no=$no+1;
													if (is_numeric($row->$field)){
														echo '<td class="text-right" '.($no<=3?'hidden':'').'>'.(is_numeric($row->$field)?number_format($row->$field,0):$row->$field).'</td>';
													}else{
														echo '<td class="text-left" '.($no<=3?'hidden':'').'>'.$row->$field.'</td>';
														
													}
												}
											
												echo '<td>'.$aksi.'</td>';
												echo '<tr>';
											} 
										}?>
								</tbody>
								
								
						</table>
					</div>
					
				</div>
			</div>
		</div>
		<div class="row hiden">
			<div class="col-md-12">
				<div class="block block-themed">
					<div class="block-header bg-primary">
						<ul class="block-options">

							<li>
								<button type="button" id="btn_rajal" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">REFUND</h3>
					</div>
					<div class="block-content">
						<table id="tabel_rajal" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<?
											if ($query_refund){
												$index=0;
												foreach ($list_field_refund as $field)
												{
													$index=$index+1;
												   echo '<th  '.($index<=3?'hidden':'').'>'.balik_spasi_header($field).'</th>'; 
													
												}
												echo '<th style="width:10%">Action</th>';
											}
										?>
										
									</tr>
									
								</thead>
								<tbody>
									<?php 
										if ($query_refund){
											foreach($query_refund as $row){ 
												$aksi='<div class="btn-group">';
												if ($row->st_verifikasi=='0'){
												$aksi .= '<a href="'.site_url().'trefund_kas/pembayaran/'.$row->id.'" '.$disabel.' data-toggle="tooltip" title="Edit" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
												}else{
												$aksi .= '<a href="'.site_url().'trefund_kas/pembayaran/'.$row->id.'/disabled" '.$disabel.' data-toggle="tooltip" title="View" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a>';
													
												}
												$aksi .= '<a href="'.site_url().'Trefund/fakturRefundTransaksi/'.$row->id.'" '.$disabel.' data-toggle="tooltip" target="_blank"  title="Print" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
												if ($row->st_verifikasi=='0'){
												$aksi .= '<button  title="Verifikasi" type="button" class="btn btn-danger btn-xs verif_refund" '.$disabel.'><i class="fa fa-check"></i></button>';
												}
												$aksi .='</div>';
												$no=0;
												echo '<tr>';
												foreach ($list_field_refund as $field)
												{
													$no=$no+1;
													if (is_numeric($row->$field)){
														echo '<td class="text-right" '.($no<=3?'hidden':'').'>'.(is_numeric($row->$field)?number_format($row->$field,0):$row->$field).'</td>';
													}else{
														echo '<td class="text-left" '.($no<=3?'hidden':'').'>'.$row->$field.'</td>';
														
													}
												}
											
												echo '<td>'.$aksi.'</td>';
												echo '<tr>';
											} 
										}?>
								</tbody>
								
								
						</table>
					</div>
					
				</div>
			</div>
		</div>
		<div class="row hiden">
			<div class="col-md-12">
				<div class="block block-themed">
					<div class="block-header bg-primary">
						<ul class="block-options">

							<li>
								<button type="button" id="btn_rajal" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">MUTASI</h3>
					</div>
					<div class="block-content">
						<table id="tabel_rajal" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<?
											if ($query_mutasi){
												$index=0;
												foreach ($list_field_mutasi as $field)
												{
													$index=$index+1;
												   echo '<th  '.($index<=2?'hidden':'').'>'.balik_spasi_header($field).'</th>'; 
													
												}
												echo '<th style="width:10%">Action</th>';
											}
										?>
										
									</tr>
									
								</thead>
								<tbody>
									<?php 
										if ($query_mutasi){
											foreach($query_mutasi as $row){ 
												$aksi='<div class="btn-group">';
												if ($row->st_verifikasi=='0'){
												$aksi .= '<a href="'.site_url().'tmutasi_kas/update/'.$row->id.'" '.$disabel.' data-toggle="tooltip" title="Edit" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
												}else{
												$aksi .= '<a href="'.site_url().'tmutasi_kas/update/'.$row->id.'/disabled" '.$disabel.' data-toggle="tooltip" title="View" target="_blank" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
													
												}
												$aksi .= '<a href="'.site_url().'Trefund/fakturRefundTransaksi/'.$row->id.'" '.$disabel.' disabled data-toggle="tooltip" target="_blank"  title="Print" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
												if ($row->st_verifikasi=='0'){
												$aksi .= '<button  title="Verifikasi" type="button" class="btn btn-danger btn-xs verif_mutasi" '.$disabel.'><i class="fa fa-check"></i></button>';
												}
												$aksi .='</div>';
												$no=0;
												echo '<tr>';
												foreach ($list_field_mutasi as $field)
												{
													$no=$no+1;
													if (is_numeric($row->$field)){
														echo '<td class="text-right" '.($no<=2?'hidden':'').'>'.(is_numeric($row->$field)?number_format($row->$field,0):$row->$field).'</td>';
													}else{
														echo '<td class="text-left" '.($no<=2?'hidden':'').'>'.$row->$field.'</td>';
														
													}
												}
											
												echo '<td>'.$aksi.'</td>';
												echo '<tr>';
											} 
										}?>
								</tbody>
								
								
						</table>
					</div>
					
				</div>
			</div>
		</div>
		<div class="row hiden">
			<div class="col-md-12">
				<div class="block block-themed">
					<div class="block-header bg-primary">
						<ul class="block-options">

							<li>
								<button type="button" id="btn_rajal" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">HONOR DOKTER</h3>
					</div>
					<div class="block-content">
						<table id="tabel_rajal" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<?
											if ($query_honor){
												$index=0;
												foreach ($list_field_honor as $field)
												{
													$index=$index+1;
												   echo '<th  '.($index<=3?'hidden':'').'>'.balik_spasi_header($field).'</th>'; 
													
												}
												echo '<th style="width:10%">Action</th>';
											}
										?>
										
									</tr>
									
								</thead>
								<tbody>
									<?php 
										if ($query_honor){
											foreach($query_honor as $row){ 
												$aksi='<div class="btn-group">';
												if ($row->st_verifikasi=='0'){
												$aksi .= '<a href="'.site_url().'thonor_bayar/detail_bayar/'.$row->id.'" '.$disabel.' data-toggle="tooltip" title="Edit" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
												}else{
												$aksi .= '<a href="'.site_url().'thonor_bayar/detail_bayar/'.$row->id.'/disabled" '.$disabel.' data-toggle="tooltip" title="View" target="_blank" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
													
												}
												$aksi .= '<a href="'.site_url().'Trefund/fakturRefundTransaksi/'.$row->id.'" '.$disabel.' disabled data-toggle="tooltip" target="_blank"  title="Print" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
												if ($row->st_verifikasi=='0'){
												$aksi .= '<button  title="Verifikasi" type="button" class="btn btn-danger btn-xs verif_honor" '.$disabel.'><i class="fa fa-check"></i></button>';
												}
												$aksi .='</div>';
												$no=0;
												echo '<tr>';
												foreach ($list_field_honor as $field)
												{
													$no=$no+1;
													if (is_numeric($row->$field)){
														echo '<td class="text-right" '.($no<=3?'hidden':'').'>'.(is_numeric($row->$field)?number_format($row->$field,0):$row->$field).'</td>';
													}else{
														echo '<td class="text-left" '.($no<=3?'hidden':'').'>'.$row->$field.'</td>';
														
													}
												}
											
												echo '<td>'.$aksi.'</td>';
												echo '<tr>';
											} 
										}?>
								</tbody>
								
								
						</table>
					</div>
					
				</div>
			</div>
		</div>
		<div class="row hiden">
			<div class="col-md-12">
				<div class="block block-themed">
					<div class="block-header bg-primary">
						<ul class="block-options">

							<li>
								<button type="button" id="btn_rajal" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">KONTRABON</h3>
					</div>
					<div class="block-content">
						<table id="tabel_rajal" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<?
											if ($query_kbo){
												$index=0;
												foreach ($list_field_kbo as $field)
												{
													$index=$index+1;
												   echo '<th  '.($index<=3?'hidden':'').'>'.balik_spasi_header($field).'</th>'; 
													
												}
												echo '<th style="width:10%">Action</th>';
											}
										?>
										
									</tr>
									
								</thead>
								<tbody>
									<?php 
										if ($query_kbo){
											foreach($query_kbo as $row){ 
												$aksi='<div class="btn-group">';
												if ($row->st_verifikasi=='0'){
												$aksi .= '<a href="'.site_url().'tkontrabon/kontrabon_kas/'.$row->id.'/1" '.$disabel.' data-toggle="tooltip" title="Edit" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
												}else{
												$aksi .= '<a href="'.site_url().'tkontrabon/kontrabon_kas/'.$row->id.'" '.$disabel.' data-toggle="tooltip" title="View" target="_blank" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
													
												}
												$aksi .= '<a href="'.site_url().'Trefund/fakturRefundTransaksi/'.$row->id.'" '.$disabel.' disabled data-toggle="tooltip" target="_blank"  title="Print" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
												if ($row->st_verifikasi=='0'){
												$aksi .= '<button  title="Verifikasi" type="button" class="btn btn-danger btn-xs verif_kbo" '.$disabel.'><i class="fa fa-check"></i></button>';
												}
												$aksi .='</div>';
												$no=0;
												echo '<tr>';
												foreach ($list_field_kbo as $field)
												{
													$no=$no+1;
													if (is_numeric($row->$field)){
														echo '<td class="text-right" '.($no<=3?'hidden':'').'>'.(is_numeric($row->$field)?number_format($row->$field,0):$row->$field).'</td>';
													}else{
														echo '<td class="text-left" '.($no<=3?'hidden':'').'>'.$row->$field.'</td>';
														
													}
												}
											
												echo '<td>'.$aksi.'</td>';
												echo '<tr>';
											} 
										}?>
								</tbody>
								
								
						</table>
					</div>
					
				</div>
			</div>
		</div>
		<div class="row hiden">
			<div class="col-md-12">
				<div class="block block-themed">
					<div class="block-header bg-primary">
						<ul class="block-options">

							<li>
								<button type="button" id="btn_rajal" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">BAGI HASIL</h3>
					</div>
					<div class="block-content">
						<table id="tabel_rajal" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<?
											if ($query_bagi_hasil){
												$index=0;
												foreach ($list_field_bagi_hasil as $field)
												{
													$index=$index+1;
												   echo '<th  '.($index<=3?'hidden':'').'>'.balik_spasi_header($field).'</th>'; 
													
												}
												echo '<th style="width:10%">Action</th>';
											}
										?>
										
									</tr>
									
								</thead>
								<tbody>
									<?php 
										if ($query_bagi_hasil){
											foreach($query_bagi_hasil as $row){ 
												$aksi='<div class="btn-group">';
												if ($row->st_verifikasi=='0'){
												$aksi .= '<a href="'.site_url().'tbagi_hasil_bayar/detail_bayar/'.$row->iddet.'" '.$disabel.' data-toggle="tooltip" title="Edit" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
												}else{
												$aksi .= '<a href="'.site_url().'tbagi_hasil_bayar/detail_bayar/'.$row->iddet.'/disabled" '.$disabel.' data-toggle="tooltip" title="View" target="_blank" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
													
												}
												// $aksi .= '<a href="'.site_url().'Trefund/fakturRefundTransaksi/'.$row->id.'" '.$disabel.' disabled data-toggle="tooltip" target="_blank"  title="Print" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
												if ($row->st_verifikasi=='0'){
													$aksi .= '<button  title="Verifikasi" type="button" class="btn btn-danger btn-xs" >Unverified</button>';
												}
												$aksi .='</div>';
												$no=0;
												echo '<tr>';
												foreach ($list_field_bagi_hasil as $field)
												{
													$no=$no+1;
													if (is_numeric($row->$field)){
														echo '<td class="text-right" '.($no<=3?'hidden':'').'>'.(is_numeric($row->$field)?number_format($row->$field,0):$row->$field).'</td>';
													}else{
														echo '<td class="text-left" '.($no<=3?'hidden':'').'>'.$row->$field.'</td>';
														
													}
												}
											
												echo '<td>'.$aksi.'</td>';
												echo '<tr>';
											} 
										}?>
								</tbody>
								
								
						</table>
					</div>
					
				</div>
			</div>
		</div>
		<div class="row hiden">
			<div class="col-md-12">
				<div class="block block-themed">
					<div class="block-header bg-primary">
						<ul class="block-options">

							<li>
								<button type="button" id="btn_rajal" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">RETUR</h3>
					</div>
					<div class="block-content">
						<table id="tabel_rajal" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<?
											if ($query_retur){
												$index=0;
												foreach ($list_field_retur as $field)
												{
													$index=$index+1;
												   echo '<th  '.($index<=3?'hidden':'').'>'.balik_spasi_header($field).'</th>'; 
													
												}
												echo '<th style="width:10%">Action</th>';
											}
										?>
										
									</tr>
									
								</thead>
								<tbody>
									<?php 
										if ($query_retur){
											foreach($query_retur as $row){ 
												$aksi='<div class="btn-group">';
												if ($row->st_verifikasi=='1'){
												$aksi .= '<a href="'.site_url().'tretur_bayar/bayar/'.$row->id.'" '.$disabel.' data-toggle="tooltip" title="Edit" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
												}else{
												$aksi .= '<a href="'.site_url().'tretur_bayar/bayar/'.$row->id.'/disabled" '.$disabel.' data-toggle="tooltip" title="View" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a>';
													
												}
												$aksi .= '<a href="'.site_url().'tkasbon/printout/'.$row->id.'/disabled" '.$disabel.' data-toggle="tooltip" target="_blank"  title="Print" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
												if ($row->st_verifikasi=='1'){
												$aksi .= '<button  title="Verifikasi" type="button" '.$disabel.' class="btn btn-danger btn-xs verif_retur"><i class="fa fa-check"></i></button>';
												}
												$aksi .='</div>';
												$no=0;
												echo '<tr>';
												foreach ($list_field_retur as $field)
												{
													$no=$no+1;
													if (is_numeric($row->$field)){
														echo '<td class="text-right" '.($no<=3?'hidden':'').'>'.(is_numeric($row->$field)?number_format($row->$field,0):$row->$field).'</td>';
													}else{
														echo '<td class="text-left" '.($no<=3?'hidden':'').'>'.$row->$field.'</td>';
														
													}
												}
											
												echo '<td>'.$aksi.'</td>';
												echo '<tr>';
											} 
										}?>
								</tbody>
								
								
						</table>
					</div>
					
				</div>
			</div>
		</div>
		<div class="row hiden">
			<div class="col-md-12">
				<div class="block block-themed">
					<div class="block-header bg-primary">
						<ul class="block-options">

							<li>
								<button type="button" id="btn_rajal" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">REKAP GAJI</h3>
					</div>
					<div class="block-content">
						<table id="tabel_rajal" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<?
											if ($query_gaji){
												$index=0;
												foreach ($list_field_gaji as $field)
												{
													$index=$index+1;
												   echo '<th  '.($index<=3?'hidden':'').'>'.balik_spasi_header($field).'</th>'; 
													
												}
												echo '<th style="width:10%">Action</th>';
											}
										?>
										
									</tr>
									
								</thead>
								<tbody>
									<?php 
										if ($query_gaji){
											foreach($query_gaji as $row){ 
												$aksi='<div class="btn-group">';
												if ($row->st_verifikasi=='0'){
												$aksi .= '<a href="'.site_url().'trekap/pembayaran/'.$row->id.'" '.$disabel.' data-toggle="tooltip" title="Edit" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>';
												}else{
												$aksi .= '<a href="'.site_url().'trekap/pembayaran/'.$row->id.'/disabled" '.$disabel.' data-toggle="tooltip" title="View" target="_blank" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
													
												}
												$aksi .= '<a href="'.site_url().'Trefund/fakturRefundTransaksi/'.$row->id.'" '.$disabel.' disabled data-toggle="tooltip" target="_blank"  title="Print" class="btn btn-success btn-xs"><i class="fa fa-print"></i></a>';
												if ($row->st_verifikasi=='0'){
												$aksi .= '<button  title="Verifikasi" type="button" class="btn btn-danger btn-xs verif_gaji" '.$disabel.'><i class="fa fa-check"></i></button>';
												}
												$aksi .='</div>';
												$no=0;
												echo '<tr>';
												foreach ($list_field_gaji as $field)
												{
													$no=$no+1;
													if (is_numeric($row->$field)){
														echo '<td class="text-right" '.($no<=3?'hidden':'').'>'.(is_numeric($row->$field)?number_format($row->$field,0):$row->$field).'</td>';
													}else{
														echo '<td class="text-left" '.($no<=3?'hidden':'').'>'.$row->$field.'</td>';
														
													}
												}
											
												echo '<td>'.$aksi.'</td>';
												echo '<tr>';
											} 
										}?>
								</tbody>
								
								
						</table>
					</div>
					
				</div>
			</div>
		</div>
		<div class="row hiden">
			<div class="col-md-6">
				<h4><span class="label label-success">REKAPITULASI</span></h4><br>
				<table class="table table-striped table-borderless table-header-bg" id="index_list">
					<thead>
						<tr>
							<th hidden></th>
							<th hidden></th>
							<th style="width:70%" class="text-center">BANK</th>
							<th style="width:30%" class="text-center">TOTAL</th>
						</tr>
					</thead>
					<tbody>
						<?
						$total_all=0;
						foreach($rekap_penggajian as $row){
						$total_all=$total_all + $row->total;	
						?>
							<tr>
							<td hidden><input type="text" readonly name="sumber_kas_id[]" class="form-control sm" value="<?=$row->sumber_kas_id?>" ></td>
							<td hidden><input type="text" readonly name="nomina[]" class="form-control sm" value="<?=$row->total?>" ></td>
							<td><?=$row->nama.' ('.$row->bank.')'?></td>
							<td class="text-right"><?=number_format($row->total)?></td>
							</tr>
						<?}?>
					</tbody>
					<tfoot>
						<tr class="success">
						  <td hidden></td>
						  <td hidden></td>
						  <td class="text-center"><strong>TOTAL</strong></td>
						  <td class="text-right"><strong><?=number_format($total_all)?></strong></td>
						</tr>
				  </tfoot>
				</table>
			</div>
			<div class="col-md-6">
				<h4><span class="label label-success">CATATAN</span></h4><br>
				<textarea class="form-control" name="catatan" id="catatan"><?=$catatan?></textarea>
			</div>
		</div>
		<br>
		<br>
		<?if ($disabel ==''){?>
		<div class="form-group hiden btn_hide">
			<div class="text-right bg-light lter">
				<button class="btn btn-danger" <?=($st_verifikasi=='1'?'disabled':'')?> type="submit"  name="btn_simpan" value="1">SELESAI VERIFIKASI</button>
				<button class="btn btn-primary" <?=($status=='2'?'disabled':'')?> type="submit"  name="btn_simpan" value="2">PROSES PENCAIRAN</button>
				<button class="btn btn-success" <?=($st_verifikasi=='1'?'disabled':'')?> type="submit"  name="btn_simpan" value="3">UPDATE</button>
				<a href="{base_url}tpencairan" class="btn btn-default" type="button">BATAL</a>
			</div>
		</div>
		<?}?>
	</div>
</div>
	<?php echo form_close() ?>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	
	$(document).ready(function(){
		$(".decimal").number(true,2,'.',',');
		$('.number').number(true, 0, '.', ',');
		$('#catatan').summernote({
		  height: 120,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})

	})
	$(document).on("click",".verif_kasbon",function(){
		   var id=$(this).closest('tr').find("td:eq(0)").html();
		   // alert(id); return false;
			// var id_approval=$(this).data('id');
			var idpencarian=$("#id").val();
			swal({
				title: "Apakah Anda Yakin  ?",
				text : "Verifikasi Kasbon ini?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#d26a5c",
				cancelButtonText: "Tidak Jadi",
			}).then(function() {
				$.ajax({
					url: '{site_url}tkasbon_kas/verifikasi/'+id,
					type: 'POST',
					success: function(data) {
						// console.log(data);
						// alert(data);
						$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi berhasil'});
						// if (data=='"1"'){
						window.location.href = "{site_url}tpencairan/review/"+idpencarian;
						// }
					}
				});
			});
			
			return false;
			
	});
	$(document).on("click",".verif_retur",function(){
		   var id=$(this).closest('tr').find("td:eq(0)").html();
		   // alert(id); return false;
			// var id_approval=$(this).data('id');
			var idpencarian=$("#id").val();
			swal({
				title: "Apakah Anda Yakin  ?",
				text : "Verifikasi Retur ini?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#d26a5c",
				cancelButtonText: "Tidak Jadi",
			}).then(function() {
				$.ajax({
					url: '{site_url}tretur_bayar/posting',
					type: 'POST',
					data: {id: id},
					success: function(data) {
						// console.log(data);
						// alert(data);
						$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi berhasil'});
						// if (data=='"1"'){
						window.location.href = "{site_url}tpencairan/review/"+idpencarian;
						// }
					}
				});
			});
			
			return false;
			
	});
	$(document).on("click",".verif_refund",function(){
		   var id=$(this).closest('tr').find("td:eq(0)").html();
		   // alert(id); return false;
			// var id_approval=$(this).data('id');
			var idpencarian=$("#id").val();
			swal({
				title: "Apakah Anda Yakin  ?",
				text : "Verifikasi Refund ini?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#d26a5c",
				cancelButtonText: "Tidak Jadi",
			}).then(function() {
				$.ajax({
					// url: '{site_url}tkasbon_kas/verifikasi/'+id,
					url: '{site_url}Trefund_kas/verifikasi/'+id,
					type: 'POST',
					success: function(data) {
						// console.log(data);
						// alert(data);
						$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi berhasil'});
						// if (data=='"1"'){
						window.location.href = "{site_url}tpencairan/review/"+idpencarian;
						// }
					}
				});
			});
			
			return false;
			
	});
	$(document).on("click",".verif_honor",function(){
		   var id=$(this).closest('tr').find("td:eq(0)").html();
		   // alert(id); return false;
			// var id_approval=$(this).data('id');
			var idpencarian=$("#id").val();
			swal({
				title: "Apakah Anda Yakin  ?",
				text : "Verifikasi Honor Dokter ini?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#d26a5c",
				cancelButtonText: "Tidak Jadi",
			}).then(function() {
				$.ajax({
					// url: '{site_url}tkasbon_kas/verifikasi/'+id,
					url: '{site_url}Thonor_bayar/verifikasi/'+id,
					type: 'POST',
					success: function(data) {
						// console.log(data);
						// alert(data);
						$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi berhasil'});
						// if (data=='"1"'){
						window.location.href = "{site_url}tpencairan/review/"+idpencarian;
						// }
					}
				});
			});
			
			return false;
			
	});
	$(document).on("click",".verif_mutasi",function(){
		   var id=$(this).closest('tr').find("td:eq(0)").html();
		   // alert(id); return false;
			// var id_approval=$(this).data('id');
			var idpencarian=$("#id").val();
			swal({
				title: "Apakah Anda Yakin  ?",
				text : "Verifikasi Mutasi ini?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#d26a5c",
				cancelButtonText: "Tidak Jadi",
			}).then(function() {
				$.ajax({
					url: '{site_url}tmutasi_kas/verif/'+id,
					type: 'POST',
					success: function(data) {
						// console.log(data);
						// alert(data);
						$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi berhasil'});
						// if (data=='"1"'){
						window.location.href = "{site_url}tpencairan/review/"+idpencarian;
						// }
					}
				});
			});
			
			return false;
			
	});
	$(document).on("click",".verif_gaji",function(){
		   var id=$(this).closest('tr').find("td:eq(0)").html();
		   // alert(id); return false;
			// var id_approval=$(this).data('id');
			var idpencarian=$("#id").val();
			swal({
				title: "Apakah Anda Yakin  ?",
				text : "Verifikasi Rekap gaji ini?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#d26a5c",
				cancelButtonText: "Tidak Jadi",
			}).then(function() {
				$.ajax({
					url: '{site_url}trekap/verifikasi',
					data: {idrekap: id},
					type: 'POST',
					success: function(data) {
						// console.log(data);
						// alert(data);
						$.toaster({priority : 'success', title : 'Succes!', message : ' Verifikasi berhasil'});
						// if (data=='"1"'){
						window.location.href = "{site_url}tpencairan/review/"+idpencarian;
						// }
					}
				});
			});
			
			return false;
			
	});
	function goBack() {
	  window.history.back();
	}
	
</script>