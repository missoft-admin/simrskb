<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}tpencairan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('tpencairan/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="deskripsi">Tanggal</label>
				<div class="col-md-3">
					<div class="js-datepicker input-group date">
						<input class="js-datepicker form-control" type="text" id="tanggal_pencairan" name="tanggal_pencairan" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?=$tanggal_pencairan?>">
						<span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-md-3 col-md-offset-2">
					<button class="btn btn-success push-5-r push-10" type="submit" id="btn_generate" style="width:100%"><i class="fa fa-play"></i> Generate</button>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>
<script type="text/javascript">
	function validate_final(){
		
		if ($("#tanggal_pencairan").val() == "") {
			sweetAlert("Maaf...", "Tanggal Belum Dipilih!", "error");
			return false;
		}
		// $.ajax({
			// var tanggal_pencairan=$("#tanggal_pencairan").val();
			// url: '{site_url}tpencairan/cek_tanggal',
			// type: 'POST',
			// dataType: "json",
			// data: {tanggal_pencairan: tanggal_pencairan},
			// success : function(data) {				
				// alert(data);
			// }
		// });
	}
	function cek_duplicate(){
		
	}
</script>