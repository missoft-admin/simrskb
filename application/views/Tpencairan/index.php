<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<style media="screen">
#datatable-simrs td {
	cursor: pointer;
}
</style>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}tpencairan/create" class="btn"><i class="fa fa-plus"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<div class="row">
			<?php echo form_open('tpencairan/filter','class="form-horizontal" id="form-work"') ?>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="example-daterange1">Tanggal</label>
						<div class="col-md-8">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" id="tanggaldari" placeholder="From" value="{tanggaldari}">
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" id="tanggalsampai" placeholder="To" value="{tanggalsampai}">
							</div>
						</div>
				</div>
				
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Status</label>
					<div class="col-md-8">
						<select id="status" name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#" <?=($status == '#' ? 'selected' : '')?>>Semua Status</option>
							<option value="1" <?=($status == '1' ? 'selected' : '')?>>Belum Diproses</option>
							<option value="2" <?=($status == '2' ? 'selected' : '')?>>Telah Diproses</option>
							<option value="3" <?=($status == '3' ? 'selected' : '')?>>Verifikasi</option>
							<option value="4" <?=($status == '4' ? 'selected' : '')?>>Pencairan</option>
							<option value="5" <?=($status == '5' ? 'selected' : '')?>>Selesai</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="button" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>

		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="index_list">
			<thead>
				<tr>
					<th></th>
					<th>No.</th>
					<th>Tanggal Create</th>
					<th>Tanggal Pencairan</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
var table;

	$(document).ready(function() {
		$('.number').number(true, 0, '.', ',');
		load_index();

	});
	function load_index(){
		var tanggaldari=$("#tanggaldari").val();
		var tanggalsampai=$("#tanggalsampai").val();
		var status=$("#status").val();
		
		table = $('#index_list').DataTable({
				autoWidth: false,
				searching: true,
				pageLength: 50,
				serverSide: true,
				"processing": true,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [{ "targets": [0], "visible": false },
								{ "width": "10%", "targets": [1] },
								{ "width": "20%", "targets": [2,3,4] },
								{ "width": "30%", "targets": [5] },
							 {"targets": [1], className: "text-right" },
							 {"targets": [5], className: "text-left" },
							 {"targets": [2,3,4], className: "text-center" }
							 ],
				ajax: { 
					url: '{site_url}tpencairan/get_index', 
					type: "POST" ,
					dataType: 'json',
					data : {
							tanggaldari:tanggaldari,tanggalsampai:tanggalsampai,
							status:status,
						   }
				}
			});
	}
	$("#btn_filter").click(function() {
		// alert('ID');
		table.destroy();
		load_index();
	});
	$(document).on("click",".verifikasi",function(){
		var table = $('#index_list').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		// alert(id); return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Verifikasi Pencairan Kas?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}tpencairan/verif/'+id,
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Pencairan diselesaikan diverifikasi'});
					$('#index_list').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	$(document).on("click",".hapus",function(){
		var table = $('#index_list').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		// alert(id); return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Hapus Pencairan Kas?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}tpencairan/hapus/'+id,
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Pencairan berhasil dihapus'});
					$('#index_list').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	$(document).on("click",".kirim",function(){
		var table = $('#index_list').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		// alert(id); return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Hapus Pencairan Kas?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}tpencairan/kirim/'+id,
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Pencairan berhasil dikirim'});
					$('#index_list').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	$(document).on("click",".finish",function(){
		var table = $('#index_list').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		// alert(id); return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menyelesaikan Pencairan Kas?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}tpencairan/finish/'+id,
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Pencairan berhasil diselesaikan'});
					$('#index_list').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	// 
</script>
