<?=ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}
?>

<style media="screen">
	.text-bold {
		font-weight: bold;
	}
	.separator-header {
		margin-top: 450px;
	}
  .modal { overflow: auto !important; }
</style>

<div class="block">
	<div class="block-header">
		
		<h3 class="block-title">{title} </h3>
	</div>
	<div class="block-content">
		<div class="row">
			<div class="col-md-6">
				<div class="input-group" style="width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">No. Transaksi</span>
					<input type="text" class="form-control tindakanNoPendaftaran" value="{notransaksi}" readonly="true">
					<input type="hidden" class="form-control" id="idhead" name="idhead" value="{idhead}" readonly="true">
					<input type="hidden" class="form-control" id="idvalidasi" name="idvalidasi" value="{idvalidasi}" readonly="true">
				</div>
				<div class="input-group" style="margin-top:5px;width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">No. Medrec</span>
					<input type="text" class="form-control tindakanNoMedrec" value="{no_medrec}" readonly="true">
				</div>
				<div class="input-group" style="margin-top:5px; width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">Nama Pasien</span>
					<input type="text" class="form-control tindakanNamaPasien" value="{namapasien}" readonly="true">
				</div>
				
			</div>
			<div class="col-md-6">
				<div class="input-group" style="width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">Kelompok Pasien</span>
					<input type="text" class="form-control tindakanKelompokPasien" value="{namakelompok}" readonly="true">
				</div>
				
        <div class="input-group" style="margin-top:5px;width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">User Transaksi</span>
					<input type="text" class="form-control tindakanUserTransaksi" value="{nama_user}" readonly="true">
				</div>
        <div class="input-group" style="margin-top:5px;width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">Tanggal Transaksi</span>
					<input type="text" class="form-control tindakanTanggalTransaksi" value="{tanggal_transaksi}" readonly="true">
				</div>
					
					
			</div>
		</div>

		<div class="content-verification">
			<?if($list_group_null){?>
			<hr>
			<div class="row">
				<div class="col-md-12">
					 <div class="form-group" style="margin-bottom: 10px;">
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<h3 class="font-w300 push-15">Ada <?=count($list_group_null)?> Group Akun yang belum ditentukan</h3>
							<? 
							$no=0;
							foreach($list_group_null as $r_null){ $no++;?>
							<p><?=$no.'. '.$r_null->keterangan ?></p>
							<?}?>
						</div>
					</div>
				</div>
			</div>
			<?}?>
			<hr>

		</div>
      <hr>
			<b><span class="label label-primary" style="font-size:12px">ADMINISTRASI</span></b>
			<table class="table table-bordered table-striped" style="margin-top: 10px;">
				<thead>
					<tr>
						<th style="width:10%">No. Pendaftaran</th>
						<th style="width:10%">Tanggal</th>
						<th style="width:20%">Tindakan</th>
						<th style="width:10%">Tarif</th>
						<th style="width:10%">Diskon (%)</th>
						<th style="width:20%">Tarif Setelah Diskon</th>
						<th style="width:5%">Status Edit</th>
						<th style="width:20%" class="editOnly">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php $totalAdministrasi = 0;?>
					<?php foreach ($this->model->ViewRincianTindakan($idhead,1) as $row) {?>
            <?php
                $statusEdit = '1';
             
            ?>
						<tr>
              <td><?=$row->nopendaftaran?></td>
              <td><?=DMYFormat($row->tanggal)?></td>
              <td hidden><?=$row->idtarif?></td>
              <td><?=$row->namatarif?></td>
              <td hidden><?=$row->jasasarana?></td>
              <td hidden><?=$row->jasasarana_disc?></td>
              <td hidden><?=$row->jasapelayanan?></td>
              <td hidden><?=$row->jasapelayanan_disc?></td>
              <td hidden><?=$row->bhp?></td>
              <td hidden><?=$row->bhp_disc?></td>
              <td hidden><?=$row->biayaperawatan?></td>
              <td hidden><?=$row->biayaperawatan_disc?></td>
              <td hidden><?=number_format($row->subtotal)?></td>
              <td hidden><?=number_format($row->kuantitas)?></td>
              <td><?=number_format($row->total)?></td>
              <td hidden><?=number_format($row->diskon, 2)?></td>
              <? if ($row->diskon > 0){ ?>
                <td><?=number_format(($row->diskon / ($row->subtotal * $row->kuantitas)) * 100, 2)?></td>
              <? } else { ?>
                <td><?=number_format($row->diskon, 2)?></td>
              <? } ?>
              <td><?=number_format($row->totalkeseluruhan)?></td>
              <td><?=StatusEditVerifikasi($statusEdit)?></td>
							<td class="editOnly">
							<a href="{base_url}tvalidasi_pendapatan_rajal/edit_tindakan/<?=$idhead?>/<?=$row->iddetail?>/1" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
							</td>
						</tr>
						<?php $totalAdministrasi = $totalAdministrasi + $row->totalkeseluruhan;?>
					<?php }?>
				</tbody>
				<tfoot>
					<tr>
						<td align="center" colspan="5"><b>TOTAL</b></td>
						<td class="text-bold" colspan="4"><?=number_format($totalAdministrasi)?></td>
					</tr>
				</tfoot>
			</table>

			<hr>

			<b><span class="label label-primary" style="font-size:12px">TINDAKAN POLIKLINIK / IGD</span></b>
			<table class="table table-bordered table-striped" style="margin-top: 10px;">
				<thead>
					<tr>
						<th style="width:10%">No. Pendaftaran</th>
						<th style="width:10%">Tanggal</th>
						<th style="width:20%">Tindakan</th>
						<th style="width:10%">Tarif</th>
						<th style="width:5%">Kuantitas</th>
						<th style="width:10%">Jumlah</th>
						<th style="width:5%">Diskon (%)</th>
						<th style="width:10%">Jumlah Setelah Diskon</th>
						<th style="width:5%">Status Edit</th>
						<th style="width:20%" class="editOnly">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php $totalPoliTindakan = 0;?>
					<?php foreach ($this->model->ViewRincianTindakan($idhead,2) as $row) {?>
            <?php
              $statusEdit = '1';
             
            ?>
						<tr>
							<td><?=$row->nopendaftaran?></td>
							<td><?=DMYFormat($row->tanggal)?></td>
							<td hidden><?=$row->idtarif?></td>
							<td><?=$row->namatarif?></td>
              <td hidden><?=$row->jasasarana?></td>
              <td hidden><?=$row->jasasarana_disc?></td>
              <td hidden><?=$row->jasapelayanan?></td>
              <td hidden><?=$row->jasapelayanan_disc?></td>
              <td hidden><?=$row->bhp?></td>
              <td hidden><?=$row->bhp_disc?></td>
              <td hidden><?=$row->biayaperawatan?></td>
              <td hidden><?=$row->biayaperawatan_disc?></td>
              <td><?=number_format($row->subtotal)?></td>
              <td><?=number_format($row->kuantitas)?></td>
              <td><?=number_format($row->total)?></td>
              <td hidden><?=number_format($row->diskon, 2)?></td>
              <? if ($row->diskon > 0){ ?>
                <td><?=number_format(($row->diskon / ($row->subtotal * $row->kuantitas)) * 100, 2)?></td>
              <? } else { ?>
                <td><?=number_format($row->diskon, 2)?></td>
              <? } ?>
              <td><?=number_format($row->totalkeseluruhan)?></td>
              <td><?=StatusEditVerifikasi($statusEdit)?></td>
							<td class="editOnly">
                <a href="{base_url}tvalidasi_pendapatan_rajal/edit_tindakan/<?=$idhead?>/<?=$row->iddetail?>/2" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
							</td>
						</tr>
						<?php $totalPoliTindakan = $totalPoliTindakan + $row->totalkeseluruhan; ?>
					<?php }?>
				</tbody>
				<tfoot>
					<tr>
						<td align="center" colspan="7"><b>TOTAL</b></td>
						<td class="text-bold" colspan="4"><?=number_format($totalPoliTindakan)?></td>
					</tr>
				</tfoot>
			</table>

			<hr>

			<b><span class="label label-primary" style="font-size:12px">TINDAKAN POLIKLINIK / IGD</span></b>
			<b><span class="label label-default" style="font-size:12px">OBAT</span></b>
			<table class="table table-bordered table-striped" style="margin-top: 10px;">
				<thead>
          <tr>
						<th style="width:10%">No. Transaksi</th>
						<th style="width:10%">Tanggal</th>
						<th style="width:20%">Obat</th>
						<th style="width:10%">Tarif</th>
						<th style="width:5%">Kuantitas</th>
						<th style="width:10%">Jumlah</th>
						<th style="width:5%">Diskon (%)</th>
						<th style="width:10%">Jumlah Setelah Diskon</th>
						<th style="width:5%">Status Edit</th>
						<th style="width:20%" class="editOnly">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php $totalPoliObat = 0;?>
					<?php foreach ($this->model->ViewRincianObatIGD($idhead,3) as $row) {?>
            <?php
              $statusEdit = '1';
            ?>
            <tr>
              <td><?=$row->nopendaftaran?></td>
              <td><?=DMYFormat($row->tanggal)?></td>
              <td><?=$row->namabarang?></td>
							<td style="display: none"><?=number_format($row->hargadasar)?></td>
							<td style="display: none"><?=number_format($row->margin)?></td>
							<td><?=number_format($row->hargajual)?></td>
							<td><?=number_format($row->kuantitas)?></td>
							<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
							<td hidden><?=number_format($row->diskon, 2)?></td>
              <? if ($row->diskon > 0){ ?>
                <td><?=number_format(($row->diskon / ($row->hargajual * $row->kuantitas)) * 100, 2)?></td>
              <? } else { ?>
                <td><?=number_format($row->diskon, 2)?></td>
              <? } ?>
							<td><?=number_format($row->totalkeseluruhan)?></td>
              <td><?=StatusEditVerifikasi($statusEdit)?></td>
							<td class="editOnly">
                <a href="{base_url}tvalidasi_pendapatan_rajal/edit_obat/<?=$idhead?>/3" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpoliklinik_obat/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpoliklinik_obat/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
							</td>
						</tr>
						<?php $totalPoliObat = $totalPoliObat + $row->totalkeseluruhan; ?>
					<?php }?>
				</tbody>
				<tfoot>
					<tr>
						<td align="center" colspan="7"><b>TOTAL</b></td>
						<td class="text-bold" colspan="4"><?=number_format($totalPoliObat)?></td>
					</tr>
				</tfoot>
			</table>

			<hr>

			<b><span class="label label-primary" style="font-size:12px">TINDAKAN POLIKLINIK / IGD</span></b>
			<b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
			<table class="table table-bordered table-striped" style="margin-top: 10px;">
				<thead>
          <tr>
						<th style="width:10%">No. Transaksi</th>
						<th style="width:10%">Tanggal</th>
						<th style="width:20%">Alat Kesehatan</th>
						<th style="width:10%">Tarif</th>
						<th style="width:5%">Kuantitas</th>
						<th style="width:10%">Jumlah</th>
						<th style="width:5%">Diskon (%)</th>
						<th style="width:10%">Jumlah Setelah Diskon</th>
						<th style="width:5%">Status Edit</th>
						<th style="width:20%" class="editOnly">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php $totalPoliAlkes = 0;?>
					<?php foreach ($this->model->ViewRincianObatIGD($idhead,1) as $row) {?>
            <?php
              $statusEdit = '1';
            ?>
            <tr>
              <td><?=$row->nopendaftaran?></td>
              <td><?=DMYFormat($row->tanggal)?></td>
              <td><?=$row->namabarang?></td>
							<td style="display: none"><?=number_format($row->hargadasar)?></td>
							<td style="display: none"><?=number_format($row->margin)?></td>
							<td><?=number_format($row->hargajual)?></td>
							<td><?=number_format($row->kuantitas)?></td>
							<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
							<td hidden><?=number_format($row->diskon, 2)?></td>
              <? if ($row->diskon > 0){ ?>
                <td><?=number_format(($row->diskon / ($row->hargajual * $row->kuantitas)) * 100, 2)?></td>
              <? } else { ?>
                <td><?=number_format($row->diskon, 2)?></td>
              <? } ?>
							<td><?=number_format($row->totalkeseluruhan)?></td>
              <td><?=StatusEditVerifikasi($statusEdit)?></td>
							<td class="editOnly">
								<a href="{base_url}tvalidasi_pendapatan_rajal/edit_obat/<?=$idhead?>/1" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpoliklinik_alkes/<=$row->iddetail?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
                <!-- <a href="{base_url}tverifikasi_transaksi/detail_tindakan/tpoliklinik_alkes/<=$row->iddetail?>#notice" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-arrow-down"></i></a> -->
							</td>
						</tr>
						<?php
            $totalPoliAlkes = $totalPoliAlkes + $row->totalkeseluruhan;
            ?>
					<?php }?>
				</tbody>
				<tfoot>
					<tr>
						<td align="center" colspan="7"><b>TOTAL</b></td>
						<td class="text-bold" colspan="4"><?=number_format($totalPoliAlkes)?></td>
					</tr>
				</tfoot>
			</table>

			<hr>

			<b><span class="label label-primary" style="font-size:12px">TINDAKAN LABORATORIUM</span></b>
			<table class="table table-bordered table-striped" style="margin-top: 10px;">
				<thead>
					<tr>
						<th style="width:10%">No. Pendaftaran</th>
						<th style="width:10%">Tanggal</th>
						<th style="width:20%">Tindakan</th>
						<th style="width:10%">Tarif</th>
						<th style="width:5%">Kuantitas</th>
						<th style="width:10%">Jumlah</th>
						<th style="width:5%">Diskon (%)</th>
						<th style="width:10%">Jumlah Setelah Diskon</th>
						<th style="width:5%">Status Edit</th>
						<th style="width:20%" class="editOnly">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php $totalLaboratorium = 0;?>
					<?php foreach ($this->model->ViewRincianTindakan($idhead,3) as $row) {?>
            <?php
              $statusEdit = '1';
            ?>
						<tr>
							<td><?=$row->nopendaftaran?></td>
              <td><?=DMYFormat($row->tanggal)?></td>
              <td hidden><?=$row->idtarif?></td>
              <td><?=$row->namatarif?></td>
              <td hidden><?=$row->jasasarana?></td>
              <td hidden><?=$row->jasasarana_disc?></td>
              <td hidden><?=$row->jasapelayanan?></td>
              <td hidden><?=$row->jasapelayanan_disc?></td>
              <td hidden><?=$row->bhp?></td>
              <td hidden><?=$row->bhp_disc?></td>
              <td hidden><?=$row->biayaperawatan?></td>
              <td hidden><?=$row->biayaperawatan_disc?></td>
              <td><?=number_format($row->subtotal)?></td>
              <td><?=number_format($row->kuantitas)?></td>
              <td><?=number_format($row->total)?></td>
              <td hidden><?=number_format($row->diskon, 2)?></td>
              <? if ($row->diskon > 0){ ?>
                <td><?=number_format(($row->diskon / ($row->subtotal * $row->kuantitas)) * 100, 2)?></td>
              <? } else { ?>
                <td><?=number_format($row->diskon, 2)?></td>
              <? } ?>
              <td><?=number_format($row->totalkeseluruhan)?></td>
              <td><?=StatusEditVerifikasi($statusEdit)?></td>
							<td class="editOnly">
                <a href="{base_url}tvalidasi_pendapatan_rajal/edit_tindakan/<?=$idhead?>/<?=$row->iddetail?>/3" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
							</td>
						</tr>
						<?php $totalLaboratorium = $totalLaboratorium + $row->totalkeseluruhan; ?>
					<?php }?>
				</tbody>
				<tfoot>
					<tr>
						<td align="center" colspan="7"><b>TOTAL</b></td>
						<td class="text-bold" colspan="4"><?=number_format($totalLaboratorium)?></td>
					</tr>
				</tfoot>
			</table>

			<hr>

			<b><span class="label label-primary" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
			<table class="table table-bordered table-striped" style="margin-top: 10px;">
				<thead>
					<tr>
						<th style="width:10%">No. Pendaftaran</th>
						<th style="width:10%">Tanggal</th>
						<th style="width:20%">Tindakan</th>
						<th style="width:10%">Tarif</th>
						<th style="width:5%">Kuantitas</th>
						<th style="width:10%">Jumlah</th>
						<th style="width:5%">Diskon (%)</th>
						<th style="width:10%">Jumlah Setelah Diskon</th>
						<th style="width:5%">Status Edit</th>
						<th style="width:20%" class="editOnly">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php $totalRadiologi = 0;?>
					<?php foreach ($this->model->ViewRincianTindakan($idhead,4) as $row) {?>
            <?php
              $statusEdit = '1';
            ?>
						<tr>
							<td><?=$row->nopendaftaran?></td>
              <td><?=DMYFormat($row->tanggal)?></td>
              <td hidden><?=$row->idtarif?></td>
              <td><?=$row->namatarif?></td>
              <td hidden><?=$row->jasasarana?></td>
              <td hidden><?=$row->jasasarana_disc?></td>
              <td hidden><?=$row->jasapelayanan?></td>
              <td hidden><?=$row->jasapelayanan_disc?></td>
              <td hidden><?=$row->bhp?></td>
              <td hidden><?=$row->bhp_disc?></td>
              <td hidden><?=$row->biayaperawatan?></td>
              <td hidden><?=$row->biayaperawatan_disc?></td>
              <td><?=number_format($row->subtotal)?></td>
              <td><?=number_format($row->kuantitas)?></td>
              <td><?=number_format($row->total)?></td>
              <td hidden><?=number_format($row->diskon, 2)?></td>
              <? if ($row->diskon > 0){ ?>
                <td><?=number_format(($row->diskon / ($row->subtotal * $row->kuantitas)) * 100, 2)?></td>
              <? } else { ?>
                <td><?=number_format($row->diskon, 2)?></td>
              <? } ?>
              <td><?=number_format($row->totalkeseluruhan)?></td>
              <td><?=StatusEditVerifikasi($statusEdit)?></td>
							<td class="editOnly">
                <a href="{base_url}tvalidasi_pendapatan_rajal/edit_tindakan/<?=$idhead?>/<?=$row->iddetail?>/4" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
							</td>
						</tr>
						<?php $totalRadiologi = $totalRadiologi + $row->totalkeseluruhan; ?>
					<?php }?>
				</tbody>
				<tfoot>
					<tr>
						<td align="center" colspan="7"><b>TOTAL</b></td>
						<td class="text-bold" colspan="4"><?=number_format($totalRadiologi)?></td>
					</tr>
				</tfoot>
			</table>

			<hr>

			<b><span class="label label-primary" style="font-size:12px">TINDAKAN FISIOTERAPI</span></b>
			<table class="table table-bordered table-striped" style="margin-top: 10px;">
				<thead>
					<tr>
						<th style="width:10%">No. Rujukan</th>
						<th style="width:10%">Tanggal</th>
						<th style="width:20%">Tindakan</th>
						<th style="width:10%">Tarif</th>
						<th style="width:5%">Kuantitas</th>
						<th style="width:10%">Jumlah</th>
						<th style="width:5%">Diskon (%)</th>
						<th style="width:10%">Jumlah Setelah Diskon</th>
						<th style="width:5%">Status Edit</th>
						<th style="width:20%" class="editOnly">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php $totalFisioterapi = 0;?>
					<?php foreach ($this->model->ViewRincianTindakan($idhead,5) as $row) {?>
            <?php
              $statusEdit = '1';
            ?>
						<tr>
							<td><?=$row->nopendaftaran?></td>
              <td><?=DMYFormat($row->tanggal)?></td>
              <td hidden><?=$row->idtarif?></td>
              <td><?=$row->namatarif?></td>
              <td hidden><?=$row->jasasarana?></td>
              <td hidden><?=$row->jasasarana_disc?></td>
              <td hidden><?=$row->jasapelayanan?></td>
              <td hidden><?=$row->jasapelayanan_disc?></td>
              <td hidden><?=$row->bhp?></td>
              <td hidden><?=$row->bhp_disc?></td>
              <td hidden><?=$row->biayaperawatan?></td>
              <td hidden><?=$row->biayaperawatan_disc?></td>
              <td><?=number_format($row->subtotal)?></td>
              <td><?=number_format($row->kuantitas)?></td>
              <td><?=number_format($row->total)?></td>
              <td hidden><?=number_format($row->diskon, 2)?></td>
              <? if ($row->diskon > 0){ ?>
                <td><?=number_format(($row->diskon / ($row->subtotal * $row->kuantitas)) * 100, 2)?></td>
              <? } else { ?>
                <td><?=number_format($row->diskon, 2)?></td>
              <? } ?>
              <td><?=number_format($row->totalkeseluruhan)?></td>
              <td><?=StatusEditVerifikasi($statusEdit)?></td>
							<td class="editOnly">
                <a href="{base_url}tvalidasi_pendapatan_rajal/edit_tindakan/<?=$idhead?>/<?=$row->iddetail?>/5" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
							</td>
						</tr>
						<?php $totalFisioterapi = $totalFisioterapi + $row->totalkeseluruhan; ?>
					<?php }?>
				</tbody>
				<tfoot>
					<tr>
						<td align="center" colspan="7"><b>TOTAL</b></td>
						<td class="text-bold" colspan="4"><?=number_format($totalFisioterapi)?></td>
					</tr>
				</tfoot>
			</table>

      <hr>

      <b><span class="label label-primary" style="font-size:12px">TINDAKAN FARMASI</span></b>
			<b><span class="label label-default" style="font-size:12px">OBAT</span></b>
			<table class="table table-bordered table-striped" style="margin-top: 10px;">
				<thead>
          <tr>
						<th style="width:10%">No. Transaksi</th>
						<th style="width:10%">Tanggal</th>
						<th style="width:20%">Obat</th>
						<th style="width:10%">Tarif</th>
						<th style="width:5%">Kuantitas</th>
						<th style="width:10%">Jumlah</th>
						<th style="width:5%">Diskon (%)</th>
						<th style="width:10%">Jumlah Setelah Diskon</th>
						<th style="width:5%">Status Edit</th>
						<th style="width:20%" class="editOnly">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php $totalFarmasiObat = 0;?>
					<?php foreach ($this->model->ViewRincianFarmasiNonRacikan($idhead, 3) as $row) {?>
            <?php
              $statusEdit = '1';
             
            ?>
            <tr>
              <td><?=$row->nopendaftaran?></td>
              <td><?=DMYFormat($row->tanggal)?></td>
              <td><?=$row->namabarang?></td>
							<td style="display: none"><?=number_format($row->hargadasar)?></td>
							<td style="display: none"><?=number_format($row->margin)?></td>
							<td><?=number_format($row->hargajual)?></td>
							<td><?=number_format($row->kuantitas)?></td>
							<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
							<td hidden><?=number_format($row->diskon, 2)?></td>
              <? if ($row->diskon > 0){ ?>
                <td><?=number_format(($row->diskon / ($row->hargajual * $row->kuantitas)) * 100, 2)?></td>
              <? } else { ?>
                <td><?=number_format($row->diskon, 2)?></td>
              <? } ?>
							<td><?=number_format($row->totalkeseluruhan)?></td>
							<td style="display: none"><?=$row->idunit?></td>
              <td><?=StatusEditVerifikasi($statusEdit)?></td>
							<td class="editOnly">
								<a href="{base_url}tvalidasi_pendapatan_rajal/edit_farmasi_refund/<?=$idhead?>/3" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
							</td>
						</tr>
						<?php $totalFarmasiObat = $totalFarmasiObat + $row->totalkeseluruhan; ?>
					<?php }?>
				</tbody>
				<tfoot>
					<tr>
						<td align="center" colspan="7"><b>TOTAL</b></td>
						<td class="text-bold" colspan="4"><?=number_format($totalFarmasiObat)?></td>
					</tr>
				</tfoot>
			</table>

			<hr>

			<b><span class="label label-primary" style="font-size:12px">TINDAKAN FARMASI</span></b>
			<b><span class="label label-default" style="font-size:12px">OBAT RACIKAN</span></b>
			<table class="table table-bordered table-striped" style="margin-top: 10px;">
				<thead>
          <tr>
						<th style="width:10%">No. Transaksi</th>
						<th style="width:10%">Tanggal</th>
						<th style="width:20%">Obat</th>
						<th style="width:10%">Tarif</th>
						<th style="width:5%">Kuantitas</th>
						<th style="width:10%">Jumlah</th>
						<th style="width:5%">Diskon (%)</th>
						<th style="width:10%">Jumlah Setelah Diskon</th>
						<th style="width:5%">Status Edit</th>
						<th style="width:20%" class="editOnly">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php $totalFarmasiObatRacikan = 0;?>
					<?php foreach ($this->model->ViewRincianFarmasiRacikan($idhead) as $row) {?>
            <tr>
              <td><?=$row->nopendaftaran?></td>
              <td><?=DMYFormat($row->tanggal)?></td>
              <td><?=$row->namabarang?></td>
							<td style="display: none"><?=number_format($row->hargadasar)?></td>
							<td style="display: none"><?=number_format($row->margin)?></td>
							<td><?=number_format($row->hargajual)?></td>
							<td><?=number_format($row->kuantitas)?></td>
							<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
							<td hidden><?=number_format($row->diskon, 2)?></td>
              <? if ($row->diskon > 0){ ?>
                <td><?=number_format(($row->diskon / ($row->hargajual * $row->kuantitas)) * 100, 2)?></td>
              <? } else { ?>
                <td><?=number_format($row->diskon, 2)?></td>
              <? } ?>
							<td><?=number_format($row->totalkeseluruhan)?></td>
							<td style="display: none">1</td>
              <td>-</td>
							<td>
								<a href="{base_url}tvalidasi_pendapatan_rajal/edit_farmasi_racikan/<?=$idhead?>/<?=$row->iddetail?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
							</td>
						</tr>
						<?php $totalFarmasiObatRacikan = $totalFarmasiObatRacikan + $row->totalkeseluruhan; ?>
					<?php }?>
				</tbody>
				<tfoot>
					<tr>
						<td align="center" colspan="7"><b>TOTAL</b></td>
						<td class="text-bold" colspan="4"><?=number_format($totalFarmasiObatRacikan)?></td>
					</tr>
				</tfoot>
			</table>

      <hr>

			<b><span class="label label-primary" style="font-size:12px">TINDAKAN FARMASI</span></b>
			<b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
			<table class="table table-bordered table-striped" style="margin-top: 10px;">
				<thead>
          <tr>
						<th style="width:10%">No. Transaksi</th>
						<th style="width:10%">Tanggal</th>
						<th style="width:20%">Alat Kesehatan</th>
						<th style="width:10%">Tarif</th>
						<th style="width:5%">Kuantitas</th>
						<th style="width:10%">Jumlah</th>
						<th style="width:5%">Diskon (%)</th>
						<th style="width:10%">Jumlah Setelah Diskon</th>
						<th style="width:5%">Status Edit</th>
						<th style="width:20%" class="editOnly">Aksi</th>
					</tr>
				</thead>
				<tbody>
          <?php $totalFarmasiAlkes = 0;?>
          <?php $dataFarmasiAlkesIGD = $this->model->ViewRincianFarmasiNonRacikan($idhead, 1);?>
					<?php foreach ($dataFarmasiAlkesIGD as $row) {?>
            <?php
              $statusEdit = '1';
             
            ?>
            <?php $subtotal = $row->hargajual * $row->kuantitas ?>
            <tr>
              <td><?=$row->nopendaftaran?></td>
              <td><?=DMYFormat($row->tanggal)?></td>
              <td><?=$row->namabarang?></td>
							<td style="display: none"><?=number_format($row->hargadasar)?></td>
							<td style="display: none"><?=number_format($row->margin)?></td>
							<td><?=number_format($row->hargajual)?></td>
							<td><?=number_format($row->kuantitas)?></td>
							<td><?=number_format($row->hargajual * $row->kuantitas)?></td>
							<td hidden><?=number_format($row->diskon, 2)?></td>
              <? if ($row->diskon > 0){ ?>
                <td><?=number_format(($row->diskon / ($row->hargajual * $row->kuantitas)) * 100, 2)?></td>
              <? } else { ?>
                <td><?=number_format($row->diskon, 2)?></td>
              <? } ?>
							<td><?=number_format($row->totalkeseluruhan)?></td>
							<td style="display: none">1</td>
              <td><?=StatusEditVerifikasi($statusEdit)?></td>
							<td class="editOnly">
								<a href="{base_url}tvalidasi_pendapatan_rajal/edit_farmasi_refund/<?=$idhead?>/1" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
							</td>
						</tr>
						<?php $totalFarmasiAlkes = $totalFarmasiAlkes + $row->totalkeseluruhan; ?>
					<?php }?>
				</tbody>
				<tfoot>
					<tr>
						<td align="center" colspan="7"><b>TOTAL</b></td>
						<td class="text-bold" colspan="4"><?=number_format($totalFarmasiAlkes)?></td>
					</tr>
				</tfoot>
			</table>

			<?php
      // All
      $totalKeseluruhan = $totalAdministrasi + $totalPoliTindakan + $totalPoliObat + $totalPoliAlkes + $totalLaboratorium + $totalRadiologi + $totalFisioterapi + $totalFarmasiObat + $totalFarmasiObatRacikan + $totalFarmasiAlkes;
      ?>

			<hr>

			<table class="table table-bordered table-striped" style="margin-top: 20px;">
				<thead>
					<tr>
						<td align="right" style="font-size:15px"><b>TOTAL KESELURUHAN</b></td>
						<td align="left" class="text-bold" id="grandTotal" align="center" style="font-size:15px"><?=number_format($totalKeseluruhan)?></td>
					</tr>
				</thead>
			</table>

		</div>
	</div>


<div id="transaction-only">
	<div class="block">
		<div class="block-content">
			<h5 style="margin-bottom: 10px;">Refund Tunai</h5>
			
		
			<div class="row">
				<div class="col-sm-12">
					<div class="progress progress-mini">
						<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
					</div>
					<div class="control-group">
						<div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0; margin-bottom:20px">
							<table id="manage_tabel_pembayaran" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 5%;">#</th>
										<th style="width: 25%;">Jenis Pembayaran</th>
										<th style="width: 35%;">Keterangan</th>
										<th style="width: 10%;">Nominal</th>
										<th style="width: 10%;" class="editOnly">Aksi</th>
									</tr>
								</thead>
								<input type="hidden" id="rowindex">
								<input type="hidden" id="nomor">
								<tbody>
									<?php 
										$no = 1;
										$bayar_rp = 0;
									
									?>
									<?php foreach ($detail_pembayaran as $row) {
										$bayar_rp=$bayar_rp + $row->nominal;
										?>
										<tr>
											<td><?=$no++?></td>
											<td><?=metodePembayaran($row->idmetode)?></td>
											<td><?=$row->metode_nama?></td>
											<td><?=number_format($row->nominal)?></td>
											
											<td class="editOnly">
												<a href="{base_url}tvalidasi_pendapatan_rajal/edit_rajal_pembayaran_refund/<?=$row->idhead?>/<?=$row->ref_id?>" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
											</td>
										</tr>
									<?php }?>
								</tbody>
								<tfoot id="foot_pembayaran">
									<tr class="bg-light dker">
										<th colspan="3"><label class="pull-right"><b>Pembayaran Rp.</b></label></th>
										<th colspan="2">
												<input type="text" class="form-control input-sm number" readonly id="bayar_rp" name="bayar_rp" value="<?=$bayar_rp?>">
										</th>
									</tr>
									
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
  	
	</div>
	<div class="block">
		<div class="block-content">
			<h5 style="margin-bottom: 10px;"><?=text_danger('RINGKASAN AKUN')?></h5>
			
			<div class="row">
				<div class="col-sm-12">
					<div class="progress progress-mini">
						<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
					</div>
					<div class="control-group">
						<div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0; margin-bottom:20px">
							<table class="table table-bordered table-striped fixed" id="index_ringkasan" style="width: 100%;">
								<thead>
									<tr>
										<th style="width:3%"  class="text-center">NO </th>
										<th style="width:15%"  class="text-center">NO AKUN</th>								
										<th style="width:30%"  class="text-center">AKUN</th>								
										<th style="width:20%"  class="text-center">DEBIT</th>								
										<th style="width:20%"  class="text-center">KREDIT</th>								
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
  	
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('.number').number(true, 0, '.', ',');
			$('.discount').number(true, 2, '.', ',');

			var statusEdit = 1;
			if (statusEdit == "0") {
			  $(".editOnly").hide();
			  $("#btn_pembayaran").hide();
			  $("input, select").prop('disabled',true);
			}
			load_ringkasan();
    
	});
	$(document).on('click', '#btn_simpan', function() {
		var idgroup_diskon = $("#idgroup_diskon").val();
		var idgroup_gabungan = $("#idgroup_gabungan").val();
		var idhead = $("#idhead").val();
		// alert(idgroup_diskon);
		// return false;

		$.ajax({
			url: '{site_url}tvalidasi_pendapatan_rajal/updateDetail',
			method: 'POST',
			dataType: 'json',
			data: {
				idhead: idhead,
				idgroup_diskon: idgroup_diskon,
				idgroup_gabungan: idgroup_gabungan,
				table: 'tpoliklinik_pendaftaran'
			},
			success: function(data) {
				// alert(data.status);
				  if (data.status) {
					swal("Informasi", "Update Berhasil", "success");
					load_ringkasan();
				  }
			}
		});
	});
			
	function load_ringkasan(){
		$("#cover-spin").show();
		var id=$("#idvalidasi").val();
		var idhead=$("#idhead").val();
		var disabel=$("#disabel").val();
		$('#index_ringkasan tbody').empty();
		$.ajax({
			url: '{site_url}tvalidasi_pendapatan_rajal/load_ringkasan/',
			dataType: "json",
			type: "POST",
			data: {id: id,disabel: disabel,idhead: idhead},
			success: function(data) {
				// console.log(data.tabel);
				$('#index_ringkasan').append(data.tabel);
				$(".opsi").select2({dropdownAutoWidth: false});
				$("#cover-spin").hide();
				// $(".tgl").datepicker();
				// $(".simpan").hide();
			}
		});
	}
</script>

