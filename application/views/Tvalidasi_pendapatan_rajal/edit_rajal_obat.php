<style>
table.fixed {
      table-layout: fixed;
      width: 100%;
    }
    table.fixed td {
      overflow: hidden;
    }
</style>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tvalidasi_pendapatan_rajal/save_edit_rajal_obat','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Pendaftaran</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="notransaksi"  name="notransaksi" value="{notransaksi}">
                        <input type="hidden" readonly class="form-control" id="idvalidasi"  name="idvalidasi" value="{idvalidasi}">
                        <input type="hidden" readonly class="form-control" id="idhead"  name="idhead" value="{idhead}">
                        <input type="hidden" readonly class="form-control" id="iddetail"  name="iddetail" value="{iddetail}">
                        <input type="hidden" readonly class="form-control" id="id"  name="id" value="{id}">
                        <input type="hidden" readonly class="form-control" id="pendaftaran_id"  name="pendaftaran_id" value="{pendaftaran_id}">
                        <input type="hidden" readonly class="form-control" id="kasir_id"  name="kasir_id" value="{kasir_id}">
                        <input type="hidden" readonly class="form-control" id="disabel"  name="disabel" value="{disabel}">
                        <input type="hidden" readonly class="form-control" id="idjenis"  name="idjenis" value="{idtipe}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Medrec </label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="notransaksi"  name="notransaksi" value="<?=($no_medrec)?>">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Pasien </label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="notransaksi"  name="notransaksi" value="<?=($namapasien)?>">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Tipe </label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="notransaksi"  name="notransaksi" value="<?=GetTipePasienValidasiRajal($idtipe)?>">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Poliklinik </label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="notransaksi"  name="notransaksi" value="<?=($namapoliklinik)?>">
                    </div>
                </div>
				
                
            </div>
			<div class="col-md-6">
				 <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Kelompok Pasien</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_transaksi" placeholder="" name="tanggal_transaksi" value="<?=($namakelompok)?>">
                        
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Asuransi</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_transaksi" placeholder="" name="tanggal_transaksi" value="<?=($rekanan)?>">
                        
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Dokter</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_transaksi" placeholder="" name="tanggal_transaksi" value="<?=($dokter)?>">
                        
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">User Transaksi</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="notransaksi"  name="notransaksi" value="{user_trx}">
                        
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Tanggal Transaksi</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="notransaksi"  name="notransaksi" value="<?=HumanDateShort($tanggaldaftar)?>">
                        
                    </div>
                </div>
               
                
				
               
			</div>
            
        </div>
	</div>
	
	<div class="block-content">
		<h3 class="block-title"> <?=text_default(GetTipeKategoriBarang($idtipe))?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped" id="index_hasil" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:15%"  class="text-center">ITEM BIAYA</th>
								<th style="width:8%"  class="text-right">HARGA</th>
								<th style="width:8%"  class="text-center">DISKON Rp.</th>
								<th style="width:5%"  class="text-center">KUANTITAS</th>
								<th style="width:10%"  class="text-center">TOTAL</th>
								<th style="width:22%"  class="text-center">GROUP JUAL</th>
								<th style="width:22%"  class="text-center">GROUP DISKON</th>
								<th style="display: none">ID</th>
							</tr>
						</thead>
						<tbody>
							<? foreach($list_obat as $r){?>
							<tr>
								<td class="text-left"><?=$r->namabarang?></td>
								<td class="text-right"><?=number_format($r->hargajual)?></td>
								<td class="text-right"><?=number_format($r->diskon_rp)?></td>
								<td class="text-right"><?=number_format($r->kuantitas)?></td>
								<td class="text-right"><?=number_format($r->totalkeseluruhan)?></td>
								<td>
									<select name="idgroup_penjualan[]" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="0" <?=($r->idgroup_penjualan==null?'selected':'')?>>Belum dipilih</option>
									<?foreach($list_group as $row){?>
										<option value="<?=$row->id?>" <?=($r->idgroup_penjualan==$row->id?'selected':'')?>><?=$row->nama?></option>
									<?}?>
									</select>
								</td>
								<td>
									<select  name="idgroup_diskon[]" <?=($r->diskon_rp>0?'':'disabled')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="0" <?=($r->idgroup_diskon==null?'selected':'')?>>Belum dipilih</option>
										<?foreach($list_group as $row){?>
											<option value="<?=$row->id?>" <?=($r->idgroup_diskon==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</td>
								<td style="display: none"><input type="text" readonly class="form-control" id="iddet_obat"  name="iddet_obat[]" value="<?=$r->id?>"></td>
							</tr>
							<?}?>
						</tbody>
					</table>
			</div>
		</div>
	</div>
	
	<div class="block-content">
		 <hr style="margin-top:10px">
		 <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<div class="col-xs-12 text-right">
						<a href="{base_url}tvalidasi_pendapatan_rajal/detail_rajal/<?=$idvalidasi?>/<?=$idhead?>" class="btn btn-default" type="reset"><i class="fa fa-reply"></i> Kembali</a>
	<?if ($disabel==''){?>
						<button class="btn btn-primary" name="btn_simpan" type="submit" value="1"><i class="fa fa-save"></i> Update</button>
	<?}?>
					</div>
				</div>
				
            </div>
        </div>
		<hr style="margin-bottom:10px">		
		
	</div>
</div>
<?php echo form_close(); ?>
<?
	$this->load->view('Tklaim_monitoring/modal_list');
?>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
  
});


function validate_final(){
	$("*[disabled]").not(true).removeAttr("disabled");
	return true;
}

</script>