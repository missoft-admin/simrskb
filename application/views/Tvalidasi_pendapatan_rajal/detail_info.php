<style>
table.fixed {
      table-layout: fixed;
      width: 100%;
    }
    table.fixed td {
      overflow: hidden;
    }
</style>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tvalidasi_pendapatan_rajal/save_detail_info','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
<div class="block">
	<div class="block-header">
       
       
        <h3 class="block-title">{title}</h3>
	</div>
	 <input type="hidden" readonly class="form-control" id="id" placeholder="No. Jurnal" name="id" value="{id}">
		<input type="hidden" readonly class="form-control" id="st_posting" placeholder="No. Jurnal" name="st_posting" value="{st_posting}">
		<input type="hidden" readonly class="form-control" id="tanggal_transaksi" placeholder="No. tanggal_transaksi" name="tanggal_transaksi" value="<?=YMDFormat($tanggal_transaksi)?>">
		<input type="hidden" readonly class="form-control" id="disabel" placeholder="No. Jurnal" name="disabel" value="{disabel}">
	<?if ($list_null){?>
	<div class="block-content">
		<div class="row">			
            <div class="col-md-12">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h3 class="font-w300 push-15">List No. Akun / Group Akun Belum ditentukan</h3>
					<?foreach($list_null as $row){?>
					<p><strong><?=$row->ket?></strong></p>
					<?}?>
				</div>
			</div>
		</div>
	</div>
	<?}?>
	<div class="block-content">
		<h3 class="block-title"> <?=text_primary('Detail Transaksi Header')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped fixed" id="index_header" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:8%"  class="text-center">No Pengajuan</th>								
								<th style="width:8%"  class="text-center">Tanggal</th>								
								<th style="width:8%"  class="text-center">Pemohon</th>								
								<th style="width:9%"  class="text-center">Nominal Diskon ALL</th>								
								<th style="width:20%"  class="text-center">Group Diskon ALL</th>								
								<th style="width:9%"  class="text-center">Nominal Round</th>								
								<th style="width:20%"  class="text-center">Group Round</th>								
							</tr>
						</thead>
						<tbody>
							<?
							$q="SELECT * FROM `tvalidasi_pendapatan_rajal_info_medis` H WHERE H.idvalidasi='$id' ORDER BY H.idtransaksi ASC";
							$list_header=$this->db->query($q)->result();
							?>
							<? foreach($list_header as $r){?>
							<tr>
								<td class="text-center"><?='<a href="{base_url}trm_pengajuan_informasi/update/'.$r->idtransaksi.'?isReadonly=true" target="_blank">'.$r->notransaksi.'</a>'?></td>
								<td class="text-center"><?=HumanDateShort($r->tanggal_transaksi)?></td>
								<td class="text-center"><?=($r->nama_pemohon)?></td>
								<td class="text-right"><?=number_format($r->nominal_diskon_all)?></td>
								<td>
									<select name="idgroup_diskon_all[]" <?=($r->nominal_diskon_all>0?'':'disabled')?> <?=$disabel?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" <?=($r->idgroup_diskon_all==null?'selected':'')?>>Belum dipilih</option>
									<?foreach($list_group as $row){?>
										<option value="<?=$row->id?>" <?=($r->idgroup_diskon_all==$row->id?'selected':'')?>><?=$row->nama?></option>
									<?}?>
									</select>
								</td>
								<td class="text-right"><?=number_format($r->nominal_round)?></td>
								<td>
									<select  name="idgroup_round[]" <?=($r->nominal_round>0?'':'disabled')?> <?=$disabel?>  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" <?=($r->idgroup_round==null?'selected':'')?>>Belum dipilih</option>
										<?foreach($list_group as $row){?>
											<option value="<?=$row->id?>" <?=($r->idgroup_round==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</td>
								<td style="display: none"><input type="text" readonly class="form-control" id="iddet_header"  name="iddet_header[]" value="<?=$r->id?>"></td>
							</tr>
							<?}?>
						</tbody>
					</table>
			</div>
		</div>
	</div>
	<div class="block-content">
		<h3 class="block-title"> <?=text_primary('Detail Transaksi')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped fixed" id="index_list" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:7%"  class="text-center">No Pengajuan</th>								
								<th style="width:10%"  class="text-center">Pasien</th>								
								<th style="width:8%"  class="text-center">Tanggal Kunjungan</th>								
								<th style="width:7%"  class="text-center">Jenis Kunjungan</th>								
								<th style="width:5%"  class="text-center">Dokter</th>								
								<th style="width:5%"  class="text-center">RS</th>								
								<th style="width:5%"  class="text-center">Diskon</th>								
								<th style="width:15%"  class="text-center">Group Dokter</th>	
								<th style="width:15%"  class="text-center">Group RS</th>	
								<th style="width:15%"  class="text-center">Group Diskon</th>	
							</tr>
						</thead>
						<tbody>
							<?
							$q="SELECT H.*,HH.idtransaksi as idtrx
								FROM `tvalidasi_pendapatan_rajal_info_medis_detail` H 
								LEFT JOIN tvalidasi_pendapatan_rajal_info_medis HH ON HH.id=H.idhead
								WHERE H.idvalidasi='$id' ORDER BY H.idtransaksi ASC";
							$list_header=$this->db->query($q)->result();
							?>
							<? foreach($list_header as $r){?>
							<tr>
								<td class="text-center"><?='<a href="{base_url}trm_pengajuan_informasi/update/'.$r->idtrx.'?isReadonly=true" target="_blank">'.$r->notransaksi.'</a>'?></td>
								<td class="text-left"><?=($r->no_medrec.'<br>'.$r->namapasien)?></td>
								<td class="text-center"><?=HumanDateShort($r->tanggal_kunjungan)?></td>
								<td class="text-center"><?=GetTipePasienPiutang($r->tujuan)?></td>
								<td class="text-right"><?=number_format($r->nominal_dokter)?></td>
								<td class="text-right"><?=number_format($r->nominal_rs)?></td>
								<td class="text-right"><?=number_format($r->diskon_rp)?></td>
								<td>
									<select name="idgroup_dokter[]" <?=($r->nominal_dokter>0?'':'disabled')?> <?=$disabel?>  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" <?=($r->idgroup_dokter==null?'selected':'')?>>Belum dipilih</option>
									<?foreach($list_group as $row){?>
										<option value="<?=$row->id?>" <?=($r->idgroup_dokter==$row->id?'selected':'')?>><?=$row->nama?></option>
									<?}?>
									</select>
								</td>
								<td>
									<select  name="idgroup_rs[]" <?=($r->nominal_rs>0?'':'disabled')?> <?=$disabel?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" <?=($r->idgroup_rs==null?'selected':'')?>>Belum dipilih</option>
										<?foreach($list_group as $row){?>
											<option value="<?=$row->id?>" <?=($r->idgroup_rs==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</td>
								<td>
									<select  name="idgroup_diskon[]" <?=($r->diskon_rp>0?'':'disabled')?> <?=$disabel?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" <?=($r->idgroup_diskon==null?'selected':'')?>>Belum dipilih</option>
										<?foreach($list_group as $row){?>
											<option value="<?=$row->id?>" <?=($r->idgroup_diskon==$row->id?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</td>
								<td style="display: none"><input type="text" readonly class="form-control" id="iddet_detail"  name="iddet_detail[]" value="<?=$r->id?>"></td>
							</tr>
							<?}?>
							
						</tbody>
					</table>
			</div>
		</div>
	</div>
	<div class="block-content">
		<h3 class="block-title"> <?=text_success('Detail Pembayaran')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped" id="index_bayar" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:5%"  class="text-center">NO </th>
								<th style="width:20%"  class="text-center">METODE</th>								
								<th style="width:20%"  class="text-center">NOMINAL</th>
								<th style="width:40%"  class="text-center">Group</th>								
							</tr>
						</thead>
						<tbody>
							<?
							$q="SELECT * FROM `tvalidasi_pendapatan_rajal_info_medis_bayar` H WHERE H.idvalidasi='$id' ORDER BY H.id ASC";
							$list_header=$this->db->query($q)->result();
							$no=1;
							?>
							<? foreach($list_header as $r){?>
							<tr>
								<td class="text-center"><?=($no)?></td>
								<td class="text-left"><?=($r->metode_nama.' '.$r->bank)?></td>
								<td class="text-right"><?=number_format($r->nominal_bayar)?></td>
								<td>
									<select name="idakun[]" <?=($r->nominal_bayar>0?'':'disabled')?> <?=$disabel?>  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" <?=($r->idakun==null?'selected':'')?>>Belum dipilih</option>
									<?foreach($list_akun as $row){?>
										<option value="<?=$row->id?>" <?=($r->idakun==$row->id?'selected':'')?>><?=$row->noakun.' - '.$row->namaakun?></option>
									<?}?>
									</select>
								</td>
								
								<td style="display: none"><input type="text" readonly class="form-control" id="iddet_bayar"  name="iddet_bayar[]" value="<?=$r->id?>"></td>
							</tr>
							<?
							$no=$no+1;
							}?>
						</tbody>
					</table>
			</div>
		</div>
	</div>
	<div class="block-content">
		<h3 class="block-title"> <?=text_danger('Ringkasan Akun')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped fixed" id="index_ringkasan" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:3%"  class="text-center">NO </th>
								<th style="width:15%"  class="text-center">NO AKUN</th>								
								<th style="width:30%"  class="text-center">AKUN</th>								
								<th style="width:20%"  class="text-center">DEBIT</th>								
								<th style="width:20%"  class="text-center">KREDIT</th>								
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
			</div>
		</div>
	</div>
	
	<?if ($disabel==''){?>
	<div class="block-content">
		 <hr style="margin-top:10px">
		 <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<div class="col-xs-12 text-right">
						<?if ($disabel==''){?>
							<button class="btn btn-primary" name="btn_simpan" type="submit" value="1"><i class="fa fa-save"></i> Update</button>
						<?}?>
						<a href="{base_url}tvalidasi_pendapatan_rajal" class="btn btn-default" type="reset"><i class="fa fa-reply"></i> Kembali</a>
					</div>
				</div>
				
            </div>
        </div>
		<hr style="margin-bottom:10px">		
		
	</div>
	<?}?>
</div>
<?php echo form_close(); ?>
<?
	$this->load->view('Tklaim_monitoring/modal_list');
?>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
   // load_detail();
   // load_bayar();
   load_ringkasan();
});
$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_detail();
});
function get_transaksi_detail($id){
	var id=$id;
	$("#modal_list_bayar").modal('show');
	get_pembayaran(id);
}


function validate_final(){
	$("*[disabled]").not(true).removeAttr("disabled");
	return true;
}
function load_ringkasan(){
	$("#cover-spin").show();
	var id=$("#id").val();
	var disabel=$("#disabel").val();
	var idhead='';
	$('#index_ringkasan tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_pendapatan_rajal/load_ringkasan/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel,idhead: idhead},
		success: function(data) {
			// console.log(data.tabel);
			$('#index_ringkasan').append(data.tabel);
			$(".opsi").select2({dropdownAutoWidth: false});
			$("#cover-spin").hide();
			// $(".tgl").datepicker();
			// $(".simpan").hide();
		}
	});
}

</script>