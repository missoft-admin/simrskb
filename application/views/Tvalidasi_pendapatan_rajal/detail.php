<style>
table.fixed {
      table-layout: fixed;
      width: 100%;
    }
    table.fixed td {
      overflow: hidden;
    }
</style>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tvalidasi_pendapatan_rajal/save_detail','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
<div class="block">
	<div class="block-header">
        <input type="hidden" readonly class="form-control" id="id" placeholder="No. Jurnal" name="id" value="{id}">
		<input type="hidden" readonly class="form-control" id="st_posting" placeholder="No. Jurnal" name="st_posting" value="{st_posting}">
		<input type="hidden" readonly class="form-control" id="tanggal_transaksi" placeholder="No. tanggal_transaksi" name="tanggal_transaksi" value="<?=YMDFormat($tanggal_transaksi)?>">
		<input type="hidden" readonly class="form-control" id="disabel" placeholder="No. Jurnal" name="disabel" value="{disabel}">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <div class="col-md-6">
                
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="">Tgl Pembayaran</label>
                    <div class="col-md-8">
                      <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
          							<input class="form-control" type="text" disabled autocomplete="off" name="tanggal_pembayaran_awal" placeholder="From" value="<?=HumanDateShort($tanggal_transaksi)?>">
          							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
          							<input class="form-control" type="text" disabled autocomplete="off" name="tanggal_pembayaran_akhir" placeholder="To" value="<?=HumanDateShort($tanggal_transaksi)?>">
          						</div>
        						</div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="">Tgl Pendaftaran</label>
                    <div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
          							<input class="form-control" type="text" autocomplete="off" id="tanggal_pendaftaran_awal" placeholder="From" value="">
          							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
          							<input class="form-control" type="text" autocomplete="off" id="tanggal_pendaftaran_akhir" placeholder="To" value="">
          				</div>
        			</div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="">Tipe</label>
                    <div class="col-md-8">
                      <select id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                        <option value="#" selected>Semua Tipe</option>
                          <option value="1">Poliklinik</option>
                          <option value="2">Instalasi Gawat Darurat (IGD)</option>
                          <option value="3">Obat Bebas</option>
                       
                      </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
          				<label class="col-md-4 control-label" for="idpoliklinik">Poliklinik</label>
          				<div class="col-md-8">
    					       <select id="idpoliklinik" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
          						<option value="#">Semua Poliklinik</option>
          						<?php foreach(get_all('mpoliklinik', array('status' => '1')) as $row){?>
    						        <option value="<?= $row->id?>" ><?= $row->nama?></option>
          						<?php }?>
          					</select>
          				</div>
          		</div>
                <div class="form-group" style="margin-bottom: 5px;">
        					<label class="col-md-4 control-label" for="nomedrec">No. Medrec</label>
        					<div class="col-md-8">
        						<input type="text" class="form-control" id="nomedrec" value="" placeholder="No. Medrec">
        					</div>
        				</div>
        				<div class="form-group" style="margin-bottom: 5px;">
        					<label class="col-md-4 control-label" for="namapasien">Nama Pasien</label>
        					<div class="col-md-8">
        						<input type="text" class="form-control" id="namapasien" value="" placeholder="Nama Pasien">
        					</div>
        				</div>
                
            </div>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                  <label class="col-md-4 control-label" for="idkelompokpasien">Kelompok Pasien</label>
                  <div class="col-md-8">
                    <select id="idkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." >
                      <option value="0">Semua Kelompok Pasien</option>
                      <?php foreach(get_all('mpasien_kelompok', array('status' => 1)) as $row) { ?>
                        <option value="<?=$row->id?>" ><?=$row->nama?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                  <label class="col-md-4 control-label" for="">Perusahaan</label>
                  <div class="col-md-8">
                    <select id="idrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                      <option value="#" selected>Semua Perusahaan</option>
                      <?php foreach (get_all('mrekanan') as $row) {?>
                      <option value="<?= $row->id?>" ><?= $row->nama?></option>
                      <?php }?>
                    </select>
                  </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                  <label class="col-md-4 control-label" for="iddokter">User Transaksi</label>
                  <div class="col-md-8">
                    <select id="iduser" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                      <option value="#" selected>Semua User</option>
                      <?php foreach (get_all('musers', array('st_kasir' => 1, 'status' => 1)) as $row){ ?>
                          <option value="<?= $row->id?>"><?= $row->name?></option>
                      <?php }?>
                    </select>
                  </div>
                </div>
                
                <div class="form-group" style="margin-bottom: 5px;">
                  <label class="col-md-4 control-label" for="nopendaftaran">No. Pendaftaran</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" id="nopendaftaran" value="" placeholder="No. Pendaftaran">
                  </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                  <label class="col-md-4 control-label" for="nokasir">No. Kasir</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" id="nokasir" value="" placeholder="No. Kasir">
                  </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
            
        </div>
	</div>
	<?if ($jml_sisa_verifikasi){?>
	<div class="block-content">
		<hr style="margin-top:10px">
		<div class="row">			
            <div class="col-md-12">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h3 class="font-w300 push-15">Informasi</h3>
					<p><strong><?=$jml_sisa_verifikasi?> TRANSAKSI BELUM DIVERIFIKASI</strong></p>
				</div>
			</div>
		</div>
	</div>
	<?}?>
	<div class="block-content">
		<h3 class="block-title"> <?=text_primary('Detail Transaksi')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped fixed" id="index_list" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:15%"  class="text-center">Tanggal</th>								
								<th style="width:8%"  class="text-center">No Pendaftaran</th>								
								<th style="width:8%"  class="text-center">Tipe</th>								
								<th style="width:9%"  class="text-center">No. Medrec</th>								
								<th style="width:10%"  class="text-center">Pasien</th>								
								<th style="width:9%"  class="text-center">Nominal</th>								
								<th style="width:10%"  class="text-center">User Transaksi</th>	
								<th style="width:9%"  class="text-center">User Verifikasi</th>								
								<th style="width:5%"  class="text-center">AKSI</th>								
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
			</div>
		</div>
	</div>
	<div class="block-content">
		<h3 class="block-title"> <?=text_success('Detail Pembayaran')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped" id="index_bayar" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:3%"  class="text-center">NO </th>
								<th style="width:40%"  class="text-center">METODE</th>								
								<th style="width:40%"  class="text-center">NOMINAL</th>
								<th style="width:20%"  class="text-center">AKSI</th>								
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
			</div>
		</div>
	</div>
	<div class="block-content">
		<h3 class="block-title"> <?=text_success('OTHER INCOME / OTHER LOSSS')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped" id="index_other" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:3%"  class="text-center">NO </th>
								<th style="width:20%"  class="text-center">JENIS</th>								
								<th style="width:15%"  class="text-center">NOMINAL</th>
								<th style="width:27%"  class="text-center">DEBIT</th>
								<th style="width:27%"  class="text-center">KREDIT</th>
								<th style="width:5%"  class="text-center">AKSI</th>								
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
			</div>
		</div>
	</div>
	<div class="block-content">
		<h3 class="block-title"> <?=text_danger('Ringkasan Akun')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped fixed" id="index_ringkasan" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:3%"  class="text-center">NO </th>
								<th style="width:15%"  class="text-center">NO AKUN</th>								
								<th style="width:30%"  class="text-center">AKUN</th>								
								<th style="width:20%"  class="text-center">DEBIT</th>								
								<th style="width:20%"  class="text-center">KREDIT</th>								
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
			</div>
		</div>
	</div>
	
	<?if ($disabel==''){?>
	<div class="block-content">
		 <hr style="margin-top:10px">
		 <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<div class="col-xs-12 text-right">
						<a href="{base_url}tvalidasi_pendapatan_rajal" class="btn btn-default" type="reset"><i class="fa fa-reply"></i> Kembali</a>
					</div>
				</div>
				
            </div>
        </div>
		<hr style="margin-bottom:10px">		
		
	</div>
	<?}?>
</div>
<?php echo form_close(); ?>
<?
	$this->load->view('Tklaim_monitoring/modal_list');
?>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
   load_detail();
   load_bayar();
   load_other();
   load_ringkasan();
});
$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_detail();
});
function get_transaksi_detail($id){
	var id=$id;
	$("#modal_list_bayar").modal('show');
	get_pembayaran(id);
}
$(document).on("click", ".list_bayar", function() {
	
});
$(document).on("click", ".btn_simpan_other", function() {
	let tr=$(this).closest('tr');
	let idtrx=tr.find(".iddet_other").val();
	let idakun_debet=tr.find(".idakun_debet").val();
	let idakun_kredit=tr.find(".idakun_kredit").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tvalidasi_pendapatan_rajal/update_other/',
		dataType: "json",
		type: "POST",
		data: {id: idtrx,idakun_debet: idakun_debet,idakun_kredit: idakun_kredit},
		success: function(data) {
			$.toaster({priority : 'success', title : 'Succes!', message : ' Edit Finish'});
			location.reload();
		}
	});
	// alert(idtrx+' '+idakun_debet);
});
function load_detail(){
	// var nojurnal=$("#nojurnal").val();
	var tanggal_pendaftaran_awal=$("#tanggal_pendaftaran_awal").val();
	var tanggal_pendaftaran_akhir=$("#tanggal_pendaftaran_akhir").val();
	var idtipe=$("#idtipe").val();
	var idpoliklinik=$("#idpoliklinik").val();
	var nomedrec=$("#nomedrec").val();
	var namapasien=$("#namapasien").val();
	var idkelompokpasien=$("#idkelompokpasien").val();
	var idrekanan=$("#idrekanan").val();
	var iduser=$("#iduser").val();
	var nopendaftaran=$("#nopendaftaran").val();
	var nokasir=$("#nokasir").val();
	
	var id=$("#id").val();
	var disabel=$("#disabel").val();
	// alert(tanggalterima1);
	table = $('#index_list').DataTable({
		autoWidth: false,
		searching: true,
		pageLength: 10,
		serverSide: true,
		"processing": true,
		"order": [],
		"ordering": false,
		columnDefs: [
						// { "targets": [0,1], "visible": false },
						{ "width": "10%", "targets": [0,1,2,3,5,6,7] },
						{ "width": "15%", "targets": [4,8] },
						{"targets": [5], className: "text-right" },
						{"targets": [0,1,2,3,6,7,8], className: "text-center" }
						
					 ],
		ajax: { 
			url: '{site_url}tvalidasi_pendapatan_rajal/load_detail', 
			type: "POST" ,
			dataType: 'json',
			data : {
					id:id,
					disabel:disabel,
					tanggal_pendaftaran_awal:tanggal_pendaftaran_awal,
					tanggal_pendaftaran_akhir:tanggal_pendaftaran_akhir,
					idtipe:idtipe,
					idpoliklinik:idpoliklinik,
					nomedrec:nomedrec,
					namapasien:namapasien,
					idkelompokpasien:idkelompokpasien,
					idrekanan:idrekanan,
					iduser:iduser,
					nopendaftaran:nopendaftaran,
					nokasir:nokasir,
				   }
		}
	});
}
function load_bayar(){
	$("#cover-spin").show();
	var id=$("#id").val();
	var disabel=$("#disabel").val();
	$('#index_bayar tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_pendapatan_rajal/load_bayar/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel},
		success: function(data) {
			// console.log(data.tabel);
			$('#index_bayar').append(data.tabel);
			$(".opsi").select2({dropdownAutoWidth: false});
			$("#cover-spin").hide();
			// $(".tgl").datepicker();
			// $(".simpan").hide();
		}
	});
}
function load_other(){
	$("#cover-spin").show();
	var id=$("#id").val();
	var disabel=$("#disabel").val();
	$('#index_other tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_pendapatan_rajal/load_other/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel},
		success: function(data) {
			// console.log(data.tabel);
			$('#index_other').append(data.tabel);
			$(".js-select2").select2({dropdownAutoWidth: false});
			$("#cover-spin").hide();
			// $(".tgl").datepicker();
			// $(".simpan").hide();
		}
	});
}
function validate_final(){
	$("*[disabled]").not(true).removeAttr("disabled");
	return true;
}
function load_ringkasan(){
	$("#cover-spin").show();
	var id=$("#id").val();
	var disabel=$("#disabel").val();
	var idhead='';
	$('#index_ringkasan tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_pendapatan_rajal/load_ringkasan/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel,idhead: idhead},
		success: function(data) {
			// console.log(data.tabel);
			$('#index_ringkasan').append(data.tabel);
			$(".opsi").select2({dropdownAutoWidth: false});
			$("#cover-spin").hide();
			// $(".tgl").datepicker();
			// $(".simpan").hide();
		}
	});
}

</script>