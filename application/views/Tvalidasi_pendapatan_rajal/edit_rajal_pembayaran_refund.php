<style>
table.fixed {
      table-layout: fixed;
      width: 100%;
    }
    table.fixed td {
      overflow: hidden;
    }
</style>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tvalidasi_pendapatan_rajal/save_edit_rajal_pembayaran_refund','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
			<div class="col-md-6">
				<div class="input-group" style="width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">No. Transaksi</span>
					<input type="text" class="form-control tindakanNoPendaftaran" value="{notransaksi}" readonly="true">
					<input type="hidden" class="form-control" id="idtipe" name="idtipe" value="{idtipe}" readonly="true">
					<input type="hidden" class="form-control" id="idhead" name="idhead" value="{idhead}" readonly="true">
					<input type="hidden" class="form-control" id="idvalidasi" name="idvalidasi" value="{idvalidasi}" readonly="true">
				</div>
				<div class="input-group" style="margin-top:5px;width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">No. Medrec</span>
					<input type="text" class="form-control tindakanNoMedrec" value="{no_medrec}" readonly="true">
				</div>
				<div class="input-group" style="margin-top:5px; width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">Nama Pasien</span>
					<input type="text" class="form-control tindakanNamaPasien" value="{namapasien}" readonly="true">
				</div>
				
			</div>
			<div class="col-md-6">
				<div class="input-group" style="width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">Kelompok Pasien</span>
					<input type="text" class="form-control tindakanKelompokPasien" value="{namakelompok}" readonly="true">
				</div>
				
        <div class="input-group" style="margin-top:5px;width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">User Transaksi</span>
					<input type="text" class="form-control tindakanUserTransaksi" value="{nama_user}" readonly="true">
				</div>
        <div class="input-group" style="margin-top:5px;width:100%">
					<span class="input-group-addon" style="width:150px; text-align:left;">Tanggal Transaksi</span>
					<input type="text" class="form-control tindakanTanggalTransaksi" value="{tanggal_transaksi}" readonly="true">
				</div>
					
					
			</div>
		</div>
	</div>
	
	<div class="block-content">
		<h3 class="block-title"> <?=text_primary('PEMBAYARAN TUNAI')?></h3>
		   <hr style="margin-top:10px">
		<div class="row">
			
            <div class="col-md-12">
					<table class="table table-bordered table-striped" id="index_tunai" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:15%"  class="text-center">METODE</th>
								<th style="width:8%"  class="text-right">NOMINAL</th>
								<th style="width:8%"  class="text-center">NO. AKUN</th>
								<th style="display: none">ID</th>
							</tr>
						</thead>
						<tbody>
							<? foreach($list_tunai as $r){?>
							<tr>
								<td class="text-left"><?=$r->metode_nama?></td>
								<td class="text-right"><?=number_format($r->nominal)?></td>
								<td>
									<select name="idakun[]" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="0" <?=($r->idakun==null?'selected':'')?>>Belum dipilih</option>
									<?foreach($list_akun as $row){?>
										<option value="<?=$row->id?>" <?=($r->idakun==$row->id?'selected':'')?>><?=$row->namaakun.'-'.$row->namaakun?></option>
									<?}?>
									</select>
								</td>
								
								<td style="display: none"><input type="text" readonly class="form-control"  name="iddet_tunai[]" value="<?=$r->id?>"></td>
							</tr>
							<?}?>
						</tbody>
					</table>
			</div>
		</div>
	</div>
	
	<div class="block-content">
		 <hr style="margin-top:10px">
		 <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<div class="col-xs-12 text-right">
						<a href="{base_url}tvalidasi_pendapatan_rajal/detail_refund_rajal/<?=$idhead?>/<?=$idvalidasi?>" class="btn btn-default" type="reset"><i class="fa fa-reply"></i> Kembali</a>
	<?if ($disabel==''){?>
						<button class="btn btn-primary" name="btn_simpan" type="submit" value="1"><i class="fa fa-save"></i> Update</button>
	<?}?>
					</div>
				</div>
				
            </div>
        </div>
		<hr style="margin-bottom:10px">		
		
	</div>
</div>
<?php echo form_close(); ?>
<?
	$this->load->view('Tklaim_monitoring/modal_list');
?>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
  
});


function validate_final(){
	$("*[disabled]").not(true).removeAttr("disabled");
	return true;
}

</script>