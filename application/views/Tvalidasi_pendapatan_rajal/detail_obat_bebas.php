<?=ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}
?>

<style media="screen">
.text-bold {
    font-weight: bold;
}

.separator-header {
    margin-top: 450px;
}

.modal {
    overflow: auto !important;
}

</style>

<div class="block">
    <div class="block-header">
        
        <h3 class="block-title">{title} </h3>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-md-6">
                <div class="input-group" style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Tanggal Transaksi</span>
                    <input type="text" class="form-control tindakanTanggalTransaksi" value="{tanggaltransaksi}" readonly="true">
					<input type="hidden" class="form-control" id="idhead" name="idhead" value="{idhead}" readonly="true">
					<input type="hidden" class="form-control" id="idvalidasi" name="idvalidasi" value="{idvalidasi}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No. Transaksi</span>
                    <input type="text" class="form-control tindakanNoTransaksi" value="{notransaksi}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Resep</span>
                    <input type="text" class="form-control tindakanStatusResep" value="{statusresep}" readonly="true">
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group" style="width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Kategori</span>
                    <input type="text" class="form-control tindakanKategori" value="{kategori}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px;width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">No. Medrec</span>
                    <input type="text" class="form-control tindakanNoMedrec" value="{nomedrec}" readonly="true">
                </div>
                <div class="input-group" style="margin-top:5px; width:100%">
                    <span class="input-group-addon" style="width:150px; text-align:left;">Nama Pasien</span>
                    <input type="text" class="form-control tindakanNamaPasien" value="{namapasien}" readonly="true">
                </div>
            </div>
        </div>

        <div class="content-verification">

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN FARMASI</span></b>
            <b><span class="label label-default" style="font-size:12px">OBAT</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Obat</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Unit Pelayanan</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalFarmasiObat = 0;?>
					
                    <?php $dataFarmasiAlkesIGD = $this->model->ViewRincianFarmasiNonRacikan($idhead, 3);?>
                    <?php foreach ($dataFarmasiAlkesIGD as $row) {?>
                    <?php
                    $statusEdit = '1';
                    
                    ?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namabarang?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none"><?=$row->idunit?></td>
                        <td>Farmasi</td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_rajal/edit_farmasi_nr/<?=$idhead?>/3/ob" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php $totalFarmasiObat = $totalFarmasiObat + $row->totalkeseluruhan; ?>
                    <?php }?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalFarmasiObat)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN FARMASI</span></b>
            <b><span class="label label-default" style="font-size:12px">OBAT RACIKAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Obat</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Unit Pelayanan</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalFarmasiObatRacikan = 0;$statusEdit='1'?>
                    <?php foreach ($this->model->ViewRincianFarmasiRacikan($idhead) as $row) { ?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namabarang?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none">1</td>
                        <td>Farmasi</td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td><a href="{base_url}tvalidasi_pendapatan_rajal/edit_farmasi_racikan/<?=$idhead?>/<?=$row->iddetail?>/ob" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a></td>
                    </tr>
                    <?php $totalFarmasiObatRacikan = $totalFarmasiObatRacikan + $row->totalkeseluruhan; ?>
                    <?php }?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalFarmasiObatRacikan)?></td>
                    </tr>
                </tfoot>
            </table>

            <hr>

            <b><span class="label label-success" style="font-size:12px">TINDAKAN FARMASI</span></b>
            <b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
            <table class="table table-bordered table-striped" style="margin-top: 10px;">
                <thead>
                    <tr>
                        <th style="width:10%">No. Transaksi</th>
                        <th style="width:10%">Tanggal</th>
                        <th style="width:10%">Alat Kesehatan</th>
                        <th style="width:10%">Tarif</th>
                        <th style="width:5%">Kuantitas</th>
                        <th style="width:10%">Jumlah</th>
                        <th style="width:5%">Diskon (%)</th>
                        <th style="width:10%">Jumlah Setelah Diskon</th>
                        <th style="width:10%">Unit Pelayanan</th>
                        <th style="width:5%">Status Edit</th>
                        <th style="width:20%" class="editOnly">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalFarmasiAlkes = 0;?>
					<?php $dataFarmasiAlkesIGD = $this->model->ViewRincianFarmasiNonRacikan($idhead, 1);?>
                    <?php foreach ($dataFarmasiAlkesIGD as $row) {?>
                    <?php
                    $statusEdit = '1';                   
                    ?>
                    <?php $subtotal = $row->hargajual * $row->kuantitas ?>
                    <tr>
                        <td><?=$row->nopendaftaran?></td>
                        <td><?=DMYFormat($row->tanggal)?></td>
                        <td><?=$row->namabarang?></td>
                        <td style="display: none"><?=number_format($row->hargadasar)?></td>
                        <td style="display: none"><?=number_format($row->margin)?></td>
                        <td><?=number_format($row->hargajual)?></td>
                        <td><?=number_format($row->kuantitas)?></td>
                        <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
                        <td hidden><?=number_format($row->diskon, 2)?></td>
                        <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
                        <td><?=number_format($row->totalkeseluruhan)?></td>
                        <td style="display: none">1</td>
                        <td>Farmasi</td>
                        <td><?=StatusEditVerifikasi($statusEdit)?></td>
                        <td class="editOnly">
                           <a href="{base_url}tvalidasi_pendapatan_rajal/edit_farmasi_nr/<?=$idhead?>/1/ob" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
                        </td>
                    </tr>
                    <?php $totalFarmasiAlkes = $totalFarmasiAlkes + $row->totalkeseluruhan; ?>
                    <?php }?>
                </tbody>
                <tfoot>
                    <tr>
                        <td align="center" colspan="7"><b>TOTAL</b></td>
                        <td class="text-bold" colspan="4"><?=number_format($totalFarmasiAlkes)?></td>
                    </tr>
                </tfoot>
            </table>

            <?php $totalKeseluruhan = $totalFarmasiObat + $totalFarmasiObatRacikan + $totalFarmasiAlkes; ?>

            <hr>

            <table class="table table-bordered table-striped" style="margin-top: 20px;">
                <thead>
                    <tr>
                        <td align="right" style="font-size:15px"><b>TOTAL KESELURUHAN</b></td>
                        <td align="left" class="text-bold" id="grandTotal" align="center" style="font-size:15px"><?=number_format($totalKeseluruhan)?></td>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<div id="transaction-only">
	<div class="block">
		<div class="block-content">
			<h5 style="margin-bottom: 10px;">Pembayaran</h5>
			
			<table id="cara_pembayaran" class="table table-bordered table-striped" style="margin-top: 20px;">
				<tbody>
					<tr>
						<th style="width: 85%;" colspan="5"><label class="pull-right"><b>SUB TOTAL Rp.</b></label></th>
						<th style="width: 15%;" colspan="2"><b>
							<input class="form-control input-sm" readonly type="text" id="gt_all" name="gt_all" value="<?=number_format($totalKeseluruhan)?>">
						</th>
					</tr>
					<tr>
						<th style="width: 85%;" colspan="5"><label class="pull-right"><b>DISKON</b></label></th>
						<th style="width: 15%;" colspan="2"><b>
							<div class="input-group">
								<span class="input-group-addon">Rp</span>
								<input class="form-control" type="text" id="diskon_rp" disabled name="diskon_rp" placeholder="Discount Rp" value="{diskon_rp}">
							</div>
							<div class="input-group">
								<span class="input-group-addon"> %. </span>
								<input class="form-control" type="text" id="diskon_persen" disabled name="diskon_persen" placeholder="Discount %"  value="{diskon_persen}">
							</div>
						</th>
					</tr>
					<tr>
						<th style="width: 85%;" colspan="5"><label class="pull-right"><b>TOTAL Rp.</b></label></th>
						<th style="width: 15%;" colspan="2"><b>
							<input class="form-control input-sm" readonly type="text" id="gt_rp" name="gt_rp" value="<?=number_format($totalKeseluruhan)?>">
						</th>
					</tr>
					
				</tbody>
			</table>
		
			<div class="row">
				<div class="col-sm-12">
					<div class="progress progress-mini">
						<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
					</div>
					<div class="control-group">
						<div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0; margin-bottom:20px">
							<table id="manage_tabel_pembayaran" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 5%;">#</th>
										<th style="width: 25%;">Jenis Pembayaran</th>
										<th style="width: 35%;">Keterangan</th>
										<th style="width: 10%;">Nominal</th>
										<th style="width: 10%;" class="editOnly">Aksi</th>
									</tr>
								</thead>
								<input type="hidden" id="rowindex">
								<input type="hidden" id="nomor">
								<tbody>
									<?php 
										$no = 1;
										$bayar_rp = 0;
									
									?>
									<?php foreach ($detail_pembayaran as $row) {
										$bayar_rp=$bayar_rp + $row->nominal;
										?>
										<tr>
											<td><?=$no++?></td>
											<td><?=metodePembayaran($row->idmetode)?></td>
											<td><?=$row->metode_nama?></td>
											<td><?=number_format($row->nominal)?></td>
											
											<td class="editOnly">
												<a href="{base_url}tvalidasi_pendapatan_rajal/edit_rajal_pembayaran/<?=$row->idhead?>/<?=$row->ref_id?>/ob" class="btn btn-sm btn-primary"><i class="fa fa-list-alt"></i></a>
											</td>
										</tr>
									<?php }?>
								</tbody>
								<tfoot id="foot_pembayaran">
									<tr class="bg-light dker">
										<th colspan="3"><label class="pull-right"><b>Pembayaran Rp.</b></label></th>
										<th colspan="2">
												<input type="text" class="form-control input-sm number" readonly id="bayar_rp" name="bayar_rp" value="<?=$bayar_rp?>">
										</th>
									</tr>
									
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
  	
	</div>
	<div class="block">
		<div class="block-content">
			<h5 style="margin-bottom: 10px;"><?=text_danger('RINGKASAN AKUN')?></h5>
			
			<div class="row">
				<div class="col-sm-12">
					<div class="progress progress-mini">
						<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
					</div>
					<div class="control-group">
						<div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0; margin-bottom:20px">
							<table class="table table-bordered table-striped fixed" id="index_ringkasan" style="width: 100%;">
								<thead>
									<tr>
										<th style="width:3%"  class="text-center">NO </th>
										<th style="width:15%"  class="text-center">NO AKUN</th>								
										<th style="width:30%"  class="text-center">AKUN</th>								
										<th style="width:20%"  class="text-center">DEBIT</th>								
										<th style="width:20%"  class="text-center">KREDIT</th>								
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
  	
	</div>
</div>

<input type="hidden" id="rowindex" value="">
<input type="hidden" id="nomor" value="">
<input type="hidden" id="pos_tabel_pembayaran" name="pos_tabel_pembayaran">
<input type="hidden" id="idpendaftaran" value="{idpendaftaran}">

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('.number').number(true, 0, '.', ',');
    $('.discount').number(true, 2, '.', ',');
      load_ringkasan();

    var statusEdit = "<?=$stedit;?>";
    if (statusEdit == "0") {
        $(".editOnly").hide();
        $("input, select").prop('disabled', true);
    }

    $(".time-datepicker").datetimepicker({
        format: "HH:mm:ss"
    });

});
function load_ringkasan(){
		$("#cover-spin").show();
		var id=$("#idvalidasi").val();
		var idhead=$("#idhead").val();
		var disabel=$("#disabel").val();
		$('#index_ringkasan tbody').empty();
		$.ajax({
			url: '{site_url}tvalidasi_pendapatan_rajal/load_ringkasan/',
			dataType: "json",
			type: "POST",
			data: {id: id,disabel: disabel,idhead: idhead},
			success: function(data) {
				// console.log(data.tabel);
				$('#index_ringkasan').append(data.tabel);
				$(".opsi").select2({dropdownAutoWidth: false});
				$("#cover-spin").hide();
				// $(".tgl").datepicker();
				// $(".simpan").hide();
			}
		});
	}

</script>
