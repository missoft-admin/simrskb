<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<style media="screen">
#detail_list input.form-control,
#detail_list textarea.form-control {
    border: 1px solid #a9a9a9;
}

</style>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}trujukan_laboratorium/index" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?php echo form_open('trujukan_laboratorium/save_review', 'class="form-horizontal push-10-t" id="form-work"') ?>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <label class="col-md-2" for="tglpendaftaran" style="margin-top: 5px;">Tanggal</label>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-6">
                            <input tabindex="2" type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" name="tanggalrujukan" value="{tanggalrujukan}" required>
                        </div>
                        <div class="col-md-6">
                            <input tabindex="3" type="text" class="time-datepicker form-control" name="wakturujukan" value="{wakturujukan}" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <label class="col-md-4">No. Rujukan</label>
                <div class="col-md-8">
                    <label><i><?=$norujukan?></i></label>
                </div>
            </div>
        </div>
        <br>
        <br>

        <hr>

        <div class="col-sm-6 col-lg-6">
            <div class="block block-rounded block-bordered">
                <div class="block-header">
                    <h3 class="block-title">Data Pasien</h3>
                </div>
                <div class="block-content">
                    <table class="table">
                        <tr>
                            <th width="30%">No. Medrec</th>
                            <td>
                                <div class="col-sm-12">
                                    <input class="form-control input-sm" type="text" value="<?=$nomedrec?>" readonly="readonly">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Nama Pasien</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="<?=$namapasien?>">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Jenis Kelamin</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="<?=$jeniskelamin?>">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Alamat</th>
                            <td>
                                <div class="col-sm-12">
                                    <textarea class="form-control input-sm" readonly="readonly"><?=$alamat?></textarea>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tanggal Lahir</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="<?=$tanggallahir?>">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Umur</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="<?=$umur_tahun . ' Tahun ' . $umur_bulan . ' Bulan ' . $umur_hari . ' Hari'?>">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Dokter Perujuk</th>
                            <td>
                                <div class="col-sm-12">
                                    <select id="iddokterperujuk" name="iddokterperujuk" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                        <option value="#">Pilih Opsi</option>
                                        <?php foreach ($list_dokterperujuk as $row) { ?>
                                        <option value="<?php echo $row->id; ?>" <?=($iddokterperujuk == $row->id ? 'selected' : '')?>><?php echo $row->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-6">
            <div class="block block-rounded block-bordered">
                <div class="block-content">
                    <table class="table">
                        <tr>
                            <th>Kelompok Pasien</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{namakelompok}">
                                </div>
                            </td>
                        </tr>
                        <?php if ($idkelompokpasien == 1) { ?>
                        <tr>
                            <th>Asuransi</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{namarekanan}">
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php if ($idkelompokpasien == 3) { ?>
                        <tr>
                            <th>No. INACBG</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{kodebpjskesehatan} - {tarif_bpjs_kesehatan}">
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php if ($idkelompokpasien == 4) { ?>
                        <tr>
                            <th>No. Paket</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{idtarif_bpjstk} - {tarif_bpjstk}">
                                </div>
                            </td>
                        </tr>
                        <?php } ?>

                        <?php if ($asalrujukan == 3) { ?>
                        <tr>
                            <th> - Kelas</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly value="<?=$kelas?>">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th> - Bed</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly value="<?=$bed?>">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th> - DPJP</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly value="<?=$namadokterdpjp?>">
                                </div>
                            </td>
                        </tr>
                        <?php } ?>

                        <?php if ($asalrujukan == 1) { ?>
                        <tr>
                            <th> - Tipe</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly value="<?=JenisTransaksi($idtipe)?>">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th> - Poliklinik</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly value="<?=$namapoliklinik?>">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th> - Dokter</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly value="<?=$namadokterpoliklinik?>">
                                </div>
                            </td>
                        </tr>
                        <?php } ?>

                        <tr>
                            <th>Dokter Laboratorium</th>
                            <td>
                                <div class="col-sm-12">
                                    <select name="iddokterlaboratorium" id="iddokterlaboratorium" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                        <option value="#">Pilih Opsi</option>
                                        <?php foreach ($list_dokterlaboratorium as $row) { ?>
                                        <option value="<?=$row->id?>" <?=($iddokterlaboratorium == $row->id ? 'selected' : '')?>><?= $row->nama ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>

                        <!-- Petugas Lab -->
                        <tr>
                            <th>Petugas Pengambil Sample</th>
                            <td>
                                <div class="col-sm-12">
                                    <select name="idpengambilsample" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                        <option value="#">Pilih Opsi</option>
                                        <?php foreach ($list_pegawai as $row) { ?>
                                        <option value="<?=$row->id?>" <?=($idpengambilsample == $row->id ? 'selected' : '')?>><?= $row->nama ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tanggal Pengambil Sample</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="tanggal_pengambilan_sample" value="{tanggal_pengambilan_sample}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Petugas Penguji Sample</th>
                            <td>
                                <div class="col-sm-12">
                                    <select name="idpengujisample" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                        <option value="#">Pilih Opsi</option>
                                        <?php foreach ($list_pegawai as $row) { ?>
                                        <option value="<?=$row->id?>" <?=($idpengujisample == $row->id ? 'selected' : '')?>><?= $row->nama ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tanggal Pengujian Sample</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="tanggal_pengujian_sample" value="{tanggal_pengujian_sample}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Petugas Pengirim Hasil</th>
                            <td>
                                <div class="col-sm-12">
                                    <select name="idpengirimhasil" id="idpengirimhasil" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                        <option value="#">Pilih Opsi</option>
                                        <?php foreach ($list_pegawai as $row) { ?>
                                        <option value="<?=$row->id?>" <?=($idpengirimhasil == $row->id ? 'selected' : '')?>><?= $row->nama ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tanggal Pengiriman Hasil</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control tanggaljam" name="tanggal_pengiriman_hasil" value="{tanggal_pengiriman_hasil}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Petugas Penerima Hasil</th>
                            <td>
                                <div class="col-sm-12">
                                    <select name="idpenerimahasil" id="idpenerimahasil" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                        <option value="#">Pilih Opsi</option>
                                        <?php foreach ($list_pegawai as $row) { ?>
                                        <option value="<?=$row->id?>" <?=($idpenerimahasil == $row->id ? 'selected' : '')?>><?= $row->nama ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tanggal Penerimaan Hasil</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control tanggaljam" name="tanggal_penerimaan_hasil" value="{tanggal_penerimaan_hasil}">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="control-group">
                    <div class="col-md-12">
                        <div class="progress progress-mini">
                            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                        </div>
                        <table class="table table-striped table-bordered" id="detail_list" style="margin-bottom:0px">
                            <thead>
                                <tr>
                                    <th>Nama Tindakan</th>
                                    <th>Hasil</th>
                                    <th>Satuan</th>
                                    <th>Nilai Normal</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($list_tindakan as $row) { ?>
                                <?php if ($row->idkelompok == 1) { ?>
                                <tr>
                                    <td colspan="5" style="background-color: #ff980014; font-size: 14px;"><b><?=$row->nama ?></b></td>
                                </tr>
                                <?php } else { ?>
                                <?php
									$satuanNilaiNormal = $this->Trujukan_laboratorium_model->getListSatuanNilaiNormal($row->idlaboratorium);
									$satuan = isset($satuanNilaiNormal->singkatansatuan) ? $satuanNilaiNormal->singkatansatuan : '0';
									$nilainormal = isset($satuanNilaiNormal->nilainormal) ? $satuanNilaiNormal->nilainormal : '';
								?>

                                <tr class="tbody-data" style="background-color: #f5f5f5;">
                                    <td><?=$row->nama ?></td>
                                    <td><input type="text" class="form-control" value="<?=$row->hasil ?>"></td>
                                    <td><select class="js-select2 form-control" style="width: 100%;">
                                            <option value="0" selected>Pilih Satuan</option>
                                            <?php foreach (get_all('msatuan', ['status' => 1]) as $item) { ?>
                                            <?php $selected = ($item->singkatan == (isset($row->satuan) ? $row->satuan : $satuan) ? 'selected' : ''); ?>
                                            <option value="<?= $item->singkatan; ?>" <?= $selected; ?>><?= $item->nama ?></option>
                                            <?php } ?>
                                        </select></td>
                                    <td><textarea class="form-control summernote-nilainormal"><?= isset($row->nilainormal) ? $row->nilainormal : $nilainormal ?></textarea></td>
                                    <td><textarea class="form-control" rows="5"><?=$row->keterangan ?></textarea></td>
                                    <td style="display: none;"><?=$row->id ?></td>
                                </tr>
                                <?php } ?>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>Catatan</td>
                                    <td colspan="4">
                                        <textarea class="form-control js-summernote" name="catatan">{catatan}</textarea>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="text-right bg-light lter">
            <button class="btn btn-info" type="submit">Simpan</button>
            <a href="{base_url}trujukan_laboratorium/index" class="btn btn-default" type="button">Batal</a>
        </div>
        <input type="hidden" name="stedit" value="{stedit}">
        <input type="hidden" id="review-value" name="review-value">
        <br><br>
        <?php echo form_hidden('id', $id); ?>
        <?php echo form_close() ?>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $(".time-datepicker").datetimepicker({
        format: "HH:mm:ss"
    });

    // @set format tanggal
    $('.tanggaljam').datetimepicker({
        format: 'DD/MM/YYYY HH:mm'
    });

    $('.summernote-nilainormal').summernote({
        toolbar: false,
        height: 100,
    });

    $("#form-work").submit(function(e) {
        var form = this;

        if ($("#iddokterlaboratorium").val() == "#") {
            e.preventDefault();
            sweetAlert("Maaf...", "Dokter Laboratorium Belum Dipilih!", "error");
            return false;
        }

        var tindakan_tbl = $('table#detail_list tbody tr.tbody-data').get().map(function(row) {
            return $(row).find('td').get().map(function(cell) {
                if ($(cell).context.firstChild) {
                    if ($(cell).context.firstChild.nodeName == "INPUT") {
                        return $(cell).find("input").val();
                    } else if ($(cell).context.firstChild.nodeName == "SELECT") {
                        return $(cell).find("select option:selected").val();
                    } else if ($(cell).context.firstChild.nodeName == "TEXTAREA") {
                        return $(cell).find("textarea").val();
                    } else {
                        return $(cell).html();
                    }
                }
            });
        });

        $("#review-value").val(JSON.stringify(tindakan_tbl));

        swal({
            title: "Berhasil!",
            text: "Proses penyimpanan data.",
            type: "success",
            timer: 1500,
            showConfirmButton: false
        });
    });
});

</script>
