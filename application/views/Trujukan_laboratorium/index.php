<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('1013'))){ ?>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <hr style="margin-top:0px">
        <div class="row">
            <?php echo form_open('trujukan_laboratorium/filter','class="form-horizontal" id="form-work"') ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="nomedrec">No. Medrec</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="nomedrec" value="{nomedrec}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="namapasien">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="namapasien" value="{namapasien}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="idasalpasien">Asal Pasien</label>
                    <div class="col-md-8">
                        <select name="idasalpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
                            <option value="0" selected>Semua Asal Pasien</option>
                            <option value="1" <?=($idasalpasien == '1' ? 'selected' : '')?>>Rawat Jalan</option>
                            <option value="2" <?=($idasalpasien == '2' ? 'selected' : '')?>>Instalasi Gawat Darurat</option>
                            <option value="3" <?=($idasalpasien == '3' ? 'selected' : '')?>>Rawat Inap</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggalrujukan">Tanggal Rujukan</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" name="tanggaldari" placeholder="From" value="{tanggaldari}">
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Status</label>
                    <div class="col-md-8">
                        <select name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
                            <option value="#" selected>Semua Status</option>
                            <option value="0" <?=($status == '0' ? 'selected' : '')?>>Dibatalkan</option>
                            <option value="1" <?=($status == '1' ? 'selected' : '')?>>Menunggu Tindakan</option>
                            <option value="2" <?=($status == '2' ? 'selected' : '')?>>Menunggu Hasil</option>
                            <option value="3" <?=($status == '3' ? 'selected' : '')?>>Selesai</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="idtindakan">Tipe</label>
                    <div class="col-md-8">
                        <select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="">Pilih Opsi</option>
                            <option value="1" <?=($idtipe == 1 ? 'selected' : '')?>>Umum</option>
                            <option value="2" <?=($idtipe == 2 ? 'selected' : '')?>>Pathologi Anatomi</option>
                            <option value="3" <?=($idtipe == 3 ? 'selected' : '')?>>PMI</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="idtindakan">Head Parent</label>
                    <div class="col-md-8">
                        <select name="idtindakan" id="idtindakan" class="js-select2 form-control" style="width: 100%;" data-placeholder=" - All Kategori - ">
                            <option value="0" <?=($idtindakan == '0') ? "selected" : "" ?>>- All -</option>
                            <?php foreach  ($list_level0 as $row) { ?>
                            <option value="<?=$row->id;?>" <?=($idtindakan == $row->id) ? "selected" : "" ?>><?=TreeView($row->level, $row->nama)?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="iddokterperujuk">Dokter Perujuk</label>
                    <div class="col-md-8">
                        <select id="iddokterperujuk" name="iddokterperujuk" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="0">Semua Dokter Perujuk</option>
                            <?php foreach ($list_dokterperujuk as $row) { ?>
                            <option value="<?php echo $row->id; ?>" <?=($iddokterperujuk == $row->id ? 'selected' : '')?>><?php echo $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="iddokterlaboratorium">Dokter Laboratorium</label>
                    <div class="col-md-8">
                        <select id="iddokterradiologi" name="iddokterlaboratorium" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="0">Semua Dokter Laboratorium</option>
                            <?php foreach ($list_dokterlaboratorium as $row) { ?>
                            <option value="<?php echo $row->id; ?>" <?=($iddokterlaboratorium == $row->id ? 'selected' : '')?>><?php echo $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <?php if (UserAccesForm($user_acces_form,array('1014'))){ ?>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                    <?}?>
                </div>
            </div>
            <?php echo form_close() ?>
        </div>
        <hr style="margin-top:10px">
		<div class="table-responsive">
        <table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>No. Lab</th>
                    <th>No. Antrian</th>
                    <th>No. Rujukan</th>
                    <th>No. Pendaftaran</th>
                    <th>No. Medrec</th>
                    <th>Nama</th>
                    <th>Asal Pasien</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
		</div>
    </div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
// Initialize when page loads
jQuery(function() {
    BaseTableDatatables.init();
    $('#datatable-simrs').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}trujukan_laboratorium/getIndex/' + '<?=$this->uri->segment(2)?>',
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 2,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 3,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 4,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 5,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 6,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 7,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 8,
                "orderable": true
            },
            {
                "width": "15%",
                "targets": 9,
                "orderable": true
            }
        ]
    });
});

$("#idtipe").change(function() {
    if ($(this).val() != '') {
        $.ajax({
            url: '{site_url}mtarif_laboratorium/find_headparent/' + $(this).val(),
            dataType: "json",
            success: function(data) {
                $('#idtindakan').find('option').remove().end().append('<option value="0" selected>- All -</option>').val('0').trigger("liszt:updated");
                $('#idtindakan').append(data.detail);
            }
        });
    }
});

</script>
