<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Bukti Pengambilan Laboratorium</title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      * {
        font-size: 14px !important;
      }
      table {
        font-size: 14px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      * {
        font-size: 9px !important;
      }
      table {
        font-size: 9px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <table class="content-2">
      <tr>
        <td rowspan="2" style="width:10%"><img src="<?= base_url() ?>assets/upload/logo/logo.png" width="100px"></td>
      </tr>
      <tr>
        <td style="text-align:left" colspan="2">
          &nbsp;&nbsp;INSTALASI LABORATORIUM<br>
          <b>BUKTI PENGAMBILAN HASIL<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LABORATORIUM</b>
        </td>
      </tr>
      <tr>
        <td style="width:100px">No Register</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$norujukan;?></td>
      </tr>
      <tr>
        <td style="width:100px">No.RM</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$nomedrec;?></td>
      </tr>
      <tr>
        <td style="width:100px">Nama</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$namapasien;?></td>
      </tr>
      <tr>
        <td style="width:100px">Tgl.Lahir / Umur</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=date("Y-m-d", strtotime($tanggallahir));?> / <?=$umur;?>.Th</td>
      </tr>
      <tr>
        <td style="width:100px">Tgl Pemeriksaan</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$tanggal;?></td>
      </tr>
      <tr>
        <td style="width:100px">Tgl Pengambilan</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$tanggal_pengambilan;?></td>
      </tr>
    </table>
    <br>
    <table>
      <tr>
        <td style="width:10%" class="text-left"><b>Petugas Laboratorium</b></td>
        <td>&nbsp;</td>
        <td rowspan="3">Bukti Harap Dibawa Saat Pengambilan.<br>Untuk Informasi Lebih Lanjut Dapat<br>Menghubungi nomor whatsapp :<br>085872031663</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td style="width:10%" class="text-left">( <?=$this->session->userdata('user_name')?> )</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </body>
</html>
