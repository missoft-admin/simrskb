<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Hasil Pemeriksaan Laboratorium</title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      * {
        font-size: 13px !important;
      }
      table {
        font-size: 13px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }

      #copy {
        font-size: 30px !important;
      }
    }

    @media screen {
      * {
        font-size: 13px !important;
      }
      table {
        font-size: 13px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }

      #copy {
        font-size: 30px !important;
      }
    }
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <table class="content">
      <tr>
        <td style="text-align:center">
          <img src="<?= base_url() ?>assets/upload/logo/logo.png" width="100px"><br>
          &nbsp;Jl. LLRE. Martadinata No.28 Bandung<br>
          &nbsp;T. (022) 4206717 F. (022) 4216436
        </td>
      </tr>
    </table>
    <p class="text-center" style="margin:0"><b>HASIL PEMERIKSAAN LABORATORIUM</b></p>
    <br>
    <table class="content-2">
      <tr>
        <td style="width:100px">No. Register</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$norujukan;?></td>

        <td style="width:100px">No. Medrec</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$nomedrec;?></td>
      </tr>
      <tr>
        <td style="width:100px">Nama</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$namapasien;?></td>

        <td style="width:100px">Kelompok Pasien</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$namakelompok;?></td>
      </tr>
      <tr>
        <td style="width:100px">Alamat</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$alamat;?></td>

        <td style="width:100px">Nama Asuransi</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=($namarekanan ? $namarekanan : '-');?></td>
      </tr>
      <tr>
        <td style="width:100px">Dokter Perujuk</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$namadokterperujuk;?></td>

        <td style="width:100px">Tanggal Lahir</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=date("Y-m-d", strtotime($tanggallahir));?></td>
      </tr>
      <tr>
        <td style="width:100px">No. Laboratorium</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?= str_pad($noantrian, 4, "0", STR_PAD_LEFT);?></td>

        <td style="width:100px">Umur</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$umur_tahun;?> Tahun <?=$umur_bulan;?> Bulan <?=$umur_hari;?> Hari</td>
      </tr>
      <tr>
        <td style="width:100px"></td>
        <td class="text-center" style="width:20px"></td>
        <td></td>

        <td style="width:100px">Rujukan</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=GetAsalRujukan($asalrujukan);?></td>
      </tr>
    </table>
    <h1 id="copy">COPY</h1>
    <br>
    <table>
      <tr>
        <td class="text-center text-bold border-full" style="width:25%">PEMERIKSAAN</td>
        <td class="text-center text-bold border-full" style="width:20%">HASIL</td>
        <td class="text-center text-bold border-full" style="width:10%">SATUAN</td>
        <td class="text-center text-bold border-full" style="width:30%">NILAI NORMAL</td>
        <td class="text-center text-bold border-full" style="width:15%">KETERANGAN</td>
      </tr>
      <?php foreach  ($list_tindakan as $row){ ?>
        <?php if($row->idkelompok == 1){ ?>
          <tr>
            <td class="border-full text-bold" colspan="5"><?=TreeView(0, strtoupper($row->nama))?></td>
          </tr>
        <?php }else{ ?>
          <tr>
            <td class="border-full"><?=TreeView(1, strtoupper($row->nama))?></td>
            <td class="text-center border-full"><?=$row->hasil?></td>
            <td class="text-center border-full"><?=$row->satuan ?></td>
            <td class="text-center border-full"><?=$row->nilainormal ?></td>
            <td class="text-center border-full"><?=$row->keterangan ?></td>
          </tr>
        <?php } ?>
      <?php } ?>
    </table>
    <br>
    <table>
      <tr>
        <td colspan="3">Catatan : <?=$catatan?></td>
      </tr>
      <tr>
        <td colspan="3">Penanggung Jawab : <?=$namadokterlaboratorium?></td>
      </tr>
      <tr>
        <td class="text-center"></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width:30%" class="text-center text-bold">Bandung, <?=date("d").' '.MONTHFormat(date("m")).' '.date("Y");?> <br> Petugas Laboratorium</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width:30%" class="text-center">&nbsp;</td>
      </tr>
      <tr>
        <td style="width:30%" class="text-center"></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width:30%" class="text-center text-bold">( {user_input_cetak} )</td>
      </tr>
    </table>

    <table>
      <tr>
        <td colspan="3" style="font-size:9px!important">Jam Input : {tanggal_input_pemeriksaan} | Jam Sampling : {tanggal_input_sampling} | Jam Cetak : {tanggal_input_cetak}<td>
      </tr>
      <tr>
        <td colspan="3"><i style="font-size:9px!important">Cetakan Ke - 1 - R: {user_input_pemeriksaan} / S: {user_input_sampling} / I: {user_input_hasil} / C: {user_input_cetak}</i><td>
      </tr>
    </table>
  </body>
</html>
