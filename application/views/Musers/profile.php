<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}page_main" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('musers/change_profile','class="form-horizontal" onsubmit="return validate_final()"') ?>
			<?php if($avatar != ''){ ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email"></label>
				<div class="col-md-7">
					<img class="img-avatar" src="{upload_path}avatars/{avatar}" />
				</div>
			</div>
			<?php } ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Avatar</label>
				<div class="col-md-7">
					<div class="box">
						<input type="file" id="file-3" class="inputfile inputfile-3" style="display:none;" name="avatar" value="{avatar}" />
						<label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Name</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{name}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Username</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="username" placeholder="username" name="username" value="{username}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Unit Pelayanan Default</label>
				<div class="col-md-7">
					<select name="unitpelayananiddefault" class="js-select2 form-control" required>
						<?foreach($unit_pelayanan as $row){?>
							<option value="<?=$row->id?>" <?=($row->id==$unitpelayananiddefault?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Password Lama</label>
				<div class="col-md-7">
					<input type="password" class="form-control" id="password_old" placeholder="Password" name="password_old" value="">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Password Baru</label>
				<div class="col-md-7">
					<input type="password" class="form-control" id="password" placeholder="Password" name="password" value="">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Password Confirmation</label>
				<div class="col-md-7">
					<input type="password" class="form-control" id="password_confirmation" placeholder="Password Confirmation" name="password_confirmation" value="">
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Submit</button>
					<a href="{base_url}musers" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
					<input type="hidden" class="form-control" id="cur_pass" placeholder="Password Confirmation" name="cur_pass" value="{password}">
					<input type="hidden" class="form-control" id="hasil_pass" placeholder="Password Confirmation" name="hasil_pass" value="">
					<input type="hidden" class="form-control" id="id" placeholder="Password Confirmation" name="id" value="{id}">
			<?php echo form_close() ?>
	</div>
</div>


<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}autoNumeric/autoNumeric.min.js"></script>
<script type="text/javascript">
	
	function validate_final(){
		
		var pass_new=$("#password").val();
		var pass_cur=$("#cur_pass").val();
		var pass_conf=$("#password_confirmation").val();
		if (pass_new !='' || pass_conf!=''){
			if ($("#hasil_pass").val() != $("#cur_pass").val()){
				
				sweetAlert("Maaf...", "Password Lama Salah", "error");
				return false;
			}else if (pass_new != pass_conf){
				// alert('Error');
				sweetAlert("Maaf...", "Password Baru dan Password Confirmation Tidak sama", "error");
				return false;
			}
			
		}

		return true;
	}
	$("#password_old").change(function(){
		var pass_old=$(this).val();
		$.ajax({
			url: '{site_url}musers/get_md5/',
			dataType: "json",
			method: "POST",
			data : {password:pass_old},
			success: function(data) {
				$("#hasil_pass").val(data)
			}
		});
	});
	
</script>
