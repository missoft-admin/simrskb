<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}musers" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('musers/save','class="form-horizontal"') ?>
			<?php if($avatar != ''){ ?>
			<div class="form-group">
	        <label class="col-md-3 control-label" for="example-hf-email"></label>
	        <div class="col-md-7">
	            <img class="img-avatar" src="{upload_path}avatars/{avatar}" />
	        </div>
	    </div>
			<?php } ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Avatar</label>
				<div class="col-md-7">
					<div class="box">
						<input type="file" id="file-3" class="inputfile inputfile-3" style="display:none;" name="avatar" value="{avatar}" />
						<label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Name</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{name}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Username</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="username" placeholder="username" name="username" value="{username}" required="" aria-required="true">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Password</label>
				<div class="col-md-7">
					<input type="password" class="form-control" id="password" placeholder="Password" name="password" value="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Password Confirmation</label>
				<div class="col-md-7">
					<input type="password" class="form-control" id="password_confirmation" placeholder="Password Confirmation" name="password_confirmation" value="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">email</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="email" placeholder="email" name="email" value="{email}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Roles</label>
				<div class="col-md-7">
					<select id="roleid"  name="roleid" class="form-control" required></select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Unit Pelayanan Default</label>
				<div class="col-md-7">
					<select name="unitpelayananiddefault" class="form-control"></select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Tipe Barang</label>
				<div class="col-md-7">
					<select name="tipebarang[]" class="form-control tipebarang"></select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Gudang</label>
				<div class="col-md-7">
					<select name="gudang[]" class="form-control gudang"></select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Setting Akses Dokter Radiologi</label>
				<div class="col-md-7">
					<select name="iddokter_rad[]" class="js-select2 form-control" multiple="multiple">
						
					<?foreach($list_dokter_radiologi as $row){?>
						<option value="<?=$row->id?>" <?=$row->selected?>><?=$row->nama?></option>
					<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Tampilkan Dokter Lain</label>
				<div class="col-md-7">
					<select name="iddokter_rad_lain[]" class="js-select2 form-control" multiple="multiple">
					<?foreach($list_dokter_radiologi_lain as $row){?>
						<option value="<?=$row->id?>" <?=$row->selected?>><?=$row->nama?></option>
					<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Setting Akses Dokter Laboratorium</label>
				<div class="col-md-7">
					<select name="iddokter_lab[]" class="js-select2 form-control" multiple="multiple">
					<?foreach($list_dokter_lab as $row){?>
						<option value="<?=$row->id?>" <?=$row->selected?>><?=$row->nama?></option>
					<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Setting Akses Dokter Anesthesi</label>
				<div class="col-md-7">
					<select name="iddokter_anesthesi[]" class="js-select2 form-control" multiple="multiple">
					<?foreach($list_dokter_anesthesi as $row){?>
						<option value="<?=$row->id?>" <?=$row->selected?>><?=$row->nama?></option>
					<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Tampilkan Dokter Lain</label>
				<div class="col-md-7">
					<select name="iddokter_anesthesi_lain[]" class="js-select2 form-control" multiple="multiple">
					<?foreach($list_dokter_anesthesi_lain as $row){?>
						<option value="<?=$row->id?>" <?=$row->selected?>><?=$row->nama?></option>
					<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="example-hf-email">Tipe User</label>
				<div class="col-md-7">
					<select name="st_kasir" class="js-select2 form-control">
						<option value="0" <?=($st_kasir=='0')?'selected':''?>>- Umum -</option>
						<option value="1"  <?=($st_kasir=='1')?'selected':''?>>Kasir</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Submit</button>
					<a href="{base_url}musers" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php //echo form_hidden('id', $id); ?>
			<input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="{id}" required="" aria-required="true">
			<input type="hidden" class="form-control" id="role_id" placeholder="role_id" name="role_id" value="{roleid}" required="" aria-required="true">
			<?php echo form_close() ?>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){

		$('.gudang').select2({
			placeholder: 'Select an Option',
			multiple: true,
			data: [
				{id: '1', text: 'Non Logistik'},
				{id: '2', text: 'Logistik'},
			]
		}).val('{gudang}').trigger('change');

		setTimeout(function() {
			$.ajax({
				type: "POST", 
				url: '{site_url}musers/get_tipegudang/{id}',
				dataType: 'JSON', 
				success: function(data) {
					var arr = [];
					for (var i in data) { arr.push(data[i].id); }
					$('.gudang').val(arr).trigger('change'); 
				}
			});
		}, 500);

		$('.tipebarang').select2({
			placeholder: 'Select an Option',
			multiple: true,
			data: [
				{id: '1', text: 'Alkes'},
				{id: '2', text: 'Implan'},
				{id: '3', text: 'Obat'},
				{id: '4', text: 'Logistik'},
			]
		}).val('{tipebarang}').trigger('change')

		setTimeout(function() {
			$.ajax({
				type: "POST", 
				url: '{site_url}musers/get_unitpelayanan_tipebarang/{id}',
				dataType: 'JSON', 
				success: function(data) {
					var arr = [];
					for (var i in data) { arr.push(data[i].id); }
					$('.tipebarang').val(arr).trigger('change'); 
				} 
			});			
		}, 500);

	    $('select[name=roleid]').select2({
	        placeholder: 'Select an Option',
	        ajax: {
	            url: '{site_url}musers/select_roleid/',
	            dataType: 'json',
	            delay: 250,
	            processResults: function (data) {
	                return {results:data}
	            },
	            cache: false,
	        }
	    })

	    setTimeout(function() {
			// alert($("#roleid").val());
	        var dataoption = $('select[name=roleid]')
	        $.ajax({
	            url: '{site_url}musers/select_roleid_select/'+$("#role_id").val(),
	            dataType: 'json',
	            success: function(data) {
	                var option = new Option(data.text, data.id, true, true);
	                dataoption.append(option).trigger('change');
	                dataoption.trigger({
	                    type: 'select2:select',
	                    params: {
	                        data: data
	                    }
	                })
	            }
	        })
	    }, 500);
		
		// alert($("#id").val());

	    $('select[name=unitpelayananiddefault]').select2({
			// var id=;
	        placeholder: 'Select an Option',
	        ajax: {
	            url: '{site_url}musers/select_unitpelayananiddefault_acep/'+$("#id").val(),
	            dataType: 'json',
	            delay: 250,
	            processResults: function (data) {
					console.log(data);
	                return {results:data}
	            },
	            cache: false,
	        }
	    })

        var dataoption = $('select[name=unitpelayananiddefault]')
        $.ajax({
            url: '{site_url}musers/select_unitpelayananiddefault_select/{unitpelayananiddefault}',
            dataType: 'json',
            success: function(data) {
                var option = new Option(data.text, data.id, true, true);
                dataoption.append(option).trigger('change');
                dataoption.trigger({
                    type: 'select2:select',
                    params: {
                        data: data
                    }
                })
            }
        })
	})
</script>
