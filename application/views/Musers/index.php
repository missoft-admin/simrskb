<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('248'))){ ?>
<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('257'))){ ?>
		<ul class="block-options">
        <li>
            <a href="{base_url}musers/create" class="btn"><i class="fa fa-plus"></i></a>
        </li>
    </ul>
		<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<table class="table table-bordered table-striped js-dataTable-full">
			<thead>
				<tr>
					<th>Avatar</th>
					<th>Name</th>
					<th>Role</th>
					<th>Last Login</th>
					<th>Last Edit</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($list_index as $row){ ?>
				<tr>
					<td>
							<img class="img-avatar" src="{upload_path}avatars/<?=($row->avatar == '' ? 'default.jpg':$row->avatar)?>" />
					</td>
					<td><?=$row->name?></td>
					<td><?=$row->permission_name?></td>
					<td><?=LastLoginDate($row->last_login)?></td>
					<td><?=(HumanDateLong($row->last_edit)=='01-01-1970 07:00:00'?'-':HumanDateLong($row->last_edit))?></td>
					<td>
						<div class="btn-group">
							<?php if (UserAccesForm($user_acces_form,array('258'))){ ?>
								<a href="{base_url}musers/update/<?=$row->id?>" data-toggle="tooltip" title="Update"><i class="fa fa-pencil"></i></a>&nbsp;
							<?php } ?>
							<?php if (UserAccesForm($user_acces_form,array('259'))){ ?>
								<a href="{base_url}musers/delete/<?=$row->id?>" data-toggle="tooltip" title="Delete" onclick="remove_row(this); return false;"><i class="fa fa-trash-o"></i></a>
							<?php } ?>
						</div>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<?}?>