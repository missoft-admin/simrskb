<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>
<?php $user_id = $this->session->userdata('user_id'); ?>

<div class="block push-10">
    <div class="block-content">
        <input type="hidden" id="tab" name="tab" value="{tab}">
        <?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
        <input type="hidden" id="st_login" name="st_login" value="{st_login}">
        <div class="row pull-10">
            <div class="col-md-2 push-10">
                <div class="form-group">
                    <label class="col-xs-12" for="ruangan_id">Ruang</label>
                    <div class="col-xs-12">
                        <select id="ruangan_id" name="ruangan_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#" <?=($ruangan_id=='#'?'selected':'')?>>- Semua Ruangan -</option>
                            <?foreach($list_ruang as $r){?>
                            <option value="<?=$r->id?>" <?=($ruangan_id==$r->id?'selected':'')?>><?=$r->nama?></option>
                            <?}?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2 push-10">
                <div class="form-group">
                    <label class="col-xs-12" for="idkelas">Kelas</label>
                    <div class="col-xs-12">
                        <select id="idkelas" name="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#" <?=($idkelas=='#'?'selected':'')?>>- Semua Kelas -</option>
                            <?foreach($list_kelas as $r){?>
                            <option value="<?=$r->id?>" <?=($idkelas==$r->id?'selected':'')?>><?=$r->nama?></option>
                            <?}?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-2 push-10">
                <div class="form-group">
                    <label class="col-xs-12" for="idbed">Bed</label>
                    <div class="col-xs-12">
                        <select id="idbed" name="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#" <?=($idbed=='#'?'selected':'')?>>- Semua Bed -</option>
                            
                        </select>
                    </div>
                </div>
            </div>
\
            <div class="col-md-2 push-10">
                <div class="form-group">
                    <label class="col-xs-12" for="idbed">&nbsp;&nbsp;</label>
                    <div class="col-xs-12">
                            <span class="input-group-btn">
								<button class="btn btn-success " type="button" onclick="load_index_all()" title="Filter"><i class="fa fa-filter"></i> Filter</button>
								<button class="btn btn-primary " type="button" title="Display Ketersediaan"><i class="fa fa-tv"></i> Display Ketersediaan</button>
							
							</span>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close() ?>
    </div>
</div>

<div class="block">
    <ul class="nav nav-pills">
        <li id="div_1" class="<?=($tab=='1'?'active':'')?>">
            <a href="javascript:void(0)" onclick="set_tab(1)"><i class="si si-list"></i> Semua</a>
        </li>
        <li id="div_2" class="<?=($tab=='2'?'active':'')?>">
            <a href="javascript:void(0)"  onclick="set_tab(2)"><i class="si si-pin"></i> Terisi </a>
        </li>
        <li id="div_3" class="<?=($tab=='3'?'active':'')?>">
            <a href="javascript:void(0)" onclick="set_tab(3)"><i class="fa fa-check-square-o"></i> Kosong</a>
        </li>
		<li id="div_4" class="<?=($tab=='3'?'active':'')?>">
            <a href="javascript:void(0)" onclick="set_tab(3)"><i class="fa fa-check-square-o"></i> Terpesan</a>
        </li>
		<li id="div_5" class="<?=($tab=='3'?'active':'')?>">
            <a href="javascript:void(0)" onclick="set_tab(3)"><i class="fa fa-check-square-o"></i> Tidak Dapat Digunakan</a>
        </li>
    </ul>
    <div class="block-content">
        <input type="hidden" id="tab" name="tab" value="{tab}">
        <div class="row   ">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered" id="index_all">
                        <thead>
                            <tr>
                                <th width="15%">Action</th>
                                <th width="15%">Ruangan</th>
                                <th width="10%">Kelas</th>
                                <th width="15%">Bed</th>
                                <th width="10%">Harga Kamar</th>
                                <th width="15%">Status</th>
                                <th width="15%">Nama Pasien</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal in" id="modal_fasilitas" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Fasilitas</h3>
				</div>
				<div class="block-content">
					<div class="row items-push" id="div_gambar">
							<div class="col-lg-6 animated fadeIn">
								<div class="img-container">
									<img class="img-responsive" src="assets/img/photos/photo1.jpg" alt="">
									<div class="img-options">
										<div class="img-options-content">
											<h3 class="font-w400 text-white push-5">Image Caption</h3>
											<h4 class="h6 font-w400 text-white-op push-15">Some Extra Info</h4>
											<a class="btn btn-sm btn-default" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
											<a class="btn btn-sm btn-default" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6 animated fadeIn">
								<div class="img-container">
									<img class="img-responsive" src="assets/img/photos/photo2.jpg" alt="">
									<div class="img-options">
										<div class="img-options-content">
											<h3 class="font-w400 text-white push-5">Image Caption</h3>
											<h4 class="h6 font-w400 text-white-op push-15">Some Extra Info</h4>
											<a class="btn btn-sm btn-default" href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
											<a class="btn btn-sm btn-default" href="javascript:void(0)"><i class="fa fa-times"></i> Delete</a>
										</div>
									</div>
								</div>
							</div>
						</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>

<script type="text/javascript">
var table;
var tab;
var st_login;

$(document).ready(function() {
    load_index_all();
    // tab=$("#tab").val();
    // st_login=$("#st_login").val();
    // // $("#div_1").classlist.remove("active");
    // cek_login();
    // set_tab(tab);
    // startWorker();
});
function set_modal_fasilitas(id){
	$("#modal_fasilitas").modal('show');
	$.ajax({
		url: '{site_url}tmonitoring_bed/get_fasilitas/',
		dataType: "json",
		type: "POST",
		data:{
			mfasilitas_id:id,
		},
		success: function(data) {
			$("#div_gambar").html(data.detail);
		}
	});
}
// $(document).on("change", "#idbed", function() {
    // get_bed();
// });
$(document).on("change", "#idkelas", function() {
    get_bed();
});
$(document).on("change", "#ruangan_id", function() {
	$("#idkelas").val("#").trigger('change');
    get_bed();
});
$(document).on("click", "#btn_cari", function() {
    load_index_all();
});

function set_tab($tab) {
    tab = $tab;
    // alert(tab);
    // $("#cover-spin").show();
    document.getElementById("div_1").classList.remove("active");
    document.getElementById("div_2").classList.remove("active");
    document.getElementById("div_3").classList.remove("active");
    if (tab == '1') {
        document.getElementById("div_1").classList.add("active");
    }
    if (tab == '2') {
        document.getElementById("div_2").classList.add("active");
    }
    if (tab == '3') {
        document.getElementById("div_3").classList.add("active");
    }

    load_index_all();
}


function load_index_all() {
    $('#index_all').DataTable().destroy();
    let idkelas = $("#idkelas").val();
    let ruangan_id = $("#ruangan_id").val();
    let idbed = $("#idbed").val();
    $("#cover-spin").show();
    // alert(ruangan_id);
    table = $('#index_all').DataTable({
        autoWidth: false,
        searching: true,
        serverSide: true,
        "processing": true,
        "order": [],
        "pageLength": 10,
        "ordering": false,
        
        ajax: {
            url: '{site_url}tmonitoring_bed/getIndex_all',
            type: "POST",
            dataType: 'json',
            data: {
                idbed: idbed,
                idkelas: idkelas,
                idruangan: ruangan_id,

            },
			columnDefs: [
				{  className: "text-right", targets:[0,4] },
				// {  className: "text-right", targets:[4] },
				{  className: "text-center", targets:[1,2,3,5] },
				 // { "width": "5%", "targets": [5,6,7] },
				 // { "width": "10%", "targets": [0,2,3,8] },
				 // { "width": "15%", "targets": [1] },
				 // { "width": "20%", "targets": [4] }
			]

        },
        "drawCallback": function(settings) {
            // $("#index_all thead").remove();
            $("#cover-spin").hide();
        }
    });
    $("#cover-spin").hide();
}

function get_bed(){
	let ruangan_id=$("#ruangan_id").val();
	let idkelas=$("#idkelas").val();
	$.ajax({
		url: '{site_url}tmonitoring_bed/get_bed/',
		dataType: "json",
		type: "POST",
		data:{
			idruangan:ruangan_id,
			idkelas:idkelas,
		},
		success: function(data) {
			$("#idbed").empty();
			$('#idbed').append('<option value="#" selected>- All Bed -</option>');
			$('#idbed').append(data.detail);
		}
	});

}

</script>
