<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<style>
#main-container {
    background-color: <?=$bg_color?>;
}
.block {
    background-color: <?=$bg_color?>;
}
.font-s25 {
  font-size: 25px !important;
}
.font-s20 {
  font-size: 20px !important;
}
.ms-container {
    height: 400px;
    overflow: hidden;
    position:relative
}
.flexslider {
    position:static;
}
.metaslider .slides img {
    width: auto;
    min-width: 100vw!important;
    min-height: 400px;
    position: absolute;
}
</style>
<div class="block">
<div class="content content-narrow">
	<div class="row pull-t">
		<div class="col-sm-4 col-lg-4">
			<div class="block-content block-content-full text-center">
				<div class="pull-left">
					<img class="img-avatar img-avatar96" src="{upload_path}apm_setting/<?=$header_logo?>" alt="">
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-lg-4">
			
		</div>
		<div class="col-sm-4 col-lg-4">
			<div class="block-content block-content-full clearfix">
				<table class="block-table text-center">
					<tbody>
						<tr>
							<td style="width: 80%;">
								<div class="text-right font-w700 text-white"><span class="font-s25" id="tanggal"><?=GetDayIndonesia(date('Y-m-d'),true,'text')?></span></div>
								<div class="text-right text-white"><h2 id="current-time-now" data-start="<?php echo time() ?>"></h2></div>
							</td>
							<td class="" style="width: 20%;">
								<div class="push-5-l">
									<i class="fa fa-bell fa-3x text-white text-left"></i>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				
				
			</div>
		</div>
		
	</div>
	<div class="row">
		<div class="col-12">
			<h1 class="text-white  text-center">{judul_header}</h1>
			<h2 class="text-white  text-center ">{judul_sub_header}</h2>
		</div>
		
	</div>

	<div class="row push-15-t">
		<div class="col-lg-12">
			<div class="block ms-container">
				<div class="js-slider slick-padding-dots remove-margin-b flexslider" data-slider-dots="true" data-slider-autoplay="true">
					<?foreach($list_slider as $r){?>
					<div>
						<img class="img-responsive metaslider slides img " style="margin: 0 auto;" src="{upload_path}apm_setting/<?=$r->filename?>" alt="">
					</div>
					<?}?>
					
				</div>
				
			</div>
		</div>
		
	</div>
	<div class="row">
		<div class="col-xs-6 col-sm-6 col-lg-3">
			<a class="block block-link-hover2 text-center" href="javascript:void(0)">
				<div class="block-content block-content-full bg-danger">
					<i class="si si-users fa-4x text-white"></i>
					<div class="font-w600 text-white-op push-15-t">ANTRIAN</div>
				</div>
			</a>
		</div>
		<div class="col-xs-6 col-sm-6 col-lg-3">
			<a class="block block-link-hover2 text-center" href="javascript:void(0)">
				<div class="block-content block-content-full bg-danger">
					<i class="si si-login fa-4x text-white"></i>
					<div class="font-w600 text-white-op push-15-t">CHECK IN RESERVASI</div>
				</div>
			</a>
		</div>
		<div class="col-xs-6 col-sm-6 col-lg-3">
			<a class="block block-link-hover2 text-center" href="javascript:void(0)">
				<div class="block-content block-content-full bg-danger">
					<i class="si si-clock fa-4x text-white"></i>
					<div class="font-w600 text-white-op push-15-t">JADWAL DOKTER</div>
				</div>
			</a>
		</div>
		<div class="col-xs-6 col-sm-6 col-lg-3">
			<a class="block block-link-hover2 text-center" href="javascript:void(0)">
				<div class="block-content block-content-full bg-danger">
					<i class="si si-note fa-4x text-white"></i>
					<div class="font-w600 text-white-op push-15-t">PENDAFTARAN MANDIRI</div>
				</div>
			</a>
		</div>
	</div>
</div>
</div>
<script src="{plugins_path}sparkline/jquery.sparkline.min.js"></script>
<script src="{plugins_path}slick/slick.min.js"></script>
<script src="{js_path}pages/base_ui_widgets.js"></script>
<script>
 var freshTime = new Date(parseInt($("#current-time-now").attr("data-start"))*1000);
//loop to tick clock every second
var func = function myFunc() {
	//set text of clock to show current time
	$("#current-time-now").text(freshTime.toLocaleTimeString());
	//add a second to freshtime var
	freshTime.setSeconds(freshTime.getSeconds() + 1);
	//wait for 1 second and go again
	setTimeout(myFunc, 1000);
};
func();
jQuery(function () {
	// Init page helpers (Slick Slider + Easy Pie Chart plugins)
	App.initHelpers(['slick', 'easy-pie-chart']);
});
</script>