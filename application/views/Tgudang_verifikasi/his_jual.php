<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('1125'))){ ?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:25px">
        <div class="row">
            <?php echo form_open('tgudang_verifikasi/filter', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Barang</label>
                    <div class="col-md-8">
                         <input class="form-control" readonly type="text" name="namabarang" placeholder="Nama Barang" value="<?=$kode.' - '.$nama?>"/>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Tipe</label>
                    <div class="col-md-8">
                         <input class="form-control" readonly type="text" name="namatipe" placeholder="Nama Barang" value="{namatipe}"/>
                    </div>
                </div>
				<input class="form-control" readonly type="hidden" id="idtipe" name="idtipe" placeholder="Nama Barang" value="{idtipe}"/>
				<input class="form-control" readonly type="hidden" id="idbarang" name="idbarang" placeholder="Nama Barang" value="{idbarang}"/>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Kategori</label>
                    <div class="col-md-8">
                         <input class="form-control" readonly type="text" name="nama_kategori" placeholder="Nama Barang" value="{nama_kategori}"/>
                    </div>
                </div>
                
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggaldari" name="tanggaldari" placeholder="From" value="{tanggaldari}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggalsampai" name="tanggalsampai" placeholder="To" value="{tanggalsampai}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" id="btn_filter" type="button" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
			<div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Total PPN</label>
                    <div class="col-md-8">
                         <input class="form-control number" readonly type="text" id="tot_ppn" name="tot_ppn" placeholder="Total PPN" value=""/>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Total Diskon</label>
                    <div class="col-md-8">
                         <input class="form-control number" readonly type="text" id="tot_diskon" name="tot_diskon" placeholder="Total Diskon" value=""/>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Total Harga</label>
                    <div class="col-md-8">
                         <input class="form-control number" readonly type="text" id="tot_harga" name="tot_harga" placeholder="Total Harga" value=""/>
                    </div>
                </div>
                
            </div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th width="5%">ID</th>
                    <th width="2%">NO</th>
                    <th width="10%">TANGGAL</th>
                    <th width="10%">JENIS TRANSAKSI</th>
                    <th width="10%">NO TRANSAKSI</th>
                    <th width="10%">QTY</th>
                    <th width="15%">HARGA DASAR</th>
                    <th width="15%">MARGIN</th>
                    <th width="15%">HARGA JUAL</th>
                    <th width="15%" class="text-center">AKSI</th>
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
</div>
<?}?>

<!-- Page JS Code -->
<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;

$(document).ready(function(){	
	$('.number').number(true, 0);
	clear_tabel();
});
function clear_tabel(){
	table = $('#index_list').DataTable({
			columnDefs: [
			{ "targets": [0], "visible": false },
			{ "width": "5%", "targets": [1] },
			{ "width": "10%", "targets": [2,3,4,5,6,7,8,9] },
			]           
        });
}
$(document).on("click", "#btn_filter", function() {
		get_total();
		loadIndex();
});
function loadIndex() {
		var idtipe=$("#idtipe").val();
		var idbarang=$("#idbarang").val();
		var tanggaldari=$("#tanggaldari").val();
		var tanggalsampai=$("#tanggalsampai").val();
		
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"order": [],
		"ajax": {
			url: '{site_url}tgudang_verifikasi/Load_His_Jual/',
			type: "POST",
			dataType: 'json',
			data: {
				idtipe: idtipe,
				idbarang: idbarang,
				tanggaldari: tanggaldari,
				tanggalsampai: tanggalsampai,
			}
		},
		columnDefs: [
			{ "targets": [0], "visible": false },
			{ "width": "5%", "targets": [1] },
			{ "width": "10%", "targets": [2,3,4,5,6,7,8,9] },
			]      
		});
	}
	function get_total() {
		var idtipe=$("#idtipe").val();
		var idbarang=$("#idbarang").val();
		var tanggaldari=$("#tanggaldari").val();
		var tanggalsampai=$("#tanggalsampai").val();
		$.ajax({
			url: '{site_url}tgudang_verifikasi/get_total_his_jual/',
			type: "POST",
			dataType: 'json',
			data: {
				idtipe: idtipe,
				idbarang: idbarang,
				tanggaldari: tanggaldari,
				tanggalsampai: tanggalsampai,
			},
			success: function(data) {
				$("#tot_ppn").val(data.total_ppn);
				$("#tot_diskon").val(data.total_diskon);
				$("#tot_harga").val(data.total_harga);
					// console.log(data);
			}
		});

	}
</script>
