<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <form id="form1" class="form-horizontal" method="post" action="{site_url}tgudang_pemesanan/save_konfirmasi_pemesanan_draft2">
            <input type="hidden" name="id" value="{id}">
            <div class="form-group">
                <label class="col-md-3 control-label">Distributor</label>
                <div class="col-md-6">
                    <select class="form-control" name="iddistributor" style="width:100%"></select>
                </div>
            </div>
            <br><br>
			<div class="table-responsive">
            <table class="table table-bordered table-striped table-responsive" id="datatable">
                <thead>
                    <th width="10%">TIPE</th>
                    <th width="20%">NAMA BARANG</th>
                    <th width="10%">KUANTITAS KECIL</th>
                    <th width="10%">KUANTITAS BESAR</th>
                    <th width="10%">JML PESAN</th>
                    <th width="10%">SATUAN PESAN</th>
                    <th width="10%">KONVERSI</th>
                    <th hidden>detail</th>
                    <th hidden>kuantitas_kecil</th>
                    <th hidden>satuan_kecil</th>
                    <th hidden>kuantitas_besar</th>
                    <th hidden>satuan_besar</th>
                </thead>
                <tbody>
					<?foreach($list_detail as $row){?>
						<tr>
							<td><?=$row->namatipe?></td>
							<td><?=$row->namabarang?></td>
							<td><?=number_format($row->kuantitas_kecil).' <strong>'.$row->satuan_kecil?></strong></td>
							<td><?=number_format($row->kuantitas_besar,0).' <strong>'.$row->satuan_besar?></strong></td>
							<td><!-- 4 -->
								<input type="text" name="kuantitas_pesan[]" readonly  class="form-control number" value="<?=$row->kuantitas_besar?>">
							</td>
							<td><?=$row->satuan_besar?></strong></td><!-- 5 -->
							<td>
								<select class="form-control detail" name="konversi[]"  style="width:100%">
									<option value="2" selected>YA</option>
									<option value="1">TIDAK</option>											
								</select>
							</td><!-- 6 -->
							<td hidden><input type="text" name="id_det[]"  class="form-control number" value="<?=$row->id?>"></td><!-- 7 -->
							<td hidden><?=$row->kuantitas_kecil?></td><!-- 8 -->
							<td hidden><?=$row->satuan_kecil?></td><!-- 9 -->
							<td hidden><?=$row->kuantitas_besar?></td><!-- 10 -->
							<td hidden><?=$row->satuan_besar?></td><!-- 11 -->
						</tr>
					<?}?>
				</tbody>
            </table>
            </div>
            <textarea id="detailvalue" name="detailvalue" hidden></textarea>
            <div style="margin-top:50px;margin-bottom:20px;" class="text-right">
                <a href="{site_url}tgudang_pemesanan" class="btn btn-default btn-md">Kembali</a>
                <button type="submit" class="btn btn-success btn-md">Konfirmasi Pemesanan Draft</button>
            </div>
        </form>
    </div>
</div>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

    var table;

    $(document).ready(function(){
		$('.detail').select2();
		$(".number").number(true,0,'.',',');
        $('select[name=iddistributor]').select2({
            placeholder: 'Cari Data ... ',
            ajax: {
                url: '{site_url}tgudang_pemesanan/getdistributor',
                dataType: 'json',
                delay: 250,                
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return { id: item.id, text: item.text}
                        })
                    };
                },
                cache: false,
            }
        })

        var newOption = new Option('{namadistributor}', '{iddistributor}', true, true);
        $('select[name=iddistributor]').append(newOption).trigger('change');

        // table = $('#datatable').DataTable({
            // info: false, sort: false, searching: false, paging: false, destroy: true,
            // columnDefs: [{ targets: [ 4,5,6 ], visible: false }],
            // ajax: '{site_url}tgudang_pemesanan/detailbarangdraft/{id}',
        // })

        // setTimeout(function() { 
            // detailvalue();
            
        // }, 1000);
    })

$(document).on("change",".detail",function(){
	var jml_kecil=$(this).closest('tr').find("td:eq(8)").text();
	var sat_kecil=$(this).closest('tr').find("td:eq(9)").text();
	var jml_besar=$(this).closest('tr').find("td:eq(10)").text();
	var sat_besar=$(this).closest('tr').find("td:eq(11)").text();
	
	// alert($(this).val());
	if ($(this).val()=='1'){
		var detail = $(this);	
		detail.closest('tr').find("td:eq(4) input").val(jml_kecil);
		detail.closest('tr').find("td:eq(5)").text(sat_kecil);
	}else{
		var detail = $(this);
		detail.closest('tr').find("td:eq(4) input").val(jml_besar);
		detail.closest('tr').find("td:eq(5)").text(sat_besar);
	}
	
});   

</script>