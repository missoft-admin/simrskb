<div class="row hiden">
	<div class="col-md-12">
		<div class="block block-themed block-opt-hidden">
			<div class="block-header bg-primary">
				<ul class="block-options">

					<li>
						<button type="button" id="btn_ranap" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title">SELISIH HUTANG & PIUTANG </h3>
			</div>
			<div class="block-content">
				<h4><span class="label label-success">PENDAPATAN SELISIH PIUTANG</span></h4><br>
				<table id="tabel_hutang" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 25%;" hidden>ID</th>
								<th style="width: 10%;">NO PENGELOLAAN</th>
								<th style="width: 10%;">NAMA PENGELOAN</th>
								<th style="width: 20%;">DESKRIPSI</th>
								<th style="width: 20%;">TANGGAL</th>
								<th style="width: 10%;">NOMINAL</th>
								<th style="width: 10%;">AKSI</th>
							</tr>
							
						</thead>
						<tbody>
							
						</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	function load_hutang(){
		var tanggal_trx=$("#tanggal_trx").val();
		$('#tabel_hutang').DataTable().destroy();
		$('#tabel_hutang').DataTable({
				"autoWidth": false,
				"searching": false,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tsetoran_kas/load_hutang',
					type: "POST",
					dataType: 'json',
					data : {
						tanggal_trx:tanggal_trx,
					   }
				},
				"columnDefs": [
						{ "visible": false, "targets": [0], "class": 'text-center' },
						{ "targets": [5], "class": 'text-right'  },
						{ "width": "8%", "targets": [1,5], "class": 'text-center'  },
						{ "width": "10%", "targets": [4], "class": 'text-center'  },
						{ "width": "30%", "targets": [2,3,6], "class": 'text-left' },
						
						
					]
			});
	}
</script>