<div class="modal in" id="RincianDepositModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:80%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title titleRincian">RINCIAN DEPOSIT</h3>
				</div>
				<div class="block-content" style="margin-bottom: 10px;padding-top: 10px;">
					<a href="" target="_blank" id="historyPrintAll" class="btn btn-sm btn-success" style="width: 20%;margin-top: 10px;margin-bottom: 10px;float:right"><i class="fa fa-print"></i> Print All</a>
					<table id="historyDeposit" class="table table-bordered table-striped" style="margin-top: 10px;">
						<thead>
							<tr>
								<th>Tanggal</th>
								<th>Metode</th>
								<th>Nominal</th>
								<th>Terima Dari</th>
								<th>User Input</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
							<tr style="background-color:white;">
								<td colspan="4" style="text-align:right;text-transform:uppercase;"><b>Total</b></td>
								<td id="depositTotal" style="font-weight:bold;"></td>
								<td></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>