<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tsetoran_kas/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tsetoran_kas" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">TRANSAKSI SETORAN KAS</h3>
	</div>
	<div class="block-content" style="margin-top: -20px;font-size:">
		<div class="row">
			<div class="col-md-12">
				
			<input type="hidden" class="form-control" id="id" placeholder="ID" name="id" value="{id}" required="" aria-required="true">
			
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Tanggal Transaksi</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input" <?=$disabel?> <?=($id?'disabled':'')?> type="text" id="tanggal_trx" name="tanggal_trx" value="<?=$tanggal_trx?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="nama">Tanggal Setoran</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input" <?=$disabel?> type="text" id="tanggal_setoran" name="tanggal_setoran" value="<?=$tanggal_setoran?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
			</div>
			<?if ($id==''){?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama"></label>
				<div class="col-md-2">
				<button class="btn btn-success" type="button" id="btn_filter" style="font-size:13px;width:100%;float:right;" name="btn_filter"><i class="fa fa-filter"></i> Filter</button>
				</div>
			</div>
			<?}?>
			
			</div>
		</div>
		<div class="row hiden">
			<div class="col-md-12">
				<div class="block block-themed block-opt-hidden">
					<div class="block-header bg-primary">
						<ul class="block-options">

							<li>
								<button type="button" id="btn_rajal" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">PENDAPATAN RAWAT JALAN</h3>
					</div>
					<div class="block-content">
						<h4><span class="label label-success">PENDAPATAN TUNAI</span></h4><br>
						<table id="tabel_rajal" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 25%;">DESKRIPSI</th>
										<th style="width: 15%;">NOMINAL SISTEM</th>
										<th style="width: 15%;">NOMINAL VERIFIKASI</th>
										<th style="width: 20%;">REAL CASH</th>
										<th style="width: 25%;">ACTION</th>
									</tr>
									
								</thead>
								<tbody>
									<tr>
										<td>PENDAPATAN TUNAI RAJAL & FARMASI</td>
										<td><input type="text" readonly name="rj_tunai_sistem" id="rj_tunai_sistem" class="form-control number sm" value="0" ></td>
										<td><input type="text" readonly name="rj_tunai_verif" id="rj_tunai_verif" class="form-control sm number" value="0" ></td>
										<td><input type="text" name="rj_tunai_real" id="rj_tunai_real" class="form-control sm number" <?=$disabel?> value="{rj_tunai_real}" ></td>
										<td>
											<div class="btn-group">
											  <button class="btn btn-primary btn-sm detail" type="button"><i class="glyphicon glyphicon-menu-down"></i></button>
											  <button class="btn btn-info btn-sm detail_pasien_tunai_rj"  type="button"><i class="fa fa-list-ul"></i></button>
											  <button class="btn btn-success btn-sm "  type="button"><i class="fa fa-pencil"></i></button>
											  <button class="btn btn-success btn-sm "  type="button"><i class="fa fa-print"></i></button>
											</div>
										</td>
									</tr>
								</tbody>
								
								
						</table>
					</div>
					<div class="block-content" id="div_rajal_detail" hidden>
						<h4><span class="label label-info">DETAIL PENDAPATAN TUNAI</span></h4><br>
						<table id="tabel_rajal_detail" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 30%;">DESKRIPSI</th>
										<th style="width: 20%;">NOMINAL SISTEM</th>
										<th style="width: 20%;">NOMINAL VERIFIKASI</th>
										<th style="width: 20%;">ACTION</th>
									</tr>
									
								</thead>
								<tbody></tbody>
								
						</table>
					</div>
					
					<div class="block-content">
						<h4><span class="label label-success">PENDAPATAN NON TUNAI</span></h4><br>
						<table id="tabel_rajal_nt" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 25%;">DESKRIPSI</th>
										<th style="width: 25%;">NOMINAL SISTEM</th>
										<th style="width: 25%;">NOMINAL VERIFIKASI</th>
										<th style="width: 25%;">ACTION</th>
									</tr>
									
								</thead>
								<tbody>
									<tr>
										<td>PENDAPATAN DEBIT RAJAL & FARMASI</td>
										<td><input type="text" readonly name="rj_debit_sistem" id="rj_debit_sistem" class="form-control number sm" value="0" ></td>
										<td><input type="text" readonly name="rj_debit_verif" id="rj_debit_verif" class="form-control sm number" value="0" ></td>
										<td>
											<div class="btn-group">
											  <button class="btn btn-primary btn-sm detail_debit"  type="button"><i class="glyphicon glyphicon-menu-down"></i></button>
											  <button class="btn btn-info btn-sm detail_pasien_debit_rj" type="button"><i class="fa fa-list-ul"></i></button>
											  <button class="btn btn-success btn-sm " type="button"><i class="fa fa-pencil"></i></button>
											  <button class="btn btn-success btn-sm " type="button"><i class="fa fa-print"></i></button>
											</div>
										</td>
									</tr>
									<tr>
										<td>PENDAPATAN KREDIT RAJAL & FARMASI</td>
										<td><input type="text" readonly name="rj_kredit_sistem" id="rj_kredit_sistem" class="form-control number sm" value="0" ></td>
										<td><input type="text" readonly name="rj_kredit_verif" id="rj_kredit_verif" class="form-control sm number" value="0" ></td>
										<td>
											<div class="btn-group">
											  <button class="btn btn-primary btn-sm detail_kredit" type="button"><i class="glyphicon glyphicon-menu-down"></i></button>
											  <button class="btn btn-info btn-sm detail_pasien_kredit_rj" type="button"><i class="fa fa-list-ul"></i></button>
											  <button class="btn btn-success btn-sm " type="button"><i class="fa fa-pencil"></i></button>
											  <button class="btn btn-success btn-sm " type="button"><i class="fa fa-print"></i></button>
											</div>
										</td>
									</tr>
									<tr>
										<td>PENDAPATAN TRANSFER RAJAL & FARMASI</td>
										<td><input type="text" readonly name="rj_tf_sistem" id="rj_tf_sistem" class="form-control number sm" value="0" ></td>
										<td><input type="text" readonly name="rj_tf_verif" id="rj_tf_verif" class="form-control sm number" value="0" ></td>
										<td>
											<div class="btn-group">
											  <button class="btn btn-primary btn-sm detail_tf" type="button"><i class="glyphicon glyphicon-menu-down"></i></button>
											  <button class="btn btn-info btn-sm detail_pasien_tf_rj" type="button"><i class="fa fa-list-ul"></i></button>
											  <button class="btn btn-success btn-sm " type="button"><i class="fa fa-pencil"></i></button>
											  <button class="btn btn-success btn-sm " type="button"><i class="fa fa-print"></i></button>
											</div>
										</td>
									</tr>
								</tbody>
								
								
						</table>
					</div>
					<div class="block-content" id="div_rajal_detail_nt" hidden>
						<h4><span class="label label-info"><label id="lbl_det">DETAIL PENDAPATAN NON TUNAI</label></span></h4><br>
						<table id="tabel_rajal_detail_nt" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 30%;">DESKRIPSI</th>
										<th style="width: 20%;">BANK</th>
										<th style="width: 20%;">NOMINAL SISTEM</th>
										<th style="width: 20%;">NOMINAL VERIFIKASI</th>
										<th style="width: 20%;">ACTION</th>
									</tr>
									
								</thead>
								<tbody></tbody>
								
						</table>
					</div>
					<div class="block-content">
						<h4><span class="label label-danger">PENGELUARAN</span></h4><br>
						<table id="tabel_rajal_pengeluaran" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 10%;">NO REFUND</th>
										<th style="width: 10%;">TIPE REFUND</th>
										<th style="width: 10%;">CARA BAYAR</th>
										<th style="width: 15%;">NO. MEDREC</th>
										<th style="width: 20%;">PASIEN</th>
										<th style="width: 20%;">NOMINAL</th>
										<th style="width: 15%;">ACTION</th>
									</tr>
									
								</thead>
								<tbody>
									
								</tbody>
								
								
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row hiden">
			<div class="col-md-12">
				<div class="block block-themed block-opt-hidden">
					<div class="block-header bg-primary">
						<ul class="block-options">

							<li>
								<button type="button" id="btn_ranap" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">PENDAPATAN RAWAT INAP</h3>
					</div>
					<div class="block-content">
						<h4><span class="label label-success">PENDAPATAN TUNAI</span></h4><br>
						<table id="tabel_ranap" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 25%;">DESKRIPSI</th>
										<th style="width: 15%;">NOMINAL SISTEM</th>
										<th style="width: 15%;">NOMINAL VERIFIKASI</th>
										<th style="width: 20%;">REAL CASH</th>
										<th style="width: 25%;">ACTION</th>
									</tr>
									
								</thead>
								<tbody>
									<tr>
										<td>PENDAPATAN TUNAI RANAP & ODS</td>
										<td><input type="text" readonly name="ranap_tunai_sistem" id="ranap_tunai_sistem" class="form-control number sm" value="0" ></td>
										<td><input type="text" readonly name="ranap_tunai_verif" id="ranap_tunai_verif" class="form-control sm number" value="0" ></td>
										<td><input type="text" name="ranap_tunai_real" id="ranap_tunai_real" class="form-control sm number" <?=$disabel?> value="{ranap_tunai_real}" ></td>
										<td>
											<div class="btn-group">
											  <button class="btn btn-primary btn-sm detail_ranap" type="button"><i class="glyphicon glyphicon-menu-down"></i></button>
											  <button class="btn btn-info btn-sm " type="button"><i class="fa fa-list-ul"></i></button>
											  <button class="btn btn-success btn-sm " type="button"><i class="fa fa-pencil"></i></button>
											  <button class="btn btn-success btn-sm " type="button"><i class="fa fa-print"></i></button>
											</div>
										</td>
									</tr>
								</tbody>
						</table>
					</div>
					<div class="block-content" id="div_ranap_detail" hidden>
						<h4><span class="label label-info">DETAIL PENDAPATAN TUNAI</span></h4><br>
						<table id="tabel_ranap_detail" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 25%;">DESKRIPSI</th>
										<th style="width: 25%;">PASIEN</th>
										<th style="width: 12%;">NOMINAL SISTEM</th>
										<th style="width: 12%;">NOMINAL VERIFIKASI</th>
										<th style="width: 15%;">TOTAL</th>
										<th style="width: 13%;">ACTION</th>
									</tr>
									
								</thead>
								<tbody></tbody>
								
						</table>
					</div>
					
					<div class="block-content">
						<h4><span class="label label-success">PENDAPATAN NON TUNAI</span></h4><br>
						<table id="tabel_ranap_nt" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 25%;">DESKRIPSI</th>
										<th style="width: 25%;">NOMINAL SISTEM</th>
										<th style="width: 25%;">NOMINAL VERIFIKASI</th>
										<th style="width: 25%;">ACTION</th>
									</tr>
									
								</thead>
								<tbody>
									<tr>
										<td>PENDAPATAN DEBIT RANAP & ODS</td>
										<td><input type="text" readonly name="ranap_debit_sistem" id="ranap_debit_sistem" class="form-control number sm" value="0" ></td>
										<td><input type="text" readonly name="ranap_debit_verif" id="ranap_debit_verif" class="form-control sm number" value="0" ></td>
										<td>
											<div class="btn-group">
											  <button class="btn btn-primary btn-sm detail_debit_ranap" type="button"><i class="glyphicon glyphicon-menu-down"></i></button>
											  <button class="btn btn-info btn-sm " type="button"><i class="fa fa-list-ul"></i></button>
											  <button class="btn btn-success btn-sm " type="button"><i class="fa fa-pencil"></i></button>
											  <button class="btn btn-success btn-sm " type="button"><i class="fa fa-print"></i></button>
											</div>
										</td>
									</tr>
									<tr>
										<td>PENDAPATAN KREDIT RANAP & ODS</td>
										<td><input type="text" readonly name="ranap_kredit_sistem" id="ranap_kredit_sistem" class="form-control number sm" value="0" ></td>
										<td><input type="text" readonly name="ranap_kredit_verif" id="ranap_kredit_verif" class="form-control sm number" value="0" ></td>
										<td>
											<div class="btn-group">
											  <button class="btn btn-primary btn-sm detail_kredit_ranap" type="button"><i class="glyphicon glyphicon-menu-down"></i></button>
											  <button class="btn btn-info btn-sm " type="button"><i class="fa fa-list-ul"></i></button>
											  <button class="btn btn-success btn-sm " type="button"><i class="fa fa-pencil"></i></button>
											  <button class="btn btn-success btn-sm " type="button"><i class="fa fa-print"></i></button>
											</div>
										</td>
									</tr>
									<tr>
										<td>PENDAPATAN TRANSFER RANAP & ODS</td>
										<td><input type="text" readonly name="ranap_tf_sistem" id="ranap_tf_sistem" class="form-control number sm" value="0" ></td>
										<td><input type="text" readonly name="ranap_tf_verif" id="ranap_tf_verif" class="form-control sm number" value="0" ></td>
										<td>
											<div class="btn-group">
											  <button class="btn btn-primary btn-sm detail_tf_ranap" type="button"><i class="glyphicon glyphicon-menu-down"></i></button>
											  <button class="btn btn-info btn-sm " type="button"><i class="fa fa-list-ul"></i></button>
											  <button class="btn btn-success btn-sm " type="button"><i class="fa fa-pencil"></i></button>
											  <button class="btn btn-success btn-sm " type="button"><i class="fa fa-print"></i></button>
											</div>
										</td>
									</tr>
								</tbody>
								
								
						</table>
					</div>
					<div class="block-content" id="div_ranap_detail_nt" hidden>
						<h4><span class="label label-info"><label id="lbl_det_ranap">DETAIL PENDAPATAN NON TUNAI</label></span></h4><br>
						<table id="tabel_ranap_detail_nt" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 25%;">DESKRIPSI</th>
										<th style="width: 25%;">PASIEN</th>
										<th style="width: 12%;">NOMINAL SISTEM</th>
										<th style="width: 12%;">NOMINAL VERIFIKASI</th>
										<th style="width: 15%;">TOTAL</th>
										<th style="width: 13%;">ACTION</th>
									</tr>
									
								</thead>
								<tbody></tbody>
								
						</table>
					</div>
					<div class="block-content">
						<h4><span class="label label-danger">PENGELUARAN</span></h4><br>
						<table id="tabel_ranap_pengeluaran" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 10%;">NO REFUND</th>
										<th style="width: 10%;">TIPE REFUND</th>
										<th style="width: 10%;">CARA BAYAR</th>
										<th style="width: 15%;">NO. MEDREC</th>
										<th style="width: 20%;">PASIEN</th>
										<th style="width: 20%;">NOMINAL</th>
										<th style="width: 15%;">ACTION</th>
									</tr>
									
								</thead>
								<tbody>
									
								</tbody>
								
								
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row hiden">
			<div class="col-md-12">
				<div class="block block-themed block-opt-hidden">
					<div class="block-header bg-primary">
						<ul class="block-options">

							<li>
								<button type="button" id="btn_ranap" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">PENDAPATAN LAIN-LAIN</h3>
					</div>
					<div class="block-content">
						<h4><span class="label label-success">PENDAPATAN</span></h4><br>
						<table id="tabel_pendapatan_lain" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 25%;" hidden>ID</th>
										<th style="width: 15%;" hidden>TIPE</th>
										<th style="width: 10%;">NO PENDAPATAN</th>
										<th style="width: 10%;">TANGGAL</th>
										<th style="width: 20%;">DESKRIPSI</th>
										<th style="width: 20%;">DARI</th>
										<th style="width: 10%;">NOMINAL</th>
										<th style="width: 10%;">CARA BAYAR</th>
										<th style="width: 20%;">ACTION</th>
									</tr>
									
								</thead>
								<tbody>
									
								</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<?$this->load->view('Tsetoran_kas/tabel_retur');?>
		<?$this->load->view('Tsetoran_kas/tabel_hutang');?>
		<div class="row hiden">
			<div class="col-md-12">
				<!-- Static Labels block-opt-hidden -->
				<div class="block block-themed" >
					<div class="block-header bg-success">
						<ul class="block-options">
							<li>
								<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title"><i class="fa fa-money"></i>  PEMBAYARAN</h3>
					</div>
					<div class="block-content block-content">
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-2 control-label" for="nama">Real Cash Rawat Jalan</label>
								<div class="col-md-2">
									<input type="text" readonly class="form-control number" id="cash_rajal" placeholder="Real Cash" name="cash_rajal" value="{cash_rajal}">
								</div>
								<label class="col-md-2 control-label" for="nama">Pendapatan Lain-Lain</label>
								<div class="col-md-2">
									<input type="text" readonly class="form-control number" id="cash_pendapatan" placeholder="Real Cash" name="cash_pendapatan" value="{cash_pendapatan}">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-2 control-label" for="nama">Real Cash Rawat inap</label>
								<div class="col-md-2">
									<input type="text" readonly class="form-control number" id="cash_ranap" placeholder="Real Cash" name="cash_ranap" value="{cash_ranap}">
								</div>
								<label class="col-md-2 control-label" for="nama">Penerimaan Retur</label>
								<div class="col-md-2">
									<input type="text" readonly class="form-control number" id="cash_retur" placeholder="Retur" name="cash_retur" value="{cash_retur}">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-2 control-label" for="nama">Selisih Hutang & Piutang</label>
								<div class="col-md-2">
									<input type="text" readonly class="form-control number" id="cash_piutang" placeholder="Hutang Piutang" name="cash_piutang" value="{cash_piutang}">
								</div>
								<label class="col-md-2 control-label" for="nama">TOTAL</label>
								<div class="col-md-2">
									<input type="text" readonly class="form-control number" id="nominal" placeholder="Real Cash" name="nominal" value="{nominal}">
								</div>
								<div class="col-md-1">
									<button  type="button" class="btn btn-danger btn_hide"  <?=$disabel?>  type="button" id="btn_add_pembayaran" title="Add"><i class="fa fa-plus"></i> Pembayaran</button>
								</div>
							</div>
							
						
							
							
							<input type="hidden" class="form-control" id="rowindex">
							<input type="hidden" class="form-control" id="iddet">
							<div class="form-group">
								 <table class="table table-bordered" id="list_pembayaran">
									<thead>
										<tr>
											<th width="5%" class="text-center" hidden>NO</th>
											<th width="10%" class="text-center">Jenis Kas</th>
											<th width="15%" class="text-center">Sumber Kas</th>
											<th width="10%" class="text-center">Metode</th>
											<th width="10%" class="text-center" hidden>Tgl Pencairan</th>
											<th width="15%" class="text-center">Keterangan</th>
											<th width="10%" class="text-center">Nominal</th>
											<th width="15%" class="text-center">Actions</th>
											
										</tr>
									</thead>
									</thead>
									<tbody>	
										<? 
											$no=1;
											if ($list_pembayaran){
												foreach($list_pembayaran as $row){ ?>
													<tr>
													<td hidden><?=$no?></td>
													<td><?=$row->jenis_kas?></td>
													<td><?=$row->sumber_kas?></td>
													<td><?=metodePembayaran_bendahara($row->idmetode)?></td>
													<td hidden><?=$row->idmetode?></td>
													<td><?=$row->ket_pembayaran?></td>
													<td align="right"><?=number_format($row->nominal_bayar,0)?></td>
													<td><button type='button'  <?=$disabel?>  class='btn btn-info btn-xs pembayaran_edit' data-toggle='modal' data-target='#modal-pembayaran' title='Edit'><i class='fa fa-pencil'></i></button>&nbsp;&nbsp;<button type='button'  <?=$disabel?>  class='btn btn-danger btn-xs pembayaran_remove' title='Remove'><i class='fa fa-trash-o'></i></button></td>
													<td style='display:none'><input readonly type='text' class='form-control' name='xiddet[]' value='<?=$row->id?>'></td>
													<td style='display:none'><input readonly type='text' class='form-control' name='xjenis_kas_id[]' value='<?=$row->jenis_kas_id?>'></td>
													<td style='display:none'><input readonly type='text' class='form-control' name='xsumber_kas_id[]' value='<?=$row->sumber_kas_id?>'></td>
													<td style='display:none'><input readonly type='text' class='form-control' name='xidmetode[]' value='<?=$row->idmetode?>'></td>
													<td style='display:none'><input readonly type='text' class='form-control' name='ket[]' value='<?=$row->ket_pembayaran?>'></td>
													<td style='display:none'><input readonly type='text' class='form-control' name='xnominal_bayar[]' value='<?=$row->nominal_bayar?>'></td>
													<td style='display:none'><input readonly type='text' class='form-control' name='xstatus[]' value='1'></td>
													</tr>
												<?
												$no=$no+1;
												}
											}
										?>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="4" class="text-right"> <strong>Total Bayar </strong></td>
											<td  colspan="2"> <input class="form-control input number" readonly type="text" id="total_bayar" name="total_bayar" value="0"/></td>
										</tr>
										<tr>
											<td colspan="4" class="text-right"> <strong>Sisa</strong></td>
											<td  colspan="2"><input class="form-control input number" readonly type="text" id="total_sisa" name="total_sisa" value="0"/> </td>
										</tr>
									</tfoot>
								</table>
							</div>
							<? if ($disabel==''){ ?>
							<div class="form-group hiden btn_hide">
								<div class="text-right bg-light lter">
									<button class="btn btn-primary" type="submit" id="btn_simpan" name="btn_simpan">Simpan</button>
									<a href="{base_url}tsetoran_kas" class="btn btn-default" type="button">Batal</a>
								</div>
							</div>
							<?}?>
							
						</div> 
				</div>
				<!-- END Static Labels -->
			</div>
		</div>
		<div class="row notif" hidden>
			<div class="col-md-12">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h3 class="font-w300 push-15">Informasi</h3>
					<p>Opps, Tanggal Transaksi Sudah <a class="alert-link" href="{base_url}tsetoran_kas"> Pernah diinput</a>!</p>
				</div>
			</div>
		</div>
		<div class="modal fade in black-overlay" id="modal_pembayaran" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-popout">
				<div class="modal-content">
					<div class="block block-themed block-transparent remove-margin-b">
						<div class="block-header bg-success">
							<ul class="block-options">
								<li>
									<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
								</li>
							</ul>
							<h3 class="block-title">Pembayaran</h3>
						</div>
						<div class="block-content">
							
								<div class="form-group">
									<label class="col-md-2 control-label" for="nama">Sisa Rp. </label>
									<div class="col-md-9">
										<input readonly type="text" class="form-control number" id="sisa_modal" placeholder="Sisa" name="sisa_modal" required="" aria-required="true">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="jenis_kas_id">Jenis Kas</label>
									<div class="col-md-9">
										<select name="jenis_kas_id" id="jenis_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
											<option value="#">- Pilih Opsi -</option>
											<? foreach($list_jenis_kas as $row){ ?>
												<option value="<?=$row->id?>" <?=$row->st_default=='1'?'selected':''?>><?=$row->nama?></option>
											<?}?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="sumber_kas_id">Sumber Kas</label>
									<div class="col-md-9">
										<select name="sumber_kas_id" id="sumber_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">							
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="idmetode">Metode</label>
									<div class="col-md-9">
										<select name="idmetode" id="idmetode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
											<option value="#">Pilih Opsi</option>							
											<option value="1">Cheq</option>	
											<option value="2">Tunai</option>	
											<option value="4">Transfer Antar Bank</option>
											<option value="3">Transfer Beda Bank</option>							
										</select>
									</div>
								</div>
								
								
								<div class="form-group">
									<label class="col-md-2 control-label" for="nama">Nominal Rp. </label>
									<div class="col-md-9">
										<input  type="hidden" class="form-control" id="sumber_kas_id_tmp" placeholder="sumber_kas_id_tmp" name="sumber_kas_id_tmp" >
										<input  type="hidden" class="form-control" id="idpembayaran" placeholder="idpembayaran" name="idpembayaran" >
										<input  type="text" class="form-control number" id="nominal_bayar" placeholder="Nominal" name="nominal_bayar" >
									</div>
								</div>
								<div class="form-group" hidden>
									<label class="col-md-2 control-label" for="nama">Tanggal Pencairan</label>
									<div class="col-md-9">
										<div class="input-group date">
										<input class="js-datepicker form-control input"  type="text" id="tanggal_pencairan" name="tanggal_pencairan" value="" data-date-format="dd-mm-yyyy"/>
										<span class="input-group-addon">
											<span class="fa fa-calendar"></span>
										</span>
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-2 control-label" for="nama">Keterangan </label>
									<div class="col-md-9">
										<textarea class="form-control js-summernote" name="ket_pembayaran" id="ket_pembayaran"></textarea>
									</div>
								</div>

								<div class="modal-footer">
									<button class="btn btn-sm btn-primary" id="btn_add_bayar" type="button"><i class="fa fa-check"></i> Add</button>
									<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Exit</button>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade in black-overlay" id="modal_det_pasien_rajal" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-popout">
				<div class="modal-content">
					<div class="block block-themed block-transparent remove-margin-b">
						<div class="block-header bg-success">
							<ul class="block-options">
								<li>
									<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
								</li>
							</ul>
							<h3 class="block-title">DETAIL PASIEN</h3>
						</div>
						<div class="block-content">
								<div class="form-group">
									<table id="tabel_det_pasien_rajal" class="table table-striped table-bordered" style="margin-bottom: 0;">
											<thead>
												<tr>
													<th style="width: 25%;" hidden>ID</th>
													<th style="width: 15%;">NO TRANSAKSI</th>
													<th style="width: 10%;">NO MEDREC</th>
													<th style="width: 10%;">NAMA</th>
													<th style="width: 20%;">TUJUAN</th>
													<th style="width: 20%;">TOTAL TRANSAKSI</th>
													<th style="width: 10%;">NOMINAL BAYAR</th>
													<th style="width: 10%;">CARA BAYAR</th>
													<th style="width: 20%;">ACTION</th>
												</tr>
												
											</thead>
											<tbody>
												
											</tbody>
									</table>
								</div>

								<div class="modal-footer">
									<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Exit</button>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade in black-overlay" id="modal_det_pasien_rajal_bank" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-dialog-popout">
				<div class="modal-content">
					<div class="block block-themed block-transparent remove-margin-b">
						<div class="block-header bg-success">
							<ul class="block-options">
								<li>
									<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
								</li>
							</ul>
							<h3 class="block-title">DETAIL PASIEN</h3>
						</div>
						<div class="block-content">
								<div class="form-group">
									<table id="tabel_det_pasien_rajal_bank" class="table table-striped table-bordered" style="margin-bottom: 0;">
											<thead>
												<tr>
													<th style="width: 25%;" hidden>ID</th>
													<th style="width: 15%;">NO TRANSAKSI</th>
													<th style="width: 10%;">NO MEDREC</th>
													<th style="width: 10%;">NAMA</th>
													<th style="width: 20%;">TUJUAN</th>
													<th style="width: 20%;">TOTAL TRANSAKSI</th>
													<th style="width: 10%;">NOMINAL BAYAR</th>
													<th style="width: 10%;">CARA BAYAR</th>
												</tr>
												
											</thead>
											<tbody>
												
											</tbody>
									</table>
								</div>

								<div class="modal-footer">
									<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Exit</button>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close() ?>
</div>
<? $this->load->view('Tsetoran_kas/modal_deposit')?>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	var st_show_rajal_tunai;
	var st_show_ranap_tunai;
	$(document).ready(function(){
		st_show_rajal_tunai=0;
		$(".decimal").number(true,2,'.',',');
		$('.number').number(true, 0, '.', ',');
		st_show_ranap_tunai=0;
		load_awal_rajal();
		load_awal_ranap();
		load_pendapatan_lain();
		load_retur();
		load_hutang();
		// ket_pembayaran
		$('#ket_pembayaran').summernote({
			  height: 100,   //set editable area's height
			  codemirror: { // codemirror options
				theme: 'monokai'
			  }	
			})

	})
	function load_detail_pasien_rajal($cara_bayar){
		var cara_bayar=$cara_bayar;
		var tanggal_trx=$("#tanggal_trx").val();
		$('#tabel_det_pasien_rajal').DataTable().destroy();
		$('#tabel_det_pasien_rajal').DataTable({
				"autoWidth": false,
				"searching": true,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tsetoran_kas/load_detail_pasien_rajal',
					type: "POST",
					dataType: 'json',
					data : {
						tanggal_trx:tanggal_trx,
						cara_bayar:cara_bayar,
					   }
				},
				"columnDefs": [
						{ "visible": false, "targets": [0,8], "class": 'text-center' },
						{ "width": "10%", "targets": [5,6], "class": 'text-right'  },
						{ "width": "10%", "targets": [1,2,4,7], "class": 'text-center'  },
						{ "width": "20%", "targets": [3], "class": 'text-left' },
						
						
					]
			});
	}
	$(document).on("click",".detail_pasien_tunai_rj",function(){
		$("#modal_det_pasien_rajal").modal('show');
		load_detail_pasien_rajal(1);
	});
	$(document).on("click",".detail_pasien_debit_rj",function(){
		$("#modal_det_pasien_rajal").modal('show');
		load_detail_pasien_rajal(2);
	});
	$(document).on("click",".detail_pasien_kredit_rj",function(){
		$("#modal_det_pasien_rajal").modal('show');
		load_detail_pasien_rajal(3);
	});
	$(document).on("click",".detail_pasien_tf_rj",function(){
		$("#modal_det_pasien_rajal").modal('show');
		load_detail_pasien_rajal(4);
	});
	$(document).on("change","#tanggal_trx",function(){
		
		if ($("#id").val()==''){
			cek_sudah_ada();
			
		}
	});
	function cek_sudah_ada(){
		var tanggal_trx=$("#tanggal_trx").val();
		$.ajax({
			url: '{site_url}tsetoran_kas/cek_sudah_ada/',
			dataType: "json",
			type: "POST",
			data : {
					tanggal_trx:tanggal_trx,
				   },
			success: function(data) {
				// console.log(data);
				if (data!='0'){					
					$(".hiden").hide();
					$(".notif").show();
				}else{
					$(".hiden").show();
					$(".notif").hide();
				}
				$(".btn_hide").hide();
			}
		});
	}
	$(document).on("click","#btn_filter",function(){
		load_awal_rajal();
		load_awal_ranap();
		load_pendapatan_lain();
		load_retur();
		load_hutang();
		$(".btn_hide").show();
	});
	function load_awal_rajal(){
		var tanggal_trx=$("#tanggal_trx").val();
		
		$("#cover-spin").show();
		$('#div_rajal_detail').hide();
		$.ajax({
			url: '{site_url}tsetoran_kas/load_awal_rajal/',
			dataType: "json",
			type: "POST",
			data : {
					tanggal_trx:tanggal_trx,
				   },
			success: function(data) {
				// console.log(data);
				$('#cash_pendapatan').val(data.total_pendapatan_lain);
				$('#rj_tunai_sistem').val(data.tunai);
				$('#rj_debit_sistem').val(data.debit);
				$('#rj_kredit_sistem').val(data.kredit);
				$('#rj_tf_sistem').val(data.tf);
				$('#cash_retur').val(data.total_retur);
				$('#cash_piutang').val(data.total_piutang);
				set_total();
			}
		});
		get_rajal_refund();
		
		$("#cover-spin").hide();
		
	}
	
	
	$(document).on("click",".detail",function(){
		if (st_show_rajal_tunai==0){
			$('#div_rajal_detail').show()
			load_detial_rajal();
			st_show_rajal_tunai=1;
		}else{
			$('#div_rajal_detail').hide();
			st_show_rajal_tunai=0;
		}
	});
	$(document).on("click",".detail_debit",function(){		
		get_rajal_detail_non_tunai(2);
		$("#lbl_det").text('DETAIL DEBIT');
	});
	$(document).on("click",".detail_kredit",function(){		
		get_rajal_detail_non_tunai(3);
		$("#lbl_det").text('DETAIL KREDIT');
	});
	$(document).on("click",".detail_tf",function(){		
		get_rajal_detail_non_tunai(4);
		$("#lbl_det").text('DETAIL TRANSFER');
	});
	function load_pendapatan_lain(){
		var tanggal_trx=$("#tanggal_trx").val();
		$('#tabel_pendapatan_lain').DataTable().destroy();
		$('#tabel_pendapatan_lain').DataTable({
				"autoWidth": false,
				"searching": false,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tsetoran_kas/load_pendapatan_lain',
					type: "POST",
					dataType: 'json',
					data : {
						tanggal_trx:tanggal_trx,
					   }
				},
				"columnDefs": [
						{ "visible": false, "targets": [0,1], "class": 'text-center' },
						{ "width": "10%", "targets": [2,3,6,7], "class": 'text-center'  },
						{ "width": "20%", "targets": [4,5,8], "class": 'text-left' },
						
						
					]
			});
	}
	
	
	function load_detial_rajal(){
		// alert('det');
		st_show_rajal_tunai=1;
		var tanggal_trx=$("#tanggal_trx").val();
		$('#tabel_rajal_detail').DataTable().destroy();
		$('#tabel_rajal_detail').DataTable({
				"autoWidth": false,
				"searching": false,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tsetoran_kas/get_rajal_detail',
					type: "POST",
					dataType: 'json',
					data : {
						tanggal_trx:tanggal_trx,
					   }
				},
				"columnDefs": [
						{ "width": "25%", "targets": 0, "class": 'text-center' },
						{ "width": "25%", "targets": 1, "class": 'text-right'  },
						{ "width": "25%", "targets": 2, "class": 'text-right' },
						{ "width": "25%", "targets": 3, "class": 'text-left	'  },
						
						
					]
			});
	}
	function get_rajal_refund(){
		// st_show_rajal_tunai=1;
		var tanggal_trx=$("#tanggal_trx").val();
		$('#tabel_rajal_pengeluaran').DataTable().destroy();
		$('#tabel_rajal_pengeluaran').DataTable({
				"autoWidth": false,
				"searching": false,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tsetoran_kas/get_rajal_refund',
					type: "POST",
					dataType: 'json',
					data : {
						tanggal_trx:tanggal_trx,
					   }
				},
				"columnDefs": [
						{ "width": "10%", "targets": 0, "class": 'text-center' },
						{ "width": "10%", "targets": 1, "class": 'text-center'  },
						{ "width": "10%", "targets": 2, "class": 'text-center' },
						{ "width": "15%", "targets": 3, "class": 'text-center	'  },
						{ "width": "20%", "targets": 4, "class": 'text-left'  },
						{ "width": "20%", "targets": 5, "class": 'text-right'  },
						{ "width": "15%", "targets": 6, "class": 'text-left	'},
						
						
					]
			});
	}
	function get_rajal_detail_non_tunai($idmetode='2'){
		// st_show_rajal_tunai=1;
		$("#div_rajal_detail_nt").show();
		var tanggal_trx=$("#tanggal_trx").val();
		var idmetode=$idmetode;
		$('#tabel_rajal_detail_nt').DataTable().destroy();
		$('#tabel_rajal_detail_nt').DataTable({
				"autoWidth": false,
				"searching": false,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tsetoran_kas/get_rajal_detail_non_tunai',
					type: "POST",
					dataType: 'json',
					data : {
						tanggal_trx:tanggal_trx,idmetode:idmetode,
					   }
				},
				"columnDefs": [
						{ "width": "20%", "targets": 0, "class": 'text-center' },
						{ "width": "20%", "targets": 1, "class": 'text-center'  },
						{ "width": "20%", "targets": 2, "class": 'text-right' },
						{ "width": "20%", "targets": 3, "class": 'text-right	'  },
						{ "width": "20%", "targets": 4, "class": 'text-left	'  },
					]
			});
	}
	function load_detail_pasien_rajal_bank($idtipe,$idbank,$idmetode='2'){
		// st_show_rajal_tunai=1;
		$("#modal_det_pasien_rajal_bank").modal('show');
		var tanggal_trx=$("#tanggal_trx").val();
		var idmetode=$idmetode;
		var idtipe=$idtipe;
		var idbank=$idbank;
		$('#tabel_det_pasien_rajal_bank').DataTable().destroy();
		$('#tabel_det_pasien_rajal_bank').DataTable({
				"autoWidth": false,
				"searching": false,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tsetoran_kas/load_detail_pasien_rajal_bank',
					type: "POST",
					dataType: 'json',
					data : {
						tanggal_trx:tanggal_trx,idmetode:idmetode,idtipe:idtipe,idbank:idbank,
					   }
				},
				"columnDefs": [
						{ "visible": false, "targets": [0], "class": 'text-center' },
						{ "width": "10%", "targets": [5,6], "class": 'text-right'  },
						{ "width": "10%", "targets": [1,2,4,7], "class": 'text-center'  },
						{ "width": "20%", "targets": [3], "class": 'text-left' },
						
						
					]
			});
	}
	function load_awal_ranap(){
		var tanggal_trx=$("#tanggal_trx").val();
		st_show_rajal_tunai=0;
		$("#cover-spin").show();
		$('#div_rajal_detail').hide();
		$.ajax({
			url: '{site_url}tsetoran_kas/load_awal_ranap/',
			dataType: "json",
			type: "POST",
			data : {
					tanggal_trx:tanggal_trx,
				   },
			success: function(data) {
				// console.log(data);
				// alert(data);
				// alert((data.tunai) + " " + (data.dep_tunai));
				$('#ranap_tunai_sistem').val(parseFloat(data.tunai));
				$('#ranap_tunai_verif').val(parseFloat(data.tunai));
				
				$('#ranap_debit_sistem').val(parseFloat(data.debit));
				$('#ranap_debit_verif').val(parseFloat(data.debit));
				
				$('#ranap_kredit_sistem').val(parseFloat(data.kredit));
				$('#ranap_kredit_verif').val(parseFloat(data.kredit));
				
				
				$('#ranap_tf_sistem').val(parseFloat(data.tf));
				$('#ranap_tf_verif').val(parseFloat(data.tf));
				
			}
		});
		get_ranap_refund();
		
		$("#cover-spin").hide();
		
	}	
	$(document).on("click",".detail_ranap",function(){		
		if (st_show_ranap_tunai==0){
			$('#div_ranap_detail').show()
			load_detial_ranap();
			st_show_ranap_tunai=1;
		}else{
			$('#div_ranap_detail').hide();
			st_show_ranap_tunai=0;
		}
	});
	$(document).on("keyup","#rj_tunai_real",function(){		
		$("#cash_rajal").val($("#rj_tunai_real").val());
		set_total()
	});
	$(document).on("keyup","#ranap_tunai_real",function(){		
		$("#cash_ranap").val($("#ranap_tunai_real").val());
		set_total()
	});
	$(document).on("keyup","#nominal_bayar",function(){		
		if (parseFloat($("#nominal_bayar").val()) > parseFloat($("#sisa_modal").val())){
			$("#nominal_bayar").val($("#sisa_modal").val());
		}
	});
	
	function set_total(){
		var total_all=0;
		total_all=parseFloat($("#cash_rajal").val()) + parseFloat($("#cash_ranap").val()) + parseFloat($("#cash_pendapatan").val()) + parseFloat($("#cash_retur").val()) + parseFloat($("#cash_piutang").val());
		$("#nominal").val(total_all);
		checkTotal();
	}
	function load_detial_ranap(){
		
		var tanggal_trx=$("#tanggal_trx").val();
		// alert(id);
		$.ajax({
			url: '{site_url}tsetoran_kas/get_ranap_detail',
			type: "POST",
			data : {
						tanggal_trx:tanggal_trx,
					   },
			dataType: "json",
			success: function(data) {
				// if (data.detail!=''){
					$('#tabel_ranap_detail tbody').empty();
					$("#tabel_ranap_detail tbody").append(data.detail);
				// }
				// console.log();
				
			}
		});
	}
	$(document).on("click",".detail_debit_ranap",function(){		
		get_ranap_detail_non_tunai(2);
		$("#lbl_det_ranap").text('DETAIL DEBIT');
	});
	$(document).on("click",".detail_kredit_ranap",function(){		
		get_ranap_detail_non_tunai(3);
		$("#lbl_det_ranap").text('DETAIL KREDIT');
	});
	$(document).on("click",".detail_tf_ranap",function(){		
		get_ranap_detail_non_tunai(4);
		$("#lbl_det_ranap").text('DETAIL TRANSFER');
	});
	function get_ranap_detail_non_tunai($idmetode='2'){
		$("#div_ranap_detail_nt").show();
		var tanggal_trx=$("#tanggal_trx").val();
		var idmetode=$idmetode;
		$.ajax({
			url: '{site_url}tsetoran_kas/get_ranap_detail_non_tunai',
			type: "POST",
			data : {
						tanggal_trx:tanggal_trx,idmetode:idmetode,
					   },
			dataType: "json",
			success: function(data) {
				// if (data.detail!=''){
					$('#tabel_ranap_detail_nt tbody').empty();
					$("#tabel_ranap_detail_nt tbody").append(data.detail);
				// }
				// console.log();
				
			}
		});
	}
	function get_ranap_refund(){
		// st_show_rajal_tunai=1;
		var tanggal_trx=$("#tanggal_trx").val();
		$('#tabel_ranap_pengeluaran').DataTable().destroy();
		$('#tabel_ranap_pengeluaran').DataTable({
				"autoWidth": false,
				"searching": false,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}tsetoran_kas/get_ranap_refund',
					type: "POST",
					dataType: 'json',
					data : {
						tanggal_trx:tanggal_trx,
					   }
				},
				"columnDefs": [
						{ "width": "10%", "targets": 0, "class": 'text-center' },
						{ "width": "10%", "targets": 1, "class": 'text-center'  },
						{ "width": "10%", "targets": 2, "class": 'text-center' },
						{ "width": "15%", "targets": 3, "class": 'text-center	'  },
						{ "width": "20%", "targets": 4, "class": 'text-left'  },
						{ "width": "20%", "targets": 5, "class": 'text-right'  },
						{ "width": "15%", "targets": 6, "class": 'text-left	'},
						
						
					]
			});
	}
	//PEMBAYARAN
	
	$(document).on("click","#btn_add_pembayaran",function(){
		// if ($("#nominal").val()=='' || $("#nominal").val()=='0'){
			// return false;
		// }
		$("#modal_pembayaran").modal('show');
		$("#jenis_kas_id").val($("#jenis_kas_id").val()).trigger('change');
		$("#idmetode").val(4).trigger('change');
		$("#total_sisa").val(parseFloat($("#nominal").val()) - parseFloat($("#total_bayar").val()));
		$("#sisa_modal").val($("#total_sisa").val());
		$("#nominal_bayar").val($("#total_sisa").val());
		
	});
	$(document).on("change","#jenis_kas_id",function(){
		// alert('soo');
		var jenis_kas_id=$("#jenis_kas_id").val();
		var sumber_kas_id=$("#sumber_kas_id_tmp").val();
		$.ajax({
			url: '{site_url}mrka_bendahara/list_sumber_kas',
			type: 'POST',
			dataType: "json",
			data: {jenis_kas_id: jenis_kas_id,sumber_kas_id: sumber_kas_id},
			success : function(data) {				
				$("#sumber_kas_id").empty();	
				// console.log(data.detail);
				// $("#sumber_kas_id").append(data.detail);	
				$('#sumber_kas_id').append(data.detail);
			}
		});
	});
	$(document).on("click","#btn_add_bayar",function(){
		add_pembayaran();
	});
	function add_pembayaran(){
			
		if ($("#jenis_kas_id").val() == "#") {
			sweetAlert("Maaf...", "Jenis Kas Harus Diisi!", "error");
			$("#jenis_kas_id").focus();
			return false;
		}
		if ($("#sumber_kas_id").val() == "#") {
			sweetAlert("Maaf...", "Sumber Kas Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		if ($("#idmetode").val() == "#" || $("#idmetode").val() == "") {
			sweetAlert("Maaf...", "Metode  Harus Diisi!", "error");
			$("#idmetode").focus();
			return false;
		}
		if ($("#nominal_bayar").val() == "" || $("#nominal_bayar").val() == "0") {
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		var content = "";

		var row_index;
		var duplicate = false;

		if($("#rowindex").val() != ''){
			var number = $("#number").val();
			var content = "";
			row_index = $("#rowindex").val();
		}else{
			var number = $('#list_pembayaran tr').length;
			var content = "<tr>";
			// alert($("#idmetode option:selected").text());
			$('#list_pembayaran tbody tr').filter(function (){
				var $cells = $(this).children('td');
				// console.log($cells.eq(1).text());
				if($cells.eq(1).text() === $("#idmetode option:selected").text()){
					sweetAlert("Maaf...", $("#pembayaran_jenis option:selected").text() + " sudah ditambahkan.", "error");
					duplicate = true;

					pembayaran_clear();
					return false;
				}
			});
		}

		if(duplicate == false){
			var ket=$('#ket_pembayaran').summernote('code');
			content += "<td style='display:none'>" + number + "</td>";
			content += "<td>" + $("#jenis_kas_id option:selected").text() + "</td>";
			content += "<td>" + $("#sumber_kas_id option:selected").text() + "</td>";
			content += "<td>" + $("#idmetode option:selected").text() + "</td>";
			content += "<td style='display:none'>-</td>";
			content += "<td>" + ket + "</td>";
			content += "<td align='right'>" + formatNumber($("#nominal_bayar").val()) + "</td>";
			content += "<td><button type='button' class='btn btn-info btn-xs pembayaran_edit' data-toggle='modal' data-target='#modal-pembayaran' title='Edit'><i class='fa fa-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-danger btn-xs pembayaran_remove' title='Remove'><i class='fa fa-trash-o'></i></button></td>";
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xiddet[]' value='"+$("#iddet").val()+"'></td>";//8
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xjenis_kas_id[]' value='"+$("#jenis_kas_id").val()+"'></td>";//9
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xsumber_kas_id[]' value='"+$("#sumber_kas_id").val()+"'></td>";//10
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xidmetode[]' value='"+$("#idmetode").val()+"'></td>";//11
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='ket[]' value='"+ket+"'></td>";//12
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xnominal_bayar[]' value='"+$("#nominal_bayar").val()+"'></td>";//13
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xstatus[]' value='1'></td>";//14
			if($("#rowindex").val() != ''){
				$('#list_pembayaran tbody tr:eq(' + row_index + ')').html(content);
			}else{
				content += "</tr>";
				$('#list_pembayaran tbody').append(content);
			}

			pembayaran_clear();
			checkTotal();
		}
		
		
	}
	$(document).on("click",".pembayaran_edit",function(){
		$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
		$("#modal_pembayaran").modal('show');
		$("#iddet").val($(this).closest('tr').find("td:eq(8) input").val());
		$("#sumber_kas_id_tmp").val($(this).closest('tr').find("td:eq(10) input").val());
		$("#jenis_kas_id").val($(this).closest('tr').find("td:eq(9) input").val()).trigger('change');
		$("#nominal_bayar").val($(this).closest('tr').find("td:eq(13) input").val());
		$("#ket_pembayaran").summernote('code',$(this).closest('tr').find("td:eq(12) input").val());
		$("#idmetode").val($(this).closest('tr').find("td:eq(11) input").val()).trigger('change');
		$("#sisa_modal").val(parseFloat($("#total_sisa").val()) + parseFloat($("#nominal_bayar").val()));
		// sisa_modal($();
		// $("#pembayaran_id").val($(this).closest('tr').find("td:eq(0)").val());

		
		return false;
	});
	$(document).on("click", ".pembayaran_remove", function() {
		var tr=$(this);
			swal({
				title: "Apakah Anda Yakin ?",
				text : "Akan Menghapus Pendapatan ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				if ($("#id").val()==''){
					tr.closest('td').parent().remove();
						
				}else{
					tr.closest('tr').find("td:eq(14) input").val(0)
					tr.closest('tr').find("td:eq(7)").html('<span class="label label-danger">DELETED</span>')
				}
				checkTotal();
			});

			// if (confirm("Hapus data ?") == true) {
				
			// }
		// return false;
	});
	function formatNumber (num) {
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}
	function checkTotal(){
		var total_pembayaran = 0;
		$('#list_pembayaran tbody tr').each(function() {
			if ($(this).closest('tr').find("td:eq(14) input").val()=='1'){
			total_pembayaran += parseFloat($(this).find('td:eq(6)').text().replace(/\,/g, ""));
			}
		});
		$("#total_bayar").val(total_pembayaran);
		$("#total_sisa").val(parseFloat($("#nominal").val()) - parseFloat(total_pembayaran));
	}
	function pembayaran_clear(){
		$("#sumber_kas_id_tmp").val('');
		$("#number").val('');
		$("#rowindex").val('');
		$("#modal_pembayaran").modal('hide');
		clear_input_pembayaran();
	}
	function clear_input_pembayaran(){
		$("#jenis_kas_id").val("#").trigger('change');
		$("#sumber_kas_id").val("#").trigger('change');
		$("#nominal_bayar").val(0);
		$("#sisa_modal").val(0);
		$("#idpembayaran").val('');
		$("#idmetode").val("#").trigger('change');
		$("#ket_pembayaran").summernote('code','');
		$("#iddet").val('');
	}
	function validate_final(){
		// if ($("#idpendapatan").val() == "#") {
			// sweetAlert("Maaf...", "Pendapatan tidak boleh kosong!", "error");
			// return false;
		// }
		// if ($("#terimadari").val() == "#") {
			// sweetAlert("Maaf...", "Sumber Pendapatan tidak boleh kosong!", "error");
			// return false;
		// }
		// if ($("#nominal").val() == "" || $("#nominal").val() == "0") {
			// sweetAlert("Maaf...", "Nominal tidak boleh kosong!", "error");
			// return false;
		// }
		if ($("#total_bayar").val() == "0") {
			sweetAlert("Maaf...", "Pembayaran Belum Dipilih!", "error");
			return false;
		}
		if ($("#total_sisa").val() != "0") {
			sweetAlert("Maaf...", "Masih Ada Sisa!", "error");
			return false;
		}
	}
	
	function goBack() {
	  window.history.back();
	}
	function getHistoryDeposit(idpendaftaran) {
		$("#RincianDepositModal").modal('show');
		$("#historyPrintAll").attr('href', '{site_url}trawatinap_tindakan/print_rincian_deposit/' + idpendaftaran);
		$('#historyDeposit tbody').empty();
		$("#depositTotal").html(0);
		$.ajax({
			url: '{site_url}trawatinap_tindakan/getHistoryDeposit/' + idpendaftaran,
			dataType: 'json',
			success: function(data) {
				
				for (var i = 0; i < data.length; i++) {
					var number = (i+1);
					var metode = '';
					if(data[i].idmetodepembayaran == 1){
						metode = data[i].metodepembayaran;
					}else{
						metode = data[i].metodepembayaran + ' ( ' + data[i].namabank+ ' )';
					}
					var action = '';
					action = '<div class="btn-group">';
					action += '<a href="{site_url}trawatinap_tindakan/print_kwitansi_deposit/' + data[i].id + '" target="_blank" class="btn btn-sm btn-success">Print <i class="fa fa-print"></i></a>';
					action += '</div>';

					$('#historyDeposit tbody').append('<tr><td>' + data[i].tanggal + '</td><td>' + metode + '</td><td>' + $.number(data[i].nominal) + '</td><td>' + (data[i].terimadari ? data[i].terimadari : "-")  + '</td><td>' + data[i].namapetugas + '</td><td>' + action + '</td></tr>');
				}

				getTotalHistoryDeposit();
			}
		});
	}
</script>