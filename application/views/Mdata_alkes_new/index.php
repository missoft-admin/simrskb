<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('107'))){ ?>
<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('87'))){ ?>
		<ul class="block-options">
			<li>
				<a href="{base_url}mdata_alkes_new/create" class="btn"><i class="fa fa-plus"></i></a>
			</li>
		</ul>
		<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?php if (UserAccesForm($user_acces_form,array('108'))){ ?>
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<hr style="margin-top:0px">
		<?php echo form_open('mdata_alkes_new/filter','class="form-horizontal" id="form-work"') ?>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien">Nama Alkes</label>
					<div class="col-md-8">
						<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien">Merk</label>
					<div class="col-md-8">
						<select tabindex="8" id="merk" class="js-select2 form-control " style="width: 100%;" multiple>
							<?foreach(list_variable_ref(93) as $row){?>
								<option value="<?=$row->id?>" ><?=$row->nama?></option>
							<?}?>
							
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;" hidden>
					<label class="col-md-4 control-label" for="asalpasien">Jenis Penggunaan</label>
					<div class="col-md-8">
						<select tabindex="8" id="idtipe" name="idtipe" class="js-select2 form-control js_95" style="width: 100%;" multiple>
							<?foreach(list_variable_ref(95) as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>
							<?}?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien">Bentuk Sediaan</label>
					<div class="col-md-8">
						<select tabindex="8" id="bentuk_sediaan" class="js-select2 form-control " style="width: 100%;" multiple>
							<?foreach(list_variable_ref(94) as $row){?>
								<option value="<?=$row->id?>" ><?=$row->nama?></option>
							<?}?>
							
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-3 control-label" for="asalpasien">Kategori</label>
					<div class="col-md-8">
						<select name="idkategori" id="idkategori" class="js-select2 form-control" style="width: 100%;" data-placeholder=" - All Kategori - ">
							<option value="0" <?=($idkategori == '0') ? "selected" : "" ?>>- All Kategori -</option>
							<?php foreach  ($list_kategori as $row) { ?>
								<option value="<?=$row->path;?>" <?=($idkategori == $row->path) ? "selected" : "" ?>><?=TreeView($row->level, $row->nama)?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-3 control-label" for="asalpasien">Status</label>
					<div class="col-md-8">
						<select tabindex="8" id="status" class="js-select2 form-control " style="width: 100%;">
							<option value="#">-Semua-</option>
							<option value="1" selected>Aktif</option>
							<option value="0" >Tidak Aktif</option>
						</select>
					</div>
				</div>
				<?php if (UserAccesForm($user_acces_form,array('87'))){ ?>
				<div class="form-group" style="margin-top: 15px;">
					<label class="col-md-3 control-label" for="asalpasien"></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="button" onclick="getIndex()" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
				<?}?>
			</div>
		</div>
		<?php echo form_close() ?>
		<hr>
		<?}?>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>Kode</th>
					<th>Nama</th>
					<th>Kategori</th>
					<th>Harga Besar</th>
					<th>Harga Kecil</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		</div>
	</div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	getIndex();
});
function getIndex(){
		
	let status=$("#status").val();
	let bentuk_sediaan=$("#bentuk_sediaan").val();
	let nama=$("#nama").val();
	let idkategori=$("#idkategori").val();
	let idtipe=$("#idtipe").val();
	let merk=$("#merk").val();
	$('#datatable-simrs').DataTable().destroy();	
	// $("#cover-spin").show();
	table = $('#datatable-simrs').DataTable({
			autoWidth: false,
			searching: true,
			serverSide: true,
			"processing": false,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": true },
					{ "width": "10%", "targets": 1, "orderable": true },
					{ "width": "25%", "targets": 2, "orderable": true },
					{ "width": "15%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 3, "orderable": true },
					{ "width": "15%", "targets": 4, "orderable": true }
				],
			ajax: { 
				url: '{site_url}mdata_alkes_new/getIndex', 
				type: "POST" ,
				dataType: 'json',
				data : {
						idkategori:idkategori,
						merk:merk,
						idtipe:idtipe,
						nama:nama,
						bentuk_sediaan:bentuk_sediaan,
						status:status,
						
					   }
			},
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
			 }  
		});
}

</script>
