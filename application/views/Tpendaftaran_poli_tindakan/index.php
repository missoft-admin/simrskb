<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<?
$user_id=$this->session->userdata('user_id');
?>
<div class="block push-10">
	<div class="block-content bg-primary">
			<input type="hidden" id="tab" name="tab" value="{tab}">
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<input type="hidden" id="st_login" name="st_login" value="{st_login}">
			<div class="row pull-10">
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="login1-password">Ruangan</label>
						<div class="col-xs-12">
							<select id="ruangan_id" name="ruangan_id" <?=($st_login=='1'?'disabled':'')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" <?=($ruangan_id=='#'?'selected':'')?>>- Pilih Ruangan -</option>
								<?foreach($list_ruangan as $r){?>
								<option value="<?=$r->id?>" <?=($ruangan_id==$r->id?'selected':'')?> <?=($r->st_login=='1' && $r->user_login !=$user_id?'disabled':'')?>><?=$r->nama_tujuan.($r->st_login?' (Login : '.$r->user_nama_login.')':'')?></option>
								<?}?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-2 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">Poliklinik</label>
						<div class="col-xs-12">
							<select id="idpoli" name="idpoli" <?=($st_login=='1'?'disabled':'')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" <?=($idpoli=='#'?'selected':'')?>>- Semua Poliklinik -</option>
								<?foreach($list_poli as $r){?>
								<option value="<?=$r->id?>" <?=($idpoli==$r->id?'selected':'')?>><?=$r->nama?></option>
								<?}?>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">Dokter</label>
						<div class="col-xs-12">
							<select id="iddokter" name="iddokter" <?=($st_login=='1'?'disabled':'')?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" <?=($iddokter=='#'?'selected':'')?>>- Semua Dokter -</option>
								<?foreach($list_dokter as $r){?>
								<option value="<?=$r->id?>" <?=($iddokter==$r->id?'selected':'')?>><?=$r->nama?></option>
								<?}?>
								
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-2 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">Tanggal</label>
						<div class="col-xs-12">
							<div class="js-datepicker input-group date">
								<input class="js-datepicker form-control div_tidak_dikenal" type="text" id="tanggal" name="tanggal" data-date-format="dd-mm-yyyy"  value="{tanggal}" placeholder="Tanggal Layanan">
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-2 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">&nbsp;&nbsp;</label>
						<div class="col-xs-12">
							<span class="input-group-btn ">
								<button class="btn btn-success" id="btn_login" type="button" title="Login"><i class="si si-login"></i> LogIn</button>&nbsp;
								<button class="btn btn-danger" type="button" title="Logout"  id="btn_logout"><i class="si si-logout"></i> Logout</button>&nbsp;
							</span>
						</div>
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
	</div>
</div>
<?if ($st_login=='0'){?>
	<div class="alert alert-danger alert-dismissable">
		<?=$this->session->userdata('user_name')?> Anda Belum Login
		
	</div>

<?}?>
<div class="block">
	<ul class="nav nav-pills">
		<li id="div_1" class="<?=($tab=='1'?'active':'')?>">
			<a href="javascript:void(0)" onclick="set_tab(1)"><i class="si si-list"></i> Semua</a>
		</li>
		<li  id="div_2" class="<?=($tab=='2'?'active':'')?>">
			<a href="javascript:void(0)"  onclick="set_tab(2)"><i class="si si-pin"></i> Menunggu Antrian </a>
		</li>
		<li  id="div_3" class="<?=($tab=='3'?'active':'')?>">
			<a href="javascript:void(0)" onclick="set_tab(3)"><i class="fa fa-check-square-o"></i> Telah Dilayani</a>
		</li>
		<li  id="div_4" class="<?=($tab=='4'?'active':'')?>">
			<a href="javascript:void(0)"  onclick="set_tab(4)"><i class="fa fa-times"></i> Terlewati</a>
		</li>
	</ul>
	<div class="block-content">
			<input type="hidden" id="tab" name="tab" value="{tab}">
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row pull-10">
				
				<div class="col-md-3">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">No Medrec</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="no_medrec" placeholder="No. Medrec" name="no_medrec" value="{no_medrec}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Nama Pasien</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="namapasien" placeholder="Nama Pasien" name="namapasien" value="{namapasien}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Status</label>
						<div class="col-md-9">
							<select id="status_transaksi" name="status_transaksi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="1" selected>- Semua Status -</option>
								<option value="2">MENUNGGU ANTRIAN</option>
								<option value="3">TELAH DILAYANI</option>
								<option value="4">TERLEWATI</option>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label" for="btn_filter_all"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="button" id="btn_filter_all" name="btn_filter_all" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						</div>
					</div>
					
				</div>
				<div class="col-md-9 push-10">
					<div class="form-group push-5 div_counter" >
						<div class="col-sm-3">
							<div class="block block-themed">
								<div class="block-header bg-danger" style="border-bottom: 2px solid white;padding:10">
									<h3 class="block-title text-center" >ANTRIAN DILAYANI</h3>
								</div>
								<div class="content-mini content-mini-full bg-danger">
									<div>
										<div class="h2 text-center text-white"></div>
										<div class="h5 text-center text-white"></div>
										<div class="h5 text-center text-white push-10"></div>
										<div class="text-center ">
										</div>
									</div>
									<div class="text-center ">
											<button class="btn btn-block btn-success" type="button" disabled onclick="recall_manual()" title="Panggil Ulang"><i class="si si-refresh pull-left"></i> PANGGIL ULANG</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="block block-themed">
								<div class="block-header bg-success" style="border-bottom: 2px solid white;padding:10">
									<h3 class="block-title text-center" >ANTRIAN SELANJUTNYA</h3>
								</div>
								<div class="content-mini content-mini-full bg-success">
									<div>
										<div class="h2 text-center text-white"></div>
										<div class="h5 text-center text-white"></div>
										<div class="h5 text-center text-white push-10"></div>
									</div>
									<div class="text-center ">
										<button class="btn btn-block btn-primary " disabled onclick="call_antrian_manual()" title="Lanjut Panggil" type="button"><i class="glyphicon glyphicon-volume-up pull-left"></i> SELANJUTNYA</button>
									</div>
								</div>
								
							</div>
							
						</div>
						<div class="col-sm-3">
							<div class="block block-themed">
								<div class="block-header bg-success" style="border-bottom: 2px solid white;padding:10">
									<h3 class="block-title text-center" >SISA ANTRIAN</h3>
								</div>
								<div class="content-mini content-mini-full bg-success">
									<div>
										<div class="h2 text-center text-white"></div>
										<div class="h5 text-center text-white">&nbsp;</div>
										<div class="h5 text-center text-white push-10">&nbsp;</div>
									</div>
									<div class="text-center ">
										<button class="btn btn-block btn-warning" type="button" disabled onclick="tf_manual()"  title="LEWATI"><i class="fa fa-mail-forward pull-left"></i> LEWATI</button>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="block block-themed">
								<div class="block-header bg-success" style="border-bottom: 2px solid white;padding:10">
									<h3 class="block-title text-center" >TOTAL ANTRIAN</h3>
								</div>
								<div class="content-mini content-mini-full bg-success">
									<div>
										<div class="h1 font-w700 text-center text-white">5</div>
									</div>
								</div>
							</div>
						</div>
						
						
							
					</div>
					
				</div>
			</div>
			<?php echo form_close() ?>
			<div class="row   ">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="index_all">
							<thead>
								<tr>
									<th width="10%">No</th>
									<th width="10%">Antrian / No Pendaftaran</th>
									<th width="10%">Tanggal Kunjungan</th>
									<th width="10%">Pasien</th>
									<th width="10%">Alamat</th>
									<th width="10%">Poliklinik / Dokter</th>
									<th width="10%">Kelompok Pasien</th>
									<th width="10%">Pemeriksaan Penunjang</th>
									<th width="10%">Status Antrian</th>
									<th width="10%">Status Tindakan</th>
									<th width="15%">Action</th>
								   
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		
	
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
var tab;
var st_login;

$(document).ready(function(){	
	load_index_all();
	tab=$("#tab").val();
	st_login=$("#st_login").val();
	// $("#div_1").classlist.remove("active");
	cek_login();
	set_tab(tab);
	startWorker();
});
function call_antrian_manual(antrian_id){
	let ruangan_id=$("#ruangan_id").val();
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}tpendaftaran_poli_tindakan/call_antrian',
		type: 'POST',
		dataType: "json",
		data: {
			ruangan_id: ruangan_id,
			antrian_id: antrian_id,
		
		},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==false){
				swal({
					title: "Maaf!",
					text: "No Antrian Tidak tersedia",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				load_index_all();
			}
		}
	});
}
function call_lewati(antrian_id,antrian_id_next){
	let ruangan_id=$("#ruangan_id").val();
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}tpendaftaran_poli_tindakan/call_lewati',
		type: 'POST',
		dataType: "json",
		data: {
			ruangan_id: ruangan_id,
			antrian_id: antrian_id,
			antrian_id_next: antrian_id_next,
		
		},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==false){
				swal({
					title: "Maaf!",
					text: "No Antrian Tidak tersedia",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				load_index_all();
			}
		}
	});
}
function recall_manual(antrian_id){
	let ruangan_id=$("#ruangan_id").val();
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}tpendaftaran_poli_tindakan/call_antrian',
		type: 'POST',
		dataType: "json",
		data: {
			ruangan_id: ruangan_id,
			antrian_id: antrian_id,
		
		},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==false){
				swal({
					title: "Maaf!",
					text: "No Antrian Tidak tersedia",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				load_index_all();
			}
		}
	});
}

function cek_login(){
	
	if (st_login=='1'){
		$('#btn_login').hide();
		$('#btn_logout').show();			
		
	}else{
		$('#btn_login').show();
		$('#btn_logout').hide();
		
	}
	
}
$(document).on("click","#btn_login",function(){
	let nama_ruangan=$("#ruangan_id option:selected").text();
	let ruangan_id=$("#ruangan_id").val();
	let idpoli=$("#idpoli").val();
	let iddokter=$("#iddokter").val();
	if (ruangan_id=='#'){
		sweetAlert("Maaf...", "Silahkan Pilih Ruangan!", "error");
		return false;
	}
	if (idpoli=='#'){
		sweetAlert("Maaf...", "Silahkan Pilih Poliklinik!", "error");
		return false;
	}
	if (iddokter=='#'){
		sweetAlert("Maaf...", "Silahkan Pilih Dokter!", "error");
		return false;
	}
	swal({
		title: "Anda Yakin ?",
		text : "Akan Login Menggunakan "+nama_ruangan+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}tpendaftaran_poli_tindakan/login',
				type: 'POST',
				dataType: "json",
				data: {
					ruangan_id: ruangan_id,
					idpoli: idpoli,
					iddokter: iddokter,
				},
				success: function(data) {
					location.reload();
				}
			});
	});
});
$(document).on("click","#btn_logout",function(){
	let nama_ruangan=$("#ruangan_id option:selected").text();
	let ruangan_id=$("#ruangan_id").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Logout "+nama_ruangan+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}tpendaftaran_poli_tindakan/logout',
				type: 'POST',
				dataType: "json",
				data: {ruangan_id: ruangan_id},
				complete: function(data) {
					location.reload();
				}
			});
	});
});
function set_tab($tab){
	tab=$tab;
	// alert(tab);
	$("#cover-spin").show();
	document.getElementById("div_1").classList.remove("active");
	document.getElementById("div_2").classList.remove("active");
	document.getElementById("div_3").classList.remove("active");
	document.getElementById("div_4").classList.remove("active");
		$("#status_transaksi").removeAttr("disabled");
		$("#status_reservasi").removeAttr("disabled");
	if (tab=='1'){
		document.getElementById("div_1").classList.add("active");
		$("#status_transaksi").val('1').trigger('change');
		$("#status_transaksi").attr('disabled', 'disabled');
	}
	if (tab=='2'){
		document.getElementById("div_2").classList.add("active");
		$("#status_transaksi").val(2).trigger('change');
		// $("#status_transaksi").removeAttr("disabled");
		$("#status_transaksi").attr('disabled', 'disabled');
	}
	if (tab=='3'){
		$("#status_transaksi").val('3').trigger('change');
		document.getElementById("div_3").classList.add("active");
		$("#status_transaksi").attr('disabled', 'disabled');
	}
	if (tab=='4'){
		document.getElementById("div_4").classList.add("active");
		$("#status_transaksi").val('4').trigger('change');
		// $("#status_reservasi").val('0').trigger('change');
		$("#status_transaksi").attr('disabled', 'disabled');
	}
	load_index_all();
}
$(document).on("click","#btn_filter_all",function(){
	load_index_all();
});
$(document).on("change","#tanggal",function(){
	load_index_all();
});
$(document).on("change","#idpoli",function(){
	get_dokter();
});
function get_dokter(){
	var idpoli = $("#idpoli").val();
	$("#iddokter").empty();

	$.ajax({
		url: '{site_url}tpendaftaran_poli_tindakan/get_dokter_poli',
		method: "POST",
		dataType: "json",
		data: {
			"idpoli": idpoli
		},
		success: function(data) {
			
			$("#iddokter").append(data);
			// for (var i = 0; i < data.length; i++) {
					// $("#iddokter").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
			// }

			// $("#iddokter").selectpicker("refresh");
			// if ($("#status_cari").val()=='1'){
				// $('#kabupaten1').val($("#ckabupaten").val()).trigger('change');
			// }
		},
		error: function(data) {
			console.log(data);
		}
	});
}
function refres_div_counter(){
	let idpoli=$("#idpoli").val();
	let iddokter=$("#iddokter").val();
	let ruangan_id=$("#ruangan_id").val();
	let tanggal=$("#tanggal").val();
	let st_login=$("#st_login").val();
	// $("#cover-spin").show();
	// $(".div_counter").html('');
	$.ajax({
		url: '{site_url}tpendaftaran_poli_tindakan/refres_div_counter',
		type: 'POST',
		dataType: "json",
		data: {
			ruangan_id:ruangan_id,			
			idpoli:idpoli,			
			iddokter:iddokter,			
			tanggal:tanggal,			
			st_login:st_login,			
		},
		success: function(data) {
			$(".div_counter").html(data);
			$("#cover-spin").hide();
		}
	});
}
function load_index_all(){
	$('#index_all').DataTable().destroy();	
	let idpoli=$("#idpoli").val();
	let iddokter=$("#iddokter").val();
	let ruangan_id=$("#ruangan_id").val();
	let no_medrec=$("#no_medrec").val();
	let namapasien=$("#namapasien").val();
	let tanggal=$("#tanggal").val();
	let st_login=$("#st_login").val();
	
	// alert(ruangan_id);
	table = $('#index_all').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			
			"processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "3%", "targets": 0,  className: "text-right" },
					{ "width": "8%", "targets": [1,2],  className: "text-center" },
					{ "width": "9%", "targets": [1,5,7,9],  className: "text-center" },
					{ "width": "11%", "targets": [3,4],  className: "text-left" },
					{ "width": "12%", "targets": [10],  className: "text-left" },
					// { "width": "8%", "targets": [1,2,3,6,9],  className: "text-center" },
					// { "width": "12%", "targets": [8,7,11,10],  className: "text-center" },
					// { "width": "15%", "targets": 12,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}tpendaftaran_poli_tindakan/getIndex_all', 
                type: "POST" ,
                dataType: 'json',
				data : {
						idpoli:idpoli,
						iddokter:iddokter,
						ruangan_id:ruangan_id,
						tab:tab,
						no_medrec:no_medrec,
						namapasien:namapasien,
						tanggal:tanggal,
						st_login:st_login,
						
						
					   }
            }
        });
	$("#cover-spin").hide();
}
function startWorker() {
	if(typeof(Worker) !== "undefined") {
		if(typeof(w) == "undefined") {
			w = new Worker("{site_url}assets/js/worker.js");
	
		}
		w.postMessage({'cmd': 'start' ,'msg': 5000});
		w.onmessage = function(event) {
			refres_div_counter();
			$('#index_all').DataTable().ajax.reload( null, false );
			// $('#index_all').DataTable().ajax.reload( null, false );
			// load_index_all();
		};
		w.onerror = function(event) {
			window.location.reload();
		};
	} else {
		alert("Sorry, your browser does not support Autosave Mode");
		$("#autosave_status").html('<button class="btn btn-warning"><i class="fa fa-warning"></i> Autosave not running!</button>');
	}
 }

</script>