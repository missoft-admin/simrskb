<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}msetting_jurnal_pembelian" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('msetting_jurnal_pembelian/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>	
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_primary('Proses Approval')?></label>
				<input type="hidden" class="form-control" id="tmp_idakun_kredit" placeholder="0" name="tmp_idakun_kredit" value="{idakun_kredit}">
				<div class="col-md-4">
					<select id="st_auto_posting" name="st_auto_posting" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="1" <?=(1 == $st_auto_posting ? 'selected' : ''); ?>>AUTO POSTING</option>
                            <option value="2" <?=(2 == $st_auto_posting ? 'selected' : ''); ?>>MANUAL</option>
                        </select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_primary('Pembayaran Kredit')?></label>
				
				<div class="col-md-8">
					<select id="idakun_kredit" name="idakun_kredit" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
					<?foreach($list_akun as $r){?>
						<option value="<?=$r->id?>" <?=($r->id == $idakun_kredit? 'selected' : ''); ?>><?=$r->noakun.' - '.$r->namaakun.' ('.$r->id.')'?></option>
					<?}?>
					</select>
				</div>
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_primary('Batas Waktu Batal')?></label>
				
				<div class="col-md-4">
					<input type="text" style="width: 100%"  class="form-control number" id="batas_batal" placeholder="0" name="batas_batal" value="<?=$batas_batal?>">
				</div>
				<div class="col-md-2">
					<button type="button" class="btn btn-sm btn-success" tabindex="8" id="btn_update" title="Update"><i class="fa fa-save"></i> UPDATE</button>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-1 control-label"><h4><?=text_success('SETTING PERSEDIAAN PEMBELIAN')?></h4></label>
				
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<table id="tabel_beli" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 0%;">#</th>															
								<th style="width: 20%;">Tipe </th>															
								<th style="width: 20%;">Kategori</th>								
								<th style="width: 25%;">Barang</th>								
								<th style="width: 25%;">No Akun </th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td></td>
								<td>
									<select name="idtipe_beli" id="idtipe_beli" style="width: 100%" id="step" class="js-select2 form-control input-sm">										
										<option value="#" selected>- Pilih Tipe -</option>																			
										<?foreach(get_all('mdata_tipebarang',array(),'id','ASC') as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama_tipe?></option>																				
										<?}?>
									</select>										
								</td>								
								<td>
									<select name="idkategori_beli" id="idkategori_beli" style="width: 100%" class="js-select2 form-control input-sm">										
										<option value="0" selected>- Semua Kategori -</option>																			
									</select>										
								</td>
								<td>
									<select name="idbarang_beli" id="idbarang_beli" style="width: 100%" class="form-control input-sm">										
										<option value="0" selected>- Semua Barang -</option>																			
									</select>										
								</td>
								
								<td>
									<select name="idakun_beli" id="idakun_beli" style="width: 100%" class="js-select2 form-control input-sm idakun">										
										
									</select>
									<input type="hidden" class="form-control" id="id_edit_beli" placeholder="0" name="id_edit_beli" value="">
								</td>	
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_beli" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_beli" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						
				</table>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-1 control-label"><h4><?=text_primary('SETTING PPN PEMBELIAN')?></h4></label>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<table id="tabel_ppn" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 0%;">#</th>															
								<th style="width: 20%;">Tipe </th>															
								<th style="width: 20%;">Kategori</th>								
								<th style="width: 25%;">Barang</th>								
								<th style="width: 25%;">No Akun </th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td></td>
								<td>
									<select name="idtipe_ppn" id="idtipe_ppn" style="width: 100%" id="step" class="js-select2 form-control input-sm">										
										<option value="#" selected>- Pilih Tipe -</option>																			
										<?foreach(get_all('mdata_tipebarang',array(),'id','ASC') as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama_tipe?></option>																				
										<?}?>
									</select>										
								</td>								
								<td>
									<select name="idkategori_ppn" id="idkategori_ppn" style="width: 100%" class="js-select2 form-control input-sm">										
										<option value="0" selected>- Semua Kategori -</option>																			
									</select>										
								</td>
								<td>
									<select name="idbarang_ppn" id="idbarang_ppn" style="width: 100%" class="form-control input-sm">										
										<option value="0" selected>- Semua Barang -</option>																			
									</select>										
								</td>
								
								<td>
									<select name="idakun_ppn" id="idakun_ppn" style="width: 100%" class="js-select2 form-control input-sm idakun">										
										
									</select>
									<input type="hidden" class="form-control" id="id_edit_ppn" placeholder="0" name="id_edit_ppn" value="">
								</td>	
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_ppn" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_ppn" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody></tbody>			
					</table>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-1 control-label"><h4><?=text_danger('SETTING DISKON PEMBELIAN')?></h4></label>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<table id="tabel_diskon" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 0%;">#</th>															
								<th style="width: 20%;">Tipe </th>															
								<th style="width: 20%;">Kategori</th>								
								<th style="width: 25%;">Barang</th>								
								<th style="width: 25%;">No Akun </th>
								<th style="width: 10%;">Actions</th>
							</tr>
							<tr>								
								<td></td>
								<td>
									<select name="idtipe_diskon" id="idtipe_diskon" style="width: 100%" class="js-select2 form-control input-sm">										
										<option value="#" selected>- Pilih Tipe -</option>																			
										<?foreach(get_all('mdata_tipebarang',array(),'id','ASC') as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama_tipe?></option>																				
										<?}?>
									</select>										
								</td>								
								<td>
									<select name="idkategori_diskon" id="idkategori_diskon" style="width: 100%"class="js-select2 form-control input-sm">										
										<option value="0" selected>- Semua Kategori -</option>																			
									</select>										
								</td>
								<td>
									<select name="idbarang_diskon" id="idbarang_diskon" style="width: 100%"class="form-control input-sm">										
										<option value="0" selected>- Semua Barang -</option>																			
									</select>										
								</td>
								
								<td>
									<select name="idakun_diskon" id="idakun_diskon" style="width: 100%" class="js-select2 form-control input-sm idakun">										
										
									</select>
									<input type="hidden" class="form-control" id="id_edit_diskon" placeholder="0" name="id_edit_diskon" value="">
								</td>	
								
								<td>									
									<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_diskon" title="Masukan Item"><i class="fa fa-save"></i></button>
									<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_diskon" title="Batalkan"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody></tbody>			
					</table>
				</div>
			</div>
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		// load_unit();
		list_akun();
		load_beli();
		load_ppn();
		load_diskon();
		clear_input_beli();
		clear_input_diskon();
		clear_input_ppn();
		list_barang_beli();
		setTimeout(function() {
			$("#idakun_kredit").val($("#tmp_idakun_kredit").val()).trigger('change');
		}, 500);
		// load_beli();
	});	
	$(document).on("click","#btn_update",function(){
		if (validate_update()==false)return false;
		var st_auto_posting=$("#st_auto_posting").val();
		var idakun_kredit=$("#idakun_kredit").val();
		var batas_batal=$("#batas_batal").val();
		$("#tmp_idakun_kredit").val(idakun_kredit)
		$.ajax({
			url: '{site_url}msetting_jurnal_pembelian/update',
			type: 'POST',
			data: {
				st_auto_posting:st_auto_posting,idakun_kredit:idakun_kredit,batas_batal:batas_batal,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$("#idakun_kredit").val($("#tmp_idakun_kredit").val()).trigger('change');
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function validate_update()
	{
		
		if ($("#st_auto_posting").val()=='#'){
			sweetAlert("Maaf...", "Setting Harus diisi", "error");
			return false;
		}
		if ($("#batas_batal").val()==''){
			sweetAlert("Maaf...", "Batas Waktu Batal Harus diisi", "error");
			return false;
		}
		if ($("#idakun_kredit").val()=='#'){
			sweetAlert("Maaf...", "No AKun Pembayaran Kredit Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function list_akun(){
		$.ajax({
			url: '{site_url}msetting_jurnal_pembelian/list_akun',
			dataType: "json",
			success: function(data) {
				$(".idakun").empty();
				$('.idakun').append('<option value="#" selected>- Pilih Akun -</option>');
				$('.idakun').append(data.detail);
				
			}
		});		
	}
	
	
	function list_barang_beli(){
		var newOption = new Option('- Semua Barang -', '0', true, true);					
		$("#idbarang_beli").append(newOption);
		$("#idbarang_beli").val("0").trigger('change');
		
		var idtipe=$("#idtipe_beli").val();
		var idkategori=$("#idkategori_beli").val();
		$("#idbarang_beli").select2({
			minimumInputLength: 0,
			noResults: 'Obat Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}msetting_jurnal_pembelian/list_barang/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,idtipe: idtipe,idkategori:idkategori,
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.kode+' - '+item.nama,
								id: item.id,
								idtipe : item.idtipe
							}
						})
					};
				}
			}
		});

	}
	$(document).on("change","#idtipe_beli",function(){
		list_kategori_beli($(this).val());
		list_barang_beli();
	});
	$(document).on("change","#idkategori_beli",function(){
		list_barang_beli();
	});
	function list_kategori_beli($id){
		if ($id!='#'){
			$.ajax({
				url: '{site_url}msetting_jurnal_pembelian/list_kategori/'+$id,
				dataType: "json",
				success: function(data) {
					$("#idkategori_beli").empty();
					$('#idkategori_beli').append('<option value="0" selected>- Semua Kategori -</option>');
					$('#idkategori_beli').append(data.detail);
				}
			});
		}else{
			$("#idkategori_beli").empty();
			$('#idkategori_beli').append('<option value="0" selected>- Semua Kategori -</option>');
		}		
	}
	function validate_add_beli()
	{
		
		if ($("#idtipe_beli").val()=='#'){
			sweetAlert("Maaf...", "Tipe Harus diisi", "error");
			return false;
		}
		if ($("#idakun_beli").val()=='#'){
			sweetAlert("Maaf...", "No AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_beli(){
		$("#idtipe_beli").val('#').trigger('change');	
		var newOption = new Option('- Semua Barang -', '0', true, true);
					
		$("#idbarang_beli").append(newOption);
		$("#idbarang_beli").val("0").trigger('change');


		// $("#idbarang_beli").val('#').trigger('change');		
		$("#id_edit_beli").val('');		
		$("#idakun_beli").val('#').trigger('change');
		// alert($("#idakun").val());
	}
	$(document).on("click","#simpan_beli",function(){
		if (validate_add_beli()==false)return false;
		var id_edit=$("#id_edit_beli").val();
		var idtipe=$("#idtipe_beli").val();
		var idkategori=$("#idkategori_beli").val();
		var idbarang=$("#idbarang_beli").val();
		var idakun=$("#idakun_beli").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_pembelian/simpan_beli',
			type: 'POST',
			data: {
				id_edit:id_edit,idtipe:idtipe,
				idkategori: idkategori,idbarang:idbarang,idakun:idakun,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_beli').DataTable().ajax.reload( null, false );
					clear_input_beli();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	function hapus_beli($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_pembelian/hapus_beli/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_beli').DataTable().ajax.reload( null, false );
						clear_input_beli();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_beli",function(){
		
		clear_input_beli();
	});
	function load_beli(){
		var idlogic=$("#id").val();
		
		$('#tabel_beli').DataTable().destroy();
		var table = $('#tabel_beli').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pembelian/load_beli/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	//PPN
	function list_barang_ppn(){
		var newOption = new Option('- Semua Barang -', '0', true, true);					
		$("#idbarang_ppn").append(newOption);
		$("#idbarang_ppn").val("0").trigger('change');
		var idtipe=$("#idtipe_ppn").val();
		var idkategori=$("#idkategori_ppn").val();
		$("#idbarang_ppn").select2({
			minimumInputLength: 0,
			noResults: 'Obat Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}msetting_jurnal_pembelian/list_barang/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,idtipe: idtipe,idkategori:idkategori,
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.kode+' - '+item.nama,
								id: item.id,
								idtipe : item.idtipe
							}
						})
					};
				}
			}
		});

	}
	$(document).on("change","#idtipe_ppn",function(){
		list_kategori_ppn($(this).val());
		list_barang_ppn();
	});
	$(document).on("change","#idkategori_ppn",function(){
		list_barang_ppn();
	});
	function list_kategori_ppn($id){
		if ($id!='#'){
			$.ajax({
				url: '{site_url}msetting_jurnal_pembelian/list_kategori/'+$id,
				dataType: "json",
				success: function(data) {
					$("#idkategori_ppn").empty();
					$('#idkategori_ppn').append('<option value="0" selected>- Semua Kategori -</option>');
					$('#idkategori_ppn').append(data.detail);
				}
			});
		}else{
			$("#idkategori_ppn").empty();
			$('#idkategori_ppn').append('<option value="0" selected>- Semua Kategori -</option>');
		}		
	}
	function validate_add_ppn()
	{
		
		if ($("#idtipe_ppn").val()=='#'){
			sweetAlert("Maaf...", "Tipe Harus diisi", "error");
			return false;
		}
		if ($("#idakun_ppn").val()=='#'){
			sweetAlert("Maaf...", "No AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	
	function clear_input_ppn(){
		$("#idtipe_ppn").val('#').trigger('change');	
		var newOption = new Option('- Semua Barang -', '0', true, true);					
		$("#idbarang_ppn").append(newOption);
		$("#idbarang_ppn").val("0").trigger('change');


		// $("#idbarang_ppn").val('#').trigger('change');		
		$("#id_edit_ppn").val('');		
		$("#idakun_ppn").val('#').trigger('change');
		// alert($("#idakun").val());
	}
	$(document).on("click","#simpan_ppn",function(){
		if (validate_add_ppn()==false)return false;
		var id_edit=$("#id_edit_ppn").val();
		var idtipe=$("#idtipe_ppn").val();
		var idkategori=$("#idkategori_ppn").val();
		var idbarang=$("#idbarang_ppn").val();
		var idakun=$("#idakun_ppn").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_pembelian/simpan_ppn',
			type: 'POST',
			data: {
				id_edit:id_edit,idtipe:idtipe,
				idkategori: idkategori,idbarang:idbarang,idakun:idakun,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_ppn').DataTable().ajax.reload( null, false );
					clear_input_ppn();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function hapus_ppn($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_pembelian/hapus_ppn/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_ppn').DataTable().ajax.reload( null, false );
						clear_input_ppn();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_ppn",function(){
		
		clear_input_ppn();
	});
	function load_ppn(){
		var idlogic=$("#id").val();
		
		$('#tabel_ppn').DataTable().destroy();
		var table = $('#tabel_ppn').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pembelian/load_ppn/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
//DISKON
function list_barang_diskon(){
		var newOption = new Option('- Semua Barang -', '0', true, true);					
		$("#idbarang_diskon").append(newOption);
		$("#idbarang_diskon").val("0").trigger('change');
		
		var idtipe=$("#idtipe_diskon").val();
		var idkategori=$("#idkategori_diskon").val();
		$("#idbarang_diskon").select2({
			minimumInputLength: 0,
			noResults: 'Obat Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}msetting_jurnal_pembelian/list_barang/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,idtipe: idtipe,idkategori:idkategori,
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.kode+' - '+item.nama,
								id: item.id,
								idtipe : item.idtipe
							}
						})
					};
				}
			}
		});

	}
	$(document).on("change","#idtipe_diskon",function(){
		list_kategori_diskon($(this).val());
		list_barang_diskon();
	});
	$(document).on("change","#idkategori_diskon",function(){
		list_barang_diskon();
	});
	function list_kategori_diskon($id){
		if ($id!='#'){
			$.ajax({
				url: '{site_url}msetting_jurnal_pembelian/list_kategori/'+$id,
				dataType: "json",
				success: function(data) {
					$("#idkategori_diskon").empty();
					$('#idkategori_diskon').append('<option value="0" selected>- Semua Kategori -</option>');
					$('#idkategori_diskon').append(data.detail);
				}
			});
		}else{
			$("#idkategori_diskon").empty();
			$('#idkategori_diskon').append('<option value="0" selected>- Semua Kategori -</option>');
		}		
	}
	function validate_add_diskon()
	{
		
		if ($("#idtipe_diskon").val()=='#'){
			sweetAlert("Maaf...", "Tipe Harus diisi", "error");
			return false;
		}
		if ($("#idakun_diskon").val()=='#'){
			sweetAlert("Maaf...", "No AKun Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_diskon(){
		$("#idtipe_diskon").val('#').trigger('change');	
		var newOption = new Option('- Semua Barang -', '0', true, true);
					
		$("#idbarang_diskon").append(newOption);
		$("#idbarang_diskon").val("0").trigger('change');


		// $("#idbarang_diskon").val('#').trigger('change');		
		$("#id_edit_diskon").val('');		
		$("#idakun_diskon").val('#').trigger('change');
		// alert($("#idakun").val());
	}
	$(document).on("click","#simpan_diskon",function(){
		if (validate_add_diskon()==false)return false;
		var id_edit=$("#id_edit_diskon").val();
		var idtipe=$("#idtipe_diskon").val();
		var idkategori=$("#idkategori_diskon").val();
		var idbarang=$("#idbarang_diskon").val();
		var idakun=$("#idakun_diskon").val();
		
		$.ajax({
			url: '{site_url}msetting_jurnal_pembelian/simpan_diskon',
			type: 'POST',
			data: {
				id_edit:id_edit,idtipe:idtipe,
				idkategori: idkategori,idbarang:idbarang,idakun:idakun,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_diskon').DataTable().ajax.reload( null, false );
					clear_input_diskon();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function hapus_diskon($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_pembelian/hapus_diskon/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_diskon').DataTable().ajax.reload( null, false );
						clear_input_diskon();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_diskon",function(){
		
		clear_input_diskon();
	});
	function load_diskon(){
		var idlogic=$("#id").val();
		
		$('#tabel_diskon').DataTable().destroy();
		var table = $('#tabel_diskon').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pembelian/load_diskon/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	
	
</script>
