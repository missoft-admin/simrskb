<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1519'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_1" onclick="load_general()"><i class="fa fa-bell"></i> General</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1525'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_2"  onclick="load_logic()"><i class="fa fa-check-square-o"></i> Logic APM</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1526'))){ ?>
		
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab_3" onclick="load_logic_checkin()"><i class="si si-camera"></i> Self Checkin</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1527'))){ ?>
		
		<li class="<?=($tab=='4'?'active':'')?>">
			<a href="#tab_4" ><i class="si si-loop"></i> Pendaftaran Mandiri</a>
		</li>
		<?}?>
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_1">
			<div class="block-content">
					<div class="row">
					<?php echo form_open_multipart('apm_setting/save_general', 'class="form-horizontal push-10-t"') ?>
						<?php if ($header_logo != '') { ?>
						<div class="form-group">
							<label class="col-md-2 control-label" for="example-hf-email"></label>
							<div class="col-md-7">

								<img class="img-avatar"  id="output_img" src="{upload_path}apm_setting/{header_logo}" />
							</div>
						</div>
						<?php } ?>
						<div class="form-group">
							<label class="col-md-2 control-label" for="example-hf-email">Logo Header (100x100)</label>
							<div class="col-md-7">
								<div class="box">
									<input type="file" id="file-3" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;" name="header_logo" value="{header_logo}" />
									<label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-2 control-label" for="bg_color">BG Color<span style="color:red;">*</span></label>
							<div class="col-md-2">
								<div class="js-colorpicker input-group colorpicker-element">
									<input class="form-control" type="text" id="bg_color" name="bg_color" value="{bg_color}">
									<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="judul_header">Judul Header<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="judul_header" placeholder="Judul Header" name="judul_header" value="{judul_header}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="login_judul">Judul Sub Header<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="judul_sub_header" placeholder="Judul Sub Header" name="judul_sub_header" value="{judul_sub_header}" required>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 col-md-offset-2">
								<button class="btn btn-success" type="submit">Simpan General</button>
								<a href="{base_url}apm_setting/index/1" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
							</div>
						</div>
						<?php echo form_hidden('id', $id); ?>
						<?php echo form_close() ?>
					</div>
			</div>
			<div class="block-content">
				<div class="row push-20-t">
					<div class="form-group">
						<label class="col-md-12 control-label" for="login_judul">GALLERY (1500px X 500px)</label>
						<div class="col-md-12">
				<?php if (UserAccesForm($user_acces_form,array('1523'))){ ?>
							<form class="dropzone" action="{base_url}apm_setting/upload_files" method="post" enctype="multipart/form-data">
								<input name="idtransaksi" type="hidden" value="{id}">
							  </form>
				<?}?>
						</div>
					</div>
				</div>
				<div class="row push-20-t">
					<div class="form-group">
						<div class="col-md-12">
								<div class="table-responsive">
								 <table class="table table-bordered table-striped table-responsive" id="listFile">
									<thead>
										<tr>
											<th width="1%">No</th>
											<th width="20%">File</th>
											<th width="15%">Upload By</th>
											<th width="5%">Size</th>
											<th width="5%">Aksi</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="block-content">
				<div class="row push-20-t">
					<div class="form-group">
						<label class="col-md-12 control-label" for="login_judul">RUNNING TEXT</label>
						
					</div>
				</div>
				<div class="row push-20-t">
					<div class="form-group">
						<div class="col-md-12">
								<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_running">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="10%">Urutan</th>
											<th width="75%">Running Text</th>
											<th width="10%">Action</th>										   
										</tr>
										<input id="idrunning" type="hidden" value="">
										<?php if (UserAccesForm($user_acces_form,array('1520'))){ ?>
										<tr>
											<th>#</th>
											<th>
												<input tabindex="0" type="text" class="form-control number" id="nourut" placeholder="No Urut" name="nourut" required>
											</th>
											<th>
												<textarea style="width:100%" class="form-control" id="isi" name="isi" rows="2" placeholder="Content.."></textarea>
											
											</th>
											
											<th>
												<div class="btn-group">
													<button class="btn btn-primary btn-sm" title="Simpan" type="button" id="btn_tambah_running_text"><i class="fa fa-save"></i></button>
													<button class="btn btn-warning btn-sm"  title="Clear" type="button" id="btn_refresh_running_text"><i class="fa fa-refresh"></i></button>
												</div>
											</th>										   
										</tr>
												<?}?>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_2">
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group" style="margin-bottom: 15px;">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_logic">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="10%">Jenis Pertemuan</th>
											<th width="15%">Tipe</th>
											<th width="15%">Poliklinik</th>
											<th width="15%">Dokter</th>
											<th width="10%">GC</th>
											<th width="10%">Skrining Pasien</th>
											<th width="10%">Skrining Covid</th>
											<th width="10%">Action</th>										   
										</tr>
										<tr>
											<th>#</th>
											<th>
												<select id="jenis_pertemuan_id" name="jenis_pertemuan_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" <?=($jenis_pertemuan_id=='#'?'selected':'')?>>-Pilih Jenis Pertemuan-</option>
													<?foreach(list_variable_ref(15) as $r){?>
														<option value="<?=$r->id?>" <?=($jenis_pertemuan_id==$r->id?'selected':'')?>><?=$r->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" <?=($idtipe=='#'?'selected':'')?>>-Pilih Tipe-</option>
													<option value="1" <?=($idtipe=='1'?'selected':'')?>>Poliklinik</option>
													<option value="2" <?=($idtipe=='2'?'selected':'')?>>Instalasi Gawat Darurat</option>
												</select>
											</th>
											<th>
												<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="#" <?=($idpoli=='0'?'selected':'')?>>-All Poliklinik-</option>
												</select>
											</th>
											<th>
												<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="0" <?=($iddokter=='0'?'selected':'')?>>-All Dokter-</option>
													<?foreach($list_dokter as $row){?>
													<option value="<?=$row->id?>"><?=$row->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="st_gc" name="st_gc" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1" <?=($st_gc=='1'?'selected':'')?>>YA</option>
													<option value="0" <?=($st_gc=='0'?'selected':'')?>>TIDAK</option>
													
												</select>
											</th>
											<th>
												<select id="st_sp" name="st_sp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1" <?=($st_sp=='1'?'selected':'')?>>YA</option>
													<option value="0" <?=($st_sp=='0'?'selected':'')?>>TIDAK</option>
												</select>
											</th>
											<th>
												<select id="st_sc" name="st_sc" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1" <?=($st_sc=='1'?'selected':'')?>>YA</option>
													<option value="0" <?=($st_sc=='0'?'selected':'')?>>TIDAK</option>
												</select>
											</th>
											<th>
												<?php if (UserAccesForm($user_acces_form,array('1477'))){ ?>
												<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_logic" name="btn_tambah_logic"><i class="fa fa-plus"></i> Tambah</button>
												<?}?>
											</th>										   
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>

		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?> " id="tab_3">
			<div class="block-content">
					<div class="row">
					<?php echo form_open_multipart('apm_setting/save_self', 'class="form-horizontal push-10-t"') ?>
						<?php if ($header_logo_self != '') { ?>
						<div class="form-group">
							<label class="col-md-2 control-label" for="example-hf-email"></label>
							<div class="col-md-7">

								<img class="img-avatar"  id="output_img_self" src="{upload_path}apm_setting/{header_logo_self}" />
							</div>
						</div>
						<?php } ?>
						<div class="form-group">
							<label class="col-md-2 control-label" for="example-hf-email">Logo Header (100x100)</label>
							<div class="col-md-7">
								<div class="box">
									<input type="file" id="file-4" class="inputfile inputfile-3" onchange="loadFile_img_self(event)" style="display:none;" name="header_logo_self" value="{header_logo_self}" />
									<label for="file-4"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-2 control-label" for="judul_header">BG Color<span style="color:red;">*</span></label>
							<div class="col-md-2">
								<div class="js-colorpicker input-group colorpicker-element">
									<input class="form-control" type="text" id="bg_color_self" name="bg_color_self" value="{bg_color_self}">
									<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="judul_header_self">Judul Header<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="judul_header_self" placeholder="Judul Header" name="judul_header_self" value="{judul_header_self}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="judul_sub_header_self">Judul Sub Header<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="judul_sub_header_self" placeholder="Judul Sub Header" name="judul_sub_header_self" value="{judul_sub_header_self}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="content_input_kode_self">Content Input Code<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<textarea class="form-control js-summernote" name="content_input_kode_self" id="content_input_kode_self"><?=$content_input_kode_self?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="content_rincian_header_self">Content Header Rincian Reservasi<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<textarea class="form-control js-summernote" name="content_rincian_header_self" id="content_rincian_header_self"><?=$content_rincian_header_self?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="content_rincian_footer_self">Content Footer Rincian Reservasi<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<textarea class="form-control js-summernote" name="content_rincian_footer_self" id="content_rincian_footer_self"><?=$content_rincian_footer_self?></textarea>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped" id="checkin_logic">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Jenis Pertemuan</th>
												<th width="15%">Tipe</th>
												<th width="15%">Poliklinik</th>
												<th width="15%">Dokter</th>
												<th width="10%">GC</th>
												<th width="10%">Skrining Pasien</th>
												<th width="10%">Skrining Covid</th>
												<th width="10%">Action</th>										   
											</tr>
											<tr>
												<th>#</th>
												<th>
													<select id="checkin_jenis_pertemuan_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
														<option value="#" <?=($jenis_pertemuan_id=='#'?'selected':'')?>>-Pilih Jenis Pertemuan-</option>
														<? foreach(list_variable_ref(15) as $r) { ?>
															<option value="<?=$r->id?>" <?=($jenis_pertemuan_id==$r->id?'selected':'')?>><?=$r->nama?></option>
														<? } ?>
													</select>
												</th>
												<th>
													<select id="checkin_idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
														<option value="#" <?=($idtipe=='#'?'selected':'')?>>-Pilih Tipe-</option>
														<option value="1" <?=($idtipe=='1'?'selected':'')?>>Poliklinik</option>
														<option value="2" <?=($idtipe=='2'?'selected':'')?>>Instalasi Gawat Darurat</option>
													</select>
												</th>
												<th>
													<select id="checkin_idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
														<option value="#" <?=($idpoli=='0'?'selected':'')?>>-All Poliklinik-</option>
													</select>
												</th>
												<th>
													<select id="checkin_iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
														<option value="0" <?=($iddokter=='0'?'selected':'')?>>-All Dokter-</option>
														<?foreach($list_dokter as $row){?>
														<option value="<?=$row->id?>"><?=$row->nama?></option>
														<?}?>
													</select>
												</th>
												<th>
													<select id="checkin_st_gc" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
														<option value="1" <?=($st_gc=='1'?'selected':'')?>>YA</option>
														<option value="0" <?=($st_gc=='0'?'selected':'')?>>TIDAK</option>
													</select>
												</th>
												<th>
													<select id="checkin_st_sp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
														<option value="1" <?=($st_sp=='1'?'selected':'')?>>YA</option>
														<option value="0" <?=($st_sp=='0'?'selected':'')?>>TIDAK</option>
													</select>
												</th>
												<th>
													<select id="checkin_st_sc" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
														<option value="1" <?=($st_sc=='1'?'selected':'')?>>YA</option>
														<option value="0" <?=($st_sc=='0'?'selected':'')?>>TIDAK</option>
													</select>
												</th>
												<th>
													<?php if (UserAccesForm($user_acces_form,array('1477'))){ ?>
													<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_logic_checkin"><i class="fa fa-plus"></i> Tambah</button>
													<?}?>
												</th>										   
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 col-md-offset-2">
								<button class="btn btn-success" type="submit">Simpan</button>
								<a href="{base_url}apm_setting/index/3" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
							</div>
						</div>
						<?php echo form_hidden('id', $id); ?>
						<?php echo form_close() ?>
					</div>
			</div>
		</div>

		<div class="tab-pane fade fade-left <?=($tab=='4'?'active in':'')?> " id="tab_4">
			<div class="block-content">
					<div class="row">
					<?php echo form_open_multipart('apm_setting/save_mandiri', 'class="form-horizontal push-10-t"') ?>
						<?php if ($header_logo_self != '') { ?>
						<div class="form-group">
							<label class="col-md-2 control-label" for="example-hf-email"></label>
							<div class="col-md-7">
								<img class="img-avatar"  id="output_img_mandiri" src="{upload_path}apm_setting/{header_logo_mandiri}" />
							</div>
						</div>
						<?php } ?>
						<div class="form-group">
							<label class="col-md-2 control-label" for="example-hf-email">Logo Header (100x100)</label>
							<div class="col-md-7">
								<div class="box">
									<input type="file" id="file-5" class="inputfile inputfile-3" onchange="loadFile_img_mandiri(event)" style="display:none;" name="header_logo_mandiri" value="{header_logo_mandiri}" />
									<label for="file-5"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="judul_header_mandiri">Judul Header<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="judul_header_mandiri" placeholder="Judul Header" name="judul_header_mandiri" value="{judul_header_mandiri}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="judul_sub_header_mandiri">Judul Sub Header<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="judul_sub_header_mandiri" placeholder="Judul Sub Header" name="judul_sub_header_mandiri" value="{judul_sub_header_mandiri}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="judul_pasien_baru_mandiri">Judul Pasien baru<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="judul_pasien_baru_mandiri" placeholder="Judul Pasien Baru" name="judul_pasien_baru_mandiri" value="{judul_pasien_baru_mandiri}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="example-hf-email"></label>
							<div class="col-md-7">
								<img class="img-avatar"  id="output_img_baru" src="{upload_path}apm_setting/{icon_pasien_baru_mandiri}" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="example-hf-email">Icon Pasien Baru</label>
							<div class="col-md-7">
								<div class="box">
									<input type="file" id="file-baru" class="inputfile inputfile-3" onchange="loadFile_img_baru(event)" style="display:none;" name="icon_pasien_baru_mandiri" value="{icon_pasien_baru_mandiri}" />
									<label for="file-baru"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="judul_pasien_lama_mandiri">Judul Pasien lama<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="judul_pasien_lama_mandiri" placeholder="Judul pasien lama" name="judul_pasien_lama_mandiri" value="{judul_pasien_baru_mandiri}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="example-hf-email"></label>
							<div class="col-md-7">
								<img class="img-avatar"  id="output_img_lama" src="{upload_path}apm_setting/{icon_pasien_lama_mandiri}" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="example-hf-email">Icon Pasien Baru</label>
							<div class="col-md-7">
								<div class="box">
									<input type="file" id="file-lama" class="inputfile inputfile-3" onchange="loadFile_img_lama(event)" style="display:none;" name="icon_pasien_lama_mandiri" value="{icon_pasien_lama_mandiri}" />
									<label for="file-lama"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="content_pencarian_header_mandiri">Content Header Pencarian Reservasi<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<textarea class="form-control js-summernote" name="content_pencarian_header_mandiri" id="content_pencarian_header_mandiri"><?=$content_pencarian_header_mandiri?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="content_pencarian_footer_mandiri">Content Footer Pencacarian Reservasi<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<textarea class="form-control js-summernote" name="content_pencarian_footer_mandiri" id="content_pencarian_footer_mandiri"><?=$content_pencarian_footer_mandiri?></textarea>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 col-md-offset-2">
								<button class="btn btn-success" type="submit">Simpan</button>
								<a href="{base_url}apm_setting/index/4" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
							</div>
						</div>
						<?php echo form_hidden('id', $id); ?>
						<?php echo form_close() ?>
					</div>
			</div>
			
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
	var table;
	var myDropzone;

	function loadFile_img(event) {
		var output_img = document.getElementById('output_img');
		output_img.src = URL.createObjectURL(event.target.files[0]);
	}

	function loadFile_img_self(event) {
		var output_img_self = document.getElementById('output_img_self');
		output_img_self.src = URL.createObjectURL(event.target.files[0]);
	}

	function loadFile_img_mandiri(event) {
		var output_img_mandiri= document.getElementById('output_img_mandiri');
		output_img_mandiri.src = URL.createObjectURL(event.target.files[0]);
	}

	function loadFile_img_baru(event) {
		var output_img_baru= document.getElementById('output_img_baru');
		output_img_baru.src = URL.createObjectURL(event.target.files[0]);
	}

	function loadFile_img_lama(event) {
		var output_img_lama= document.getElementById('output_img_lama');
		output_img_lama.src = URL.createObjectURL(event.target.files[0]);
	}

	$(document).ready(function(){	
		$(".number").number(true,0,'.',',');
		
		var tab=$("#tab").val();
		if (tab=='1'){
			load_general();
		}
		if (tab=='2'){
			load_logic();
		}
		if (tab=='3'){
			load_logic_checkin();
		}

		Dropzone.autoDiscover = false;
		myDropzone = new Dropzone(".dropzone", { 
			autoProcessQueue: true,
			maxFilesize: 30,
		});

		myDropzone.on("complete", function(file) {	
			refresh_image();
			myDropzone.removeFile(file);
		});

		$('.js-summernote').summernote({
				height: 100,   //set editable area's height
				codemirror: { // codemirror options
				theme: 'monokai'
				}	
			})
	});

	function refresh_image(){
		$('#listFile tbody').empty();

		$.ajax({
			url: '{site_url}apm_setting/refresh_image/',
			dataType: "json",
			success: function(data) {
				$('#listFile tbody').empty();
				$("#listFile tbody").append(data.detail);
			}
		});
	}

	function removeFile($id){
		var id = $id;
		
		swal({
			title: "Anda Yakin ?",
			text : "Untuk Hapus data?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}apm_setting/removeFile',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
					refresh_image();
				}
			});
		});
	}

	function edit_running($id){
		let id=$id;
		$.ajax({
			url: '{site_url}apm_setting/edit_running', 
			dataType: "JSON",
			method: "POST",
			data : {id:id},
			success: function(data) {
				$("#nourut").val(data.nourut);
				$("#isi").val(data.isi);
				$("#idrunning").val(data.id);
			}
		});
	}

	$("#btn_tambah_running_text").click(function() {
		let idrunning=$("#idrunning").val();
		let nourut=$("#nourut").val();
		let isi=$("#isi").val();
		if ($("#nourut").val()==''){
			sweetAlert("Maaf...", "Tentukan No Urut", "error");
			return false;
		}
		if ($("#isi").val()==''){
			sweetAlert("Maaf...", "Tentukan Isi", "error");
			return false;
		}
		
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}apm_setting/simpan_running', 
			dataType: "JSON",
			method: "POST",
			data : {
				idrunning:idrunning,
				nourut:nourut,
				isi:isi,
				},
			complete: function(data) {
				$('#index_running').DataTable().ajax.reload( null, false ); 
				$("#cover-spin").hide();
				clear_running();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}
		});
	});

	$("#btn_refresh_running_text").click(function() {
		clear_running();
	});

	function clear_running(){
		$("#idrunning").val('');
		$("#nourut").val('');
		$("#isi").val('');
	}

	function load_running(){
		$('#index_running').DataTable().destroy();	
		table = $('#index_running').DataTable({
			autoWidth: false,
			searching: false,
			serverSide: true,
			processing: true,
			order: [],
			pageLength: 10,
			ordering: false,
			ajax: { 
				url: '{site_url}apm_setting/load_running_text', 
				type: "POST" ,
				dataType: 'json',
				data : {}
			}
		});
	}

	function hapus_running($id){
		var id=$id;
		
		swal({
			title: "Anda Yakin ?",
			text : "Untuk Hapus Running Text?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}apm_setting/hapus_running',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
					$('#index_running').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
				}
			});
		});
	}

	function load_general(){
		$('.js-summernote').summernote({
			height: 100,   //set editable area's height
			codemirror: { // codemirror options
			theme: 'monokai'
			}	
		});	
		refresh_image();
		clear_running();
		load_running();
	}

	// LOGIC
	function load_logic(){
		$('#index_logic').DataTable().destroy();	
		table = $('#index_logic').DataTable({
				autoWidth: false,
				searching: true,
				pageLength: 100,
				serverSide: true,
				processing: true,
				order: [],
				ordering: false,
				ajax: { 
						url: '{site_url}apm_setting/load_logic', 
						type: "POST" ,
						dataType: 'json',
						data : {}
				}
		});
	}

	$("#idtipe").change(function() {
		let idtipe = $("#idtipe").val();
		let jenis_pertemuan_id = $("#jenis_pertemuan_id").val();

		$("#idpoli").empty();

		$.ajax({
			url: '{site_url}apm_setting/list_poli_2', 
			dataType: "JSON",
			method: "POST",
			data : {idtipe:idtipe,jenis_pertemuan_id:jenis_pertemuan_id},
			success: function(data) {
				$("#idpoli").append(data);
			}
		});

	});

	$("#btn_tambah_logic").click(function() {
		let jenis_pertemuan_id=$("#jenis_pertemuan_id").val();
		let idtipe=$("#idtipe").val();
		let idpoli=$("#idpoli").val();
		let iddokter=$("#iddokter").val();
		let st_gc=$("#st_gc").val();
		let st_sp=$("#st_sp").val();
		let st_sc=$("#st_sc").val();
		
		if (jenis_pertemuan_id=='#'){
			sweetAlert("Maaf...", "Tentukan Jenis Pertemuan", "error");
			return false;
		}
		if (idtipe=='#'){
			sweetAlert("Maaf...", "Tentukan Tipe", "error");
			return false;
		}
		if (idpoli=='#'){
			sweetAlert("Maaf...", "Tentukan Poliklinik", "error");
			return false;
		}
		if (iddokter=='#'){
			sweetAlert("Maaf...", "Tentukan Dokter", "error");
			return false;
		}
		
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}apm_setting/simpan_logic', 
			dataType: "JSON",
			method: "POST",
			data : {
					jenis_pertemuan_id:jenis_pertemuan_id,
					idtipe:idtipe,
					idpoli:idpoli,
					iddokter:iddokter,
					st_gc:st_gc,
					st_sp:st_sp,
					st_sc:st_sc,
				
				},
			complete: function(data) {
				$("#jenis_pertemuan_id").val('#').trigger('change');
				$("#idtipe").val('#').trigger('change');
				$("#idpoli").val('0').trigger('change');
				$("#iddokter").val('0').trigger('change');
				$('#index_logic').DataTable().ajax.reload( null, false ); 
				$("#cover-spin").hide();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}
		});
	});

	function hapus_logic(id){
		swal({
			title: "Anda Yakin ?",
			text : "Akan Hapus Logic?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			
			$.ajax({
					url: '{site_url}apm_setting/hapus_logic',
					type: 'POST',
					data: {id: id},
					complete: function() {
						$('#index_logic').DataTable().ajax.reload( null, false ); 
						$("#cover-spin").hide();
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						
					}
				});
		});
	}

	// LOGIC CHECKIN
	function load_logic_checkin(){
		$('#checkin_logic').DataTable().destroy();	
		$('#checkin_logic').DataTable({
				autoWidth: false,
				searching: true,
				pageLength: 100,
				serverSide: true,
				processing: true,
				order: [],
				ordering: false,
				ajax: { 
					url: '{site_url}apm_setting/load_logic_checkin', 
					type: "POST" ,
					dataType: 'json',
					data: {}
				}
		});
	}

	$("#checkin_idtipe").change(function() {
		let idtipe = $("#checkin_idtipe").val();
		let jenis_pertemuan_id = $("#checkin_jenis_pertemuan_id").val();

		$("#checkin_idpoli").empty();

		$.ajax({
			url: '{site_url}apm_setting/list_poli_2', 
			dataType: "JSON",
			method: "POST",
			data : {idtipe:idtipe,jenis_pertemuan_id:jenis_pertemuan_id},
			success: function(data) {
				$("#checkin_idpoli").append(data);
			}
		});
	});

	$("#btn_tambah_logic_checkin").click(function() {
		let jenis_pertemuan_id=$("#checkin_jenis_pertemuan_id").val();
		let idtipe=$("#checkin_idtipe").val();
		let idpoli=$("#checkin_idpoli").val();
		let iddokter=$("#checkin_iddokter").val();
		let st_gc=$("#checkin_st_gc").val();
		let st_sp=$("#checkin_st_sp").val();
		let st_sc=$("#checkin_st_sc").val();
		
		if (jenis_pertemuan_id=='#'){
			sweetAlert("Maaf...", "Tentukan Jenis Pertemuan", "error");
			return false;
		}
		if (idtipe=='#'){
			sweetAlert("Maaf...", "Tentukan Tipe", "error");
			return false;
		}
		if (idpoli=='#'){
			sweetAlert("Maaf...", "Tentukan Poliklinik", "error");
			return false;
		}
		if (iddokter=='#'){
			sweetAlert("Maaf...", "Tentukan Dokter", "error");
			return false;
		}
		
		$("#cover-spin").show();

		$.ajax({
			url: '{site_url}apm_setting/simpan_logic_checkin', 
			dataType: "JSON",
			method: "POST",
			data : {
					jenis_pertemuan_id:jenis_pertemuan_id,
					idtipe:idtipe,
					idpoli:idpoli,
					iddokter:iddokter,
					st_gc:st_gc,
					st_sp:st_sp,
					st_sc:st_sc,
				
				},
			complete: function(data) {
				$("#checkin_jenis_pertemuan_id").val('#').trigger('change');
				$("#checkin_idtipe").val('#').trigger('change');
				$("#checkin_idpoli").val('0').trigger('change');
				$("#checkin_iddokter").val('0').trigger('change');
				$('#checkin_logic').DataTable().ajax.reload( null, false ); 
				$("#cover-spin").hide();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}
		});
	});

	function hapus_logic_checkin(id){
		swal({
			title: "Anda Yakin ?",
			text : "Akan Hapus Logic?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			
			$.ajax({
					url: '{site_url}apm_setting/hapus_logic_checkin',
					type: 'POST',
					data: {id: id},
					complete: function() {
						$('#checkin_logic').DataTable().ajax.reload( null, false ); 
						$("#cover-spin").hide();
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						
					}
				});
		});
	}
</script>