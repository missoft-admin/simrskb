<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<style>
	.scrollme {
		overflow-x: auto;
	}
    .profile-dockter {
        background-color: #379ee7;
    }
    .profile-dockter .image {
        padding: 10px 10px 0 10px;
    }
    .profile-dockter .image img {
        width: 200px;
    }
    .profile-dockter .information {
        text-align: center;
        padding: 16px;
        color: #fff;
    }
    .profile-dockter .information .name {
        font-weight: bold;
        font-size: 14px;
    }
    .profile-dockter .information .uniqueId {
        font-size: 12px;
    }

    .timetable {
        color: #fff;
        width: 100%;
        padding: 8px;
        margin-bottom: 10px;
    }
    .timetable .time {
        font-size: 14px;
        font-weight: bold;
    }
    .timetable.success {
        background-color: #e9ad25;
    }
    .timetable.danger {
        background-color: #c54736;
    }
	.timetable.default {
        background-color: #7a7a7a;
    }
	.pasien-hadir {
        background-color: #379ee7;
    }
	.pasien-hapus {
        background-color: #c54736;
    }
	.pasien-belum_hadir {
        background-color: #a59e9d;
    }
	.pasien-beres {
        background-color: #1cbd46;
    }
    .column-day {
        background-color: #000000;
        font-weight: bold;
        color: #fff;
    }
	.column-libur {
        background-color: #f31313;
        font-weight: bold;
        color: #fff;
    }
	.column-nothing {
        background-color: #c54736;
        font-weight: bold;
        color: #fff;
    }
	.column-poli {
        background-color: #379ee7;
        font-weight: bold;
        color: #fff;
    }

    .header-section {
        padding: 8px;
        font-weight: bold;
        text-decoration: underline;
        text-transform: uppercase;
    }
</style>
<style>
.tableFix { /* Scrollable parent element */
  position: relative;
  overflow: auto;
  height: 500px;
}

.tableFix table{
  width: 100%;
  border-collapse: collapse;
}

.tableFix th,
.tableFix td{
  padding: 8px;
  text-align: left;
}

.tableFix thead th {
  position: sticky;  /* Edge, Chrome, FF */
  top: 0px;
  background: #fff;  /* Some background is needed */
}
</style>
<style>
	/* kalendar */
	table.kalendar {
        width: 100%;
		border-left: 1px solid #e6e9ea;
	}

	tr.kalendar-row {}

	td.kalendar-day {
		min-height: 80px;
		font-size: 11px;
		position: relative;
	}

	* html div.kalendar-day {
		height: 80px;
	}

	td.kalendar-day:hover {
		background: #eceff5;
	}
	td.kalendar-day-last {
		background: #fdfdfd;
		min-height: 80px;
	}

	td.kalendar-day-np {
		background: #fdfdfd;
		min-height: 80px;
	}

	* html div.kalendar-day-np {
		height: 80px;
	}

	td.kalendar-day-head {
        width: calc(100% / 7);
        border-right: 1px solid #2c7aca;
        padding: 5px;
        text-transform: uppercase;
        font-size: 12px;
        font-weight: bold;
        color: #818589;
        color: #fff;
        background-color: #448cd6;
	}

    td.kalendar-day-head:nth-child(7) {
        border: none;
    }

	div.day-number {
        padding: 8px;
        font-weight: bold;
        color: #7c878d;
        cursor: pointer;
        min-height: 100px;
	}

	div.day-number-mini {
        padding: 8px;
        font-weight: bold;
        color: #7c878d;
        cursor: pointer;
	}

	/* shared */
	td.kalendar-day,
	td.kalendar-day-np {
		width: 120px;
		padding: 5px;
		border-bottom: 1px solid #e6e9ea;
		border-right: 1px solid #e6e9ea;
	}

	bg-isi {
		background-color: #ffcac9;
	}
	.kalendar-event {
		margin-top: 10px;
	}

	.kalendar-event a {
        font-weight: 500;
        padding: 3px 6px;
        border-radius: 4px;
        background-color: #ce5151;
        color: #fff;
        word-wrap: break-word;
	}

	.kalendar-event a.FULL {
        background-color: #d90234;
	}

	.kalendar-event a.AVAILABLE {
        background-color: #1db573;
	}

	.kalendar-event a.PUBLISH {
        background-color: #518fce;
	}

	.kalendar-event a.SELESAI {
        background-color: #51ce57;
	}

	.kalendar-event a.DIBATALKAN {
        background-color: #ce5151;
	}

    div.scroll-content {
        overflow-x: auto;
        white-space: nowrap;
    }
    div.scroll-content [class*="col"]{
        display: inline-block;
        float: none;
    }
</style>
<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		
		<?php if (UserAccesForm($user_acces_form,array('1863'))){ ?>
		<li class="<?=($tab=='2'?'active':'')?>">
			<a href="#tab_2"  onclick="load_index_daftar()"><i class="fa fa-calendar-check-o"></i> Daftar Reservasi</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1875'))){ ?>
		<li class="<?=($tab=='3'?'active':'')?>">
			<a href="#tab_3"  onclick="hasil_generate_ls()"><i class="fa fa-calendar"></i> Schedule Reservasi</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1876'))){ ?>
		<li class="<?=($tab=='4'?'active':'')?>">
			<a href="#tab_4"  onclick="hasil_generate_petugas()"><i class="fa fa-user-plus"></i> Schedule Petugas</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1877'))){ ?>
		<li class="<?=($tab=='5'?'active':'')?>">
			<a href="#tab_5"  onclick="hasil_generate_jadwal()"><i class="fa fa-calendar"></i> Calendar View</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1878'))){ ?>
		<li class="<?=($tab=='6'?'active':'')?>">
			<a href="#tab_6"  onclick="show_all_patien()"><i class="si si-call-in"></i> All Patien</a>
		</li>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1879'))){ ?>
		<li class="<?=($tab=='7'?'active':'')?>">
			<a href="#tab_6"  onclick="show_my_patien()"><i class="fa fa fa-user-md"></i> My Patien</a>
		</li>
		<?}?>
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<input type="hidden" id="show_all" value="{show_all}">
		
		<?php if (UserAccesForm($user_acces_form,array('1874'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_2">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Jenis Reservasi</label>
						<div class="col-md-9">
							<select id="reservasi_cara2" name="reservasi_cara2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Reservasi -</option>
								<?foreach($list_reservasi_cara as $r){?>
								<option value="<?=$r['id']?>"><?=$r['nama']?></option>
								<?}?>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Status Pasien</label>
						<div class="col-md-9">
							<select id="statuspasienbaru2" name="statuspasienbaru2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Status -</option>
								<option value="1">BARU</option>
								<option value="0">LAMA</option>
								
							</select>
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Status Kehadiran</label>
						<div class="col-md-9">
							<select id="status_kehadiran2" name="status_kehadiran2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Status -</option>
								<option value="1">HADIR</option>
								<option value="0">BELUM HADIR</option>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tanggal Reservasi</label>
						<div class="col-md-9">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" id="tanggal_12" name="tanggal_1" placeholder="From" value="{tanggal_1}"/>
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" id="tanggal_22" name="tanggal_2" placeholder="To" value="{tanggal_2}"/>
							</div>
						</div>
					</div>
					
					
				</div>
				<div class="col-md-6 push-10">
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">No Medrec</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="no_medrec2" placeholder="No. Medrec" name="no_medrec2" value="{no_medrec}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Nama Pasien</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="namapasien2" placeholder="Nama Pasien" name="namapasien2" value="{namapasien}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tujuan Poliklinik</label>
						<div class="col-md-9">
							<select id="idpoli2" name="idpoli2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Poliklinik -</option>
								<?foreach($list_poli as $r){?>
								<option value="<?=$r->id?>"><?=$r->nama?></option>
								<?}?>
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tujuan Dokter</label>
						<div class="col-md-9">
							<select id="iddokter2" name="iddokter2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Dokter -</option>
								<?foreach($list_dokter as $r){?>
								<option value="<?=$r->id?>"><?=$r->nama?></option>
								<?}?>
								
								
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-3 control-label" for="tanggal">Tanggal Transaksi</label>
						<div class="col-md-9">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" id="waktu_reservasi_12" name="waktu_reservasi_12" placeholder="From" value="{waktu_reservasi_1}"/>
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" id="waktu_reservasi_22" name="waktu_reservasi_22" placeholder="To" value="{waktu_reservasi_2}"/>
							</div>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-3 control-label" for="btn_filter_proses"></label>
						<div class="col-md-9">
							<button class="btn btn-success text-uppercase" type="button" id="btn_filter_daftar" name="btn_filter_daftar" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						</div>
					</div>
						
				</div>
			</div>
			<?php echo form_close() ?>
			<div class="row   ">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="index_daftar">
							<thead>
								<tr>
									<th width="10%">No</th>
									<th width="10%">Jenis Reservasi</th>
									<th width="10%">Tanggal Transaksi</th>
									<th width="10%">Status Pasien</th>
									<th width="10%">Pasien</th>
									<th width="10%">Tujuan</th>
									<th width="10%">Tanggal Reservasi</th>
									<th width="10%">Antrian</th>
									<th width="10%">Status Reservasi</th>
									<th width="10%">Kelompok Pasien</th>
									<th width="15%">Action</th>
								   
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<?}?>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?>" id="tab_3">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label class="col-md-12" for="login1-username">Tujuan Kunjungan</label>
						<div class="col-md-12">
							<select id="idpoli3" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<?foreach($list_poli as $r){?>
								<option value="<?=$r->id?>" selected><?=$r->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					
				</div>
				<div class="col-md-2" hidden>
					<div class="form-group">
						<label class="col-md-12" for="login1-username">Dokter</label>
						<div class="col-md-12">
							<select id="iddokter3" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>-Semua Dokter-</option>
								<?foreach($list_dokter as $r){?>
								<option value="<?=$r->id?>"><?=$r->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label class="col-md-12" for="login1-username">View</label>
						<div class="col-md-12">
							<select id="present" name="present" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								
								<option value="1" <?=($present=="1"?'selected':'')?>>MONTHLY</option>
								<option value="2" <?=($present=="2"?'selected':'')?>>WEEKLY</option>
								<option value="3" <?=($present=="3"?'selected':'')?>>DAILY</option>
								
							</select>
						</div>
					</div>
					
				</div>
				<div class="col-md-2 div_present">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-12" for="login1-username">&nbsp;</label>
						<div class="col-md-12">
						<div class="btn-group btn-group-justified">
							<div class="btn-group">
								<button class="btn btn-default" id="btn_back2" type="button"><i class="fa fa-arrow-left"></i></button>
							</div>
							
							<div class="btn-group">
								<button class="btn btn-default" id="btn_next2" type="button"><i class="fa fa-arrow-right"></i></button>
							</div>
							
						</div>
						</div>
					</div>
					
				</div>
				<div class="col-md-3 div_date">
					<div class="form-group">
						<label class="col-md-12" for="login1-username">Date</label>
						<div class="col-md-12">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" id="tanggaldari" name="tanggaldari" placeholder="From" value="{tanggaldari}">
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" id="tanggalsampai" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
							</div>
						</div>
					</div>
					
				</div>
				<div class="col-md-1 div_date">
					<div class="form-group">
						<label class="col-md-12" for="login1-username">&nbsp;</label>
						<div class="col-md-12">
							<button class="btn btn-success present" id="btn_filter_ls" type="button"><i class="fa fa-search"></i> Cari Jadwal</button>
						</div>
					</div>
					
				</div>
			</div>
			<div class="row push-20-t">
					<div class="col-md-12">
						<div class="scrollme">
						<div id="div_ls"></div>
						</div>
					</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<div class="tab-pane fade fade-left <?=($tab=='4'?'active in':'')?>" id="tab_4">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label class="col-md-12" for="login1-username">Tujuan Kunjungan</label>
						<div class="col-md-12">
							<select id="idpoli4" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<?foreach($list_poli as $r){?>
								<option value="<?=$r->id?>" selected><?=$r->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label class="col-md-12" for="login1-username">Dokter</label>
						<div class="col-md-12">
							<select id="iddokter4" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>-Semua Dokter-</option>
								<?foreach($list_dokter as $r){?>
								<option value="<?=$r->id?>"><?=$r->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					
				</div>
				
				<div class="col-md-3">
					<div class="form-group">
						<label class="col-md-12" for="login1-username">Date</label>
						<div class="col-md-12">
							<div class="input-group" data-date-format="dd-mm-yyyy">
								<input class="form-control" disabled type="text" id="tanggalpetugas" name="tanggalpetugas" placeholder="From" value="{tanggalpetugas}">
								<span class="input-group-btn">
									<button class="btn btn-default" id="btn_back4" type="button"><i class="fa fa-arrow-left"></i></button>
									<button class="btn btn-default" id="btn_next4" type="button"><i class="fa fa-arrow-right"></i></button>
								</span>
								
							</div>
						</div>
					</div>
					
				</div>
				
			</div>
			<div class="row push-20-t">
					<div class="col-md-12">
						<div class="scrollme">
						<div id="div_ls_petugas"></div>
						</div>
					</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<div class="tab-pane fade fade-left <?=($tab=='5'?'active in':'')?>" id="tab_5">
			<?php echo form_open('#','class="form-horizontal" id="form-work"') ?>
			<input class="form-control input-sm " type="hidden" id="present5" name="present5" value="{present5}"/>	
			<input class="form-control input-sm " readonly type="hidden" id="tanggal" name="tanggal" value="{tanggal}"/>	
			<div class="row">
				<div class="col-md-3">
					<div class="form-group" style="margin-bottom: 5px;">
						<div class="col-md-12">
							<select id="idpoli5" name="idpoli5" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<?foreach($list_poli as $r){?>
								<option value="<?=$r->id?>" selected><?=$r->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
					
				</div>
				<div class="col-md-2">
					<div class="form-group" style="margin-bottom: 5px;">
						<div class="btn-group btn-group-justified">
							<div class="btn-group">
								<button class="btn btn-default" id="btn_back5" type="button"><i class="fa fa-arrow-left"></i></button>
							</div>
							
							<div class="btn-group">
								<button class="btn btn-default" id="btn_next5" type="button"><i class="fa fa-arrow-right"></i></button>
							</div>
							<div class="btn-group">
								<button class="btn btn-default" id="btn_present" type="button"> PRESENT</button>
							</div>
						</div>
					</div>
					
				</div>
				<div class="col-md-4">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="control-label" for="edit_vendor"></label>
					</div>
					
				</div>
				<div class="col-md-3">
					<div class="form-group" style="margin-bottom: 5px;">
						<div class="btn-group btn-group-justified">
							<div class="btn-group">
								<button class="btn btn-default present5" id="present_1" type="button">MONTH</button>
							</div>
							
							<div class="btn-group">
								<button class="btn btn-default present5" id="present_2"  type="button">WEEK</button>
							</div>
							<div class="btn-group">
								<button class="btn btn-default present5" id="present_3"  type="button">DAY</button>
							</div>
						</div>
					</div>
					
				</div>
				
			</div>
			<div class="row push-20-t">
				<div class="col-md-12">
					<h4 class="text-center text-uppercase">RESERVASI REHABILITAS MEDIS</h4>
					<h4 class="text-center text-uppercase" id="nama_periode"></h4>
					<br>
					<div id="hasil"></div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
		<div class="tab-pane fade fade-left <?=($tab=='6' || $tab=='7'?'active in':'')?>" id="tab_6">
			<div class="block-content bg-primary">
			<?php echo form_open('#','class="form-horizontal" id="form-work"') ?>
			<div class="row pull-10">
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="login1-password">PPA</label>
						<div class="col-xs-12">
							<select id="mppa_id" name="mppa_id" disabled class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" >- Pilih PPA -</option>
								<?foreach(get_all('mppa',array('staktif'=>1)) as $r){?>
								<option value="<?=$r->id?>" <?=($login_ppa_id==$r->id?'selected':'')?> ><?=$r->nama?></option>
								<?}?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-2 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">Poliklinik</label>
						<div class="col-xs-12">
							<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" >- Semua Poliklinik -</option>
								<?foreach($list_poli as $r){?>
								<option value="<?=$r->id?>" <?=($jumlah_poli==1?'selected':'')?>><?=$r->nama?></option>
								<?}?>
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="iddokter_patien">Dokter</label>
						<div class="col-xs-12">
							<select id="iddokter_patien" name="iddokter_patien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
								<option value="#" selected>- Semua Dokter -</option>
								<?foreach(get_all('mppa',array('staktif'=>1,'tipepegawai'=>2)) as $r){?>
								<option value="<?=$r->id?>" ><?=$r->nama?></option>
								<?}?>
								
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-2 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">Tanggal</label>
						<div class="col-xs-12">
							<div class="js-datepicker input-group date">
								<input class="js-datepicker form-control div_tidak_dikenal" type="text" id="tanggal_antrian" data-date-format="dd-mm-yyyy"  value="{tanggal_antrian}" placeholder="Tanggal Layanan">
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-2 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">&nbsp;&nbsp;</label>
						<div class="col-xs-12">
							<button class="btn btn-success text-uppercase" type="button" onclick="load_index_all_patien()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						</div>
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			</div>
			<div class="block push-10-t">
				<ul class="nav nav-pills">
					<li id="div_1" class="<?=($tab=='1'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab_detail(1)"><i class="si si-list"></i> Semua</a>
					</li>
					<li  id="div_2" class="<?=($tab=='2'?'active':'')?>">
						<a href="javascript:void(0)"  onclick="set_tab_detail(2)"><i class="si si-pin"></i> Menunggu Antrian </a>
					</li>
					<li  id="div_3" class="<?=($tab=='3'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab_detail(3)"><i class="fa fa-check-square-o"></i> Telah Dilayani</a>
					</li>
					<li  id="div_4" class="<?=($tab=='4'?'active':'')?>">
						<a href="javascript:void(0)"  onclick="set_tab_detail(4)"><i class="fa fa-times"></i> Terlewati</a>
					</li>
				</ul>
				<div class="block-content">
						<input type="hidden" id="tab_detail" value="{tab_detail}">
						<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
						<div class="row pull-10">
							
							<div class="col-md-3">
								<div class="form-group" style="margin-bottom: 10px;">
									<label class="col-md-3 control-label" for="tanggal">No Medrec</label>
									<div class="col-md-9">
										<input type="text" class="form-control" id="no_medrec_patien" placeholder="No. Medrec" name="no_medrec" value="{no_medrec}">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 10px;">
									<label class="col-md-3 control-label" for="tanggal">Nama Pasien</label>
									<div class="col-md-9">
										<input type="text" class="form-control" id="namapasien_patien" placeholder="Nama Pasien" name="namapasien" value="{namapasien}">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 10px;">
									<label class="col-md-3 control-label" for="tanggal">Status</label>
									<div class="col-md-9">
										<select id="status_transaksi_patien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<option value="1" selected>- Semua Status -</option>
											<option value="2">MENUNGGU ANTRIAN</option>
											<option value="3">TELAH DILAYANI</option>
											<option value="4">TERLEWATI</option>
											
										</select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="btn_filter_all"></label>
									<div class="col-md-9">
										<button class="btn btn-success text-uppercase" type="button" onclick="load_index_all_patien()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
									</div>
								</div>
								
							</div>
							<div class="col-md-9 push-10">
								<div class="form-group push-5 div_counter" >
									<div class="col-sm-3">
										<div class="block block-themed">
											<div class="block-header bg-danger" style="border-bottom: 2px solid white;padding:10">
												<h3 class="block-title text-center" >ANTRIAN DILAYANI</h3>
											</div>
											<div class="content-mini content-mini-full bg-danger">
												<div>
													<div class="h2 text-center text-white"></div>
													<div class="h5 text-center text-white"></div>
													<div class="h5 text-center text-white push-10"></div>
													<div class="text-center ">
													</div>
												</div>
												<div class="text-center ">
														<button class="btn btn-block btn-success" type="button" disabled onclick="recall_manual()" title="Panggil Ulang"><i class="si si-refresh pull-left"></i> PANGGIL ULANG</button>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="block block-themed">
											<div class="block-header bg-success" style="border-bottom: 2px solid white;padding:10">
												<h3 class="block-title text-center" >ANTRIAN SELANJUTNYA</h3>
											</div>
											<div class="content-mini content-mini-full bg-success">
												<div>
													<div class="h2 text-center text-white"></div>
													<div class="h5 text-center text-white"></div>
													<div class="h5 text-center text-white push-10"></div>
												</div>
												<div class="text-center ">
													<button class="btn btn-block btn-primary " disabled onclick="call_antrian_manual()" title="Lanjut Panggil" type="button"><i class="glyphicon glyphicon-volume-up pull-left"></i> SELANJUTNYA</button>
												</div>
											</div>
											
										</div>
										
									</div>
									<div class="col-sm-3">
										<div class="block block-themed">
											<div class="block-header bg-success" style="border-bottom: 2px solid white;padding:10">
												<h3 class="block-title text-center" >SISA ANTRIAN</h3>
											</div>
											<div class="content-mini content-mini-full bg-success">
												<div>
													<div class="h2 text-center text-white"></div>
													<div class="h5 text-center text-white">&nbsp;</div>
													<div class="h5 text-center text-white push-10">&nbsp;</div>
												</div>
												<div class="text-center ">
													<button class="btn btn-block btn-warning" type="button" disabled onclick="tf_manual()"  title="LEWATI"><i class="fa fa-mail-forward pull-left"></i> LEWATI</button>
												</div>
											</div>
										</div>
									</div>
									
									<div class="col-sm-3">
										<div class="block block-themed">
											<div class="block-header bg-success" style="border-bottom: 2px solid white;padding:10">
												<h3 class="block-title text-center" >TOTAL ANTRIAN</h3>
											</div>
											<div class="content-mini content-mini-full bg-success">
												<div>
													<div class="h1 font-w700 text-center text-white">5</div>
												</div>
											</div>
										</div>
									</div>
									
									
										
								</div>
								
							</div>
						</div>
						<?php echo form_close() ?>
						<div class="row   ">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table" id="index_all">
										<thead>
											<tr>
												<th width="10%"></th>
												<th width="45%"></th>
												<th width="25%"></th>
												<th width="20%"></th>
												
											</tr>
											
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					
				
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal in" id="modal_petugas" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Pilih Petugas</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-12 push-10">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-12" for="snomedrec">Pilih Petugas</label>
									<div class="col-md-12">
										<select id="idpetugas" name="idpetugas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Petugas" required>
											
											
										</select>
									</div>
								</div>
								<input type="hidden" readonly id="app_idpoli" name="app_idpoli" value="">
								<input type="hidden" readonly id="app_id" name="app_id" value="">
								<input type="hidden" readonly id="app_tab" name="app_tab" value="">
								<input type="hidden" readonly id="jenis_update" name="jenis_update" value="">
							</div>
						
						</div>
					</div>

				</div>
				<div class="block-content block-content-mini block-content-full border-t">
					<div class="row">
						
						<div class="col-xs-12 text-right">
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
							<button class="btn btn-sm btn-success" type="button" id="btn_save_petugas"><i class="fa fa-save"></i> Update</button>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width:50%">
        <form class="form-horizontal" action="javascript:void(0)" id="frm1">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">Detail Slot</h3>
                    </div>
                    <div class="block-content">
						<div class="block">
						<ul class="nav nav-tabs" data-toggle="tabs" id="tabs">
							<li class="active" id="tab_kuota">
								<a href="#btabs-animated-slideleft-kuota"><i class="fa fa-pencil"></i> Pengaturan Slot</a>
							</li>
							<li id="tab_add">
								<a href="#btabs-animated-slideleft-note"><i class="fa fa-pencil"></i> Update Notes</a>
							</li>
							<li id="tab_note">
								<a href="#btabs-animated-slideleft-list"><i class="fa fa-list-ol"></i> History Notes</a>
							</li>
						</ul>
						<div class="block-content tab-content">
							<div class="tab-pane fade fade-left in active" id="btabs-animated-slideleft-kuota">
								<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Hari</label>
											<div class="col-md-10">
												<input class="form-control input-sm " readonly type="text" id="namahari" name="namahari" value=""/>	
												<input class="form-control input-sm " readonly type="hidden" id="kodehari" name="kodehari" value=""/>	
												<input class="form-control input-sm " readonly type="hidden" id="tmp_tanggal" name="tmp_tanggal" value=""/>	
												<input class="form-control input-sm " readonly type="hidden" id="tmp_idpoli" name="tmp_idpoli" value=""/>	
												<input class="form-control input-sm " readonly type="hidden" id="tmp_jam_id" name="tmp_jam_id" value=""/>	
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Jam</label>
											<div class="col-md-10">
												<input class="form-control input-sm " readonly type="text" id="jam" name="jam" value=""/>	
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="tipeid">Kuota</label>
											<div class="col-md-10">
												<select id="kuota" name="kuota" class="form-control js-select2" style="width: 100%;" data-placeholder="Choose one..">
													<option value="999">TANPA BATAS</option>
													<option value="0" selected>ISI PENUH</option>
													<?for ($x = 1; $x <= 100; $x++) {?>
														<option value="<?=$x?>"><?=$x?></option>
													<?}?>
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="tipeid">Reservasi Online</label>
											<div class="col-md-10">
												<select id="st_reservasi_online" name="st_reservasi_online" class="form-control js-select2" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1">RESERVASI ONLINE</option>
													<option value="0">HANYA OFFLINE</option>
													
												</select>
											</div>
										</div>
										
									</div>
									<div class="col-md-12">
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-2 control-label" for="idprespektif"></label>
											<div class="col-md-10">
												<button class="btn btn-success text-uppercase"  type="button" name="btn_save_kuota" id="btn_save_kuota"><i class="fa fa-save"></i> Simpan</button>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_close() ?>
							</div>
							<div class="tab-pane fade fade-left" id="btabs-animated-slideleft-note">
								<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
								<div class="row">
									<div class="col-md-12">
										
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Notes</label>
											<div class="col-md-10">
												<input class="form-control input-sm " type="text" id="catatan" name="catatan" value=""/>	
											</div>
										</div>
										
									</div>
									<div class="col-md-12">
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-2 control-label" for="idprespektif"></label>
											<div class="col-md-10">
												<button class="btn btn-success text-uppercase"  type="button" name="btn_save_catatan" id="btn_save_catatan"><i class="fa fa-save"></i> Simpan</button>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_close() ?>
							</div>
							<div class="tab-pane fade fade-left " id="btabs-animated-slideleft-list">
								<h5 class="font-w300 push-15" id="lbl_list_rekening">History Notes </h5>
								<table class="table table-bordered table-striped" id="index_Catatan">
									<thead>
										<th style="width:5%">No</th>
										<th style="width:60%">Notes</th>
										<th style="width:35%">Posted By</th>
									</thead>
									<tbody></tbody>
								  
								</table>
							</div>
							
							
							
							
						</div>
					</div>
                  </div>
                </div>
                <div class="modal-footer" style="margin-top:20px">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>	
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
var present5;
var st_plus_minus;
var tab_detail=<?=$tab_detail?>;
var tab;
$(document).ready(function(){	
	tab=$("#tab").val();
	// alert(tab_detail);
	var tab=$("#tab").val();
	if (tab=='1'){
		load_index_proses();
		
	}
	if (tab=='2'){
		load_index_daftar();
		
	}
	refresh_present();
	if (tab=='3'){
		hasil_generate_ls();
		
	}
	if (tab=='4'){
		hasil_generate_petugas();
		
	}
	if (tab=='5'){
		refresh_present5();
		
	}
	if (tab=='6'){
		$("#show_all").val(1);
		set_tab_detail(tab_detail);
	}
	if (tab=='7'){
		$("#show_all").val(0);
		set_tab_detail(tab_detail);
	}
	startWorker();
	// alert(tab_detail);
})
function show_my_patien(){
	$("#show_all").val(0);
	load_index_all_patien();
}
function show_all_patien(){
	$("#show_all").val(1);
	load_index_all_patien();
}
function load_index_all_patien(){
	refres_div_counter();
	let all=$("#show_all").val();
	let tanggal=$("#tanggal_antrian").val();
	let idpoli=$("#idpoli").val();
	let iddokter_patien=$("#iddokter_patien").val();
	let no_medrec_patien=$("#no_medrec_patien").val();
	let namapasien_patien=$("#namapasien_patien").val();
	console.log(namapasien_patien);
	let status_transaksi_patien=$("#status_transaksi_patien").val();
	// alert(idpoli);
	$('#index_all').DataTable().destroy();	
	table = $('#index_all').DataTable({
            autoWidth: false,
            searching: false,
            pageLength: 50,
            serverSide: true,
			"processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			// "columnDefs": [
					// { "width": "3%", "targets": 0,  className: "text-right" },
					// { "width": "8%", "targets": [1,2,3,5,7,8,9],  className: "text-center" },
					// { "width": "12%", "targets": [4,10],  className: "text-center" },
					// { "width": "15%", "targets": 6,  className: "text-center" },
				// ],
            ajax: { 
                url: '{site_url}Tpoliklinik_rm_rj/index_all_patien', 
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal:tanggal,
						idpoli:idpoli,
						iddokter_patien:iddokter_patien,
						no_medrec:no_medrec_patien,
						namapasien:namapasien_patien,
						status_transaksi:status_transaksi_patien,
						all:all,
						
						
					   }
            }
        });
}

function set_tab_detail($tab){
	tab_detail=$tab;
	// alert(tab_detail)
	$("#cover-spin").show();
	document.getElementById("div_1").classList.remove("active");
	document.getElementById("div_2").classList.remove("active");
	document.getElementById("div_3").classList.remove("active");
	document.getElementById("div_4").classList.remove("active");
		$("#status_transaksi_patien").removeAttr("disabled");
		$("#status_reservasi").removeAttr("disabled");
	if (tab_detail=='1'){
		document.getElementById("div_1").classList.add("active");
		$("#status_transaksi_patien").val('1').trigger('change');
		$("#status_transaksi_patien").attr('disabled', 'disabled');
	}
	if (tab_detail=='2'){
		document.getElementById("div_2").classList.add("active");
		$("#status_transaksi_patien").val(2).trigger('change');
		// $("#status_transaksi_patien").removeAttr("disabled");
		$("#status_transaksi_patien").attr('disabled', 'disabled');
	}
	if (tab_detail=='3'){
		$("#status_transaksi_patien").val('3').trigger('change');
		document.getElementById("div_3").classList.add("active");
		$("#status_transaksi_patien").attr('disabled', 'disabled');
	}
	if (tab_detail=='4'){
		document.getElementById("div_4").classList.add("active");
		$("#status_transaksi_patien").val('4').trigger('change');
		// $("#status_reservasi").val('0').trigger('change');
		$("#status_transaksi_patien").attr('disabled', 'disabled');
	}
	$("#cover-spin").hide();
	load_index_all_patien();
}
function set_modal_petugas(id,tab,idpoli,idpetugas){
	$("#modal_petugas").modal('show');
	// $("#idpetugas").val(idpetugas).trigger('change');
	$("#app_idpoli").val(idpoli);
	$("#app_id").val(id);
	$("#app_tab").val(tab);
	$("#jenis_update").val('reservasi');
	$.ajax({
		url: '{site_url}tbooking_rehab/get_petugas/',
		method: "POST",
		dataType: "JSON",
		data: {
			idpoli:idpoli,
			idpetugas:idpetugas,
		},
		success: function(data) {
			$("#idpetugas").empty();
			$("#idpetugas").append(data);
			
		}
	});
}
function set_modal_petugas_poli(id,tab,idpoli,idpetugas){
	$("#modal_petugas").modal('show');
	// $("#idpetugas").val(idpetugas).trigger('change');
	$("#jenis_update").val('poli');
	$("#app_idpoli").val(idpoli);
	$("#app_id").val(id);
	$("#app_tab").val(tab);
	
	$.ajax({
		url: '{site_url}tbooking_rehab/get_petugas/',
		method: "POST",
		dataType: "JSON",
		data: {
			idpoli:idpoli,
			idpetugas:idpetugas,
		},
		success: function(data) {
			$("#idpetugas").empty();
			$("#idpetugas").append(data);
			
		}
	});
}
$("#btn_filter_ls").click(function() {
	hasil_generate_ls();
});	
$("#btn_save_petugas").click(function() {
	let iddokter=$("#idpetugas").val();
	let id=$("#app_id").val();
	let tab=$("#app_tab").val();
	let jenis_update=$("#jenis_update").val();
	$("#modal_petugas").modal('hide');
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_rm_rj/simpan_petugas/',
		method: "POST",
		dataType: "JSON",
		data: {
			id:id,
			iddokter:iddokter,
			jenis_update:jenis_update,
		},
		success: function(data) {
			$("#cover-spin").hide();
			if (jenis_update=='poli'){
				load_index_all();
				
			}else{
				load_index_daftar();
			}
			// $('#index_all').DataTable().ajax.reload( null, false );
			
			// if (tab=='1'){
				// $('#index_proses').DataTable().ajax.reload( null, false );
			// }
			// if (tab=='2'){
				// $('#index_daftar').DataTable().ajax.reload( null, false );
			// }
			// if (tab=='3'){
				// hasil_generate_ls();
			// }
			// if (tab=='4'){
				// hasil_generate_petugas();
			// }
		}
	});
});	
function refresh_present(){
	present=$("#present").val();
	if (present=='3'){
		$(".div_date").show();
		$(".div_present").hide();
	}else{
		$(".div_date").hide();
		$(".div_present").show();
		
	}
}
$("#present").change(function() {
	refresh_present();
	// if ($("#present").val()!='3'){
		st_plus_minus=0;
		refresh_tanggal2();
	// }
});
$("#iddokter4,#idpoli4").change(function() {
	hasil_generate_petugas();
});
function refresh_tanggal2(){
	var tanggaldari=$("#tanggaldari").val();
	var tanggalsampai=$("#tanggalsampai").val();
	present=$("#present").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/refresh_tanggal2/',
		method: "POST",
		dataType: "JSON",
		data: {
			tanggaldari:tanggaldari,
			tanggalsampai:tanggalsampai,
			present:present,
			st_plus_minus:st_plus_minus,
		},
		success: function(data) {
			$("#tanggaldari").val(data.tanggaldari);
			$("#tanggalsampai").val(data.tanggalsampai);
			$("#cover-spin").hide();
			hasil_generate_ls();
		}
	});
}
function refresh_tanggal_petugas(){
	var tanggalpetugas=$("#tanggalpetugas").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/refresh_tanggal_petugas/',
		method: "POST",
		dataType: "JSON",
		data: {
			tanggalpetugas:tanggalpetugas,
			st_plus_minus:st_plus_minus,
		},
		success: function(data) {
			$("#tanggalpetugas").val(data.tanggalpetugas);
			$("#cover-spin").hide();
			hasil_generate_petugas();
		}
	});
}
$("#btn_back2").click(function() {
	st_plus_minus='-1'
	refresh_tanggal2();
	
});
$("#btn_next2").click(function() {
	st_plus_minus='1'
	refresh_tanggal2();
	
});
$("#btn_back4").click(function() {
	st_plus_minus='-1'
	refresh_tanggal_petugas();
	
});
$("#btn_next4").click(function() {
	st_plus_minus='1'
	refresh_tanggal_petugas();
	
});
$("#idpoli3").change(function() {
	hasil_generate_ls();
});
$("#iddokter3").change(function() {
	hasil_generate_ls();
});
function hasil_generate_ls(){
	tanggaldari=$("#tanggaldari").val();
	tanggalsampai=$("#tanggalsampai").val();
	let idpoli=$("#idpoli3").val();
	let iddokter=$("#iddokter3").val();
	let present=$("#present").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tpoliklinik_rm_rj/hasil_generate/',
		method: "POST",
		dataType: "JSON",
		data: {
			tanggaldari:tanggaldari,
			tanggalsampai:tanggalsampai,
			present:present,
			idpoli:idpoli,
			iddokter:iddokter,
		},
		success: function(data) {
			$("#div_ls").html(data.tabel);
			$("#cover-spin").hide();
			
			// window.table = $('#index_ls').DataTable(
			  // {
			   // "autoWidth": false,
					// // "pageLength": 50,
					// autoWidth: true,
					// scrollX: true,
					// scrollY: 400,
					// "paging": true,
					// paging: false,
					// ordering: false,
					// scrollCollapse: true,
					// "processing": true,
					
				// ordering: [
				  // [1, 'asc']
				// ],
				// colReorder:
				// {
				  // fixedColumnsLeft: 3,
				  // fixedColumnsRight: 1
				// }
			  // });
			$("#cover-spin").hide();
		}
	});
}
function hasil_generate_petugas(){
	let tanggalpetugas=$("#tanggalpetugas").val();
	let idpoli=$("#idpoli4").val();
	let iddokter=$("#iddokter4").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}Tpoliklinik_rm_rj/hasil_generate_petugas/',
		method: "POST",
		dataType: "JSON",
		data: {
			tanggalpetugas:tanggalpetugas,
			idpoli:idpoli,
			iddokter:iddokter,
		},
		success: function(data) {
			$("#div_ls_petugas").html(data.tabel);
			$("#cover-spin").hide();
			
		}
	});
}
$(document).on("click","#btn_filter_proses",function(){
	load_index_proses();
});
$(document).on("click","#btn_filter_daftar",function(){
	load_index_daftar();
});

function load_index_proses(){
	$('#index_proses').DataTable().destroy();	
	let reservasi_cara=$("#reservasi_cara").val();
	let statuspasienbaru=$("#statuspasienbaru").val();
	let status_reservasi=$("#status_reservasi").val();
	let tanggal_1=$("#tanggal_1").val();
	let tanggal_2=$("#tanggal_2").val();
	let no_medrec=$("#no_medrec").val();
	let namapasien=$("#namapasien").val();
	let idpoli=$("#idpoli").val();
	let iddokter=$("#iddokter").val();
	let waktu_reservasi_1=$("#waktu_reservasi_1").val();
	let waktu_reservasi_2=$("#waktu_reservasi_2").val();
	let status_kehadiran=$("#status_kehadiran").val();
	
	
	table = $('#index_proses').DataTable({
            autoWidth: false,
            searching: false,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "3%", "targets": 0,  className: "text-right" },
					{ "width": "8%", "targets": [1,2,3,5,7,8,9],  className: "text-center" },
					{ "width": "12%", "targets": [4,10],  className: "text-center" },
					{ "width": "15%", "targets": 6,  className: "text-center" },
				],
            ajax: { 
                url: '{site_url}tbooking_rehab/getIndex_proses', 
                type: "POST" ,
                dataType: 'json',
				data : {
						reservasi_cara:reservasi_cara,
						statuspasienbaru:statuspasienbaru,
						status_reservasi:status_reservasi,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						no_medrec:no_medrec,
						namapasien:namapasien,
						idpoli:idpoli,
						iddokter:iddokter,
						waktu_reservasi_1:waktu_reservasi_1,
						waktu_reservasi_2:waktu_reservasi_2,
						status_kehadiran:status_kehadiran,
						
					   }
            }
        });
}
function load_index_daftar(){
	$('#index_daftar').DataTable().destroy();	
	let reservasi_cara=$("#reservasi_cara2").val();
	let statuspasienbaru=$("#statuspasienbaru2").val();
	let tanggal_1=$("#tanggal_12").val();
	let tanggal_2=$("#tanggal_22").val();
	let no_medrec=$("#no_medrec2").val();
	let namapasien=$("#namapasien2").val();
	let idpoli=$("#idpoli2").val();
	let iddokter=$("#iddokter2").val();
	let waktu_reservasi_1=$("#waktu_reservasi_12").val();
	let waktu_reservasi_2=$("#waktu_reservasi_22").val();
	let status_kehadiran=$("#status_kehadiran2").val();
	
	
	table = $('#index_daftar').DataTable({
            autoWidth: false,
            searching: false,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "3%", "targets": 0,  className: "text-right" },
					{ "width": "8%", "targets": [1,2,3,5,7,8,9],  className: "text-center" },
					{ "width": "12%", "targets": [4,10],  className: "text-center" },
					{ "width": "15%", "targets": 6,  className: "text-center" },
				],
            ajax: { 
                url: '{site_url}Tpoliklinik_rm_rj/getIndex_daftar', 
                type: "POST" ,
                dataType: 'json',
				data : {
						reservasi_cara:reservasi_cara,
						statuspasienbaru:statuspasienbaru,
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						no_medrec:no_medrec,
						namapasien:namapasien,
						idpoli:idpoli,
						iddokter:iddokter,
						waktu_reservasi_1:waktu_reservasi_1,
						waktu_reservasi_2:waktu_reservasi_2,
						status_kehadiran:status_kehadiran,
						
					   }
            }
        });
}
function verifikasi_poli(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan verifikasi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}tbooking_rehab/verifikasi_poli',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_proses').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Publish'});
					
				}
			});
	});
}
function batalkan_poli(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Batal Verifikasi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}tbooking_rehab/batal_poli',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_proses').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Publish'});
					
				}
			});
	});
}

$("#present_1").click(function() {
	present5=1;
	$("#present5").val(present5);
	refresh_present5();
});
$("#present_2").click(function() {
	present5=2;
	// alert('sini');
	$("#present5").val(present5);
	refresh_present5();
});
$("#present_3").click(function() {
	present5=3;
	$("#present5").val(present5);
	refresh_present5();
});
function refresh_present5(){
	clear_present5();
	present5=$("#present5").val();
	// alert(present5);
	if (present5=='1'){
		$("#present_1").addClass("btn-success");
	}
	if (present5=='2'){
		$("#present_2").addClass("btn-success");
	}
	if (present5=='3'){
		$("#present_3").addClass("btn-success");
	}
	hasil_generate_jadwal();
}
$("#btn_back5").click(function() {
	st_plus_minus='-1'
	refresh_tanggal5();
	
});
$("#btn_next5").click(function() {
	// alert('next');
	st_plus_minus='1'
	refresh_tanggal5();
	
});
function clear_present5(){
	 $(".present5").removeClass("btn-success");
}
function refresh_tanggal5(){
	var tanggal=$("#tanggal").val();
	present=$("#present5").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/refresh_tanggal/',
		method: "POST",
		dataType: "JSON",
		data: {
			tanggal:tanggal,
			present:present,
			st_plus_minus:st_plus_minus,
		},
		success: function(data) {
			$("#tanggal").val(data.tanggal);
			$("#cover-spin").hide();
			hasil_generate_jadwal();
		}
	});
}
$("#btn_present").click(function() {
	present5=parseFloat($("#present5").val())+1;
	if (present5>3){
		present5=1;
	}
	st_plus_minus=0;
	$("#present5").val(present5);
	refresh_present5();
});
function hasil_generate_jadwal(){
	var tanggal=$("#tanggal").val();
	var idpoli=$("#idpoli5").val();
	present=$("#present5").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}tbooking_rehab/hasil_generate_jadwal/',
		method: "POST",
		dataType: "JSON",
		data: {
			tanggal:tanggal,
			present:present,
			idpoli:idpoli,
			st_plus_minus:st_plus_minus,
		},
		success: function(data) {
			$("#hasil").html(data.tabel);
			$("#nama_periode").html(data.nama_periode);
			$("#cover-spin").hide();
		}
	});
}
function pillih_calendar(idpoli,tanggal){
	$('[href="#tab_3"]').tab('show');
	  $("#tanggaldari").val(tanggal);
	  $("#tanggaldari").val(tanggal);
	  // alert(tanggal);
	$("#present").val(3).trigger('change');
	  hasil_generate_ls();
}
function add_kuota($idpoli,$kodehari,$jam_id,$tanggal){
	// alert($jam_id);
	$("#tmp_tanggal").val($tanggal);
	$("#tmp_idpoli").val($idpoli);
	$("#tmp_jam_id").val($jam_id);
	$("#modal_edit").modal('show');
	$('[href="#btabs-animated-slideleft-kuota"]').tab('show');
	get_kuota($idpoli,$kodehari,$jam_id);
	
}
function get_kuota($idpoli,$kodehari,$jam_id){
	$tanggal=$("#tmp_tanggal").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/get_kuota_detail/',
		method: "POST",
		dataType: "JSON",
		data: {
			idpoli:$idpoli,
			tanggal:$tanggal,
			jam_id:$jam_id
		},
		success: function(data) {
			$("#jam").val(data.jam);
			$("#namahari").val(data.namahari);
			$("#catatan").val(data.catatan);
			$("#kuota").val(data.kuota).trigger('change');
			$("#st_reservasi_online").val(data.st_reservasi_online).trigger('change');
			$("#cover-spin").hide();
			load_catatan();
		}
	});
}
$("#btn_save_kuota").click(function() {
	$idpoli=$("#tmp_idpoli").val();
	$tanggal=$("#tmp_tanggal").val();
	// alert($tanggal);
	$jam_id=$("#tmp_jam_id").val();
	$kuota=$("#kuota").val();
	$st_reservasi_online=$("#st_reservasi_online").val();
	// alert($idpoli+' '+$kodehari+' '+$jam_id);
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/save_kuota_detail/',
		method: "POST",
		dataType: "JSON",
		data: {
			idpoli:$idpoli,
			tanggal:$tanggal,
			jam_id:$jam_id,
			kuota:$kuota,
			st_reservasi_online:$st_reservasi_online,
		},
		success: function(data) {
			$("#modal_edit").modal('hide');
			hasil_generate_ls();
			$("#cover-spin").hide();
		}
	});
});
$("#btn_save_catatan").click(function() {
	$idpoli=$("#tmp_idpoli").val();
	$tanggal=$("#tmp_tanggal").val();
	$jam_id=$("#tmp_jam_id").val();
	$catatan=$("#catatan").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/save_catatan_detail/',
		method: "POST",
		dataType: "JSON",
		data: {
			idpoli:$idpoli,
			tanggal:$tanggal,
			jam_id:$jam_id,
			catatan:$catatan
		},
		success: function(data) {
			$("#modal_edit").modal('hide');
			hasil_generate_ls();
			$("#cover-spin").hide();
		}
	});
});
function stop_kuota($idpoli,$kodehari,$jam_id,$tanggal){
	// alert($kodehari);
	// $tanggal=$("#tanggal").val();
	// $tgl=$("#nama_periode").html();
	$nama_poli=$("#idpoli3 option:selected").text();
	// alert($nama_poli);
	// return false;
	swal({
			title: "Akan Menghentikan Reservasi "+$nama_poli+ " " + $tanggal + "?",
			text : "Stelah menekan Ya maka slot yang tersedia pada tanggal tersebut tidak dapat dipilih diseluruh platform!",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}app_setting/stop_kuota/',
				method: "POST",
				dataType: "JSON",
				data: {
					idpoli:$idpoli,
					tanggal:$tanggal,
					jam_id:$jam_id
				},
				success: function(data) {
					hasil_generate_ls();
					$("#cover-spin").hide();
					
				}
			});
		});

}
function load_catatan(){
	$idpoli=$("#tmp_idpoli").val();
	$tanggal=$("#tmp_tanggal").val();
	$jam_id=$("#tmp_jam_id").val();
	$('#index_Catatan').DataTable().destroy();	
	table = $('#index_Catatan').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{ "width": "5%", "targets": 0,  className: "text-right" },
					{ "width": "60%", "targets": [1],  className: "text-left" },
					{ "width": "35%", "targets": [2],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}tbooking_rehab/load_catatan', 
                type: "POST" ,
                dataType: 'json',
				data : {
						idpoli:$idpoli,
						tanggal:$tanggal,
						jam_id:$jam_id,
					   }
            }
        });
}
function hapus_poli(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Reservasi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}tbooking/hapus_poli',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_proses').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Publish'});
					
				}
			});
	});
}
function call_antrian_manual(antrian_id,tujuan){
	// alert(tujuan);
	let ruangan_id=tujuan;
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}Tpoliklinik_rm_rj/call_antrian',
		type: 'POST',
		dataType: "json",
		data: {
			ruangan_id: ruangan_id,
			antrian_id: antrian_id,
		
		},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==false){
				swal({
					title: "Maaf!",
					text: "No Antrian Tidak tersedia",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				load_index_all_patien();
			}
		}
	});
}
function call_lewati(antrian_id,antrian_id_next,tujuan){
	let ruangan_id=tujuan;
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}tpendaftaran_poli_tindakan/call_lewati',
		type: 'POST',
		dataType: "json",
		data: {
			ruangan_id: ruangan_id,
			antrian_id: antrian_id,
			antrian_id_next: antrian_id_next,
		
		},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==false){
				swal({
					title: "Maaf!",
					text: "No Antrian Tidak tersedia",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				load_index_all();
			}
		}
	});
}
function recall_manual(antrian_id){
	let ruangan_id=$("#ruangan_id").val();
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}tpendaftaran_poli_tindakan/call_antrian',
		type: 'POST',
		dataType: "json",
		data: {
			ruangan_id: ruangan_id,
			antrian_id: antrian_id,
		
		},
		success: function(data) {
			$("#cover-spin").hide();
			if (data==false){
				swal({
					title: "Maaf!",
					text: "No Antrian Tidak tersedia",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
			}else{
				load_index_all();
			}
		}
	});
}
function refres_div_counter(){
	let idpoli=$("#idpoli").val();
	let iddokter=$("#iddokter").val();
	let tanggal=$("#tanggal_antrian").val();
	let show_all=$("#show_all").val();
	// $("#cover-spin").show();
	// $(".div_counter").html('');
	$.ajax({
		url: '{site_url}Tpoliklinik_rm_rj/refres_div_counter',
		type: 'POST',
		dataType: "json",
		data: {
			idpoli:idpoli,			
			iddokter:iddokter,			
			tanggal:tanggal,			
			show_all:show_all,			
		},
		success: function(data) {
			$(".div_counter").html(data);
			$("#cover-spin").hide();
		}
	});
}
function startWorker() {
	if(typeof(Worker) !== "undefined") {
		if(typeof(w) == "undefined") {
			w = new Worker("{site_url}assets/js/worker.js");
	
		}
		w.postMessage({'cmd': 'start' ,'msg': 5000});
		w.onmessage = function(event) {
			refres_div_counter();
			$('#index_all').DataTable().ajax.reload( null, false );
			// $('#index_all').DataTable().ajax.reload( null, false );
			// load_index_all();
		};
		w.onerror = function(event) {
			window.location.reload();
		};
	} else {
		alert("Sorry, your browser does not support Autosave Mode");
		$("#autosave_status").html('<button class="btn btn-warning"><i class="fa fa-warning"></i> Autosave not running!</button>');
	}
 }
</script>