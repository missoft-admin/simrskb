<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}antrian_display" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
			<div class="block-content">
					<div class="row">
					<?php echo form_open_multipart('antrian_display/save_general', 'class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
						<div class="form-group">
							<label class="col-md-2 control-label" for="nama_display">Nama Display<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="nama_display" placeholder="Nama Display" name="nama_display" value="{nama_display}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="example-hf-email"></label>
							<div class="col-md-7">

								<img class="img-avatar"  id="output_img" src="{upload_path}antrian/{header_logo}" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="example-hf-email">Logo Header (100x100)</label>
							<div class="col-md-7">
								<div class="box">
									<input type="file" id="file-3" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;" name="header_logo" value="{header_logo}" />
									<label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-2 control-label" for="bg_color">BG Color<span style="color:red;">*</span></label>
							<div class="col-md-2">
								<div class="js-colorpicker input-group colorpicker-element">
									<input class="form-control" type="text" id="bg_color" name="bg_color" value="{bg_color}">
									<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="judul_header">Judul Header<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="judul_header" placeholder="Judul Header" name="judul_header" value="{judul_header}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="judul_sub_header">Judul Sub Header<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="judul_sub_header" placeholder="Judul Sub Header" name="judul_sub_header" value="{judul_sub_header}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="alamat">Alamat Rumah Sakit<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="alamat" placeholder="Alamat Rumah Sakit" name="alamat" value="{alamat}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="telepone">Nomor Telphone Rumah Sakit<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="telepone" placeholder="Nomor Telphone Rumah Sakit" name="telepone" value="{telepone}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="website">Website Rumah Sakit<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="website" placeholder="Website Rumah Sakit" name="website" value="{website}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="email">Email Rumah Sakit<span style="color:red;">*</span></label>
							<div class="col-md-10">
								<input tabindex="0" type="text" class="form-control" id="email" placeholder="Email Rumah Sakit" name="email" value="{email}" required>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-12 col-md-offset-2">
								<button class="btn btn-success" type="submit">Simpan</button>
								<a href="{base_url}antrian_display/index/1" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
							</div>
						</div>
						<input type="hidden" class="form-control" id="id" name="id" value="{id}">
						<?php echo form_close() ?>
					</div>
			</div>
			<?if ($id){?>
			<?php if (UserAccesForm($user_acces_form,array('1549'))){ ?>
			<div class="row push-20-t">
				<div class="form-group">
					<label class="col-md-12 control-label text-danger" for="login_judul"><i class="fa fa-tv text-danger"></i> COUNTER YANG DITAMPILKAN</label>
					
				</div>
			</div>
			<div class="row push-20-t">
				<div class="form-group">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_counter">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Urutan</th>
										<th width="75%">Counter</th>
										<th width="10%">Action</th>										   
									</tr>
									<tr>
										<th>#</th>
										<th>
											<input tabindex="0" type="text" class="form-control number" id="nourut_counter" placeholder="No Urut" name="nourut_counter" required>
										</th>
										<th>
											<select id="counter_id"  name="counter_id" class="js-select2 form-control" style="width: 100%;">
												<option value="#" selected>-Pilih Counter-</option>
												<?foreach($list_counter as $row){?>
												  <option value="<?=$row->id?>"><?=$row->nama_counter?></option>
												<?}?>
											</select>
										
										</th>
										
										<th>
											<div class="btn-group">
												<button class="btn btn-primary btn-sm" title="Simpan" type="button" id="btn_tambah_counter"><i class="fa fa-save"></i></button>
												<button class="btn btn-warning btn-sm"  title="Clear" type="button" id="btn_refresh_counter"><i class="fa fa-refresh"></i></button>
											</div>
										</th>			
										<input type="text" id="idcounter" value="" hidden>
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<?}?>
			<?}?>
			<?if ($id){?>
			<div class="row push-20-t">
				<div class="form-group">
					<label class="col-md-12 control-label text-danger" for="login_judul"><i class="fa fa-text-width text-danger"></i> RUNNING TEXT</label>
					
				</div>
			</div>
			<div class="row push-20-t">
				<div class="form-group">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_running">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Urutan</th>
										<th width="75%">Running Text</th>
										<th width="10%">Action</th>										   
									</tr>
									<input id="idrunning" type="hidden" value="">
									<?php if (UserAccesForm($user_acces_form,array('1541'))){ ?>
									<tr>
										<th>#</th>
										<th>
											<input tabindex="0" type="text" class="form-control number" id="nourut" placeholder="No Urut" name="nourut" required>
										</th>
										<th>
											<textarea style="width:100%" class="form-control" id="isi" name="isi" rows="2" placeholder="Content.."></textarea>
										
										</th>
										
										<th>
											<div class="btn-group">
												<button class="btn btn-primary btn-sm" title="Simpan" type="button" id="btn_tambah_running_text"><i class="fa fa-save"></i></button>
												<button class="btn btn-warning btn-sm"  title="Clear" type="button" id="btn_refresh_running_text"><i class="fa fa-refresh"></i></button>
											</div>
										</th>										   
									</tr>
											<?}?>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<?}?>
			<?if ($id){?>
				<?php if (UserAccesForm($user_acces_form,array('1542'))){ ?>
				<div class="row push-20-t" hidden>
					<div class="form-group">
						<div class="col-md-12">
							<label class="control-label text-danger" for="login_judul"><i class="fa fa-play text-danger"></i> SOUND PLAY</label>
							<button class="btn btn-primary btn-xs" onclick="show_modal()"  title="Add Sound" type="button" id="btn_modal_sound"><i class="fa fa-plus"></i> Tambah Sound</button>
						</div>
					</div>
				</div>
				<div class="row push-20-t" hidden>
					<div class="form-group">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_running">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="10%">Urutan</th>
											<th width="75%">SOUND FILE</th>
											<th width="10%">Action</th>										   
										</tr>
										
										
									</thead>
									<tbody>
										<?
											$no=1;
										foreach($list_sound as $r){
											
											?>
											<tr>
												<td><?=$no;?></td>
												<td><?=$r->nourut;?></td>
												<td><?=$r->file_sound;?></td>
												<td><a href="{base_url}antrian_display/hapus_sound/<?=$r->id?>/<?=$id?>" type="button" title="Hapus" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i></a></td>
											</tr>
										<?
											$no=$no+1;
										}?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<?}?>
			<?}?>
			<?if ($id){?>
				<?php if (UserAccesForm($user_acces_form,array('1542'))){ ?>
				<div class="row push-20-t">
					<div class="form-group">
						<div class="col-md-12">
							<label class="control-label text-danger" for="login_judul"><i class="fa fa-file-video-o text-danger"></i> LIST VIDEO</label>
						</div>
					</div>
				</div>
				<div class="row push-20-t">
					<div class="form-group">
						<div class="col-md-12">
							<form action="{base_url}antrian_display/upload_video" enctype="multipart/form-data" class="dropzone" id="image-upload">
								<input type="hidden" class="form-control" name="display_id" id="display_id_video" value="{id}" readonly>
								<div>
								  <h5>Video Upload</h5>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="row push-20-t">
					<div class="form-group">
						<div class="col-md-12">
							<div class="table-responsive">
								 <table class="table table-bordered" id="list_file">
									<thead>
										<tr>
											<th width="50%" class="text-center">File</th>
											<th width="15%" class="text-center">Size</th>
											<th width="35%" class="text-center">User</th>
											<th width="35%" class="text-center">X</th>
											
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<?}?>
			<?}?>
	</div>
			
	</div>
</div>
 <div class="modal" id="modal_sound" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<?php echo form_open_multipart('antrian_display/save_sound', 'class="form-horizontal push-10-t" onsubmit="return validate_sound()"') ?>
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Add Sound</h3>
				</div>
				<div class="block-content">
					<div class="form-group">
						<label class="col-md-12" for="nourut">Urutan<span style="color:red;">*</span></label>
						<div class="col-md-12">
							<input tabindex="0" type="text" class="form-control number" id="nourut_sound" placeholder="No Urut" name="nourut" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="email">File<span style="color:red;">*</span></label>
						<div class="col-md-12">
							<input type="file" id="file-sound" accept=".mp3,.wav" class="inputfile" style="display:none;" name="file_sound" value="{file_sound}" />
							<label for="file-sound" class="text-danger"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a sound&hellip;</span></label>
							<input id="idsound" type="hidden" value="">			
						</div>
					</div>
					<input type="hidden" class="form-control" name="display_id" value="{id}">	
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check"></i> Simpan</button>
			</div>
			<?php echo form_close() ?>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
var table;
var myDropzone 
function loadFile_img(event){
	// alert('sini');
	var output_img = document.getElementById('output_img');
	output_img.src = URL.createObjectURL(event.target.files[0]);
}

$(document).ready(function(){	
	$(".number").number(true,0,'.',',');
	load_running();
	load_counter();
	refresh_video();
	Dropzone.autoDiscover = false;
	myDropzone = new Dropzone(".dropzone", { 
	   autoProcessQueue: true,
	   maxFilesize: 30,
	});
	myDropzone.on("complete", function(file) {
	  
	  refresh_video();
	  myDropzone.removeFile(file);
	  
	});
})	
function refresh_video(){
	var id=$("#display_id_video").val();
	$('#list_file tbody').empty();
	// alert(id);
	$.ajax({
		url: '{site_url}antrian_display/refresh_video/'+id,
		dataType: "json",
		success: function(data) {
			if (data.detail!=''){
				$('#list_file tbody').empty();
				$("#box_file2").attr("hidden",false);
				$("#list_file tbody").append(data.detail);
			}
			// console.log();
			
		}
	});
}
function show_modal(){
	$("#modal_sound").modal('show');
}
function validate_final(){
	
	$("#cover-spin").show();
}
function validate_sound(){
	if ($("#nourut_sound").val()==''){
		sweetAlert("Maaf...", "Tentukan No Urut", "error");
		return false;
	}
	if (!$('#file-sound').val()) {
		sweetAlert("Maaf...", "Tentukan File", "error");
		return false;
	}
	$("#cover-spin").show();
}
function edit_running($id){
	let id=$id;
	$.ajax({
		url: '{site_url}antrian_display/edit_running', 
		dataType: "JSON",
		method: "POST",
		data : {id:id},
		success: function(data) {
			$("#nourut").val(data.nourut);
			$("#isi").val(data.isi);
			$("#idrunning").val(data.id);
		}
	});

}
$("#btn_tambah_running_text").click(function() {
	let display_id=$("#id").val();
	let idrunning=$("#idrunning").val();
	let nourut=$("#nourut").val();
	let isi=$("#isi").val();
	if ($("#nourut").val()==''){
		sweetAlert("Maaf...", "Tentukan No Urut", "error");
		return false;
	}
	if ($("#isi").val()==''){
		sweetAlert("Maaf...", "Tentukan Isi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}antrian_display/simpan_running', 
		dataType: "JSON",
		method: "POST",
		data : {
			display_id:display_id,
			idrunning:idrunning,
			nourut:nourut,
			isi:isi,
			},
		complete: function(data) {
			// $("#idtipe").val('#').trigger('change');
			$('#index_running').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			clear_running();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
$("#btn_refresh_running_text").click(function() {
	clear_running();
});
function clear_running(){
	$("#idrunning").val('');
	$("#nourut").val('');
	$("#isi").val('');
}
function load_running(){
	var display_id=$("#id").val();
	
	$('#index_running').DataTable().destroy();	
	table = $('#index_running').DataTable({
		autoWidth: false,
		searching: false,
		serverSide: true,
		"processing": true,
		"order": [],
		"pageLength": 10,
		"ordering": false,
		
		ajax: { 
			url: '{site_url}antrian_display/load_running_text', 
			type: "POST" ,
			dataType: 'json',
			data : {
					display_id:display_id
				   }
		}
	});
}
function hapus_running($id){
 var id=$id;
swal({
		title: "Anda Yakin ?",
		text : "Untuk Hapus Running Text?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}antrian_display/hapus_running',
			type: 'POST',
			data: {id: id},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
				$('#index_running').DataTable().ajax.reload( null, false ); 
				$("#cover-spin").hide();
				// filter_form();
			}
		});
	});
}
$(document).on("click",".hapus_file",function(){	
		var currentRow=$(this).closest("tr");          
         var id=currentRow.find("td:eq(4)").text();
		 $.ajax({
			url: '{site_url}antrian_display/hapus_file',
			type: 'POST',
			data: {id: id},
			success : function(data) {				
				$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
				currentRow.remove();
				// table.ajax.reload( null, false );				
			}
		});
	});
function edit_counter($id){
	let id=$id;
	$.ajax({
		url: '{site_url}antrian_display/edit_counter', 
		dataType: "JSON",
		method: "POST",
		data : {id:id},
		success: function(data) {
			$("#nourut_counter").val(data.nourut);
			$("#counter_id").val(data.counter_id).trigger('change');
			$("#idcounter").val(data.id);
		}
	});

}
$("#btn_tambah_counter").click(function() {
	let display_id=$("#id").val();
	let idcounter=$("#idcounter").val();
	// alert(idcounter);
	let nourut_counter=$("#nourut_counter").val();
	let counter_id=$("#counter_id").val();
	if ($("#nourut_counter").val()==''){
		sweetAlert("Maaf...", "Tentukan No Urut", "error");
		return false;
	}
	if ($("#counter_id").val()=='#'){
		sweetAlert("Maaf...", "Tentukan Counter", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}antrian_display/simpan_counter', 
		dataType: "JSON",
		method: "POST",
		data : {
			display_id:display_id,
			idcounter:idcounter,
			nourut_counter:nourut_counter,
			counter_id:counter_id,
			},
		complete: function(data) {
			// $("#idtipe").val('#').trigger('change');
			$('#index_counter').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			clear_counter();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
$("#btn_refresh_counter").click(function() {
	clear_counter();
});
function clear_counter(){
	$("#idcounter").val('');
	$("#nourut_counter").val('');
	$("#counter_id").val('#').trigger('change');
}
function load_counter(){
	var display_id=$("#id").val();
	
	$('#index_counter').DataTable().destroy();	
	table = $('#index_counter').DataTable({
		autoWidth: false,
		searching: false,
		serverSide: true,
		"processing": true,
		"order": [],
		"pageLength": 10,
		"ordering": false,
		
		ajax: { 
			url: '{site_url}antrian_display/load_counter', 
			type: "POST" ,
			dataType: 'json',
			data : {
					display_id:display_id
				   }
		}
	});
}
function hapus_counter($id){
 var id=$id;
swal({
		title: "Anda Yakin ?",
		text : "Untuk Hapus Counter?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}antrian_display/hapus_counter',
			type: 'POST',
			data: {id: id},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
				$('#index_counter').DataTable().ajax.reload( null, false ); 
				$("#cover-spin").hide();
				// filter_form();
			}
		});
	});
}
</script>