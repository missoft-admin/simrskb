<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mtarif_harga_kamar" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mtarif_harga_kamar/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="truang_perawatan">Tarif Ruang (Perawatan)</label>
				<div class="col-md-7">
					<select name="truang_perawatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_ruangperawatan',array('idtipe' => '1', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($truang_perawatan == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="truang_hcu">Tarif Ruang (HCU)</label>
				<div class="col-md-7">
					<select name="truang_hcu" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_ruangperawatan',array('idtipe' => '2', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($truang_hcu == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="truang_icu">Tarif Ruang (ICU)</label>
				<div class="col-md-7">
					<select name="truang_icu" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_ruangperawatan',array('idtipe' => '3', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($truang_icu == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="truang_isolasi">Tarif Ruang (Isolasi)</label>
				<div class="col-md-7">
					<select name="truang_isolasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach (get_all('mtarif_ruangperawatan',array('idtipe' => '4', 'status' => '1')) as $row){ ?>
							<option value="<?=$row->id;?>" <?=($truang_isolasi == $row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mtarif_harga_kamar" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>
