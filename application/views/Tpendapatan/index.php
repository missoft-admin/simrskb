<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo error_message($error)?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
		<ul class="block-options">
        <li>
            <a href="{base_url}tpendapatan/create" class="btn"><i class="fa fa-plus"></i></a>
        </li>
    </ul>
	</div>
	<div class="block-content">

		<hr style="margin-top:0px">

		<div class="row">
			<?php echo form_open('tpendapatan/filter','class="form-horizontal" id="form-work"') ?>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="iddokter">Pendapatan</label>
					<div class="col-md-8">
						<select id="idpendapatan" name="idpendapatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
							<option value="#" selected>Semua Pendapatan</option>
							<?php foreach  ($list_biaya as $row) { ?>
								<option value="<?=$row->id?>" <?=($idpendapatan == $row->id ? 'selected' : '')?>><?=$row->keterangan?></option>
							<?php } ?>
						</select>
					</div>
				</div>

				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="idrekanan">User Input</label>
					<div class="col-md-8">
						<select id="iduser" name="iduser" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
							<option value="#" selected>Semua User</option>
							<?php foreach ($list_user as $row){?>
								<option value="<?= $row->id?>" <?=($iduser == $row->id ? 'selected':'')?>><?= $row->name?></option>
							<?php }?>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="tanggaldaftar">Tanggal</label>
					<div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                <input class="form-control" type="text" id="tanggaldari" name="tanggaldari" placeholder="From" value="{tanggaldari}">
                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                <input class="form-control" type="text" id="tanggalsampai" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
            </div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="button" id="btn_filter" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>

		<hr style="margin-top:10px">

		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_index">
			<thead>
				<tr>
					<th>No.</th>
					<th>No Transaksi</th>
					<th>Tanggal</th>
					<th>Pendapatan</th>
					<th>Keterangan</th>
					<th>Terima Dari</th>
					<th>Nominal</th>
					<th>Action</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	// alert('sini');
	load_index();
	
})	
function load_index(){
	var idpendapatan=$("#idpendapatan").val();
	var tanggaldari=$("#tanggaldari").val();
	var tanggalsampai=$("#tanggalsampai").val();
	var iduser=$("#iduser").val();
	
	// alert(tanggal_dibutuhkan1);
	$('#table_index').DataTable().destroy();
	$('#table_index').DataTable({
			"autoWidth": false,
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}tpendapatan/getIndex',
				type: "POST",
				dataType: 'json',
				data : {
					idpendapatan:idpendapatan,
					tanggaldari:tanggaldari,
					tanggalsampai:tanggalsampai,
					iduser:iduser,
					
					
				   }
			},
			"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": true },
					{ "width": "10%", "targets": 1, "orderable": true },
					{ "width": "10%", "targets": 2, "orderable": true },
					{ "width": "10%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 4, "orderable": true },
					{ "width": "10%", "targets": 5, "orderable": true },
					{ "width": "10%", "targets": 6, "orderable": true },
					{ "width": "10%", "targets": 7, "orderable": true },
					{ "width": "10%", "targets": 8, "visible": false }
				]
		});
}
	$(document).on("click","#btn_filter",function(){
		load_index();
	});
	$(document).on("click",".verif",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,8).data()
		// alert(id); return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Verifikasi Pendapatan Lain?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}tpendapatan/verif/'+id,
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Pendapatan diselesaikan diverifikasi'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	$(document).on("click",".hapus",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,8).data()
		// alert(id); return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Hapus Pendapatan?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}tpendapatan/hapus/'+id,
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Pendapatan diselesaikan diverifikasi'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});

</script>
