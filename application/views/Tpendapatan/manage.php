<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}tpendapatan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?php echo form_open('tpendapatan/save','class="form-horizontal" id="form-work" onsubmit="return validate_final()"') ?>
		<div class="form-group">
			<label class="col-md-3 control-label" for="example">Tanggal</label>
			<div class="col-md-7">
				<input class="js-datepicker form-control" <?=$disabel?>  type="text" id="tanggal" name="tanggal" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" value="<?=DMYFormat($tanggal);?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="example">Tipe Pendapatan</label>
			<div class="col-md-7">
				<div class="input-group">	
					<select class="js-select2 form-control" <?=$disabel?> id="idpendapatan" name="idpendapatan" style="width: 100%;" data-placeholder="Biaya">
						<option value="#">Select an Option</option>
						<?php foreach($list_biaya as $row){ ?>
							<option value="<? echo $row->id ?>" <?=($idpendapatan == $row->id ? 'selected':'')?>><?php echo $row->keterangan; ?></option>
						<?php } ?>
					</select>
					<span class="input-group-btn">
						<button class="btn btn-success"  type="button" id="btn_refresh_tipe" title="Refresh"><i class="fa fa-refresh"></i></button>
						<a href="{base_url}mpendapatan/create" target="_blank" class="btn btn-primary"  type="button" id="btn_add_dari" title="Tambah Tipe"><i class="fa fa-plus"></i></a>
					</span>
				</div>
			</div>
		</div>
		<div class="form-group" hidden>
			<label class="col-md-3 control-label" for="noakun">No. Akun</label>
			<div class="col-md-7">
				<input type="text" class="form-control" id="noakun" name="noakun" readonly value="{noakun}" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="keterangan">Deskripsi</label>
			<div class="col-md-7">
				<textarea class="form-control js-summernote"  <?=$disabel?> name="keterangan" id="keterangan"><?=$keterangan?></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="terimadari">Terima Dari</label>
			<div class="col-md-7">
				<div class="input-group">
					<select id="terimadari" name="terimadari"  <?=$disabel?>   class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" <?=($terimadari=='#'?'selected':'')?>>- Pilih Sumber Pendapatan -</option>
						<? foreach($list_dari as $row){ ?>
							<option value="<?=$row->id?>" <?=($terimadari==$row->id?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
					<span class="input-group-btn">
						<button class="btn btn-success"  type="button" id="btn_refresh_dari" title="Refresh"><i class="fa fa-refresh"></i></button>
						<a href="{base_url}mdari" target="_blank" class="btn btn-primary"  type="button" id="btn_add_dari" title="Tambah Sumber Pendapatan"><i class="fa fa-plus"></i></a>
					</span>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="nominal">Nominal</label>
			<div class="col-md-5">
				<input type="text" class="form-control number" <?=$disabel?>  id="nominal" name="nominal" value="{nominal}" />
			</div>
			<div class="col-md-2">
				<button  type="button" class="btn btn-danger"  <?=$disabel?>  id="btn_add_pembayaran" title="Edit"><i class="fa fa-plus"></i> Pembayaran</button>
			</div>
		</div>

		<hr class="m-b-xs m-t-xs">

		<div class="row">
			<div class="col-sm-12">
				
			</div>
		</div>
		
		<? $this->load->view('Tpendapatan/modal_bayar'); ?>
		
	
	</div>
	<footer class="panel-footer text-right bg-light lter">
		<? if ($disabel==''){?>
			<button class="btn btn-success" type="submit" id="btn_simpan" name="btn_simpan" value="2">Simpan & Verifikasi</button>
			<button class="btn btn-primary" type="submit" id="btn_simpan" name="btn_simpan" value="1">Simpan</button>
		<?}?>
		<a href="{base_url}tpendapatan" class="btn btn-default" type="button">Batalkan</a>
	</footer>
	<input type="hidden" name="idtransaksi" value="{id}"/>
	<?php echo form_close() ?>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#keterangan,#ket_pembayaran').summernote({
		  height: 100,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }
		});
		$(".decimal").number(true,2,'.',',');
		$('.number').number(true, 0, '.', ',');
		// refresh_tipe();
		// print_r('Dinisi');exit();
	})	
	$(document).on("click", "#btn_refresh_dari", function() {
		refresh_dari();
	});
	//PEMBAYARAN
	$(document).on("change","#jenis_kas_id",function(){
		// alert('soo');
		var jenis_kas_id=$("#jenis_kas_id").val();
		var sumber_kas_id=$("#sumber_kas_id_tmp").val();
		$.ajax({
			url: '{site_url}mrka_bendahara/list_sumber_kas',
			type: 'POST',
			dataType: "json",
			data: {jenis_kas_id: jenis_kas_id,sumber_kas_id: sumber_kas_id},
			success : function(data) {				
				$("#sumber_kas_id").empty();	
				console.log(data.detail);
				// $("#sumber_kas_id").append(data.detail);	
				$('#sumber_kas_id').append(data.detail);
			}
		});
	});
	function clear_input_pembayaran(){
		$("#jenis_kas_id").val("#").trigger('change');
		$("#sumber_kas_id").val("#").trigger('change');
		$("#nominal_bayar").val(0);
		$("#sisa_modal").val(0);
		$("#idpembayaran").val('');
		$("#idmetode").val("#").trigger('change');
		$("#ket_pembayaran").summernote('code','');
		$("#iddet").val('');
	}
	function add_pembayaran(){
			
		if ($("#jenis_kas_id").val() == "#") {
			sweetAlert("Maaf...", "Jenis Kas Harus Diisi!", "error");
			$("#jenis_kas_id").focus();
			return false;
		}
		if ($("#sumber_kas_id").val() == "#") {
			sweetAlert("Maaf...", "Sumber Kas Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		if ($("#idmetode").val() == "#" || $("#idmetode").val() == "") {
			sweetAlert("Maaf...", "Metode  Harus Diisi!", "error");
			$("#idmetode").focus();
			return false;
		}
		if ($("#nominal_bayar").val() == "" || $("#nominal_bayar").val() == "0") {
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		var content = "";

		var row_index;
		var duplicate = false;

		if($("#rowindex").val() != ''){
			var number = $("#number").val();
			var content = "";
			row_index = $("#rowindex").val();
		}else{
			var number = $('#list_pembayaran tr').length;
			var content = "<tr>";
			// alert($("#idmetode option:selected").text());
			$('#list_pembayaran tbody tr').filter(function (){
				var $cells = $(this).children('td');
				console.log($cells.eq(1).text());
				if($cells.eq(1).text() === $("#idmetode option:selected").text()){
					sweetAlert("Maaf...", $("#pembayaran_jenis option:selected").text() + " sudah ditambahkan.", "error");
					duplicate = true;

					pembayaran_clear();
					return false;
				}
			});
		}

		if(duplicate == false){
			var ket=$('#ket_pembayaran').summernote('code');
			content += "<td style='display:none'>" + number + "</td>";
			content += "<td>" + $("#jenis_kas_id option:selected").text() + "</td>";
			content += "<td>" + $("#sumber_kas_id option:selected").text() + "</td>";
			content += "<td>" + $("#idmetode option:selected").text() + "</td>";
			content += "<td style='display:none'>-</td>";
			content += "<td>" + ket + "</td>";
			content += "<td align='right'>" + formatNumber($("#nominal_bayar").val()) + "</td>";
			content += "<td><button type='button' class='btn btn-info btn-xs pembayaran_edit' data-toggle='modal' data-target='#modal-pembayaran' title='Edit'><i class='fa fa-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-danger btn-xs pembayaran_remove' title='Remove'><i class='fa fa-trash-o'></i></button></td>";
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xiddet[]' value='"+$("#iddet").val()+"'></td>";//8
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xjenis_kas_id[]' value='"+$("#jenis_kas_id").val()+"'></td>";//9
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xsumber_kas_id[]' value='"+$("#sumber_kas_id").val()+"'></td>";//10
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xidmetode[]' value='"+$("#idmetode").val()+"'></td>";//11
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='ket[]' value='"+ket+"'></td>";//12
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xnominal_bayar[]' value='"+$("#nominal_bayar").val()+"'></td>";//13
			content += "<td style='display:none'><input readonly type='text' class='form-control' name='xstatus[]' value='1'></td>";//14
			if($("#rowindex").val() != ''){
				$('#list_pembayaran tbody tr:eq(' + row_index + ')').html(content);
			}else{
				content += "</tr>";
				$('#list_pembayaran tbody').append(content);
			}

			pembayaran_clear();
			checkTotal();
		}
		
		
	}
	$(document).on("click",".pembayaran_edit",function(){
		$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
		$("#modal_pembayaran").modal('show');
		$("#iddet").val($(this).closest('tr').find("td:eq(8) input").val());
		$("#sumber_kas_id_tmp").val($(this).closest('tr').find("td:eq(10) input").val());
		$("#jenis_kas_id").val($(this).closest('tr').find("td:eq(9) input").val()).trigger('change');
		$("#nominal_bayar").val($(this).closest('tr').find("td:eq(13) input").val());
		$("#ket_pembayaran").summernote('code',$(this).closest('tr').find("td:eq(12) input").val());
		$("#idmetode").val($(this).closest('tr').find("td:eq(11) input").val()).trigger('change');
		$("#sisa_modal").val(parseFloat($("#total_sisa").val()) + parseFloat($("#nominal_bayar").val()));
		// sisa_modal($();
		// $("#pembayaran_id").val($(this).closest('tr').find("td:eq(0)").val());

		
		return false;
	});
	$(document).on("click", ".pembayaran_remove", function() {
			if (confirm("Hapus data ?") == true) {
				if ($("#idtransaksi").val()==''){
					$(this).closest('td').parent().remove();
						
				}else{
					$(this).closest('tr').find("td:eq(14) input").val(0)
					// $(this).closest('tr').find("td:eq(6)").html('<span class="label label-danger">DELETED</span>')
					$(this).closest('tr').find("td:eq(7)").html('<span class="label label-danger">DELETED</span>')
				}
				checkTotal();
			}
		return false;
	});
	function formatNumber (num) {
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}

	$(document).on("click","#btn_add_pembayaran",function(){
		if ($("#nominal").val()=='' || $("#nominal").val()=='0'){
			return false;
		}
		$("#modal_pembayaran").modal('show');
		$("#total_sisa").val(parseFloat($("#nominal").val()) - parseFloat($("#total_bayar").val()));
		$("#sisa_modal").val($("#total_sisa").val());
		
	});
	
	$(document).on("click","#btn_add_bayar",function(){
		add_pembayaran();
	});
	///END Pembayaran
	
	checkTotal();

	// $("#nominal").autoNumeric('init', {decimalPlacesOverride: '0'});
	// $("#pembayaran_nominal").autoNumeric('init', {decimalPlacesOverride: '0'});

	// $("#idpendapatan").select2().on('change', function () {
		// $.ajax({
			// url: '{base_url}tpendapatan/get_noakun/' + $(this).val(),
			// dataType: 'json',
			// success: function(data) {
				// $("#noakun").val(data.kredit_noakun);
			// }
		// });
	// });
	$(document).on("click","#btn_refresh_tipe",function(){
		refresh_tipe();
	});
	
	

	
	function validate_final(){
		if ($("#idpendapatan").val() == "#") {
			sweetAlert("Maaf...", "Pendapatan tidak boleh kosong!", "error");
			return false;
		}
		if ($("#terimadari").val() == "#") {
			sweetAlert("Maaf...", "Sumber Pendapatan tidak boleh kosong!", "error");
			return false;
		}
		if ($("#nominal").val() == "" || $("#nominal").val() == "0") {
			sweetAlert("Maaf...", "Nominal tidak boleh kosong!", "error");
			return false;
		}
		if ($("#total_bayar").val() == "0") {
			sweetAlert("Maaf...", "Pembayaran Belum Dipilih!", "error");
			return false;
		}
		if ($("#total_sisa").val() != "0") {
			sweetAlert("Maaf...", "Masih Ada Sisa!", "error");
			return false;
		}
	}
	
	function pembayaran_clear(){
		$("#sumber_kas_id_tmp").val('');
		$("#number").val('');
		$("#rowindex").val('');
		$("#modal_pembayaran").modal('hide');
		clear_input_pembayaran();
	}
	

	function checkTotal(){
		var total_pembayaran = 0;
		$('#list_pembayaran tbody tr').each(function() {
			if ($(this).closest('tr').find("td:eq(14) input").val()=='1'){
			total_pembayaran += parseFloat($(this).find('td:eq(6)').text().replace(/\,/g, ""));
			}
		});
		$("#total_bayar").val(total_pembayaran);
		$("#total_sisa").val(parseFloat($("#nominal").val()) - parseFloat(total_pembayaran));
	}
	function refresh_dari(){
		$.ajax({
			url: '{site_url}tpendapatan/refresh_dari/',
			dataType: "json",
			success: function(data) {
				$("#terimadari").empty();
				$('#terimadari').append('<option value="#">- Pilih Sumber Pendapatan -</option>');
				$('#terimadari').append(data.detail);
			}
		});
	}
	function refresh_tipe(){
		$.ajax({
			url: '{site_url}tpendapatan/refresh_tipe/',
			dataType: "json",
			success: function(data) {
				$("#idpendapatan").empty();
				$('#idpendapatan').append('<option value="#">- Pilih Sumber Pendapatan -</option>');
				$('#idpendapatan').append(data.detail);
			}
		});
	}
</script>
