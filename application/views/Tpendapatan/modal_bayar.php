<div class="row">
	<div class="col-md-12">
		<!-- Static Labels block-opt-hidden -->
		<div class="block block-themed" >
			<div class="block-header bg-primary">
				<ul class="block-options">
					<li>
						<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
					</li>
				</ul>
				<h3 class="block-title"><i class="fa fa-money"></i>  PEMBAYARAN</h3>
			</div>
			<div class="block-content block-content">
					<input type="hidden" class="form-control" id="rowindex">
					<input type="hidden" class="form-control" id="iddet">
					<div class="form-group">
						 <table class="table table-bordered" id="list_pembayaran">
							<thead>
								<tr>
									<th width="5%" class="text-center" hidden>NO</th>
									<th width="10%" class="text-center">Jenis Kas</th>
									<th width="15%" class="text-center">Sumber Kas</th>
									<th width="10%" class="text-center">Metode</th>
									<th width="10%" class="text-center" hidden>Tgl Pencairan</th>
									<th width="15%" class="text-center">Keterangan</th>
									<th width="10%" class="text-center">Nominal</th>
									<th width="15%" class="text-center">Actions</th>
									
								</tr>
							</thead>
							</thead>
							<tbody>	
								<? 
									$no=1;
								foreach($list_pembayaran as $row){ ?>
									<tr>
									<td hidden><?=$no?></td>
									<td><?=$row->jenis_kas?></td>
									<td><?=$row->sumber_kas?></td>
									<td><?=metodePembayaran_bendahara($row->idmetode)?></td>
									<td hidden><?=$row->idmetode?></td>
									<td><?=$row->ket_pembayaran?></td>
									<td align="right"><?=number_format($row->nominal_bayar,0)?></td>
									<td><button type='button'  <?=$disabel?>  class='btn btn-info btn-xs pembayaran_edit' data-toggle='modal' data-target='#modal-pembayaran' title='Edit'><i class='fa fa-pencil'></i></button>&nbsp;&nbsp;<button type='button'  <?=$disabel?>  class='btn btn-danger btn-xs pembayaran_remove' title='Remove'><i class='fa fa-trash-o'></i></button></td>
									<td style='display:none'><input readonly type='text' class='form-control' name='xiddet[]' value='<?=$row->id?>'></td>
									<td style='display:none'><input readonly type='text' class='form-control' name='xjenis_kas_id[]' value='<?=$row->jenis_kas_id?>'></td>
									<td style='display:none'><input readonly type='text' class='form-control' name='xsumber_kas_id[]' value='<?=$row->sumber_kas_id?>'></td>
									<td style='display:none'><input readonly type='text' class='form-control' name='xidmetode[]' value='<?=$row->idmetode?>'></td>
									<td style='display:none'><input readonly type='text' class='form-control' name='ket[]' value='<?=$row->ket_pembayaran?>'></td>
									<td style='display:none'><input readonly type='text' class='form-control' name='xnominal_bayar[]' value='<?=$row->nominal_bayar?>'></td>
									<td style='display:none'><input readonly type='text' class='form-control' name='xstatus[]' value='1'></td>
									</tr>
								<?
								$no=$no+1;
								}?>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="4" class="text-right"> <strong>Total Bayar </strong></td>
									<td  colspan="2"> <input class="form-control input number" readonly type="text" id="total_bayar" name="total_bayar" value="0"/></td>
								</tr>
								<tr>
									<td colspan="4" class="text-right"> <strong>Sisa</strong></td>
									<td  colspan="2"><input class="form-control input number" readonly type="text" id="total_sisa" name="total_sisa" value="0"/> </td>
								</tr>
							</tfoot>
						</table>
					</div>
					
				</div> 
		</div>
		<!-- END Static Labels -->
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_pembayaran" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Pembayaran</h3>
				</div>
				<div class="block-content">
					
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Sisa Rp. </label>
							<div class="col-md-7">
								<input readonly type="text" class="form-control number" id="sisa_modal" placeholder="Sisa" name="sisa_modal" required="" aria-required="true">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="jenis_kas_id">Jenis Kas</label>
							<div class="col-md-7">
								<select name="jenis_kas_id" id="jenis_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="#">- Pilih Opsi -</option>
									<? foreach($list_jenis_kas as $row){ ?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>
									<?}?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="sumber_kas_id">Sumber Kas</label>
							<div class="col-md-7">
								<select name="sumber_kas_id" id="sumber_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">							
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="idmetode">Metode</label>
							<div class="col-md-7">
								<select name="idmetode" id="idmetode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="#">Pilih Opsi</option>							
									<option value="1">Cheq</option>	
									<option value="2">Tunai</option>	
									<option value="4">Transfer Antar Bank</option>
									<option value="3">Transfer Beda Bank</option>							
								</select>
							</div>
						</div>
						
						
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Nominal Rp. </label>
							<div class="col-md-7">
								<input  type="hidden" class="form-control" id="sumber_kas_id_tmp" placeholder="sumber_kas_id_tmp" name="sumber_kas_id_tmp" >
								<input  type="hidden" class="form-control" id="idpembayaran" placeholder="idpembayaran" name="idpembayaran" >
								<input  type="text" class="form-control number" id="nominal_bayar" placeholder="Nominal" name="nominal_bayar" >
							</div>
						</div>
						<div class="form-group" hidden>
							<label class="col-md-3 control-label" for="nama">Tanggal Pencairan</label>
							<div class="col-md-7">
								<div class="input-group date">
								<input class="js-datepicker form-control input"  type="text" id="tanggal_pencairan" name="tanggal_pencairan" value="" data-date-format="dd-mm-yyyy"/>
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Keterangan </label>
							<div class="col-md-7">
								<textarea class="form-control js-summernote" name="ket_pembayaran" id="ket_pembayaran"></textarea>
							</div>
						</div>

						<div class="modal-footer">
							<button class="btn btn-sm btn-primary" id="btn_add_bayar" type="button"><i class="fa fa-check"></i> Add</button>
							<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Exit</button>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">



</script>