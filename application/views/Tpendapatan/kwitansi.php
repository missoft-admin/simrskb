<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Kwitansi Pendapatan</title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 9px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
        font-size: 18px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
	  .header {
		  font-size: 20px !important;
        font-weight: bold;
        text-align: center;
      }
      .content td {
		  border: 0px solid #000 !important;
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <?php for ($i=0; $i < 3; $i++) { ?>
   
	<table class="content">
      <tr  height="200px">
        <td rowspan="4" width="20%" class="text-center"><img src="assets/upload/logo/logo.png" alt="" width="100" height="100"></td>
        <td width="60%" colspan="2" class="text-right header"><b>KWITANSI</b> &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; </td>
		<td width="10%"></td>
      </tr>
	  <tr>
			<td class="text-left"></td>
			<td colspan="2"></td>
	  </tr>
	  <tr>
			<td width="20%" class="text-left">TANGGAL</td>
			<td width="90%" colspan="2" >:  <?=HumanDateLong($created_at)?></td>
	  </tr>
	  <tr>
			<td class="text-left">NO TRANSAKSI</td>
			<td colspan="2">:  <?=$notransaksi;?></td>
	  </tr>
	  
	  
  </table>
  <br>
    <table class="content-2">
      
      <tr>
        <td class="text-italic text-bold" style="width:20%">Sudah terima dari</td>
        <td class="text-center" style="width:5%">:</td>
        <td class="text-italic"><?=strtoupper($dari_nama);?></td>
      </tr>
      <tr>
        <td class="text-italic text-bold">Uang Sebesar</td>
        <td class="text-center" style="width:5%">:</td>
        <td class="text-italic" style="background: #e2e2e2;"># <?=ucwords(terbilang($nominal));?> Rupiah #</td>
      </tr>
      <tr>
        <td class="text-italic text-bold">Untuk Pembayaran</td>
        <td class="text-center" style="width:5%">:</td>
        <td class="border-dotted"><?=$keterangan;?></td>
      </tr>
     
    </table>
	<br>
    <table class="content">
		
      <tr>
       <td style="width:25%" class="border-thick-top border-thick-bottom text-italic text-bold text-center">Rp.</td>
        <td class="border-thick-top border-thick-bottom text-italic text-bold text-right" style="background: #e2e2e2;opacity: 0.6;width:25%">Rp <?=number_format($nominal);?></td>
        <td style="width:10%" style="width:25%">&nbsp;</td>
        <td style="width:40%" class="text-italic text-center" style="width:25%">Bandung, <?=HumanDate($created_at)?></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td >&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
	  <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td >&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
	  <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td >&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
	  <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td >&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
	  <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td >&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
	  <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td >&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
	  
	  <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td >&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="text-italic  text-center">(<?=$this->session->userdata('user_name');?>)</td>
      </tr>
    </table>
    <br>
    
    <div class="border-dotted"></div>
    <br>
    <?php } ?>
  </body>
</html>
