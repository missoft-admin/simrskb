<?php echo ErrorSuccess($this->session)?> 
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>
<div id="cover-spin"></div>
<form action="{site_url}tgudang_pemesanan/save_edit_po/" method="post" class="form-horizontal" onsubmit="return validate_final()">
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">{title}</h3>
        </div>
        <div class="block-content">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered info_permintaan">
                        <tr>
                            <td>No.Transaksi</td>
                            <td><?=$nopemesanan ?><input type="hidden" id="idpemesanan" name="idpemesanan" value="<?=$id?>">
							<input type="hidden" id="xidunit" name="xidunit" value="<?=$xidunit?>">
							<input type="hidden" id="xidistributor" name="xidistributor" value="<?=$iddistributor?>">
							<input type="hidden" id="id" name="id" value="<?=$id?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Distributor</td>
                            <td><select class="form-control" disabled name="iddistributor" style="width:100%"></select>
                            </td>
                        </tr>
                        
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered info_permintaan">
                        <tr>
                            <td>Tanggal</td>
                            <td><?=$tanggal ?>
                            </td>
                        </tr>
                        <tr>
                            <td>User Edit</td>
                            <td><?=$this->session->userdata("user_name") ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            
            <table class="table table-bordered" id="datatable">
                <thead>
                    <tr>
                        <th width="5%" class="text-center">Tipe</th>
                        <th width="20%" class="text-center">Nama</th>
                        <th width="10%" class="text-center">SATUAN</th>
                        <th width="5%" class="text-center">Jumlah Alihkan</th>
                        <th width="10%" class="text-center">Satuan</th>
                        <th width="10%" class="text-center">Total</th>
                        <th width="10%" class="text-center">Status</th>
                        <th width="40%" class="text-center">No PO Baru</th>
                        <th hidden>idunitpeminta</th>
                        <th hidden>idunitpnerima</th>
                        <th hidden>idtipe</th>
                        <th hidden>idbarang</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($list_detail as $r): ?>
                    <tr>
                        <td class="text-center"><?php echo $r->namatipe ?></td>
                        <td><?php echo $r->namabarang ?></td>
                        <td class="text-center"><?php echo ($r->opsisatuan=='1'?'KECIL':'BESAR') ?></td>
                        <td class="text-center"><?php echo ($r->sisa) ?></td>
                        <td class="text-center"><?php echo ($r->opsisatuan=='1'?$r->satuan_kecil:$r->satuan_besar) ?></td>
                        <td class="text-right"><?php echo ($r->opsisatuan=='1'?number_format($r->hargabeli*$r->kuantitas_kecil,0):number_format($r->hargabeli_besar*$r->kuantitas_besar,0)) ?></td>
                        <td class="text-center"><?php echo ($r->sudah_terima=='0'?'<span class="label label-success">SEMUA</span>':'<span class="label label-warning">SEBAGIAN</span>') ?></td>
                        <td class="text-left">
							<select name="nopemesanan[]" class="js-select2 form-control new_nopesan" style="width: 100%;"></select>
						</td>
                        
						<td hidden><input type="text" name="e_id_det[]" value="<?=$r->id?>"></td><!-- 10-->
						<td hidden><input type="text" name="e_idpemesanan_awal[]" value="<?=$id?>"></td><!-- 11-->
						<td hidden><input type="text" name="e_idtipe[]" value="<?php echo $r->idtipe ?>"></td><!-- 11-->
						<td hidden><input type="text" name="e_idbarang[]" value="<?php echo $r->idbarang ?>"></td><!-- 11-->
						<td hidden><input type="text" name="e_namabarang[]" value="<?php echo $r->namabarang ?>"></td><!-- 11-->
						<td hidden><input type="text" name="e_idpemesana_new[]" value="#"></td><!-- 12-->
						<td hidden><input type="text" name="e_terima[]" value="<?=$r->sudah_terima?>"></td><!-- 12-->
						<td hidden><input type="text" name="e_sisa[]" value="<?=$r->sisa?>"></td><!-- 12-->
						<td hidden><input type="text" name="e_pesan[]" value="<?=$r->kuantitas?>"></td><!-- 12-->
						<td hidden><input type="text" name="e_opsisatuan[]" value="<?=$r->opsisatuan?>"></td><!-- 12-->
						<td hidden><input type="text" name="e_jumlahsatuanbesar[]" value="<?=$r->jumlahsatuanbesar?>"></td><!-- 23-->
						
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
            <div class="form-group text-right">
                <div class="col-md-12">
                    <a href="{site_url}tgudang_penerimaan/create2" class="btn btn-default" type="reset">Kembali</a>
                    <button class="btn btn-success" id="btn_simpan" type="submit">Submit</button>
                </div>
            </div>
			
			
			
        </div>
    </div>
</form>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
var table;
$(document).ready(function() {
	
	$(".number").number(true,0,'.',',');
	$('select[name=iddistributor]').select2({
		placeholder: 'Cari Data ... ',
		ajax: {
			url: '{site_url}tgudang_pemesanan/getdistributor',
			dataType: 'json',
			delay: 250,                
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return { id: item.id, text: item.text}
					})
				};
			},
			cache: false,
		}
	})
	 var newOption = new Option('{namadistributor}', '{iddistributor}', true, true);
     $('select[name=iddistributor]').append(newOption).trigger('change');
	 
	noPemesanan();
	get_validasi();
});
$(document).on("change",".new_nopesan ",function(){
	var new_id=$(this).val();
	
	var detail = $(this);	
	detail.closest('tr').find("td:eq(13) input").val(new_id);
	get_validasi();
});   
function get_validasi(){
	var st_save=false;
	$('#datatable tbody tr').each(function() {
		var tr = $(this).closest('tr');	
		if (tr.find("td:eq(13) input").val()!='#'){
			st_save=true;
		}
		
	});
	console.log(st_save);
	if (st_save==false){
		$("#btn_simpan").attr('disabled',true);		
	}else{
		$("#btn_simpan").attr('disabled',false);
	}
}
function noPemesanan() {
	// get nopemesanan
	var id=$("#id").val();
	$.getJSON('{site_url}tgudang_penerimaan/get_nopemesanan2/'+id, function(data) {
		var dataoption = '<option value="#" selected>Pilih No. Pemesanan</option>';
		$.each(data, function(i, item) {
			dataoption += '<option value="' + item.id + '">(' + item.nopemesanan + ') ' + item.nama + '</option>';
		})
		$('.new_nopesan').empty()
		$('.new_nopesan').append(dataoption)
	})
}

function validate_final(){
	$("*[disabled]").not(true).removeAttr("disabled");
	 $("#btn_simpan").hide();
	$("#cover-spin").show();
	return true;
}
</script>
