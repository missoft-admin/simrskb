<?php echo ErrorSuccess($this->session)?> 
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>
<div id="cover-spin"></div>
<form action="{site_url}tgudang_pemesanan/save_edit/" method="post" class="form-horizontal" onsubmit="return validate_final()">
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">{title}</h3>
        </div>
        <div class="block-content">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered info_permintaan">
                        <tr>
                            <td>No.Transaksi</td>
                            <td><?=$nopemesanan ?><input type="hidden" id="idpemesanan" name="idpemesanan" value="<?=$id?>">
							<input type="hidden" id="xidunit" name="xidunit" value="<?=$xidunit?>">
							<input type="hidden" id="xidistributor" name="xidistributor" value="<?=$iddistributor?>">
							<input type="hidden" id="xtipe_bayar" name="xtipe_bayar" value="<?=$tipe_bayar?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Distributor</td>
                            <td><select class="form-control" name="iddistributor" style="width:100%"></select>
                            </td>
                        </tr>
						<tr>
                            <td>Cara Bayar</td>
                            <td>
								<select id="tipe_bayar" name="tipe_bayar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#">- Pilih Tipe-</option>
									<option value="1" <?=("1" == $tipe_bayar ? 'selected' : ''); ?>>TUNAI</option>
									<option value="2" <?=("2" == $tipe_bayar ? 'selected' : ''); ?>>KREDIT</option>
								</select>
                            </td>
                        </tr>
                        
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered info_permintaan">
                        <tr>
                            <td>Tanggal</td>
                            <td><?=$tanggal ?>
                            </td>
                        </tr>
                        <tr>
                            <td>User Edit</td>
                            <td><?=$this->session->userdata("user_name") ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="form-group">
               
				<div class="col-md-2">
                   <button class="btn  btn-success" id="add" type="button" title="Tambah Barang"><i class="fa fa-plus"></i> Tambah Barang</button>
                </div>
				
            </div>
            <table class="table table-bordered" id="datatable">
                <thead>
                    <tr>
                        <th width="5%" class="text-center">Tipe</th>
                        <th width="20%" class="text-center">Nama</th>
                        <th width="5%" class="text-center">SATUAN</th>
                        <th width="10%" class="text-center">Jumlah Pesan</th>
                        <th width="5%" class="text-center">Satuan Pesan</th>
                        <th width="10%" class="text-center">Harga</th>
                        <th width="10%" class="text-center">Total</th>
                        <th width="10%" class="text-center">Aksi</th>
                        <th hidden>idunitpeminta</th>
                        <th hidden>idunitpnerima</th>
                        <th hidden>idtipe</th>
                        <th hidden>idbarang</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($list_detail as $r): ?>
                    <tr>
                        <td class="text-center"><?php echo $r->namatipe ?></td>
                        <td><?php echo $r->namabarang ?></td>
                        <td class="text-center"><?php echo ($r->opsisatuan=='1'?'KECIL':'BESAR') ?></td>
                        <td class="text-right"><?php echo ($r->opsisatuan=='1'?$r->kuantitas_kecil:$r->kuantitas_besar) ?></td>
                        <td class="text-left"><?php echo ($r->opsisatuan=='1'?$r->satuan_kecil:$r->satuan_besar) ?></td>
                        <td class="text-right"><?php echo ($r->opsisatuan=='1'?number_format($r->hargabeli,0):number_format($r->hargabeli_besar,0)) ?></td>
                        <td class="text-right"><?php echo ($r->opsisatuan=='1'?number_format($r->hargabeli*$r->kuantitas_kecil,0):number_format($r->hargabeli_besar*$r->kuantitas_besar,0)) ?></td>
                        <td class="text-center">
							<button class="btn btn-xs btn-success pemesanan"  type="button" title="Edit Barang"><i class="fa fa-pencil"></i> Edit</button>
							<button class="btn btn-xs btn-danger hapus"  type="button" title="Hapus Barang"><i class="glyphicon glyphicon-remove"></i> Hapus</button>
						</td><!-- 7-->
                        <td hidden><input type="text" name="e_idtipe[]" value="<?=$r->idtipe?>"></td>
						<td hidden><input type="text" name="e_idbarang[]" value="<?=$r->idbarang?>"></td>
						<td hidden><input type="text" name="e_id_det[]" value="<?=$r->id?>"></td><!-- 10-->
						<td hidden><input type="text" name="e_idpemesanan[]" value="<?=$r->idpemesanan?>"></td><!-- 11-->
						<td hidden><input type="text" name="e_st_edit[]" value="0"></td><!-- 12-->
						<td hidden><input type="text" name="e_st_hapus[]" value="0"></td><!-- 13-->
						<td hidden><input type="text" name="e_kuantitas_kecil[]" value="<?=$r->kuantitas_kecil?>"></td><!-- 14-->
						<td hidden><input type="text" name="e_kuantitas_besar[]" value="<?=$r->kuantitas_besar?>"></td><!-- 15-->
						<td hidden><input type="text" name="e_namabarang[]" value="<?=$r->namabarang?>"></td><!-- 16-->
						<td hidden><input type="text" name="e_satuankecil[]" value="<?=$r->satuan_kecil?>"></td><!-- 17-->
						<td hidden><input type="text" name="e_satuanbesar[]" value="<?=$r->satuan_besar?>"></td><!-- 18-->
						<td hidden><input type="text" name="e_hargabeli[]" value="<?=$r->hargabeli?>"></td><!-- 19-->
						<td hidden><input type="text" name="e_hargabeli_besar[]" value="<?=$r->hargabeli_besar?>"></td><!-- 20-->
						<td hidden><input type="text" name="opsisatuan[]" value="<?=$r->opsisatuan?>"></td><!-- 21-->
						<td hidden><input type="text" name="jumlahsatuanbesar[]" value="<?=$r->jumlahsatuanbesar?>"></td><!-- 22-->
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
            <div class="form-group text-right">
                <div class="col-md-12">
                    <a href="{site_url}tgudang_pemesanan" class="btn btn-default" type="reset">Kembali</a>
                    <button class="btn btn-success" id="btn_simpan" type="submit">Submit</button>
                </div>
            </div>
			
			
			
        </div>
    </div>
</form>

<form action="javascript:(0)" method="post" class="form-horizontal" id="form3">
    <div class="modal in" id="modal_edit" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">Edit Barang</h3>
                    </div>
                    <div class="block-content">
                        <input type="hidden" id="rowIndex" value="">
                        <input type="hidden" id="xidtipe" name="xidtipe">
                        <input type="hidden" id="xjumlahsatuanbesar" name="xjumlahsatuanbesar">
                        <input type="hidden" id="xsatuankecil" name="xsatuankecil">
                        <input type="hidden" id="xsatuanbesar" name="xsatuanbesar">
                        <input type="hidden" id="xhargabeli" name="xhargabeli">
                        <input type="hidden" id="xhargabeli_besar" name="xhargabeli_besar">
                        <input type="hidden" id="st_edit" name="st_edit">
						<div class="form-group">
                            <label class="control-label col-md-3">Tipe Barang</label>
							
                            <div class="col-md-6">
                               <select class="js-select2 form-control" id="idtipe_baru" name="idtipe_baru" style="width:100%" multiple>
								<?foreach ($get_list_tipe as $row){?>
									<option value="<?=$row->idtipe?>" selected><?=$row->nama_tipe?></option>
								<?}?>
								</select>
                            </div>
							<div class="col-md-3">
                                <input type="text" class="form-control" id="xtipenama" name="xtipenama" readonly>
                            </div>
                        </div>
						<div id="div_edit">
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Barang</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="xnamabarang" name="xnamabarang" readonly>
                            </div>
                        </div>
                        </div>
						<div id="div_add">
                        
						</div>
						<div class="form-group">
							 <label class="control-label col-md-3">Nama Barang</label>
							<div class="col-md-6">
								<div class="input-group">
									<select name="idbarang" id="idbarang" data-placeholder="Cari Barang" class="form-control" style="width:100%"></select>
									<div class="input-group-btn">
										<button class='btn btn-info modal_list_barang' data-toggle="modal" data-target="#modal_list_barang" type='button' id='btn_cari'>
											<i class="fa fa-search"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
                       <div class="form-group">
                            <label class="control-label col-md-3">Satuan</label>
                            <div class="col-md-6">
                                <select id="opsisatuan" name="opsisatuan" class="form-control" style="width: 100%;" data-placeholder="Choose one..">
									
								</select>
                            </div>
                        </div>
						
                        <div class="form-group">
                            <label class="control-label col-md-3">Kuantitas</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                    <input type="text" class="form-control angka" id="kuantitas" placeholder="Kuantitas" name="kuantitas" value="" required="" aria-required="true" autocomplete="off">
                                    <input type="text" class="form-control angka" readonly id="kuantitas_kecil" placeholder="Kuantitas Kecil" name="kuantitas_kecil" value="">
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3">Harga Rp.</label>
                            <div class="col-md-6">
                                <input type="text" readonly class="form-control number" id="xharga" name="xharga" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-sm btn-success" id="btn_update" type="submit">Update</button>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="modal in" id="modal_list_obat" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
            <div class="block-header bg-success">
                <h3 class="block-title">Pencarian Barang</h3>
            </div>
            <div class="block-content">
                <table class="table table-bordered" id="datatable-1">
                    <thead>
                        <th>ID</th>
                        <th>TIPE</th>
                        <th>KODE</th>
                        <th>NAMA</th>
                        <th>STOK</th>
                        <th>PILIH</th>
                        <th hidden>id</th>
                        <th hidden>idtipe</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
var table;
    $(document).ready(function() {
		// $("#idbarang").select2({
		  // // tags: true
		// });
		$('#idbarang').select2();
		$(".number").number(true,0,'.',',');
		$('select[name=iddistributor]').select2({
            placeholder: 'Cari Data ... ',
            ajax: {
                url: '{site_url}tgudang_pemesanan/getdistributor',
                dataType: 'json',
                delay: 250,                
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return { id: item.id, text: item.text}
                        })
                    };
                },
                cache: false,
            }
        })
		$('#opsisatuan').select2({
			placeholder: 'Select an Option',
			data: [
				{id: '', text: ''},
				{id: '1', text: 'KECIL'},
				{id: '2', text: 'BESAR'},
			]
		}).val('').trigger('change')
        var newOption = new Option('{namadistributor}', '{iddistributor}', true, true);
        $('select[name=iddistributor]').append(newOption).trigger('change');
		
		$("#add").click(function() {
				// alert('sini');
				// var tr = $(this).closest('tr');
				
				$("#modal_edit").modal("show");
				$("#xidtipe").val('');
				$("#xjumlahsatuanbesar").val('');
				$("#xsatuankecil").val('');
				$("#xsatuanbesar").val('');
				$("#xhargabeli").val('');
				$("#xhargabeli_besar").val('');
				$("#xtipenama").val('');
				$("#xnamabarang").val('');
				$("#kuantitas").val('0');
				$("#kuantitas_kecil").val('0');
				$('#idbarang').val(null).trigger('change');  
				
				$("#st_edit").val('0');
				hide_show();
				$('#opsisatuan').val(null).trigger('change');  
				// alert(val);
				select2_barang();
			
				  
		});
		function validate() {
				if($('#idbarang').val() == '') {
				   sweetAlert("Maaf...", "Barang harus diisi!", "error");
					$("#kuantitas").focus();
					return false;
				}
				
				if($('#kuantitas').val() <= 0) {
				   sweetAlert("Maaf...", "kuantitas harus diisi!", "error");
					$("#kuantitas").focus();
					return false;
				}
				if($('#xidtipe').val() == '') {
				   sweetAlert("Maaf...", "Tipe harus diisi!", "error");
					$("#kuantitas").focus();
					return false;
				}
				if($('#opsisatuan').val() == '') {
				   sweetAlert("Maaf...", "Satuan harus diisi!", "error");
					$("#kuantitas").focus();
					return false;
				}
				
			   
				return true;
		}
		
		
		$("#btn_update").click(function() {
			if (validate()){
				$("#modal_edit").modal("hide");
				if ($("#st_edit").val()=='1'){
					$('#datatable tbody tr').each(function() {
						var tr = $(this).closest('tr');					
						var $cells = $(this).children('td');
						if (tr[0].sectionRowIndex==$("#rowIndex").val()){
							tr.find("td:eq(12) input").val('1');//st_edit
							tr.find("td:eq(9) input").val($("#idbarang").val());
							tr.find("td:eq(14) input").val($("#kuantitas_kecil").val());
							tr.find("td:eq(15) input").val($("#kuantitas").val());
							tr.find("td:eq(16) input").val($("#xnamabarang").val());
							tr.find("td:eq(17) input").val($("#xsatuankecil").val());
							tr.find("td:eq(18) input").val($("#xsatuanbesar").val());
							tr.find("td:eq(19) input").val($("#xhargabeli").val());
							tr.find("td:eq(20) input").val($("#xhargabeli_besar").val());
							tr.find("td:eq(21) input").val($("#opsisatuan").val());
							tr.find("td:eq(22) input").val($("#xjumlahsatuanbesar").val());
							//Yang Tampil
							tr.find("td:eq(0)").text($("#xtipenama").val());
							tr.find("td:eq(8) input").val($("#xidtipe").val());
							tr.find("td:eq(1)").text($("#xnamabarang").val());
							tr.find("td:eq(2)").text($("#opsisatuan option:selected").text());
							tr.find("td:eq(3)").text($("#kuantitas").val());
							if ($("#opsisatuan").val()=='1'){
								tr.find("td:eq(4)").text($("#xsatuankecil").val());
								tr.find("td:eq(5)").text(formatNumber($("#xhargabeli").val()));
								tr.find("td:eq(6)").text(formatNumber(parseFloat($("#xhargabeli").val())*parseFloat($("#kuantitas").val())));
							}else{
								tr.find("td:eq(4)").text($("#xsatuanbesar").val());
								tr.find("td:eq(5)").text(formatNumber($("#xhargabeli_besar").val()));
								tr.find("td:eq(6)").text(formatNumber(parseFloat($("#xhargabeli_besar").val())*parseFloat($("#kuantitas").val())));
							}
						}
					});
				}else{
					var duplicate = false;
					$('#datatable tbody tr').filter(function (){
						var tr = $(this).closest('tr');
						var c_idtipe=tr.find("td:eq(8) input").val();
						var c_idbarang=tr.find("td:eq(9) input").val();
						// alert(tr.find("td:eq(6) input").val());
						if(c_idtipe == $("#xidtipe").val() && c_idbarang == $("#idbarang").val()){
							// big_notification("Barang sudah ada didalam daftar","error");
							duplicate = true;
							sweetAlert("Duplicate...", "Barang Duplicate!", "error");
						}
					});
					if(duplicate) return false;
					var satuan;
					var harga;
					var tot_harga;
					if ($("#opsisatuan").val()=='1'){
						satuan=$("#xsatuankecil").val();
						harga=formatNumber($("#xhargabeli").val());
						tot_harga=formatNumber(parseFloat($("#xhargabeli").val())*parseFloat($("#kuantitas").val()));
					}else{
						satuan=$("#xsatuanbesar").val();
						harga=formatNumber($("#xhargabeli_besar").val());
						tot_harga=formatNumber(parseFloat($("#xhargabeli_besar").val())*parseFloat($("#kuantitas").val()));
					}
					var content = "<tr>";
					content += "<td class='text-center'>" + $("#xtipenama").val() + "</td>";		
					content += "<td class='text-left'>" + $("#xnamabarang").val(); + "</td>";
					content += "<td class='text-center'>" + $("#opsisatuan option:selected").text() + "</td>";
					content += "<td class='text-right'>" + $("#kuantitas").val(); + "</td>";
					content += "<td class='text-left'>" + satuan + "</td>";
					content += "<td class='text-right'>" + harga + "</td>";
					content += "<td class='text-right'>" + tot_harga + "</td>";
					content += '<td class="text-center"><button class="btn btn-xs btn-success pemesanan"  type="button" title="Edit Barang"><i class="fa fa-pencil"></i> Edit</button> ';
					content += '<button class="btn btn-xs btn-danger hapus"  type="button" title="Hapus Barang"><i class="glyphicon glyphicon-remove"></i> Hapus</button></td>';
					content += '<td hidden><input type="text" name="e_idtipe[]" value="'+$("#xidtipe").val()+'"></td>';
					content += '<td hidden><input type="text" name="e_idbarang[]" value="'+$("#idbarang").val()+'"></td>';
					content += '<td hidden><input type="text" name="e_id_det[]" value="0"></td>';
					content += '<td hidden><input type="text" name="e_idpemesanan[]" value="'+$("#idpemesanan").val()+'"></td>';
					content += '<td hidden><input type="text" name="e_st_edit[]" value="1"></td>';
					content += '<td hidden><input type="text" name="e_st_hapus[]" value="0"></td>';
					content += '<td hidden><input type="text" name="e_kuantitas_kecil[]" value="'+$("#kuantitas_kecil").val()+'"></td>';
					content += '<td hidden><input type="text" name="e_kuantitas_besar[]" value="'+$("#kuantitas").val()+'"></td>';
					content += '<td hidden><input type="text" name="e_namabarang[]" value="'+$("#xnamabarang").val()+'"></td>';
					content += '<td hidden><input type="text" name="e_satuankecil[]" value="'+$("#xsatuankecil").val()+'"></td>';
					content += '<td hidden><input type="text" name="e_satuanbesar[]" value="'+$("#xsatuanbesar").val()+'"></td>';
					content += '<td hidden><input type="text" name="e_hargabeli[]" value="'+$("#xhargabeli").val()+'"></td>';
					content += '<td hidden><input type="text" name="e_hargabeli_besar[]" value="'+$("#xhargabeli_besar").val()+'"></td>';
					content += '<td hidden><input type="text" name="opsisatuan[]" value="'+$("#opsisatuan").val()+'"></td>';
					content += '<td hidden><input type="text" name="jumlahsatuanbesar[]" value="'+$("#xjumlahsatuanbesar").val()+'"></td>';
					content += "</tr>";
					// alert(content);
					$('#datatable tbody').append(content);
				}
			   // alert('update');
		}
        
    });
	var table, tr;
	$(document).on("click",".pemesanan",function(){
  
    // $('#datatable tbody').on('click', '.pemesanan', function() {
        var tr = $(this).closest('tr');
		$('#xidtipe').val(tr.find('td:eq(8) input').val());
		var n = tr.find('td:eq(8) input').val();
        var idunitpelayanan = $("#xidunit").val();
		// alert(idunitpelayanan+n);
		
		var data = { 
            id: tr.find('td:eq(9) input').val(), 
            text: tr.find('td:eq(1)').text()
        };
		var newOption = new Option(data.text, data.id, true, true);
        $('#idbarang').append(newOption);
        // var newOption = new Option(data.text, data.id, true, true);
		// newOption.setAttribute("idtipe","4");	
        // $('#idbarang').append(newOption).trigger('change');
		// select2_barang();
		// $("#idbarang").select2('data', {id: data.id, text: data.text,idtipe:4});
		// $('#idbarang').append($('<option>', { 
				// value: data.id,
				// text : data.text, 
				// 'data-idtipe' : $('#xidtipe').val(),
				// 'data-namatipe' : tr.find('td:eq(0)').text()
			// }));
		// $('#idbarang').append($('<option>', { 
				// value: data.id,
				// text : data.text, 
				// 'data-idtipe' : $('#xidtipe').val(),
				// 'data-namatipe' : tr.find('td:eq(0)').text()
			// }));
		// $('#idbarang').select2();
		
		select2_barang();
        $('#opsisatuan').val(tr.find('td:eq(21) input').val()).trigger('change');
        $('#xtipenama').val(tr.find('td:eq(0)').text());
        $('#xnamabarang').val(tr.find('td:eq(1)').text());
		
       
		get_harga();
        $('#st_edit').val('1');
		hide_show();
		if ($('#opsisatuan').val()=='1'){
			// alert('SINI');
			$('#kuantitas').val(tr.find('td:eq(14) input').val());
			$('#kuantitas_kecil').val(tr.find('td:eq(14) input').val());
			$('#xharga').val($('#xhargabeli').val());
		}else{
			$('#kuantitas').val(tr.find('td:eq(15) input').val());
			$('#kuantitas_kecil').val(tr.find('td:eq(14) input').val());
			$('#xharga').val($('#xhargabeli_besar').val());
		}
		var kd_barang=tr.find('td:eq(9) input').val();
		// $('#idbarang').val(kd_barang);	
		// $("#idbarang").select2('val',kd_barang);
        // alert($("#idbarang").val());
	
		$('#modal_edit').modal('show');
        $("#rowIndex").val(tr[0].sectionRowIndex);
		
		
    });
});
	function select2_barang(){
		var idtipe=$("#idtipe_baru").val();
		var idunitpelayanan=$("#xidunit").val();
		$("#idbarang").select2({
			tags: true,
            minimumInputLength: 2,
            noResults: 'Barang tidak ditemukan.',          
            allowClear: true,
            ajax: {
                url: '{site_url}tgudang_pemesanan/selectbarang_array_tipe/', 
                dataType: 'json', 
                type: 'post', 
                quietMillis: 50, 
                data: function (params) {
                    var query = { search: params.term, idtipe: idtipe, idunitpelayanan: idunitpelayanan } 
                    return query; 
                }, 
                processResults: function (data) {
                    return {
						results: $.map(data, function(item) {
							return {
								text: item.nama+' ['+item.namatipe+']',
								id: item.id,
								idtipe: item.idtipe,
								namatipe: item.namatipe
							}
						})
					};
                }
            }
        }); 
	}
	$("#idbarang").change(function(){
		var obat2 = $("#idbarang").find(":selected").data("idtipe");
		console.log(obat2);
		var data_obat=$("#idbarang").select2('data')[0];
		console.log(data_obat);
		// alert(data_obat.idtipe);
		if (data_obat){
			var idtipe=data_obat.idtipe;
			var namatipe=data_obat.namatipe;
			$("#xidtipe").val(idtipe);
			$("#xtipenama").val(namatipe);
			// cek_stok();
		}else{
			$("#xidtipe").val('');
			$("#xtipenama").val('');
		}
		
		$("#xnamabarang").val($("#idbarang option:selected").text());
		get_harga();

	});
	function get_harga(){
		var ke_unit=$("#xidunit").val();
		var tipe_id=$("#xidtipe").val();
		var idbarang=$("#idbarang").val();
		
		if (idbarang){
			$.ajax({
				url: '{site_url}tgudang_pemesanan/selected_barang_harga/',
				dataType: "JSON",
				method: "POST",
				data : {idtipe: tipe_id, idbarang: idbarang},
				success: function(data) {
					// alert('SINI');
					console.log(data);
					$("#xsatuankecil").val(data.satuan_kecil);
					$("#xsatuanbesar").val(data.satuan_besar);
					$("#xhargabeli").val(data.hargabeli);
					$("#xhargabeli_besar").val(data.hargabeli_besar);
					$("#xjumlahsatuanbesar").val(data.jumlahsatuanbesar);
					konversi_satuan();
				}
			});
		}
	}
	function formatNumber (num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}

	$(document).on("click",".hapus",function(){
	// $(".hapus").click(function() {
		var tr = $(this).closest('tr');
		tr.find("td:eq(7)").html('<label class="label label-danger">Dihapus</label>')
		tr.find("td:eq(12) input").val('1')
		tr.find("td:eq(13) input").val('1')
	   
	});
	$('.angka').keydown(function(event){
		keys = event.keyCode;
		// Untuk: backspace, delete, tab, escape, and enter
		if (event.keyCode == 116 || event.keyCode == 46 || event.keyCode == 188 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
			 // Untuk: Ctrl+A
			(event.keyCode == 65 && event.ctrlKey === true) ||
			 // Untuk: home, end, left, right
			(event.keyCode >= 35 && event.keyCode <= 39) || event.keyCode == 190 || event.keyCode == 110|| event.keyCode == 188) {
				 // melanjutkan untuk memunculkan angka
				return;
		}
		else {
			// jika bukan angka maka tidak terjadi apa-apa
			if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault();
			}
		}

	});
	$("#btn_cari").click(function() {
		// $("#modal_edit").modal("hide");
		
		// if ($("#xidtipe").val()){
			$("#modal_list_obat").modal("show");
			var ke_unit=$("#xidunit").val();
			var tipe_id=$("#idtipe_baru").val();
			// var idbarang=$("#idbarang").val();
			table = $('#datatable-1').DataTable({
				destroy: true,
				serverSide: true,
				searching: true,
				sort: false,
				autoWidth: false,
				ajax: {
					url: '{site_url}tgudang_pemesanan/view_barang/',
					type: 'post',
					data: {idtipe: tipe_id, idunitpelayanan: ke_unit}
				},
				columns: [
					{data: 'id', searchable: false},
					{data: 'namatipe'},
					{data: 'kode'},
					{data: 'nama'},
					{data: 'stok'},
					{defaultContent: '<a href="#" class="btn btn-xs btn-success pilih">Pilih</a>', searchable: false },
					{data: 'id', visible: false},
					{data: 'idtipe', visible: false},
				]
			})
			setTimeout(function() {
				$('div.dataTables_filter input').focus()
			}, 500);
		// }
	
	});
	$('#datatable-1 tbody').on('click', 'a', function(){
		var table = $('#datatable-1').DataTable();
        tr = table.row( $(this).parents('tr') ).index()


        var data = { 
            id: table.cell(tr,0).data(), 
            text: table.cell(tr,3).data()
        };
        var newOption = new Option(data.text, data.id, true, true);
        $('#idbarang').append(newOption);  
		// alert(table.cell(tr,8).data());
		$("#xidtipe").val(table.cell(tr,7).data());
		$("#idbarang").val(table.cell(tr,6).data());
		$("#xnamabarang").val(table.cell(tr,3).data());
		$("#xtipenama").val(table.cell(tr,1).data());
		get_harga();
        $('#modal_list_obat').modal('hide');
    })
    
    
	$('#kuantitas').keyup(function(event){
		konversi_satuan();
	});
	
	function hide_show(){
		// alert($("#st_edit").val());
		if ($("#st_edit").val()=='0') {
			$("#div_add").show();
			$("#div_edit").hide();
		}else{
			$("#div_add").hide();
			$("#div_edit").show();
		}
	}
	$("#opsisatuan").change(function(){
		konversi_satuan();
		   

	});

    function konversi_satuan(){
		if ($("#opsisatuan").val()=='2'){
			$('#kuantitas_kecil').val(parseFloat($("#xjumlahsatuanbesar").val())*parseFloat($("#kuantitas").val()));		
			$('#xharga').val($('#xhargabeli_besar').val());		
		}else{
			$('#kuantitas_kecil').val($("#kuantitas").val());
			$('#xharga').val($('#xhargabeli').val());	
		}
	}
	$(document).on("keyup",".angka",function(){
		var jml=$(this).closest('tr').find("td:eq(5)").html();
		// alert($(this).val()+' '+jml);
		if (parseFloat($(this).val())>parseFloat(jml)){
			alert('Pengiriman Lebih Besar Dari Sisa');
			$(this).val(jml);
		}
	});
	function validate_final(){
		 $("#btn_simpan").hide();
		$("#cover-spin").show();
		return true;
	}
</script>
