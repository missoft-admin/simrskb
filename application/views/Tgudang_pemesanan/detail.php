<?php echo ErrorSuccess($this->session)?> 
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>

<form action="{site_url}tgudang_pemesanan/save_edit/" method="post" class="form-horizontal">
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">{title}</h3>
        </div>
        <div class="block-content">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered info_permintaan">
                        <tr>
                            <td>No.Transaksi</td>
                            <td><?=$nopemesanan ?><input type="hidden" id="idpemesanan" name="idpemesanan" value="<?=$id?>">
							<input type="hidden" id="xidunit" name="xidunit" value="0">
							<input type="hidden" id="xidistributor" name="xidistributor" value="<?=$iddistributor?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Distributor</td>
                            <td><?=$namadistributor?>
                            </td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td><?=($stdraft?'<span class="label label-info">DRAFT</span>':status_pesan_gudang($status))?>
                            </td>
                        </tr>
                        
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered info_permintaan">
                        <tr>
                            <td>Tipe Bayar</td>
                            <td><?=($tipe_bayar=='1'?text_success('TUNAI'):text_primary('KREDIT')) ?>
                            </td>
                        </tr>
						<tr>
                            <td>Tanggal</td>
                            <td><?=$tanggal ?>
                            </td>
                        </tr>
                        <tr>
                            <td>User Edit</td>
                            <td><?=$this->session->userdata("user_name") ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            
            <table class="table table-bordered" id="datatable">
                <thead>
                    <tr>
                        <th width="5%" class="text-center">Tipe</th>
                        <th width="20%" class="text-center">Nama</th>
                        <th width="5%" class="text-center">SATUAN</th>
                        <th width="10%" class="text-center">Jumlah Pesan</th>
                        <th width="5%" class="text-center">Satuan Pesan</th>
                        <th width="10%" class="text-center">Harga</th>
                        <th width="10%" class="text-center">Total</th>
                        <th width="10%" class="text-center">detail</th>
                        <th hidden>idunitpeminta</th>
                        <th hidden>idunitpnerima</th>
                        <th hidden>idtipe</th>
                        <th hidden>idbarang</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($list_detail as $r): ?>
                    <tr>
                        <td class="text-center"><?php echo $r->namatipe ?></td>
                        <td><?php echo $r->namabarang ?></td>
                        <td class="text-center"><?php echo ($r->opsisatuan=='1'?'KECIL':'BESAR') ?></td>
                        <td class="text-right"><?php echo ($r->opsisatuan=='1'?$r->kuantitas_kecil:$r->kuantitas_besar) ?></td>
                        <td class="text-left"><?php echo ($r->opsisatuan=='1'?$r->satuan_kecil:$r->satuan_besar) ?></td>
                        <td class="text-right"><?php echo ($r->opsisatuan=='1'?number_format($r->hargabeli,0):number_format($r->hargabeli_besar,0)) ?></td>
                        <td class="text-right"><?php echo ($r->opsisatuan=='1'?number_format($r->hargabeli*$r->kuantitas_kecil,0):number_format($r->hargabeli_besar*$r->kuantitas_besar,0)) ?></td>
                        <td class="text-center">
							<button class="btn btn-xs btn-success pemesanan"  type="button" title="Unit Peminta"><i class="fa fa-eye"></i> Unit</button>
						</td><!-- 7-->
                        <td hidden><input type="text" name="e_idtipe[]" value="<?=$r->idtipe?>"></td>
						<td hidden><input type="text" name="e_idbarang[]" value="<?=$r->idbarang?>"></td>
						<td hidden><input type="text" name="e_id_det[]" value="<?=$r->id?>"></td><!-- 10-->
						<td hidden><input type="text" name="e_idpemesanan[]" value="<?=$r->idpemesanan?>"></td><!-- 11-->
						<td hidden><input type="text" name="e_st_edit[]" value="0"></td><!-- 12-->
						<td hidden><input type="text" name="e_st_hapus[]" value="0"></td><!-- 13-->
						<td hidden><input type="text" name="e_kuantitas_kecil[]" value="<?=$r->kuantitas_kecil?>"></td><!-- 14-->
						<td hidden><input type="text" name="e_kuantitas_besar[]" value="<?=$r->kuantitas_besar?>"></td><!-- 15-->
						<td hidden><input type="text" name="e_namabarang[]" value="<?=$r->namabarang?>"></td><!-- 16-->
						<td hidden><input type="text" name="e_satuankecil[]" value="<?=$r->satuan_kecil?>"></td><!-- 17-->
						<td hidden><input type="text" name="e_satuanbesar[]" value="<?=$r->satuan_besar?>"></td><!-- 18-->
						<td hidden><input type="text" name="e_hargabeli[]" value="<?=$r->hargabeli?>"></td><!-- 19-->
						<td hidden><input type="text" name="e_hargabeli_besar[]" value="<?=$r->hargabeli_besar?>"></td><!-- 20-->
						<td hidden><input type="text" name="opsisatuan[]" value="<?=$r->opsisatuan?>"></td><!-- 21-->
						<td hidden><input type="text" name="jumlahsatuanbesar[]" value="<?=$r->jumlahsatuanbesar?>"></td><!-- 22-->
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
            <div class="form-group text-right">
                <div class="col-md-12">
                    <a href="{site_url}tgudang_pemesanan" class="btn btn-default" type="reset">Kembali</a>
                </div>
            </div>
			
			
			
        </div>
    </div>
</form>

<form action="javascript:(0)" method="post" class="form-horizontal" id="form3">
    <div class="modal in" id="modal_edit" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">Unit Peminta</h3>
                    </div>
                    <div class="block-content">
                        <table class="table table-bordered" id="tabel_detail">
							<thead>
								<th>NO</th>
								<th>KODE</th>
								<th>UNIT</th>
								<th>STOK UNIT</th>
								<th>QTY MINTA</th>
								<th>SATUAN</th>
							</thead>
							<tbody></tbody>
						</table>
					</div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="modal in" id="modal_list_obat" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
            <div class="block-header bg-success">
                <h3 class="block-title">Pencarian Barang</h3>
            </div>
            <div class="block-content">
                <table class="table table-bordered" id="datatable-1">
                    <thead>
                        <th>ID</th>
                        <th>TIPE</th>
                        <th>KODE</th>
                        <th>NAMA</th>
                        <th>STOK</th>
                        <th>PILIH</th>
                        <th hidden>id</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
		$(".number").number(true,0,'.',',');
		
	var table, tr;
	$(document).on("click",".pemesanan",function(){
		var tr = $(this).closest('tr');
		var iddet=tr.find('td:eq(10) input').val();
		$('#modal_edit').modal('show');	
		table = $('#tabel_detail').DataTable({
				destroy: true,
				serverSide: true,
				searching: false,
				sort: false,
				autoWidth: false,
				ajax: {
					url: '{site_url}tgudang_pemesanan/view_unit/',
					type: 'post',
					data: {iddet: iddet}
				},
				columns: [
					{data: 'nomor', searchable: false},
					{data: 'no_up_permintaan'},
					{data: 'unit'},
					{data: 'stok'},
					{data: 'kuantitas'},
					{data: 'jenis_satuan'},
				]
			})
			setTimeout(function() {
				$('div.dataTables_filter input').focus()
			}, 500);
    });
});
	
</script>
