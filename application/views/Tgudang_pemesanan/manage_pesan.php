<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <form id="form1" class="form-horizontal" method="post" action="{base_url}tgudang_pemesanan/save_pesan/" onsubmit="return validate_final()">
			<div class="form-group">
				<label class="col-md-2">Tipe Barang</label>
				<div class="col-md-8"> 
					<select name="idtipe[]" id="idtipe" class="js-select2 form-control" style="width:100%" multiple>
						<?foreach($list_tipe as $row){?>
							<option value="<?=$row->idtipe?>" selected><strong><?=$row->nama_tipe?></strong></option>
						<?}?>
					</select>
				</div>
			</div>
			
			<div class="form-group">
                <label class="col-md-2">Barang</label>
                <div class="col-md-6">
                    <div class="input-group">
                        <select name="idbarang" id="idbarang" data-placeholder="Cari Barang" class="form-control" style="width:100%"></select>
                        <div class="input-group-btn">
                            <button class='btn btn-info modal_list_barang' data-toggle="modal" data-target="#modal_list_barang" type='button' id='caribarang'>
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>	
				<div class="col-md-2">
                    <input type="text" class="form-control angka" readonly id="nama_tipe" name="nama_tipe">
                </div>
            </div>
			<div class="form-group">
				<label class="col-md-2">Satuan</label>
				<div class="col-md-2"> 
					<select name="opsisatuan" id="opsisatuan" class="js-select2 form-control" style="width:100%">
						<option value="1">Kecil</option>
						<option value="2" selected>Besar</option>						
					</select>
				</div>
				<label class="col-md-1">Harga</label>
                <div class="col-md-3">
                    <input type="text" class="form-control number" readonly id="harga" name="harga">
                </div>
			</div>
			<div class="form-group">
                
				<label class="col-md-2">Jumlah</label>
                <div class="col-md-2">
                    <input type="text" class="form-control number" id="kuantitas" name="kuantitas">
                </div>
				<label class="col-md-1">Total Harga</label>
                <div class="col-md-3">
                    <input type="text" class="form-control number" readonly id="totalharga" name="totalharga">
                </div>				
            </div>
			<div class="form-group">
                
											
            </div>
			<div class="form-group">
				<label class="col-md-2">Distributor</label>
                <div class="col-md-6">
				   <select id="iddistributor" name="iddistributor" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#">- Pilih Distributor -</option>
						<?php foreach ($list_distributor as $row) { ?>
								<option value="<?php echo $row->id; ?>" <?=($filter_distributor == $row->id ? 'selected' : ''); ?>><?php echo $row->nama; ?></option>
						<?php } ?>
					</select>					
				</div>
            </div>
            <div class="form-group" hidden>
                <label class="col-md-2">Identitas Barang</label>
                <div class="col-md-2">
                    <input type="text" class="form-control" id="t_idbarang" name="t_idbarang">
                </div>
				<div class="col-md-2">
                    <input type="text" class="form-control" id="t_idtipe" name="t_idtipe">
                </div>
				<div class="col-md-2">
                    <input type="text" class="form-control" id="t_harga_besar" name="t_harga_besar">
                </div>
				<div class="col-md-2">
                    <input type="text" class="form-control" id="t_harga_kecil" name="t_harga_kecil">
                </div>
				<div class="col-md-2">
                    <input type="text" class="form-control" id="t_jml_besar" name="t_jml_besar">
                </div>
				
				
            </div>

            <div class="form-group">
               <label class="col-md-2"></label>
				<div class="col-md-6 pull-left text-left">

                    <button type="button" class="btn btn-primary" id="tambahBarang"><li class="fa fa-plus"></li>Tambahkan</button>
                </div>
				
            </div>
			 <table class="table table-bordered" id="tempPemesanan">
				<thead>
					<tr>
						<th>Tipe</th>
						<th>Barang</th>
						<th>Distributor</th>
						<th>Harga</th>
						<th>Kuantitas</th>
						<th>Satuan</th>
						<th>idtipe</th>
						<th>iddistributor</th>
						<th>idbarang</th>
						<th>Aksi</th>
						<th>opsisatuan</th>
						<th>hargaterakhir</th>
						<th>stokterakhir</th>
						<th>kuantitas_kecil</th>
					</tr>
				</thead>
				<tbody></tbody>
			 </table>
			<input type="hidden" name="detailValue" id="detailValue">
			<input type="hidden" name="rowIndex" id="rowIndex">
			<div style="margin-top:50px;margin-bottom:20px;" class="text-right">
                <a href="{site_url}tgudang_pemesanan" class="btn btn-default btn-md">Kembali</a>
                <button type="submit" class="btn btn-success btn-md" id="btn_simpan">Simpan</button>
            </div>
        </form>
        
    </div>
</div>

<div class="modal in" id="modal_list_barang" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
            <div class="block-header bg-success">
                <h3 class="block-title">Pencarian Barang</h3>
            </div>
            <div class="block-content">
			<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
				<div class="row">
					<label class="col-md-2 control-label" for="idkategori">Kategori Barang</label>
					<div class="col-md-8">
						<div class="form-group" style="margin-bottom: 5px;">
							
							<select name="idtipe_cari[]" id="idtipe_cari" class="js-select2 form-control" style="width:100%" multiple>
								<?foreach($list_tipe as $row){?>
									<option value="<?=$row->idtipe?>" selected><strong><?=$row->nama_tipe?></strong></option>
								<?}?>
							</select>
						</div>
						
					</div>
					
				</div>
				<?php echo form_close() ?>
				<div class="block-content">
				<div class="row">
                <table class="table table-bordered" id="datatable-1">
                    <thead>
                        <th hidden>idbarang</th>
                        <th hidden>idtipe</th>
                        <th>Tipe</th>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>Stok</th>
                        <th>Satuan</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>

<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

    var table;
	var t;
	var option;
	// var tabel = $('#datatable-2').DataTable();
    $(document).ready(function(){
      validate_save();
	 
		$(".number").number(true,0,'.',',');
		$("#idbarang").select2({
			minimumInputLength: 2,
			noResults: 'Obat Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}tgudang_pemesanan/selectbarang/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,

				data: function(params) {
					var query = {
						search: params.term,
						idtipe: $("#idtipe").val(),
					}
					return query;
				},
				processResults: function(data) {
					return {
						results: $.map(data, function(item) {
							return {
								text: item.nama+' ['+item.namatipe+']',
								id: item.id,
								idtipe: item.idtipe,
								namatipe: item.namatipe
							}
						})
					};
				}
			}
		});
		clearTempPemesanan();
    })
	$('select[name=idbarang]').on('change', function(){
		var data_obat=$("#idbarang").select2('data')[0];
		console.log(data_obat);
		if (data_obat){
			var id=data_obat.id;
			var idtipe=data_obat.idtipe;
			var namatipe=data_obat.namatipe;
			$("#t_idbarang").val(id);
			$("#t_idtipe").val(idtipe);
			$("#nama_tipe").val(namatipe);
			cek_harga();
			load_distributor();
			cek_distibutor();
			$("#kuantitas").focus();
		}else{
			$("#t_idbarang").val('');
			$("#t_idtipe").val('');
			$("#t_namatipe").val('');
		}
		// alert(idtipe);
       
    })
	function load_distributor(){
		var idtipe=$("#t_idtipe").val();
		var idbarang=$("#t_idbarang").val();
		// alert($("#iddistributor").val());
		$('#iddistributor').select2({
			placeholder: 'Cari Data ... ',
			allowClear: true,
			ajax: {
				url: '{site_url}tgudang_pemesanan/getsorted_distributor_2/',
				dataType: 'json',
				delay: 500,
				type: 'post', 
				quietMillis: 50, 
				data: function (params) {
					var query = { search: params.term, idtipe: idtipe, idbarang: idbarang } 
					return query; 
				}, 
				processResults: function(data) {
					return {
						results: data
					}
				},
				cache: false,
			}
		});  
	}
	
   
	function cek_harga(){
		var idbarang=$("#t_idbarang").val();
		var idtipe=$("#t_idtipe").val();
		
		if (idbarang){
		$.ajax({
			url: '{site_url}tgudang_pemesanan/get_harga/',
			dataType: "JSON",
			method: "POST",
			data: { idbarang: idbarang, idtipe: idtipe},
			success: function(data) {
				// console(data);			
				if (data){
					// alert(data.harga_besar);
					$("#t_harga_besar").val(data.harga_besar);
					$("#t_harga_kecil").val(data.harga_kecil);
					$("#t_jml_besar").val(data.jumlahsatuanbesar);
					set_harga();
				}
			}
		});
		}
	}
	function cek_distibutor(){
		var idbarang=$("#t_idbarang").val();
		var idtipe=$("#t_idtipe").val();
		var iddistributor=$("#iddistributor").val();
		
		if (iddistributor !='#'){
			$.ajax({
				url: '{site_url}tgudang_pemesanan/cek_distibutor/',
				dataType: "JSON",
				method: "POST",
				data: { idbarang: idbarang, idtipe: idtipe, iddistributor: iddistributor},
				success: function(data) {
					// alert(data.text);
					var newOption = new Option(data.text, data.id, true, true);
					$('#iddistributor').append(newOption).trigger('change');

				}
			});
		}
	}
	function set_harga(){
		if ($("#opsisatuan").val()=='1'){
			$("#harga").val($("#t_harga_kecil").val());
		}else{
		// alert($("#t_harga_besar").val());
			$("#harga").val($("#t_harga_besar").val());			
		}
		$("#totalharga").val($("#harga").val()*$("#kuantitas").val());	
	}
	$('#opsisatuan').on('change', function(){
		set_harga();        
    })
	$('#kuantitas').on('keyup', function(){
		set_harga();        
    })
	
	$('.modal_list_barang').on('click', function(){
        $('select[name=idbarang]').val('').trigger('change');
        var idtipe = $('select[name=idtipe]').val();
        var idunitpelayanan = $('select[name=dariunit]').val();
		// load_kategori();
        if(idtipe !== '') {
			loadBarang();
           
        }
    })
	function clearTempPemesanan() {
		t = $('#tempPemesanan').DataTable({
			paging: false,
			info: false,
			sort: false,
			searching: false,
			destroy: true,
			autoWidth: false,
			columnDefs: [{ "targets": [ 6,7,8,10,11,12,13], "visible": false }]
		})
		t.clear().draw()
	}
	function clearform() {
        $('select[name=idbarang]').val(null).trigger('change');
        // $('select[name=dariunit], select[name=keunit]').empty();
        // $('input[name=kodebarang], input[name=namabarang], input[name=idbarang], input[name=stokunit], input[name=kuantitas], input[name=t_idtipe], input[name=t_namatipe]').val('');
        $("#rowIndex").val('');
        $("#harga").val('');
        $("#totalharga").val('');
        $("#kuantitas").val('');
		validate_save();
    }
    $(document).on( 'click', '#tambahBarang', function(){
		// alert($("#iddistributor").val());
		var t = $('#tempPemesanan').DataTable({
			paging: false,
			info: false,
			sort: false,
			searching: false,
			destroy: true,
			autoWidth: false,
			columnDefs: [{ "targets": [6,7,8,10,11,12,13], "visible": false }]
		})
		var kuantitas_kecil;
		if ($("#opsisatuan").val()=='1'){
			kuantitas_kecil=$('#kuantitas').val();
		}else{
			kuantitas_kecil=$('#kuantitas').val()*$('#t_jml_besar').val();
		}
		// alert($('#tambahBarang').text());
		if(validate()) {
			if($('#tambahBarang').text() == 'Tambahkan') {
				t.row.add([
					$('#nama_tipe').val(),
					$('#idbarang :selected').text(),
					$('#iddistributor :selected').text(),
					$('#harga').val(),
					$('#kuantitas').val(),
					$('#opsisatuan :selected').text(),
					$('#t_idtipe').val(),
					$('#iddistributor').val(),
					$('#t_idbarang').val(),
					"<a href='#' class='on-default detailEdit' title='Edit'><i class='fa fa-pencil'></i></a>&nbsp;"+
					"<a href='#' class='on-default detailRemove' title='Hapus'><i class='fa fa-trash-o'></i></a>",
					$('#opsisatuan :selected').val(),
					$('#harga').val(),
					0,
					kuantitas_kecil,
				]).draw(false)
				clearform();
				$.toaster({priority : 'success', title : 'Sukses!', message : 'Data berhasil ditambahkan'});
			} else {
				t.row( $('#rowIndex').val() ).data([
					$('#nama_tipe').val(),
					$('#idbarang :selected').text(),
					$('#iddistributor :selected').text(),
					$('#harga').val(),
					$('#kuantitas').val(),
					$('#opsisatuan :selected').text(),
					$('#t_idtipe').val(),
					$('#iddistributor').val(),
					$('#t_idbarang').val(),
					"<a href='#' class='on-default detailEdit' title='Edit'><i class='fa fa-pencil'></i></a>&nbsp;"+
					"<a href='#' class='on-default detailRemove' title='Hapus'><i class='fa fa-trash-o'></i></a>",
					$('#opsisatuan :selected').val(),
					$('#harga').val(),
					0,
					kuantitas_kecil,
				]).draw(false)
				clearform()
				$.toaster({priority : 'success', title : 'Sukses!', message : 'Data berhasil diperbaharui'});
			}
				$('#tambahBarang').text('Tambahkan').removeClass('btn-warning').addClass('btn-success')
			return true;
		} else {
			return false;
		}

	})


$('#tempPemesanan tbody').on( 'click', 'a.detailEdit', function () {
    $('#tambahBarang').text('Update').removeClass('btn-success').addClass('btn-warning')
    t = $('#tempPemesanan').DataTable({
        paging: false,
        info: false,
        sort: false,
        searching: false,
        destroy: true,
        autoWidth: false,
        columnDefs: [{ "targets": [ 6,7,8,10,11,12,13], "visible": false }]
    })

    var rowIndex = t.row($(this).parents('tr')).index()
    

    $('#rowIndex').val( rowIndex )
    $('#harga').val( t.cell(rowIndex,3).data() )
    $('#kuantitas').val( t.cell(rowIndex,4).data() )
	var namabarang=t.cell(rowIndex,1).data();
	var idbarang=t.cell(rowIndex,8).data();
	var idtipe=t.cell(rowIndex,6).data();
	var iddistributor=t.cell(rowIndex,7).data();
	var namadist=t.cell(rowIndex,2).data();
	var newOption = new Option(namabarang, idbarang, true, true);$('#idbarang').append(newOption);
	var newOption = new Option(namadist, iddistributor, true, true);$('#iddistributor').append(newOption);
	$('#t_idbarang').val(idbarang);
	$('#t_idtipe').val(idtipe);	
    $('#opsisatuan').val(t.cell(rowIndex,10).data()).trigger('change');
	cek_harga();
	set_harga();
    
})
$('#tempPemesanan tbody').on( 'click', '.detailRemove', function () {
    t = $('#tempPemesanan').DataTable({
        paging: false,
        info: false,
        sort: false,
        searching: false,
        destroy: true,
        autoWidth: false,
        columnDefs: [{ "targets": [ 6,7,8,10,11,12,13], "visible": false }]
    })
    t.row($(this).parents('tr')).remove().draw()

    $('#tambahBarang').text('Tambahkan').removeClass('btn-warning').addClass('btn-success')
   
    $.toaster({priority : 'success', title : 'Sukses!', message : 'Data berhasil dihapus'});
})

    function validate() {
        if($('#t_idtipe').val() == '') {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Barang Belum Pilih'});
            return false;
        }
        if($('#t_idbarang').val() == '') {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Kode Barang tidak boleh kosong'});
            return false;
        }
        if($('#idbarang').val() == null) {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Kode Barang tidak boleh kosong'});
            return false;
        }
		// alert($('#iddistributor').val());
		if($('#kuantitas').val() == '' || $('#kuantitas').val() == '0') {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Kuantitas tidak boleh kosong'});
            return false;
        }
		if($('#harga').val() == '' || $('#harga').val() == '0') {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Harga tidak boleh kosong'});
            return false;
        }
		if($('#kuantitas').val() == '' || $('#kuantitas').val() == '0') {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Kuantitas tidak boleh kosong'});
            return false;
        }
		if($('#iddistributor').val() == null || $('#iddistributor').val() == '#') {
            $.toaster({priority : 'danger', title : 'Error!', message : 'Distributor tidak boleh kosong'});
            return false;
        }
        return true;
    }
	function validate_save() {
		var total_grand = 0;
			$('#tempPemesanan tbody tr').each(function() {
				total_grand += 1;
			});
		// var table = $('#datatable-2').DataTable();
       // var jml_baris=table.length;
	   if (total_grand>0){
		   $("#btn_simpan").attr('disabled', false);
	   }else{
		   $("#btn_simpan").attr('disabled', true);
		   
	   }
	   // alert(total_grand);
    }
	function validate_final() {
		t = $('#tempPemesanan').DataTable({
			paging: false,
			info: false,
			sort: false,
			searching: false,
			destroy: true,
			autoWidth: false,
			columnDefs: [{ "targets": [6,7,8,10,11,12,13], "visible": false }],
			stateSave: true,
		})

		 var arr = t.rows().data().toArray();
		 $("#detailValue").val(JSON.stringify(arr));
		 $("#btn_simpan").hide();
		 $("#cover-spin").show();
		
		 // return false;
		 // alert(arr);
    }
	
    
	$(document).on("click", "#btn_filter_barang", function() {

			loadBarang();

	});
	function loadBarang() {
		var idtipe=$("#idtipe_cari").val();
		$('#datatable-1').DataTable().destroy();
		var table = $('#datatable-1').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}tgudang_pemesanan/getBarang_cari/',
			type: "POST",
			dataType: 'json',
			data: {
				idtipe: idtipe,
			}
		},
		columnDefs: [
		{ targets: [0,1], visible: false}
		]
		});
	}
	$(document).on("click", ".pilih", function() {
		 table = $('#datatable-1').DataTable()
		var tr = $(this).parents('tr')
		var idbarang = table.cell(tr,0).data()
		var namabarang = table.cell(tr,4).data()
		var idtipe = table.cell(tr,1).data()
		$("#t_idtipe").val(idtipe);
		$("#t_idbarang").val(idbarang);
		cek_harga();
		set_harga();
		load_distributor();
		cek_distibutor();
		var newOption = new Option(namabarang, idbarang, true, true);
		$('#idbarang').append(newOption);
		$("#modal_list_barang").modal('hide');
		$('#kuantitas').focus();
		
	});
</script>
