<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('1115'))){ ?>
<div class="block">
	<div class="block-header">
        <?php if (UserAccesForm($user_acces_form,array('1116'))){ ?>
    		<ul class="block-options">
                <li><a href="{base_url}tgudang_pemesanan/add" class="btn btn-default"><i class="fa fa-plus"></i> Tambah</a></li>
            </ul>
        <?php }?>
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:25px">
        <div class="row">
            <?php echo form_open('tgudang_pemesanan/filter', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="distributor">Distributor</label>
                    <div class="col-md-8">
                        <select id="filter_distributor" name="filter_distributor" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#">- Semua Distributor -</option>
                            <?php foreach ($list_distributor as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?=($filter_distributor == $row->id ? 'selected' : ''); ?>><?php echo $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Status</label>
                    <div class="col-md-8">
                        <select id="status" name="statuspesan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#">- Semua Status -</option>
                            <option value="10" <?=("10" == $statuspesan ? 'selected' : ''); ?>>DRAFT</option>
                            <option value="1" <?=("1" == $statuspesan ? 'selected' : ''); ?>>PENDING</option>
                            <option value="2" <?=("2" == $statuspesan ? 'selected' : ''); ?>>PERSETUJUAN KEUANGAN</option>
                            <option value="3" <?=("3" == $statuspesan ? 'selected' : ''); ?>>PERSETUJUAN PEMESANAN</option>
                            <option value="4" <?=("4" == $statuspesan ? 'selected' : ''); ?>>PROSES PEMESANAN</option>
                            <option value="0" <?=("0" == $statuspesan ? 'selected' : ''); ?>>DIBATALKAN</option>
                            <option value="5" <?=("5" == $statuspesan ? 'selected' : ''); ?>>SELESAI</option>
                        </select>
                    </div>
                </div>
                
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" name="tanggaldari" placeholder="From" value="{tanggaldari}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" name="tanggalsampai" placeholder="To" value="{tanggalsampai}"/>
                        </div>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">No. PO</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_trx" placeholder="No. PO" name="no_trx" value="{no_trx}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
					<?php if (UserAccesForm($user_acces_form,array('1118'))){ ?>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
					<?}?>
                </div>
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<div class="table-responsive">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th width="5%">ID</th>
                    <th width="2%">ID</th>
                    <th width="10%">NO. PO</th>
                    <th width="10%">TANGGAL</th>
                    <th width="10%">TIPE</th>
                    <th width="15%">DISTRIBUTOR</th>
                    <th width="5%">ITEM</th>
                    <th width="15%" class="text-center">Status</th>
                    <th width="15%" class="text-center">AKSI</th>
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
	</div>
</div>
<?}?>
<div class="modal" id="modalAdd" role="dialog" aria-hidden="true" aria-labelledby="modalAdd" font size="15">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Tambah Pemesanan</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal" id="form_detail">
                        <input type="hidden" id="rowIndex"/>
                        <input type="hidden" id="idbarang"/>
                        <input type="hidden" id="hargaterakhir"/>
                        <input type="hidden" id="stokterakhir"/>
                        <input type="hidden" name="c_harga_b" id="c_harga_b"/>
                        <input type="hidden" name="c_harga_k" id="c_harga_k"/>
                        <input type="hidden" name="jml_besar" id="jml_besar"/>

                        <div class="form-group c" id="f_idtipe">
                            <label class="col-md-3 control-label">Tipe</label>
                            <div class="col-md-6">
                                <select id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.."></select>
                            </div>
                        </div>

                        <div class="form-group c" id="f_namabarang">
                            <label class="col-md-3 control-label" for="namabarang">Nama Barang</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                    <input readonly type="text" class="form-control" id="namabarang" placeholder="Nama Barang" name="namabarang" value="" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
						<div class="form-group">
							 <label class="control-label col-md-3">Nama Barang</label>
							<div class="col-md-6">
								<div class="input-group">
									<select name="c_idbarang" id="c_idbarang" data-placeholder="Cari Barang" class="form-control" style="width:100%"></select>
									<div class="input-group-btn">
										<button class='btn btn-info modal_list_barang' data-toggle="modal" data-target="#modal_list_barang" type='button' id='btn_cari'>
											<i class="fa fa-search"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
                        
                        <div class="form-group c" id="f_hargabeli">
                            <label class="col-md-3 control-label" for="hargabeli">Harga Beli</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                    <input readonly type="text" class="form-control" id="hargabeli" placeholder="Harga Beli" name="hargabeli" value="" required="" aria-required="true">
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-3 control-label">Opsi Satuan</label>
                            <div class="col-md-6">
                                <select id="opsisatuan" name="opsisatuan" class="form-control" style="width: 100%;" data-placeholder="Choose one..">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Distributor</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <select class="form-control" id="c_iddistributor" name="iddistributor" style="width:100%" required></select>
                                    <div class="input-group-btn">
                                        <button class='btn btn-info modal_list_distributor'  data-target="#modal_list_distributor" id="btn_cari_distributor" type='button' title="Pencarian">
                                            <i class="fa fa-search"></i>
                                        </button>
										<?php if (UserAccesForm($user_acces_form,array('1117'))){ ?>
										<a href="{base_url}mdistributor" class="btn btn-success" target="blank" title="Distributor Baru"><i class="fa fa-plus"></i></a>
										<?}?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group c" id="f_kuantitas">
                            <label class="col-md-3 control-label" for="kuantitas">Kuantitas</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                    <input type="text" class="form-control angka" id="kuantitas" placeholder="Kuantitas" name="kuantitas" value="" required="" aria-required="true" autocomplete="off">
                                    <input type="text" class="form-control angka" readonly id="kuantitas_kecil" placeholder="Kuantitas Kecil" name="kuantitas_kecil" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="kuantitas">&nbsp;</label>
                            <div class="col-md-6">
                                <button class="btn btn-sm btn-success pull-right" type="button" id="tambahBarang">Tambahkan</button>
                            </div>
                        </div>
						<div class="table-responsive">
                        <table class="table table-bordered" id="tempPemesanan">
                            <thead>
                                <tr>
                                    <th>Tipe</th>
                                    <th>Barang</th>
                                    <th>Distributor</th>
                                    <th>Harga</th>
                                    <th>Kuantitas</th>
                                    <th>Satuan</th>
                                    <th>idtipe</th>
                                    <th>iddistributor</th>
                                    <th>idbarang</th>
                                    <th>Aksi</th>
                                    <th>opsisatuan</th>
                                    <th>hargaterakhir</th>
                                    <th>stokterakhir</th>
                                    <th>kuantitas_kecil</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" id="tutup" data-dismiss="modal">Tutup</button>
                <button class="btn btn-sm btn-success" type="button" id="simpan">Simpan</button>
            </div>
        </div>
    </div>
</div>


<div class="modal in" id="modal-list-barang" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">List Barang</h3>
                </div>
                <div class="block-content">
					<div class="table-responsive">
                    <table class="table table-bordered" id="list-barang">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>TIPE</th>
                                <th>KODE</th>
                                <th>NAMA</th>
                                <th>Harga</th>
                                <th>Stok</th>
                                <th>pilih</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" id="btnBarangBack" type="button" data-dismiss="modal">Back</button>
            </div>
        </div>
    </div>
</div>

<div class="modal in" id="modalDetailPemesanan" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Detail Pemesanan</h3>
                </div>
                <div class="block-content">
                    <p id="status"></p>
					<div class="table-responsive">
                    <table class="table table-bordered" id="detailPemesanan">
                        <thead>
                            <tr>
                                <th>Tipe</th>
                                <th>Barang</th>
                                <th>Satuan</th>
                                <th>Harga</th>
                                <th>Jml Pesan</th>
                                <th>Jml Terima</th>
                                <th>Selisih</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modalconfdraft" role="dialog" aria-hidden="true" aria-labelledby="modalAdd">
    <div class="modal-dialog modal-lg">
        <form class="form-horizontal" id="form_detail">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">Konfirmasi Draft Pemesanan</h3>
                    </div>
                    <div class="block-content">
                        <input type="hidden" id="rowIndex">
                        <input type="hidden" id="idbarang">
						<div class="table-responsive">
                        <table class="table table-bordered" id="temppemesanandraft">
                            <thead>
                                <tr>
                                    <th>Tipe</th>
                                    <th>Barang</th>
                                    <th>Kuantitas</th>
                                    <th>Harga</th>
                                    <th>Konversi Satuan</th>
                                    <th hidden>idtipe</th>
                                    <th hidden>idbarang</th>
                                    <th hidden>konversisatuan</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                        <textarea hidden id="detailbarangdraft" name="detailbarangdraft"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" id="tutup" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-sm btn-warning" type="button" id="saveconfdraft2">Tidak Dikonversi</button>
                    <button class="btn btn-sm btn-success" type="button" id="saveconfdraft1">Konversi Satuan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal in" id="modal_list_distributor" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="block block-themed block-transparent remove-margin-b">
            <div class="block-header bg-success">
                <h3 class="block-title">Pencarian Distributor</h3>
            </div>
            <div class="block-content">
				<div class="table-responsive">
                <table class="table table-bordered" id="tabel_distributor">
                    <thead>
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Telepon</th>
                        <th>Aksi</th>
                        <th hidden>id</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>
</div>
<div class="modal fade in black-overlay" id="modal_user" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<input type="hidden" class="form-control" id="idrka" placeholder="" name="idrka" value="">
						<div class="col-md-12">
							<div class="table-responsive">
							<table width="100%" id="tabel_user_proses" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Step</th>
										<th>User</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						</div>
						
					</div>
					<?php echo form_close() ?>
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
			
		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_approval" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<input type="hidden" class="form-control" id="id_setuju" placeholder="" name="id_setuju" value="">
						<div class="col-md-12">
							<div class="table-responsive">
							<table width="100%" id="tabel_user" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Step</th>
										<th>User</th>
										<th>Jika Setuju</th>
										<th>Jika Menolak</th>								
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						</div>
						
					</div>
					<?php echo form_close() ?>
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-primary" id="btn_simpan_approval" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Proses Persetujuan</button>
			</div>
			
		</div>
	</div>
</div>
<!-- Page JS Code -->
<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">

var table;
var t;
var option;

function get_option(){
    $.ajax({
        dataType: "json",
        async: false, 
        url: '{site_url}tgudang_pemesanan/select_tipe',
        success: function(res) {
            option = res;
        }
    });
    return option;
}
$(document).ready(function(){
	$("#btn_cari").click(function() {
            $("#modal_edit").modal("hide");
           
			var ke_unit='0';
			var tipe_id=$("#idtipe").val();
			// alert(tipe_id);
			// var idbarang=$("#idbarang").val();
			if (tipe_id){
				 $("#modal-list-barang").modal("show");
            table = $('#list-barang').DataTable({
                destroy: true,
                serverSide: true,
                searching: true,
                sort: false,
                autoWidth: false,
				"processing": true,
                ajax: {
                    url: '{site_url}tunit_permintaan/view_barang/',
                    type: 'post',
                    data: {idtipe: tipe_id, idunitpelayanan: ke_unit}
                },
                columns: [
                    {data: 'id', searchable: false},
                    {data: 'namatipe'},
                    {data: 'kode'},
                    {data: 'nama'},
                    {data: 'hargabeli',render: $.fn.dataTable.render.number(',', '.', 0, '')},
                    {data: 'stok'},
                    {defaultContent: '<a href="#" class="btn btn-xs btn-success pilih">Pilih</a>', searchable: false },
                    {data: 'id', visible: false},
                ]
            })
            setTimeout(function() {
                $('div.dataTables_filter input').focus()
            }, 500);
        }
		
    });
		
   
});
$("#btn_cari_distributor").click(function() {
	// $("#modal_pemesanan").modal("hide");
	$("#modal_list_distributor").modal("show");
});
$('#tabel_distributor tbody').on('click', 'a', function(){
        tr = table.row( $(this).parents('tr') ).index()

        var data = { 
            id: table.cell(tr,0).data(), 
            text: table.cell(tr,1).data()
        };
        var newOption = new Option(data.text, data.id, true, true);
        $('select[name=iddistributor]').append(newOption).trigger('change');
        // $('#margin').val(data.margin);
        // $('#harga').val(data.harga);

        $('#modal_list_distributor').modal('hide')
    })
$('.modal_list_distributor').on('click', function(){
        $('select[name=iddistributor]').val('').trigger('change');
        // var idtipe = $('select[name=idtipe]').val();
		var idunitpelayanan='';
        // var idunitpelayanan = $('select[name=dariunit]').val();
        // if(idtipe !== '') {
            table = $('#tabel_distributor').DataTable({
                destroy: true,
                serverSide: true,
                searching: true,
                sort: false,
                autoWidth: false,
                ajax: {
                    url: '{site_url}tunit_permintaan/filter_distributor/',
                    type: 'post',
                    data: {idunitpelayanan: idunitpelayanan}
                },
                columns: [
                    {data: 'id'},
                    {data: 'nama'},
                    {data: 'alamat'},
                    {data: 'telepon'},
                    {defaultContent: '<a href="#" class="btn btn-xs btn-default">Pilih</a>', searchable: false, },
                    {data: 'id', visible: false},
                ]
            })
            setTimeout(function() {
                $('div.dataTables_filter input').focus()
            }, 500);
        // }
    })
$('.angka').keydown(function(event){
		keys = event.keyCode;
		// Untuk: backspace, delete, tab, escape, and enter
		if (event.keyCode == 116 || event.keyCode == 46 || event.keyCode == 188 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
			 // Untuk: Ctrl+A
			(event.keyCode == 65 && event.ctrlKey === true) ||
			 // Untuk: home, end, left, right
			(event.keyCode >= 35 && event.keyCode <= 39) || event.keyCode == 190 || event.keyCode == 110|| event.keyCode == 188) {
				 // melanjutkan untuk memunculkan angka
				return;
		}
		else {
			// jika bukan angka maka tidak terjadi apa-apa
			if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault();
			}
		}

	});
$('#kuantitas').keyup(function(event){
	konversi_satuan();
});

$(document).on('click','#addPemesanan', function(){
    $('#simpan').text('Simpan').removeClass('btn-warning').addClass('btn-success')
    $('#modalAdd .block-title').text('Tambah Pemesanan')
    $('#tambahBarang').text('Tambahkan').removeClass('btn-warning').addClass('btn-success')

    table = $('#list-barang').DataTable().destroy()

    var option      = get_option();
    var dataoption  = '<option value="">Pilih Opsi</option>';
    $.each(option, function(i,item){
        dataoption += '<option value="'+i+'">'+item+'</option>';
    })    

    $('#opsisatuan').select2({
        placeholder: 'Select an Option',
        data: [
            {id: '', text: ''},
            {id: '1', text: 'Kecil'},
            {id: '2', text: 'Besar'},
        ]
    }).val('').trigger('change')

    $('#namabarang,#hargabeli,#kuantitas').val('')
    $('#jml_besar,#kuantitas_kecil').val('0')
    $('#idtipe').empty().append('#idtipe',dataoption).selectpicker('refresh')
    $('#iddistributor').empty().selectpicker('refresh')
    $('#opsisatuan').val('').trigger('change')
    clearTempPemesanan()
});
$(document).on('change','#idtipe', function(){

    var n = $('#idtipe').val();
    var idunitpelayanan = '0';
	$("#c_idbarang").val(null).trigger('change');
	$("#c_idbarang").select2({
            minimumInputLength: 0,
            noResults: 'Barang tidak ditemukan.',          
            allowClear: true,
            ajax: {
                url: '{site_url}tunit_permintaan/selectbarang_edit/', 
                dataType: 'json', 
                type: 'post', 
                quietMillis: 50, 
                data: function (params) {
                    var query = { search: params.term, idtipe: n, idunitpelayanan: idunitpelayanan } 
                    return query; 
                }, 
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
							// alert(data);
							// console.log(item);
                            return { id: item.id, text: item.nama }
                        })
                    };
                }
            }
        }); 
    

})

$(document).on('click','#btnBarangBack', function(){
    table = $('#list-barang').DataTable().destroy()
    $('#modalAdd').show();
    $('#modal-list-barang').hide()

    clearForm();
})
$('#list-barang tbody').on('click', 'a', function(){
        tr = table.row( $(this).parents('tr') ).index()
		// alert('sini');
        var data = { 
            id: table.cell(tr,0).data(), 
            text: table.cell(tr,3).data()
        };
        var newOption = new Option(data.text, data.id, true, true);
        $('#namabarang').val(table.cell(tr,3).data());        
        $('#c_idbarang').append(newOption).trigger('change');        
        $('#modal-list-barang').modal('hide');
});

$("#c_idbarang").change(function(){
	var ke_unit='0';
	var tipe_id=$("#idtipe").val();
	var idbarang=$(this).val();
	
	$.ajax({
		url: '{site_url}tunit_permintaan/selected_barang_harga/',
		dataType: "JSON",
		method: "POST",
		data : {idtipe: tipe_id, idunitpelayanan: ke_unit, idbarang: idbarang},
		success: function(data) {
			console.log(data);
			if (data != null){
				$("#c_harga_b").val(data.hargabeli_besar);
				$("#c_harga_k").val(data.hargabeli);
				$("#jml_besar").val(data.jumlahsatuanbesar);
				if ($("#opsisatuan").val()=='1'){
					$("#hargabeli").val($("#c_harga_k").val());
				}else{
					$("#hargabeli").val($("#c_harga_b").val());
				}
			}
		}
	});
	$("#namabarang").val($("#c_idbarang option:selected").text());
	$("#iddistributor").val(null).trigger('change');
	$('select[name=iddistributor]').select2({
		placeholder: 'Cari Data ... ',
		allowClear: true,
		ajax: {
			url: '{site_url}tgudang_pemesanan/getsorted_distributor_2/',
			dataType: 'json',
			delay: 500,
			type: 'post', 
			quietMillis: 50, 
			data: function (params) {
				var query = { search: params.term, idtipe: tipe_id, idbarang: idbarang } 
				return query; 
			}, 
			processResults: function(data) {
				return {
					results: data
				}
			},
			cache: false,
		}
	});  
	konversi_satuan();
});

$(document).on( 'change', '#opsisatuan', function(){
    // get harga beli
    var n = $(this).val(); 
    if (n=='1'){
		$("#hargabeli").val($("#c_harga_k").val());
	}else{
		$("#hargabeli").val($("#c_harga_b").val());
	}
	konversi_satuan();
});

$(document).on( 'click', '#tambahBarang', function(){
	// alert($("#iddistributor").val());
    t = $('#tempPemesanan').DataTable({
        paging: false,
        info: false,
        sort: false,
        searching: false,
        destroy: true,
        autoWidth: false,
		"processing": true,
        columnDefs: [{ "targets": [6,7,8,10,11,12,13], "visible": false }]
    })

    if(validate()) {
        if($('#tambahBarang').text() == 'Tambahkan') {
            t.row.add([
                $('#idtipe :selected').text(),
                $('#namabarang').val(),
                $('#c_iddistributor :selected').text(),
                $('#hargabeli').val(),
                $('#kuantitas').val(),
                $('#opsisatuan :selected').text(),
                $('#idtipe :selected').val(),
                $('#c_iddistributor').val(),
                $('#c_idbarang').val(),
                "<a href='#' class='on-default detailEdit' title='Edit'><i class='fa fa-pencil'></i></a>&nbsp;"+
                "<a href='#' class='on-default detailRemove' title='Hapus'><i class='fa fa-trash-o'></i></a>",
                $('#opsisatuan :selected').val(),
                $('#hargaterakhir').val(),
                $('#stokterakhir').val(),
                $('#kuantitas_kecil').val()
            ]).draw(false)
            clearForm();
            $.toaster({priority : 'success', title : 'Sukses!', message : 'Data berhasil ditambahkan'});
        } else {
            t.row( $('#rowIndex').val() ).data([
                $('#idtipe :selected').text(),
                $('#namabarang').val(),
                $('#c_iddistributor :selected').text(),
                $('#hargabeli').val(),
                $('#kuantitas').val(), //4
                $('#opsisatuan :selected').text(),
                $('#idtipe :selected').val(),
                $('#c_iddistributor').val(),
                $('#c_idbarang').val(),
                "<a href='#' class='on-default detailEdit' title='Edit'><i class='fa fa-pencil'></i></a>&nbsp;"+
                "<a href='#' class='on-default detailRemove' title='Hapus'><i class='fa fa-trash-o'></i></a>",
                $('#opsisatuan :selected').val(),
                $('#hargaterakhir').val(),
                $('#stokterakhir').val(),
                $('#kuantitas_kecil').val()
            ]).draw(false)
            clearForm()
            $.toaster({priority : 'success', title : 'Sukses!', message : 'Data berhasil diperbaharui'});
        }
        $('#tambahBarang').text('Tambahkan').removeClass('btn-warning').addClass('btn-success')
        return true;
    } else {
        return false;
    }

})

$('#tempPemesanan tbody').on( 'click', '.detailRemove', function () {
    t = $('#tempPemesanan').DataTable({
        paging: false,
        info: false,
        sort: false,
        searching: false,
        destroy: true,
        autoWidth: false,
        columnDefs: [{ "targets": [ 6,7,8,10,11,12,13], "visible": false }]
    })
    t.row($(this).parents('tr')).remove().draw()

    $('#tambahBarang').text('Tambahkan').removeClass('btn-warning').addClass('btn-success')
    table = $('#list-barang').DataTable().destroy()
    var option      = get_option();
    var dataoption  = '<option value="">Pilih Opsi</option>';
    $.each(option, function(i,item){
        dataoption += '<option value="'+i+'">'+item+'</option>';
    })

    $('#namabarang,#hargabeli,#kuantitas').val('')
    $('#idtipe').empty().append('#idtipe',dataoption).selectpicker('refresh')
    $('#iddistributor').empty().selectpicker('refresh')

    $.toaster({priority : 'success', title : 'Sukses!', message : 'Data berhasil dihapus'});
})


$('#tempPemesanan tbody').on( 'click', 'a.detailEdit', function () {
    $('#tambahBarang').text('Sunting').removeClass('btn-success').addClass('btn-warning')
    t = $('#tempPemesanan').DataTable({
        paging: false,
        info: false,
        sort: false,
        searching: false,
        destroy: true,
        autoWidth: false,
        columnDefs: [{ "targets": [ 6,7,8,10,11,12,13], "visible": false }]
    })

    var rowIndex = t.row($(this).parents('tr')).index()
    var option      = get_option();
    var dataoption  = '<option value="">Pilih Opsi</option>';
    $.each(option, function(i,item){
        dataoption += '<option value="'+i+'">'+item+'</option>';
    })

    $('#rowIndex').val( rowIndex )
    $('#namabarang').val( t.cell(rowIndex,1).data() )
    $('#hargabeli').val( t.cell(rowIndex,3).data() )
    $('#kuantitas').val( t.cell(rowIndex,4).data() )
    $('#idtipe').empty().append('#idtipe',dataoption).val( t.cell(rowIndex,6).data() ).selectpicker('refresh')
    $('#idbarang').val(t.cell(rowIndex,8).data())
    $('#opsisatuan').val(t.cell(rowIndex,10).data()).trigger('change')
    // distributor
    $.ajax({
        url: '{site_url}tgudang_pemesanan/get_distributor/',
        method:'get',
        data:{
            idbarang: $('#idbarang').val()
        },
        success: function(data) {
            $('#iddistributor').empty()
            setTimeout(function() {
                $('#iddistributor').append(this,data).val( t.cell(rowIndex,7).data() ).selectpicker('refresh')
            }, 50);
        }
    })
})

$(document).on( 'click', '#simpan', function () {
    t = $('#tempPemesanan').DataTable({
        paging: false,
        info: false,
        sort: false,
        searching: false,
        destroy: true,
        autoWidth: false,
        columnDefs: [{ "targets": [6,7,8,10,11,12,13], "visible": false }],
        stateSave: true,
    })

    if( t.rows().count() !== 0) {
        var arr = t.rows().data().toArray()
        var idPemesanan = $('#idpemesanan').val()

        if($('#simpan').text() == 'Update') {
            $.ajax({
                url: '{site_url}tgudang_pemesanan/update',
                data: {detailValue: arr, idpemesanan: idPemesanan},
                dataType: 'text',
                type: 'POST',
            })
            $('#simpan').html('<i class="fa fa-sun-o fa-spin"></i> Update').attr('disabled',true)
        } else {
            $.ajax({
                url: '{site_url}tgudang_pemesanan/save',
                data: {detailValue: arr},
                dataType: 'text',
                type: 'POST',
            })
            $('#simpan').html('<i class="fa fa-sun-o fa-spin"></i> Simpan').attr('disabled',true)
        }
        setTimeout(function() {
            $('#simpan').text('Simpan').attr('disabled',false)
            swal('Sukses','Data berhasil disimpan!','success')
            $('#tutup').click()
            $('#index_list').DataTable().ajax.reload()
            setTimeout(function() { window.location = '{site_url}tgudang_pemesanan' }, 1000);
        }, 1000)
    } else {
        swal('Kesalahan','Belum ada data ditambahkan!','warning')
    }
})



$('#index_list tbody').on('click','a.confirmAcc', function(){
    table = $('#index_list').DataTable()
    var tr = $(this).parents('tr')
    var idPemesanan = table.cell(tr,0).data()
    setTimeout(function() {
        swal({
            text: 'Apakah yakin akan mengkonfirmasi pemesanan?',
            showCancelButton: true,
            confirmButtonColor: '#46C379',
            confirmButtonText: 'Ya, Konfirmasi pemesanan!',
            html: false,
            preConfirm: function() {
                return new Promise(function (resolve) {
                    setTimeout(function() { resolve() }, 50);
                })
            }
        }).then(
            function (result) {
                $.ajax({url: '{site_url}tgudang_pemesanan/acc/'+idPemesanan})
                table.ajax.reload()
                swal('Berhasil!', 'Data telah disimpan.', 'success');
                setTimeout(function() { window.location = '{site_url}tgudang_pemesanan' }, 1000);
            }
        )
    }, 300);
})

$('#index_list tbody').on('click','a.confirmCancel', function(){
    table = $('#index_list').DataTable()
    var tr = $(this).parents('tr')
    var idPemesanan = table.cell(tr,0).data()
	// alert(idPemesanan);
    setTimeout(function() {
        swal({
            text: 'Apakah yakin akan membatalkan pemesanan?',
            showCancelButton: true,
            confirmButtonColor: '#d26a5c',
            confirmButtonText: 'Ya, batalkan pemesanan!',
            html: false,
            preConfirm: function() {
              return new Promise(function (resolve) {
                setTimeout(function() { resolve() }, 50);
              })
            }
        }).then(
            function (result) {
                $.ajax({url: '{site_url}tgudang_pemesanan/cancel/'+idPemesanan})
                table.ajax.reload()
                swal('Berhasil!', 'Data telah dibatalkan.', 'success');
                setTimeout(function() { window.location = '{site_url}tgudang_pemesanan' }, 1000);
            }
        )
    }, 300);
})


$('#index_list tbody').on('click','a.confirmdraft', function(){

    // $('#modalconfdraft').modal('show')

    table = $('#index_list').DataTable()
    var tr = $(this).parents('tr')
    var idpemesanan = table.cell(tr,0).data()
    window.location = '{site_url}tgudang_pemesanan/konfirmasi_pemesanan_draft/'+idpemesanan;

})

$('#temppemesanandraft tbody').on('click','a.detailEdit', function(){
    alert(1)
})

function detailbarangdraft() {
    var t = $('#temppemesanandraft').DataTable();
    var arr = t.rows().data().toArray();
    $('#detailbarangdraft').val(JSON.stringify(arr));
}

$('#index_list tbody').on('click','a.view', function(){
    table = $('#index_list').DataTable()
    var tr = $(this).parents('tr')
    var idPemesanan = table.cell(tr,0).data()
	// alert(idPemesanan);
    // get status
    var statuspemesanan = table.cell(tr,7).data()
    if(statuspemesanan == 1) $('#status').html('<b>Status :</b> Pesanan belum dikonfirmasi')
    else if(statuspemesanan == 2) $('#status').html('<b>Status :</b> Pesanan sudah dikonfirmasi')
    else if(statuspemesanan == 3) $('#status').html('<b>Status :</b> Pesanan dalam proses penerimaan')
    else if(statuspemesanan == 4) $('#status').html('<b>Status :</b> Pesanan sudah selesai')
    else if(statuspemesanan == 5) $('#status').html('<b>Status :</b> Pesanan dibatalkan')

    var t = $('#detailPemesanan').DataTable({
        paging: false,
        info: false,
        sort: false,
        searching: false,
        destroy: true,
        autoWidth: false,
        ajax: {
            url: '{site_url}tgudang_pemesanan/viewDetail',
            data: {idpemesanan: idPemesanan},
            type: 'POST',
        },
        columns: [
            {data: "idtipe"},
            {data: "idbarang"},
            {data: "kuantitas"},
            {data: "harga"},
            {data: "kuantitas"},
            {data: "harga"},
            {data: "status"},
            {data: "status"},
        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var arr  = [null,'Alkes','Implan','Obat','Logistik'];
            $('td:eq(0)', nRow).text(arr[aData['idtipe']])

            // get nama barang
            $.ajax({
                url: '{site_url}tgudang_pemesanan/getNamaBarang',
                data: {idtipe: aData['idtipe'], idbarang: aData['idbarang']},
                type: 'POST',
                success: function(res) {
                    $('td:eq(1)', nRow).text(res)
                }
            })

            // get satuan barang
            $.ajax({
                url: '{site_url}tgudang_pemesanan/getSatuanBarang',
                data: {idtipe: aData['idtipe'], idbarang: aData['idbarang']},
                type: 'POST',
                success: function(res) {
                    $('td:eq(2)', nRow).text(res)
                }
            })

            // get jumlah pesan
            $.ajax({
                url: '{site_url}tgudang_pemesanan/getAnyQty',
                data: {idpemesanan: idPemesanan, idtipe: aData['idtipe'], idbarang: aData['idbarang']},
                type: 'POST',
                dataType: 'json',
                success: function(res) {
                    var jmlterima = 0;
                    if(res.jmlterima !== null) {
                        jmlterima = res.jmlterima;
                    }
                    $('td:eq(4)', nRow).text(res.jmlpesan)
                    $('td:eq(5)', nRow).text(jmlterima)
                    $('td:eq(6)', nRow).text(res.selisih)
                    if(res.selisih == 0) {
                        $('td:eq(7)', nRow).html('<span class="label label-success">Selesai</span>')
                    } else {
                        $('td:eq(7)', nRow).html('<span class="label label-warning">Proses</span>')
                    }
                }
            })
            return nRow;
        }
    })
})
$('#index_list tbody').on('click','a.edit', function(){

    $('#opsisatuan').select2({
        placeholder: 'Select an Option',
        data: [
            {id: '', text: ''},
            {id: '1', text: 'Kecil'},
            {id: '2', text: 'Besar'},
        ]
    }).val('').trigger('change')	

    table = $('#index_list').DataTable()
    var tr = $(this).parents('tr')
    var idPemesanan = table.cell(tr,0).data()
    var distributor = table.cell(tr,3).data()

    $('#tambahBarang').text('Tambahkan').removeClass('btn-warning').addClass('btn-success')
    table = $('#list-barang').DataTable().destroy()
    var option      = get_option();
    var dataoption  = '<option value="">Pilih Opsi</option>';
    $.each(option, function(i,item){
        dataoption += '<option value="'+i+'">'+item+'</option>';
    })

    $('#namabarang,#hargabeli,#kuantitas').val('')
    $('#idtipe').empty().append('#idtipe',dataoption).selectpicker('refresh')
    $('#iddistributor').empty().selectpicker('refresh')


    $('#simpan').text('Update').removeClass('btn-success').addClass('btn-warning')
    $('#modalAdd .block-title').text('Edit Pemesanan')
    $('#idpemesanan').remove()
    $('#form_detail').append('<input type="hidden" name="idpemesanan" id="idpemesanan" value="'+idPemesanan+'">')
    clearTempPemesanan()

    t = $('#tempPemesanan').DataTable({
        paging: false,
        info: false,
        serverSide: true,
        sort: false,
        searching: false,
        destroy: true,
        autoWidth: false,
        ajax: {
            url: '{site_url}tgudang_pemesanan/editPemesanan',
            data: {idpemesanan: idPemesanan},
            type: 'POST',
        },
        columns: [
            {data: "idtipe"},
            {data: "idbarang"},
            {data: "iddistributor"},
            {data: "harga"},
            {data: "kuantitas"},
            {
                data: "opsisatuan",
                render: function(data) {
                    if(data == 1) return 'Kecil';
                    else return 'Besar';
                }
            },
            {data: "idtipe"},
            {data: "iddistributor"},
            {data: "idbarang"},
            {defaultContent: "-"},
            {data: "opsisatuan"},
        ],
        columnDefs: [{ "targets": [ 6,7,8,10,11,12,13], "visible": false }],
        fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

        	// if(aData['satuan'] == 1) $('td:eq(5)', nRow).text('Kecil');
        	// else $('td:eq(5)', nRow).text('Besar');

            var arr  = [null,'Alkes','Implan','Obat','Logistik'];
            $('td:eq(0)', nRow).text(arr[aData['idtipe']])
            $.ajax({
                url: '{site_url}tgudang_pemesanan/getNamaBarang',
                data: {idtipe: aData['idtipe'], idbarang: aData['idbarang']},
                type: 'POST',
                success: function(res) {
                    $('td:eq(1)', nRow).text(res)
                }
            })
            $('td:eq(2)', nRow).text(distributor)
            var aksi =  "<a href='#' class='on-default detailEdit' title='Edit'><i class='fa fa-pencil'></i></a>&nbsp;"+
                        "<a href='#' class='on-default detailRemove' title='Hapus'><i class='fa fa-trash-o'></i></a>";
            $('td:eq(6)', nRow).html(aksi)
            return nRow;
        }
    })
})

$(document).ready(function(){
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: false,
            pageLength: 50,
            serverSide: true,
            "order": [],
			"processing": true,
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0], "visible": false },
						 {"targets": [2], className: "text-left" },
						 {"targets": [7], className: "text-left" },
						 {"targets": [8], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}tgudang_pemesanan/get_index', 
                type: "POST" ,
                dataType: 'json'
            }
        });

});
// $(document).ready(function(){

    // table = $('#index_list').DataTable({
        // "pageLength": 50,
        // autoWidth: false,
        // serverSide: true,
        // "order": [[8,'asc'],[0,'desc']],
        // "ajax": { "url": '{site_url}tgudang_pemesanan/ajax_list', "type": "POST" },
        // "columns": [
            // {data: "id", visible: false},
            // {
                // data: "nopemesanan", width: '10%',
                // render: function(data,type,row) {
                    // if(row.tipepemesanan == 1) {
                        // return '<label class="label label-default">'+data+'</label>';
                    // } else {
                        // return '<label class="label label-info">'+data+'</label>';
                    // }
                // }
            // },
            // {data: "tanggal", width: '15%'},
            // {data: "nama"},
            // {data: "totalbarang", sorting: false, "render": $.fn.DataTable.render.number('.',','), width: '5%'},
            // {data: "totalharga", sorting: false, "render": $.fn.DataTable.render.number('.',','), width: '5%'},
            // {data: "totalharga", sorting: false, "render": $.fn.DataTable.render.number('.',','), width: '5%'},
            // {data: "totalharga", sorting: false, "render": $.fn.DataTable.render.number('.',','), width: '5%'},
            // {data: "status"},
            // {data: "action", width: '15%', sorting: false, className: 'text-right'},
        // ],
        // "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

            // var action = '<div class="btn-group" data-toggle="buttons">';
            // action += '<a class="view btn btn-xs btn-info" type="button" data-target="#modalDetailPemesanan" data-toggle="modal"><i class="fa fa-eye"></i></a>';
            // <?php if(button_roles('tgudang_pemesanan/add')): ?>
                // if(aData['status'] == 1 && aData['stdraft'] == 0) {
                    // action += '<a class="edit btn btn-xs btn-default" data-target="#modalAdd" data-toggle="modal"><i class="fa fa-pencil"></i></a>';
                    // action += '<a class="confirmAcc btn btn-xs btn-success" type="button"><i class="fa fa-check"></i></a>';
                // }
            // <?php endif ?>

            // <?php if(button_roles('tgudang_pemesanan/acc')): ?>
                // if(aData['status'] == 1 || aData['status'] == 2 && aData['stdraft'] == 0) {
                    // action += '<a class="confirmCancel btn btn-xs btn-danger" type="button"><i class="fa fa-times"></i></a>';
                // }
            // <?php endif ?>
            // <?php if(button_roles('tgudang_pemesanan/cancel')): ?>
                // if(aData['status'] == 1 && aData['stdraft'] == 1) {
                    // action += '<a class="confirmdraft btn btn-xs btn-warning" type="button"><i class="fa fa-hand-o-left"></i></a>';
                // }
            // <?php endif ?>
            // <?php if(button_roles('tgudang_pemesanan/print_po')): ?>
                // action += '<a onclick="window.open( \'{site_url}tgudang_pemesanan/print_po/'+aData['id']+'\', \'_blank\' );" class="btn btn-xs btn-default"><i class="fa fa-print"></i></a>';
            // <?php endif ?>

            // action += '</div>';

            // if(aData['status'] == 1 && aData['stdraft'] == 0){
                // $("td:eq(5)", nRow).html(aData['totalbarang']);
                // $("td:eq(6)", nRow).html(0);
                // $("td:eq(7)", nRow).html('<span class="label label-default">PENDING</span>');
                // $("td:eq(8)", nRow).html(action);
            // }else if(aData['status'] == 2) {
                // $("td:eq(5)", nRow).html(aData['totalbarang']);
                // $("td:eq(6)", nRow).html(0);
                // $("td:eq(7)", nRow).html('<span class="label label-info">SUDAH DIPESAN</span>');
                // $("td:eq(8)", nRow).html(action);
            // } else if(aData['status'] == 3) {
                // $.ajax({
                    // url: '{site_url}tgudang_pemesanan/getJmlPesanTerima',
                    // data: {idpemesanan: aData['id']},
                    // type: 'POST',
                    // dataType: 'json',
                    // success: function(res) {
                        // $('td:eq(5)', nRow).text(aData['totalbarang'])
                        // $('td:eq(6)', nRow).text(res.jmlterima)
                    // },
                // })
                // $("td:eq(7)", nRow).html('<span class="label label-warning">SUDAH DIPESAN</span>');
                // $("td:eq(8)", nRow).html(action);
            // } else if(aData['status'] == 4) {
                // $("td:eq(5)", nRow).html(aData['totalbarang']);
                // $("td:eq(6)", nRow).html(aData['totalbarang']);
                // $("td:eq(7)", nRow).html('<span class="label label-success">SELESAI</span>');
                // $("td:eq(8)", nRow).html(action);
            // } else if(aData['status'] == 1 && aData['stdraft'] == 1) {
                // $("td:eq(5)", nRow).html(aData['totalbarang']);
                // $("td:eq(6)", nRow).html(0);
                // $("td:eq(7)", nRow).html('<span class="label label-info">DRAFT</span>');
                // $("td:eq(8)", nRow).html(action);
            // } else {
                // $("td:eq(5)", nRow).html(aData['totalbarang']);
                // $("td:eq(6)", nRow).html(0);
                // $("td:eq(7)", nRow).html('<span class="label label-danger">DIBATALKAN</span>');
                // $("td:eq(8)", nRow).html(action);
            // }
            // return nRow;
        // }
    // })
// })

function clearTempPemesanan() {
    t = $('#tempPemesanan').DataTable({
        paging: false,
        info: false,
        sort: false,
        searching: false,
        destroy: true,
        autoWidth: false,
        columnDefs: [{ "targets": [ 6,7,8,10,11,12,13], "visible": false }]
    })
    t.clear().draw()
}

function validate() {
    if($('#idtipe').val() == '') {
        $.toaster({priority : 'danger', title : 'Error!', message : 'Tipe tidak boleh kosong'});
        return false;
    }
    if($('#c_idbarang').val() == '') {
        $.toaster({priority : 'danger', title : 'Error!', message : 'Barang tidak boleh kosong'});
        return false;
    }
    if($('#hargabeli').val() == '') {
        $.toaster({priority : 'danger', title : 'Error!', message : 'Distributor tidak boleh kosong'});
        return false;
    }
    if($('#iddistributor').val() == '') {
        $.toaster({priority : 'danger', title : 'Error!', message : 'Distributor tidak boleh kosong'});
        return false;
    }
    if($('#kuantitas').val() == '') {
        $.toaster({priority : 'danger', title : 'Error!', message : 'Kuantitas tidak boleh kosong'});
        return false;
    }

    return true;
}

function clearForm() {
    // var option      = get_option();
    // var dataoption  = '<option value="">Pilih Opsi</option>';
    // $.each(option, function(i,item){
        // dataoption += '<option value="'+i+'">'+item+'</option>';
    // })

    $('#namabarang,#c_harga_k,#c_harga_b,#kuantitas,#hargabeli').val('')
    // $('#idtipe').empty().append('#idtipe',dataoption).selectpicker('refresh')
    // $('#iddistributor').empty().selectpicker('refresh')
    $('#c_idbarang').val(null).trigger('change');
    $('#c_iddistributor').val(null).trigger('change');
    $('#c_iddistributor').select2('data', null);
    $('#idtipe').val(null).trigger('change');
    setTimeout(function() {
	    $('#opsisatuan').val('').trigger('change')
    }, 100);
}
function konversi_satuan(){
	if ($("#opsisatuan").val()=='2'){
		$('#kuantitas_kecil').val(parseFloat($("#jml_besar").val())*parseFloat($("#kuantitas").val()));		
	}else{
		$('#kuantitas_kecil').val($("#kuantitas").val());
	}
}
function load_user($id){
	var id=$id;
	$("#modal_user").modal('show');
	
	$.ajax({
		url: '{site_url}tgudang_pemesanan/list_user/'+id,
		dataType: "json",
		success: function(data) {
			$("#tabel_user_proses tbody").empty();
			$("#tabel_user_proses tbody").append(data.detail);
			
		}
	});
}
function kirim($id){
	var id=$id;
	$("#modal_approval").modal('show');
	
	load_user_approval(id);
	
}
function load_user_approval($id){
	var id=$id;
	// alert(id)
	$('#btn_simpan_approval').attr('disabled', true);
	$("#id_setuju").val(id);
	$('#tabel_user').DataTable().destroy();
	table=$('#tabel_user').DataTable({
			"autoWidth": false,
			"pageLength": 10,
			"searching": false,
			"lengthChange": false,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}tgudang_approval/load_user_approval',
				type: "POST",
				dataType: 'json',
				data : {
					id:id,
											
				   }
			},
			"columnDefs": [
				 {  className: "text-right", targets:[0] },
				 {  className: "text-center", targets:[1,2,3] },
				 { "width": "10%", "targets": [0] },
				 { "width": "40%", "targets": [1] },
				 { "width": "25%", "targets": [2,3] },

			]
		}
	);
		setTimeout(function() {
			// if ($('#tabel_user').data().count()>0 ) {
				// // alert( 'Empty table' );
			// $('#btn_simpan_approval').attr('disabled', true)
				// }else{
			// $('#btn_simpan_approval').attr('disabled', false)
					
				// }
				// alert($('#tabel_user').data().count());
				var rowCount = table.data().rows().count();
				if (rowCount>0){
					$('#btn_simpan_approval').attr('disabled', false)
				}
				// alert(table.data().rows().count());
		}, 1000);

		
}
$(document).on("click","#btn_simpan_approval",function(){
	var id=$("#id_setuju").val();
	swal({
		title: "Anda Yakin ?",
		text : "Akan Kirim Ke User Persetujuan?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		table = $('#index_list').DataTable()	
		$.ajax({
			url: '{site_url}tgudang_pemesanan/konfirmasi/'+id,
			complete: function(data) {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Kirim Data'});
				table.ajax.reload( null, false ); 
			}
		});
	});
});
</script>
