<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-rounded block-bordered">
	<div class="block-header">
		<ul class="block-options">
			<li><a href="{site_url}tgudang_pemesanan" class="btn"><i class="fa fa-reply"></i></a></li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<?php echo form_open('tgudang_pemesanan/save','class="form-horizontal" id="form-work"') ?>
	<div class="block-content">
		<div class="row">
			<div class="col-sm-12">
				<div class="control-group">
					<div class="col-md-12">
						<div class="progress progress-mini">
							<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
						</div>
						<table class="table table-striped table-bordered" id="detail_list" style="margin-bottom:0px">
							<thead>
								<tr>
									<th>Tipe</th>
									<th>Barang</th>
									<th>Distributor</th>
									<th>Harga Beli</th>
									<th>Kuantitas</th>
									<th>Actions</th>
									<th hidden>idtipe</th>
									<th hidden>kodebarang</th>
									<th hidden>iddistributor</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
						<table class="table table-striped table-bordered">
							<tbody>
								<tr>
									<td colspan="2" class="hidden-phone">
										<button class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal-detail-pemesanan" data-backdrop="static" id="detail-pemesanan-add" type="button">Tambah Pemesanan</button>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="text-right bg-light lter">
							<button class="btn btn-info" type="submit">Simpan</button>
							<a href="{site_url}tgudang_pemesanan" class="btn btn-default">Batal</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" id="detail_value" name="detail_value" value="[]">
		<br><br>
		<?php echo form_hidden('id', $id); ?>
		<?php echo form_close() ?>
	</div>
</div>
<button hidden id="success_msg" class="js-notify" data-notify-type="success" data-notify-message="Success!">Success</button>
<button hidden id="error_msg" class="js-notify" data-notify-type="danger" data-notify-message="Error!">Danger</button>


<div class="modal in" id="modal-detail-pemesanan" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Tambah Detail Pemesanan Gudang</h3>
                </div>
                <div class="block-content">
                	<form class="form-horizontal" id="form_detail">
                		<input type="hidden" id="idbarang">

                		<div class="form-group c" id="f_idtipe">
                			<label class="col-md-4 control-label">Tipe</label>
                			<div class="col-md-6">
                				<select id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.."></select>
                			</div>
                		</div>
						<div class="form-group c" id="f_namabarang">
							<label class="col-md-4 control-label" for="namabarang">Nama Barang</label>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-edit"></i></span>
									<input readonly type="text" class="form-control" id="namabarang" placeholder="Nama Barang" name="namabarang" value="" required="" aria-required="true">
								</div>
							</div>
						</div>
						<div class="form-group c" id="f_hargabeli">
							<label class="col-md-4 control-label" for="hargabeli">Harga Beli</label>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-edit"></i></span>
									<input readonly type="text" class="form-control" id="hargabeli" placeholder="Harga Beli" name="hargabeli" value="" required="" aria-required="true">
								</div>
							</div>
						</div>
						<div class="form-group c" id="f_iddistributor">
                			<label class="col-md-4 control-label">Distributor</label>
                			<div class="col-md-6">
                				<select id="iddistributor" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.."></select>
                			</div>
                		</div>
						<div class="form-group c" id="f_kuantitas">
							<label class="col-md-4 control-label" for="kuantitas">Kuantitas</label>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-edit"></i></span>
									<input type="text" class="form-control" id="kuantitas" placeholder="Kuantitas" name="kuantitas" value="" required="" aria-required="true">
								</div>
							</div>
						</div>
                	</form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                <button class="btn btn-sm btn-primary" id="submit-form_detail" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Ok</button>
            </div>
        </div>
    </div>
</div>

<div class="modal in" id="modal-list-barang" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">List Barang</h3>
                </div>
                <div class="block-content">
					<table class="table table-striped table-bordered" id="list-barang">
						<thead>
							<tr>
								<th>ID</th>
								<th>Kode</th>
								<th>Nama</th>
								<th>Harga</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>                	
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" id="btnback" type="button" data-dismiss="modal">Back</button>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/tgudang_pemesanan_manage.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>