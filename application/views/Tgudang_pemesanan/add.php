<form class="form-horizontal" id="form_detail">
	<input type="hidden" id="rowIndex">
	<div class="form-group"> 
		<label class="col-md-3 control-label">TIPE</label> 
		<div class="col-md-6"> 
			<select id="idtipe" name="idtipe" class="form-control" style="width: 100%;"></select> 
		</div> 
	</div>
	<div class="form-group"> 
		<label class="col-md-3 control-label">Barang</label> 
		<div class="col-md-4"> 
			<select id="idbarang" name="idbarang" class="form-control" style="width: 100%;" required></select> 
		</div>
		<div class="col-md-4"> 
			<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_cari_barang">Cari Barang</button>
		</div>
	</div>
	<div class="form-group"> 
		<label class="col-md-3 control-label">Kode</label> 
		<div class="col-md-6"> 
			<input readonly type="text" class="form-control" id="kodebarang" name="kodebarang" value="" required="">
		</div>
	</div>
</form>

<div class="modal in" id="modal_cari_barang" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">List Barang</h3>
                </div>
                <div class="block-content">
                    <table class="table table-bordered" id="list-barang">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Kode</th>
                                <th>Nama</th>
                                <th>Satuan</th>
                                <th>Harga</th>
                                <th>Diskon</th>
                                <th>Stok</th>
                                <th>Catatan</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" id="btnback" type="button" data-dismiss="modal">Back</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#modal .block-title').html('{title}');

		$('#idtipe').select2({
			placeholder: 'Select an Option',
			data: [
				{id: 'mdata_alkes', text: 'ALKES'},
				{id: 'mdata_implan', text: 'IMPLAN'},
				{id: 'mdata_obat', text: 'OBAT'},
				{id: 'mdata_logistik', text: 'LOGISTIK'},
			]
		}).val('{idtipe}').trigger('change');
		$('#idtipe').select2()
	})

	$(document).on('change','#idtipe',function(){
		var n = $(this).val();
        $("#idbarang").select2({
        	placeholder: 'Select an Option',
            minimumInputLength: 1,
            noResults: 'Barang tidak ditemukan.',
            ajax: {
                url: '{site_url}tgudang_pemesanan/get_barang/', 
                dataType: 'json', 
                type: 'post', 
                quietMillis: 50, 
                data: function (params) {
                    var query = { term: params.term, tipe: n} 
                    return query; 
                }, 
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return { id: item.id, text: item.text }
                        })
                    };
                }
            }
        });		
	})
</script>