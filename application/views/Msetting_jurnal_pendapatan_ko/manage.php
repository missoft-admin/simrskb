<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content" style="margin-top: -20px;font-size:">
		<?php echo form_open('msetting_jurnal_pendapatan_ko/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>	
		<?
		$q="SELECT *FROM mdata_tipebarang WHERE id IN(1,3)";
		$list_tipe=$this->db->query($q)->result();
		
		
		$q="SELECT *FROM mkelas WHERE status='1'";
		$list_kelas=$this->db->query($q)->result();
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="block block-themed ">
					<div class="block-header bg-primary">
						<ul class="block-options">

							<li>
								<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">SETTING DOKTER ANESTESI</h3>
					</div>
					
					<div class="block-content">						
						<input type="hidden" class="form-control" id="id_edit_da" placeholder="0" name="id_edit_da" value="">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Tipe Operasi</label>							
							<div class="col-md-4">
								<select name="idtipe_da" id="idtipe_da" style="width: 100%" class="js-select2 form-control input-sm idbarang">
									<option value="#" selected>- Semua Tipe -</option>		
									<option value="1">Rawat Inap</option>		
									<option value="2">ODS</option>		
									
								</select>	
							</div>
							<label class="col-md-2 control-label" >Group Penjualan</label>
							<div class="col-md-4">
								<select name="idgroup_penjualan_da" id="idgroup_penjualan_da" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
							
						</div>
						
						
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Kelas</label>
							<div class="col-md-4">
								<select name="idkelas_da" id="idkelas_da" style="width: 100%" class="js-select2 form-control input-sm">
									<option value="0" selected>- Semua Kelas -</option>	
									<?foreach($list_kelas as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>																				
									<?}?>	
								</select>
							</div>
							<label class="col-md-2 control-label" >Group Diskon</label>
							<div class="col-md-4">
								<select name="idgroup_diskon_da" id="idgroup_diskon_da" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
							
						</div>
						<div class="form-group">
							
							<label class="col-md-2 control-label" >Dokter</label>
							
							<div class="col-md-4">
								<select name="iddokter_da" id="iddokter_da" style="width: 100%" class="form-control input-sm iddokter"></select>
							</div>
							
							<label class="col-md-2 control-label" ></label>
							
							<div class="col-md-4">
								<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_da" title="Masukan Item"><i class="fa fa-save"></i> Simpan</button>
								<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_da" title="Batalkan"><i class="fa fa-refresh"></i> Clear Input</button>
							</div>
						</div>
						<div class="form-group">
							
							
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<table id="tabel_da" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 0%;">#</th>															
											<th style="width: 8%;">Tipe Operasi</th>															
											<th style="width: 8%;">Kelas</th>															
											<th style="width: 9%;">Dokter</th>															
											<th style="width: 12%;">Group Pembelian</th>															
											<th style="width: 12%;">Group Diskon</th>															
											<th style="width: 5%;">Actions</th>
										</tr>
										
									</thead>
									<tbody>
										
									</tbody>
									
							</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-md-12">
				<div class="block block-themed ">
					<div class="block-header bg-info">
						<ul class="block-options">

							<li>
								<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">SETTING ASISTEN ANESTESI</h3>
					</div>
					
					<div class="block-content">						
						<input type="hidden" class="form-control" id="id_edit_daa" placeholder="0" name="id_edit_daa" value="">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Tipe Operasi</label>							
							<div class="col-md-4">
								<select name="idtipe_daa" id="idtipe_daa" style="width: 100%" class="js-select2 form-control input-sm idbarang">
									<option value="#" selected>- Semua Tipe -</option>		
									<option value="1">Rawat Inap</option>		
									<option value="2">ODS</option>		
									
								</select>	
							</div>
							<label class="col-md-2 control-label" >Kelas</label>
							<div class="col-md-4">
								<select name="idkelas_daa" id="idkelas_daa" style="width: 100%" class="js-select2 form-control input-sm">
									<option value="0" selected>- Semua Kelas -</option>	
									<?foreach($list_kelas as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>																				
									<?}?>	
								</select>
							</div>
							
							
						</div>
						
						
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Tipe Anestesi</label>
							<div class="col-md-4">
								<select name="jenis_asisten_daa" id="jenis_asisten_daa" style="width: 100%" class="js-select2 form-control input-sm">
									<option value="0" selected>- Semua Tipe -</option>		
									<option value="1">Dokter</option>		
									<option value="2">Pegawai</option>		
								</select>
							</div>
							<label class="col-md-2 control-label" >Group Penjualan</label>
							<div class="col-md-4">
								<select name="idgroup_penjualan_daa" id="idgroup_penjualan_daa" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
							
							
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							
							<label class="col-md-2 control-label" >Dokter</label>							
							<div class="col-md-4">
								<select name="iddokter_daa" id="iddokter_daa" style="width: 100%" class="form-control input-sm iddokter"></select>
							</div>
							<label class="col-md-2 control-label" >Group Diskon</label>
							<div class="col-md-4">
								<select name="idgroup_diskon_daa" id="idgroup_diskon_daa" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>							
						</div>
						<div class="form-group">
							<label class="col-md-8 control-label" ></label>
							<div class="col-md-4">
								<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_daa" title="Masukan Item"><i class="fa fa-save"></i> Simpan</button>
								<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_daa" title="Batalkan"><i class="fa fa-refresh"></i> Clear Input</button>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<table id="tabel_daa" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 0%;">#</th>															
											<th style="width: 8%;">Tipe Operasi</th>															
											<th style="width: 8%;">Kelas</th>															
											<th style="width: 8%;">Tipe Asisten</th>															
											<th style="width: 9%;">Asisten</th>															
											<th style="width: 12%;">Group Pembelian</th>															
											<th style="width: 12%;">Group Diskon</th>															
											<th style="width: 5%;">Actions</th>
										</tr>
										
									</thead>
									<tbody>
										
									</tbody>
									
							</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="block block-themed ">
					<div class="block-header bg-success">
						<ul class="block-options">

							<li>
								<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">SETTING ASISTEN OPERATOR</h3>
					</div>
					
					<div class="block-content">						
						<input type="hidden" class="form-control" id="id_edit_dao" placeholder="0" name="id_edit_dao" value="">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Tipe Operasi</label>							
							<div class="col-md-4">
								<select name="idtipe_dao" id="idtipe_dao" style="width: 100%" class="js-select2 form-control input-sm idbarang">
									<option value="#" selected>- Semua Tipe -</option>		
									<option value="1">Rawat Inap</option>		
									<option value="2">ODS</option>		
									
								</select>	
							</div>
							<label class="col-md-2 control-label" >Kelas</label>
							<div class="col-md-4">
								<select name="idkelas_dao" id="idkelas_dao" style="width: 100%" class="js-select2 form-control input-sm">
									<option value="0" selected>- Semua Kelas -</option>	
									<?foreach($list_kelas as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>																				
									<?}?>	
								</select>
							</div>
							
							
						</div>
						
						
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Tipe Anestesi</label>
							<div class="col-md-4">
								<select name="jenis_asisten_dao" id="jenis_asisten_dao" style="width: 100%" class="js-select2 form-control input-sm">
									<option value="0" selected>- Semua Tipe -</option>		
									<option value="1">Dokter</option>		
									<option value="2">Pegawai</option>		
								</select>
							</div>
							<label class="col-md-2 control-label" >Group Penjualan</label>
							<div class="col-md-4">
								<select name="idgroup_penjualan_dao" id="idgroup_penjualan_dao" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
							
							
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							
							<label class="col-md-2 control-label" >Dokter</label>							
							<div class="col-md-4">
								<select name="iddokter_dao" id="iddokter_dao" style="width: 100%" class="form-control input-sm iddokter"></select>
							</div>
							<label class="col-md-2 control-label" >Group Diskon</label>
							<div class="col-md-4">
								<select name="idgroup_diskon_dao" id="idgroup_diskon_dao" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>							
						</div>
						<div class="form-group">
							<label class="col-md-8 control-label" ></label>
							<div class="col-md-4">
								<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_dao" title="Masukan Item"><i class="fa fa-save"></i> Simpan</button>
								<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_dao" title="Batalkan"><i class="fa fa-refresh"></i> Clear Input</button>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<table id="tabel_dao" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 0%;">#</th>															
											<th style="width: 8%;">Tipe Operasi</th>															
											<th style="width: 8%;">Kelas</th>															
											<th style="width: 8%;">Tipe Asisten</th>															
											<th style="width: 9%;">Asisten</th>															
											<th style="width: 12%;">Group Pembelian</th>															
											<th style="width: 12%;">Group Diskon</th>															
											<th style="width: 5%;">Actions</th>
										</tr>
										
									</thead>
									<tbody>
										
									</tbody>
									
							</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		list_group();
	
		load_dokter_anestesi();
		load_da();
		list_dokter_pegawai_anestesi();
		load_daa();
		list_dokter_pegawai_operator();
		load_dao();
	});	
	function load_dokter_anestesi(){
		var newOption = new Option('- Semua Dokter -', '0', false, false);
		$("#iddokter_da").append(newOption);
		$("#iddokter_da").val("0").trigger('change');
		$("#iddokter_da").select2({
			noResults: 'Dokter Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}tko/get_dokter_anestesi/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,
				  }
				  return query;
				},
				processResults: function (data) {
					// console.log(data);
					return {
						results: $.map(data, function (item) {
							return {
								text: item.nama,
								id: item.id,
								tipe : item.tipe
							}
						})
					};
				}
			}
		});

	}
	function list_dokter_pegawai_anestesi(){
		var newOption = new Option('- Semua Dokter -', '0', false, false);
		$("#iddokter_daa").append(newOption);
		$("#iddokter_daa").val("0").trigger('change');
		var jenis_asisten_daa=$("#jenis_asisten_daa").val();
		// alert(jenis_asisten_daa);
		if (jenis_asisten_daa !='0'){
			
		
			$("#iddokter_daa").select2({
				noResults: 'Dokter Tidak Ditemukan.',
				// allowClear: true
				// tags: [],
				ajax: {
					url: '{site_url}msetting_jurnal_pendapatan_ko/list_dokter_pegawai_anestesi/',
					dataType: 'json',
					type: "POST",
					quietMillis: 50,
				 data: function (params) {
					  var query = {
						search: params.term,jenis_asisten_daa:jenis_asisten_daa
					  }
					  return query;
					},
					processResults: function (data) {
						// console.log(data);
						return {
							results: $.map(data, function (item) {
								return {
									text: item.nama,
									id: item.id,
									tipe : item.tipe
								}
							})
						};
					}
				}
			});
		}else{
			$("#iddokter_daa").select2();
		}

	}
	function list_dokter_pegawai_operator(){
		var newOption = new Option('- Semua Dokter -', '0', false, false);
		$("#iddokter_dao").append(newOption);
		$("#iddokter_dao").val("0").trigger('change');
		var jenis_asisten_dao=$("#jenis_asisten_dao").val();
		// alert(jenis_asisten_daa);
		if (jenis_asisten_dao !='0'){
			
		
			$("#iddokter_dao").select2({
				noResults: 'Dokter Tidak Ditemukan.',
				// allowClear: true
				// tags: [],
				ajax: {
					url: '{site_url}msetting_jurnal_pendapatan_ko/list_dokter_pegawai_operator/',
					dataType: 'json',
					type: "POST",
					quietMillis: 50,
				 data: function (params) {
					  var query = {
						search: params.term,jenis_asisten_dao:jenis_asisten_dao
					  }
					  return query;
					},
					processResults: function (data) {
						// console.log(data);
						return {
							results: $.map(data, function (item) {
								return {
									text: item.nama,
									id: item.id,
									tipe : item.tipe
								}
							})
						};
					}
				}
			});
		}else{
			$("#iddokter_dao").select2();
		}

	}
	function list_group(){
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_general/list_akun_group',
			dataType: "json",
			success: function(data) {
				$(".idgroup").empty();
				$('.idgroup').append('<option value="#" selected>- Pilih Group -</option>');
				$('.idgroup').append(data.detail);
				
			}
		});		
	}
	
	
	function validate_add_da()
	{
			var idtipe_da=$("#idtipe_da").val();
			var idgroup_penjualan_da=$("#idgroup_penjualan_da").val();
			var idgroup_diskon_da=$("#idgroup_diskon_da").val();
			var idgroup_tuslah_da=$("#idgroup_tuslah_da").val();
		
					
			if (idgroup_penjualan_da=='#'){
				sweetAlert("Maaf...", "Group Penjualan Harus diisi", "error");
				return false;
			}
			if (idgroup_diskon_da=='#'){
				sweetAlert("Maaf...", "Group Diskon Harus diisi", "error");
				return false;
			}
			
			
		return true;
	}
	function clear_input_da(){
		$("#idtipe_da").val('#').trigger('change');	
		$("#idgroup_penjualan_da").val('#').trigger('change');
		$("#idgroup_diskon_da").val('#').trigger('change');
		
		$("#iddokter_da").val('0').trigger('change');
		$("#idkelas_da").val('0').trigger('change');
	}
	$(document).on("click","#simpan_da",function(){
		if (validate_add_da()==false)return false;
		var idkelas=$("#idkelas_da").val();
		var idtipe=$("#idtipe_da").val();
		var iddokter=$("#iddokter_da").val();
		var idgroup_penjualan=$("#idgroup_penjualan_da").val();
		var idgroup_diskon=$("#idgroup_diskon_da").val();
	
		
		// alert('SINI');
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_ko/simpan_da',
			type: 'POST',
			data: {
				idkelas:idkelas
				,idtipe:idtipe
				,iddokter:iddokter
				,idgroup_penjualan:idgroup_penjualan
				,idgroup_diskon:idgroup_diskon
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_da').DataTable().ajax.reload( null, false );
					clear_input_da();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	
	$(document).on("click","#clear_da",function(){		
		clear_input_da();
	});
	function load_da(){
		var jenis=1;
		var idlogic=$("#id").val();
		
		$('#tabel_da').DataTable().destroy();
		var table = $('#tabel_da').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pendapatan_ko/load_da/',
			type: "POST",
			dataType: 'json',
			data: {
				jenis: jenis
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
					]
		});
	}
	function hapus_da($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_pendapatan_ko/hapus_da/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_da').DataTable().ajax.reload( null, false );							
						
						clear_input_da();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	
	$(document).on("change","#jenis_asisten_daa",function(){
		list_dokter_pegawai_anestesi();
	});
	
	function validate_add_daa()
	{
			var idtipe_daa=$("#idtipe_daa").val();
			var idgroup_penjualan_daa=$("#idgroup_penjualan_daa").val();
			var idgroup_diskon_daa=$("#idgroup_diskon_daa").val();
			var idgroup_tuslah_daa=$("#idgroup_tuslah_daa").val();
		
					
			if (idgroup_penjualan_daa=='#'){
				sweetAlert("Maaf...", "Group Penjualan Harus diisi", "error");
				return false;
			}
			if (idgroup_diskon_daa=='#'){
				sweetAlert("Maaf...", "Group Diskon Harus diisi", "error");
				return false;
			}
			
			
		return true;
	}
	function clear_input_daa(){
		$("#idtipe_daa").val('#').trigger('change');	
		$("#jenis_asisten_daa").val('0').trigger('change');	
		$("#idgroup_penjualan_daa").val('#').trigger('change');
		$("#idgroup_diskon_daa").val('#').trigger('change');
		
		$("#iddokter_daa").val('0').trigger('change');
		$("#idkelas_daa").val('0').trigger('change');
	}
	$(document).on("click","#simpan_daa",function(){
		if (validate_add_daa()==false)return false;
		var idkelas=$("#idkelas_daa").val();
		var idtipe=$("#idtipe_daa").val();
		var jenis_asisten=$("#jenis_asisten_daa").val();
		var iddokter=$("#iddokter_daa").val();
		var idgroup_penjualan=$("#idgroup_penjualan_daa").val();
		var idgroup_diskon=$("#idgroup_diskon_daa").val();
	
		
		// alert('SINI');
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_ko/simpan_daa',
			type: 'POST',
			data: {
				idkelas:idkelas
				,idtipe:idtipe
				,jenis_asisten:jenis_asisten
				,iddokter:iddokter
				,idgroup_penjualan:idgroup_penjualan
				,idgroup_diskon:idgroup_diskon
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_daa').DataTable().ajax.reload( null, false );
					clear_input_daa();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	
	$(document).on("click","#clear_daa",function(){		
		clear_input_daa();
	});
	function load_daa(){
		var jenis=1;
		var idlogic=$("#id").val();
		
		$('#tabel_daa').DataTable().destroy();
		var table = $('#tabel_daa').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pendapatan_ko/load_daa/',
			type: "POST",
			dataType: 'json',
			data: {
				jenis: jenis
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
					]
		});
	}
	function hapus_daa($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_pendapatan_ko/hapus_daa/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_daa').DataTable().ajax.reload( null, false );							
						
						clear_input_daa();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	
	$(document).on("change","#jenis_asisten_dao",function(){
		list_dokter_pegawai_anestesi();
	});
	
	function validate_add_dao()
	{
			var idtipe_dao=$("#idtipe_dao").val();
			var idgroup_penjualan_dao=$("#idgroup_penjualan_dao").val();
			var idgroup_diskon_dao=$("#idgroup_diskon_dao").val();
			var idgroup_tuslah_dao=$("#idgroup_tuslah_dao").val();
		
					
			if (idgroup_penjualan_dao=='#'){
				sweetAlert("Maaf...", "Group Penjualan Harus diisi", "error");
				return false;
			}
			if (idgroup_diskon_dao=='#'){
				sweetAlert("Maaf...", "Group Diskon Harus diisi", "error");
				return false;
			}
			
			
		return true;
	}
	function clear_input_dao(){
		$("#idtipe_dao").val('#').trigger('change');	
		$("#jenis_asisten_dao").val('0').trigger('change');	
		$("#idgroup_penjualan_dao").val('#').trigger('change');
		$("#idgroup_diskon_dao").val('#').trigger('change');
		
		$("#iddokter_dao").val('0').trigger('change');
		$("#idkelas_dao").val('0').trigger('change');
	}
	$(document).on("click","#simpan_dao",function(){
		if (validate_add_dao()==false)return false;
		var idkelas=$("#idkelas_dao").val();
		var idtipe=$("#idtipe_dao").val();
		var jenis_asisten=$("#jenis_asisten_dao").val();
		var iddokter=$("#iddokter_dao").val();
		var idgroup_penjualan=$("#idgroup_penjualan_dao").val();
		var idgroup_diskon=$("#idgroup_diskon_dao").val();
	
		
		// alert('SINI');
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_ko/simpan_dao',
			type: 'POST',
			data: {
				idkelas:idkelas
				,idtipe:idtipe
				,jenis_asisten:jenis_asisten
				,iddokter:iddokter
				,idgroup_penjualan:idgroup_penjualan
				,idgroup_diskon:idgroup_diskon
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_dao').DataTable().ajax.reload( null, false );
					clear_input_dao();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	
	$(document).on("click","#clear_dao",function(){		
		clear_input_dao();
	});
	function load_dao(){
		var jenis=1;
		var idlogic=$("#id").val();
		
		$('#tabel_dao').DataTable().destroy();
		var table = $('#tabel_dao').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pendapatan_ko/load_dao/',
			type: "POST",
			dataType: 'json',
			data: {
				jenis: jenis
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
					]
		});
	}
	function hapus_dao($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_pendapatan_ko/hapus_dao/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_dao').DataTable().ajax.reload( null, false );							
						
						clear_input_dao();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("change","#jenis_asisten_dao",function(){
		list_dokter_pegawai_operator();
	});
</script>
