<style>
table.fixed {
      table-layout: fixed;
      width: 100%;
    }
    table.fixed td {
      overflow: hidden;
    }
</style>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('msetting_tarif_pengajuan/save_setting','class="form-horizontal push-10-t"') ?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <div class="col-md-12">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="status">Nama Setting</label>
                    <div class="col-md-8">
                        <input type="text" disabled class="form-control" placeholder="Nama Setting" name="nama" value="{nama}">
                        <input type="hidden" readonly class="form-control" id="id" placeholder="No. Jurnal" name="id" value="{id}">
                    </div>
                </div>
                
                
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="status">Nama Pengajuan</label>
                    <div class="col-md-8">
                        <select name="idpengajuan[]" disabled class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
						<?php foreach (get_all('mpengajuan_skd', array('status' => '1')) as $row) { ?>
							<option value="<?= $row->id; ?>" <?= (in_array($row->id, $idpengajuan) ? 'selected':'') ?>><?= $row->nama; ?></option>
						<?php } ?>
					</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="status">Diskon All</label>
                    <div class="col-md-8">
                        <select name="group_diskon_all" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="0" <?=($group_diskon_all==''?"selected":"")?>>-Belum dipilih-</option>
						<?php foreach (get_all('mgroup_pembayaran', array('status' => '1')) as $row) { ?>
							<option value="<?= $row->id; ?>" <?=($group_diskon_all==$row->id?"selected":"")?>><?= $row->nama; ?></option>
						<?php } ?>
					</select>
                    </div>
                </div>
				
				
			</div>
			
        </div>
	</div>
	<div class="block-content">
		 <hr style="margin-top:10px">
		<table class="table table-bordered table-striped fixed" id="index_list" style="width: 100%;">
			<thead>
				<tr>
					<th style="width:10%"  class="text-center">Pengajuan</th>
					<th style="width:5%"  class="text-center">Permintaan Ke</th>
					<th style="width:8%"  class="text-center">Tarif Rumah Sakit</th>
					<th style="width:25%"  class="text-center">Group RS</th>
					<th style="width:8%"  class="text-center">Tarif Dokter</th>
					<th style="width:25%"  class="text-center">Group Dokter</th>
					<th style="width:25%"  class="text-center">Group Diskon</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	
	
	
	<div class="block-content">
		 <hr style="margin-top:10px">
		 <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<div class="col-xs-12 text-right">
						<a href="{base_url}msetting_tarif_pengajuan" class="btn btn-default" type="reset"><i class="fa fa-reply"></i> Kembali</a>
						<button class="btn btn-primary" name="btn_simpan" type="submit" value="1"><i class="fa fa-save"></i> Simpan</button>
					</div>
				</div>
				
            </div>
        </div>
		<hr style="margin-bottom:10px">		
		
	</div>
</div>


<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
	$("#cover-spin").show();
	load_index();
	
   
});

function load_index(){
	var id=$("#id").val();
	var disabel=$("#disabel").val();
	$('#index_list tbody').empty();
	$.ajax({
		url: '{site_url}msetting_tarif_pengajuan/load_index/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel},
		success: function(data) {
			// console.log(data.tabel);
			$('#index_list').append(data.tabel);
			$(".opsi").select2({dropdownAutoWidth: false});
			$("#cover-spin").hide();
			// $(".tgl").datepicker();
			// $(".simpan").hide();
		}
	});
}


</script>