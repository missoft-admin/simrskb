<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}msetting_tarif_pengajuan/index/1" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content block-content-narrow">
        <?php echo form_open('msetting_tarif_pengajuan/save','class="form-horizontal push-10-t" id="form-work"') ?>
        <div class="form-group">
            <label class="col-md-3 control-label" for="nama">Nama Setting</label>
            <div class="col-md-7">
                <input type="text" class="form-control" placeholder="Nama Setting" name="nama" value="{nama}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="idtipe">Nama Pengajuan</label>
            <div class="col-md-7">
                <select name="idpengajuan[]" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
                    <?php foreach (get_all('mpengajuan_skd', array('status' => '1')) as $row) { ?>
                    <option value="<?= $row->id; ?>" <?= (in_array($row->id, $idpengajuan) ? 'selected':'') ?>><?= $row->nama; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="block-content">
        <input type="hidden" class="form-control" id="rowindex" />
        <input type="hidden" class="form-control" id="number" />
		<div class="table-responsive">
        <table class="table table-bordered table-striped" id="detail_list">
            <thead>
                <tr>
                    <th width="10%">Permintaan Ke</th>
                    <th width="20%">Tarif Untuk Rumah Sakit</th>
                    <th width="20%">Tarif Untuk Dokter</th>
                    <th width="20%">Total Tarif</th>
                    <th width="10%">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <select class="form-control" id="permintaan_ke" value="">
                            <?php for ($i=1; $i <= 10; $i++) { ?>
                            <option value="<?= $i; ?>"><?= $i; ?></option>
                            <? } ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" class="form-control number detail" id="tarif_rs" value="">
                    </td>
                    <td>
                        <input type="text" class="form-control number detail" id="tarif_dokter" value="">
                    </td>
                    <td>
                        <input type="text" class="form-control number detail" id="total_tarif" disabled value="">
                    </td>
                    <td>
                        <a id="detail_add" class="btn btn-primary">Tambahkan</a>
                    </td>
                </tr>
                <?php if ($this->uri->segment(2) == 'update') { ?>
                <?php foreach ($detail as $row) { ?>
                <tr>
                    <td><?= number_format($row->permintaan_ke); ?></td>
                    <td><?= number_format($row->tarif_rs); ?></td>
                    <td><?= number_format($row->tarif_dokter); ?></td>
                    <td><?= number_format($row->total_tarif); ?></td>
                    <td>
                        <a href="#" class="btn btn-success detail_edit" data-toggle="tooltip" title="Edit">
							<i class="fa fa-pencil"></i>
						</a>
                        &nbsp;&nbsp;
                        <a href="#" class="btn btn-danger detail_remove" data-toggle="tooltip" title="Remove">
							<i class="fa fa-trash-o"></i>
						</a>
                    </td>
                </tr>
                <? } ?>
                <? } ?>
            </tbody>
        </table>
            </div>

        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-9">
                <button class="btn btn-success" type="submit">Simpan</button>
                <a href="{base_url}msetting_tarif_pengajuan" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
            </div>
        </div>
        <br><br>

        <input type="hidden" id="detail_value" name="detail_value">

        <?php echo form_hidden('id', $id); ?>
        <?php echo form_close() ?>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $(".number").number(true, 0, '.', ',');

    $("#tarif_rs, #tarif_dokter").keyup(function() {
        var tarif_rs = parseInt($("#tarif_rs").val() ? $("#tarif_rs").val() : 0);
        var tarif_dokter = parseInt($("#tarif_dokter").val() ? $("#tarif_dokter").val() : 0);

        $("#total_tarif").val(tarif_rs + tarif_dokter);
    });

    $("#detail_add").click(function() {
        var valid = validate_detail();
        if (!valid) return false;

        var rowIndex;
        var duplicate = false;

        if ($("#rowindex").val() != '') {
            var content = "";
            rowIndex = $("#rowindex").val();
        } else {
            var content = "<tr>";
            $('#detail_list tbody tr').filter(function() {
                var $cells = $(this).children('td');
                if ($cells.eq(1).text() === $("#permintaan_ke").val()) {
                    sweetAlert("Maaf...", "Permintaan Ke " + $("#permintaan_ke option:selected").text() + " sudah ditambahkan.", "error");
                    duplicate = true;
                    return false;
                }
            });
        }

        if (duplicate == false) {
            content += "<td width='10%'>" + $("#permintaan_ke option:selected").val(); + "</td>";
            content += "<td width='10%'>" + $.number($("#tarif_rs").val()); + "</td>";
            content += "<td width='10%'>" + $.number($("#tarif_dokter").val()); + "</td>";
            content += "<td width='10%'>" + $.number($("#total_tarif").val()); + "</td>";
            content += "<td width='10%'><a href='#' class='btn btn-success detail_edit' data-toggle='tooltip' title='Edit'><i class='fa fa-pencil'></i></a>&nbsp;&nbsp;<a href='#' class='btn btn-danger detail_remove' data-toggle='tooltip' title='Remove'><i class='fa fa-trash-o'></i></a></td>";

            if ($("#rowindex").val() != '') {
                $('#detail_list tbody tr:eq(' + rowIndex + ')').html(content);
            } else {
                content += "</tr>";
                $('#detail_list tbody').append(content);
            }

            detail_clear();
        }
    });

    $(document).on("click", ".detail_edit", function() {
        $("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
        $("#permintaan_ke").val($(this).closest('tr').find("td:eq(0)").html()).trigger("change");
        $("#tarif_rs").val($(this).closest('tr').find("td:eq(1)").html());
        $("#tarif_dokter").val($(this).closest('tr').find("td:eq(2)").html());
        $("#total_tarif").val($(this).closest('tr').find("td:eq(3)").html());

        return false;
    });

    $(document).on("click", ".detail_remove", function() {
        if (confirm("Hapus data ?") == true) {
            $(this).closest('td').parent().remove();
        }
        return false;
    });

    $("#form-work").submit(function(e) {
        var form = this;

        var detail_tbl = $('table#detail_list tbody tr').get().map(function(row) {
            return $(row).find('td').get().map(function(cell) {
                return $(cell).html();
            });
        });

        $("#detail_value").val(JSON.stringify(detail_tbl));

        swal({
            title: "Berhasil!",
            text: "Proses penyimpanan data.",
            type: "success",
            timer: 1500,
            showConfirmButton: false
        });
    });

});

function detail_clear() {
    $("#rowindex").val('');
    $("#number").val('');
    $(".detail").val('');

    var permintaanKe = parseInt($("#permintaan_ke option:selected").val()) + 1;
    if (permintaanKe <= 10) {
        $("#permintaan_ke").val(permintaanKe);
    } else {
        $("#permintaan_ke").val('');
    }
}

function validate_detail() {
    if ($("#permintaan_ke").val() == undefined) {
        sweetAlert("Maaf...", "Permintaan Ke, Belum Dipilih!", "error").then((value) => {
            $("#permintaan_ke").focus();
        });
        return false;
    }

    return true;
}

</script>
