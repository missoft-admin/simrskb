<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}antrian_layanan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('antrian_layanan/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="row">
				<div class="form-group" id="div_3">
					<label class="col-md-2 control-label" for="kode">Kode</label>
					<div class="col-md-8">
						<input type="text" class="form-control" id="kode" placeholder="Kode" name="kode" value="{kode}">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-2 control-label" for="nama_pelayanan">Nama Pelayanan</label>
					<div class="col-md-8">
						<input type="text" class="form-control" id="nama_pelayanan" placeholder="Nama Pelayanan" name="nama_pelayanan" value="{nama_pelayanan}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="st_default">Urutan</label>
					<div class="col-md-8">
						<select id="urutan"  name="urutan" class="js-select2 form-control" style="width: 100%;">
							<?for($i=1;$i<20;$i++){?>
							  <option value="<?=$i?>" <?=($i == $urutan ? 'selected="selected"':'')?>><?=$i?></option>
							<?}?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="kuota">Kuota</label>
					<div class="col-md-8">
						<input type="text" class="form-control number" id="kuota" placeholder="kuota" name="kuota" value="{kuota}">
					</div>
				</div>	
				
			</div>
			<div class="row">
				<div class="form-group">
					<label class="col-md-2 control-label"></label>
					<div class="col-md-8">
						<table class="table table-bordered">
							<thead>
								<th class="text-center" style="width:10%">x</th>
								<th class="text-center" style="width:20%">HARI</th>
								<th class="text-center" style="width:10%">24 JAM LAYANAN</th>
								<th class="text-center" style="width:10%">OPEN</th>
								<th class="text-center" style="width:10%">CLOSE</th>
								<th class="text-center" style="width:10%">AUTO OPEN</th>
								<th class="text-center" style="width:15%">KUOTA</th>
							</thead>
							<tbody>
								<?foreach($list_hari as $r){?>
								<tr>
									<td  class="text-center"><input type="checkbox" class="chk_status" name="status[<?=$r->kodehari?>]" value="<?=$r->status?>" <?=($r->status?'checked':'')?>></td>
									<td><?=$r->namahari?><input type="hidden" class="form-control id_det" name="id_det[<?=$r->kodehari?>]" value="<?=$r->id?>"><input type="hidden" class="form-control kodehari" name="kodehari[<?=$r->kodehari?>]" value="<?=$r->kodehari?>"><input type="hidden" class="form-control namahari" name="namahari[<?=$r->kodehari?>]" value="<?=$r->namahari?>"></td>
									<td  class="text-center"><input <?=($r->status?'':'disabled')?> type="checkbox" class="sound_idField" name="st_24_jam[<?=$r->kodehari?>]" value="<?=$r->st_24_jam?>" <?=($r->st_24_jam?'checked':'')?>></td>
									<td  class="text-center"><input <?=($r->status?'':'disabled')?> type="text" class="form-control js-masked-time input-sm jam_buka sound_idField" name="jam_buka[<?=$r->kodehari?>]" value="<?=$r->jam_buka?>"></td>
									<td  class="text-center"><input <?=($r->status?'':'disabled')?> type="text" class="form-control js-masked-time input-sm jam_tutup sound_idField" name="jam_tutup[<?=$r->kodehari?>]" value="<?=$r->jam_tutup?>"></td>
									<td  class="text-center"><input <?=($r->status?'':'disabled')?> type="checkbox" class="sound_idField" name="auto_open[<?=$r->kodehari?>]" value="<?=$r->auto_open?>" <?=($r->auto_open?'checked':'')?>></td>
									<td  class="text-center"><input <?=($r->status?'':'disabled')?> type="text" class="form-control input-sm kuota_harian number sound_idField" name="kuota_harian[<?=$r->kodehari?>]" value="<?=$r->kuota_harian?>"></td>
								</tr>
								<?}?>
							</tbody>
						</table>
					</div>
				</div>
				
			</div>
			<?if ($id){?>
			<div class="row push-20-t">
				
			</div>
			<div class="row">
				<div class="form-group">
					<label class="col-md-10 col-md-offset-2 text-danger">SOUND</label>
					<input type="hidden" class="form-control" id="antrian_id" placeholder="No Urut" name="antrian_id" value="{id}">
					<input type="hidden" class="form-control" id="arr_sound" placeholder="No Urut" name="arr_sound" value="">
				</div>
				<div class="form-group">
					<div class="col-md-10 col-md-offset-2">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="index_sound">
								<thead>
									<tr>
										<th width="5%">No</th>
										<th width="10%">Urutan</th>
										<th width="65%">Asset Sound</th>
										<th width="20%">Action</th>										   
									</tr>
									<input id="idsound" type="hidden" value="">
									<tr>
										<th>#</th>
										<th>
											<input tabindex="0" type="text" class="form-control number" id="nourut" placeholder="No Urut" name="nourut">
										</th>
										<th>
											<select id="sound_id"  name="sound_id" class="js-select2 form-control" style="width: 100%;">
												<option value="#" selected>-Pilih Asset Sound-</option>
												<?foreach($list_sound as $row){?>
												  <option value="<?=$row->id?>"><?=$row->nama_asset?></option>
												<?}?>
											</select>
										
										</th>
										
										<th>
											<div class="btn-group">
												<button class="btn btn-primary btn-sm" title="Simpan" type="button" id="btn_tambah_sound"><i class="fa fa-save"></i></button>
												<button class="btn btn-warning btn-sm"  title="Clear" type="button" id="btn_refresh_sound"><i class="fa fa-refresh"></i></button>
												<button class="btn btn-danger btn-sm"  title="Play All" type="button" id="btn_play_all"><i class="fa fa-play"></i></button>
											</div>
										</th>										   
									</tr>
									
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<?}?>
			<div class="form-group">
				<div class="col-md-2 col-md-offset-2">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}antrian_layanan" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>

</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".number").number(true,0,'.',',');
		if ($("#antrian_id").val()!=''){
			load_sound();
		}
	})	
	$(".chk_status").on("click", function(){
		let id_det;
		 check = $(this).is(":checked");
			if(check) {
				$(this).closest('tr').find(".sound_idField").removeAttr("disabled");
			} else {
				$(this).closest('tr').find(".sound_idField").attr('disabled', 'disabled');
			}
	}); 
	
	function validate_final(){
		
		if ($("#kode").val()==''){
			sweetAlert("Maaf...", "Tentukan Kode", "error");
			return false;
		}
		if ($("#nama_pelayanan").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Pelayanan", "error");
			return false;
		}
		if ($("#kuota").val()==''){
			sweetAlert("Maaf...", "Tentukan kuota", "error");
			return false;
		}
		$("*[disabled]").not(true).removeAttr("disabled");
		$("#cover-spin").show();
		return true;

		
	}
	$("#btn_tambah_sound").click(function() {
		let antrian_id=$("#antrian_id").val();
		let idsound=$("#idsound").val();
		let nourut=$("#nourut").val();
		let sound_id=$("#sound_id").val();
		if ($("#nourut").val()==''){
			sweetAlert("Maaf...", "Tentukan No Urut", "error");
			return false;
		}
		if ($("#sound_id").val()==''){
			sweetAlert("Maaf...", "Tentukan Isi", "error");
			return false;
		}
		
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}antrian_layanan/simpan_sound', 
			dataType: "JSON",
			method: "POST",
			data : {
				antrian_id:antrian_id,
				idsound:idsound,
				nourut:nourut,
				sound_id:sound_id,
				},
			complete: function(data) {
				// $("#idtipe").val('#').trigger('change');
				$('#index_sound').DataTable().ajax.reload( null, false ); 
				$("#cover-spin").hide();
				clear_sound();
				
				load_data_sound();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}
		});

	});
	$("#btn_refresh_sound_text").click(function() {
	clear_sound();
	});
	$("#btn_play_all").click(function() {
		play_sound_arr();
	});
	function clear_sound(){
		$("#idsound").val('');
		$("#nourut").val('');
		$("#sound_id").val('#').trigger('change');
	}
	function load_sound(){
		var antrian_id=$("#antrian_id").val();
		$('#index_sound').DataTable().destroy();	
		table = $('#index_sound').DataTable({
			autoWidth: false,
			searching: false,
			serverSide: true,
			"processing": true,
			"order": [],
			"pageLength": 10,
			"ordering": false,
			
			ajax: { 
				url: '{site_url}antrian_layanan/load_sound', 
				type: "POST" ,
				dataType: 'json',
				data : {
						antrian_id:antrian_id
					   }
			}
		});
		load_data_sound();
	}
	function hapus_sound($id){
	 var id=$id;
	swal({
			title: "Anda Yakin ?",
			text : "Untuk Hapus Sound?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}antrian_layanan/hapus_sound',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
					$('#index_sound').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					load_data_sound();
				}
			});
		});
	}
	function edit_sound($id){
		let id=$id;
		$.ajax({
			url: '{site_url}antrian_layanan/edit_sound', 
			dataType: "JSON",
			method: "POST",
			data : {id:id},
			success: function(data) {
				$("#nourut").val(data.nourut);
				$("#sound_id").val(data.sound_id).trigger('change');
				$("#idsound").val(data.id);
			}
		});

	}
	function load_data_sound(){
		let antrian_id=$("#antrian_id").val();
		$.ajax({
			url: '{site_url}antrian_layanan/load_data_sound', 
			dataType: "JSON",
			method: "POST",
			data : {antrian_id:antrian_id},
			success: function(data) {
				$("#arr_sound").val(data.file);
				
			}
		});

	}
	function play_sound_arr(){
		let str_file=$("#arr_sound").val();
		var myArray = str_file.split(":");
		console.log(myArray[0]);
		queue_sounds(myArray);
	}
	function queue_sounds(sounds){
		// $config['upload_path'] = './assets/upload/sound_antrian/';
		let path_sound='{site_url}assets/upload/sound_antrian/';
		
		var index = 0;
		function recursive_play()
		{
			let file_sound = new Audio(path_sound+sounds[index]);
		  if(index+1 === sounds.length)
		  {
			play(file_sound);
		  }
		  else
		  {
			play(file_sound,function(){
				index++; recursive_play();
				});
		  }
		}

	recursive_play();   
	}
	 function play(audio, callback) {

		audio.play();
		if(callback)
		{
			audio.onended = callback;
		}
	}
</script>