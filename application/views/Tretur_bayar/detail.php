<style>
table.fixed {
      table-layout: fixed;
      width: 100%;
    }
    table.fixed td {
      overflow: hidden;
    }
</style>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tvalidasi_pendapatan/save_detail','class="form-horizontal push-10-t"') ?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Penerimaan</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="nopenerimaan_asal" placeholder="No. Jurnal" name="nopenerimaan_asal" value="{nopenerimaan_asal}">
                        <input type="hidden" readonly class="form-control" id="id" placeholder="No. Jurnal" name="id" value="{id}">
                        <input type="hidden" readonly class="form-control" id="st_posting" placeholder="No. Jurnal" name="st_posting" value="{st_posting}">
                        <input type="hidden" readonly class="form-control" id="disabel" placeholder="No. Jurnal" name="disabel" value="{disabel}">
                    </div>
                </div>
                
                
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Distributor</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="distributor" placeholder="No. Jurnal" name="distributor" value="{distributor}">
                    </div>
                </div>
				
            </div>
			<div class="col-md-6">
				
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Nominal Barang</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_transaksi" placeholder="No. Faktur" name="tanggal_transaksi" value="<?=number_format($nominal_retur,2)?>">
                        
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nominal Pengganti</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="no_terima" placeholder="No. Penerimaan" name="no_terima" value="<?=number_format($nominal_ganti,2)?>">
                    </div>
                </div>
				
			</div>
            
        </div>
	</div>
	<?
		$q="SELECT *from tretur_penerimaan_detail D WHERE D.idretur='$id' ORDER BY D.id ";
		$list_barang=$this->db->query($q)->result();
		
		$q="SELECT *from tretur_penerimaan_ganti D WHERE D.idretur='$id' ORDER BY D.id ";	
		$list_ganti=$this->db->query($q)->result();
	?>
	<div class="block-content">
		<div class="row">
			<label class="col-md-2 control-label" for="tanggal"><h3><?=text_primary('BARANG RETUR')?></h3></label>
            <div class="col-md-10">
					<table class="table table-striped table-borderless table-header-bg" id="index_list" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:5%"  class="text-center">NO </th>
								<th style="width:10%"  class="text-center">TIPE</th>
								<th style="width:35%"  class="text-center">BARANG</th>								
								<th style="width:15%"  class="text-center">KUANTITAS</th>								
								<th style="width:15%"  class="text-center">HARGA (ppn+diskon)</th>								
								<th style="width:15%"  class="text-center">TOTAL</th>								
							</tr>
						</thead>
						<tbody>
							<?
								$no=1;
								$total=0;
							foreach($list_barang as $row){
								$total=$row->tot_harga + $total;
								?>
							<tr>
								<td class="text-right"><?=$no?></td>
								<td class="text-center"><?=GetTipeKategoriBarang($row->idtipe)?></td>
								<td class="text-left"><?=$row->namabarang?></td>
								<td class="text-right"><?=$row->kuantitas?></td>
								<td class="text-right"><?=number_format($row->harga,2)?></td>
								<td class="text-right"><?=number_format($row->tot_harga,2)?></td>
							</tr>
							<?
							$no=$no+1;
							}?>
							
						</tbody>
						<tfoot>
							<tr>
								<td colspan="5" class="text-right"><strong>TOTAL</strong></td>
								<td class="text-right"><strong><?=number_format($total,2)?></strong></td>
							</tr>
						</tfoot>
					</table>
			</div>
		</div>
		<div class="row">
			<label class="col-md-2 control-label" for="tanggal"><h3><?=text_danger('BARANG PENGGANTI')?></h3></label>
            <div class="col-md-10">
					<table class="table table-striped table-borderless table-header-bg" id="index_list" style="width: 100%;">
						<thead>
							<tr>
								<th style="width:5%"  class="text-center">NO </th>
								<th style="width:10%"  class="text-center">TIPE</th>
								<th style="width:35%"  class="text-center">BARANG</th>								
								<th style="width:15%"  class="text-center">KUANTITAS</th>								
								<th style="width:15%"  class="text-center">HARGA (ppn+diskon)</th>								
								<th style="width:15%"  class="text-center">TOTAL</th>								
							</tr>
						</thead>
						<tbody>
							<?
								$no=1;
								$total=0;
							foreach($list_ganti as $row){
								$total=$row->tot_harga + $total;
								?>
							<tr>
								<td class="text-right"><?=$no?></td>
								<td class="text-center"><?=GetTipeKategoriBarang($row->idtipe)?></td>
								<td class="text-left"><?=$row->namabarang?></td>
								<td class="text-right"><?=$row->kuantitas?></td>
								<td class="text-right"><?=number_format($row->harga,2)?></td>
								<td class="text-right"><?=number_format($row->tot_harga,2)?></td>
							</tr>
							<?
							$no=$no+1;
							}?>
							
						</tbody>
						<tfoot>
							<tr>
								<td colspan="5" class="text-right"><strong>TOTAL</strong></td>
								<td class="text-right"><strong><?=number_format($total,2)?></strong></td>
							</tr>
							<tr>
								<td colspan="5" class="text-right"><strong>SISA</strong></td>
								<td class="text-right"><strong><?=number_format($nominal_retur-$nominal_ganti,2)?></strong></td>
							</tr>
						</tfoot>
					</table>
			</div>
		</div>
	</div>
	
</div>
<?php echo form_close(); ?>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
   
});


</script>