<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">

		<hr style="margin-top:0px">
		<div class="row">
			<?php echo form_open('trujukan_rumahsakit_pembayaran/filter', 'class="form-horizontal" id="form-work"') ?>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">No. Pembayaran</label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="notransaksi" value="{notransaksi}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Rujukan</label>
					<div class="col-md-8">
						<select name="idrujukan" class="js-select2 form-control" style="width: 100%;"
							data-placeholder="Pilih Opsi">
							<option value="0">Semua Rujukan</option>
							<?php foreach (get_all('mrumahsakit', array('status' => 1)) as $row){?>
							<option value="<?= $row->id?>" <?=($idrujukan == $row->id ? 'selected="selected"':'')?>><?= $row->nama?></option>
							<?php }?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="example-daterange1">Tanggal Jatuh Tempo</label>
					<div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
							<input class="form-control" type="text" name="tanggaljatuhtempo_dari" placeholder="From" value="{tanggaljatuhtempo_dari}">
							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
							<input class="form-control" type="text" name="tanggaljatuhtempo_sampai" placeholder="To" value="{tanggaljatuhtempo_sampai}">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Status</label>
					<div class="col-md-8">
						<select name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#" <?=($status == '#' ? 'selected="selected"':'')?>>Semua Status</option>
							<option value="0" <?=($status == '0' ? 'selected="selected"':'')?>>On Proses</option>
							<option value="1" <?=($status == '1' ? 'selected="selected"':'')?>>Selesai Perhitungan</option>
							<option value="2" <?=($status == '2' ? 'selected="selected"':'')?>>Menunggu Approval</option>
							<option value="3" <?=($status == '3' ? 'selected="selected"':'')?>>Disetujui</option>
							<option value="4" <?=($status == '4' ? 'selected="selected"':'')?>>Ditolak</option>
							<option value="5" <?=($status == '5' ? 'selected="selected"':'')?>>Pembayaran</option>
							<option value="6" <?=($status == '6' ? 'selected="selected"':'')?>>Telah Dibayarkan</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
		<hr>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No.</th>
					<th>No. Pembayaran</th>
					<th>Nama Rujukan</th>
					<th>Tanggal Jatuh Tempo</th>
					<th>Nominal</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}trujukan_rumahsakit_pembayaran/getIndex/' + '<?= $this->uri->segment(2); ?>',
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": false },
					{ "width": "5%", "targets": 1, "orderable": true },
					{ "width": "10%", "targets": 2, "orderable": false },
					{ "width": "10%", "targets": 3, "orderable": false },
					{ "width": "5%", "targets": 4, "orderable": false },
					{ "width": "5%", "targets": 5, "orderable": false },
					{ "width": "10%", "targets": 6, "orderable": false },
				]
			});
	});
</script>