<div class="modal fade in black-overlay" id="PembayaranModal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Pembayaran</h3>
				</div>
				<div class="block-content block-content-narrow">

					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-md-3 control-label" for="">Metode</label>
							<div class="col-md-7">
								<select id="modal_idmetode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="">Pilih Opsi</option>
									<option value="1">Tunai</option>
									<option value="2">Debit</option>
									<option value="3">Kartu Kredit</option>
									<option value="4">Transfer</option>
									
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="">Sisa Pembayaran</label>
							<div class="col-md-7">
								<input readonly type="text" class="form-control number" id="modal_sisa" placeholder="Sisa Pembayaran" value="">
							</div>
						</div>
						<div class="form-group" id="div_bank" hidden>
							<label class="col-md-3 control-label" for="">Bank</label>
							<div class="col-md-7">
								<select id="modal_idbank" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="">Pilih Opsi</option>
									<?php $mbank = get_all('mbank', array('status' => '1')); ?>
									<?php foreach ($mbank as $row) { ?>
										<option value="<?=$row->id?>"><?=$row->nama . ' (' . $row->atasnama . ')'?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group" id="div_cc" hidden>
							<label class="col-md-3 control-label" for="">Ket. CC </label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="modal_keterangan_cc" placeholder="Ket. CC" value="">
							</div>
						</div>
						<div class="form-group" id="div_trace" hidden>
							<label class="col-md-3 control-label" for="">Trace Number</label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="modal_trace_number" placeholder="Trace Number" value="">
							</div>
						</div>
						<div class="form-group" id="div_kontraktor" hidden>
							<label class="col-md-3 control-label" for="">Tipe Kontraktor </label>
							<div class="col-md-7">
								<select id="modal_tipekontraktor" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="#">Pilih Opsi</option>
									<?php $tipeKontraktor = get_all('mpasien_kelompok', array('status' => 1, 'id !=' => 5)); ?>
									<?php foreach ($tipeKontraktor as $row) {?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group" id="div_kontraktor2" hidden>
							<label class="col-md-3 control-label" for="">Kontraktor</label>
							<div class="col-md-7">
								<select id="modal_idkontraktor" class="form-control input-sm" style="width:100%" data-placeholder="Kontraktor"></select>
							</div>
						</div>
						<div class="form-group" id="div_pegawai" hidden>
							<label class="col-md-3 control-label" for="">Pegawai</label>
							<div class="col-md-7">
								<select style="width:100%" id="modal_idpegawai" data-placeholder="Pegawai" class="form-control input-sm"></select>
							</div>
						</div>
						<div class="form-group" id="div_dokter" hidden>
							<label class="col-md-3 control-label" for="iddokter">Dokter</label>
							<div class="col-md-7">
								<select style="width:100%" id="modal_iddokter" data-placeholder="Dokter" class="form-control input-sm">
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Nominal Pembayaran</label>
							<div class="col-md-7">
								<input type="text" class="form-control number" id="modal_nominal" placeholder="Nominal Pembayaran" value="">
							</div>
						</div>
						<div class="form-group" id="div_jaminan">
							<label class="col-md-3 control-label" for="nama">Jaminan </label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="modal_jaminan" placeholder="Jaminan" value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="nama">Keterangan </label>
							<div class="col-md-7">
								<input type="text" class="form-control" id="modal_keterangan" placeholder="Keterangan" value="">
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button class="btn btn-sm btn-primary" type="button" data-dismiss="modal" id="btnAddPembayaran">Tambahkan</button>
						<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batalkan</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		initFormInputNumber();
		generateGrandTotal();
        generateSisaPembayaran();
        generateNominalPembayaran();


		$("#btnModalPembayaran").click(function () {
			formOpenModal();
		});

		$("#modal_idmetode").change(function () {
			var idmetode = $(this).val();

			if (idmetode == "1") {
				formMethodCash();
			}
			if (idmetode == "2" || idmetode == "3" || idmetode == "4") {
				formMethodDebitCreditTransfer();
			}
			if (idmetode == "5") {
				formMethodTagihanPegawai();
			}
			if (idmetode == "6") {
				formMethodTagihanDokter();
			}
			if (idmetode == "7") {
				formMethodTidakTertagihkan();
			}
			if (idmetode == "8") {
				formMethodKontraktor();
			}
		});

		$("#modal_idpegawai").select2({
			minimumInputLength: 2,
			noResults: 'Pegawai Tidak Ditemukan.',
			ajax: {
				url: '{site_url}tkasir/get_pegawai',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
				data: function (params) {
					var query = {
						search: params.term,
					}
					return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.nama,
								id: item.id,
							}
						})
					};
				}
			}
		});

		$("#modal_iddokter").select2({
			minimumInputLength: 2,
			noResults: 'Dokter Tidak Ditemukan.',
			ajax: {
				url: '{site_url}tkasir/get_dokter',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
				data: function (params) {
					var query = {
						search: params.term,
					}
					return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.nama,
								id: item.id,
							}
						})
					};
				}
			}
		});

		$("#modal_tipekontraktor").change(function () {
			if ($(this).val() != '') {
				$.ajax({
					url: '{base_url}trawatinap_tindakan/find_kontraktor/' + $(this).val(),
					dataType: "json",
					success: function (data) {
						$('#modal_idkontraktor').empty();
						$.each(data.detail, function (i, row) {
							$('#modal_idkontraktor').append('<option value="' + row.id + '" selected>' + row.nama + '</option>');
						});
					}
				});
				$("#modal_keterangan").val($("#modal_tipekontraktor option:selected").text() + ' : ' + $("#modal_idkontraktor option:selected").text());
			}
		});

		$("#modal_idkontraktor").change(function () {
			$("#modal_keterangan").val($("#modal_tipekontraktor option:selected").text() + ' : ' + $("#modal_idkontraktor option:selected").text());
		});

		$("#modal_idbank").change(function () {
			$("#modal_keterangan").val($("#modal_idmetode option:selected").text() + ' : ' + $("#modal_idbank option:selected").text());
		});

		$("#modal_idpegawai").change(function () {
			$("#modal_keterangan").val('Tagihan Karyawan : ' + $("#modal_idpegawai option:selected").text());
		});

		$("#modal_keterangan_cc").keyup(function () {
			$("#modal_keterangan").val($("#modal_idmetode option:selected").text() + ' : ' + $("#modal_idbank option:selected").text() + ', Keterangan CC : ' + $("#modal_keterangan_cc").val());
		});

		$("#modal_jaminan").keyup(function () {
			$("#modal_keterangan").val('Tidak Tertagihkan dg Jaminan ' + $("#modal_jaminan").val() + ' Karena : ');
		});

		$("#btnAddPembayaran").click(function () {
			if (!validateAddPembayaran()) return false;
			var duplicate = false;
			var content = "";

			if ($("#nomor").val() != '') {
				var number = $("#nomor").val();
			} else {
				var number = $('#detailPembayaran tr').length - 2;
				var content = "<tr>";
			}

			if (duplicate) return false;

			content += "<td hidden>" + $("#modal_idmetode").val() + "</td>";
			content += "<td>" + number + "</td>";
			content += "<td>" + $("#modal_idmetode option:selected").text() + "</td>";
			content += "<td>" + $("#modal_keterangan").val() + "</td>";
			content += "<td hidden>" + $("#modal_nominal").val() + "</td>";
			content += "<td>" + formatNumber($("#modal_nominal").val()) + "</td>";
			content += "<td hidden>" + $("#modal_idkasir").val() + "</td>";
			content += "<td hidden>" + $("#modal_idmetode").val() + "</td>";
			content += "<td hidden>" + $("#modal_idpegawai").val() + "</td>";
			content += "<td hidden>" + $("#modal_idbank").val() + "</td>";
			content += "<td hidden>" + $("#modal_keterangan_cc").val() + "</td>";
			content += "<td hidden>" + $("#modal_idkontraktor").val() + "</td>";
			content += "<td hidden>" + $("#modal_keterangan").val() + "</td>";
			content += "<td><button type='button' class='btn btn-sm btn-info btnUbahPembayaran'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger btnHapusPembayaranHapus'><i class='glyphicon glyphicon-trash'></i></button></td>"
			content += "<td hidden>" + $("#modal_jaminan").val() + "</td>";
			content += "<td hidden>" + $("#modal_trace_number").val() + "</td>";
			
			if ($("#modal_idmetode").val() == '8') {
				content += "<td hidden>" + $("#modal_tipekontraktor").val() + "</td>";
			} else {
				content += "<td hidden>0</td>";
			}

			if ($("#rowindex").val() != '') {
				$('#detailPembayaran tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
			} else {
				content += "</tr>";
				$('#detailPembayaran tbody').append(content);
			}

			generateNominalPembayaran();
		});

		$(document).on("click", ".btnHapusPembayaranHapus", function () {
			if (confirm("Hapus Data ?") == true) {
				$(this).closest('td').parent().remove();
				generateNominalPembayaran();
			}
		});

		$(document).on("click", ".btnUbahPembayaran", function () {
			var nominal = $(this).closest('tr').find("td:eq(4)").html();
			var idmetode = $(this).closest('tr').find("td:eq(0)").html();
			var idpegawai = $(this).closest('tr').find("td:eq(8)").html();
			var idbank = $(this).closest('tr').find("td:eq(9)").html();
			var keterangan_cc = $(this).closest('tr').find("td:eq(10)").html();
			var idkontraktor = $(this).closest('tr').find("td:eq(11)").html();
			var keterangan = $(this).closest('tr').find("td:eq(12)").html();
			var jaminan = $(this).closest('tr').find("td:eq(14)").html();
			var trace_number = $(this).closest('tr').find("td:eq(15)").html();

			$('#modal_idmetode').val(idmetode).trigger('change');
			$('#modal_idpegawai').val(idpegawai).trigger('change');
			$('#modal_idbank').val(idbank).trigger('change');
			$('#modal_keterangan_cc').val(keterangan_cc);
			$('#modal_keterangan').val(keterangan);
			$('#modal_nominal').val(nominal);
			$('#modal_jaminan').val(jaminan);
			$('#modal_trace_number').val(trace_number);
			$("#modal_sisa").val(parseFloat($("#sisa_pembayaran").val()) + parseFloat($("#modal_nominal").val()));
			
			$("#PembayaranModal").modal('show');
			$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
			$("#nomor").val($(this).closest('tr').find("td:eq(1)").html());
		});

		$("#diskon_persen").keyup(function () {
			if ($("#diskon_persen").val() == '') {
				$("#diskon_persen").val(0)
			}
			if (parseFloat($(this).val()) > 100) {
				$(this).val(100);
			}

			var discount_price = 0;
			discount_price = (parseFloat($("#sub_total").val()) * (parseFloat($("#diskon_persen").val() / 100)));
			$("#diskon_rp").val(discount_price);

			generateGrandTotal();
			generateSisaPembayaran();
		});

		$("#diskon_rp").keyup(function () {
			if ($("#diskon_rp").val() == '') {
				$("#diskon_rp").val(0)
			}
			if (parseFloat($(this).val()) > parseFloat($("#grand_total").val())) {
				$(this).val(parseFloat($("#grand_total").val()));
			}

			var discount_percent = 0;
			discount_percent = (parseFloat($("#diskon_rp").val()) * 100) / (parseFloat($("#sub_total").val()));
			$("#diskon_persen").val(parseFloat(discount_percent).toFixed(2));
			
			generateGrandTotal();
			generateSisaPembayaran();
		});
	});

	function formOpenModal()
	{
		$("#modal_sisa").val($("#sisa_pembayaran").val());
		$("#modal_nominal").val($("#sisa_pembayaran").val());
		$("#modal_trace_number").val('');
		$('#modal_idmetode').val('').trigger('change');
		$('#modal_idbank').val('').trigger('change');
		$('#modal_idpegawai').val('').trigger('change');
		$("#modal_keterangan").val('');

		$("#nomor").val('');
		$("#rowindex").val('');

		$("#div_bank").hide();
		$("#div_kontraktor").hide();
		$("#div_pegawai").hide();
		$("#div_cc").hide();
		$("#div_trace").hide();
		$("#div_jaminan").hide();
	}
	
	function formMethodCash()
	{
		$("#div_jaminan").hide();
		$("#div_cc").hide();
		$("#div_trace").hide();
		$("#div_bank").hide();
		$("#div_kontraktor").hide();
		$("#div_kontraktor2").hide();
		$("#div_pegawai").hide();
		$("#div_tipepegawai").hide();
		$("#div_dokter").hide();
		$("#keterangan").val('Tunai');
	}

	function formMethodDebitCreditTransfer()
	{
		if ($("#idmetode").val() == "3") {
			$("#div_cc").show();
		} else {
			$("#div_cc").hide();
		}

		$("#div_kontraktor").hide();
		$("#div_kontraktor2").hide();
		$("#div_jaminan").hide();
		$("#div_trace").show();
		$("#div_pegawai").hide();
		$("#div_tipepegawai").hide();
		$("#div_dokter").hide();
		$("#div_bank").show();
		$("#keterangan").val($("#idmetode option:selected").text() + ' : ' + $("#idbank option:selected").text());
	}

	function formMethodTagihanPegawai()
	{
		$("#div_jaminan").hide();
		$("#div_bank").hide();
		$("#div_cc").hide();
		$("#div_trace").hide();
		$("#div_kontraktor").hide();
		$("#div_kontraktor2").hide();
		$("#div_tipepegawai").show();
		$("#div_pegawai").show();
		$("#div_dokter").hide();
		$("#keterangan").val('Tagihan Karyawan : ' + $("#idpegawai option:selected").text());
	}

	function formMethodTagihanDokter()
	{
		$("#div_jaminan").hide();
		$("#div_bank").hide();
		$("#div_cc").hide();
		$("#div_trace").hide();
		$("#div_kontraktor").hide();
		$("#div_kontraktor2").hide();
		$("#div_tipepegawai").show();
		$("#div_pegawai").hide();
		$("#div_dokter").show();
		$("#keterangan").val('Tagihan Dokter : ' + $("#iddokter option:selected").text());
	}

	function formMethodTidakTertagihkan()
	{
		$("#div_jaminan").show();
		$("#div_bank").hide();
		$("#div_trace").hide();
		$("#div_cc").hide();
		$("#div_kontraktor").hide();
		$("#div_kontraktor2").hide();
		$("#div_pegawai").hide();
		$("#div_tipepegawai").hide();
		$("#div_dokter").hide();
		$("#keterangan").val('Tidak Tertagihkan dg Jaminan ' + $("#jaminan").val() + ' Karena : ');
	}

	function formMethodKontraktor()
	{
		$("#div_bank").hide();
		$("#div_trace").hide();
		$("#div_jaminan").hide();
		$("#div_cc").hide();
		$("#div_kontraktor").show();
		$("#div_kontraktor2").show();
		$("#div_pegawai").hide();
		$("#div_tipepegawai").hide();
		$("#div_dokter").hide();
		$("#keterangan").val('Pembayaran ' + $("#tipekontraktor option:selected").text() + ' : ' + $("#idkontraktor option:selected").text());
	}

	function initFormInputNumber()
	{
		$("#diskon_rp").number(true, 0, '.', ',');
		$("#diskon_persen").number(true, 0, '.', ',');

		$("#sub_total").number(true, 0, '.', ',');
		$("#grand_total").number(true, 0, '.', ',');

		$("#nominal_pembayaran").number(true, 0, '.', ',');
		$("#sisa_pembayaran").number(true, 0, '.', ',');
	}

	function formatNumber(num)
	{
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
	}

	function validateAddPembayaran()
	{
		if ($("#modal_idmetode").val() == "") {
			sweetAlert("Maaf...", "Metode Pembayaran Harus dipilih!", "error");
			$("#modal_idmetode").focus();
			return false;
		}
		if ($("#modal_idmetode").val() == null) {
			sweetAlert("Maaf...", "Metode Pembayaran Harus dipilih!", "error");
			$("#modal_idmetode").focus();
			return false;
		}
		if ($("#modal_idmetode").val() == '2' || $("#modal_idmetode").val() == '3' || $("#modal_idmetode").val() == '4') {
			if ($("#modal_idbank option:selected").val() == '' || $("#modal_idbank option:selected").val() == null) {
				sweetAlert("Maaf...", "Bank Harus dipilih!", "error");
				$("#modal_idbank").focus();
				return false;
			}
		}
		if ($("#modal_idmetode").val() == '3') {
			if ($("#modal_keterangan_cc").val() == '' || $("#modal_keterangan_cc").val() == null) {
				sweetAlert("Maaf...", "CC Harus dipilih!", "error");
				$("#modal_keterangan_cc").focus();
				return false;
			}
		}
		if ($("#modal_idmetode").val() == '5') {
			if ($("#modal_idpegawai option:selected").val() == '' || $("#modal_idpegawai option:selected").val() == null) {
				sweetAlert("Maaf...", "Pegawai Harus dipilih!", "error");
				$("#modal_idpegawai").focus();
				return false;
			}
		}
		if ($("#modal_idmetode").val() == '6') {
			if ($("#modal_iddokter option:selected").val() == '' || $("#modal_iddokter option:selected").val() == null) {
				sweetAlert("Maaf...", "Dokter Harus dipilih!", "error");
				$("#modal_iddokter").focus();
				return false;
			}
		}
		if ($("#modal_idmetode").val() == '7') {
			if ($("#modal_jaminan").val() == '' || $("#modal_jaminan").val() == null) {
				sweetAlert("Maaf...", "Jaminan Harus diisi!", "error");
				$("#modal_jaminan").focus();
				return false;
			}
		}
		if ($("#modal_idmetode").val() == '8') {
			if ($("#modal_tipekontraktor option:selected").val() != '2') {
				if ($("#modal_idkontraktor option:selected").val() == '' || $("#modal_idkontraktor option:selected").val() == null) {
					sweetAlert("Maaf...", "Kontraktor Harus dipilih!", "error");
					$("#modal_idpegawai").focus();
					return false;
				}
			}
		}
		if (parseFloat($("#modal_nominal").val()) < 1 || $("#modal_nominal").val() == '') {
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			return false;
		}

		return true;
	}

	function generateNominalPembayaran()
	{
		var grandTotal = 0;
		$('#detailPembayaran tbody tr').each(function () {
			grandTotal += parseFloat($(this).find('td:eq(4)').text());
		});

		$("#nominal_pembayaran").val(grandTotal);

		generateSisaPembayaran();
	}

	function generateGrandTotal()
	{
		var total = parseFloat($("#sub_total").val().replace(/,/g, '')) - parseFloat($("#diskon_rp").val().replace(/,/g, ''));
		var grandTotal = Math.ceil(total / 100) * 100;
		$("#grand_total").val(grandTotal);
	}

	function checkSisaPembayaran()
	{
		if (parseFloat($("#sisa_pembayaran").val()) > 0) {
			$("#btnSubmit").attr('disabled', true);
		} else {
			$("#btnSubmit").attr('disabled', false);
		}
	}

	function generateSisaPembayaran()
	{
		var sisaPembayarn = parseFloat($("#grand_total").val()) - parseFloat($("#nominal_pembayaran").val());
		$("#sisa_pembayaran").val(sisaPembayarn);

		checkSisaPembayaran();
	}
</script>