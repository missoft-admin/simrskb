<?php echo ErrorSuccess($this->session)?>
<?php
    if ($error != '') {
        echo ErrorMessage($error);
    }
?>

<style>
	.control-label {
		text-align: left !important;
	}
</style>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
		<ul class="block-options">
			<li>
				<a href="{base_url}trm_pembayaran_informasi/index" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
	</div>
	<div class="block-content">

		<hr style="margin-top:-10px">

		<?php echo form_open('trm_pembayaran_informasi/save_transaction', 'class="form-horizontal" id="form-work"') ?>
		
		<div class="col-12">
			<div class="form-group">
				<label class="col-md-2 control-label" for="">No. Pembayaran</label>
				<div class="col-md-10">
					<input class="form-control" type="text" readonly placeholder="No. Pembayaran" value="{notransaksi}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-2 control-label" for="">Nama Rujukan</label>
				<div class="col-md-10">
					<input class="form-control" type="text" readonly placeholder="Nama Rujukan" value="{namarujukan}">
				</div>
			</div>
		
			<div class="form-group">
				<label class="col-md-2 control-label" for="">Tanggal Jatuh Tempo</label>
				<div class="col-md-10">
					<input class="form-control" type="text" readonly placeholder="Tanggal Jatuh Tempo" value="{tanggal_jatuhtempo}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-2 control-label" for="">Nominal</label>
				<div class="col-md-10">
					<input class="form-control" type="text" readonly placeholder="Nominal" value="{nominal}">
				</div>
			</div>
		</div>

		<hr>
	
		<div class="col-12">
			<table id="detailPembayaran" class="table table-striped table-bordered" style="margin-bottom: 0;">
				<thead>
					<tr>
						<td colspan="6"><label class="pull-right"><b>Total</b></label></td>
						<td><input class="form-control input-sm " readonly type="text" id="total" name="total" value="{nominal}"></td>
					</tr>
					<tr>
						<td colspan="6"></td>
						<td><button class="btn btn-success" id="btnModalPembayaran" data-toggle="modal"  data-target="#PembayaranModal"  type="button">Pilih Cara Pembayaran</button></td>
					</tr>
					<tr>
						<th style="width: 5%;">#</th>
						<th style="width: 10%;">Jenis Kas</th>
						<th style="width: 10%;">Sumber Kas</th>
						<th style="width: 10%;">Metode</th>
						<th style="width: 10%;">Nominal</th>
						<th style="width: 10%;">Keterangan</th>
						<th style="width: 10%;">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php $no = 1;?>
					<?php foreach ($detail_pembayaran as $row) {?>
						
					<?php }?>
				</tbody>
				<tfoot>
					<tr>
						<th colspan="6"><label class="pull-right"><b>Total Bayar</b></label></th>
						<th>
							<input type="text" class="form-control input-sm" readonly id="nominal_pembayaran" name="nominal_pembayaran" value="{nominal_pembayaran}">
						</th>
					</tr>
					<tr>
						<th colspan="6"><label class="pull-right"><b>Sisa Pembayaran</b></label></th>
						<th>
							<input type="text" class="form-control input-sm" readonly id="sisa_pembayaran" name="sisa_pembayaran" value="{sisa_pembayaran}">
						</th>
					</tr>
				</tfoot>
			</table>
		</div>

		<br><br>

		<div style="float: right;">	
			<button type="submit" class="btn btn-primary" id="btnSubmitWithVerification">
				<i class="fa fa-save"></i> Simpan & Verifikasi
			</button>
			<button type="submit" class="btn btn-success" id="btnSubmit">
				<i class="fa fa-save"></i> Simpan
			</button>
		</div>

		<br><br><br>
		
		<input type="hidden" id="rowindex" value="">
		<input type="hidden" id="nomor" value="">
		<input type="hidden" id="detail_pembayaran" name="detail_pembayaran" value="">

		<?php echo form_hidden('id', $id) ?>
		<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script>
	const idtransaksi = '{id}';
	
    $(document).ready(function() {
		$('.number').number(true, 0, '.', ',');
	});

	$(document).on("click", "#btnSubmit", function (e) {
		var detailPembayaran = $('table#detailPembayaran tbody tr').get().map(function (row) {
			return $(row).find('td').get().map(function (cell) {
				return $(cell).html();
			})
		})

		$("#detail_pembayaran").val(JSON.stringify(detailPembayaran));
	});
</script>
<?php $this->load->view('Trm_pembayaran_informasi/modal_pembayaran'); ?>