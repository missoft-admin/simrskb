<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>FAKTUR PEMBAYARAN INFORMASI MEDIS</title>
    <style>
        @page {
            margin-top: 1, 8em;
            margin-left: 0.8em;
            margin-right: 1.5em;
            margin-bottom: 2.5em;
        }

        table {
            font-family: "Courier", Arial, sans-serif;
            font-size: 20px !important;
            border-collapse: collapse !important;
            width: 100% !important;
        }

        p {
            font-family: "Courier", Arial, sans-serif;
            font-size: 17px !important;
        }

        h5 {
            font-family: "Courier", Arial, sans-serif;
            font-size: 18px !important;
            font-weight: normal !important;
            margin-top: 0;
            margin-bottom: 0;
            margin-left: 0;
            margin-right: 0;
        }

        td {
            padding: 0px;
            border: 0px solid #000 !important;
        }

        .header {
            font-family: "Courier", Arial, sans-serif;
            padding-left: 1px;
            font-size: 35px !important;
            font-weight: bold;
        }

        .detail {
            font-size: 18px !important;
            font-style: italic !important;
        }


        .text-header {
            font-size: 13px !important;

        }

        .border-bottom {
            border-bottom: 1px solid #000 !important;
        }

        .border-top {
            border-top: 1px solid #000 !important;
        }

        .border-full {
            border: 1px solid #000 !important;
        }

        .border-bottom-top {
            border-bottom: 1px solid #000 !important;
            border-top: 1px solid #000 !important;
        }

        .border-bottom-top-left {
            border-bottom: 1px solid #000 !important;
            border-top: 1px solid #000 !important;
            border-left: 1px solid #000 !important;
        }

        .border-left {
            border-left: 1px solid #000 !important;
        }

        /* border-thick */
        .border-thick-top {
            border-top: 2px solid #000 !important;
        }

        .border-thick-bottom {
            border-bottom: 2px solid #000 !important;
        }

        .border-dotted {
            border-width: 1px;
            border-bottom-style: dotted;
        }

        /* text-position */
        .text-center {
            text-align: center !important;
        }

        .text-left {
            text-align: left !important;
        }

        .text-top {
            vertical-align: top !important;
        }

        .text-right {
            text-align: right !important;
        }

        /* text-style */
        .text-italic {
            font-style: italic;
        }

        .text-bold {
            font-weight: bold;
        }

        fieldset {
            border: 1px solid #000;
            border-radius: 8px;
            box-shadow: 0 0 10px #000;
        }

        legend {
            background: #fff;
        }

        @media print {
            html,
            body {
                height: 99%;
            }
        }
    </style>
</head>

<body>
    <table>
        <tr>
            <td style="width:15%" rowspan="3" class="text-center"><img src="<?=base_url()?>assets/upload/logo/logo.png" alt="" style="width: 150px;height: 150px;"></td>
            <td colspan="7" class="text-center header">NOTA RINCIAN PEMBAYARAN</td>
        </tr>
        <tr>
            <td>NO REGISTER</td>
            <td>:</td>
            <td><?= $no_pengajuan; ?></td>
            <td></td>
            <td>TANGGAL PENGAJUAN</td>
            <td>:</td>
            <td><?= $tanggal_pengajuan; ?></td>
        </tr>
        <tr>
            <td style="width:17%">NAMA PEMOHON</td>
            <td style="width:1%">:</td>
            <td style="width:20%"><?= $nama_pemohon; ?></td>
            <td style="width:1%"></td>
            <td style="width:8%">JUMLAH</td>
            <td style="width:1%">:</td>
            <td style="width:20%"><?= number_format(COUNT($list_detail_pengajuan)); ?></td>
        </tr>
    </table>

    <br>

    <table class="content-2">
        <tr>
            <td class="border-bottom-top text-center" width="10%">NO </td>
            <td class="border-bottom-top text-center" width="10%">NO MEDREC</td>
            <td class="border-bottom-top text-center" width="10%">NAMA PASIEN</td>
            <td class="border-bottom-top text-center" width="10%">PENGAJUAN</td>
            <td class="border-bottom-top text-center" width="10%">TANGGAL KUNJUNGAN</td>
            <td class="border-bottom-top text-center" width="10%">NOMINAL</td>
            <td class="border-bottom-top text-center" width="10%">DISKON</td>
            <td class="border-bottom-top text-center" width="10%">NOMINAL AKHIR</td>
        </tr>
        <?php $number = 0; ?>
        <?php foreach  ($list_detail_pengajuan as $row) { ?>
            <?php $number = $number + 1; ?>
            <tr>
                <td class="border-bottom text-center"><?= $number; ?></td>
                <td class="border-bottom text-center"><?= $row->no_medrec; ?></td>
                <td class="border-bottom text-center"><?= $row->nama_pasien; ?></td>
                <td class="border-bottom text-center"><?= $row->jenis_pengajuan; ?></td>
                <td class="border-bottom text-center"><?= $row->tanggal_kunjungan; ?></td>
                <td class="border-bottom text-center"><?= $row->nominal; ?></td>
                <td class="border-bottom text-center"><?= $row->diskon; ?></td>
                <td class="border-bottom text-center"><?= $row->nominal_akhir; ?></td>
            </tr>
        <?php } ?>
    </table>
    <br>
    </table>

    <div style="clear:both; position:relative;">
        <div style="position:absolute; left:10pt; width:275pt;">
            <table>
                <tr>
                    <td colspan="2" class="border-full">Terbilang : <br> <i><?=strtoupper(terbilang($grand_total))?> RUPIAH</i> </td>
                </tr>
                <tr>
                    <td width="50%" class="text-center"> <br>Paraf Kasir<u><br><br><br><br><?= $user_created ?></u> </td>
                    <td width="50%"> </td>
                </tr>
                <br><br><br><br><br>
                <p><u>Cetakan Ke-<?= $print_version; ?>\UC:<?= $print_user; ?> \ <?=HumanDateLong(date('d-m-Y H:i:s'))?></u></p>
            </table>
        </div>
        <div style="margin-left:300pt;">
            <table>
                <tr>
                    <td class="border-bottom text-left">
                        <h5>SUB TOTAL</h5>
                    </td>
                    <td class="border-bottom text-center">: </td>
                    <td class="border-bottom text-right"><?=number_format($sub_total)?> </td>
                </tr>

                <tr>
                    <td class="border-bottom text-left">
                        <h5>DISCOUNT (RP)</h5>
                    </td>
                    <td class="border-bottom text-center">: </td>
                    <td class="border-bottom text-right"><?=number_format($discount)?> </td>
                </tr>

                <tr>
                    <td class="border-bottom text-left">
                        <h5>GRAND TOTAL</h5>
                    </td>
                    <td class="border-bottom text-center">: </td>
                    <td class="border-bottom text-right"><?=number_format($grand_total)?> </td>
                </tr>

                <tr>
                    <td colspan="3" class="text-left">
                        <h5>PEMBAYARAN</h5>
                    </td>
                </tr>
                <?php foreach(get_all('trm_pembayaran_informasi_detail', array('idpengajuan' => $idtransaksi)) as $row) { ?>
                    <tr>
                        <td class="text-left">
                            <h5><?= $row->keterangan; ?></h5>
                        </td>
                        <td class="text-center">: </td>
                        <td class="text-right"><?= number_format($row->nominal); ?></td>
                    </tr>
                <? } ?>
                <tr>
                    <td class="border-top text-left">
                        <h5>KEMBALI</h5>
                    </td>
                    <td class="border-top text-center">: </td>
                    <td class="border-top text-right"><?=number_format($selisih_pembayaran)?> </td>
                </tr>
            </table>
        </div>
    </div>

    <script type="text/javascript">
        try {
            window.print();
        } catch(e) {
            window.onload = window.print;
        }
    </script>
</body>

</html>