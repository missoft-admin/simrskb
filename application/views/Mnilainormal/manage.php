<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}mnilainormal" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content block-content-narrow">
        <?php echo form_open('mnilainormal/save','class="form-horizontal" id="form-work"') ?>
        <div class="form-group">
            <label class="col-md-3 control-label" for="example-hf-email">Nama</label>
            <div class="col-md-7">
                <input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="example-hf-email">Satuan</label>
            <div class="col-md-7">
                <div class="input-group">
                    <select name="idsatuan" id="idsatuan" class="js-select2 form-control" style="width: 98%;" data-placeholder="Pilih Opsi">
                        <option value="">Pilih Opsi</option>
                        <?php foreach  ($list_satuan as $row) { ?>
                        <option value="<?=$row->id?>" <?=($idsatuan == $row->id) ? "selected" : "" ?>><?=$row->nama?></option>
                        <?php } ?>
                    </select>
                    <span class="input-group-addon btn btn-primary" style="border-left: 1px solid #0f8699; border-radius: 3px;" data-toggle="modal" data-target="#modalAddSatuan"><i class="fa fa-plus"></i> Tambah</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="example-hf-email">Tarif Laboratorium</label>
            <div class="col-md-7">
                <select class="form-control" name="idtariflaboratorium[]" multiple id="idtariflaboratorium" value="">
                    <?php if($this->uri->segment(2) == 'update'){ ?>
                    <?php foreach  ($list_tariflab_selected as $row) { ?>
                    <option value="<?=$row->nama?>"><?=$row->nama?></option>
                    <?php } ?>
                    <?php } ?>
                </select>
                <script>
                $(document).ready(function() {
                    //Typehead Sample Code

                    // Basic Sample using Bloodhound
                    // constructs the suggestion engine
                    var states = new Bloodhound({
                        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                        queryTokenizer: Bloodhound.tokenizers.whitespace,
                        local: <?= $list_tariflab ?>
                    });
                    states.initialize();

                    $("#idtariflaboratorium").tagsinput({
                        allowDuplicates: false,
                        typeaheadjs: {
                            name: "states",
                            displayKey: 'name',
                            valueKey: 'name',
                            limit: 100,
                            source: states.ttAdapter()
                        },
                        freeInput: false
                    });

                });

                </script>
            </div>
        </div>
    </div>
    <div class="block-content">
        <hr>

        <input type="hidden" class="form-control" id="rowindex" />
        <input type="hidden" class="form-control" id="number" />

        <b><span class="label label-success" style="font-size:12px">NILAI NORMAL / RUJUKAN</span></b><br /><br />
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="detail_list">
                <thead>
                    <tr>
                        <th style="width:25%">Kriteria</th>
                        <th style="width:50%">Nilai</th>
                        <th style="width:25%">Aksi</th>
                    </tr>
                    <tr>
                        <th>
                            <input type="text" class="form-control" id="kriteria" placeholder="Kriteria" value="">
                        </th>
                        <th>
                            <input type="text" class="form-control" id="nilai" placeholder="Nilai" value="">
                        </th>
                        <th>
                            <a id="detail_add" class="btn btn-success">Tambahkan</a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($this->uri->segment(2) == 'update') { ?>
                    <?php foreach  ($list_kriteria as $row) { ?>
                    <tr>
                        <td><?=$row->kriteria;?></td>
                        <td><?=$row->nilai;?></td>
                        <td><a href="#" class="detail_edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;<a href='#' class='detail_remove' data-toggle='tooltip' title='Remove'><i class='fa fa-trash-o'></i></a></td>
                    </tr>
                    <?php } ?>
                    <?php } ?>
                </tbody>
            </table>
        </div>

        <b><span class="label label-success" style="font-size:12px">NILAI KRITIS</span></b><br /><br />
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="detail_kritis_list">
                <thead>
                    <tr>
                        <th style="width:25%">Kriteria</th>
                        <th style="width:50%">Nilai</th>
                        <th style="width:25%">Aksi</th>
                    </tr>
                    <tr>
                        <th>
                            <input type="text" class="form-control" id="kritis_kriteria" placeholder="Kriteria" value="">
                        </th>
                        <th>
                            <input type="text" class="form-control" id="kritis_nilai" placeholder="Nilai" value="">
                        </th>
                        <th>
                            <a id="detail_kritis_add" class="btn btn-success">Tambahkan</a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($this->uri->segment(2) == 'update') { ?>
                    <?php foreach  ($list_kritis as $row) { ?>
                    <tr>
                        <td><?=$row->kriteria;?></td>
                        <td><?=$row->nilai;?></td>
                        <td><a href="#" class="detail_edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;<a href='#' class='detail_kritis_remove' data-toggle='tooltip' title='Remove'><i class='fa fa-trash-o'></i></a></td>
                    </tr>
                    <?php } ?>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row" style="margin-top:-25px">
        <div class="block-content block-content-narrow">
            <div class="form-group">
                <label class="col-md-3 control-label"></label>
                <div class="col-md-7">
                    <button class="btn btn-success" type="submit">Simpan</button>
                    <a href="{base_url}mnilainormal" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
                </div>
            </div>
        </div>
    </div>

    <br><br>
    
		<input type="hidden" id="kriteria_value" name="kriteria_value" />
    <input type="hidden" id="kritis_value" name="kritis_value" />

    <?php echo form_hidden('id', $id); ?>
    <?php echo form_close() ?>
</div>

<div class="modal" id="modalAddSatuan" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal" id="form_satuan">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">Tambah Data Satuan</h3>
                    </div>
                    <div class="block-content">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="nama">Nama</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="namasatuan" placeholder="Nama" name="nama" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="nama">Singkatan</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="singkatan" placeholder="Singkatan" name="singkatan" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="idkategori">Kategori</label>
                            <div class="col-md-7">
                                <select name="idkategori" id="idkategori" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="2">Laboratorium</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success" id="simpanSatuan" type="button" data-dismiss="modal">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{js_path}plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<script src="{js_path}plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<link href="{js_path}plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css" rel="stylesheet" type="text/css">
<link href="{js_path}plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
$(document).ready(function() {
    $("#detail_add").click(function() {
        var valid = validateDetail();
        if (!valid) return false;

        var row_index;
        var number;
        var duplicate = false;

        if ($("#rowindex").val() != '') {
            var content = "";
            number = $("#number").val();
            row_index = $("#rowindex").val();
        } else {
            var content = "<tr>";
            number = $('#number').val();
            $('#detail_list tbody tr').filter(function() {
                var $cells = $(this).children('td');
                if ($cells.eq(1).text() === $("#kriteria").val()) {
                    sweetAlert("Maaf...", "Kriteria " + $("#kriteria").val() + " sudah ditambahkan.", "error");
                    duplicate = true;
                    return false;
                } else {
                    no = $("#number").val();
                }
            });
        }

        if (duplicate == false) {
            content += "<td>" + $("#kriteria").val() + "</td>";
            content += "<td>" + $("#nilai").val() + "</td>";
            content += "<td><a href='#' class='detail_edit' data-toggle='tooltip' title='Edit'><i class='fa fa-pencil'></i></a>&nbsp;&nbsp;<a href='#' class='detail_remove' data-toggle='tooltip' title='Remove'><i class='fa fa-trash-o'></i></a></td>";

            if ($("#rowindex").val() != '') {
                $('#detail_list tbody tr:eq(' + row_index + ')').html(content);
            } else {
                content += "</tr>";
                $('#detail_list tbody').append(content);
            }

            detailClear();
        }
    });

    $(document).on("click", ".detail_edit", function() {
        $("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);

        $("#kriteria").val($(this).closest('tr').find("td:eq(0)").html());
        $("#nilai").val($(this).closest('tr').find("td:eq(1)").html());

        return false;
    });

    $(document).on("click", ".detail_remove", function() {
        if (confirm("Hapus data ?") == true) {
            $(this).closest('td').parent().remove();
        }
        return false;
    });

    $("#detail_kritis_add").click(function() {
        var valid = validateKritisDetail();
        if (!valid) return false;

        var row_index;
        var number;
        var duplicate = false;

        if ($("#rowindex").val() != '') {
            var content = "";
            number = $("#number").val();
            row_index = $("#rowindex").val();
        } else {
            var content = "<tr>";
            number = $('#number').val();
            $('#detail_kritis_list tbody tr').filter(function() {
                var $cells = $(this).children('td');
                if ($cells.eq(1).text() === $("#kritis_kriteria").val()) {
                    sweetAlert("Maaf...", "Kriteria " + $("#kritis_kriteria").val() + " sudah ditambahkan.", "error");
                    duplicate = true;
                    return false;
                } else {
                    no = $("#number").val();
                }
            });
        }

        if (duplicate == false) {
            content += "<td>" + $("#kritis_kriteria").val() + "</td>";
            content += "<td>" + $("#kritis_nilai").val() + "</td>";
            content += "<td><a href='#' class='detail_kritis_edit' data-toggle='tooltip' title='Edit'><i class='fa fa-pencil'></i></a>&nbsp;&nbsp;<a href='#' class='detail_kritis_remove' data-toggle='tooltip' title='Remove'><i class='fa fa-trash-o'></i></a></td>";

            if ($("#rowindex").val() != '') {
                $('#detail_kritis_list tbody tr:eq(' + row_index + ')').html(content);
            } else {
                content += "</tr>";
                $('#detail_kritis_list tbody').append(content);
            }

            detailKritisClear();
        }
    });

    $(document).on("click", ".detail_kritis_edit", function() {
        $("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);

        $("#kritis_kriteria").val($(this).closest('tr').find("td:eq(0)").html());
        $("#kritis_nilai").val($(this).closest('tr').find("td:eq(1)").html());

        return false;
    });

    $(document).on("click", ".detail_kritis_remove", function() {
        if (confirm("Hapus data ?") == true) {
            $(this).closest('td').parent().remove();
        }
        return false;
    });

    $("#form-work").submit(function(e) {
        var form = this;

        if ($("#nama").val() == "") {
            e.preventDefault();
            sweetAlert("Maaf...", "Nama Nilai Normal tidak boleh kosong!", "error");
            return false;
        }

        if ($("#idtariflaboratorium").val() == null) {
            e.preventDefault();
            sweetAlert("Maaf...", "Tarif Laboratorium tidak boleh kosong!", "error");
            return false;
        }

        var detail_tbl = $('table#detail_list tbody tr').get().map(function(row) {
            return $(row).find('td').get().map(function(cell) {
                return $(cell).html();
            });
        });

        var detail_kritis_tbl = $('table#detail_kritis_list tbody tr').get().map(function(row) {
            return $(row).find('td').get().map(function(cell) {
                return $(cell).html();
            });
        });

        $("#kriteria_value").val(JSON.stringify(detail_tbl));
        $("#kritis_value").val(JSON.stringify(detail_kritis_tbl));

        swal({
            title: "Berhasil!",
            text: "Proses penyimpanan data.",
            type: "success",
            timer: 1500,
            showConfirmButton: false
        });
    });

    $(document).on("click", "#simpanSatuan", function() {
        $.post("{base_url}msatuan/save", {
                nama: $("#namasatuan").val(),
                singkatan: $("#singkatan").val(),
                idkategori: $("#idkategori").val()
            },
            function(data, status) {
                swal({
                    title: "Berhasil!",
                    text: "Data Satuan telah tersimpan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                // Clear Form
                $("#namasatuan").val('');
                $("#singkatan").val('');
                $("#idkategori").select2('val', '#');

                // Refresh Satuan
                $.ajax({
                    url: "{site_url}mnilainormal/getSatuanLab",
                    method: "GET",
                    dataType: "json",
                    success: function(data) {
                        $("#idsatuan").empty();
                        $("#idsatuan").append("<option value='#' selected disabled>Pilih Opsi</option>");
                        for (var i = 0; i < data.length; i++) {
                            $("#idsatuan").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
                        }
                        $("#idsatuan").selectpicker('refresh');
                    }
                });

            });
    });
});

function detailClear() {
    $("#rowindex").val('');
    $("#number").val('');
    $("#kriteria").val('');
    $("#nilai").val('');
}

function detailKritisClear() {
    $("#rowindex").val('');
    $("#number").val('');
    $("#kritis_kriteria").val('');
    $("#kritis_nilai").val('');
}

function validateDetail() {
    if ($("#kriteria").val() == "") {
        sweetAlert("Maaf...", "Kriteria tidak boleh kosong!", "error");
        return false;
    }

    if ($("#nilai").val() == "") {
        sweetAlert("Maaf...", "Nilai tidak boleh kosong!", "error");
        return false;
    }

    return true;
}

function validateKritisDetail() {
    if ($("#kritis_kriteria").val() == "") {
        sweetAlert("Maaf...", "Kriteria tidak boleh kosong!", "error");
        return false;
    }

    if ($("#kritis_nilai").val() == "") {
        sweetAlert("Maaf...", "Nilai tidak boleh kosong!", "error");
        return false;
    }

    return true;
}

</script>
