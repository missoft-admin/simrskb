<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mdata_kategori/index" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mdata_kategori/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="idtipe">Tipe</label>
				<div class="col-md-7">
					<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<?php $get_kategori_barang = $this->Mdata_kategori_model->get_kategori_barang() ?>
						<option value="">Pilih Opsi</option>
						<?php if ($get_kategori_barang): ?>
							<?php foreach ($get_kategori_barang as $row): ?>
								<option value="<?php echo $row['idtipe'] ?>" <?=($idtipe == $row['idtipe'] ? 'selected="selected"':'')?>><?php echo $row['nama'] ?></option>
							<?php endforeach ?>
						<?php endif ?>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="idparent">Parent</label>
				<div class="col-md-7">
					<select name="idparent" id="idparent" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="0" <?=($idparent == '0'?'selected="selected"':'')?>>ROOT [TIDAK MEMILIKI PARENT]</option>
						<?foreach($list_parent  as $row){?>
							<option value="<?php echo $row->id ?>" <?=($idparent == $row->id ? 'selected="selected"':'')?>><?php echo $row->nama ?></option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group" id="form-margin" style="display:none">
				<label class="col-md-3 control-label" for="margin">Margin</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="margin" placeholder="Margin" name="margin" value="{margin}">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mdata_kategori/index" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script type="text/javascript">
	var idtipe = <?=$idtipe?>;

  $(document).ready(function() {
		if(idtipe == 2){
			$("#form-margin").show();
		}else{
			$("#form-margin").hide();
		}

		$(document).on("change","#idtipe",function(){
			if($(this).val() == 2){
				$("#form-margin").show();
			}else{
				$("#form-margin").hide();
			}
			if ($(this).val()!=''){
				$('#idparent')
					.find('option')
					.remove()
					.end()
					.append('<option value="">- Pilih Parent -</option>')
					.val('').trigger("liszt:updated");

				$.ajax({
				url: '{site_url}mdata_kategori/find_parent/'+$(this).val(),
				dataType: "json",
				success: function(data) {
				$('#idparent').append('<option value="0">ROOT [TIDAK MEMILIKI PARENT]');
				$.each(data.detail, function (i,row) {
				$('#idparent').append('<option value="' + row.id + '">' + row.nama + '</option>');
				});
				}
				});
			}

		});
	});
</script>
