<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('48'))){ ?>
<div class="block">
	<div class="block-header">
	<?php if (UserAccesForm($user_acces_form,array('50'))){ ?>
		<ul class="block-options">
        <li>
            <a href="{base_url}mdata_kategori/create/<?=$this->uri->segment(3)?>" class="btn"><i class="fa fa-plus"></i></a>
        </li>
    </ul>
	<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<hr style="margin-top:0px">
		<?php if (UserAccesForm($user_acces_form,array('49'))){ ?>
		<div class="row">
			<div class="col-md-6">
				<?php echo form_open('mdata_kategori/filter','class="form-horizontal" id="form-work"') ?>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien">Kategori</label>
					<div class="col-md-8">
						<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<?php $get_kategori_barang = $this->Mdata_kategori_model->get_kategori_barang() ?>
							<option value="#">Semua Kategori</option>
							<?php if ($get_kategori_barang): ?>
								<?php foreach ($get_kategori_barang as $row): ?>
									<option value="<?php echo $row['idtipe'] ?>" <?=($idtipe == $row['idtipe'] ? 'selected="selected"':'')?>><?php echo $row['nama'] ?></option>
								<?php endforeach ?>
							<?php endif ?>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
		<hr>
		<?}?>
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Kategori</th>
					<th>Parent</th>
					<th>Margin</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		</div>
	</div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mdata_kategori/getIndex/' + "<?=$this->uri->segment(2)?>",
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "10%", "targets": 0, "orderable": true },
					{ "width": "20%", "targets": 1, "orderable": true },
					{ "width": "20%", "targets": 2, "orderable": true },
					{ "width": "20%", "targets": 3, "orderable": true },
					{ "width": "20%", "targets": 4, "orderable": true },
					{ "width": "15%", "targets": 5, "orderable": true }
				]
			});
	});
</script>
