<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>
<?php if (UserAccesForm($user_acces_form,array('1165'))){?>
<div class="block" id="div_header">
    <div class="block-header">		
        <h3 class="block-title">Peminjaman Berkas</h3>
		<hr style="margin-top:15px">
        <div class="row">
            <?php echo form_open('trm_pinjam_berkas/simpan', 'class="form-horizontal" id="form" onsubmit="return validate_final()"'); ?>
            <div class="col-md-12">
                
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-2 control-label" for="nomedrec">User</label>
                    <div class="col-md-5">
                        <input type="text" readonly id="user" name="user" class="form-control" value="<?=$this->session->userdata('user_name')?>">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-2 control-label" for="idjenis">Yang Meminjam</label>
                    <div class="col-md-5">
                        <select id="iddokter" name="iddokter" class="form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Pilih Peminjam -</option>
							
                        </select>
                    </div>
					<div class="col-md-5">
                        <input type="hidden" readonly id="idtipe" name="idtipe" class="form-control" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-2 control-label" for="idjenis">Catatan</label>
                    <div class="col-md-8">
                        <textarea class="form-control js-summernote-custom-height" id="tcatatan" placeholder="Catatan" name="tcatatan" rows="5"></textarea>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-2 control-label" for="idjenis">Pasien</label>
                    <div class="col-md-10">
                        <table id="tabel" class="table table-striped table-bordered" style="margin-bottom: 0;">
							<thead>
								<tr>
									<th style="width: 30%;">PASIEN</th>
									<th style="width: 15%;">TANGGAL LAHIR</th>
									<th style="width: 45%;">ALAMAT</th>
									<th style="width: 10%;">Actions</th>
								</tr>
								<tr>
									<td>
										<div class="input-group">
											<select name="idpasien" tabindex="16"  style="width: 100%" id="idpasien" data-placeholder="Cari Pasien" class="form-control input-sm">
												<option value="#" selected>-Pilih Pasien-</option>
												
											</select>
											<span class="input-group-btn">
												<button tabindex="17" class="btn btn-info" type="button" id="btn_search"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</td>
									<td><input type="text" name="tanggal_lahir" id="tanggal_lahir" class="form-control "  value="" readonly></td>
									<td><input type="text" name="alamat" id="alamat" class="form-control "  value="" readonly></td>
									<td>
										<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="add" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
										<button type="button" class="btn btn-sm btn-danger" tabindex="9" id="cancel" title="Refresh"><i class="fa fa-times"></i></button>
									</td>
									<input type="hidden" name="nomor" id="nomor" class="form-control"  value="" readonly>
									<input type="hidden" name="row" id="row" class="form-control"  value="" readonly>
									<input type="hidden" name="auto_number" id="auto_number" class="form-control"  value="1" readonly>
								</tr>
							</thead>
							<tbody>								
										
							</tbody>
							<tfoot id="foot-total-nonracikan">
								<tr>
									
									<th class="hidden-phone">
										<span class="pull-right total"><b>TOTAL PASIEN</b></span></th>											
									<th ><b><span id="lbl_total" class="total_red">0</span></b></th>
									<th ></th>
									<th colspan="2">
											<input type="hidden" id="gt" name="gt" value="0">
											<input type="hidden" id="gt_persen" name="gt_persen" value="0">
									</th>
								</tr>
							</tfoot>
					</table>
                    </div>
                </div>
				
				<hr>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="status_kirim"></label>
					<?php if (UserAccesForm($user_acces_form,array('1166'))){?>
                    <div class="col-md-2">
                        <button class="btn btn-primary text-uppercase" type="submit" id="btn_save" name="btn_save" style="font-size:13px;width:100%;float:left;"><i class="fa fa-save"></i> Save </button>
                    </div>
					<?}?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<?}?>
<div class="modal in" id="modal_pasien" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width:70%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Data Pasien</h3>
				</div>
				<div class="block-content">
					<hr>
					<div class="row">
						<div class="form-horizontal" id="filter-datatable">
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snomedrec">No. Medrec</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snomedrec" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snamapasien">Nama Pasien</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snamapasien" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snama_keluarga">Penanggung Jawab</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snama_keluarga" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="sjeniskelamin">Jenis Kelamin</label>
									<div class="col-md-8">
										<button class="btn btn-success text-uppercase" type="button" id="btn_filter" name="btn_filter" style="font-size:13px;width:100%;float:left;"><i class="fa fa-filter"></i> Filter </button>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="salamat">Alamat</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="salamat" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="stanggallahir">Tanggal Lahir</label>
									<div class="col-md-8">
										<input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="stanggallahir" value="">
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label" for="snotelepon">Telepon</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snotelepon" value="">
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>

					<table width="100%" class="table table-bordered table-striped" id="datatable-pasien">
						<thead>
							<tr>
								<th>No. Medrec</th>
								<th>Nama</th>
								<th>Jenis Kelamin</th>
								<th>Tanggal Lahir</th>
								<th>Alamat</th>
								<th>Telepon</th>
								<th>Keluarga</th>
								<th>Tlp. Keluarga</th>
							</tr>
						</thead>
						<tbody id="pasienmodaltabel">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- BATAS HAPUS -->

<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-/jquery..min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {      
		clear();
    });
	$("#idpasien").select2({
		minimumInputLength: 3,
		noResults: 'Pasien Tidak Ditemukan.',
		ajax: {
			url: '{site_url}trm_pinjam_berkas/js_pasien/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,
		 data: function (params) {
			  var query = {
				search: params.term
			  }
			  return query;
			},
			processResults: function (data) {
				// console.log($("#idunit").val());
				return {
					results: $.map(data, function (item) {
						return {
							text: item.text,
							id: item.id,
						}
					})
				};
			}
		}
	});
	$("#iddokter").select2({
			noResults: 'Dokter Tidak ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}trm_pinjam_berkas/get_dokter_pegawai/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,
				  }
				  return query;
				},
				processResults: function (data) {
					// console.log(data);
					return {
						results: $.map(data, function (item) {
							return {
								text: item.nama,
								id: item.id,
								tipe : item.tipe
							}
						})
					};
				}
			}
		});
	$("#iddokter").change(function(){
		data=$("#iddokter").select2('data')[0];
		var tipe_dokter=data.tipe;
		$("#idtipe").val(tipe_dokter);
		
	});
	$("#idpasien").change(function(){
			
		if ($(this).val()!=null){
			$.ajax({
			  url: '{site_url}trm_pinjam_berkas/get_pasien/'+$(this).val(),
			  dataType: "json",
			  success: function(data) {
				  console.log(data);
				$("#tanggal_lahir").val(data.tanggal_lahir);
				$("#alamat").val(data.alamat_jalan);
				
			  }
			});
		}else{

		}
	});
	function clear(){
		$("#idpasien").val(null).trigger('change');
		$("#tanggal_lahir").val('');
		$("#alamat").val('');
		$("#row").val('');
		$("#nomor").val('');
		
		$("#lbl_total").text($('#tabel tr').length-3);
		$("#btn_search").attr('disabled',false);
		$(".edit").attr('disabled',false);
		$(".hapus").attr('disabled',false);
	}
	$("#add").click(function() {
			if(!validate()) return false;
			var duplicate = false;
			var content = "";
			if($("#nomor").val() != ''){
					var no = $("#nomor").val();
			}else{
				// var no = $('#manage-tabel-nonRacikan tr').length - 2;
				var no = $('#auto_number').val();
				var content = "<tr>";
				$('#tabel tbody tr').filter(function (){
					var $cells = $(this).children('td');
					if($cells.eq(1).text() === $("#idpasien").val()){
						// big_notification("Barang sudah ada didalam daftar","error");
						duplicate = true;
						sweetAlert("Duplicate...", "Data Pasien Duplicate!", "error");
					}
				});
			}
			if(duplicate) return false;
			
			content += "<td style='display:none'>" + no + "</td>";//0 nomor
			content += "<td style='display:none'>" + $("#idpasien").val(); + "</td>";//1 UNIT
			content += "<td>" + $("#idpasien option:selected").text() + "</td>";//2 Nama 
			content += "<td>" + $("#tanggal_lahir").val() + "</td>";//3 Nama 
			content += "<td>" + $("#alamat").val() + "</td>";//4 Nama 			
			content += "<td ><button type='button' class='btn btn-sm btn-info edit'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus'><i class='glyphicon glyphicon-remove'></i></button></td>";
			content += '<td style="display:none"><input type="text" name="idpasien_detail[]" value="'+ $("#idpasien").val() +'" ></td>';//6 Persen
			// alert(content);
			if($("#row").val() != ''){
				$('#tabel tbody tr:eq(' + $("#row").val() + ')').html(content);
			}else{
				content += "</tr>";
				$('#tabel tbody').append(content);
			}

			$(".edit").attr('disabled',false);
			$(".hapus").attr('disabled',false);
			$("#auto_number").val(parseInt($("#auto_number").val()) + 1);
			clear();
	});
	$("#btn_search").click(function() {			
			$('#modal_pasien').modal('show');
	});
	$("#cancel").click(function() {			
			clear();
	});
	$(document).on("click",".hapus",function(){
		var td=$(this).closest('td');
		var tr=$(this).closest('tr');
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {			
			td.parent().remove();
			clear();
		});
		
		
	});
	$(document).on("click", ".selectPasien", function() {
		var idpasien = ($(this).data('idpasien'));
		$.ajax({
			url: "{site_url}trm_pinjam_berkas/get_pasien/" + idpasien,
			method: "POST",
			dataType: "json",
			success: function(data) {
				
				$("#tanggal_lahir").val(data.tanggal_lahir);
				$("#alamat").val(data.alamat_jalan);
				var data2 = {
					id: idpasien,
					text: data.no_medrec+' - '+data.title+'. '+data.nama,
				};
				var newOption = new Option(data2.text, data2.id, true, true);
				$('#idpasien').append(newOption);
				

			}
		});
		return false;
	});
	
	$(document).on("click",".edit",function(){
		$(".edit").attr('disabled',true);
		$(".hapus").attr('disabled',true);
		$("#btn_search").attr('disabled',true);
		$id = $(this).closest('tr').find("td:eq(1)").html();
		$nama = $(this).closest('tr').find("td:eq(2)").html();
		var data2 = {
			id: $id,
			text: $nama,
		};
		var newOption = new Option(data2.text, data2.id, true, true);
		$('#idpasien').append(newOption);
		$('#tanggal_lahir').val($(this).closest('tr').find("td:eq(3)").html());
		$('#alamat').val($(this).closest('tr').find("td:eq(4)").html());
		
		$("#nomor").val($(this).closest('tr').find("td:eq(0)").html());		
		$("#row").val($(this).closest('tr')[0].sectionRowIndex);
		
		
	});
	$(document).on("click", "#btn_filter", function() {
		loadDataPasien();
	});
	function loadDataPasien() {
		var snomedrec=$('#snomedrec').val();	
		var	snamapasien=$('#snamapasien').val();
		var snama_keluarga=$('#snama_keluarga').val();
		var salamat=$('#salamat').val();
		var	stanggallahir=$('#stanggallahir').val();
		var	snotelepon=$('#snotelepon').val();
		var tablePasien = $('#datatable-pasien').DataTable({
			"pageLength": 8,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}tpoliklinik_pendaftaran/getPasien',
				type: "POST",
				dataType: 'json',
				data: {
					snomedrec: snomedrec,
					snamapasien: snamapasien,
					snama_keluarga: snama_keluarga,
					salamat: salamat,
					stanggallahir: stanggallahir,
					snotelepon: snotelepon,
				}
			},
			"columnDefs": [{
				"targets": [0],
				"orderable": true
			}]
		});
	}
	function validate()
	{
		
		if($("#idpasien").val() == "" && $("#id").val() == null){
			sweetAlert("Maaf...", "Pasien Harus Diisi!", "error");
			$("#id").focus();
			return false;
		}
		
		return true;
	}
	function validate_final()
	{
		// alert($("#tcatatan").val());
		
		if($("#iddokter").val() == "" || $("#iddokter").val() == null || $("#iddokter").val() == '#'){
			sweetAlert("Maaf...", "Peminjam Harus Diisi!", "error");
			$("#id").focus();
			return false;
		}
		if($("#tcatatan").val() == "" || $("#tcatatan").val() == null){
			sweetAlert("Maaf...", "Catatan Harus Diisi!", "error");
			$("#id").focus();
			return false;
		}
		if($("#lbl_total").text() == "0"){
			sweetAlert("Maaf...", "Tabel Pasien Harus Diisi!", "error");
			$("#id").focus();
			return false;
		}
		
		return true;
	}
</script>