<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mjadwal_dokter" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mjadwal_dokter/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="">Dokter</label>
				<div class="col-md-7">
					<select disabled class="js-select2 form-control" style="width: 100%;">
						<?php foreach (get_all('mdokter', array('status' => 1)) as $row) { ?>
							<option value="<?=$row->id?>" <?=($iddokter == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="">Poliklinik</label>
				<div class="col-md-7">
					<select disabled class="js-select2 form-control" style="width: 100%;">
						<?php foreach (get_all('mpoliklinik', array('status' => 1)) as $row) { ?>
							<option value="<?=$row->id?>" <?=($idpoliklinik == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="">Hari</label>
				<div class="col-md-7">
					<select name="hari" <?=($idjadwal!=''?'disabled':'')?> class="js-select2 form-control" style="width: 100%;">
						<option value="Senin" <?= ($hari == 'Senin' ? 'selected' : '') ?>>Senin</option>
						<option value="Selasa" <?= ($hari == 'Selasa' ? 'selected' : '') ?>>Selasa</option>
						<option value="Rabu" <?= ($hari == 'Rabu' ? 'selected' : '') ?>>Rabu</option>
						<option value="Kamis" <?= ($hari == 'Kamis' ? 'selected' : '') ?>>Kamis</option>
						<option value="Jumat" <?= ($hari == 'Jumat' ? 'selected' : '') ?>>Jumat</option>
						<option value="Sabtu" <?= ($hari == 'Sabtu' ? 'selected' : '') ?>>Sabtu</option>
						<option value="Minggu" <?= ($hari == 'Minggu' ? 'selected' : '') ?>>Minggu</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="">Jam</label>
				<div class="col-md-7">
					<div class="input-group">
						<input class="form-control input_time" type="text" name="jam_dari" placeholder="Jam Dari" value="{jam_dari}">
						<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
						<input class="form-control input_time" type="text" name="jam_sampai" placeholder="Jam Sampai" value="{jam_sampai}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="">Ruangan</label>
				<div class="col-md-7">
					<select name="idruangan" class="js-select2 form-control" style="width: 100%;">
						<?php foreach (get_all('mruangan', array('status' => 1)) as $row) { ?>
							<option value="<?=$row->id?>" <?=($idruangan == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="">Keterangan</label>
				<div class="col-md-7">
					<input class="form-control" type="text" name="keterangan" placeholder="Keterangan" value="{keterangan}">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Tambahkan</button>
				</div>
			</div>

			<hr>

			<table class="table table-bordered">
				<thead>
					<tr>
						<td class="text-center" style="font-weight: bold;" width="10%">Hari</td>
						<td class="text-center" style="font-weight: bold;" width="10%">Jadwal</td>
						<td class="text-center" style="font-weight: bold;" width="15%">Ruangan</td>
						<td class="text-center" style="font-weight: bold;" width="15%">Keterangan</td>
						<td class="text-center" style="font-weight: bold;" width="5%">Kuota</td>
						<td class="text-center" style="font-weight: bold;" width="15%">Note</td>
						<td class="text-center" style="font-weight: bold;" width="10%">Aksi</td>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($list_jadwal as $row) { ?>
						<tr>
							<td class="text-center"><?= $row->hari; ?></td>
							<td class="text-center">
								<label class="label label-primary">
									<?= $row->jam_dari; ?> - <?= $row->jam_sampai; ?>
								</label>
							</td>
							<td><?= $row->ruangan; ?></td>
							<td><?= $row->keterangan; ?></td>
							<td><?= $row->kuota; ?></td>
							<td><?= $row->notes; ?></td>
							<td>
								<div class="btn-group">
									<a href="<?= base_url(); ?>mjadwal_dokter/update/<?= $row->iddokter; ?>/<?= $row->idpoliklinik; ?>/<?= $row->idjadwal; ?>" data-toggle="tooltip" title="Ubah" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>
									<a href="<?= base_url(); ?>mjadwal_dokter/delete_detail/<?= $row->iddokter; ?>/<?= $row->idpoliklinik; ?>/<?= $row->idjadwal; ?>" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
								</div>
							</td>
						</tr>
					<? } ?>
				</tbody>
			</table>
			<br>
			<?php echo form_hidden('idjadwal', $idjadwal); ?>
			<?php echo form_hidden('iddokter', $iddokter); ?>
			<?php echo form_hidden('idpoliklinik', $idpoliklinik); ?>
			<?php echo form_close() ?>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
	$(".input_time").datetimepicker({
		format: "HH:mm",
		stepping: 30
	});
</script>
