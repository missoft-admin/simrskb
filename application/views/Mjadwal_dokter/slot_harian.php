<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<style>
	.scrollme {
		overflow-x: auto;
	}
    .profile-dockter {
        background-color: #379ee7;
    }
    .profile-dockter .image {
        padding: 10px 10px 0 10px;
    }
    .profile-dockter .image img {
        width: 200px;
    }
    .profile-dockter .information {
        text-align: center;
        padding: 16px;
        color: #fff;
    }
    .profile-dockter .information .name {
        font-weight: bold;
        font-size: 14px;
    }
    .profile-dockter .information .uniqueId {
        font-size: 12px;
    }

    .timetable {
        color: #fff;
        width: 100%;
        padding: 8px;
        margin-bottom: 10px;
    }
    .timetable .time {
        font-size: 14px;
        font-weight: bold;
    }
    .timetable.success {
        background-color: #46c37b;
    }
    .timetable.danger {
        background-color: #c54736;
    }
	.timetable.default {
        background-color: #7a7a7a;
    }
    .column-day {
        background-color: #000000;
        font-weight: bold;
        color: #fff;
    }
	.column-libur {
        background-color: #f31313;
        font-weight: bold;
        color: #fff;
    }
	.column-poli {
        background-color: #379ee7;
        font-weight: bold;
        color: #fff;
    }

    .header-section {
        padding: 8px;
        font-weight: bold;
        text-decoration: underline;
        text-transform: uppercase;
    }
</style>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li><a href="{base_url}app_setting/reservasi" title="Kembali"><i class="fa fa-reply"></i></a></li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
		
	<div class="block-content">
		<div class="block">
				<?php echo form_open('#','class="form-horizontal push-5-t" id="form-work"') ?>
				<div class="row">
					<div class="col-md-2">
						<div class="form-group">
							<label class="col-md-12" for="login1-username">Tujuan Kunjungan</label>
							<div class="col-md-12">
								<select id="idpoli" name="idpoli" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" <?=($idpoli=='#'?'selected':'')?>>-Semua Poliklinik-</option>
									<?foreach($list_poli as $r){?>
									<option value="<?=$r->idpoli?>" <?=($idpoli==$r->idpoli?'selected':'')?>><?=$r->nama?></option>
									<?}?>
								</select>
							</div>
						</div>
						
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label class="col-md-12" for="login1-username">Dokter</label>
							<div class="col-md-12">
								<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" <?=($iddokter=='#'?'selected':'')?>>-Semua Dokter-</option>
									<?foreach($list_dokter as $r){?>
									<option value="<?=$r->iddokter?>" <?=($iddokter==$r->iddokter?'selected':'')?>><?=$r->nama?></option>
									<?}?>
								</select>
							</div>
						</div>
						
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label class="col-md-12" for="login1-username">View</label>
							<div class="col-md-12">
								<select id="present" name="present" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									
									<option value="1" <?=($present=="1"?'selected':'')?>>MONTHLY</option>
									<option value="2" <?=($present=="2"?'selected':'')?>>WEEKLY</option>
									<option value="3" <?=($present=="3"?'selected':'')?>>DAILY</option>
									
								</select>
							</div>
						</div>
						
					</div>
					<div class="col-md-2 div_present">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-12" for="login1-username">&nbsp;</label>
							<div class="col-md-12">
							<div class="btn-group btn-group-justified">
								<div class="btn-group">
									<button class="btn btn-default" id="btn_back2" type="button"><i class="fa fa-arrow-left"></i></button>
								</div>
								
								<div class="btn-group">
									<button class="btn btn-default" id="btn_next2" type="button"><i class="fa fa-arrow-right"></i></button>
								</div>
								
							</div>
							</div>
						</div>
						
					</div>
					<div class="col-md-3 div_date">
						<div class="form-group">
							<label class="col-md-12" for="login1-username">Date</label>
							<div class="col-md-12">
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" id="tanggaldari" name="tanggaldari" placeholder="From" value="{tanggaldari}">
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" id="tanggalsampai" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
								</div>
							</div>
						</div>
						
					</div>
					<div class="col-md-1 div_date">
						<div class="form-group">
							<label class="col-md-12" for="login1-username">&nbsp;</label>
							<div class="col-md-12">
								<button class="btn btn-success present" id="btn_filter_ls" type="button"><i class="fa fa-search"></i> Cari Jadwal</button>
							</div>
						</div>
						
					</div>
				</div>
				<div class="row push-20-t">
					<div class="col-md-12">
						<div class="scrollme">
						<div id="div_ls"></div>
						</div>
					</div>
				</div>
				<?php echo form_close() ?>
			</div>
		
			
  </div>
</div>
<div class="modal" id="modal_edit" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width:50%">
        <form class="form-horizontal" action="javascript:void(0)" id="frm1">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-success">
                        <h3 class="block-title">Detail Slot</h3>
                    </div>
                    <div class="block-content">
						<div class="block">
						<ul class="nav nav-tabs" data-toggle="tabs" id="tabs">
							<li class="active" id="tab_kuota">
								<a href="#btabs-animated-slideleft-kuota"><i class="fa fa-pencil"></i> Pengaturan Slot</a>
							</li>
							<li id="tab_add">
								<a href="#btabs-animated-slideleft-note"><i class="fa fa-pencil"></i> Update Notes</a>
							</li>
							
						</ul>
						<div class="block-content tab-content">
							<div class="tab-pane fade fade-left in active" id="btabs-animated-slideleft-kuota">
								<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Hari</label>
											<div class="col-md-10">
												<input class="form-control input-sm " readonly type="text" id="namahari" name="namahari" value=""/>	
												<input class="form-control input-sm " readonly type="hidden" id="xtanggal" name="xtanggal" value=""/>	
												<input class="form-control input-sm " readonly type="hidden" id="xiddokter" name="xiddokter" value=""/>	
												<input class="form-control input-sm " readonly type="hidden" id="xjadwal_id" name="xjadwal_id" value=""/>	
												<input class="form-control input-sm " readonly type="hidden" id="nama_dokter" name="nama_dokter" value=""/>	
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Jam</label>
											<div class="col-md-10">
												<input class="form-control input-sm " readonly type="text" id="jam" name="jam" value=""/>	
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="tipeid">Kuota</label>
											<div class="col-md-10">
												<select id="kuota" name="kuota" class="form-control js-select2" style="width: 100%;" data-placeholder="Choose one..">
													<option value="999">TANPA BATAS</option>
													<option value="0" selected>ISI PENUH</option>
													<?for ($x = 1; $x <= 100; $x++) {?>
														<option value="<?=$x?>"><?=$x?></option>
													<?}?>
												</select>
											</div>
										</div>
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="tipeid">Reservasi Online</label>
											<div class="col-md-10">
												<select id="st_reservasi_online" name="st_reservasi_online" class="form-control js-select2" style="width: 100%;" data-placeholder="Choose one..">
													<option value="1">RESERVASI ONLINE</option>
													<option value="0">HANYA OFFLINE</option>
													
												</select>
											</div>
										</div>
										
										
									</div>
									<div class="col-md-12">
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-2 control-label" for="btn_save_kuota"></label>
											<div class="col-md-10">
												<button class="btn btn-success text-uppercase"  type="button" name="btn_save_kuota" id="btn_save_kuota"><i class="fa fa-save"></i> Simpan</button>
												<button class="btn btn-danger text-uppercase"  type="button" name="btn_save_penuh" id="btn_save_penuh"><i class="si si-ban"></i> Tutup Pendaftaran</button>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_close() ?>
							</div>
							<div class="tab-pane fade fade-left" id="btabs-animated-slideleft-note">
								<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
								<div class="row">
									<div class="col-md-12">
										
										<div class="form-group" style="margin-bottom: 5px;">
											<label class="col-md-2 control-label" for="edit_vendor">Notes</label>
											<div class="col-md-10">
												<input class="form-control input-sm " type="text" id="catatan" name="catatan" value=""/>	
											</div>
										</div>
										
									</div>
									<div class="col-md-12">
										<div class="form-group" style="margin-top: 15px;">
											<label class="col-md-2 control-label" for="idprespektif"></label>
											<div class="col-md-10">
												<button class="btn btn-success text-uppercase"  type="button" name="btn_save_catatan" id="btn_save_catatan"><i class="fa fa-save"></i> Simpan</button>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_close() ?>
							</div>
						
							
							
							
						</div>
					</div>
                  </div>
                </div>
                <div class="modal-footer" style="margin-top:20px">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>	
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
var present;
var st_plus_minus;
var tab;
var tanggaldari;
var tanggalsampai;
$(document).ready(function(){	
	refresh_present();
	hasil_generate_ls();
	
})	
function refresh_present(){
	present=$("#present").val();
	if (present=='3'){
		$(".div_date").show();
		$(".div_present").hide();
	}else{
		$(".div_date").hide();
		$(".div_present").show();
		
	}
}

function hasil_generate_ls(){
	tanggaldari=$("#tanggaldari").val();
	tanggalsampai=$("#tanggalsampai").val();
	let idpoli=$("#idpoli").val();
	let iddokter=$("#iddokter").val();
	let present=$("#present").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mjadwal_dokter/hasil_generate/',
		method: "POST",
		dataType: "JSON",
		data: {
			tanggaldari:tanggaldari,
			tanggalsampai:tanggalsampai,
			present:present,
			idpoli:idpoli,
			iddokter:iddokter,
		},
		success: function(data) {
			$("#div_ls").html(data.tabel);
			$("#cover-spin").hide();
			// window.table = $('#index_ls').DataTable(
			  // {
			   // "autoWidth": false,
					// // "pageLength": 50,
					// autoWidth: true,
					// scrollX: true,
					// scrollY: 400,
					// "paging": true,
					// paging: false,
					// ordering: false,
					// scrollCollapse: true,
					// "processing": true,
					
				// ordering: [
				  // [1, 'asc']
				// ],
				// colReorder:
				// {
				  // fixedColumnsLeft: 3,
				  // fixedColumnsRight: 1
				// }
			  // });
			$("#cover-spin").hide();
		}
	});
}

function refresh_tanggal2(){
	var tanggaldari=$("#tanggaldari").val();
	var tanggalsampai=$("#tanggalsampai").val();
	present=$("#present").val();
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}app_setting/refresh_tanggal2/',
		method: "POST",
		dataType: "JSON",
		data: {
			tanggaldari:tanggaldari,
			tanggalsampai:tanggalsampai,
			present:present,
			st_plus_minus:st_plus_minus,
		},
		success: function(data) {
			$("#tanggaldari").val(data.tanggaldari);
			$("#tanggalsampai").val(data.tanggalsampai);
			$("#cover-spin").hide();
			hasil_generate_ls();
		}
	});
}
$("#idpoli").change(function() {
	hasil_generate_ls();
});
$("#iddokter").change(function() {
	hasil_generate_ls();
});
$("#present").change(function() {
	refresh_present();
	// if ($("#present").val()!='3'){
		st_plus_minus=0;
		refresh_tanggal2();
	// }
});

$("#btn_filter_ls").click(function() {
	hasil_generate_ls();
});

$("#btn_back2").click(function() {
	st_plus_minus='-1'
	refresh_tanggal2();
	
});
$("#btn_next2").click(function() {
	st_plus_minus='1'
	refresh_tanggal2();
	
});
$("#btn_save_kuota").click(function() {
	$iddokter=$("#xiddokter").val();
	$tanggal=$("#xtanggal").val();
	$jadwal_id=$("#xjadwal_id").val();
	$kuota=$("#kuota").val();
	$st_reservasi_online=$("#st_reservasi_online").val();
	// alert($idpoli+' '+$kodehari+' '+$jam_id);
	$.ajax({
		url: '{site_url}mjadwal_dokter/save_kuota_detail/',
		method: "POST",
		dataType: "JSON",
		data: {
			iddokter:$iddokter,
			tanggal:$tanggal,
			jadwal_id:$jadwal_id,
			kuota:$kuota,
			st_reservasi_online:$st_reservasi_online,
		},
		success: function(data) {
			$("#modal_edit").modal('hide');
			hasil_generate_ls();
		}
	});
});
$("#btn_save_catatan").click(function() {
	$iddokter=$("#xiddokter").val();
	$tanggal=$("#xtanggal").val();
	$jadwal_id=$("#xjadwal_id").val();
	$catatan=$("#catatan").val();
	// alert($idpoli+' '+$kodehari+' '+$jam_id);
	$.ajax({
		url: '{site_url}mjadwal_dokter/save_catatan_detail/',
		method: "POST",
		dataType: "JSON",
		data: {
			iddokter:$iddokter,
			tanggal:$tanggal,
			jadwal_id:$jadwal_id,
			catatan:$catatan
		},
		success: function(data) {
			$("#modal_edit").modal('hide');
			hasil_generate_ls();
		}
	});
});
$("#btn_save_penuh").click(function() {
	$nama_dokter=$("#nama_dokter").val();
	$namahari=$("#namahari").val();
	swal({
			title: "Apakah Anda Yakin ?",
			text: "Akan Menghentikan reservasi "+$nama_dokter+", Hari "+$namahari+" ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$iddokter=$("#xiddokter").val();
			$tanggal=$("#xtanggal").val();
			$jadwal_id=$("#xjadwal_id").val();
			$catatan=$("#catatan").val();
			// alert($idpoli+' '+$kodehari+' '+$jam_id);
			$.ajax({
				url: '{site_url}mjadwal_dokter/stop_kuota/',
				method: "POST",
				dataType: "JSON",
				data: {
					iddokter:$iddokter,
					tanggal:$tanggal,
					jadwal_id:$jadwal_id,
					catatan:$catatan
				},
				success: function(data) {
					$("#modal_edit").modal('hide');
					hasil_generate_ls();
				}
			});
		});


	
});


function clear_present(){
	 $(".present").removeClass("btn-success");
}
function add_kuota($tanggal,$iddokter,$jadwal_id){
	// alert($tanggal);
	$("#xjadwal_id").val($jadwal_id);
	$("#xiddokter").val($iddokter);
	$("#modal_edit").modal('show');
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mjadwal_dokter/get_kuota_detail/',
		method: "POST",
		dataType: "JSON",
		data: {
			iddokter:$iddokter,
			tanggal:$tanggal,
			jadwal_id:$jadwal_id
		},
		success: function(data) {
			$("#jam").val(data.jam);
			$("#xtanggal").val(data.tanggal);
			$("#namahari").val(data.periode_nama);
			$("#catatan").val(data.catatan);
			$("#nama_dokter").val(data.nama_dokter);
			$("#kuota").val(data.kuota).trigger('change');
			$("#st_reservasi_online").val(data.st_reservasi_online).trigger('change');
			$("#cover-spin").hide();
		}
	});
	
}

</script>