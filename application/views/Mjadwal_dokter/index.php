<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options"></ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">

		<hr style="margin-top:0px">
		<div class="row">
			<?php echo form_open('mjadwal_dokter/filter','class="form-horizontal" id="form-work"') ?>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="idpoliklinik">Poliklinik</label>
					<div class="col-md-8">
						<select name="idpoliklinik" class="js-select2 form-control" style="width: 100%;">
							<option value="#">Semua Poliklinik</option>
							<?php foreach (get_all('mpoliklinik', array('status' => 1)) as $row){?>
								<option value="<?= $row->id?>" <?=($idpoliklinik == $row->id ? 'selected':'')?>><?= $row->nama?></option>
							<?php }?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="iddokter">Dokter</label>
					<div class="col-md-8">
						<select name="iddokter" class="js-select2 form-control" style="width: 100%;">
							<option value="#">Semua Dokter</option>
							<?php foreach (get_all('mdokter', array('status' => 1)) as $row) { ?>
								<option value="<?=$row->id?>" <?=($iddokter == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<div class="col-md-6">
			</div>
			<?php echo form_close() ?>
		</div>

		<hr style="margin-top:10px">

		<a href="{base_url}mjadwal_dokter/display" target="_blank" class="btn btn-primary"><i class="fa fa-list"></i> Lihat Jadwal</a>
		<a href="{base_url}mjadwal_dokter/print" target="_blank" class="btn btn-success"><i class="fa fa-print"></i> Cetak Jadwal</a>
		<a href="{base_url}mcuti_dokter/create" target="_blank" class="btn btn-danger"><i class="fa fa-calendar"></i> Cuti Praktek</a>
		<a href="{base_url}mjadwal_dokter/slot_harian" target="_blank" class="btn btn-warning"><i class="fa fa-delicious"></i> Slot Harian</a>

		<hr style="margin-top:10px">
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>Dokter</th>
					<th>Poliklinik</th>
					<th>Senin</th>
					<th>Selasa</th>
					<th>Rabu</th>
					<th>Kamis</th>
					<th>Jumat</th>
					<th>Sabtu</th>
					<th>Minggu</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	jQuery(function() {
		BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
			"bSort": false,
			"autoWidth": false,
			"pageLength": 10,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}mjadwal_dokter/getIndex/' + "<?= $this->uri->segment(2); ?>",
				type: "POST",
				dataType: 'json'
			},
			"columnDefs": [{
					"width": "5%",
					"targets": 0,
					"orderable": true
				},
				{
					"width": "15%",
					"targets": 1,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 2,
					"orderable": true
				},
				{
					"width": "5%",
					"targets": 3,
					"orderable": false
				},
				{
					"width": "8%",
					"targets": 4,
					"orderable": false
				},
				{
					"width": "8%",
					"targets": 5,
					"orderable": false
				},
				{
					"width": "8%",
					"targets": 6,
					"orderable": false
				},
				{
					"width": "8%",
					"targets": 7,
					"orderable": false
				},
				{
					"width": "8%",
					"targets": 8,
					"orderable": false
				},
				{
					"width": "8%",
					"targets": 9,
					"orderable": false
				},
				{
					"width": "10%",
					"targets": 10,
					"orderable": false
				},
			]
		});
	});
</script>
