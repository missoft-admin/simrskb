<?php $dataSchedule = $this->model->getScheduleDokterPoliklinikDay($iddokter, $idpoliklinik, $hari); ?>
<?php foreach($dataSchedule as $index => $schedule) { ?>
    <?php if ($index == 0) { ?>
        <div class="header-section"><?= $schedule->poliklinik; ?></div>
    <? } ?>

    <div class="timetable success">
        <div class="time"><?= $schedule->jam_dari . ' - ' . $schedule->jam_sampai; ?></div>
        <div class="room">R. <?= $schedule->ruangan; ?></div>
    </div>
<? } ?>

<?php $dataLeave = $this->model->getLeaveDokter($iddokter, $tanggal); ?>
<?php foreach($dataLeave as $leave) { ?>
    <div class="timetable danger">
        <div class="time"><?= $leave->sebab; ?></div>
        <div class="room"><?= $leave->alasan; ?></div>
    </div>
<? } ?>