<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mjadwal_dokter" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mjadwal_dokter/save_slot','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-12" for="">Dokter</label>
				<div class="col-md-12">
					<select disabled class="js-select2 form-control" style="width: 100%;">
						<?php foreach (get_all('mdokter', array('status' => 1)) as $row) { ?>
							<option value="<?=$row->id?>" <?=($iddokter == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-12" for="">Poliklinik</label>
				<div class="col-md-12">
					<select disabled class="js-select2 form-control" style="width: 100%;">
						<?php foreach (get_all('mpoliklinik', array('status' => 1)) as $row) { ?>
							<option value="<?=$row->id?>" <?=($idpoliklinik == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			
			
			<hr>

			<table class="table table-bordered">
				<thead>
					<tr>
						<td class="text-center" style="font-weight: bold;" width="20%">Hari</td>
						<td class="text-center" style="font-weight: bold;" width="20%">Jadwal</td>
						<td class="text-center" style="font-weight: bold;" width="10%">Slot</td>
						<td class="text-center" style="font-weight: bold;" width="10%">Reservasi Online</td>
						<td class="text-center" style="font-weight: bold;" width="30%">Notes</td>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($list_jadwal as $row) { ?>
						<tr>
							<td class="text-center"><?= $row->hari; ?><input class="form-control" type="hidden" name="id[]" placeholder="Slot" value="<?=$row->idjadwal?>"></td>
							<td class="text-center">
								<label class="label label-primary">
									<?= $row->jam_dari; ?> - <?= $row->jam_sampai; ?>
								</label>
							</td>
							<td><input class="form-control number" type="text" name="kuota[]" placeholder="Slot" value="<?=$row->kuota?>"></td>
							<td>
								<select name="st_reservasi_online[]" class="form-control js-select2" style="width: 100%;" data-placeholder="Choose one..">
									<option value="1" <?=($row->st_reservasi_online=='1'?'selected':'')?>>YA</option>
									<option value="0" <?=($row->st_reservasi_online=='0'?'selected':'')?>>TIDAK</option>
									
								</select>
							</td>
							<td><input class="form-control" type="text" name="notes[]" placeholder="Notes" value="<?=$row->notes?>"></td>
							
						</tr>
					<? } ?>
				</tbody>
			</table>
			<div class="form-group">
				<div class="col-md-12 pull-right">
					<button class="btn btn-success push-5-r push-10" type="submit"><i class="fa fa-save"></i> Simpan</button>
					<a href="{base_url}mjadwal_dokter/manage_slot/<?=$iddokter?>/<?=$idpoliklinik?>/<?=$idjadwal?>" class="btn btn-default push-5-r push-10" type="button"><i class="fa fa-refresh"></i> Batal</a>
				</div>
			</div>

			<br>
			<?php echo form_hidden('idjadwal', $idjadwal); ?>
			<?php echo form_hidden('iddokter', $iddokter); ?>
			<?php echo form_hidden('idpoliklinik', $idpoliklinik); ?>
			<?php echo form_close() ?>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
	$('.number').number(true, 0);
	
</script>
