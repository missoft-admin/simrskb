<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<style>
    .profile-dockter {
        background-color: #afadad;
    }
    .profile-dockter .image {
        padding: 10px 10px 0 10px;
    }
    .profile-dockter .image img {
        width: 200px;
    }
    .profile-dockter .information {
        text-align: center;
        padding: 16px;
        color: #fff;
    }
    .profile-dockter .information .name {
        font-weight: bold;
        font-size: 14px;
    }
    .profile-dockter .information .uniqueId {
        font-size: 12px;
    }

    .timetable {
        color: #fff;
        width: 100%;
        padding: 8px;
        margin-bottom: 10px;
    }
    .timetable .time {
        font-size: 14px;
        font-weight: bold;
    }
    .timetable.success {
        background-color: #14adc4;
    }
    .timetable.danger {
        background-color: #c54736;
    }
    .column-day {
        background-color: #afadad;
        font-weight: bold;
        color: #fff;
    }

    .header-section {
        padding: 8px;
        font-weight: bold;
        text-decoration: underline;
        text-transform: uppercase;
    }
</style>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
            <?= DayMonthYear(date("Y-m-d")); ?>
        </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">

		<hr style="margin-top:0px">
		<div class="row">
			<form method="get" action="{base_url}mjadwal_dokter/display">
                <div class="col-md-4">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-2 control-label" for="iddokter">Dokter</label>
                        <div class="col-md-10">
                            <select name="iddokter" class="js-select2 form-control" style="width: 100%;">
                                <option value="">Semua Dokter</option>
                                <?php foreach (get_all('mdokter', array('status' => 1)) as $row) { ?>
                                    <option value="<?=$row->id?>" <?=($iddokter == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-2 control-label" for="idpoliklinik">Poliklinik</label>
                        <div class="col-md-10">
                            <select name="idpoliklinik" class="js-select2 form-control" style="width: 100%;">
                                <option value="">Semua Poliklinik</option>
                                <?php foreach (get_all('mpoliklinik', array('status' => 1)) as $row){?>
                                    <option value="<?= $row->id?>" <?=($idpoliklinik == $row->id ? 'selected':'')?>><?= $row->nama?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <button class="btn btn-success text-uppercase" type="submit" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                </div>
            </form>
		</div>

		<hr style="margin-top:10px">

		<table class="table table-bordered">
			<tbody>
                <?php foreach ($dataDokter as $dokter) { ?>
                    <tr>
                        <td width="10%" rowspan="<?= $dokter->rowspan; ?>">
                            <div class="profile-dockter">
                                <div class="image">
                                    <img src="{base_url}assets/upload/dokter/<?= (fileExists("/assets/upload/dokter/$dokter->foto") ? $dokter->foto : ($dokter->jeniskelamin == 1 ? 'default-man.jpeg' : 'default-woman.jpeg')) ?>">
                                </div>
                                <div class="information">
                                    <div class="name"><?= $dokter->nama; ?></div>
                                    <div class="uniqueId"><?= $dokter->nip; ?></div>
                                </div>
                            </div>
                        </td>
                        <td class="text-center column-day" height="10px" width="100px">SENIN ( <?= getDateWithDay('Senin', 'd-m-Y'); ?> )</td>
                        <td class="text-center column-day" height="10px" width="100px">SELASA ( <?= getDateWithDay('Selasa', 'd-m-Y'); ?> )</td>
                        <td class="text-center column-day" height="10px" width="100px">RABU ( <?= getDateWithDay('Rabu', 'd-m-Y'); ?> )</td>
                        <td class="text-center column-day" height="10px" width="100px">KAMIS ( <?= getDateWithDay('Kamis', 'd-m-Y'); ?> )</td>
                        <td class="text-center column-day" height="10px" width="100px">JUMAT ( <?= getDateWithDay('Jumat', 'd-m-Y'); ?> )</td>
                        <td class="text-center column-day" height="10px" width="100px">SABTU ( <?= getDateWithDay('Sabtu', 'd-m-Y'); ?> )</td>
                        <td class="text-center column-day" height="10px" width="100px">MINGGU ( <?= getDateWithDay('Minggu', 'd-m-Y'); ?> )</td>
                    </tr>

                    <?php $dataPoliklinik = $this->model->getScheduleDokterPoliklinik($dokter->id, $idpoliklinik); ?>
                    <?php foreach ($dataPoliklinik as $poliklinik) { ?>
                        <tr>
                            <! -- SENIN -->
                            <td class="text-center">
                                <?php 
                                    $this->load->view('Mjadwal_dokter/schedule_day', array(
                                        'iddokter' => $dokter->id, 
                                        'idpoliklinik' => $poliklinik->id, 
                                        'tanggal' => getDateWithDay('Senin', 'Y-m-d'),
                                        'hari' => 'Senin')
                                    );
                                ?>
                            </td>
                            <! -- SELASA -->
                            <td class="text-center">
                                <?php 
                                    $this->load->view('Mjadwal_dokter/schedule_day', array(
                                        'iddokter' => $dokter->id, 
                                        'idpoliklinik' => $poliklinik->id, 
                                        'tanggal' => getDateWithDay('Selasa', 'Y-m-d'),
                                        'hari' => 'Selasa')
                                    );
                                ?>
                            </td>
                            <! -- RABU -->
                            <td class="text-center">
                                <?php 
                                    $this->load->view('Mjadwal_dokter/schedule_day', array(
                                        'iddokter' => $dokter->id, 
                                        'idpoliklinik' => $poliklinik->id, 
                                        'tanggal' => getDateWithDay('Rabu', 'Y-m-d'),
                                        'hari' => 'Rabu')
                                    );
                                ?>
                            </td>
                            <! -- KAMIS -->
                            <td class="text-center">
                                <?php 
                                    $this->load->view('Mjadwal_dokter/schedule_day', array(
                                        'iddokter' => $dokter->id, 
                                        'idpoliklinik' => $poliklinik->id, 
                                        'tanggal' => getDateWithDay('Kamis', 'Y-m-d'),
                                        'hari' => 'Kamis')
                                    );
                                ?>
                            </td>
                            <! -- JUMAT -->
                            <td class="text-center">
                                <?php 
                                    $this->load->view('Mjadwal_dokter/schedule_day', array(
                                        'iddokter' => $dokter->id, 
                                        'idpoliklinik' => $poliklinik->id, 
                                        'tanggal' => getDateWithDay('Jumat', 'Y-m-d'),
                                        'hari' => 'Jumat')
                                    );
                                ?>
                            </td>
                            <! -- SABTU -->
                            <td class="text-center">
                                <?php 
                                    $this->load->view('Mjadwal_dokter/schedule_day', array(
                                        'iddokter' => $dokter->id, 
                                        'idpoliklinik' => $poliklinik->id, 
                                        'tanggal' => getDateWithDay('Sabtu', 'Y-m-d'),
                                        'hari' => 'Sabtu')
                                    );
                                ?>
                            </td>
                            <! -- MINGGU -->
                            <td class="text-center">
                                <?php 
                                    $this->load->view('Mjadwal_dokter/schedule_day', array(
                                        'iddokter' => $dokter->id, 
                                        'idpoliklinik' => $poliklinik->id, 
                                        'tanggal' => getDateWithDay('Minggu', 'Y-m-d'),
                                        'hari' => 'Minggu')
                                    );
                                ?>
                            </td>
                        </tr>
                    <? } ?>
                <? } ?>
			</tbody>
		</table>
	</div>
</div>