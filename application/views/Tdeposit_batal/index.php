<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<?
$user_id=$this->session->userdata('user_id');
?>
<div class="block push-10">
	<div class="block-content bg-primary">
			<input type="hidden" id="tab" name="tab" value="{tab}">
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<input type="hidden" id="st_login" name="st_login" value="{st_login}">
			<div class="row pull-10">
				
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">Tanggal</label>
						<div class="col-xs-12">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                <input class="form-control" type="text" id="tanggal_1" placeholder="From" value="{tanggal_1}">
                                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                <input class="form-control" type="text" id="tanggal_2" placeholder="To" value="{tanggal_2}">
                            </div>
						</div>
					</div>
				</div>
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">No Medrec</label>
						<div class="col-xs-12">
							<input type="text" class="form-control" id="no_medrec" value="">
						</div>
					</div>
				</div>
				<div class="col-md-3 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">Nama Pasien</label>
						<div class="col-xs-12">
							<input type="text" class="form-control" id="nama_pasien" value="">
						</div>
					</div>
				</div>
				
				<div class="col-md-1 push-10">
					<div class="form-group">
						<label class="col-xs-12" for="idpoli">&nbsp;&nbsp;</label>
						<div class="col-xs-12">
							<span class="input-group-btn ">
								<button class="btn btn-warning btn-block " id="btn_cari" type="button" title="Login"><i class="fa fa-search pull-left"></i> Cari</button>&nbsp;
							</span>
						</div>
					</div>
				</div>
				
			</div>
			<?php echo form_close() ?>
			
	</div>
</div>

<div class="block">
	<ul class="nav nav-pills">
		<li id="div_1" class="<?=($tab=='1'?'active':'')?>">
			<a href="javascript:void(0)" onclick="set_tab(1)"><i class="fa fa-file-pdf-o"></i> History PDF</a>
		</li>
	</ul>
	<div class="block-content">
			<input type="hidden" id="tab" name="tab" value="{tab}">
			<div class="row   ">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table" id="index_all">
							<thead>
								<tr>
									<th width="15%">Pasien</th>
									<th width="10%">No Transaksi</th>
									<th width="10%">Jenis Deposit</th>
									<th width="10%">Tanggal Deposit</th>
									<th width="10%">Metode</th>
									<th width="10%">Nominal</th>
									<th width="15%">Terima Dari</th>
									<th width="10%">Alasan</th>
									<th width="10%">PDF</th>
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		
	
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
var tab=<?=$tab?>;
var st_login;

$(document).ready(function(){	
	load_index_all();
	
});
$(document).on("click","#btn_cari",function(){
	load_index_all();
});
function set_tab($tab){
	tab=$tab;
	// alert(tab);
	// $("#cover-spin").show();
	document.getElementById("div_1").classList.remove("active");
	
	if (tab=='1'){
		document.getElementById("div_1").classList.add("active");
	}
	load_index_all();
}

function load_index_all(){
	$('#index_all').DataTable().destroy();	
	$("#cover-spin").show();
	let tanggal_1=$("#tanggal_1").val();
	let tanggal_2=$("#tanggal_2").val();
	let no_medrec=$("#no_medrec").val();
	let nama_pasien=$("#nama_pasien").val();
	
	// alert(ruangan_id);
	table = $('#index_all').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 50,
            serverSide: true,
			
			"processing": false,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			"columnDefs": [
					{"targets": [0,1,2,3,4,5,6,7,8],  className: "text-center" },
				],
            ajax: { 
                url: '{site_url}tdeposit_batal/getIndex_all', 
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_1:tanggal_1,
						tanggal_2:tanggal_2,
						no_medrec:no_medrec,
						nama_pasien:nama_pasien,
						tab:tab,
						
					   }
            },
			"drawCallback": function( settings ) {
				$("#cover-spin").hide();
			 }  
        });
}

</script>