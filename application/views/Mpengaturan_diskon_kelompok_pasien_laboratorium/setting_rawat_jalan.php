<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-rounded">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <div class="form-horizontal push-10-t">
            <div class="form-group">
                <label class="col-md-12" for="">Kelompok Pasien</label>
                <div class="col-md-12">
                    <input type="text" class="form-control" readonly value="{nama}">
                </div>
            </div>

            <hr>

            <ul class="nav nav-tabs" data-toggle="tabs">
                <li class="active">
                    <a href="#tab_diskon_all" data-toggle="tab">Diskon All</a>
                </li>
                <li class="">
                    <a href="#tab_diskon_tarif_pelayanan" data-toggle="tab">Tarif Pelayanan</a>
                </li>
                <li class="">
                    <a href="#tab_diskon_bmhp" data-toggle="tab">BMHP</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade active in" id="tab_diskon_all">
                    <table class="table table-bordered table-striped" style="width: 100%;">
                        <thead>
                            <tr>
                                <th style="width: 5%;" class="text-center">No</th>
                                <th style="width: 10%;">Tujuan</th>
                                <th style="width: 10%;">Tujuan Poliklinik</th>
                                <th style="width: 10%;">Status Pasien</th>
                                <th style="width: 10%;">Kasus</th>
                                <th style="width: 10%;">Diskon Rp</th>
                                <th style="width: 10%;">Diskon %</th>
                                <th style="width: 10%;">Aksi</th>
                            </tr>
                            <tr>
                                <td class="text-center" style="vertical-align: middle;">#</td>
                                <td>
                                    <select id="all_tujuan_pasien" tabindex="16" style="width: 150px" data-placeholder="Tipe" class="js-select2 form-control input-sm">
                                        <option value="0">Semua Tujuan</option>
                                        <option value="1">Poliklinik</option>
                                        <option value="2">Instalasi Gawat Darurat (IGD)</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="all_status_tujuan_poliklinik" tabindex="16" style="width: 150px" data-placeholder="Status Tujuan Poliklinik" class="js-select2 form-control input-sm">
                                        <option value="0">Tidak Ditentukan</option>
                                        <option value="1">Ditentukan</option>
                                    </select>
                                    <br />
                                    <br />
                                    <select id="all_tujuan_poliklinik" tabindex="16" style="width: 150px" data-placeholder="Tujuan Poliklinik"  disabled class="js-select2 form-control input-sm">
                                        <option value="0">Semua Poliklinik</option>
                                        <?php foreach (get_all('mpoliklinik', array('status' => 1)) as $row){?>
                                        <option value="<?= $row->id?>"><?= $row->nama?></option>
                                        <?php }?>
                                    </select>
                                </td>
                                <td>
                                    <select id="all_status_pasien" tabindex="16" style="width: 150px" data-placeholder="Status Pasien" class="js-select2 form-control input-sm">
                                        <option value="0">Semua Status</option>
                                        <option value="1">Pasien Baru</option>
                                        <option value="2">Pasien Lama</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="all_kasus" tabindex="16" style="width: 150px" data-placeholder="Kasus" class="js-select2 form-control input-sm">
                                        <option value="0">Semua Kasus</option>
                                        <option value="1">Kasus Baru</option>
                                        <option value="2">Kasus Lama</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="all_status_diskon_rp" tabindex="16" style="width: 150px" data-placeholder="Tipe" class="js-select2 form-control input-sm">
                                        <option value="0">Tidak Ditentukan</option>
                                        <option value="1">Ditentukan</option>
                                    </select>
                                    <br />
                                    <br />
                                    <input type="text" class="form-control input-sm number" disabled id="all_diskon_rp" value="" />
                                </td>
                                <td>
                                    <select id="all_status_diskon_persen" tabindex="16" style="width: 150px" data-placeholder="Tipe" class="js-select2 form-control input-sm">
                                        <option value="0">Tidak Ditentukan</option>
                                        <option value="1">Ditentukan</option>
                                    </select>
                                    <br />
                                    <br />
                                    <input type="text" class="form-control input-sm discount" disabled id="all_diskon_persen" value="" />
                                </td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-primary" id="btn-submit-diskon-all"><i class="fa fa-save"></i> Tambahkan</button>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list_setting_diskon_all as $index => $row) { ?>
                                <tr>
                                    <td class="text-center"><?php echo $index + 1; ?></td>
                                    <td><?php echo $row->tujuan_pasien; ?></td>
                                    <td><?php echo $row->tujuan_poliklinik; ?></td>
                                    <td><?php echo $row->status_pasien; ?></td>
                                    <td><?php echo $row->kasus; ?></td>
                                    <td><?php echo $row->diskon_rp; ?></td>
                                    <td><?php echo $row->diskon_persen; ?></td>
                                    <td>
                                        <a href="#" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm delete-link" data-url="{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/delete_setting_all/<?= $row->id; ?>"><i class="fa fa-trash-o"></i></a>    
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                
                <div class="tab-pane fade" id="tab_diskon_tarif_pelayanan">
                    <hr>
                    
                    <h5><?=text_primary('Diskon Tarif Administrasi Rawat Jalan');?></h5>
                    <br />
                    
                    <div style="overflow-x: auto; overflow-y: auto; max-height: 500px;">
                        <table class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width: 5%;" class="text-center">No</th>
                                    <th style="width: 10%;">Tujuan</th>
                                    <th style="width: 10%;">Tujuan Poliklinik</th>
                                    <th style="width: 10%;">Status Pasien</th>
                                    <th style="width: 10%;">Kasus</th>
                                    <th style="width: 10%;">Jenis Tarif</th>
                                    <th style="width: 10%;">Tarif Pelayanan</th>
                                    <th style="width: 10%;">Diskon Semua Per Tarif</th>
                                    <th style="width: 10%;">Diskon Jasa Sarana</th>
                                    <th style="width: 10%;">Diskon Jasa Pelayanan</th>
                                    <th style="width: 10%;">Diskon BHP</th>
                                    <th style="width: 10%;">Diskon Biaya Perawatan</th>
                                    <th style="width: 10%;">Aksi</th>
                                </tr>
                                <tr>
                                    <td class="text-center" style="vertical-align: middle;">#</td>
                                    <td>
                                        <select id="adm_tujuan_pasien" tabindex="16" style="width: 200px" data-placeholder="Tipe" class="js-select2 form-control input-sm">
                                            <option value="0">Semua Tujuan</option>
                                            <option value="1">Poliklinik</option>
                                            <option value="2">Instalasi Gawat Darurat (IGD)</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="adm_status_tujuan_poliklinik" tabindex="16" style="width: 200px" data-placeholder="Status Tujuan Poliklinik" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="adm_tujuan_poliklinik" tabindex="16" style="width: 200px" data-placeholder="Tujuan Poliklinik"  disabled class="js-select2 form-control input-sm">
                                            <option value="0">Semua Poliklinik</option>
                                            <?php foreach (get_all('mpoliklinik', array('status' => 1)) as $row){?>
                                            <option value="<?= $row->id?>"><?= $row->nama?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="adm_status_pasien" tabindex="16" style="width: 200px" data-placeholder="Status Pasien" class="js-select2 form-control input-sm">
                                            <option value="0">Semua Status</option>
                                            <option value="1">Pasien Baru</option>
                                            <option value="2">Pasien Lama</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="adm_kasus" tabindex="16" style="width: 200px" data-placeholder="Kasus" class="js-select2 form-control input-sm">
                                            <option value="0">Semua Kasus</option>
                                            <option value="1">Kasus Baru</option>
                                            <option value="2">Kasus Lama</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="adm_jenis_tarif" tabindex="16" style="width: 200px" data-placeholder="Jenis Tarif" class="js-select2 form-control input-sm">
                                            <option value="0">Semua</option>
                                            <option value="1">Administrasi</option>
                                            <option value="2">Cetak Kartu</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="adm_tarif_pelayanan" tabindex="16" style="width: 200px" data-placeholder="Tarif Pelayanan" class="js-select2 form-control input-sm">
                                            <option value="0">Semua</option>

                                        </select>
                                    </td>
                                    <td>
                                        <select id="adm_status_diskon_all" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="adm_tipe_diskon_all" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="adm_diskon_all_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="adm_status_diskon_jasasarana" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="adm_tipe_diskon_jasasarana" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="adm_diskon_jasasarana_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="adm_status_diskon_jasapelayanan" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="adm_tipe_diskon_jasapelayanan" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="adm_diskon_jasapelayanan_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="adm_status_diskon_bhp" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="adm_tipe_diskon_bhp" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="adm_diskon_bhp_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="adm_status_diskon_biayaperawatan" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="adm_tipe_diskon_biayaperawatan" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="adm_diskon_biayaperawatan_nilai" value="" />
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-xs btn-primary" id="btn-submit-diskon-adm"><i class="fa fa-save"></i> Tambahkan</button>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($list_setting_diskon_adm as $index => $row) { ?>
                                    <tr>
                                        <td class="text-center"><?php echo $index + 1; ?></td>
                                        <td><?php echo $row->tujuan_pasien; ?></td>
                                        <td><?php echo $row->tujuan_poliklinik; ?></td>
                                        <td><?php echo $row->status_pasien; ?></td>
                                        <td><?php echo $row->kasus; ?></td>
                                        <td><?php echo $row->jenis_tarif; ?></td>
                                        <td>
                                            <?php
                                            if ($row->tarif_pelayanan == "Semua") {
                                                echo "Semua";
                                            } else {
                                                echo GetJenisAdministrasi($row->idjenis_tarif_pelayanan) . ' ( ' . $row->tarif_pelayanan . ' )';
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $row->diskon_all; ?></td>
                                        <td><?php echo $row->diskon_jasasarana; ?></td>
                                        <td><?php echo $row->diskon_jasapelayanan; ?></td>
                                        <td><?php echo $row->diskon_bhp; ?></td>
                                        <td><?php echo $row->diskon_biaya_perawatan; ?></td>
                                        <td>
                                            <a href="#" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm delete-link" data-url="{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/delete_setting_adm/<?= $row->id; ?>"><i class="fa fa-trash-o"></i></a>    
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <hr>
                    
                    <h5><?=text_primary('Diskon Tarif Pelayanan Rawat Jalan');?></h5>
                    <br />
                    
                    <div style="overflow-x: auto; overflow-y: auto; max-height: 500px;">
                        <table class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width: 5%;" class="text-center">No</th>
                                    <th style="width: 10%;">Tujuan</th>
                                    <th style="width: 10%;">Tujuan Poliklinik</th>
                                    <th style="width: 10%;">Status Pasien</th>
                                    <th style="width: 10%;">Kasus</th>
                                    <th style="width: 10%;">Head Parent Tarif</th>
                                    <th style="width: 10%;">Tarif Pelayanan</th>
                                    <th style="width: 10%;">Diskon Semua Per Tarif</th>
                                    <th style="width: 10%;">Diskon Jasa Sarana</th>
                                    <th style="width: 10%;">Diskon Jasa Pelayanan</th>
                                    <th style="width: 10%;">Diskon BHP</th>
                                    <th style="width: 10%;">Diskon Biaya Perawatan</th>
                                    <th style="width: 10%;">Aksi</th>
                                </tr>
                                <tr>
                                    <td class="text-center" style="vertical-align: middle;">#</td>
                                    <td>
                                        <select id="pelayanan_tujuan_pasien" tabindex="16" style="width: 200px" data-placeholder="Tipe" class="js-select2 form-control input-sm">
                                            <option value="0">Semua Tujuan</option>
                                            <option value="1">Poliklinik</option>
                                            <option value="2">Instalasi Gawat Darurat (IGD)</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="pelayanan_status_tujuan_poliklinik" tabindex="16" style="width: 200px" data-placeholder="Status Tujuan Poliklinik" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="pelayanan_tujuan_poliklinik" tabindex="16" style="width: 200px" data-placeholder="Tujuan Poliklinik"  disabled class="js-select2 form-control input-sm">
                                            <option value="0">Semua Poliklinik</option>
                                            <?php foreach (get_all('mpoliklinik', array('status' => 1)) as $row){?>
                                            <option value="<?= $row->id?>"><?= $row->nama?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="pelayanan_status_pasien" tabindex="16" style="width: 200px" data-placeholder="Status Pasien" class="js-select2 form-control input-sm">
                                            <option value="0">Semua Status</option>
                                            <option value="1">Pasien Baru</option>
                                            <option value="2">Pasien Lama</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="pelayanan_kasus" tabindex="16" style="width: 200px" data-placeholder="Kasus" class="js-select2 form-control input-sm">
                                            <option value="0">Semua Kasus</option>
                                            <option value="1">Kasus Baru</option>
                                            <option value="2">Kasus Lama</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="pelayanan_head_parent_tarif" tabindex="16" style="width: 200px" data-placeholder="Semua" class="js-select2 form-control input-sm">
                                            <option value="0">Semua</option>
                                            <?php foreach ($list_pelayanan_head_parent_tarif as $row) { ?>
                                                <option value="<?=$row->id;?>" data-path="<?=$row->path;?>"><?=TreeView($row->level, $row->nama)?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="pelayanan_tarif_pelayanan" tabindex="16" style="width: 200px" data-placeholder="Semua" class="js-select2 form-control input-sm">
                                            <option value="0">Semua</option>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <select id="pelayanan_status_diskon_all" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="pelayanan_tipe_diskon_all" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="pelayanan_diskon_all_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="pelayanan_status_diskon_jasasarana" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="pelayanan_tipe_diskon_jasasarana" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="pelayanan_diskon_jasasarana_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="pelayanan_status_diskon_jasapelayanan" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="pelayanan_tipe_diskon_jasapelayanan" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="pelayanan_diskon_jasapelayanan_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="pelayanan_status_diskon_bhp" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="pelayanan_tipe_diskon_bhp" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="pelayanan_diskon_bhp_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="pelayanan_status_diskon_biayaperawatan" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="pelayanan_tipe_diskon_biayaperawatan" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="pelayanan_diskon_biayaperawatan_nilai" value="" />
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-xs btn-primary" id="btn-submit-diskon-pelayanan"><i class="fa fa-save"></i> Tambahkan</button>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($list_setting_diskon_pelayanan as $index => $row) { ?>
                                    <tr>
                                        <td class="text-center"><?php echo $index + 1; ?></td>
                                        <td><?php echo $row->tujuan_pasien; ?></td>
                                        <td><?php echo $row->tujuan_poliklinik; ?></td>
                                        <td><?php echo $row->status_pasien; ?></td>
                                        <td><?php echo $row->kasus; ?></td>
                                        <td><?php echo $row->head_parent_tarif; ?></td>
                                        <td>
                                            <?php
                                            if ($row->tarif_pelayanan == "Semua") {
                                                echo "Semua";
                                            } else {
                                                echo TreeView($row->level_tarif_pelayanan, $row->tarif_pelayanan);
                                            }
                                            ?>    
                                        </td>
                                        <td><?php echo $row->diskon_all; ?></td>
                                        <td><?php echo $row->diskon_jasasarana; ?></td>
                                        <td><?php echo $row->diskon_jasapelayanan; ?></td>
                                        <td><?php echo $row->diskon_bhp; ?></td>
                                        <td><?php echo $row->diskon_biaya_perawatan; ?></td>
                                        <td>
                                            <a href="#" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm delete-link" data-url="{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/delete_setting_pelayanan/<?= $row->id; ?>"><i class="fa fa-trash-o"></i></a>    
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <hr>
                    
                    <h5><?=text_primary('Diskon Tarif Pelayanan Radiologi');?></h5>
                    <br />
                    
                    <div style="overflow-x: auto; overflow-y: auto; max-height: 500px;">
                        <table class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width: 5%;" class="text-center">No</th>
                                    <th style="width: 10%;">Tujuan</th>
                                    <th style="width: 10%;">Tujuan Poliklinik</th>
                                    <th style="width: 10%;">Status Pasien</th>
                                    <th style="width: 10%;">Kasus</th>
                                    <th style="width: 10%;">Tipe Tarif</th>
                                    <th style="width: 10%;">Head Parent Tarif</th>
                                    <th style="width: 10%;">Tarif Pelayanan</th>
                                    <th style="width: 10%;">Diskon Semua Per Tarif</th>
                                    <th style="width: 10%;">Diskon Jasa Sarana</th>
                                    <th style="width: 10%;">Diskon Jasa Pelayanan</th>
                                    <th style="width: 10%;">Diskon BHP</th>
                                    <th style="width: 10%;">Diskon Biaya Perawatan</th>
                                    <th style="width: 10%;">Aksi</th>
                                </tr>
                                <tr>
                                    <td class="text-center" style="vertical-align: middle;">#</td>
                                    <td>
                                        <select id="radiologi_tujuan_pasien" tabindex="16" style="width: 200px" data-placeholder="Tipe" class="js-select2 form-control input-sm">
                                            <option value="0">Semua Tujuan</option>
                                            <option value="1">Poliklinik</option>
                                            <option value="2">Instalasi Gawat Darurat (IGD)</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="radiologi_status_tujuan_poliklinik" tabindex="16" style="width: 200px" data-placeholder="Status Tujuan Poliklinik" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="radiologi_tujuan_poliklinik" tabindex="16" style="width: 200px" data-placeholder="Tujuan Poliklinik"  disabled class="js-select2 form-control input-sm">
                                            <option value="0">Semua Poliklinik</option>
                                            <?php foreach (get_all('mpoliklinik', array('status' => 1)) as $row){?>
                                            <option value="<?= $row->id?>"><?= $row->nama?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="radiologi_status_pasien" tabindex="16" style="width: 200px" data-placeholder="Status Pasien" class="js-select2 form-control input-sm">
                                            <option value="0">Semua Status</option>
                                            <option value="1">Pasien Baru</option>
                                            <option value="2">Pasien Lama</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="radiologi_kasus" tabindex="16" style="width: 200px" data-placeholder="Kasus" class="js-select2 form-control input-sm">
                                            <option value="0">Semua Kasus</option>
                                            <option value="1">Kasus Baru</option>
                                            <option value="2">Kasus Lama</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="radiologi_tipe_tarif" tabindex="16" style="width: 200px" data-placeholder="Semua" class="js-select2 form-control input-sm">
                                            <option value="0">Semua</option>
                                            <option value="1">X-Ray</option>
                                            <option value="2">USG</option>
                                            <option value="3">CT Scan</option>
                                            <option value="4">MRI</option>
                                            <option value="5">BMD</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="radiologi_head_parent_tarif" tabindex="16" style="width: 200px" data-placeholder="Semua" class="js-select2 form-control input-sm">
                                            <option value="0">Semua</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="radiologi_tarif_pelayanan" tabindex="16" style="width: 200px" data-placeholder="Semua" class="js-select2 form-control input-sm">
                                            <option value="0">Semua</option>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <select id="radiologi_status_diskon_all" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="radiologi_tipe_diskon_all" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="radiologi_diskon_all_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="radiologi_status_diskon_jasasarana" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="radiologi_tipe_diskon_jasasarana" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="radiologi_diskon_jasasarana_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="radiologi_status_diskon_jasapelayanan" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="radiologi_tipe_diskon_jasapelayanan" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="radiologi_diskon_jasapelayanan_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="radiologi_status_diskon_bhp" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="radiologi_tipe_diskon_bhp" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="radiologi_diskon_bhp_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="radiologi_status_diskon_biayaperawatan" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="radiologi_tipe_diskon_biayaperawatan" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="radiologi_diskon_biayaperawatan_nilai" value="" />
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-xs btn-primary" id="btn-submit-diskon-radiologi"><i class="fa fa-save"></i> Tambahkan</button>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($list_setting_diskon_radiologi as $index => $row) { ?>
                                    <tr>
                                        <td class="text-center"><?php echo $index + 1; ?></td>
                                        <td><?php echo $row->tujuan_pasien; ?></td>
                                        <td><?php echo $row->tujuan_poliklinik; ?></td>
                                        <td><?php echo $row->status_pasien; ?></td>
                                        <td><?php echo $row->kasus; ?></td>
                                        <td><?php echo $row->jenis_tarif; ?></td>
                                        <td><?php echo $row->head_parent_tarif; ?></td>
                                        <td>
                                            <?php
                                            if ($row->tarif_pelayanan == "Semua") {
                                                echo "Semua";
                                            } else {
                                                echo TreeView($row->level_tarif_pelayanan, $row->tarif_pelayanan);
                                            }
                                            ?>      
                                        </td>
                                        <td><?php echo $row->diskon_all; ?></td>
                                        <td><?php echo $row->diskon_jasasarana; ?></td>
                                        <td><?php echo $row->diskon_jasapelayanan; ?></td>
                                        <td><?php echo $row->diskon_bhp; ?></td>
                                        <td><?php echo $row->diskon_biaya_perawatan; ?></td>
                                        <td>
                                            <a href="#" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm delete-link" data-url="{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/delete_setting_radiologi/<?= $row->id; ?>"><i class="fa fa-trash-o"></i></a>    
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <hr>
                    
                    <h5><?=text_primary('Diskon Tarif Pelayanan Laboratorium');?></h5>
                    <br />
                    
                    <div style="overflow-x: auto; overflow-y: auto; max-height: 500px;">
                        <table class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width: 5%;" class="text-center">No</th>
                                    <th style="width: 10%;">Tujuan</th>
                                    <th style="width: 10%;">Tujuan Poliklinik</th>
                                    <th style="width: 10%;">Status Pasien</th>
                                    <th style="width: 10%;">Kasus</th>
                                    <th style="width: 10%;">Tipe Tarif</th>
                                    <th style="width: 10%;">Head Parent Tarif</th>
                                    <th style="width: 10%;">Tarif Pelayanan</th>
                                    <th style="width: 10%;">Diskon Semua Per Tarif</th>
                                    <th style="width: 10%;">Diskon Jasa Sarana</th>
                                    <th style="width: 10%;">Diskon Jasa Pelayanan</th>
                                    <th style="width: 10%;">Diskon BHP</th>
                                    <th style="width: 10%;">Diskon Biaya Perawatan</th>
                                    <th style="width: 10%;">Aksi</th>
                                </tr>
                                <tr>
                                    <td class="text-center" style="vertical-align: middle;">#</td>
                                    <td>
                                        <select id="laboratorium_tujuan_pasien" tabindex="16" style="width: 200px" data-placeholder="Tipe" class="js-select2 form-control input-sm">
                                            <option value="0">Semua Tujuan</option>
                                            <option value="1">Poliklinik</option>
                                            <option value="2">Instalasi Gawat Darurat (IGD)</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="laboratorium_status_tujuan_poliklinik" tabindex="16" style="width: 200px" data-placeholder="Status Tujuan Poliklinik" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="laboratorium_tujuan_poliklinik" tabindex="16" style="width: 200px" data-placeholder="Tujuan Poliklinik"  disabled class="js-select2 form-control input-sm">
                                            <option value="0">Semua Poliklinik</option>
                                            <?php foreach (get_all('mpoliklinik', array('status' => 1)) as $row){?>
                                            <option value="<?= $row->id?>"><?= $row->nama?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="laboratorium_status_pasien" tabindex="16" style="width: 200px" data-placeholder="Status Pasien" class="js-select2 form-control input-sm">
                                            <option value="0">Semua Status</option>
                                            <option value="1">Pasien Baru</option>
                                            <option value="2">Pasien Lama</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="laboratorium_kasus" tabindex="16" style="width: 200px" data-placeholder="Kasus" class="js-select2 form-control input-sm">
                                            <option value="0">Semua Kasus</option>
                                            <option value="1">Kasus Baru</option>
                                            <option value="2">Kasus Lama</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="laboratorium_tipe_tarif" tabindex="16" style="width: 200px" data-placeholder="Semua" class="js-select2 form-control input-sm">
                                            <option value="0">Semua</option>
                                            <option value="1">Umum</option>
                                            <option value="2">Pathologi Anatomi</option>
                                            <option value="3">PMI</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="laboratorium_head_parent_tarif" tabindex="16" style="width: 200px" data-placeholder="Semua" class="js-select2 form-control input-sm">
                                            <option value="0">Semua</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="laboratorium_tarif_pelayanan" tabindex="16" style="width: 200px" data-placeholder="Semua" class="js-select2 form-control input-sm">
                                            <option value="0">Semua</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="laboratorium_status_diskon_all" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="laboratorium_tipe_diskon_all" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="laboratorium_diskon_all_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="laboratorium_status_diskon_jasasarana" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="laboratorium_tipe_diskon_jasasarana" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="laboratorium_diskon_jasasarana_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="laboratorium_status_diskon_jasapelayanan" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="laboratorium_tipe_diskon_jasapelayanan" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="laboratorium_diskon_jasapelayanan_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="laboratorium_status_diskon_bhp" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="laboratorium_tipe_diskon_bhp" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="laboratorium_diskon_bhp_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="laboratorium_status_diskon_biayaperawatan" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="laboratorium_tipe_diskon_biayaperawatan" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="laboratorium_diskon_biayaperawatan_nilai" value="" />
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-xs btn-primary" id="btn-submit-diskon-laboratorium"><i class="fa fa-save"></i> Tambahkan</button>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($list_setting_diskon_laboratorium as $index => $row) { ?>
                                    <tr>
                                        <td class="text-center"><?php echo $index + 1; ?></td>
                                        <td><?php echo $row->tujuan_pasien; ?></td>
                                        <td><?php echo $row->tujuan_poliklinik; ?></td>
                                        <td><?php echo $row->status_pasien; ?></td>
                                        <td><?php echo $row->kasus; ?></td>
                                        <td><?php echo $row->jenis_tarif; ?></td>
                                        <td><?php echo $row->head_parent_tarif; ?></td>
                                        <td>
                                            <?php
                                            if ($row->tarif_pelayanan == "Semua") {
                                                echo "Semua";
                                            } else {
                                                echo TreeView($row->level_tarif_pelayanan, $row->tarif_pelayanan);
                                            }
                                            ?>      
                                        </td>
                                        <td><?php echo $row->diskon_all; ?></td>
                                        <td><?php echo $row->diskon_jasasarana; ?></td>
                                        <td><?php echo $row->diskon_jasapelayanan; ?></td>
                                        <td><?php echo $row->diskon_bhp; ?></td>
                                        <td><?php echo $row->diskon_biaya_perawatan; ?></td>
                                        <td>
                                            <a href="#" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm delete-link" data-url="{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/delete_setting_laboratorium/<?= $row->id; ?>"><i class="fa fa-trash-o"></i></a>    
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <hr>
                    
                    <h5><?=text_primary('Diskon Tarif Pelayanan Fisioterapi');?></h5>
                    <br />
                    
                    <div style="overflow-x: auto; overflow-y: auto; max-height: 500px;">
                        <table class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width: 5%;" class="text-center">No</th>
                                    <th style="width: 10%;">Tujuan</th>
                                    <th style="width: 10%;">Tujuan Poliklinik</th>
                                    <th style="width: 10%;">Status Pasien</th>
                                    <th style="width: 10%;">Kasus</th>
                                    <th style="width: 10%;">Head Parent Tarif</th>
                                    <th style="width: 10%;">Tarif Pelayanan</th>
                                    <th style="width: 10%;">Diskon Semua Per Tarif</th>
                                    <th style="width: 10%;">Diskon Jasa Sarana</th>
                                    <th style="width: 10%;">Diskon Jasa Pelayanan</th>
                                    <th style="width: 10%;">Diskon BHP</th>
                                    <th style="width: 10%;">Diskon Biaya Perawatan</th>
                                    <th style="width: 10%;">Aksi</th>
                                </tr>
                                <tr>
                                    <td class="text-center" style="vertical-align: middle;">#</td>
                                    <td>
                                        <select id="fisioterapi_tujuan_pasien" tabindex="16" style="width: 200px" data-placeholder="Tipe" class="js-select2 form-control input-sm">
                                            <option value="0">Semua Tujuan</option>
                                            <option value="1">Poliklinik</option>
                                            <option value="2">Instalasi Gawat Darurat (IGD)</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="fisioterapi_status_tujuan_poliklinik" tabindex="16" style="width: 200px" data-placeholder="Status Tujuan Poliklinik" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="fisioterapi_tujuan_poliklinik" tabindex="16" style="width: 200px" data-placeholder="Tujuan Poliklinik"  disabled class="js-select2 form-control input-sm">
                                            <option value="0">Semua Poliklinik</option>
                                            <?php foreach (get_all('mpoliklinik', array('status' => 1)) as $row){?>
                                            <option value="<?= $row->id?>"><?= $row->nama?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="fisioterapi_status_pasien" tabindex="16" style="width: 200px" data-placeholder="Status Pasien" class="js-select2 form-control input-sm">
                                            <option value="0">Semua Status</option>
                                            <option value="1">Pasien Baru</option>
                                            <option value="2">Pasien Lama</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="fisioterapi_kasus" tabindex="16" style="width: 200px" data-placeholder="Kasus" class="js-select2 form-control input-sm">
                                            <option value="0">Semua Kasus</option>
                                            <option value="1">Kasus Baru</option>
                                            <option value="2">Kasus Lama</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="fisioterapi_head_parent_tarif" tabindex="16" style="width: 200px" data-placeholder="Semua" class="js-select2 form-control input-sm">
                                            <option value="0">Semua</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="fisioterapi_tarif_pelayanan" tabindex="16" style="width: 200px" data-placeholder="Semua" class="js-select2 form-control input-sm">
                                            <option value="0">Semua</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="fisioterapi_status_diskon_all" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="fisioterapi_tipe_diskon_all" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="fisioterapi_diskon_all_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="fisioterapi_status_diskon_jasasarana" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="fisioterapi_tipe_diskon_jasasarana" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="fisioterapi_diskon_jasasarana_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="fisioterapi_status_diskon_jasapelayanan" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="fisioterapi_tipe_diskon_jasapelayanan" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="fisioterapi_diskon_jasapelayanan_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="fisioterapi_status_diskon_bhp" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="fisioterapi_tipe_diskon_bhp" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="fisioterapi_diskon_bhp_nilai" value="" />
                                    </td>
                                    <td>
                                        <select id="fisioterapi_status_diskon_biayaperawatan" tabindex="16" style="width: 200px" data-placeholder="Status Diskon" class="js-select2 form-control input-sm">
                                            <option value="0">Tidak Ditentukan</option>
                                            <option value="1">Ditentukan</option>
                                        </select>
                                        <br />
                                        <br />
                                        <select id="fisioterapi_tipe_diskon_biayaperawatan" tabindex="16" disabled style="width: 200px" data-placeholder="Tipe Diskon" class="js-select2 form-control input-sm">
                                            <option value="1">%</option>
                                            <option value="2">Rp</option>
                                        </select>
                                        <br />
                                        <br />
                                        <input type="text" class="form-control input-sm number" disabled id="fisioterapi_diskon_biayaperawatan_nilai" value="" />
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-xs btn-primary" id="btn-submit-diskon-fisioterapi"><i class="fa fa-save"></i> Tambahkan</button>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($list_setting_diskon_fisioterapi as $index => $row) { ?>
                                    <tr>
                                        <td class="text-center"><?php echo $index + 1; ?></td>
                                        <td><?php echo $row->tujuan_pasien; ?></td>
                                        <td><?php echo $row->tujuan_poliklinik; ?></td>
                                        <td><?php echo $row->status_pasien; ?></td>
                                        <td><?php echo $row->kasus; ?></td>
                                        <td><?php echo $row->head_parent_tarif; ?></td>
                                        <td>
                                            <?php
                                            if ('Semua' === $row->tarif_pelayanan) {
                                                echo 'Semua';
                                            } else {
                                                echo TreeView($row->level_tarif_pelayanan, $row->tarif_pelayanan);
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $row->diskon_all; ?></td>
                                        <td><?php echo $row->diskon_jasasarana; ?></td>
                                        <td><?php echo $row->diskon_jasapelayanan; ?></td>
                                        <td><?php echo $row->diskon_bhp; ?></td>
                                        <td><?php echo $row->diskon_biaya_perawatan; ?></td>
                                        <td>
                                            <a href="#" data-url="{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/delete_setting_fisioterapi/<?= $row->id; ?>" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm delete-link"><i class="fa fa-trash-o"></i></a>    
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <div class="tab-pane fade" id="tab_diskon_bmhp">
					<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group" style="margin-top: 15px;">
								<h5><?=text_primary('DISKON INPUTAN BMHP')?></h5>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 15px;">
									<div class="table-responsive">
										<table class="table table-bordered table-striped" id="index_tampil_bmhp">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="10%">Tujuan</th>
													<th width="15%">Poliklinik</th>
													<th width="10%">Status Pasien</th>
													<th width="10%">Kasus</th>
													<th width="10%">Tipe</th>
													<th width="20%">Nama Barang</th>
													<th width="10%">Jumlah Diskon</th>
													<th width="10%">Action</th>										   
												</tr>
												<?
													$idtipe='0';
													$tipe='0';
													$idpoli='0';
													$statuspasienbaru='2';
													$pertemuan_id='0';
												?>
												<tr>
													<th>#</th>
													<th>
														<select id="idtipe_poli_bmhp"  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
															
															<option value="0" selected>-Tujuan-</option>
															<option value="1" >POLIKINIK</option>
															<option value="2" >IGD</option>
															
														</select>
													</th>
													<th>
														<select id="idpoli_bmhp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
															<option value="#" <?=($idpoli=='#'?'selected':'')?>>-Semua Poli-</option>
															<?foreach(get_all('mpoliklinik',array('status'=>1)) as $r){?>
																<option value="<?=$r->id?>" <?=($idpoli==$r->id?'selected':'')?>><?=$r->nama?></option>
															<?}?>
															
															
														</select>
													</th>
													
													<th>
														<select tabindex="4" id="statuspasienbaru_bmhp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
															<option value="2" selected>SEMUA</option>
															<option value="1" <?=($statuspasienbaru == 1 ? 'selected="selected"' : '')?>>PASIEN BARU</option>
															<option value="0" <?=($statuspasienbaru == 0 ? 'selected="selected"' : '')?>>PASIEN LAMA</option>
														</select>
													</th>
													<th>
														<select id="pertemuan_id_bmhp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
															<option value="#" <?=($pertemuan_id==''?'selected':'')?>>- All Kasus -</option>
															<?foreach(list_variable_ref(15) as $r){?>
															<option value="<?=$r->id?>" <?=($pertemuan_id==$r->id?'selected':'')?>><?=$r->nama?></option>
															<?}?>
															
														</select>
													</th>
													<th>
														<select id="idtipe_bmhp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
															<option value="0" selected>SEMUA</option>
															<?foreach(get_all('mdata_tipebarang',array('id <>'=>'4')) as $row){?>
															<option value="<?=$row->id?>"><?=$row->nama_tipe?></option>
															<?}?>
															
															
														</select>
													</th>
													<th>
														<select id="idbarang_bmhp" class="form-control" style="width: 100%;" data-placeholder="Choose one..">
															
														</select>
													</th>
													<th>
														<select id="operand_bmhp" style="width: 100%" id="operand" class="js-select2 form-control input-sm">					
															<option value="0" selected>TIDAK DITENTUKAN</option>										
															<option value="1">%</option>										
															<option value="2">Rp.</option>	
														</select>
														<input type="text" style="width: 100%" disabled class="form-control discount" id="diskon_bmhp" placeholder="0" value="0">
													</th>
													
													<th>
														<button class="btn btn-primary btn-sm" type="button" onclick="simpan_bmhp()"><i class="fa fa-plus"></i> Tambah</button>
													</th>										   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>					
								</div>
							</div>
						</div>
					</div>

                    <hr>

					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="form-group" style="margin-top: 15px;">
								<h5><?=text_primary('DISKON INPUTAN FARMASI')?></h5>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 15px;">
									<div class="table-responsive">
										<table class="table table-bordered table-striped" id="index_tampil_farmasi">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="10%">Tujuan</th>
													<th width="15%">Poliklinik</th>
													<th width="10%">Status Pasien</th>
													<th width="10%">Kasus</th>
													<th width="10%">Tipe</th>
													<th width="20%">Nama Barang</th>
													<th width="10%">Jumlah Diskon</th>
													<th width="10%">Action</th>										   
												</tr>
												<?
													$idtipe='0';
													$tipe='0';
													$idpoli='0';
													$statuspasienbaru='2';
													$pertemuan_id='0';
												?>
												<tr>
													<th>#</th>
													<th>
														<select id="idtipe_poli_farmasi"  class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
															
															<option value="0" selected>-Tujuan-</option>
															<option value="1" >POLIKINIK</option>
															<option value="2" >IGD</option>
															
														</select>
													</th>
													<th>
														<select id="idpoli_farmasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
															<option value="#" <?=($idpoli=='#'?'selected':'')?>>-Semua Poli-</option>
															<?foreach(get_all('mpoliklinik',array('status'=>1)) as $r){?>
																<option value="<?=$r->id?>" <?=($idpoli==$r->id?'selected':'')?>><?=$r->nama?></option>
															<?}?>
															
															
														</select>
													</th>
													
													<th>
														<select tabindex="4" id="statuspasienbaru_farmasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
															<option value="2" selected>SEMUA</option>
															<option value="1" <?=($statuspasienbaru == 1 ? 'selected="selected"' : '')?>>PASIEN BARU</option>
															<option value="0" <?=($statuspasienbaru == 0 ? 'selected="selected"' : '')?>>PASIEN LAMA</option>
														</select>
													</th>
													<th>
														<select id="pertemuan_id_farmasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
															<option value="#" <?=($pertemuan_id==''?'selected':'')?>>- All Kasus -</option>
															<?foreach(list_variable_ref(15) as $r){?>
															<option value="<?=$r->id?>" <?=($pertemuan_id==$r->id?'selected':'')?>><?=$r->nama?></option>
															<?}?>
															
														</select>
													</th>
													<th>
														<select id="idtipe_farmasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
															<option value="0" selected>SEMUA</option>
															<?foreach(get_all('mdata_tipebarang',array('id <>'=>'4')) as $row){?>
															<option value="<?=$row->id?>"><?=$row->nama_tipe?></option>
															<?}?>
															
															
														</select>
													</th>
													<th>
														<select id="idbarang_farmasi" class="form-control" style="width: 100%;" data-placeholder="Choose one..">
															
														</select>
													</th>
													<th>
														<select id="operand_farmasi" style="width: 100%" id="operand" class="js-select2 form-control input-sm">					
															<option value="0" selected>TIDAK DITENTUKAN</option>										
															<option value="1">%</option>										
															<option value="2">Rp.</option>	
														</select>
														<input type="text" style="width: 100%" disabled class="form-control discount" id="diskon_farmasi" placeholder="0" value="0">
													</th>
													
													<th>
														<button class="btn btn-primary btn-sm" type="button" onclick="simpan_farmasi()"><i class="fa fa-plus"></i> Tambah</button>
													</th>										   
												</tr>
												
											</thead>
											<tbody></tbody>
										</table>
									</div>					
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".number").number(true,0,'.',',');
        $(".discount").number(true,2,'.',',');
		
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var targetTab = $(e.target).attr('href');
            localStorage.setItem('activeTab', targetTab);
        });

        // Baca nilai dari localStorage dan tetapkan tab yang sesuai sebagai tab aktif
        var activeTab = localStorage.getItem('activeTab');
        if (activeTab) {
            $('.nav-tabs a[href="' + activeTab + '"]').tab('show');
        }

        $('ul.nav-tabs').on('click', 'li', function () {
            var activeTab = $(this).find('a').attr('href');
            localStorage.setItem('activeTab', activeTab);
        });

        populateTarifAdministrasi(0);
        populateTarifPelayanan(0);
        populateHeadTarifRadiologi(0);
        populateTarifRadiologi(0, 0);
        populateHeadTarifLaboratorium(0);
        populateTarifLaboratorium(0, 0);
        populateHeadTarifFisioterapi(0);
        populateTarifFisioterapi(0);
		load_bmhp();
		load_farmasi();
		
        $("#adm_jenis_tarif").change(function () {
            var selectedJenisTarif = $(this).val();
            populateTarifAdministrasi(selectedJenisTarif);
        });

        $("#pelayanan_head_parent_tarif").change(function () {
            var selectedHeadParentTarif = $(this).find('option:selected').attr('data-path');
            populateTarifPelayanan(selectedHeadParentTarif);
        });

        $("#radiologi_tipe_tarif").change(function () {
            var selectedTipeTarif = $(this).val();
            populateHeadTarifRadiologi(selectedTipeTarif);
        });

        $("#radiologi_head_parent_tarif").change(function () {
            var selectedTipeTarif = $("#radiologi_tipe_tarif option:selected").val();
            var selectedHeadParentTarif = $("#radiologi_head_parent_tarif option:selected").data('path');

            populateTarifRadiologi(selectedTipeTarif, selectedHeadParentTarif);
        });

        $("#laboratorium_tipe_tarif").change(function () {
            var selectedTipeTarif = $(this).val();
            populateHeadTarifLaboratorium(selectedTipeTarif);
        });

        $("#laboratorium_head_parent_tarif").change(function () {
            var selectedTipeTarif = $("#laboratorium_tipe_tarif option:selected").val();
            var selectedHeadParentTarif = $("#laboratorium_head_parent_tarif option:selected").data('path');

            populateTarifLaboratorium(selectedTipeTarif, selectedHeadParentTarif);
        });

        $("#fisioterapi_tipe_tarif").change(function () {
            var selectedTipeTarif = $(this).val();
            populateHeadTarifFisioterapi(selectedTipeTarif);
        });

        $("#fisioterapi_head_parent_tarif").change(function () {
            var selectedHeadParentTarif = $("#fisioterapi_head_parent_tarif option:selected").data('path');

            populateTarifFisioterapi(selectedHeadParentTarif);
        });

        $("#btn-submit-diskon-all").click(function(){
            var tujuan_pasien = $("#all_tujuan_pasien").val();
            var status_tujuan_poliklinik = $("#all_status_tujuan_poliklinik").val();
            var tujuan_poliklinik = $("#all_tujuan_poliklinik").val();
            var status_pasien = $("#all_status_pasien").val();
            var kasus = $("#all_kasus").val();
            var status_diskon_rp = $("#all_status_diskon_rp").val();
            var diskon_rp = $("#all_diskon_rp").val();
            var status_diskon_persen = $("#all_status_diskon_persen").val();
            var diskon_persen = $("#all_diskon_persen").val();

            $.ajax({
                type: 'POST',
                url: '{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/save_setting_all',
                data: {
                    tipe: 1,
                    idkelompokpasien: '{id}',
                    idrekanan: 0,
                    tujuan_pasien: tujuan_pasien,
                    status_tujuan_poliklinik: status_tujuan_poliklinik,
                    tujuan_poliklinik: tujuan_poliklinik,
                    status_pasien: status_pasien,
                    kasus: kasus,
                    status_diskon_rp: status_diskon_rp,
                    diskon_rp: diskon_rp,
                    status_diskon_persen: status_diskon_persen,
                    diskon_persen: diskon_persen
                },
                success: function(response){
                    swal("Berhasil!", "Data Setting telah berhasil ditambahkan", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 150);
                },
                error: function(xhr, status, error){
                    // Handle error here
                }
            });
        });

        $("#btn-submit-diskon-adm").click(function(){
            var tujuan_pasien = $("#adm_tujuan_pasien").val();
            var status_tujuan_poliklinik = $("#adm_status_tujuan_poliklinik").val();
            var tujuan_poliklinik = $("#adm_tujuan_poliklinik").val();
            var status_pasien = $("#adm_status_pasien").val();
            var kasus = $("#adm_kasus").val();
            var jenis_tarif = $("#adm_jenis_tarif").val();
            var tarif_pelayanan = $("#adm_tarif_pelayanan").val();
            var status_diskon_all = $("#adm_status_diskon_all").val();
            var tipe_diskon_all = $("#adm_tipe_diskon_all").val();
            var diskon_all_nilai = $("#adm_diskon_all_nilai").val();
            var status_diskon_jasasarana = $("#adm_status_diskon_jasasarana").val();
            var tipe_diskon_jasasarana = $("#adm_tipe_diskon_jasasarana").val();
            var diskon_jasasarana_nilai = $("#adm_diskon_jasasarana_nilai").val();
            var status_diskon_jasapelayanan = $("#adm_status_diskon_jasapelayanan").val();
            var tipe_diskon_jasapelayanan = $("#adm_tipe_diskon_jasapelayanan").val();
            var diskon_jasapelayanan_nilai = $("#adm_diskon_jasapelayanan_nilai").val();
            var status_diskon_bhp = $("#adm_status_diskon_bhp").val();
            var tipe_diskon_bhp = $("#adm_tipe_diskon_bhp").val();
            var diskon_bhp_nilai = $("#adm_diskon_bhp_nilai").val();
            var status_diskon_biayaperawatan = $("#adm_status_diskon_biayaperawatan").val();
            var tipe_diskon_biayaperawatan = $("#adm_tipe_diskon_biayaperawatan").val();
            var diskon_biayaperawatan_nilai = $("#adm_diskon_biayaperawatan_nilai").val();

            $.ajax({
                type: 'POST',
                url: '{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/save_setting_adm',
                data: {
                    tipe: 1,
                    idkelompokpasien: '{id}',
                    idrekanan: 0,
                    tujuan_pasien: tujuan_pasien,
                    status_tujuan_poliklinik: status_tujuan_poliklinik,
                    tujuan_poliklinik: tujuan_poliklinik,
                    status_pasien: status_pasien,
                    kasus: kasus,
                    jenis_tarif: jenis_tarif,
                    tarif_pelayanan: tarif_pelayanan,
                    status_diskon_all: status_diskon_all,
                    tipe_diskon_all: tipe_diskon_all,
                    diskon_all_nilai: diskon_all_nilai,
                    status_diskon_jasasarana: status_diskon_jasasarana,
                    tipe_diskon_jasasarana: tipe_diskon_jasasarana,
                    diskon_jasasarana_nilai: diskon_jasasarana_nilai,
                    status_diskon_jasapelayanan: status_diskon_jasapelayanan,
                    tipe_diskon_jasapelayanan: tipe_diskon_jasapelayanan,
                    diskon_jasapelayanan_nilai: diskon_jasapelayanan_nilai,
                    status_diskon_bhp: status_diskon_bhp,
                    tipe_diskon_bhp: tipe_diskon_bhp,
                    diskon_bhp_nilai: diskon_bhp_nilai,
                    status_diskon_biayaperawatan: status_diskon_biayaperawatan,
                    tipe_diskon_biayaperawatan: tipe_diskon_biayaperawatan,
                    diskon_biayaperawatan_nilai: diskon_biayaperawatan_nilai,
                },
                success: function(response){
                    swal("Berhasil!", "Data Setting telah berhasil ditambahkan", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 150);
                },
                error: function(xhr, status, error){
                    // Handle error here
                }
            });
        });

        $("#btn-submit-diskon-pelayanan").click(function(){
            var tujuan_pasien = $("#pelayanan_tujuan_pasien").val();
            var status_tujuan_poliklinik = $("#pelayanan_status_tujuan_poliklinik").val();
            var tujuan_poliklinik = $("#pelayanan_tujuan_poliklinik").val();
            var status_pasien = $("#pelayanan_status_pasien").val();
            var kasus = $("#pelayanan_kasus").val();
            var head_parent_tarif = $("#pelayanan_head_parent_tarif").val();
            var tarif_pelayanan = $("#pelayanan_tarif_pelayanan").val();
            var status_diskon_all = $("#pelayanan_status_diskon_all").val();
            var tipe_diskon_all = $("#pelayanan_tipe_diskon_all").val();
            var diskon_all_nilai = $("#pelayanan_diskon_all_nilai").val();
            var status_diskon_jasasarana = $("#pelayanan_status_diskon_jasasarana").val();
            var tipe_diskon_jasasarana = $("#pelayanan_tipe_diskon_jasasarana").val();
            var diskon_jasasarana_nilai = $("#pelayanan_diskon_jasasarana_nilai").val();
            var status_diskon_jasapelayanan = $("#pelayanan_status_diskon_jasapelayanan").val();
            var tipe_diskon_jasapelayanan = $("#pelayanan_tipe_diskon_jasapelayanan").val();
            var diskon_jasapelayanan_nilai = $("#pelayanan_diskon_jasapelayanan_nilai").val();
            var status_diskon_bhp = $("#pelayanan_status_diskon_bhp").val();
            var tipe_diskon_bhp = $("#pelayanan_tipe_diskon_bhp").val();
            var diskon_bhp_nilai = $("#pelayanan_diskon_bhp_nilai").val();
            var status_diskon_biayaperawatan = $("#pelayanan_status_diskon_biayaperawatan").val();
            var tipe_diskon_biayaperawatan = $("#pelayanan_tipe_diskon_biayaperawatan").val();
            var diskon_biayaperawatan_nilai = $("#pelayanan_diskon_biayaperawatan_nilai").val();

            $.ajax({
                type: 'POST',
                url: '{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/save_setting_pelayanan',
                data: {
                    tipe: 1,
                    idkelompokpasien: '{id}',
                    idrekanan: 0,
                    tujuan_pasien: tujuan_pasien,
                    status_tujuan_poliklinik: status_tujuan_poliklinik,
                    tujuan_poliklinik: tujuan_poliklinik,
                    status_pasien: status_pasien,
                    kasus: kasus,
                    head_parent_tarif: head_parent_tarif,
                    tarif_pelayanan: tarif_pelayanan,
                    status_diskon_all: status_diskon_all,
                    tipe_diskon_all: tipe_diskon_all,
                    diskon_all_nilai: diskon_all_nilai,
                    status_diskon_jasasarana: status_diskon_jasasarana,
                    tipe_diskon_jasasarana: tipe_diskon_jasasarana,
                    diskon_jasasarana_nilai: diskon_jasasarana_nilai,
                    status_diskon_jasapelayanan: status_diskon_jasapelayanan,
                    tipe_diskon_jasapelayanan: tipe_diskon_jasapelayanan,
                    diskon_jasapelayanan_nilai: diskon_jasapelayanan_nilai,
                    status_diskon_bhp: status_diskon_bhp,
                    tipe_diskon_bhp: tipe_diskon_bhp,
                    diskon_bhp_nilai: diskon_bhp_nilai,
                    status_diskon_biayaperawatan: status_diskon_biayaperawatan,
                    tipe_diskon_biayaperawatan: tipe_diskon_biayaperawatan,
                    diskon_biayaperawatan_nilai: diskon_biayaperawatan_nilai,
                },
                success: function(response){
                    swal("Berhasil!", "Data Setting telah berhasil ditambahkan", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 150);
                },
                error: function(xhr, status, error){
                    // Handle error here
                }
            });
        });

        $("#btn-submit-diskon-radiologi").click(function(){
            var tujuan_pasien = $("#radiologi_tujuan_pasien").val();
            var status_tujuan_poliklinik = $("#radiologi_status_tujuan_poliklinik").val();
            var tujuan_poliklinik = $("#radiologi_tujuan_poliklinik").val();
            var status_pasien = $("#radiologi_status_pasien").val();
            var kasus = $("#radiologi_kasus").val();
            var tipe_tarif = $("#radiologi_tipe_tarif").val();
            var head_parent_tarif = $("#radiologi_head_parent_tarif").val();
            var tarif_pelayanan = $("#radiologi_tarif_pelayanan").val();
            var status_diskon_all = $("#radiologi_status_diskon_all").val();
            var tipe_diskon_all = $("#radiologi_tipe_diskon_all").val();
            var diskon_all_nilai = $("#radiologi_diskon_all_nilai").val();
            var status_diskon_jasasarana = $("#radiologi_status_diskon_jasasarana").val();
            var tipe_diskon_jasasarana = $("#radiologi_tipe_diskon_jasasarana").val();
            var diskon_jasasarana_nilai = $("#radiologi_diskon_jasasarana_nilai").val();
            var status_diskon_jasapelayanan = $("#radiologi_status_diskon_jasapelayanan").val();
            var tipe_diskon_jasapelayanan = $("#radiologi_tipe_diskon_jasapelayanan").val();
            var diskon_jasapelayanan_nilai = $("#radiologi_pelayanan_nilai").val();
            var status_diskon_bhp = $("#radiologi_status_diskon_bhp").val();
            var tipe_diskon_bhp = $("#radiologi_tipe_diskon_bhp").val();
            var diskon_bhp_nilai = $("#radiologi_diskon_bhp_nilai").val();
            var status_diskon_biayaperawatan = $("#radiologi_status_diskon_biayaperawatan").val();
            var tipe_diskon_biayaperawatan = $("#radiologi_tipe_diskon_biayaperawatan").val();
            var diskon_biayaperawatan_nilai = $("#radiologi_diskon_biayaperawatan_nilai").val();

            $.ajax({
                type: 'POST',
                url: '{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/save_setting_radiologi',
                data: {
                    tipe: 1,
                    idkelompokpasien: '{id}',
                    idrekanan: 0,
                    tujuan_pasien: tujuan_pasien,
                    status_tujuan_poliklinik: status_tujuan_poliklinik,
                    tujuan_poliklinik: tujuan_poliklinik,
                    status_pasien: status_pasien,
                    kasus: kasus,
                    tipe_tarif: tipe_tarif,
                    head_parent_tarif: head_parent_tarif,
                    tarif_pelayanan: tarif_pelayanan,
                    status_diskon_all: status_diskon_all,
                    tipe_diskon_all: tipe_diskon_all,
                    diskon_all_nilai: diskon_all_nilai,
                    status_diskon_jasasarana: status_diskon_jasasarana,
                    tipe_diskon_jasasarana: tipe_diskon_jasasarana,
                    diskon_jasasarana_nilai: diskon_jasasarana_nilai,
                    status_diskon_jasapelayanan: status_diskon_jasapelayanan,
                    tipe_diskon_jasapelayanan: tipe_diskon_jasapelayanan,
                    diskon_jasapelayanan_nilai: diskon_jasapelayanan_nilai,
                    status_diskon_bhp: status_diskon_bhp,
                    tipe_diskon_bhp: tipe_diskon_bhp,
                    diskon_bhp_nilai: diskon_bhp_nilai,
                    status_diskon_biayaperawatan: status_diskon_biayaperawatan,
                    tipe_diskon_biayaperawatan: tipe_diskon_biayaperawatan,
                    diskon_biayaperawatan_nilai: diskon_biayaperawatan_nilai,
                },
                success: function(response){
                    swal("Berhasil!", "Data Setting telah berhasil ditambahkan", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 150);
                },
                error: function(xhr, status, error){
                    // Handle error here
                }
            });
        });

        $("#btn-submit-diskon-laboratorium").click(function(){
            var tujuan_pasien = $("#laboratorium_tujuan_pasien").val();
            var status_tujuan_poliklinik = $("#laboratorium_status_tujuan_poliklinik").val();
            var tujuan_poliklinik = $("#laboratorium_tujuan_poliklinik").val();
            var status_pasien = $("#laboratorium_status_pasien").val();
            var kasus = $("#laboratorium_kasus").val();
            var tipe_tarif = $("#laboratorium_tipe_tarif").val();
            var head_parent_tarif = $("#laboratorium_head_parent_tarif").val();
            var tarif_pelayanan = $("#laboratorium_tarif_pelayanan").val();
            var status_diskon_all = $("#laboratorium_status_diskon_all").val();
            var tipe_diskon_all = $("#laboratorium_tipe_diskon_all").val();
            var diskon_all_nilai = $("#laboratorium_diskon_all_nilai").val();
            var status_diskon_jasasarana = $("#laboratorium_status_diskon_jasasarana").val();
            var tipe_diskon_jasasarana = $("#laboratorium_tipe_diskon_jasasarana").val();
            var diskon_jasasarana_nilai = $("#laboratorium_diskon_jasasarana_nilai").val();
            var status_diskon_jasapelayanan = $("#laboratorium_status_diskon_jasapelayanan").val();
            var tipe_diskon_jasapelayanan = $("#laboratorium_tipe_diskon_jasapelayanan").val();
            var diskon_jasapelayanan_nilai = $("#laboratorium_pelayanan_nilai").val();
            var status_diskon_bhp = $("#laboratorium_status_diskon_bhp").val();
            var tipe_diskon_bhp = $("#laboratorium_tipe_diskon_bhp").val();
            var diskon_bhp_nilai = $("#laboratorium_diskon_bhp_nilai").val();
            var status_diskon_biayaperawatan = $("#laboratorium_status_diskon_biayaperawatan").val();
            var tipe_diskon_biayaperawatan = $("#laboratorium_tipe_diskon_biayaperawatan").val();
            var diskon_biayaperawatan_nilai = $("#laboratorium_diskon_biayaperawatan_nilai").val();

            $.ajax({
                type: 'POST',
                url: '{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/save_setting_laboratorium',
                data: {
                    tipe: 1,
                    idkelompokpasien: '{id}',
                    idrekanan: 0,
                    tujuan_pasien: tujuan_pasien,
                    status_tujuan_poliklinik: status_tujuan_poliklinik,
                    tujuan_poliklinik: tujuan_poliklinik,
                    status_pasien: status_pasien,
                    kasus: kasus,
                    tipe_tarif: tipe_tarif,
                    head_parent_tarif: head_parent_tarif,
                    tarif_pelayanan: tarif_pelayanan,
                    status_diskon_all: status_diskon_all,
                    tipe_diskon_all: tipe_diskon_all,
                    diskon_all_nilai: diskon_all_nilai,
                    status_diskon_jasasarana: status_diskon_jasasarana,
                    tipe_diskon_jasasarana: tipe_diskon_jasasarana,
                    diskon_jasasarana_nilai: diskon_jasasarana_nilai,
                    status_diskon_jasapelayanan: status_diskon_jasapelayanan,
                    tipe_diskon_jasapelayanan: tipe_diskon_jasapelayanan,
                    diskon_jasapelayanan_nilai: diskon_jasapelayanan_nilai,
                    status_diskon_bhp: status_diskon_bhp,
                    tipe_diskon_bhp: tipe_diskon_bhp,
                    diskon_bhp_nilai: diskon_bhp_nilai,
                    status_diskon_biayaperawatan: status_diskon_biayaperawatan,
                    tipe_diskon_biayaperawatan: tipe_diskon_biayaperawatan,
                    diskon_biayaperawatan_nilai: diskon_biayaperawatan_nilai,
                },
                success: function(response){
                    swal("Berhasil!", "Data Setting telah berhasil ditambahkan", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 150);
                },
                error: function(xhr, status, error){
                    // Handle error here
                }
            });
        });

        $("#btn-submit-diskon-fisioterapi").click(function(){
            var tujuan_pasien = $("#fisioterapi_tujuan_pasien").val();
            var status_tujuan_poliklinik = $("#fisioterapi_status_tujuan_poliklinik").val();
            var tujuan_poliklinik = $("#fisioterapi_tujuan_poliklinik").val();
            var status_pasien = $("#fisioterapi_status_pasien").val();
            var kasus = $("#fisioterapi_kasus").val();
            var head_parent_tarif = $("#fisioterapi_head_parent_tarif").val();
            var tarif_pelayanan = $("#fisioterapi_tarif_pelayanan").val();
            var status_diskon_all = $("#fisioterapi_status_diskon_all").val();
            var tipe_diskon_all = $("#fisioterapi_tipe_diskon_all").val();
            var diskon_all_nilai = $("#fisioterapi_diskon_all_nilai").val();
            var status_diskon_jasasarana = $("#fisioterapi_status_diskon_jasasarana").val();
            var tipe_diskon_jasasarana = $("#fisioterapi_tipe_diskon_jasasarana").val();
            var diskon_jasasarana_nilai = $("#fisioterapi_diskon_jasasarana_nilai").val();
            var status_diskon_jasapelayanan = $("#fisioterapi_status_diskon_jasapelayanan").val();
            var tipe_diskon_jasapelayanan = $("#fisioterapi_tipe_diskon_jasapelayanan").val();
            var diskon_jasapelayanan_nilai = $("#fisioterapi_diskon_jasapelayanan_nilai").val();
            var status_diskon_bhp = $("#fisioterapi_status_diskon_bhp").val();
            var tipe_diskon_bhp = $("#fisioterapi_tipe_diskon_bhp").val();
            var diskon_bhp_nilai = $("#fisioterapi_diskon_bhp_nilai").val();
            var status_diskon_biayaperawatan = $("#fisioterapi_status_diskon_biayaperawatan").val();
            var tipe_diskon_biayaperawatan = $("#fisioterapi_tipe_diskon_biayaperawatan").val();
            var diskon_biayaperawatan_nilai = $("#fisioterapi_diskon_biayaperawatan_nilai").val();

            $.ajax({
                type: 'POST',
                url: '{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/save_setting_fisioterapi',
                data: {
                    tipe: 1,
                    idkelompokpasien: '{id}',
                    idrekanan: 0,
                    tujuan_pasien: tujuan_pasien,
                    status_tujuan_poliklinik: status_tujuan_poliklinik,
                    tujuan_poliklinik: tujuan_poliklinik,
                    status_pasien: status_pasien,
                    kasus: kasus,
                    head_parent_tarif: head_parent_tarif,
                    tarif_pelayanan: tarif_pelayanan,
                    status_diskon_all: status_diskon_all,
                    tipe_diskon_all: tipe_diskon_all,
                    diskon_all_nilai: diskon_all_nilai,
                    status_diskon_jasasarana: status_diskon_jasasarana,
                    tipe_diskon_jasasarana: tipe_diskon_jasasarana,
                    diskon_jasasarana_nilai: diskon_jasasarana_nilai,
                    status_diskon_jasapelayanan: status_diskon_jasapelayanan,
                    tipe_diskon_jasapelayanan: tipe_diskon_jasapelayanan,
                    diskon_jasapelayanan_nilai: diskon_jasapelayanan_nilai,
                    status_diskon_bhp: status_diskon_bhp,
                    tipe_diskon_bhp: tipe_diskon_bhp,
                    diskon_bhp_nilai: diskon_bhp_nilai,
                    status_diskon_biayaperawatan: status_diskon_biayaperawatan,
                    tipe_diskon_biayaperawatan: tipe_diskon_biayaperawatan,
                    diskon_biayaperawatan_nilai: diskon_biayaperawatan_nilai,
                },
                success: function(response){
                    swal("Berhasil!", "Data Setting telah berhasil ditambahkan", "success");
                    setTimeout(function() {
                        location.reload();
                    }, 150);
                },
                error: function(xhr, status, error){
                    // Handle error here
                }
            });
        });

        // On change event for select elements
        handleInputChange("#all_status_tujuan_poliklinik", "#all_tujuan_poliklinik");
        handleInputChange("#all_status_diskon_rp", "#all_diskon_rp");
        handleInputChange("#all_status_diskon_persen", "#all_diskon_persen");
        
        handleInputChange("#adm_status_tujuan_poliklinik", "#adm_tujuan_poliklinik");
        handleInputChange("#adm_status_diskon_all", "#adm_tipe_diskon_all");
        handleInputChange("#adm_status_diskon_all", "#adm_diskon_all_nilai");
        handleInputChange("#adm_status_diskon_jasasarana", "#adm_tipe_diskon_jasasarana");
        handleInputChange("#adm_status_diskon_jasasarana", "#adm_diskon_jasasarana_nilai");
        handleInputChange("#adm_status_diskon_jasapelayanan", "#adm_tipe_diskon_jasapelayanan");
        handleInputChange("#adm_status_diskon_jasapelayanan", "#adm_diskon_jasapelayanan_nilai");
        handleInputChange("#adm_status_diskon_bhp", "#adm_tipe_diskon_bhp");
        handleInputChange("#adm_status_diskon_bhp", "#adm_diskon_bhp_nilai");
        handleInputChange("#adm_status_diskon_biayaperawatan", "#adm_tipe_diskon_biayaperawatan");
        handleInputChange("#adm_status_diskon_biayaperawatan", "#adm_diskon_biayaperawatan_nilai");
        
        handleInputChange("#pelayanan_status_tujuan_poliklinik", "#pelayanan_tujuan_poliklinik");
        handleInputChange("#pelayanan_status_diskon_all", "#pelayanan_tipe_diskon_all");
        handleInputChange("#pelayanan_status_diskon_all", "#pelayanan_diskon_all_nilai");
        handleInputChange("#pelayanan_status_diskon_jasasarana", "#pelayanan_tipe_diskon_jasasarana");
        handleInputChange("#pelayanan_status_diskon_jasasarana", "#pelayanan_diskon_jasasarana_nilai");
        handleInputChange("#pelayanan_status_diskon_jasapelayanan", "#pelayanan_tipe_diskon_jasapelayanan");
        handleInputChange("#pelayanan_status_diskon_jasapelayanan", "#pelayanan_diskon_jasapelayanan_nilai");
        handleInputChange("#pelayanan_status_diskon_bhp", "#pelayanan_tipe_diskon_bhp");
        handleInputChange("#pelayanan_status_diskon_bhp", "#pelayanan_diskon_bhp_nilai");
        handleInputChange("#pelayanan_status_diskon_biayaperawatan", "#pelayanan_tipe_diskon_biayaperawatan");
        handleInputChange("#pelayanan_status_diskon_biayaperawatan", "#pelayanan_diskon_biayaperawatan_nilai");
        
        handleInputChange("#radiologi_status_tujuan_poliklinik", "#radiologi_tujuan_poliklinik");
        handleInputChange("#radiologi_status_diskon_all", "#radiologi_tipe_diskon_all");
        handleInputChange("#radiologi_status_diskon_all", "#radiologi_diskon_all_nilai");
        handleInputChange("#radiologi_status_diskon_jasasarana", "#radiologi_tipe_diskon_jasasarana");
        handleInputChange("#radiologi_status_diskon_jasasarana", "#radiologi_diskon_jasasarana_nilai");
        handleInputChange("#radiologi_status_diskon_jasapelayanan", "#radiologi_tipe_diskon_jasapelayanan");
        handleInputChange("#radiologi_status_diskon_jasapelayanan", "#radiologi_diskon_jasapelayanan_nilai");
        handleInputChange("#radiologi_status_diskon_bhp", "#radiologi_tipe_diskon_bhp");
        handleInputChange("#radiologi_status_diskon_bhp", "#radiologi_diskon_bhp_nilai");
        handleInputChange("#radiologi_status_diskon_biayaperawatan", "#radiologi_tipe_diskon_biayaperawatan");
        handleInputChange("#radiologi_status_diskon_biayaperawatan", "#radiologi_diskon_biayaperawatan_nilai");
        
        handleInputChange("#laboratorium_status_tujuan_poliklinik", "#laboratorium_tujuan_poliklinik");
        handleInputChange("#laboratorium_status_diskon_all", "#laboratorium_tipe_diskon_all");
        handleInputChange("#laboratorium_status_diskon_all", "#laboratorium_diskon_all_nilai");
        handleInputChange("#laboratorium_status_diskon_jasasarana", "#laboratorium_tipe_diskon_jasasarana");
        handleInputChange("#laboratorium_status_diskon_jasasarana", "#laboratorium_diskon_jasasarana_nilai");
        handleInputChange("#laboratorium_status_diskon_jasapelayanan", "#laboratorium_tipe_diskon_jasapelayanan");
        handleInputChange("#laboratorium_status_diskon_jasapelayanan", "#laboratorium_diskon_jasapelayanan_nilai");
        handleInputChange("#laboratorium_status_diskon_bhp", "#laboratorium_tipe_diskon_bhp");
        handleInputChange("#laboratorium_status_diskon_bhp", "#laboratorium_diskon_bhp_nilai");
        handleInputChange("#laboratorium_status_diskon_biayaperawatan", "#laboratorium_tipe_diskon_biayaperawatan");
        handleInputChange("#laboratorium_status_diskon_biayaperawatan", "#laboratorium_diskon_biayaperawatan_nilai");
        
        handleInputChange("#fisioterapi_status_tujuan_poliklinik", "#fisioterapi_tujuan_poliklinik");
        handleInputChange("#fisioterapi_status_diskon_all", "#fisioterapi_tipe_diskon_all");
        handleInputChange("#fisioterapi_status_diskon_all", "#fisioterapi_diskon_all_nilai");
        handleInputChange("#fisioterapi_status_diskon_jasasarana", "#fisioterapi_tipe_diskon_jasasarana");
        handleInputChange("#fisioterapi_status_diskon_jasasarana", "#fisioterapi_diskon_jasasarana_nilai");
        handleInputChange("#fisioterapi_status_diskon_jasapelayanan", "#fisioterapi_tipe_diskon_jasapelayanan");
        handleInputChange("#fisioterapi_status_diskon_jasapelayanan", "#fisioterapi_diskon_jasapelayanan_nilai");
        handleInputChange("#fisioterapi_status_diskon_bhp", "#fisioterapi_tipe_diskon_bhp");
        handleInputChange("#fisioterapi_status_diskon_bhp", "#fisioterapi_diskon_bhp_nilai");
        handleInputChange("#fisioterapi_status_diskon_biayaperawatan", "#fisioterapi_tipe_diskon_biayaperawatan");
        handleInputChange("#fisioterapi_status_diskon_biayaperawatan", "#fisioterapi_diskon_biayaperawatan_nilai");

        $(".delete-link").click(function (e) {
            e.preventDefault();
            
            var url = $(this).data("url");

            swal({
                title: 'Konfirmasi',
                text: 'Apakah Anda yakin ingin menghapus setting ini?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Hapus!',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result) {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        success: function(response) {
                            swal({
                                title: 'Berhasil',
                                text: 'Setting berhasil dihapus.',
                                icon: 'success',
                                timer: 1500, // Waktu dalam milidetik (ms)
                                showConfirmButton: false
                            });

                            setTimeout(function () {  
                                $(e.target).closest('tr').remove();
                            }, 1500);
                        },
                        error: function(xhr, status, error) {
                            swal({
                                title: 'Gagal',
                                text: 'Gagal menghapus setting. Silakan coba lagi.',
                                icon: 'error'
                            });
                        }
                    });
                }
            });
        });
		
		
		$("#idbarang_bmhp").select2({
			minimumInputLength: 2,
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}Mpengaturan_diskon_kelompok_pasien_laboratorium/get_obat/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
				data: function(params) {
					var query = {
						search: params.term,
						idtipe : $("#idtipe_bmhp").val(),
					}
					return query;
				},
				processResults: function(data) {
					return {
						results: $.map(data, function(item) {
							return {
								text: item.nama,
								id: item.id,
								idtipe: item.idtipe
							}
						})
					};
				}
			}
		});
		$("#idbarang_farmasi").select2({
			minimumInputLength: 2,
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}Mpengaturan_diskon_kelompok_pasien_laboratorium/get_obat/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
				data: function(params) {
					var query = {
						search: params.term,
						idtipe : $("#idtipe_farmasi").val(),
					}
					return query;
				},
				processResults: function(data) {
					return {
						results: $.map(data, function(item) {
							return {
								text: item.nama,
								id: item.id,
								idtipe: item.idtipe
							}
						})
					};
				}
			}
		});
    });

    function handleInputChange(statusId, targetId) {
        $(statusId).change(function() {
            var value = $(this).val();
            if (value === "1") {
                $(targetId).prop('disabled', false);
            } else {
                $(targetId).prop('disabled', true);
            }
        });
    }

    function populateTarifAdministrasi(selectedJenisTarif) {
        $.ajax({
            type: "GET",
            url: "{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/get_tarif_adm",
            data: {
                idtipe: '1',
                idjenis: selectedJenisTarif,
                status: '1'
            },
            dataType: "json",
            success: function (data) {
                // Clear existing options
                $("#adm_tarif_pelayanan").empty();

                // Add new options
                $("#adm_tarif_pelayanan").append('<option value="0">Semua</option>');
                $.each(data, function (index, option) {
                    $("#adm_tarif_pelayanan").append('<option value="' + option.id + '" data-path="' + option.path + '">' + option.nama + '</option>');
                });
            },
            error: function (xhr, status, error) {
                console.error("Error fetching data:", error);
                // Handle error if needed
            }
        });
    }

    function populateTarifPelayanan(selectedHeadParentTarif) {
        $.ajax({
            type: "GET",
            url: "{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/get_tarif_pelayanan",
            data: {
                path: selectedHeadParentTarif,
                status: '1'
            },
            dataType: "json",
            success: function (data) {
                // Clear existing options
                $("#pelayanan_tarif_pelayanan").empty();

                // Add new options
                $("#pelayanan_tarif_pelayanan").append('<option value="0">Semua</option>');
                $.each(data, function (index, option) {
                    $("#pelayanan_tarif_pelayanan").append('<option value="' + option.id + '" data-path="' + option.path + '">' + option.nama + '</option>');
                });
            },
            error: function (xhr, status, error) {
                console.error("Error fetching data:", error);
                // Handle error if needed
            }
        });
    }

    function populateHeadTarifRadiologi(selectedTipeTarif) {
        $.ajax({
            type: "GET",
            url: "{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/get_tarif_radiologi",
            data: {
                tipe_tarif: selectedTipeTarif,
                head_parent_tarif: 0,
                status: 'head'
            },
            dataType: "json",
            success: function (data) {
                // Clear existing options
                $("#radiologi_head_parent_tarif").empty();

                // Add new options
                $("#radiologi_head_parent_tarif").append('<option value="0">Semua</option>');
                $.each(data, function (index, option) {
                    $("#radiologi_head_parent_tarif").append('<option value="' + option.id + '" data-path="' + option.path + '">' + option.nama + '</option>');
                });
            },
            error: function (xhr, status, error) {
                console.error("Error fetching data:", error);
                // Handle error if needed
            }
        });
    }

    function populateTarifRadiologi(selectedTipeTarif, selectedHeadParentTarif) {
        $.ajax({
            type: "GET",
            url: "{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/get_tarif_radiologi",
            data: {
                tipe_tarif: selectedTipeTarif,
                head_parent_tarif: selectedHeadParentTarif,
                status: 'child'
            },
            dataType: "json",
            success: function (data) {
                // Clear existing options
                $("#radiologi_tarif_pelayanan").empty();

                // Add new options
                $("#radiologi_tarif_pelayanan").append('<option value="0">Semua</option>');
                $.each(data, function (index, option) {
                    $("#radiologi_tarif_pelayanan").append('<option value="' + option.id + '" data-path="' + option.path + '">' + option.nama + '</option>');
                });
            },
            error: function (xhr, status, error) {
                console.error("Error fetching data:", error);
                // Handle error if needed
            }
        });
    }

    function populateHeadTarifLaboratorium(selectedTipeTarif) {
        $.ajax({
            type: "GET",
            url: "{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/get_tarif_laboratorium",
            data: {
                tipe_tarif: selectedTipeTarif,
                head_parent_tarif: 0,
                status: 'head'
            },
            dataType: "json",
            success: function (data) {
                // Clear existing options
                $("#laboratorium_head_parent_tarif").empty();

                // Add new options
                $("#laboratorium_head_parent_tarif").append('<option value="0">Semua</option>');
                $.each(data, function (index, option) {
                    $("#laboratorium_head_parent_tarif").append('<option value="' + option.id + '" data-path="' + option.path + '">' + option.nama + '</option>');
                });
            },
            error: function (xhr, status, error) {
                console.error("Error fetching data:", error);
                // Handle error if needed
            }
        });
    }

    function populateTarifLaboratorium(selectedTipeTarif, selectedHeadParentTarif) {
        $.ajax({
            type: "GET",
            url: "{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/get_tarif_laboratorium",
            data: {
                tipe_tarif: selectedTipeTarif,
                head_parent_tarif: selectedHeadParentTarif,
                status: 'child'
            },
            dataType: "json",
            success: function (data) {
                // Clear existing options
                $("#laboratorium_tarif_pelayanan").empty();

                // Add new options
                $("#laboratorium_tarif_pelayanan").append('<option value="0">Semua</option>');
                $.each(data, function (index, option) {
                    $("#laboratorium_tarif_pelayanan").append('<option value="' + option.id + '" data-path="' + option.path + '">' + option.nama + '</option>');
                });
            },
            error: function (xhr, status, error) {
                console.error("Error fetching data:", error);
                // Handle error if needed
            }
        });
    }

    function populateHeadTarifFisioterapi(selectedTipeTarif) {
        $.ajax({
            type: "GET",
            url: "{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/get_tarif_fisioterapi",
            data: {
                tipe_tarif: selectedTipeTarif,
                head_parent_tarif: 0,
                status: 'head'
            },
            dataType: "json",
            success: function (data) {
                // Clear existing options
                $("#fisioterapi_head_parent_tarif").empty();

                // Add new options
                $("#fisioterapi_head_parent_tarif").append('<option value="0">Semua</option>');
                $.each(data, function (index, option) {
                    $("#fisioterapi_head_parent_tarif").append('<option value="' + option.id + '" data-path="' + option.path + '">' + option.nama + '</option>');
                });
            },
            error: function (xhr, status, error) {
                console.error("Error fetching data:", error);
                // Handle error if needed
            }
        });
    }

    function populateTarifFisioterapi(selectedHeadParentTarif) {
        $.ajax({
            type: "GET",
            url: "{base_url}mpengaturan_diskon_kelompok_pasien_laboratorium/get_tarif_fisioterapi",
            data: {
                head_parent_tarif: selectedHeadParentTarif,
                status: 'child'
            },
            dataType: "json",
            success: function (data) {
                // Clear existing options
                $("#fisioterapi_tarif_pelayanan").empty();

                // Add new options
                $("#fisioterapi_tarif_pelayanan").append('<option value="0">Semua</option>');
                $.each(data, function (index, option) {
                    $("#fisioterapi_tarif_pelayanan").append('<option value="' + option.id + '" data-path="' + option.path + '">' + option.nama + '</option>');
                });
            },
            error: function (xhr, status, error) {
                console.error("Error fetching data:", error);
                // Handle error if needed
            }
        });
    }
	$("#idtipe_poli_bmhp").change(function(){
			$.ajax({
				url: '{site_url}setting_assesmen/find_poli/'+$(this).val(),
				dataType: "json",
				success: function(data) {
					// alert(data);
					$("#idpoli_bmhp").empty();
					$("#idpoli_bmhp").append(data);
				}
			});

	});
	$("#idtipe_bmhp").change(function(){
		$("#idbarang_bmhp").val(null).trigger('change');
	});
	$("#operand_bmhp").change(function(){
		// alert($(this).val());
		if ($(this).val()=='0'){
			$("#diskon_bmhp").prop('disabled',true);			
			$("#diskon_bmhp").val(0);			
		}else{
			$("#diskon_bmhp").removeAttr('disabled');	
		}
	});
	function simpan_bmhp(){
		let idtipe_poli=$("#idtipe_poli_bmhp").val();
		let idpoli=$("#idpoli_bmhp").val();
		let statuspasienbaru=$("#statuspasienbaru_bmhp").val();
		let pertemuan_id=$("#pertemuan_id_bmhp").val();
		let idtipe=$("#idtipe_bmhp").val();
		let idbarang=$("#idbarang_bmhp").val();
		let operand=$("#operand_bmhp").val();
		// alert(operand);
		let diskon=$("#diskon_bmhp").val();
		
		if (idtipe_poli=='0'){
			sweetAlert("Maaf...", "Tentukan Tujuan", "error");
			return false;
		}
		
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}mpengaturan_diskon_kelompok_pasien_laboratorium/simpan_bmhp', 
			dataType: "JSON",
			method: "POST",
			data : {
					// tipe:tipe,
					idkelompokpasien:'{id}',
					idrekanan:0,
					idtipe_poli:idtipe_poli,
					idpoli:idpoli,
					statuspasienbaru:statuspasienbaru,
					pertemuan_id:pertemuan_id,
					idtipe:idtipe,
					idbarang:idbarang,
					operand:operand,
					diskon:diskon,

				
				},
			success: function(data) {
				// alert(data);
					$("#cover-spin").hide();
				if (data==true){
					$("#tipe_bmhp").val('#').trigger('change');
					$("#idpoli_bmhp").val('#').trigger('change');
					$("#statuspasienbaru_bmhp").val('#').trigger('change');
					$("#pertemuan_id_bmhp").val('#').trigger('change');
					$("#idtipe_bmhp").val(null).trigger('change');
					$("#operand").val('0').trigger('change');
					$("#diskon_bmhp").val('0').trigger('change');
					$('#index_tampil_bmhp').DataTable().ajax.reload( null, false ); 
					$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});

				}else{
					swal({
						title: "Data Error!",
						text: "Dupicate Data.",
						type: "error",
						timer: 500,
						showConfirmButton: false
					});
					
				}
			}
		});
	}
	function load_bmhp(){
		// alert('sini');
		$('#index_tampil_bmhp').DataTable().destroy();	
		$('#index_tampil_bmhp').DataTable({
				autoWidth: false,
				searching: true,
				pageLength: 100,
				serverSide: true,
				"processing": true,
				"order": [],
				"ordering": false,
				"columnDefs": [
						// { "width": "5%", "targets": 0,  className: "text-right" },
						// { "width": "10%", "targets": [3,2,4,5,6] },
						// { "width": "15%", "targets": [1]},
						// { "width": "8%", "targets": [8]},
						// { "width": "30%", "targets": 3,  className: "text-left" },
					],
				ajax: { 
					url: '{site_url}mpengaturan_diskon_kelompok_pasien_laboratorium/load_bmhp', 
					type: "POST" ,
					dataType: 'json',
					data : {
						idkelompokpasien:'{id}',
						   }
				}
			});
	}
	function hapus_bmhp(id){
		swal({
			title: "Anda Yakin ?",
			text : "Akan Hapus Logic?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			
			 $.ajax({
					url: '{site_url}mpengaturan_diskon_kelompok_pasien_laboratorium/hapus_bmhp',
					type: 'POST',
					data: {id: id},
					complete: function() {
						$('#index_tampil_bmhp').DataTable().ajax.reload( null, false ); 
						$("#cover-spin").hide();
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						
					}
				});
		});
	}
	
	
	//FARMASI
	$("#idtipe_poli_farmasi").change(function(){
			$.ajax({
				url: '{site_url}setting_assesmen/find_poli/'+$(this).val(),
				dataType: "json",
				success: function(data) {
					// alert(data);
					$("#idpoli_farmasi").empty();
					$("#idpoli_farmasi").append(data);
				}
			});

	});
	$("#idtipe_farmasi").change(function(){
		$("#idbarang_farmasi").val(null).trigger('change');
	});
	$("#operand_farmasi").change(function(){
		// alert($(this).val());
		if ($(this).val()=='0'){
			$("#diskon_farmasi").prop('disabled',true);			
			$("#diskon_farmasi").val(0);			
		}else{
			$("#diskon_farmasi").removeAttr('disabled');	
		}
	});
	function simpan_farmasi(){
		let idtipe_poli=$("#idtipe_poli_farmasi").val();
		let idpoli=$("#idpoli_farmasi").val();
		let statuspasienbaru=$("#statuspasienbaru_farmasi").val();
		let pertemuan_id=$("#pertemuan_id_farmasi").val();
		let idtipe=$("#idtipe_farmasi").val();
		let idbarang=$("#idbarang_farmasi").val();
		let operand=$("#operand_farmasi").val();
		// alert(operand);
		let diskon=$("#diskon_farmasi").val();
		
		if (idtipe_poli=='0'){
			sweetAlert("Maaf...", "Tentukan Tujuan", "error");
			return false;
		}
		
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}mpengaturan_diskon_kelompok_pasien_laboratorium/simpan_farmasi', 
			dataType: "JSON",
			method: "POST",
			data : {
					// tipe:tipe,
					idkelompokpasien:'{id}',
					idrekanan:0,
					idtipe_poli:idtipe_poli,
					idpoli:idpoli,
					statuspasienbaru:statuspasienbaru,
					pertemuan_id:pertemuan_id,
					idtipe:idtipe,
					idbarang:idbarang,
					operand:operand,
					diskon:diskon,

				
				},
			success: function(data) {
				// alert(data);
					$("#cover-spin").hide();
				if (data==true){
					$("#tipe_farmasi").val('#').trigger('change');
					$("#idpoli_farmasi").val('#').trigger('change');
					$("#statuspasienbaru_farmasi").val('#').trigger('change');
					$("#pertemuan_id_farmasi").val('#').trigger('change');
					$("#idtipe_farmasi").val(null).trigger('change');
					$("#operand").val('0').trigger('change');
					$("#diskon_farmasi").val('0').trigger('change');
					$('#index_tampil_farmasi').DataTable().ajax.reload( null, false ); 
					$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});

				}else{
					swal({
						title: "Data Error!",
						text: "Dupicate Data.",
						type: "error",
						timer: 500,
						showConfirmButton: false
					});
					
				}
			}
		});
	}
	function load_farmasi(){
		// alert('sini');
		$('#index_tampil_farmasi').DataTable().destroy();	
		$('#index_tampil_farmasi').DataTable({
				autoWidth: false,
				searching: true,
				pageLength: 100,
				serverSide: true,
				"processing": true,
				"order": [],
				"ordering": false,
				"columnDefs": [
						// { "width": "5%", "targets": 0,  className: "text-right" },
						// { "width": "10%", "targets": [3,2,4,5,6] },
						// { "width": "15%", "targets": [1]},
						// { "width": "8%", "targets": [8]},
						// { "width": "30%", "targets": 3,  className: "text-left" },
					],
				ajax: { 
					url: '{site_url}mpengaturan_diskon_kelompok_pasien_laboratorium/load_farmasi', 
					type: "POST" ,
					dataType: 'json',
					data : {
						idkelompokpasien:'{id}',
						   }
				}
			});
	}
	function hapus_farmasi(id){
		swal({
			title: "Anda Yakin ?",
			text : "Akan Hapus Logic?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			
			 $.ajax({
					url: '{site_url}mpengaturan_diskon_kelompok_pasien_laboratorium/hapus_farmasi',
					type: 'POST',
					data: {id: id},
					complete: function() {
						$('#index_tampil_farmasi').DataTable().ajax.reload( null, false ); 
						$("#cover-spin").hide();
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						
					}
				});
		});
	}
</script>