<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mtemplate_tindakan_anestesi" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mtemplate_tindakan_anestesi/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group" style="margin-bottom:10px">
				<label class="col-md-2 control-label" for="nama">Nama</label>
				<div class="col-md-10">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="mtemplate_tindakan_anestesi_id" placeholder="mtemplate_tindakan_anestesi_id" name="mtemplate_tindakan_anestesi_id" value="{id}" required="" aria-required="true">
				</div>
			</div>
			
			<?if ($id){?>
			<div class="form-group">
				<div class="col-md-12">
					<h5><?=text_primary('INFORMASI TINDAKAN ')?></h5>
				</div>
			</div>
			<div class="form-group push-5-t">
				<div class="col-md-12 ">
					<div class="col-md-12 ">
						<div class="table-responsive">
					
							<table class="table table-bordered" id="tabel_informasi">
								<thead>
									<tr>
										<th width="5%" class="text-center">No</th>
										<th width="25%" class="text-center">Jenis Informasi / Type Of Information	</th>
										<th width="50%" class="text-center">Isi Informasi / Content Information</th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<?}?>
			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mtemplate_tindakan_anestesi" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){	
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
	$('.js-summernote').summernote({
		  height: 60,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
	let mtemplate_tindakan_anestesi_id=$("#mtemplate_tindakan_anestesi_id").val();
	if (mtemplate_tindakan_anestesi_id){
		load_index_informasi();
	}
	
})	
function load_index_informasi(){
	$("#cover-spin").show();
	let mtemplate_tindakan_anestesi_id=$("#mtemplate_tindakan_anestesi_id").val();
	
	$.ajax({
		url: '{site_url}mtemplate_tindakan_anestesi/load_index_informasi_tindakan_anestesi/',
		dataType: "json",
		type: 'POST',
		  data: {
				id:mtemplate_tindakan_anestesi_id,
				
		  },
		success: function(data) {
			$("#tabel_informasi tbody").empty();
			$("#tabel_informasi tbody").append(data.tabel);
			$("#cover-spin").hide();
			$('.auto_blur_tabel').summernote({
				height: 100,
			    codemirror: { // codemirror options
					theme: 'monokai'
				  },	
			  callbacks: {
				onBlur: function(contents, $editable) {
						assesmen_id=$("#assesmen_id").val();
						var tr=$(this).closest('tr');
						var informasi_id=tr.find(".informasi_id").val();
						var isi=$(this).val();
						$.ajax({
							url: '{site_url}mtemplate_tindakan_anestesi/update_isi_informasi_tindakan_anestesi/',
							dataType: "json",
							type: 'POST',
							data: {
							informasi_id:informasi_id,isi:isi,assesmen_id:assesmen_id
							},success: function(data) {

							$.toaster({priority : 'success', title : 'Succes!', message : ' Auto Save '});
							}
						});
				}
			  }
			});
				
		}
	});
}
function clear_parameter(){
	$("#parameter_id").val('');
	$("#inisial").val('');
	
	$('#parameter_nama').summernote('code','');
	
	$("#btn_tambah_parameter").html('<i class="fa fa-save"></i> Save');
	
}
function load_parameter(){
	let mtemplate_tindakan_anestesi_id=$("#mtemplate_tindakan_anestesi_id").val();
	$('#index_parameter').DataTable().destroy();	
	table = $('#index_parameter').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mtemplate_tindakan_anestesi/load_parameter', 
                type: "POST" ,
                dataType: 'json',
				data : {
							mtemplate_tindakan_anestesi_id:mtemplate_tindakan_anestesi_id
					   }
            }
        });
}		
function edit_parameter(parameter_id){
	$("#parameter_id").val(parameter_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mtemplate_tindakan_anestesi/find_parameter',
		type: 'POST',
		dataType: "JSON",
		data: {id: parameter_id},
		success: function(data) {
			$("#btn_tambah_parameter").html('<i class="fa fa-save"></i> Edit');
			$("#parameter_id").val(data.id);
			$("#inisial").val(data.inisial);
			$("#st_nilai").val(data.st_nilai).trigger('change');
			$('#parameter_nama').summernote('code',data.parameter_nama);
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_parameter(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Parameter?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mtemplate_tindakan_anestesi/hapus_parameter',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_parameter').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

$("#btn_tambah_parameter").click(function() {
	let mtemplate_tindakan_anestesi_id=$("#mtemplate_tindakan_anestesi_id").val();
	let parameter_id=$("#parameter_id").val();
	let parameter_nama=$("#parameter_nama").val();
	let inisial=$("#inisial").val();
	let st_nilai=$("#st_nilai").val();
	
	if (parameter_nama==''){
		sweetAlert("Maaf...", "Tentukan Deskripsi","error");
		return false;
	}
	if (inisial==''){
		sweetAlert("Maaf...", "Tentukan Inisial", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mtemplate_tindakan_anestesi/simpan_parameter', 
		dataType: "JSON",
		method: "POST",
		data : {
				mtemplate_tindakan_anestesi_id:mtemplate_tindakan_anestesi_id,
				parameter_id:parameter_id,
				parameter_nama:parameter_nama,
				inisial:inisial,
				st_nilai:st_nilai,
				
			},
		complete: function(data) {
			clear_parameter();
			$('#index_parameter').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
$("#btn_tambah_jawaban").click(function() {
	let parameter_id=$("#parameter_id2").val();
	let skor=$("#skor").val();
	let skor_id=$("#skor_id").val();
	let deskripsi_nama=$("#deskripsi_nama").val();
	let st_default=$("#st_default").val();
	
	if (skor==''){
		sweetAlert("Maaf...", "Tentukan Skor", "error");
		return false;
	}
	if (deskripsi_nama==''){
		sweetAlert("Maaf...", "Tentukan Deskripsi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mtemplate_tindakan_anestesi/simpan_jawaban', 
		dataType: "JSON",
		method: "POST",
		data : {
				parameter_id:parameter_id,
				skor_id:skor_id,
				deskripsi_nama:deskripsi_nama,
				skor:skor,
				st_default:st_default,
				
			},
		complete: function(data) {
			clear_jawaban();
			load_parameter(); 
			$('#index_jawaban').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
function add_jawaban(parameter_id){
	$("#parameter_id2").val(parameter_id);
	$("#modal_tambah").modal('show');
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mtemplate_tindakan_anestesi/find_parameter',
		type: 'POST',
		dataType: "JSON",
		data: {id: parameter_id},
		success: function(data) {
			// $("#btn_tambah_parameter").html('<i class="fa fa-save"></i> Edit');
			// $("#parameter_id").val(data.id);
			$("#parameter_add").html(data.parameter_nama);
			// $('#parameter_add').summernote('code',data.parameter_nama);
			$("#cover-spin").hide();
			load_jawaban();
		}
	});
}
function clear_jawaban(){
	$("#skor_id").val('');
	$('#deskripsi_nama').val('');
	$('#skor').val('');
	
	$("#btn_tambah_jawaban").html('<i class="fa fa-save"></i> Save');
	
}
function load_jawaban(){
	let parameter_id=$("#parameter_id2").val();
	$('#index_jawaban').DataTable().destroy();	
	table = $('#index_jawaban').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mtemplate_tindakan_anestesi/load_jawaban', 
                type: "POST" ,
                dataType: 'json',
				data : {
							parameter_id:parameter_id
					   }
            }
        });
}	
function edit_jawaban(skor_id){
	$("#skor_id").val(skor_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mtemplate_tindakan_anestesi/find_jawaban',
		type: 'POST',
		dataType: "JSON",
		data: {id: skor_id},
		success: function(data) {
			$("#modal_tambah").modal('show');
			$("#btn_tambah_jawaban").html('<i class="fa fa-save"></i> Edit');
			$("#skor").val(data.skor);
			$("#skor_id").val(data.id);
			$("#deskripsi_nama").val(data.deskripsi_nama);
			$("#st_default").val(data.st_default).trigger('change');
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_jawaban(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Skor?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mtemplate_tindakan_anestesi/hapus_jawaban',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_jawaban').DataTable().ajax.reload( null, false ); 
					load_parameter();
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
</script>