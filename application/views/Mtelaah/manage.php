<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mtelaah" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?php echo form_open('mtelaah/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group" style="margin-bottom:10px">
				<label class="col-md-2 control-label" for="nama">No Urut</label>
				<div class="col-md-10">
					<input type="text" class="form-control number" id="nourut" placeholder="No Urut" name="nourut" value="{nourut}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="mtelaah_id" placeholder="mtelaah_id" name="mtelaah_id" value="{id}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group" style="margin-bottom:10px">
				<label class="col-md-2 control-label" for="nama">Nama</label>
				<div class="col-md-10">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			
			<?if ($id){?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="">Telaah</label>
				<div class="col-md-10">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="index_telaah" style=" vertical-align: baseline;">
							<thead>
								<tr>
									<th width="10%" class="text-center">No</th>
									<th width="30%" class="text-center">Aspek Telaah</th>
									<th width="20%" class="text-center">Nilai</th>										   
									<th width="10%" class="text-center">Default</th>										   
									<th width="10%" class="text-center">Resep</th>										   
									<th width="10%" class="text-center">Obat</th>										   
									<th width="10%" class="text-center">Action</th>										   
								</tr>
								<tr>
									<th  style=" vertical-align: top;"  class="text-center">
										<select tabindex="8" id="nourut_detail" class="js-select2 form-control opsi_change_racikan" style="width: 100%;" data-placeholder="No" >
											<option value="">Pilih No Urut</option>
											<?for($i=1;$i<=100;$i++){?>
											<option value="<?=$i?>"><?=$i?></option>
											<?}?>
											
										</select>
									</th>
									<th  style=" vertical-align: top;">
										<input type="text" class="form-control" id="nama_detail" style="width:100%" placeholder="Nama" name="nama_detail" value="" >
									</th>
									<th  style=" vertical-align: top;">
										<select tabindex="8" id="opsi_jawab" class="js-select2 form-control opsi_change_racikan" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
											<option value="">Pilih Opsi</option>
											<?foreach(list_variable_ref(109) as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>
											
										</select>
									</th>
									<th  style=" vertical-align: top;">
										<select tabindex="8" id="opsi_default" class="js-select2 form-control opsi_change_racikan" style="width: 100%;" data-placeholder="Pilih Opsi" >
											<option value="">Pilih Opsi</option>
											<?foreach(list_variable_ref(109) as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>
											
										</select>
									</th>
									<th  style=" vertical-align: top;"  class="text-center">
										<label class="css-input css-checkbox css-checkbox-primary">
											<input id="chk_resep" type="checkbox"><span></span> 
										</label>
											<input id="st_resep" type="hidden" value='0'><span></span> 
									</th>
									<th  style=" vertical-align: top;"  class="text-center">
										<label class="css-input css-checkbox css-checkbox-primary">
											<input id="chk_obat" type="checkbox"><span></span> 
										</label>
											<input id="st_obat" type="hidden" value='0'><span></span> 
									</th>
									<th style=" vertical-align: top;"  class="text-center">
										<div class="btn-group">
										<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_telaah" name="btn_tambah_telaah"><i class="fa fa-save"></i></button>
										<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_telaah()"><i class="fa fa-refresh"></i></button>
										</div>
									</th>										   
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>					
				</div>
			</div>
			<?}?>
			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mtelaah" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){	
	$(".number").number(true,0,'.',',');
	$('.js-summernote').summernote({
		  height: 60,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
	let mtelaah_id=$("#mtelaah_id").val();
	if (mtelaah_id){
		load_detail();
	}
	
})	
$('#index_telaah tbody').on('click', '.chk_resep', function() {
	let id_det;
	check = $(this).is(":checked");
	if(check) {
		$(this).closest('tr').find(".st_resep").val(1);
	} else {
		$(this).closest('tr').find(".st_resep").val(0);
	}
	update_record_detail($(this));
	
});
$('#index_telaah tbody').on('click', '.chk_obat', function() {
	let id_det;
	 check = $(this).is(":checked");
	if(check) {
		$(this).closest('tr').find(".st_obat").val(1);
	} else {
		$(this).closest('tr').find(".st_obat").val(0);
	}
	update_record_detail($(this));
	
});
$(".chk_resep").on("click", function(){
	let id_det;
	 check = $(this).is(":checked");
		if(check) {
			$(this).closest('tr').find(".st_resep").val();
		} else {
			$(this).closest('tr').find(".st_resep").val();
		}
}); 
$(".chk_obat").on("click", function(){
	let id_det;
	 check = $(this).is(":checked");
		if(check) {
			$(this).closest('tr').find(".st_obat").val();
		} else {
			$(this).closest('tr').find(".st_obat").val();
		}
}); 
$('#index_telaah tbody').on('change', '.opsi_change', function() {
	update_record_detail($(this));
	
});
$('#index_telaah tbody').on('blur', '.nama_detail', function() {
	update_record_detail($(this));
	
});
$('#index_telaah tbody').on('blur', '.nama_detail', function() {
	update_record_detail($(this));
	
});
function update_record_detail(tabel){
	let telaah_id=tabel.closest('tr').find(".id").val();
	let nourut=tabel.closest('tr').find(".nourut").val();
	let nama_detail=tabel.closest('tr').find(".nama_detail").val();
	let opsi_jawab=tabel.closest('tr').find(".opsi_jawab").val();
	let st_obat=tabel.closest('tr').find(".st_obat").val();
	let st_resep=tabel.closest('tr').find(".st_resep").val();
	let opsi_default=tabel.closest('tr').find(".opsi_default").val();
	let mtelaah_id=$("#mtelaah_id").val();
	console.log(telaah_id);
	console.log(nourut);
	console.log(nama_detail);
	console.log(opsi_jawab);
	console.log(st_resep);
	console.log(st_obat);
	console.log(mtelaah_id);
	
	$.ajax({
		url: '{site_url}mtelaah/simpan_telaah', 
		dataType: "JSON",
		method: "POST",
		data : {
				mtelaah_id:mtelaah_id,
				telaah_id:telaah_id,
				nourut:nourut,
				nama_detail:nama_detail,
				st_obat:st_obat,
				st_resep:st_resep,
				opsi_jawab:opsi_jawab,
				opsi_default:opsi_default,
				
			},
		complete: function(data) {
			// clear_telaah();
			// $('#index_telaah').DataTable().ajax.reload( null, false ); 
			// $("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});
}
$("#btn_tambah_telaah").click(function() {
	let mtelaah_id=$("#mtelaah_id").val();
	let telaah_id=$("#telaah_id").val();
	let nourut_detail=$("#nourut_detail").val();
	let nama_detail=$("#nama_detail").val();
	let st_obat=$("#st_obat").val();
	let st_resep=$("#st_resep").val();
	let opsi_jawab=$("#opsi_jawab").val();
	let opsi_default=$("#opsi_default").val();
	if (nama_detail==''){
		sweetAlert("Maaf...", "Tentukan Aspek telaah", "error");
		return false;
	}
	if (opsi_jawab==null){
		sweetAlert("Maaf...", "Tentukan Nilai", "error");
		return false;
	}
	if (nourut_detail==null || nourut_detail==''){
		sweetAlert("Maaf...", "Tentukan No Urut", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mtelaah/simpan_telaah', 
		dataType: "JSON",
		method: "POST",
		data : {
				mtelaah_id:mtelaah_id,
				telaah_id:telaah_id,
				nama_detail:nama_detail,
				st_obat:st_obat,
				st_resep:st_resep,
				opsi_jawab:opsi_jawab,
				nourut:nourut_detail,
				opsi_default:opsi_default,
				
			},
		complete: function(data) {
			clear_telaah();
			$('#index_telaah').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
function load_detail(){
	// alert('sini');
	let mtelaah_id=$("#mtelaah_id").val();
	$('#index_telaah').DataTable().destroy();	
	table = $('#index_telaah').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			"columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					{ "width": "10%", "targets": [3,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}mtelaah/load_detail', 
                type: "POST" ,
                dataType: 'json',
				data : {
							mtelaah_id:mtelaah_id
					   }
            },
			"drawCallback": function( settings ) {
				 $("#cover-spin").hide();
				 $(".opsi_change").select2();
				 $(".number").number(true,0,'.',',');
			 }  
        });
}	
function load_telaah(){
	let mtelaah_id=$("#mtelaah_id").val();
	$('#index_telaah').DataTable().destroy();	
	table = $('#index_telaah').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mtelaah/load_telaah', 
                type: "POST" ,
                dataType: 'json',
				data : {
							mtelaah_id:mtelaah_id
					   }
            }
        });
}	
$("#chk_resep").on("click", function(){
	let id_det;
	 check = $(this).is(":checked");
		if(check) {
			$("#st_resep").val(1);
		} else {
			$("#st_resep").val(0);
		}
}); 
$("#chk_obat").on("click", function(){
	let id_det;
	 check = $(this).is(":checked");
		if(check) {
			$("#st_obat").val(1);
		} else {
			$("#st_obat").val(0);
		}
}); 

function clear_telaah(){
	$("#telaah_id").val('');
	$('#nama_detail').val('');
	$('#nourut_detail').val(parseFloat($('#nourut_detail').val())+1).trigger('change');
	
	$("#btn_tambah_telaah").html('<i class="fa fa-save"></i> Save');
	
}
	
function edit_telaah(telaah_id){
	$("#telaah_id").val(telaah_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mtelaah/find_telaah',
		type: 'POST',
		dataType: "JSON",
		data: {id: telaah_id},
		success: function(data) {
			$("#btn_tambah_telaah").html('<i class="fa fa-save"></i> Edit');
			$("#telaah_id").val(data.id);
			$('#nama_detail').summernote('code',data.nama_detail);
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_telaah(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Aspek Telaah?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mtelaah/hapus_telaah',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_telaah').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

$("#btn_tambah_jawaban").click(function() {
	let telaah_id=$("#telaah_id2").val();
	let skor=$("#skor").val();
	let skor_id=$("#skor_id").val();
	let deskripsi_nama=$("#deskripsi_nama").val();
	
	if (skor==''){
		sweetAlert("Maaf...", "Tentukan Skor", "error");
		return false;
	}
	if (deskripsi_nama==''){
		sweetAlert("Maaf...", "Tentukan Deskripsi", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mtelaah/simpan_jawaban', 
		dataType: "JSON",
		method: "POST",
		data : {
				telaah_id:telaah_id,
				skor_id:skor_id,
				deskripsi_nama:deskripsi_nama,
				skor:skor,
				
			},
		complete: function(data) {
			clear_jawaban();
			load_telaah(); 
			$('#index_jawaban').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
function add_jawaban(telaah_id){
	$("#telaah_id2").val(telaah_id);
	$("#modal_tambah").modal('show');
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mtelaah/find_telaah',
		type: 'POST',
		dataType: "JSON",
		data: {id: telaah_id},
		success: function(data) {
			// $("#btn_tambah_telaah").html('<i class="fa fa-save"></i> Edit');
			// $("#telaah_id").val(data.id);
			$("#telaah_add").html(data.nama_detail);
			// $('#telaah_add').summernote('code',data.nama_detail);
			$("#cover-spin").hide();
			load_jawaban();
		}
	});
}
function clear_jawaban(){
	$("#skor_id").val('');
	$('#deskripsi_nama').val('');
	$('#skor').val('');
	
	$("#btn_tambah_jawaban").html('<i class="fa fa-save"></i> Save');
	
}

function edit_jawaban(skor_id){
	$("#skor_id").val(skor_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mtelaah/find_jawaban',
		type: 'POST',
		dataType: "JSON",
		data: {id: skor_id},
		success: function(data) {
			$("#modal_tambah").modal('show');
			$("#btn_tambah_jawaban").html('<i class="fa fa-save"></i> Edit');
			$("#skor").val(data.skor);
			$("#skor_id").val(data.id);
			$("#deskripsi_nama").val(data.deskripsi_nama);
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_jawaban(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Skor?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mtelaah/hapus_jawaban',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_jawaban').DataTable().ajax.reload( null, false ); 
					load_telaah();
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
</script>