<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mrisiko_jatuh" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mrisiko_jatuh/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group" style="margin-bottom:10px">
				<label class="col-md-2 control-label" for="nama">Nama</label>
				<div class="col-md-10">
					<input type="text" class="form-control" disabled id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="mrisiko_id" placeholder="mrisiko_id" name="mrisiko_id" value="{id}" required="" aria-required="true">
				</div>
			</div>
			
			<?if ($id){?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="">&nbsp;</label>
				<div class="col-md-10">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="index_nilai" style=" vertical-align: baseline;">
							<thead>
								<tr>
									<th width="5%">No</th>
									<th width="20%">Range Nilai</th>
									<th width="20%">Referensi Nilai</th>
									<th width="10%">Perlu Tindakan</th>
									<th width="30%">Nama Tindakan</th>
									<th width="15%">Action</th>										   
								</tr>
								<tr>
									<th  style=" vertical-align: top;">#<input class="form-control" type="hidden" id="parameter_id" value=""></th>
									<th  style=" vertical-align: top;">
										<div class="input-group" style="width:100%">
											<input class="form-control decimal" type="text" id="skor_1" name="skor_1" placeholder="Skor Terendah" value="" >
											<span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
											<input class="form-control decimal" type="text" id="skor_2" name="skor_2" placeholder="Skor Tertinggi" value="" >
											
											
										</div>
									</th>
									<th  style=" vertical-align: top;">
										<input class="form-control" type="hidden" style="width:100%" id="ref_nilai" name="ref_nilai" placeholder="Referensi Nilai" value="" >
										<select tabindex="8" id="ref_nilai_id" name="ref_nilai_id" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Referensi Nilai" required>
											<option value="" selected></option>
											<?foreach(get_all('merm_referensi',array('ref_head_id'=>37,'status'=>1)) as $r){?>
											<option value="<?=$r->nilai?>"><?=$r->ref?></option>
											<?}?>
											
										</select>
									</th>
									<th  style=" vertical-align: top;">
										<select tabindex="8" id="st_tindakan" name="st_tindakan" class="js-select2 form-control auto_blur" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="1" >YA</option>
											<option value="0" >TIDAK</option>
											
										</select>
									</th>
									<th  style=" vertical-align: top;">
										<input class="form-control" type="text" style="width:100%" id="nama_tindakan" name="nama_tindakan" placeholder="Nama Tindakan" value="" >
									</th>
									
									<th style=" vertical-align: top;">
										<div class="btn-group">
										<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_nilai" name="btn_tambah_nilai"><i class="fa fa-save"></i> Simpan</button>
										<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_nilai()"><i class="fa fa-refresh"></i> Clear</button>
										</div>
									</th>										   
								</tr>
								
							</thead>
							<tbody></tbody>
						</table>
					</div>					
				</div>
			</div>
			<?}?>
			
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){	
	$(".decimal").number(true,0,'.',',');
	$('.js-summernote').summernote({
		  height: 60,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})
	let mrisiko_id=$("#mrisiko_id").val();
	if (mrisiko_id){
		load_nilai();
	}
	
})	
function clear_nilai(){
	$("#parameter_id").val('');
	$("#skor_1").val('');
	$("#skor_2").val('');
	$("#ref_nilai").val('');
	$("#st_tindakan").val('0').trigger('change');
	$("#nama_tindakan").val('');
	
	$("#btn_tambah_nilai").html('<i class="fa fa-save"></i> Save');
	
}
function load_nilai(){
	let mrisiko_id=$("#mrisiko_id").val();
	$('#index_nilai').DataTable().destroy();	
	table = $('#index_nilai').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mrisiko_jatuh/load_nilai', 
                type: "POST" ,
                dataType: 'json',
				data : {
							mrisiko_id:mrisiko_id
					   }
            }
        });
}		
function edit_nilai(parameter_id){
	$("#parameter_id").val(parameter_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mrisiko_jatuh/find_nilai',
		type: 'POST',
		dataType: "JSON",
		data: {id: parameter_id},
		success: function(data) {
			$("#btn_tambah_nilai").html('<i class="fa fa-save"></i> Edit');
			$("#parameter_id").val(data.id);
			$("#skor_1").val(data.skor_1);
			$("#skor_2").val(data.skor_2);
			$("#ref_nilai").val(data.ref_nilai);
			$("#ref_nilai_id").val(data.ref_nilai_id).trigger('change');
			$("#st_tindakan").val(data.st_tindakan).trigger('change');
			$("#nama_tindakan").val(data.nama_tindakan);
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_nilai(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Referensi?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mrisiko_jatuh/hapus_nilai',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_nilai').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

$("#btn_tambah_nilai").click(function() {
	let mrisiko_id=$("#mrisiko_id").val();
	let parameter_id=$("#parameter_id").val();
	let skor_1=$("#skor_1").val();
	let skor_2=$("#skor_2").val();
	let ref_nilai=$("#ref_nilai").val();
	let st_tindakan=$("#st_tindakan").val();
	let nama_tindakan=$("#nama_tindakan").val();
	let ref_nilai_id=$("#ref_nilai_id").val();
	
	if (ref_nilai==''){
		sweetAlert("Maaf...", "Tentukan Rafe Nilai", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mrisiko_jatuh/simpan_nilai', 
		dataType: "JSON",
		method: "POST",
		data : {
				mrisiko_id:mrisiko_id,
				parameter_id:parameter_id,
				skor_1:skor_1,
				skor_2:skor_2,
				ref_nilai:ref_nilai,
				ref_nilai_id:ref_nilai_id,
				st_tindakan:st_tindakan,
				nama_tindakan:nama_tindakan,
				
			},
		complete: function(data) {
			clear_nilai();
			$('#index_nilai').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
$("#ref_nilai_id").change(function() {
	$("#ref_nilai").val($("#ref_nilai_id option:selected").text());

});

</script>