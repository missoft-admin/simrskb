<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<style>
.nama_detail {
	border: 0;
	outline: none;
	width:100%;
}
.nama_detail:focus {

	border: 2px solid #000;
	background-color: #fdffe2;
	border: 1;
}
</style>
<?php if (UserAccesForm($user_acces_form,array('1827'))){ ?>

<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('1828'))){ ?>
		<ul class="block-options">
			<li>
				<a href="{base_url}mtelaah/create" class="btn"><i class="fa fa-plus"></i></a>
			</li>
		</ul>
		<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>Aspek Telaah</th>
					<th>Nilai</th>
					<th>Default</th>
					<th>Telaah Resep</th>
					<th>Telaah Obat</th>
					<th>Aksi</th>
					<th>level</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){	
	load_index();
})	
function load_index(){
	$('#datatable-simrs').DataTable({
		"autoWidth": false,
		"pageLength": 10,
		"ordering": true,
		"processing": true,
		"serverSide": true,
		"order": [],
		"ajax": {
			url: '{site_url}mtelaah/getIndex',
			type: "POST",
			dataType: 'json'
		},
		"columnDefs": [
			{ "width": "5%", "targets": 0, "orderable": true },
			{ "width": "30%", "targets": 1, "orderable": true },
			{ "width": "20%", "targets": 2, "orderable": true },
			{ "width": "10%", "targets": [3,4,5,6], "orderable": true },
			{ "width": "10%", "targets": [3,4,5,6],  className: "text-center" },
			 { "visible": false, "targets": 7 }
			// { "width": "15%", "targets": 3, "orderable": true },
			// { "width": "15%", "targets": 4, "orderable": true }
		],
		"drawCallback": function( settings ) {
			 $("#cover-spin").hide();
			 $(".opsi_change").select2();
		},
		"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			if (aData[6]=='0'){
				$('td', nRow).css('background-color', '#e5eeff');
			}else{
				$('td', nRow).css('background-color', '#ffffff');
			}
			
			},				
	});
}	
	// Initialize when page loads
	// jQuery(function(){ BaseTableDatatables.init();
		// $('#datatable-simrs').DataTable({
				// "autoWidth": false,
				// "pageLength": 10,
				// "ordering": true,
				// "processing": true,
				// "serverSide": true,
				// "order": [],
				// "ajax": {
					// url: '{site_url}mtelaah/getIndex',
					// type: "POST",
					// dataType: 'json'
				// },
				// "columnDefs": [
					// { "width": "10%", "targets": 0, "orderable": true },
					// { "width": "40%", "targets": 1, "orderable": true },
					// { "width": "20%", "targets": 2, "orderable": true },
					// { "width": "10%", "targets": [3,4,5], "orderable": true },
					// { "width": "10%", "targets": [0,3,4,5],  className: "text-center" },
					 // { "visible": false, "targets": 6 }
					// // { "width": "15%", "targets": 3, "orderable": true },
					// // { "width": "15%", "targets": 4, "orderable": true }
				// ],
				// "drawCallback": function( settings ) {
					 // $("#cover-spin").hide();
					 // $(".opsi_change").select2();
				// },
				// "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					// if (aData[6]=='0'){
						// $('td', nRow).css('background-color', '#e5eeff');
					// }else{
						// $('td', nRow).css('background-color', '#ffffff');
					// }
					
					// },				
				// });
	// });
	$('#datatable-simrs tbody').on('click', '.chk_resep', function() {
		let id_det;
		check = $(this).is(":checked");
		if(check) {
			$(this).closest('tr').find(".st_resep").val(1);
		} else {
			$(this).closest('tr').find(".st_resep").val(0);
		}
		update_record_detail($(this));
		
	});
	$('#datatable-simrs tbody').on('click', '.chk_obat', function() {
		let id_det;
		 check = $(this).is(":checked");
		if(check) {
			$(this).closest('tr').find(".st_obat").val(1);
		} else {
			$(this).closest('tr').find(".st_obat").val(0);
		}
		update_record_detail($(this));
		
	});
	
	$('#datatable-simrs tbody').on('change', '.opsi_change', function() {
		update_record_detail($(this));
		
	});
	$('#datatable-simrs tbody').on('blur', '.nama_detail', function() {
		update_record_detail($(this));
		
	});
	$('#datatable-simrs tbody').on('blur', '.nama_detail', function() {
		update_record_detail($(this));
		
	});
	function update_record_detail(tabel){
		let mtelaah_id=tabel.closest('tr').find(".mtelaah_id").val();
		let telaah_id=tabel.closest('tr').find(".id").val();
		let nourut=tabel.closest('tr').find(".nourut").val();
		let nama_detail=tabel.closest('tr').find(".nama_detail").val();
		let opsi_jawab=tabel.closest('tr').find(".opsi_jawab").val();
		let opsi_default=tabel.closest('tr').find(".opsi_default").val();
		let st_obat=tabel.closest('tr').find(".st_obat").val();
		let st_resep=tabel.closest('tr').find(".st_resep").val();
		console.log(st_resep);
		$.ajax({
			url: '{site_url}mtelaah/simpan_telaah', 
			dataType: "JSON",
			method: "POST",
			data : {
					mtelaah_id:mtelaah_id,
					telaah_id:telaah_id,
					nourut:nourut,
					nama_detail:nama_detail,
					st_obat:st_obat,
					st_resep:st_resep,
					opsi_jawab:opsi_jawab,
					opsi_default:opsi_default,
					
				},
			complete: function(data) {
				// clear_telaah();
				// $('#datatable-simrs').DataTable().ajax.reload( null, false ); 
				// $("#cover-spin").hide();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
			}
		});
	}
	function hapus_telaah(id){
		swal({
			title: "Anda Yakin ?",
			text : "Akan Hapus Aspek Telaah?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mtelaah/hapus_telaah',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#datatable-simrs').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
				});
		});
	
	}
	function hapus_telaah_header(id){
		swal({
			title: "Anda Yakin ?",
			text : "Akan Hapus Aspek Telaah?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mtelaah/hapus_telaah_header',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#datatable-simrs').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
				});
		});
	
	}
</script>
