<div class="modal fade in black-overlay" id="modal_add_akun" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Tambah No. Akun</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm">Nama Akun</label>
									<div class="col-md-9">
										<input type="hidden" class="form-control" id="tmp_idakun" value="">	
										<input type="hidden" class="form-control" id="id_edit_akun" value="">	
										<input type="hidden" class="form-control" id="header_id_akun" value="">	
										<select name="idakun" id="idakun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
											
										</select>							
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm">Urutan</label>
									<div class="col-md-9">
										<input type="text" class="form-control number" id="urutan_akun"  name="urutan_akun" placeholder="Urutan" value="">									
									</div>
								</div>
																						
							</div>							
						</form>
					</div>
					
				</div>
			</div>
			<div class="modal-footer">				
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Keluar</button>
				<button class="btn btn-sm btn-success" type="button" id="btn_simpan_akun">Simpan</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	$(document).on('click', '#btn_simpan_akun', function() {	
		if (validate_simpan_akun()==false){return false}
		var id_edit_akun=$("#id_edit_akun").val();
		var header_id=$("#header_id_akun").val();
		var header_id=$("#header_id_akun").val();
		var template_id=$("#template_id").val();
		var idakun=$("#idakun").val();
		var urutan=$("#urutan_akun").val();
		var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}tneraca_setting/save_akun',
			type: 'POST',
			data: {
				id_edit_akun: id_edit_akun,template_id: template_id,idakun: idakun,urutan: urutan,section_id: header_id
				},
			complete: function() {
				// $("#modal_add_akun").modal('hide');
				$.toaster({priority : 'success', title : 'Succes!', message : ' Save Data'});
				clear_akun();
				load_akun();
				table.ajax.reload( null, false );
				get_max_akun();
			}
		});
		
	});
	function load_akun(){
		var template_id=$("#template_id").val();
		var idakun=$("#tmp_idakun").val();
		// alert(idakun);
		$.ajax({
			url: '{site_url}tneraca_setting/load_akun/',
			type: 'POST',
			dataType: "json",
			data: {
				template_id: template_id,idakun: idakun
				},
			success: function(data) {
				$("#idakun").empty();
				$('#idakun').append(data.detail);
			}
		});

		
	}
	function add_akun($id){		
		clear_akun();
		$('#header_id_akun').val($id);
		load_akun();
		get_max_akun();
		$("#modal_add_akun").modal('show');
	}
	function get_max_akun(){
		$.ajax({
			url: '{site_url}tneraca_setting/max_akun',
			type: 'POST',
			dataType: 'json',
			data: {
				template_id: $("#template_id").val(),header_id:$("#header_id_akun").val()
				},
			success: function(data) {
				// alert(data.nama);
				$("#urutan_akun").val(data.urutan)
			}
		});
	}
	function clear_akun(){
		$("#tmp_idakun").val('');
		$("#id_edit_akun").val('');
		$("#nama_akun").val('');
		$("#urutan_akun").val('');
	}
	function validate_simpan_akun(){
		if ($("#idakun").val()=='#'){
			sweetAlert("Maaf...", "Tentukan No Akun", "error");
			return false;
		}
		if ($("#urutan_akun").val()==''){
			sweetAlert("Maaf...", "Tentukan Urutan Akun", "error");
			return false;
		}
		
		return true;
	}
	function edit_akun($id,$idakun,$header_id){
		$("#tmp_idakun").val($idakun);
		$("#id_edit_akun").val($id);
		$("#header_id_akun").val($header_id);
				load_akun();
		$.ajax({
			url: '{site_url}tneraca_setting/get_info_akun',
			type: 'POST',
			dataType: 'json',
			data: {
				id: $id
				},
			success: function(data) {
				// alert(data.nama);
				$("#urutan_akun").val(data.urutan)
				$("#modal_add_akun").modal('show');
			}
		});
	}
	function hapus_akun($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Akun ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			var table = $('#index_list').DataTable();
			$.ajax({
				url: '{site_url}tneraca_setting/hapus_akun',
				type: 'POST',
				dataType: 'json',
				data: {
					id: $id
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});						
					table.ajax.reload( null, false );
				}
			});
		});

		
	}
</script>
