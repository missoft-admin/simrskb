<div class="modal fade in black-overlay" id="modal_add_section" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Tambah Section</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm">Nama Section</label>
									<div class="col-md-9">
																		
										<input type="hidden" class="form-control" id="id_edit_section" value="">									
										<input type="text"  autocomplete="off" class="form-control" id="nama_section" placeholder="Nama Section" name="nama_section" value="">									
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm">Urutan</label>
									<div class="col-md-9">
										<input type="text" class="form-control number" id="urutan_section"  name="urutan_section" placeholder="Urutan" value="">									
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="tipe_adm">Summary</label>
									<div class="col-md-9">
										<select name="st_jumlah_section" style="width: 100%" id="st_jumlah_section" class="js-select2 form-control input-sm">										
											<option value="1" selected> YA </option>
											<option value="0"> TIDAK </option>										
										</select>
									</div>
								</div>															
							</div>							
						</form>
					</div>
					
				</div>
			</div>
			<div class="modal-footer">				
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Keluar</button>
				<button class="btn btn-sm btn-success" type="button" id="btn_simpan_section">Simpan</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	$(document).on('click', '#btn_simpan_section', function() {	
		if (validate_simpan_section()==false){return false}
		var id_edit_section=$("#id_edit_section").val();
		var template_id=$("#template_id").val();
		var nama=$("#nama_section").val();
		var urutan=$("#urutan_section").val();
		var st_jumlah=$("#st_jumlah_section").val();
		var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}tneraca_setting/save_section',
			type: 'POST',
			data: {
				id_edit_section: id_edit_section,template_id: template_id,nama: nama,urutan: urutan,st_jumlah: st_jumlah
				},
			complete: function() {
				$("#modal_add_section").modal('hide');
				$.toaster({priority : 'success', title : 'Succes!', message : ' Save Data'});
				
				table.ajax.reload( null, false );
			}
		});
		
	});
	$(document).on("click","#btn_add_section",function(){	
		clear_section();
		$.ajax({
			url: '{site_url}tneraca_setting/max_section',
			type: 'POST',
			dataType: 'json',
			data: {
				template_id: $("#template_id").val()
				},
			success: function(data) {
				// alert(data.nama);
				$("#urutan_section").val(data.urutan)
				$("#nama_section").focus();
			}
		});
		$("#modal_add_section").modal('show');
	});
	function clear_section(){
		$("#id_edit_section").val('');
		$("#nama_section").val('');
		$("#urutan_section").val('');
	}
	function validate_simpan_section(){
		if ($("#nama_section").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Section", "error");
			return false;
		}
		if ($("#urutan_section").val()==''){
			sweetAlert("Maaf...", "Tentukan Urutan Section", "error");
			return false;
		}
		
		return true;
	}
	function edit_section($id){
		$("#id_edit_section").val($id);
		$.ajax({
			url: '{site_url}tneraca_setting/get_info_section',
			type: 'POST',
			dataType: 'json',
			data: {
				id: $id
				},
			success: function(data) {
				// alert(data.nama);
				$("#nama_section").val(data.nama)
				$("#urutan_section").val(data.urutan)
				$("#st_jumlah_section").val(data.st_jumlah).trigger('change');
				$("#modal_add_section").modal('show');
			}
		});
	}
	function hapus_section($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Section ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			var table = $('#index_list').DataTable();
			$.ajax({
				url: '{site_url}tneraca_setting/hapus_section',
				type: 'POST',
				dataType: 'json',
				data: {
					id: $id
					},
				success: function(data) {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});						
					table.ajax.reload( null, false );
				}
			});
		});

		
	}
</script>
