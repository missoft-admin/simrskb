<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mrka_kegiatan/add/{idrka}" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mrka_kegiatan/save_kegiatan','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			
			<input type="hidden" class="form-control" id="id"  placeholder="Nama RKA" name="id" value="{id}" required="" aria-required="true">
			<input type="hidden" class="form-control" id="idrka"  placeholder="Nama RKA" name="idrka" value="{idrka}" required="" aria-required="true">
			<input type="hidden" class="form-control" id="disabel"  placeholder="Nama RKA" name="disabel" value="{disabel}" required="" aria-required="true">
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Prespektif</label>
				<div class="col-md-4">
					<select id="idprespektif" <?=$disabel?> name="idprespektif" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<? foreach($list_prespektif as $row){?>		
							<option value="<?=$row->idprespektif?>" <?=($idprespektif==$row->idprespektif?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
				<label class="col-md-2 control-label" for="nama">Program</label>
				<div class="col-md-4">
					<select id="idprogram" <?=$disabel?> name="idprogram" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<? foreach($list_program as $row){?>		
								<option value="<?=$row->idprogram?>" <?=($idprogram==$row->idprogram?'selected':'')?>><?=$row->nama?></option>
							<?}?>			
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Kegiatan</label>
				<div class="col-md-10">
					<div class="input-group">
						<select id="idkegiatan" <?=$disabel?> name="idkegiatan" class="js-select2 form-control" required="" data-placeholder="Choose one..">
							<option value="#" <?=($idkegiatan=='#'?'selected':'')?>>- Pilih Kegiatan -</option>
							<? foreach($list_kegiatan as $row){ ?>
								<option value="<?=$row->id?>" <?=($idkegiatan==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
						<span class="input-group-btn">
							<button class="btn btn-success" <?=$disabel?> type="button" title="Refresh Kegiatan" id="btn_refresh"><i class="fa fa-refresh"></i></button>
							<a href="{base_url}mkegiatan/create" <?=$disabel?> target="_blank" class="btn btn-info" type="button" title="Add Kegiatan" ><i class="fa fa-plus"></i></a>
						</span>
					</div>
					
				</div>
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">KPI</label>
				<div class="col-md-4">
					<input type="text" <?=$disabel?> class="form-control" id="kpi" name="kpi" value="{kpi}" placeholder="KPI"   required="" aria-required="true">
				</div>
				<label class="col-md-2 control-label" for="nama">Bobot %</label>
				<div class="col-md-4">
					<input type="text" <?=$disabel?> class="form-control decimal" id="bobot" name="bobot" value="{bobot}" placeholder="Bobot"   required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Target</label>
				<div class="col-md-4">
					<input type="text" <?=$disabel?> class="form-control" id="target" name="target" value="{target}" placeholder="Target"   required="" aria-required="true">
				</div>
				<label class="col-md-2 control-label" for="nama">Initiative</label>
				<div class="col-md-4">
					<input type="text" <?=$disabel?> class="form-control" id="initiative" name="initiative" value="{initiative}" placeholder="Inisiatif">
				</div>
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Unit Kegiatan</label>
				<div class="col-md-4">
					<select id="idunit" <?=$disabel?> name="idunit" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Unit -</option>
						<? foreach($list_unit as $row){ ?>
							<option value="<?=$row->idunit?>" <?=($idunit==$row->idunit?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
				<label class="col-md-2 control-label" for="nama">Unit Kolaburasi</label>
				<div class="col-md-4">
					<select id="idunit_kolab" <?=$disabel?> name="idunit_kolab[]" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one.." multiple>
						<? foreach($list_unit_kolab as $row){ ?>
							<option value="<?=$row->idunit?>" <?=$row->pilih?>><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Keterangan</label>
				<div class="col-md-10">
					<textarea class="form-control js-summernote" <?=$disabel?> rows="2" name="keterangan" id="keterangan"><?=$keterangan?></textarea>
				</div>
				
			</div>
			<div class="form-group">
				
				<label class="col-md-2 control-label" for="nama">Total Anggaran</label>
				<div class="col-md-4">
					<div class="input-group">
						<input type="text" <?=$disabel?> class="form-control number" id="total_anggaran" name="total_anggaran" value="{total_anggaran}" placeholder="Total Anggaran"   required="" aria-required="true">
						<span class="input-group-btn">
							<button class="btn btn-primary" type="button" <?=$disabel?> id="terapkan" title="Terapkan sama rata"><i class="si si-check"></i> Terapkan</button>
						</span>
					</div>
					
				</div>
				
			</div>
			<div class="form-group">				
				<label class="col-md-2 control-label" for="nama">Waktu Pelaksanaan</label>
				<div class="col-md-5">
					<table class="table table-condensed" id="tabel_s1">
						<thead>
							<tr class="primary">
								<th class="text-center" style="width: 10%;">#</th>
								<th class="text-center" style="width: 40%;">SEMESTER 1</th>
								<th class="text-center" style="width: 60%;">Nominal</th>
							</tr>
						</thead>
						<tbody>
							<? for($i=1;$i<=6;$i++){?>
							<tr>
								<td class="text-center"><?=$i?></td>
								<td><?=MONTHFormat($i)?></td>
								<td class="hidden-xs">
									<input type="text" <?=$disabel?> class="form-control number num" name="nominal[<?=$i?>]" value="<?=$nominal[$i]?>">
								</td>
								
							</tr>
							<?}?>
							
						</tbody>
					</table>		
				</div>
				<div class="col-md-5">
					<table class="table table-condensed" id="tabel_s2">
						<thead>
							<tr class="primary">
								<th class="text-center" style="width: 10%;">#</th>
								<th class="text-center" style="width: 40%;">SEMESTER 2</th>
								<th class="text-center" style="width: 60%;">Nominal</th>
							</tr>
						</thead>
						<tbody>
							<? for($i=7;$i<=12;$i++){?>
							<tr>
								<td class="text-center"><?=$i?></td>
								<td><?=MONTHFormat($i)?></td>
								<td class="hidden-xs">
									<input type="text" <?=$disabel?> class="form-control number num" name="nominal[<?=$i?>]" value="<?=$nominal[$i]?>">
								</td>								
							</tr>
							<?}?>
							
						</tbody>
					</table>		
				</div>
				
			</div>
			<hr>
			<? if ($disabel==''){?>
			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mrka"  class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?}?>
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		$('#keterangan').summernote({height: 50});
		$(".decimal").number(true,2,'.',',');
		$('.number').number(true, 0, '.', ',');
		// load_kegiatan();
	})	
	
	function load_kegiatan(){
		$.ajax({
			url: '{site_url}mrka_kegiatan/list_kegiatan/',
			dataType: "json",
			success: function(data) {
				$("#idkegiatan").empty();
				$('#idkegiatan').append('<option value="#" selected>- Pilih Kegiatan -</option>');
				$('#idkegiatan').append(data.detail);
			}
		});
	}
	$(document).on("keyup",".num",function(){
		hitung_total();
	});
	$(document).on("keyup","#bobot",function(){
		if (parseFloat($(this).val())>100){
			$(this).val(100);
		}
	});
	function hitung_total(){		
		var total=0;
		
		$('#tabel_s1 tbody tr').each(function() {
			total =parseFloat(total) + parseFloat($(this).find('td:eq(2) input').val());
			console.log(parseFloat($(this).find('td:eq(2) input').val()));
		});
		$('#tabel_s2 tbody tr').each(function() {
			total =parseFloat(total) + parseFloat($(this).find('td:eq(2) input').val());
		});
		
		$("#total_anggaran").val(total);
	}
	function validate_final(){	
		if ($("#idkegiatan").val()=='#'){
			sweetAlert("Maaf...", "Kegiatan Harus diisi", "error");
			return false;
		}
		if ($("#idunit").val()=='#'){
			sweetAlert("Maaf...", "Unit Harus diisi", "error");
			return false;
		}
		return true;
	}
	$(document).on("click","#terapkan",function(){
		terapkan_total();
	});
	function terapkan_total(){		
		var total=parseFloat($("#total_anggaran").val()) / 12;
		
		$('#tabel_s1 tbody tr').each(function() {
			$(this).find('td:eq(2) input').val(total);
			// console.log(parseFloat($(this).find('td:eq(2) input').val()));
		});
		$('#tabel_s2 tbody tr').each(function() {
			$(this).find('td:eq(2) input').val(total);
		});
		
	}
	$(document).on("click","#btn_refresh",function(){
		load_kegiatan();
	});

</script>