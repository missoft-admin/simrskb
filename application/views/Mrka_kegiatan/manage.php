<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('1386'))){ ?>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mrka_kegiatan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mrka_kegiatan/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama RKA</label>
				<div class="col-md-7">
					<input type="hidden" class="form-control" id="id"  placeholder="Nama RKA" name="id" value="{id}" required="" aria-required="true">
					<input type="text" class="form-control" id="nama" disabled placeholder="Nama RKA" name="nama" value="{nama}" required="" aria-required="true">
					<input type="hidden" class="form-control"  id="disabel" placeholder="Nama RKA" name="disabel" value="{disabel}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Periode</label>
				<div class="col-md-7">
					<select id="periode" disabled name="periode" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="">- Pilih Periode -</option>
						<?
						$tahun=date('Y');
						for($i=($tahun-1);$i<($tahun+5);$i++){?>
							<option value="<?=$i?>" <?=$periode==$i?'selected':''?>><?=$i?></option>									
						<?}?>
						
					</select>
				</div>
			</div>
			
			
			<?php echo form_close() ?>
	</div>
</div>
<?if ($id!=''){?>
<div class="block">
	<div class="block-header">	
		<h3 class="block-title">LIST KEGIATAN</h3>
		<hr style="margin-top:20px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Prespektif</label>
                    <div class="col-md-8">
                       <select id="idprespektif" name="idprespektif" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Prespektif -</option>
							<?foreach(get_all('mprespektif_rka',array('status'=>1),'urutan') as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Program</label>
                    <div class="col-md-8">
                       <select id="idprogram" name="idprogram" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Program -</option>
							<?foreach(get_all('mprogram_rka',array('status'=>1),'urutan') as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>
				
                                
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Kegiatan</label>
                    <div class="col-md-8">
                       <select id="idkegiatan" name="idkegiatan" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Kegiatan -</option>
							<?foreach(get_all('mkegiatan_rka',array('status'=>1),'urutan') as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>	
                <div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="status"></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter_detail" id="btn_filter_detail" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_program">
			<thead>
				<tr>
					<th hidden></th>
					<th hidden></th>
					<th hidden></th>					
					<th hidden></th>					
					<th hidden></th>					
					<th>PERSPEKTIF / PROGRAM / KEGIATAN</th>
					<th>PIC</th>
					<th>KODE</th>
					<th>KPI</th>
					<th>TARGET</th>
					<th>BOBOT	</th>
					<th>INITATIVE</th>
					<th>JAN</th>
					<th>FEB</th>
					<th>MAR</th>
					<th>APRL</th>
					<th>MEI</th>
					<th>JUN</th>
					<th>JUL</th>
					<th>AGS</th>
					<th>SEP</th>
					<th>OKT</th>
					<th>NOV</th>
					<th>DES</th>
					<th>SUB TOTAL</th>
					<th>KOL</th>
					<th>KET</th>
					<th>ACTION</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<?}?>
<?}?>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		if ($("#id").val()!=''){
			load_kegiatan();
		}
	})	
	
	function load_kegiatan(){
		var idprespektif=$("#idprespektif").val();
		var idrka=$("#id").val();
		var idprogram=$("#idprogram").val();
		var idkegiatan=$("#idkegiatan").val();
		var disabel=$("#disabel").val();
		// alert(idrka);
			$('#table_program').DataTable().destroy();
			table=$('#table_program').DataTable({
					"autoWidth": true,
					"pageLength": 10,
					"ordering": false,
					"processing": true,
					"serverSide": true,
					"scrollX": true,
					"order": [],
					"ajax": {
						url: '{site_url}mrka_kegiatan/load_kegiatan',
						type: "POST",
						dataType: 'json',
						data : {
							idrka:idrka,
							idprespektif:idprespektif,
							disabel:disabel,
							idprogram:idprogram,
							idkegiatan:idkegiatan,
							
						   }
					},
					createdRow: function (row, data, index) {
						//
						// if the second column cell is blank apply special formatting
						//
						if (data[1] == "") {
							console.dir(row);
							$('tr', row).addClass('warning');
						}
					},
					"columnDefs": [
						{"targets": [0,1,2,3,4], "visible": false },
						 { "width": "200px", "targets": [5,27] },
						{  className: "text-center", targets:[2] },
					],
					
				});
		
	}
	$(document).on("click","#btn_filter_detail",function(){
		load_kegiatan();
	});
	$(document).on("click",".hapus_kegiatan",function(){
		var table = $('#table_program').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,4).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menghapus Kegiatan ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka_kegiatan/hapus_kegiatan/'+id+'/0',
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Kegiatan berhasil dihapus'});
					$('#table_program').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
</script>