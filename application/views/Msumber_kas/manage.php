<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}msumber_kas" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('msumber_kas/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Jenis Kas</label>
				<div class="col-md-7">
					<select id="jenis_kas_id" name="jenis_kas_id" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="" <?=($jenis_kas_id==''?'selected':'')?> selected>- Pilih Jenis Kas -</option>
						<?foreach(get_all('mjenis_kas',array('status'=>1)) as $row){?>
							<option value="<?=$row->id?>" <?=$jenis_kas_id==$row->id?'selected':''?>><?=$row->nama?></option>									
						<?}?>
						
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama Sumber Kas</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama Sumber Kas" name="nama" value="{nama}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="noakun" placeholder="No Akun" name="noakun" value="{noakun}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Bank</label>
				<div class="col-md-7">
					<select id="bank_id" name="bank_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
						<option value="" <?=($bank_id==''?'selected':'')?> selected>- Pilih Bank -</option>
						<?foreach(get_all('mbank',array('status'=>1)) as $row){?>
							<option value="<?=$row->id?>" <?=$bank_id==$row->id?'selected':''?>><?=$row->nama?></option>									
						<?}?>
						
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">No Akun</label>
				<div class="col-md-7">
					<select id="idakun" name="idakun" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="" <?=($idakun==''?'selected':'')?> selected>- Pilih No Akun -</option>
						<?foreach(get_all('makun_nomor',array('status'=>1)) as $row){?>
							<option value="<?=$row->id?>" <?=$idakun==$row->id?'selected':''?>><?=$row->noakun.' - '.$row->namaakun?></option>									
						<?}?>
						
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Akses User</label>
				<div class="col-md-7">
					<select name="userid[]" class="js-select2 form-control" multiple="multiple">
					<?foreach($list_user as $row){?>
						<option value="<?=$row->id?>" <?=$row->selected?>><?=$row->name?></option>
					<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Koneksi Kas</label>
				<div class="col-md-7">
					<select name="kasid[]" class="js-select2 form-control" multiple="multiple">
					<?foreach($list_kas as $row){?>
						<option value="<?=$row->id?>" <?=$row->selected?>><?=$row->nama?></option>
					<?}?>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}msumber_kas" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		
	
	})	
	$("#idakun").change(function(){
		$.ajax({
			url: '{site_url}msumber_kas/get_noakun/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				$("#noakun").val(data);
			}
		});

	});

</script>