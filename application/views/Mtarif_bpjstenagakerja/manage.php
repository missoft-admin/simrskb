<?php echo ErrorSuccess($this->session) ?>
<?php if ($error != '') echo ErrorMessage($error) ?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}mtarif_bpjstenagakerja" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mtarif_bpjstenagakerja/save', 'class="form-horizontal push-10-t"') ?>
		<div class="form-group">
			<label class="col-md-3 control-label" for="nama">Nama</label>
			<div class="col-md-7">
				<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="tarifrawatjalan">Tarif Rawat Jalan</label>
			<div class="col-md-7">
				<input type="text" class="form-control number" id="tarifrawatjalan" onkeyup="sumTotal()" placeholder="Tarif Rawat Jalan" name="tarifrawatjalan" value="{tarifrawatjalan}" required="" aria-required="true">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="tarifrawatinap">Tarif Rawat Inap</label>
			<div class="col-md-7">
				<input type="text" class="form-control number" id="tarifrawatinap" onkeyup="sumTotal()" placeholder="Tarif Rawat Inap" name="tarifrawatinap" value="{tarifrawatinap}" required="" aria-required="true">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label" for="tariftotal">Tarif Total</label>
			<div class="col-md-7">
				<input type="text" class="form-control number" id="tariftotal" placeholder="Tarif Total" name="tariftotal" value="{tariftotal}" readonly required="" aria-required="true">
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-9 col-md-offset-3">
				<button class="btn btn-success" type="submit">Simpan</button>
				<a href="{base_url}mtarif_bpjstenagakerja" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
			</div>
		</div>
		<?php echo form_hidden('id', $id); ?>
		<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$(".number").number(true, 0, '.', ',');
	});

	function sumTotal() {
		var tarifrawatjalan = ($("#tarifrawatjalan").val() ? $("#tarifrawatjalan").val().replace(/,/g, '') : 0);
		var tarifrawatinap = ($("#tarifrawatinap").val() ? $("#tarifrawatinap").val().replace(/,/g, '') : 0);

		$("#tariftotal").val(parseFloat(tarifrawatjalan) + parseFloat(tarifrawatinap));
	}
</script>
