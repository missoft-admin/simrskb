<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mbagi_hasil_setting" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mbagi_hasil_setting/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			
			<div class="form-group" id="div_3">
				<label class="col-md-3 control-label" for="nama">Nama Bagi Hasil</label>
				<div class="col-md-7">
					<input type="text" class="form-control" readonly id="nama" placeholder="Nama Bagi Hasil" name="nama" value="{nama}">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="bagian_rs">Bagian RS %</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="text" class="form-control decimal" readonly id="bagian_rs" placeholder="Bagian RS" name="bagian_rs" value="{bagian_rs}">
						<span class="input-group-addon"><i class="fa fa-percent"></i></span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="bagian_rs">Bagian Pemilik Saham %</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="text" class="form-control decimal" readonly id="bagian_ps" placeholder="Bagian Pemilik Saham" name="bagian_ps" value="{bagian_ps}">
						<span class="input-group-addon"><i class="fa fa-percent"></i></span>
					</div>
					
				</div>
			</div>
			<div class="form-group" >
				<label class="col-md-3 control-label" for="nama">Tanggal Mulai</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input"  readonly type="text" id="tanggal_mulai" name="tanggal_mulai" value="<?=HumanDateShort($tanggal_mulai)?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
			</div>
			
			<?php echo form_close() ?>
	</div>

</div>

<? if ($id !=''){ ?>
<input type="hidden" class="form-control input-sm number" id="mbagi_hasil_id" name="mbagi_hasil_id"  value="{id}"/>	

<div class="block">
	<div class="block-header">
		<ul class="block-options">
       
    </ul>
		<h3 class="block-title">SETTING APPROVAL</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
			<div class="form-group">
				<div class="col-md-12">
					<div class="table-responsive">
					<table class="table table-bordered table-striped" id="index_list" style="width: 100%;">
						<thead>
							<tr>
								<th hidden>Actions</th>
								<th hidden>Actions</th>
								<th style="width: 15%;">Step</th>
								<th style="width: 15%;">Nominal</th>
								<th style="width: 20%;">User</th>	
								<th style="width: 10%;">Jika Setuju</th>								
								<th style="width: 10%;">Jika Tolak</th>	
								<th style="width: 13%;">Actions</th>
							</tr>
							<tr>
								<td hidden></td>
								<td hidden></td>
								<td>
									<select name="step" style="width: 100%" id="step" class="js-select2 form-control input-sm">
									<option value="#">- Pilih -</option>
										<?for($i=1;$i<10;$i++){?>
											<option value="<?=$i?>"><?=$i?></option>
										<?}?>
									</select>											
								</td>
								<td>
									<div class="input-group">
									<select name="operand" style="width: 100%" id="operand" class="js-select2 form-control input-sm">					
										
										<option value=">">></option>										
										<option value=">=">>=</option>	
										<option value="<"><</option>
										<option value="<="><=</option>
										<option value="=">=</option>										
									</select>
									<input type="text" style="width: 100%"  class="form-control number" id="nominal" placeholder="0" name="nominal" value="0">
									</div>
									<input type="hidden" class="form-control" id="id_edit" placeholder="0" name="id_edit" value="">										
								</td>														
								<td>									
									<select name="iduser" tabindex="16"  style="width: 100%" id="iduser" data-placeholder="User" class="js-select2 form-control input-sm"></select>	
								</td>
								<td>
									<select name="proses_setuju" style="width: 100%" id="proses_setuju" class="js-select2 form-control input-sm">										
										<option value="1">Langsung</option>
										<option value="2">Menunggu User</option>										
									</select>										
								</td>
								<td>
									<select name="proses_tolak" style="width: 100%" id="proses_tolak" class="js-select2 form-control input-sm">										
										<option value="1">Langsung</option>
										<option value="2">Menunggu User</option>										
									</select>										
								</td>
								<td>									
									<button type="button" class="btn btn-xs btn-primary" tabindex="8" id="btn_simpan" title="Masukan Item"><i class="fa fa-save"></i> Simpan</button>
									<button type="button" class="btn btn-xs btn-success" tabindex="8" id="btn_update" title="Update"><i class="fa fa-save"></i> Update</button>
									<button type="button" class="btn btn-xs btn-warning" tabindex="8" id="btn_batal" title="Batal"><i class="fa fa-refresh"></i></button>
								</td>
							</tr>
						</thead>
						<tbody></tbody>							
					</table>
					
				</div>
				</div>
			</div>
			<?php echo form_close() ?>
	</div>

</div>

<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var myDropzone 
	$(document).ready(function(){
		$(".number").number(true,0,'.',',');
		$(".decimal").number(true,2,'.',',');
		
		clear_all();
		list_user();
		load_index();
		
	})	
	
	// TANGGAL
	$(document).on("click","#btn_batal",function(){	
		clear_all();		
	});
	function clear_all(){
		$('#id_edit').val('');
		$('#nominal').val('0');
		// $('#prose').val('0');
		$('#step').val('#').trigger('change');
		$('#proses_setuju').val('1').trigger('change');
		$('#proses_tolak').val('1').trigger('change');
		
		$("#btn_simpan").show()
		$("#btn_update").hide()
		$("#btn_batal").hide()
	}
	function load_index() {
		var mbagi_hasil_id=$("#mbagi_hasil_id").val();
		
		
		// $('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 50,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"lengthChange": true,
		"order": [],
		"ajax": {
			url: '{site_url}mbagi_hasil_setting/load_setting/',
			type: "POST",
			dataType: 'json',
			data: {
				mbagi_hasil_id: mbagi_hasil_id,
			}
		},
		columnDefs: [
					{"targets": [0,1], "visible": false },
					 // {  className: "text-right", targets:[2] },
					 // {  className: "text-center", targets:[1,3] },
					 // { "width": "30%", "targets": [0,1] },
					 // { "width": "20%", "targets": [0,1,2,3,4] },
					 // { "width": "10%", "targets": [2,4] }
					 // { "width": "15%", "targets": [2,13] }

					]
		});
	}
	$(document).on("click","#btn_simpan,#btn_update",function(){	
		if (validate_add_detail()==false)return false;
		
		swal({
				title: "Anda Yakin ?",
				text : "Untuk Save Data ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				save();
			});
	});
	function validate_add_detail()
	{
		if ($("#step").val()=='' || $("#step").val()=="#"){
			sweetAlert("Maaf...", "Step Harus diisi", "error");
			return false;
		}
		if ($("#iduser").val()=='' || $("#iduser").val()==null || $("#iduser").val()=="#"){
			sweetAlert("Maaf...", "User Harus diisi", "error");
			return false;
		}
		
		
		if ($("#nominal").val()=='0'){
			if (($("#operand").val()!='>=' && $("#operand").val()!='>')){
			sweetAlert("Maaf...", "Nominal Harus diisi", "error");
			return false;
			}
		}
		
		return true;
	}
	$(document).on("change","#step",function(){
		// if ($("#id_edit").val()==''){
		list_user();
		// }
	});
	function list_user(){
		// alert($("#idjenis").val());
		$('#iduser')
			.find('option')
			.remove()
			.end()
			.append('<option value="#" selected>- Pilih User -</option>')
			.val('#').trigger("liszt:updated");
		if ($("#step").val()!='#'){
			// console.log($("#id").val()+'/'+$("#step").val()+'/'+$("#tipe_rka").val()+'/'+$("#idjenis").val());
			$.ajax({
				url: '{site_url}mbagi_hasil_setting/list_user/'+$("#mbagi_hasil_id").val()+'/'+$("#step").val(),
				dataType: "json",
				success: function(data) {
				$('#iduser')
			.find('option')
			.remove()
			.end()
			.append('<option value="#" selected>- Pilih User -</option>')
			.val('#').trigger("liszt:updated");
				$.each(data.detail, function (i,unit) {
				$('#iduser')
				.append('<option value="' + unit.id + '">' + unit.name + '</option>');
				});
				}
			});
		}else{
			$('#iduser')
			.find('option')
			.remove()
			.end()
			.append('<option value="#">- Pilih User -</option>')
		}

	}
	
	function save(){
		var id_edit=$("#id_edit").val();
		var mbagi_hasil_id=$("#mbagi_hasil_id").val();
		var iduser=$("#iduser").val();
		var step=$("#step").val();
		var proses_setuju=$("#proses_setuju").val();
		var proses_tolak=$("#proses_tolak").val();
		var operand=$("#operand").val();
		var nominal=$("#nominal").val();
		var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}mbagi_hasil_setting/save_setting',
			type: 'POST',
			data: {
				mbagi_hasil_id: mbagi_hasil_id,iduser:iduser,step:step,
				proses_setuju: proses_setuju,proses_tolak:proses_tolak,operand:operand,nominal:nominal,id_edit:id_edit
			},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Save Data'});
				table.ajax.reload( null, false );
				clear_all();
			}
		});
		
	}
	
	$(document).on("click",".edit",function(){	
		var table = $('#index_list').DataTable();
        tr = table.row($(this).parents('tr')).index()
		var id=table.cell(tr,0).data();
		var step=table.cell(tr,1).data();
		$("#step").val(step).trigger('change');
		// var tanggal_hari=table.cell(tr,1).data();
		$.ajax({
			url: '{site_url}mbagi_hasil_setting/get_edit',
			type: 'POST',
			dataType: "json",
			data: {
				id: id
			},
			success: function(data) {
				// var item=data.detail;
				$("#id_edit").val(data.id);
				
				$("#proses_setuju").val(data.proses_setuju).trigger('change');
				$("#proses_tolak").val(data.proses_tolak).trigger('change');
				$("#operand").val(data.operand).trigger('change');
				$("#nominal").val(data.nominal).trigger('change');
				var newOption = new Option(data.user_nama, data.iduser, true, true);
					
				$("#iduser").append(newOption);
				$("#iduser").val(data.iduser).trigger('change');

				console.log(data.id);
			}
		});
		
		$("#btn_simpan").hide()
		$("#btn_update").show()
		$("#btn_batal").show()
	});
	// $(document).on("click","#btn_update",function(){	
		// var tanggal_hari=$("#tanggal_hari").val();
		// var deskripsi=$("#deskripsi").val();
		
		// if (tanggal_hari=='#'){
			// sweetAlert("Maaf...", "Tanggal Harus diisi!", "error");
			// return false;
		// }
		// if (deskripsi==''){
			// sweetAlert("Maaf...", "Deskripsi Harus diisi!", "error");
			// return false;
		// }
		// swal({
				// title: "Anda Yakin ?",
				// text : "Untuk Update Data ?",
				// type : "success",
				// showCancelButton: true,
				// confirmButtonText: "Ya",
				// confirmButtonColor: "#34a263",
				// cancelButtonText: "Batalkan",
			// }).then(function() {
				// update();
			// });
	// });
	function update(){
		var id_edit=$("#id_edit").val();
		var mbagi_hasil_id=$("#mbagi_hasil_id").val();
		var tanggal_hari=$("#tanggal_hari").val();
		var deskripsi=$("#deskripsi").val();
		var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}mbagi_hasil_setting/update',
			type: 'POST',
			data: {id_edit: id_edit,mbagi_hasil_id: mbagi_hasil_id,tanggal_hari: tanggal_hari,deskripsi: deskripsi},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Update Data'});
				table.ajax.reload( null, false );
				clear_all();
				// $("#cover-spin").hide();
				// filter_form();
			}
		});
		
	}
	$(document).on("click",".hapus",function(){	
		var table = $('#index_list').DataTable();
		tr = table.row($(this).parents('tr')).index()
		var id_edit=table.cell(tr,0).data();
		$('#id_edit').val(id_edit);
        swal({
				title: "Anda Yakin ?",
				text : "Untuk Hapus data?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				hapus();
			});
	});
	function hapus(){
		var id_edit=$("#id_edit").val();
		var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}mbagi_hasil_setting/hapus',
			type: 'POST',
			data: {id_edit: id_edit},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
				table.ajax.reload( null, false );
				clear_all();
				// $("#cover-spin").hide();
				// filter_form();
			}
		});
		
	}
	
</script>