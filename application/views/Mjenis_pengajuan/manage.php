<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mjenis_pengajuan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mjenis_pengajuan/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama Jenis Pengajuan</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama Jenis Pengajuan" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="deskripsi">Aktifkan Proses Pencairan Berjenjang</label>
				<div class="col-md-7">
					<select id="st_berjenjang"  name="st_berjenjang" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="0"  <?=($st_berjenjang=='0'?'selected':'')?>>TIDAK</option>
						<option value="1"  <?=($st_berjenjang=='1'?'selected':'')?>>YA</option>
					</select>
				</div>
			</div>
			<div class="form-group" id="div_akun">
				<label class="col-md-3 control-label" for="deskripsi">Nomor Akun</label>
				<div class="col-md-7">
					<select id="idakun"  name="idakun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
						<option value="0" >- Pilih Akun -</option>
						<? foreach($list_akun as $row){ ?>
							<option value="<?=$row->id?>" <?=($idakun==$row->id?'selected':'')?>><?=$row->noakun.' '.$row->namaakun?></option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="deskripsi">Deskripsi</label>
				<div class="col-md-7">
					<textarea class="form-control js-summernote" name="deskripsi" id="deskripsi"><?=$deskripsi?></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mjenis_pengajuan" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#deskripsi').summernote({
		  height: 100,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})

		show_hide();
	})	
	$("#st_berjenjang").change(function(){
			show_hide();
	});

	function show_hide(){
		if ($("#st_berjenjang").val()=='0'){
			$("#div_akun").hide();
			$("#idakun").val(0).trigger('change');
			
		}else{
			$("#div_akun").show();
			
		}
	}
	function validate_final(){
		if ($("#st_berjenjang").val()=='1' && $("#idakun").val()=='0'){
			sweetAlert("Maaf...", "Tentukan Nomor Akun", "error");
			return false;
		}
		return true;

	}
</script>