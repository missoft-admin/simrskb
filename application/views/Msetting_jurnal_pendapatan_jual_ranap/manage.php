<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content" style="margin-top: -20px;font-size:">
		<?php echo form_open('msetting_jurnal_pendapatan_jual_ranap/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>	
		<?
		$q="SELECT *FROM mdata_tipebarang WHERE id IN(1,3)";
		$list_tipe=$this->db->query($q)->result();
		
		$q="SELECT *FROM mruangan WHERE idtipe='1'";
		$list_ruang=$this->db->query($q)->result();
		$q="SELECT *FROM mkelas WHERE status='1'";
		$list_kelas=$this->db->query($q)->result();
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="block block-themed ">
					<div class="block-header bg-primary">
						<ul class="block-options">

							<li>
								<button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
							</li>
						</ul>
						<h3 class="block-title">SETTING AKUN PENJUALAN OBAT & ALKES RAWAT INAP</h3>
					</div>
					<div class="block-content">						
						<input type="hidden" class="form-control" id="id_edit_ranap" placeholder="0" name="id_edit_ranap" value="">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Tipe Ruangan</label>
							
							<div class="col-md-4">
								<select name="idruangan" id="idruangan" style="width: 100%" id="step" class="js-select2 form-control input-sm">										
									<option value="#" selected>- Pilih Tipe Ruangan -</option>																			
									<?foreach($list_ruang as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>																				
									<?}?>	
																										
								</select>			
							</div>
							<label class="col-md-2 control-label" >Kelas</label>
							<div class="col-md-4">
								<select name="idkelas" id="idkelas" style="width: 100%" class="js-select2 form-control input-sm">
									<option value="0" selected>- Semua Kelas -</option>	
									<?foreach($list_kelas as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama?></option>																				
									<?}?>	
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Tipe Barang</label>							
							<div class="col-md-4">
								<select name="idtipe_ranap" id="idtipe_ranap" style="width: 100%" class="js-select2 form-control input-sm idbarang">
									<option value="#" selected>- Semua Tipe -</option>		
									<?foreach($list_tipe as $row){?>
										<option value="<?=$row->id?>"><?=$row->nama_tipe?></option>																				
									<?}?>
								</select>	
							</div>
							
						</div>
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-2 control-label" >Kategori</label>
							
							<div class="col-md-4">
								<select name="idkategori_ranap" id="idkategori_ranap" style="width: 100%" class="js-select2 form-control input-sm idkategori"></select>
							</div>
							<label class="col-md-2 control-label" >Group Penjualan</label>
							<div class="col-md-4">
								<select name="idgroup_penjualan_ranap" id="idgroup_penjualan_ranap" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" >Barang</label>
							
							<div class="col-md-4">
								<select name="idbarang_ranap" id="idbarang_ranap" style="width: 100%" class="js-select2 form-control input-sm"></select>
							</div>
							<label class="col-md-2 control-label" >Group Diskon</label>
							<div class="col-md-4">
								<select name="idgroup_diskon_ranap" id="idgroup_diskon_ranap" style="width: 100%" class="js-select2 form-control input-sm idgroup"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" ></label>
							
							<div class="col-md-4">
								<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_ranap" title="Masukan Item"><i class="fa fa-save"></i> Simpan</button>
								<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_ranap" title="Batalkan"><i class="fa fa-refresh"></i> Clear Input</button>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<table id="tabel_ranap" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 0%;">#</th>															
											<th style="width: 8%;">Ruang</th>															
											<th style="width: 8%;">Kelas</th>															
											<th style="width: 8%;">Tipe</th>															
											<th style="width: 9%;">Kategori</th>															
											<th style="width: 11%;">Barang</th>															
											<th style="width: 12%;">Group Pembelian</th>															
											<th style="width: 12%;">Group Diskon</th>															
											<th style="width: 5%;">Actions</th>
										</tr>
										
									</thead>
									<tbody>
										
									</tbody>
									
							</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		list_group();
		list_kategori_ranap('#');
		clear_input_ranap();
		list_barang_ranap();
		load_ranap();
		
	});	
	function list_group(){
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_general/list_akun_group',
			dataType: "json",
			success: function(data) {
				$(".idgroup").empty();
				$('.idgroup').append('<option value="#" selected>- Pilih Group -</option>');
				$('.idgroup').append(data.detail);
				
			}
		});		
	}
	
	
	
	
	$(document).on("change","#idtipe_ranap",function(){
		list_kategori_ranap($(this).val());
		list_barang_ranap();
	});
	$(document).on("change","#idkategori_ranap",function(){
		list_barang_ranap();
	});
	function list_kategori_ranap($id){
		if ($id!='#'){
			$.ajax({
				url: '{site_url}msetting_jurnal_pembelian/list_kategori/'+$id,
				dataType: "json",
				success: function(data) {
					$("#idkategori_ranap").empty();
					$('#idkategori_ranap').append('<option value="0" selected>- Semua Kategori -</option>');
					$('#idkategori_ranap').append(data.detail);
				}
			});
		}else{
			$("#idkategori_ranap").empty();
			$('#idkategori_ranap').append('<option value="0" selected>- Semua Kategori -</option>');
		}		
	}
	function list_barang_ranap(){
		var newOption = new Option('- Semua Barang -', '0', true, true);					
		$("#idbarang_ranap").append(newOption);
		$("#idbarang_ranap").val("0").trigger('change');
		
		var idtipe=$("#idtipe_ranap").val();
		var idkategori=$("#idkategori_ranap").val();
		$("#idbarang_ranap").select2({
			minimumInputLength: 0,
			noResults: 'Obat Tidak Ditemukan.',
			// allowClear: true
			// tags: [],
			ajax: {
				url: '{site_url}msetting_jurnal_pembelian/list_barang/',
				dataType: 'json',
				type: "POST",
				quietMillis: 50,
			 data: function (params) {
				  var query = {
					search: params.term,idtipe: idtipe,idkategori:idkategori,
				  }
				  return query;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.kode+' - '+item.nama,
								id: item.id,
								idtipe : item.idtipe
							}
						})
					};
				}
			}
		});

	}
	
	function validate_add_ranap()
	{
			var idtipe_ranap=$("#idtipe_ranap").val();
			var idruangan=$("#idruangan").val();
			var idgroup_penjualan_ranap=$("#idgroup_penjualan_ranap").val();
			var idgroup_diskon_ranap=$("#idgroup_diskon_ranap").val();
			var idgroup_tuslah_ranap=$("#idgroup_tuslah_ranap").val();
		
			if (idruangan=='#'){
				sweetAlert("Maaf...", "Jenis Kunjungan Harus diisi", "error");
				return false;
			}
			// if (idtipe_ranap=='#'){
				// sweetAlert("Maaf...", "Tipe Barang Harus diisi", "error");
				// return false;
			// }
			
			
			if (idgroup_penjualan_ranap=='#'){
				sweetAlert("Maaf...", "Group Penjualan Harus diisi", "error");
				return false;
			}
			if (idgroup_diskon_ranap=='#'){
				sweetAlert("Maaf...", "Group Diskon Harus diisi", "error");
				return false;
			}
			// if (idgroup_tuslah_ranap=='#'){
				// sweetAlert("Maaf...", "Group Tuslah Harus diisi", "error");
				// return false;
			// }
			
		return true;
	}
	function clear_input_ranap(){
		$("#idtipe_ranap").val('#').trigger('change');	
		$("#idruangan").val('#').trigger('change');	
		
		$("#idgroup_penjualan_ranap").val('#').trigger('change');
		$("#idgroup_diskon_ranap").val('#').trigger('change');
		
		$("#idkategori_ranap").val('0').trigger('change');
		$("#idkelas").val('0').trigger('change');
		$("#idbarang_ranap").val('0').trigger('change');
	}
	$(document).on("click","#simpan_ranap",function(){
		if (validate_add_ranap()==false)return false;
		var idruangan=$("#idruangan").val();
		var idkelas=$("#idkelas").val();
		var idtipe=$("#idtipe_ranap").val();
		var idkategori=$("#idkategori_ranap").val();
		var idbarang=$("#idbarang_ranap").val();
		var idgroup_penjualan=$("#idgroup_penjualan_ranap").val();
		var idgroup_diskon=$("#idgroup_diskon_ranap").val();
	
		
		// alert('SINI');
		$.ajax({
			url: '{site_url}msetting_jurnal_pendapatan_jual_ranap/simpan_ranap',
			type: 'POST',
			data: {
				idruangan:idruangan
				,idkelas:idkelas
				,idtipe:idtipe
				,idkategori:idkategori
				,idbarang:idbarang
				,idgroup_penjualan:idgroup_penjualan
				,idgroup_diskon:idgroup_diskon
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_ranap').DataTable().ajax.reload( null, false );
					clear_input_ranap();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	function hapus_ranap($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_pendapatan_jual_ranap/hapus_ranap/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_ranap').DataTable().ajax.reload( null, false );
						clear_input_ranap();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_ranap",function(){		
		clear_input_ranap();
	});
	function load_ranap(){
		var idlogic=$("#id").val();
		
		$('#tabel_ranap').DataTable().destroy();
		var table = $('#tabel_ranap').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_pendapatan_jual_ranap/load_ranap/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
					]
		});
	}
	
	// Racikan
	$(document).on("change","#idruangan_r",function(){
		list_poli_kelas_r($(this).val());
	});
	
</script>
