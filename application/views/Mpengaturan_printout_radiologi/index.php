<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mpengaturan_printout_radiologi" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?php echo form_open_multipart('mpengaturan_printout_radiologi/save','class="form-horizontal push-10-t" id="form-work"') ?>
			
			<h5 style="margin-bottom: 10px;" for="">LOGO</h5>
			<div class="row">
				<div class="col-md-1">
					<?php if($logo != ''){ ?>
					<div class="form-group">
						<div class="col-md-12">
							<img class="img-fluid" src="{upload_path}pengaturan_printout_radiologi/{logo}" width="100px" height="100px" style="object-fit: cover; border-radius: 10px; border: 1px solid #DEDEDE;"/>
						</div>
					</div>
					<?php } ?>
				</div>
				<div class="col-md-11">
					<div class="form-group">
						<div class="col-md-12">
							<div class="box">
								<input type="file" id="file-3" class="inputfile inputfile-3" style="display:none;" name="logo" value="{logo}" />
								<label for="file-3" class="btn-default">
									<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg>
									<span>Pilih File&hellip;</span>
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>

			<hr>

			<h5 style="margin-bottom: 10px;" for="">Header Name</h5>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_header" placeholder="Header Name (Indonesia Version)">{label_header}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_header_eng" placeholder="Header Name (English Version)">{label_header_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Sub Header Name
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_subheader" placeholder="Sub Header Name (Indonesia Version)">{label_subheader}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_subheader_eng" placeholder="Tujuan Radiologi (English Version)">{label_subheader_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Footer</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_footer" placeholder="Footer (Indonesia Version)">{label_footer}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_footer_eng" placeholder="Footer (English Version)">{label_footer_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-12" for="">Tampilkan Tanggan & Jam Cetak</label>
				<div class="col-md-12">
					<select name="tampilkan_tanggal_jam_cetak" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="1" <?=($tampilkan_tanggal_jam_cetak == 1 ? 'selected="selected"':'')?>>Ya</option>
						<option value="0" <?=($tampilkan_tanggal_jam_cetak == 2 ? 'selected="selected"':'')?>>Tidak</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-12" for="">Tampilkan Tanda Tangan</label>
				<div class="col-md-12">
					<select name="tampilkan_tanda_tangan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="1" <?=($tampilkan_tanda_tangan == 1 ? 'selected="selected"':'')?>>Ya</option>
						<option value="0" <?=($tampilkan_tanda_tangan == 2 ? 'selected="selected"':'')?>>Tidak</option>
					</select>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 text-right">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mpengaturan_printout_radiologi" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>

			<br /><br />

	</div>
</div>