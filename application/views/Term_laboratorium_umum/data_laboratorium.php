<?php declare(strict_types=1);
echo ErrorSuccess($this->session); ?>
<?php if ('' !== $error) {
    echo ErrorMessage($error);
} ?>

<div class="block">
    <div class="block-content">
        <h4 class="text-center">DATA LABORATORIUM</h4> <br>

        <div class="row" id="form-work">
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 15px">
                    <label for="nomorRekamMedis">Nomor Rekam Medis & Nama Pasien</label>
                    <input type="text" class="form-control" id="nomorRekamMedis" placeholder="Nomor Rekam Medis" disabled value="{nomor_medrec} {nama_pasien}">
                </div>

                <div class="form-group" style="margin-bottom: 15px">
                    <label for="nik">Nomor Induk Kependudukan</label>
                    <input type="text" class="form-control" id="nik" placeholder="NIK" disabled value="{nomor_ktp}">
                </div>

                <div class="form-group" style="margin-bottom: 15px">
                    <label for="jenisKelamin">Jenis Kelamin</label>
                    <input type="text" class="form-control" id="jenisKelamin" placeholder="Jenis Kelamin" disabled value="{jenis_kelamin}">
                </div>

                <div class="form-group" style="margin-bottom: 15px">
                    <label for="alamat">Alamat</label>
                    <input type="text" class="form-control" id="alamat" placeholder="Alamat" disabled value="{alamat_pasien}">
                </div>

                <div class="row" style="margin-bottom: 15px">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="tglLahir">Tanggal Lahir</label>
                            <input type="date" class="form-control" id="tglLahir" placeholder="Tanggal Lahir" disabled value="{tanggal_lahir}">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="umur">Umur</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="tahun" placeholder="Tahun" disabled value="{umur_tahun}">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">Tahun</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="umur">&nbsp;</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="bulan" placeholder="Bulan" disabled value="{umur_bulan}">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">Bulan</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="umur">&nbsp;</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="hari" placeholder="Hari" disabled value="{umur_hari}">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">Hari</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="kelompok_pasien">Kelompok Pasien</label>
                            <input type="text" class="form-control" id="kelompok_pasien" placeholder="Kelompok Pasien" disabled value="{kelompok_pasien}">
                        </div>

                        <div class="form-group">
                            <label for="tipe_kunjungan">Tipe Kunjungan</label>
                            <input type="text" class="form-control" id="tipe_kunjungan" placeholder="Tipe Kunjungan" disabled value="{tipe_kunjungan}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama_asuransi">Nama Asuransi</label>
                            <input type="text" class="form-control" id="nama_asuransi" placeholder="Nama Asuransi" disabled value="{nama_asuransi}">
                        </div>

                        <div class="form-group">
                            <label for="nama_poliklinik">Nama Poliklinik</label>
                            <input type="text" class="form-control" id="nama_poliklinik" placeholder="Nama Poliklinik" disabled value="{nama_poliklinik}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dpjp">DPJP</label>
                            <input type="text" class="form-control" id="dpjp" placeholder="Tipe Kunjungan" disabled value="{dpjp}">
                        </div>
                    </div>
                        
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dokter_peminta_id">Dokter Peminta</label>
                            <select name="dokter_peminta_id" id="dokter_peminta_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <?php foreach (get_all('mdokter', ['status' => '1']) as $r) { ?>
                                <option value="<?php echo $r->id; ?>" <?php echo $r->id == $dokter_peminta_id ? 'selected' : ''; ?>><?php echo $r->nama; ?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama_dokter_luar_rs">Nama Dokter</label>
                            <input type="text" class="form-control" id="nama_dokter_luar_rs" placeholder="* Diisi Jika Permintaan Dari Luar RS" value="{nama_dokter_luar_rs}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="notelp_dokter_pengirim_luar_rs">No. Telepon Dokter Pengirim</label>
                            <input type="text" class="form-control" id="notelp_dokter_pengirim_luar_rs" placeholder="* Diisi Jika Permintaan Dari Luar RS" value="{notelp_dokter_pengirim_luar_rs}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama_fasilitas_pelayanana_kesehatan_luar_rs">Nama Fasilitas Pelayanan Kesehatan</label>
                            <input type="text" class="form-control" id="nama_fasilitas_pelayanana_kesehatan_luar_rs" placeholder="* Diisi Jika Permintaan Dari Luar RS" value="{nama_fasilitas_pelayanana_kesehatan_luar_rs}">
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <!-- First row -->
                <div class="row">
                    <!-- First set of col-md-4 -->
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="nomor_pendaftaran">Nomor Pendaftaran</label>
                            <input type="text" class="form-control" id="nomor_pendaftaran" placeholder="Nomor Pendaftaran" disabled value="{nomor_pendaftaran}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="nomor_permintaan">Nomor Permintaan</label>
                            <input type="text" class="form-control" id="nomor_permintaan" placeholder="Nomor Permintaan" disabled value="{nomor_permintaan}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="nomor_transaksi_laboratorium">Nomor Transaksi Laboratorium</label>
                            <input type="text" class="form-control" id="nomor_transaksi_laboratorium" placeholder="Nomor Transaksi Laboratorium" disabled value="{nomor_transaksi_laboratorium}">
                        </div>
                    </div>
                </div>

                <!-- Second row -->
                <div class="row">
                    <!-- Second set of col-md-4 -->
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="waktu_pembuatan">Waktu Pembuatan</label>
                            <input type="text" class="form-control" id="waktu_pembuatan" placeholder="Waktu Pembuatan" disabled value="{waktu_pembuatan}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="waktu_permintaan">Waktu Permintaan</label>
                            <input type="text" class="form-control" id="waktu_permintaan" placeholder="Waktu Permintaan" disabled value="{waktu_permintaan}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="rencana_pemeriksaan">Rencana Pemeriksaan</label>
                            <input type="text" class="form-control" id="rencana_pemeriksaan" placeholder="Rencana Pemeriksaan" disabled value="{rencana_pemeriksaan}">
                        </div>
                    </div>
                </div>

                <!-- Third row -->
                <div class="row">
                    <!-- Third set of col-md-4 -->
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="diagnosa">Diagnosa</label>
                            <input type="text" class="form-control" id="diagnosa" placeholder="Diagnosa" disabled value="{diagnosa}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="catatan_permintaan">Catatan Permintaan</label>
                            <input type="text" class="form-control" id="catatan_permintaan" placeholder="Catatan Permintaan" disabled value="{catatan_permintaan}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="nomor_laboratorium">No Laboratorium</label>
                            <input type="text" class="form-control" id="nomor_laboratorium" placeholder="No Laboratorium" disabled value="{nomor_laboratorium}">
                        </div>
                    </div>
                </div>

                <!-- Fourth row -->
                <div class="row">
                    <!-- First set of col-md-4 with select elements -->
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="prioritas">Prioritas Pemeriksaan</label>
                            <select name="prioritas" id="prioritas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php foreach (list_variable_ref(85) as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?php echo $row->id == $prioritas ? 'selected' : ''; ?> <?php echo $row->id == $prioritas ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="pasien_puasa">Pasien Puasa</label>
                            <select name="pasien_puasa" id="pasien_puasa" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php foreach (list_variable_ref(87) as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?php echo $row->id == $pasien_puasa ? 'selected' : ''; ?> <?php echo $row->id == $pasien_puasa ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="pengiriman_hasil">Pengiriman Hasil</label>
                            <select name="pengiriman_hasil" id="pengiriman_hasil" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php foreach (list_variable_ref(88) as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?php echo $row->id == $pengiriman_hasil ? 'selected' : ''; ?> <?php echo $row->id == $pengiriman_hasil ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <!-- Fifth row -->
                <div class="row">
                    <!-- First set of col-md-6 with input date and select elements -->
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-xs-12" for="">Waktu Pengambilan Sample</label>
                            <div class="col-xs-12">
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group date">
                                    <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_pengambilan_sample" id="tanggal_pengambilan_sample" placeholder="HH/BB/TTTT" value="{tanggal_pengambilan_sample}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                    <input type="text" class="time-datepicker form-control" name="waktu_pengambilan_sample" id="waktu_pengambilan_sample" value="{waktu_pengambilan_sample}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="petugas_pengambilan_sample">Petugas Pengambil Sample</label>
                            <select name="petugas_pengambilan_sample" class="js-select2 form-control" id="petugas_pengambilan_sample" style="width: 100%;" data-placeholder="Pilih Opsi">
                            </select>
                        </div>
                    </div>
                </div>

                <!-- Sixth row -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-xs-12" for="">Waktu Pengujian dan Pengolahan</label>
                            <div class="col-xs-12">
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group date">
                                    <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_pengujian_sample" id="tanggal_pengujian_sample" placeholder="HH/BB/TTTT" value="{tanggal_pengujian_sample}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                    <input type="text" class="time-datepicker form-control" name="waktu_pengujian_sample" id="waktu_pengujian_sample" value="{waktu_pengujian_sample}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="petugas_pengujian_sample">Petugas Pengujian dan Pengolahan</label>
                            <select name="petugas_pengujian_sample" class="js-select2 form-control" id="petugas_pengujian_sample" style="width: 100%;" data-placeholder="Pilih Opsi">
                            </select>
                        </div>
                    </div>
                </div>

                <!-- Seventh row -->
                <div class="row">
                    <!-- Third set of col-md-6 with input date and select elements -->
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-xs-12" for="">Waktu Penginputan Hasil</label>
                            <div class="col-xs-12">
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group date">
                                    <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_penginputan_hasil" id="tanggal_penginputan_hasil" placeholder="HH/BB/TTTT" value="{tanggal_penginputan_hasil}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                    <input type="text" class="time-datepicker form-control" name="waktu_penginputan_hasil" id="waktu_penginputan_hasil" value="{waktu_penginputan_hasil}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="petugas_penginputan_hasil">Petugas Yang Melakukan Penginputan</label>
                            <select name="petugas_penginputan_hasil" class="js-select2 form-control" id="petugas_penginputan_hasil" style="width: 100%;" data-placeholder="Pilih Opsi">
                            </select>
                        </div>
                    </div>
                </div>

                <!-- Eighth row -->
                <div class="row">
                    <!-- Fourth set of col-md-6 with input date and select elements -->
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-xs-12" for="">Waktu Validasi Hasil</label>
                            <div class="col-xs-12">
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group date">
                                    <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_validasi_hasil" id="tanggal_validasi_hasil" placeholder="HH/BB/TTTT" value="{tanggal_validasi_hasil}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                    <input type="text" class="time-datepicker form-control" name="waktu_validasi_hasil" id="waktu_validasi_hasil" value="{waktu_validasi_hasil}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="petugas_validasi_hasil">Dokter Yang Melakukan Validasi</label>
                            <select name="petugas_validasi_hasil" class="js-select2 form-control" id="petugas_validasi_hasil" style="width: 100%;" data-placeholder="Pilih Opsi">
                            </select>
                        </div>
                    </div>
                </div>

                <!-- Ninth row -->
                <div class="row">
                    <!-- Fifth set of col-md-6 with input date and select elements -->
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-xs-12" for="">Waktu Pengiriman Hasil</label>
                            <div class="col-xs-12">
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group date">
                                    <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_pengiriman_hasil" id="tanggal_pengiriman_hasil" placeholder="HH/BB/TTTT" value="{tanggal_pengiriman_hasil}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                    <input type="text" class="time-datepicker form-control" name="waktu_pengiriman_hasil" id="waktu_pengiriman_hasil" value="{waktu_pengiriman_hasil}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="petugas_pengiriman_hasil">Petugas Yang Mengirim Hasil</label>
                            <select name="petugas_pengiriman_hasil" class="js-select2 form-control" id="petugas_pengiriman_hasil" style="width: 100%;" data-placeholder="Pilih Opsi">
                            </select>
                        </div>
                    </div>
                </div>

                <!-- Tenth row -->
                <div class="row">
                    <!-- Sixth set of col-md-6 with input date and select elements -->
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-xs-12" for="">Waktu Permintaan Hasil oleh Unit</label>
                            <div class="col-xs-12">
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group date">
                                    <input type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" name="tanggal_penerimaan_hasil" id="tanggal_penerimaan_hasil" placeholder="HH/BB/TTTT" value="{tanggal_penerimaan_hasil}">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                    <input type="text" class="time-datepicker form-control" name="waktu_penerimaan_hasil" id="waktu_penerimaan_hasil" value="{waktu_penerimaan_hasil}">
                                    <span class="input-group-addon"><i class="si si-clock"></i></span>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" style="margin-bottom: 15px">
                            <label for="petugas_penerimaan_hasil">Petugas Yang Menerima Hasil</label>
                            <select name="petugas_penerimaan_hasil" class="js-select2 form-control" id="petugas_penerimaan_hasil" style="width: 100%;" data-placeholder="Pilih Opsi">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        loadDataPetugasPengambilSample('{tujuan_laboratorium}', '{petugas_pengambilan_sample}');
        loadDataPetugasPengujian('{tujuan_laboratorium}', '{petugas_pengujian_sample}');
        loadDataPetugasPenginputanHasil('{tujuan_laboratorium}', '{petugas_penginputan_hasil}');
        loadDataPetugasValidasiHasil('{tujuan_laboratorium}', '{petugas_validasi_hasil}');
        loadDataPetugasPengirimanHasil('{tujuan_laboratorium}', '{petugas_pengiriman_hasil}');
        loadDataPetugasPenerimaHasil('{tujuan_laboratorium}', '{petugas_penerimaan_hasil}');

        $("#form-work input").on('blur', function() {
            updateDataLaboratorium();
        });

        $("#form-work select").on('change', function() {
            updateDataLaboratorium();
        });
    });

    function loadDataPetugasPengambilSample(value, selected) {
        $.ajax({
            url: '{site_url}term_laboratorium_umum/petugas_pengambilan_sample/' + value,
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                $("#petugas_pengambilan_sample").select2('destroy');
                $("#petugas_pengambilan_sample").empty();
                
                $("#petugas_pengambilan_sample").append('<option value="" ' + (selected == '' ? "selected" : "") + '>Pilih Opsi</option>');
                
                response.petugas.map(function(petugas) {
                $("#petugas_pengambilan_sample").append('<option value="' + petugas.iduser + '" ' + (petugas.iduser == selected ? "selected" : "") + '>' + petugas.nama_petugas + '</option>');
                });

                $("#petugas_pengambilan_sample").select2();
            },
            error: function(error) {
                console.error('Error loading data:', error);
            }
        });
    }

    function loadDataPetugasPengujian(value, selected) {
        $.ajax({
            url: '{site_url}term_laboratorium_umum/petugas_pengujian_sample/' + value,
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                $("#petugas_pengujian_sample").select2('destroy');
                $("#petugas_pengujian_sample").empty();
                
                $("#petugas_pengujian_sample").append('<option value="" ' + (selected == '' ? "selected" : "") + '>Pilih Opsi</option>');
                
                response.petugas.map(function(petugas) {
                $("#petugas_pengujian_sample").append('<option value="' + petugas.iduser + '" ' + (petugas.iduser == selected ? "selected" : "") + '>' + petugas.nama_petugas + '</option>');
                });

                $("#petugas_pengujian_sample").select2();
            },
            error: function(error) {
                console.error('Error loading data:', error);
            }
        });
    }

    function loadDataPetugasPenginputanHasil(value, selected) {
        $.ajax({
            url: '{site_url}term_laboratorium_umum/petugas_penginputan_hasil/' + value,
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                $("#petugas_penginputan_hasil").select2('destroy');
                $("#petugas_penginputan_hasil").empty();
                
                $("#petugas_penginputan_hasil").append('<option value="" ' + (selected == '' ? "selected" : "") + '>Pilih Opsi</option>');

                response.petugas.map(function(petugas) {
                $("#petugas_penginputan_hasil").append('<option value="' + petugas.iduser + '" ' + (petugas.iduser == selected ? "selected" : "") + '>' + petugas.nama_petugas + '</option>');
                });

                $("#petugas_penginputan_hasil").select2();
            },
            error: function(error) {
                console.error('Error loading data:', error);
            }
        });
    }

    function loadDataPetugasValidasiHasil(value, selected) {
        $.ajax({
            url: '{site_url}term_laboratorium_umum/petugas_validasi_hasil/' + value,
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                $("#petugas_validasi_hasil").select2('destroy');
                $("#petugas_validasi_hasil").empty();
                
                $("#petugas_validasi_hasil").append('<option value="" ' + (selected == '' ? "selected" : "") + '>Pilih Opsi</option>');

                response.petugas.map(function(petugas) {
                $("#petugas_validasi_hasil").append('<option value="' + petugas.iduser + '" ' + (petugas.iduser == selected ? "selected" : "") + '>' + petugas.nama_petugas + '</option>');
                });

                $("#petugas_validasi_hasil").select2();
            },
            error: function(error) {
                console.error('Error loading data:', error);
            }
        });
    }

    function loadDataPetugasPengirimanHasil(value, selected) {
        $.ajax({
            url: '{site_url}term_laboratorium_umum/petugas_pengiriman_hasil/' + value,
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                $("#petugas_pengiriman_hasil").select2('destroy');
                $("#petugas_pengiriman_hasil").empty();
                
                $("#petugas_pengiriman_hasil").append('<option value="" ' + (selected == '' ? "selected" : "") + '>Pilih Opsi</option>');

                response.petugas.map(function(petugas) {
                $("#petugas_pengiriman_hasil").append('<option value="' + petugas.iduser + '" ' + (petugas.iduser == selected ? "selected" : "") + '>' + petugas.nama_petugas + '</option>');
                });

                $("#petugas_pengiriman_hasil").select2();
            },
            error: function(error) {
                console.error('Error loading data:', error);
            }
        });
    }

    function loadDataPetugasPenerimaHasil(value, selected) {
        $.ajax({
            url: '{site_url}term_laboratorium_umum/petugas_penerimaan_hasil/' + value,
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                $("#petugas_penerimaan_hasil").select2('destroy');
                $("#petugas_penerimaan_hasil").empty();
                
                $("#petugas_penerimaan_hasil").append('<option value="" ' + (selected == '' ? "selected" : "") + '>Pilih Opsi</option>');

                response.petugas.map(function(petugas) {
                $("#petugas_penerimaan_hasil").append('<option value="' + petugas.iduser + '" ' + (petugas.iduser == selected ? "selected" : "") + '>' + petugas.nama_petugas + '</option>');
                });

                $("#petugas_penerimaan_hasil").select2();
            },
            error: function(error) {
                console.error('Error loading data:', error);
            }
        });
    }

    function updateDataLaboratorium() {
        var payload = {
            transaksi_id: '{transaksi_id}',
            nama_dokter_luar_rs: $("#nama_dokter_luar_rs").val(),
            notelp_dokter_pengirim_luar_rs: $("#notelp_dokter_pengirim_luar_rs").val(),
            nama_fasilitas_pelayanana_kesehatan_luar_rs: $("#nama_fasilitas_pelayanana_kesehatan_luar_rs").val(),
            prioritas: $("#prioritas option:selected").val(),
            pasien_puasa: $("#pasien_puasa option:selected").val(),
            pengiriman_hasil: $("#pengiriman_hasil option:selected").val(),
            tanggal_pengambilan_sample: $("#tanggal_pengambilan_sample").val(),
            waktu_pengambilan_sample: $("#waktu_pengambilan_sample").val(),
            petugas_pengambilan_sample: $("#petugas_pengambilan_sample option:selected").val(),
            tanggal_pengujian_sample: $("#tanggal_pengujian_sample").val(),
            waktu_pengujian_sample: $("#waktu_pengujian_sample").val(),
            petugas_pengujian_sample: $("#petugas_pengujian_sample option:selected").val(),
            tanggal_penginputan_hasil: $("#tanggal_penginputan_hasil").val(),
            waktu_penginputan_hasil: $("#waktu_penginputan_hasil").val(),
            petugas_penginputan_hasil: $("#petugas_penginputan_hasil option:selected").val(),
            tanggal_validasi_hasil: $("#tanggal_validasi_hasil").val(),
            waktu_validasi_hasil: $("#waktu_validasi_hasil").val(),
            petugas_validasi_hasil: $("#petugas_validasi_hasil option:selected").val(),
            tanggal_pengiriman_hasil: $("#tanggal_pengiriman_hasil").val(),
            waktu_pengiriman_hasil: $("#waktu_pengiriman_hasil").val(),
            petugas_pengiriman_hasil: $("#petugas_pengiriman_hasil option:selected").val(),
            tanggal_penerimaan_hasil: $("#tanggal_penerimaan_hasil").val(),
            waktu_penerimaan_hasil: $("#waktu_penerimaan_hasil").val(),
            petugas_penerimaan_hasil: $("#petugas_penerimaan_hasil option:selected").val(),
        };

        $.ajax({
            url: '{site_url}term_laboratorium_umum/update_data_laboratorium', 
            type: 'POST',
            data: payload,
            success: function (result) {
                $.toaster({ priority: 'success', title: 'Success!', message: 'Perubahan Data berhasil disimpan' });
            },
            error: function (error) {
                // Handle error if deletion fails
                console.error('Error cloning order:', error);
                alert('Error cloning order. Please try again.');
            }
        });
    }
</script>