<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php 
	$st_info='';$d_info='';
	$st_alok='';$d_alok='';
	$st_margin='';$d_margin='';
	$st_harga='';$d_harga='';
	if ($id==''){
		if (UserAccesForm($user_acces_form,array('87'))=='0'){
			$st_info='readonly';
			$d_info='disabled';
		}
		if (UserAccesForm($user_acces_form,array('88'))=='0'){
			$st_alok='readonly';
			$d_alok='disabled';
		}
		if (UserAccesForm($user_acces_form,array('89'))=='0'){
			$st_margin='readonly';
			$d_margin='disabled';
		}
		if (UserAccesForm($user_acces_form,array('90'))=='0'){
			$st_harga='readonly';
			$d_harga='disabled';
		}
	}else{
		if (UserAccesForm($user_acces_form,array('91'))=='0'){
			$st_info='readonly';
			$d_info='disabled';
			
		}
		if (UserAccesForm($user_acces_form,array('92'))=='0'){
			$st_alok='readonly';
			$d_alok='disabled';
		}
		if (UserAccesForm($user_acces_form,array('93'))=='0'){
			$st_margin='readonly';
			$d_margin='disabled';
		}
		if (UserAccesForm($user_acces_form,array('94'))=='0'){
			$st_harga='readonly';
			$d_harga='disabled';
		}
	}
	

?>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mdata_alkes" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mdata_alkes/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="idkategori">Kategori</label>
				<div class="col-md-7">
					<select name="idkategori" <?=$d_info?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach  ($list_kategori as $row) { ?>
							<option value="<?=$row->id;?>" <?=($idkategori==$row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama</label>
				<div class="col-md-7">
					<input type="text" <?=$st_info?> class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="komposisi">Komposisi</label>
				<div class="col-md-7">
					<textarea  <?=$st_info?>  class="form-control" id="komposisi" placeholder="Komposisi" name="komposisi" required="" aria-required="true">{komposisi}</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="alamat">Indikasi</label>
				<div class="col-md-7">
					<textarea  <?=$st_info?>  class="form-control" id="indikasi" placeholder="Indikasi" name="indikasi" required="" aria-required="true">{indikasi}</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="idsatuanbesar">Satuan Besar</label>
				<div class="col-md-7">
					<select  <?=$d_info?>  name="idsatuanbesar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach  ($list_satuan as $row) { ?>
							<option value="<?=$row->id;?>" <?=($idsatuanbesar==$row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="hargasatuanbesar">Harga Satuan Besar</label>
				<div class="col-md-7">
					<input type="text"  <?=$st_harga?> class="form-control number" id="hargasatuanbesar" placeholder="Harga Satuan Besar" name="hargasatuanbesar" value="{hargasatuanbesar}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="jumlahsatuanbesar">Jumlah Satuan Besar</label>
				<div class="col-md-7">
					<input type="text"  <?=$st_harga?> class="form-control number" id="jumlahsatuanbesar" placeholder="Jumlah Satuan Besar" name="jumlahsatuanbesar" value="{jumlahsatuanbesar}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="idsatuankecil">Satuan Kecil</label>
				<div class="col-md-7">
					<select name="idsatuankecil"  <?=$d_info?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach  ($list_satuan as $row) { ?>
							<option value="<?=$row->id;?>" <?=($idsatuankecil==$row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="hargabeli">Harga Dasar</label>
				<div class="col-md-7">
					<input type="text"  <?=$st_harga?> readonly class="form-control number" id="hargabeli" placeholder="Harga Beli" name="hargabeli" value="{hargabeli}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="ppn">PPN (%)</label>
				<div class="col-md-7">
					<input type="text"  <?=$st_harga?> class="form-control number" id="ppn" placeholder="PPN" name="ppn" value="{ppn}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="hargadasar">Harga Beli</label>
				<div class="col-md-7">
					<input type="text"  <?=$st_harga?> class="form-control number" id="hargadasar" placeholder="Harga Dasar" name="hargadasar" value="{hargadasar}" readonly required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="idrakgudang">Rak Gudang</label>
				<div class="col-md-7">
					<select name="idrakgudang"  <?=$d_info?>  class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach  ($list_rakgudang as $row) { ?>
							<option value="<?=$row->id;?>" <?=($idrakgudang==$row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="idrakfarmasi">Rak Farmasi</label>
				<div class="col-md-7">
					<select name="idrakfarmasi"  <?=$d_info?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php foreach  ($list_rakfarmasi as $row) { ?>
							<option value="<?=$row->id;?>" <?=($idrakfarmasi==$row->id) ? "selected" : "" ?>><?=$row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="pengeculianpenggunaan">Alokasi<br> Penggunaan</label>
				<div class="col-md-7">
					<div class="row">
					<div class="col-xs-12">
						<div class="checkbox">
							<label for="alokasiumum">
							<input <?=$d_alok?> type="checkbox" <?=($alokasiumum == 1 ? 'checked':'')?> id="alokasiumum" name="alokasiumum" value="1"> Umum
							</label>
						</div>
						<div class="checkbox">
							<label for="alokasiasuransi">
							<input  <?=$d_alok?> type="checkbox" <?=($alokasiasuransi == 1 ? 'checked':'')?> id="alokasiasuransi" name="alokasiasuransi" value="1"> Perusahaan Asuransi
							</label>
						</div>
						<div class="checkbox">
							<label for="alokasijasaraharja">
							<input  <?=$d_alok?> type="checkbox" <?=($alokasijasaraharja == 1 ? 'checked':'')?> id="alokasijasaraharja" name="alokasijasaraharja" value="1"> Jasa Raharja
							</label>
						</div>
						<div class="checkbox">
							<label for="alokasibpjskesehatan">
							<input  <?=$d_alok?> type="checkbox" <?=($alokasibpjskesehatan == 1 ? 'checked':'')?> id="alokasibpjskesehatan" name="alokasibpjskesehatan" value="1"> BPJS Kesehatan
							</label>
						</div>
						<div class="checkbox">
							<label for="alokasibpjstenagakerja">
							<input  <?=$d_alok?> type="checkbox" <?=($alokasibpjstenagakerja == 1 ? 'checked':'')?> id="alokasibpjstenagakerja" name="alokasibpjstenagakerja" value="1"> BPJS Ketenagakerjaan
							</label>
						</div>
					</div>
					</div>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-12 control-label"><h5 align="left"><b>Margin (%)</b></h5></label>
				<div class="col-md-12">
					<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="marginumum">Umum</label>
				<div class="col-md-7">
					<input type="text"  <?=$st_margin?>  class="form-control number" id="marginumum" placeholder="Margin Umum" name="marginumum" value="{marginumum}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="marginasuransi">Perusahaan Asuransi</label>
				<div class="col-md-7">
					<input type="text"  <?=$st_margin?> class="form-control number" id="marginasuransi" placeholder="Margin Perusahaan Asuransi" name="marginasuransi" value="{marginasuransi}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="marginjasaraharja">Jasa Raharja</label>
				<div class="col-md-7">
					<input type="text"  <?=$st_margin?> class="form-control number" id="marginjasaraharja" placeholder="Margin Jasa Raharja" name="marginjasaraharja" value="{marginjasaraharja}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="marginbpjskesehatan">BPJS Kesehatan</label>
				<div class="col-md-7">
					<input type="text"  <?=$st_margin?> class="form-control number" id="marginbpjskesehatan" placeholder="Margin BPJS Kesehatan" name="marginbpjskesehatan" value="{marginbpjskesehatan}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="marginbpjstenagakerja">BPJS Ketenagakerjaan</label>
				<div class="col-md-7">
					<input type="text"  <?=$st_margin?> class="form-control number" id="marginbpjstenagakerja" placeholder="Margin BPJS Ketenagakerjaan" name="marginbpjstenagakerja" value="{marginbpjstenagakerja}" required="" aria-required="true">
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-3 control-label" for="stokreorder">Stok Reorder</label>
				<div class="col-md-7">
					<input type="text" class="form-control number"  <?=$st_info?> id="stokreorder" placeholder="Stok Reorder" name="stokreorder" value="{stokreorder}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="stokminimum">Stok Minimum</label>
				<div class="col-md-7">
					<input type="text"  <?=$st_info?> class="form-control number" id="stokminimum" placeholder="Stok Minimum" name="stokminimum" value="{stokminimum}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="catatan">Catatan</label>
				<div class="col-md-7">
					<textarea  <?=$st_info?> class="form-control" id="catatan" placeholder="Catatan" name="catatan">{catatan}</textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" id="btn_simpan" type="submit">Simpan</button>
					<a href="{base_url}mdata_alkes" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_hidden('kode', $kode); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(".number").number(true, 0, '.', ',');
	// $("#ppn").keyup(function(){
		// if($(this).val().replace(/,/g, '') > 100){
			// $(this).val(100);
		// }

		// var hargabeli = parseFloat($("#hargabeli").val().replace(/,/g, ''));
		// var ppn = $(this).val() / 100;
		// $("#hargadasar").val( hargabeli + (hargabeli * ppn) );
	// });
	$("#hargasatuanbesar,#jumlahsatuanbesar,#ppn").keyup(function(){
		set_harga_dasar();
		
	});
	function set_harga_dasar(){
		var hargabesar=$("#hargasatuanbesar").val();
		var jml_besar=$("#jumlahsatuanbesar").val();
		var hargabeli=hargabesar/jml_besar;
		$("#hargabeli").val(hargabeli);
		var ppn=$("#ppn").val();
		
		var hargadasar=hargabeli+(hargabeli*ppn/100);
		$("#hargadasar").val(hargadasar);
		
	}
	$("#marginumum, #marginasuransi, #marginjasaraharja, #marginbpjskesehatan, #marginbpjstenagakerja").keyup(function(){
		if($(this).val().replace(/,/g, '') > 100){
			$(this).val(100);
		}
	});
	$(document.body).on('click', '#btn_simpan' ,function(){
		$('input').removeAttr("disabled")
		$('.js-select2').removeAttr("disabled")
		// alert('Simpan');return false;
		return true;
        // alert($("#st_email").val());
        
    });
</script>
