<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mjenis_gaji" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mjenis_gaji/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Jenis</label>
				<div class="col-md-10">
					<input type="text" readonly class="form-control" id="nama" placeholder="Jenis" name="nama" value="{nama}" required="" aria-required="true">
					<input type="hidden" readonly class="form-control" id="idjenis" placeholder="Jenis" name="idjenis" value="{id}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama"></label>
				<div class="col-md-10">
					<button class="btn btn-sm btn-success" type="button" id="btn_add"><i class="fa fa-plus"></i> Variable</button>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama"></label>
				<div class="col-md-10">
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-responsive" id="data_variable">
						<thead>
							<tr>
								<th>Variable</th>
								<th>No Akun</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				</div>
			</div>
			
			
			<div class="form-group">
				<div class="col-md-9 col-md-offset-2">
					
					<a href="{base_url}mjenis_gaji" class="btn btn-default" type="reset"><i class="pg-close"></i> Kembali</a>
				</div>
			</div>
			<?php echo form_close() ?>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_variable" role="dialog" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Variable Rekapan</h3>
				</div>
				<div class="block-content">
					<div class="form-horizontal">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="" style="margin-top: 5px;">Tipe</label>
							<div class="col-md-8">
								<select id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="0">- Semua Variable -</option>
									<option value="1">Pendapatan</option>
									<option value="2">Potongan</option>
								</select>
							</div>
						</div>
					</div>
					<br>
					<div class="table-responsive">
					<table class="table table-bordered table-striped table-responsive" id="variableData">
						<thead>
							<tr>
								<th>X</th>
								<th>Nama Variable</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button id="addVariable" class="btn btn-sm btn-success" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-check"></i> Proses</button>
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_akun" role="dialog" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">No AKun</h3>
				</div>
				<div class="block-content">					
					<br>
					<input type="hidden" readonly class="form-control" id="idsetting" placeholder="" name="idsetting" value="">
					<div class="table-responsive">
					<table class="table table-bordered table-striped table-responsive" id="table_akun">
						<thead>
							<tr>
								<th>No Akun</th>
								<th>Nama Akun</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		//btn_add
		load_akun();
		get_list_variable();
		

	})	
	$(document).on("click","#btn_add",function(){	
		$("#modal_variable").modal('show');
		getVariableRekapan(0);
	});
	$(document).on("click",".add_akun",function(){	
		$("#idsetting").val($(this).closest('tr').find("td:eq(0)").html());
		$("#modal_akun").modal('show');
		load_akun();
	});
	$(document).on("click",".pilih",function(){	
		var table = $('#table_akun').DataTable();
        tr = table.row( $(this).parents('tr') ).index()

		var idakun=table.cell(tr,0).data();
		var idsetting = $('#idsetting').val();
		$.ajax({
				url: '{site_url}mjenis_gaji/update_akun',
				type: 'POST',
				data: {idsetting: idsetting,idakun:idakun},
				complete: function() {					
					swal({
						title: "Berhasil!",
						text: "Data telah update.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});
					$("#modal_akun").modal('hide');
					get_list_variable();
					// $('#index_list').DataTable().ajax.reload()
				}
			});
		
	});
	$(document).on("click",".hapus_akun",function(){	
		$("#idsetting").val();
		
		var idsetting = $(this).closest('tr').find("td:eq(0)").html();
		$.ajax({
				url: '{site_url}mjenis_gaji/hapus_akun',
				type: 'POST',
				data: {idsetting: idsetting},
				complete: function() {					
					swal({
						title: "Berhasil!",
						text: "Data telah dihapus.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});
					get_list_variable();
					// $('#index_list').DataTable().ajax.reload()
				}
			});
		
	});
	$(document).on("click",".hapus_var",function(){	
		// $("#idsetting").val();
		var idjenis=$("#idjenis").val();
		var idsetting = $(this).closest('tr').find("td:eq(0)").html();
		$.ajax({
				url: '{site_url}mjenis_gaji/hapus_var',
				type: 'POST',
				data: {idsetting: idsetting,idjenis:idjenis},
				complete: function() {					
					swal({
						title: "Berhasil!",
						text: "Data telah dihapus.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});
					get_list_variable();
					// $('#index_list').DataTable().ajax.reload()
				}
			});
		
	});
	$(document).on("change", "#idtipe", function() {
		getVariableRekapan($(this).val());
	});
	function getVariableRekapan(idtipe){

		var idjenis=$("#idjenis").val();
		$.ajax({
			url: '{site_url}mjenis_gaji/getVariableRekapan/' + idjenis +'/'+idtipe,
			method: 'GET',
			success: function(data) {
				$("#variableData tbody").html(data);
			}
		});
	}
	
	function load_akun(){
		$('#table_akun').DataTable().destroy();
		table = $('#table_akun').DataTable({
				autoWidth: false,
				searching: true,
				pageLength: 50,
				serverSide: true,
				"order": [],
				"pageLength": 10,
				"ordering": false,
				columnDefs: [{ "targets": [0], "visible": false },
								{ "width": "80%", "targets": [1] },
								{ "width": "20%", "targets": [2] },
							 ],
				ajax: { 
					url: '{site_url}mjenis_gaji/load_akun', 
					type: "POST" ,
					dataType: 'json',
					data : {
							
						   }
				}
			});
	}
	function get_list_variable(){

		var idjenis=$("#idjenis").val();
		$.ajax({
			url: '{site_url}mjenis_gaji/get_list_variable/' + idjenis,
			method: 'GET',
			success: function(data) {
				$("#data_variable tbody").html(data);
			}
		});
	}
	

	$(document).on("click", "#addVariable", function() {
		var arr_id=[];
		var idjenis=$("#idjenis").val();
		$('#variableData').find('tr').each(function () {
		var row = $(this);
		if (row.find('input[type="checkbox"]').is(':checked')) {
				arr_id.push(row.children('td').eq(1).html());
		}
		});
		 setTimeout(function() {
			$.ajax({
				url: '{site_url}mjenis_gaji/addVariable',
				type: 'POST',
				data: {id: arr_id,idjenis:idjenis},
				complete: function() {					
					swal({
						title: "Berhasil!",
						text: "Data telah ditambahkan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});
					get_list_variable();
					// $('#index_list').DataTable().ajax.reload()
				}
			});
		}, 200);
		// alert(arr_id);
		
	});
</script>