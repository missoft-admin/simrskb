<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<b><span class="label label-success" style="font-size:12px">TIPE PASIEN</span></b>
		<br><br>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_tipe_pasien">
			<thead>
				<tr>
					<th width="80%">Tipe Pasien</th>
					<th width="20%">Aksi</th>
				</tr>
				<tr>
					<th>
						<select class="js-select2 form-control" id="tipe_pasien" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Pasien RS</option>
							<option value="2">Pasien Pribadi</option>
						</select>
					</th>
					<th>
						<button class="btn btn-primary" id="submit_tipe_pasien">Tambahkan</button>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($tipe_pasien as $row) { ?>
					<tr>
						<td hidden><?=$row[0]?></td>
						<td><?=$row[1]?></td>
						<td>
							<div class='btn btn-group'>
								<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		</div>
		<hr>

		<b><span class="label label-success" style="font-size:12px">ASAL PASIEN</span></b>
		<br><br>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_asal_pasien">
			<thead>
				<tr>
					<th width="80%">Asal Pasien</th>
					<th width="20%">Aksi</th>
				</tr>
				<tr>
					<th>
						<select class="js-select2 form-control" id="asal_pasien" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Poliklinik</option>
							<option value="2">Instalasi Gawat Darurat</option>
						</select>
					</th>
					<th>
						<button class="btn btn-primary" id="submit_asal_pasien">Tambahkan</button>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($asal_pasien as $row) { ?>
					<tr>
						<td hidden><?=$row[0]?></td>
						<td><?=$row[1]?></td>
						<td>
							<div class='btn btn-group'>
								<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		</div>
		<hr>

		<b><span class="label label-success" style="font-size:12px">POLIKLINIK</span></b>
		<br><br>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_poliklinik">
			<thead>
				<tr>
					<th width="80%">Poliklinik</th>
					<th width="20%">Aksi</th>
				</tr>
					<tr>
						<th>
							<select class="js-select2 form-control" id="poliklinik" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="">Pilih Opsi</option>
								<?php foreach(get_all('mpoliklinik') as $row) { ?>
									<option value="<?=$row->id?>"><?= $row->nama?></option>
								<?php } ?>
							</select>
						</th>
						<th>
							<button class="btn btn-primary" id="submit_poliklinik">Tambahkan</button>
						</th>
					</tr>
			</thead>
			<tbody>
				<?php foreach ($poliklinik as $row) { ?>
					<tr>
						<td hidden><?=$row[0]?></td>
						<td><?=$row[1]?></td>
						<td>
							<div class='btn btn-group'>
								<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		</div>
		<hr>

		<b><span class="label label-success" style="font-size:12px">TIPE DOKTER</span></b>
		<br><br>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_tipe_dokter">
			<thead>
				<tr>
					<th width="80%">Tipe Dokter</th>
					<th width="20%">Aksi</th>
				</tr>
					<tr>
						<th>
							<select class="js-select2 form-control" id="tipe_dokter" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="">Pilih Opsi</option>
								<?php foreach(get_all('mdokter_kategori') as $row) { ?>
									<option value="<?=$row->id?>"><?= $row->nama?></option>
								<?php } ?>
							</select>
						</th>
						<th>
							<button class="btn btn-primary" id="submit_tipe_dokter">Tambahkan</button>
						</th>
					</tr>
			</thead>
			<tbody>
				<?php foreach ($tipe_dokter as $row) { ?>
					<tr>
						<td hidden><?=$row[0]?></td>
						<td><?=$row[1]?></td>
						<td>
							<div class='btn btn-group'>
								<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		</div>
		<hr>

		<b><span class="label label-success" style="font-size:12px">DOKTER</span></b>
		<br><br>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_dokter">
			<thead>
				<tr>
					<th width="80%">Dokter</th>
					<th width="20%">Aksi</th>
				</tr>
					<tr>
						<th>
							<select class="js-select2 form-control" id="dokter" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="">Pilih Opsi</option>
								<?php foreach(get_all('mdokter') as $row) { ?>
									<option value="<?=$row->id?>"><?= $row->nama?></option>
								<?php } ?>
							</select>
						</th>
						<th>
							<button class="btn btn-primary" id="submit_dokter">Tambahkan</button>
						</th>
					</tr>
			</thead>
			<tbody>
				<?php foreach ($nama_dokter as $row) { ?>
					<tr>
						<td hidden><?=$row[0]?></td>
						<td><?=$row[1]?></td>
						<td>
							<div class='btn btn-group'>
								<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		</div>
		<hr>

		<b><span class="label label-success" style="font-size:12px">OPERASI KE</span></b>
		<br><br>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_operasi_ke">
			<thead>
				<tr>
					<th width="80%">Operasi Ke</th>
					<th width="20%">Aksi</th>
				</tr>
					<tr>
						<th>
							<input type="number" class="form-control" id="operasi_ke" readonly value="">
						</th>
						<th>
							<button class="btn btn-primary" id="submit_operasi">Tambahkan</button>
						</th>
					</tr>
			</thead>
			<tbody>
				<?php foreach ($hitung_operasi as $row) { ?>
					<tr>
						<td hidden><?=$row[0]?></td>
						<td><?=$row[1]?></td>
						<td>
							<div class='btn btn-group'>
								<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		</div>
		<hr>

		<b><span class="label label-success" style="font-size:12px">VISITE KE</span></b>
		<br><br>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_visite_ke">
			<thead>
				<tr>
					<th width="80%">Visite Ke</th>
					<th width="20%">Aksi</th>
				</tr>
					<tr>
						<th>
							<input type="number" class="form-control" id="visite_ke" readonly value="">
						</th>
						<th>
							<button class="btn btn-primary" id="submit_visite">Tambahkan</button>
						</th>
					</tr>
			</thead>
			<tbody>
				<?php foreach ($hitung_visite as $row) { ?>
					<tr>
						<td hidden><?=$row[0]?></td>
						<td><?=$row[1]?></td>
						<td>
							<div class='btn btn-group'>
								<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		</div>
	</div>
</div>

<input type="hidden" class="form-control" id="rowindex">

<script type="text/javascript">
	var lengthOperasi = $('#table_operasi_ke tbody tr').length + 1;
	var lengthVisite = $('#table_visite_ke tbody tr').length + 1;

	$('#operasi_ke').val(lengthOperasi);
	$('#visite_ke').val(lengthVisite);

	$(document).on("click", "#submit_tipe_pasien", function() {
		var rowindex;
		var duplicate = false;

		if ($("#rowindex").val() != '') {
			var content = "";
			rowindex = $("#rowindex").val();
		} else {
			var content = "<tr>";
			$('#table_tipe_pasien tbody tr').filter(function() {
				var $cells = $(this).children('td');
				if ($cells.eq(0).text() === $("#tipe_pasien option:selected").val()) {
					sweetAlert("Maaf...", "Tipe Pasien '" + $("#tipe_pasien option:selected").text() + "' sudah ditambahkan.", "error");
					duplicate = true;
					return false;
				}
			});
		}

		if (duplicate == false) {
			content += "<td style='display: none'>" + $("#tipe_pasien option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#tipe_pasien option:selected").text(); + "</td>";
			content += "<td width='10%'>";
			content += "	<div class='btn btn-group'>";
			content += "		<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
			content += "	</div>";
			content += "</td>";

			if ($("#rowindex").val() != '') {
				$('#table_tipe_pasien tbody tr:eq(' + rowindex + ')').html(content);
			} else {
				content += "</tr>";
				$('#table_tipe_pasien tbody').append(content);
			}

			submitForm();
		}
	});

	$(document).on("click", "#submit_asal_pasien", function() {
		var rowindex;
		var duplicate = false;

		if ($("#rowindex").val() != '') {
			var content = "";
			rowindex = $("#rowindex").val();
		} else {
			var content = "<tr>";
			$('#table_asal_pasien tbody tr').filter(function() {
				var $cells = $(this).children('td');
				if ($cells.eq(0).text() === $("#asal_pasien option:selected").val()) {
					sweetAlert("Maaf...", "Asal Pasien '" + $("#asal_pasien option:selected").text() + "' sudah ditambahkan.", "error");
					duplicate = true;
					return false;
				}
			});
		}

		if (duplicate == false) {
			content += "<td style='display: none'>" + $("#asal_pasien option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#asal_pasien option:selected").text(); + "</td>";
			content += "<td width='10%'>";
			content += "	<div class='btn btn-group'>";
			content += "		<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
			content += "	</div>";
			content += "</td>";

			if ($("#rowindex").val() != '') {
				$('#table_asal_pasien tbody tr:eq(' + rowindex + ')').html(content);
			} else {
				content += "</tr>";
				$('#table_asal_pasien tbody').append(content);
			}

			submitForm();
		}
	});

	$(document).on("click", "#submit_poliklinik", function() {
		var rowindex;
		var duplicate = false;

		if ($("#rowindex").val() != '') {
			var content = "";
			rowindex = $("#rowindex").val();
		} else {
			var content = "<tr>";
			$('#table_poliklinik tbody tr').filter(function() {
				var $cells = $(this).children('td');
				if ($cells.eq(0).text() === $("#poliklinik option:selected").val()) {
					sweetAlert("Maaf...", "Poliklinik '" + $("#poliklinik option:selected").text() + "' sudah ditambahkan.", "error");
					duplicate = true;
					return false;
				}
			});
		}

		if (duplicate == false) {
			content += "<td style='display: none'>" + $("#poliklinik option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#poliklinik option:selected").text(); + "</td>";
			content += "<td width='10%'>";
			content += "	<div class='btn btn-group'>";
			content += "		<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
			content += "	</div>";
			content += "</td>";

			if ($("#rowindex").val() != '') {
				$('#table_poliklinik tbody tr:eq(' + rowindex + ')').html(content);
			} else {
				content += "</tr>";
				$('#table_poliklinik tbody').append(content);
			}

			submitForm();
		}
	});

	$(document).on("click", "#submit_tipe_dokter", function() {
		var rowindex;
		var duplicate = false;

		if ($("#rowindex").val() != '') {
			var content = "";
			rowindex = $("#rowindex").val();
		} else {
			var content = "<tr>";
			$('#table_tipe_dokter tbody tr').filter(function() {
				var $cells = $(this).children('td');
				if ($cells.eq(0).text() === $("#tipe_dokter option:selected").val()) {
					sweetAlert("Maaf...", "Tipe Dokter '" + $("#tipe_dokter option:selected").text() + "' sudah ditambahkan.", "error");
					duplicate = true;
					return false;
				}
			});
		}

		if (duplicate == false) {
			content += "<td style='display: none'>" + $("#tipe_dokter option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#tipe_dokter option:selected").text(); + "</td>";
			content += "<td width='10%'>";
			content += "	<div class='btn btn-group'>";
			content += "		<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
			content += "	</div>";
			content += "</td>";

			if ($("#rowindex").val() != '') {
				$('#table_tipe_dokter tbody tr:eq(' + rowindex + ')').html(content);
			} else {
				content += "</tr>";
				$('#table_tipe_dokter tbody').append(content);
			}

			submitForm();
		}
	});

	$(document).on("click", "#submit_dokter", function() {
		var rowindex;
		var duplicate = false;

		if ($("#rowindex").val() != '') {
			var content = "";
			rowindex = $("#rowindex").val();
		} else {
			var content = "<tr>";
			$('#table_dokter tbody tr').filter(function() {
				var $cells = $(this).children('td');
				if ($cells.eq(0).text() === $("#dokter option:selected").val()) {
					sweetAlert("Maaf...", "Dokter '" + $("#dokter option:selected").text() + "' sudah ditambahkan.", "error");
					duplicate = true;
					return false;
				}
			});
		}

		if (duplicate == false) {
			content += "<td style='display: none'>" + $("#dokter option:selected").val(); + "</td>";
			content += "<td width='10%'>" + $("#dokter option:selected").text(); + "</td>";
			content += "<td width='10%'>";
			content += "	<div class='btn btn-group'>";
			content += "		<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
			content += "	</div>";
			content += "</td>";

			if ($("#rowindex").val() != '') {
				$('#table_dokter tbody tr:eq(' + rowindex + ')').html(content);
			} else {
				content += "</tr>";
				$('#table_dokter tbody').append(content);
			}

			submitForm();
		}
	});

	$(document).on("click", "#submit_operasi", function() {
		var rowindex;
		var duplicate = false;

		if ($("#rowindex").val() != '') {
			var content = "";
			rowindex = $("#rowindex").val();
		} else {
			var content = "<tr>";
			$('#table_operasi_ke tbody tr').filter(function() {
				var $cells = $(this).children('td');
				if ($cells.eq(0).text() === $("#operasi_ke").val()) {
					sweetAlert("Maaf...", "Operasi Ke '" + $("#operasi_ke").val() + "' sudah ditambahkan.", "error");
					duplicate = true;
					return false;
				}
			});
		}

		if (duplicate == false) {
			content += "<td style='display: none'>" + $("#operasi_ke").val(); + "</td>";
			content += "<td width='10%'>" + $("#operasi_ke").val(); + "</td>";
			content += "<td width='10%'>";
			content += "	<div class='btn btn-group'>";
			content += "		<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
			content += "	</div>";
			content += "</td>";

			if ($("#rowindex").val() != '') {
				$('#table_operasi_ke tbody tr:eq(' + rowindex + ')').html(content);
			} else {
				content += "</tr>";
				$('#table_operasi_ke tbody').append(content);
			}

			var lengthOperasi = $('#table_operasi_ke tbody tr').length + 1;
			$('#operasi_ke').val(lengthOperasi);

			submitForm();
		}
	});

	$(document).on("click", "#submit_visite", function() {
		var rowindex;
		var duplicate = false;

		if ($("#rowindex").val() != '') {
			var content = "";
			rowindex = $("#rowindex").val();
		} else {
			var content = "<tr>";
			$('#table_visite_ke tbody tr').filter(function() {
				var $cells = $(this).children('td');
				if ($cells.eq(0).text() === $("#visite_ke").val()) {
					sweetAlert("Maaf...", "Visite Ke '" + $("#visite_ke").val() + "' sudah ditambahkan.", "error");
					duplicate = true;
					return false;
				}
			});
		}

		if (duplicate == false) {
			content += "<td style='display: none'>" + $("#visite_ke").val(); + "</td>";
			content += "<td width='10%'>" + $("#visite_ke").val(); + "</td>";
			content += "<td width='10%'>";
			content += "	<div class='btn btn-group'>";
			content += "		<a href='#' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
			content += "	</div>";
			content += "</td>";

			if ($("#rowindex").val() != '') {
				$('#table_visite_ke tbody tr:eq(' + rowindex + ')').html(content);
			} else {
				content += "</tr>";
				$('#table_visite_ke tbody').append(content);
			}

			var lengthVisite = $('#table_visite_ke tbody tr').length + 1;
			$('#visite_ke').val(lengthVisite);

			submitForm();
		}
	});

	$(document).on("click", ".delete", function() {
		var row = $(this).closest('td').parent();
		swal({
			title: '',
			text: "Apakah anda yakin akan menghapus data ini?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Ya!",
			cancelButtonText: "Batalkan!",
			closeOnConfirm: false,
			closeOnCancel: true
		}).then(function() {
			row.remove();

			var lengthOperasi = $('#table_operasi_ke tbody tr').length + 1;
			$('#operasi_ke').val(lengthOperasi);
			
			var lengthVisite = $('#table_visite_ke tbody tr').length + 1;
			$('#visite_ke').val(lengthVisite);

			return submitForm();
		})
		return false;
	});

	function submitForm() {
		var tipe_pasien_tbl = $('table#table_tipe_pasien tbody tr').get().map(function(row) {
			return $(row).find('td:not(:eq(2))').get().map(function(cell) {
				return $(cell).html();
			});
		});

		var asal_pasien_tbl = $('table#table_asal_pasien tbody tr').get().map(function(row) {
			return $(row).find('td:not(:eq(2))').get().map(function(cell) {
				return $(cell).html();
			});
		});

		var poliklinik_tbl = $('table#table_poliklinik tbody tr').get().map(function(row) {
			return $(row).find('td:not(:eq(2))').get().map(function(cell) {
				return $(cell).html();
			});
		});

		var tipe_dokter_tbl = $('table#table_tipe_dokter tbody tr').get().map(function(row) {
			return $(row).find('td:not(:eq(2))').get().map(function(cell) {
				return $(cell).html();
			});
		});

		var dokter_tbl = $('table#table_dokter tbody tr').get().map(function(row) {
			return $(row).find('td:not(:eq(2))').get().map(function(cell) {
				return $(cell).html();
			});
		});

		var operasi_tbl = $('table#table_operasi_ke tbody tr').get().map(function(row) {
			return $(row).find('td:not(:eq(2))').get().map(function(cell) {
				return $(cell).html();
			});
		});

		var visite_tbl = $('table#table_visite_ke tbody tr').get().map(function(row) {
			return $(row).find('td:not(:eq(2))').get().map(function(cell) {
				return $(cell).html();
			});
		});

		$.ajax({
			url: '{site_url}msetting_feerujukan_dokter/update',
			method: 'POST',
			data: {
				tipe_pasien: JSON.stringify(tipe_pasien_tbl),
				asal_pasien: JSON.stringify(asal_pasien_tbl),
				poliklinik: JSON.stringify(poliklinik_tbl),
				tipe_dokter: JSON.stringify(tipe_dokter_tbl),
				nama_dokter: JSON.stringify(dokter_tbl),
				hitung_operasi: JSON.stringify(operasi_tbl),
				hitung_visite: JSON.stringify(visite_tbl)
			},
			success: function(data) {
				swal({
					title: "Berhasil!",
					text: "Proses penyimpanan data.",
					type: "success",
					timer: 1000,
					showConfirmButton: false
				});
			}
		});
	}
</script>
