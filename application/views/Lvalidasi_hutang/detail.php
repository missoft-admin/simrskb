<style>
table.fixed {
      table-layout: fixed;
      width: 100%;
    }
    table.fixed td {
      overflow: hidden;
    }
</style>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tvalidasi_gudang/save_detail','class="form-horizontal push-10-t"') ?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <div class="col-md-6">
				 <input type="hidden" readonly class="form-control" id="id" placeholder="No. Jurnal" name="id" value="{id}">
				<input type="hidden" readonly class="form-control" id="st_posting" placeholder="No. Jurnal" name="st_posting" value="{st_posting}">
				<input type="hidden" readonly class="form-control" id="disabel" placeholder="No. Jurnal" name="disabel" value="{disabel}">
                
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Kontrabon</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="no_terima" placeholder="No. Penerimaan" name="no_terima" value="{notransaksi}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Distributor</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="distributor" placeholder="No. Jurnal" name="distributor" value="{distributor}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Cara Bayar</label>
					<div class="col-md-8">
                    <h4><?=cara_bayar($st_cara_bayar)?></h4>
                   
					</div>
                </div>
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">No Jurnal</label>
                    <div class="col-md-8">
						<input type="text"  readonly class="form-control" id="nofakturexternal" placeholder="No. Faktur" name="nofakturexternal" value="<?=$nojurnal?>">
					</div>
                </div>
				
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal KBO</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="tanggal_terima" placeholder="No. Faktur" name="tanggal_terima" value="<?=HumanDateShort($tanggal_transaksi)?>">
                    </div>
                </div>
                
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Status Posting</label>
                    <div class="col-md-8">
                       <h2 class="block-title"><?=($st_posting=='1'?text_primary('SUDAH DIPOSTING'):text_default('MENUNGGU DIPOSTING'))?></h2>
                    </div>
                </div>
               
			</div>
            
        </div>
	</div>
	
	<div class="block-content">
		 <h3 class="block-title"><?=text_primary('Ringkasan')?></h3>
		 <hr style="margin-top:10px">
		<table class="table table-bordered table-striped fixed" id="ringkasan_list" style="width: 100%;">
			<thead>
				<tr>
					<th style="width:10%"  class="text-center">NO</th>
					<th style="width:20%"  class="text-center">KETERANGAN</th>
					<th style="width:25%"  class="text-center">NOMINAL</th>
					
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	
</div>
<?php echo form_close(); ?>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
	$("#cover-spin").show();
	load_ringkasan();
   
});

function load_ringkasan(){
	var id=$("#id").val();
	var disabel=$("#disabel").val();
	$('#ringkasan_list tbody').empty();
	$.ajax({
		url: '{site_url}lvalidasi_hutang/load_ringkasan/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel},
		success: function(data) {
			$('#ringkasan_list').append(data.tabel);
			$("#cover-spin").hide();
			
		}
	});
}

</script>