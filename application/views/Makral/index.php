<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1734'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_akral()"><i class="fa fa-bar-chart-o"></i> {title}</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('1734'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?>" id="tab_info">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
						<div class="form-group">
							<label class="col-xs-12" for="satuan_akral">Satuan Akral</label>
							<div class="col-xs-4">
								<div class="input-group">
									<input class="form-control" type="text" id="satuan_akral" name="satuan_akral" value="{satuan_akral}" placeholder="Satuan Akral">
									<span class="input-group-btn">
										<button class="btn btn-success" onclick="update_satuan()" type="button"><i class="fa fa-save"></i>  Update</button>
									</span>
								</div>
								
							</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_akral">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="40%">Range Akral</th>
											<th width="25%">Kategori</th>
											<th width="15%">Warna</th>
											<th width="15%">Action</th>										   
										</tr>
										<tr>
											<th>#<input class="form-control" type="hidden" id="akral_id" name="akral_id" value="" placeholder="id"></th>
											<th>
												<div class="input-group" style="width:100%">
													<input class="form-control decimal" type="text" id="akral_1" name="akral_1" placeholder="<?=$satuan_akral?>" value="" >
													<span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
													
													<input class="form-control decimal" type="text" id="akral_2" name="akral_2" placeholder="<?=$satuan_akral?>" value="" >
													
													
												</div>
											</th>
											<th>
												<input class="form-control" type="text" style="width:100%" id="kategori_akral" name="kategori_akral" placeholder="Kategori" value="" >
											</th>
											<th>
												
												<div class="js-colorpicker input-group colorpicker-element">
													<input class="form-control" type="text" id="warna" name="warna" value="#0000">
													<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
												</div>
											
											</th>
											<th>
												<div class="btn-group">
												<?php if (UserAccesForm($user_acces_form,array('1735'))){ ?>
												<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_akral" name="btn_tambah_akral"><i class="fa fa-save"></i> Simpan</button>
												<?}?>
												<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_akral()"><i class="fa fa-refresh"></i> Clear</button>
												</div>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			
			
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
	$(".decimal").number(true,1,'.',',');	
	load_akral();		
	
})	
function clear_akral(){
	$("#akral_id").val('');
	$("#akral_1").val('');
	$("#akral_2").val('');
	$("#kategori_akral").val('');
	$("#warna").val('#000');
	$("#btn_tambah_akral").html('<i class="fa fa-save"></i> Save');
	
}
function load_akral(){
	$('#index_akral').DataTable().destroy();	
	table = $('#index_akral').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}makral/load_akral', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function update_satuan(){
	let satuan_akral=$("#satuan_akral").val();
	
	if (satuan_akral==''){
		sweetAlert("Maaf...", "Tentukan Ssatuan", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}makral/update_satuan', 
		dataType: "JSON",
		method: "POST",
		data : {
				satuan_akral:satuan_akral,
				
				
			},
		complete: function(data) {
			location.reload();
		}
	});
}
$("#btn_tambah_akral").click(function() {
	let akral_id=$("#akral_id").val();
	let akral_1=$("#akral_1").val();
	let akral_2=$("#akral_2").val();
	let kategori_akral=$("#kategori_akral").val();
	let warna=$("#warna").val();
	
	if (akral_1==''){
		sweetAlert("Maaf...", "Tentukan Range Akral", "error");
		return false;
	}
	if (akral_2==''){
		sweetAlert("Maaf...", "Tentukan Range Akral", "error");
		return false;
	}
	if (kategori_akral==''){
		sweetAlert("Maaf...", "Tentukan Kategori", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}makral/simpan_akral', 
		dataType: "JSON",
		method: "POST",
		data : {
				akral_id:akral_id,
				akral_1:akral_1,
				akral_2:akral_2,
				kategori_akral:kategori_akral,
				warna:warna,
				
			},
		complete: function(data) {
			clear_akral();
			$('#index_akral').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
function edit_akral(akral_id){
	$("#akral_id").val(akral_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}makral/find_akral',
		type: 'POST',
		dataType: "JSON",
		data: {id: akral_id},
		success: function(data) {
			$("#btn_tambah_akral").html('<i class="fa fa-save"></i> Edit');
			$("#akral_id").val(data.id);
			$("#akral_1").val(data.akral_1);
			$("#akral_2").val(data.akral_2);
			$("#warna").val(data.warna).trigger('change');
			$("#kategori_akral").val(data.kategori_akral);
			
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_akral(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Akral?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}makral/hapus_akral',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_akral').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

</script>