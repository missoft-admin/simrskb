
<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<style>
	#chartPemesananGudangHarian {
      width: 100%;
      height: 400px;
    }
	#chartPemesananGudangRekap {
      width: 100%;
      height: 400px;
    }
	#chartPemesananGudangHarianTipeBarang {
      width: 100%;
      height: 400px;
    }
	#chartPenerimaanGudangHarian {
      width: 100%;
      height: 400px;
    }
	#chartPenerimaanGudangRekap {
      width: 100%;
      height: 400px;
    }
	#chartPenerimaanGudangHarianTipeBarang {
      width: 100%;
      height: 400px;
    }
	#chartPenerimaanGudangTahunan {
      width: 100%;
      height: 400px;
    }
	#chartPenerimaanGudangTahunanTipeBarang {
      width: 100%;
      height: 400px;
    }
</style>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<li class="active">
			<a href="#tab_gudang">Gudang</a>
		</li>
		<li class="">
			<a href="#tab_kunjungan">Kunjungan</a>
		</li>
		<li class="">
			<a href="#tab_lainnya">Lain Lain</a>
		</li>
	</ul>
	<div class="block-content tab-content">
		<div class="tab-pane fade fade-left active in" id="tab_gudang">
			<div class="row items-push">
                <?php if (UserAccesForm($user_acces_form, array('2582'))) {?>
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-4">
							<h5 class="text-primary">DATA PEMESANAN</h5>
						</div>
						<div class="col-md-8">
							<div class="row">
								<div class="col-md-4">
									<select name="periode_tanggal_pemesanan" id="periode_tanggal_pemesanan" class="js-select2 form-control periode_filter" data-field="data_pemesanan" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="1" <?= ($periode_tanggal_pemesanan == '1' ? 'selected' : ''); ?>>Hari Ini</option>
										<option value="2" <?= ($periode_tanggal_pemesanan == '2' ? 'selected' : ''); ?>>Bulan Ini</option>
										<option value="3" <?= ($periode_tanggal_pemesanan == '3' ? 'selected' : ''); ?>>Tahun Ini</option>
										<option value="4" <?= ($periode_tanggal_pemesanan == '4' ? 'selected' : ''); ?>>Triwulan 1</option>
										<option value="5" <?= ($periode_tanggal_pemesanan == '5' ? 'selected' : ''); ?>>Triwulan 2</option>
										<option value="6" <?= ($periode_tanggal_pemesanan == '6' ? 'selected' : ''); ?>>Triwulan 3</option>
										<option value="7" <?= ($periode_tanggal_pemesanan == '7' ? 'selected' : ''); ?>>Triwulan 4</option>
										<option value="8" <?= ($periode_tanggal_pemesanan == '8' ? 'selected' : ''); ?>>Semester 1</option>
										<option value="9" <?= ($periode_tanggal_pemesanan == '9' ? 'selected' : ''); ?>>Semester 2</option>
									</select>
								</div>
								<div class="col-md-8">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggal_pemesanan_gudang_dari" name="tanggal_pemesanan_gudang_dari" placeholder="From" value="<?= date('d/m/Y'); ?>"/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_pemesanan_gudang_sampai" name="tanggal_pemesanan_gudang_sampai" placeholder="To" value="<?= date('d/m/Y'); ?>"/>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 push-10-t">
							<a class="block block-link-hover3" href="#">
								<table class="block-table text-center">
									<tbody>
										<tr>
											<td class="bg-primary" style="width: 50%;">
												<div class="push-30 push-30-t">
													<i class="fa fa-shopping-bag fa-3x text-white"></i>
												</div>
											</td>
											<td class="bg-gray-lighter" style="width: 50%;">
												<div class="h1 font-w700"><span class="h2 text-primary">+</span> <span id="jumlah_pemesanan_gudang" class="text-primary">-</span></div>
												<div class="h1 text-primary text-uppercase push-5-t" id="total_nominal_pemesanan_gudang">0</div>
											</td>
										</tr>
									</tbody>
								</table>
							</a>
						</div>
						<div class="col-md-12 push-10-t">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="data_pemesanan_gudang">
									<thead>
										<tr>
											<th width="10%">Nomor Pemesanan</th>
											<th width="10%">Tanggal Pemesanan</th>
											<th width="10%">Nama Distributor</th>
											<th width="10%">Total Item</th>
											<th width="10%">Total Nominal </th>
											<th width="15%">Action</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
                <? } ?>
                <?php if (UserAccesForm($user_acces_form, array('2583'))) {?>
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-4">
							<h5 class="text-primary">DATA PENERIMAAN / PEMBELIAN</h4>
						</div>
						<div class="col-md-8">
							<div class="row">
								<div class="col-md-4">
									<select name="periode_tanggal_penerimaan" id="periode_tanggal_penerimaan" class="js-select2 form-control periode_filter" data-field="data_penerimaan" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="1" <?= ($periode_tanggal_penerimaan == '1' ? 'selected' : ''); ?>>Hari Ini</option>
										<option value="2" <?= ($periode_tanggal_penerimaan == '2' ? 'selected' : ''); ?>>Bulan Ini</option>
										<option value="3" <?= ($periode_tanggal_penerimaan == '3' ? 'selected' : ''); ?>>Tahun Ini</option>
										<option value="4" <?= ($periode_tanggal_penerimaan == '4' ? 'selected' : ''); ?>>Triwulan 1</option>
										<option value="5" <?= ($periode_tanggal_penerimaan == '5' ? 'selected' : ''); ?>>Triwulan 2</option>
										<option value="6" <?= ($periode_tanggal_penerimaan == '6' ? 'selected' : ''); ?>>Triwulan 3</option>
										<option value="7" <?= ($periode_tanggal_penerimaan == '7' ? 'selected' : ''); ?>>Triwulan 4</option>
										<option value="8" <?= ($periode_tanggal_penerimaan == '8' ? 'selected' : ''); ?>>Semester 1</option>
										<option value="9" <?= ($periode_tanggal_penerimaan == '9' ? 'selected' : ''); ?>>Semester 2</option>
									</select>
								</div>
								<div class="col-md-8">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggal_penerimaan_gudang_dari" name="tanggal_penerimaan_gudang_dari" placeholder="From" value="<?= date('d/m/Y'); ?>"/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_penerimaan_gudang_sampai" name="tanggal_penerimaan_gudang_sampai" placeholder="To" value="<?= date('d/m/Y'); ?>"/>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 push-10-t">
							<a class="block block-link-hover3" href="#">
								<table class="block-table text-center">
									<tbody>
										<tr>
											<td class="bg-primary" style="width: 50%;">
												<div class="push-30 push-30-t">
													<i class="fa fa-shopping-bag fa-3x text-white"></i>
												</div>
											</td>
											<td class="bg-gray-lighter" style="width: 50%;">
												<div class="h1 font-w700"><span class="h2 text-primary">+</span> <span id="jumlah_penerimaan_gudang" class="text-primary">-</span></div>
												<div class="h1 text-primary text-uppercase push-5-t" id="total_nominal_penerimaan_gudang">0</div>
											</td>
										</tr>
									</tbody>
								</table>
							</a>
						</div>
						<div class="col-md-12 push-10-t">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="data_penerimaan_gudang">
									<thead>
										<tr>
											<th width="10%">Nomor Penerimaan</th>
											<th width="10%">Tanggal Penerimaan</th>
											<th width="10%">Nama Distributor</th>
											<th width="10%">Total Item</th>
											<th width="10%">Total Nominal </th>
											<th width="15%">Action</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
                <? } ?>
                <?php if (UserAccesForm($user_acces_form, array('2584'))) {?>
				<div class="col-md-6">
					<hr>
					<div class="row">
						<div class="col-md-3">
							<h4 class="text-primary">GRAFIK PEMESANAN HARIAN</h4>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-4">
									<select name="grafik_harian_periode_tanggal_pemesanan" id="grafik_harian_periode_tanggal_pemesanan" class="js-select2 form-control periode_filter" data-field="grafik_pemesanan_harian" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="1" <?= ($grafik_harian_periode_tanggal_pemesanan == '1' ? 'selected' : ''); ?>>Hari Ini</option>
										<option value="2" <?= ($grafik_harian_periode_tanggal_pemesanan == '2' ? 'selected' : ''); ?>>Bulan Ini</option>
										<option value="3" <?= ($grafik_harian_periode_tanggal_pemesanan == '3' ? 'selected' : ''); ?>>Tahun Ini</option>
										<option value="4" <?= ($grafik_harian_periode_tanggal_pemesanan == '4' ? 'selected' : ''); ?>>Triwulan 1</option>
										<option value="5" <?= ($grafik_harian_periode_tanggal_pemesanan == '5' ? 'selected' : ''); ?>>Triwulan 2</option>
										<option value="6" <?= ($grafik_harian_periode_tanggal_pemesanan == '6' ? 'selected' : ''); ?>>Triwulan 3</option>
										<option value="7" <?= ($grafik_harian_periode_tanggal_pemesanan == '7' ? 'selected' : ''); ?>>Triwulan 4</option>
										<option value="8" <?= ($grafik_harian_periode_tanggal_pemesanan == '8' ? 'selected' : ''); ?>>Semester 1</option>
										<option value="9" <?= ($grafik_harian_periode_tanggal_pemesanan == '9' ? 'selected' : ''); ?>>Semester 2</option>
									</select>
								</div>
								<div class="col-md-8">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="grafik_harian_tanggal_pemesanan_gudang_dari" name="grafik_harian_tanggal_pemesanan_gudang_dari" placeholder="From" value="<?= date('d/m/Y'); ?>"/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="grafik_harian_tanggal_pemesanan_gudang_sampai" name="grafik_harian_tanggal_pemesanan_gudang_sampai" placeholder="To" value="<?= date('d/m/Y'); ?>"/>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 push-10-t">
							<canvas id="chartPemesananGudangHarian"></canvas>
						</div>
					</div>
				</div>
                <? } ?>
                <?php if (UserAccesForm($user_acces_form, array('2585'))) {?>
				<div class="col-md-6">
					<hr>
					<div class="row">
						<div class="col-md-3">
							<h4 class="text-primary">GRAFIK PENERIMAAN / PEMBELIAN</h4>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-4">
									<select name="grafik_harian_periode_tanggal_penerimaan" id="grafik_harian_periode_tanggal_penerimaan" class="js-select2 form-control periode_filter" data-field="grafik_penerimaan_harian" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="1" <?= ($grafik_harian_periode_tanggal_penerimaan == '1' ? 'selected' : ''); ?>>Hari Ini</option>
										<option value="2" <?= ($grafik_harian_periode_tanggal_penerimaan == '2' ? 'selected' : ''); ?>>Bulan Ini</option>
										<option value="3" <?= ($grafik_harian_periode_tanggal_penerimaan == '3' ? 'selected' : ''); ?>>Tahun Ini</option>
										<option value="4" <?= ($grafik_harian_periode_tanggal_penerimaan == '4' ? 'selected' : ''); ?>>Triwulan 1</option>
										<option value="5" <?= ($grafik_harian_periode_tanggal_penerimaan == '5' ? 'selected' : ''); ?>>Triwulan 2</option>
										<option value="6" <?= ($grafik_harian_periode_tanggal_penerimaan == '6' ? 'selected' : ''); ?>>Triwulan 3</option>
										<option value="7" <?= ($grafik_harian_periode_tanggal_penerimaan == '7' ? 'selected' : ''); ?>>Triwulan 4</option>
										<option value="8" <?= ($grafik_harian_periode_tanggal_penerimaan == '8' ? 'selected' : ''); ?>>Semester 1</option>
										<option value="9" <?= ($grafik_harian_periode_tanggal_penerimaan == '9' ? 'selected' : ''); ?>>Semester 2</option>
									</select>
								</div>
								<div class="col-md-8">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="grafik_harian_tanggal_penerimaan_gudang_dari" name="grafik_harian_tanggal_penerimaan_gudang_dari" placeholder="From" value="<?= date('d/m/Y'); ?>"/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="grafik_harian_tanggal_penerimaan_gudang_sampai" name="grafik_harian_tanggal_penerimaan_gudang_sampai" placeholder="To" value="<?= date('d/m/Y'); ?>"/>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 push-10-t">
							<canvas id="chartPenerimaanGudangHarian"></canvas>
						</div>
					</div>
				</div>
                <? } ?>
                <?php if (UserAccesForm($user_acces_form, array('2586'))) {?>
				<div class="col-md-6">
					<hr>
					<div class="row">
						<div class="col-md-3">
							<h4 class="text-primary">GRAFIK PEMESANAN REKAP</h4>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-4">
									<select name="grafik_rekap_periode" id="grafik_rekap_periode" class="js-select2 form-control periode_filter" data-field="grafik_pemesanan_rekap" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="1" <?= ($grafik_rekap_periode == '1' ? 'selected' : ''); ?>>Bulanan</option>
										<option value="2" <?= ($grafik_rekap_periode == '2' ? 'selected' : ''); ?>>Semester</option>
										<option value="3" <?= ($grafik_rekap_periode == '3' ? 'selected' : ''); ?>>Triwulan</option>
									</select>
								</div>
								<div class="col-md-8">
									<select name="grafik_rekap_periode_tahun" id="grafik_rekap_periode_tahun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<?php for ($i = date('Y'); $i >= 2020; $i--) {?>
											<option value="<?= $i; ?>"><?= $i; ?></option>
										<?php }?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-xs-12 push-10-t">
							<canvas id="chartPemesananGudangRekap"></canvas>
						</div>
					</div>
				</div>
                <? } ?>
                <?php if (UserAccesForm($user_acces_form, array('2587'))) {?>
				<div class="col-md-6">
					<hr>
					<div class="row">
						<div class="col-md-3">
							<h4 class="text-primary">GRAFIK PENERIMAAN / PEMBELIAN REKAP</h4>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-4">
									<select name="grafik_penerimaan_rekap_periode" id="grafik_penerimaan_rekap_periode" class="js-select2 form-control periode_filter" data-field="grafik_penerimaan_rekap" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="1" <?= ($grafik_penerimaan_rekap_periode == '1' ? 'selected' : ''); ?>>Bulanan</option>
										<option value="2" <?= ($grafik_penerimaan_rekap_periode == '2' ? 'selected' : ''); ?>>Semester</option>
										<option value="3" <?= ($grafik_penerimaan_rekap_periode == '3' ? 'selected' : ''); ?>>Triwulan</option>
									</select>
								</div>
								<div class="col-md-8">
									<select name="grafik_penerimaan_rekap_periode_tahun" id="grafik_penerimaan_rekap_periode_tahun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<?php for ($i = date('Y'); $i >= 2020; $i--) {?>
											<option value="<?= $i; ?>"><?= $i; ?></option>
										<?php }?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-xs-12 push-10-t">
							<canvas id="chartPenerimaanGudangRekap"></canvas>
						</div>
					</div>
				</div>
                <? } ?>
                <?php if (UserAccesForm($user_acces_form, array('2588'))) {?>
				<div class="col-md-6">
					<hr>
					<div class="row">
						<div class="col-md-3">
							<h4 class="text-primary">GRAFIK PEMESANAN HARIAN PER TIPE BARANG</h4>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-4">
									<select name="grafik_harian_tipe_barang_periode_tanggal_pemesanan" id="grafik_harian_tipe_barang_periode_tanggal_pemesanan" class="js-select2 form-control periode_filter" data-field="grafik_pemesanan_harian_per_tipe_barang" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="1" <?= ($grafik_harian_tipe_barang_periode_tanggal_pemesanan == '1' ? 'selected' : ''); ?>>Hari Ini</option>
										<option value="2" <?= ($grafik_harian_tipe_barang_periode_tanggal_pemesanan == '2' ? 'selected' : ''); ?>>Bulan Ini</option>
										<option value="3" <?= ($grafik_harian_tipe_barang_periode_tanggal_pemesanan == '3' ? 'selected' : ''); ?>>Tahun Ini</option>
										<option value="4" <?= ($grafik_harian_tipe_barang_periode_tanggal_pemesanan == '4' ? 'selected' : ''); ?>>Triwulan 1</option>
										<option value="5" <?= ($grafik_harian_tipe_barang_periode_tanggal_pemesanan == '5' ? 'selected' : ''); ?>>Triwulan 2</option>
										<option value="6" <?= ($grafik_harian_tipe_barang_periode_tanggal_pemesanan == '6' ? 'selected' : ''); ?>>Triwulan 3</option>
										<option value="7" <?= ($grafik_harian_tipe_barang_periode_tanggal_pemesanan == '7' ? 'selected' : ''); ?>>Triwulan 4</option>
										<option value="8" <?= ($grafik_harian_tipe_barang_periode_tanggal_pemesanan == '8' ? 'selected' : ''); ?>>Semester 1</option>
										<option value="9" <?= ($grafik_harian_tipe_barang_periode_tanggal_pemesanan == '9' ? 'selected' : ''); ?>>Semester 2</option>
									</select>
								</div>
								<div class="col-md-8">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="grafik_harian_tipe_barang_tanggal_pemesanan_gudang_dari" name="grafik_harian_tipe_barang_tanggal_pemesanan_gudang_dari" placeholder="From" value="<?= date('d/m/Y'); ?>"/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="grafik_harian_tipe_barang_tanggal_pemesanan_gudang_sampai" name="grafik_harian_tipe_barang_tanggal_pemesanan_gudang_sampai" placeholder="To" value="<?= date('d/m/Y'); ?>"/>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 push-10-t">
							<canvas id="chartPemesananGudangHarianTipeBarang"></canvas>
						</div>
					</div>
				</div>
                <? } ?>
                <?php if (UserAccesForm($user_acces_form, array('2589'))) {?>
				<div class="col-md-6">
					<hr>
					<div class="row">
						<div class="col-md-3">
							<h4 class="text-primary">GRAFIK PENERIMAAN / PEMBELIAN HARIAN PER TIPE BARANG</h4>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-4">
									<select name="grafik_harian_tipe_barang_periode_tanggal_penerimaan" id="grafik_harian_tipe_barang_periode_tanggal_penerimaan" class="js-select2 form-control periode_filter" data-field="grafik_penerimaan_harian_per_tipe_barang" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="1" <?= ($grafik_harian_tipe_barang_periode_tanggal_penerimaan == '1' ? 'selected' : ''); ?>>Hari Ini</option>
										<option value="2" <?= ($grafik_harian_tipe_barang_periode_tanggal_penerimaan == '2' ? 'selected' : ''); ?>>Bulan Ini</option>
										<option value="3" <?= ($grafik_harian_tipe_barang_periode_tanggal_penerimaan == '3' ? 'selected' : ''); ?>>Tahun Ini</option>
										<option value="4" <?= ($grafik_harian_tipe_barang_periode_tanggal_penerimaan == '4' ? 'selected' : ''); ?>>Triwulan 1</option>
										<option value="5" <?= ($grafik_harian_tipe_barang_periode_tanggal_penerimaan == '5' ? 'selected' : ''); ?>>Triwulan 2</option>
										<option value="6" <?= ($grafik_harian_tipe_barang_periode_tanggal_penerimaan == '6' ? 'selected' : ''); ?>>Triwulan 3</option>
										<option value="7" <?= ($grafik_harian_tipe_barang_periode_tanggal_penerimaan == '7' ? 'selected' : ''); ?>>Triwulan 4</option>
										<option value="8" <?= ($grafik_harian_tipe_barang_periode_tanggal_penerimaan == '8' ? 'selected' : ''); ?>>Semester 1</option>
										<option value="9" <?= ($grafik_harian_tipe_barang_periode_tanggal_penerimaan == '9' ? 'selected' : ''); ?>>Semester 2</option>
									</select>
								</div>
								<div class="col-md-8">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="grafik_harian_tipe_barang_tanggal_penerimaan_gudang_dari" name="grafik_harian_tipe_barang_tanggal_penerimaan_gudang_dari" placeholder="From" value="<?= date('d/m/Y'); ?>"/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="grafik_harian_tipe_barang_tanggal_penerimaan_gudang_sampai" name="grafik_harian_tipe_barang_tanggal_penerimaan_gudang_sampai" placeholder="To" value="<?= date('d/m/Y'); ?>"/>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 push-10-t">
							<canvas id="chartPenerimaanGudangHarianTipeBarang"></canvas>
						</div>
					</div>
				</div>
                <? } ?>
                <?php if (UserAccesForm($user_acces_form, array('2590'))) {?>
				<div class="col-md-6">
					<hr>
					<div class="row">
						<div class="col-md-3">
							<h4 class="text-primary">GRAFIK PEMBELIAN TAHUNAN</h4>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-12">
									<select name="grafik_tahunan_periode_tanggal_penerimaan" id="grafik_tahunan_periode_tanggal_penerimaan" class="js-select2 form-control periode_filter" data-field="grafik_tahunan_periode_tanggal_penerimaan" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
										<?php for ($x = 2020; $x <=  date('Y'); $x++) { ?>
											<option value="<?= $x; ?>" <?= (in_array($x, $grafik_tahunan_periode_tanggal_penerimaan) ? 'selected' : ''); ?>><?= $x ?></option>									
										<? } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-xs-12 push-10-t">
							<canvas id="chartPenerimaanGudangTahunan"></canvas>
						</div>
					</div>
				</div>
                <? } ?>
                <?php if (UserAccesForm($user_acces_form, array('2591'))) {?>
				<div class="col-md-6">
					<hr>
					<div class="row">
						<div class="col-md-3">
							<h4 class="text-primary">GRAFIK PEMBELIAN PER TIPE BARANG TAHUNAN</h4>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-12">
									<select name="grafik_tahunan_tipe_barang_periode_tanggal_penerimaan" id="grafik_tahunan_tipe_barang_periode_tanggal_penerimaan" class="js-select2 form-control periode_filter" data-field="grafik_tahunan_tipe_barang_periode_tanggal_penerimaan" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
										<?php for ($x = 2020; $x <=  date('Y'); $x++) { ?>
											<option value="<?= $x; ?>" <?= (in_array($x, $grafik_tahunan_tipe_barang_periode_tanggal_penerimaan) ? 'selected' : ''); ?>><?= $x ?></option>									
										<? } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-xs-12 push-10-t">
							<canvas id="chartPenerimaanGudangTahunanTipeBarang"></canvas>
						</div>
					</div>
				</div>
                <? } ?>
			</div>
		</div>
		<div class="tab-pane fade fade-left active in" id="tab_kunjungan">
		</div>
		<div class="tab-pane fade fade-left active in" id="tab_lainnya">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-primary"><?= $setting_top_pembelian_obat_label; ?></h4>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive push-10-t">
                            <table class="table table-bordered table-striped" id="data_top_pembelian_obat">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="10%">Nama Barang</th>
                                        <th width="10%">Qty</th>
                                        <th width="10%">Nominal</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <i style="color: red; font-size: 14px;">PERIODE : <span id="label_periode_top_pembelian_obat">01-08-2024 S/D 30-08-2024</span></i>
                    </div>
                    <div class="col-md-6">
                        <button data-toggle="modal" data-target="#modal-periode-top-pembelian-obat" style="float: right;"><i class="fa fa-cog"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-primary"><?= $setting_top_pembelian_alkes_label; ?></h4>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive push-10-t">
                            <table class="table table-bordered table-striped" id="data_top_pembelian_alkes">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="10%">Nama Barang</th>
                                        <th width="10%">Qty</th>
                                        <th width="10%">Nominal</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <i style="color: red; font-size: 14px;">PERIODE : <span id="label_periode_top_pembelian_alkes">01-08-2024 S/D 30-08-2024</span></i>
                    </div>
                    <div class="col-md-6">
                        <button data-toggle="modal" data-target="#modal-periode-top-pembelian-alkes" style="float: right;"><i class="fa fa-cog"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-primary"><?= $setting_top_pembelian_implan_label; ?></h4>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive push-10-t">
                            <table class="table table-bordered table-striped" id="data_top_pembelian_implan">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="10%">Nama Barang</th>
                                        <th width="10%">Qty</th>
                                        <th width="10%">Nominal</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <i style="color: red; font-size: 14px;">PERIODE : <span id="label_periode_top_pembelian_implan">01-08-2024 S/D 30-08-2024</span></i>
                    </div>
                    <div class="col-md-6">
                        <button data-toggle="modal" data-target="#modal-periode-top-pembelian-implan" style="float: right;"><i class="fa fa-cog"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-primary"><?= $setting_top_pembelian_logistik_label; ?></h4>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive push-10-t">
                            <table class="table table-bordered table-striped" id="data_top_pembelian_logistik">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="10%">Nama Barang</th>
                                        <th width="10%">Qty</th>
                                        <th width="10%">Nominal</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <i style="color: red; font-size: 14px;">PERIODE : <span id="label_periode_top_pembelian_logistik">01-08-2024 S/D 30-08-2024</span></i>
                    </div>
                    <div class="col-md-6">
                        <button data-toggle="modal" data-target="#modal-periode-top-pembelian-logistik" style="float: right;"><i class="fa fa-cog"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="text-primary"><?= $setting_top_pembelian_distributor_label; ?></h4>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive push-10-t">
                            <table class="table table-bordered table-striped" id="data_top_pembelian_distributor">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="10%">Nama Distributor</th>
                                        <th width="10%">Nominal</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <i style="color: red; font-size: 14px;">PERIODE : <span id="label_periode_top_pembelian_distributor">01-08-2024 S/D 30-08-2024</span></i>
                    </div>
                    <div class="col-md-6">
                        <button data-toggle="modal" data-target="#modal-periode-top-pembelian-distributor" style="float: right;"><i class="fa fa-cog"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <h4 class="text-primary">DATA STOKOPNAME</h4>
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-4">
                                        <select id="unit_pelayanan_stokopname" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                            <?php foreach($unit_pelayanan_stokopname as $row) { ?>
                                            <option value="<?= $row->id; ?>"><?= $row->nama; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select id="periode_stokopname" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                            <?php foreach($periode_stokopname as $row) { ?>
                                            <option value="<?= $row->periode; ?>"><?= get_bulan(substr($row->periode,4,2)).' '.substr($row->periode,0,4); ?></option>	
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select id="nomor_stokopname" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive push-10-t">
                            <table class="table table-bordered table-striped" id="data_stokopname">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="10%">Nama Barang</th>
                                        <th width="10%">Tipe</th>
                                        <th width="10%">Stok Sistem</th>
                                        <th width="10%">Stok Fisik</th>
                                        <th width="10%">Selisih</th>
                                        <th width="10%">Catatan</th>
                                        <th width="10%">HPP</th>
                                        <th width="10%">Nilai</th>
                                        <th width="10%">Nilai Selisih</th>
                                        <th width="10%">Update By</th>
                                        <th width="10%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
    <br><br>
</div>

<!-- Modal Setting Periode Top Pembelian Alkes -->
<div class="modal fade" id="modal-periode-top-pembelian-alkes" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="block-options">
                    <li>
                        <button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
                    </li>
                </ul>
                <h3 class="block-title">Setting</h3>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="">Periode</label>
                        <div class="row">
                            <div class="col-md-4">
                                <select id="periode_tanggal_top_pembelian_alkes" class="js-select2 form-control periode_filter" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="1" <?= ($setting_top_pembelian_alkes_periode == '1' ? 'selected' : ''); ?>>Hari Ini</option>
                                    <option value="2" <?= ($setting_top_pembelian_alkes_periode == '2' ? 'selected' : ''); ?>>Bulan Ini</option>
                                    <option value="3" <?= ($setting_top_pembelian_alkes_periode == '3' ? 'selected' : ''); ?>>Tahun Ini</option>
                                    <option value="4" <?= ($setting_top_pembelian_alkes_periode == '4' ? 'selected' : ''); ?>>Triwulan 1</option>
                                    <option value="5" <?= ($setting_top_pembelian_alkes_periode == '5' ? 'selected' : ''); ?>>Triwulan 2</option>
                                    <option value="6" <?= ($setting_top_pembelian_alkes_periode == '6' ? 'selected' : ''); ?>>Triwulan 3</option>
                                    <option value="7" <?= ($setting_top_pembelian_alkes_periode == '7' ? 'selected' : ''); ?>>Triwulan 4</option>
                                    <option value="8" <?= ($setting_top_pembelian_alkes_periode == '8' ? 'selected' : ''); ?>>Semester 1</option>
                                    <option value="9" <?= ($setting_top_pembelian_alkes_periode == '9' ? 'selected' : ''); ?>>Semester 2</option>
                                </select>
                            </div>
                            <div class="col-md-8">
                                <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                    <input class="form-control" type="text" id="tanggal_top_pembelian_alkes_dari" name="tanggal_top_pembelian_alkes_dari" placeholder="From" value="<?= date('d/m/Y'); ?>"/>
                                    <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                    <input class="form-control" type="text" id="tanggal_top_pembelian_alkes_sampai" name="tanggal_top_pembelian_alkes_sampai" placeholder="To" value="<?= date('d/m/Y'); ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Label</label>
                        <input type="text" class="form-control" id="label_top_pembelian_alkes" value="<?= $setting_top_pembelian_alkes_label; ?>">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Showing</label>
                                <select class="js-select2 form-control" id="show_top_pembelian_alkes" style="width: 100%;">
                                    <option value="10" <?= ($setting_top_pembelian_alkes_showing == '10' ? 'selected' : ''); ?>>10</option>
                                    <option value="25" <?= ($setting_top_pembelian_alkes_showing == '25' ? 'selected' : ''); ?>>25</option>
                                    <option value="50" <?= ($setting_top_pembelian_alkes_showing == '50' ? 'selected' : ''); ?>>50</option>
                                    <option value="100" <?= ($setting_top_pembelian_alkes_showing == '100' ? 'selected' : ''); ?>>100</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Order By</label>
                                <select class="js-select2 form-control" multiple id="order_by_top_pembelian_alkes" style="width: 100%;">
                                    <option value="nama_barang" <?= (in_array("nama_barang", explode(',', $setting_top_pembelian_alkes_order)) ? 'selected' : ''); ?>>Nama Barang</option>
                                    <option value="kuantitas"<?= (in_array("kuantitas", explode(',', $setting_top_pembelian_alkes_order)) ? 'selected' : ''); ?>>Qty</option>
                                    <option value="nominal"<?= (in_array("nominal", explode(',', $setting_top_pembelian_alkes_order)) ? 'selected' : ''); ?>>Nominal</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="filter-top-pembelian-alkes" class="btn btn-sm btn-success" type="button" data-dismiss="modal">Simpan</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Setting Periode Top Pembelian Obat -->
<div class="modal fade" id="modal-periode-top-pembelian-obat" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="block-options">
                    <li>
                        <button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
                    </li>
                </ul>
                <h3 class="block-title">Setting</h3>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="">Periode</label>
                        <div class="row">
                            <div class="col-md-4">
                                <select id="periode_tanggal_top_pembelian_obat" class="js-select2 form-control periode_filter" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="1" <?= ($setting_top_pembelian_obat_periode == '1' ? 'selected' : ''); ?>>Hari Ini</option>
                                    <option value="2" <?= ($setting_top_pembelian_obat_periode == '2' ? 'selected' : ''); ?>>Bulan Ini</option>
                                    <option value="3" <?= ($setting_top_pembelian_obat_periode == '3' ? 'selected' : ''); ?>>Tahun Ini</option>
                                    <option value="4" <?= ($setting_top_pembelian_obat_periode == '4' ? 'selected' : ''); ?>>Triwulan 1</option>
                                    <option value="5" <?= ($setting_top_pembelian_obat_periode == '5' ? 'selected' : ''); ?>>Triwulan 2</option>
                                    <option value="6" <?= ($setting_top_pembelian_obat_periode == '6' ? 'selected' : ''); ?>>Triwulan 3</option>
                                    <option value="7" <?= ($setting_top_pembelian_obat_periode == '7' ? 'selected' : ''); ?>>Triwulan 4</option>
                                    <option value="8" <?= ($setting_top_pembelian_obat_periode == '8' ? 'selected' : ''); ?>>Semester 1</option>
                                    <option value="9" <?= ($setting_top_pembelian_obat_periode == '9' ? 'selected' : ''); ?>>Semester 2</option>
                                </select>
                            </div>
                            <div class="col-md-8">
                                <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                    <input class="form-control" type="text" id="tanggal_top_pembelian_obat_dari" name="tanggal_top_pembelian_obat_dari" placeholder="From" value="<?= date('d/m/Y'); ?>"/>
                                    <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                    <input class="form-control" type="text" id="tanggal_top_pembelian_obat_sampai" name="tanggal_top_pembelian_obat_sampai" placeholder="To" value="<?= date('d/m/Y'); ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Label</label>
                        <input type="text" class="form-control" id="label_top_pembelian_obat" value="<?= $setting_top_pembelian_obat_label; ?>">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Showing</label>
                                <select class="js-select2 form-control" id="show_top_pembelian_obat" style="width: 100%;">
                                    <option value="10" <?= ($setting_top_pembelian_obat_showing == '10' ? 'selected' : ''); ?>>10</option>
                                    <option value="25" <?= ($setting_top_pembelian_obat_showing == '25' ? 'selected' : ''); ?>>25</option>
                                    <option value="50" <?= ($setting_top_pembelian_obat_showing == '50' ? 'selected' : ''); ?>>50</option>
                                    <option value="100" <?= ($setting_top_pembelian_obat_showing == '100' ? 'selected' : ''); ?>>100</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Order By</label>
                                <select class="js-select2 form-control" multiple id="order_by_top_pembelian_obat" style="width: 100%;">
                                    <option value="nama_barang" <?= (in_array("nama_barang", explode(',', $setting_top_pembelian_obat_order)) ? 'selected' : ''); ?>>Nama Barang</option>
                                    <option value="kuantitas"<?= (in_array("kuantitas", explode(',', $setting_top_pembelian_obat_order)) ? 'selected' : ''); ?>>Qty</option>
                                    <option value="nominal"<?= (in_array("nominal", explode(',', $setting_top_pembelian_obat_order)) ? 'selected' : ''); ?>>Nominal</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="filter-top-pembelian-obat" class="btn btn-sm btn-success" type="button" data-dismiss="modal">Simpan</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Setting Periode Top Pembelian Implan -->
<div class="modal fade" id="modal-periode-top-pembelian-implan" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="block-options">
                    <li>
                        <button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
                    </li>
                </ul>
                <h3 class="block-title">Setting</h3>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="">Periode</label>
                        <div class="row">
                            <div class="col-md-4">
                                <select id="periode_tanggal_top_pembelian_implan" class="js-select2 form-control periode_filter" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="1" <?= ($setting_top_pembelian_implan_periode == '1' ? 'selected' : ''); ?>>Hari Ini</option>
                                    <option value="2" <?= ($setting_top_pembelian_implan_periode == '2' ? 'selected' : ''); ?>>Bulan Ini</option>
                                    <option value="3" <?= ($setting_top_pembelian_implan_periode == '3' ? 'selected' : ''); ?>>Tahun Ini</option>
                                    <option value="4" <?= ($setting_top_pembelian_implan_periode == '4' ? 'selected' : ''); ?>>Triwulan 1</option>
                                    <option value="5" <?= ($setting_top_pembelian_implan_periode == '5' ? 'selected' : ''); ?>>Triwulan 2</option>
                                    <option value="6" <?= ($setting_top_pembelian_implan_periode == '6' ? 'selected' : ''); ?>>Triwulan 3</option>
                                    <option value="7" <?= ($setting_top_pembelian_implan_periode == '7' ? 'selected' : ''); ?>>Triwulan 4</option>
                                    <option value="8" <?= ($setting_top_pembelian_implan_periode == '8' ? 'selected' : ''); ?>>Semester 1</option>
                                    <option value="9" <?= ($setting_top_pembelian_implan_periode == '9' ? 'selected' : ''); ?>>Semester 2</option>
                                </select>
                            </div>
                            <div class="col-md-8">
                                <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                    <input class="form-control" type="text" id="tanggal_top_pembelian_implan_dari" name="tanggal_top_pembelian_implan_dari" placeholder="From" value="<?= date('d/m/Y'); ?>"/>
                                    <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                    <input class="form-control" type="text" id="tanggal_top_pembelian_implan_sampai" name="tanggal_top_pembelian_implan_sampai" placeholder="To" value="<?= date('d/m/Y'); ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Label</label>
                        <input type="text" class="form-control" id="label_top_pembelian_implan" value="<?= $setting_top_pembelian_implan_label; ?>">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Showing</label>
                                <select class="js-select2 form-control" id="show_top_pembelian_implan" style="width: 100%;">
                                    <option value="10" <?= ($setting_top_pembelian_implan_showing == '10' ? 'selected' : ''); ?>>10</option>
                                    <option value="25" <?= ($setting_top_pembelian_implan_showing == '25' ? 'selected' : ''); ?>>25</option>
                                    <option value="50" <?= ($setting_top_pembelian_implan_showing == '50' ? 'selected' : ''); ?>>50</option>
                                    <option value="100" <?= ($setting_top_pembelian_implan_showing == '100' ? 'selected' : ''); ?>>100</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Order By</label>
                                <select class="js-select2 form-control" multiple id="order_by_top_pembelian_implan" style="width: 100%;">
                                    <option value="nama_barang" <?= (in_array("nama_barang", explode(',', $setting_top_pembelian_implan_order)) ? 'selected' : ''); ?>>Nama Barang</option>
                                    <option value="kuantitas"<?= (in_array("kuantitas", explode(',', $setting_top_pembelian_implan_order)) ? 'selected' : ''); ?>>Qty</option>
                                    <option value="nominal"<?= (in_array("nominal", explode(',', $setting_top_pembelian_implan_order)) ? 'selected' : ''); ?>>Nominal</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="filter-top-pembelian-implan" class="btn btn-sm btn-success" type="button" data-dismiss="modal">Simpan</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Setting Periode Top Pembelian Logistik -->
<div class="modal fade" id="modal-periode-top-pembelian-logistik" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="block-options">
                    <li>
                        <button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
                    </li>
                </ul>
                <h3 class="block-title">Setting</h3>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="">Periode</label>
                        <div class="row">
                            <div class="col-md-4">
                                <select id="periode_tanggal_top_pembelian_logistik" class="js-select2 form-control periode_filter" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="1" <?= ($setting_top_pembelian_logistik_periode == '1' ? 'selected' : ''); ?>>Hari Ini</option>
                                    <option value="2" <?= ($setting_top_pembelian_logistik_periode == '2' ? 'selected' : ''); ?>>Bulan Ini</option>
                                    <option value="3" <?= ($setting_top_pembelian_logistik_periode == '3' ? 'selected' : ''); ?>>Tahun Ini</option>
                                    <option value="4" <?= ($setting_top_pembelian_logistik_periode == '4' ? 'selected' : ''); ?>>Triwulan 1</option>
                                    <option value="5" <?= ($setting_top_pembelian_logistik_periode == '5' ? 'selected' : ''); ?>>Triwulan 2</option>
                                    <option value="6" <?= ($setting_top_pembelian_logistik_periode == '6' ? 'selected' : ''); ?>>Triwulan 3</option>
                                    <option value="7" <?= ($setting_top_pembelian_logistik_periode == '7' ? 'selected' : ''); ?>>Triwulan 4</option>
                                    <option value="8" <?= ($setting_top_pembelian_logistik_periode == '8' ? 'selected' : ''); ?>>Semester 1</option>
                                    <option value="9" <?= ($setting_top_pembelian_logistik_periode == '9' ? 'selected' : ''); ?>>Semester 2</option>
                                </select>
                            </div>
                            <div class="col-md-8">
                                <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                    <input class="form-control" type="text" id="tanggal_top_pembelian_logistik_dari" name="tanggal_top_pembelian_logistik_dari" placeholder="From" value="<?= date('d/m/Y'); ?>"/>
                                    <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                    <input class="form-control" type="text" id="tanggal_top_pembelian_logistik_sampai" name="tanggal_top_pembelian_logistik_sampai" placeholder="To" value="<?= date('d/m/Y'); ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Label</label>
                        <input type="text" class="form-control" id="label_top_pembelian_logistik" value="<?= $setting_top_pembelian_logistik_label; ?>">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Showing</label>
                                <select class="js-select2 form-control" id="show_top_pembelian_logistik" style="width: 100%;">
                                    <option value="10" <?= ($setting_top_pembelian_logistik_showing == '10' ? 'selected' : ''); ?>>10</option>
                                    <option value="25" <?= ($setting_top_pembelian_logistik_showing == '25' ? 'selected' : ''); ?>>25</option>
                                    <option value="50" <?= ($setting_top_pembelian_logistik_showing == '50' ? 'selected' : ''); ?>>50</option>
                                    <option value="100" <?= ($setting_top_pembelian_logistik_showing == '100' ? 'selected' : ''); ?>>100</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Order By</label>
                                <select class="js-select2 form-control" multiple id="order_by_top_pembelian_logistik" style="width: 100%;">
                                    <option value="nama_barang" <?= (in_array("nama_barang", explode(',', $setting_top_pembelian_logistik_order)) ? 'selected' : ''); ?>>Nama Barang</option>
                                    <option value="kuantitas"<?= (in_array("kuantitas", explode(',', $setting_top_pembelian_logistik_order)) ? 'selected' : ''); ?>>Qty</option>
                                    <option value="nominal"<?= (in_array("nominal", explode(',', $setting_top_pembelian_logistik_order)) ? 'selected' : ''); ?>>Nominal</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="filter-top-pembelian-logistik" class="btn btn-sm btn-success" type="button" data-dismiss="modal">Simpan</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Setting Periode Top Pembelian Distributor -->
<div class="modal fade" id="modal-periode-top-pembelian-distributor" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="block-options">
                    <li>
                        <button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
                    </li>
                </ul>
                <h3 class="block-title">Setting</h3>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="">Periode</label>
                        <div class="row">
                            <div class="col-md-4">
                                <select id="periode_tanggal_top_pembelian_distributor" class="js-select2 form-control periode_filter" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="1" <?= ($setting_top_pembelian_distributor_periode == '1' ? 'selected' : ''); ?>>Hari Ini</option>
                                    <option value="2" <?= ($setting_top_pembelian_distributor_periode == '2' ? 'selected' : ''); ?>>Bulan Ini</option>
                                    <option value="3" <?= ($setting_top_pembelian_distributor_periode == '3' ? 'selected' : ''); ?>>Tahun Ini</option>
                                    <option value="4" <?= ($setting_top_pembelian_distributor_periode == '4' ? 'selected' : ''); ?>>Triwulan 1</option>
                                    <option value="5" <?= ($setting_top_pembelian_distributor_periode == '5' ? 'selected' : ''); ?>>Triwulan 2</option>
                                    <option value="6" <?= ($setting_top_pembelian_distributor_periode == '6' ? 'selected' : ''); ?>>Triwulan 3</option>
                                    <option value="7" <?= ($setting_top_pembelian_distributor_periode == '7' ? 'selected' : ''); ?>>Triwulan 4</option>
                                    <option value="8" <?= ($setting_top_pembelian_distributor_periode == '8' ? 'selected' : ''); ?>>Semester 1</option>
                                    <option value="9" <?= ($setting_top_pembelian_distributor_periode == '9' ? 'selected' : ''); ?>>Semester 2</option>
                                </select>
                            </div>
                            <div class="col-md-8">
                                <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                    <input class="form-control" type="text" id="tanggal_top_pembelian_distributor_dari" name="tanggal_top_pembelian_distributor_dari" placeholder="From" value="<?= date('d/m/Y'); ?>"/>
                                    <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                    <input class="form-control" type="text" id="tanggal_top_pembelian_distributor_sampai" name="tanggal_top_pembelian_distributor_sampai" placeholder="To" value="<?= date('d/m/Y'); ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Label</label>
                        <input type="text" class="form-control" id="label_top_pembelian_distributor" value="<?= $setting_top_pembelian_distributor_label; ?>">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Showing</label>
                                <select class="js-select2 form-control" id="show_top_pembelian_distributor" style="width: 100%;">
                                    <option value="10" <?= ($setting_top_pembelian_distributor_showing == '10' ? 'selected' : ''); ?>>10</option>
                                    <option value="25" <?= ($setting_top_pembelian_distributor_showing == '25' ? 'selected' : ''); ?>>25</option>
                                    <option value="50" <?= ($setting_top_pembelian_distributor_showing == '50' ? 'selected' : ''); ?>>50</option>
                                    <option value="100" <?= ($setting_top_pembelian_distributor_showing == '100' ? 'selected' : ''); ?>>100</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Order By</label>
                                <select class="js-select2 form-control" multiple id="order_by_top_pembelian_distributor" style="width: 100%;">
                                    <option value="nama_barang" <?= (in_array("nama_barang", explode(',', $setting_top_pembelian_distributor_order)) ? 'selected' : ''); ?>>Nama Barang</option>
                                    <option value="kuantitas"<?= (in_array("kuantitas", explode(',', $setting_top_pembelian_distributor_order)) ? 'selected' : ''); ?>>Qty</option>
                                    <option value="nominal"<?= (in_array("nominal", explode(',', $setting_top_pembelian_distributor_order)) ? 'selected' : ''); ?>>Nominal</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="filter-top-pembelian-distributor" class="btn btn-sm btn-success" type="button" data-dismiss="modal">Simpan</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
let tablePemesanan;
let tablePenerimaan;
let chartPemesananGudangHarian;
let chartPenerimaanGudangHarian;
let chartPemesananGudangRekap;
let chartPenerimaanGudangRekap;
let chartPemesananGudangHarianTipeBarang;
let chartPenerimaanGudangHarianTipeBarang;
let chartPenerimaanGudangTahunan;
let chartPenerimaanGudangTahunanTipeBarang;

$(document).ready(function () {
    // Tandai elemen yang akan di-trigger secara programatis
    $(".periode_filter").each(function () {
        $(this).data("triggeredOnLoad", true); // Tambahkan flag
    });

    // Trigger perubahan
    $('#periode_tanggal_pemesanan').trigger('change');
    $('#periode_tanggal_penerimaan').trigger('change');
    $('#grafik_harian_periode_tanggal_pemesanan').trigger('change');
    $('#grafik_harian_periode_tanggal_penerimaan').trigger('change');
    $('#grafik_rekap_periode').trigger('change');
    $('#grafik_penerimaan_rekap_periode').trigger('change');
    $('#grafik_harian_tipe_barang_periode_tanggal_pemesanan').trigger('change');
    $('#grafik_harian_tipe_barang_periode_tanggal_penerimaan').trigger('change');
    $('#grafik_tahunan_periode_tanggal_penerimaan').trigger('change');
    $('#grafik_tahunan_tipe_barang_periode_tanggal_penerimaan').trigger('change');
    $('#unit_pelayanan_stokopname').trigger('change');
    $('#nomor_stokopname').trigger('change');

    // Hapus flag setelah onload selesai
    $(".periode_filter").each(function () {
        $(this).data("triggeredOnLoad", false);
    });

    // Event listener untuk perubahan
    $(".periode_filter").change(function () {
        // Abaikan jika event dipicu secara programatis saat onload
        if ($(this).data("triggeredOnLoad")) {
            return;
        }

        // Jalankan logika perubahan
        const field = $(this).data('field');
        const value = $(this).val();

        swal({
            title: "Notifikasi",
            text: "Apakah Data ini akan dijadikan default dalam dashboard?",
            type: "success",
            showCancelButton: true,
            confirmButtonText: "Ya",
            confirmButtonColor: "#34a263",
            cancelButtonText: "Tidak",
        }).then(function () {
            $.ajax({
                url: '{site_url}executive_dashboard/update_periode_filter',
                method: "POST",
                dataType: "json",
                data: {
                    field: field,
                    value: value,
                },
                success: function (data) {
                    console.log(data);
                }
            });
        });
    });
});

$("#periode_tanggal_pemesanan").change(function(){
	const periode_tanggal = $("#periode_tanggal_pemesanan").val();

	$.ajax({
		url: '{site_url}lsurvey/list_tipe/' + periode_tanggal,
		dataType: "json",
		success: function(data) {
			$("#tanggal_pemesanan_gudang_dari").val(data.tanggal_1);
			$("#tanggal_pemesanan_gudang_sampai").val(data.tanggal_2);
			loadDataPemesananGudang();
		}
	});
});

$("#periode_tanggal_penerimaan").change(function(){
	const periode_tanggal = $("#periode_tanggal_penerimaan").val();

	$.ajax({
		url: '{site_url}lsurvey/list_tipe/' + periode_tanggal,
		dataType: "json",
		success: function(data) {
			$("#tanggal_penerimaan_gudang_dari").val(data.tanggal_1);
			$("#tanggal_penerimaan_gudang_sampai").val(data.tanggal_2);
			loadDataPenerimaanGudang();
		}
	});
});

$("#periode_tanggal_top_pembelian_alkes").change(function(){
	const periode_tanggal = $("#periode_tanggal_top_pembelian_alkes").val();

	$.ajax({
		url: '{site_url}lsurvey/list_tipe/' + periode_tanggal,
		dataType: "json",
		success: function(data) {
			$("#tanggal_top_pembelian_alkes_dari").val(data.tanggal_1);
			$("#tanggal_top_pembelian_alkes_sampai").val(data.tanggal_2);
			loadDataTopPembelianAlkes();
		}
	});
});

$("#periode_tanggal_top_pembelian_implan").change(function(){
	const periode_tanggal = $("#periode_tanggal_top_pembelian_implan").val();

	$.ajax({
		url: '{site_url}lsurvey/list_tipe/' + periode_tanggal,
		dataType: "json",
		success: function(data) {
			$("#tanggal_top_pembelian_implan_dari").val(data.tanggal_1);
			$("#tanggal_top_pembelian_implan_sampai").val(data.tanggal_2);
			loadDataTopPembelianImplan();
		}
	});
});

$("#periode_tanggal_top_pembelian_obat").change(function(){
	const periode_tanggal = $("#periode_tanggal_top_pembelian_obat").val();

	$.ajax({
		url: '{site_url}lsurvey/list_tipe/' + periode_tanggal,
		dataType: "json",
		success: function(data) {
			$("#tanggal_top_pembelian_obat_dari").val(data.tanggal_1);
			$("#tanggal_top_pembelian_obat_sampai").val(data.tanggal_2);
			loadDataTopPembelianObat();
		}
	});
});

$("#periode_tanggal_top_pembelian_logistik").change(function(){
	const periode_tanggal = $("#periode_tanggal_top_pembelian_logistik").val();

	$.ajax({
		url: '{site_url}lsurvey/list_tipe/' + periode_tanggal,
		dataType: "json",
		success: function(data) {
			$("#tanggal_top_pembelian_logistik_dari").val(data.tanggal_1);
			$("#tanggal_top_pembelian_logistik_sampai").val(data.tanggal_2);
			loadDataTopPembelianLogistik();
		}
	});
});

$("#periode_tanggal_top_pembelian_distributor").change(function(){
	const periode_tanggal = $("#periode_tanggal_top_pembelian_distributor").val();

	$.ajax({
		url: '{site_url}lsurvey/list_tipe/' + periode_tanggal,
		dataType: "json",
		success: function(data) {
			$("#tanggal_top_pembelian_distributor_dari").val(data.tanggal_1);
			$("#tanggal_top_pembelian_distributor_sampai").val(data.tanggal_2);
			loadDataTopPembelianDistributor();
		}
	});
});

$("#grafik_harian_periode_tanggal_pemesanan").change(function(){
	const periode_tanggal = $("#grafik_harian_periode_tanggal_pemesanan").val();

	$.ajax({
		url: '{site_url}lsurvey/list_tipe/' + periode_tanggal,
		dataType: "json",
		success: function(data) {
			$("#grafik_harian_tanggal_pemesanan_gudang_dari").val(data.tanggal_1);
			$("#grafik_harian_pemesanan_gudang_sampai").val(data.tanggal_2);
			loadGrafikHarianPemesananGudang();
		}
	});
});

$("#grafik_harian_periode_tanggal_penerimaan").change(function(){
	const periode_tanggal = $("#grafik_harian_periode_tanggal_penerimaan").val();

	$.ajax({
		url: '{site_url}lsurvey/list_tipe/' + periode_tanggal,
		dataType: "json",
		success: function(data) {
			$("#grafik_harian_tanggal_penerimaan_gudang_dari").val(data.tanggal_1);
			$("#grafik_harian_penerimaan_gudang_sampai").val(data.tanggal_2);
			loadGrafikHarianPenerimaanGudang();
		}
	});
});

$("#grafik_harian_tipe_barang_periode_tanggal_pemesanan").change(function(){
	const periode_tanggal = $("#grafik_harian_tipe_barang_periode_tanggal_pemesanan").val();

	$.ajax({
		url: '{site_url}lsurvey/list_tipe/' + periode_tanggal,
		dataType: "json",
		success: function(data) {
			$("#grafik_harian_tipe_barang_tanggal_pemesanan_gudang_dari").val(data.tanggal_1);
			$("#grafik_harian_tipe_barang_pemesanan_gudang_sampai").val(data.tanggal_2);
			loadGrafikDataPemesananGudangHarianTipeBarang();
		}
	});
});

$("#grafik_harian_tipe_barang_periode_tanggal_penerimaan").change(function(){
	const periode_tanggal = $("#grafik_harian_tipe_barang_periode_tanggal_penerimaan").val();

	$.ajax({
		url: '{site_url}lsurvey/list_tipe/' + periode_tanggal,
		dataType: "json",
		success: function(data) {
			$("#grafik_harian_tipe_barang_tanggal_penerimaan_gudang_dari").val(data.tanggal_1);
			$("#grafik_harian_tipe_barang_penerimaan_gudang_sampai").val(data.tanggal_2);
			loadGrafikDataPenerimaanGudangHarianTipeBarang();
		}
	});
});

$("#tanggal_pemesanan_gudang_dari, #tanggal_pemesanan_gudang_sampai").change(function(){
	loadDataPemesananGudang();
});

$("#tanggal_penerimaan_gudang_dari, #tanggal_penerimaan_gudang_sampai").change(function(){
	loadDataPenerimaanGudang();
});

$("#grafik_harian_tanggal_pemesanan_gudang_dari, #grafik_harian_pemesanan_gudang_sampai").change(function(){
	loadGrafikHarianPemesananGudang();
});

$("#grafik_harian_tanggal_penerimaan_gudang_dari, #grafik_harian_penerimaan_gudang_sampai").change(function(){
	loadGrafikHarianPenerimaanGudang();
});

$("#grafik_rekap_periode, #grafik_rekap_periode_tahun").change(function(){
	loadGrafikRekapPemesananGudang();
});

$("#grafik_penerimaan_rekap_periode, #grafik_penerimaan_rekap_periode_tahun").change(function(){
	loadGrafikRekapPenerimaanGudang();
});

$("#grafik_harian_tipe_barang_tanggal_pemesanan_gudang_dari, #grafik_harian_tipe_barang_pemesanan_gudang_sampai").change(function(){
	loadGrafikDataPemesananGudangHarianTipeBarang();
});

$("#grafik_harian_tipe_barang_tanggal_penerimaan_gudang_dari, #grafik_harian_tipe_barang_penerimaan_gudang_sampai").change(function(){
	loadGrafikDataPenerimaanGudangHarianTipeBarang();
});

$("#grafik_tahunan_periode_tanggal_penerimaan").change(function(){
	loadGrafikDataPenerimaanGudangTahunan();
});

$("#grafik_tahunan_tipe_barang_periode_tanggal_penerimaan").change(function(){
	loadGrafikDataPenerimaanGudangTahunanTipeBarang();
});

$("#tanggal_top_pembelian_alkes_dari, #tanggal_top_pembelian_alkes_sampai").change(function(){
	loadDataTopPembelianAlkes();
});

$("#tanggal_top_pembelian_implan_dari, #tanggal_top_pembelian_implan_sampai").change(function(){
	loadDataTopPembelianImplan();
});

$("#tanggal_top_pembelian_obat_dari, #tanggal_top_pembelian_obat_sampai").change(function(){
	loadDataTopPembelianObat();
});

$("#tanggal_top_pembelian_logistik_dari, #tanggal_top_pembelian_logistik_sampai").change(function(){
	loadDataTopPembelianLogistik();
});

$("#tanggal_top_pembelian_distributor_dari, #tanggal_top_pembelian_distributor_sampai").change(function(){
	loadDataTopPembelianDistributor();
});

$("#unit_pelayanan_stokopname, #periode_stokopname").change(function(){
    const unit_pelayanan = $("#unit_pelayanan_stokopname").val();
    const periode = $("#periode_stokopname").val();

	getNomorStokopname(unit_pelayanan, periode);
});

$("#nomor_stokopname").change(function(){
    loadDataStokopname();
});

function loadSummaryDataPemesananGudang() {
	$("#cover-spin").show();
	
	const tanggal_dari = $("#tanggal_pemesanan_gudang_dari").val();
	const tanggal_sampai = $("#tanggal_pemesanan_gudang_sampai").val();

	$.ajax({
		url: '{site_url}executive_dashboard/load_summary_pemesanan_gudang',
		type: "POST" ,
		dataType: 'json',
		data : {
			tanggal_dari: tanggal_dari,
			tanggal_sampai: tanggal_sampai,
		},
		success: function(data) {
			$("#cover-spin").hide();
			$("#jumlah_pemesanan_gudang").text(data.jumlah_pemesanan_gudang);
			$("#total_nominal_pemesanan_gudang").text('Rp ' + data.total_nominal_pemesanan_gudang);
		}
	});
}

function loadDataPemesananGudang(){
	$('#data_pemesanan_gudang').DataTable().destroy();	

	const tanggal_dari = $("#tanggal_pemesanan_gudang_dari").val();
	const tanggal_sampai = $("#tanggal_pemesanan_gudang_sampai").val();
	
	tablePemesanan = $('#data_pemesanan_gudang').DataTable({
		"autoWidth": true,
		"searching": true,
		"serverSide": true,
		"processing": true,
		"order": [],
		"pageLength": 5,
		"ordering": false,
		"paging": true,
		"columnDefs": [
			{ "width": "10%", "targets": 0 },
			{ "width": "10%", "targets": 1,  className: "text-center" },
			{ "width": "10%", "targets": 2 },
			{ "width": "10%", "targets": 3,  className: "text-center" },
			{ "width": "10%", "targets": 4 },
			{ "width": "10%", "targets": 5,  className: "text-center" },
		],
		"ajax": { 
			url: '{site_url}executive_dashboard/load_data_pemesanan_gudang', 
			type: "POST" ,
			dataType: 'json',
			data: {
				tanggal_dari: tanggal_dari,
				tanggal_sampai: tanggal_sampai,
			}
		}
	});

	loadSummaryDataPemesananGudang();
}

function loadSummaryDataPenerimaanGudang() {
	$("#cover-spin").show();
	
	const tanggal_dari = $("#tanggal_penerimaan_gudang_dari").val();
	const tanggal_sampai = $("#tanggal_penerimaan_gudang_sampai").val();

	$.ajax({
		url: '{site_url}executive_dashboard/load_summary_penerimaan_gudang',
		type: "POST" ,
		dataType: 'json',
		data : {
			tanggal_dari: tanggal_dari,
			tanggal_sampai: tanggal_sampai,
		},
		success: function(data) {
			$("#cover-spin").hide();
			$("#jumlah_penerimaan_gudang").text(data.jumlah_penerimaan_gudang);
			$("#total_nominal_penerimaan_gudang").text('Rp ' + data.total_nominal_penerimaan_gudang);
		}
	});
}

function loadDataPenerimaanGudang(){
	$('#data_penerimaan_gudang').DataTable().destroy();	

	const tanggal_dari = $("#tanggal_penerimaan_gudang_dari").val();
	const tanggal_sampai = $("#tanggal_penerimaan_gudang_sampai").val();
	
	tablePenerimaan = $('#data_penerimaan_gudang').DataTable({
		"autoWidth": true,
		"searching": true,
		"serverSide": true,
		"processing": true,
		"order": [],
		"pageLength": 5,
		"ordering": false,
		"paging": true,
		"columnDefs": [
			{ "width": "10%", "targets": 0 },
			{ "width": "10%", "targets": 1,  className: "text-center" },
			{ "width": "10%", "targets": 2 },
			{ "width": "10%", "targets": 3,  className: "text-center" },
			{ "width": "10%", "targets": 4 },
			{ "width": "10%", "targets": 5,  className: "text-center" },
		],
		"ajax": { 
			url: '{site_url}executive_dashboard/load_data_penerimaan_gudang', 
			type: "POST" ,
			dataType: 'json',
			data: {
				tanggal_dari: tanggal_dari,
				tanggal_sampai: tanggal_sampai,
			}
		}
	});

	loadSummaryDataPenerimaanGudang();
}

function loadGrafikHarianPemesananGudang() {
	const tanggal_dari = $("#grafik_harian_tanggal_pemesanan_gudang_dari").val();
	const tanggal_sampai = $("#grafik_harian_tanggal_pemesanan_gudang_sampai").val();

	$.ajax({
		url: '{site_url}executive_dashboard/load_grafik_harian_pemesanan_gudang',
		type: "POST",
		dataType: 'json',
		data: {
			tanggal_dari: tanggal_dari,
			tanggal_sampai: tanggal_sampai,
		},
		success: function (data) {
			$("#cover-spin").hide();

			// Transformasi data untuk Chart.js
			const labels = [...new Set(data.map((item) => item.tanggal))];
			const nonLogistikData = labels.map((label) =>
				data
					.filter((item) => item.tanggal === label && item.nama_tipe === "NON LOGISTIK")
					.reduce((sum, item) => sum + parseFloat(item.total_nominal), 0)
			);
			const logistikData = labels.map((label) =>
				data
					.filter((item) => item.tanggal === label && item.nama_tipe === "LOGISTIK")
					.reduce((sum, item) => sum + parseFloat(item.total_nominal), 0)
			);

			// Render Chart.js
			renderChartPemesananGudangHarian(labels, nonLogistikData, logistikData);
		},
	});
}

function renderChartPemesananGudangHarian(labels, nonLogistikData, logistikData) {
    if (chartPemesananGudangHarian) {
        chartPemesananGudangHarian.destroy(); // Destroy existing chart instance
    }

    const ctx = document.getElementById("chartPemesananGudangHarian").getContext("2d");

    chartPemesananGudangHarian = new Chart(ctx, {
        type: 'bar', // Specify chart type
        data: {
            labels: labels,
            datasets: [
                {
                    label: 'Gudang Non Logistik', // Label untuk Non-Logistik
                    data: nonLogistikData,
                    backgroundColor: 'rgba(75, 192, 192, 0.6)', // Hijau
                },
                {
                    label: 'Gudang Logistik', // Label untuk Logistik
                    data: logistikData,
                    backgroundColor: 'rgba(54, 162, 235, 0.6)', // Biru
                },
            ],
        },
        options: {
            responsive: true,
            scales: {
                x: {
                    beginAtZero: true,
                },
                y: {
                    beginAtZero: true,
                    ticks: {
                        // Menambahkan pemformatan untuk nilai di sumbu Y (nominal)
                        callback: function(value) {
                            return value.toLocaleString('id-ID'); // Format angka dengan pemisah ribuan
                        }
                    }
                },
            },
            plugins: {
                tooltip: {
                    callbacks: {
                        label: function(tooltipItem) {
                            return 'Nominal: ' + tooltipItem.raw.toLocaleString('id-ID'); // Menampilkan tooltip dengan format nominal
                        }
                    }
                }
            }
        },
        plugins: [
            {
                id: "customBarLabels", // Plugin untuk menampilkan label di atas bar
                afterDatasetsDraw: function (chart) {
                    const ctx = chart.ctx; // Akses context canvas
                    chart.data.datasets.forEach((dataset, i) => {
                        const meta = chart.getDatasetMeta(i);
                        meta.data.forEach((bar, index) => {
                            const value = dataset.data[index];
                            // Posisi teks
                            const x = bar.x;
                            const y = bar.y - 10; // Sedikit di atas bar
                            ctx.fillStyle = "black"; // Warna teks
                            ctx.textAlign = "center";
                            ctx.font = "12px Arial";
                            // Format angka (tambahkan titik untuk ribuan)
                            ctx.fillText(value.toLocaleString('id-ID'), x, y);
                        });
                    });
                },
            },
        ],
    });
}

function loadGrafikHarianPenerimaanGudang() {
	const tanggal_dari = $("#grafik_harian_tanggal_penerimaan_gudang_dari").val();
	const tanggal_sampai = $("#grafik_harian_tanggal_penerimaan_gudang_sampai").val();

	$.ajax({
		url: '{site_url}executive_dashboard/load_grafik_harian_penerimaan_gudang',
		type: "POST",
		dataType: 'json',
		data: {
			tanggal_dari: tanggal_dari,
			tanggal_sampai: tanggal_sampai,
		},
		success: function (data) {
			$("#cover-spin").hide();

			// Transformasi data untuk Chart.js
			const labels = [...new Set(data.map((item) => item.tanggal))];
			const nonLogistikData = labels.map((label) =>
				data
					.filter((item) => item.tanggal === label && item.nama_tipe === "NON LOGISTIK")
					.reduce((sum, item) => sum + parseFloat(item.total_nominal), 0)
			);
			const logistikData = labels.map((label) =>
				data
					.filter((item) => item.tanggal === label && item.nama_tipe === "LOGISTIK")
					.reduce((sum, item) => sum + parseFloat(item.total_nominal), 0)
			);

			// Render Chart.js
			renderChartPenerimaanGudangHarian(labels, nonLogistikData, logistikData);
		},
	});
}

function renderChartPenerimaanGudangHarian(labels, nonLogistikData, logistikData) {
    if (chartPenerimaanGudangHarian) {
        chartPenerimaanGudangHarian.destroy(); // Destroy existing chart instance
    }

    const ctx = document.getElementById("chartPenerimaanGudangHarian").getContext("2d");

    chartPenerimaanGudangHarian = new Chart(ctx, {
        type: 'bar', // Specify chart type
        data: {
            labels: labels,
            datasets: [
                {
                    label: 'Gudang Non Logistik', // Label untuk Non-Logistik
                    data: nonLogistikData,
                    backgroundColor: 'rgba(75, 192, 192, 0.6)', // Hijau
                },
                {
                    label: 'Gudang Logistik', // Label untuk Logistik
                    data: logistikData,
                    backgroundColor: 'rgba(54, 162, 235, 0.6)', // Biru
                },
            ],
        },
        options: {
            responsive: true,
            scales: {
                x: {
                    beginAtZero: true,
                },
                y: {
                    beginAtZero: true,
                    ticks: {
                        // Menambahkan pemformatan untuk nilai di sumbu Y (nominal)
                        callback: function(value) {
                            return value.toLocaleString('id-ID'); // Format angka dengan pemisah ribuan
                        }
                    }
                },
            },
            plugins: {
                tooltip: {
                    callbacks: {
                        label: function(tooltipItem) {
                            return 'Nominal: ' + tooltipItem.raw.toLocaleString('id-ID'); // Menampilkan tooltip dengan format nominal
                        }
                    }
                }
            }
        },
        plugins: [
            {
                id: "customBarLabels", // Plugin untuk menampilkan label di atas bar
                afterDatasetsDraw: function (chart) {
                    const ctx = chart.ctx; // Akses context canvas
                    chart.data.datasets.forEach((dataset, i) => {
                        const meta = chart.getDatasetMeta(i);
                        meta.data.forEach((bar, index) => {
                            const value = dataset.data[index];
                            // Posisi teks
                            const x = bar.x;
                            const y = bar.y - 10; // Sedikit di atas bar
                            ctx.fillStyle = "black"; // Warna teks
                            ctx.textAlign = "center";
                            ctx.font = "12px Arial";
                            // Format angka (tambahkan titik untuk ribuan)
                            ctx.fillText(value.toLocaleString('id-ID'), x, y);
                        });
                    });
                },
            },
        ],
    });
}

function loadGrafikRekapPemesananGudang() {
    const grafik_rekap_periode = $("#grafik_rekap_periode").val(); // Monthly, Semester, Triwulan
    const grafik_rekap_periode_tahun = $("#grafik_rekap_periode_tahun").val(); // Year

    $.ajax({
        url: '{site_url}executive_dashboard/load_grafik_rekap_pemesanan_gudang',
        type: "POST",
        dataType: 'json',
        data: {
            periode: grafik_rekap_periode,
            tahun: grafik_rekap_periode_tahun
        },
        success: function (data) {
            $("#cover-spin").hide();

            // Transformasi data untuk Chart.js
            const labels = [...new Set(data.map((item) => item.periode))]; // Periodic labels
            const nonLogistikData = labels.map((label) =>
                data
                    .filter((item) => item.periode === label && item.nama_tipe === "NON LOGISTIK")
                    .reduce((sum, item) => sum + parseFloat(item.total_nominal), 0)
            );
            const logistikData = labels.map((label) =>
                data
                    .filter((item) => item.periode === label && item.nama_tipe === "LOGISTIK")
                    .reduce((sum, item) => sum + parseFloat(item.total_nominal), 0)
            );

            // Render Chart.js
            renderChartPemesananGudangRekap(labels, nonLogistikData, logistikData);
        },
    });
}

function renderChartPemesananGudangRekap(labels, nonLogistikData, logistikData) {
    if (chartPemesananGudangRekap) {
        chartPemesananGudangRekap.destroy(); // Destroy existing chart instance
    }

    const ctx = document.getElementById("chartPemesananGudangRekap").getContext("2d");

    chartPemesananGudangRekap = new Chart(ctx, {
        type: 'bar', // Specify chart type
        data: {
            labels: labels,
            datasets: [
                {
                    label: 'Gudang Non Logistik', // Label untuk Non-Logistik
                    data: nonLogistikData,
                    backgroundColor: 'rgba(75, 192, 192, 0.6)', // Hijau
                },
                {
                    label: 'Gudang Logistik', // Label untuk Logistik
                    data: logistikData,
                    backgroundColor: 'rgba(54, 162, 235, 0.6)', // Biru
                },
            ],
        },
        options: {
            responsive: true,
            scales: {
                x: {
                    beginAtZero: true,
                },
                y: {
                    beginAtZero: true,
                    ticks: {
                        // Menambahkan pemformatan untuk nilai di sumbu Y (nominal)
                        callback: function(value) {
                            return value.toLocaleString('id-ID'); // Format angka dengan pemisah ribuan
                        }
                    }
                },
            },
            plugins: {
                tooltip: {
                    callbacks: {
                        label: function(tooltipItem) {
                            return 'Nominal: ' + tooltipItem.raw.toLocaleString('id-ID'); // Menampilkan tooltip dengan format nominal
                        }
                    }
                }
            }
        },
        plugins: [
            {
                id: "customBarLabels", // Plugin untuk menampilkan label di atas bar
                afterDatasetsDraw: function (chart) {
                    const ctx = chart.ctx; // Akses context canvas
                    chart.data.datasets.forEach((dataset, i) => {
                        const meta = chart.getDatasetMeta(i);
                        meta.data.forEach((bar, index) => {
                            const value = dataset.data[index];
                            // Posisi teks
                            const x = bar.x;
                            const y = bar.y - 10; // Sedikit di atas bar
                            ctx.fillStyle = "black"; // Warna teks
                            ctx.textAlign = "center";
                            ctx.font = "12px Arial";
                            // Format angka (tambahkan titik untuk ribuan)
                            ctx.fillText(value.toLocaleString('id-ID'), x, y);
                        });
                    });
                },
            },
        ],
    });
}

function loadGrafikRekapPenerimaanGudang() {
    const grafik_rekap_periode = $("#grafik_penerimaan_rekap_periode").val(); // Monthly, Semester, Triwulan
    const grafik_rekap_periode_tahun = $("#grafik_penerimaan_rekap_periode_tahun").val(); // Year

    $.ajax({
        url: '{site_url}executive_dashboard/load_grafik_rekap_penerimaan_gudang',
        type: "POST",
        dataType: 'json',
        data: {
            periode: grafik_rekap_periode,
            tahun: grafik_rekap_periode_tahun
        },
        success: function (data) {
            $("#cover-spin").hide();

            // Transformasi data untuk Chart.js
            const labels = [...new Set(data.map((item) => item.periode))]; // Periodic labels
            const nonLogistikData = labels.map((label) =>
                data
                    .filter((item) => item.periode === label && item.nama_tipe === "NON LOGISTIK")
                    .reduce((sum, item) => sum + parseFloat(item.total_nominal), 0)
            );
            const logistikData = labels.map((label) =>
                data
                    .filter((item) => item.periode === label && item.nama_tipe === "LOGISTIK")
                    .reduce((sum, item) => sum + parseFloat(item.total_nominal), 0)
            );

            // Render Chart.js
            renderChartPenerimaanGudangRekap(labels, nonLogistikData, logistikData);
        },
    });
}

function renderChartPenerimaanGudangRekap(labels, nonLogistikData, logistikData) {
    if (chartPenerimaanGudangRekap) {
        chartPenerimaanGudangRekap.destroy(); // Destroy existing chart instance
    }

    const ctx = document.getElementById("chartPenerimaanGudangRekap").getContext("2d");

    chartPenerimaanGudangRekap = new Chart(ctx, {
        type: 'bar', // Specify chart type
        data: {
            labels: labels,
            datasets: [
                {
                    label: 'Gudang Non Logistik', // Label untuk Non-Logistik
                    data: nonLogistikData,
                    backgroundColor: 'rgba(75, 192, 192, 0.6)', // Hijau
                },
                {
                    label: 'Gudang Logistik', // Label untuk Logistik
                    data: logistikData,
                    backgroundColor: 'rgba(54, 162, 235, 0.6)', // Biru
                },
            ],
        },
        options: {
            responsive: true,
            scales: {
                x: {
                    beginAtZero: true,
                },
                y: {
                    beginAtZero: true,
                    ticks: {
                        // Menambahkan pemformatan untuk nilai di sumbu Y (nominal)
                        callback: function(value) {
                            return value.toLocaleString('id-ID'); // Format angka dengan pemisah ribuan
                        }
                    }
                },
            },
            plugins: {
                tooltip: {
                    callbacks: {
                        label: function(tooltipItem) {
                            return 'Nominal: ' + tooltipItem.raw.toLocaleString('id-ID'); // Menampilkan tooltip dengan format nominal
                        }
                    }
                }
            }
        },
        plugins: [
            {
                id: "customBarLabels", // Plugin untuk menampilkan label di atas bar
                afterDatasetsDraw: function (chart) {
                    const ctx = chart.ctx; // Akses context canvas
                    chart.data.datasets.forEach((dataset, i) => {
                        const meta = chart.getDatasetMeta(i);
                        meta.data.forEach((bar, index) => {
                            const value = dataset.data[index];
                            // Posisi teks
                            const x = bar.x;
                            const y = bar.y - 10; // Sedikit di atas bar
                            ctx.fillStyle = "black"; // Warna teks
                            ctx.textAlign = "center";
                            ctx.font = "12px Arial";
                            // Format angka (tambahkan titik untuk ribuan)
                            ctx.fillText(value.toLocaleString('id-ID'), x, y);
                        });
                    });
                },
            },
        ],
    });
}

function loadGrafikDataPemesananGudangHarianTipeBarang() {
    const periode = $("#grafik_harian_tipe_barang_periode_tanggal_pemesanan").val();
    const tanggal_dari = $("#grafik_harian_tipe_barang_tanggal_pemesanan_gudang_dari").val();
    const tanggal_sampai = $("#grafik_harian_tipe_barang_tanggal_pemesanan_gudang_sampai").val();

    $.ajax({
        url: '{site_url}executive_dashboard/load_grafik_harian_pemesanan_gudang_tipe_barang',
        type: "POST",
        dataType: 'json',
        data: {
            periode: periode,
            tanggal_dari: tanggal_dari,
            tanggal_sampai: tanggal_sampai,
        },
        success: function (data) {
            $("#cover-spin").hide();

            // Extracting unique dates and grouping data by tipe_barang
            const labels = [...new Set(data.map((item) => item.tanggal))];
            const tipeBarangData = {
                'Alat Kesehatan': [],
                'Implan': [],
                'Obat': [],
                'Logistik': []
            };

            labels.forEach((label) => {
                // Filtering and summing data for each tipe_barang
                ['Alat Kesehatan', 'Implan', 'Obat', 'Logistik'].forEach((tipe) => {
                    const sum = data
                        .filter((item) => item.tanggal === label && item.tipe_barang === tipe)
                        .reduce((acc, item) => acc + parseFloat(item.total_nominal), 0);
                    tipeBarangData[tipe].push(sum);
                });
            });

            // Render the chart with grouped data by tipe_barang
            renderChartPemesananGudangHarianTipeBarang(labels, tipeBarangData);
        },
    });
}

function renderChartPemesananGudangHarianTipeBarang(labels, tipeBarangData) {
	console.log(labels, tipeBarangData);
	
    if (chartPemesananGudangHarianTipeBarang) {
        chartPemesananGudangHarianTipeBarang.destroy(); // Destroy existing chart instance
    }

    const ctx = document.getElementById("chartPemesananGudangHarianTipeBarang").getContext("2d");

    chartPemesananGudangHarianTipeBarang = new Chart(ctx, {
        type: 'bar', // Specify chart type
        data: {
            labels: labels,
            datasets: [
                {
                    label: 'Alat Kesehatan',
                    data: tipeBarangData['Alat Kesehatan'],
                    backgroundColor: 'rgba(75, 192, 192, 0.6)', // Green
                },
                {
                    label: 'Implan',
                    data: tipeBarangData['Implan'],
                    backgroundColor: 'rgba(54, 162, 235, 0.6)', // Blue
                },
                {
                    label: 'Obat',
                    data: tipeBarangData['Obat'],
                    backgroundColor: 'rgba(255, 159, 64, 0.6)', // Orange
                },
                {
                    label: 'Logistik',
                    data: tipeBarangData['Logistik'],
                    backgroundColor: 'rgba(153, 102, 255, 0.6)', // Purple
                },
            ],
        },
        options: {
            responsive: true,
            scales: {
                x: {
                    beginAtZero: true,
                },
                y: {
                    beginAtZero: true,
                    ticks: {
                        callback: function (value) {
                            return value.toLocaleString('id-ID'); // Format number with thousands separator
                        }
                    }
                },
            },
            plugins: {
                tooltip: {
                    callbacks: {
                        label: function (tooltipItem) {
                            return 'Nominal: ' + tooltipItem.raw.toLocaleString('id-ID'); // Format tooltip with thousands separator
                        }
                    }
                }
            }
        },
    });
}

function loadGrafikDataPenerimaanGudangHarianTipeBarang() {
    const periode = $("#grafik_harian_tipe_barang_periode_tanggal_penerimaan").val();
    const tanggal_dari = $("#grafik_harian_tipe_barang_tanggal_penerimaan_gudang_dari").val();
    const tanggal_sampai = $("#grafik_harian_tipe_barang_tanggal_penerimaan_gudang_sampai").val();

    $.ajax({
        url: '{site_url}executive_dashboard/load_grafik_harian_penerimaan_gudang_tipe_barang',
        type: "POST",
        dataType: 'json',
        data: {
            periode: periode,
            tanggal_dari: tanggal_dari,
            tanggal_sampai: tanggal_sampai,
        },
        success: function (data) {
            $("#cover-spin").hide();

            // Extracting unique dates and grouping data by tipe_barang
            const labels = [...new Set(data.map((item) => item.tanggal))];
            const tipeBarangData = {
                'Alat Kesehatan': [],
                'Implan': [],
                'Obat': [],
                'Logistik': []
            };

            labels.forEach((label) => {
                // Filtering and summing data for each tipe_barang
                ['Alat Kesehatan', 'Implan', 'Obat', 'Logistik'].forEach((tipe) => {
                    const sum = data
                        .filter((item) => item.tanggal === label && item.tipe_barang === tipe)
                        .reduce((acc, item) => acc + parseFloat(item.total_nominal), 0);
                    tipeBarangData[tipe].push(sum);
                });
            });

            // Render the chart with grouped data by tipe_barang
            renderChartPenerimaanGudangHarianTipeBarang(labels, tipeBarangData);
        },
    });
}

function renderChartPenerimaanGudangHarianTipeBarang(labels, tipeBarangData) {
	console.log(labels, tipeBarangData);
	
    if (chartPenerimaanGudangHarianTipeBarang) {
        chartPenerimaanGudangHarianTipeBarang.destroy(); // Destroy existing chart instance
    }

    const ctx = document.getElementById("chartPenerimaanGudangHarianTipeBarang").getContext("2d");

    chartPenerimaanGudangHarianTipeBarang = new Chart(ctx, {
        type: 'bar', // Specify chart type
        data: {
            labels: labels,
            datasets: [
                {
                    label: 'Alat Kesehatan',
                    data: tipeBarangData['Alat Kesehatan'],
                    backgroundColor: 'rgba(75, 192, 192, 0.6)', // Green
                },
                {
                    label: 'Implan',
                    data: tipeBarangData['Implan'],
                    backgroundColor: 'rgba(54, 162, 235, 0.6)', // Blue
                },
                {
                    label: 'Obat',
                    data: tipeBarangData['Obat'],
                    backgroundColor: 'rgba(255, 159, 64, 0.6)', // Orange
                },
                {
                    label: 'Logistik',
                    data: tipeBarangData['Logistik'],
                    backgroundColor: 'rgba(153, 102, 255, 0.6)', // Purple
                },
            ],
        },
        options: {
            responsive: true,
            scales: {
                x: {
                    beginAtZero: true,
                },
                y: {
                    beginAtZero: true,
                    ticks: {
                        callback: function (value) {
                            return value.toLocaleString('id-ID'); // Format number with thousands separator
                        }
                    }
                },
            },
            plugins: {
                tooltip: {
                    callbacks: {
                        label: function (tooltipItem) {
                            return 'Nominal: ' + tooltipItem.raw.toLocaleString('id-ID'); // Format tooltip with thousands separator
                        }
                    }
                }
            }
        },
    });
}

function loadGrafikDataPenerimaanGudangTahunanTipeBarang() {
    const periode = $("#grafik_tahunan_tipe_barang_periode_tanggal_penerimaan").val();

    $.ajax({
        url: '{site_url}executive_dashboard/load_grafik_tahunan_penerimaan_gudang_tipe_barang',
        type: "POST",
        dataType: 'json',
        data: {
            periode: periode
        },
        success: function (data) {
            $("#cover-spin").hide();

            // Extracting unique dates and grouping data by tipe_barang
            const labels = [...new Set(data.map((item) => item.tahun))];
            const tipeBarangData = {
                'Alat Kesehatan': [],
                'Implan': [],
                'Obat': [],
                'Logistik': []
            };

            labels.forEach((label) => {
                // Filtering and summing data for each tipe_barang
                ['Alat Kesehatan', 'Implan', 'Obat', 'Logistik'].forEach((tipe) => {
                    const sum = data
                        .filter((item) => item.tahun === label && item.tipe_barang === tipe)
                        .reduce((acc, item) => acc + parseFloat(item.total_nominal), 0);
                    tipeBarangData[tipe].push(sum);
                });
            });

            // Render the chart with grouped data by tipe_barang
            renderChartPenerimaanGudangTahunanTipeBarang(labels, tipeBarangData);
        },
    });
}

function renderChartPenerimaanGudangTahunanTipeBarang(labels, tipeBarangData) {
	console.log(labels, tipeBarangData);
	
    if (chartPenerimaanGudangTahunanTipeBarang) {
        chartPenerimaanGudangTahunanTipeBarang.destroy(); // Destroy existing chart instance
    }

    const ctx = document.getElementById("chartPenerimaanGudangTahunanTipeBarang").getContext("2d");

    chartPenerimaanGudangTahunanTipeBarang = new Chart(ctx, {
        type: 'bar', // Specify chart type
        data: {
            labels: labels,
            datasets: [
                {
                    label: 'Alat Kesehatan',
                    data: tipeBarangData['Alat Kesehatan'],
                    backgroundColor: 'rgba(75, 192, 192, 0.6)', // Green
                },
                {
                    label: 'Implan',
                    data: tipeBarangData['Implan'],
                    backgroundColor: 'rgba(54, 162, 235, 0.6)', // Blue
                },
                {
                    label: 'Obat',
                    data: tipeBarangData['Obat'],
                    backgroundColor: 'rgba(255, 159, 64, 0.6)', // Orange
                },
                {
                    label: 'Logistik',
                    data: tipeBarangData['Logistik'],
                    backgroundColor: 'rgba(153, 102, 255, 0.6)', // Purple
                },
            ],
        },
        options: {
            responsive: true,
            scales: {
                x: {
                    beginAtZero: true,
                },
                y: {
                    beginAtZero: true,
                    ticks: {
                        callback: function (value) {
                            return value.toLocaleString('id-ID'); // Format number with thousands separator
                        }
                    }
                },
            },
            plugins: {
                tooltip: {
                    callbacks: {
                        label: function (tooltipItem) {
                            return 'Nominal: ' + tooltipItem.raw.toLocaleString('id-ID'); // Format tooltip with thousands separator
                        }
                    }
                }
            }
        },
    });
}

function loadGrafikDataPenerimaanGudangTahunan() {
    const periode = $("#grafik_tahunan_periode_tanggal_penerimaan").val();

    $.ajax({
        url: '{site_url}executive_dashboard/load_grafik_tahunan_penerimaan_gudang',
        type: "POST",
        dataType: 'json',
        data: {
            periode: periode,
        },
        success: function (data) {
            $("#cover-spin").hide();

            // Transformasi data untuk Chart.js
            const labels = [...new Set(data.map((item) => item.tahun))]; // Periodic labels
            const nonLogistikData = labels.map((label) =>
                data
                    .filter((item) => item.tahun === label && item.nama_tipe === "NON LOGISTIK")
                    .reduce((sum, item) => sum + parseFloat(item.total_nominal), 0)
            );
            const logistikData = labels.map((label) =>
                data
                    .filter((item) => item.tahun === label && item.nama_tipe === "LOGISTIK")
                    .reduce((sum, item) => sum + parseFloat(item.total_nominal), 0)
            );

            // Render Chart.js
            renderChartPenerimaanGudangTahunan(labels, nonLogistikData, logistikData);
        },
    });
}

function renderChartPenerimaanGudangTahunan(labels, nonLogistikData, logistikData) {
    if (chartPenerimaanGudangTahunan) {
        chartPenerimaanGudangTahunan.destroy(); // Destroy existing chart instance
    }

    const ctx = document.getElementById("chartPenerimaanGudangTahunan").getContext("2d");

    chartPenerimaanGudangTahunan = new Chart(ctx, {
        type: 'bar', // Specify chart type
        data: {
            labels: labels,
            datasets: [
                {
                    label: 'Gudang Non Logistik', // Label untuk Non-Logistik
                    data: nonLogistikData,
                    backgroundColor: 'rgba(75, 192, 192, 0.6)', // Hijau
                },
                {
                    label: 'Gudang Logistik', // Label untuk Logistik
                    data: logistikData,
                    backgroundColor: 'rgba(54, 162, 235, 0.6)', // Biru
                },
            ],
        },
        options: {
            responsive: true,
            scales: {
                x: {
                    beginAtZero: true,
                },
                y: {
                    beginAtZero: true,
                    ticks: {
                        // Menambahkan pemformatan untuk nilai di sumbu Y (nominal)
                        callback: function(value) {
                            return value.toLocaleString('id-ID'); // Format angka dengan pemisah ribuan
                        }
                    }
                },
            },
            plugins: {
                tooltip: {
                    callbacks: {
                        label: function(tooltipItem) {
                            return 'Nominal: ' + tooltipItem.raw.toLocaleString('id-ID'); // Menampilkan tooltip dengan format nominal
                        }
                    }
                }
            }
        },
        plugins: [
            {
                id: "customBarLabels", // Plugin untuk menampilkan label di atas bar
                afterDatasetsDraw: function (chart) {
                    const ctx = chart.ctx; // Akses context canvas
                    chart.data.datasets.forEach((dataset, i) => {
                        const meta = chart.getDatasetMeta(i);
                        meta.data.forEach((bar, index) => {
                            const value = dataset.data[index];
                            // Posisi teks
                            const x = bar.x;
                            const y = bar.y - 10; // Sedikit di atas bar
                            ctx.fillStyle = "black"; // Warna teks
                            ctx.textAlign = "center";
                            ctx.font = "12px Arial";
                            // Format angka (tambahkan titik untuk ribuan)
                            ctx.fillText(value.toLocaleString('id-ID'), x, y);
                        });
                    });
                },
            },
        ],
    });
}

function loadDataTopPembelianAlkes() {
	$('#data_top_pembelian_alkes').DataTable().destroy();	

	const tanggal_dari = $("#tanggal_top_pembelian_alkes_dari").val();
	const tanggal_sampai = $("#tanggal_top_pembelian_alkes_sampai").val();
	const page_length = $("#show_top_pembelian_alkes option:selected").val();
	const order_by = $("#order_by_top_pembelian_alkes").val();
	
	tableTopPembelianBarang = $('#data_top_pembelian_alkes').DataTable({
		"autoWidth": true,
		"searching": true,
		"serverSide": true,
		"processing": true,
		"order": [],
		"pageLength": page_length,
		"ordering": false,
		"paging": true,
		"columnDefs": [
			{ "width": "10%", "targets": 0,  className: "text-center" },
			{ "width": "50%", "targets": 1 },
			{ "width": "10%", "targets": 2,  className: "text-center" },
			{ "width": "20%", "targets": 3,  className: "text-right" }
		],
		"ajax": { 
			url: '{site_url}executive_dashboard/load_data_top_pembelian_barang', 
			type: "POST" ,
			dataType: 'json',
			data: {
                idtipe: 1,
				tanggal_dari: tanggal_dari,
				tanggal_sampai: tanggal_sampai,
			}
		}
	});
}

function loadDataTopPembelianImplan() {
	$('#data_top_pembelian_implan').DataTable().destroy();	

	const tanggal_dari = $("#tanggal_top_pembelian_implan_dari").val();
	const tanggal_sampai = $("#tanggal_top_pembelian_implan_sampai").val();
	const page_length = $("#show_top_pembelian_implan option:selected").val();
	const order_by = $("#order_by_top_pembelian_implan").val();
	
	tableTopPembelianBarang = $('#data_top_pembelian_implan').DataTable({
		"autoWidth": true,
		"searching": true,
		"serverSide": true,
		"processing": true,
		"order": [],
		"pageLength": page_length,
		"ordering": false,
		"paging": true,
		"columnDefs": [
			{ "width": "10%", "targets": 0,  className: "text-center" },
			{ "width": "50%", "targets": 1 },
			{ "width": "10%", "targets": 2,  className: "text-center" },
			{ "width": "20%", "targets": 3,  className: "text-right" }
		],
		"ajax": { 
			url: '{site_url}executive_dashboard/load_data_top_pembelian_barang', 
			type: "POST" ,
			dataType: 'json',
			data: {
                idtipe: 2,
				tanggal_dari: tanggal_dari,
				tanggal_sampai: tanggal_sampai,
			}
		}
	});
}

function loadDataTopPembelianObat() {
	$('#data_top_pembelian_obat').DataTable().destroy();	

	const tanggal_dari = $("#tanggal_top_pembelian_obat_dari").val();
	const tanggal_sampai = $("#tanggal_top_pembelian_obat_sampai").val();
	const page_length = $("#show_top_pembelian_obat option:selected").val();
	const order_by = $("#order_by_top_pembelian_obat").val();
	
	tableTopPembelianBarang = $('#data_top_pembelian_obat').DataTable({
		"autoWidth": true,
		"searching": true,
		"serverSide": true,
		"processing": true,
		"order": [],
		"pageLength": page_length,
		"ordering": false,
		"paging": true,
		"columnDefs": [
			{ "width": "10%", "targets": 0,  className: "text-center" },
			{ "width": "50%", "targets": 1 },
			{ "width": "10%", "targets": 2,  className: "text-center" },
			{ "width": "20%", "targets": 3,  className: "text-right" }
		],
		"ajax": { 
			url: '{site_url}executive_dashboard/load_data_top_pembelian_barang', 
			type: "POST" ,
			dataType: 'json',
			data: {
                idtipe: 3,
				tanggal_dari: tanggal_dari,
				tanggal_sampai: tanggal_sampai,
			}
		}
	});
}

function loadDataTopPembelianLogistik() {
	$('#data_top_pembelian_logistik').DataTable().destroy();	

	const tanggal_dari = $("#tanggal_top_pembelian_logistik_dari").val();
	const tanggal_sampai = $("#tanggal_top_pembelian_logistik_sampai").val();
	const page_length = $("#show_top_pembelian_logistik option:selected").val();
	const order_by = $("#order_by_top_pembelian_logistik").val();
	
	tableTopPembelianBarang = $('#data_top_pembelian_logistik').DataTable({
		"autoWidth": true,
		"searching": true,
		"serverSide": true,
		"processing": true,
		"order": [],
		"pageLength": page_length,
		"ordering": false,
		"paging": true,
		"columnDefs": [
			{ "width": "10%", "targets": 0,  className: "text-center" },
			{ "width": "50%", "targets": 1 },
			{ "width": "10%", "targets": 2,  className: "text-center" },
			{ "width": "20%", "targets": 3,  className: "text-right" }
		],
		"ajax": { 
			url: '{site_url}executive_dashboard/load_data_top_pembelian_barang', 
			type: "POST" ,
			dataType: 'json',
			data: {
                idtipe: 4,
				tanggal_dari: tanggal_dari,
				tanggal_sampai: tanggal_sampai,
			}
		}
	});
}

function loadDataTopPembelianDistributor() {
	$('#data_top_pembelian_distributor').DataTable().destroy();	

	const tanggal_dari = $("#tanggal_top_pembelian_distributor_dari").val();
	const tanggal_sampai = $("#tanggal_top_pembelian_distributor_sampai").val();
	const page_length = $("#show_top_pembelian_distributor option:selected").val();
	const order_by = $("#order_by_top_pembelian_distributor").val();
	
	tableTopPembelianBarang = $('#data_top_pembelian_distributor').DataTable({
		"autoWidth": true,
		"searching": true,
		"serverSide": true,
		"processing": true,
		"order": [],
		"pageLength": page_length,
		"ordering": false,
		"paging": true,
		"columnDefs": [
			{ "width": "10%", "targets": 0,  className: "text-center" },
			{ "width": "50%", "targets": 1 },
			{ "width": "10%", "targets": 2,  className: "text-center" }
		],
		"ajax": { 
			url: '{site_url}executive_dashboard/load_data_top_distributor_pembelian_barang', 
			type: "POST" ,
			dataType: 'json',
			data: {
				tanggal_dari: tanggal_dari,
				tanggal_sampai: tanggal_sampai,
			}
		}
	});
}

function getNomorStokopname(unit_pelayanan, periode) {
    $.ajax({
        url: '{site_url}executive_dashboard/get_nomor_stokopname',
        method: "POST",
        dataType: "json",
        data: {
            unit_pelayanan: unit_pelayanan,
            periode: periode
        },
        success: function(data) {
            $("#nomor_stokopname").empty();
            $("#nomor_stokopname").append("<option value=''>Pilih Opsi</option>");
            data.map(({id, nostockopname, tgl_so}) => {
                $("#nomor_stokopname").append("<option value='" + id + "'>" + nostockopname + " " + tgl_so + "</option>");
            });
            $("#nomor_stokopname").selectpicker("refresh");
        },
    });
}

function loadDataStokopname(){
	$('#data_stokopname').DataTable().destroy();	

    const unit_pelayanan = $("#unit_pelayanan_stokopname").val();
    const periode = $("#periode_stokopname").val();
	const nomor_stokopname = $("#nomor_stokopname").val();
	
	tablePemesanan = $('#data_stokopname').DataTable({
		"autoWidth": true,
		"searching": true,
		"serverSide": true,
		"processing": true,
		"order": [],
		"pageLength": 5,
		"ordering": false,
		"paging": true,
		"columnDefs": [
			{ "width": "10%", "targets": 0,  className: "text-center" },
			{ "width": "10%", "targets": 1 },
			{ "width": "10%", "targets": 2 },
			{ "width": "10%", "targets": 3, className: "text-center" },
			{ "width": "10%", "targets": 4, className: "text-center" },
			{ "width": "10%", "targets": 5,  className: "text-center" },
			{ "width": "10%", "targets": 6 },
			{ "width": "10%", "targets": 7, className: "text-right" },
			{ "width": "10%", "targets": 8, className: "text-right" },
			{ "width": "10%", "targets": 9, className: "text-right" },
			{ "width": "10%", "targets": 10,  className: "text-center" },
			{ "width": "10%", "targets": 11,  className: "text-center" },
		],
		"ajax": { 
			url: '{site_url}executive_dashboard/load_data_stokopname', 
			type: "POST" ,
			dataType: 'json',
			data: {
				unit_pelayanan: unit_pelayanan,
				periode: periode,
				nomor_stokopname: nomor_stokopname,
			}
		}
	});
}
</script>