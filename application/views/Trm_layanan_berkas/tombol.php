<div class="content-grid">
	<div class="row">
		<?php if (UserAccesForm($user_acces_form,array('1155'))){ ?>
		<div class="col-xs-6 col-sm-4 col-lg-2">
			<a class="block block-link-hover2 text-center" href="<?=($tombol=='index'?'javascript:void(0)':'{site_url}trm_layanan_berkas/index')?>">
				<div class="block-content block-content-full <?=($tombol=='index'?'bg-primary':'')?>">
					<i class="fa fa-send fa-4x text-<?=($tombol=='index'?'white':'danger')?>"></i>
					<div class="font-w600 <?=($tombol=='index'?'text-white-op':'')?> push-15-t">Kirim Berkas</div>
				</div>
			</a>
		</div>
		<?}?>
		<?php if (UserAccesForm($user_acces_form,array('1161'))){ ?>
		<div class="col-xs-6 col-sm-4 col-lg-2">
			<a class="block block-link-hover3 text-center" href="<?=($tombol=='kembali'?'javascript:void(0)':'{site_url}trm_layanan_berkas/kembali')?>">
				<div class="block-content block-content-full <?=($tombol=='kembali'?'bg-primary':'')?>">
					<i class="si si-arrow-left fa-4x text-<?=($tombol=='kembali'?'white':'danger')?>"></i>
					<div class="font-w600 <?=($tombol=='kembali'?'text-white-op':'')?> push-15-t">Kembali Berkas</div>
				</div>
			</a>
		</div>
		<?}?>
		
	</div>
</div>