<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>
<?
	$this->load->view('Trm_layanan_berkas/tombol');
?>


<div class="block" id="div_header">
    <div class="block-header">
		
        <h3 class="block-title">Pengembalian Berkas</h3>
		<hr style="margin-top:15px">
        <div class="row">
            <?php echo form_open('trm_layanan_berkas/filter', 'class="form-horizontal" id="form"'); ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="idjenis">Jenis</label>
                    <div class="col-md-8">
                        <select id="idjenis" name="idjenis" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua Jenis -</option>
							<option value="1" >Pelayanan</option>
							<option value="2" >Peminjaman</option>
                        </select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status_pasien">Status Pasien</label>
                    <div class="col-md-8">
                        <select id="status_pasien" name="status_pasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>-Semua Status -</option>
							<option value="0" >Pasien Lama</option>
							<option value="1" >Pasien baru</option>
                        </select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tujuan">Tujuan</label>
                    <div class="col-md-8">
                        <select id="tujuan" name="tujuan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua Tujuan -</option>
							<option value="1" >Poliklinik</option>
							<option value="2" >IGD</option>
							<option value="3" >Rawat Inap</option>
							<option value="4" >ODS</option>
							<option value="5" >Pinjam</option>
                        </select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="nomedrec">Medrec</label>
                    <div class="col-md-8">
                        <input type="text" id="nomedrec" name="nomedrec" class="form-control" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="namapasien">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" id="namapasien" name="namapasien" class="form-control" value="">
                    </div>
                </div>
                
            </div>
			<div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="idjenis">Poliklinik</label>
                    <div class="col-md-8">
                        <select id="idpoliklinik" name="idpoliklinik" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua Poliklinik -</option>
							<?foreach($list_poli as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>
							<?}?>
                        </select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="iddokter">Dokter</label>
                    <div class="col-md-8">
                        <select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua Dokter -</option>
							<?foreach($list_dokter as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>
							<?}?>
                        </select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="status_kirim">Status Kirim</label>
                    <div class="col-md-8">
                        <select id="status_kirim" name="status_kirim" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>-Semua Status -</option>
							<option value="1"  >Belum Kembali</option>
							<option value="2" >Sudah Kembali</option>
                        </select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-3 control-label" for="nomedrec">Tanggal</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
							<input class="form-control" type="text" id="tanggaldari"  name="tanggaldari" placeholder="From" value=""/>
							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
							<input class="form-control" type="text" id="tanggalsampai" name="tanggalsampai" placeholder="To" value=""/>
						</div>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="namapasien"></label>
                   <div class="col-md-4">
						<?php if (UserAccesForm($user_acces_form,array('1220'))){?>
                        <button class="btn btn-success text-uppercase" type="button" id="btn_filter" name="btn_filter" style="font-size:13px;width:100%;float:left;"><i class="fa fa-filter"></i> Filter </button>
						<?}?>
					</div>
					<div class="col-md-4" >
						<?php if (UserAccesForm($user_acces_form,array('1221'))){?>
                        <a href="{site_url}trm_pinjam_berkas" class="btn btn-primary text-uppercase" type="button" id="btn_tambah" name="btn_tambah" style="font-size:13px;width:100%;float:left;"><i class="fa fa-plus"></i> Peminjaman </a>
						<?}?>
                    </div>
                </div>
                
            </div>
			
            <?php echo form_close(); ?>
        </div>
    </div>
	
    <div class="block-content" id="div_tabel">
        <table class="table table-bordered table-striped" id="index_list" style="width: 100%;">
            <thead>
                <tr>
                    <th style="width:5%">#</th>
                    <th style="width:5%">Tanggal</th>
                    <th style="width:15%">Jenis</th>
                    <th style="width:5%">Status</th>
                    <th style="width:5%">No.Medrec</th>
                    <th style="width:5%">Pasien</th>
                    <th style="width:5%">Tujuan</th>
                    <th style="width:5%">Detail</th>
                    <th style="width:5%">Status</th>
                    <th style="width:15%">Durasi</th>
                    <th style="width:10%">User</th>
                    <th style="width:10%">User Kembali</th>
                    <th style="width:15%" align="center">
						<?php if (UserAccesForm($user_acces_form,array('1219'))){?>
						<div class="btn-group" role="group">
							<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu dropdown-menu-right">
								<li class="dropdown-header">Pilih Action</li>
								<li>
									<a tabindex="-1" class="kembali_semua" href="javascript:void(0)">Kembali Semua</a>
								</li>
								
							</ul>
						</div>
						<?}else{?>
							Aksi
						<?}?>
					</th>
                    <th style="width:0%">id_trx</th>
                    <th style="width:0%">jenis</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
	

</div>
<div class="modal" id="modal_kembali" role="dialog" aria-hidden="true" aria-labelledby="modalAdd" font size="15">
    <div class="modal-dialog modal-lg" style="width:30%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Pengembalian</h3>
                </div>
                <div class="block-content">
                    <form class="form-horizontal" id="form_detail">
                        <input type="hidden" id="id_trx"/>                        

                        <div class="form-group c" id="f_idtipe">
                            <label class="col-md-4 control-label">Tanggal & Waktu</label>
                            <div class="col-md-8">
								<div class="input-group">
                                    <input type="text" class="js-datetimepicker form-control" id="tanggalkembali" placeholder="" name="tanggalkembali" value="<?=date('d-m-Y H:i:s')?>" required="" aria-required="true" >
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" id="tutup" data-dismiss="modal">Tutup</button>
                <button class="btn btn-sm btn-success" type="button" id="simpan">Simpan</button>
            </div>
        </div>
    </div>
</div>
<!-- BATAS HAPUS -->

<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
    var table;
    $(document).ready(function() {   
		$('.js-datetimepicker').datetimepicker({
			format: 'DD-MM-YYYY HH:mm:ss'
		});
		// $("#waktu").datetimepicker({
			// format: "HH:mm",
			// stepping: 30
		// });
		// $("#modal_kembali").modal('show')
        // $(".number").number(true,0,'.',',');
		LoadIndex();
    });
	
	function LoadIndex(){
		var idjenis=$("#idjenis").val();
		var status_pasien=$("#status_pasien").val();
		var tujuan=$("#tujuan").val();
		var nomedrec=$("#nomedrec").val();
		var namapasien=$("#namapasien").val();
		var idpoliklinik=$("#idpoliklinik").val();
		var iddokter=$("#iddokter").val();
		var status_kirim=$("#status_kirim").val();
		var tanggaldari=$("#tanggaldari").val();
		var tanggalsampai=$("#tanggalsampai").val();
		// alert(tanggaldari);
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"order": [],
		"ajax": {
			url: '{site_url}trm_layanan_berkas/getKembali/',
			type: "POST",
			dataType: 'json',
			data: {
				idjenis: idjenis,
				status_pasien: status_pasien,
				tujuan: tujuan,
				nomedrec: nomedrec,
				namapasien: namapasien,
				idpoliklinik: idpoliklinik,
				iddokter: iddokter,
				status_kirim: status_kirim,
				tanggaldari: tanggaldari,
				tanggalsampai: tanggalsampai,
			}
		},
		columnDefs: [
			 { "width": "5%", "targets": [9] },
			 { "width": "3%", "targets": [0] },
			 { "width": "8%", "targets": [2,3,6] },
			 { "width": "8%", "targets": [1,4,12,8] },
			 { "width": "10%", "targets": [7,5] },
			 { "visible": false, "targets": [14,13] }
				]
		});
		
	}
	$(document).on("click", "#btn_filter", function() {
		LoadIndex();
	});
	$(document).on("click", ".kembali", function() {
		var table = $('#index_list').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,13).data()
		// alert();
		swal({
			title: "Anda Yakin ?",
			text : "Akan Mengembalikan Berkas ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			// pause_so();
			kembali(id);
		});
	});
	function kembali($id){
		var id=$id;		
		table = $('#index_list').DataTable()	
		$.ajax({
			url: '{site_url}trm_layanan_berkas/set_kembali',
			type: 'POST',
			data: {id: id},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Selesai Dikirim'});
				table.ajax.reload( null, false ); 
			}
		});
	}
	$(document).on("click", "#simpan", function() {
		
		swal({
			title: "Anda Yakin ?",
			text : "Akan Mengembalikan Berkas ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			// pause_so();
			kembali_edit();
		});
	});
	
	function kembali_edit(){
		var id=$("#id_trx").val();		
		var tanggalkembali=$("#tanggalkembali").val();	
		table = $('#index_list').DataTable()	
		// alert(id+' : '+tanggalkembali);
		$.ajax({
			url: '{site_url}trm_layanan_berkas/set_kembali_edit',
			type: 'POST',
			data: {id: id,tanggalkembali:tanggalkembali},
			complete: function() {
				$("#modal_kembali").modal('hide');
				$.toaster({priority : 'success', title : 'Succes!', message : ' Selesai Dikirim'});
				table.ajax.reload( null, false ); 
			}
		});
	}
	
	$(document).on("click", ".kembali_semua", function() {
		swal({
			title: "Anda Yakin ?",
			text : "Sudah Mengecek kesesuaian Data ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			kembali_semua();
		});
	});
	$(document).on("click", ".edit", function() {
		var table = $('#index_list').DataTable();
		tr = table.row($(this).parents('tr')).index();
		var id=table.cell(tr,13).data()
		$("#id_trx").val(id)
		$("#modal_kembali").modal('show')
	});
	
	function kembali_semua(){
		// alert('cek');
		var id;
		var st_valid='1';
		var arr_id=[];
		 // var table = $('#index_list').DataTable();		
		 // var data = table.rows().data();
		 // data.each(function (value, index) {
			 // if (table.cell(index,14).data()=='1'){
				 // st_valid='0';
			 // }
			
		 // });
		 // if (st_valid=='0'){
			 // sweetAlert("Maaf...", "Ada Berkas yang sudah dikirim!", "error");
             // return false;
		 // }
		 var table = $('#index_list').DataTable();		
		 var data = table.rows().data();
		 data.each(function (value, index) {
			 if (table.cell(index,14).data()=='1'){
				id=table.cell(index,13).data();
				arr_id.push(id);
			 }
			// console.log(id);
			
		 });
		 // alert(arr_id);return false;
		 setTimeout(function() {
			$.ajax({
				url: '{site_url}trm_layanan_berkas/kembali_semua',
				type: 'POST',
				data: {id: arr_id},
				complete: function() {
					
					$.toaster({priority : 'success', title : 'Succes!', message : ' Selesai Dikirim'});
					// $('#index_list').DataTable().ajax.reload()
				}
			});
		}, 200);
		 setTimeout(function() {
			table.ajax.reload( null, false ); 
			// sweetAlert("Maaf...", "Data Berhasil", "error");
		 }, 500);
		 
			 
		
	}
	//BATAS HAPUS
	
</script>