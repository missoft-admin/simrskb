<!DOCTYPE html>
<html>

<head>
	<title>Format Slip Penggunaan / Jasa Dokter</title>
	<style type="text/css">
		@page {
			size: 100mm 250mm;
            margin-top: 1.5em;
            margin-left: 1em;
            margin-bottom: 0.5em;
        }
		* {
			font-size: 24px;
		}
		body {

		}
		br {
		   display: block;
		   margin: 2px 0;
		}
		#batas {
		
			height: 9px;
		}
		table {
			
			border: 0px solid black;
			font-size: 24px !important;
			border-collapse: collapse !important;
            font-family: "Courier", Arial,sans-serif;
			/*font-family: Consolas, Menlo, Monaco, Lucida Console, Liberation Mono, DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace, serif;*/
		  }
		  td {
			border: 0px solid black;
			padding: 2px;
			height : 10px;
			vertical-align: middle;
		  }
		  .text-center{
			text-align: center !important;
		  }
		  .text-bold{
			font-weight: bold;
		  }
		  .border-thick-bottom{
			border-bottom:1px dashed #000 !important;
		  }
		  .border-thick-top{
			border-top:1px dashed #000 !important;
		  }
		  .font-antrian-top{
			border-left:1px solid #000 !important;
			border-right:1px solid #000 !important;
			border-top:1px solid #000 !important;
			font-size: 29px !important;
		  }
		  .font-antrian{
			border-left:1px solid #000 !important;
			border-right:1px solid #000 !important;
			border-bottom:1px solid #000 !important;
			font-size: 84px !important;
		  }
		  
		  .font-biasa-top{
			border-left:1px solid #000 !important;
			border-right:1px solid #000 !important;
			border-top:1px solid #000 !important;
			border-bottom:1px dashed #000 !important;
			font-size: 24px !important;
		  }
		  .font-biasa-bottom{
			border-left:1px solid #000 !important;
			border-right:1px solid #000 !important;
			border-bottom:1px solid #000 !important;
			
			font-size: 24px !important;
		  }.
		  .font-daftar{
			font-size: 26px !important;
		  }
		  
		  .border-left{
			border-left:1px solid #000 !important;
		  }
		  .border-right{
			border-right:1px solid #000 !important;
		  }
		  .border-bottom{
			border-bottom:1px solid #000 !important;
		  }
		  .border-top{
			border-top:1px solid #000 !important;
		  }
		  input[type=checkbox] { display: inline; }
		  .font-checkbox{
			font-size: 23px !important;
		  }
		  .border-full {
			border: 1px solid #000 !important;
			
		  }
	</style>
</head>

<body>
<table style="width:90mm;">
	<tr>
		<td class="text-bold text-center" colspan="3">RSKB HALMAHERA SIAGA <br>
							  JL.LLRE.Martadinata No.28<br>
								     Bandung, 40015<br></td>
	</tr>
</table>
<table style="width:90mm;">
	<tr>
		<td class="border-thick-bottom" colspan="3"></td>
	</tr>
	<tr>
		<td class="text-bold text-center font-daftar" colspan="3">SLIP PENGGUNAAN / JASA DOKTER</td>
	</tr>
	<tr>
		<td class="border-thick-top" colspan="3"></td>
	</tr>
	
</table>
<br>
<table style="width:90mm;">
	<tr>
		<td class="font-biasa-top text-center" style="width:60mm;"><?=$tipe_nama?> <br> <?=$namapoliklinik?></td>
		<td class="font-antrian-top text-center text-bold"  style="width:30mm;">ANTRIAN</td>
	</tr>
	<tr>
		<td class="font-biasa-bottom text-center" style="width:60mm;"><?=$namadokter?></td>
		<td class="font-antrian text-center text-bold"  style="width:30mm;"><?=$noantrian?></td>
	</tr>
</table>
<div id="batas"> </div>
<table style="width:90mm;" >
	<tr>
		<td style="width:30mm;" class="border-left border-top"> No. Pendaftaran</td>
		<td style="width:5mm;" class="border-top">:</td>
		<td style="width:55mm;" class="border-top border-right"><?=$nopendaftaran?></td>
	</tr>
	<tr>
		<td style="width:30mm;" class="border-left"> No. Medrec</td>
		<td style="width:5mm;">:</td>
		<td style="width:55mm;" class="border-right"><?=$no_medrec?></td>
	</tr>
	<tr>
		<td style="width:30mm;" class="border-left"> Nama Pasien</td>
		<td style="width:5mm;">:</td>
		<td style="width:55mm;" class="border-right"><?=$title.'. '.$namapasien?></td>
	</tr>
	<tr>
		<td style="width:30mm;" class="border-left"> Tanggal Lahir</td>
		<td style="width:5mm;">:</td>
		<td style="width:55mm;" class="border-right"><?=HumanDateShort($tanggal_lahir)?></td>
	</tr>
	
	
	<tr>
		<td style="width:30mm;" class="border-left"> Pembayaran</td>
		<td style="width:5mm;">:</td>
		<td style="width:55mm;" class="border-right"><?=$namakelompok?></td>
	</tr>
	<tr>
		<td style="width:30mm;" class="border-left border-bottom"> Rekanan</td>
		<td style="width:5mm;" class=" border-bottom">:</td>
		<td style="width:55mm;" class="border-right border-bottom"><?=$namarekanan?></td>
	</tr>
	
</table>
<table style="width:90mm;" >
	<tr>
		<td colspan="3" class="text-bold">PEMERIKSAAN PENUNJANG</td>
	</tr>
	<tr>
		<td style="width:30mm;" class="font-checkbox">
			<input type="checkbox"><label class="font-checkbox"> Radiologi</label>
		</td>
		<td style="width:35mm;" class="font-checkbox">
			<input type="checkbox"><label class="font-checkbox"> Laboratorium</label>
		</td>
		<td style="width:25mm;" class="font-checkbox">
			<input type="checkbox"><label class="font-checkbox"> Farmasi</label>
		</td>
	</tr>
	
</table>
<table style="width:90mm;" >
	
	<tr>
		<td colspan="3" class="text-bold">ADMINISTRASI</td>
	</tr>
	<tr>
		<td style="width:50mm;" colspan="2"  class="font-checkbox">
			<input type="checkbox"><label class="font-checkbox"> Biaya Pendaftaran</label>
		</td>
		<td style="width:40mm;" class="font-checkbox" >
			<input type="checkbox"><label class="font-checkbox"> Cetak Kartu</label>
		</td>
		
	</tr>
</table>
<table style="width:90mm;" >	
	<tr>
		<td colspan="2" class="text-bold">KONSULTASI</td>
	</tr>
	<tr>
		<td style="width:90mm;" class="font-checkbox">
			<input type="checkbox"><label class="font-checkbox"> Konsultasi dr. Spesialis</label>
		</td>
	</tr>
	<tr>
		<td style="width:90mm;" class="font-checkbox" >
			<input type="checkbox"><label class="font-checkbox"> Konsultasi dr. Sub Spesialis</label>
		</td>
	</tr>
	<tr>
		<td style="width:90mm;" class="font-checkbox" >
			<input type="checkbox"><label class="font-checkbox"> Konsultasi dr. Umum</label>
		</td>
	</tr>
</table>
<table style="width:90mm;" >	
	<tr>
		<td colspan="4" class="text-bold">TINDAKAN / PEMAKAIAN</td>
	</tr>
	<tr>
		<td style="width:8mm;" class="font-checkbox text-center border-full">NO</td>
		<td style="width:45mm;" class="font-checkbox text-center border-full">TINDAKAN/PEMAKAIAN</td>
		<td style="width:8mm;" class="font-checkbox text-center border-full">QTY</td>
		<td style="width:28mm;" class="font-checkbox text-center border-full">HARGA</td>
	</tr>
	<?for ($x = 1; $x <= 10; $x++) {?>
	<tr>
		<td style="width:8mm;" class="font-checkbox text-center border-full">&nbsp;<?=$x?></td>
		<td style="width:45mm;" class="font-checkbox text-center border-full">&nbsp;</td>
		<td style="width:8mm;" class="font-checkbox text-center border-full">&nbsp;</td>
		<td style="width:28mm;" class="font-checkbox text-center border-full">&nbsp;</td>
	</tr>
	<?}?>
</table>
<table style="width:90mm;" >	
	<tr>
		<td class="font-checkbox" style="width:50mm;">Cetak : <?= date("d-m-Y H:m:s")?>-<?=$namauserinput?></td>
		<td class="font-checkbox text-center" style="width:50mm;">Petugas</td>
	</tr>
	<tr>
		<td class="font-checkbox" style="width:50mm;"></td>
		<td class="font-checkbox text-center" style="width:40mm; ;height:25mm">
		<br>
		(............)</td>
	</tr>
	
</table>

</body>

</html>
