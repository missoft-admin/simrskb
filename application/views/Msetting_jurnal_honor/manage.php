<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}msetting_jurnal_honor" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('msetting_jurnal_honor/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>	
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_primary('Pengaturan Posting')?></label>
				<input type="hidden" class="form-control" id="tmp_idakun_kredit" placeholder="0" name="tmp_idakun_kredit" value="{idakun_kredit}">
				<div class="col-md-4">
					<select id="st_auto_posting" name="st_auto_posting" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="1" <?=(1 == $st_auto_posting ? 'selected' : ''); ?>>AUTO POSTING</option>
                            <option value="2" <?=(2 == $st_auto_posting ? 'selected' : ''); ?>>MANUAL</option>
                        </select>
				</div>
			</div>
			<?
				$q="SELECT * FROM `mdokter_kategori` M ORDER BY M.id ASC";
				$list_kategori=$this->db->query($q)->result();
			?>
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_primary('Batas Waktu Batal')?></label>
				
				<div class="col-md-4">
					<input type="text" style="width: 100%"  class="form-control number" id="batas_batal" placeholder="0" name="batas_batal" value="<?=$batas_batal?>">
				</div>
				<div class="col-md-2">
					<button type="button" class="btn btn-sm btn-success" tabindex="8" id="btn_update" title="Update"><i class="fa fa-save"></i> UPDATE</button>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-1 control-label"><h4><?=text_success('SETTING AKUN HONOR DOKTER')?></h4></label>
				
			</div>
					<input type="hidden" class="form-control" id="id_edit_honor" placeholder="0" name="id_edit_honor" value="">
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-2 control-label" >Kategori Dokter</label>
				
				<div class="col-md-4">
					<select name="idkategori" id="idkategori" style="width: 100%" id="step" class="js-select2 form-control input-sm">										
						<option value="#" selected>- Pilih Kategori -</option>																			
						<?foreach($list_kategori as $row){?>
						<option value="<?=$row->id?>"><?=$row->nama?></option>
						<?}?>																				
						
					</select>			
				</div>
				<label class="col-md-2 control-label" >Dokter</label>
				<div class="col-md-4">
					<select name="iddokter" id="iddokter" style="width: 100%" id="step" class="js-select2 form-control input-sm"></select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-2 control-label" >Akun Beban Jasa</label>
				
				<div class="col-md-4">
					<select name="idakun_beban" id="idakun_beban" style="width: 100%" class="js-select2 form-control input-sm idakun"></select>	
				</div>
				<label class="col-md-2 control-label" >Akun Pajak Dokter</label>
				<div class="col-md-4">
					<select name="idakun_pajak" id="idakun_pajak" style="width: 100%" class="js-select2 form-control input-sm idakun"></select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 5px;">
				<label class="col-md-2 control-label" >Akun Pembelian Obat</label>
				
				<div class="col-md-4">
					<select name="idakun_pembelian" id="idakun_pembelian" style="width: 100%" class="js-select2 form-control input-sm idakun"></select>
				</div>
				<label class="col-md-2 control-label" >Akun Berobat</label>
				<div class="col-md-4">
					<select name="idakun_berobat" id="idakun_berobat" style="width: 100%" class="js-select2 form-control input-sm idakun"></select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" >Akun Kasbon</label>
				
				<div class="col-md-4">
					<select name="idakun_kasbon" id="idakun_kasbon" style="width: 100%" class="js-select2 form-control input-sm idakun"></select>
				</div>
				<label class="col-md-2 control-label" >Akun Potongan Lain</label>
				<div class="col-md-4">
					<select name="idakun_potongan" id="idakun_potongan" style="width: 100%" class="js-select2 form-control input-sm idakun"></select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" ></label>
				
				<div class="col-md-4">
					<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_honor" title="Masukan Item"><i class="fa fa-save"></i> Simpan</button>
					<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_honor" title="Batalkan"><i class="fa fa-refresh"></i> Clear Input</button>
				</div>
				
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<table id="tabel_honor" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 0%;">#</th>															
								<th style="width: 8%;">Kategori Dokter</th>															
								<th style="width: 8%;">Dokter</th>															
								<th style="width: 11%;">Akun Beban Jasa</th>															
								<th style="width: 11%;">Akun Pajak Dokter</th>															
								<th style="width: 11%;">Akun Pembelian Obat</th>															
								<th style="width: 11%;">Akun Berobat</th>															
								<th style="width: 11%;">Akun Kasbon</th>															
								<th style="width: 11%;">Akun Potongan Lain</th>															
								<th style="width: 5%;">Actions</th>
							</tr>
							
						</thead>
						<tbody>
							
						</tbody>
						
				</table>
				</div>
			</div>
			
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
var table;
	$(document).ready(function() {
		$('.number').number(true, 0);
		// load_unit();
		list_akun();
		load_honor();
		
		clear_input_honor();

	});	
	$(document).on("change","#idkategori",function(){
		list_dokter($(this).val());
	});
	function list_dokter($id){
		
		$.ajax({
			url: '{site_url}msetting_jurnal_honor/list_dokter/'+$id,
			dataType: "json",
			success: function(data) {
				$("#iddokter").empty();
				$('#iddokter').append('<option value="0" selected>- Semua Dokter -</option>');
				$('#iddokter').append(data.detail);
			}
		});
		
	}
	$(document).on("click","#btn_update",function(){
		if (validate_update()==false)return false;
		var st_auto_posting=$("#st_auto_posting").val();
		var idakun_kredit=$("#idakun_kredit").val();
		var batas_batal=$("#batas_batal").val();
		$("#tmp_idakun_kredit").val(idakun_kredit)
		$.ajax({
			url: '{site_url}msetting_jurnal_honor/update',
			type: 'POST',
			data: {
				st_auto_posting:st_auto_posting,idakun_kredit:idakun_kredit,batas_batal:batas_batal,
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$("#idakun_kredit").val($("#tmp_idakun_kredit").val()).trigger('change');
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	function validate_update()
	{
		
		if ($("#st_auto_posting").val()=='#'){
			sweetAlert("Maaf...", "Setting Harus diisi", "error");
			return false;
		}
		if ($("#batas_batal").val()==''){
			sweetAlert("Maaf...", "Batas Waktu Batal Harus diisi", "error");
			return false;
		}
	
		return true;
	}
	
	function list_akun(){
		$.ajax({
			url: '{site_url}msetting_jurnal_honor/list_akun',
			dataType: "json",
			success: function(data) {
				$(".idakun").empty();
				$('.idakun').append('<option value="#" selected>- Pilih Akun -</option>');
				$('.idakun').append(data.detail);
				
			}
		});		
	}
	
	
	function validate_add_honor()
	{
			var idakun_beban=$("#idakun_beban").val();
			var idakun_pajak=$("#idakun_pajak").val();
			var idakun_pembelian=$("#idakun_pembelian").val();
			var idakun_berobat=$("#idakun_berobat").val();
			var idakun_kasbon=$("#idakun_kasbon").val();
			var idakun_potongan=$("#idakun_potongan").val();
		
		
		if (idakun_beban=='#'){
			sweetAlert("Maaf...", "No AKun Beban Harus diisi", "error");
			return false;
		}
		if (idakun_pajak=='#'){
			sweetAlert("Maaf...", "No AKun Pajak Harus diisi", "error");
			return false;
		}
		if (idakun_pembelian=='#'){
			sweetAlert("Maaf...", "No AKun Pembelian Harus diisi", "error");
			return false;
		}
		if (idakun_berobat=='#'){
			sweetAlert("Maaf...", "No AKun Berobat Harus diisi", "error");
			return false;
		}
		if (idakun_kasbon=='#'){
			sweetAlert("Maaf...", "No AKun Kasbon Harus diisi", "error");
			return false;
		}
		if (idakun_potongan=='#'){
			sweetAlert("Maaf...", "No AKun Potongan Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	function clear_input_honor(){
		$("#idkategori").val('#').trigger('change');	
		$("#iddokter").val('#').trigger('change');	
		$("#id_edit_honor").val('');		
		$("#idakun_beban").val('#').trigger('change');
		$("#idakun_pajak").val('#').trigger('change');
		$("#idakun_pembelian").val('#').trigger('change');
		$("#idakun_berobat").val('#').trigger('change');
		$("#idakun_kasbon").val('#').trigger('change');
		$("#idakun_potongan").val('#').trigger('change');
		// alert($("#idakun").val());
	}
	$(document).on("click","#simpan_honor",function(){
		if (validate_add_honor()==false)return false;
		var id_edit=$("#id_edit_honor").val();
		var iddokter=$("#iddokter").val();
		var idkategori=$("#idkategori").val();
		var idakun_beban=$("#idakun_beban").val();
		var idakun_pajak=$("#idakun_pajak").val();
		var idakun_pembelian=$("#idakun_pembelian").val();
		var idakun_berobat=$("#idakun_berobat").val();
		var idakun_kasbon=$("#idakun_kasbon").val();
		var idakun_potongan=$("#idakun_potongan").val();
	
		
		// alert('SINI');
		$.ajax({
			url: '{site_url}msetting_jurnal_honor/simpan_honor',
			type: 'POST',
			data: {
				id_edit:id_edit,idkategori:idkategori,iddokter:iddokter
				,idakun_beban:idakun_beban
				,idakun_pajak:idakun_pajak
				,idakun_pembelian:idakun_pembelian
				,idakun_berobat:idakun_berobat
				,idakun_kasbon:idakun_kasbon
				,idakun_potongan:idakun_potongan
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Berhasil disimpan'});
					$('#tabel_honor').DataTable().ajax.reload( null, false );
					clear_input_honor();
				}else{
					sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
					
				}
			}
		});
	});
	
	function hapus_honor($id){
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus Record?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}msetting_jurnal_honor/hapus_honor/'+$id,			
				success: function(data) {
					if (data=='true'){
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapuss disimpan'});
						$('#tabel_honor').DataTable().ajax.reload( null, false );
						clear_input_honor();
					}else{
						sweetAlert("Gagal...", "Penyimpanan!", "error");
						
					}
				}
			});
		});
	}
	$(document).on("click","#clear_honor",function(){
		
		clear_input_honor();
	});
	function load_honor(){
		var idlogic=$("#id").val();
		
		$('#tabel_honor').DataTable().destroy();
		var table = $('#tabel_honor').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}msetting_jurnal_honor/load_honor/',
			type: "POST",
			dataType: 'json',
			data: {
				idlogic: idlogic
			}
		},
		columnDefs: [
		{"targets": [0], "visible": false },
		// { "width": "30%", "targets": [0] },
		 // { "width": "60%", "targets": [1] },
		 // { "width": "10%", "targets": [3] },
					]
		});
	}
	
	
	
</script>
