<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>FAKTUR</title>
    <style>


    @page {
            margin-top: 1,8em;
            margin-left: 0.8em;
            margin-right: 1.5em;
            margin-bottom: 2.5em;
        }

      table {

		font-family: "Courier", Arial,sans-serif;
        font-size: 20px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
	  p{
		font-family: "Courier", Arial,sans-serif;
        font-size: 17px !important;
	  }
      h5{
		font-family: "Courier", Arial,sans-serif;
        font-size: 18px !important;
		font-weight:normal !important;
		margin-top: 0;
		margin-bottom: 0;
		margin-left: 0;
		margin-right: 0;
	  }
      td {
         padding: 0px;
		 border:0px solid #000 !important;
      }
	  .header {
		 font-family: "Courier", Arial,sans-serif;
		 padding-left: 1px;
		 font-size: 35px !important;
		 font-weight:bold;
      }
	  .detail {
		 font-size: 18px !important;
		 font-style: italic !important;
      }


      /* border-normal */

	  .text-header{
		font-size: 13px !important;

      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	  .border-top {
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border:1px solid #000 !important;
      }

	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-bottom-top-left {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
	  .text-top{
        vertical-align:top !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  fieldset {
			border:1px solid #000;
			border-radius:8px;
			box-shadow:0 0 10px #000;
		}
		legend {
			background:#fff;
		}
    }

	@media print {
		html, body {
			height: 99%;
		}
	}
    </style>
  </head>
  <body>
   <table>
      <tr>
		<td style="width:15%" rowspan="3" class="text-center"><img src="<?=base_url()?>assets/upload/logo/logo.png" alt="" style="width: 150px;height: 150px;"></td>
		<td colspan="7" class="text-center header">NOTA RINCIAN PEMBAYARAN</td>

	  </tr>


	  <tr>
		<td>NO REGISTER/NO MEDREC</td>
		<td>:</td>
		<td><?=$nokasir.'/'.$nomedrec?></td>
		<td></td>
		<td>DOKTER</td>
		<td>:</td>
		<td><?=$namadokter?></td>
	  </tr>
	  <tr>
		<td style="width:17%">TANGGAL & WAKTU</td>
		<td style="width:1%" >:</td>
		<td style="width:20%"><?=HumanDateLong($tanggal)?></td>
		<td style="width:1%" ></td>
		<td style="width:8%">PELAYANAN</td>
		<td style="width:1%" >:</td>
		<td style="width:20%"><?=$pelayanan?></td>
	  </tr>
	  <tr>
		<td rowspan="2" class="text-center">Jl. L. L. R.E. Martadinata No.28<br>Bandung<br>(022) 4206061</td>
		<td>NAMA PASIEN</td>
		<td style="width:1%" >:</td>
		<td><?=$namapasien?></td>
		<td></td>
		<td>POLIKLINIK</td>
		<td style="width:1%" >:</td>
		<td><?=$namapoli?></td>
	  </tr>
	  <tr>
		<td>ALAMAT</td>
		<td style="width:1%" >:</td>
		<td><?=$alamat?></td>
		<td></td>
		<td>PAYOR</td>
		<td style="width:1%" >:</td>
		<td><?=$payor?></td>
	  </tr>

  </table>
	<br>
  <?php $totalAdm = 0; ?>
  <?php $totalTindakanPoli = 0; ?>
  <?php $totalTindakanObat = 0; ?>
  <?php $totalTindakanAlkes = 0; ?>
  <?php $totalLab = 0; ?>
  <?php $totalRad = 0; ?>
  <?php $totalFisio = 0; ?>
  <?php $totalFarmasiObat = 0; ?>
  <?php $totalFarmasiAlkes = 0; ?>
  <?php $totalFarmasiImplan = 0; ?>
  <?php $total_nonracikan = 0; ?>
  <?php $total_racikan = 0; ?>

  <?php if($jenis_id!='N'){ ?>
  <?php if(COUNT($list_admin) > 0){ ?>
	<h5>ADMINISTRASI :</h5>
  <table class="content-2">
    <tr>
      <td class="border-bottom-top text-center" width="3%">NO</td>
      <!-- <td class="border-bottom-top-top text-left" width="25%">KODE</td> -->
      <td class="border-bottom-top text-center" width="25%">KETERANGAN</td>
      <td class="border-bottom-top text-center" width="10%">HARGA</td>
      <td class="border-bottom-top text-center" width="5%">QTY</td>
      <td class="border-bottom-top text-center" width="10%">DISKON (%)</td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH</td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_admin as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-bottom text-center"><?=strtoupper(($number))?>. </td>
        <!-- <td class="border-bottom text-left"></td> -->
        <td class="border-bottom text-left"><?=$row->namatarif?></td>
        <td class="border-bottom text-right"><?= number_format($row->subtotal, 0)?>  </td>
        <td class="border-bottom text-center"><?= number_format($row->kuantitas, 0)?>  </td>
        <td class="border-bottom text-center"><?= number_format($row->diskon, 0)?> %  </td>
        <td class="border-bottom text-right"><?= number_format($row->total, 0) ?>  </td>
      </tr>
      <?php $totalAdm = $totalAdm + $row->total; ?>
    <?php } ?>
    <tr>
      <td class="text-right" colspan="4"><h5>SUB TOTAL ADMINISTRASI</h5>  </td>
      <td class="text-center">  </td>
      <td class="text-right"><?=number_format($totalAdm)?>  </td>
    </tr>
  </table>
  <br>

  <?php } ?>
  <?php if(COUNT($list_tindakan) > 0){ ?>
	<h5>TINDAKAN POLIKLINIK / IGD :</h5>
  <table>
    <tr>
      <td class="border-bottom-top text-center" width="3%">NO  </td>
      <!-- <td class="border-bottom-top-top text-left" width="25%">KODE  </td> -->
      <td class="border-bottom-top text-center" width="25%">KETERANGAN  </td>
      <td class="border-bottom-top text-center" width="10%">HARGA  </td>
      <td class="border-bottom-top text-center" width="5%">QTY  </td>
      <td class="border-bottom-top text-center" width="10%">DISKON (%)  </td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH  </td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_tindakan as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-bottom text-center text-top"><?=strtoupper(($number))?>.   </td>
        <!-- <td class="border-bottom text-left">  </td> -->
        <td class="border-bottom text-left text-top"><?=$row->namatarif.'<br> <span class="detail">&nbsp; - Jasa Sarana Rp.'.number_format($row->jasasarana).'<br>&nbsp; - Jasa Pelayanan Rp.'.number_format($row->jasapelayanan).'<br>&nbsp; - BHP Rp.'.number_format($row->bhp).'<br>&nbsp; - Jasa Perawatan Rp.'.number_format($row->biayaperawatan).'</span>'?>  </td>
        <td class="border-bottom text-right text-top"><?= number_format($row->total, 0)?>  </td>
        <td class="border-bottom text-center text-top"><?= number_format($row->kuantitas, 0) ?>  </td>
        <td class="border-bottom text-center text-top"><?= number_format($row->diskon, 2)?> %  </td>
        <td class="border-bottom text-right text-top"><?= number_format($row->totalkeseluruhan, 0) ?>  </td>
      </tr>
      <?php $totalTindakanPoli = $totalTindakanPoli + $row->totalkeseluruhan; ?>
    <?php } ?>
    <tr>
      <td class="text-right" colspan="4"><h5>SUB TOTAL TINDAKAN POLIKLINIK / IGD</h5>  </td>
      <td class="text-center">  </td>
      <td class="text-right"><?=number_format($totalTindakanPoli)?>  </td>
    </tr>
  </table>
  <br>
  <?php } ?>
  <?php if(COUNT($list_igd_obat) > 0){ ?>
	<h5>POLIKLINIK / IGD OBAT :</h5>
  <table class="content-2">
    <tr>
     <td class="border-bottom-top text-center" width="3%">NO  </td>
      <!-- <td class="border-bottom-top-top text-left" width="25%">KODE  </td> -->
      <td class="border-bottom-top text-center" width="5%">  </td>
      <td class="border-bottom-top text-center" width="20%">KETERANGAN  </td>
      <td class="border-bottom-top text-center" width="10%">HARGA  </td>
      <td class="border-bottom-top text-center" width="5%">QTY  </td>
      <td class="border-bottom-top text-center" width="10%">DISKON (%)  </td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH  </td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_igd_obat as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-bottom text-center"><?=strtoupper(($number))?>.   </td>
        <!-- <td class="border-bottom text-left">  </td> -->
        <td class="border-bottom text-left"><?=$row->kode?>  </td>
        <td class="border-bottom text-left"><?=$row->namatarif?>  </td>
        <td class="border-bottom text-right"><?= number_format($row->hargajual, 0)?>  </td>
		<td class="border-bottom text-center"><?= number_format($row->kuantitas, 0) ?>  </td>
        <td class="border-bottom text-center"><?= number_format($row->diskon, 2)?> %  </td>
        <td class="border-bottom text-right"><?= number_format($row->totalkeseluruhan, 0) ?>  </td>
      </tr>
      <?php $totalTindakanObat = $totalTindakanObat + $row->totalkeseluruhan; ?>
    <?php } ?>
    <tr>
      <td class="text-right" colspan="5"><h5>SUB TOTAL POLIKLINIK / IGD OBAT</h5>  </td>
      <td class="text-center">  </td>
      <td class="text-right"><?=number_format($totalTindakanObat)?>  </td>
    </tr>
  </table>
  <br>
  <?php } ?>
  <?php if(COUNT($list_igd_alkes) > 0){ ?>
	<h5>POLIKLINIK / IGD ALKES :</h5>
  <table class="content-2">
    <tr>
      <td class="border-bottom-top text-center" width="3%">NO  </td>
      <!-- <td class="border-bottom-top-top text-left" width="25%">KODE  </td> -->
      <td class="border-bottom-top text-center" width="5%">  </td>
      <td class="border-bottom-top text-center" width="20%">KETERANGAN  </td>
      <td class="border-bottom-top text-center" width="10%">HARGA  </td>
      <td class="border-bottom-top text-center" width="5%">QTY  </td>
      <td class="border-bottom-top text-center" width="10%">DISKON (%)  </td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH  </td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_igd_alkes as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-bottom text-center"><?=strtoupper(($number))?>.   </td>
        <!-- <td class="border-bottom text-left">  </td> -->
        <td class="border-bottom text-left"><?=$row->kode?>  </td>
        <td class="border-bottom text-left"><?=$row->namatarif?>  </td>
        <td class="border-bottom text-right"><?= number_format($row->hargajual, 0)?>  </td>
		<td class="border-bottom text-center"><?= number_format($row->kuantitas, 0) ?>  </td>
        <td class="border-bottom text-center"><?= number_format($row->diskon, 2)?> %  </td>
        <td class="border-bottom text-right"><?= number_format($row->totalkeseluruhan, 0) ?>  </td>
      </tr>
      <?php $totalTindakanAlkes = $totalTindakanAlkes + $row->totalkeseluruhan; ?>
    <?php } ?>
    <tr>
      <td class="text-right" colspan="5"><h5>SUB TOTAL POLIKLINIK / IGD ALKES</h5>  </td>
      <td class="text-center">  </td>
      <td class="text-right"><?=number_format($totalTindakanAlkes)?>  </td>
    </tr>
  </table>
  <br>
  <?php } ?>
  <?php if(COUNT($list_lab) > 0){ ?>
	<h5>LABORATORIUM :</h5>
  <table class="content-2">
    <tr>
      <td class="border-bottom-top text-center" width="3%">NO  </td>
      <!-- <td class="border-bottom-top-top text-left" width="25%">KODE  </td> -->
      <td class="border-bottom-top text-center" width="25%">KETERANGAN  </td>
      <td class="border-bottom-top text-center" width="10%">HARGA  </td>
      <td class="border-bottom-top text-center" width="5%">QTY  </td>
      <td class="border-bottom-top text-center" width="10%">DISKON (%)  </td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH  </td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_lab as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-bottom text-center"><?=strtoupper(($number))?>.   </td>
        <!-- <td class="border-bottom text-left">  </td> -->
        <td class="border-bottom text-left"><?=$row->namatarif?>  </td>
        <td class="border-bottom text-right"><?= number_format($row->total, 0)?>  </td>
		<td class="border-bottom text-center"><?= number_format($row->kuantitas, 0) ?>  </td>
        <td class="border-bottom text-center"><?= number_format($row->diskon, 2)?> %  </td>
        <td class="border-bottom text-right"><?= number_format($row->totalkeseluruhan, 0) ?>  </td>
      </tr>
      <?php $totalLab = $totalLab + $row->totalkeseluruhan; ?>
    <?php } ?>
    <tr>
      <td class="text-right" colspan="4"><h5>SUB TOTAL LABORATORIUM</h5>  </td>
      <td class="text-center">  </td>
      <td class="text-right"><?=number_format($totalLab)?>  </td>
    </tr>
  </table>
  <br>
  <?php } ?>
  <?php if(COUNT($list_radiologi) > 0){ ?>
	<h5>RADIOLOGI :</h5>
  <table class="content-2">
    <tr>
      <td class="border-bottom-top text-center" width="3%">NO  </td>
      <!-- <td class="border-bottom-top-top text-left" width="25%">KODE  </td> -->
      <td class="border-bottom-top text-center" width="25%">KETERANGAN  </td>
      <td class="border-bottom-top text-center" width="10%">HARGA  </td>
      <td class="border-bottom-top text-center" width="5%">QTY  </td>
      <td class="border-bottom-top text-center" width="10%">DISKON (%)  </td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH  </td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_radiologi as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-bottom text-center"><?=strtoupper(($number))?>.   </td>
        <!-- <td class="border-bottom text-left">  </td> -->
        <td class="border-bottom text-left"><?=$row->namatarif?>  </td>
        <td class="border-bottom text-right"><?= number_format($row->total, 0)?>  </td>
		<td class="border-bottom text-center"><?= number_format($row->kuantitas, 0) ?>  </td>
        <td class="border-bottom text-center"><?= number_format($row->diskon, 2)?> %  </td>
        <td class="border-bottom text-right"><?= number_format($row->totalkeseluruhan, 0) ?>  </td>
      </tr>
      <?php $totalRad = $totalRad + $row->totalkeseluruhan; ?>
    <?php } ?>
    <tr>
      <td class="text-right" colspan="4"><h5>SUB TOTAL RADIOLOGI</h5>  </td>
      <td class="text-center">  </td>
      <td class="text-right"><?=number_format($totalRad)?>  </td>
    </tr>
  </table>
  <?php } ?>
  <?php if(COUNT($list_fisio) > 0){ ?>
	<h5>FISIOTERAPI :</h5>
  <table class="content-2">
    <tr>
      <td class="border-bottom-top text-center" width="3%">NO  </td>
      <!-- <td class="border-bottom-top-top text-left" width="25%">KODE  </td> -->
      <td class="border-bottom-top text-center" width="25%">KETERANGAN  </td>
      <td class="border-bottom-top text-center" width="10%">HARGA  </td>
      <td class="border-bottom-top text-center" width="5%">QTY  </td>
      <td class="border-bottom-top text-center" width="10%">DISKON (%)  </td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH  </td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_fisio as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-bottom text-center"><?=strtoupper(($number))?>.   </td>
        <!-- <td class="border-bottom text-left">  </td> -->
        <td class="border-bottom text-left"><?=$row->namatarif?>  </td>
        <td class="border-bottom text-right"><?= number_format($row->total, 0)?>  </td>
		<td class="border-bottom text-center"><?= number_format($row->kuantitas, 0) ?>  </td>
        <td class="border-bottom text-center"><?= number_format($row->diskon, 2)?> %  </td>
        <td class="border-bottom text-right"><?= number_format($row->totalkeseluruhan, 0) ?>  </td>
      </tr>
      <?php $totalFisio = $totalFisio + $row->totalkeseluruhan; ?>
    <?php } ?>
    <tr>
      <td class="text-right" colspan="4"><h5>SUB TOTAL FISIOTERAPI</h5>  </td>
      <td class="text-center">  </td>
      <td class="text-right"><?=number_format($totalFisio)?>  </td>
    </tr>
  </table>
  <br>
  <?php } ?>
  <?php if(COUNT($list_farmasi_obat) > 0){ ?>
	<h5>FARMASI OBAT :</h5>
  <table class="content-2">
    <tr>
      <td class="border-bottom-top text-center" width="3%">NO  </td>
      <!-- <td class="border-bottom-top-top text-left" width="25%">KODE  </td> -->
      <td class="border-bottom-top text-center" width="5%">  </td>
      <td class="border-bottom-top text-center" width="20%">KETERANGAN  </td>
      <td class="border-bottom-top text-center" width="10%">HARGA  </td>
      <td class="border-bottom-top text-center" width="5%">QTY  </td>
      <td class="border-bottom-top text-center" width="10%">DISKON (%)  </td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH  </td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_farmasi_obat as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-bottom text-center"><?=strtoupper(($number))?>.   </td>
        <!-- <td class="border-bottom text-left">  </td> -->
		<td class="border-bottom text-left"><?=$row->kode?>  </td>
        <td class="border-bottom text-left"><?=$row->namatarif?>  </td>
        <td class="border-bottom text-right"><?= number_format($row->hargajual, 0)?>  </td>
		<td class="border-bottom text-center"><?= number_format($row->kuantitas, 0) ?>  </td>
        <td class="border-bottom text-center"><?= number_format($row->diskon, 2)?> %  </td>
        <td class="border-bottom text-right"><?= number_format($row->totalkeseluruhan, 0) ?>  </td>
      </tr>
      <?php $totalFarmasiObat = $totalFarmasiObat + $row->totalkeseluruhan; ?>
    <?php } ?>
    <tr>
      <td class="text-right" colspan="5"><h5>SUB TOTAL FARMASI OBAT</h5>  </td>
      <td class="text-center">  </td>
      <td class="text-right"><?=number_format($totalFarmasiObat)?>  </td>
    </tr>
  </table>
  <br>
  <?php } ?>
  <?php if(COUNT($list_farmasi_alkes) > 0){ ?>
	<h5>FARMASI ALKES :</h5>
  <table class="content-2">
    <tr>
      <td class="border-bottom-top text-center" width="3%">NO  </td>
      <!-- <td class="border-bottom-top-top text-left" width="25%">KODE  </td> -->
      <td class="border-bottom-top text-center" width="5%">  </td>
      <td class="border-bottom-top text-center" width="20%">KETERANGAN  </td>
      <td class="border-bottom-top text-center" width="10%">HARGA  </td>
      <td class="border-bottom-top text-center" width="5%">QTY  </td>
      <td class="border-bottom-top text-center" width="10%">DISKON (%)  </td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH  </td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_farmasi_alkes as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-bottom text-center"><?=strtoupper(($number))?>.   </td>
        <!-- <td class="border-bottom text-left">  </td> -->
        <td class="border-bottom text-left"><?=$row->kode?>  </td>
        <td class="border-bottom text-left"><?=$row->namatarif?>  </td>
        <td class="border-bottom text-right"><?= number_format($row->hargajual, 0)?>  </td>
		<td class="border-bottom text-center"><?= number_format($row->kuantitas, 0) ?>  </td>
        <td class="border-bottom text-center"><?= number_format($row->diskon, 2)?> %  </td>
        <td class="border-bottom text-right"><?= number_format($row->totalkeseluruhan, 0) ?>  </td>
      </tr>
      <?php $totalFarmasiAlkes = $totalFarmasiAlkes + $row->totalkeseluruhan; ?>
    <?php } ?>
    <tr>
      <td class="text-right" colspan="5"><h5>SUB TOTAL FARMASI ALKES</h5>  </td>
      <td class="text-center">  </td>
      <td class="text-right"><?=number_format($totalFarmasiAlkes)?>  </td>
    </tr>
  </table>
  <br>
  <?php } ?>
  <?php if(COUNT($list_farmasi_implan) > 0){ ?>
	<h5>FARMASI IMPLAN :</h5>
  <table class="content-2">
    <tr>
      <td class="border-bottom-top text-center" width="3%">NO  </td>
      <!-- <td class="border-bottom-top-top text-left" width="25%">KODE  </td> -->
      <td class="border-bottom-top text-center" width="5%">  </td>
      <td class="border-bottom-top text-center" width="20%">KETERANGAN  </td>
      <td class="border-bottom-top text-center" width="10%">HARGA  </td>
      <td class="border-bottom-top text-center" width="5%">QTY  </td>
      <td class="border-bottom-top text-center" width="10%">DISKON (%)  </td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH  </td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_farmasi_implan as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-bottom text-center"><?=strtoupper(($number))?>.   </td>
        <!-- <td class="border-bottom text-left">  </td> -->
        <td class="border-bottom text-left"><?=$row->kode?>  </td>
        <td class="border-bottom text-left"><?=$row->namatarif?>  </td>
        <td class="border-bottom text-right"><?= number_format($row->hargajual, 0)?>  </td>
		<td class="border-bottom text-center"><?= number_format($row->kuantitas, 0) ?>  </td>
        <td class="border-bottom text-center"><?= number_format($row->diskon, 2)?> %  </td>
        <td class="border-bottom text-right"><?= number_format($row->totalkeseluruhan, 0) ?>  </td>
      </tr>
      <?php $totalFarmasiImplan = $totalFarmasiImplan + $row->totalkeseluruhan; ?>
    <?php } ?>
    <tr>
      <td class="text-right" colspan="5"><h5>SUB TOTAL FARMASI IMPLAN</h5>  </td>
      <td class="text-center">  </td>
      <td class="text-right"><?=number_format($totalFarmasiImplan)?>  </td>
    </tr>
  </table>
  <br>
  <?php } ?>
  <?php if(COUNT($list_farmasi_racikan) > 0){ ?>
		<h5> OBAT - RACIKAN :</h5>
		<table class="content-2">
		<tr>
		  <td class="border-bottom-top text-center" width="3%">NO  </td>
		  <!-- <td class="border-bottom-top-top text-left" width="25%">KODE  </td> -->
		  <td class="border-bottom-top text-center" width="20%">KETERANGAN  </td>
		  <td class="border-bottom-top text-center" width="10%">HARGA  </td>
		  <td class="border-bottom-top text-center" width="5%">QTY  </td>
		  <td class="border-bottom-top text-center" width="10%">DISKON (%)  </td>
		  <td class="border-bottom-top text-center" width="10%">TUSLAH  </td>
		  <td class="border-bottom-top text-center" width="15%">JUMLAH  </td>
		</tr>
		<?php $number = 0; ?>
		<?php foreach  ($list_farmasi_racikan as $row) { ?>
		  <?php $number = $number + 1; ?>
		  <tr>
			<td class="border-bottom text-center"><?=strtoupper(($number))?>.   </td>
			<!-- <td class="border-bottom text-left">  </td> -->
			<td class="border-bottom text-left"><?=$row->namatarif?>  </td>
			<td class="border-bottom text-right"><?= number_format($row->hargajual, 0)?>  </td>
			<td class="border-bottom text-center"><?= number_format($row->kuantitas, 0) ?>  </td>
			<td class="border-bottom text-center">0 %  </td>
			<td class="border-bottom text-center"><?= number_format($row->tuslah, 0) ?>  </td>
			<td class="border-bottom text-right"><?= number_format($row->totalkeseluruhan, 0) ?>  </td>
		  </tr>
		  <?php $total_racikan = $total_racikan + $row->totalkeseluruhan; ?>
		<?php } ?>
		<tr>
		  <td class="text-right" colspan="5"><h5>SUB TOTAL RACIKAN</h5>  </td>
		  <td class="text-center">  </td>
		  <td class="text-right"><?=number_format($total_racikan)?>  </td>
		</tr>
	  </table>
	  <br>
	  <?php } ?>
  <?php }else{ ?>
		<?php if(COUNT($list_non_racikan) > 0){ ?>
		<h5>NON RACIKAN :</h5>
		<table class="content-2">
		<tr>
		  <td class="border-bottom-top text-center" width="3%">NO  </td>
		  <!-- <td class="border-bottom-top-top text-left" width="25%">KODE  </td> -->
		  <td class="border-bottom-top text-center" width="5%">  </td>
		  <td class="border-bottom-top text-center" width="20%">KETERANGAN  </td>
		  <td class="border-bottom-top text-center" width="10%">HARGA  </td>
		  <td class="border-bottom-top text-center" width="5%">QTY  </td>
		  <td class="border-bottom-top text-center" width="10%">DISKON (%)  </td>
		  <td class="border-bottom-top text-center" width="15%">JUMLAH  </td>
		</tr>
		<?php $number = 0; ?>
		<?php foreach  ($list_non_racikan as $row) { ?>
		  <?php $number = $number + 1; ?>
		  <tr>
			<td class="border-bottom text-center"><?=strtoupper(($number))?>.   </td>
			<!-- <td class="border-bottom text-left">  </td> -->
			<td class="border-bottom text-left"><?=$row->kode?>  </td>
			<td class="border-bottom text-left"><?=$row->namatarif?>  </td>
			<td class="border-bottom text-right"><?= number_format($row->harga, 0)?>  </td>
			<td class="border-bottom text-center"><?= number_format($row->kuantitas, 0) ?>  </td>
			<td class="border-bottom text-center"><?= number_format($row->diskon, 2)?> %  </td>
			<td class="border-bottom text-right"><?= number_format($row->totalharga, 0) ?>  </td>
		  </tr>
		  <?php $total_nonracikan = $total_nonracikan + $row->totalharga; ?>
		<?php } ?>
		<tr>
		  <td class="text-right" colspan="5"><h5>SUB TOTAL NON RACIKAN</h5>  </td>
		  <td class="text-center">  </td>
		  <td class="text-right"><?=number_format($total_nonracikan)?>  </td>
		</tr>
	  </table>
	  <br>
	  <?php } ?>
	  <?php if(COUNT($list_racikan) > 0){ ?>
		<h5> RACIKAN :</h5>
		<table class="content-2">
		<tr>
		  <td class="border-bottom-top text-center" width="3%">NO  </td>
		  <!-- <td class="border-bottom-top-top text-left" width="25%">KODE  </td> -->
		  <td class="border-bottom-top text-center" width="20%">KETERANGAN  </td>
		  <td class="border-bottom-top text-center" width="10%">HARGA  </td>
		  <td class="border-bottom-top text-center" width="5%">QTY  </td>
		  <td class="border-bottom-top text-center" width="10%">DISKON (%)  </td>
		  <td class="border-bottom-top text-center" width="10%">TUSLAH  </td>
		  <td class="border-bottom-top text-center" width="15%">JUMLAH  </td>
		</tr>
		<?php $number = 0; ?>
		<?php foreach  ($list_racikan as $row) { ?>
		  <?php $number = $number + 1; ?>
		  <tr>
			<td class="border-bottom text-center"><?=strtoupper(($number))?>.   </td>
			<!-- <td class="border-bottom text-left">  </td> -->
			<td class="border-bottom text-left"><?=$row->namatarif?>  </td>
			<td class="border-bottom text-right"><?= number_format($row->harga, 0)?>  </td>
			<td class="border-bottom text-center"><?= number_format($row->kuantitas, 0) ?>  </td>
			<td class="border-bottom text-center">0 %  </td>
			<td class="border-bottom text-center"><?= number_format($row->tuslah, 0) ?>  </td>
			<td class="border-bottom text-right"><?= number_format($row->totalharga, 0) ?>  </td>
		  </tr>
		  <?php $total_racikan = $total_racikan + $row->totalharga; ?>
		<?php } ?>
		<tr>
		  <td class="text-right" colspan="5"><h5>SUB TOTAL RACIKAN</h5>  </td>
		  <td class="text-center">  </td>
		  <td class="text-right"><?=number_format($total_racikan)?>  </td>
		</tr>
	  </table>
	  <br>
	  <?php } ?>
  <?php } ?>
  </table>
  <?php $totalAll = $totalAdm + $totalTindakanPoli + $totalTindakanObat + $totalTindakanAlkes + $totalLab + $totalRad + $totalFisio + $totalFarmasiObat + $totalFarmasiAlkes+ $totalFarmasiImplan + $total_nonracikan + $total_racikan;
  $totalAll=ceiling($totalAll,100);
  ?>
  <div style="clear:both; position:relative;">
		<div style="position:absolute; left:10pt; width:275pt;">
			<table>
				<tr>
				  <td colspan="2"  class="border-full">Terbilang : <br> <i><?=strtoupper(terbilang($totalAll))?> RUPIAH</i>  </td>
				</tr>
				<tr>
				  <td colspan="2" class="text-left">*Faktur ini bukan merupakan kwitansi yang sah  </td>
				</tr>
				<tr>
				  <td colspan="2" class="text-left">*Obat yang sudah dibeli tidak dapat dikembalikan lagi  </td>
				</tr>
				<tr>
				  <td width="50%" class="text-center"> <br>Paraf Kasir<u><br><br><br><br><?=$nama_kasir?></u>  </td>
				  <td width="50%">  </td>
				</tr>
				<br>
				<br>
				<br>
				<br>
				<br>
				<p><u>Cetakan Ke-<?=$jml_cetak?>\UC:<?=$user_cetak?> \ <?=HumanDateLong(date('d-m-Y H:i:s'))?></u></p>
			</table>
		</div>
		<div style="margin-left:300pt;">
			<table>
				<tr>
				  <td class="border-bottom text-left"><h5>GRAND TOTAL</h5>  </td>
				  <td class="border-bottom text-center">:  </td>
				  <td class="border-bottom text-right"><?=number_format($totalAll)?>  </td>
				</tr>

				<tr>
				  <td colspan="3" class="text-left"><h5>PEMBAYARAN</h5>  </td>

				</tr>
				<?foreach($list_bayar as $row){?>
				<tr>
				  <td class="text-left"><h5><?=$row->keterangan?></h5>  </td>
				  <td class="text-center">:  </td>
				  <td class="text-right"><?=number_format($row->nominal)?>  </td>
				</tr>
				<?}?>
				<tr>
				  <td class="border-top text-left"><h5>KEMBALI</h5>  </td>
				  <td class="border-top text-center">:  </td>
				  <td class="border-top text-right"><?=number_format($kembalian)?>  </td>
				</tr>

			</table>
		</div>
	</div>
    <script type="text/javascript">
	// alert('TS');
		// try {
			// window.print();
		// }
		// catch(e) {
			// window.onload = window.print;
		// }
	</script>

  </body>
</html>
