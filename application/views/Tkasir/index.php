<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('250'))){ ?>
<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<li class="<?=($tab==0)?'active':''?>">
			<a href="#tabRawatJalan">Poliklinik & IGD</a>
		</li>
		<li class="<?=($tab==1)?'active':''?>">
			<a href="#tabRetur">Transaksi Retur</a>
		</li>
	</ul>
	<div class="block-content tab-content">
		<div class="tab-pane fade <?=($tab==0)?'in active':''?>" id="tabRawatJalan">

			<hr style="margin-top:0px">

			<div class="row">
				<?php echo form_open('tkasir/filter','class="form-horizontal" id="form-work"') ?>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">No Register</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="noreg" name="noreg" value="{noreg}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">No Medrec</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="nomedrec" name="nomedrec" value="{nomedrec}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Nama Pasien</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="namapasien" name="namapasien" value="{namapasien}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Tipe Transaksi</label>
						<div class="col-md-8">
							<select id="idasalpasien" name="idasalpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="999" <?=($idasalpasien == 999 ? 'selected':'')?>>Semua Transaksi</option>
								<option value="1" <?=($idasalpasien == '1' ? 'selected' : '')?>>Poliklinik</option>
								<option value="2" <?=($idasalpasien == '2' ? 'selected' : '')?>>Instalasi Gawat Darurat</option>
								<option value="3" <?=($idasalpasien == '3' ? 'selected' : '')?>>Obat Bebas</option>
							</select>
						</div>
					</div>
					<div class="form-group">
					  <label class="col-md-4 control-label" for="example-daterange1">Tanggal</label>
					  <div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
							<input class="form-control" type="text" id="tanggaldari" name="tanggaldari" placeholder="From" value="{tanggaldari}">
							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
							<input class="form-control" type="text" id="tanggalsampai" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
						</div>
					  </div>
				  </div>

				<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for=""></label>
						<div class="col-md-8">
						<?=text_default('TIDAK DIRUJUK')?> <?=text_warning('MENUNGGU DITINDAK')?> <?=text_success('SUDAH DITINDAK')?> <?=text_danger('DIBATALKAN')?>

						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Kelompok Pasien</label>
						<div class="col-md-8">
							<select id="idkelompokpasien" name="idkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="999" <?=($idkelompokpasien == 999 ? 'selected':'')?>>Semua Kelompok Pasien</option>
							<?php foreach (get_all('mpasien_kelompok') as $row){?>
							  <option value="<?=$row->id ?>" <?=($idkelompokpasien == $row->id ? 'selected':'')?>><?=$row->nama ?></option>
							<?php } ?>
						  </select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Poliklinik</label>
						<div class="col-md-8">
							<select id="idpoliklinik" name="idpoliklinik" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="999" <?=($idpoliklinik == 999 ? 'selected':'')?>>Semua Poliklinik</option>
							<?php foreach (get_all('mpoliklinik') as $row){?>
							  <option value="<?=$row->id ?>" <?=($idpoliklinik == $row->id ? 'selected':'')?>><?=$row->nama ?></option>
							<?php } ?>
						  </select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Dokter</label>
						<div class="col-md-8">
							<select id="iddokter" name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="999" <?=($iddokter == 999 ? 'selected':'')?>>Semua Dokter</option>
							<?php foreach (get_all('mdokter') as $row){?>
							  <option value="<?=$row->id ?>" <?=($iddokter == $row->id ? 'selected':'')?>><?=$row->nama ?></option>
							<?php } ?>
						  </select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Status</label>
						<div class="col-md-8">
							<select id="status" name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="999" <?=($status == 999 ? 'selected':'')?>>Semua Status</option>
								<option value="0" <?=($status == 0 ? 'selected':'')?>>Dibatalkan</option>
								<option value="1" <?=($status == 1 ? 'selected':'')?>>Menunggu Diproses</option>
								<option value="2" <?=($status == 2 ? 'selected':'')?>>Telah Diproses</option>
							  </select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;" hidden>
						<label class="col-md-4 control-label" for="">Tipe Transaksi</label>
						<div class="col-md-8">
							<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="999" <?=($idtipe == 999 ? 'selected':'')?>>Semua Transaksi</option>
								<option value="1" <?=($idtipe == 1 ? 'selected':'')?>>Poliklinik</option>
								<option value="2" <?=($idtipe == 2 ? 'selected':'')?>>IGD</option>
								<option value="3" <?=($idtipe == 3 ? 'selected':'')?>>Obat Bebas</option>
							  </select>
						</div>
					</div>
					<?php if (UserAccesForm($user_acces_form,array('1071'))){ ?>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for=""></label>
						<div class="col-md-8">
							<button class="btn btn-success text-uppercase" type="button" onclick="load_index()" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
						</div>
					</div>
					<?}?>
				</div>
				<?php echo form_close() ?>
			</div>

			<hr style="margin-top:10px">

			<table width="100%" id="datatable-simrs" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>No.</th>
						<th>Tanggal/Jam</th>
						<th>No. Register</th>
						<th>No. Medrec</th>
						<th>Nama Pasien</th>
						<th>Poliklinik</th>
						<th>Kelompok Pasien</th>
						<th>Rujukan</th>
						<th>Aksi</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>

	</div>
</div>
<?}?>
<div class="modal" id="modalPembatalan" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="formPembatalan"  class=" form-horizontal push-10-t" action="{base_url}/tkasir/batalkan/{idkasir}" method="post">
				<div class="block block-themed block-transparent remove-margin-b">
					<div class="block-header bg-danger">
						<ul class="block-options">
							<li>
								<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
							</li>
						</ul>
						<h3 class="block-title">Konfirmasi Pembatalan</h3>
					</div>
					<div class="block-content">
						<h4>Anda Yakin Untuk membatalkan Transaksi? </h4>
						<p>Jika anda sudah yakin dengan pembatalan Silahkan isi Alasannya.</p>
							<input type="hidden" name="idkasir" id="idkasir" value="0">
							<select name="idalasan" id="idalasan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
								<option value="#" selected>- Alasan -</option>
								<?php foreach  ($list_alasan as $row) { ?>
									<option value="<?=$row->id?>" <?=($idalasan == $row->id ? 'selected' : '')?>><?=$row->keterangan?></option>
								<?php } ?>
							</select>
							<br>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
					<button class="btn btn-sm btn-danger" id="btnPembatalan" type="submit" data-dismiss="modal"><i class="fa fa-check"></i> Ok</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal" id="modalKwitansi" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form target="_blank" id="formKwitansi"  class=" form-horizontal push-10-t" action="{base_url}tkasir/print_kwitansi/1" method="post">
				<div class="block block-themed block-transparent remove-margin-b">
					<div class="block-header bg-success">
						<ul class="block-options">
							<li>
								<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
							</li>
						</ul>
						<h3 class="block-title">Deskripsi Kwitansi</h3>
					</div>
					<div class="block-content block-content-narrow">
							<input type="hidden" name="idkasir_cetak" id="idkasir_cetak" value="0">
							<input type="hidden" name="idprint" id="idprint" value="0">
							<input type="hidden" name="jenisprint" id="jenisprint" value="0">
							<div class="form-group">
								<label for="example-nf-email">Sudah Terima dari </label>
								<textarea class="form-control" id="penyetor" name="penyetor" rows="1" placeholder="Nama Penyetor"></textarea>
							</div>
							<div class="form-group">
								<label for="example-nf-email">Deskripsi</label>
								<textarea class="form-control" id="deskripsi_kwitansi" name="deskripsi_kwitansi" rows="2" placeholder="Deskripsi ..."></textarea>
							</div>

							<br>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
					<button class="btn btn-sm btn-success" id="btnKwitansi" type="submit" data-dismiss="modal"><i class="fa fa-check"></i> Cetak</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript" src="{ajax}basic-ajax.js"></script>
<script type="text/javascript">

$(document).ready(function(){	
	load_index();
	startWorker();
})
	// jQuery(function() {
		// // BaseTableDatatables.init();
		// var namapasien=$("#namapasien").val();
		// var idasalpasien=$("#idasalpasien").val();
		// var tanggaldari=$("#tanggaldari").val();
		// var tanggalsampai=$("#tanggalsampai").val();
		// var status=$("#status").val();
		// var idkelompokpasien=$("#idkelompokpasien").val();
		// var idpoliklinik=$("#idpoliklinik").val();
		// var iddokter=$("#iddokter").val();
		// var idtipe=$("#idtipe").val();
		// var noreg=$("#noreg").val();
		// var nomedrec=$("#nomedrec").val();
		// $('#datatable-simrs').DataTable({
			// "autoWidth": false,
			// "pageLength": 10,
			// "ordering": true,
			// "processing": false,
			// "serverSide": true,
			// "scrollX": true,
			// "order": [],
			// "ajax": {
				// url: '{site_url}tkasir/getIndex/',
				// type: "POST",
				// dataType: 'json',
					// data: {
						// namapasien: namapasien,
						// idasalpasien: idasalpasien,
						// tanggaldari: tanggaldari,
						// tanggalsampai: tanggalsampai,
						// status: status,
						// idkelompokpasien: idkelompokpasien,
						// idpoliklinik: idpoliklinik,
						// iddokter: iddokter,
						// idtipe: idtipe,
						// noreg: noreg,
						// nomedrec: nomedrec,
					// }
			// },
			// "columnDefs": [{
					// "width": "5%","targets": 0,"orderable": true
				// },
				// {"width": "10%","targets": 1,"orderable": true},
				// {"width": "10%","targets": 2,"orderable": true},
				// {"width": "15%","targets": 3,"orderable": true},
				// {"width": "10%","targets": 4,"orderable": true},
				// {"width": "10%","targets": 5,"orderable": false},
				// {"width": "10%","targets": 6,"orderable": false},
				// {"width": "10%","targets": 7,"orderable": false},
				// {"width": "20%","targets": 8,"orderable": false},
				// {"width": "10%","targets": 9,"orderable": false}
			// ]
		// });
	// });
	function load_index(){
		var namapasien=$("#namapasien").val();
		var idasalpasien=$("#idasalpasien").val();
		var tanggaldari=$("#tanggaldari").val();
		var tanggalsampai=$("#tanggalsampai").val();
		var status=$("#status").val();
		var idkelompokpasien=$("#idkelompokpasien").val();
		var idpoliklinik=$("#idpoliklinik").val();
		var iddokter=$("#iddokter").val();
		var idtipe=$("#idtipe").val();
		var noreg=$("#noreg").val();
		var nomedrec=$("#nomedrec").val();
		$('#datatable-simrs').DataTable().destroy();
		$('#datatable-simrs').DataTable({
			"autoWidth": false,
			"pageLength": 10,
			"ordering": true,
			"processing": false,
			"serverSide": true,
			"scrollX": true,
			"order": [],
			"ajax": {
				url: '{site_url}tkasir/getIndex/',
				type: "POST",
				dataType: 'json',
					data: {
						namapasien: namapasien,
						idasalpasien: idasalpasien,
						tanggaldari: tanggaldari,
						tanggalsampai: tanggalsampai,
						status: status,
						idkelompokpasien: idkelompokpasien,
						idpoliklinik: idpoliklinik,
						iddokter: iddokter,
						idtipe: idtipe,
						noreg: noreg,
						nomedrec: nomedrec,
					}
			},
			"columnDefs": [{
					"width": "5%","targets": 0,"orderable": true
				},
				{"width": "10%","targets": 1,"orderable": true},
				{"width": "10%","targets": 2,"orderable": true},
				{"width": "15%","targets": 3,"orderable": true},
				{"width": "10%","targets": 4,"orderable": true},
				{"width": "10%","targets": 5,"orderable": false},
				{"width": "10%","targets": 6,"orderable": false},
				{"width": "10%","targets": 7,"orderable": false},
				{"width": "20%","targets": 8,"orderable": false},
				{"width": "10%","targets": 9,"orderable": false}
			]
		});
	}
	function ShowOld($idkasir,$idprint,$jenisprint){
		if ($idkasir!=''){
			$.ajax({
				url: '{site_url}tkasir/find_old/'+$idkasir,
				dataType: "json",
				success: function(data) {
					// alert();
					$("#idkasir_cetak").val($idkasir);
					$("#idprint").val($idprint);
					$("#jenisprint").val($jenisprint);
					$("#penyetor").val(data.penyetor);
					$("#deskripsi_kwitansi").val(data.deskripsi_kwitansi);
				}
			});
		}



		// alert('IDKASIR :'+$idkasir+' IDPRINT : '+$idprint);
	}
	$(document).on("click",".selectedBatal",function(){
		var idkasir = ($(this).data('idkasir'));

		$("#idkasir").val(idkasir);
		// $("#alasan").val('');
		// $("#btnPembatalan").attr('disabled',true);
	});
	$(document).on("click",".selectedKwitansi",function(){
		var idkasir = ($(this).data('idkasir'));
		$("#idkasir").val(idkasir);
		alert($("#idkasir").val());
		// $("#alasan").val('');
		// $("#btnPembatalan").attr('disabled',true);
	});

	// $("#alasan").keyup(function(){
		// if ($("#alasan").val().trim().length < 1){
			// $("#btnPembatalan").attr('disabled',true);
		// }else{
			// $("#btnPembatalan").attr('disabled',false);
		// }
	// });

	$(document).on("click","#btnPembatalan",function(){
		$("#formPembatalan").submit();
	});
	$(document).on("click","#btnKwitansi",function(){
		$("#formKwitansi").submit();
	});
	function startWorker() {
		if(typeof(Worker) !== "undefined") {
			if(typeof(w) == "undefined") {
				w = new Worker("{site_url}assets/js/worker.js");

			}
			w.postMessage({'cmd': 'start' ,'msg': 30000});
			w.onmessage = function(event) {
				// console.log(event.data);
				$('#datatable-simrs').DataTable().ajax.reload( null, false );
				// w.terminate();
				// w = undefined;
			};
			w.onerror = function(event) {
				window.location.reload();
			};
		} else {
			alert("Sorry, your browser does not support Autosave Mode");
			$("#autosave_status").html('<button class="btn btn-warning"><i class="fa fa-warning"></i> Autosave not running!</button>');
		}
	 }
	 
</script>
