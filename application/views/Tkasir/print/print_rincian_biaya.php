<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>
		Rincian Biaya
	</title>
	<style>
		@page {
				size: 210mm 297mm;
				margin-top: 2em;
				margin-left: 2.8em;
				margin-bottom: 0.5em;
			}
		*
			table {
				font-size: 21px;
				font-family: "Times New Roman", Times, serif;
				border-collapse: collapse !important;
				width: 100% !important;
				border: 0px solid black;
			}
			td {
				padding: 1px;
				border: 0px solid black;
			}
			.header {
				font-size: 20px;
				font-family: "Times New Roman", Times, serif;
				font-weight: bold;
				margin-bottom: 20px;
			}

			.content td {
				padding: 0px;
			}
			.border-full {
				border: 1px solid #000 !important;
			}
			.border-bottom {
				border-bottom: 2px solid #000 !important;
			}
			.border-thick-top {
				border-top: 2px solid #000 !important;
			}
			.border-thick-bottom {
				border-bottom: 2px solid #000 !important;
			}
			.text-center {
				text-align: center !important;
			}
			.text-right {
				text-align: right !important;
			}
			.bold {
				font-weight: bold;
			}

	</style>
</head>

<body>
	<table  style="width:200mm;">
		<tr>
			<td width="40%" align="center" rowspan="<?=$idkelompokpasien == 1 ? '7':'6'?>"><img src="assets/upload/logo/logomenu.jpg"></td>
		</tr>
		<tr>
			<td class="text-left" width="25%">Nama Pasien</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%"><?=$namapasien?></td>
		</tr>
		<tr>
			<td class="text-left" width="25%">No. Reg / No. RM</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%"><?=$nopendaftaran?> / <?=$nomedrec?></td>
		</tr>
		<tr>
			<td class="text-left" width="25%">Tanggal Kunjugan</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%"><?=$tanggaldaftar?></td>
		</tr>
		<tr>
			<td class="text-left" width="25%">Kelompok Pasien</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%"><?=$namakelompok?></td>
		</tr>
		<?php if($idkelompokpasien == 1) { ?>
		<tr>
			<td class="text-left" width="25%">Rekanan</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%"><?=$namarekanan?></td>
		</tr>
		<? } ?>
		<tr>
			<td class="text-left" width="25%">Dokter</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%"><?=$namadokter?></td>
		</tr>
	</table>
	<br>
	<table>
		<tr>
			<td class="text-center header" width="75%">&nbsp;&nbsp;RINCIAN BIAYA</td>
		</tr>
	</table>
	<table  style="width:200mm;">
		<tr>
			<td class="border-bottom" width="100%"></td>
		</tr>
		<tr>
			<td class="border-bottom" width="100%"></td>
		</tr>
	</table>
	<table  style="width:200mm;">
		<tr>
			<td width="100%"></td>
		</tr>
	</table>
	<br>
	<table  style="width:200mm;">
		<tr>
			<td class="text-left" width="25%">Jasa Dokter Spesialis</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%">Rp <?=$jasa_dokter_spesialis?></td>
		</tr>
		<tr>
			<td class="text-left" width="25%">Jasa Dokter Umum</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%">Rp <?=$jasa_dokter_umum?></td>
		</tr>
		<tr>
			<td class="text-left" width="25%">Alkes Di Poliklinik</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%">Rp <?=$alkes_poliklinik?></td>
		</tr>
		<tr>
			<td class="text-left" width="25%">Tindakan IGD / Rawat Jalan</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%">Rp <?=$tindakan_rajal?></td>
		</tr>
		<tr>
			<td class="text-left" width="25%">Radiologi</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%">Rp <?=$radiologi ?></td>
		</tr>
		<tr>
			<td class="text-left" width="25%">Fisioterapi</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%">Rp <?=$fisioterapi?></td>
			<td width="5%"></td>
		</tr>
		<tr>
			<td class="text-left" width="25%">BMD</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%">Rp <?=$bmd?></td>
		</tr>
		<tr>
			<td class="text-left" width="25%">USG</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%">Rp <?=$usg?></td>
		</tr>
		<tr>
			<td class="text-left" width="25%">Laboratorium</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%">Rp <?=$laboratorium?></td>
		</tr>
		<tr>
			<td class="text-left" width="25%">Farmasi / Obat di Poliklinik</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%">Rp <?=$farmasi?></td>
		</tr>
		<tr>
			<td class="text-left" width="25%">Administrasi</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%">Rp <?=$administrasi?></td>
		</tr>
		<tr>
			<td class="text-left" width="25%">Jasa Keperawatan</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%">Rp <?=$jasa_keperawatan?></td>
		</tr>
		<tr>
			<td class="border-bottom" colspan="3"></td>
		</tr>
		<tr>
			<td class="text-left" width="25%"><b>JUMLAH</b></td>
			<td width="1%" class="text-center">:</td>
			<td width="64%">Rp <?=$jumlah?></td>
		</tr>
		<tr>
			<td class="text-left" width="25%">Jumlah Ditanggung Pasien</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%">Rp <?=$jumlah_ditanggung_pasien?></td>
		</tr>
		<tr>
			<td class="text-left" width="25%">Jumlah Tagihan</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%">Rp <?=$jumlah_tagihan?></td>
		</tr>
		<tr>
			<td class="text-left" colspan="3" style="color: #3abad8; font-size: 16px;"><br>JL. L.L.RE Martadinata No. 28 40115 Jawa Barat, Indonesia<br>T. (022) 4206717 F. (022) 4216436 E. rskb.halmahera@gmail.com ; www.halmaherasiaga.com</td>
		</tr>
	</table>

</body>

</html>
