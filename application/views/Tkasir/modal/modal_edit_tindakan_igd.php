<div class="modal fade in black-overlay" id="modal_edit_p_tindakan_igd" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-popout">
    <div class="modal-content">
      <div class="block block-themed block-transparent remove-margin-b">
        <div class="block-header bg-success">
          <ul class="block-options">
            <li>
              <button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
            </li>
          </ul>
          <h3 class="block-title">EDIT TINDAKAN POLIKLINIK / IGD</h3>
        </div>
        <div class="block-content form-horizontal">
          <div class="form-group">
            <label class="col-md-3 control-label" for="pt_igd_tindakan">Tindakan</label>
            <div class="col-md-9">
              <input readonly type="text" class="form-control input-sm" id="pt_igd_tindakan" placeholder="Tindakan" name="pt_igd_tindakan" required="" aria-required="true">
            </div>
          </div>
          <div class="form-group" style="margin-top: 35px;">
            <label class="col-md-3 control-label" for="pt_igd_dokter">Dokter</label>
            <div class="col-md-9">
              <input readonly type="text" class="form-control input-sm" id="pt_igd_dokter" placeholder="Dokter" name="pt_igd_dokter" required="" aria-required="true">
              <input readonly type="hidden" class="form-control input-sm" id="t_jenis" placeholder="t_jenis" name="t_jenis" required="" aria-required="true">
            </div>
          </div>
          <br><br><br>
          <table id="manage_tabel_edit_igd" class="table table-striped table-bordered" style="margin-bottom: 0;">
            <thead>
              <tr>
                <th style="width: 25%;">Item Biaya</th>
                <th style="width: 25%;">Harga</th>
                <th style="width: 10%;">Diskon(%)</th>
                <th style="width: 20%;">Diskon (Rp.)</th>
                <th style="width: 20%;">Total</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Jasa Sarana</td>
                <td><input type="text" class="form-control input-sm nominal td_awal" id="pt_igd_sarana" placeholder="Jasa Sarana" name="pt_igd_sarana" required="" aria-required="true"></td>
                <td><input type="text" class="form-control input-sm disc td_disc_persen" id="pt_igd_sarana_persen" placeholder="%" name="pt_igd_sarana_persen" required="" aria-required="true"></td>
                <td><input type="text" class="form-control input-sm nominal td_disc_rp" id="pt_igd_sarana_disc" placeholder="Disc Rp" name="pt_igd_sarana_disc" required="" aria-required="true"></td>
                <td><input type="text" class="form-control input-sm nominal td_tot" readonly id="pt_igd_sarana_tot" placeholder="Rp" name="pt_igd_sarana_tot" required="" aria-required="true"></td>
              </tr>
              <tr>
                <td>Jasa Pelayanan</td>
                <td><input type="text" class="form-control input-sm nominal td_awal" id="pt_igd_pelayanan" placeholder="Jasa Pelayanan" name="pt_igd_pelayanan" required="" aria-required="true"></td>
                <td><input type="text" class="form-control input-sm disc td_disc_persen" id="pt_igd_pelayanan_persen" placeholder="%" name="pt_igd_pelayanan_persen" required="" aria-required="true"></td>
                <td><input type="text" class="form-control input-sm nominal td_disc_rp" id="pt_igd_pelayanan_disc" placeholder="Disc Rp" name="pt_igd_pelayanan_disc" required="" aria-required="true"></td>
                <td><input type="text" class="form-control input-sm nominal td_tot" readonly id="pt_igd_pelayanan_tot" placeholder="Rp." name="pt_igd_pelayanan_tot" required="" aria-required="true"></td>
              </tr>
              <tr>
                <td>BHP</td>
                <td><input type="text" class="form-control input-sm nominal td_awal" id="pt_igd_bhp" placeholder="BHP" name="pt_igd_bhp" required="" aria-required="true"></td>
                <td><input type="text" class="form-control input-sm disc td_disc_persen" id="pt_igd_bhp_persen" placeholder=" %" name="pt_igd_bhp_persen" required="" aria-required="true"></td>
                <td><input type="text" class="form-control input-sm nominal td_disc_rp" id="pt_igd_bhp_disc" placeholder="Disc Rp" name="pt_igd_bhp_disc" required="" aria-required="true"></td>
                <td><input type="text" class="form-control input-sm nominal td_tot" readonly id="pt_igd_bhp_tot" placeholder="Total Rp" name="pt_igd_bhp_tot" required="" aria-required="true"></td>
              </tr>
              <tr>
                <td>Biaya Perawatan</td>
                <td><input type="text" class="form-control input-sm nominal td_awal" id="pt_igd_perawatan" placeholder="Biaya Perawatan" name="pt_igd_bhp" required="" aria-required="true"></td>
                <td><input type="text" class="form-control input-sm disc td_disc_persen" id="pt_igd_perawatan_persen" placeholder="%" name="pt_igd_perawatan_persen" required="" aria-required="true"></td>
                <td><input type="text" class="form-control input-sm nominal td_disc_rp" id="pt_igd_perawatan_disc" placeholder="Disc Rp" name="pt_igd_perawatan_disc" required="" aria-required="true"></td>
                <td><input type="text" class="form-control input-sm nominal td_tot" readonly id="pt_igd_perawatan_tot" placeholder="Total Rp" name="pt_igd_perawatan_tot" required="" aria-required="true"></td>
              </tr>
            </tbody>
            <tfoot id="foot_pembayaran">
              <tr class="bg-light dker">
                <th colspan="3"><label class="pull-right"><b>Sub Total Rp.</b></label></th>
                <th colspan="2"><b><input type="text" readonly class="form-control input-sm nominal" id="pt_igd_st" placeholder="Total" name="pt_igd_st" required="" aria-required="true"></th>
              </tr>
              <tr class="bg-light dker">
                <th colspan="3"><label class="pull-right"><b>Kuantitas.</b></label></th>
                <th colspan="2"><b><input type="text" class="form-control input-sm nominal" id="pt_igd_kuantitas" placeholder="Kuantitas" name="pt_igd_kuantitas" required="" aria-required="true"></th>
              </tr>
              <tr class="bg-light dker">
                <th colspan="3"><label class="pull-right"><b>Total Rp.</b></label></th>
                <th colspan="2"><b><input type="text" readonly class="form-control input-sm nominal" id="pt_igd_total" placeholder="Total" name="pt_igd_total" required="" aria-required="true"></th>
              </tr>
              <tr>
                <th style="width: 85%;" colspan="3"><label class="pull-right"><b>Diskon</b></label></th>
                <th style="width: 15%;" colspan="2"><b>
                    <div class="input-group">
                      <span class="input-group-addon">Rp</span>
                      <input class="form-control nominal input-sm" type="text" id="pt_igd_diskon_rp" name="pt_igd_diskon_rp" placeholder="Discount Rp">
                    </div>
                    <div class="input-group">
                      <span class="input-group-addon"> %. </span>
                      <input class="form-control disc input-sm" type="text" id="pt_igd_diskon" name="pt_igd_diskon" placeholder="Discount %">
                    </div>
                </th>
              </tr>
              <tr class="bg-light dker">
                <th colspan="3"><label class="pull-right"><b>GT.</b></label></th>
                <th colspan="2"><b><input type="text" readonly class="form-control input-sm nominal" id="pt_igd_gt" placeholder="Grand Total" name="pt_igd_gt" required="" aria-required="true"></th>
              </tr>
            </tfoot>
          </table>

          <input type="hidden" readonly class="form-control" id="pt_igd_iddetail" name="pt_igd_iddetail">
          <input type="hidden" readonly class="form-control" id="pt_igd_idpoliklinik" name="pt_igd_idpoliklinik">
          <input type="hidden" readonly class="form-control" id="pt_igd_idtindakan" name="pt_igd_idtindakan">

          <div class="modal-footer">
            <button class="btn btn-sm btn-primary" id="btn_edit_pt_igd" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Edit</button>
            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $("#btn_edit_pt_igd").click(function() {
  		var duplicate = false;
  		var content = "";

  		if ($("#nomor").val() != '') {
  			var no = $("#nomor").val();
  		} else {
  			var no = $('#manage_tabel_pembayaran tr').length - 2;
  			var content = "<tr>";
  		}

  		if (duplicate) return false;
  		content += "<td >" + $("#pt_igd_tindakan").val() + "</td>"; //0 Nomor
  		content += "<td align='right'>" + formatNumber($("#pt_igd_st").val()) + "</td>"; //1 Nomor
  		content += "<td align='right'>" + formatNumber($("#pt_igd_kuantitas").val()) + "</td>"; //2 Nomor
  		content += "<td >" + $("#pt_igd_dokter").val() + "</td>"; //3 Nomor
  		content += "<td align='right'>" + formatNumber($("#pt_igd_diskon").val()) + "</td>"; //4 Nomor
  		content += "<td align='right'>" + formatNumber($("#pt_igd_gt").val()) + "</td>"; //5 Nomor
  		content += "<td style='display:none'>1</td>"; //6
  		content += "<td style='display:none'>" + $("#t_jenis").val() + "</td>"; //7
  		content += "<td style='display:none'>" + $("#nomor").val() + "</td>"; //8
  		content += "<td style='display:none'>" + $("#pt_igd_iddetail").val() + "</td>"; //9
  		content += "<td style='display:none'>" + $("#pt_igd_idtindakan").val() + "</td>"; //10
  		content +=
  			"<td><div class='btn-group'><button type='button' class='btn btn-sm btn-info edit_p_tindakan_igd'><i class='glyphicon glyphicon-pencil'></i></button><button type='button' class='btn btn-sm btn-danger hapus_igd'><i class='glyphicon glyphicon-remove'></i></button></div></td>";
  		content += "<td style='display:none'>" + $("#pt_igd_sarana").val() + "</td>"; //12
  		content += "<td style='display:none'>" + $("#pt_igd_pelayanan").val() + "</td>"; //13
  		content += "<td style='display:none'>" + $("#pt_igd_bhp").val() + "</td>"; //14
  		content += "<td style='display:none'>" + $("#pt_igd_perawatan").val() + "</td>"; //15
  		content += "<td style='display:none'>" + $("#pt_igd_st").val() + "</td>"; //16
  		content += "<td style='display:none'>" + $("#pt_igd_kuantitas").val() + "</td>"; //17
  		content += "<td style='display:none'>" + $("#pt_igd_diskon").val() + "</td>"; //18
  		content += "<td style='display:none'>" + $("#pt_igd_gt").val() + "</td>"; //19
  		content += "<td style='display:none'>" + $("#pt_igd_sarana_persen").val() + "</td>"; //20
  		content += "<td  style='display:none'>" + $("#pt_igd_pelayanan_persen").val() + "</td>"; //21
  		content += "<td  style='display:none'>" + $("#pt_igd_bhp_persen").val() + "</td>"; //22
  		content += "<td  style='display:none'>" + $("#pt_igd_perawatan_persen").val() + "</td>"; //23
  		if ($("#rowindex").val() != '') {

  			$('#datatable-simrs tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
  		} else {
  			content += "</tr>";
  			$('#datatable-simrs tbody').append(content);
  		}

  		sumTotalTindakan();
  		gen_harga_diskon();
  	});

    $(document).on("click", ".edit_p_tindakan_igd", function() {

      $('#t_jenis').val($(this).closest('tr').find("td:eq(7)").html());
      $('#pt_igd_tindakan').val($(this).closest('tr').find("td:eq(0)").html());
      $('#pt_igd_dokter').val($(this).closest('tr').find("td:eq(3)").html());

      $('#pt_igd_diskon').val($(this).closest('tr').find("td:eq(18)").html());
      $('#pt_igd_iddetail').val($(this).closest('tr').find("td:eq(9)").html());
      $('#pt_igd_idtindakan').val($(this).closest('tr').find("td:eq(10)").html());

      $('#pt_igd_sarana').val($(this).closest('tr').find("td:eq(12)").html());
      $('#pt_igd_pelayanan').val($(this).closest('tr').find("td:eq(13)").html());
      $('#pt_igd_bhp').val($(this).closest('tr').find("td:eq(14)").html());
      $('#pt_igd_perawatan').val($(this).closest('tr').find("td:eq(15)").html());

      $('#pt_igd_total').val($(this).closest('tr').find("td:eq(16)").html());

      $('#pt_igd_kuantitas').val($(this).closest('tr').find("td:eq(17)").html());
      $('#pt_igd_diskon').val($(this).closest('tr').find("td:eq(4)").html());
      $('#pt_igd_gt').val($(this).closest('tr').find("td:eq(19)").html());
      $('#pt_igd_sarana_persen').val($(this).closest('tr').find("td:eq(20)").html());
      $('#pt_igd_pelayanan_persen').val($(this).closest('tr').find("td:eq(21)").html());
      $('#pt_igd_bhp_persen').val($(this).closest('tr').find("td:eq(22)").html());
      $('#pt_igd_perawatan_persen').val($(this).closest('tr').find("td:eq(23)").html());

      $("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
      $("#nomor").val($(this).closest('tr').find("td:eq(8)").html());
      gen_gt_edit_pt_igd();
      $("#modal_edit_p_tindakan_igd").modal('show');
      terapkan_dari_edit();
    });

  	$(document).on("keyup", ".td_disc_persen", function() {
  		if ($(this).val() == '') {
  			$(this).val(0);
  		}
  		if (parseFloat($(this).val()) > 100) { //DISC % KEY UP
  			$(this).val(100);
  		}
  		var harga_jadi = 0;
  		var harga_awal = $(this).closest('tr').find("td:eq(1) input").val();
  		harga_jadi = (parseFloat(harga_awal) * (parseFloat($(this).val() / 100)));

  		// harga disc rp
  		$(this).closest('tr').find("td:eq(3) input").val(harga_jadi);

  		// total
  		$(this).closest('tr').find("td:eq(4) input").val(harga_awal - harga_jadi);
  		gen_gt_edit_pt_igd();
  		return false;
  	});

  	$(document).on("keyup", ".td_disc_rp", function() { //Diskon RP Keyup
  		var harga_awal = $(this).closest('tr').find("td:eq(1) input").val();
  		if ($(this).val() == '') {
  			$(this).val(0);
  		}
  		if (parseFloat($(this).val()) > harga_awal) {
  			$(this).val(harga_awal);
  		}
  		var harga_jadi = 0;
  		harga_jadi = parseFloat($(this).val() * 100) / parseFloat(harga_awal);
  		// harga disc rp
  		$(this).closest('tr').find("td:eq(2) input").val(harga_jadi);
  		// total
  		$(this).closest('tr').find("td:eq(4) input").val(harga_awal - $(this).val());
  		gen_gt_edit_pt_igd();
  		return false;
  	});

  	$(document).on("keyup", ".td_awal", function() { //Harga Awal RP Keyup
  		if ($(this).val() == '') {
  			$(this).val(0);
  		}
  		var harga_awal = $(this).val();
  		var diskon = parseFloat(parseFloat($(this).closest('tr').find("td:eq(3) input").val()) * 100) / parseFloat(harga_awal);
  		$(this).closest('tr').find("td:eq(2) input").val(diskon);
  		$(this).closest('tr').find("td:eq(4) input").val(harga_awal - parseFloat($(this).closest('tr').find("td:eq(3) input").val()));
  		gen_gt_edit_pt_igd();
  		return false;
  	});

    $(document).on("click", ".hapus_igd", function() {
  		if (confirm("Anda Yakin Hendak Menghapus Data  ?") == true) {
  			var content = "<tr>";
  			content += "<td>pt_igd</td>"; //0 Nomor
  			content += "<td>" + $(this).closest('tr').find("td:eq(9)").html() + "</td>"; //1 Nomor
  			content += "</tr>";
  			$('#tabel_hapus_igd tbody').append(content);
  			$(this).closest('td').parent().remove();
  			sumTotalTindakan();
  			gen_harga_diskon();
  		}
  	});
  });

  function terapkan_dari_edit() {
		var diskon = 0;
		var harga_awal = 0;
		var harga_diskon = 0;
		$('#manage_tabel_edit_igd tbody tr').each(function() {
			harga_awal = $(this).closest('tr').find("td:eq(1) input").val();
			diskon = $(this).closest('tr').find("td:eq(2) input").val();
			harga_diskon = harga_awal * diskon / 100;
			$(this).closest('tr').find("td:eq(4) input").val(harga_awal - harga_diskon);
			$(this).closest('tr').find("td:eq(2) input").val(diskon);
			$(this).closest('tr').find("td:eq(3) input").val(harga_diskon);
		});
		gen_gt_edit_pt_igd();
	}

  function sumTotalTindakan() {
		var total_grand = 0;
		$('#datatable-simrs tbody tr').each(function() {
			if ($(this).find('td:eq(5)').text() != '') {
				total_grand += parseFloat($(this).find('td:eq(5)').text().replace(/,/g, ''));
			}
		});
		$("#gt_all").val(total_grand);
	}
</script>
