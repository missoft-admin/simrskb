<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>FAKTUR</title>
    <style>
		 

    @page {
           margin-top: 1,8em;
            margin-left: 0.8em;
            margin-right: 1.5em;
            margin-bottom: 2.5em;
        }
		
      table {
		 
		font-family: "Courier", Arial,sans-serif;
        font-size: 20px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
	  p{
		font-family: "Courier", Arial,sans-serif;
        font-size: 17px !important; 
	  }
	  .header {
		 font-family: "Courier", Arial,sans-serif;
		 padding-left: 1px;
		 font-size: 35px !important;
		 font-weight:bold;
      }
      h5{
		font-family: "Courier";
        font-size: 20px !important; 
		margin-top: 0;
		margin-bottom: 0;
		margin-left: 0;
		margin-right: 0;
	  }
      td {
        padding: 0px;
		 border: 0px solid #000 !important;
      }
	  

      /* border-normal */
     
	  .text-header{
		font-size: 13px !important;
		
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	  .border-top {
        border-top:1px solid #000 !important;
      }
	  .border-full {
        border:1px solid #000 !important;
      }
	  .border-full-bg {
        border:0px solid #000 !important;
		background-color: #c8c8c8;
      }
	  
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-bottom-top-left {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }
	  
      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  fieldset {
			border:1px solid #000;
			border-radius:8px;
			box-shadow:0 0 10px #000;
		}
		legend {
			background:#fff;
		}
    }
	
	@media print {
		html, body {
			height: 99%;    
		}
	}
    </style>
  </head>
  <body>
	<table class="content">
      <tr>
        <td class="text-header" rowspan="8" width="10%">
    			<img src="<?=base_url()?>assets/upload/logo/logo.png" alt="" style="width: 150px;height: 150px;">
    	</td>
    	<td class="text-left" rowspan="8" width="100%">
              &nbsp;<b>RSKB HALMAHERA SIAGA</b><br>
              &nbsp;JL. LLRE. Martadinata No. 28<br>
              &nbsp;Telp. +6222-4206061<br>
              &nbsp;Bandung
    	</td>
		
      </tr>
    </table>
	<table class="content">
      <tr>
        <td class="header text-center" rowspan="1" width="100%">
    			KWITANSI
    	</td>		
      </tr>
    </table>
   <br>
   <table>
		<tr>
		  <td class="text-left"  width="20%"><h5>No. Medrec</h5></td>
		  <td class="text-center"  width="5%">:</td>
		  <td class="text-left text-bold"  width="75%"><?=$nomedrec?></td>
		</tr>
		<tr>
		  <td class="text-left"  width="20%"><h5>Sudah terima dari</h5></td>
		  <td class="text-center"  width="5%">:</td>
		  <td class="text-left text-bold"  width="75%"><?=($penyetor=='')?$namapasien:$penyetor?></td>
		</tr>
		<tr>
		  <td class="text-left"  width="20%"><h5>Banyaknya Uang</h5></td>
		  <td class="text-center"  width="5%">:</td>
		  <td class="border-full-bg text-left"  width="75%"><?=strtoupper(terbilang($total_terima))?> RUPIAH</td>
		</tr>
		<tr>
		  <td class="text-left"><h5>Untuk Pembayaran</h5></td>
		  <td class="text-center">:</td>
		  <td class="border-bottom text-left"><?=$deskripsi_kwitansi?></td>
		</tr>
		<tr>
		  <td class="text-left"><h5>Tanggal Transaksi</h5></td>
		  <td class="text-center">:</td>
		  <td class="border-bottom text-left"><?=$tgl_trx?> </td>
		</tr>
		
	</table>
	
	<table>
		<tr>
		  <td colspan="3"  class="text-right">Bandung :  <?=HumanDate($tgl_cetak)?></i></td>				  
		</tr>
		<tr>
		  <td width="50%" colspan="2"  class="text-left">&nbsp; </td>
		  <td width="50%">&nbsp;</td>
		</tr>
		<tr>
		  <td width="25%" class="border-top"> &nbsp;</td>
		  <td width="25%" class="border-top">&nbsp;  </td>
		  <td width="50%"></td>
		</tr>
		
		<tr>
		  <td width="25%" class="text-right text-bold"><i> Jumlah Rp.&nbsp;</i></td>
		  <td width="25%" class="text-right border-full-bg">&nbsp;  <?=number_format($total_terima)?></td>
		  <td width="50%"></td>
		</tr>
		<tr>
		  <td width="25%" class="border-bottom"> &nbsp;</td>
		  <td width="25%" class="border-bottom">&nbsp;  </td>
		  <td width="50%"></td>
		</tr>
		<tr>
		  <td width="50%" colspan="2"  class="text-left">&nbsp;</td>
		  <td width="50%">&nbsp;</td>
		</tr>
		
		<tr>
		  <td colspan="3"  class="text-right"><?=$nama_kasir?></td>				  
		</tr>		
		
		<p><u>Cetakan Ke-<?=$jml_cetak?>\UC:<?=$user_cetak?> \ <?=HumanDateLong($tgl_cetak)?></u></p>
	</table>
		
		
		
    <script type="text/javascript">
	
	</script>
  
  </body>
</html>
