<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>FAKTUR</title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		      font-family: "Segoe UI", Arial, sans-serif;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
		      margin: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
	    .text-header{
		       font-size: 20px !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
		    font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;

      }
      .content td {
        padding: 0px;
		    border: 0px solid #6033FF;
      }
      .content-2 td {
        margin: 3px;
		    border: 0px solid #6033FF;
      }

      /* border-normal */
      .border-full {
        border: 0px solid #000 !important;
      }
  	  .text-header{
  		  font-size: 13px !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	    .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	     .border-bottom-top-left {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		    border-left:1px solid #000 !important;
      }
	    .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }
    </style>
  </head>
  <body>
    <table class="content">
      <tr>
        <td class="text-header" rowspan="8" width="15%">
    			<img src="<?=base_url()?>assets/upload/logo/logomenu.jpg" alt="" style="width: 150px;height: 50px;">
    		</td>
    		<td class="text-header" rowspan="8" width="85%">
              &nbsp;RSKB HALMAHERA SIAGA<br>
              &nbsp;JL. LLRE. Martadinata No. 28<br>
              &nbsp;Telp. +6222-4206061<br>
              &nbsp;Bandung
    		</td>
      </tr>
    </table>
	<br>
	<br>
	<table class="content-2">
	  <tr>
        <td width="25%">NO. REG / NO. MEDREC</td>
        <td width="1%">:</td>
        <td width="30%"><?=$nokasir?> / <?=$nomedrec?></td>
        <td width="10%"></td>
    </tr>
    <tr>
		<td width="15%">TANGGAL</td>
        <td width="1%">:</td>
        <td width="25%"><?=$tanggal?></td>
        <td width="10%"></td>
    </tr>
	  <tr>
        <td width="25%">NAMA PASIEN</td>
        <td width="1%">:</td>
        <td width="30%"><?=$namapasien?></td>
  		  <td width="10%"></td>
    </tr>
    <tr>
		    <td width="15%">ALAMAT</td>
        <td width="1%">:</td>
        <td width="25%"><?=$alamat?></td>
    </tr>
	  <tr>
      <td width="25%">DOKTER</td>
      <td width="1%">:</td>
      <td width="30%"><?=$namadokter?></td>
	    <td width="10%"></td>
    </tr>
  </table>
	<br>

  <?php $totalAdm = 0; ?>
  <?php $totalTindakanPoli = 0; ?>
  <?php $totalTindakanObat = 0; ?>
  <?php $totalTindakanAlkes = 0; ?>
  <?php $totalLab = 0; ?>
  <?php $totalRad = 0; ?>
  <?php $totalFisio = 0; ?>
  <?php $totalFarmasiObat = 0; ?>
  <?php $totalFarmasiAlkes = 0; ?>

  <?php if(COUNT($list_admin) > 0){ ?>
	<b>ADMINISTRASI :</b>
  <table class="content-2">
    <tr>
      <td class="border-bottom-top text-left" width="5%">NO</td>
      <!-- <td class="border-bottom-top text-left" width="25%">KODE</td> -->
      <td class="border-bottom-top text-right" width="15%">KETERANGAN</td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH</td>
      <td class="border-bottom-top text-center" width="5%">DISKON (%)</td>
      <td class="border-bottom-top text-right" width="15%">SUB TOTAL</td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_admin as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-full text-left"><?=strtoupper(($number))?></td>
        <!-- <td class="border-full text-left"></td> -->
        <td class="border-full text-left"><?=$row->namatarif?></td>
        <td class="border-full text-center"><?= number_format($row->total, 0)?></td>
        <td class="border-full text-center"><?= number_format($row->diskon, 0)?> %</td>
        <td class="border-full text-right"><?= number_format($row->totalkeseluruhan, 0) ?></td>
      </tr>
      <?php $totalAdm = $totalAdm + $row->totalkeseluruhan; ?>
    <?php } ?>
    <tr>
      <td class="border-full text-right" colspan="3">SUB TOTAL</td>
      <td class="border-full text-center">:</td>
      <td class="border-full text-right"><?=number_format($totalAdm)?></td>
    </tr>
  </table>
  <br>
  <br>
  <?php } ?>
  <?php if(COUNT($list_tindakan) > 0){ ?>
	<b>TINDAKAN POLIKLINIK / IGD :</b>
  <table class="content-2">
    <tr>
      <td class="border-bottom-top text-left" width="5%">NO</td>
      <!-- <td class="border-bottom-top text-left" width="25%">KODE</td> -->
      <td class="border-bottom-top text-right" width="15%">KETERANGAN</td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH</td>
      <td class="border-bottom-top text-center" width="5%">DISKON (%)</td>
      <td class="border-bottom-top text-right" width="15%">SUB TOTAL</td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_tindakan as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-full text-left"><?=strtoupper(($number))?></td>
        <!-- <td class="border-full text-left"></td> -->
        <td class="border-full text-left"><?=$row->namatarif?></td>
        <td class="border-full text-center"><?= number_format($row->total, 0)?></td>
        <td class="border-full text-center"><?= number_format($row->diskon, 0)?> %</td>
        <td class="border-full text-right"><?= number_format($row->totalkeseluruhan, 0) ?></td>
      </tr>
      <?php $totalTindakanPoli = $totalTindakanPoli + $row->totalkeseluruhan; ?>
    <?php } ?>
    <tr>
      <td class="border-full text-right" colspan="3">SUB TOTAL</td>
      <td class="border-full text-center">:</td>
      <td class="border-full text-right"><?=number_format($totalTindakanPoli)?></td>
    </tr>
  </table>
  <br>
  <br>
  <?php } ?>
  <?php if(COUNT($list_igd_obat) > 0){ ?>
	<b>POLIKLINIK / IGD OBAT :</b>
  <table class="content-2">
    <tr>
      <td class="border-bottom-top text-left" width="5%">NO</td>
      <!-- <td class="border-bottom-top text-left" width="25%">KODE</td> -->
      <td class="border-bottom-top text-right" width="15%">KETERANGAN</td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH</td>
      <td class="border-bottom-top text-center" width="5%">DISKON (%)</td>
      <td class="border-bottom-top text-right" width="15%">SUB TOTAL</td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_igd_obat as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-full text-left"><?=strtoupper(($number))?></td>
        <!-- <td class="border-full text-left"></td> -->
        <td class="border-full text-left"><?=$row->namatarif?></td>
        <td class="border-full text-center"><?= number_format($row->hargajual, 0)?></td>
        <td class="border-full text-center"><?= number_format($row->diskon, 0)?> %</td>
        <td class="border-full text-right"><?= number_format($row->totalkeseluruhan, 0) ?></td>
      </tr>
      <?php $totalTindakanObat = $totalTindakanObat + $row->totalkeseluruhan; ?>
    <?php } ?>
    <tr>
      <td class="border-full text-right" colspan="3">SUB TOTAL</td>
      <td class="border-full text-center">:</td>
      <td class="border-full text-right"><?=number_format($totalTindakanObat)?></td>
    </tr>
  </table>
  <?php } ?>
  <?php if(COUNT($list_igd_obat) > 0){ ?>
	<b>POLIKLINIK / IGD ALKES :</b>
  <table class="content-2">
    <tr>
      <td class="border-bottom-top text-left" width="5%">NO</td>
      <!-- <td class="border-bottom-top text-left" width="25%">KODE</td> -->
      <td class="border-bottom-top text-right" width="15%">KETERANGAN</td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH</td>
      <td class="border-bottom-top text-center" width="5%">DISKON (%)</td>
      <td class="border-bottom-top text-right" width="15%">SUB TOTAL</td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_igd_alkes as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-full text-left"><?=strtoupper(($number))?></td>
        <!-- <td class="border-full text-left"></td> -->
        <td class="border-full text-left"><?=$row->namatarif?></td>
        <td class="border-full text-center"><?= number_format($row->hargajual, 0)?></td>
        <td class="border-full text-center"><?= number_format($row->diskon, 0)?> %</td>
        <td class="border-full text-right"><?= number_format($row->totalkeseluruhan, 0) ?></td>
      </tr>
      <?php $totalTindakanAlkes = $totalTindakanAlkes + $row->totalkeseluruhan; ?>
    <?php } ?>
    <tr>
      <td class="border-full text-right" colspan="3">SUB TOTAL</td>
      <td class="border-full text-center">:</td>
      <td class="border-full text-right"><?=number_format($totalTindakanAlkes)?></td>
    </tr>
  </table>
  <?php } ?>
  <?php if(COUNT($list_lab) > 0){ ?>
	<b>LABORATORIUM :</b>
  <table class="content-2">
    <tr>
      <td class="border-bottom-top text-left" width="5%">NO</td>
      <!-- <td class="border-bottom-top text-left" width="25%">KODE</td> -->
      <td class="border-bottom-top text-right" width="15%">KETERANGAN</td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH</td>
      <td class="border-bottom-top text-center" width="5%">DISKON (%)</td>
      <td class="border-bottom-top text-right" width="15%">SUB TOTAL</td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_lab as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-full text-left"><?=strtoupper(($number))?></td>
        <!-- <td class="border-full text-left"></td> -->
        <td class="border-full text-left"><?=$row->namatarif?></td>
        <td class="border-full text-center"><?= number_format($row->total, 0)?></td>
        <td class="border-full text-center"><?= number_format($row->diskon, 0)?> %</td>
        <td class="border-full text-right"><?= number_format($row->totalkeseluruhan, 0) ?></td>
      </tr>
      <?php $totalLab = $totalLab + $row->totalkeseluruhan; ?>
    <?php } ?>
    <tr>
      <td class="border-full text-right" colspan="3">SUB TOTAL</td>
      <td class="border-full text-center">:</td>
      <td class="border-full text-right"><?=number_format($totalLab)?></td>
    </tr>
  </table>
  <?php } ?>
  <?php if(COUNT($list_radiologi) > 0){ ?>
	<b>RADIOLOGI :</b>
  <table class="content-2">
    <tr>
      <td class="border-bottom-top text-left" width="5%">NO</td>
      <!-- <td class="border-bottom-top text-left" width="25%">KODE</td> -->
      <td class="border-bottom-top text-right" width="15%">KETERANGAN</td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH</td>
      <td class="border-bottom-top text-center" width="5%">DISKON (%)</td>
      <td class="border-bottom-top text-right" width="15%">SUB TOTAL</td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_radiologi as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-full text-left"><?=strtoupper(($number))?></td>
        <!-- <td class="border-full text-left"></td> -->
        <td class="border-full text-left"><?=$row->namatarif?></td>
        <td class="border-full text-center"><?= number_format($row->total, 0)?></td>
        <td class="border-full text-center"><?= number_format($row->diskon, 0)?> %</td>
        <td class="border-full text-right"><?= number_format($row->totalkeseluruhan, 0) ?></td>
      </tr>
      <?php $totalRad = $totalRad + $row->totalkeseluruhan; ?>
    <?php } ?>
    <tr>
      <td class="border-full text-right" colspan="3">SUB TOTAL</td>
      <td class="border-full text-center">:</td>
      <td class="border-full text-right"><?=number_format($totalRad)?></td>
    </tr>
  </table>
  <?php } ?>
  <?php if(COUNT($list_fisio) > 0){ ?>
	<b>FISIOTERAPI :</b>
  <table class="content-2">
    <tr>
      <td class="border-bottom-top text-left" width="5%">NO</td>
      <!-- <td class="border-bottom-top text-left" width="25%">KODE</td> -->
      <td class="border-bottom-top text-right" width="15%">KETERANGAN</td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH</td>
      <td class="border-bottom-top text-center" width="5%">DISKON (%)</td>
      <td class="border-bottom-top text-right" width="15%">SUB TOTAL</td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_fisio as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-full text-left"><?=strtoupper(($number))?></td>
        <!-- <td class="border-full text-left"></td> -->
        <td class="border-full text-left"><?=$row->namatarif?></td>
        <td class="border-full text-center"><?= number_format($row->total, 0)?></td>
        <td class="border-full text-center"><?= number_format($row->diskon, 0)?> %</td>
        <td class="border-full text-right"><?= number_format($row->totalkeseluruhan, 0) ?></td>
      </tr>
      <?php $totalFisio = $totalFisio + $row->totalkeseluruhan; ?>
    <?php } ?>
    <tr>
      <td class="border-full text-right" colspan="3">SUB TOTAL</td>
      <td class="border-full text-center">:</td>
      <td class="border-full text-right"><?=number_format($totalFisio)?></td>
    </tr>
  </table>
  <?php } ?>
  <?php if(COUNT($list_farmasi_obat) > 0){ ?>
	<b>FARMASI OBAT :</b>
  <table class="content-2">
    <tr>
      <td class="border-bottom-top text-left" width="5%">NO</td>
      <!-- <td class="border-bottom-top text-left" width="25%">KODE</td> -->
      <td class="border-bottom-top text-right" width="15%">KETERANGAN</td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH</td>
      <td class="border-bottom-top text-center" width="5%">DISKON (%)</td>
      <td class="border-bottom-top text-right" width="15%">SUB TOTAL</td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_farmasi_obat as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-full text-left"><?=strtoupper(($number))?></td>
        <!-- <td class="border-full text-left"></td> -->
        <td class="border-full text-left"><?=$row->namatarif?></td>
        <td class="border-full text-center"><?= number_format($row->harga, 0)?></td>
        <td class="border-full text-center"><?= number_format($row->diskon, 0)?> %</td>
        <td class="border-full text-right"><?= number_format($row->totalharga, 0) ?></td>
      </tr>
      <?php $totalFarmasiObat = $totalFarmasiObat + $row->totalharga; ?>
    <?php } ?>
    <tr>
      <td class="border-full text-right" colspan="3">SUB TOTAL</td>
      <td class="border-full text-center">:</td>
      <td class="border-full text-right"><?=number_format($totalFarmasiObat)?></td>
    </tr>
  </table>
  <?php } ?>
  <?php if(COUNT($list_farmasi_alkes) > 0){ ?>
	<b>FARMASI ALKES :</b>
  <table class="content-2">
    <tr>
      <td class="border-bottom-top text-left" width="5%">NO</td>
      <!-- <td class="border-bottom-top text-left" width="25%">KODE</td> -->
      <td class="border-bottom-top text-right" width="15%">KETERANGAN</td>
      <td class="border-bottom-top text-center" width="15%">JUMLAH</td>
      <td class="border-bottom-top text-center" width="5%">DISKON (%)</td>
      <td class="border-bottom-top text-right" width="15%">SUB TOTAL</td>
    </tr>
    <?php $number = 0; ?>
    <?php foreach  ($list_farmasi_alkes as $row) { ?>
      <?php $number = $number + 1; ?>
      <tr>
        <td class="border-full text-left"><?=strtoupper(($number))?></td>
        <!-- <td class="border-full text-left"></td> -->
        <td class="border-full text-left"><?=$row->namatarif?></td>
        <td class="border-full text-center"><?= number_format($row->harga, 0)?></td>
        <td class="border-full text-center"><?= number_format($row->diskon, 0)?> %</td>
        <td class="border-full text-right"><?= number_format($row->totalharga, 0) ?></td>
      </tr>
      <?php $totalFarmasiAlkes = $totalFarmasiAlkes + $row->totalharga; ?>
    <?php } ?>
    <tr>
      <td class="border-full text-right" colspan="3">SUB TOTAL</td>
      <td class="border-full text-center">:</td>
      <td class="border-full text-right"><?=number_format($totalFarmasiAlkes)?></td>
    </tr>
  </table>
  <?php } ?>
  </table>
  <?php $totalAll = $totalAdm + $totalTindakanPoli + $totalTindakanObat + $totalTindakanAlkes + $totalLab + $totalRad + $totalFisio + $totalFarmasiObat + $totalFarmasiAlkes; ?>
  <br>
  <br>
  <table>
    <tr>
      <td class="border-full text-left">Terbilang : <br> <u><?=strtoupper(terbilang($totalAll))?></u></td>
      <td class="border-full text-left">TOTAL :</td>
      <td class="border-full text-right"><?=number_format($totalAll)?></td>
    </tr>
    <tr>
      <td class="border-full text-left">Paraf Kasir janco,</td>
    </tr>
  </table>

  </body>

  </html>
