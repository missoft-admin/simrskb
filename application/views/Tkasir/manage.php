<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tkasir/index" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<?php echo form_open('tkasir/save_rajal','class="form-horizontal" id="form-work" method="post" onsubmit="return validate_final()"') ?>
	<div class="block-content">
		<hr style="margin-top:0px">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="notrx">No Transaksi</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control" name="notrx" value="{notrx}">
						<input readonly type="hidden" class="form-control" id="idpendaftaran" name="idpendaftaran" value="{id}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="tanggal">Tanggal</label>
					<div class="col-md-8">
						<input class="js-datetimepicker form-control input-sm" <?=(UserAccesForm($user_acces_form,array('1073'))?'':'disabled')?> type="text" id="tanggal" name="tanggal"  value="<?=date('d-m-Y H:i:s')?>"/>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="nomedrec">No. Medrec</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control"  value="{nomedrec}" name="nomedrec" value="{nomedrec}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="namapasien">Nama Pasien</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control" value="{nama}" name="namapasien" value="{namapasien}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="alamat">Alamat</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control" value="{alamat}" name="alamat" value="{alamat}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="alamat">No. HP</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control" id="{nohp}" name="nohp" value="{nohp}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="alamat">No. Tlp</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control" value="{telepon}" name="telepon">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="alamat">Keluarga</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control"  name="namapenanggungjawab" value="<?=$namapenanggungjawab. ' ('.$hubungan.')'?>">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="alamat">No. Keluarga</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control" value="{teleponpenanggungjawab}" name="teleponpenanggungjawab">
					</div>
				</div>


			</div>
			<div class="col-md-6">

				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="kelompok">Kelompok Pasien</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control" name="kelompok" value="{kelompok}">
					</div>
				</div>
				<?if($idkelompokpasien=='3'){?>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="tarif_bpjs_kes">BPJS Kesehatan</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control" name="tarif_bpjs_kes" value="{tarif_bpjs_kes}">
					</div>
				</div>
				<?}?>
				<?if($idkelompokpasien=='4'){?>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="tarif_bpjs_ket">BPJS Ketenagakerjaan</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control" name="tarif_bpjs_ket" value="{tarif_bpjs_ket}">
					</div>
				</div>
				<?}?>
				<?if($idkelompokpasien=='1'){?>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="rekanan">Asuransi</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control" name="rekanan" value="{rekanan}">
					</div>
				</div>
				<?}?>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="rekanan">Jenis Pasien</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control" name="rekanan" value="<?=($idjenispasien=='2'?'COB':'NON COB')?>">
					</div>
				</div>
				<?if($idjenispasien=='2'){?>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="kelompok">Kelompok Pasien COB</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control" name="kelompok2" value="{kelompok2}">
					</div>
				</div>

				<?if($idkelompokpasien2=='3'){?>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="tarif_bpjs_kes">BPJS Kesehatan 2</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control" name="tarif_bpjs_kes2" value="{tarif_bpjs_kes2}">
					</div>
				</div>
				<?}?>
				<?if($idkelompokpasien2=='4'){?>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="tarif_bpjs_ket">BPJS Ketenagakerjaan 2</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control" name="tarif_bpjs_ket2" value="{tarif_bpjs_ket2}">
					</div>
				</div>
				<?}?>
				<?if($idkelompokpasien2=='1'){?>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="rekanan">Asuransi 2</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control" name="rekanan2" value="{rekanan2}">
					</div>
				</div>
				<?}?>
				<?}?>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="dokter">Dokter</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control" name="dokter" value="{dokter}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="poli">Tipe</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control" name="poli" value="<?=($idtipe=='1'?'Poliklinik':'Instalasi Gawat Darurat')?>">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="poli">Poliklinik</label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control" name="poli" value="{poli}">
					</div>
				</div>

			</div>

		</div>

		<hr style="margin-top:10px">
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>Uraian</th>
					<th>Harga Satuan</th>
					<th>Quantity</th>
					<th>Tindakan Dokter</th>
					<th>Discount</th>
					<th>Jumlah</th>
					<th>Action</th>

				</tr>
			</thead>
			<tbody>
			<?php $no=1;?>

					<tr>
						<td bgcolor="#c2c7ef" colspan="6"><h5>ADMINISTRASI<h5></td>
						<td bgcolor="#c2c7ef">
							<?if ($form_verif=='0'){ ?>
							<button style="height:100%" id="add_admin" type='button' class='btn btn-sm btn-success add_admin'><i class='fa fa-plus'></i> Administrasi</button>
							<?}?>
						</td>
					</tr>
					<?php foreach ($list_admin as $row){?>
						<tr>
							<td><?=$row->namatarif?></td>
							<td align="right"><?=number_format($row->subtotal)?></td>
							<td align="right"><?=number_format($row->kuantitas)?></td>
							<td>-</td>
							<?php
							$total_sebelum_diskon = $row->total * $row->kuantitas;
							if ($total_sebelum_diskon == 0){
								$diskon_persen = 100;
							}else{
								$diskon_persen = ($row->diskon * 100) / ($row->total * $row->kuantitas);
							}
							?>
							<td align="right"><?=number_format($diskon_persen, 2)?> %</td><!-- 4  -->
							<td align="right"><?=number_format($row->total)?></td>
							<td style='display:none'>0</td><!-- 6  Variable tambahan-->
							<td style='display:none'>administrasi</td><!-- 7  Nama Tabel -->
							<td style='display:none'><?=$no?></td><!-- 8  Variable tambahan -->
							<td style='display:none'><?=$row->iddetail?></td><!-- 9  ID DETAIL -->
							<td style='display:none'></td><!-- 10 Variable tambahan -->
							<td>
								<?if ($form_verif=='0'){ ?>
								<div class="btn-group">
									<?php if (UserAccesForm($user_acces_form,array('1074'))){ ?>
									<button type='button' class='btn btn-sm btn-info edit_p_tindakan_igd'><i class='glyphicon glyphicon-pencil'></i></button>
									<?}?>
									<?php if (UserAccesForm($user_acces_form,array('1075'))){ ?>
									<button type='button' class='btn btn-sm btn-danger hapus_admin'><i class='glyphicon glyphicon-remove'></i></button>
									<?}?>
								</div>
								<?}?>
							</td>
							<td style='display:none'><?=$row->jasasarana?></td><!-- 12  -->
							<td style='display:none'><?=$row->jasapelayanan?></td><!-- 13  -->
							<td style='display:none'><?=$row->bhp?></td><!-- 14  -->
							<td style='display:none'><?=$row->biayaperawatan?></td><!-- 15  -->
							<td style='display:none'><?=$row->total?></td><!-- 16 -->
							<td style='display:none'><?=$row->kuantitas?></td><!-- 17  -->
							<td style='display:none'><?=$row->diskon?></td><!-- 18  -->
							<td style='display:none'><?=$row->totalkeseluruhan?></td><!-- 19  -->
							<td  style='display:none'><?=$row->jasasarana_disc?></td><!-- 20 disc jasasarana -->
							<td  style='display:none'><?=$row->jasapelayanan_disc?></td><!-- 21 disc jasapelayanan -->
							<td  style='display:none'><?=$row->bhp_disc?></td><!-- 22 disc bhp -->
							<td  style='display:none'><?=$row->biayaperawatan_disc?></td><!-- 23 disc biayaperawatan -->
						</tr>
					<?php
					$no=$no+1;
					}?>

				<?php if ($list_tindakan){

					?>

					<tr>
						<td bgcolor="#c2c7ef" colspan="7"><h5>TINDAKAN POLIKLINIK / IGD<h5></td>
					</tr>
					<input type="hidden" id="rowindex_pt_igd">
					<input type="hidden" id="nomor_pt_igd">
					<?php foreach ($list_tindakan as $row){?>
						<tr>
							<td><?=$row->namatarif?></td><!-- 0  -->
							<td align="right"><?=number_format($row->total)?></td><!-- 1  -->
							<td align="right"><?=number_format($row->kuantitas)?></td><!-- 2  -->
							<td><?=$row->namadokter?></td><!-- 3  -->
							<?php
							$total_sebelum_diskon = $row->total * $row->kuantitas;
							if ($total_sebelum_diskon == 0) {
								$diskon_persen = 100;
							} else {
								$diskon_persen = ($row->diskon * 100) / ($row->total * $row->kuantitas);
							}
							?>
							<td align="right"><?=number_format($diskon_persen, 2)?> %</td><!-- 4  -->
							<td align="right"><?=number_format($row->totalkeseluruhan)?></td><!-- 5  -->
							<td style='display:none'>0</td><!-- 6  Variable tambahan-->
							<td style='display:none'>pt_igd</td><!-- 7  Nama Tabel -->
							<td style='display:none'><?=$no?></td><!-- 8  Variable tambahan -->
							<td style='display:none'><?=$row->iddetail?></td><!-- 9  ID DETAIL -->
							<td style='display:none'><?=$row->idtindakan?></td><!-- 10 Variable tambahan -->
							<td>
								<?if ($form_verif=='0'){ ?>
								<div class="btn-group">
									<?php if (UserAccesForm($user_acces_form,array('1076'))){ ?>
									<button type='button' class='btn btn-sm btn-info edit_p_tindakan_igd'><i class='glyphicon glyphicon-pencil'></i></button>
									<?}?>
									<?php if (UserAccesForm($user_acces_form,array('1077'))){ ?>
									<button type='button' class='btn btn-sm btn-danger hapus_igd'><i class='glyphicon glyphicon-remove'></i></button>
									<?}?>
								</div>
								<?}?>
							</td>
							<td style='display:none'><?=$row->jasasarana?></td><!-- 12  -->
							<td style='display:none'><?=$row->jasapelayanan?></td><!-- 13  -->
							<td style='display:none'><?=$row->bhp?></td><!-- 14  -->
							<td style='display:none'><?=$row->biayaperawatan?></td><!-- 15  -->
							<td style='display:none'><?=$row->total?></td><!-- 16 -->
							<td style='display:none'><?=$row->kuantitas?></td><!-- 17  -->
							<td style='display:none'><?=$row->diskon?></td><!-- 18  -->
							<td style='display:none'><?=$row->totalkeseluruhan?></td><!-- 19  -->
							<td  style='display:none'><?=$row->jasasarana_disc?></td><!-- 20 disc jasasarana -->
							<td  style='display:none'><?=$row->jasapelayanan_disc?></td><!-- 21 disc jasapelayanan -->
							<td  style='display:none'><?=$row->bhp_disc?></td><!-- 22 disc bhp -->
							<td  style='display:none'><?=$row->biayaperawatan_disc?></td><!-- 23 disc biayaperawatan -->
						</tr>
					<?php
					$no=$no+1;}?>
				<?php }?>
				<?php if ($list_igd_obat){?>
					<tr>
						<td bgcolor="#c2c7ef" colspan="7"><h5>POLIKLINIK / IGD OBAT<h5></td>
					</tr>
					<?php foreach ($list_igd_obat as $row){?>
						<tr>
							<td><?=$row->namatarif?></td>
							<td align="right"><?=number_format($row->hargajual)?></td>
							<td align="right"><?=number_format($row->kuantitas)?></td>
							<td>-</td>
							<td align="right">0.00 %</td>
							<td align="right"><?=number_format($row->totalkeseluruhan)?></td>
							<td style='display:none'>0</td><!-- 6  Variable tambahan-->
							<td style='display:none'>p_obat</td><!-- 7  Nama Tabel -->
							<td style='display:none'><?=$no?></td><!-- 8  Variable tambahan -->
							<td style='display:none'></td><!-- 9  Variable tambahan -->
							<td style='display:none'></td><!-- 10 Variable tambahan -->
							<td>
								<div class="btn-group">
									<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>
								</div>
							</td>
						</tr>
					<?php
					$no=$no+1;
					}?>
				<?php }?>
				<?php if ($list_igd_alkes){?>
					<tr>
						<td bgcolor="#c2c7ef" colspan="7"><h5>POLIKLINIK / IGD ALKES<h5></td>
					</tr>
					<?php foreach ($list_igd_alkes as $row){?>
						<tr>
							<td><?=$row->namatarif?></td>
							<td align="right"><?=number_format($row->hargajual)?></td>
							<td align="right"><?=number_format($row->kuantitas)?></td>
							<td>-</td>
							<td align="right">0.00 %</td>
							<td align="right"><?=number_format($row->totalkeseluruhan)?></td>
							<td style='display:none'>0</td><!-- 6  Variable tambahan-->
							<td style='display:none'>p_alkes</td><!-- 7  Nama Tabel -->
							<td style='display:none'><?=$no?></td><!-- 8  Variable tambahan -->
							<td style='display:none'></td><!-- 9  Variable tambahan -->
							<td style='display:none'></td><!-- 10 Variable tambahan -->
							<td>
								<div class="btn-group">
									<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>
								</div>
							</td>
						</tr>
					<?php }?>
				<?php }?>
				<?php if ($list_lab){?>
					<tr>
						<td bgcolor="#c2c7ef" colspan="7"><h5>LABORATORIUM<h5></td>
					</tr>
					<?php foreach ($list_lab as $row){?>
						<tr>
							<td><?=$row->namatarif?></td>
							<td align="right"><?=number_format($row->total)?></td>
							<td align="right"><?=number_format($row->kuantitas)?></td>
							<td>-</td>
							<?php
							$total_sebelum_diskon = $row->total * $row->kuantitas;
							if ($total_sebelum_diskon == 0) {
								$diskon_persen = 100;
							} else {
								$diskon_persen = ($row->diskon * 100) / ($row->total * $row->kuantitas);
							}
							?>
							<td align="right"><?=number_format($diskon_persen, 2)?> %</td><!-- 4  -->
							<td align="right"><?=number_format($row->totalkeseluruhan)?></td>
							<td style='display:none'>0</td><!-- 6  Variable tambahan-->
							<td style='display:none'>lab</td><!-- 7  Nama Tabel -->
							<td style='display:none'><?=$no?></td><!-- 8  Variable tambahan -->
							<td style='display:none'><?=$row->iddetail?></td><!-- 9  ID DETAIL -->
							<td style='display:none'></td><!-- 10 Variable tambahan -->
							<td>
								<?if ($form_verif=='0'){ ?>
								<div class="btn-group">
								<?php if (UserAccesForm($user_acces_form,array('1078'))){ ?>
									<button type='button' class='btn btn-sm btn-info edit_p_tindakan_igd'><i class='glyphicon glyphicon-pencil'></i></button>
								<?}?>
								<?php if (UserAccesForm($user_acces_form,array('1079'))){ ?>
									<button type='button' class='btn btn-sm btn-danger hapus_igd'><i class='glyphicon glyphicon-remove'></i></button>
								<?}?>
								</div>
								<?}?>
							</td>
							<td style='display:none'><?=$row->jasasarana?></td><!-- 12  -->
							<td style='display:none'><?=$row->jasapelayanan?></td><!-- 13  -->
							<td style='display:none'><?=$row->bhp?></td><!-- 14  -->
							<td style='display:none'><?=$row->biayaperawatan?></td><!-- 15  -->
							<td style='display:none'><?=$row->total?></td><!-- 16 -->
							<td style='display:none'><?=$row->kuantitas?></td><!-- 17  -->
							<td style='display:none'><?=$row->diskon?></td><!-- 18  -->
							<td style='display:none'><?=$row->totalkeseluruhan?></td><!-- 19  -->
							<td style='display:none' ><?=$row->jasasarana_disc?></td><!-- 20 disc jasasarana -->
							<td style='display:none' ><?=$row->jasapelayanan_disc?></td><!-- 21 disc jasapelayanan -->
							<td style='display:none' ><?=$row->bhp_disc?></td><!-- 22 disc bhp -->
							<td style='display:none' ><?=$row->biayaperawatan_disc?></td><!-- 23 disc biayaperawatan -->
						</tr>
					<?php
					$no=$no+1;
					}?>
				<?php }?>
				<?php if ($list_radiologi){?>
					<tr>
						<td bgcolor="#c2c7ef" colspan="7"><h5>RADIOLOGI<h5></td>
					</tr>
					<?php foreach ($list_radiologi as $row){?>
						<tr>
							<td><?=$row->namatarif?></td>
							<td align="right"><?=number_format($row->total)?></td>
							<td align="right"><?=number_format($row->kuantitas)?></td>
							<td>-</td>
							<?php
							$total_sebelum_diskon = $row->total * $row->kuantitas;
							if ($total_sebelum_diskon == 0) {
								$diskon_persen = 100;
							} else {
								$diskon_persen = ($row->diskon * 100) / ($row->total * $row->kuantitas);
							}
							?>
							<td align="right"><?=number_format($diskon_persen, 2)?> %</td><!-- 4  -->
							<td align="right"><?=number_format($row->totalkeseluruhan)?></td>
							<td style='display:none'>0</td><!-- 6  Variable tambahan-->
							<td style='display:none'>radiologi</td><!-- 7  Nama Tabel -->
							<td style='display:none'><?=$no?></td><!-- 8  Variable tambahan -->
							<td style='display:none'><?=$row->iddetail?></td><!-- 9  ID DETAIL -->
							<td style='display:none'></td><!-- 10 Variable tambahan -->
							<td>
								<?if ($form_verif=='0'){ ?>
								<div class="btn-group">
									<?php if (UserAccesForm($user_acces_form,array('1080'))){ ?>
									<button type='button' class='btn btn-sm btn-info edit_p_tindakan_igd'><i class='glyphicon glyphicon-pencil'></i></button>
									<?}?>
									<?php if (UserAccesForm($user_acces_form,array('1081'))){ ?>
									<button type='button' class='btn btn-sm btn-danger hapus_igd'><i class='glyphicon glyphicon-remove'></i></button>
									<?}?>
								</div>
								<?}?>
							</td>
							<td style='display:none'><?=$row->jasasarana?></td><!-- 12  -->
							<td style='display:none'><?=$row->jasapelayanan?></td><!-- 13  -->
							<td style='display:none'><?=$row->bhp?></td><!-- 14  -->
							<td style='display:none'><?=$row->biayaperawatan?></td><!-- 15  -->
							<td style='display:none'><?=$row->total?></td><!-- 16 -->
							<td style='display:none'><?=$row->kuantitas?></td><!-- 17  -->
							<td style='display:none'><?=$row->diskon?></td><!-- 18  -->
							<td style='display:none'><?=$row->totalkeseluruhan?></td><!-- 19  -->
							<td style='display:none' ><?=$row->jasasarana_disc?></td><!-- 20 disc jasasarana -->
							<td style='display:none' ><?=$row->jasapelayanan_disc?></td><!-- 21 disc jasapelayanan -->
							<td  style='display:none'><?=$row->bhp_disc?></td><!-- 22 disc bhp -->
							<td style='display:none' ><?=$row->biayaperawatan_disc?></td><!-- 23 disc biayaperawatan -->
						</tr>
					<?php }?>
				<?php }?>
				<?php if ($list_fisio){?>
					<tr>
						<td bgcolor="#c2c7ef" colspan="7"><h5>FISIOTERAPI<h5></td>
					</tr>
					<?php foreach ($list_fisio as $row){?>
						<tr>
							<td><?=$row->namatarif?></td>
							<td align="right"><?=number_format($row->total)?></td>
							<td align="right"><?=number_format($row->kuantitas)?></td>
							<td>-</td>
							<?php
							$total_sebelum_diskon = $row->total * $row->kuantitas;
							if ($total_sebelum_diskon == 0) {
								$diskon_persen = 100;
							} else {
								$diskon_persen = ($row->diskon * 100) / ($row->total * $row->kuantitas);
							}
							?>
							<td align="right"><?=number_format($diskon_persen, 2)?> %</td><!-- 4  -->
							<td align="right"><?=number_format($row->totalkeseluruhan)?></td>
							<td style='display:none'>0</td><!-- 6  Variable tambahan-->
							<td style='display:none'>fisio</td><!-- 7  Nama Tabel -->
							<td style='display:none'><?=$no?></td><!-- 8  Variable tambahan -->
							<td style='display:none'><?=$row->iddetail?></td><!-- 9  ID DETAIL -->
							<td style='display:none'></td><!-- 10 Variable tambahan -->
							<td>
								<?if ($form_verif=='0'){ ?>
								<div class="btn-group">
									<?php if (UserAccesForm($user_acces_form,array('1082'))){ ?>
									<button type='button' class='btn btn-sm btn-info edit_p_tindakan_igd'><i class='glyphicon glyphicon-pencil'></i></button>
									<?}?>
									<?php if (UserAccesForm($user_acces_form,array('1083'))){ ?>
									<button type='button' class='btn btn-sm btn-danger hapus_igd'><i class='glyphicon glyphicon-remove'></i></button>
									<?}?>
								</div>
								<?}?>
							</td>
							<td style='display:none'><?=$row->jasasarana?></td><!-- 12  -->
							<td style='display:none'><?=$row->jasapelayanan?></td><!-- 13  -->
							<td style='display:none'><?=$row->bhp?></td><!-- 14  -->
							<td style='display:none'><?=$row->biayaperawatan?></td><!-- 15  -->
							<td style='display:none'><?=$row->total?></td><!-- 16 -->
							<td style='display:none'><?=$row->kuantitas?></td><!-- 17  -->
							<td style='display:none'><?=$row->diskon?></td><!-- 18  -->
							<td style='display:none'><?=$row->totalkeseluruhan?></td><!-- 19  -->
							<td style='display:none' ><?=$row->jasasarana_disc?></td><!-- 20 disc jasasarana -->
							<td style='display:none' ><?=$row->jasapelayanan_disc?></td><!-- 21 disc jasapelayanan -->
							<td style='display:none' ><?=$row->bhp_disc?></td><!-- 22 disc bhp -->
							<td  style='display:none'><?=$row->biayaperawatan_disc?></td><!-- 23 disc biayaperawatan -->
						</tr>
					<?php
					$no=$no+1;
					}?>
				<?php }?>
				<?php if ($list_farmasi_obat){?>
					<tr>
						<td bgcolor="#c2c7ef" colspan="7"><h5>FARMASI OBAT<h5></td>
					</tr>
					<?php foreach ($list_farmasi_obat as $row){?>
						<tr>
							<td><?=$row->namatarif?></td>
							<td align="right"><?=number_format($row->hargajual)?></td>
							<td align="right"><?=number_format($row->kuantitas)?></td>
							<td>-</td>
							<td align="right"><?=($row->totalkeseluruhan > 0)?number_format($row->diskon, 2):'0'?></td>
							<td align="right"><?=number_format($row->totalkeseluruhan)?></td>
							<td style='display:none'>0</td><!-- 6  Variable tambahan-->
							<td style='display:none'>f_obat</td><!-- 7  Nama Tabel -->
							<td style='display:none'><?=$no?></td><!-- 8  Variable tambahan -->
							<td style='display:none'></td><!-- 9  Variable tambahan -->
							<td style='display:none'></td><!-- 10 Variable tambahan -->
							<td>
								<div class="btn-group">
									<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>
								</div>
							</td>
						</tr>
					<?php
					$no=$no+1;
					}?>
				<?php }?>
				<?php if ($list_farmasi_racikan){?>
					<tr>
						<td bgcolor="#c2c7ef" colspan="7"><h5>FARMASI OBAT - RACIKAN<h5></td>
					</tr>
					<?php foreach ($list_farmasi_racikan as $row){?>
						<tr>
							<td><?=$row->namatarif?></td>
							<td align="right"><?=number_format($row->hargajual)?></td>
							<td align="right"><?=number_format($row->kuantitas)?></td>
							<td>-</td>
							<td align="right"><?=($row->totalkeseluruhan > 0)?number_format($row->diskon, 2):'0'?></td>
							<td align="right"><?=number_format($row->totalkeseluruhan)?></td>
							<td style='display:none'>0</td><!-- 6  Variable tambahan-->
							<td style='display:none'>f_obat</td><!-- 7  Nama Tabel -->
							<td style='display:none'><?=$no?></td><!-- 8  Variable tambahan -->
							<td style='display:none'></td><!-- 9  Variable tambahan -->
							<td style='display:none'></td><!-- 10 Variable tambahan -->
							<td>
								<div class="btn-group">
									<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>
								</div>
							</td>
						</tr>
					<?php
					$no=$no+1;
					}?>
				<?php }?>
				<?php if ($list_farmasi_alkes){?>
					<tr>
						<td bgcolor="#c2c7ef" colspan="7"><h5>FARMASI ALKES<h5></td>
					</tr>
					<?php foreach ($list_farmasi_alkes as $row){?>
						<tr>
							<td><?=$row->namatarif?></td>
							<td align="right"><?=number_format($row->hargajual)?></td>
							<td align="right"><?=number_format($row->kuantitas)?></td>
							<td>-</td>
							<td align="right"><?=number_format($row->diskon, 2)?></td>
							<td align="right"><?=number_format($row->totalkeseluruhan)?></td>
							<td style='display:none'>0</td><!-- 6  Variable tambahan-->
							<td style='display:none'>f_alkes</td><!-- 7  Nama Tabel -->
							<td style='display:none'><?=$no?></td><!-- 8  Variable tambahan -->
							<td style='display:none'></td><!-- 9  Variable tambahan -->
							<td style='display:none'></td><!-- 10 Variable tambahan -->
							<td>
								<div class="btn-group">
									<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>
								</div>
							</td>
						</tr>
					<?php
					$no=$no+1;
					}?>
				<?php }?>
				<?php if ($list_farmasi_implan){?>
					<tr>
						<td bgcolor="#c2c7ef" colspan="7"><h5>FARMASI IMPLAN<h5></td>
					</tr>
					<?php foreach ($list_farmasi_implan as $row){?>
						<tr>
							<td><?=$row->namatarif?></td>
							<td align="right"><?=number_format($row->hargajual)?></td>
							<td align="right"><?=number_format($row->kuantitas)?></td>
							<td>-</td>
							<td align="right"><?=number_format($row->diskon, 2)?></td>
							<td align="right"><?=number_format($row->totalkeseluruhan)?></td>
							<td style='display:none'>0</td><!-- 6  Variable tambahan-->
							<td style='display:none'>f_implan</td><!-- 7  Nama Tabel -->
							<td style='display:none'><?=$no?></td><!-- 8  Variable tambahan -->
							<td style='display:none'></td><!-- 9  Variable tambahan -->
							<td style='display:none'></td><!-- 10 Variable tambahan -->
							<td>
								<div class="btn-group">
									<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>
								</div>
							</td>
						</tr>
					<?php
					$no=$no+1;
					}?>
				<?php }?>

				<tr>
					<th style="width: 85%;" colspan="5"><label class="pull-right"><b>SUB TOTAL Rp.</b></label></th>
					<th style="width: 15%;" colspan="2"><b>
						<input class="form-control input-sm " readonly type="text" id="gt_all" name="gt_all" />
					</th>
				</tr>
				<tr>
					<th style="width: 85%;" colspan="5"><label class="pull-right"><b>Discount</b></label></th>
					<th style="width: 15%;" colspan="2"><b>

						<div class="input-group">
							<span class="input-group-addon">Rp</span>
							<input class="form-control nominal" <?=(UserAccesForm($user_acces_form,array('1084'))?'':'disabled')?> type="text" id="diskon_rp" name="diskon_rp" placeholder="Discount Rp" value="{diskon_rp}">
						</div>
						<div class="input-group">
							<span class="input-group-addon"> %. </span>
							<input class="form-control disc"  <?=(UserAccesForm($user_acces_form,array('1084'))?'':'disabled')?> type="text" id="diskon_persen" name="diskon_persen" placeholder="Discount %"  value="{diskon_persen}">
						</div>
					</th>
				</tr>
				<tr>
					<th style="width: 85%;" colspan="5"><label class="pull-right"><b>TOTAL Rp.</b></label></th>
					<th style="width: 15%;" colspan="2"><b>
						<input class="form-control input-sm " readonly type="text" id="gt_rp" name="gt_rp" />
					</th>
				</tr>
				<tr class="bg-light dker">
					<th colspan="5"></th>
					<th colspan="2">
					<?if ($form_verif=='0'){ ?>
					<?php if (UserAccesForm($user_acces_form,array('1085'))){ ?>
						<button class="btn btn-success  btn_pembayaran" id="btn_pembayaran" data-toggle="modal"  data-target="#modal_pembayaran"  type="button">Pilih Cara Pembayaran</button>
					<?}?>
					</th>
					<?}?>
				</tr>
			</tbody>
		</table>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="progress progress-mini">
					<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
				</div>
				<h5 style="margin-bottom: 10px;">Pembayaran</h5>
				<div class="control-group">
					<div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0;">
						<table id="manage_tabel_pembayaran" class="table table-striped table-bordered" style="margin-bottom: 0;">
							<thead>
								<tr>
									<th style="width: 5%;">#</th>
									<th style="width: 25%;">Jenis Pembayaran</th>
									<th style="width: 35%;">Keterangan</th>
									<th style="width: 10%;">Nominal</th>
									<th style="width: 10%;">Action</th>

								</tr>

							</thead>
							<input type="hidden" id="rowindex">
							<input type="hidden" id="nomor">
							<tbody>
								<?php if ($form_edit=='1'){
									$no=0;
									foreach($list_pembayaran as $row){
										$no=$no+1;
									?>
									<tr>
									<td style='display:none'><?= $row->idmetode ?></td>
									<td><?= $no ?></td>
									<td><?= metodePembayaranKasirRajal($row->idmetode) ?></td>
									<td><?= $row->keterangan ?></td>
									<td style='display:none'><?= $row->nominal ?></td>
									<td><?= number($row->nominal) ?></td>
									<td style='display:none'><?= $row->idkasir ?></td>
									<td style='display:none'><?= $row->idmetode ?></td>
									<td style='display:none'><?= $row->idpegawai ?></td>
									<td style='display:none'><?= $row->idbank ?></td>
									<td style='display:none'><?= $row->ket_cc ?></td>
									<td style='display:none'><?= $row->idkontraktor ?></td>
									<td style='display:none'><?= $row->keterangan ?></td>
									<td>
										<?if ($form_verif=='0'){ ?>
										<?php if (UserAccesForm($user_acces_form,array('1086'))){ ?>
										<button type='button' class='btn btn-sm btn-info edit'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;
										<?}?>
										<?php if (UserAccesForm($user_acces_form,array('1087'))){ ?>
										<button type='button' class='btn btn-sm btn-danger hapus'><i class='glyphicon glyphicon-remove'></i></button>
										<?}?>
										<?}?>
									</td>
									<td style='display:none'><?= $row->jaminan ?></td>
									<td style='display:none'><?= $row->trace_number ?></td>
									<td style='display:none'><?= $row->iddokter ?></td>
									<td style='display:none'><?= $row->tipepegawai ?></td>
									<td style='display:none'><?= $row->tipekontraktor ?></td>
									</tr>
								<?php }}?>
							</tbody>
							<tfoot id="foot_pembayaran">
								<tr class="bg-light dker">
									<th colspan="3"><label class="pull-right"><b>Pembayaran Rp.</b></label></th>
									<th colspan="2"><b>
										<input type="text"  class="form-control input-sm " readonly id="bayar_rp" name="bayar_rp" value="{bayar_rp}" />

									</th>
								</tr>
								<tr class="bg-light dker">
									<th colspan="3"><label class="pull-right"><b>Sisa Rp.</b></label></th>
									<th colspan="2"><b>
										<input type="text"  class="form-control input-sm " readonly id="sisa_rp" name="sisa_rp" value="{sisa_rp}" />
									</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" name="id"  id="id" value="{id}">
	<input type="hidden" name="idkasir"  id="idkasir" value="{idkasir}">
	<input type="hidden" name="pos_tabel_pembayaran"  id="pos_tabel_pembayaran" >
	<input type="hidden" name="pos_tabel_detail"  id="pos_tabel_detail" >
	<input type="hidden" name="pos_tabel_hapus_igd"  id="pos_tabel_hapus_igd" >
	<div class="modal-footer">
		<div class="buton">
			<?if ($form_verif=='0'){ ?>
			<button type="button" id="btn_save" class="btn btn-primary simpan-rencana" data-dismiss="modal">Simpan</button>
			<a href="{base_url}tkasir" type="button" class="btn btn-default">Close</a>
			<?}?>
		</div>
	</div>
	<div class="modal fade in black-overlay" id="modal_pembayaran" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-popout">
			<div class="modal-content">
				<div class="block block-themed block-transparent remove-margin-b">
					<div class="block-header bg-success">
						<ul class="block-options">
							<li>
								<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
							</li>
						</ul>
						<h3 class="block-title">Pembayaran</h3>
					</div>
					<div class="block-content">

					<div class="form-group">
						<label class="col-md-3 control-label" for="idmetode">Metode</label>
						<div class="col-md-7">
							<select name="idmetode" id="idmetode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="">Pilih Opsi</option>
								<option value="1">Tunai</option>

								<option value="2">Debit</option>
								<option value="3">Kartu Kredit</option>
								<option value="4">Transfer</option>
								<option value="5">Tagihan Karyawan</option>
								<option value="6">Tidak Tertagihkan</option>
								<option value="7">Kontraktor</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="nama">Sisa Rp. </label>
						<div class="col-md-7">
							<input readonly type="text" class="form-control" id="sisa_modal" placeholder="Sisa" name="sisa_modal" required="" aria-required="true">
						</div>
					</div>
					<div class="form-group" id="div_bank" hidden>
						<label class="col-md-3 control-label" for="idbank">Bank</label>
						<div class="col-md-7">
							<select name="idbank" id="idbank" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="">Pilih Opsi</option>
								<?php foreach  ($list_bank as $row){?>
									<option value="<?=$row->id?>"><?=$row->nama?></option>
								<?php }?>

							</select>
						</div>
					</div>
					<div class="form-group" id="div_cc" hidden>
						<label class="col-md-3 control-label" for="nama">Ket. CC </label>
						<div class="col-md-7">
							<input type="text" class="form-control" id="ket_cc" placeholder="Ket. CC" name="ket_cc" required="" aria-required="true">
						</div>
					</div>
					<div class="form-group" id="div_trace" hidden>
						<label class="col-md-3 control-label" for="nama">Trace Number</label>
						<div class="col-md-7">
							<input type="text" class="form-control" id="trace_number" placeholder="Trace Number" name="trace_number" required="" aria-required="true">
						</div>
					</div>
					<div class="form-group" id="div_kontraktor" hidden>
						<label class="col-md-3 control-label" for="tipekontraktor">Tipe Kontraktor </label>
						<div class="col-md-7">

							<select name="tipekontraktor" id="tipekontraktor" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="">Pilih Opsi</option>
								<?foreach($arr_asuransi_id as $key => $value){?>
								<option value="<?=$value?>" selected><?=$arr_asuransi_nama[$key]?></option>
								<?}?>
							</select>
						</div>
					</div>
					<div class="form-group" id="div_kontraktor2" hidden>
						<label class="col-md-3 control-label" for="idkontraktor">ID Kontraktor</label>
						<div class="col-md-7">
							<select style="width:100%" name="idkontraktor" id="idkontraktor" data-placeholder="Kontraktor" class="form-control  input-sm"></select>
						</div>
					</div>

					<div class="form-group" id="div_tipepegawai" hidden>
						<label class="col-md-3 control-label" for="tipepegawai">Tipe Pegawai</label>
						<div class="col-md-7">
							<select style="width:100%" name="tipepegawai" class="js-select2 form-control"  id="tipepegawai" data-placeholder="Pegawai" class="form-control  input-sm">
								<option value="1" selected>Pegawai</option>
								<option value="2">Dokter</option>

							</select>
						</div>
					</div>
					<div class="form-group" id="div_pegawai" hidden>
						<label class="col-md-3 control-label" for="idpegawai">Pegawai</label>
						<div class="col-md-7">
							<select style="width:100%" name="idpegawai" id="idpegawai" data-placeholder="Pegawai" class="form-control  input-sm"></select>
						</div>
					</div>
					<div class="form-group" id="div_dokter" hidden>
						<label class="col-md-3 control-label" for="iddokter">Dokter</label>
						<div class="col-md-7">
							<select style="width:100%" name="iddokter" id="iddokter" data-placeholder="Dokter" class="form-control  input-sm">
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="nama">Nominal Rp. </label>
						<div class="col-md-7">
							<input  type="text" class="form-control" id="nominal" placeholder="Nominal" name="nominal" required="" aria-required="true">
						</div>
					</div>
					<div class="form-group" id="div_jaminan">
						<label class="col-md-3 control-label" for="nama">Jaminan </label>
						<div class="col-md-7">
							<input type="text" class="form-control" id="jaminan" placeholder="Jaminan" name="jaminan" required="" aria-required="true">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="nama">Keterangan </label>
						<div class="col-md-7">
							<input type="text" class="form-control" id="ket" placeholder="Keterangan" name="ket" required="" aria-required="true">
						</div>
					</div>

					<div class="modal-footer">
						<button class="btn btn-sm btn-primary" id="btn_add_bayar" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Add</button>
						<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Exit</button>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade in black-overlay" id="modal_edit_p_tindakan_igd" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-popout">
			<div class="modal-content">
				<div class="block block-themed block-transparent remove-margin-b">
					<div class="block-header bg-success">
						<ul class="block-options">
							<li>
								<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
							</li>
						</ul>
						<h3 class="block-title">EDIT TINDAKAN POLIKLINIK / IGD</h3>
					</div>
					<div class="block-content">
					<div class="form-group">
						<label class="col-md-3 control-label" for="pt_igd_tindakan">Tindakan. </label>
						<div class="col-md-9">
							<input readonly type="text" class="form-control input-sm" id="pt_igd_tindakan" placeholder="Tindakan" name="pt_igd_tindakan" required="" aria-required="true">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="pt_igd_dokter">Dokter. </label>
						<div class="col-md-9">
							<input readonly type="text" class="form-control input-sm" id="pt_igd_dokter" placeholder="Dokter" name="pt_igd_dokter" required="" aria-required="true">
							<input readonly type="hidden" class="form-control input-sm" id="t_jenis" placeholder="t_jenis" name="t_jenis" required="" aria-required="true">
						</div>
					</div>
					<table id="manage_tabel_edit_igd" class="table table-striped table-bordered" style="margin-bottom: 0;">
							<thead>
								<tr>
									<th style="width: 25%;">Item Biaya</th>
									<th style="width: 25%;">Harga</th>
									<th style="width: 10%;">Diskon(%)</th>
									<th style="width: 20%;">Diskon (Rp.)</th>
									<th style="width: 20%;">Total</th>

								</tr>

							</thead>
							<input type="hidden" id="rowindex">
							<input type="hidden" id="nomor">
							<tbody>
									<tr>
										<td>Jasa Sarana</td>
										<td><input  type="text" class="form-control input-sm nominal td_awal" id="pt_igd_sarana" placeholder="Jasa Sarana" name="pt_igd_sarana" required="" aria-required="true"></td>
										<td><input  type="text" class="form-control input-sm disc td_disc_persen" id="pt_igd_sarana_persen" placeholder="%" name="pt_igd_sarana_persen" required="" aria-required="true"></td>
										<td><input  type="text" class="form-control input-sm nominal td_disc_rp" id="pt_igd_sarana_disc" placeholder="Disc Rp" name="pt_igd_sarana_disc" required="" aria-required="true"></td>
										<td><input  type="text" class="form-control input-sm nominal td_tot" readonly id="pt_igd_sarana_tot" placeholder="Rp" name="pt_igd_sarana_tot" required="" aria-required="true"></td>
									</tr>
									<tr>
										<td>Jasa Pelayanan</td>
										<td><input  type="text" class="form-control input-sm nominal td_awal" id="pt_igd_pelayanan" placeholder="Jasa Pelayanan" name="pt_igd_pelayanan" required="" aria-required="true"></td>
										<td><input  type="text" class="form-control input-sm disc td_disc_persen" id="pt_igd_pelayanan_persen" placeholder="%" name="pt_igd_pelayanan_persen" required="" aria-required="true"></td>
										<td><input  type="text" class="form-control input-sm nominal td_disc_rp" id="pt_igd_pelayanan_disc" placeholder="Disc Rp" name="pt_igd_pelayanan_disc" required="" aria-required="true"></td>
										<td><input  type="text" class="form-control input-sm nominal td_tot" readonly id="pt_igd_pelayanan_tot" placeholder="Rp." name="pt_igd_pelayanan_tot" required="" aria-required="true"></td>
									</tr>
									<tr>
										<td>BHP</td>
										<td><input  type="text" class="form-control input-sm nominal td_awal" id="pt_igd_bhp" placeholder="BHP" name="pt_igd_bhp" required="" aria-required="true"></td>
										<td><input  type="text" class="form-control input-sm disc td_disc_persen" id="pt_igd_bhp_persen" placeholder=" %" name="pt_igd_bhp_persen" required="" aria-required="true"></td>
										<td><input  type="text" class="form-control input-sm nominal td_disc_rp" id="pt_igd_bhp_disc" placeholder="Disc Rp" name="pt_igd_bhp_disc" required="" aria-required="true"></td>
										<td><input  type="text" class="form-control input-sm nominal td_tot"  readonly id="pt_igd_bhp_tot" placeholder="Total Rp" name="pt_igd_bhp_tot" required="" aria-required="true"></td>
									</tr>
									<tr>
										<td>Biaya Perawatan</td>
										<td><input  type="text" class="form-control input-sm nominal td_awal" id="pt_igd_perawatan" placeholder="Biaya Perawatan" name="pt_igd_bhp" required="" aria-required="true"></td>
										<td><input  type="text" class="form-control input-sm disc td_disc_persen" id="pt_igd_perawatan_persen" placeholder="%" name="pt_igd_perawatan_persen" required="" aria-required="true"></td>
										<td><input  type="text" class="form-control input-sm nominal td_disc_rp" id="pt_igd_perawatan_disc" placeholder="Disc Rp" name="pt_igd_perawatan_disc" required="" aria-required="true"></td>
										<td><input  type="text" class="form-control input-sm nominal td_tot" readonly id="pt_igd_perawatan_tot" placeholder="Total Rp" name="pt_igd_perawatan_tot" required="" aria-required="true"></td>
									</tr>
							</tbody>
							<tfoot id="foot_pembayaran">
								<tr class="bg-light dker">
									<th colspan="3"><label class="pull-right"><b>Sub Total Rp.</b></label></th>
									<th colspan="2"><b><input  type="text" readonly class="form-control input-sm nominal" id="pt_igd_st" placeholder="Total" name="pt_igd_st" required="" aria-required="true"></th>
								</tr>
								<tr class="bg-light dker">
									<th colspan="3"><label class="pull-right"><b>Kuantitas.</b></label></th>
									<th colspan="2"><b><input  type="text" class="form-control input-sm nominal" id="pt_igd_kuantitas" placeholder="Kuantitas" name="pt_igd_kuantitas" required="" aria-required="true"></th>
								</tr>
								<tr class="bg-light dker">
									<th colspan="3"><label class="pull-right"><b>Total Rp.</b></label></th>
									<th colspan="2"><b><input  type="text" readonly class="form-control input-sm nominal" id="pt_igd_total" placeholder="Total" name="pt_igd_total" required="" aria-required="true"></th>
								</tr>
								<tr>
									<th style="width: 85%;" colspan="3"><label class="pull-right"><b>Discount</b></label></th>
									<th style="width: 15%;" colspan="2"><b>
										<div class="input-group">
											<span class="input-group-addon">Rp</span>
											<input class="form-control nominal input-sm" type="text" id="pt_igd_diskon_rp" name="pt_igd_diskon_rp" placeholder="Discount Rp">
										</div>
										<div class="input-group">
											<span class="input-group-addon"> %. </span>
											<input class="form-control disc input-sm" type="text" id="pt_igd_diskon" name="pt_igd_diskon" placeholder="Discount %">
										</div>
									</th>
								</tr>
								<tr class="bg-light dker">
									<th colspan="3"><label class="pull-right"><b>GT.</b></label></th>
									<th colspan="2"><b><input  type="text" readonly class="form-control input-sm nominal" id="pt_igd_gt" placeholder="Grand Total" name="pt_igd_gt" required="" aria-required="true"></th>
								</tr>
							</tfoot>
						</table>

					<input  type="hidden" readonly class="form-control" id="pt_igd_iddetail" name="pt_igd_iddetail">
					<input  type="hidden" readonly class="form-control" id="pt_igd_idpoliklinik" name="pt_igd_idpoliklinik">
					<input  type="hidden" readonly class="form-control" id="pt_igd_idtindakan" name="pt_igd_idtindakan">
					<div class="modal-footer">
						<button class="btn btn-sm btn-primary" id="btn_edit_pt_igd" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Edit</button>
						<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
	<form action="javascript:(0)" method="#" class="form-horizontal" id="form3">
		<div class="modal in" id="modal_add_admin" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="block block-themed block-transparent remove-margin-b">
						<div class="block-header bg-success">
							<h3 class="block-title">Edit Barang</h3>
						</div>
						<div class="block-content">

							<div class="form-group">
								 <label class="control-label col-md-3">Tarif Administrasi</label>
								<div class="col-md-6">
									<div class="input-group">
										<select name="idadmin" id="idadmin" data-placeholder="Cari Tarif" class="js-select2 form-control" style="width:100%">
											<option value="#" disabled selected>-Silahkan Pilih Tarif-</option>
											<?foreach ($list_tarif_admin as $r) {?>
												<option value="<?=$r->id?>"><?=$r->nama?></option>
											<?}?>
										</select>
										<div class="input-group-btn">
											<button class='btn btn-info modal_list_barang' disabled data-toggle="modal" data-target="#modal_list_barang" type='button' id='btn_cari'>
												<i class="fa fa-search"></i>
											</button>
										</div>
									</div>
								</div>
							</div>
						   <div class="form-group">
								<label class="control-label col-md-3">Harga</label>
								<div class="col-md-6">
									<input type="text" readonly class="form-control nominal" id="tharga" name="tharga" required>
									<input type="hidden" readonly class="form-control nominal" id="idadministrasi" name="idadministrasi" required>
								</div>
							</div>

						</div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
						<button class="btn btn-sm btn-success" id="btn_tambah_adm">Tambah</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<div id="data-hapus-igd">
		<table id="tabel_hapus_igd" class="table table-striped table-bordered" style="margin-bottom: 0;display:none">
			<thead>
				<tr>
					<th style="width: 25%;">Nama Proses</th>
					<th style="width: 5%;">ID Detail</th>
				</tr>

			</thead>
			<tbody></tbody>
		</table>
	</div>
	<?php echo form_close() ?>

</div>




<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>

<script src="{js_path}pages/base_index_datatable.js"></script>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	// App.initHelpers(['datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
	$("#diskon_rp").number(true,0,'.',',');
	$("#diskon_persen").number(true,2,'.',',');
	$("#gt_all").number(true,0,'.',',');
	$("#gt_rp").number(true,0,'.',',');
	$("#sisa_rp").number(true,0,'.',',');
	$(".nominal").number(true,0,'.',',');
	$(".disc").number(true,2,'.',',');

	$("#bayar_rp").number(true,0,'.',',');
	$("#sisa_modal").number(true,0,'.',',');
	$("#nominal").number(true,0,'.',',');
	 // $('#tanggal').datetimepicker({
                // format: 'HH:mm:ss',
                // pickDate: false,
                // minuteStepping:30,
                // pickTime: true,
                // autoclose: true,
                // defaultDate: new Date(1979, 0, 1, 8, 0, 0, 0),
                // //this is what you will need!
                // disabledTimeIntervals: [[moment({ h: 0 }), moment({ h: 8 })], [moment({ h: 18 }), moment({ h: 20 })]],
                // //hoursDisabled: '0,1,2,3,4,5,6,7,21,22,23',//Not Working!!
                // //hoursDisabled: [00, 01, 02, 03, 04, 05, 06, 07, 21, 22, 23],//Not Working!!
                // language:'en',
            // });
	$("#tanggal").datetimepicker({
				format: "DD-MM-YYYY HH:mm",
				// stepping: 30
			});
	// $('#tanggal').datetimepicker({
		// dateFormat: "dd-mm-yy",
		// timeFormat: "H:i:s"
	// });
	 // $('#tanggal').datetimepicker({
        // 'timeFormat':'H:i'
    // });
	sumTotalTindakan();
	gen_harga_diskon();
	validasi_save();
	$('#tipekontraktor').val($("#tipekontraktor").val()).trigger('change');

	$("#idpegawai").select2({
		minimumInputLength: 2,
		noResults: 'Pegawai Tidak Ditemukan.',
		// allowClear: true
		// tags: [],
		ajax: {
			url: '{site_url}tkasir/get_pegawai/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,

		 data: function (params) {
			  var query = {
				search: params.term,
			  }
			  return query;
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							text: item.nama,
							id: item.id,
						}
					})
				};
			}
		}
	});

	$("#iddokter").select2({
		minimumInputLength: 2,
		noResults: 'Dokter Tidak Ditemukan.',
		// allowClear: true
		// tags: [],
		ajax: {
			url: '{site_url}tkasir/get_dokter/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,

		 data: function (params) {
			  var query = {
				search: params.term,
			  }
			  return query;
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							text: item.nama,
							id: item.id,
						}
					})
				};
			}
		}
	});

});
$("#diskon_persen").keyup(function(){
	if ($("#diskon_persen").val()==''){
		$("#diskon_persen").val(0)
	}
	if (parseFloat($(this).val()) > 100){
		$(this).val(100);
	}
	var harga_jadi=0;
	harga_jadi= (parseFloat($("#gt_all").val()) * (parseFloat($("#diskon_persen").val()/100)));
	$("#diskon_rp").val(harga_jadi);
	gen_harga_diskon();
});
$("#pt_igd_diskon").keyup(function(){
	if ($("#pt_igd_diskon").val()==''){
		$("#pt_igd_diskon").val(0)
	}
	if (parseFloat($(this).val()) > 100){
		$(this).val(100);
	}
	var diskon_rp=parseFloat($("#pt_igd_total").val() * parseFloat($(this).val())/100);
	$("#pt_igd_diskon_rp").val(diskon_rp);
	gen_gt_edit_pt_igd();
});
$("#pt_igd_kuantitas").keyup(function(){

	gen_gt_edit_pt_igd();
});

$("#diskon_rp").keyup(function(){
	if ($("#diskon_rp").val()==''){
		$("#diskon_rp").val(0)
	}
	if (parseFloat($(this).val()) > parseFloat($("#gt_all").val())){
		$(this).val(parseFloat($("#gt_all").val()));
	}
	var harga_jadi=0;
	harga_jadi= (parseFloat($("#diskon_rp").val()) *100) / (parseFloat($("#gt_all").val()));
	$("#diskon_persen").val(parseFloat(harga_jadi).toFixed(2));
	gen_harga_diskon();
});
$("#pt_igd_diskon_rp").keyup(function(){
	if ($("#pt_igd_diskon_rp").val()==''){
		$("#pt_igd_diskon_rp").val(0)
	}
	if (parseFloat($(this).val()) > $("#pt_igd_total").val()){
		$(this).val($("#pt_igd_total").val());
	}
	var diskon_persen=parseFloat((parseFloat($(this).val()*100))/$("#pt_igd_total").val());
	$("#pt_igd_diskon").val(diskon_persen);
	gen_gt_edit_pt_igd();

});
function gen_harga_diskon(){
	var total;
	total=parseFloat($("#gt_all").val()) - parseFloat($("#diskon_rp").val());
	total=Math.ceil(total/100)*100;
	$("#gt_rp").val(total);
	gen_sisa();

}
function gen_gt_edit_pt_igd(){
	var total_kotor;
	var total_bersih;
	var total_diskon_rp;
	var total_diskon_persen;
	total_kotor=(parseFloat($("#pt_igd_sarana_tot").val()) + parseFloat($("#pt_igd_pelayanan_tot").val()) + parseFloat($("#pt_igd_bhp_tot").val()) + parseFloat($("#pt_igd_perawatan_tot").val()));
	$("#pt_igd_st").val(total_kotor);
	$("#pt_igd_total").val(total_kotor * parseFloat($("#pt_igd_kuantitas").val()));
	// $("#pt_igd_diskon_rp").val(parseFloat(total_diskon_rp));
	// alert(total_diskon_rp);
	total_diskon_rp= parseFloat($("#pt_igd_diskon_rp").val());
	// total_diskon_persen= parseFloat($("#pt_igd_diskon").val());
	// total_diskon_rp=total_diskon_persen / 100 * parseFloat($("#pt_igd_total").val());
	// $("#pt_igd_diskon_rp").val(total_diskon_rp)
	total_bersih = parseFloat($("#pt_igd_total").val()) - parseFloat(total_diskon_rp);
	$("#pt_igd_gt").val(parseFloat(total_bersih));

}
function gen_sisa(){
	var total;
	total=parseFloat($("#gt_rp").val()) - parseFloat($("#bayar_rp").val());

	$("#sisa_rp").val(total);
	validasi_save();
}
function sumTotalTindakan() {
	var total_grand=0;
	$('#datatable-simrs tbody tr').each(function() {
		if($(this).find('td:eq(5)').text() != ''){
			total_grand +=parseFloat($(this).find('td:eq(5)').text().replace(/,/g, ''));
		}
	});
	$("#gt_all").val(total_grand);
	// $("#gt_all").val(total_grand.toLocaleString(undefined, {
		// minimumFractionDigits: 0
	// }));
}
$("#btn_pembayaran").click(function() {

	$("#sisa_modal").val($("#sisa_rp").val());
	$("#nominal").val($("#sisa_rp").val());

	$("#nomor").val('')
	$("#trace_number").val('')
	$("#rowindex").val('')
	$('#idmetode').val('').trigger('change');
	$('#idbank').val('').trigger('change');
	$('#idpegawai').val('').trigger('change');
	$('#iddokter').val('').trigger('change');
	$('#tipepegawai').val('').trigger('change');
	$("#div_bank").hide();
	$("#div_kontraktor").hide();
	$("#div_kontraktor2").hide();
	$("#div_pegawai").hide();
	$("#div_cc").hide();
	$("#div_trace").hide();
	$("#div_jaminan").hide();
	$("#div_tipepegawai").hide();
	$("#div_pegawai").hide();
	$("#div_dokter").hide();

	$("#ket").val('');


});
$("#idmetode").change(function(){
	if ($("#idmetode").val()=="1"){//CASH
		$("#div_jaminan").hide();
		$("#div_cc").hide();
		$("#div_trace").hide();
		$("#div_bank").hide();
		$("#div_kontraktor").hide();
		$("#div_kontraktor2").hide();
		$("#div_pegawai").hide();
		$("#div_tipepegawai").hide();
		$("#div_dokter").hide();
		$("#ket").val('Tunai');
	}if ($("#idmetode").val()=="2" || $("#idmetode").val()=="3" || $("#idmetode").val()=="4"){
		$("#div_kontraktor").hide();
		$("#div_kontraktor2").hide();
		$("#div_jaminan").hide();
		if ($("#idmetode").val()=="3"){
			$("#div_cc").show();
		}else{
			$("#div_cc").hide();
		}
		$("#div_trace").show();
		$("#div_pegawai").hide();
		$("#div_tipepegawai").hide();
		$("#div_dokter").hide();
		$("#div_bank").show();
		$("#ket").val($("#idmetode option:selected").text()+' : '+$("#idbank option:selected").text()+' ('+$("#trace_number").val()+')');
	}if ($("#idmetode").val()=="5"){//Karyawan
		$("#div_jaminan").hide();
		$("#div_bank").hide();
		$("#div_cc").hide();
		$("#div_trace").hide();
		$("#div_kontraktor").hide();
		$("#div_kontraktor2").hide();
		$("#div_tipepegawai").show();
		// $("#div_dokter").show();
		if ($("#tipepegawai").val()=='1'){
			$("#div_pegawai").show();
			$("#div_dokter").hide();
			$("#ket").val('Tagihan Karyawan : '+$("#idpegawai option:selected").text());
		}else{
			$("#div_pegawai").hide();
			$("#div_dokter").show();
		}
	}if ($("#idmetode").val()=="6"){//Tidak Tertagihkan
		$("#div_jaminan").show();
		$("#div_bank").hide();
		$("#div_trace").hide();
		$("#div_cc").hide();
		$("#div_kontraktor").hide();
		$("#div_kontraktor2").hide();
		$("#div_pegawai").hide();
		$("#div_tipepegawai").hide();
		$("#div_dokter").hide();
		$("#ket").val('Tidak Tertagihkan dg Jaminan '+$("#jaminan").val()+' Karena : ');
	}if ($("#idmetode").val()=="7"){//Kontraktor
		$("#div_bank").hide();
		$("#div_trace").hide();
		$("#div_jaminan").hide();
		$("#div_cc").hide();
		$("#div_kontraktor").show();
		$("#div_kontraktor2").show();
		$("#div_pegawai").hide();
		$("#div_tipepegawai").hide();
		$("#div_dokter").hide();
		// $("#ket").val('Pembayaran Kontraktor : ');
		$("#ket").val('Pembayaran ' +$("#tipekontraktor option:selected").text()+' : '+$("#idkontraktor option:selected").text());
	}

});
$("#trace_number").keyup(function(){

			$("#ket").val($("#idmetode option:selected").text()+' : '+$("#idbank option:selected").text()+' ('+$("#trace_number").val()+')');

	});
$("#tipekontraktor").change(function(){
	// alert($("#id").val()+'/'+$(this).val());
	if ($(this).val()!=''){
		$.ajax({
		url: '{site_url}tkasir/find_kontraktor/'+$("#id").val()+'/'+$(this).val(),
		dataType: "json",
		success: function(data) {
		$('#idkontraktor').empty();
		$.each(data.detail, function (i,row) {
		$('#idkontraktor')
		.append('<option value="' + row.idkontraktor + '" selected>' + row.nama_kontraktor + '</option>');
		});
		}
		});
		// alert($("#idkontraktor").val());
		$("#ket").val($("#tipekontraktor option:selected").text()+' : '+$("#idkontraktor option:selected").text());
	}


});
$("#tipepegawai").change(function(){
	if ($("#tipepegawai").val()=="1"){//PEGAWAI
		$("#div_pegawai").show();
		$("#div_dokter").hide();

	}else if ($("#tipepegawai").val()=="2"){
		$("#div_pegawai").hide();
		$("#div_dokter").show();
	}

});
function formatNumber (num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}
$("#idkontraktor").change(function(){

	$("#ket").val($("#tipekontraktor option:selected").text()+' : '+$("#idkontraktor option:selected").text());

});
$("#idbank").change(function(){

	$("#ket").val($("#idmetode option:selected").text()+' : '+$("#idbank option:selected").text()+' ('+$("#trace_number").val()+')');

});
$("#idpegawai").change(function(){
	$("#ket").val('Tagihan Karyawan : '+$("#idpegawai option:selected").text());

});
$("#iddokter").change(function(){
	$("#ket").val('Tagihan Karyawan (Dokter) : '+$("#iddokter option:selected").text());

});
$("#ket_cc").keyup(function(){
	$("#ket").val($("#idmetode option:selected").text()+' : '+$("#idbank option:selected").text()+', Keterangan CC : '+$("#ket_cc").val());


});

$("#jaminan").keyup(function(){
	$("#ket").val('Tidak Tertagihkan dg Jaminan '+$("#jaminan").val()+' Karena : ');


});
function validate_detail()
{

	if($("#idmetode").val() == ""){
		sweetAlert("Maaf...", "Metode Pembayaran Harus dipilih!", "error");
		$("#idmetode").focus();
		return false;
	}
	if($("#idmetode").val() == null){
		sweetAlert("Maaf...", "Metode Pembayaran Harus dipilih!", "error");
		$("#idmetode").focus();
		return false;
	}
	if($("#idmetode").val() == '2' || $("#idmetode").val() == '3' || $("#idmetode").val() == '4'){
		if ($("#idbank").val()=='' || $("#idbank").val()== null){
			sweetAlert("Maaf...", "Bank Harus dipilih!", "error");
			$("#idbank").focus();
			return false;
		}
	}
	if($("#idmetode").val() == '3'){
		if ($("#ket_cc").val()=='' || $("#ket_cc").val()== null){
			sweetAlert("Maaf...", "CC Harus dipilih!", "error");
			$("#ket_cc").focus();
			return false;
		}
	}
	if($("#idmetode").val() == '5'){
		if($("#tipepegawai").val() == '1'){
			if ($("#idpegawai").val()=='' || $("#idpegawai").val()== null){
				sweetAlert("Maaf...", "Pegawai Harus dipilih!", "error");
				$("#idpegawai").focus();
				return false;
			}
		}else{
			if ($("#iddokter").val()=='' || $("#iddokter").val()== null){
				sweetAlert("Maaf...", "Dokter Harus dipilih!", "error");
				$("#iddokter").focus();
				return false;
			}

		}
	}
	if($("#idmetode").val() == '6'){
		if ($("#jaminan").val()=='' || $("#jaminan").val()== null){
			sweetAlert("Maaf...", "Jaminan Harus diisi!", "error");
			$("#jaminan").focus();
			return false;
		}
	}
	if($("#idmetode").val() == '7'){
		if ($("#idkontraktor").val()=='' || $("#idkontraktor").val()== null){
			sweetAlert("Maaf...", "Kontraktor Harus dipilih!", "error");
			$("#idpegawai").focus();
			return false;
		}
	}
	if (parseFloat($("#nominal").val()) < 1 || $("#nominal").val() == ''){
		sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");

		return false;
	}


	return true;
}
$("#btn_edit_pt_igd").click(function() {

	var duplicate = false;
	var content = "";

	if($("#nomor").val() != ''){

			var no = $("#nomor").val();
	}else{

		var no = $('#manage_tabel_pembayaran tr').length - 2;
		var content = "<tr>";

	}

	if(duplicate) return false;
	content += "<td >" + $("#pt_igd_tindakan").val() + "</td>"; //0 Nomor
	content += "<td align='right'>" + formatNumber($("#pt_igd_st").val()) + "</td>"; //1 Nomor
	content += "<td align='right'>" + formatNumber($("#pt_igd_kuantitas").val()) + "</td>"; //2 Nomor
	content += "<td >" + $("#pt_igd_dokter").val() + "</td>"; //3 Nomor
	content += "<td align='right'>" + formatNumber($("#pt_igd_diskon").val()) + " %</td>"; //4 Nomor
	content += "<td align='right'>" + formatNumber($("#pt_igd_gt").val()) + "</td>"; //5 Nomor
	content +="<td style='display:none'>1</td>";//6
	content +="<td style='display:none'>" + $("#t_jenis").val() + "</td>";//7
	content +="<td style='display:none'>" + $("#nomor_pt_igd").val() + "</td>";//8
	content +="<td style='display:none'>" + $("#pt_igd_iddetail").val() + "</td>";//9
	content +="<td style='display:none'>" + $("#pt_igd_idtindakan").val() + "</td>";//10
	content += "<td><div class='btn-group'><button type='button' class='btn btn-sm btn-info edit_p_tindakan_igd'><i class='glyphicon glyphicon-pencil'></i></button><button type='button' class='btn btn-sm btn-danger hapus_igd'><i class='glyphicon glyphicon-remove'></i></button></div></td>";
	content +="<td style='display:none'>" + $("#pt_igd_sarana").val() + "</td>";//12
	content +="<td style='display:none'>" + $("#pt_igd_pelayanan").val() + "</td>";//13
	content +="<td style='display:none'>" + $("#pt_igd_bhp").val() + "</td>";//14
	content +="<td style='display:none'>" + $("#pt_igd_perawatan").val() + "</td>";//15
	content +="<td style='display:none'>" + $("#pt_igd_st").val() + "</td>";//16
	content +="<td style='display:none'>" + $("#pt_igd_kuantitas").val() + "</td>";//17
	content +="<td style='display:none'>" + $("#pt_igd_diskon_rp").val() + "</td>";//18
	content +="<td style='display:none'>" + $("#pt_igd_gt").val() + "</td>";//19
	content +="<td style='display:none'>" + $("#pt_igd_sarana_disc").val() + "</td>";//20
	content +="<td  style='display:none'>" + $("#pt_igd_pelayanan_disc").val() + "</td>";//21
	content +="<td  style='display:none'>" + $("#pt_igd_bhp_disc").val() + "</td>";//22
	content +="<td  style='display:none'>" + $("#pt_igd_perawatan_disc").val() + "</td>";//23
	
	if($("#rowindex").val() != ''){
		content += "</tr>";
		$('#datatable-simrs tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
	}else{
		$('#datatable-simrs tbody').append(content);
	}

	sumTotalTindakan();
	gen_harga_diskon();
});

$("#btn_add_bayar").click(function() {
	if(!validate_detail()) return false;
	var duplicate = false;
	var content = "";

	if($("#nomor").val() != ''){

			var no = $("#nomor").val();
	}else{

		var no = $('#manage_tabel_pembayaran tr').length - 2;
		var content = "<tr>";

	}
	// alert($("#idkontraktor").val());
	if(duplicate) return false;
	content += "<td style='display:none'>" + $("#idmetode").val() + "</td>";//0 Nama Obat
	content += "<td >" + no + "</td>"; //1 Nomor
	content += "<td >" + $("#idmetode option:selected").text() + "</td>"; //2 Nama
	content += "<td >" + $("#ket").val() + "</td>"; //3 Keterangan
	content += "<td style='display:none'>" + $("#nominal").val() + "</td>"; //4 Nominal
	content += "<td >" + formatNumber($("#nominal").val()) + "</td>"; //5 Nominal
	content += "<td  style='display:none'>" + $("#idkasir").val() + "</td>"; //6 Kasir
	content += "<td  style='display:none'>" + $("#idmetode").val() + "</td>"; //7 Metode
	content += "<td  style='display:none'>" + $("#idpegawai").val() + "</td>"; //8 Metode
	content += "<td  style='display:none'>" + $("#idbank").val() + "</td>"; //9 Metode
	content += "<td  style='display:none'>" + $("#ket_cc").val() + "</td>"; //10 Metode
	content += "<td style='display:none'>" + $("#idkontraktor").val() + "</td>"; //11 Metode
	content += "<td  style='display:none'>" + $("#ket").val() + "</td>"; //12 Metode
	content += "<td>";
	<?php if (UserAccesForm($user_acces_form,array('1086'))){ ?>
	content += "	<button type='button' class='btn btn-sm btn-info edit'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;";
	<?}?>
	<?php if (UserAccesForm($user_acces_form,array('1087'))){ ?>
	content += "	<button type='button' class='btn btn-sm btn-danger hapus'><i class='glyphicon glyphicon-remove'></i></button>";
	<?}?>
	content += "	</td>";
	content += "<td  style='display:none'>" + $("#jaminan").val() + "</td>"; //14 Metode
	content += "<td  style='display:none'>" + $("#trace_number").val() + "</td>"; //15 Metode
	content += "<td  style='display:none'>" + $("#iddokter").val() + "</td>"; //16 ID Dokter
	content += "<td style='display:none'>" + $("#tipepegawai").val() + "</td>"; //17 Tipe Pegawai
	if ($("#idmetode").val() =='7'){
		content += "<td style='display:none'>" + $("#tipekontraktor").val() + "</td>"; //18 Tipe Kontraktor
	}else{
		content += "<td style='display:none'>0</td>"; //18 Tipe Kontraktor
	}
	if($("#rowindex").val() != ''){
		$('#manage_tabel_pembayaran tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
	}else{
		content += "</tr>";
		$('#manage_tabel_pembayaran tbody').append(content);
	}




	grand_total_bayar();


});
$(document).on("click",".hapus",function(){
if(confirm("Hapus Data ?") == true){

	$(this).closest('td').parent().remove();

}
grand_total_bayar();

});
$(document).on("click",".hapus_igd",function(){
if(confirm("Anda Yakin Hendak Menghapus Data  ?") == true){
	var content = "<tr>";
	content += "<td>pt_igd</td>"; //0 Nomor
	content += "<td>" + $(this).closest('tr').find("td:eq(9)").html() + "</td>"; //1 Nomor
	content += "</tr>";
	$('#tabel_hapus_igd tbody').append(content);
	// alert($(this).closest('tr').find("td:eq(0)").html());
	$(this).closest('td').parent().remove();
	sumTotalTindakan();
	gen_harga_diskon();
}


});
$(document).on("click",".hapus_admin",function(){
if(confirm("Anda Yakin Hendak Menghapus Data Administrasi ?") == true){

	var id_nya=$(this).closest('tr').find("td:eq(9)").html();
	$.ajax({
		url: '{site_url}tkasir/hapus_tarif_admin/',
		dataType: "JSON",
		method: "POST",
		data : {id: id_nya},
		success: function(data) {
			console.log(data);
			// $("#tharga").val(data.total);
			if (data==true){
				location.reload();
				// $("#cover-spin").hide();
			}
		}
	});

}


});
$(document).on("click",".edit",function(){

	$nominal = $(this).closest('tr').find("td:eq(4)").html();
	$idmetode = $(this).closest('tr').find("td:eq(0)").html();
	$idpegawai = $(this).closest('tr').find("td:eq(8)").html();
	$idbank = $(this).closest('tr').find("td:eq(9)").html();
	$ket_cc = $(this).closest('tr').find("td:eq(10)").html();
	$idkontraktor = $(this).closest('tr').find("td:eq(11)").html();
	$ket = $(this).closest('tr').find("td:eq(12)").html();
	$jaminan = $(this).closest('tr').find("td:eq(14)").html();
	$trace_number = $(this).closest('tr').find("td:eq(15)").html();
	$iddokter = $(this).closest('tr').find("td:eq(16)").html();
	$tipepegawai = $(this).closest('tr').find("td:eq(17)").html();
	$tipekontraktor = $(this).closest('tr').find("td:eq(18)").html();

	$('#idmetode').val($idmetode).trigger('change');
	$('#idpegawai').val($idpegawai).trigger('change');
	$('#iddokter').val($iddokter).trigger('change');
	$('#tipepegawai').val($tipepegawai).trigger('change');
	// masuk_tipe_kontraktor($('#tipekontraktor').val());
	$('#tipekontraktor').val($tipekontraktor).trigger('change');
	$('#idkontraktor').val($idkontraktor).trigger('change');
	$('#idbank').val($idbank).trigger('change');
	$('#ket_cc').val($ket_cc);
	$('#ket').val($ket);
	$('#nominal').val($nominal);
	$('#jaminan').val($jaminan);
	$('#trace_number').val($trace_number);

	$('#nominal_edit').val($nominal);
	$("#sisa_modal").val(parseFloat($("#sisa_rp").val())+parseFloat($("#nominal").val()));
	$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
	$("#nomor").val($(this).closest('tr').find("td:eq(1)").html());
	$("#modal_pembayaran").modal('show');


});

$(document).on("click",".edit_p_tindakan_igd",function(){

	$('#t_jenis').val($(this).closest('tr').find("td:eq(7)").html());
	$('#pt_igd_tindakan').val($(this).closest('tr').find("td:eq(0)").html());
	$('#pt_igd_dokter').val($(this).closest('tr').find("td:eq(3)").html());
	$('#pt_igd_diskon_rp').val($(this).closest('tr').find("td:eq(18)").html());
	// $('#pt_igd_diskon').val($(this).closest('tr').find("td:eq(18)").html());
	$('#pt_igd_iddetail').val($(this).closest('tr').find("td:eq(9)").html());
	$('#pt_igd_idtindakan').val($(this).closest('tr').find("td:eq(10)").html());

	$('#pt_igd_sarana').val($(this).closest('tr').find("td:eq(12)").html());
	$('#pt_igd_pelayanan').val($(this).closest('tr').find("td:eq(13)").html());
	$('#pt_igd_bhp').val($(this).closest('tr').find("td:eq(14)").html());
	$('#pt_igd_perawatan').val($(this).closest('tr').find("td:eq(15)").html());

	$('#pt_igd_total').val($(this).closest('tr').find("td:eq(16)").html());

	$('#pt_igd_kuantitas').val($(this).closest('tr').find("td:eq(17)").html());
	$('#pt_igd_diskon').val($(this).closest('tr').find("td:eq(4)").html());
	$('#pt_igd_gt').val($(this).closest('tr').find("td:eq(19)").html());

	$('#pt_igd_sarana_disc').val($(this).closest('tr').find("td:eq(20)").html());
	$('#pt_igd_pelayanan_disc').val($(this).closest('tr').find("td:eq(21)").html());
	$('#pt_igd_bhp_disc').val($(this).closest('tr').find("td:eq(22)").html());
	$('#pt_igd_perawatan_disc').val($(this).closest('tr').find("td:eq(23)").html());



	console.log($(this).closest('tr').find("td:eq(18)").html());
	console.log($(this).closest('tr').find("td:eq(21)").html());

	$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
	$("#nomor").val($(this).closest('tr').find("td:eq(8)").html());
	gen_gt_edit_pt_igd();
	$("#modal_edit_p_tindakan_igd").modal('show');
	terapkan_dari_edit();

});
function terapkan_dari_edit(){
	var harga_awal = 0;
	var diskon_rp = 0;
	var diskon_persen = 0;

	$('#manage_tabel_edit_igd tbody tr').each(function() {
		harga_awal = $(this).closest('tr').find("td:eq(1) input").val();
		diskon_rp = $(this).closest('tr').find("td:eq(3) input").val();
		diskon_persen = parseFloat(diskon_rp * 100) / parseFloat(harga_awal);
		total = parseFloat(harga_awal) - parseFloat(diskon_rp);
		
		$(this).closest('tr').find("td:eq(2) input").val(diskon_persen);
		$(this).closest('tr').find("td:eq(4) input").val(total);
	});
	gen_gt_edit_pt_igd();
	// hitung_st();
}
// function hitung_st(){
	// $("#pt_igd_st").val(parseFloat($("#pt_igd_sarana_tot").val())+parseFloat($("#pt_igd_pelayanan_tot").val())+parseFloat($("#pt_igd_bhp_tot").val())+parseFloat($("#pt_igd_perawatan_tot").val()));
// }
function grand_total_bayar()
{
	var total_grand = 0;
	$('#manage_tabel_pembayaran tbody tr').each(function() {
		total_grand +=parseFloat($(this).find('td:eq(4)').text());
	});


	$("#bayar_rp").val(total_grand);

	gen_sisa();
}
function validasi_save(){

	if (parseFloat($("#sisa_rp").val()) > 0){
		// alert($("#sisa_rp").val());
		$("#btn_save").attr('disabled',true);
	}else{
		$("#btn_save").attr('disabled',false);
	}
}
function validate_final(){

	$("*[disabled]").not(true).removeAttr("disabled");
	return true;
}

$(document).on("keyup", ".td_disc_persen", function () {
	if ($(this).val()==''){
		$(this).val(0);
	}
	if (parseFloat($(this).val()) > 100){//DISC % KEY UP
		$(this).val(100);
	}
	var harga_jadi=0;
	var harga_awal = $(this).closest('tr').find("td:eq(1) input").val();
	harga_jadi = (parseFloat(harga_awal) * (parseFloat($(this).val()/100)));

	// harga disc rp
	$(this).closest('tr').find("td:eq(3) input").val(harga_jadi);

	// total
	$(this).closest('tr').find("td:eq(4) input").val(harga_awal-harga_jadi);
	gen_gt_edit_pt_igd();
	return false;
});
$(document).on("keyup", ".td_disc_rp", function () {//Diskon RP Keyup
	var harga_awal = $(this).closest('tr').find("td:eq(1) input").val();
	if ($(this).val()==''){
		$(this).val(0);
	}
	if (parseFloat($(this).val()) > harga_awal){
		$(this).val(harga_awal);
	}
	var harga_jadi=0;
	harga_jadi = parseFloat($(this).val()*100) / parseFloat(harga_awal);
	// harga disc rp
	$(this).closest('tr').find("td:eq(2) input").val(harga_jadi);
	// total
	$(this).closest('tr').find("td:eq(4) input").val(harga_awal-$(this).val());
	gen_gt_edit_pt_igd();
	return false;
});
$(document).on("keyup", ".td_awal", function () {//Harga Awal RP Keyup
	if ($(this).val()==''){
		$(this).val(0);
	}
	var harga_awal = $(this).val();
	var diskon= parseFloat(parseFloat($(this).closest('tr').find("td:eq(3) input").val())*100) / parseFloat(harga_awal);
	$(this).closest('tr').find("td:eq(2) input").val(diskon);
	$(this).closest('tr').find("td:eq(4) input").val(harga_awal-parseFloat($(this).closest('tr').find("td:eq(3) input").val()));
	gen_gt_edit_pt_igd();
	return false;
});
// $(document).on("change", ".td_tot", function () {//Total Chante
	// console.log('Total Broo');

	// return false;
// });
$("#add_admin").click(function() {
	$("#modal_add_admin").modal('show');
});
$("#idadmin").change(function(){
		var idadmin=$(this).val();

		$.ajax({
			url: '{site_url}tkasir/get_tarif_admin/',
			dataType: "JSON",
			method: "POST",
			data : {idadmin: idadmin},
			success: function(data) {
				console.log();
				$("#tharga").val(data.total);
				$("#idadministrasi").val(data.idjenis);
			}
		});


	});
$("#btn_tambah_adm").click(function(){
	$("#modal_add_admin").modal('hide');
	$("#cover-spin").show();
	var idadmin=$("#idadmin").val();
	var idpendaftaran=$("#idpendaftaran").val();
	var idadministrasi=$("#idadministrasi").val();
	$.ajax({
		url: '{site_url}tkasir/save_tarif_admin/',
		dataType: "JSON",
		method: "POST",
		data : {idadmin: idadmin,idpendaftaran:idpendaftaran,idadministrasi:idadministrasi},
		success: function(data) {
			console.log(data);
			// $("#tharga").val(data.total);
			if (data==true){
				location.reload();
				// $("#cover-spin").hide();
			}
		}
	});


});
$(document).on("click","#btn_save",function(){
	// alert('Simpan');exit();
	// Tabel Non Racikan
	$("#cover-spin").show();
	var pos_tabel_pembayaran = $('table#manage_tabel_pembayaran tbody tr').get().map(function(row) {
	  return $(row).find('td').get().map(function(cell) {
		return $(cell).html();
	  });
	});

	$("#pos_tabel_pembayaran").val(JSON.stringify(pos_tabel_pembayaran));

	var pos_tabel_detail = $('table#datatable-simrs tbody tr').get().map(function(row) {
	  return $(row).find('td').get().map(function(cell) {
		return $(cell).html();
	  });
	});

	$("#pos_tabel_detail").val(JSON.stringify(pos_tabel_detail));

	var pos_tabel_hapus_igd = $('table#tabel_hapus_igd tbody tr').get().map(function(row) {
	  return $(row).find('td').get().map(function(cell) {
		return $(cell).html();
	  });
	});

	$("#pos_tabel_hapus_igd").val(JSON.stringify(pos_tabel_hapus_igd));


	$("#form-work").submit();
});

</script>
