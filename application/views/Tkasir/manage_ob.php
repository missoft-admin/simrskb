<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-rounded">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}tkasir/index" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-bottom: 0px;">
	</div>
<div class="block-content">
<form id="form"  class=" form-horizontal push-10-t" action="{base_url}/tkasir/save" method="post">
<input type="hidden"  name="json-detail-nonracikan" id="json-detail-nonracikan" value=""/>
<input type="hidden"  name="json-detail-racikan" id="json-detail-racikan" value=""/>
<input type="hidden"  name="json-detail-obat-racikan" id="json-detail-obat-racikan" value=""/>
<input type="hidden"  name="post-totalbarang" id="post-totalbarang" value=""/>
<input type="hidden"  name="id_penjualan" id="id_penjualan" value="{id_penjualan}"/>
<!-- Start Input Data Pasien -->
<?php if($st_rujukan_non_rujukan=='R'){?>
	<div class="col-sm-6 col-lg-12">
		<?php }else{?>
	<div class="col-sm-12 col-lg-12">
		<?php }?>
		<div class="block block-rounded block-bordered">
				<div class="block-header">
					<h3 class="block-title">DETAIL PASIEN</h3>
				</div>
				<div class="block-content">

					<table class="table">

						<?php
						if($st_rujukan_non_rujukan=='N'){
						?>
						<tr>
							<th width="20%">Tanggal Kasir <span class="text-danger">*</span></th>
							<td width="85%">
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
												<input class="js-datetimepicker form-control input-sm" <?=(UserAccesForm($user_acces_form,array('1073'))?'':'disabled')?> type="text" id="tanggal" name="tanggal"  value="<?=date('d-m-Y H:i:s')?>"/>
										</div>
								</div>
							</td>
						</tr>
						<tr>
							<th width="20%">No Transaksi<span class="text-danger">*</span></th>
							<td width="85%">
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
												<input readonly class="form-control input-sm" type="text" id="nopenjualan" name="nopenjualan" data-date-format="dd-mm-yyyy" value="{nopenjualan}"/>
										</div>
								</div>
							</td>
						</tr>
						<tr>
							<th width="20%">Tanggal Transaksi<span class="text-danger">*</span></th>
							<td width="85%">
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
												<input readonly class="form-control input-sm" type="text" id="tgl_trx" name="tgl_trx" data-date-format="dd-mm-yyyy" value="{tgl_trx}"/>
										</div>
								</div>
							</td>
						</tr>
						<tr>
							<th>Resep <span class="text-danger">*</span></th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-6">
											<select disabled class="js-select2 form-control input-sm" id="manage-select-resep" name="manage-select-resep">
											  <option <?=($statusresep=='0')?'selected':''?> value="0">Tidak Ada</option>
											  <option <?=($statusresep=='1')?'selected':''?> value="1">Ada</option>
											</select>
										</div>
								</div>
							</td>
						</tr>
						<tr>
							<th>Kategori <span class="text-danger">*</span></th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
											<input type="hidden" id="idkelompokpasien" name="idkelompokpasien" value="0" />
											<select disabled class="js-select2 form-control input-sm" id="idkategori" name="idkategori">
											  <option <?=($idkategori=='0')?'selected':''?> value="0">- Penjualan Bebas -</option>
											  <option <?=($idkategori=='1')?'selected':''?> value="1">Pasien RS</option>
											  <option <?=($idkategori=='2')?'selected':''?> value="2">Pegawai</option>
											  <option <?=($idkategori=='3')?'selected':''?> value="3">Dokter</option>
											</select>
										</div>
								</div>
							</td>
						</tr>
						<input type="hidden" class="form-control input-sm" readonly id="idpasien" name="idpasien" value="<?=$idpasien?>"/>
						<tr id="tr_cari_data" hidden>
							<th>No Medrec / ID <span class="text-danger">*</span></th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
									<div class="col-sm-6" id="data_pasien" hidden >
										<div class="input-group" >
											<input type="text" class="form-control input-sm" readonly id="no_medrec" name="no_medrec" value="{no_medrec}" />
											<span class="input-group-btn">
												<button id="btn_cari_data" disabled data-toggle="modal" data-target="#cari-pasien-modal"  class="btn btn-sm btn-info" type="button"><i class="fa fa-search"></i>  Cari Data</button>
											</span>
										</div>
									</div>
									<div class="col-sm-12" id="data_non_pasien" hidden >
										<select disabled name="dokter_pegawai_id" id="dokter_pegawai_id" data-placeholder="Cari Data" class="form-control  input-sm" style="width:100%"></select>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<th>Nama <span class="text-danger">*</span></th>
									<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
											<input  disabled type="text"  class="form-control input-sm" id="nama" name="nama" value="<?=$nama?>"/>
										</div>
								</div>
							</td>
						</tr>
						<tr>
							<th>Alamat <span class="text-danger">*</span></th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
											<textarea class="form-control input-sm"  id="alamat" name="alamat"><?=$alamat?></textarea>
										</div>
								</div>
							</td>
						</tr>
						<tr>
							<th>No. Telp</th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
									<div class="col-sm-12">
										<input type="text" class="form-control input-sm"  name="telprumah" id="telprumah" value="{telprumah}"/>
									</div>
								</div>
							</td>
						</tr>
						<?php
					}else{
							foreach($listpasien1 as $listpasien){
$mpasien=getwhere('id',$listpasien->idasalpasien,'mpasien_asal')->row();
$rowsPasien=getwhere('id',$listpasien->idasalpasien,'mpasien_asal')->num_rows();
								if($rowsPasien>0){
$asalpasien=$mpasien->nama;
								}else{
$asalpasien='';
								}
							?>
							<input type="hidden" name="asalrujukan" id="asalrujukan" value="<?=$asalrujukan?>">
							<input type="hidden" name="getIDtindakan" id="getIDtindakan" value="<?=(!empty($id_penjualan))?$listpasien->idtindakan:''?>">
							<input type="hidden" name="getIDpasien" id="getIDpasien" value="<?=(!empty($id_penjualan))?$listpasien->idpasien:''?>">
							<!-- <input type="hidden" name="getIDbatal" id="getIDbatal" value="{getIDbatal}"> -->
						<tr width="30%">
							<th width="20%">Tanggal <span class="text-danger">*</span></th>
							<td width="35%">
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
												<input class="js-datetimepicker form-control input-sm" <?=(UserAccesForm($user_acces_form,array('1073'))?'':'disabled')?> type="text" id="tanggal" name="tanggal"  value="<?=date('d-m-Y H:i:s')?>"/>
										</div>
								</div>
							</td>
							<th>Rujukan</th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
											<input readonly type="text" class="form-control input-sm" id="manage-input-rujukan-pasien" name="manage-input-rujukan-pasien"  value="<?=(!empty($id_penjualan))?$asalpasien:''?>"/>
										</div>
								</div>
							</td>
						</tr>
						<tr>
							<th>Status Pasien <span class="text-danger">*</span></th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
									<div class="col-sm-12">
										<label class="css-input css-radio css-radio-sm css-radio-primary push-10-r">
											<input type="radio" id="manage-input-status-pasien" name="manage-input-status-pasien" checked value="1" /><span></span> Pasien RS
										</label>
									</div>
								</div>
							</td>
							<th>Kelompok Pasien</th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
									<div class="col-sm-12">
										<input readonly type="text" class="form-control input-sm" id="manage-input-nama-kelompok" name="manage-input-nama-kelompok" value="<?=(!empty($id_penjualan))?$listpasien->namakelompok:''?>" />
										<input readonly type="hidden" class="form-control input-sm" id="idkelompokpasien" name="idkelompokpasien" value="<?=(!empty($id_penjualan))?$listpasien->idkelompokpasien:''?>" />
									</div>
								</div>
							</td>

						</tr>
						<tr>
							<th>Resep <span class="text-danger">*</span></th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
											<select class="js-select2 form-control input-sm" id="manage-select-resep" name="manage-select-resep">
											  <option <?=($statusresep=='1')?'selected':''?> value="1">Ada</option>
											  <option <?=($statusresep=='0')?'selected':''?> value="0">Tidak Ada</option>
											</select>
										</div>
								</div>
							</td>
							<?php if($listpasien->asalrujukan != 3){ ?>
								<th>Poliklinik</th>
								<td>
									<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
											<div class="col-sm-12">
												<input readonly type="text" class="form-control input-sm" id="manage-input-nama-poliklinik" name="manage-input-nama-poliklinik" value="<?=(!empty($id_penjualan))?$listpasien->namapoliklinik:''?>" />
											</div>
									</div>
								</td>
							<?php }else{ ?>
								<th>Kelas</th>
								<td>
									<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
											<div class="col-sm-12">
												<input readonly type="text" class="form-control input-sm" id="manage-input-nama-kelas" name="manage-input-nama-kelas" value="<?=(!empty($id_penjualan))?$listpasien->namakelas:''?>" />
											</div>
									</div>
								</td>
							<?php } ?>

						</tr>
						<tr>
							<th>No. Medrec <span class="text-danger">*</span></th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
											<input type="text" class="form-control input-sm" id="manage-input-medrec-pasien" name="manage-input-medrec-pasien" readonly value="<?=(!empty($id_penjualan))?$listpasien->no_medrec:''?>" />
										</div>
								</div>
							</td>

							<?php if($listpasien->asalrujukan != 3){ ?>
								<th>Alergi Obat</th>
								<td>
									<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
											<div class="col-sm-12">
												<textarea readonly class="form-control input-sm" id="manage-input-alergi-pasien" name="manage-input-alergi-pasien"><?=(!empty($id_penjualan))?$listpasien->alergi_obat:''?></textarea>
											</div>
									</div>
								</td>
							<?php }else{ ?>
								<th>Bed</th>
								<td>
									<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
											<div class="col-sm-12">
												<input readonly type="text" class="form-control input-sm" id="manage-input-nama-bed" name="manage-input-nama-bed" value="<?=(!empty($id_penjualan))?$listpasien->namabed:''?>" />
											</div>
									</div>
								</td>
							<?php } ?>



						</tr>
						<tr>
							<th>Nama Pasien <span class="text-danger">*</span></th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
											<input readonly type="text" class="form-control input-sm" id="manage-input-nama-pasien" name="manage-input-nama-pasien" value="<?=(!empty($id_penjualan))?$listpasien->namapasien:''?>" />
										</div>
								</div>
							</td>
							<?php if($listpasien->asalrujukan != 3){ ?>

							<?php }else{ ?>
								<th>Alergi Obat</th>
								<td>
									<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
											<div class="col-sm-12">
												<textarea readonly class="form-control input-sm" id="manage-input-alergi-pasien" name="manage-input-alergi-pasien"><?=(!empty($id_penjualan))?$listpasien->alergi_obat:''?></textarea>
											</div>
									</div>
								</td>
							<?php } ?>

						</tr>
						<tr>
							<th>Alamat <span class="text-danger">*</span></th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
										<div class="col-sm-12">
											<textarea readonly class="form-control input-sm" id="manage-textarea-alamat-pasien" name="manage-textarea-alamat-pasien"><?=(!empty($id_penjualan))?$listpasien->alamat_jalan:''?></textarea>
										</div>
								</div>
							</td>

						</tr>
						<tr>
							<th>No. Telp</th>
							<td>
							<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
									<div class="col-sm-12">
										<input readonly type="text" class="form-control input-sm" name="manage-input-notelp-pasien" id="manage-input-notelp-pasien" value="<?=(!empty($id_penjualan))?$listpasien->telepon:''?>" />
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<th>Asal Pasien</th>
							<td>
								<div class="form-group" style="margin-top: 0px; margin-bottom: 0px;">
									<div class="col-sm-12">
										<input readonly type="text" class="form-control input-sm" id="manage-input-asal-pasien" name="manage-input-asal-pasien" value="<?=GetAsalRujukan($listpasien->asalrujukan)?>" />
									</div>
								</div>
							</td>
						</tr>
						<?php
					}
					}
					?>
					</table>
				<!-- </div>
			</div>
		</div> -->
	</div>
</div>
</div>
		<!-- End Input Data Pasien -->

		<!-- List Obat Non-Racikan -->
			<div class="row">
				<div class="col-sm-12">
					<div class="progress progress-mini">
						<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
					</div>
					<h5 style="margin-bottom: 10px;">Non Racikan</h5>
					<div class="control-group">
						<div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0;">
							<table id="manage-tabel-nonRacikan" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 20%;">Nama Obat</th>
										<th style="width: 7%;">Tarif</th>
										<th style="width: 5%;">Satuan</th>
										<th style="width: 7%;">Cara Pakai</th>
										<th style="width: 10%;">Aturan</th>
										<th style="width: 7%;">Qty</th>
										<th style="width: 5%;">Disc(%)</th>
										<th style="width: 7%;">Tuslah</th>
										<th style="width: 11%;">Expire</th>
										<th style="width: 10%;">Jumlah</th>
									</tr>

								</thead>
								<tbody>
									<?php if ($list_non_racikan){
										foreach($list_non_racikan as $row){

									?>
										<tr>
										<td><?=$row->nama?></td>
										<td style='display:none'><?=$row->idbarang?></td>
										<td style='display:none'><?=$row->harga?></td>
										<td align="right"><?=number_format($row->harga,0)?></td>
										<td><?=$row->singkatan?></td>
										<td><?=$row->carapakai?></td>
										<td><?=$row->jenispenggunaan?></td>
										<td align="right"><?=$row->kuantitas?></td>
										<td align="right"><?=$row->diskon?></td>
										<td align="right"><?=$row->tuslah?></td>
										<td align="center"><?=HumanDateShort($row->expire_date)?></td>
										<td align="right"><?=number_format($row->totalharga,0)?></td>
										<td style='display:none'><?=$row->totalharga?></td>
										<td  style='display:none'>
											<?php if($row->statusverifikasi == 0){ ?>
												<button type='button' class='btn btn-sm btn-info edit'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus'><i class='glyphicon glyphicon-remove'></i></button>
											<?php }else{ ?>
												<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>
											<?php } ?>
										</td>
										<td style='display:none'><?=$row->id?></td>
										<td style='display:none'><?=$row->statusverifikasi?></td>
										<td style='display:none'><?=$row->idtipe?></td>
										</tr>
										<?php }
									}?>
								</tbody>
								<tfoot id="foot-total-nonracikan">
									<tr>
										<th colspan="9" class="hidden-phone">

											<span class="pull-right"  class="pull-right"><b>TOTAL NON RACIKAN Rp.</b></span></th>
											<input type="hidden" id="tempgrandtotal" name="tempgrandtotal" value="{total_non_racikan}">
										<th colspan="2"><b><span id="lblgrandtotal"  class="pull-right"><?=number_format($total_non_racikan)?></span></b></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- Modal Obat Non Racikan -->
			<div class="modal fade in black-overlay" id="obat_non_racikan-modal" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg modal-dialog-popout">
					<div class="modal-content">
						<div class="block block-themed block-transparent remove-margin-b">
							<div class="block-header bg-success">
								<ul class="block-options">
									<li>
										<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
									</li>
								</ul>
								<h3 class="block-title">Daftar Obat</h3>
							</div>
							<div class="block-content">

									<table width="100%" id="table-Rujukan" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th>Kode Obat</th>
												<th>Nama Obat</th>
												<th>Satuan</th>
												<th>Stok</th>
												<th>Catatan</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
							</div>

						</div>
					</div>
				</div>
			</div>

		<!-- END Obat Racikan -->
		<!-- List Obat Racikan -->
			<div class="row">
				<div class="col-sm-12">
					<div class="progress progress-mini">
						<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
					</div>
					<h5 style="margin-bottom: 10px;">Racikan</h5>
					<div class="control-group">
						<div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0;">
							<table id="table_racikan_detail" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 20%;">Nama</th>
										<th style="width: 12%;">Jenis</th>
										<th style="width: 7%;">Cara Pakai</th>
										<th style="width: 9%;">Aturan</th>
										<th style="width: 5%;">Qty Racikan</th>
										<th style="width: 7%;">Tuslah</th>
										<th style="width: 10%;">Expire</th>
										<th style="width: 7%;">Harga</th>
										<th style="width: 10%;">Jumlah</th>
										<th style='display:none' >Actions</th>
									</tr>
									<tr>
										<td style='display:none'>
											<div id="" class="input-group">
												<input class="form-control input-sm nama_racikan" tabindex="9" type="text" id="rac_nama" />
												<span class="input-group-btn">
													<button class="btn btn-info input-sm btn_list_obat_racikan" tabindex="10"  id="btn_list_obat_racikan" data-toggle="modal"   type="button"><span id="lbl_rows_obat">List Obat (0)</span></button>
												</span>
											</div>
										</td>
										<td style='display:none'>
											<select style="width: 100%" tabindex="11"  class="form-control input-sm" id="rac_jenis">
											  <option value="0">-- Pilih Jenis --</option>
											  <option value="1">Kapsul</option>
											  <option value="2">Sirup</option>
											  <option value="3">Salep</option>
											  <option value="99">Lainnya</option>
											</select>
										</td>
										<td style='display:none'>
											<input class="form-control input-sm" tabindex="12" type="text" id="rac_cara_pakai" onkeypress="return hanyaAngka(event)" onfocus="inMask(this)" />
										</td>
										<td style='display:none'>
											<select id="rac_aturan"class="form-control input-sm" >
												<option value="SBM">SBM</option>
												<option value="SSM">SSM</option>
												<option value="BM">BM</option>
											</select>
										</td>


										<td style='display:none'>
											<input class="form-control input-sm " tabindex="13" type="text" id="rac_qty" onkeypress="return hanyaAngka(event)" />

										</td>
										<td style='display:none'>
											<input class="form-control input-sm" tabindex="14" type="text" id="rac_tuslah" onkeypress="return hanyaAngka(event)" />
										</td>

										<td style='display:none'><input class="js-datepicker form-control input-sm" type="text" tabindex="15" id="rac_expire" name="rac_expire"  value="<?=date('d-m-Y')?>"/></td>

										<td style='display:none'>
											<span id="rac_harga_label">0</span>
											<input class="form-control input-sm" type="hidden"  id="rac_harga" onkeypress="return hanyaAngka(event)" />
										</td style='display:none'>
										<td style='display:none'>
											<span id="rac_jumlah_label">0</span>
											<input type="hidden" id="rac_jumlah" value="0">
										</td>
										<td style='display:none'>
											<input type="hidden" id="statusverifikasi_rac" value="0">
											<input type="hidden" id="rac_jml_racikan" value="0">
											<input type="hidden" id="no_urut" value="{no_urut}">
											<input type="hidden" id="no_urut_akhir" value="{no_urut_akhir}">
											<input type="hidden" id="st_edit" value="0">
											<input type="hidden" id="rowindex_rac">
											<input type="hidden" id="nomor_rac">
											<button type="button" class="btn btn-sm  btn-primary" tabindex="16" id="rac_addjual" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
											<button type='button' class='btn btn-sm btn-danger' tabindex="17" id="rac_canceljual" title="Refresh"><i class='glyphicon glyphicon-remove'></i></button>
										</td>

								</thead>
								<tbody>
									<?php if (count($list_racikan)>0){
										foreach($list_racikan as $row){
											if ($row->id != null){
										$nama='<div class="input-group"><input class="form-control input-sm" type="text" readonly id="label_racikan" value="'.$row->namaracikan.'" /><span class="input-group-btn"><button class="btn btn-success input-sm btn_detail_racikan" data-toggle="modal"  type="button"><span id="lbl_rows_obat">List Obat ('.$row->total_row.')</span></button></span></div>';
									?>
										<tr>
											<td style='display:none'><?=$row->id?></td>
											<td style='display:none'><?=$row->namaracikan?></td>
											<td><?=$nama?></td>
											<td><?=jenis_racikan($row->jenisracikan)?></td>
											<td><?=($row->carapakai)?></td>
											<td><?=($row->jenispenggunaan)?></td>
											<td align="right"><?=$row->kuantitas?></td>
											<td align="right"><?=$row->tuslah?></td>
											<td align="center"><?=HumanDateShort($row->expire_date)?></td>
											<td align="right"><?=number_format($row->harga,0)?></td>
											<td align="right"><?=number_format($row->totalharga,0)?></td>
											<td style='display:none'><?=$row->totalharga?></td>
											<td style='display:none'>
												<?php if($row->statusverifikasi == 0){ ?>
													<button type='button' class='btn btn-sm btn-info rac_edit'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger rac_hapus'><i class='glyphicon glyphicon-remove'></i></button>
												<?php }else{ ?>
													<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>
												<?php } ?>
											</td>
											<td style='display:none'><?=$row->id?></td>
											<td style='display:none'><?='tabel'.$row->id?></td>
											<td style='display:none'><?=$row->harga?></td>
											<td style='display:none'><?=$row->total_row?></td>
											<td style='display:none'><?=$row->jenisracikan?></td>
											<td style='display:none'><?=$row->statusverifikasi?></td>

										</tr>
											<?php }}
									}?>
								</tbody>
								<tfoot>
									<tr>
										<th colspan="8" class="hidden-phone">

										<label class="pull-right"><b>TOTAL RACIKAN Rp.</b></label></th>
										<th colspan="2"><b>
											<input type="hidden" id="total_racikan" name="total_racikan" value="{total_harga_racikan}" />
											<label id="total_racikan_label"  class="pull-right"><?=number_format($total_harga_racikan,0)?></label></b>
										</th>
									</tr>

								</tfoot>
							</table>

					<input type="hidden" id="counter" name="counter" value="0" />
					<input type="hidden" id="counter_racikan" name="counter_racikan" value="0" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="progress progress-mini">
						<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
					</div>
					<h5 style="margin-bottom: 10px;">TOTAL</h5>
					<div class="control-group">
						<div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0;">
							<table id="div_subtotal" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<tfoot>
									<tr class="bg-light dker">
										<th style="width: 85%;"><label class="pull-right"><b>SUB TOTAL Rp.</b></label></th>
										<th style="width: 15%;"><b>
											<input class="form-control input-sm " readonly type="text" id="gt_all" name="gt_all" value="{totalall}" />

										</th>
									</tr>

									<tr class="bg-light dker">
										<th><label class="pull-right"><b>Discount</b></label></th>
										<th>
											<div class="input-group">
												<span class="input-group-addon">Rp</span>
												<input class="form-control" type="text" id="diskon_rp" name="diskon_rp" placeholder="Discount Rp" onkeypress="return hanyaAngka(event)" value="{diskon_rp}">
											</div>
											<div class="input-group">
												<span class="input-group-addon"> %. </span>
												<input class="form-control" type="text" id="diskon_persen" name="diskon_persen" placeholder="Discount %" onkeypress="return hanyaAngka(event)"  value="{diskon_persen}">
											</div>

										</th>
									</tr>
									<tr class="bg-light dker">
										<th><label class="pull-right"><b>TOTAL Rp.</b></label></th>
										<th><b>
											<input type="text"  class="form-control input-sm " readonly id="gt_rp" name="gt_rp" value="{gt_rp}" />

										</th>
									</tr>
									<tr class="bg-light dker">
										<th></th>
										<th>
											<button class="btn btn-success  btn_pembayaran" id="btn_pembayaran" data-toggle="modal"  data-target="#modal_pembayaran"  type="button">Pilih Cara Pembayaran</button>
										</th>
									</tr>

								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- Modal Pembayaran -->

			<div class="row">
				<div class="col-sm-12">
					<div class="progress progress-mini">
						<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
					</div>
					<h5 style="margin-bottom: 10px;">Pembayaran</h5>
					<div class="control-group">
						<div class="col-md-12" style="padding-left: 0; padding-right: 0; margin-left: 0;">
							<table id="manage_tabel_pembayaran" class="table table-striped table-bordered" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 5%;">#</th>
										<th style="width: 25%;">Jenis Pembayaran</th>
										<th style="width: 35%;">Keterangan</th>
										<th style="width: 10%;">Nominal</th>
										<th style="width: 10%;">Action</th>

									</tr>

								</thead>
								<input type="hidden" id="rowindex">
								<input type="hidden" id="nomor">
								<tbody>
									<?php if ($form_edit=='1'){
										$no=0;
										foreach($list_pembayaran as $row){
											$no=$no+1;
										?>
										<tr>
										<td style='display:none'><?= $row->idmetode ?></td>
										<td><?= $no ?></td>
										<td><?= metodePembayaranKasirRajal($row->idmetode) ?></td>
										<td><?= $row->keterangan ?></td>
										<td style='display:none'><?= $row->nominal ?></td>
										<td><?= number($row->nominal) ?></td>
										<td style='display:none'><?= $row->idkasir ?></td>
										<td style='display:none'><?= $row->idmetode ?></td>
										<td style='display:none'><?= $row->idpegawai ?></td>
										<td style='display:none'><?= $row->idbank ?></td>
										<td style='display:none'><?= $row->ket_cc ?></td>
										<td style='display:none'><?= $row->idkontraktor ?></td>
										<td style='display:none'><?= $row->keterangan ?></td>
										<td><button type='button' class='btn btn-sm btn-info edit'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus'><i class='glyphicon glyphicon-remove'></i></button></td>
										<td style='display:none'><?= $row->jaminan ?></td>
										<td style='display:none'><?= $row->trace_number ?></td>
										</tr>
									<?php }}?>
								</tbody>
								<tfoot id="foot_pembayaran">
									<tr class="bg-light dker">
										<th colspan="3"><label class="pull-right"><b>Pembayaran Rp.</b></label></th>
										<th colspan="2"><b>
											<input type="text"  class="form-control input-sm number" readonly id="bayar_rp" name="bayar_rp" value="{bayar_rp}" />

										</th>
									</tr>
									<tr class="bg-light dker">
										<th colspan="3"><label class="pull-right"><b>Sisa Rp.</b></label></th>
										<th colspan="2"><b>
											<input type="text"  class="form-control input-sm number" readonly id="sisa_rp" name="sisa_rp" value="{sisa_rp}" />
										</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade in black-overlay" id="modal_pembayaran" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg modal-dialog-popout">
					<div class="modal-content">
						<div class="block block-themed block-transparent remove-margin-b">
							<div class="block-header bg-success">
								<ul class="block-options">
									<li>
										<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
									</li>
								</ul>
								<h3 class="block-title">Pembayaran</h3>
							</div>
							<div class="block-content block-content-narrow">

							<div class="form-group">
								<label class="col-md-3 control-label" for="idmetode">Metode</label>
								<div class="col-md-7">
									<select name="idmetode" id="idmetode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="">Pilih Opsi</option>
										<option value="1">Tunai</option>
										<option value="2">Debit</option>
										<option value="3">Kartu Kredit</option>
										<option value="4">Transfer</option>
										<option value="5">Tagihan Karyawan</option>
										<option value="6">Tidak Tertagihkan</option>
										<option value="7">Kontraktor</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="nama">Sisa Rp. </label>
								<div class="col-md-7">
									<input  onkeypress="return hanyaAngka(event)" readonly type="text" class="form-control" id="sisa_modal" placeholder="Sisa" name="sisa_modal" required="" aria-required="true">
								</div>
							</div>
							<div class="form-group" id="div_bank" hidden>
								<label class="col-md-3 control-label" for="idbank">Bank</label>
								<div class="col-md-7">
									<select name="idbank" id="idbank" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="">Pilih Opsi</option>
										<?php foreach  ($list_bank as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
										<?php }?>

									</select>
								</div>
							</div>
							<div class="form-group" id="div_cc" hidden>
								<label class="col-md-3 control-label" for="nama">Ket. CC </label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="ket_cc" placeholder="Ket. CC" name="ket_cc" required="" aria-required="true">
								</div>
							</div>
							<div class="form-group" id="div_trace" hidden>
								<label class="col-md-3 control-label" for="nama">Trace Number</label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="trace_number" placeholder="Trace Number" name="trace_number" required="" aria-required="true">
								</div>
							</div>
							<div class="form-group" id="div_kontraktor" hidden>
								<label class="col-md-3 control-label" for="tipekontraktor">Tipe Kontraktor </label>
								<div class="col-md-7">

									<select name="tipekontraktor" id="tipekontraktor" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
										<option value="" selected>Pilih Opsi</option>
										<?foreach($arr_asuransi_id as $key){?>
										<option value="<?=$key->id?>"><?=$key->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group" id="div_kontraktor2" hidden>
								<label class="col-md-3 control-label" for="idkontraktor">Kontraktor</label>
								<div class="col-md-7">
									<select style="width:100%" name="idkontraktor" id="idkontraktor" data-placeholder="Kontraktor" class="form-control  input-sm"></select>
								</div>
							</div>
							<div class="form-group" id="div_tipepegawai" hidden>
								<label class="col-md-3 control-label" for="tipepegawai">Tipe Pegawai</label>
								<div class="col-md-7">
									<select style="width:100%" name="tipepegawai" class="js-select2 form-control"  id="tipepegawai" data-placeholder="Pegawai" class="form-control  input-sm">
										<option value="1" selected>Pegawai</option>
										<option value="2">Dokter</option>

									</select>
								</div>
							</div>
							<div class="form-group" id="div_pegawai" hidden>
								<label class="col-md-3 control-label" for="idpegawai">Pegawai</label>
								<div class="col-md-7">
									<select style="width:100%" name="idpegawai" id="idpegawai" data-placeholder="Pegawai" class="form-control  input-sm"></select>
								</div>
							</div>
							<div class="form-group" id="div_dokter" hidden>
								<label class="col-md-3 control-label" for="iddokter">Dokter</label>
								<div class="col-md-7">
									<select style="width:100%" name="iddokter" id="iddokter" data-placeholder="Dokter" class="form-control  input-sm">
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="nama">Nominal Rp. </label>
								<div class="col-md-7">
									<input  onkeypress="return hanyaAngka(event)" type="text" class="form-control" id="nominal" placeholder="Nominal" name="nominal" required="" aria-required="true">
								</div>
							</div>
							<div class="form-group" id="div_jaminan">
								<label class="col-md-3 control-label" for="nama">Jaminan </label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="jaminan" placeholder="Jaminan" name="jaminan" required="" aria-required="true">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="nama">Keterangan </label>
								<div class="col-md-7">
									<input type="text" class="form-control" id="ket" placeholder="Keterangan" name="ket" required="" aria-required="true">
								</div>
							</div>

							<div class="modal-footer">
								<button class="btn btn-sm btn-primary" id="btn_add_bayar" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Add</button>
								<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Exit</button>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Modal Obat Racikan -->
			<div class="modal fade in black-overlay" id="list_obat_racikan-modal" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg modal-dialog-popout">
					<div class="modal-content">
						<div class="block block-themed block-transparent remove-margin-b">
							<div class="block-header bg-success">
								<ul class="block-options">
									<li>
										<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
									</li>
								</ul>
								<h3 class="block-title">List Obat Racikan  UNTUK [<span id="nama_obat_racikan"></span>]</h3>
							</div>
							<div class="block-content">
								<table id="manage-tabel-Racikan" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 20%;">Nama Obat</th>
											<th style="width: 10%;">Harga</th>
											<th style="width: 5%;">Satuan</th>
											<th style="width: 7%;">Qty</th>
											<th style="width: 9%;">Disc(%)</th>
											<th style="width: 12%;">Jumlah</th>
											<th style='display:none'>Actions</th>
										</tr>
										<tr>
											<td style='display:none'><div id="" class="input-group">
												<select name="lbl_obat_id2" tabindex="16"  style="width: 100%" id="lbl_obat_id2" data-placeholder="Cari Obat" class="form-control input-sm"></select>
												<span class="input-group-btn">
													<button data-toggle="modal" data-target="#obat_racikan-modal" tabindex="17" class="btn btn-sm btn-info" type="button" id="search_obat_2"><i class="fa fa-search"></i></button>
												</span>
											</div></td>
											<td style='display:none'><label class="datatrans" id="lbltarif2"> - </label></td>
											<td style='display:none'><label class="datatrans" id="lblsatuan2"> - </label></td>

											<td style='display:none'><input type="text" class="form-control input-sm" tabindex="18" id="lbl_qty2"  onkeypress="return hanyaAngka(event)"/></td>
											<td style='display:none'><input type="text" class="form-control input-sm" tabindex="19" id="lbl_disc2"  onkeypress="return hanyaAngka(event)" /></td>
											<td style='display:none'><label class="datatrans" id="lbl_jumlah2"> - </label></td>
											<td style='display:none'>

												<input type="hidden" id="idtipe2" >
												<input type="hidden" id="tstok2" >
												<input type="hidden" id="nomor2" >
												<input type="hidden" id="rowindex2" >
												<input type="hidden" class="form-control" id="lbl_nama_obat2"/>
												<input type="hidden" class="form-control" id="ttarif2"/>
												<input type="hidden" class="form-control" id="tjumlah2"/>
												<button type="button" class="btn btn-sm  btn-primary" tabindex="20" id="addjual2" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
												<button type='button' class='btn btn-sm btn-danger' tabindex="21" id="canceljual2" title="Refresh"><i class='glyphicon glyphicon-remove'></i></button>
										</td>
										</tr>
									</thead>
									<tbody></tbody>
									<tfoot id="foot-total-nonracikan">
										<tr>
											<th colspan="5" class="hidden-phone">

												<span class="pull-right"><b>TOTAL</b></span></th>
												<input type="hidden" id="tempgrandtotal2" name="tempgrandtotal">
											<th colspan="2"><b><span id="lblgrandtotal2">0</span></b></th>
										</tr>
									</tfoot>
								</table>
							</div>
							<div class="modal-footer">

								<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Exit</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade in black-overlay" id="obat_racikan-modal" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg modal-dialog-popout" style="width:60%; margin-top:100px">
					<div class="modal-content">
						<div class="block block-themed block-transparent remove-margin-b">
							<div class="block-header bg-success">
								<ul class="block-options">
									<li>
										<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
									</li>
								</ul>
								<h3 class="block-title">Daftar Obat</h3>
							</div>
							<div class="block-content">

									<table width="100%" id="table-Rujukan2" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th>Kode Obat</th>
												<th>Nama Obat</th>
												<th>Satuan</th>
												<th>Stok</th>
												<th>Catatan</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
							</div>

						</div>
					</div>
				</div>
			</div>
		<!-- END Obat Racikan -->
			<div class="progress progress-mini">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<hr>
			<input type="hidden" name="idkasir"  id="idkasir" value="{idkasir}">
			<div class="text-right bg-light lter">
				<button class="btn btn-info" type="submit" id="btnSubmit-Penjualan" name="btnSubmit-Penjualan">Simpan <img style="display: none;" id="loading-button-ajax" src="{ajax}img/ajax-loader.gif"></button>
				<button class="btn btn-default" type="button" id="btnCancel-Penjualan" name="btnCancel-Penjualan">Batal</button>
			</div>
			<br>

<!-- Modal List nonRacikan -->




<!-- End Modal Data Pasien -->
<div id="data-obat-racikan" hidden>
	<?php if ($list_racikan_tabel){
		$idracikan='';
		$no=0;
		foreach($list_racikan_tabel as $row){
			if ($idracikan <> $row->idracikan){
				if ($idracikan <> ''){
					echo '</table>';
				}
				$no=0;
				$namatabel="tabel".$row->idracikan;
				echo '<input type="text" class="form-control  input-sm" name="pos_tabel'.$row->idracikan.'" id="pos_tabel'.$row->idracikan.'">';
				$idracikan=$row->idracikan;
				echo '<table id="'.$namatabel.'">';
			}
			$no=$no+1;
			$tjumlah=($row->harga_detail*$row->kuantitas_detail);
			$tjumlah2=$tjumlah - (($tjumlah*$row->diskon_detail)/100);
			echo '<tr>';
			echo '<td>'.$row->nama.'</td>';
			echo '<td>'.$row->idbarang.'</td>';
			echo '<td>'.$row->harga_detail.'</td>';
			echo '<td align="right">'.number_format($row->harga_detail).'</td>';
			echo '<td>'.$row->singkatan.'</td>';
			echo '<td align="right">'.$row->kuantitas_detail.'</td>';
			echo '<td align="right">'.$row->diskon_detail.'</td>';
			echo '<td>'.$tjumlah2.'</td>';
			echo '<td  align="right">'.number_format($tjumlah2).'</td>';
			echo "<td ><button type='button' class='btn btn-sm btn-info edit2'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus2'><i class='glyphicon glyphicon-remove'></i></button></td>";
			echo '<td>'.$no.'</td>';
			echo '<td>'.$row->idtipe.'</td>';
			echo '</tr>';
		}
		echo '</table>';
	}



	?>
</div>

<input type="hidden" name="nominal_edit"  id="nominal_edit" value="0">
<input type="hidden" name="st_browse"  id="st_browse" value="0">
<input type="hidden" name="totalbarang"  id="totalbarang" value="{totalbarang}">
<input type="hidden" name="idpegawaiapoteker"  id="idpegawaiapoteker" value="{idpegawaiapoteker}">
<input type="hidden" name="tgl_lahir"  id="tgl_lahir" value="{tgl_lahir}">
<input type="hidden" name="nopenjualan"  id="nopenjualan" value="{nopenjualan}">
<input type="hidden" name="status"  id="status" value="{status}">
<input type="hidden" name="form_edit"  id="form_edit" value="{form_edit}">
<input type="hidden" name="st_rujukan_non_rujukan"  id="st_rujukan_non_rujukan" value="{st_rujukan_non_rujukan}">
<input type="hidden" name="pos_tabel_pembayaran"  id="pos_tabel_pembayaran" >
</form>
</div>
</div>
<link rel="stylesheet" type="text/css" href="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="{ajax}tpasien-penjualan/js-clone.js"></script>
<!-- <script type="text/javascript" src="{ajax}tpasien-penjualan/manage.js"></script> -->
<script src="{js_path}pages/base_index_datatable.js"></script>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
function stopRKey(evt) {
  var evt = (evt) ? evt : ((event) ? event : null);
  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
}

document.onkeypress = stopRKey;

jQuery(function(){ BaseTableDatatables.init();
$("#btn_pembayaran").click(function() {
	$("#sisa_modal").val($("#sisa_rp").val());
	$("#nominal").val($("#sisa_rp").val());

	$("#nomor").val('')
	$("#trace_number").val('')
	$("#rowindex").val('')
	$('#idmetode').val('').trigger('change');
	$('#idbank').val('').trigger('change');
	$('#idpegawai').val('').trigger('change');
	$("#div_bank").hide();
	$("#div_kontraktor").hide();
	$("#div_kontraktor2").hide();
	$("#div_pegawai").hide();
	$("#div_cc").hide();
	$("#div_trace").hide();
	$("#div_jaminan").hide();
	$("#div_tipepegawai").hide();
	$("#ket").val('');
	$("#div_dokter").hide();

});
// $(document).ready(function() {
	// alert('SINI');

    $("#tanggal").datetimepicker({
				format: "DD-MM-YYYY HH:mm",
				// stepping: 30
			});

    $("#gt_rp").number(true,0,'.',',');
	$("#diskon_rp").number(true,0,'.',',');
	$("#diskon_persen").number(true,2,'.',',');
	$("#gt_all").number(true,0,'.',',');
	$("#bayar_rp").number(true,0,'.',',');
	$("#sisa_rp").number(true,0,'.',',');
	$("#sisa_modal").number(true,0,'.',',');
	$("#nominal").number(true,0,'.',',');
	gen_harga_diskon();
		if ($("#idkategori").val() != '0'){
				if ($("#idkategori").val() == '1'){//Jika  Pasien
					$("#data_pasien").attr('hidden',false);
					$("#data_non_pasien").attr('hidden',true);

				}else{
					$("#data_pasien").attr('hidden',true);
					$("#data_non_pasien").attr('hidden',false);
				}

			$("#tr_cari_data").attr('hidden',false);
			$idpasien=$("#idpasien").val();
			// $("#dokter_pegawai_id").append('<option value="'+$("#dokter_pegawai_id").val()+'">'+$("#dokter_pegawai_id").val()+'</option>');
			// $("#dokter_pegawai_id").select2('val',$("#dokter_pegawai_id").val());
			var newOption = new Option($("#nama").val(), $idpasien, false, false);
			$('#dokter_pegawai_id').append(newOption).trigger('change');
			$("#idpasien").val($idpasien);
			$("#nama").attr('readonly',true);
			$("#alamat").attr('readonly',true);
			$("#telprumah").attr('readonly',true);
			$("#btn_cari_data").attr('disabled',false);
		}else{
			$("#tr_cari_data").attr('hidden',true);

			$("#btn_cari_data").attr('disabled',true);
			$("#nama").attr('readonly',false);
			$("#alamat").attr('readonly',false);
			$("#telprumah").attr('readonly',false);
		}

	validasi_save();
	// $('.select2').select2();



	// DataTable
	$("#tipepegawai").change(function(){
		if ($("#tipepegawai").val()=="1"){//PEGAWAI
			$("#div_pegawai").show();
			$("#div_dokter").hide();

		}else if ($("#tipepegawai").val()=="2"){
			$("#div_pegawai").hide();
			$("#div_dokter").show();
		}

	});
	$("#tipekontraktor").change(function(){
		$("#idkontraktor").val(null).trigger('change');
		// alert('sini');
	});
	$("#idmetode").change(function(){
		if ($("#idmetode").val()=="1"){//CASH
			$("#div_jaminan").hide();
			$("#div_cc").hide();
			$("#div_trace").hide();
			$("#div_bank").hide();
			$("#div_kontraktor").hide();
			$("#div_kontraktor2").hide();
			$("#div_pegawai").hide();
			$("#ket").val('Tunai');
			$("#div_dokter").hide();
			$("#div_tipepegawai").hide();
		}if ($("#idmetode").val()=="2" || $("#idmetode").val()=="3" || $("#idmetode").val()=="4"){
			$("#div_kontraktor").hide();
			$("#div_kontraktor2").hide();
			$("#div_jaminan").hide();
			if ($("#idmetode").val()=="3"){
				$("#div_cc").show();
			}else{
				$("#div_cc").hide();
			}
			$("#div_trace").show();
			$("#div_pegawai").hide();
			$("#div_dokter").hide();
			$("#div_bank").show();
			$("#div_tipepegawai").hide();
			$("#ket").val($("#idmetode option:selected").text()+' : '+$("#idbank option:selected").text()+' ('+$("#trace_number").val()+')');
		}if ($("#idmetode").val()=="5"){//Karyawan
			$("#div_jaminan").hide();
			$("#div_bank").hide();
			$("#div_cc").hide();
			$("#div_trace").hide();
			$("#div_kontraktor").hide();
			$("#div_kontraktor2").hide();
			// $("#div_pegawai").show();
			$("#div_tipepegawai").show();
			$("#ket").val('Tagihan Karyawan : '+$("#idpegawai option:selected").text());
			if ($("#tipepegawai").val()=='1'){
				$("#div_pegawai").show();
				$("#div_dokter").hide();
				$("#ket").val('Tagihan Karyawan : '+$("#idpegawai option:selected").text());
			}else{
				$("#div_pegawai").hide();
				$("#div_dokter").show();
				$("#ket").val('Tagihan Dokter : '+$("#iddokter option:selected").text());
			}
		}if ($("#idmetode").val()=="6"){//Tidak Tertagihkan
			$("#div_jaminan").show();
			$("#div_bank").hide();
			$("#div_trace").hide();
			$("#div_cc").hide();
			$("#div_kontraktor").hide();
			$("#div_kontraktor2").hide();
			$("#div_tipepegawai").hide();
			$("#div_pegawai").hide();
			$("#div_dokter").hide();
			$("#ket").val('Tidak Tertagihkan dg Jaminan '+$("#jaminan").val()+' Karena : ');
		}if ($("#idmetode").val()=="7"){//Kontraktor
			$("#div_bank").hide();
			$("#div_trace").hide();
			$("#div_jaminan").hide();
			$("#div_cc").hide();
			$("#div_kontraktor").show();
			$("#div_kontraktor2").show();
			$("#div_pegawai").hide();
			$("#div_tipepegawai").hide();
			$("#div_dokter").hide();

			$("#ket").val('Pembayaran ' +$("#idmetode option:selected").text()+' : '+$("#idkontraktor option:selected").text());
			// $("#ket").val('Pembayaran Kontraktor : ');
		}

	});
	$("#trace_number").keyup(function(){

			$("#ket").val($("#idmetode option:selected").text()+' : '+$("#idbank option:selected").text()+' ('+$("#trace_number").val()+')');

	});
	$("#idkontraktor").change(function(){
		console.log($("#tipekontraktor").val());
		if ($("#tipekontraktor").val()=='3' || $("#tipekontraktor").val()=='4'){
			$("#ket").val($("#idmetode option:selected").text()+' '+$("#tipekontraktor option:selected").text() +' : '+$("#idkontraktor option:selected").text());
		}else{
			$("#ket").val($("#idmetode option:selected").text()+' : '+$("#idkontraktor option:selected").text());
		}

	});
	$("#idbank").change(function(){

		$("#ket").val($("#idmetode option:selected").text()+' : '+$("#idbank option:selected").text()+' ('+$("#trace_number").val()+')');

	});
	$("#idpegawai").change(function(){
		$("#ket").val('Tagihan Karyawan : '+$("#idpegawai option:selected").text());


	});
	$("#ket_cc").keyup(function(){
		$("#ket").val($("#idmetode option:selected").text()+' : '+$("#idbank option:selected").text()+', Keterangan CC : '+$("#ket_cc").val());


	});

	$("#jaminan").keyup(function(){
		$("#ket").val('Tidak Tertagihkan dg Jaminan '+$("#jaminan").val()+' Karena : ');


	});
	$("#iddokter").change(function(){
		$("#ket").val('Tagihan Karyawan (Dokter) : '+$("#iddokter option:selected").text());

	});
	$("#iddokter").select2({
		minimumInputLength: 2,
		noResults: 'Dokter Tidak Ditemukan.',
		// allowClear: true
		// tags: [],
		ajax: {
			url: '{site_url}tkasir/get_dokter/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,

		 data: function (params) {
			  var query = {
				search: params.term,
			  }
			  return query;
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							text: item.nama,
							id: item.id,
						}
					})
				};
			}
		}
	});
// });
	function formatNumber (num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}
	function gen_jumlah(){
		var total;
		if ($("#lbl_qty").val() != ''){
			total=parseFloat($("#ttarif").val())*parseFloat($("#lbl_qty").val());
		}else{
			total=0;
		}

		if ($("#lbl_disc").val() != ''){
			total=parseFloat(total) - parseFloat((total * $("#lbl_disc").val() /100));
		}
		if ($("#lbl_tuslah").val() != ''){
			total=parseFloat(total) + parseFloat(($("#lbl_tuslah").val()));
		}
		$("#tjumlah").val(total);
		$("#lbl_jumlah").text(formatNumber($("#tjumlah").val()));
	}
	function gen_harga_diskon(){
		var total;
		total=parseFloat($("#gt_all").val()) - parseFloat($("#diskon_rp").val());
		total=Math.ceil(total/100)*100;
		$("#gt_rp").val(total);
		gen_sisa();

	}
	function gen_sisa(){
		var total;
		total=parseFloat($("#gt_rp").val()) - parseFloat($("#bayar_rp").val());

		$("#sisa_rp").val(total);
		validasi_save();
	}

	function validasi_save(){

		if (parseFloat($("#sisa_rp").val()) > 0){
			// alert($("#sisa_rp").val());
			$("#btnSubmit-Penjualan").attr('disabled',true);
		}else{
			$("#btnSubmit-Penjualan").attr('disabled',false);
		}
	}
	function validate_detail()
	{

		if($("#idmetode").val() == ""){
			sweetAlert("Maaf...", "Metode Pembayaran Harus dipilih!", "error");
			$("#idmetode").focus();
			return false;
		}
		if($("#idmetode").val() == null){
			sweetAlert("Maaf...", "Metode Pembayaran Harus dipilih!", "error");
			$("#idmetode").focus();
			return false;
		}
		if($("#idmetode").val() == '2' || $("#idmetode").val() == '3' || $("#idmetode").val() == '4'){
			if ($("#idbank").val()=='' || $("#idbank").val()== null){
				sweetAlert("Maaf...", "Bank Harus dipilih!", "error");
				$("#idbank").focus();
				return false;
			}
		}
		if($("#idmetode").val() == '3'){
			if ($("#ket_cc").val()=='' || $("#ket_cc").val()== null){
				sweetAlert("Maaf...", "CC Harus dipilih!", "error");
				$("#ket_cc").focus();
				return false;
			}
		}
		if($("#idmetode").val() == '5'){
			if ($("#tipepegawai").val()=='1'){
				if ($("#idpegawai").val()=='' || $("#idpegawai").val()== null){
					sweetAlert("Maaf...", "Pegawai Harus dipilih!", "error");
					$("#idpegawai").focus();
					return false;
				}
			}else{
				if ($("#iddokter").val()=='' || $("#iddokter").val()== null){
					sweetAlert("Maaf...", "Dokter Harus dipilih!", "error");
					$("#iddokter").focus();
					return false;
				}
			}
		}
		if($("#idmetode").val() == '6'){
			if ($("#jaminan").val()=='' || $("#jaminan").val()== null){
				sweetAlert("Maaf...", "Jaminan Harus diisi!", "error");
				$("#jaminan").focus();
				return false;
			}
		}
		if($("#idmetode").val() == '7'){
			if ($("#idkontraktor").val()=='' || $("#idkontraktor").val()== null){
				sweetAlert("Maaf...", "Kontraktor Harus dipilih!", "error");
				$("#idpegawai").focus();
				return false;
			}
		}
		if (parseFloat($("#nominal").val()) < 1 || $("#nominal").val() == ''){
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");

			return false;
		}


		return true;
	}
	$("#btn_add_bayar").click(function() {
			if(!validate_detail()) return false;
			var duplicate = false;
			var content = "";

			if($("#nomor").val() != ''){

					var no = $("#nomor").val();
			}else{

				var no = $('#manage_tabel_pembayaran tr').length - 2;
				var content = "<tr>";
				// $('#manage_tabel_pembayaran tbody tr').filter(function (){
					// var $cells = $(this).children('td');
					// if($cells.eq(0).text() === $("#idmetode").val()){
						// // big_notification("Barang sudah ada didalam daftar","error");
						// duplicate = true;
						// sweetAlert("Duplicate...", "Data Pembayaran Duplicate!", "error");
					// }
				// });
			}

			if(duplicate) return false;
			content += "<td style='display:none'>" + $("#idmetode").val() + "</td>";//0 Nama Obat
			content += "<td >" + no + "</td>"; //1 Nomor
			content += "<td >" + $("#idmetode option:selected").text() + "</td>"; //2 Nama
			content += "<td >" + $("#ket").val() + "</td>"; //3 Keterangan
			content += "<td style='display:none'>" + $("#nominal").val() + "</td>"; //4 Nominal
			content += "<td >" + formatNumber($("#nominal").val()) + "</td>"; //5 Nominal
			content += "<td  style='display:none'>" + $("#idkasir").val() + "</td>"; //6 Kasir
			content += "<td  style='display:none'>" + $("#idmetode").val() + "</td>"; //7 Metode
			content += "<td  style='display:none'>" + $("#idpegawai").val() + "</td>"; //8 Id Pegawai
			content += "<td  style='display:none'>" + $("#idbank").val() + "</td>"; //9 Id Bank
			content += "<td  style='display:none'>" + $("#ket_cc").val() + "</td>"; //10 Ket CC
			content += "<td  style='display:none'>" + $("#idkontraktor").val() + "</td>"; //11 Metode
			content += "<td  style='display:none'>" + $("#ket").val() + "</td>"; //12 Metode
			content += "<td><button type='button' class='btn btn-sm btn-info edit'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus'><i class='glyphicon glyphicon-remove'></i></button></td>";
			content += "<td  style='display:none'>" + $("#jaminan").val() + "</td>"; //14 Metode
			content += "<td  style='display:none'>" + $("#trace_number").val() + "</td>"; //15 Metode
			content += "<td  style='display:none'>" + $("#iddokter").val() + "</td>"; //16 ID Dokter
			content += "<td style='display:none'>" + $("#tipepegawai").val() + "</td>"; //17 Tipe Pegawai
			if ($("#idmetode").val() =='7'){
				content += "<td style='display:none'>" + $("#tipekontraktor").val() + "</td>"; //18 Tipe Kontraktor
			}else{
				content += "<td style='display:none'>0</td>"; //18 Tipe Kontraktor
			}
			if($("#rowindex").val() != ''){
				$('#manage_tabel_pembayaran tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
			}else{
				content += "</tr>";
				$('#manage_tabel_pembayaran tbody').append(content);
			}




			grand_total_bayar();


	});
	$(document).on("click",".hapus",function(){
		if(confirm("Hapus Data ?") == true){

			$(this).closest('td').parent().remove();

		}
		grand_total_bayar();

	});
	$(document).on("click",".edit",function(){

		$nominal = $(this).closest('tr').find("td:eq(4)").html();
		$idmetode = $(this).closest('tr').find("td:eq(0)").html();
		$idpegawai = $(this).closest('tr').find("td:eq(8)").html();
		$idbank = $(this).closest('tr').find("td:eq(9)").html();
		$ket_cc = $(this).closest('tr').find("td:eq(10)").html();
		$idkontraktor = $(this).closest('tr').find("td:eq(11)").html();
		$ket = $(this).closest('tr').find("td:eq(12)").html();
		$jaminan = $(this).closest('tr').find("td:eq(14)").html();
		$trace_number = $(this).closest('tr').find("td:eq(15)").html();

		$('#idmetode').val($idmetode).trigger('change');
		$('#idpegawai').val($idpegawai).trigger('change');
		$('#idbank').val($idbank).trigger('change');
		$('#ket_cc').val($ket_cc);
		$('#ket').val($ket);
		$('#nominal').val($nominal);
		$('#jaminan').val($jaminan);
		$('#trace_number').val($trace_number);

		$('#nominal_edit').val($nominal);
		$("#sisa_modal").val(parseFloat($("#sisa_rp").val())+parseFloat($("#nominal").val()));
		$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);
		$("#nomor").val($(this).closest('tr').find("td:eq(1)").html());
		$("#modal_pembayaran").modal('show');


	});
	function grand_total_bayar()
	{
		var total_grand = 0;
		$('#manage_tabel_pembayaran tbody tr').each(function() {
			total_grand +=parseFloat($(this).find('td:eq(4)').text());
		});


		$("#bayar_rp").val(total_grand);

		gen_sisa();
	}

	$("#btn_list_obat_racikan").click(function() {
		$("#nama_obat_racikan").text($("#rac_nama").val());
		if ($("#lbl_rows_obat").text()=='List Obat (0)'){
			$("#tempgrandtotal2").val(0);
			$("#lblgrandtotal2").text(0);
			$('#manage-tabel-Racikan tbody tr').remove();
		}
		if ($("#st_edit").val()=='1'){//Jika Edit

			$('#manage-tabel-Racikan tbody tr').remove();
			$no_urut = $("#no_urut").val();
			var idtabel='tabel'+$no_urut;
			$("#list_obat_racikan-modal").modal('show');
			var content;

			content ='';
			var rows=0;
			var total=0
			$('#'+idtabel+' tbody tr').each(function() {
				content +='<tr>';
				content += "<td>" + $(this).find('td:eq(0)').text() + "</td>";//0 Nama Obat
				content += "<td style='display:none'>" + $(this).find('td:eq(1)').text() + "</td>"; //1 Kode barang ID Hidden
				content += "<td style='display:none'>" + $(this).find('td:eq(2)').text(); + "</td>";//2 tarif Hidden
				content += "<td>" + $(this).find('td:eq(3)').text(); + "</td>";//3
				content += "<td>" + $(this).find('td:eq(4)').text(); + "</td>";//4
				content += "<td align='right'>" + $(this).find('td:eq(5)').text(); + "</td>";//5
				content += "<td align='right'>" + $(this).find('td:eq(6)').text(); + "</td>";//6 Diskon
				content += "<td style='display:none'>" + $(this).find('td:eq(7)').text(); + "</td>";//7 Jumlah
				content += "<td>" + $(this).find('td:eq(8)').text(); + "</td>";//8 Jumlah
				content += "<td style='display:none'><button type='button' class='btn btn-sm btn-info edit2'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus2'><i class='glyphicon glyphicon-remove'></i></button></td>";
				content += "<td style='display:none'>" + $(this).find('td:eq(10)').text() + "</td>";//10 nomor
				content += "<td style='display:none'>" + $(this).find('td:eq(11)').text() + "</td>";//11 idtipe
				content +='</tr>';
				total +=parseFloat($(this).find('td:eq(7)').text());
				rows+=1;
			});
			$("#tempgrandtotal2").val(total);
			$("#lblgrandtotal2").text(formatNumber(total));
			$('#manage-tabel-Racikan tbody').append(content);
		}else{
			$("#no_urut").val($("#no_urut_akhir").val());
		}

		$("#list_obat_racikan-modal").modal('show');
		$("#btn_ok_racikan").attr('disabled',true);
		$("#lbl_obat_id2").attr('disabled',false);
		$("#search_obat_2").attr('disabled',false);
		$("#addjual2").attr('disabled',false);
		$("#canceljual2").attr('disabled',false);


	});
	$(document).on("click",".btn_detail_racikan",function(){
		$("#nama_obat_racikan").text($(this).closest('tr').find("td:eq(1)").html());
		$('#manage-tabel-Racikan tbody tr').remove();
		$no_urut = $(this).closest('tr').find("td:eq(0)").html();
		var idtabel=$(this).closest('tr').find("td:eq(14)").html();
		$("#list_obat_racikan-modal").modal('show');

		var content;

		content ='';
		var rows=0;
		var total=0
		$('#'+idtabel+' tbody tr').each(function() {
			content +='<tr>';
			content += "<td>" + $(this).find('td:eq(0)').text() + "</td>";//0 Nama Obat
			content += "<td style='display:none'>" + $(this).find('td:eq(1)').text() + "</td>"; //1 Kode barang ID Hidden
			content += "<td style='display:none'>" + $(this).find('td:eq(2)').text(); + "</td>";//2 tarif Hidden
			content += "<td>" + $(this).find('td:eq(3)').text(); + "</td>";//3
			content += "<td>" + $(this).find('td:eq(4)').text(); + "</td>";//4
			content += "<td align='right'>" + $(this).find('td:eq(5)').text(); + "</td>";//5
			content += "<td align='right'>" + $(this).find('td:eq(6)').text(); + "</td>";//6 Diskon
			content += "<td style='display:none'>" + $(this).find('td:eq(7)').text(); + "</td>";//7 Jumlah
			content += "<td>" + $(this).find('td:eq(8)').text(); + "</td>";//8 Jumlah
			content += "<td style='display:none'><button type='button' class='btn btn-sm btn-info edit2'><i class='glyphicon glyphicon-pencil'></i></button>&nbsp;&nbsp;<button type='button' class='btn btn-sm btn-danger hapus2'><i class='glyphicon glyphicon-remove'></i></button></td>";
			content += "<td style='display:none'>" + $(this).find('td:eq(10)').text() + "</td>";//10 nomor
			content += "<td style='display:none'>" + $(this).find('td:eq(11)').text() + "</td>";//11 idtipe
			content +='</tr>';
			total +=parseFloat($(this).find('td:eq(7)').text());
			rows+=1;
		});
		$("#tempgrandtotal2").val(total);
		$("#lblgrandtotal2").text(formatNumber(total));
		$('#manage-tabel-Racikan tbody').append(content);
	});



	$("#idkontraktor").select2({
        minimumInputLength: 1,
		noResults: 'Obat Tidak Ditemukan.',
		// allowClear: true
        // tags: [],
        ajax: {
            url: '{site_url}tkasir/get_kontraktor/',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
		// data : {nama:values},
		 data: function (params) {
              var query = {
                tipekontraktor: $("#tipekontraktor").val(),
                search: params.term,
              }
              return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nama,
                            id: item.id,
                        }
                    })
                };
            }
        }
    });
	$("#idpegawai").select2({
        minimumInputLength: 2,
		noResults: 'Pegawai Tidak Ditemukan.',
		// allowClear: true
        // tags: [],
        ajax: {
            url: '{site_url}tkasir/get_pegawai/',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,

		 data: function (params) {
              var query = {
                search: params.term,
              }
              return query;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nama,
                            id: item.id,
                        }
                    })
                };
            }
        }
    });


	$("#diskon_persen").keyup(function(){
		if ($("#diskon_persen").val()==''){
			$("#diskon_persen").val(0)
		}
		if (parseFloat($(this).val()) > 100){
			$(this).val(100);
		}
		var harga_jadi=0;
		harga_jadi= (parseFloat($("#gt_all").val()) * (parseFloat($("#diskon_persen").val()/100)));
		$("#diskon_rp").val(harga_jadi);
		gen_harga_diskon();
	});
	$("#diskon_rp").keyup(function(){
		if ($("#diskon_rp").val()==''){
			$("#diskon_rp").val(0)
		}
		if (parseFloat($(this).val()) > parseFloat($("#gt_all").val())){
			$(this).val(parseFloat($("#gt_all").val()));
		}
		var harga_jadi=0;
		harga_jadi= (parseFloat($("#diskon_rp").val()) *100) / (parseFloat($("#gt_all").val()));
		$("#diskon_persen").val(parseFloat(harga_jadi).toFixed(2));
		gen_harga_diskon();
	});




});
$(document).on("click","#btnSubmit-Penjualan",function(){
	// alert('Simpan');exit();
	// Tabel Non Racikan

	var pos_tabel_pembayaran = $('table#manage_tabel_pembayaran tbody tr').get().map(function(row) {
	  return $(row).find('td').get().map(function(cell) {
		return $(cell).html();
	  });
	});
	// alert(pos_tabel_pembayaran);
	// Tabel Racikan

	$("#pos_tabel_pembayaran").val(JSON.stringify(pos_tabel_pembayaran));


	$("#form").submit();
});
$("#nominal").keyup(function() {
	if ($("#nominal").val()==''){
		$("#nominal").val(0);
	}
	if ($("#nomor").val() != ''){//Jika Edit
		$("#sisa_modal").val(parseFloat($("#sisa_rp").val())-parseFloat($("#nominal").val())+parseFloat($("#nominal_edit").val()));
	}else{//Jika Bukan edit Edit
		$("#sisa_modal").val(parseFloat($("#sisa_rp").val())-parseFloat($("#nominal").val()));
	}


});
$("#btn_add_bayar").click(function() {

});


</script>
<script type="text/javascript" src="{ajax}basic-ajax.js"></script>

<!-- Load Lib Validation -->
<script src="{plugins_path}jquery-validation/jquery.validate.min.js"></script>
<script src="{plugins_path}jquery-validation/additional-methods.min.js"></script>
<script src="{plugins_path}js-validation/validation-tpasien-penjualan.js"></script>
<link rel="stylesheet" id="css-main" href="{toastr_css}">
<script src="{toastr_js}"></script>
