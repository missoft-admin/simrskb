<!-- Header Navigation Right -->
<aside id="side-overlay">
	<!-- Side Overlay Scroll Container -->
	<div id="side-overlay-scroll">
		<!-- Side Header -->
		<div class="side-header side-content">
			<!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
			<button class="btn btn-default pull-right" type="button" data-toggle="layout" data-action="side_overlay_close">
				<i class="fa fa-times"></i>
			</button>
			<span>
				<img class="img-avatar img-avatar32" src="{upload_path}avatars/<?=($user_avatar == '' ? 'default.jpg':$user_avatar); ?>" alt="">
				<span class="font-w600 push-10-l">{user_name}</span>
			</span>
		</div>
		<!-- END Side Header -->

		<!-- Side Content -->
		<div class="side-content remove-padding-t">
			<!-- Side Overlay Tabs -->
			<div class="block pull-r-l border-t">
				<ul class="nav nav-tabs nav-tabs-alt nav-justified" data-toggle="tabs">
					<li class="active">
						<a href="#tabs-side-overlay-overview"><i class="si si-fw si-info"></i> Overview</a>
					</li>
					
				</ul>
				<div class="block-content tab-content">
					<!-- Overview Tab -->
					<div class="tab-pane fade fade-right in active" id="tabs-side-overlay-overview">
						<!-- Activity -->
						<div class="block pull-r-l">
							<div class="block-header bg-gray-lighter">
								<ul class="block-options">
									<li>
										<button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
									</li>
									<li>
										<button type="button" data-toggle="block-option" data-action="content_toggle"></button>
									</li>
								</ul>
								<h3 class="block-title">Periode Akuntansi</h3>
							</div>
							<div class="block-content">
								<!-- Activity List -->
								<ul class="list list-activity">
									<li>
										<i class="si si-wallet text-success"></i>
										<div class="font-w600"><?=tanggal_indo($tanggal_awal_akuntansi)?></div>
										<div><a href="javascript:void(0)"><?=$periode_akuntansi?></a></div>
									</li>
									
								</ul>
								<!-- END Activity List -->
							</div>
							 
						</div>
						<!-- END Activity -->
						
						<div class="block pull-r-l">
							<div class="block-header bg-gray-lighter">
								<ul class="block-options">
									<li>
										<button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
									</li>
									<li>
										<button type="button" data-toggle="block-option" data-action="content_toggle"></button>
									</li>
								</ul>
								<h3 class="block-title">Users Online</h3>
							</div>
							
							<div class="block-content block-content-full">
								
								<ul class="nav-users remove-margin-b">
									<?
										foreach($rows_session as $row){
										$online = extract_user_data($row->data);
										// print_r($online);exit();
										// if(count($online) > 0){
											// $online = unserialize($row->data);
										// }
										?>
											<li>
												<a href="javascript:void(0)">
													<img class="img-avatar" src="{upload_path}avatars/<?=($online['user_avatar'] == '' ? 'default.jpg':$online['user_avatar']); ?>" alt="">
													<i class="fa fa-circle text-success"></i><?=$online['user_name']?>
													<div class="font-w400 text-muted"><small><?=$online['user_permission']?></small></div>
												</a>
											</li>
										<?
										}   
									?>
									
								</ul>
							</div>
						</div>
								<!-- END Users Navigation -->
						
					</div>
					
				</div>
			</div>
			<!-- END Side Overlay Tabs -->
		</div>
		<!-- END Side Content -->
	</div>
	<!-- END Side Overlay Scroll Container -->
</aside>
<ul class="nav-header pull-right">
    <li>
        <div class="btn-group">
            <button class="btn btn-default btn-image dropdown-toggle" data-toggle="dropdown" type="button">
                <img src="{upload_path}avatars/<?=($user_avatar == '' ? 'default.jpg':$user_avatar); ?>" alt="Avatar">
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-right">
                <li class="dropdown-header">{user_name}</li>
                <li>
                    <a tabindex="-1" href="{base_url}musers/profile">
                        <i class="si si-user pull-right"></i>Profile
                    </a>
                </li>
                <li class="divider"></li>
                <li class="dropdown-header">Actions</li>
                <li>
                    <a tabindex="-1" href="{base_url}musers/sign_out">
                        <i class="si si-logout pull-right"></i>Log out
                    </a>
                </li>
            </ul>
        </div>
    </li>
	<li>
		<!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
		<button class="btn btn-default" data-toggle="layout" data-action="side_overlay_toggle" type="button">
			<i class="fa fa-tasks"></i>
		</button>
	</li>
</ul>
<!-- END Header Navigation Right -->

<!-- Header Navigation Left -->
<ul class="nav-header pull-left">
    <li class="hidden-md hidden-lg">
        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
        <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
            <i class="fa fa-navicon"></i>
        </button>
    </li>
    <li class="hidden-xs hidden-sm">
        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
        <button class="btn btn-default" id="sidebar_menu" data-toggle="layout" data-action="sidebar_mini_toggle" type="button">
            <i class="fa fa-ellipsis-v"></i>
        </button>
    </li>
</ul>
<!-- END Header Navigation Left -->
