<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mpengaturan_form_order_laboratorium_patalogi_anatomi" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?php echo form_open('mpengaturan_form_order_laboratorium_patalogi_anatomi/save','class="form-horizontal push-10-t" id="form-work"') ?>
			<div class="form-group">
				<label class="col-md-12" for="">Pesan Informasi Berhasil</label>
				<div class="col-md-12">
					<textarea class="form-control summernote js-summernote-custom-height" name="pesan_informasi_berhasil" placeholder="Pesan Informasi Berhasil">{pesan_informasi_berhasil}</textarea>
				</div>
			</div>

			<h5 style="margin-bottom: 10px;" for="">Judul Formulir</h5>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_judul" placeholder="Judul Formulir (Indonesia Version)">{label_judul}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_judul_eng" placeholder="Judul Formulir (English Version)">{label_judul_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Nomor Spesimen</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_nomor_spesimen" <?=($required_nomor_spesimen == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_nomor_spesimen" placeholder="Nomor Spesimen (Indonesia Version)">{label_nomor_spesimen}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_nomor_spesimen_eng" placeholder="Nomor Spesimen (English Version)">{label_nomor_spesimen_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Sumber Spesimen Klinis</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_sumber_spesimen_klinis" <?=($required_sumber_spesimen_klinis == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_sumber_spesimen_klinis" placeholder="Sumber Spesimen Klinis (Indonesia Version)">{label_sumber_spesimen_klinis}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_sumber_spesimen_klinis_eng" placeholder="Sumber Spesimen Klinis (English Version)">{label_sumber_spesimen_klinis_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Lokasi Pengambilan Spesimen Klinis</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_lokasi_pengambilan_spesimen_klinis" <?=($required_lokasi_pengambilan_spesimen_klinis == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_lokasi_pengambilan_spesimen_klinis" placeholder="Lokasi Pengambilan Spesimen Klinis (Indonesia Version)">{label_lokasi_pengambilan_spesimen_klinis}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_lokasi_pengambilan_spesimen_klinis_eng" placeholder="Lokasi Pengambilan Spesimen Klinis (English Version)">{label_lokasi_pengambilan_spesimen_klinis_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Jumlah Spesimen</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_jumlah_spesimen" <?=($required_jumlah_spesimen == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_jumlah_spesimen" placeholder="Jumlah Spesimen (Indonesia Version)">{label_jumlah_spesimen}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_jumlah_spesimen_eng" placeholder="Jumlah Spesimen (English Version)">{label_jumlah_spesimen_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Volume Spesimen</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_volume_spesimen" <?=($required_volume_spesimen == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_volume_spesimen" placeholder="Volume Spesimen (Indonesia Version)">{label_volume_spesimen}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_volume_spesimen_eng" placeholder="Volume Spesimen (English Version)">{label_volume_spesimen_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Prioritas</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_cara_pengambilan_spesimen" <?=($required_cara_pengambilan_spesimen == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_cara_pengambilan_spesimen" placeholder="Prioritas (Indonesia Version)">{label_cara_pengambilan_spesimen}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_cara_pengambilan_spesimen_eng" placeholder="Prioritas (English Version)">{label_cara_pengambilan_spesimen_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Waktu Pengambilan</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_waktu_pengambilan" <?=($required_waktu_pengambilan == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_waktu_pengambilan" placeholder="Waktu Pengambilan (Indonesia Version)">{label_waktu_pengambilan}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_waktu_pengambilan_eng" placeholder="Waktu Pengambilan (English Version)">{label_waktu_pengambilan_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Kondisi Spesimen</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_kondisi_spesimen" <?=($required_kondisi_spesimen == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_kondisi_spesimen" placeholder="Kondisi Spesimen (Indonesia Version)">{label_kondisi_spesimen}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_kondisi_spesimen_eng" placeholder="Kondisi Spesimen (English Version)">{label_kondisi_spesimen_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Waktu Fiksasi Spesimen Klinis</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_waktu_fiksasi_spesimen_klinis" <?=($required_waktu_fiksasi_spesimen_klinis == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_waktu_fiksasi_spesimen_klinis" placeholder="Waktu Fiksasi Spesimen Klinis (Indonesia Version)">{label_waktu_fiksasi_spesimen_klinis}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_waktu_fiksasi_spesimen_klinis_eng" placeholder="Waktu Fiksasi Spesimen Klinis (English Version)">{label_waktu_fiksasi_spesimen_klinis_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Cairan Fiksasi</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_cairan_fiksasi" <?=($required_cairan_fiksasi == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_cairan_fiksasi" placeholder="Cairan Fiksasi (Indonesia Version)">{label_cairan_fiksasi}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_cairan_fiksasi_eng" placeholder="Cairan Fiksasi (English Version)">{label_cairan_fiksasi_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Volume Cairan Fiksasi</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_volume_cairan_fiksasi" <?=($required_volume_cairan_fiksasi == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_volume_cairan_fiksasi" placeholder="Volume Cairan Fiksasi (Indonesia Version)">{label_volume_cairan_fiksasi}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_volume_cairan_fiksasi_eng" placeholder="Volume Cairan Fiksasi (English Version)">{label_volume_cairan_fiksasi_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Nama Pengambil Spesimen</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_nama_pengambil_spesimen" <?=($required_nama_pengambil_spesimen == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_nama_pengambil_spesimen" placeholder="Nama Pengambil Spesimen (Indonesia Version)">{label_nama_pengambil_spesimen}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_nama_pengambil_spesimen_eng" placeholder="Nama Pengambil Spesimen (English Version)">{label_nama_pengambil_spesimen_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Nama Pengantar Spesimen</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_nama_pengantar_spesimen" <?=($required_nama_pengantar_spesimen == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_nama_pengantar_spesimen" placeholder="Nama Pengantar Spesimen (Indonesia Version)">{label_nama_pengantar_spesimen}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_nama_pengantar_spesimen_eng" placeholder="Nama Pengantar Spesimen (English Version)">{label_nama_pengantar_spesimen_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<label style="margin-bottom: 10px;" for="">Catatan / Keterangan</label>&nbsp; # &nbsp;  
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary" style="font-style: italic; color: red;">
					<input type="checkbox" name="required_catatan" <?=($required_catatan == 1 ? 'checked onclick="return false;"' : '')?> value="1"><span></span>
					Set Required
			</label>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>Indonesia Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_catatan" placeholder="Catatan / Keterangan (Indonesia Version)">{label_catatan}</textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-12" for=""><i>English Version</i></label>
						<div class="col-md-12">
							<textarea class="form-control summernote js-summernote-custom-height" name="label_catatan_eng" placeholder="Catatan / Keterangan (English Version)">{label_catatan_eng}</textarea>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 text-right">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mpengaturan_form_order_laboratorium_patalogi_anatomi" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>

			<br /><br />

	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
	});
</script>
