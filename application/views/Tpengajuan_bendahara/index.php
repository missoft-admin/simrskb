<!-- @tab navigation &start -->
<div class="content-grid">
    <div class="row">

        <?php if ($this->session->userdata('user_idpermission') != 5) { ?>
            <div class="col-xs-6 col-sm-4 col-lg-2">
                <a class="block block-link-hover2 text-center" href="{base_url}tpengajuan">
                    <div class="block-content block-content-full bg-modern">
                        <i class="si si-user fa-4x text-white"></i>
                        <div class="font-w600 text-white-op push-15-t text-uppercase">Unit</div>
                    </div>
                </a>
            </div>

            <?php if ($this->session->userdata('user_idpermission') == 4) { ?>
                <div class="col-xs-6 col-sm-4 col-lg-2">
                    <a class="block block-link-hover2 text-center" href="{base_url}tpengajuan_bendahara">
                        <div class="block-content block-content-full bg-primary-dark">
                            <i class="si si-user fa-4x text-white"></i>
                            <div class="font-w600 text-white-op push-15-t text-uppercase">Bendahara</div>
                        </div>
                    </a>
                </div>
            <?php } ?>

            <div class="col-xs-6 col-sm-4 col-lg-2">
                <a class="block block-link-hover2 text-center" href="{base_url}tpengajuan_unitpemroses">
                    <div class="block-content block-content-full bg-modern">
                        <i class="si si-user fa-4x text-white"></i>
                        <div class="font-w600 text-white-op push-15-t text-uppercase">Unit Proses</div>
                    </div>
                </a>
            </div>
        <?php } ?>

        <?php if ($this->session->userdata('user_idpermission') == 5) { ?>
            <div class="col-xs-6 col-sm-4 col-lg-2">
                <a class="block block-link-hover2 text-center" href="#">
                    <div class="block-content block-content-full bg-modern">
                        <i class="si si-user fa-4x text-white"></i>
                        <div class="font-w600 text-white-op push-15-t text-uppercase">Wadir</div>
                    </div>
                </a>
            </div>
        <?php } ?>

        <!-- 1 unit, 2 unit, 3 bendahara & 4 unit proses -->
        <!-- #comment : (1& 4 biasa) (1, 2, 4 & bendahara) (2 wadir) -->

        <!-- 5 wadir & 4 bendahara -->
        <!---->
    </div>
</div>
<!-- @tab navigation &end -->

<?php echo ErrorSuccess($this->session) ?>
<?php if ($error != '') echo ErrorMessage($error) ?>

<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
        <hr style="margin-bottom: 0;">
    </div>
    <div class="block-content">
        <div class="row">
            <?php echo form_open('tpengajuan_bendahara/filter', 'class="form-horizontal" id="form-filter"') ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label text-uppercase" for="">Pemohon</label>
                    <div class="col-md-8">
                        <input type="text" name="namapemohon" class="form-control" placeholder="pemohon"
                               value="{namapemohon}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label text-uppercase" for="">Unit Pelayanan</label>
                    <div class="col-md-8">
                        <select name="untukbagian" class="js-select2 form-control" style="width: 100%;"
                                data-placeholder="Pilih Opsi">
                            <option value="#" selected>Semua Unit Pelayanan</option>
                            <?php foreach ($list_unitpelayanan as $row) { ?>
                            <option value="<?= $row->id; ?>" <?= ($untukbagian == $row->id) ? "selected" : "" ?>><?= $row->nama ?>
                                /option>
                                <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label text-uppercase" for="jenispembayaran">Jenis Pembayaran</label>
                    <div class="col-md-8">
                        <select name="jenispembayaran" class="js-select2 form-control" style="width: 100%;"
                                data-placeholder="Pilih Opsi">
                            <option value="#" selected>Semua Jenis Pembayaran</option>
                            <option value="1" <?= ($jenispembayaran == 1 ? 'selected' : '') ?>>Tunai</option>
                            <option value="2" <?= ($jenispembayaran == 2 ? 'selected' : '') ?>>Kontrabon</option>
                            <option value="3" <?= ($jenispembayaran == 3 ? 'selected' : '') ?>>Transfer</option>
                            <option value="4" <?= ($jenispembayaran == 4 ? 'selected' : '') ?>>Termin By Progress
                            </option>
                            <option value="5" <?= ($jenispembayaran == 5 ? 'selected' : '') ?>>Termin Fix (Cicilan)
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label text-uppercase" for="stproses">Status</label>
                    <div class="col-md-8">
                        <select name="stproses" class="js-select2 form-control" style="width: 100%;"
                                data-placeholder="Pilih Opsi">
                            <option value="#" selected>Semua Status</option>
                            <option value="4" <?= ($stproses == 4 ? 'selected' : '') ?>>Aktivasi Bendahara</option>
                            <option value="5" <?= ($stproses == 5 ? 'selected' : '') ?>>Proses Belanja</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label text-uppercase" for="">Tanggal</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" name="tanggaldari" placeholder="From"
                                   value="22/11/2018">
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" name="tanggalsampai" placeholder="To"
                                   value="22/11/2018">
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label text-uppercase" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="submit" name="button"
                                style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
            </div>
            <?php echo form_close() ?>
        </div>
        <hr>

        <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
        <table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
            <thead>
            <tr>
                <th>No</th>
                <th>No. Pengajuan</th>
                <th>Tgl Pengajuan</th>
                <th>Tgl Dibutuhkan</th>
                <th>Pemohon</th>
                <th>Unit Memerlukan</th>
                <th>Subjek</th>
                <th>Nominal</th>
                <th>Persetujuan</th>
                <th>Status</th>
                <th>User Perintah</th>
                <th>Jenis Pembayaran</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    </div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
    // Initialize when page loads
    jQuery(function () {
        BaseTableDatatables.init();
        $('#datatable-simrs').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "scrollX": true,
            "order": [],
            "ajax": {
                url: '{site_url}tpengajuan_bendahara/getIndex/' + '<?=$this->uri->segment(2);?>',
                type: "POST",
                dataType: 'json'
            },
            "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": true
            },
                {
                    "width": "5%",
                    "targets": 1,
                    "orderable": true
                },
                {
                    "width": "5%",
                    "targets": 2,
                    "orderable": true
                },
                {
                    "width": "5%",
                    "targets": 3,
                    "orderable": true
                },
                {
                    "width": "5%",
                    "targets": 4,
                    "orderable": true
                },
                {
                    "width": "5%",
                    "targets": 5,
                    "orderable": true
                },
                {
                    "width": "5%",
                    "targets": 6,
                    "orderable": true
                },
                {
                    "width": "5%",
                    "targets": 7,
                    "orderable": true
                },
                {
                    "width": "5%",
                    "targets": 8,
                    "orderable": true
                },
                {
                    "width": "5%",
                    "targets": 9,
                    "orderable": true
                },
                {
                    "width": "5%",
                    "targets": 10,
                    "orderable": true
                },
                {
                    "width": "15%",
                    "targets": 11,
                    "orderable": true
                }
            ]
        });
    });

</script>
