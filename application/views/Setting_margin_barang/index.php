<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1810'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_margin()"><i class="fa fa-refresh"></i> Setting Margin</a>
		</li>
		<?}?>
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('1810'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?> " id="tab_info">
			
			<?php echo form_open('#','class="js-form1 validation form-horizontal" id="form-work"') ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
						<h5><?=text_primary('MARGIN BARANG')?></h5>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_margin">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="10%">Tipe Barang</th>
											<th width="10%">Kategori</th>
											<th width="10%">Barang</th>
											<th width="10%">Jenis</th>
											<th width="10%">Tujuan</th>
											<th width="10%">Poli / Kelas</th>
											<th width="10%">Kelompok</th>
											<th width="10%">Asuransi</th>
											<th width="10%">Resep</th>
											<th width="5%">Margin</th>
											<th width="10%">Action</th>										   
										</tr>
										<?
											$idtipe_barang='#';
											$idkategori_barang='0';
											$jenis_penjualan='0';
											$idbarang='0';
											$idtipe='0';
											$idpoli_kelas='0';
											$st_resep='0';
										?>
										<tr>
											<th>#</th>
											<th>
												<select id="idtipe_barang" name="idtipe_barang" class="js-select2 form-control" style="width: 200px;" data-placeholder="Choose one..">
													<option value="#" <?=($idtipe_barang=='#'?'selected':'')?>>-Pilih Tipe-</option>
													<?foreach(get_all('mdata_tipebarang') as $row){?>
													<option value="<?=$row->id?>"><?=$row->nama_tipe?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="idkategori_barang" name="idkategori_barang" class="js-select2 form-control" style="width: 200px;" data-placeholder="Choose one..">
													<option value="0" <?=($idkategori_barang=='0'?'selected':'')?>>-All-</option>
													
												</select>
											</th>
											<th>
												<select id="idbarang" name="idbarang" class="form-control" style="width: 300px;" data-placeholder="ALL">
												</select>
											</th>
											<th>
												<select id="jenis_penjualan" name="jenis_penjualan" class="js-select2 form-control" style="width: 150px;" data-placeholder="Choose one..">
													
													<option value="0" <?=($jenis_penjualan=='#'?'selected':'')?>>-ALL-</option>
													<option value="1" <?=($jenis_penjualan=='1'?'selected':'')?>>FARMASI</option>
													<option value="2" <?=($jenis_penjualan=='2'?'selected':'')?>>OBAT BEBAS</option>
													<option value="3" <?=($jenis_penjualan=='3'?'selected':'')?>>NON FARMASI</option>
													
												</select>
											</th>
											<th>
												<select id="idtipe" name="idtipe" class="js-select2 form-control" style="width: 150px;" data-placeholder="Choose one..">
													
													<option value="0" <?=($idtipe=='0'?'selected':'')?>>-SEMUA-</option>
													<option value="1" <?=($idtipe=='1'?'selected':'')?>>POLIKINIK</option>
													<option value="2" <?=($idtipe=='2'?'selected':'')?>>IGD</option>
													<option value="3" <?=($idtipe=='3'?'selected':'')?>>RANAP</option>
													<option value="4" <?=($idtipe=='4'?'selected':'')?>>ODS</option>
													
												</select>
											</th>
											<th>
												<select id="idpoli_kelas" name="idpoli_kelas" class="js-select2 form-control" style="width: 150px;" data-placeholder="Choose one..">
												</select>
											</th>
											
											<th>
												<select tabindex="4" id="idkelompokpasien" name="idkelompokpasien" class="js-select2 form-control" style="width: 150px;" data-placeholder="Pilih Opsi" required>
													<option value="0" selected>SEMUA</option>
													<?foreach(get_all('mpasien_kelompok') as $row){?>
													<option value="<?=$row->id?>"><?=$row->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select tabindex="4" id="idrekanan"  class="js-select2 form-control" style="width: 150px;" data-placeholder="Pilih Opsi" required>
													<option value="0" selected>SEMUA</option>
													<?foreach(get_all('mrekanan') as $row){?>
													<option value="<?=$row->id?>"><?=$row->nama?></option>
													<?}?>
												</select>
											</th>
											<th>
												<select id="st_resep" name="st_resep" class="js-select2 form-control" style="width: 150px;" data-placeholder="Choose one..">
													<option value="0" <?=($st_resep=='0'?'selected':'')?>>TIDAK DITENTUKAN</option>
													<option value="1" <?=($st_resep=='1'?'selected':'')?>>YA</option>
													<option value="2" <?=($st_resep=='2'?'selected':'')?>>TIDAK</option>
													
												</select>
											</th>
											
											<th>
												<input type="text" id="margin" class="form-control number" style="width: 100px;" value="0" required="" aria-required="true">
											</th>
											
											<th>
												<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_margin" onclick="add_margin()"><i class="fa fa-plus"></i> Tambah</button>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
function selec2Barang(){
	$("#idbarang").select2({
		minimumInputLength: 2,
		noResults: 'Barang Tidak Ditemukan.',
		// allowClear: true
		// tags: [],
		ajax: {
			// url: '{site_url}tpasien_penjualan/get_obat/',
			url: '{site_url}setting_margin_barang/get_obat/',
			dataType: 'json',
			type: "POST",
			quietMillis: 50,
		 data: function (params) {
			  var query = {
				search: params.term,
				idtipe: $("#idtipe_barang").val(),
				idkategori: $("#idkategori_barang").val(),
			  }
			  return query;
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							text: item.nama,
							id: item.idbarang,
						}
					})
				};
			}
		}
	});
}

$(document).ready(function(){	
		$(".number").number(true,1,'.',',');
	var tab=$("#tab").val();
	if (tab=='1'){
		load_margin();
	}
	if (tab=='2'){
			load_tab2();
	}
	set_poli();
	selec2Barang();
})	
$("#idtipe_barang").change(function(){
	// $("#idbarang").val(null).trigger('change');
	$.ajax({
		url: '{site_url}setting_margin_barang/get_kategori/',
		dataType: "json",
		 type: "POST" ,
		dataType: 'json',
		data : {
				idtipe : $("#idtipe_barang").val(),
			   },
		success: function(data) {
			// alert(data);
			$("#idkategori_barang").empty();
			$("#idkategori_barang").append(data);
		}
	});
	// selec2Barang();
});
function set_poli(){
		$.ajax({
			url: '{site_url}setting_margin_barang/get_poli/',
			dataType: "json",
			 type: "POST" ,
			dataType: 'json',
			data : {
					idtipe : $("#idtipe").val(),
				   },
			success: function(data) {
				// alert(data);
				$("#idpoli_kelas").empty();
				$("#idpoli_kelas").append(data);
			}
		});
}
function set_asuransi(){
	$.ajax({
		url: '{site_url}setting_margin_barang/get_asuransi/',
		dataType: "json",
		 type: "POST" ,
		dataType: 'json',
		data : {
				idkelompokpasien : $("#idkelompokpasien").val(),
			   },
		success: function(data) {
			// alert(data);
			$("#idrekanan").empty();
			$("#idrekanan").append(data);
		}
	});
}
$("#idtipe").change(function(){
	set_poli();
});
$("#idkelompokpasien").change(function(){
	set_asuransi();
});

function add_margin(){
	let idtipe_barang=$("#idtipe_barang").val();
	let margin=$("#margin").val();
	
	if (idtipe_barang=='#'){
		sweetAlert("Maaf...", "Tentukan Tipe Barang", "error");
		return false;
	}
	
	if (margin=='0'){
		sweetAlert("Maaf...", "Tentukan Margin", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}setting_margin_barang/simpan_margin', 
		dataType: "JSON",
		method: "POST",
		data : {
				idtipe_barang : $("#idtipe_barang").val(),
				idkategori_barang : $("#idkategori_barang").val(),
				idbarang : $("#idbarang").val(),
				jenis_penjualan : $("#jenis_penjualan").val(),
				asal_pasien : $("#idtipe").val(),
				idpoli_kelas : $("#idpoli_kelas").val(),
				idkelompokpasien : $("#idkelompokpasien").val(),
				idrekanan : $("#idrekanan").val(),
				st_resep : $("#st_resep").val(),
				margin : $("#margin").val(),

			},
		success: function(data) {
			// alert(data);
				$("#cover-spin").hide();
			if (data==true){
				load_margin();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});

			}else{
				swal({
					title: "Data Error!",
					text: "Dupicate Data.",
					type: "error",
					timer: 1500,
					showConfirmButton: false
				});
				
			}
		}
	});
}

function load_margin(){
	$('#index_margin').DataTable().destroy();	
	table = $('#index_margin').DataTable({
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			scrollX: true,
			"columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [3,2,4,5,6] },
					// { "width": "15%", "targets": [1]},
					// { "width": "8%", "targets": [8]},
					// { "width": "30%", "targets": 3,  className: "text-left" },
				],
            ajax: { 
                url: '{site_url}setting_margin_barang/load_margin', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function hapus_margin(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Logic?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}setting_margin_barang/hapus_margin',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_margin').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}
</script>