<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('224'))){ ?>
<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('226'))){ ?>
		<ul class="block-options">
			<li>
          <a href="{base_url}mtarif_operasi/create/{idtipe}" class="btn"><i class="fa fa-plus"></i></a>
			</li>
    </ul>
		<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?php if (UserAccesForm($user_acces_form,array('225'))){ ?>
		<hr style="margin-top:0px">
		<div class="row">
			<?php echo form_open('mtarif_operasi/filter','class="form-horizontal" id="form-work"') ?>
			<div class="col-md-4">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien">Tipe</label>
					<div class="col-md-8">
						<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#">Semua Tipe</option>
							<option value="1" <?=($idtipe == 1) ? "selected" : "" ?>>Ruangan</option>
							<option value="3" <?=($idtipe == 3) ? "selected" : "" ?>>Tindakan</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien">Jenis Operasi</label>
					<div class="col-md-8">
						<select name="idjenis" id="idjenis" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#">Semua Jenis</option>
							<?foreach($list_jenis as $row){?>
							<option value="<?=$row->id?>" <?=($idjenis == $row->id) ? "selected" : "" ?>><?=$row->nama?></option>
							<?}?>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<input type="hidden" class="form-control" id="index_filter" placeholder="index_filter" name="index_filter" value="{index_filter}" required="" aria-required="true">
			<?php echo form_close() ?>
		</div>
		<hr style="margin-top:10px">
		<?}?>
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		var uri;
		uri=$("#index_filter").val();;
		// alert(uri);
		$('#datatable-simrs').DataTable({
				"bSort": false,
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
				url: '{site_url}mtarif_operasi/getIndex/'+uri,
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "10%", "targets": 0, "orderable": true },
					{ "width": "70%", "targets": 1, "orderable": true },
					{ "width": "20%", "targets": 2, "orderable": true }
				]
			});
	});
</script>
