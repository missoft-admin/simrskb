<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
      <li>
          <a href="{base_url}mtarif_operasi/index/<?=$this->uri->segment(3)?>" class="btn"><i class="fa fa-reply"></i></a>
      </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mtarif_operasi/save_setting','class="form-horizontal push-10-t" id="form-work"') ?>
      <div class="form-group">
        <label class="col-md-3 control-label" for="nama">Nama</label>
        <div class="col-md-7">
          <input type="text" class="form-control" disabled value="{nama}">
        </div>
      </div>
	  <div class="form-group">
		<label class="col-md-3 control-label" for="total">Group Diskon Tarif</label>
		<div class="col-md-7">
			 <select name="group_diskon_all" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
				<option value="">Group Diskon Tarif</option>
				<?php foreach ($list_group_pembayaran as $row) { ?>
				  <option value="<?=$row->id?>" <?=($row->id == $group_diskon_all ? 'selected="selected"':'')?>><?=$row->nama?></option>
				<? } ?>
			  </select>
		</div>
	   </div>
    </div>
		<div class="block-content" id="form-detail">
			<hr>
			<div class="table-responsive">
			<table class="table table-bordered table-striped" id="setting_list">
				<thead>
					<tr>
						<th>Kelas</th>
						<th>Group Jasa Sarana</th>
						<th>Group Jasa Pelayanan</th>
						<th>Group BHP</th>
						<th>Group Biaya Perawatan</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach  ($list_tarif as $row) { ?>
						<tr>
							<td hidden><?=$row->idtarif?></td>
							<td hidden><?=$row->kelas?></td>
							<td style="text-align:center"><?=GetKelas($row->kelas)?></td>
							<td>
                <select name="group_jasasarana" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
                  <option value="">Group Pembayaran</option>
                  <?php foreach ($list_group_pembayaran as $group) { ?>
                    <option value="<?=$group->id?>" <?=($group->id == $row->group_jasasarana ? 'selected="selected"':'')?>><?=$group->nama?></option>
                  <? } ?>
                </select>
              </td>
							<td>
                <select name="group_jasapelayanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
                  <option value="">Group Pembayaran</option>
                  <?php foreach ($list_group_pembayaran as $group) { ?>
                    <option value="<?=$group->id?>" <?=($group->id == $row->group_jasapelayanan ? 'selected="selected"':'')?>><?=$group->nama?></option>
                  <? } ?>
                </select>
              </td>
							<td>
                <select name="group_bhp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
                  <option value="">Group Pembayaran</option>
                  <?php foreach ($list_group_pembayaran as $group) { ?>
                    <option value="<?=$group->id?>" <?=($group->id == $row->group_bhp ? 'selected="selected"':'')?>><?=$group->nama?></option>
                  <? } ?>
                </select>
              </td>
							<td>
                <select name="group_biayaperawatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
                  <option value="">Group Pembayaran</option>
                  <?php foreach ($list_group_pembayaran as $group) { ?>
                    <option value="<?=$group->id?>" <?=($group->id == $row->group_biayaperawatan ? 'selected="selected"':'')?>><?=$group->nama?></option>
                  <? } ?>
                </select>
              </td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
		</div>
		<div class="block-content" id="form-detail">
			<div class="form-group">
				<label class="col-md-2 control-label" ><?=text_danger('Setting Diskon')?></label>
			</div>
			<hr>
			<div class="table-responsive">
			<table class="table table-bordered table-striped" id="setting_diskon_list">
				<thead>
					<tr>
						<th>Kelas</th>
						<th>Group Jasa Sarana Diskon</th>
						<th>Group Jasa Pelayanan Diskon</th>
						<th>Group BHP Diskon</th>
						<th>Group Biaya Perawatan Diskon</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach  ($list_tarif as $row) { ?>
						<tr>
							<td hidden><?=$row->idtarif?></td>
							<td hidden><?=$row->kelas?></td>
							<td style="text-align:center"><?=GetKelas($row->kelas)?></td>
							<td>
                <select name="group_jasasarana_diskon" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
                  <option value="">Group Pembayaran</option>
                  <?php foreach ($list_group_pembayaran as $group) { ?>
                    <option value="<?=$group->id?>" <?=($group->id == $row->group_jasasarana_diskon ? 'selected="selected"':'')?>><?=$group->nama?></option>
                  <? } ?>
                </select>
              </td>
							<td>
                <select name="group_jasapelayanan_diskon" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
                  <option value="">Group Pembayaran</option>
                  <?php foreach ($list_group_pembayaran as $group) { ?>
                    <option value="<?=$group->id?>" <?=($group->id == $row->group_jasapelayanan_diskon ? 'selected="selected"':'')?>><?=$group->nama?></option>
                  <? } ?>
                </select>
              </td>
							<td>
                <select name="group_bhp_diskon" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
                  <option value="">Group Pembayaran</option>
                  <?php foreach ($list_group_pembayaran as $group) { ?>
                    <option value="<?=$group->id?>" <?=($group->id == $row->group_bhp_diskon ? 'selected="selected"':'')?>><?=$group->nama?></option>
                  <? } ?>
                </select>
              </td>
							<td>
                <select name="group_biayaperawatan_diskon" class="js-select2 form-control" style="width: 100%;" data-placeholder="Group Pembayaran">
                  <option value="">Group Pembayaran</option>
                  <?php foreach ($list_group_pembayaran as $group) { ?>
                    <option value="<?=$group->id?>" <?=($group->id == $row->group_biayaperawatan_diskon ? 'selected="selected"':'')?>><?=$group->nama?></option>
                  <? } ?>
                </select>
              </td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
		</div>
		<div class="row" style="margin-top:-25px">
			<div class="block-content block-content-narrow">
				<div class="form-group">
					<label class="col-md-3 control-label"></label>
					<div class="col-md-7">
						<button class="btn btn-success" type="submit">Simpan</button>
						<a href="{base_url}mtarif_operasi/index/<?=$this->uri->segment(3)?>" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
					</div>
				</div>
			</div>
		</div>

    <input type="hidden" name="setting_value" id="setting_value" value="">
	<input type="hidden" name="setting_diskon_value" id="setting_diskon_value" value="">
		<br><br>
		<?php echo form_hidden('id', $id); ?>
		<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
		$(".number").number(true, 0, '.', ',');

		$("#form-work").submit(function(e) {
			var form = this;

			var setting_tbl = $('table#setting_list tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function (cell) {
						if($(cell).find("select").length >= 1){
								return $(cell).find("select").val();
						}else{
								return $(cell).html();
						}
				});
			});

			$("#setting_value").val(JSON.stringify(setting_tbl));
			
			var setting_diskon_tbl = $('table#setting_diskon_list tbody tr').get().map(function(row) {
				return $(row).find('td').get().map(function (cell) {
						if($(cell).find("select").length >= 1){
								return $(cell).find("select").val();
						}else{
								return $(cell).html();
						}
				});
			});
			$("#setting_diskon_value").val(JSON.stringify(setting_diskon_tbl));
			
			swal({
				title: "Berhasil!",
				text: "Proses penyimpanan data.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});
	});
</script>
