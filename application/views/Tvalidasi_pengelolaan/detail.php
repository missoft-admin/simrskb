<style>
table.fixed {
      table-layout: fixed;
      width: 100%;
    }
    table.fixed td {
      overflow: hidden;
    }
</style>
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<?php echo form_open('tvalidasi_pengelolaan/save_detail','class="form-horizontal push-10-t"') ?>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Jurnal</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="nojurnal" placeholder="No. Jurnal" name="nojurnal" value="{nojurnal}">
                        <input type="hidden" readonly class="form-control" id="id" placeholder="No. Jurnal" name="id" value="{id}">
                        <input type="hidden" readonly class="form-control" id="st_posting" placeholder="No. Jurnal" name="st_posting" value="{st_posting}">
                        <input type="hidden" readonly class="form-control" id="disabel" placeholder="No. Jurnal" name="disabel" value="{disabel}">
                    </div>
                </div>
                
                
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Transaksi</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="no_terima" placeholder="No. Penerimaan" name="no_terima" value="{notransaksi}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Tanggal Hitung</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="distributor" placeholder="No. Jurnal" name="distributor" value="<?=HumanDateShort($tanggal_hitung)?>">
                    </div>
                </div>
				
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Pengelolaan</label>
                    <div class="col-md-8">
                        <input type="text" readonly class="form-control" id="nojurnal" placeholder="No. Jurnal" name="nojurnal" value="{nama_pengelolaan}">
                        
                    </div>
                </div>
                
                
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Deskripsi</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control" id="deskripsi_pengelolaan" placeholder="deskripsi_pengelolaan" name="deskripsi_pengelolaan" value="{deskripsi_pengelolaan}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">NOMINAL</label>
                    <div class="col-md-8">
                        <input type="text"  readonly class="form-control decimal" id="distributor" placeholder="No. Jurnal" name="distributor" value="{nominal}">
                    </div>
                </div>
				
			</div>
            
		
        </div>
	</div>
	<div class="block-content">
		 <hr style="margin-top:10px">
		<table class="table table-bordered table-striped fixed" id="index_list" style="width: 100%;">
			<thead>
				<tr>
					<th style="width:3%"  class="text-center">NO</th>
					<th style="width:5%"  class="text-center">TIPE</th>
					<th style="width:10%"  class="text-center">JENIS TRANSAKSI</th>
					<th style="width:15%"  class="text-center">NAMA TRANSAKSI</th>
					<th style="width:30%"  class="text-center">NO AKUN</th>
					<th  style="width:8%" class="text-center">DEBIT</th>
					<th style="width:8%" class="text-center">KREDIT</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	
	<div class="block-content">
		 <h3 class="block-title"><?=text_primary('Ringkasan')?></h3>
		 <hr style="margin-top:10px">
		<table class="table table-bordered table-striped fixed" id="ringkasan_list" style="width: 100%;">
			<thead>
				<tr>
					<th style="width:10%"  class="text-center">NO</th>
					<th style="width:20%"  class="text-center">NO AKUN</th>
					<th style="width:25%"  class="text-center">NAMA AKUN</th>
					<th style="width:15%"  class="text-center">DEBIT</th>
					<th style="width:15%"  class="text-center">KREDIT</th>
					
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<?if ($disabel==''){?>
	<div class="block-content">
		 <hr style="margin-top:10px">
		 <div class="row">
            <div class="col-md-12">
				<div class="form-group">
					<div class="col-xs-12 text-right">
						<a href="{base_url}tvalidasi_pengelolaan" class="btn btn-default" type="reset"><i class="fa fa-reply"></i> Batal</a>
						<button class="btn btn-success" name="btn_simpan" type="submit" value="2"><i class="si si-arrow-right"></i> Update & Posting</button>
						<button class="btn btn-primary" name="btn_simpan" type="submit" value="1"><i class="fa fa-save"></i> Update Akun</button>
					</div>
				</div>
				
            </div>
        </div>
		<hr style="margin-bottom:10px">		
		
	</div>
	<?}?>
</div>
<?php echo form_close(); ?>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	$(".decimal").number(true,2,'.',',');
	$("#cover-spin").show();
	load_index();
	load_ringkasan();
	
   
});

function load_index(){
	var id=$("#id").val();
	var disabel=$("#disabel").val();
	$('#index_list tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_pengelolaan/load_index/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel},
		success: function(data) {
			// console.log(data.tabel);
			$('#index_list').append(data.tabel);
			$(".opsi").select2({dropdownAutoWidth: false});
			$("#cover-spin").hide();
			// $(".tgl").datepicker();
			// $(".simpan").hide();
		}
	});
}

function load_ringkasan(){
	var id=$("#id").val();
	var disabel=$("#disabel").val();
	$('#ringkasan_list tbody').empty();
	$.ajax({
		url: '{site_url}tvalidasi_pengelolaan/load_ringkasan/',
		dataType: "json",
		type: "POST",
		data: {id: id,disabel: disabel},
		success: function(data) {
			$('#ringkasan_list').append(data.tabel);
			
		}
	});
}

</script>