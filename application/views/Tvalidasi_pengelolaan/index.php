<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">
       
        <h3 class="block-title">{title}</h3>
        <hr style="margin-top:10px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
                
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Jurnal</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="nojurnal" placeholder="No. Jurnal" name="nojurnal" value="{nojurnal}">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Transaksi</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_terima" placeholder="No. Transaksi" name="no_terima" value="{no_terima}">
                    </div>
                </div>
				              
            </div>
			<div class="col-md-6">
				
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Transaksi</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggal_trx1" name="tanggal_trx1" placeholder="From" value="{tanggal_trx1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_trx2" name="tanggal_trx2" placeholder="To" value="{tanggal_trx2}"/>
                        </div>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Status</label>
                    <div class="col-md-8">
						<select id="st_posting" name="st_posting" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Status -</option>
							<option value="0" <?=("0" == $st_posting ? 'selected' : ''); ?>>Menunggu Diposting</option>
							<option value="1" <?=("1" == $st_posting ? 'selected' : ''); ?>>Sudah Diposting</option>
							
						</select>
					</div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<table class="table table-bordered table-striped" id="index_list">
			<thead>
                <tr>
                    <th hidden width="2%">ID</th>
                    <th hidden width="2%">ID</th>
                    <th width="10%">NO</th>
                    <th width="10%">No Jurnal</th>
                    <th width="10%">No Transaksi</th>
                    <th width="10%">Tanggal Transaksi</th>
                    <th width="10%">Nama</th>
                    <th width="10%">Tanggal Hitung</th>
                    <th width="15%">Deskripsi</th>
                    <th width="15%">Nominal</th>
                    <th width="15%">Status</th>
                    <th width="15%">
						<div class="btn-group">
								<button class="btn btn-default  btn-xs" type="button">Button Aksi</button>
								<div class="btn-group">
									<button class="btn btn-default dropdown-toggle btn-xs" data-toggle="dropdown" aria-expanded="false">
										<span class="caret"></span>
									</button>
									<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a tabindex="-1" href="javascript:posting_all()"> Posting All</a>
										</li>										
									</ul>
								</div>
							</div>
					</th>
                </tr>
                
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>

<script src="{js_path}pages/base_ui_activity.js"></script>
<script src="{js_path}custom/basic.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">

var table;
var t;
var option;
$(document).ready(function(){
	$(".number").number(true,0,'.',',');
	load_index();
   
});
function load_index(){
	var iddistributor=$("#iddistributor").val();
	var nojurnal=$("#nojurnal").val();
	var no_terima=$("#no_terima").val();
	var jenis_retur=$("#jenis_retur").val();
	var status_penerimaan=$("#status_penerimaan").val();
	var asal_transaksi=$("#asal_transaksi").val();
	var tanggal_trx1=$("#tanggal_trx1").val();
	var tanggal_trx2=$("#tanggal_trx2").val();
	var st_posting=$("#st_posting").val();
	// alert(asal_transaksi);
	table = $('#index_list').DataTable({
            autoWidth: false,
            searching: false,
            pageLength: 50,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			columnDefs: [{ "targets": [0,1], "visible": false },
							{ "width": "3%", "targets": [2] },
							{ "width": "7%", "targets": [3,4,5,7,9,10] },
							// { "width": "9%", "targets": [9] },
							{ "width": "10%", "targets": [6,8,11] },
							{"targets": [2,9], className: "text-right" },
							{"targets": [11,3,4,5,7,10], className: "text-center" }
							
						 ],
            ajax: { 
                url: '{site_url}tvalidasi_pengelolaan/get_index', 
                type: "POST" ,
                dataType: 'json',
				data : {
						iddistributor:iddistributor,nojurnal:nojurnal,no_terima:no_terima,
						jenis_retur:jenis_retur,status_penerimaan:status_penerimaan,status:status,
						asal_transaksi:asal_transaksi,tanggal_trx1:tanggal_trx1,tanggal_trx2:tanggal_trx2,st_posting:st_posting
					   }
            }
        });
}
$("#btn_filter").click(function() {
	// alert('ID');
	table.destroy();
	load_index();
});

$(document).on("click", ".posting", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,0).data()
	var nopenerimaan=table.cell(tr,3).data()
	// alert(id);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Posting No. Jurnal "+nopenerimaan+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		posting(id);
	});
});

function posting($id){
	console.log($id);
	var id=$id;		
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tvalidasi_pengelolaan/posting',
		type: 'POST',
		data: {id: id},
		complete: function() {
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' posting'});
			table.ajax.reload( null, false ); 
			
		}
	});
}
function posting_all(){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Posting All ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		var arr_id=[];
		 var table = $('#index_list').DataTable();		
		 var data = table.rows().data();
		 
		 var table = $('#index_list').DataTable();		
		 var data = table.rows().data();
		 data.each(function (value, index) {
			 if (table.cell(index,1).data()=='0'){
				 id=table.cell(index,0).data();
					arr_id.push(id);
			 }
			
		 });
		 $.ajax({
				url: '{site_url}tvalidasi_pengelolaan/posting_all',
				type: 'POST',
				data: {id: arr_id},
				complete: function() {
					table.ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' posting'});
					
				}
			});
			
			// console.log(arr_id);
		 
			
		// $("#cover-spin").show();
		// batalkan(id);
	});
	
	// console.log($id);
	// var id=$id;		
	// table = $('#index_list').DataTable()	
	
}

$(document).on("click", ".batalkan", function() {
	var table = $('#index_list').DataTable();
	tr = table.row($(this).parents('tr')).index();
	var id=table.cell(tr,0).data()
	var nopenerimaan=table.cell(tr,3).data()
	// alert(id);return false;
	swal({
		title: "Anda Yakin ?",
		text : "Akan Membatalkan Posting No. Jurnal "+nopenerimaan+" ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		batalkan(id);
	});
});
function batalkan($id){
	// console.log($id);
	var id=$id;		
	table = $('#index_list').DataTable()	
	$.ajax({
		url: '{site_url}tvalidasi_pengelolaan/batalkan',
		type: 'POST',
		data: {id: id},
		complete: function() {
			table.ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Membatalkan Posting'});
		}
	});
}

</script>