<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">		
		<ul class="block-options">
			<li>
				<a href="{base_url}tbuku_besar_hutang_setting" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:10px">
		<div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
           <input type="hidden" class="form-control" readonly id="tipe_distributor" name="tipe_distributor" value="{tipe_distributor}" />
           <input type="hidden" class="form-control" readonly id="iddistributor" name="iddistributor" value="{iddistributor}" />
           <input type="hidden" class="form-control" readonly id="id" name="id" value="{id}" />
			<div class="col-md-6">				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="tanggal">Distributor</label>
                    <div class="col-md-10">
						<input type="text" class="form-control" readonly id="nama" name="nama" value="{nama}" />
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="tanggal">No Transaksi</label>
                    <div class="col-md-10">
						<input type="text" class="form-control"  id="index_notransaksi" name="index_notransaksi" placeholder="No. Transaksi" value="" />
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="tanggal">No Faktur</label>
                    <div class="col-md-10">
						<input type="text" class="form-control"  id="index_nofaktur" name="index_nofaktur" placeholder="No. Faktur" value="" />
					</div>
                </div>
               
			</div>
			<div class="col-md-6">				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="tanggal">Tipe Distributor</label>
                    <div class="col-md-9">
						<?=GetTipeDistributor($tipe_distributor);?>
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for="tanggal">Tipe Pemesanan</label>
                    <div class="col-md-9">
						<select id="index_tipe" name="index_tipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Tipe -</option>
							<option value="1">PEMESANAN GUDANG</option>
							<option value="2">RETUR</option>
							<option value="3">MODUL PENGAJUAN</option>							
						</select>
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-3 control-label" for="tanggal">Tanggal</label>
                    <div class="col-md-9">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggal_trx1" name="tanggal_trx1" placeholder="From" value="{tanggal_trx1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_trx2" name="tanggal_trx2" placeholder="To" value="{tanggal_trx2}"/>
                        </div>					
					</div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for=""></label>
                    <div class="col-md-9">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="index_list">
			<thead>
				<tr>
					<th width="10%">No Transaksi</th>
					<th width="8%">No Faktur</th>
					<th width="10%">Tanggal</th>
					<th width="15%">Nominal</th>
					<th width="25%">Setting AKun</th>
					<th width="5%">Aksi</th>
				</tr>
				<tr>
					<th>
						<div id="" class="input-group">
							<input type="hidden" class="form-control" readonly id="idedit" name="idedit" value="" />
							<input type="hidden" class="form-control" readonly id="idtransaksi" name="idtransaksi" value="" />
							<input type="hidden" class="form-control" readonly id="tipe_transaksi" name="tipe_transaksi" value="" />
							<input type="hidden" class="form-control" readonly id="nama_distributor" name="nama_distributor" value="" />
							<input type="text" class="form-control input-sm" readonly id="notransaksi" name="notransaksi" placeholder="No. Transaksi" value="" />
							<span class="input-group-btn">
								<button data-toggle="modal" <?=$disabel?> title="Cari Transaksi" class="btn btn-sm btn-success kunci" type="button" id="btn_cari_trx"><i class="fa fa-search"></i></button>
							</span>
						</div>
					</th>
					<th><input type="text" class="form-control input-sm" readonly id="nofaktur" name="nofaktur" placeholder="No. Faktur" value="" /></th>
					<th>
						<div class="input-group date">
							<input class="js-datepicker form-control input" readonly type="text" id="tanggal_transaksi" name="tanggal_transaksi" value="" data-date-format="dd-mm-yyyy"/>
							<span class="input-group-addon">
								<span class="fa fa-calendar"></span>
							</span>
						</div>
						
					</th>
					<th><input type="text" class="form-control decimal  input-sm" readonly id="nominal" name="nominal" placeholder="Nominal" value="" style="width:100%" /></th>
					<th>
						<select name="idakun"  id="idakun" <?=$disabel?>  class="js-select2 form-control  input-sm" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>-Belum dipilih-</option>
							<?foreach($list_akun as $row){?>
								<option value="<?=$row->id?>"><?=$row->noakun.' - '.$row->namaakun?></option>
							<?}?>
						</select>
					</th>
					<th>
						<?if ($disabel==''){?>
						<button type="button" class="btn btn-sm btn-primary kunci" tabindex="8" id="add" title="Masukan Item"><i class="fa fa-level-down"></i></button>
						<button type="button" class="btn btn-sm btn-success kunci" tabindex="8" id="btn_clear" title="Refresh"><i class="fa fa-refresh"></i></button>
						<?}?>
					</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<?$this->load->view('Tbuku_besar_hutang_setting/mod_cari_transaksi')?>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/number_format.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".decimal").number(true,2,'.',',');
	load_detail();
})
function load_detail() {
	// alert('sini');
	var iddistributor=$("#iddistributor").val();
	var tipe_distributor=$("#tipe_distributor").val();
	
	$('#index_list').DataTable().destroy();
	var table = $('#index_list').DataTable({
	"pageLength": 10,
	"ordering": false,
	"processing": true,
	"serverSide": true,
	"autoWidth": false,
	"fixedHeader": true,
	"searching": true,
	"order": [],
	"ajax": {
		url: '{site_url}Tbuku_besar_hutang_setting/load_transaksi_hutang/',
		type: "POST",
		dataType: 'json',
		data: {
			iddistributor: iddistributor,
			tipe_distributor: tipe_distributor,
			notransaksi: $("#index_notransaksi").val(),
			nofaktur: $("#index_nofaktur").val(),
			tipe: $("#index_tipe").val(),
			tanggal_trx1: $("#tanggal_trx1").val(),
			tanggal_trx2: $("#tanggal_trx2").val(),
		}
	},
	columnDefs: [
				 // {"targets": [0], "visible": false },
				 {  className: "text-right", targets:[3] },
				 {  className: "text-center", targets:[0,1,2,5] },
				 // { "width": "5%", "targets": [0] },
				 { "width": "10%", "targets": [1] },
				 { "width": "15%", "targets": [0,2,3,5] },
				 { "width": "35%", "targets": [4] },
				 // { "width": "15%", "targets": [2,13] }

				]
	});
}
$(document).on("click","#btn_cari_trx",function(){	
	cari_trx()
});
$(document).on("click","#btn_clear",function(){	
	clear_input()
});
function hapus($id){
	$.ajax({
		url: '{site_url}Tbuku_besar_hutang_setting/hapus_hutang/'+$id,
			type: "POST" ,
			dataType: 'json',			
			success: function(data) {
				swal("Berhasil!", "Input Data Transaksi!", "success");
				$('#index_list').DataTable().ajax.reload( null, false );
				clear_input();
			}
		});
	$("#modal_trx").modal('hide');
}
function pilih_hutang($tipe,$id){
	var idedit=$("#idedit").val();
	$.ajax({
		url: '{site_url}Tbuku_besar_hutang_setting/get_hutang/',
			type: "POST" ,
			dataType: 'json',
			data : {
					tipe:$tipe,
					idtransaksi:$id,
					idedit:idedit,
				   },
			success: function(data) {
				$("#idedit").val('');
				$("#idtransaksi").val(data.idtransaksi);
				$("#notransaksi").val(data.notransaksi);
				$("#nofaktur").val(data.nofaktur);
				$("#tanggal_transaksi").val(data.tanggal_transaksi);
				$("#nominal").val(data.nominal);
				$("#tipe_distributor").val(data.tipe_distributor);
				$("#tipe_transaksi").val(data.tipe_transaksi);
				$("#nama_distributor").val(data.nama_distributor);
				
			}
		});
	$("#modal_trx").modal('hide');
}
function get_edit($id){
	$(".edit").attr('disabled', true);
	$(".hapus").attr('disabled', true);

	var idedit=$("#idedit").val();
	$.ajax({
		url: '{site_url}Tbuku_besar_hutang_setting/get_edit/',
			type: "POST" ,
			dataType: 'json',
			data : {
					idtransaksi:$id,
				   },
			success: function(data) {
				$("#idedit").val($id);
				$("#idtransaksi").val(data.idtransaksi);
				$("#notransaksi").val(data.notransaksi);
				$("#nofaktur").val(data.nofaktur);
				$("#tanggal_transaksi").val(data.tanggal_transaksi);
				$("#nominal").val(data.nominal);
				$("#tipe_distributor").val(data.tipe_distributor);
				$("#tipe_transaksi").val(data.tipe_transaksi);
				$("#nama_distributor").val(data.nama_distributor);
				$("#idakun").val(data.idakun).trigger('change');
				
			}
		});
	$("#modal_trx").modal('hide');
}
function clear_input(){
	$("#idedit").val('');
	$("#idtransaksi").val('');
	$("#notransaksi").val('');
	$("#nofaktur").val('');
	$("#tanggal_transaksi").val('');
	$("#nominal").val(0);
	$("#tipe_transaksi").val('');
	$("#nama_distributor").val('');
	$(".edit").attr('disabled', false);
	$(".hapus").attr('disabled', false);
}
$(document).on("click","#add",function(){
	if (validate_final()==false)return false;
	
	
	$.ajax({
		url: '{site_url}Tbuku_besar_hutang_setting/simpan_hutang',
		type: 'POST',
		data: {
			 iddistributor:$("#iddistributor").val(),
			 idedit:$("#idedit").val(),
			 idtransaksi:$("#idtransaksi").val(),
			 tipe_transaksi:$("#tipe_transaksi").val(),
			 tipe_distributor:$("#tipe_distributor").val(),
			 tanggal_transaksi:$("#tanggal_transaksi").val(),
			 nama_distributor:$("#nama").val(),
			 notransaksi:$("#notransaksi").val(),
			 nofaktur:$("#nofaktur").val(),
			 nominal:$("#nominal").val(),
			 idakun:$("#idakun").val(),
		},
		success: function(data) {
			if (data=='true'){
				swal("Berhasil!", "Input Data Transaksi!", "success");
				$('#index_list').DataTable().ajax.reload( null, false );
				clear_input();
			}else{
				sweetAlert("Gagal...", "Penyimpanan Duplicate!", "error");
				
			}
		}
	});
});
function validate_final()
{
	if ($("#idakun").val()=='#'){
		sweetAlert("Maaf...", "Tentukan Akun", "error");
		return false;
	}
	if ($("#idtransaksi").val()==''){
		sweetAlert("Maaf...", "Pilih Transaksi Akun", "error");
		return false;
	}
	return true;
}


function cari_trx(){
	$("#modal_trx").modal('show');
	load_gudang();
}
$(document).on("click","#btn_filter",function(){	
	load_detail();		
});


</script>
