<div class="modal fade in black-overlay" id="modal_trx" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 65%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Cari Transaksi</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="cari_tipe_adm">TIPE</label>
									<div class="col-md-9">
										<select id="cari_tipe" name="cari_tipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
											<?if ($tipe_distributor=='1'){?>
											<option value="#">- Semua Tipe -</option>
											<option value="1" <?=("1" == $cari_tipe ? 'selected' : ''); ?>>PEMESANAN GUDANG</option>
											<option value="2" <?=("2" == $cari_tipe ? 'selected' : ''); ?>>RETUR</option>
											<?}else{?>
											<option value="3" <?=("3" == $tipe_distributor ? 'selected' : ''); ?>>MODUL PENGAJUAN</option>		
											<?}?>
										</select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 10px;">
									<label class="col-md-3 control-label" for="xke_id">Tanggal</label>
									<div class="col-md-9">
										<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="form-control" type="text" id="xtgl1" name="xtgl1" placeholder="From" value=""/>
											<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
											<input class="form-control" type="text" id="xtgl2" name="xtgl2" placeholder="To" value=""/>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-3 control-label" for="cari_tipe_adm">No Transaksi</label>
									<div class="col-md-9">
										<input type="text" style="width: 100%"  class="form-control" id="xnotransaksi" placeholder="No Transaksi" name="xnotransaksi" value="">									
									</select>
									</div>
								</div>
								<div class="form-group" style="margin-bottom: 10px;">
									<label class="col-md-3 control-label" for="cari_tipe_adm">No Faktur</label>
									<div class="col-md-9">
										<input type="text" style="width: 100%"  class="form-control" id="xnofaktur" placeholder="No Faktur" name="xnofaktur" value="">									
									</select>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label" for="btn_cari_penerimaan"></label>
									<div class="col-md-9">
										<button class="btn btn-sm btn-success" id="btn_cari_penerimaan" type="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-search"></i> Cari</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<table id="tabel_cari_trx" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 25%;" hidden></th>
								<th style="width: 25%;" hidden></th>
								<th style="width: 5%;">X</th>
								<th style="width: 10%;">Tipe</th>
								<th style="width: 10%;">Distributor</th>
								<th style="width: 10%;">No Transaksi</th>
								<th style="width: 20%;">No Fafktur</th>
								<th style="width: 10%;">Tanggal Transaksi</th>
								<th style="width: 10%;">Nominal</th>
								<th style="width: 20%;">ACTION</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">				
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Keluar</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
var table_permintaan;
	$(document).ready(function() {
		// $('#modal_trx').modal('show');
		// load_gudang();
	});
	$(document).on('click', '#btn_cari_penerimaan', function() {
		load_gudang();
	});
	// $(document).on('click', '.pilih_permintaan', function() {
		
	// });
	
	function load_gudang(){
		var cari_tipe=$("#cari_tipe").val();
		var xnotransaksi=$("#xnotransaksi").val();
		var iddistributor=$("#iddistributor").val();
		var xnofaktur=$("#xnofaktur").val();
		var xtgl1=$("#xtgl1").val();
		var xtgl2=$("#xtgl2").val();
		$('#tabel_cari_trx').DataTable().destroy();
		table_permintaan = $('#tabel_cari_trx').DataTable({
            autoWidth: false,
            searching: false,
            "lengthChange": false,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			
			columnDefs: [
							{ "targets": [0,1], "visible": false },
							{ "width": "3%", "targets": [2] },
							{ "width": "8%", "targets": [3,5,7,8] },
							{ "width": "10%", "targets": [9,6] },
							{ "width": "15%", "targets": [4] },
							{"targets": [2,8], className: "text-right" },
							{"targets": [3,5,6,7,9], className: "text-center" }
						 ],
            ajax: { 
                url: '{site_url}Tbuku_besar_hutang_setting/load_gudang', 
                type: "POST" ,
                dataType: 'json',
				data : {
						tipe:cari_tipe,
						iddistributor:iddistributor,
						xnotransaksi:xnotransaksi
						,no_fakur:xnofaktur
						,xtgl1:xtgl1
						,xtgl2:xtgl2
					   }
            }
        });
	}
	
	
	

</script>
