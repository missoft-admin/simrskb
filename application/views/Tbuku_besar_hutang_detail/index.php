<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="block">
	<div class="block-header">		
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:10px">
		<div class="row">
            <?php echo form_open('Tbuku_besar_hutang_detail/export','class="form-horizontal" id="form-work" target="_blank"') ?>
           
			<div class="col-md-10">				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="tanggal">Distributor</label>
                    <div class="col-md-10">
						<select name="iddistributor[]" id="iddistributor" <?=($iddistributor_gabungan?'disabled':'')?> class="js-select2 form-control" style="width: 100%;" multiple>
						<?php foreach  ($list_distributor as $row) { ?>
							<option value="'<?=$row->id?>'" <?=($iddistributor_gabungan==$row->id?'selected':'')?>><?=$row->nama?></option>
						<?php } ?>
					</select>
					</div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for="tanggal">Periode</label>
                    <div class="col-md-4">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggal_trx1" name="tanggal_trx1" placeholder="From" value="{tanggal_trx1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_trx2" name="tanggal_trx2" placeholder="To" value="{tanggal_trx2}"/>
                        </div>					
					</div>
                </div>
				
				<div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-2 control-label" for="tanggal">Tipe</label>
                    <div class="col-md-10">
						<select name="tipe_transaksi[]" id="tipe_transaksi" class="js-select2 form-control" style="width: 100%;" multiple>
							<option value="1">PEMBELIAN</option>
							<option value="2">RETUR</option>
							<option value="3">PENGAJUAN</option>
							<option value="4">PEMBAYARAN</option>						
					</select>
					</div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-2 control-label" for=""></label>
                    <div class="col-md-10">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter"><i class="fa fa-filter"></i> Filter</button>
						<button  class="btn btn-success" type="submit" name="btn_export" value="1"><i class="fa fa-file-excel-o"></i> Export Excel</button>
						<button  class="btn btn-danger" type="submit" name="btn_export" value="2"><i class="fa fa-file-pdf-o"></i> Export PDF</button>
						<button  class="btn btn-primary" type="submit" name="btn_export" value="3"><i class="fa fa-print"></i> Print</button>
					</div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="index_list">
			<thead>
				<tr>
					<th>Tanggal</th>
					<th>Kode</th>
					<th>Distributor</th>
					<th>Tipe</th>
					<th>Keterangan</th>
					<th>Saldo Awal</th>
					<th>Debit</th>
					<th>Kredit</th>
					<th>Saldo Akhir</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".decimal").number(true,2,'.',',');
	load_detail();
})

$(document).on("click","#btn_filter",function(){	
	load_detail();		
});
function load_detail() {
	// alert('sini');
	var iddistributor=$("#iddistributor").val();
	var tipe_transaksi=$("#tipe_transaksi").val();
	var tanggal_trx1=$("#tanggal_trx1").val();
	var tanggal_trx2=$("#tanggal_trx2").val();
	
	$('#index_list').DataTable().destroy();
	var table = $('#index_list').DataTable({
	"pageLength": 100,
	"ordering": false,
	"processing": true,
	"serverSide": true,
	"autoWidth": false,
	"fixedHeader": true,
	"searching": false,
	"order": [],
	"ajax": {
		url: '{site_url}Tbuku_besar_hutang_detail/getIndex/',
		type: "POST",
		dataType: 'json',
		data: {
			iddistributor: iddistributor,
			tanggal_trx1: tanggal_trx1,
			tanggal_trx2: tanggal_trx2,
			tipe_transaksi: tipe_transaksi,
		}
	},
	columnDefs: [
				 // {"targets": [0], "visible": false },
				 {  className: "text-right", targets:[5,6,7,8] },
				 {  className: "text-center", targets:[0,1,3,4] },
				 { "width": "5%", "targets": [1] },
				 { "width": "8%", "targets": [0,3,9] },
				 { "width": "15%", "targets": [2] },
				 { "width": "10%", "targets": [3,4,5,6,7,8] },
				 // { "width": "15%", "targets": [2,13] }

				]
	});
}

</script>
