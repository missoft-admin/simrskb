<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('1404'))){ ?>
<div class="block">
	<div class="block-header">
	
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:20px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">No Pengajuan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="no_pengajuan" placeholder="No Pengajuan" name="no_pengajuan" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Nama Pengajuan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="nama_kegiatan" placeholder="Nama Kegiatan / Pengajuan" name="nama_kegiatan" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Tipe Pengajuan</label>
                    <div class="col-md-8">
                       <select id="tipe_rka" name="tipe_rka" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua -</option>
							<option value="1" >RKA</option>
							<option value="2" >NON RKA</option>							
						</select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Unit Yang Mengajukan</label>
                    <div class="col-md-8">
                       <select id="idunit_pengaju" name="idunit_pengaju" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua -</option>
							<? foreach($list_unit as $row){ ?>
							<option value="<?=$row->idunit?>" ><?=$row->nama?></option>
							<?}?>
						</select>
                    </div>
                </div>
				
                                
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Status</label>
                    <div class="col-md-8">
                      <select id="status" name="status" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#">- All Status -</option>
							<option value="3" <?=($status=='3'?'selected':'')?>>PROSES BENDAHARA</option>
							<option value="4" <?=($status=='4'?'selected':'')?>>PROSES BELANJA</option>
							<option value="5" <?=($status=='5'?'selected':'')?>>SELESAI</option>
						
					</select>
                    </div>
                </div>	
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Jenis</label>
                    <div class="col-md-8">
                      <select id="idjenis" name="idjenis" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Jenis -</option>
						<? foreach($list_jenis as $row){ ?>
							<option value="<?=$row->id?>" <?=($idjenis==$row->id?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
                    </div>
                </div>	
				 <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Pengajuan</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggal_pengajuan1" name="tanggal_pengajuan1" placeholder="From" value="{tanggal_pengajuan1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_pengajuan2" name="tanggal_pengajuan2" placeholder="To" value="{tanggal_pengajuan2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 10px;">
                    <label class="col-md-4 control-label" for="tanggal">Tanggal Dibutuhkan</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" id="tanggal_dibutuhkan1" name="tanggal_dibutuhkan1" placeholder="From" value="{tanggal_dibutuhkan1}"/>
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" id="tanggal_dibutuhkan2" name="tanggal_dibutuhkan2" placeholder="To" value="{tanggal_dibutuhkan2}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
                
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_index">
			<thead>
				<tr>
					<th>#</th>
					<th>#</th>
					<th>#</th>
					<th>#</th>
					<th>No Pengajuan</th>
					<th>Tanggal Pengajuan</th>
					<th>Tipe</th>
					<th>Nama Pengajuan</th>
					<th>Unit Yang Mengajukan</th>
					<th>Tanggal Dibutuhkan</th>
					<th>Nominal</th>
					<th>Jenis</th>
					<th>Status</th>
					<th>TASK Unit</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<?}?>


<div class="modal fade in black-overlay" id="modal_user" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<input type="hidden" class="form-control" id="idrka" placeholder="" name="idrka" value="">
						<div class="col-md-12">
							<table width="100%" id="tabel_user_proses" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Step</th>
										<th>User</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						
					</div>
					<?php echo form_close() ?>
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
			
		</div>
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_user_lanjutan" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval Lanjutan</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<input type="hidden" class="form-control" id="idrka_lanjutan2" placeholder="" name="idrka_lanjutan2" value="">
						<div class="col-md-12">
							<table width="100%" id="tabel_user_proses_lanjutan" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Step</th>
										<th>User</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						
					</div>
					<?php echo form_close() ?>
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
			
		</div>
	</div>
</div>
<div class="modal fade" id="modal_tolak" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Alasan Tolak</h3>
                </div>
                <div class="block-content">
					<div class="row">
						<div class="col-md-12">						
							<div class="form-group" style="margin-top: 5px;">
								<label class="col-md-4 control-label" for="status">Alasan</label>
								<div class="col-md-8">
									<textarea class="form-control" rows="2" name="alasan_tolak" id="alasan_tolak"></textarea>
									<input type="hidden" readonly class="form-control" id="id_approval"  placeholder="id_approval" name="id_approval" value="">
								</div>
							</div>
						</div>							
					</div>
					
				</div>
            </div>
            <div class="modal-footer">
				<button class="btn btn-sm btn-danger text-uppercase" type="button" name="btn_tolak" id="btn_tolak"><i class="fa fa-save"></i> SIMPAN TOLAK</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade in black-overlay" id="modal_approval_lanjutan" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">List User Approval Lanjutan</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('mrka_bendahara/simpan_approval_lanjutan','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<input type="hidden" class="form-control" id="idrka_lanjutan" placeholder="" name="idrka_lanjutan" value="">
						<input type="hidden" class="form-control" id="id_edit" placeholder="" name="id_edit" value="">
						<div class="col-md-12">
							<table width="100%" id="tabel_user_lanjutan" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th width="20%">Step</th>
										<th width="60%">User</th>
										<th width="20%">Action</th>
									</tr>
									<tr>
										<th>
											<select name="step" style="width: 100%" id="step" class="js-select2 form-control input-sm">
												<?for($i=1;$i<10;$i++){?>
													<option value="<?=$i?>"><?=$i?></option>
												<?}?>
											</select>										
										</th>
										<th>
											<select name="iduser_lanjutan" tabindex="16"  style="width: 100%" id="iduser_lanjutan" data-placeholder="User" class="js-select2 form-control input-sm"></select>										
										</th>
										<th>
											<button type="button" class="btn btn-sm btn-primary" tabindex="8" id="simpan_detail_lanjutan" title="Simpan"><i class="fa fa-save"></i></button>
											<button type="button" class="btn btn-sm btn-warning" tabindex="8" id="clear_edit_lanjutan" title="Batalkan"><i class="fa fa-refresh"></i></button>
										</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						
					</div>
					<?php echo form_close() ?>
				</div>
				
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-primary" id="btn_simpan_approval" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Proses Persetujuan</button>
			</div>
			
		</div>
	</div>
</div>
<?$this->load->view('Mrka_pengajuan/modal_upload')?>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		// alert('sini');
		load_index();
		
	})	
	$("#btn_filter").click(function() {
		// alert('ID');
		table.destroy();
		load_index();
	});
	function verifikasi_berjenjang(nip,tanggal,idakun){
		swal({
			title: "Apakah Anda Yakin  ?",
			text : "Kirim Notif Pengajuan ini?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka_bendahara/verifikasi_berjenjang/',
				type: 'POST',
				data: {nip:nip,tanggal:tanggal,idakun:idakun},
				dataType: 'json',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Notif berhasil dikirim'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
	}
	function approval_lanjutan(id){
		$("#modal_approval_lanjutan").modal('show');
		$("#idrka_lanjutan").val(id);
		clear_input_detail();
		
	}
	function load_user_add(){
		let step=$("#step").val();
		let idrka=$("#idrka_lanjutan").val();
		var id_edit=$("#id_edit").val();
		$.ajax({
			url: '{site_url}mrka_bendahara/load_user_add',
			type: 'POST',
			dataType: 'json',
			data: {
				step: step,idrka:idrka,id_edit:id_edit
			},
			success: function(data) {
				$("#iduser_lanjutan").empty();
				$("#iduser_lanjutan").append(data.detail);
				// alert(data.detail)
			}
		});
	}
	$(document).on("click","#simpan_detail_lanjutan",function(){
		if (validate_add_detail()==false)return false;
		var id_edit=$("#id_edit").val();
		var idrka=$("#idrka_lanjutan").val();
		var iduser=$("#iduser_lanjutan").val();
		var user_nama=$("#iduser_lanjutan option:selected").text();
		// alert(user_nama);
		var step=$("#step").val();
		
		$.ajax({
			url: '{site_url}mrka_bendahara/simpan_detail_lanjutan',
			type: 'POST',
			data: {
				id_edit: id_edit,iduser:iduser,step:step,
				idrka: idrka,user_nama: user_nama
			},
			success: function(data) {
				if (data=='true'){
					$.toaster({priority : 'success', title : 'Succes!', message : ' Logic Berhasil disimpan'});
					// $('#tabel_detail').DataTable().ajax.reload( null, false );
					clear_input_detail();
					// $("#id_edit").val('');
				}else{
					sweetAlert("Gagal...", "Penyimpanan!", "error");
					
				}
			}
		});
		
	});
	$(document).on("change","#step",function(){
		load_user_add();
	});
	function clear_input_detail(){
		$("#id_edit").val('');
		$("#step").val('1').trigger('change');
		$("#iduser_lanjutan").val('#').trigger('change');
		load_user_approval_lanjutan();
	}
	function validate_add_detail()
	{
		if ($("#iduser_lanjutan").val()=='' || $("#iduser_lanjutan").val()==null || $("#iduser_lanjutan").val()=="#"){
			sweetAlert("Maaf...", "User Harus diisi", "error");
			return false;
		}
		
		return true;
	}
	$(document).on("click",".notif",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		var id_approval=$(this).data('id');
		swal({
			title: "Apakah Anda Yakin  ?",
			text : "Kirim Notif Pengajuan ini?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka_bendahara/setuju_notif/'+id+'/1',
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Notif berhasil dikirim'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
		
	});
	$(document).on("click",".pencairan",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		var id_approval=$(this).data('id');
		swal({
			title: "Apakah Anda Yakin  ?",
			text : "Proses Pencairan & akan berubah status menjadi belanja?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka_bendahara/setuju_pencairan/'+id+'/1',
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Pencairan berhasil diganti'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
		
	});
	$(document).on("click",".batal",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		var id_approval=$(this).data('id');
		swal({
			title: "Apakah Anda Yakin  ?",
			text : "Mengembalikan Status Konfirmasi Pengajuan ini?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka_approval/setuju_batal/'+id_approval+'/0',
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' Pengembalian berhasil disetujui'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
		
	});
	$(document).on("click","#btn_tolak",function(){
		
		var id_approval=$("#id_approval").val();
		var alasan_tolak=$("#alasan_tolak").val();
		swal({
			title: "Apakah Anda Yakin  ?",
			text : "Menolak Pengajuan ini?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka_approval/tolak/',
				type: 'POST',
				data: {
					id_approval:id_approval,
					alasan_tolak:alasan_tolak,
				},
				complete: function() {
					$('#modal_tolak').modal('hide');
					$.toaster({priority : 'success', title : 'Succes!', message : ' Pengajuan berhasil ditolak'});
					$('#table_index').DataTable().ajax.reload( null, false );
					
				}
			});
		});
		
		return false;
		
	});
	$(document).on("click",".tolak",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		var id_approval=$(this).data('id');
		$("#id_approval").val(id_approval)
		$('#modal_tolak').modal('show');
		// modal_tolak
		
	});
	$(document).on("click",".user",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		$("#idrka").val(id);
		load_user();
	});
	
	
	$(document).on("click","#btn_simpan_approval",function(){
		
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Melanjutkan Step Persetujuan Lanjutan?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka_bendahara/simpan_proses_peretujuan/'+$("#idrka_lanjutan").val(),
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' RKA Berhasil diselesaikan'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	$(document).on("click",".selesai",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,6).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menyelesaikan RKA?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka_pengajuan/update_status/'+id+'/4',
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' RKA Berhasil diselesaikan'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	function load_index(){
		var status=$("#status").val();
		var no_pengajuan=$("#no_pengajuan").val();
		var nama_kegiatan=$("#nama_kegiatan").val();
		var tipe_rka=$("#tipe_rka").val();
		var idunit_pengaju=$("#idunit_pengaju").val();
		var idjenis=$("#idjenis").val();
		var tanggal_pengajuan1=$("#tanggal_pengajuan1").val();
		var tanggal_pengajuan2=$("#tanggal_pengajuan2").val();
		var tanggal_dibutuhkan1=$("#tanggal_dibutuhkan1").val();
		var tanggal_dibutuhkan2=$("#tanggal_dibutuhkan2").val();
		// alert(tanggal_dibutuhkan1);
		table=$('#table_index').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mrka_bendahara/getIndex',
					type: "POST",
					dataType: 'json',
					data : {
						no_pengajuan:no_pengajuan,
						nama_kegiatan:nama_kegiatan,
						tipe_rka:tipe_rka,
						idunit_pengaju:idunit_pengaju,
						idjenis:idjenis,
						tanggal_pengajuan1:tanggal_pengajuan1,
						tanggal_pengajuan2:tanggal_pengajuan2,
						tanggal_dibutuhkan1:tanggal_dibutuhkan1,
						tanggal_dibutuhkan2:tanggal_dibutuhkan2,
						status:status,
						
					   }
				},
				"columnDefs": [
					{"targets": [0,1,2,11], "visible": false },
					 {  className: "text-right", targets:[3,10] },
					 {  className: "text-center", targets:[4,5,6,8,9,11,12] },
					 { "width": "5%", "targets": [3] },
					 { "width": "8%", "targets": [4,5,6,9] },
					 { "width": "10%", "targets": [7,8,11] },
					 { "width": "12%", "targets": [12,13] },
					 { "width": "15%", "targets": [14] },

				]
			});
	}
	function load_user_approval(){
		var idrka=$("#idrka").val();
		$('#tabel_user').DataTable().destroy();
		table=$('#tabel_user').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mrka_pengajuan/load_user_approval',
					type: "POST",
					dataType: 'json',
					data : {
						idrka:idrka,
												
					   }
				},
				"columnDefs": [
					 {  className: "text-right", targets:[0] },
					 {  className: "text-center", targets:[1,2,3] },
					 { "width": "10%", "targets": [0] },
					 { "width": "40%", "targets": [1] },
					 { "width": "25%", "targets": [2,3] },

				]
			});
	}
	function edit_approval(id,iduser,step){
		$("#id_edit").val(id);
		$("#step").val(step).trigger('change');
		setTimeout(function() {
			$("#iduser_lanjutan").val(iduser).trigger('change');			
		}, 1000);
		
	}
	function hapus_approval(id){
		swal({
			title: "Apakah Anda Yakin  ?",
			text : "Hapus User Approval?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka_bendahara/hapus_approval/',
				type: 'POST',
				data: {id:id},
				dataType: 'json',
				complete: function() {
					clear_input_detail();
				}
			});
		});
		
	}
	function load_user_approval_lanjutan(){
		var idrka=$("#idrka_lanjutan").val();
		// $('#btn_simpan_approval').attr('disabled', true);
		$("#btn_simpan_approval").attr('disabled', true);
		// alert('sini');
		$('#tabel_user_lanjutan').DataTable().destroy();
		table=$('#tabel_user_lanjutan').DataTable({
			"autoWidth": false,
			"pageLength": 10,
			"ordering": false,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{site_url}mrka_bendahara/load_user_approval_lanjutan',
				type: "POST",
				dataType: 'json',
				data : {
					idrka:idrka,
					// id_edit:id_edit,
											
				   }
			},
			"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				if (aData[1]){
					$("#btn_simpan_approval").attr('disabled', false);
				}
				
			},
				
			});
	}
	function load_user(){
		var idrka=$("#idrka").val();
		$("#modal_user").modal('show');
		
		// alert(idrka);
		$.ajax({
			url: '{site_url}mrka_pengajuan/list_user/'+idrka,
			dataType: "json",
			success: function(data) {
				$("#tabel_user_proses tbody").empty();
				$("#tabel_user_proses tbody").append(data.detail);
			}
		});
	}
	function lihat_approval_lanjutan(id){
		var idrka=id;
		$("#modal_user_lanjutan").modal('show');
		
		// alert(idrka);
		$.ajax({
			url: '{site_url}mrka_bendahara/lihat_approval_lanjutan/'+idrka,
			dataType: "json",
			success: function(data) {
				$("#tabel_user_proses_lanjutan tbody").empty();
				$("#tabel_user_proses_lanjutan tbody").append(data.detail);
			}
		});
	}
</script>
