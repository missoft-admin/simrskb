<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<style>
	.input-error{
		color:#a94442;
		border-color: #a94442;.
	}
</style>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <button onclick="goBack()" class="btn"><i class="fa fa-reply"></i></button>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mrka_bendahara/save_realisasi','class="form-horizontal push-10-t" id="myForm"') ?>
			<input type="hidden" class="form-control" id="id" placeholder="Nama RKA" name="id" value="{id}" required="" aria-required="true">
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Nama Pemohon</label>
				<div class="col-md-4">
					<input type="hidden" class="form-control" id="id" placeholder="Nama RKA" name="id" value="{id}" required="" aria-required="true">
					<input type="text" readonly class="form-control" id="nama_pemohon" disabled placeholder="Nama Pemohon" name="nama_pemohon" value="{nama_pemohon}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="disabel" placeholder="Nama RKA" name="disabel" value="{disabel}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="idrka_kegiatan" placeholder="Nama RKA" name="idrka_kegiatan" value="{idrka_kegiatan}" required="" aria-required="true">
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="bulan" id="bulan" value="<?=$bulan?>" required="" aria-required="true">
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="idlogic" id="idlogic" value="<?=$idlogic?>">
					
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="st_berjenjang" id="st_berjenjang" value="{st_berjenjang}">
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="proses_pencairan" id="proses_pencairan" value="{proses_pencairan}">
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="disabel_proses_pencarian" id="disabel_proses_pencarian" value="{disabel_proses_pencarian}">
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="st_realisasi" id="st_realisasi" value="{st_realisasi}">
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="st_simpan" id="st_simpan" value="1">
					
				</div>
				<label class="col-md-2 control-label" for="nama">Unit Yang Mengajukan</label>
				<div class="col-md-4">
					<select id="idunit_pengaju" disabled <?=($tipe_rka=='1'?'disabled':'')?> name="idunit_pengaju" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Unit -</option>
						<? foreach($list_unit as $row){ ?>
							<option value="<?=$row->idunit?>" <?=($idunit_pengaju==$row->idunit?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Tanggal Pengajuan</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input" disabled type="text" id="tanggal_pengajuan" name="tanggal_pengajuan" value="<?=$tanggal_pengajuan?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="nama"></label>
				<label class="col-md-2 control-label" for="nama">Tanggal Dibutuhkan</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input" disabled type="text" id="tanggal_dibutuhkan" name="tanggal_dibutuhkan" value="<?=$tanggal_dibutuhkan?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Untuk  Unit</label>
				<div class="col-md-4">
					<select id="idunit" name="idunit" disabled disabled class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Unit -</option>
						<? foreach($list_unit as $row){ ?>
							<option value="<?=$row->idunit?>" <?=($idunit==$row->idunit?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
				<label class="col-md-2 control-label" for="nama">Jenis Pengajuan</label>
				<div class="col-md-4">
					<select id="idjenis" name="idjenis" disabled disabled class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Jenis -</option>
						<? foreach($list_jenis as $row){ ?>
							<option value="<?=$row->id?>" <?=($idjenis==$row->id?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Tipe Pengajuan</label>
				<div class="col-md-4">
					<select id="tipe_rka" disabled name="tipe_rka" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="1" <?=$tipe_rka=='1'?'selected':''?>>RKA</option>
						<option value="2" <?=$tipe_rka=='2'?'selected':''?>>NON RKA</option>						
					</select>
				</div>
				<label class="col-md-2 control-label" for="nama">Nama Kegiatan</label>
				<div class="col-md-4">
					<? if ($tipe_rka=='1'){ ?>
					<div class="input-group date">
						<input type="text" class="form-control" disabled readonly id="nama_kegiatan" disabled placeholder="Nama Kegiatan" name="nama_kegiatan" value="{nama_kegiatan}">
						<span class="input-group-btn">
							<button class="btn btn-primary" disabled type="button" id="btn_open_modal_kegiatan" title="Terapkan sama rata"><i class="fa fa-search"></i></button>
						</span>
					</div>
					<?}else{?>
					<input type="text" class="form-control" disabled disabled placeholder="Nama Kegiatan" name="nama_kegiatan" value="{nama_kegiatan}">
					<?}?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Vendor</label>
				<div class="col-md-4">
						<select id="idvendor" name="idvendor" disabled class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" <?=($idvendor=='#'?'selected':'')?>>- Pilih Vendor -</option>
							<option value="0" <?=($idvendor=='0'?'selected':'')?>>Tentukan Vendor Nanti</option>
							<? foreach($list_vendor as $row){ ?>
								<option value="<?=$row->id?>" <?=($idvendor==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
						
				</div>
				<label class="col-md-2 control-label" for="nama">Klasifikasi</label>
				<div class="col-md-4">
					<select id="idklasifikasi" name="idklasifikasi" disabled class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" <?=($idklasifikasi=='#'?'selected':'')?>>- Pilih Klasifikasi -</option>
						<? foreach($list_klasifikasi as $row){ ?>
							<option value="<?=$row->id?>" <?=($idklasifikasi==$row->id?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
					
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Catatan</label>
				<div class="col-md-10">
						<textarea class="form-control" rows="2" name="catatan" id="catatan" disabled><?=$catatan?></textarea>
						
				</div>
				
			</div>
			<div class="progress progress-mini">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			
			
			<div class="row div_tabel_realisasi">
				<div class="col-md-12">
					<!-- Static Labels block-opt-hidden -->
					<div class="block block-themed" >
						<div class="block-header bg-primary">
							<ul class="block-options">
								<li>
									<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="fa fa-money"></i>  REALISASI PENGAJUAN</h3>
						</div>
						<div class="block-content block-content">
							<div class="table-responsive">	
								<table id="tabel_realisasi" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 3%;" class="text-center">No</th>
											<th style="width: 15%;" class="text-center">Nama Barang</th>
											<th style="width: 15%;" class="text-center">Merk Barang</th>
											<th style="width: 5%;" class="text-center">QTY</th>
											<th style="width: 10%;" class="text-center">Harga Pengajuan</th>
											<th style="width: 8%;" class="text-center">Total Pengajuan</th>
											<th style="width: 9%;" class="text-center">Realisasi Harga Pokok</th>
											<th style="width: 8%;" class="text-center">Realisasi Diskon</th>
											<th style="width: 9%;" class="text-center">Realisasi PPN</th>
											<th style="width: 9%;" class="text-center">Realisasi Harga Final</th>
											<th style="width: 5%;" class="text-center">Actions</th>
										</tr>
										
									</thead>
									<tbody>
										<? 
										$gt=0;
										$item=0;
										foreach($list_detail as $row){ 
											$gt=$gt + $row->total_harga;
											$item=$item + 1;
										?>
											<tr>
												<td>	
													<?=$item?>
													<input class="form-control input-sm harga_pokok_asal" readonly type="hidden"  name="harga_pokok_asal[]" value="<?=$row->harga_pokok_asal?>"/>
													<input class="form-control input-sm harga_diskon_asal" readonly type="hidden"  name="harga_diskon_asal[]" value="<?=$row->harga_diskon_asal?>"/>
													<input class="form-control input-sm harga_ppn_asal" readonly type="hidden"  name="harga_diskon_asal[]" value="<?=$row->harga_ppn_asal?>"/>
													<input class="form-control input-sm total_harga_asal" readonly type="hidden"  name="total_harga_asal[]" value="<?=$row->total_harga_asal?>"/>
													
												</td>
												<td><?=$row->nama_barang?></td>
												<td><?=$row->merk_barang?></td>
												<td><?=$row->kuantitas?></td>
												<td  class="text-left"><?=number_format($row->harga_satuan_asal).'<br>└─ Hpp. '.number_format($row->harga_pokok_asal).'<br>└─ Disc. '.number_format($row->harga_diskon_asal).'<br>└─ PPN. '.number_format($row->harga_ppn_asal)?></td>
												<td  class="text-right"><?=number_format($row->total_harga_asal)?></td>
												<td style='display:none'> <!-- 6 -->
													<input style="text-align:right;"  class="form-control input-sm iddet" readonly type="text"  name="iddet[]" value="<?=$row->id?>"/>
												</td>
												<td style='display:none'><!-- 7 -->
													<input style="text-align:right;" class="form-control input-sm kuantitas" readonly type="text"  name="kuantitas[]" value="<?=$row->kuantitas?>"/>
												</td>
												<td style='display:none'> <!-- 8 -->
													<input style="text-align:right;" class="form-control input-sm number harga_satuan" readonly type="text"  name="harga_satuan[]" value="<?=$row->harga_satuan?>"/>
												</td>
												
												<td><!-- 9 -->
													<input style="text-align:right;background-color: #fff;" class="form-control input-sm number harga_pokok" readonly type="text"  name="harga_pokok[]" value="<?=$row->harga_pokok?>"/>
												</td>	
												
												<td><!-- 10 -->
													<input style="text-align:right;;background-color: #fff" class="form-control input-sm number harga_diskon" readonly type="text"  name="harga_diskon[]" value="<?=$row->harga_diskon?>"/>
												</td>
												<td><!-- 11 -->
													<input style="text-align:right;;background-color: #fff" class="form-control input-sm number harga_ppn" readonly type="text"  name="harga_ppn[]" value="<?=$row->harga_ppn?>"/>
												</td>
												<td><!-- 12 -->
													<input style="text-align:right;background-color: #fff;" class="form-control input-sm number total_harga " readonly type="text"  name="total_harga[]" value="<?=$row->total_harga?>"/>
												</td>
												<td class="text-center"	><? echo "<div class='btn-group'><button type='button' ".$disabel." class='btn btn-xs btn-info edit kunci'><i class='glyphicon glyphicon-pencil'></i></button></div>"?></td>
												
											</tr>
										<?}?>
									</tbody>
									
								</table>
							
							</div>
						</div>
						<div class="block-content block-content">
							
							<div class="row">
								<div class="col-md-6">
								<table id="tabel_rekap" class="table table-striped table-bordered" style="margin-bottom: 0;">
									<thead>
										<tr>
											<th style="width: 60%;" class="text-center">Keterangan</th>
											<th style="width: 40%;" class="text-center">Nominal</th>							
										</tr>
										
									</thead>
									<tbody>
										<tr>
											<td><strong>TOTAL ANGGARAN</strong></td>
											<td>
												<input class="form-control input-sm number " readonly type="text"  id="total_anggaran" name="total_anggaran" value="{nominal_bayar}" style="text-align:right;"/>
											</td>
										</tr>
										<tr>
											<td><strong>REALISASI ANGGARAN</strong></td>
											<td>
												<input class="form-control input-sm number " readonly type="text"  id="total_realisasi" name="total_realisasi" value="0" style="text-align:right;"/>
											</td>
										</tr>
										<tr>
											<td><strong>SISA ANGGARAN</strong></td>
											<td>
												<input class="form-control input-sm number total_sisa" readonly type="text"  id="total_sisa" name="total_sisa" value="0" style="text-align:right;"/>
											</td>
										</tr>
										<tr>
											<td><strong>NOMINAL PENGEMBALIAN</strong></td>
											<td>
												<input class="form-control input-sm number total_pengembalian" readonly type="text"  id="total_pengembalian" name="total_pengembalian" value="0" style="text-align:right;"/>
											</td>
										</tr>
										<tr>
											<td><strong>NOMINAL POSTING JURNAL</strong></td>
											<td>
												<input class="form-control input-sm number total_validasi_jurnal" readonly type="text"  id="total_validasi_jurnal" name="total_validasi_jurnal" value="0" style="text-align:right;"/>
											</td>
										</tr>
									</tbody>
									
								</table>
								</div>
								<div class="col-md-6">
									<div class="col-md-12">
										<div class="alert alert-warning alert-dismissable">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
											<p id="label_info"><strong>Pembayaran Berjenjang</strong></p>
										</div>
									</div>
									<div class="col-md-12" id="div_valid_jurnal">
										<div class="alert alert-danger alert-dismissable">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
											<p id="label_info"><strong>Ada Pembayaran Yang belum divalidasi ke jurnal atau nominal tidak sesuai</strong></p>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					<!-- END Static Labels -->
					</div>
				</div>
			</div>
			<div class="row div_tabel_pengembalian">
				<div class="col-md-12">
					<!-- Static Labels block-opt-hidden -->
					<div class="block block-themed" >
						<div class="block-header bg-primary">
							<ul class="block-options">
								<li>
									<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="fa fa-money"></i>  PENGEMBALIAN ANGGARAN</h3>
						</div>
						<div class="block-content block-content">
							<div class="row">
								
								<div class="form-group">
									<label class="col-md-2 control-label" for="nama">Total Sisa</label>
									<div class="col-md-3">
										<input class="form-control input number total_sisa" readonly type="text" id="total_trx" name="total_trx" value=""/>
										
									</div>									
									<div class="col-md-4">
										<button class="btn btn-success" <?=$disabel?> type="button" id="btn_add_pembayaran"><i class="fa fa-plus"></i> Pengembalian</button>
									</div>
									
									
								</div>
								<div class="form-group">
							
									 
								</div>
							</div>
							<br>
								<div class="table-responsive">
								<table class="table table-bordered" id="list_pengembalian">
									<thead>
										<tr>
											<th width="5%" class="text-center">NO</th>
											<th width="10%" class="text-center">Jenis Kas</th>
											<th width="15%" class="text-center">Sumber Kas</th>
											<th width="10%" class="text-center">Metode</th>
											<th width="10%" class="text-center">Nominal</th>
											<th width="10%" class="text-center">Tgl Pencairan</th>
											<th width="15%" class="text-center">Keterangan</th>
											<th width="15%" class="text-center">Actions</th>
											
										</tr>
									</thead>
									<tbody>											
									</tbody>
									<tfoot>
										<tr>
											<td colspan="5" class="text-right"> <strong>Total Pengembalian </strong></td>
											<td colspan="2"> <input class="form-control input number total_pengembalian" readonly type="text" id="total_pengembalian" name="total_pengembalian" value="0"/></td>
										</tr>
										
									</tfoot>
								</table>	
							</div> 
						</div> 
					</div>
					<!-- END Static Labels -->
				</div>
			</div>
			<? if ($disabel==''){ ?>
			<div class="form-group">
				<div class="text-left bg-light lter">
					<button class="btn btn-primary" onclick="validate_final_simpan()"  type="submit" id="btn_simpan" name="btn_simpan" value="1"><i class="fa fa-save"></i>  Simpan</button>
					<button class="btn btn-danger" onclick="validate_final_validasi()"  type="submit" id="btn_simpan_validasi" name="btn_simpan" value="2"><i class="fa fa-check"></i> Simpan & Validasi</button>
					<button onclick="goBack()" class="btn btn-default" type="button"><i class="fa fa-history"></i> Kembali </button>
				</div>
			</div>
			<?}?>
			<?php echo form_close() ?>
	</div>
	<div class="modal fade in black-overlay" id="modal_pembayaran" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li><button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button></li>
					</ul>
					<h3 class="block-title">Pengembalian</h3>
				</div>
				<div class="block-content">
				<?php echo form_open('#','class="form-horizontal push-10-t" ') ?>
				<input  type="hidden" class="form-control" id="sumber_kas_id_tmp" placeholder="sumber_kas_id_tmp" name="sumber_kas_id_tmp" >
				<div class="form-group">
					<label class="col-md-3 control-label" for="nama">Sisa Rp. </label>
					<div class="col-md-8">
						<input readonly type="text" class="form-control number" id="sisa_modal" placeholder="Sisa" name="sisa_modal" required="" aria-required="true">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="jenis_kas_id">Jenis Kas</label>
					<div class="col-md-8">
						<select name="jenis_kas_id" id="jenis_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#">- Pilih Opsi -</option>
							<? foreach($list_jenis_kas as $row){ ?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>
							<?}?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="sumber_kas_id">Sumber Kas</label>
					<div class="col-md-8">
						<select name="sumber_kas_id" id="sumber_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">							
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="idmetode">Metode</label>
					<div class="col-md-8">
						<select name="idmetode" id="idmetode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#">Pilih Opsi</option>							
							<option value="1">Cheq</option>	
							<option value="2">Tunai</option>	
							<option value="4">Transfer Antar Bank</option>
							<option value="3">Transfer Beda Bank</option>							
						</select>
					</div>
				</div>
				
				
				<div class="form-group">
					<label class="col-md-3 control-label" for="nama">Nominal Rp. </label>
					<div class="col-md-8">
						<input  type="hidden" class="form-control" id="idpembayaran" placeholder="idpembayaran" name="idpembayaran" >
						<input  type="text" class="form-control number" id="nominal_bayar" placeholder="Nominal" name="nominal_bayar" required="" aria-required="true">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="nama">Tanggal Pengembalian</label>
					<div class="col-md-8">
						<div class="input-group date">
						<input class="js-datepicker form-control input" autocomplete="off" type="text" id="tanggal_pencairan" name="tanggal_pencairan" value="<?=$tanggal_dibutuhkan?>" data-date-format="dd-mm-yyyy"/>
						<span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-3 control-label" for="nama">Keterangan </label>
					<div class="col-md-8">
						<textarea class="form-control js-summernote" name="ket_pembayaran" id="ket_pembayaran"></textarea>
					</div>
				</div>

				<div class="modal-footer">
					<button class="btn btn-sm btn-primary" id="btn_add_bayar" type="button"><i class="fa fa-check"></i> Add</button>
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Exit</button>
				</div>
				</form>
			</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<?$this->load->view('Mrka_bendahara/modal_harga')?>
<script type="text/javascript">
	var table;
	let total_realisasi;
	let ada_perubahan=0;
	$(document).ready(function(){
		
		$('#ket_pembayaran').summernote({
		  height: 100,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})

		$id=$("#id").val();
		$(".decimal").number(true,0,'.',',');
		$('.number').number(true, 0, '.', ',');
		hitung_total();
		refresh_pengembalian();
	})	
	function validate_final_simpan() {
		$("#st_simpan").val(1);
		if (ada_perubahan){			
			event.preventDefault();
			swal({
				title: "Apakah Anda Yakin ?",
				text : "Akan Simpan dengan perubahan baru?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				
				$("#myForm").submit();
			});
		}else{
			$("#myForm").submit();
		}

		return false;
	}
	function validate_final_validasi() {
			$("#st_simpan").val(2);		
			event.preventDefault();
			swal({
				title: "Apakah Anda Yakin ?",
				text : "Akan validasi Ke Jurnal?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				
				$("#myForm").submit();
			});
		
		return false;
	}
	
	$(document).on("click","#btn_add_pembayaran",function(){
		let total_sisa=parseFloat($("#total_sisa").val()) - parseFloat($("#total_pengembalian").val());
		if (total_sisa <=0){
			sweetAlert("Maaf...", "Sisa Pembayaran Tidak sesuai!", "error");
			return false;
		}
		$("#sumber_kas_id_tmp").val('');
		// $("#sumber_kas_id").val($("#sumber_kas_id").val(''));
		$("#modal_pembayaran").modal('show');
		$("#sisa_modal").val(total_sisa);
		$("#nominal_bayar").val(total_sisa);
		
	});
	$(document).on("change","#jenis_kas_id",function(){
		var jenis_kas_id=$("#jenis_kas_id").val();
		var sumber_kas_id=$("#sumber_kas_id_tmp").val();
		$.ajax({
			url: '{site_url}mrka_bendahara/list_sumber_kas',
			type: 'POST',
			dataType: "json",
			data: {jenis_kas_id: jenis_kas_id,sumber_kas_id: sumber_kas_id},
			success : function(data) {				
				$("#sumber_kas_id").empty();	
				console.log(data.detail);
				// $("#sumber_kas_id").append(data.detail);	
				$('#sumber_kas_id').append(data.detail);
			}
		});
	});
	$(document).on("click","#btn_add_bayar",function(){
		add_pembayaran();
	});
	function clear_input_pembayaran(){
		$("#jenis_kas_id").val("#").trigger('change');
		$("#sumber_kas_id").val("#").trigger('change');
		$("#nominal_bayar").val(0);
		$("#sisa_modal").val(0);
		$("#idpembayaran").val('');
		$("#idmetode").val("#").trigger('change');
		$("#ket_pembayaran").summernote('code','');
	}
	function add_pembayaran(){
		var idrka=$("#id").val();
		var jenis_kas_id=$("#jenis_kas_id").val();
		var sumber_kas_id=$("#sumber_kas_id").val();
		var nominal_bayar=$("#nominal_bayar").val();
		var idmetode=$("#idmetode").val();
		var idpembayaran=$("#idpembayaran").val();
		var tanggal_pencairan=$("#tanggal_pencairan").val();
		var ket_pembayaran=$("#ket_pembayaran").summernote('code');
		
		if ($("#jenis_kas_id").val() == "#") {
			sweetAlert("Maaf...", "Jenis Kas Harus Diisi!", "error");
			$("#jenis_kas_id").focus();
			return false;
		}
		if ($("#sumber_kas_id").val() == "#") {
			sweetAlert("Maaf...", "Sumber Kas Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		if ($("#idmetode").val() == "#" || $("#idmetode").val() == "") {
			sweetAlert("Maaf...", "Metode  Harus Diisi!", "error");
			$("#idmetode").focus();
			return false;
		}
		if ($("#nominal_bayar").val() == "" || $("#nominal_bayar").val() == "0") {
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		
		
		$.ajax({
			url: '{site_url}mrka_bendahara/save_pengembalian/',
			dataType: "json",
			type: "POST",
			data: {
				idrka:idrka
				,jenis_kas_id:jenis_kas_id
				,sumber_kas_id:sumber_kas_id
				,nominal_bayar:nominal_bayar				
				,idmetode:idmetode				
				,ket_pembayaran:ket_pembayaran				
				,idpembayaran:idpembayaran				
				,tanggal_pencairan:tanggal_pencairan				
			},
			success: function(data) {
				$("#sumber_kas_id_tmp").val('');
				sweetAlert("Success...", "Pembayaran Berhasil disimpan!", "info");
				refresh_pengembalian();
				$("#modal_pembayaran").modal('hide');
			}
		});
		
	}
	function refresh_pengembalian(){
		var id=$("#id").val();
		// alert(id);
		var disabel=$("#disabel").val();
		// alert(id);
		$.ajax({
			url: '{site_url}mrka_bendahara/refresh_pengembalian/'+id+'/'+disabel,
			dataType: "json",
			success: function(data) {
				// if (data.detail!=''){
					$('.total_validasi_jurnal').val(data.total_validasi_jurnal);
					$('.total_pengembalian').val(data.nominal_bayar);
					$('#list_pengembalian tbody').empty();
					$("#list_pengembalian tbody").append(data.detail);
					hitung_total();
					
				
			}
		});
	}
	$(document).on("click",".edit_pengembalian",function(){
		$("#idpembayaran").val($(this).closest('tr').find("td:eq(8)").text());
		var id=$("#idpembayaran").val();
		var sisa=0;
		$("#modal_pembayaran").modal('show');
		// get_pembayaran
		$.ajax({
			url: '{site_url}mrka_bendahara/get_pengembalian/'+id,
			dataType: "json",
			success: function(data) {
				// alert($("#total_sisa").val());
				var row=data.detail;
				if (data.detail!=''){
					sisa=parseFloat(row.nominal_bayar) + parseFloat($("#total_sisa").val());
					// alert(sisa);
					$("#sumber_kas_id_tmp").val(row.sumber_kas_id);
					// $("#sumber_kas_id").val(row.sumber_kas_id);
					$("#jenis_kas_id").val(row.jenis_kas_id).trigger('change');
					$("#idmetode").val(row.idmetode).trigger('change');
					$("#nominal_bayar").val(row.nominal_bayar);
					$("#tanggal_pencairan").val(row.tanggal_pencairan);
					
					$("#sisa_modal").val(sisa);
					$("#ket_pembayaran").summernote('code',row.ket_pembayaran);
					
				}
				// console.log(data);
				
			}
		});
	});
	$(document).on("click",".hapus_pengembalian",function(){	
		var currentRow=$(this).closest("tr");          
         var id=currentRow.find("td:eq(8)").text();
		 $.ajax({
			url: '{site_url}mrka_bendahara/hapus_pengembalian/',
			type: 'POST',
			data: {id: id},
			success : function(data) {				
				$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
				refresh_pengembalian();
				// table.ajax.reload( null, false );				
			}
		});
	});
	$(document).on("click", ".edit", function() {
			$("#modal_harga").modal('show');
			$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);			
			$("#iddet").val($(this).closest('tr').find(".iddet").val());			
	
			
			$("#tharga_pokok").val($(this).closest('tr').find(".harga_pokok").val());
			$("#tharga_diskon").val($(this).closest('tr').find(".harga_diskon").val());
			$("#tharga_ppn").val($(this).closest('tr').find(".harga_ppn").val());
			$("#tkuantitas").val($(this).closest('tr').find(".kuantitas").val());
			$("#tharga_satuan").val($(this).closest('tr').find(".harga_satuan").val());
			$("#ttotal_harga").val($(this).closest('tr').find(".total_harga").val());
	});
	$(document).on("keyup",".harga",function(){
		if ($(this).val()==''){
			$(this).val(0);
		}
		generate_harga_final();
	});
	function generate_harga_final(){
		var harga_final=0;
		var hpp=parseFloat($("#tharga_pokok").val());
		var hd=parseFloat($("#tharga_diskon").val());
		var hp=parseFloat($("#tharga_ppn").val());
		var kuantitas=parseFloat($("#tkuantitas").val());
		harga_final=hpp-hd+hp;
		$("#tharga_satuan").val(harga_final);
		$("#ttotal_harga").val(harga_final*kuantitas);
		
	}
	function hitung_total(){
		// total_realisasi
		let total_anggaran=$("#total_anggaran").val();
		let total_validasi_jurnal=$("#total_validasi_jurnal").val();
		let total_sisa=0;
		let total_realisasi=0;
		let total_harga;
		let input_pokok;
		let input_diskon;
		let input_ppn;
		let input_total;
		let total_pengembalian=$(".total_pengembalian").val();
		ada_perubahan=0;
		$('#tabel_realisasi tbody tr').each(function() {
			input_pokok=$(this).closest('tr').find(".harga_pokok");
			input_diskon=$(this).closest('tr').find(".harga_diskon");
			input_ppn=$(this).closest('tr').find(".harga_ppn");
			input_total=$(this).closest('tr').find(".total_harga");
			if (parseFloat($(this).closest('tr').find(".harga_pokok_asal").val()) != parseFloat($(this).closest('tr').find(".harga_pokok").val())){
				input_pokok.addClass("input-error");
				ada_perubahan=1;
			}else{
				input_pokok.removeClass("input-error");
			}
			if (parseFloat($(this).closest('tr').find(".harga_diskon_asal").val()) != parseFloat($(this).closest('tr').find(".harga_diskon").val())){
				input_diskon.addClass("input-error");
				ada_perubahan=1;
			}else{
				input_diskon.removeClass("input-error");
			}
			if (parseFloat($(this).closest('tr').find(".harga_ppn_asal").val()) != parseFloat($(this).closest('tr').find(".harga_ppn").val())){
				input_ppn.addClass("input-error");
				ada_perubahan=1;
			}else{
				input_ppn.removeClass("input-error");
			}
			if (parseFloat($(this).closest('tr').find(".total_harga_asal").val()) != parseFloat($(this).closest('tr').find(".total_harga").val())){
				input_total.addClass("input-error");
				ada_perubahan=1;
			}else{
				input_total.removeClass("input-error");
			}
			
			total_harga=$(this).closest('tr').find(".total_harga").val();
			total_realisasi=parseFloat(total_realisasi)+parseFloat(total_harga);
			
		});
		$("#total_realisasi").val(total_realisasi);
		total_sisa=parseFloat(total_anggaran)-parseFloat(total_realisasi);
		$(".total_sisa").val(total_sisa);
		let st_valid=1;
		if (total_sisa !='0'){
			if (total_pengembalian!=total_sisa){
				if (total_pengembalian > total_sisa){
					$("#label_info").html('<strong>Ada transaksi pengembalian yang harus dihapus<strong>');
					st_valid=0;
				}else{
				$("#label_info").html('<strong>Ada sisa anggaran harus ditransaksikan pengembalian<strong>');
					st_valid=0;
				}
				$("#btn_simpan_validasi").hide();
			}else{
				$("#btn_simpan_validasi").show();
				$("#label_info").html('<strong>Transaksi Sudah Valid<strong>');
			}
			$(".div_tabel_pengembalian").show();
		}else{
			if (total_pengembalian > total_sisa){
				$("#label_info").html('<strong>Ada transaksi pengembalian yang harus dihapus<strong>');
				st_valid=0;
				$("#btn_simpan_validasi").hide();
			}else{
				$("#label_info").html('<strong>Transaksi Sudah Valid<strong>');
				$("#btn_simpan_validasi").show();
				$(".div_tabel_pengembalian").hide();
				
			}
		}
		if (total_anggaran != total_validasi_jurnal){
			$("#div_valid_jurnal").show();
			$("#btn_simpan_validasi").hide();
			
		}else{
			$("#div_valid_jurnal").hide();
			if (st_valid=='1'){
				$("#btn_simpan_validasi").show();
				
			}
		}
	}
	function set_harga(){
		total_realisasi=0;
		let total_harga;
		$('#tabel_realisasi tbody tr').each(function() {
			let row_index=$(this).closest('tr')[0].sectionRowIndex;
			let row_index_select=$("#rowindex").val();
			
			console.log($(this).closest('tr')[0].sectionRowIndex);
			if (row_index==row_index_select){
				$(this).closest('tr').find(".harga_pokok").val($("#tharga_pokok").val());
				$(this).closest('tr').find(".harga_diskon").val($("#tharga_diskon").val());
				$(this).closest('tr').find(".harga_ppn").val($("#tharga_ppn").val());
				$(this).closest('tr').find(".kuantitas").val($("#tkuantitas").val());
				$(this).closest('tr').find(".harga_satuan").val($("#tharga_satuan").val());
				$(this).closest('tr').find(".total_harga").val($("#ttotal_harga").val());
				$("#modal_harga").modal('hide');
			}
			total_harga=$(this).closest('tr').find(".total_harga").val();
			total_realisasi=total_realisasi+total_harga;
		});
		hitung_total();
	}
	function goBack() {
	  window.history.back();
	}
</script>