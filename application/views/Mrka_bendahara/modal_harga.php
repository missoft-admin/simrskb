<div class="modal fade in black-overlay" id="modal_harga" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-popout">
		<div class="modal-content" >
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Menentukan Harga baru</h3>
				</div>
				<div class="block-content">
					<?php echo form_open('lgudang_stok/filter','class="form-horizontal" id="form-work"') ?>
					<div class="row">
						<div class="col-md-12">
							
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Harga Pokok</label>
								<div class="col-md-8">
									<input class="form-control input-sm decimal harga" tabindex="4" type="text" id="tharga_pokok" />
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Nominal Diskon</label>
								<div class="col-md-8">
									<input class="form-control input-sm decimal harga" tabindex="4" type="text" id="tharga_diskon" />
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Nominal PPN</label>
								<div class="col-md-8">
									<input class="form-control input-sm decimal harga" tabindex="4" type="text" id="tharga_ppn" />
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Harga Satuan</label>
								<div class="col-md-8">
									<input class="form-control input-sm decimal" readonly tabindex="4" type="text" id="tharga_satuan" />
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Kuantitas</label>
								<div class="col-md-8">
									<input class="form-control input-sm decimal" readonly tabindex="4" type="text" id="tkuantitas" />
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-4 control-label" for="tipeid">Harga Final</label>
								<div class="col-md-8">
									<input class="form-control input-sm decimal" readonly tabindex="4" type="text" id="ttotal_harga" />
								</div>
							</div>
						</div>
						
					</div>
					<input type="hidden" readonly id="rowindex" name="rowindex" value="">
					<input type="hidden" readonly id="iddet" name="iddet" value="">
					<?php echo form_close() ?>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
					<button class="btn btn-sm btn-primary" type="button" onclick="set_harga()"><i class="fa fa-check"></i> Ok</button>
				</div>
			</div>
		</div>
	</div>
</div>
