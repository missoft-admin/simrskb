<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <button onclick="goBack()" class="btn"><i class="fa fa-reply"></i></button>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mrka_bendahara/save','class="form-horizontal push-10-t" onsubmit="return validate_final()" id="form1"') ?>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Nama Pemohon</label>
				<div class="col-md-4">
					<input type="hidden" class="form-control" id="id" placeholder="Nama RKA" name="id" value="{id}" required="" aria-required="true">
					<input type="text" readonly class="form-control" id="nama_pemohon" <?=$disabel?> placeholder="Nama Pemohon" name="nama_pemohon" value="{nama_pemohon}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="disabel" placeholder="Nama RKA" name="disabel" value="{disabel}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="idrka_kegiatan" placeholder="Nama RKA" name="idrka_kegiatan" value="{idrka_kegiatan}" required="" aria-required="true">
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="bulan" id="bulan" value="<?=$bulan?>" required="" aria-required="true">
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="idlogic" id="idlogic" value="<?=$idlogic?>">
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="btn_simpan_value" id="btn_simpan_value" value="0">
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="st_berjenjang" id="st_berjenjang" value="{st_berjenjang}">
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="proses_pencairan" id="proses_pencairan" value="{proses_pencairan}">
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="disabel_proses_pencarian" id="disabel_proses_pencarian" value="{disabel_proses_pencarian}">
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="st_realisasi" id="st_realisasi" value="{st_realisasi}">
					
				</div>
				<label class="col-md-2 control-label" for="nama">Unit Yang Mengajukan</label>
				<div class="col-md-4">
					<select id="idunit_pengaju" disabled <?=($tipe_rka=='1'?'disabled':'')?> name="idunit_pengaju" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Unit -</option>
						<? foreach($list_unit as $row){ ?>
							<option value="<?=$row->idunit?>" <?=($idunit_pengaju==$row->idunit?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Tanggal Pengajuan</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input" <?=$disabel?> type="text" id="tanggal_pengajuan" name="tanggal_pengajuan" value="<?=$tanggal_pengajuan?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="nama"></label>
				<label class="col-md-2 control-label" for="nama">Tanggal Dibutuhkan</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input" <?=$disabel?> type="text" id="tanggal_dibutuhkan" name="tanggal_dibutuhkan" value="<?=$tanggal_dibutuhkan?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Untuk  Unit</label>
				<div class="col-md-4">
					<select id="idunit" name="idunit" disabled <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Unit -</option>
						<? foreach($list_unit as $row){ ?>
							<option value="<?=$row->idunit?>" <?=($idunit==$row->idunit?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
				<label class="col-md-2 control-label" for="nama">Jenis Pengajuan</label>
				<div class="col-md-4">
					<select id="idjenis" name="idjenis" disabled <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Jenis -</option>
						<? foreach($list_jenis as $row){ ?>
							<option value="<?=$row->id?>" <?=($idjenis==$row->id?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Tipe Pengajuan</label>
				<div class="col-md-4">
					<select id="tipe_rka" disabled name="tipe_rka" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="1" <?=$tipe_rka=='1'?'selected':''?>>RKA</option>
						<option value="2" <?=$tipe_rka=='2'?'selected':''?>>NON RKA</option>						
					</select>
				</div>
				<label class="col-md-2 control-label" for="nama">Nama Kegiatan</label>
				<div class="col-md-4">
					<? if ($tipe_rka=='1'){ ?>
					<div class="input-group date">
						<input type="text" class="form-control" <?=$disabel?> readonly id="nama_kegiatan" <?=$disabel?> placeholder="Nama Kegiatan" name="nama_kegiatan" value="{nama_kegiatan}">
						<span class="input-group-btn">
							<button class="btn btn-primary" <?=$disabel?> type="button" id="btn_open_modal_kegiatan" title="Terapkan sama rata"><i class="fa fa-search"></i></button>
						</span>
					</div>
					<?}else{?>
					<input type="text" class="form-control" <?=$disabel?> <?=$disabel?> placeholder="Nama Kegiatan" name="nama_kegiatan" value="{nama_kegiatan}">
					<?}?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Vendor</label>
				<div class="col-md-4">
					<div class="input-group">
						<select id="idvendor" name="idvendor" <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" <?=($idvendor=='#'?'selected':'')?>>- Pilih Vendor -</option>
							<option value="0" <?=($idvendor=='0'?'selected':'')?>>Tentukan Vendor Nanti</option>
							<? foreach($list_vendor as $row){ ?>
								<option value="<?=$row->id?>" <?=($idvendor==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
						<span class="input-group-btn">
							<button class="btn btn-success" <?=$disabel?> type="button" id="btn_refresh_vendor" title="Refresh"><i class="fa fa-refresh"></i></button>
							<a href="{base_url}mvendor" target="_blank" class="btn btn-primary" <?=$disabel?> type="button" id="btn_add_vendor" title="Tambah Vendor"><i class="fa fa-plus"></i></a>
						</span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="nama">Klasifikasi</label>
				<div class="col-md-4">
					<select id="idklasifikasi" name="idklasifikasi" <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" <?=($idklasifikasi=='#'?'selected':'')?>>- Pilih Klasifikasi -</option>
						<? foreach($list_klasifikasi as $row){ ?>
							<option value="<?=$row->id?>" <?=($idklasifikasi==$row->id?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
					
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Catatan</label>
				<div class="col-md-10">
						<textarea class="form-control" rows="2" name="catatan" id="catatan"><?=$catatan?></textarea>
						
				</div>
				
			</div>
			<div class="progress progress-mini">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<h5 style="margin-bottom: 10px;">DETAIL PENGAJUAN</h5>
			
			<div class="form-group">
				<table id="tabel_add" class="table table-striped table-bordered" style="margin-bottom: 0;">
					<thead>
						<tr>
							<th style="width: 15%;">Nama Barang</th>
							<th style="width: 15%;">Merk Barang</th>
							<th style="width: 5%;">QTY</th>
							<th style="width: 5%;">Satuan</th>
							<th style="width: 10%;">Harga</th>
							<th style="width: 10%;">Total</th>
							<th style="width: 15%;">Keterangan</th>
							<th style="width: 8%;">Actions</th>
						</tr>
						<tr>							
							<td><input class="form-control input-sm" tabindex="2" type="text" id="nama_barang" /></td>
							<td><input class="form-control input-sm" tabindex="3" type="text" id="merk_barang" /></td>
							<td><input class="form-control input-sm number" tabindex="4" type="text" id="kuantitas" /></td>
							<td><input class="form-control input-sm" tabindex="5" type="text" id="satuan" /></td>
							<td>
								<div class="input-group date">
								<input class="form-control input-sm decimal" readonly tabindex="6" type="text" id="harga_satuan" />
								<input class="form-control input-sm decimal" readonly tabindex="6" type="hidden" id="harga_pokok" />
								<input class="form-control input-sm decimal" readonly tabindex="6" type="hidden" id="harga_diskon" />
								<input class="form-control input-sm decimal" readonly tabindex="6" type="hidden" id="harga_ppn" />
								<input class="form-control input-sm " readonly tabindex="6" type="hidden" id="idklasifikasi_det" />
									<span class="input-group-btn">
										<button class="btn btn-primary btn-sm" <?=$disabel?> type="button" id="btn_open_modal_harga" title="Terapkan sama rata"><i class="fa fa-pencil"></i></button>
									</span>
								</div>
							</td>
							<td><input class="form-control input-sm number" readonly  type="text" id="total_harga" /></td>
							<td><input class="form-control input-sm" tabindex="7" type="text" id="keterangan" /></td>
							<td>
								<button type="button" <?=$disabel?> <?=($st_validasi=='1'?'disabled':'')?> class="btn btn-sm btn-primary kunci" tabindex="8" id="addjual" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
								<button type='button' <?=$disabel?> <?=($st_validasi=='1'?'disabled':'')?> class='btn btn-sm btn-danger kunci' tabindex="9" id="canceljual" title="Refresh"><i class='fa fa-times'></i></button>
							</td>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
					<tfoot id="foot-total-nonracikan">
						<tr>
							<th colspan="5" class="hidden-phone">

								<span class="pull-right"><b>TOTAL</b></span></th>
							<input type="hidden" id="rowindex" name="rowindex" value="">
							<input type="hidden" id="iddet" name="iddet" value="">
							<th><input class="form-control input-sm number" readonly type="text" id="grand_total" name="grand_total" value="" /></th>
							<th><input class="form-control input-sm number"  type="hidden" id="total_item" name="total_item" value="" /></th>
							<th><input class="form-control input-sm number"  type="hidden" id="total_jenis_barang" name="total_jenis_barang" value=""/></th>
							
						</tr>
					</tfoot>
				</table>
				
			</div>
			<div class="progress progress-mini">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama"><h4><span class="label label-primary">Cara Pembayaran</span></h4></label>
				<div class="col-md-4">
					<select id="cara_pembayaran" name="cara_pembayaran" <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" <?=($cara_pembayaran=='#'?'selected':'')?>>- Pilih Cara Pembayaran -</option>
						<option value="0" <?=($cara_pembayaran=='0'?'selected':'')?>>Tentukan Cara Pembayaran Nanti</option>
						<option value="1" <?=($cara_pembayaran=='1'?'selected':'')?>>Satu Kali Bayar</option>
						<option value="2" <?=($cara_pembayaran=='2'?'selected':'')?>>Cicilan</option>
						<option value="3" <?=($cara_pembayaran=='3'?'selected':'')?>>By Termin</option>
						
					</select>						
				</div>				
			</div>
			<div class="form-group" style="display:block" id="div_satu">
				<label class="col-md-2 control-label" for="nama"><h4><span class="label label-primary">Jenis Pembayaran</span></h4></label>
				<div class="col-md-4">
					<select id="jenis_pembayaran" name="jenis_pembayaran" <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" <?=($jenis_pembayaran=='#'?'selected':'')?>>- Pilih Jenis Pembayaran -</option>
						<option value="0" <?=($jenis_pembayaran=='0'?'selected':'')?>>Tentukan Jenis Pembayaran Nanti</option>
						<option value="1" <?=($jenis_pembayaran=='1'?'selected':'')?>>Tunai</option>
						<option value="2" <?=($jenis_pembayaran=='2'?'selected':'')?>>Transfer</option>
						<option value="3" <?=($jenis_pembayaran=='3'?'selected':'')?>>Kartu Kredit</option>
						<option value="4" <?=($jenis_pembayaran=='4'?'selected':'')?>>Kontrabon</option>
						
					</select>						
				</div>				
			</div>
			<div id="div_tf">
				<div class="form-group" style="display:block">
					<label class="col-md-2 control-label" for="nama">No Rekening</label>
					<div class="col-md-4">
						<div class="input-group">
							<input class="form-control input-sm "  type="hidden" readonly id="rekening_id" name="rekening_id" value="{rekening_id}"/>	
							<input class="form-control input-sm " <?=$disabel?> type="text" id="norek" name="norek" value="{norek}"/>	
							<span class="input-group-btn">
								<button class="btn btn-primary btn-sm" <?=$disabel?> type="button" id="btn_add_rekening" title="Rekening Vendor"><i class="fa fa-credit-card"></i></button>
							</span>
						</div>			
					</div>
					<label class="col-md-2 control-label" for="nama">Bank</label>
					<div class="col-md-4">
						<input class="form-control input-sm " <?=$disabel?> type="text" id="bank" name="bank" value="{bank}"/>			
					</div>				
				</div>
				<div class="form-group" style="display:block">
					<label class="col-md-2 control-label" for="nama">Atas Nama</label>
					<div class="col-md-10">
						<input class="form-control input-sm " <?=$disabel?> type="text" id="atasnama" name="atasnama" value="{atasnama}"/>			
					</div>
								
				</div>
				<div class="form-group" style="display:block">
					<label class="col-md-2 control-label" for="nama">Keterangan</label>
					<div class="col-md-10">
						<textarea class="form-control" rows="2" <?=$disabel?>  name="keterangan_bank" id="keterangan_bank"><?=$keterangan_bank?></textarea>	
					</div>
								
				</div>
			</div>
			<div id="div_kbo">
				<div class="form-group" style="display:block">
					<label class="col-md-2 control-label" for="nama">Tanggal Kontrabon</label>
					<div class="col-md-4">
						<div class="input-group date">
							<input class="js-datepicker form-control input" <?=$disabel?> type="text" id="tanggal_kontrabon" name="tanggal_kontrabon" value="<?=($tanggal_kontrabon?HumanDateShort($tanggal_kontrabon):'')?>" data-date-format="dd-mm-yyyy"/>
							<span class="input-group-addon">
								<span class="fa fa-calendar"></span>
							</span>
						</div>
					</div>
								
				</div>
				
			</div>
			<?$this->load->view('Mrka_pengajuan/div_cicilan')?>
			<?$this->load->view('Mrka_pengajuan/div_termin')?>
			
			<div class="progress progress-mini">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<!-- Static Labels block-opt-hidden -->
					<div class="block block-themed block-opt-hidden" >
						<div class="block-header bg-primary">
							<ul class="block-options">
								<li>
									<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="si si-user-following"></i>  User Approval</h3>
						</div>
						<div class="block-content block-content">
							

								<input class="form-control"  value="0" type="hidden" readonly id="st_load_berkas">
								<table width="100%" id="tabel_user_proses" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Step</th>
											<th>User</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							
						</div>
					</div>
					<!-- END Static Labels -->
				</div>

			</div>
			<div class="row">
				<div class="col-md-12">
					<!-- Static Labels block-opt-hidden -->
					<div class="block block-themed block-opt-hidden" >
						<div class="block-header bg-primary">
							<ul class="block-options">
								<li>
									<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="si si-docs"></i>  MEMO</h3>
						</div>
						<div class="block-content block-content">
							<textarea class="form-control js-summernote" name="memo_bendahara" id="memo_bendahara"><?=$memo_bendahara?></textarea>
						</div>
					</div>
					<!-- END Static Labels -->
				</div>

			</div>
			
			<div class="row">
				<div class="col-md-12">
					<!-- Static Labels block-opt-hidden -->
					<div class="block block-themed block-opt-hidden" >
						<div class="block-header bg-primary">
							<ul class="block-options">
								<li>
									<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="si si-info"></i>  Update Informasi</h3>
						</div>
						<div class="block-content block-content">
							<div class="form-group">
								<div class="col-md-12">
									<textarea class="form-control js-summernote" name="informasi" id="informasi"></textarea>
									
								</div>
								
							</div>
							<div class="form-group">
									<label class="col-md-2 control-label" for="nama">Reminder</label>
									<div class="col-md-4">
										<div class="input-group date">
										<input class="js-datepicker form-control input"  type="text" id="tgl_reminder" name="tgl_reminder" value="" data-date-format="dd-mm-yyyy"/>
										<span class="input-group-addon">
											<span class="fa fa-calendar"></span>
										</span>
										</div>									
									</div>
									
									<div class="col-md-4">
										<button class="btn btn-primary" type="button" id="btn_tambah_informasi"><i class="fa fa-plus"></i> Tambah</button>
										<button class="btn btn-danger" type="button" id="btn_clear_data"><i class="fa fa-refresh"></i> Clear Data</button>						
									</div>
									<div class="col-md-2">
										<input class="form-control input"  type="hidden" id="informasi_id" name="informasi_id" value=""/>
									</div>
								
							</div>
							<hr>
							<div class="form-group">
								<table width="100%" id="tabel_informasi" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>NO</th>
											<th>TANGGAL</th>
											<th>INFORMASI</th>
											<th>USER</th>
											<th>REMINDER</th>
											<th>ACTION</th>
											<th hidden>ID</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>								
							</div>
						</div>
					</div>
					<!-- END Static Labels -->
				</div>
			</div>
			<?php echo form_close() ?>
			<div class="row">
				<div class="col-md-12">
					<!-- Static Labels block-opt-hidden -->
					<div class="block block-themed block-opt-hidden" >
						<div class="block-header bg-primary">
							<ul class="block-options">
								<li>
									<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="si si-docs"></i>  UPLOAD DOC</h3>
						</div>
						<div class="block-content block-content">
							<div class="col-md-12" style="margin-top: 10px;">
								<div class="block" id="box_file2">					
									 <table class="table table-bordered" id="list_file">
										<thead>
											<tr>
												<th width="50%" class="text-center">File</th>
												<th width="15%" class="text-center">Size</th>
												<th width="35%" class="text-center">User</th>
												<th width="35%" class="text-center">X</th>
												
											</tr>
										</thead>
										<tbody>
											
										</tbody>
									</table>
								</div>
								<div class="block">
									<form action="{base_url}mrka_bendahara/upload" enctype="multipart/form-data" class="dropzone" id="image-upload">
										<input type="hidden" class="form-control" name="idrka" id="idrka" value="{id}" readonly>
										<div>
										  <h5>Bukti Upload</h5>
										</div>
									</form>
								</div>
								
							</div> 
						</div>
					</div>
					<!-- END Static Labels -->
				</div>
			</div>
			
			<div class="row satu_kali div_tabel_bayar">
				<div class="col-md-12">
					<!-- Static Labels block-opt-hidden -->
					<div class="block block-themed" >
						<div class="block-header bg-primary">
							<ul class="block-options">
								<li>
									<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="fa fa-money"></i>  PEMBAYARAN</h3>
						</div>
						<div class="block-content block-content">
							<div class="row">
								<?if ($st_berjenjang=='1'){?>
								<div class="form-group">
									<div class="col-md-12">
										<div class="alert alert-success alert-dismissable">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
											<p><strong>Pembayaran Berjenjang</strong></p>
										</div>
									</div>
								</div>
								<?}?>
							
								<div class="form-group">
									<label class="col-md-2 control-label" for="nama">Total Transaksi</label>
									<div class="col-md-3">
										<input class="form-control input number" readonly type="text" id="total_trx" name="total_trx" value=""/>
										
									</div>									
									<div class="col-md-4">
										<button class="btn btn-success" <?=$disabel?> type="button" id="btn_add_pembayaran"><i class="fa fa-plus"></i> Tentukan Cara Pembayaran</button>
									</div>
									
									<div class="col-md-2">
										<input class="form-control input"  type="hidden" id="informasi_id" name="informasi_id" value=""/>
									</div>
								</div>
								<div class="form-group">
							
									 
								</div>
							</div>
							<br>
								<div class="table-responsive">
								<table class="table table-bordered" id="list_pembayaran">
									<thead>
										<tr>
											<th width="5%" class="text-center">NO</th>
											<th width="10%" class="text-center">Jenis Kas</th>
											<th width="15%" class="text-center">Sumber Kas</th>
											<th width="10%" class="text-center">Metode</th>
											<th width="10%" class="text-center">Nominal</th>
											<th width="10%" class="text-center">Tgl Pencairan</th>
											<th width="15%" class="text-center">Keterangan</th>
											<th width="15%" class="text-center">Actions</th>
											
										</tr>
									</thead>
									<tbody>											
									</tbody>
									<tfoot>
										<tr>
											<td colspan="5" class="text-right"> <strong>Total Bayar </strong></td>
											<td colspan="2"> <input class="form-control input number" readonly type="text" id="total_bayar" name="total_bayar" value="0"/></td>
										</tr>
										<tr>
											<td colspan="5" class="text-right"> <strong>Sisa</strong></td>
											<td colspan="2"><input class="form-control input number" readonly type="text" id="total_sisa" name="total_sisa" value="0"/> </td>
										</tr>
									</tfoot>
								</table>	
							</div> 
						</div> 
					</div>
					<!-- END Static Labels -->
				</div>
			</div>
			<? if ($proses_pencairan=='1' && $st_berjenjang=='1'){?>
			<div class="row div_tabel_pencairan">
				<div class="col-md-12">
					<!-- Static Labels block-opt-hidden -->
					<div class="block block-themed" >
						<div class="block-header bg-primary">
							<ul class="block-options">
								<li>
									<button  type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="fa fa-handshake-o"></i> PENGAMBILAN PENCAIRAN</h3>
						</div>
						<div class="block-content block-content">
					
							<br>
								<div class="table-responsive">
								<table class="table table-bordered" id="list_pencairan">
									<thead>
										<tr>
											<th width="5%" class="text-center">NO</th>
											<th width="20%" class="text-center">Tanggal</th>
											<th width="15%" class="text-center">Nominal</th>
											<th width="25%" class="text-center">Keterangan</th>
											<th width="20%" class="text-center">Pengambil</th>
											<th width="15%" class="text-center">Actions</th>
											
										</tr>
										<tr>
											<th width="5%" class="text-center">#
												<input class="form-control input" type="hidden" id="id_pengambilan" name="id_pengambilan" value=""/>
											</th>
											<th width="10%" class="text-center">
												<div class="input-group date">
													<input class="js-datepicker form-control input" <?=$disabel_proses_pencarian?> type="text" id="tanggal_pengambilan" name="tanggal_pengambilan" value="" data-date-format="dd-mm-yyyy"/>
													<span class="input-group-addon">
														<span class="fa fa-calendar"></span>
													</span>
												</div>
											</th>
											<th width="15%" class="text-center">
												<input class="form-control input number" type="text" <?=$disabel_proses_pencarian?> id="nominal_pengambilan" name="nominal_pengambilan" value="0"/>
											</th>
											<th width="10%" class="text-center">
												<input class="form-control" name="ket_pengambilan" <?=$disabel_proses_pencarian?> id="ket_pengambilan"></input>
											</th>
											<th width="10%" class="text-center">
												<select name="iduser_pengambil" id="iduser_pengambil" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
													<option value="#">- Pilih User -</option>
													<?
														$q="SELECT * FROM musers H WHERE H.`status`='1'";
														$list_user=$this->db->query($q)->result();
													?>
													<? foreach($list_user as $row){ ?>
														<option value="<?=$row->id?>"><?=$row->name?></option>
													<?}?>
												</select>
											</th>
											<th width="15%" class="text-center">
												<span class="input-group-btn">
													<button class="btn btn-primary btn-sm" type="button" <?=$disabel_proses_pencarian?> id="btn_add_pengambilan" title="Add"><i class="fa fa-level-down"></i></button>
													<button class="btn btn-success btn-sm" type="button" <?=$disabel_proses_pencarian?> id="btn_add_pengambilan_verif" title="Add & Verifikasi"><i class="fa fa-check"></i></button>
													<button class="btn btn-danger btn-sm"  type="button" <?=$disabel_proses_pencarian?> id="btn_cancel_pengambilan" title="Cancel"><i class="si si-close"></i></button>
												</span>
											</th>
											
										</tr>
									</thead>
									<tbody>											
									</tbody>
									<tfoot>
										<tr>
											<td colspan="5" class="text-right"> <strong>Total Pencairan </strong></td>
											<td > <input class="form-control input number" readonly type="text" id="total_pembayaran" name="total_pembayaran" value="0"/></td>
										</tr>
										<tr>
											<td colspan="5" class="text-right"> <strong>Total Pengambilan </strong></td>
											<td > <input class="form-control input number" readonly type="text" id="total_pengambilan" name="total_pengambilan" value="0"/></td>
										</tr>
										<tr>
											<td colspan="5" class="text-right"> <strong>Sisa</strong></td>
											<td ><input class="form-control input number" readonly type="text" id="total_sisa_pengambilan" name="total_sisa_pengambilan" value="0"/> </td>
										</tr>
									</tfoot>
								</table>	
							</div> 
						</div> 
					</div>
					<!-- END Static Labels -->
				</div>
			</div>
			<?}?>
			<div class="form-group">
				<div class="text-right bg-light">
					
					<? if ($disabel==''){?>
						<button class="btn btn-success satu_kali" type="submit" id="btn_simpan_biasa" name="btn_simpan_biasa" value="1">Simpan </button>					
							<?// if ($idvendor!='0' && $cara_pembayaran !='0'){?>
							<?php if (UserAccesForm($user_acces_form,array('1407'))){ ?>
								<?if ($st_validasi=='0'){?>
								<button class="btn btn-primary satu_kali" type="submit" id="btn_simpan" name="btn_simpan_biasa" value="2">Simpan & Validasi</button>
								<?}?>
							<?}?>
							<?//}?>
						<?//}?>
						<? //if ($cara_pembayaran=='2' || $cara_pembayaran=='3'){?>
						<button class="btn btn-danger generate" type="submit" id="btn_generate" name="btn_generate">Generate Pengajuan</button>
					<?}?>
				</div>
			</div>
			
			
	</div>
</div>
<div class="modal fade in black-overlay" id="modal_pembayaran" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li><button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button></li>
					</ul>
					<h3 class="block-title">Pembayaran</h3>
				</div>
				<div class="block-content">
				<?php echo form_open('#','class="form-horizontal push-10-t" ') ?>
				<input  type="hidden" class="form-control" id="sumber_kas_id_tmp" placeholder="sumber_kas_id_tmp" name="sumber_kas_id_tmp" >
				<div class="form-group">
					<label class="col-md-3 control-label" for="nama">Sisa Rp. </label>
					<div class="col-md-7">
						<input readonly type="text" class="form-control number" id="sisa_modal" placeholder="Sisa" name="sisa_modal" required="" aria-required="true">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="jenis_kas_id">Jenis Kas</label>
					<div class="col-md-7">
						<select name="jenis_kas_id" id="jenis_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#">- Pilih Opsi -</option>
							<? foreach($list_jenis_kas as $row){ ?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>
							<?}?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="sumber_kas_id">Sumber Kas</label>
					<div class="col-md-7">
						<select name="sumber_kas_id" id="sumber_kas_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">							
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="idmetode">Metode</label>
					<div class="col-md-7">
						<select name="idmetode" id="idmetode" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="#">Pilih Opsi</option>							
							<option value="1">Cheq</option>	
							<option value="2">Tunai</option>	
							<option value="4">Transfer Antar Bank</option>
							<option value="3">Transfer Beda Bank</option>							
						</select>
					</div>
				</div>
				
				
				<div class="form-group">
					<label class="col-md-3 control-label" for="nama">Nominal Rp. </label>
					<div class="col-md-7">
						<input  type="hidden" class="form-control" id="idpembayaran" placeholder="idpembayaran" name="idpembayaran" >
						<input  type="text" class="form-control number" id="nominal_bayar" placeholder="Nominal" name="nominal_bayar" required="" aria-required="true">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="nama">Tanggal Pencairan</label>
					<div class="col-md-7">
						<div class="input-group date">
						<input class="js-datepicker form-control input" autocomplete="off" type="text" id="tanggal_pencairan" name="tanggal_pencairan" value="<?=$tanggal_dibutuhkan?>" data-date-format="dd-mm-yyyy"/>
						<span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-3 control-label" for="nama">Keterangan </label>
					<div class="col-md-7">
						<textarea class="form-control js-summernote" name="ket_pembayaran" id="ket_pembayaran"></textarea>
					</div>
				</div>

				<div class="modal-footer">
					<button class="btn btn-sm btn-primary" id="btn_add_bayar" type="button"><i class="fa fa-check"></i> Add</button>
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Exit</button>
				</div>
				</form>
			</div>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<?$this->load->view('Mrka_pengajuan/modal_harga')?>
<?$this->load->view('Mrka_pengajuan/modal_vendor')?>
<script type="text/javascript">
	var table;
	var myDropzone 
	$(document).ready(function(){
		$("#sumber_kas_id_tmp").val('');
		Dropzone.autoDiscover = false;
		myDropzone = new Dropzone(".dropzone", { 
		   autoProcessQueue: true,
		   maxFilesize: 30,
		});
		myDropzone.on("complete", function(file) {
		  
		  refresh_image();
		  myDropzone.removeFile(file);
		  
		});
		$(".decimal").number(true,2,'.',',');
		$('.number').number(true, 0, '.', ',');
		clear_input();
		show_hide_pembayaran();
		load_user();
		$('#informasi').summernote({
		  height: 70,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }
		});
		$('#memo_bendahara').summernote({
		  height: 100,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }
		});
		$('#ket_pembayaran').summernote({
		  height: 100,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }
		});
		clear_data_info();
		refresh_image();
		
		$id=$("#id").val();
		if ($id){
			load_item_barang($id);
			load_list_cicilan();
			load_item_termin();
		}
		sleep(1000).then(() => {
			refresh_pembayaran();
		});
		
		// hitung_total();
		
	})	
	function sleep (time) {
	  return new Promise((resolve) => setTimeout(resolve, time));
	}
	function refresh_pembayaran(){
		var id=$("#id").val();
		var disabel=$("#disabel").val();
		// alert($("#total_trx").val());
		$.ajax({
			url: '{site_url}mrka_bendahara/refresh_pembayaran/'+id+'/'+disabel,
			dataType: "json",
			success: function(data) {
				// if (data.detail!=''){
					$('#total_bayar').val(data.nominal_bayar);
					$('#list_pembayaran tbody').empty();
					$("#list_pembayaran tbody").append(data.detail);
					hitung_total();
					$("#total_sisa").val($("#total_trx").val()-data.nominal_bayar);
					clear_input_pembayaran();
					if ($("#cara_pembayaran").val()=='1'){						
						logic_satu_kali();
						
						
					}
					if ($("#proses_pencairan").val()=='1'){						
						clear_pengambilan();
					}
					// alert($("#total_trx").val());
				// }
				
				console.log($("#cara_pembayaran").val());
				
			}
		});
	}
	function refresh_pencairan(){
		var id=$("#id").val();
		var disabel=$("#disabel_proses_pencarian").val();
		
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}mrka_bendahara/refresh_pencairan/'+id+'/'+disabel,
			dataType: "json",
			success: function(data) {
					$('#list_pencairan tbody').empty();
					$("#list_pencairan tbody").append(data.detail);
					$('#total_pengambilan').val(data.total_pengambilan);
					$('#total_pembayaran').val(parseFloat($("#total_bayar").val()));
					$('#total_sisa_pengambilan').val(parseFloat($("#total_bayar").val()) - parseFloat($("#total_pengambilan").val()));
					
					$("#cover-spin").hide();
					
			}
		});
	}
	function refresh_image(){
		var id=$("#id").val();
		
		$.ajax({
			url: '{site_url}mrka_bendahara/refresh_image/'+id,
			dataType: "json",
			success: function(data) {
				if (data.detail!=''){
					$('#list_file tbody').empty();
					$("#box_file2").attr("hidden",false);
					$("#list_file tbody").append(data.detail);
				}
				// console.log();
				
			}
		});
	}
	function clear_pengambilan(){
		$("#id_pengambilan").val('');
		$("#tanggal_pengambilan").val('');
		$("#nominal_pengambilan").val('0');
		$("#ket_pengambilan").val('');
		$("#iduser_pengambil").val('#').trigger('change');
		refresh_pencairan();
	}
	$(document).on("click","#btn_add_pengambilan",function(){
		if (!validate_pengambilan()) return false;
		add_pengambilan(0);
		// get_pembayaran
		
	});
	$(document).on("click","#btn_add_pengambilan_verif",function(){
		if (!validate_pengambilan()) return false;
		add_pengambilan(1);
		// get_pembayaran
		
	});
	$(document).on("click","#btn_cancel_pengambilan",function(){
		clear_pengambilan();
	});
	function validate_pengambilan() {
		if ($("#tanggal_pengambilan").val() == "") {
			sweetAlert("Maaf...", "Tentukan Tanggal!", "error");
			$("#tanggal_pengambilan").focus();
			return false;
		}
		if (parseFloat($("#nominal_pengambilan").val()) == "0") {
			sweetAlert("Maaf...", "Tentukan Nominal!", "error");
			$("#nominal_pengambilan").focus();
			return false;
		}
			
		if (parseFloat($("#nominal_pengambilan").val()) > parseFloat($("#total_sisa_pengambilan").val())) {
			sweetAlert("Maaf...", "Nominal Melebihi Sisa!", "error");
			$("#nominal_pengambilan").focus();
			return false;
		}
		if (($("#iduser_pengambil").val()) == "#") {
			sweetAlert("Maaf...", "Tentukan User pengambil!", "error");
			$("#iduser_pengambil").focus();
			return false;
		}
		return true;
	}
	function add_pengambilan(st_verifikasi){		
		var id=$("#id").val();		
		var id_pengambilan=$("#id_pengambilan").val();		
		var tanggal_pengambilan=$("#tanggal_pengambilan").val();		
		var nominal_pengambilan=$("#nominal_pengambilan").val();		
		var ket_pengambilan=$("#ket_pengambilan").val();		
		var iduser_pengambil=$("#iduser_pengambil").val();		
			
		// table = $('#harga_pokok').DataTable()	
		$.ajax({
			url: '{site_url}mrka_bendahara/save_pengambilan',
			type: 'POST',
			data: {
				id: id,id_pengambilan: id_pengambilan,
				tanggal_pengambilan: tanggal_pengambilan,nominal_pengambilan: nominal_pengambilan,
				ket_pengambilan: ket_pengambilan,iduser_pengambil: iduser_pengambil,
				st_verifikasi:st_verifikasi,
				
			},
			complete: function() {
				swal({
					title: "Berhasil!",
					text: "Proses.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});

				clear_pengambilan();
			}
		});
	}
	$(document).on("click",".edit_pembayaran",function(){
		$("#idpembayaran").val($(this).closest('tr').find("td:eq(8)").text());
		var id=$("#idpembayaran").val();
		var sisa=0;
		$("#modal_pembayaran").modal('show');
		// get_pembayaran
		$.ajax({
			url: '{site_url}mrka_bendahara/get_pembayaran/'+id,
			dataType: "json",
			success: function(data) {
				// alert($("#total_sisa").val());
				var row=data.detail;
				if (data.detail!=''){
					sisa=parseFloat(row.nominal_bayar) + parseFloat($("#total_sisa").val());
					// alert(sisa);
					$("#sumber_kas_id_tmp").val(row.sumber_kas_id);
					// $("#sumber_kas_id").val(row.sumber_kas_id);
					$("#jenis_kas_id").val(row.jenis_kas_id).trigger('change');
					$("#idmetode").val(row.idmetode).trigger('change');
					$("#nominal_bayar").val(row.nominal_bayar);
					$("#tanggal_pencairan").val(row.tanggal_pencairan);
					
					$("#sisa_modal").val(sisa);
					$("#ket_pembayaran").summernote('code',row.ket_pembayaran);
					
				}
				// console.log(data);
				
			}
		});
	});
	$(document).on("click",".hapus_pembayaran",function(){	
		var currentRow=$(this).closest("tr");          
         var id=currentRow.find("td:eq(8)").text();
		 $.ajax({
			url: '{site_url}mrka_bendahara/hapus_pembayaran/',
			type: 'POST',
			data: {id: id},
			success : function(data) {				
				$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
				refresh_pembayaran();
				// table.ajax.reload( null, false );				
			}
		});
	});
	
	$(document).on("click",".hapus_file",function(){	
		var currentRow=$(this).closest("tr");          
         var id=currentRow.find("td:eq(4)").text();
		 $.ajax({
			url: '{site_url}mrka_bendahara/hapus_file',
			type: 'POST',
			data: {id: id},
			success : function(data) {				
				$.toaster({priority : 'success', title : 'Succes!', message : ' FIle Berhasil Dihapus'});
				currentRow.remove();
				// table.ajax.reload( null, false );				
			}
		});
	});
	$(document).on("click","#btn_clear_data",function(){
		clear_data_info();
	});
	$(document).on("click","#btn_simpan",function(){
		// clear_data_info();
		if ($("#jenis_pembayaran").val() !='4'){			
			if (parseFloat($("#total_sisa").val())!='0'){
				sweetAlert("Maaf...", "Sisa Harus di selesaikan!", "error");
				return false;
			}
		}else{
			if ($("#tanggal_kontrabon").val() ==''){	
				sweetAlert("Maaf...", "Tanggal Kontrabon Harus diisi!", "error");
				return false;
			}
			
		}
		// var id=$("#id").val();
		 // $.ajax({
			// url: '{site_url}mrka_bendahara/selesai',
			// type: 'POST',
			// data: {id: id},
			// success : function(data) {				
				 // window.location.href = "{site_url}mrka_bendahara";		
			// }
		// });
		$("#btn_simpan_value").val('2');
		document.getElementById("form1").submit();
	});
	$(document).on("click","#btn_simpan_biasa",function(){
		$("#btn_simpan_value").val('1');
		document.getElementById("form1").submit();
	});
	$(document).on("click","#btn_generate",function(){
		// alert($("#cara_pembayaran").val());
		if ($("#cara_pembayaran").val() !='2' && $("#cara_pembayaran").val() !='3'){	
			sweetAlert("Maaf...", "Tentukan Cara Pembayaran Cicilan / Termin Fix!", "error");
			return false;
		}
		
		// return false;
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Generate Cicilan / Termin Fix ?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			generate_rka();
		});

		
	});
	function generate_rka(){
		var id=$("#id").val();
		$("#cover-spin").show();
		 $.ajax({
			url: '{site_url}mrka_bendahara/generate_cicilan/'+id,
			dataType: "json",
			success : function(data) {				
				window.location.href = "{site_url}mrka_bendahara";		
			}
		});
	}
	$(document).on("keyup","#nominal_bayar",function(){
		console.log($("#nominal_bayar").val());
		if (parseFloat($("#nominal_bayar").val()) > parseFloat($("#sisa_modal").val())){
			sweetAlert("Maaf...", "Nominal Harus sesuai!", "error");
			$("#nominal_bayar").val($("#sisa_modal").val())
			return false;
		}
	});
	$(document).on("click","#btn_add_bayar",function(){
		add_pembayaran();
	});
	function clear_input_pembayaran(){
		$("#jenis_kas_id").val("#").trigger('change');
		$("#sumber_kas_id").val("#").trigger('change');
		$("#nominal_bayar").val(0);
		$("#sisa_modal").val(0);
		$("#idpembayaran").val('');
		$("#idmetode").val("#").trigger('change');
		$("#ket_pembayaran").summernote('code','');
	}
	function add_pembayaran(){
		var idrka=$("#id").val();
		var jenis_kas_id=$("#jenis_kas_id").val();
		var sumber_kas_id=$("#sumber_kas_id").val();
		var nominal_bayar=$("#nominal_bayar").val();
		var idmetode=$("#idmetode").val();
		var idpembayaran=$("#idpembayaran").val();
		var tanggal_pencairan=$("#tanggal_pencairan").val();
		var ket_pembayaran=$("#ket_pembayaran").summernote('code');
		
		if ($("#jenis_kas_id").val() == "#") {
			sweetAlert("Maaf...", "Jenis Kas Harus Diisi!", "error");
			$("#jenis_kas_id").focus();
			return false;
		}
		if ($("#sumber_kas_id").val() == "#") {
			sweetAlert("Maaf...", "Sumber Kas Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		if ($("#idmetode").val() == "#" || $("#idmetode").val() == "") {
			sweetAlert("Maaf...", "Metode  Harus Diisi!", "error");
			$("#idmetode").focus();
			return false;
		}
		if ($("#nominal_bayar").val() == "" || $("#nominal_bayar").val() == "0") {
			sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			$("#sumber_kas_id").focus();
			return false;
		}
		
		
		$.ajax({
			url: '{site_url}mrka_bendahara/save_pembayaran/',
			dataType: "json",
			type: "POST",
			data: {
				idrka:idrka
				,jenis_kas_id:jenis_kas_id
				,sumber_kas_id:sumber_kas_id
				,nominal_bayar:nominal_bayar				
				,idmetode:idmetode				
				,ket_pembayaran:ket_pembayaran				
				,idpembayaran:idpembayaran				
				,tanggal_pencairan:tanggal_pencairan				
			},
			success: function(data) {
				$("#sumber_kas_id_tmp").val('');
				sweetAlert("Success...", "Pembayaran Berhasil disimpan!", "info");
				refresh_pembayaran();
				$("#modal_pembayaran").modal('hide');
			}
		});
		
	}
	$(document).on("change","#jenis_kas_id",function(){
		var jenis_kas_id=$("#jenis_kas_id").val();
		var sumber_kas_id=$("#sumber_kas_id_tmp").val();
		$.ajax({
			url: '{site_url}mrka_bendahara/list_sumber_kas',
			type: 'POST',
			dataType: "json",
			data: {jenis_kas_id: jenis_kas_id,sumber_kas_id: sumber_kas_id},
			success : function(data) {				
				$("#sumber_kas_id").empty();	
				console.log(data.detail);
				// $("#sumber_kas_id").append(data.detail);	
				$('#sumber_kas_id').append(data.detail);
			}
		});
	});
	$(document).on("click","#btn_add_pembayaran",function(){
		let total_sisa=parseFloat($("#total_sisa").val());
		if (total_sisa <=0){
			sweetAlert("Maaf...", "Sisa Pembayaran Tidak sesuai!", "error");
			return false;
		}
		$("#sumber_kas_id_tmp").val('');
		// $("#sumber_kas_id").val($("#sumber_kas_id").val(''));
		$("#modal_pembayaran").modal('show');
		$("#sisa_modal").val($("#total_sisa").val());
		$("#nominal_bayar").val($("#total_sisa").val());
		
	});
	$(document).on("click","#btn_tambah_informasi",function(){
		if ($("#tgl_reminder").val() == "") {
			sweetAlert("Maaf...", "Tanggal Reminder Harus Diisi!", "error");
			$("#tgl_reminder").focus();
			return false;
		}
		// alert($("#informasi").summernote('code'));
		if ($("#informasi").summernote('code') == "<p><br></p>" || $("#informasi").summernote('code') == "") {
			sweetAlert("Maaf...", "Informasi Harus Diisi!", "error");
			$("#informasi").focus();
			return false;
		}
		var idrka=$("#id").val();
		var informasi_id=$("#informasi_id").val();
		var tgl_reminder=$("#tgl_reminder").val();
		var informasi=$("#informasi").summernote('code');
		$.ajax({
			url: '{site_url}mrka_bendahara/save_informasi/',
			dataType: "json",
			type: "POST",
			data: {
				tgl_reminder:tgl_reminder,informasi:informasi,idrka:idrka,informasi_id:informasi_id				
			},
			success: function(data) {
				sweetAlert("Success", "Informasi Berhasil disimpan!", "info");
				clear_data_info();
			}
		});
	});
	function clear_data_info(){
		// $("#informasi").val('');
		$('#informasi').summernote('code','');
		$("#tgl_reminder").val('');
		$("#informasi_id").val('');
		load_informasi();
	}
	function load_informasi(){
		var idrka=$("#id").val();
		
		$.ajax({
			url: '{site_url}mrka_bendahara/load_informasi/'+idrka,
			dataType: "json",
			success: function(data) {
				$("#tabel_informasi tbody").empty();
				$("#tabel_informasi tbody").append(data.detail);
			}
		});
	}
	$(document).on("click",".edit_info",function(){
		$("#informasi_id").val($(this).closest('tr').find("td:eq(6)").text());
		$("#tgl_reminder").val($(this).closest('tr').find("td:eq(4)").text());
		$("#informasi").summernote('code',$(this).closest('tr').find("td:eq(2)").text());
	});
	$(document).on("click",".hapus_info",function(){
		var informasi_id=$(this).closest('tr').find("td:eq(6)").text();
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Akan Menghapus informasi?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka_bendahara/hapus_informasi/'+informasi_id,
				dataType: "json",
				success: function(data) {
					sweetAlert("Maaf...", "Informasi Berhasil dihapus!", "info");
					clear_data_info();
				}
			});
		});

		
	});
	function load_user(){
		var idrka=$("#id").val();
		$("#modal_user").modal('show');
		
		// alert(idrka);
		$.ajax({
			url: '{site_url}mrka_pengajuan/list_user/'+idrka,
			dataType: "json",
			success: function(data) {
				$("#tabel_user_proses tbody").empty();
				$("#tabel_user_proses tbody").append(data.detail);
			}
		});
	}
	function show_hide_pembayaran(){
		$('#div_cicilan').hide();
		$('#div_satu').hide();
		$('#div_kbo').hide();
		$('#div_termin').hide();
		$('#div_tf').hide();
		// alert($("#cara_pembayaran").val());
		if ($("#cara_pembayaran").val()=='1'){//Satu Kali	
			$(':button[type=submit]').removeAttr("disabled");
			$('#div_satu').show();
			// $(".satu_kali").show();
				$(".generate").hide();
			if ($("#jenis_pembayaran").val()=='2' || $("#jenis_pembayaran").val()=='3'){//TF
				$('#div_tf').show();
				
			}else if ($("#jenis_pembayaran").val()=='4'){//Konta	
				$('#div_kbo').show();
			}
			if ($("#idvendor").val()=='0' || $("#idvendor").val()=='#'){
				$("#btn_simpan").hide();
			}else{
				$("#btn_simpan").show();
			}
			logic_satu_kali();
		}else if ($("#cara_pembayaran").val()=='2'){//Cicilan
			$('#div_tf').show();
			$(".satu_kali").hide();
			$(".generate").show();
			$('#div_cicilan').show();
			hitung_total_cicilan();
		}else if ($("#cara_pembayaran").val()=='3'){//Fix
			$('#div_tf').show();
			$(".satu_kali").hide();
			$(".generate").show();
			$('#div_termin').show();
			hitung_total_termin();
		}else if ($("#cara_pembayaran").val()=='0' || $("#cara_pembayaran").val()=='#'){//Pembayaran Nanti
			$(':button[type=submit]').removeAttr("disabled");
			$("#btn_simpan_biasa").show();
			$("#btn_simpan").hide();
			// alert('sini');
			$(".generate").hide();
		}
	}
	function logic_satu_kali(){
		let jenis_bayar=$("#jenis_pembayaran").val();
		let st_berjenjang=$("#st_berjenjang").val();
		$("#btn_simpan_biasa").show();
		// alert(st_berjenjang);
		if (jenis_bayar!='4'){//SELAIN KONTRABON
			if (st_berjenjang=='1'){
				$("#btn_simpan").hide();
			}else{
				if (jenis_bayar=='0' || jenis_bayar=='#'){
					$("#btn_simpan").hide();
				}else{
					
					if ($("#total_sisa").val()>0){
						$("#btn_simpan").hide();
						
					}else{
						$("#btn_simpan").show();
					}
					
					
				}
			}
			$(".div_tabel_bayar").show();
			// alert($("#jenis_bayar").val());
			if (jenis_bayar=='0'){
				$("#btn_add_pembayaran").hide();
			}else{
				$("#btn_add_pembayaran").show();
				
			}
		}else{
			$(".div_tabel_bayar").hide();
			// alert('sini');
			$("#btn_simpan").show();
		}
		console.log('sini satu kali '+$("#total_sisa").val());
	}
	// function clear_input(){
		// $("#iddet").val('')
		// $("#nama_barang").val('')
		// $("#merk_barang").val('')
		// $("#kuantitas").val('0')
		// $("#satuan").val('')
		// $("#harga_satuan").val('0')
		// $("#total_harga").val('')
		// $("#keterangan").val('')
		// $("#rowindex").val('')
		// if ($("#disabel").val==''){
			// $(".edit").attr('disabled', false);
			// $(".hapus").attr('disabled', false);
		// }
		// // hitung_total();
	// }
	function clear_input(){
		$("#iddet").val('')
		$("#nama_barang").val('')
		$("#merk_barang").val('')
		$("#kuantitas").val('0')
		$("#satuan").val('')
		$("#harga_satuan").val('0')
		$("#harga_pokok").val('0')
		$("#harga_diskon").val('0')
		$("#harga_ppn").val('0')
		$("#total_harga").val('')
		$("#keterangan").val('')
		$("#idklasifikasi_det").val($("#idklasifikasi").val());
		$("#rowindex").val('')
		
		if ($("#disabel").val()==''){
			$(".edit").attr('disabled', false);
			$(".hapus").attr('disabled', false);
		}
		// hitung_total();
	}
	$(document).on("change","#jenis_pembayaran,#cara_pembayaran,#idvendor",function(){
		// console.log('sini');
		show_hide_pembayaran();
		
	});
	$(document).on("keyup","#kuantitas,#harga_satuan",function(){
		// console.log('sini');
		perkalian();
		
	});
	function formatNumber(num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}

	function perkalian(){
		var total=0;
		total=parseFloat($("#kuantitas").val()) * parseFloat($("#harga_satuan").val());
		$("#total_harga").val(total)
	}
	// $("#addjual").click(function() {
			// var content = "";
			// if (!validate_detail()) return false;
			// if ($("#rowindex").val() == '') {
				// content += "<tr>";
			// }
			// content += "<td>" + $("#nama_barang").val() + "</td>"; //0 No Urut
			// content += "<td>" + $("#merk_barang").val() + "</td>"; //1 
			// content += "<td>" + $("#kuantitas").val() + "</td>"; //2 
			// content += "<td>" + $("#satuan").val() + "</td>"; //3 
			// content += "<td>" + formatNumber($("#harga_satuan").val()) + "</td>"; //4 
			// content += "<td>" + formatNumber($("#total_harga").val()) + "</td>"; //5 
			// content += "<td>" + $("#keterangan").val() + "</td>"; //6			
			// content +="<td ><div class='btn-group'><button type='button' class='btn btn-xs btn-info edit kunci'><i class='glyphicon glyphicon-pencil'></i></button><button type='button' class='btn btn-xs btn-danger hapus kunci'><i class='glyphicon glyphicon-remove'></i></button></div></td>";
			// content += "<td style='display:none'><input type='text' name='xiddet[]' value='"+$("#iddet").val()+"'></td>"; //8 nomor
			// content += "<td style='display:none'><input type='text' name='xnama_barang[]' value='"+$("#nama_barang").val()+"'></td>"; //9 
			// content += "<td style='display:none'><input type='text' name='xmerk_barang[]' value='"+$("#merk_barang").val()+"'></td>"; //10
			// content += "<td style='display:none'><input type='text' name='xkuantitas[]' value='"+$("#kuantitas").val()+"'></td>"; //11
			// content += "<td style='display:none'><input type='text' name='xsatuan[]' value='"+$("#satuan").val()+"'></td>"; //12
			// content += "<td style='display:none'><input type='text' name='xharga_satuan[]' value='"+$("#harga_satuan").val()+"'></td>"; //13
			// content += "<td style='display:none'><input type='text' name='xtotal_harga[]' value='"+$("#total_harga").val()+"'></td>"; //14
			// content += "<td style='display:none'><input type='text' name='xketerangan[]' value='"+$("#keterangan").val()+"'></td>"; //15
			// content += "<td style='display:none'><input type='text' name='xstatus[]' value='1'></td>"; //16
			// content += "<td style='display:none'><input type='text' name='xharga_pokok[]' value='"+$("#harga_pokok").val()+"'></td>"; //17
			// content += "<td style='display:none'><input type='text' name='xharga_diskon[]' value='"+$("#harga_diskon").val()+"'></td>"; //18
			// content += "<td style='display:none'><input type='text' name='xharga_ppn[]' value='"+$("#harga_ppn").val()+"'></td>"; //19
			// content += "<td style='display:none'><input type='text' name='xidklasifikasi_det[]' value='"+$("#idklasifikasi_det").val()+"'></td>"; //20
			
			// if ($("#rowindex").val() != '') {
				// $('#tabel_add tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
			// } else {
				// content += "</tr>";
				// $('#tabel_add tbody').append(content);
			// }
			 // hitung_total();
			// clear_input();
	// });
	// function validate_detail() {
		// if ($("#nama_barang").val() == "") {
			// sweetAlert("Maaf...", "Nama barang Harus Diisi!", "error");
			// $("#nama_barang").focus();
			// return false;
		// }
		// if ($("#merk_barang").val() == "") {
			// sweetAlert("Maaf...", "Merk barang Harus Diisi!", "error");
			// $("#merk_barang").focus();
			// return false;
		// }	
		
		// if ($("#kuantitas").val() < 1) {
			// sweetAlert("Maaf...", "QTY Harus Diisi!", "error");
			// $("#kuantitas").focus();
			// return false;
		// }
		// if ($("#satuan").val() == "") {
			// sweetAlert("Maaf...", "Satuan barang Harus Diisi!", "error");
			// $("#satuan").focus();
			// return false;
		// }
		// if ($("#harga_satuan").val() < 1) {
			// sweetAlert("Maaf...", "Harga Harus Diisi!", "error");
			// $("#harga_satuan").focus();
			// return false;
		// }
		// if ($("#total_harga").val() < 1) {
			// sweetAlert("Maaf...", "Total Harga Harus Diisi!", "error");
			// $("#total_harga").focus();
			// return false;
		// }
		// return true;
	// }
	function validate_final() {
		var rowCount = $('#tabel_add tbody tr').length;
		if (rowCount < 1){
			sweetAlert("Maaf...", "Tidak ada barang yang diajukan!", "error");
			return false;
		}
		if ($("#idunit").val() == "#") {
			sweetAlert("Maaf...", "Unit Harus Diisi!", "error");
			$("#idunit").focus();
			return false;
		}
		if ($("#idunit_pengaju").val() == "#") {
			sweetAlert("Maaf...", "Unit Pengaju Harus Diisi!", "error");
			$("#idunit_pengaju").focus();
			return false;
		}
		if ($("#idjenis").val() == "#") {
			sweetAlert("Maaf...", "Jenis Pengajuan Harus Diisi!", "error");
			$("#idjenis").focus();
			return false;
		}
		if ($("#nama_kegiatan").val() == "") {
			sweetAlert("Maaf...", "Kegiatan Harus Diisi!", "error");
			$("#merk_barang").focus();
			return false;
		}	
		if ($("#idvendor").val() == "#") {
			sweetAlert("Maaf...", "Teruntukan Vendor!", "error");
			$("#cara_pembayaran").focus();
			return false;
		}
		if ($("#cara_pembayaran").val() == "#") {
			sweetAlert("Maaf...", "Cara Pembayaran Harus Diisi!", "error");
			$("#cara_pembayaran").focus();
			return false;
		}
		
		// if ($("#cara_pembayaran").val() != "#" && $("#cara_pembayaran").val() != "0") {
			// if ($("#jenis_pembayaran").val() == "#") {
				// sweetAlert("Maaf...", "Jenis Pembayaran Diisi!", "error");
				// $("#jenis_pembayaran").focus();
				// return false;
			// }	
		// }
		if ($("#cara_pembayaran").val() == "1") {//1 kali
				if ($("#jenis_pembayaran").val()=='2' || $("#jenis_pembayaran").val()=='3'){//TF
					if ($("#norek").val()=='' || $("#bank").val()=='' || $("#atasnama").val()==''){
						sweetAlert("Maaf...", "Data Bank harus Diisi!", "error");
						return false;
					}
				}else if ($("#jenis_pembayaran").val()=='4'){//Konta	
					if ($("#tanggal_kontrabon").val()==''){
						sweetAlert("Maaf...", "tanggal Kontrabon harus Diisi!", "error");
						return false;
					}
				}
		}
		// alert($("#cara_pembayaran").val() + ' : '+ $("#sisa_termin").val());
		if ($("#cara_pembayaran").val() == "2") {//CICILAN
			// alert('sini');
			// if ($("#total_dp_cicilan").val()=='0'){
				// sweetAlert("Maaf...", "DP Belum diinput Harus Diisi!", "error");
				// $("#cara_pembayaran").focus();
				// return false;
			// }else{
				var rowCount = $('#tabel_cicilan tbody tr').length;
				if (rowCount==0){
					sweetAlert("Maaf...", "Cicilan Belum diinput Harus Diisi!", "error");
					return false;
				}
			// }
			
			// return false;
			
		}
		if ($("#cara_pembayaran").val() == "3" && $("#sisa_termin").val()>0) {
			sweetAlert("Maaf...", "Termin Belum selesai Harus Diisi!", "error");
			$("#cara_pembayaran").focus();
			return false;
		}
		if ($('input[name="st_user_unit"]').val()=='0' && $('input[name="st_user_unit_lain"]').val()=='0' && $('input[name="st_user_bendahara"]').val()=='0'){
			sweetAlert("Maaf...", "Tentukan Unit Proses!", "error");
			return false;
		}
		$("#cover-spin").show();
		$("*[disabled]").not(true).removeAttr("disabled");
		return true;
	}
	$(document).on("click", ".edit", function() {
			$(".edit").attr('disabled', true);
			$(".hapus").attr('disabled', true);
			$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);			
			$("#nama_barang").val($(this).closest('tr').find("td:eq(9) input").val());
			$("#merk_barang").val($(this).closest('tr').find("td:eq(10) input").val());
			$("#kuantitas").val($(this).closest('tr').find("td:eq(11) input").val());
			$("#satuan").val($(this).closest('tr').find("td:eq(12) input").val());
			$("#harga_satuan").val($(this).closest('tr').find("td:eq(13) input").val());
			$("#total_harga").val($(this).closest('tr').find("td:eq(14) input").val());
			$("#keterangan").val($(this).closest('tr').find("td:eq(15) input").val());
			$("#iddet").val($(this).closest('tr').find("td:eq(8) input").val());
			
			$("#harga_pokok").val($(this).closest('tr').find("td:eq(17) input").val());
			$("#harga_diskon").val($(this).closest('tr').find("td:eq(18) input").val());
			$("#harga_ppn").val($(this).closest('tr').find("td:eq(19) input").val());
			$("#idklasifikasi_det").val($(this).closest('tr').find("td:eq(20) input").val());
			
			// alert($(this).closest('tr').find("td:eq(8) input").val());
			$("#kuantitas").focus();
	});
	// $(document).on("click", ".hapus", function() {
		// var baris=$(this).closest('td').parent();
		
			// if ($("#id").val()==''){
				// // alert($("#id").val());
				// baris.remove();
				// hitung_total();
			// }else{
				// $(this).closest('tr').find("td:eq(16) input").val(0)
				// $(this).closest('tr').find("td:eq(6)").html('<span class="label label-danger">DELETED</span>')
				// $(this).closest('tr').find("td:eq(7)").html('<span class="label label-danger">DELETED</span>')
			// }
		
		 // hitung_total();
	// });
	
	function hitung_total(){
		var total_grand = 0;
		var total_item = 0;
		var total_jenis_barang = 0;
		$('#tabel_add tbody tr').each(function() {
			if ($(this).closest('tr').find("td:eq(16) input").val()=='1'){
				total_jenis_barang +=1;
				total_grand += parseFloat($(this).find('td:eq(14) input').val());
				total_item += parseFloat($(this).find('td:eq(11) input').val());
			}
		});
		// alert(total_grand);
		$("#grand_total").val(total_grand);
		$("#total_trx").val(total_grand);
		$("#total_item").val(total_item);
		$("#total_jenis_barang").val(total_jenis_barang);
	}
	$(document).on("click", "#canceljual", function() {
		clear_input();
	});
	$(document).on("change", "#idrka", function() {
		refresh_program();
	});
	$(document).on("click", "#btn_refresh_vendor", function() {
		refresh_vendor();
	});
	
	function refresh_vendor(){
		$.ajax({
			url: '{site_url}mrka_pengajuan/refresh_vendor/',
			dataType: "json",
			success: function(data) {
				$("#idvendor").empty();
				$('#idvendor').append('<option value="#">- Pilih Vendor -</option><option value="0">Tentukan Vendor Nanti</option>');
				$('#idvendor').append(data.detail);
			}
		});
	}
	function refresh_program(){
		var idrka=$("#idrka").val();
		$.ajax({
			url: '{site_url}mrka_pengajuan/refresh_program/'+idrka,
			dataType: "json",
			success: function(data) {
				$("#idprogram").empty();
				$('#idprogram').append('<option value="#">- Pilih Program -</option>');
				$('#idprogram').append(data.detail);
			}
		});
	}
	
	// //Termin
	// $("#add_dp_termin").click(function() {
			// var content = "";
			
			// if (!validate_detail_termin()) return false;
			// if ($("#rowindex_dp_cicilan").val() == '') {
				// content += "<tr>";
			// }
		// content+='<td><input class="form-control input-sm" readonly type="text" value="'+$("#xjenis_termin option:selected").text()+'"/></td>';	
		// content+='<td><input class="form-control input-sm" readonly type="text" name="judul_termin[]" value="'+$("#xjudul_termin").val()+'"</td>';
		// content+='<td><input class="form-control input-sm" readonly type="text" name="deskripsi_termin[]" value="'+$("#xdeskripsi_termin").val()+'"/></td>';
		// content+='<td><input class="form-control input-sm number" readonly type="text" name="nominal_termin[]" value="'+$("#xnominal_termin").val()+'"/></td>';
		// content+='<td>';
			// content+='<div class="input-group date">';
				// content+='<input class="js-datepicker form-control input-sm"  disabled type="text" name="tanggal_termin[]" value="'+$("#xtanggal_termin").val()+'" data-date-format="dd-mm-yyyy"/>';
				// content+='<span class="input-group-addon">';
					// content+='<span class="fa fa-calendar"></span>';
				// content+='</span>';
			// content+='</div>';
		// content+='</td>';
		// content+='<td><button type="button" class="btn btn-sm btn-danger hapus_termin" tabindex="9" id="cancel_dp_termin" title="Refresh"><i class="fa fa-times"></i></button></td>';
		// content+='<td style="display:none"><input class="form-control input-sm" type="text" name="jenis_termin[]" value="'+$("#xjenis_termin").val()+'"/></td>';
		// content+='<td style="display:none"><input class="form-control input-sm" type="text" name="status_dibayar_termin[]" value="0"/></td>';
			
			// if ($("#rowindex").val() != '') {
				// $('#tabel_dp_termin tbody tr:eq(' + $("#rowindex_dp_cicilan").val() + ')').html(content);
			// } else {
				// content += "</tr>";
				// $('#tabel_dp_termin tbody').append(content);
			// }
			// hitung_total_termin();
			// clear_input_termin();
	// });
	// function validate_detail_termin() {
		// // alert('sini');
		// if ($("#xjenis_termin").val() == "#") {
			// sweetAlert("Maaf...", "Tipe Transaksi Harus Diisi!", "error");
			// $("#xjenis_termin").focus();
			// return false;
		// }
		// if ($("#xjudul_termin").val() == "") {
			// sweetAlert("Maaf...", "Judul Harus Diisi!", "error");
			// $("#xjudul_termin").focus();
			// return false;
		// }
		// if ($("#xdeskripsi_termin").val() == "") {
			// sweetAlert("Maaf...", "Deskripsi Harus Diisi!", "error");
			// $("#xdeskripsi_termin").focus();
			// return false;
		// }
		// if ($("#xnominal_termin").val() == "" || $("#xnominal_termin").val() == "0") {
			// sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			// $("#xnominal_termin").focus();
			// return false;
		// }
		// if ($("#xtanggal_termin").val() == "") {
			// sweetAlert("Maaf...", "Tanggal Harus Diisi!", "error");
			// $("#xtanggal_termin").focus();
			// return false;
		// }
		// var sisa=0;
		// sisa=parseFloat($("#grand_total").val()) - (parseFloat($("#xnominal_termin").val()) + parseFloat($("#total_dp_termin").val()));
		// if (sisa < 0){
			// sweetAlert("Maaf...", "Pembayaran Melebihi dari tagihan!", "error");
			// return false;
		// }
		
		
		// return true;
	// }
	// $(document).on("click", ".hapus_termin", function() {
		// var baris=$(this).closest('td').parent();		
		// baris.remove();
		// hitung_total_termin();
		// clear_input_termin();
	// });
	
	
	// function hitung_total_termin(){
		// var total_dp = 0;
		// $('#tabel_dp_termin tbody tr').each(function() {
				// console.log($(this).find('td:eq(3) input').val());
				// total_dp += parseFloat($(this).find('td:eq(3) input').val());
		// });
		// $("#total_dp_termin").val(total_dp);
		// $("#sisa_termin").val(parseFloat($("#grand_total").val()) - total_dp);
		
	// }
	// function clear_input_termin(){
		// $("#xjenis_termin").val('#').trigger('change')
		// $("#xjudul_termin").val('')
		// $("#xdeskripsi_termin").val('')
		// $("#xnominal_termin").val('0')
		// $("#xtanggal_termin").val('')
		// $('.number').number(true, 0, '.', ',');
	// }
	
	// // END TERMIN
	$(document).on("click", "#btn_open_modal_kegiatan", function() {
		$("#modal_kegiatan").modal('show');
		
	});
	function load_kegiatan(){
		var idrka=$("#idrka").val();
		var idprespektif=$("#idprespektif").val();
		var idprogram=$("#idprogram").val();
		
		$('#tabel_kegiatan').DataTable().destroy();
		table=$('#tabel_kegiatan').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mrka_pengajuan/load_kegiatan_reminder',
					type: "POST",
					dataType: 'json',
					data : {
						idrka:idrka,
						idprespektif:idprespektif,
						idprogram:idprogram,
						
					   }
				},
				"columnDefs": [
					{"targets": [0,1,2,3,4], "visible": false },
					{"targets": [10], "class": 'text-center' },
					
				]
			});
	}
	$(document).on("click",".pilih",function(){
		var table = $('#tabel_kegiatan').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		var bulan = table.cell(tr,1).data()
		$('#tabel_add tbody').empty();
		hitung_total();
		$.ajax({
			url: '{site_url}mrka_pengajuan/get_pilih_kegiatan/'+id+'/'+bulan,
			dataType: "json",
			success: function(data) {
				$("#nama_kegiatan").val(data.nama);
				$("#bulan").val(bulan);
		// alert(id+' '+bulan);
				$("#idrka_kegiatan").val(id);
				$("#idunit_pengaju").val(data.idunit).trigger('change');
				$("#idunit").val(data.idunit).trigger('change');
				$("#modal_kegiatan").modal('hide');
			}
		});
		
		
	});
	$(document).on("click","#btn_filter_rka",function(){
		load_kegiatan();
		
	});
	function verifikasi_berjenjang(idrka,tanggal_pencairan,idakun){
		// alert(tanggal_pencairan);
		// return false
		swal({
			title: "Apakah Anda Yakin  ?",
			text : "Proses validasi pembayaran ini?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}mrka_bendahara/verifikasi_berjenjang',
				type: 'POST',
				data: {idrka:idrka,tanggal_pencairan:tanggal_pencairan,idakun:idakun},
				complete: function() {
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Validasi berhasil'});
					refresh_pembayaran();
				}
			});
		});
		
		
	}
	function hapus_pencairan(id){
		swal({
			title: "Apakah Anda Yakin  ?",
			text : "Hapus Bukti pengambilan?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}mrka_bendahara/hapus_pencairan',
				type: 'POST',
				data: {id:id},
				complete: function() {
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus berhasil'});
					clear_pengambilan();
				}
			});
		});
		
		
	}
	function verifikasi_pencairan(id){
		swal({
			title: "Apakah Anda Yakin  ?",
			text : "Verifikasi Bukti pengambilan?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$("#cover-spin").show();
			$.ajax({
				url: '{site_url}mrka_bendahara/verifikasi_pencairan',
				type: 'POST',
				data: {id:id},
				complete: function() {
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' proses berhasil'});
					refresh_pencairan();
				}
			});
		});
		
		
	}
	function edit_pencairan(id){
		
		$("#id_pengambilan").val(id);
		
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}mrka_bendahara/edit_pencairan',
			type: 'POST',
			dataType: "json",
			data: {id:id},
			success: function(data) {
				console.log(data.tanggal);
				$("#tanggal_pengambilan").val(data.tanggal);
				$("#nominal_pengambilan").val(data.nominal);
				$("#total_sisa_pengambilan").val($("#total_sisa_pengambilan").val()+data.nominal);
				$("#ket_pengambilan").val(data.keterangan);
				$("#iduser_pengambil").val(data.iduser_pengambil).trigger('change');
				
				$("#cover-spin").hide();
			}
		});
	
	}
	function goBack() {
	  window.history.back();
	}
</script>
</script>