<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo error_message($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mpendapatan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mpendapatan/save','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="keterangan">Keterangan</label>
				<div class="col-md-7">
					<input type="text" class="form-control" name="keterangan" value="{keterangan}" />
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="noakun">No. Akun Kredit</label>
				<div class="col-md-7">
					<select class="js-select2 form-control" id="idakun" name="idakun" style="width: 100%;" data-placeholder="No. Akun Kredit">
						<option value="#">Pilih Opsi</option>
						<?php foreach ($list_noakun as $row){ ?>
							<option value="<? echo $row->id ?>" <?=($idakun==$row->id) ? "selected" : "" ?>><?php echo $row->noakun.' '.$row->namaakun; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="keterangan">No AKun</label>
				<div class="col-md-7">
					<input type="text" readonly class="form-control" id="noakun" placeholder="No Akun" name="noakun" value="{noakun}">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mpotongan_dokter" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
		<?php echo form_hidden('id', $id); ?>
		<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		// $('.number').number(true, 0, '.', ',');
	});
	$("#idakun").change(function(){
		$.ajax({
			url: '{site_url}msumber_kas/get_noakun/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				$("#noakun").val(data);
			}
		});

	});
</script>
