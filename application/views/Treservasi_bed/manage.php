<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>
<?php $user_id = $this->session->userdata('user_id'); ?>
<?
?>
<div class="block">
	<div class="block-header">
		
		<ul class="block-options">
			<li>
				<a href="{base_url}treservasi_bed" type="button" data-toggle="tooltip" title="" data-original-title="Undo"><i class="si si-action-undo"></i> Kembali</a>
			</li>
			
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
    <div class="block-content ">
		<?php echo form_open_multipart('treservasi_bed/simpan', 'class="js-validation-bootstrap form-horizontal" id="form_input" onsubmit="return validate_final()"') ?>
		<div class="row">
			<input type="hidden" id="id" name="id" value="{id}">
			<input type="hidden" id="idpoliklinik" name="idpoliklinik" value="{idpoliklinik}">
			<input type="hidden" id="idtipe" name="idtipe" value="{idtipe}">
			<input type="hidden" id="asal_reservasi" name="asal_reservasi" value="{asal_reservasi}">
			<input type="hidden" id="pendaftaran_id" name="pendaftaran_id" value="{pendaftaran_id}">
			<input type="hidden" id="perencanaan_id" name="perencanaan_id" value="{perencanaan_id}">
			<input type="hidden" id="idpasien" name="idpasien" value="{idpasien}">
			<input type="hidden" id="iddokter_perujuk" name="iddokter_perujuk" value="{iddokter_perujuk}">
			<div class="col-md-12">
				<div class="form-group" style="margin-bottom: 10px;">
					<div class="col-md-2 ">
						<label for="no_medrec">No Medrec</label>
						<input type="text" readonly class="form-control" id="no_medrec" name="no_medrec" placeholder="No Medrec"  value="{no_medrec}">
					</div>
					<div class="col-md-2 ">
						<label for="no_medrec">No Pendaftaran</label>
						<div class="input-group">
							<input type="text" readonly class="form-control" id="nopendaftaran" name="nopendaftaran" placeholder="No Pendaftaran"  value="{nopendaftaran}">
							<span class="input-group-btn">
								<button class="btn btn-primary" type="button" onclick="show_modal_pendaftaran()"><i class="fa fa-pencil"></i></button>
							</span>
						</div>
					</div>
					<div class="col-md-4 ">
						<label for="namapasien">Nama Pasien</label>
						<input type="text" readonly class="form-control" id="namapasien" name="namapasien" placeholder="namapasien"  value="{namapasien}">
					</div>
					<div class="col-md-4 ">
						<label for="asal_poli">Asal Rujukan</label>
						<input type="text" readonly class="form-control" id="asal_poli" name="asal_poli" placeholder="asal_poli"  value="{asal_poli}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<div class="col-md-4 ">
						<label for="nama_poli">Poliklinik</label>
						<input type="text" readonly class="form-control" id="nama_poli" name="nama_poli" placeholder="No nama_poli"  value="{nama_poli}">
					</div>
					<div class="col-md-4 ">
						<label for="nama_dokter">Dokter Perujuk</label>
						<input type="text" readonly class="form-control" id="nama_dokter" name="nama_dokter" placeholder="No nama_dokter"  value="{nama_dokter}">
					</div>
					<div class="col-md-4 ">
						<label for="dpjp">DPJP</label>
						<select id="dpjp" name="dpjp" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="" <?=($dpjp==''?'selected':'')?>>- Pilih DPJP-</option>
							<?foreach(get_all('mdokter',array('status'=>1)) as $r){?>
							<option value="<?=$r->id?>" <?=($dpjp==$r->id?'selected':'')?>><?=$r->nama?></option>
							<?}?>
							
							
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<div class="col-md-4 ">
						<label for="nopermintaan">No Perencanaan</label>
						<div class="input-group">
						<input type="text" readonly class="form-control" id="nopermintaan" name="nopermintaan" placeholder="No Permintaan"  value="{nopermintaan}">
							<span class="input-group-btn">
								<button class="btn btn-primary" onclick="show_modal_perencanaan()" type="button"><i class="fa fa-pencil"></i></button>
							</span>
						</div>
					</div>
					<div class="col-md-8 ">
						<label for="diagnosa">Dengan Diagnosa</label>
						<input type="text"  class="form-control" id="diagnosa" name="diagnosa" placeholder="Dengan Diagnosa"  value="<?=strip_tags($diagnosa)?>">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<div class="col-md-4 ">
						<label class="h5 text-primary">RENCANA</label>
					</div>
					
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<div class="col-md-3 ">
						<label for="idruangan">Rencana Ruangan</label>
						<select id="idruangan"  name="idruangan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="" <?=($idruangan==''?'selected':'')?>>- Pilih Ruangan-</option>
							<?foreach($list_ruang as $r){?>
							<option value="<?=$r->id?>" <?=($idruangan==$r->id?'selected':'')?>><?=$r->nama?></option>
							<?}?>
						</select>
					</div>
					<div class="col-md-2 ">
						<label for="idkelas">Rencana Kelas</label>
						<select id="idkelas"  name="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="" <?=($idkelas==''?'selected':'')?>>- Pilih Rencana Kelas-</option>
							<?foreach($list_kelas as $r){?>
							<option value="<?=$r->id?>" <?=($idkelas==$r->id?'selected':'')?>><?=$r->nama?></option>
							<?}?>
						</select>
						
					</div>
					<div class="col-md-3 ">
						<label for="idbed">Rencana Tempat Tidur</label>
						<select id="idbed"  name="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="" <?=($idbed==''?'selected':'')?>>- Rencana Tempat Tidur-</option>
							<?foreach($list_bed as $r){?>
							<option value="<?=$r->id?>" <?=($idbed==$r->id?'selected':'')?>><?=$r->nama?></option>
							<?}?>
						</select>
					</div>
					<div class="col-md-2 ">
						<label for="prioritas">Prioritas</label>
						<select id="prioritas"  name="prioritas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="" <?=($prioritas==''?'selected':'')?>>- Pilih Prioritas-</option>
							<?foreach(list_variable_ref(125) as $r){?>
							<option value="<?=$r->id?>" <?=($prioritas==$r->id?'selected':'')?>><?=$r->nama?></option>
							<?}?>
						</select>
						
					</div>
					<div class="col-md-2 ">
						<label for="rencana_masuk">Rencana Masuk</label>
						<div class="input-group date">
							<input tabindex="2" type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" id="rencana_masuk" placeholder="dd-mm-yyyy" name="rencana_masuk" value="<?= $rencana_masuk ?>" required>
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<div class="col-md-4 ">
						<label class="h5 text-primary">ALTERNATIF</label>
					</div>
					
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<div class="col-md-3 ">
						<label for="idruangan_alter">Rencana Ruangan</label>
						<select id="idruangan_alter"  name="idruangan_alter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="" <?=($idruangan_alter==''?'selected':'')?>>- Pilih DPJP-</option>
							<?foreach($list_ruang as $r){?>
							<option value="<?=$r->id?>" <?=($idruangan_alter==$r->id?'selected':'')?>><?=$r->nama?></option>
							<?}?>
						</select>
					</div>
					<div class="col-md-2 ">
						<label for="idkelas_alter">Rencana Kelas</label>
						<select id="idkelas_alter"  name="idkelas_alter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="" <?=($idkelas_alter==''?'selected':'')?>>- Pilih DPJP-</option>
							<?foreach($list_kelas as $r){?>
							<option value="<?=$r->id?>" <?=($idkelas_alter==$r->id?'selected':'')?>><?=$r->nama?></option>
							<?}?>
						</select>
						
					</div>
					<div class="col-md-3 ">
						<label for="idbed_alter">Rencana Tempat Tidur</label>
						<select id="idbed_alter"  name="idbed_alter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="" <?=($idbed_alter==''?'selected':'')?>>- Pilih DPJP-</option>
							<?foreach($list_bed as $r){?>
							<option value="<?=$r->id?>" <?=($idbed_alter==$r->id?'selected':'')?>><?=$r->nama?></option>
							<?}?>
						</select>
					</div>
					
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<div class="col-md-2 ">
						<label for="tipe_pemohon">Data Pemohon</label>
						<select id="tipe_pemohon"  name="tipe_pemohon" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="" <?=($tipe_pemohon==''?'selected':'')?>>- Pemohon-</option>
							<?foreach(list_variable_ref(126) as $r){?>
							<option value="<?=$r->id?>" <?=($tipe_pemohon==$r->id?'selected':'')?>><?=$r->nama?></option>
							<?}?>
						</select>
					</div>
					<div class="col-md-3 ">
						<label for="nama_pemohon">Nama Pemohon</label>
						<input type="text"  class="form-control" id="nama_pemohon" name="nama_pemohon" placeholder="Nama Pemohon"  value="{nama_pemohon}">
						
					</div>
					<div class="col-md-3 ">
						<label for="nohp_pemohon">No Handphone</label>
						<input type="text"  class="form-control" id="nohp_pemohon" name="nohp_pemohon" placeholder="No HP Pemohon"  value="{nohp_pemohon}">
						
					</div>
					<div class="col-md-4 ">
						<label for="alamat_pemohon">Alamat Pemohon</label>
						<input type="text"  class="form-control" id="alamat_pemohon" name="alamat_pemohon" placeholder="Alamat Pemohon"  value="{alamat_pemohon}">
						
					</div>
					
					
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<div class="col-md-12 ">
						<label for="catatan">Catatan</label>
						<input type="text"  class="form-control" id="catatan" name="catatan" placeholder="Catatan"  value="{catatan}">
						
					</div>
					
					
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<div class="col-md-12 ">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<div class="col-md-12 text-right">
						<button type="submit" class="btn btn-success" id="btn_simpan"  type="button"><i class="fa fa-save"></i> Simpan</button>
						<button type="submit" class="btn btn-default" id="btn_simpan"  type="button"><i class="fa fa-reply"></i> Kembali</button>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close() ?>
	
	</div>
</div>
<div class="modal" id="modal_pendaftaran" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog modal-lg" >
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Kunjungan Poliklinik</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal push-10-t">
					<div class="row">
						<div class="col-md-6">                               
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="notransaksi_2" placeholder="No Pendaftaran" name="notransaksi" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Pasien</label>
								<div class="col-md-9">
									<input type="text" class="form-control" placeholder="No Pendaftaran" disabled value="{namapasien}">
								</div>
							</div>
						</div>
						<div class="col-md-6">                               
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
								<div class="col-md-9">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggal_21" name="tanggal_1" placeholder="From" value="{tanggal_1}"/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_22" name="tanggal_2" placeholder="To" value="{tanggal_2}"/>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal"></label>
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="button" onclick="getIndexPoli()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
								</div>
							</div>
						</div>
						<div class="col-md-12">                               
							<div class="table-responsive">
								<table class="table table-bordered" id="index_poli">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="8%">Action</th>
											<th width="10%">Pendaftaran</th>
											<th width="10%">Tanggal Kunjungan</th>
											<th width="8%">No Medrec</th>
											<th width="20%">Nama Pasien</th>
											<th width="15%">Dokter</th>
											<th width="10%">Asal Pasien</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
                    </div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="modal_perencanaan" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" >
	<div class="modal-dialog modal-lg" >
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">Perencanaan Rawat Inap</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal push-10-t">
					<div class="row">
						<div class="col-md-6">                               
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">No Perencanaan</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="nopermintaan_filter" placeholder="No Perencanaan" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Pasien</label>
								<div class="col-md-9">
									<input type="text" class="form-control" placeholder="Nama Pasien" disabled value="{namapasien}">
								</div>
							</div>
						</div>
						<div class="col-md-6">                               
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Tanggal Rencana Masuk</label>
								<div class="col-md-9">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggal_11" name="tanggal_11" placeholder="From" value="{tanggal_1}"/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_12" name="tanggal_12" placeholder="To" value="{tanggal_2}"/>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal"></label>
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="button" onclick="getIndexRencana()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
								</div>
							</div>
						</div>
						<div class="col-md-12">                               
							<div class="table-responsive">
								<table class="table table-bordered" id="index_rencana">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="8%">Action</th>
											<th width="10%">No Perencanaan</th>
											<th width="10%">Rencana Pelayanan</th>
											<th width="8%">No Pendaftaran</th>
											<th width="15%">Nama Pasien</th>
											<th width="10%">Dokter</th>
											<th width="10%">Asal Pasien</th>
											<th width="10%">Dengan Diagnosa</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
                    </div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
$(document).ready(function() {
	// show_modal_perencanaan();
});
$(document).on("change", "#idkelas", function() {
    get_bed();
});
$(document).on("change", "#idruangan", function() {
	$("#idkelas").val("#").trigger('change');
    get_bed();
});
$(document).on("change", "#idkelas_alter", function() {
    get_bed_alter();
});
$(document).on("change", "#idruangan_alter", function() {
	$("#idkelas_alter").val("#").trigger('change');
    get_bed_alter();
});
function show_modal_pendaftaran(){
	$("#modal_pendaftaran").modal('show');
	getIndexPoli();
}
function show_modal_perencanaan(){
	$("#modal_perencanaan").modal('show');
	getIndexRencana();
}

function validate_final()
{
	let asal_reservasi=$("#asal_reservasi").val();
	let dpjp=$("#dpjp").val();
	let idruangan=$("#idruangan").val();
	let idkelas=$("#idkelas").val();
	let prioritas=$("#prioritas").val();
	let rencana_masuk=$("#rencana_masuk").val();
	let diagnosa=$("#diagnosa").val();
	
	if (dpjp=='' || dpjp==null){
		sweetAlert("Maaf...", "Tentukan Dokter DPJP", "error");
		return false;
	}
	if (asal_reservasi=='2'){
		if (diagnosa=='' || diagnosa==null){
		sweetAlert("Maaf...", "Tentukan Diagnosa", "error");
			return false;
		}
	}
	if (idruangan=='' || idruangan==null){
		sweetAlert("Maaf...", "Tentukan Ruangan", "error");
		return false;
	}
	if (idkelas=='' || idkelas==null){
		sweetAlert("Maaf...", "Tentukan Kelas", "error");
		return false;
	}
	if (prioritas=='' || prioritas==null){
		sweetAlert("Maaf...", "Tentukan Prioritas", "error");
		return false;
	}
	if (rencana_masuk=='' || rencana_masuk==null){
		sweetAlert("Maaf...", "Tentukan Rencana Tanggal", "error");
		return false;
	}
	
	
	$("#cover-spin").show();
	return true;
}

function get_bed(){
	let ruangan_id=$("#idruangan").val();
	let idkelas=$("#idkelas").val();
	$.ajax({
		url: '{site_url}treservasi_bed/get_bed/',
		dataType: "json",
		type: "POST",
		data:{
			idruangan:ruangan_id,
			idkelas:idkelas,
		},
		success: function(data) {
			$("#idbed").empty();
			$('#idbed').append('<option value="0" selected>Tidak Ditentukan</option>');
			$('#idbed').append(data.detail);
		}
	});

}
function get_bed_alter(){
	let ruangan_id=$("#idruangan_alter").val();
	let idkelas=$("#idkelas_alter").val();
	$.ajax({
		url: '{site_url}treservasi_bed/get_bed/',
		dataType: "json",
		type: "POST",
		data:{
			idruangan:ruangan_id,
			idkelas:idkelas,
		},
		success: function(data) {
			$("#idbed_alter").empty();
			$('#idbed_alter').append('<option value="0" selected>Tidak Ditentukan</option>');
			$('#idbed_alter').append(data.detail);
		}
	});

}
function getIndexRencana() {
   
    $("#cover-spin").show();
    // alert(ruangan_id);
    $('#index_rencana').DataTable().destroy();
    table = $('#index_rencana').DataTable({
		 autoWidth: false,
            searching: true,
            pageLength: 10,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			 "columnDefs": [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,4,5,6,7] },
						 { "width": "5%", "targets": [0]},
						
					],
            ajax: { 
                url: '{site_url}treservasi_bed/getIndexRencanaCari',
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_1:$("#tanggal_31").val(),
						tanggal_2:$("#tanggal_32").val(),
						nopermintaan:$("#nopermintaan_filter").val(),
						idpasien:$("#idpasien").val(),
						
					   }
            },
			"drawCallback": function(settings) {
				$("#cover-spin").hide();
			}
			
    });
    $("#cover-spin").hide();
}
function getIndexPoli() {
    $("#cover-spin").show();
    // alert(ruangan_id);
    $('#index_poli').DataTable().destroy();
    table = $('#index_poli').DataTable({
		 autoWidth: false,
            searching: true,
            pageLength: 10,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			 "columnDefs": [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,4,5,6,7] },
						 { "width": "5%", "targets": [0]},
						
					],
            ajax: { 
                url: '{site_url}treservasi_bed/getIndexPoliCari',
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_1:$("#tanggal_21").val(),
						tanggal_2:$("#tanggal_22").val(),
						notransaksi:$("#notransaksi_2").val(),
						idpasien:$("#idpasien").val(),
						
					   }
            },
			"drawCallback": function(settings) {
				$("#cover-spin").hide();
			}
			
    });
    $("#cover-spin").hide();
}
function get_data_pendaftaran(id){
	$("#cover-spin").show();
	$("#modal_pendaftaran").modal('hide');
	$.ajax({
		url: '{site_url}treservasi_bed/get_data_pendaftaran/',
		dataType: "json",
		type: "POST",
		data:{
			id:id,
		},
		success: function(data) {
			$("#cover-spin").hide();
			$("#pendaftaran_id").val(id);
			$("#nopendaftaran").val(data.nopendaftaran);
			$("#nama_poli").val(data.nama_poli);
			$("#nama_dokter").val(data.nama_dokter);
			$("#asal_poli").val(data.asal_poli);
		}
	});
}
function get_data_perencanaan(id){
	$("#cover-spin").show();
	$("#modal_perencanaan").modal('hide');
	$.ajax({
		url: '{site_url}treservasi_bed/get_data_perencanaan/',
		dataType: "json",
		type: "POST",
		data:{
			id:id,
		},
		success: function(data) {
			$("#cover-spin").hide();
			$("#perencanaan_id").val(id);
			$("#nopermintaan").val(data.nopermintaan);
			$("#diagnosa").val(data.diagnosa);
		}
	});
}
</script>
