<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') { echo ErrorMessage($error); } ?>
<?php $user_id = $this->session->userdata('user_id'); ?>
<?
?>
<div class="block">
    <ul class="nav nav-tabs" data-toggle="tabs">
        <li class="<?=($tab=='1'?'active':'')?>">
            <a href="#tab_1" ><i class="si si-list"></i> Data Reservasi</a>
        </li>
        <li class="<?=($tab=='2'?'active':'')?>">
            <a href="#tab_2"  ><i class="si si-pin"></i> Kunjungan Rawat Jalan </a>
        </li>
        <li class="<?=($tab=='3'?'active':'')?>">
            <a href="#tab_3" ><i class="fa fa-check-square-o"></i> Perencanaan Rawat Inap</a>
        </li>
		
    </ul>
    <input type="hidden" id="tab" name="tab" value="{tab}">
    <input type="hidden" id="tab_detail" name="tab_detail" value="{tab_detail}">
    <div class="block-content tab-content">
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?>" id="tab_1">
			<?php echo form_open_multipart('#', 'class="js-validation-bootstrap form-horizontal" id="form_input" ') ?>
			<div class="block">
				<div class="row pull-10">
					<div class="col-md-6">
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Tanggal Rencana Masuk</label>
								<div class="col-md-9">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggal_11" name="tanggal_11" placeholder="From" value="{tanggal_1}"/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_12" name="tanggal_12" placeholder="To" value="{tanggal_2}"/>
									</div>
								</div>
							</div>
							
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Ruangan</label>
								<div class="col-md-9">
									<select id="idruangan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- All -</option>
										<?foreach($list_ruang as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
										
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Kelas</label>
								<div class="col-md-9">
									<select id="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- All -</option>
										<?foreach($list_kelas as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
										
										
									</select>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Bed</label>
								<div class="col-md-9">
									<select id="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- All -</option>
										<?foreach($list_bed as $r){?>
										<option value="<?=$r->id?>"><?=$r->nama?></option>
										<?}?>
										
										
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6 push-10">
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Tanggal Input</label>
								<div class="col-md-9">
									<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
										<input class="form-control" type="text" id="tanggal_input_1" name="tanggal_input_1" placeholder="From" value=""/>
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_input_2" name="tanggal_input_2" placeholder="To" value=""/>
									</div>
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">No Medrec</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="no_medrec_1" placeholder="No. Medrec" value="">
								</div>
							</div>
							<div class="form-group" style="margin-bottom: 10px;">
								<label class="col-md-3 control-label" for="tanggal">Nama Pasien</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="namapasien_1" placeholder="Nama Pasien" value="">
								</div>
							</div>
							
							<div class="form-group" style="margin-bottom: 5px;">
								<label class="col-md-3 control-label" for="btn_filter_all"></label>
								<div class="col-md-9">
									<button class="btn btn-success text-uppercase" type="button" onclick="load_index_all()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
								</div>
							</div>
								
						</div>
				</div>
			</div>
			<div class="block">
				<ul class="nav nav-pills">
					<li id="div_1" class="<?=($tab_detail=='1'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab(1)"><i class="si si-list"></i> Semua</a>
					</li>
					<li id="div_2" class="<?=($tab_detail=='2'?'active':'')?>">
						<a href="javascript:void(0)"  onclick="set_tab(2)"><i class="si si-pin"></i> Belum Diproses </a>
					</li>
					<li id="div_3" class="<?=($tab_detail=='3'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab(3)"><i class="fa fa-check-square-o"></i> Telah Diproses</a>
					</li>
					<li id="div_4" class="<?=($tab_detail=='4'?'active':'')?>">
						<a href="javascript:void(0)" onclick="set_tab(4)"><i class="fa fa-times"></i> Batal</a>
					</li>
					
				</ul>
				<div class="block-content">
					<div class="row   ">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-bordered" id="index_all">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="8%">Tanggal</th>
											<th width="12%">Pasien</th>
											<th width="10%">Rencana Ruangan</th>
											<th width="5%">Rencana Kelas</th>
											<th width="8%">Rencana Bed</th>
											<th width="10%">Tanggal Rencana</th>
											<th width="12%">DPJP</th>
											<th width="8%">Priority</th>
											<th width="10%">Status</th>
											<th width="15%">Aksi</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
		<div class="tab-pane fade fade-left <?=($tab=='2'?'active in':'')?>" id="tab_2">
			<?php echo form_open_multipart('#', 'class="js-validation-bootstrap form-horizontal" id="form_input_poli" ') ?>
			<div class="row pull-10">
				<div class="col-md-6">
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
							<div class="col-md-9">
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" id="tanggal_21" name="tanggal_1" placeholder="From" value="{tanggal_1}"/>
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" id="tanggal_22" name="tanggal_2" placeholder="To" value="{tanggal_2}"/>
								</div>
							</div>
						</div>
						
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
							<div class="col-md-9">
								<select tabindex="13" id="idtipe_2" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="#" selected>All</option>
									<option value="1">POLIKLINIK</option>
									<option value="2">IGD</option>
									
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Poliklinik</label>
							<div class="col-md-9">
								<select id="idpoli_2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- All -</option>
									<?foreach($list_poli as $r){?>
									<option value="<?=$r->id?>"><?=$r->nama?></option>
									<?}?>
									
									
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Dokter Perujuk</label>
							<div class="col-md-9">
								<select id="iddokter_2" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua Dokter -</option>
									<?foreach(get_all('mdokter',array('status'=>1)) as $r){?>
									<option value="<?=$r->id?>"><?=$r->nama?></option>
									<?}?>
									
									
								</select>
							</div>
						</div>
						
						
						
						
					</div>
					<div class="col-md-6 push-10">
						
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">No Pendaftaran</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="notransaksi_2" placeholder="No Pendaftaran" name="notransaksi" value="">
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">No Medrec</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="no_medrec_2" placeholder="No. Medrec" name="no_medrec" value="">
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Nama Pasien</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="namapasien_2" placeholder="Nama Pasien" name="namapasien" value="">
							</div>
						</div>
						
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-3 control-label" for="btn_filter_all"></label>
							<div class="col-md-9">
								<button class="btn btn-success text-uppercase" type="button" onclick="getIndexPoli()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
							</div>
						</div>
							
					</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered" id="index_poli">
							<thead>
								<tr>
									<th width="5%">No</th>
									<th width="8%">Action</th>
									<th width="10%">Pendaftaran</th>
									<th width="10%">Tanggal Kunjungan</th>
									<th width="8%">No Medrec</th>
									<th width="20%">Nama Pasien</th>
									<th width="15%">Dokter</th>
									<th width="10%">Asal Pasien</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
		<div class="tab-pane fade fade-left <?=($tab=='3'?'active in':'')?>" id="tab_3">
			<?php echo form_open_multipart('#', 'class="js-validation-bootstrap form-horizontal" id="form_input_rencana" ') ?>
			<div class="row pull-10">
				<div class="col-md-6">
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Tanggal Pendaftaran</label>
							<div class="col-md-9">
								<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
									<input class="form-control" type="text" id="tanggal_31" name="tanggal_1" placeholder="From" value="{tanggal_1}"/>
									<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
									<input class="form-control" type="text" id="tanggal_32" name="tanggal_2" placeholder="To" value="{tanggal_2}"/>
								</div>
							</div>
						</div>
						
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Tipe Pelayanan</label>
							<div class="col-md-9">
								<select tabindex="13" id="idtipe_3" class="js-select2 form-control  opsi_change" style="width: 100%;" data-placeholder="Pilih Opsi" required>
									<option value="#" selected>All</option>
									<option value="1">POLIKLINIK</option>
									<option value="2">IGD</option>
									
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Poliklinik</label>
							<div class="col-md-9">
								<select id="idpoli_3" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- All -</option>
									<?foreach($list_poli as $r){?>
									<option value="<?=$r->id?>"><?=$r->nama?></option>
									<?}?>
									
									
								</select>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Dokter Perujuk</label>
							<div class="col-md-9">
								<select id="iddokter_3" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
									<option value="#" selected>- Semua Dokter -</option>
									<?foreach(get_all('mdokter',array('status'=>1)) as $r){?>
									<option value="<?=$r->id?>"><?=$r->nama?></option>
									<?}?>
									
									
								</select>
							</div>
						</div>
						
						
						
						
					</div>
					<div class="col-md-6 push-10">
						
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">No Perencanaan</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="notransaksi_3" placeholder="No Perencanaan" name="notransaksi" value="">
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">No Medrec</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="no_medrec_3" placeholder="No. Medrec" name="no_medrec" value="">
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 10px;">
							<label class="col-md-3 control-label" for="tanggal">Nama Pasien</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="namapasien_3" placeholder="Nama Pasien" name="namapasien" value="">
							</div>
						</div>
						
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-3 control-label" for="btn_filter_all"></label>
							<div class="col-md-9">
								<button class="btn btn-success text-uppercase" type="button" onclick="getIndexRencana()" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
							</div>
						</div>
							
					</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered" id="index_rencana">
							<thead>
								<tr>
									<th width="5%">No</th>
									<th width="8%">Action</th>
									<th width="10%">No Perencanaan</th>
									<th width="10%">Rencana Pelayanan</th>
									<th width="8%">No Pendaftaran</th>
									<th width="15%">Nama Pasien</th>
									<th width="10%">Dokter</th>
									<th width="10%">Asal Pasien</th>
									<th width="10%">Dengan Diagnosa</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
var table;
var tab;
var tab_detail=<?=$tab_detail?>;
var st_login;

$(document).ready(function() {
	// alert(tab_detail);
    getIndexPoli();
    getIndexRencana();
    load_index_all();
});
function set_tab($tab) {
    tab_detail = $tab;
    // alert(tab);
    // $("#cover-spin").show();
    document.getElementById("div_1").classList.remove("active");
    document.getElementById("div_2").classList.remove("active");
    document.getElementById("div_3").classList.remove("active");
    document.getElementById("div_4").classList.remove("active");
    if (tab_detail == '1') {
        document.getElementById("div_1").classList.add("active");
    }
    if (tab_detail == '2') {
        document.getElementById("div_2").classList.add("active");
    }
    if (tab_detail == '3') {
        document.getElementById("div_3").classList.add("active");
    }
	if (tab_detail == '4') {
        document.getElementById("div_4").classList.add("active");
    }
	
    load_index_all();
}

$(document).on("change", "#idkelas", function() {
    get_bed();
});
$(document).on("change", "#ruangan_id", function() {
	$("#idkelas").val("#").trigger('change');
    get_bed();
});

function getIndexPoli() {
    $("#cover-spin").show();
    // alert(ruangan_id);
    $('#index_poli').DataTable().destroy();
    table = $('#index_poli').DataTable({
		 autoWidth: false,
            searching: true,
            pageLength: 10,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			 "columnDefs": [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,4,5,6,7] },
						 { "width": "5%", "targets": [0]},
						
					],
            ajax: { 
                url: '{site_url}treservasi_bed/getIndexPoli',
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_1:$("#tanggal_21").val(),
						tanggal_2:$("#tanggal_22").val(),
						idtipe:$("#idtipe_2").val(),
						idpoli:$("#idpoli_2").val(),
						iddokter:$("#iddokter_2").val(),
						notransaksi:$("#notransaksi_2").val(),
						no_medrec:$("#no_medrec_2").val(),
						namapasien:$("#namapasien_2").val(),
						
					   }
            },
			"drawCallback": function(settings) {
				$("#cover-spin").hide();
			}
			
    });
    $("#cover-spin").hide();
}
function getIndexRencana() {
   
    $("#cover-spin").show();
    // alert(ruangan_id);
    $('#index_rencana').DataTable().destroy();
    table = $('#index_rencana').DataTable({
		 autoWidth: false,
            searching: true,
            pageLength: 10,
            serverSide: true,
			"processing": true,
            "order": [],
            "pageLength": 10,
            "ordering": false,
			 "columnDefs": [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,4,5,6,7] },
						 { "width": "5%", "targets": [0]},
						
					],
            ajax: { 
                url: '{site_url}treservasi_bed/getIndexRencana',
                type: "POST" ,
                dataType: 'json',
				data : {
						tanggal_1:$("#tanggal_31").val(),
						tanggal_2:$("#tanggal_32").val(),
						idtipe:$("#idtipe_3").val(),
						idpoli:$("#idpoli_3").val(),
						iddokter:$("#iddokter_3").val(),
						notransaksi:$("#notransaksi_3").val(),
						no_medrec:$("#no_medrec_3").val(),
						namapasien:$("#namapasien_3").val(),
						
					   }
            },
			"drawCallback": function(settings) {
				$("#cover-spin").hide();
			}
			
    });
    $("#cover-spin").hide();
}
function load_index_all() {
    $('#index_all').DataTable().destroy();
    let idkelas = $("#idkelas").val();
    let idruangan = $("#idruangan").val();
    let idbed = $("#idbed").val();
    $("#cover-spin").show();
    // alert(ruangan_id);
    table = $('#index_all').DataTable({
        autoWidth: false,
        searching: true,
        serverSide: true,
        "processing": true,
        "order": [],
        "pageLength": 10,
        "ordering": false,
         "columnDefs": [
						{  className: "text-right", targets:[0] },
						{  className: "text-center", targets:[1,2,3,4,5,6,7,8,9,10] },
						 { "width": "5%", "targets": [0]},
						
					],
        ajax: {
            url: '{site_url}treservasi_bed/getIndex_all',
            type: "POST",
            dataType: 'json',
            data: {
                idbed: idbed,
                idkelas: idkelas,
                idruangan: idruangan,
                tab_detail: tab_detail,
				no_medrec:$("#no_medrec_1").val(),
				namapasien:$("#namapasien_1").val(),
				tanggal_1:$("#tanggal_11").val(),
				tanggal_2:$("#tanggal_12").val(),
				tanggal_input_1:$("#tanggal_input_1").val(),
				tanggal_input_2:$("#tanggal_input_2").val(),
            },
			
        },
        "drawCallback": function(settings) {
            // $("#index_all thead").remove();
            $("#cover-spin").hide();
        }
    });
    $("#cover-spin").hide();
}
function batal(id){
	swal({
		title: "Anda Yakin ?",
		text : "Membatalkan Reservasi ?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
	$.ajax({
		url: '{site_url}treservasi_bed/batal', 
			dataType: "JSON",
			method: "POST",
			data : {
					id :  id,
				},
			success: function(data) {
				$("#cover-spin").hide();
				$.toaster({priority : 'success', title : 'Succes!', message : ' Proses Berhasil Disimpan'});
				$('#index_all').DataTable().ajax.reload( null, false );
			
			}
		});
	});
}
function get_bed(){
	let ruangan_id=$("#idruangan").val();
	let idkelas=$("#idkelas").val();
	$.ajax({
		url: '{site_url}treservasi_bed/get_bed/',
		dataType: "json",
		type: "POST",
		data:{
			idruangan:ruangan_id,
			idkelas:idkelas,
		},
		success: function(data) {
			$("#idbed").empty();
			$('#idbed').append('<option value="#" selected>- All Bed -</option>');
			$('#idbed').append(data.detail);
		}
	});

}

</script>
