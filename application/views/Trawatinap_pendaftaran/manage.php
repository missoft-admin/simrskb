<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block" style="text-transform:uppercase;">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<a href="{base_url}trawatinap_pendaftaran/index" class="btn"><i class="fa fa-reply"></i></a>
			</li>
			</li>
		</ul>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-bottom: 0px;">
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('trawatinap_pendaftaran/save','class="form-horizontal push-10-t" id="form-work"') ?>
		<div class="col-md-8">
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label">Tipe</label>
				<div class="col-md-8">
					<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<?php if (UserAccesForm($user_acces_form,array('368'))){ ?>
							<option value="1" <?=($idtipe == 1 ? 'selected' : '')?>>Rawat Inap</option>
						<?}?>
						<?php if (UserAccesForm($user_acces_form,array('1335'))){ ?>
							<option value="2" <?=($idtipe == 2 ? 'selected' : '')?>>One Day Surgery (ODS)</option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label">Tanggal Pendaftaran<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-6">
							<input type="text" class="js-datepicker form-control" id="tanggaldaftar" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" name="tanggaldaftar" value="{tanggaldaftar}" required>
						</div>
						<div class="col-md-6">
							<input type="text" class="time-datepicker form-control" id="waktudaftar" name="waktudaftar" value="{waktudaftar}" required>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label">No. Medrec<span style="color:red;">*</span></label>
				<div class="col-md-5" style="padding-right: 0px;">
					<input type="text" class="form-control form-input" id="nomedrec" placeholder="No Medrec" name="nomedrec" value="{nomedrec}" readonly="true" required="true">
				</div>
				<div class="col-md-3" style="margin-bottom: 10px;" id="containerButtonSearch">
					<button type="button" class="btn btn-success" data-toggle="modal" data-target="#PasienModal" style="width:100%;">CARI PASIEN</button>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label">Nama Pasien<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<input type="text" class="form-control form-input" id="namapasien" name="namapasien" placeholder="Nama Pasien" value="{namapasien}" required readonly="true">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label">Alamat<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<textarea class="form-control form-input" id="alamat" placeholder="Alamat Pasien" name="alamat" required readonly="true">{alamatpasien}</textarea>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label">Tanggal Lahir<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<input type="text" class="form-control form-input" placeholder="Tanggal Lahir" id="tanggallahir" value="{tanggallahir}" required readonly="true">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label">Umur<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<input type="text" class="form-control form-input" placeholder="Umur" id="umur" value="{umur}" required readonly="true">
				</div>
			</div>

			<hr>

			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label">Rujukan<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<input type="text" class="form-control form-input" placeholder="Rujukan" id="rujukan" value="{rujukan}" required readonly="true">
				</div>
			</div>
			<div id="form-poliklinik" class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label">Poliklinik<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<input type="text" class="form-control form-input" placeholder="Poliklinik" id="poliklinik" value="{poliklinik}" required readonly="true">
				</div>
			</div>

			<!-- Sebelum Pemilihan COB -->
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label">Kelompok Pasien 1<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<select name="idkelompokpasien" id="idkelompokpasien" class="js-select2 form-control form-select" style="width: 100%;" data-placeholder="Pilih Opsi" required>
						<option value="0">Pilih Opsi</option>
						<?php foreach (get_all('mpasien_kelompok') as $row){?>
						<option value="<?=$row->id;?>" <?=($row->id == $idkelompokpasien ? 'selected':'')?>><?= $row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div id="form-rekanan" class="form-group" style="<?=($idkelompokpasien == 1 ? '': 'display: none;')?> margin-bottom: 10px;">
				<label class="col-md-4 control-label">Rekanan 1</label>
				<div class="col-md-8">
					<select name="idrekanan" id="idrekanan" class="js-select2 form-control form-input" style="width: 100%;" data-placeholder="Pilih Opsi">
						<?php if ($id) { ?>
							<?php foreach (get_all('mrekanan') as $row){?>
								<option value="<?=$row->id;?>" <?=($row->id == $idrekanan ? 'selected':'')?>><?= $row->nama;?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>
			<div id="form-bpjskesehatan" class="form-group" style="<?=($idkelompokpasien == 3 ? '': 'display: none;')?> margin-bottom: 10px;">
				<label class="col-md-4 control-label">BPJS Kesehatan 1</label>
				<div class="col-md-8">
					<select name="idtarifbpjskesehatan" id="idtarifbpjskesehatan" class="js-select2 form-control form-input" style="width: 100%;" data-placeholder="Pilih Opsi">
						<?php if ($id) { ?>
							<?php foreach (get_all('mtarif_bpjskesehatan') as $row){?>
								<option value="<?=$row->id;?>" <?=($row->id == $idtarifbpjskesehatan ? 'selected':'')?>><?= $row->nama;?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>
			<div id="form-bpjstenagakerja" class="form-group" style="<?=($idkelompokpasien == 4 ? '': 'display: none;')?> margin-bottom: 10px;">
				<label class="col-md-4 control-label">BPJS Tenagakerja 1</label>
				<div class="col-md-8">
					<select name="idtarifbpjstenagakerja" id="idtarifbpjstenagakerja" class="js-select2 form-control form-input" style="width: 100%;" data-placeholder="Pilih Opsi">
						<?php if ($id) { ?>
							<?php foreach (get_all('mtarif_bpjstenagakerja') as $row){?>
								<option value="<?=$row->id;?>" <?=($row->id == $idtarifbpjstenagakerja ? 'selected':'')?>><?= $row->nama;?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label">Tipe Pasien</label>
				<div class="col-md-8">
					<select name="idtipepasien" id="idtipepasien" class="js-select2 form-control form-select" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="0">Pilih Opsi</option>
						<option value="1" <?=($idtipepasien == 1 ? 'selected' : '')?>>Pasien RS</option>
						<option value="2" <?=($idtipepasien == 2 ? 'selected' : '')?>>Pasien Pribadi</option>
					</select>
				</div>
			</div>
			<div id="form-jenispasien" class="form-group" style="<?=(($idkelompokpasien == '0' || $idkelompokpasien == '5') ? '': 'display: none;')?> margin-bottom: 10px;">
				<label class="col-md-4 control-label">Jenis Pasien</label>
				<div class="col-md-8">
					<select name="idjenispasien" id="idjenispasien" class="js-select2 form-control form-select" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="0">Pilih Opsi</option>
						<option value="1" <?=($idjenispasien == 1 ? 'selected' : '')?>>Non COB</option>
						<option value="2" <?=($idjenispasien == 2 ? 'selected' : '')?>>COB</option>
					</select>
				</div>
			</div>

			<!-- Sesudah Pemilihan COB -->
			<div id="form-kelompok-cob" class="form-group" style="<?=($idjenispasien == 2 ? '': 'display: none;')?> margin-bottom: 10px;">
				<label class="col-md-4 control-label">Kelompok Pasien 2</label>
				<div class="col-md-8">
					<select name="idkelompokpasien_cob" id="idkelompokpasienCOB" class="js-select2 form-control form-select" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="0">Pilih Opsi</option>
						<?php foreach (get_all('mpasien_kelompok') as $row){?>
							<option value="<?=$row->id;?>" <?=($row->id == $idkelompokpasienCOB ? 'selected':'')?>><?= $row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div id="form-rekanan-cob" class="form-group" style="<?=($idkelompokpasienCOB == 1 ? '': 'display: none;')?> margin-bottom: 10px;">
				<label class="col-md-4 control-label">Rekanan 2</label>
				<div class="col-md-8">
					<select name="idrekanan_cob" id="idrekananCOB" class="js-select2 form-control form-select" style="width: 100%;" data-placeholder="Pilih Opsi">
						<?php if ($id) { ?>
							<?php foreach (get_all('mrekanan') as $row){?>
								<option value="<?=$row->id;?>" <?=($row->id == $idrekananCOB ? 'selected':'')?>><?= $row->nama;?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>
			<div id="form-bpjskesehatan-cob" class="form-group" style="<?=($idkelompokpasienCOB == 3 ? '': 'display: none;')?> margin-bottom: 10px;">
				<label class="col-md-4 control-label">BPJS Kesehatan 2</label>
				<div class="col-md-8">
					<select name="idtarifbpjskesehatan_cob" id="idtarifbpjskesehatanCOB" class="js-select2 form-control form-select" style="width: 100%;" data-placeholder="Pilih Opsi">
						<?php if ($id) { ?>
							<?php foreach (get_all('mtarif_bpjskesehatan') as $row){?>
								<option value="<?=$row->id;?>" <?=($row->id == $idtarifbpjskesehatanCOB ? 'selected':'')?>><?= $row->nama;?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>
			<div id="form-bpjstenagakerja-cob" class="form-group" style="<?=($idkelompokpasienCOB == 4 ? '': 'display: none;')?> margin-bottom: 10px;">
				<label class="col-md-4 control-label">BPJS TenagaKerja 2</label>
				<div class="col-md-8">
					<select name="idtarifbpjstenagakerja_cob" id="idtarifbpjstenagakerjaCOB" class="js-select2 form-control form-select" style="width: 100%;" data-placeholder="Pilih Opsi">
						<?php if ($id) { ?>
							<?php foreach (get_all('mtarif_bpjstenagakerja') as $row){?>
								<option value="<?=$row->id;?>" <?=($row->id == $idtarifbpjstenagakerjaCOB ? 'selected':'')?>><?= $row->nama;?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>

			<hr>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label">Dokter Perujuk<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<input type="text" class="form-control form-input" placeholder="Dokter Perujuk" id="dokterperujuk" value="{dokterperujuk}" required readonly="true">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-4 control-label">Dokter Penanggung Jawab<span style="color:red;">*</span></label>
				<div class="col-md-8">
					<select name="iddokterpenanggungjawab" id="iddokterpenanggungjawab" class="js-select2 form-control form-select" required style="width: 100%;" data-placeholder="Pilih Opsi">
						<?php foreach (get_all('mdokter') as $row){?>
							<option value="<?=$row->id;?>" <?=($row->id == $iddokterpenanggungjawab ? 'selected':'')?>><?= $row->nama;?></option>
						<?php } ?>
					</select>
				</div>
			</div>
		</div>

		<div class="col-md-4">
			<div id="form-rawatinap" style="margin-bottom: 30px;">
				<div class="form-group" style="margin-bottom: 0px;padding-bottom: 0px;">
					<label class="col-md-12 control-label">
						<h5 align="left"><b>Bed</b></h5>
					</label>
				</div>
				<div class="form-group" style="margin-bottom: 10px;margin-top: 0px;padding-top: 0px;">
					<label class="col-md-12 control-label">
						<hr style="margin-top: 0px;"></label>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label">Ruangan</label>
					<div class="col-md-8">
						<select name="idruangan" id="idruangan" class="js-select2 form-control form-select" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value='0'>Pilih Opsi</option>
							<?php foreach (get_all('mruangan',array('status' => 1,'idtipe'=>1)) as $row){?>
							<option value="<?=$row->id?>" <?=($row->id == $idruangan ? 'selected':'')?>><?=$row->nama?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label">Kelas</label>
					<div class="col-md-8">
						<select name="idkelas" id="idkelas" class="js-select2 form-control form-select" style="width: 100%;" data-placeholder="Pilih Opsi">
							<?php if ($id != '') { ?>
								<?php foreach (get_all('mkelas',array('status' => 1,'')) as $row){?>
									<option value="<?=$row->id?>" <?=($row->id == $idkelas ? 'selected':'')?>><?=$row->nama?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label">Bed</label>
					<div class="col-md-8">
						<select name="idbed" id="idbed" class="js-select2 form-control form-select" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value='0'>Pilih Opsi</option>
							<?if ($id != '') {?>
								<?php foreach ($list_bed as $row){?>
									<option value="<?=$row->id?>" <?=($row->id == $idbed ? 'selected':'')?>><?=$row->nama?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 0px;padding-bottom: 0px;">
				<label class="col-md-12 control-label">
					<h5 align="left"><b>History Kunjungan</b></h5>
				</label>
				<hr style="margin-bottom: 10px;">
				<hr style="margin-bottom: 0px;">
				<div class="col-md-12" id="historykunjungan">
				</div>
			</div>
		</div>
		<div class="form-group"></div>
		<hr>
		<div class="text-right bg-light lter">
			<button class="btn btn-info" id="btnSubmit" <?= ($id ? '':'disabled') ?> type="submit">Simpan</button>
			<a href="{base_url}trawatinap_pendaftaran/index" class="btn btn-default" type="button">Batal</a>
		</div>
		<br>

		<input type="hidden" name="id" id="id" value="{id}">
		<input type="hidden" name="idpoliklinik" id="idpoliklinik" value="{idpoliklinik}">
		<input type="hidden" name="datapasien" id="datapasien" value='{datapasien}'>

		<?php echo form_close() ?>
	</div>
</div>

<!-- Modal Cari Pasien -->
<div class="modal in" id="PasienModal" tabindex="-1" role="dialog" aria-hidden="true" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg" style="width:90%">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Data Pasien</h3>
				</div>
				<div class="block-content">
					<hr>
					<div class="row">
						<div class="form-horizontal">
							<div class="col-md-6">

								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label">Tanggal</label>
									<div class="col-md-8">
										<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
											<input class="form-control" type="text" id="stgl1" name="stgl1" placeholder="From" value="{stgl1}">
											<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
											<input class="form-control" type="text" id="stgl2" name="stgl2" placeholder="To" value="{stgl2}">
										</div>
									</div>
								</div>

								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label">Nama Pasien</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="snamapasien" value="">
									</div>
								</div>

								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label">Asal Pasien</label>
									<div class="col-md-8">
										<select tabindex="1" name="sasalpasien" id="sasalpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="#">Pilih Semua</option>
											<option value="1">Poliklinik</option>
											<option value="2">Instalasi Gawat Darurat</option>
										</select>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label">Dokter Perujuk</label>
									<div class="col-md-8">
										<select tabindex="1" name="sdokter" id="sdokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" required>
											<option value="#">Semua Dokter</option>
											<? foreach($list_dokter as $row){?>
											<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?}?>
										</select>
									</div>
								</div>

								<div class="form-group" style="margin-bottom: 5px;">
									<label class="col-md-4 control-label">Rencana</label>
									<div class="col-md-8">
										<select class="js-select2 form-control" id="srencana" style="width: 100%;" data-placeholder="Pilih Opsi">
											<option value="#">Semua Rencana</option>
											<option value="1">Rawat Inap</option>
											<option value="2">One Day Surgery</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>

					<table width="100%" class="table table-bordered table-striped" id="TableDataPasien">
						<thead>
							<tr>
								<th>Tanggal</th>
								<th>No. Medrec</th>
								<th>Nama Pasien</th>
								<th>Dokter</th>
								<th>Asal Pasien</th>
								<th>Rencana</th>
								<th>Diagnosa</th>
								<th>Operasi</th>
								<th>Waktu Operasi</th>
								<th>Petugas</th>
								<th>Catatan</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- End Modal Cari Pasien -->

<script src="{js_path}pages/base_index_datatable.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}custom/timeline_css.css">
<script type="text/javascript">
	$(document).ready(function() {

		$(".time-datepicker").datetimepicker({
			format: "HH:mm:ss"
		});

		<?php if ($this->uri->segment(2) == 'update') { ?>
			getHistoryKunjungan('{idpasien}');
			$("#containerButtonSearch").hide();

			if ('<?= $idtipe ?>' == '1') {
				$('#form-rawatinap').show();
			} else {
				$('#form-rawatinap').hide();
			}
		<? } ?>

		$("#idtipe").change(function() {
			if ($(this).val() == '1') {
				$('#form-rawatinap').show();
			} else {
				$('#form-rawatinap').hide();
			}

			$("#idpoliklinik").val('');
			$("#datapasien").val('');
			$(".form-input").val('');
			$(".form-select").val('0');
			$(".form-select").selectpicker('refresh');
		});

		$("#idjenispasien").change(function() {
			let idjenispasien = $(this).val();
			if (idjenispasien == '1') {
				$('#form-kelompok-cob').hide();
			} else {
				$('#form-kelompok-cob').show();
			}
		});

		$("#idkelompokpasien").change(function() {
			let idkelompokpasien = $(this).val();
			if (idkelompokpasien == "0") {
				hideFormNonAsuransi();
			} else if (idkelompokpasien == "1") {
				$("#form-rekanan").show();
				$("#form-bpjskesehatan").hide();
				$("#form-bpjstenagakerja").hide();
				$("#form-jenispasien").show();

				getRekanan();
			} else if (idkelompokpasien == "3") {
				$("#form-rekanan").hide();
				$("#form-bpjskesehatan").show();
				$("#form-bpjstenagakerja").hide();
				$("#form-jenispasien").show();

				getBpjsKesehatan();
			} else if (idkelompokpasien == "4") {
				$("#form-rekanan").hide();
				$("#form-bpjstenagakerja").show();
				$("#form-bpjskesehatan").hide();
				$("#form-jenispasien").show();

				getBpjsTenagakerja();
			} else if (idkelompokpasien == "5") {
				hideFormNonAsuransi()
			} else {
				hideFormNonAsuransi()
				$("#form-jenispasien").show();
			}
		});

		$("#idkelompokpasienCOB").change(function() {
			let idkelompokpasien = $(this).val();
			if (idkelompokpasien == "1") {
				$("#form-rekanan-cob").show();
				$("#form-bpjskesehatan-cob").hide();
				$("#form-bpjstenagakerja-cob").hide();

				getRekanan('COB');
			} else if (idkelompokpasien == "3") {
				$("#form-rekanan-cob").hide();
				$("#form-bpjskesehatan-cob").show();
				$("#form-bpjstenagakerja-cob").hide();

				getBpjsKesehatan('COB');
			} else if (idkelompokpasien == "4") {
				$("#form-rekanan-cob").hide();
				$("#form-bpjskesehatan-cob").hide();
				$("#form-bpjstenagakerja-cob").show();

				getBpjsTenagakerja('COB');
			} else {
				$("#form-rekanan-cob").hide();
				$("#form-bpjskesehatan-cob").hide();
				$("#form-bpjstenagakerja-cob").hide();
			}
		});

		$("#idruangan").change(function() {
			getRanapRuangan();
		});

		$("#idkelas").change(function() {
			let idruangan = $("#idruangan").val();
			let idkelas = $("#idkelas").val();
			let id = $("#id").val();

			getRanapBed(idruangan, idkelas, id);
		});

		$("#idbed").change(function() {
			if ($(this).val() == 0) {
				let bed = $("#idbed option:selected").text();
				sweetAlert("Maaf...", `Bed ${bed} tidak bisa dipergunakan`, "error");

				$('#btnSubmit').attr('disabled', 'disabled');
			} else {
				$('#btnSubmit').removeAttr('disabled');
			}
		});

		$("#snamapasien").keyup(function() {
			let stgl1 = $("#stgl1").val();
			let stgl2 = $("#stgl2").val();
			let snamapasien = $("#snamapasien").val();
			let sasalpasien = $("#sasalpasien").val();
			let sdokter = $("#sdokter").val();
			let srencana = $("#srencana").val();

			delay(function() {
				$('#TableDataPasien').DataTable().destroy();
				loadDataPasien(stgl1, stgl2, snamapasien, sasalpasien, sdokter, srencana);
			}, 1000);
		});

		$("#stgl1, #stgl2, #sasalpasien, #sdokter, #srencana").change(function() {
			let stgl1 = $("#stgl1").val();
			let stgl2 = $("#stgl2").val();
			let snamapasien = $("#snamapasien").val();
			let sasalpasien = $("#sasalpasien").val();
			let sdokter = $("#sdokter").val();
			let srencana = $("#srencana").val();
			delay(function() {
				$('#TableDataPasien').DataTable().destroy();
				loadDataPasien(stgl1, stgl2, snamapasien, sasalpasien, sdokter, srencana);
			}, 1000);
		});

		$(document).on("click", ".selectPasien", async function() {
			let idtipe = $('#idtipe').val();
			let idpoliklinik = $(this).closest('tr').find("td:eq(0) a").data('idpoliklinik');

			let dataPasien = await getPasienPoliklinik(idpoliklinik);
			let statusExist = await getStatusExistTransaction(dataPasien.idpasien, idtipe);

			if (!statusExist) {
				$('#nomedrec').val(dataPasien.no_medrec);
				$('#namapasien').val(dataPasien.namapasien);
				$('#alamat').val(dataPasien.alamatpasien);
				$('#tanggallahir').val(dataPasien.tgllahir);
				$('#umur').val(dataPasien.umurtahun + " Th " + dataPasien.umurbulan + " Bln " + dataPasien.umurhari + " Hr");

				$('#rujukan').val((dataPasien.idtipe == 1 ? 'Poliklinik' : 'Instalasi Gawat Darurat (IGD)'));
				$('#idpoliklinik').val(idpoliklinik);
				if (dataPasien.idtipe == '1') {
					$('#form-poliklinik').show();
					$('#poliklinik').val(dataPasien.namapoliklinik);
				} else {
					$('#form-poliklinik').hide();
					$('#poliklinik').val('');
				}

				$('#idtipepasien').select2('val', dataPasien.idtipepasien);
				$('#idkelompokpasien').select2('val', '0');
				$('#dokterperujuk').val(dataPasien.namadokter);

				let datapasien = JSON.stringify({
					idpasien: dataPasien.idpasien,
					title: dataPasien.title,
					namapasien: dataPasien.namapasien,
					no_medrec: dataPasien.no_medrec,
					nohp: dataPasien.nohp,
					telepon: dataPasien.telepon,
					tempat_lahir: dataPasien.tempat_lahir,
					tanggal_lahir: dataPasien.tanggal_lahir,
					alamatpasien: dataPasien.alamatpasien,
					provinsi_id: dataPasien.provinsi_id,
					kabupaten_id: dataPasien.kabupaten_id,
					kecamatan_id: dataPasien.kecamatan_id,
					kelurahan_id: dataPasien.kelurahan_id,
					kodepos: dataPasien.kodepos,
					namapenanggungjawab: dataPasien.namapenanggungjawab,
					hubungan: dataPasien.hubungan,
					umurhari: dataPasien.umurhari,
					umurbulan: dataPasien.umurbulan,
					umurtahun: dataPasien.umurtahun,
					jenis_kelamin: dataPasien.jenis_kelamin
				});

				$('#datapasien').val(datapasien);

				getDokter();
				getHistoryKunjungan(dataPasien.idpasien);

				$('#btnSubmit').removeAttr('disabled');
			} else {
				let tipe = (idtipe == 1 ? 'Rawat Inap' : 'One Day Surgery (ODS)');

				sweetAlert("Maaf...", `Pasien Sudah Terdaftar di ${tipe} dan Belum Checkout`, "error");

				$('#btnSubmit').attr('disabled', 'disabled');
			}
		});

		$("#form-work").submit(function(e) {
			let form = this;

			if ($("#iddokterpenanggungjawab").val() == "0") {
				e.preventDefault();
				sweetAlert("Maaf...", "Dokter Penanggung Jawab Belum Dipilih!", "error");
				return false;
			}

			if ($("#idkelompokpasien").val() == "0") {
				e.preventDefault();
				sweetAlert("Maaf...", "Kelompok Pasien 1 Belum Dipilih!", "error");
				return false;
			}

			if ($("#idjenispasien").val() == "2") {
				if ($("#idkelompokpasienCOB").val() == "0") {
					e.preventDefault();
					sweetAlert("Maaf...", "Kelompok Pasien 2 Belum Dipilih!", "error");
					return false;
				}
			}

			if ($("#idtipe").val() == "1") {
				if ($("#idruangan").val() == "") {
					e.preventDefault();
					sweetAlert("Maaf...", "Ruangan Belum Dipilih!", "error");
					return false;
				}
				if ($("#idkelas").val() == "") {
					e.preventDefault();
					sweetAlert("Maaf...", "Kelas Belum Dipilih!", "error");
					return false;
				}
				if ($("#idbed").val() == "") {
					e.preventDefault();
					sweetAlert("Maaf...", "Bed Belum Dipilih!", "error");
					return false;
				}
			}

			swal({
				title: "Berhasil!",
				text: "Proses penyimpanan data.",
				type: "success",
				timer: 1500,
				showConfirmButton: false
			});
		});
	});

	let delay = (function() {
		let timer = 0;
		return function(callback, ms) {
			clearTimeout(timer);
			timer = setTimeout(callback, ms);
		};
	})();

	function loadDataPasien(stgl1, stgl2, snamapasien, sasalpasien, sdokter, srencana) {
		let tablePasien = $('#TableDataPasien').DataTable({
			"pageLength": 5,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: '{base_url}Trawatinap_pendaftaran/getListPasien',
				type: "POST",
				dataType: 'json',
				data: {
					stgl1: stgl1,
					stgl2: stgl2,
					snamapasien: snamapasien,
					sasalpasien: sasalpasien,
					sdokter: sdokter,
					srencana: srencana,
				}
			},
			"columnDefs": [{
				"targets": [0],
				"orderable": true
			}]
		});
	}

	function hideFormNonAsuransi() {
		$("#form-rekanan").hide();
		$("#form-bpjskesehatan").hide();
		$("#form-bpjstenagakerja").hide();
		$("#form-jenispasien").hide();
	}

	function getDokter() {
		$.ajax({
			url: "{base_url}trawatinap_pendaftaran/getAllDokter",
			dataType: "json",
			success: function(data) {
				$("#iddokterpenanggungjawab").empty();
				$("#iddokterpenanggungjawab").append("<option value='0'>Pilih Opsi</option>");
				for (let i = 0; i < data.length; i++) {
					$("#iddokterpenanggungjawab").append("<option value='" + data[i].iddokter + "'>" + data[i].nama + "</option>");
				}
				$("#iddokterpenanggungjawab").selectpicker('refresh');
			}
		});
	}

	function getRekanan(status = '') {
		$.ajax({
			url: '{base_url}tpoliklinik_pendaftaran/getRekanan',
			dataType: "json",
			success: function(data) {
				if (status == 'COB') {
					$("#idrekananCOB").empty();
					$("#idrekananCOB").append("<option value=''>Pilih Opsi</option>");
					data.map((item) => {
						let selected = ($("#id").val() != '' && item.id == '{idrekanan2}');
						$("#idrekananCOB").append(`<option value="${item.id}" ${selected ? 'selected':''}>${item.nama}</option>`);
					});
					$("#idrekananCOB").selectpicker("refresh");
				} else {
					$("#idrekanan").empty();
					$("#idrekanan").append("<option value=''>Pilih Opsi</option>");
					data.map((item) => {
						let selected = ($("#id").val() != '' && item.id == '{idrekanan}');
						$("#idrekanan").append(`<option value="${item.id}" ${selected ? 'selected':''}>${item.nama}</option>`);
					});
					$("#idrekanan").selectpicker("refresh");
				}
			}
		});
	}

	function getBpjsKesehatan(status = '') {
		$.ajax({
			url: '{base_url}trawatinap_pendaftaran/getBpjsKesehatan',
			dataType: "json",
			success: function(data) {
				if (status == 'COB') {
					$("#idtarifbpjskesehatanCOB").empty();
					$("#idtarifbpjskesehatanCOB").append("<option value=''>Pilih Opsi</option>");

					let selected = ($("#id").val() != '' && item.id == '{idtarifbpjskesehatan2}');
					data.bpjskesehatan.map((item) => {
						$("#idtarifbpjskesehatanCOB").append(`<option value="${item.id}" ${selected ? 'selected' : ''}>${item.kode}</option>`);
					});
					$("#idtarifbpjskesehatanCOB").selectpicker("refresh");
				} else {
					$("#idtarifbpjskesehatan").empty();
					$("#idtarifbpjskesehatan").append("<option value=''>Pilih Opsi</option>");

					let selected = ($("#id").val() != '' && item.id == '{idtarifbpjskesehatan}');
					data.bpjskesehatan.map((item) => {
						$("#idtarifbpjskesehatan").append(`<option value="${item.id}" ${selected ? 'selected' : ''}>${item.kode}</option>`);
					});
					$("#idtarifbpjskesehatan").selectpicker("refresh");
				}
			}
		});
	}

	function getBpjsTenagakerja(status = '') {
		$.ajax({
			url: '{base_url}trawatinap_pendaftaran/getBpjsTenagakerja',
			dataType: "json",
			success: function(data) {
				if (status == 'COB') {
					$("#idtarifbpjstenagakerjaCOB").empty();
					$("#idtarifbpjstenagakerjaCOB").append("<option value=''>Pilih Opsi</option>");

					let selected = ($("#id").val() != '' && item.id == '{idtarifbpjstenagakerja2}');
					data.bpjstenaga.map((item) => {
						$("#idtarifbpjstenagakerjaCOB").append(`<option value="${item.id}" ${selected ? 'selected' : ''}>${item.id}</option>`);
					});
					$("#idtarifbpjstenagakerjaCOB").selectpicker("refresh");
				} else {
					$("#idtarifbpjstenagakerja").empty();
					$("#idtarifbpjstenagakerja").append("<option value=''>Pilih Opsi</option>");

					let selected = ($("#id").val() != '' && item.id == '{idtarifbpjstenagakerja}');
					data.bpjstenaga.map((item) => {
						$("#idtarifbpjstenagakerja").append(`<option value="${item.id}" ${selected ? 'selected' : ''}>${item.id}</option>`);
					});
					$("#idtarifbpjstenagakerja").selectpicker("refresh");
				}
			}
		});
	}

	function getRanapRuangan() {
		$("#idbed").empty();
		$("#idbed").append("<option value=''>Pilih Opsi</option>");

		$.ajax({
			url: "{base_url}trawatinap_pendaftaran/getKelas",
			dataType: "json",
			success: function(data) {
				$("#idkelas").empty();
				$("#idkelas").append("<option value=''>Pilih Opsi</option>");
				data.map((item) => {
					$("#idkelas").append(`<option value="${item.id}">${item.nama}</option>`);
				});
				$("#idkelas").selectpicker('refresh');
			}
		});
	}

	function getRanapBed(idruangan, idkelas, id) {
		let idRuangan = idruangan;
		let idKelas = idkelas;
		let idTransaksi = id;

		$.ajax({
			url: `{base_url}trawatinap_pendaftaran/getBed/${idRuangan}/${idKelas}/${idTransaksi}`,
			dataType: "json",
			success: function(data) {
				$("#idbed").empty();
				$("#idbed").append("<option value=''>Pilih Opsi</option>");
				data.map((item) => {
					$("#idbed").append(`<option value="${item.id}">${item.nama}</option>`);
				})
				$("#idbed").selectpicker('refresh');
			}
		});
	}

	function getHistoryKunjungan(idpasien) {
		let idPasien = idpasien;

		$.ajax({
			url: `{base_url}trawatinap_pendaftaran/getHistory/${idPasien}`,
			dataType: 'json',
			success: function(data) {
				$('#historykunjungan').empty();

				let history = data.map((item) => {
					return `<li>
						<i class="si si-pencil text-info"></i>
						<div class="font-w600">${item.tipe} - ${item.poli}</div>
						<div><a href="javascript:void(0)">Dokter - ${item.namadokter}</a></div>
						<div><small class="text-muted">Tanggal - ${item.tanggaldaftar}</small></div>
					</li>`
				});

				let html = `<div class="block">
					<div class="block-content">
						<ul class="list list-activity push">
							${history}
						</ul>
					</div>
				</div>`;

				$('#historykunjungan').html(html);
			}
		});
	}

	async function getPasienPoliklinik(idpoliklinik) {
		let idPoliklinik = idpoliklinik;

		return await $.ajax({
			url: `{base_url}trawatinap_pendaftaran/getPasienPoliklinik/${idPoliklinik}`,
			dataType: "json",
			success: function (data) {
				return data;
			}
		});
	}

	async function getStatusExistTransaction(idpasien, idtipe) {
		let idPasien = idpasien;
		let idTipe = idtipe;

		return await $.ajax({
			url: `{base_url}trawatinap_pendaftaran/getStatusExistTransaction/${idPasien}/${idTipe}`,
			success: function(status) {
				return status;
			}
		});
	}
</script>
