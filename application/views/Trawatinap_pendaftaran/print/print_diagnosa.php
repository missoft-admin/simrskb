<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>
		Surat Keterangan Diagnosa
	</title>
	<style>
		@page {
				size: 210mm 297mm;
				margin-top: 2em;
				margin-left: 2.8em;
				margin-bottom: 0.5em;
			}
		*
			table {
				font-size: 16px;
				font-family: "Times New Roman", Times, serif;
				border-collapse: collapse !important;
				width: 100% !important;
				border: 0px solid black;
			}
			td {
				padding: 1px;
				border: 0px solid black;
			}
			.header {
				font-size: 20px;
				font-family: "Times New Roman", Times, serif;
				font-weight: bold;
				margin-bottom: 20px;
			}

			.content td {
				padding: 0px;
			}
			.border-full {
				border: 1px solid #000 !important;
			}
			.border-bottom {
				border-bottom: 4px solid #000 !important;
			}
			.border-thick-top {
				border-top: 2px solid #000 !important;
			}
			.border-thick-bottom {
				border-bottom: 2px solid #000 !important;
			}
			.text-center {
				text-align: center !important;
			}
			.text-right {
				text-align: right !important;
			}
			.bold {
				font-weight: bold;
			}

	</style>
</head>

<body>
	<table  style="width:200mm;">
		<tr>
			<td rowspan="3" width="15%" align="center"><img style="width:60px;" src="assets/upload/logo/logoreport.jpg"></td>
			<td class="text-center header"  width="75%">&nbsp;&nbsp;RSKB HALMAHERA SIAGA</td>
			<td class="text-center header"  width="10%"></td>

		</tr>
		<tr>
			<td class="text-center">&nbsp;&nbsp;Jl. LLRE. Martadinata No.28 Bandung &nbsp;&nbsp;T. (022) 4206717 F. (022) 4216436</td>
			<td class="text-center"></td>
		</tr>
		<tr>
			<td  class="text-center">&nbsp;&nbsp;Bandung -  40115</td>
			<td  class="text-center"></td>
		</tr>
	</table>
	<table  style="width:200mm;">
		<tr>
			<td  class="border-bottom " width="100%"></td>
		</tr>
	</table>
	<table  style="width:200mm;">
		<tr>
			<td width="100%"></td>
		</tr>
	</table>
	<br>
	<table  style="width:200mm;">
		<tr>
			<td width="5%"></td>
			<td class="text-left bold"  colspan="4"><i>Resume Medis</i></td>
			<td width="5%"></td>
		</tr>
		<tr>
			<td width="5%"></td>
			<td class="text-center bold" colspan="4"><i><u>SURAT KETERANGAN DIAGNOSA</u></i></td>
			<td width="5%"></td>
		</tr>
		<tr>
			<td width="5%"></td>
			<td class="text-center bold" colspan="4"></td>
			<td width="5%"></td>
		</tr>

		<tr>
			<td width="5%"></td>
			<td class="text-left" colspan="4"><br>Yang Bertanda tangan dibawah ini, <?=$namadokter?>., dokter di RS HALMAHERA SIAGA Bandung, Menerangkan Bahwa :</td>
			<td width="5%"></td>
		</tr>
		<tr>
			<td width="5%"></td>
			<td width="5%"></td>
			<td class="text-left" width="15%">Nama</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%"><?=$namapasien?></td>
			<td width="5%"></td>
		</tr>
		<tr>
			<td width="5%"></td>
			<td width="5%"></td>
			<td class="text-left" width="15%">Umur</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%"><?=$umurtahun?> Th</td>
			<td width="5%"></td>
		</tr>
		<tr>
			<td width="5%"></td>
			<td width="5%"></td>
			<td class="text-left" width="15%">Jenis Kelamin</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%"><?=($jeniskelamin=='L')?'Laki-Laki / <strike>Perempuan</strike>':'<strike>Laki-Laki</strike> / Perempuan'?></td>
			<td width="5%"></td>
		</tr>
		<tr>
			<td width="5%"></td>
			<td width="5%"></td>
			<td class="text-left" width="15%">Pekerjaan</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%"><?=$pekerjaan?></td>
			<td width="5%"></td>
		</tr>
		<tr>
			<td width="5%"></td>
			<td width="5%"></td>
			<td class="text-left" width="15%">Alamat</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%"><?=$alamat ?>  <?=($desa!='')?' DS/KEL. '.$desa.',':'' ?> <?=($kec!='')?' KEC. '.$kec.', ':'' ?> <?=($kab!='')?' - '.$kab:'' ?></td>
			<td width="5%"></td>
		</tr>
		<tr>
			<td width="5%"></td>
			<td width="5%"></td>
			<td class="text-left" width="15%">No. Rekam Medis</td>
			<td width="1%" class="text-center">:</td>
			<td width="64%"><?=$nomedrec?></td>
			<td width="5%"></td>
		</tr>
		<tr>
			<td width="5%"></td>
			<td class="text-left" colspan="4"><br> Telah Mendapatkan <?=($idtipe=='1'? 'Pelayanan Rawat Inap / <strike>Rawat Jalan</strike>':'<strike>Pelayanan Rawat Inap</strike> / Rawat Jalan')?> , dari Tanggal  <?=HumanDateShort($tanggaldaftar)?> sampai <?=HumanDateShort($tanggalcheckout)?>, dengan <br><br></td>
			<td width="5%"></td>
		</tr>
		<tr>
			<td width="5%"></td>
			<td class="text-left" width="20%" colspan="2">Riwayat Penyakit<br><br><br></td>
			<td width="1%" class="text-center">:</td>
			<td width="64%"></td>
			<td width="5%"></td>
		</tr>
		<tr>
			<td width="5%"></td>
			<td class="text-left" width="20%" colspan="2">Pemeriksaan Penunjang<br><br><br></td>
			<td width="1%" class="text-center">:</td>
			<td width="64%"></td>
			<td width="5%"></td>
		</tr>
		<tr>
			<td width="5%"></td>
			<td class="text-left" width="20%" colspan="2">Diagnosa Akhir<br><br><br></td>
			<td width="1%" class="text-center">:</td>
			<td width="64%"></td>
			<td width="5%"></td>
		</tr>
		<tr>
			<td width="5%"></td>
			<td class="text-left" width="20%" colspan="2">Terapi / Tindakan<br><br><br></td>
			<td width="1%" class="text-center">:</td>
			<td width="64%"></td>
			<td width="5%"></td>
		</tr>
		<tr>
			<td width="5%"></td>
			<td class="text-left" width="20%" colspan="2">Keadaan Sekarang<br><br><br></td>
			<td width="1%" class="text-center">:</td>
			<td width="64%"></td>
			<td width="5%"></td>
		</tr>
		<tr>
			<td width="5%"></td>
			<td class="text-left" colspan="4"><br>Demikian surat keterangan ini dibuat untuk dipergunakan sebagaimana mestinya. <br><br><br></td>
			<td width="5%"></td>
		</tr>
		<tr>
			<td width="5%"></td>
			<td class="text-left" width="20%" colspan="3">Bandung, <?= date("d-m-Y")?><br><br><br><br><br><br></td>
			<td width="64%"></td>
			<td width="5%"></td>
		</tr>
		<tr>
			<td width="5%"></td>
			<td class="text-left" width="20%" colspan="3">(<?=$namadokter?>)</td>
			<td width="64%"></td>
			<td width="5%"></td>
		</tr>
	</table>



</body>

</html>
