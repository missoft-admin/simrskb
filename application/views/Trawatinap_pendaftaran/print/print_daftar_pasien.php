<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>
		Informasi Pasien Rawat Inap Yang Belum Checkout
	</title>
	<style>
		@media print {
			table {
				font-size: 9px !important;
				border-collapse: collapse !important;
				width: 100% !important;
			}
			td {
				padding: 5px;
			}
			.content td {
				padding: 0px;
			}
			.border-full {
				border: 1px solid #000 !important;
			}
			.border-bottom {
				border-bottom: 1px solid #000 !important;
			}
			.border-thick-top {
				border-top: 2px solid #000 !important;
			}
			.border-thick-bottom {
				border-bottom: 2px solid #000 !important;
			}
			.text-center {
				text-align: center !important;
			}
			.text-right {
				text-align: right !important;
			}
		}

		@media screen {
			table {
				font-size: 9px !important;
				border-collapse: collapse !important;
				width: 100% !important;
			}
			td {
				padding: 5px;
			}
			.content td {
				padding: 0px;
			}
			.border-full {
				border: 1px solid #000 !important;
			}
			.border-bottom {
				border-bottom: 1px solid #000 !important;
			}
			.border-thick-top {
				border-top: 2px solid #000 !important;
			}
			.border-thick-bottom {
				border-bottom: 2px solid #000 !important;
			}
			.text-center {
				text-align: center !important;
			}
			.text-right {
				text-align: right !important;
			}
		}
	</style>
</head>

<body>
	<table class="content">
		<tr>
			<td rowspan="3" width="10px"><img style="width:40px;" src="assets/upload/logo/logoreport.jpg"></td>
			<td>&nbsp;&nbsp;RSKB HALMAHERA SIAGA</td>
		</tr>
		<tr>
			<td>&nbsp;&nbsp;Jl. LLRE. Martadinata No.28 Bandung</td>
		</tr>
		<tr>
			<td>&nbsp;&nbsp;T. (022) 4206717 F. (022) 4216436</td>
		</tr>
		<tr>
			<td>&nbsp;&nbsp;</td>
		</tr>
	</table>
	<table>
		<tr>
			<td colspan="9" class="text-center"><b><u>INFORMASI PASIEN RAWAT INAP YANG BELUM CHECKOUT</u></b></td>
		</tr>
		<tr>
			<td colspan="9" class="text-center">&nbsp;</td>
		</tr>
		<tr>
			<th class="border-full text-center">No.</th>
			<th class="border-full text-center">No. Medrec</th>
			<th class="border-full text-center">Tanggal Masuk</th>
			<th class="border-full text-center">Ruang Perawatan</th>
			<th class="border-full text-center">Nama Pasien</th>
			<th class="border-full text-center">Umur</th>
			<th class="border-full text-center">Alamat</th>
			<th class="border-full text-center">Dokter Yang Merawat</th>
			<th class="border-full text-center">Kelompok Pasien</th>
			<th class="border-full text-center">Rekanan</th>
		</tr>

		<?php $no = 0; ?>
		<?php foreach ($report as $row){ ?>
		<?php $no = $no+1; ?>
		<tr>
			<td class="border-full text-center">
				<?php echo $no; ?>
			</td>
			<td class="border-full text-center">
				<?php echo $row->nomedrec; ?>
			</td>
			<td class="border-full text-center">
				<?php echo $row->tanggaldaftar; ?>
			</td>
			<td class="border-full text-center">
				Kelas <?php echo $row->namakelas; ?>
			</td>
			<td class="border-full">
				<?php echo $row->namapasien; ?>
			</td>
			<td class="border-full text-center">
				<?php echo $row->umurtahun.' Th '.$row->umurbulan.' Bln'; ?>
			</td>
			<td class="border-full">
				<?php echo $row->alamat; ?>
			</td>
			<td class="border-full">
				<?php echo $row->namadokter; ?>
			</td>
			<td class="border-full">
				<?php echo $row->namakelompok; ?>
			</td>
			<td class="border-full">
				<?php if ($row->idkelompokpasien == 1) { ?>
					<?php echo $row->namarekanan; ?>
				<?php } else { ?>
					-
				<?php } ?>
			</td>
		</tr>
		<?php } ?>
	</table>
</body>
<script type="text/javascript">
	try {
		this.print();
	} catch (e) {
		window.onload = window.print;
	}
</script>

</html>
