<?php echo ErrorSuccess($this->session) ?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}
?>
<?php if (UserAccesForm($user_acces_form,array('359'))){ ?>
<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('360'))){ ?>
		<ul class="block-options">
			<li>
				<a href="{base_url}trawatinap_pendaftaran/create" class="btn"><i class="fa fa-plus"></i> Tambah</a>
			</li>
		</ul>
		<?}?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<hr style="margin-top:0px">
		<div class="row">
			<?php echo form_open('trawatinap_pendaftaran/filter', 'class="form-horizontal" id="form-work"') ?>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">No. Medrec</label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="nomedrec" value="{nomedrec}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Nama Pasien</label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="namapasien" value="{namapasien}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Tipe</label>
					<div class="col-md-8">
						<select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<?php if (UserAccesForm($user_acces_form,array('1334'))){ ?>
								<option value="#" <?=($idtipe == '' || $idtipe=='#' ? 'selected' : '')?> selected>Pilih Semua</option>
							<?}?>
							<?php if (UserAccesForm($user_acces_form,array('361'))){ ?>
								<option value="1" <?=($idtipe == 1 ? 'selected' : '')?>>Rawat Inap</option>
							<?}?>
							<?php if (UserAccesForm($user_acces_form,array('362'))){ ?>
								<option value="2" <?=($idtipe == 2 ? 'selected' : '')?>>One Day Surgery (ODS)</option>
							<?}?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 10px;">
					<label class="col-md-4 control-label" for="">Status</label>
					<div class="col-md-8">
						<select name="idstatus" id="idstatus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="#" selected>Pilih Semua</option>
								<option value="10" <?=($idstatus == '10' ? 'selected' : '')?>>[Rawat Inap] - Semua Status</option>
								<option value="11" <?=($idstatus == '11' ? 'selected' : '')?>>[Rawat Inap] - Dirawat</option>
								<option value="12" <?=($idstatus == '12' ? 'selected' : '')?>>[Rawat Inap] - Pembayaran</option>
								<option value="13" <?=($idstatus == '13' ? 'selected' : '')?>>[Rawat Inap] - Pulang</option>
								<option value="14" <?=($idstatus == '14' ? 'selected' : '')?>>[Rawat Inap] - Dibatalkan</option>
								<option value="20" <?=($idstatus == '20' ? 'selected' : '')?>>[ODS] - Semua Status</option>
								<option value="21" <?=($idstatus == '21' ? 'selected' : '')?>>[ODS] - Menunggu Tindakan</option>
								<option value="22" <?=($idstatus == '22' ? 'selected' : '')?>>[ODS] - Telah Ditindak</option>
								<option value="23" <?=($idstatus == '23' ? 'selected' : '')?>>[ODS] - Dibatalkan</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="iddokter">Dokter</label>
					<div class="col-md-8">
						<select tabindex="1" name="iddokter" id="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
							<option value="#" selected>Pilih Semua</option>
							<?foreach ($list_dokter as $row) {?>
								<option value="<?=$row->id?>" <?=($iddokter == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
							<?}?>
						</select>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Kelompok Pasien</label>
					<div class="col-md-8">
						<select id="idkelompokpasien" name="idkelompokpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." >
							<option value="#" selected>Semua Kelompok Pasien</option>
							<?php foreach ($list_kelompok_pasien as $row) {?>
								<option value="<?=$row->id?>" <?=($idkelompokpasien == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
							<?php }?>
						</select>
					</div>
				</div>
				<div id="containerPerusahaanAsuransi" class="form-group" style="margin-bottom: 5px;display:none">
					<label class="col-md-4 control-label" for="">Perusahaan Asuransi</label>
					<div class="col-md-8">
						<select name="idrekanan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." >
							<option value="#" selected>Semua Perusahaan</option>
							<?php foreach ($list_rekanan as $row) {?>
								<option value="<?=$row->id?>" <?=($idrekanan == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
							<?php }?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="tanggaldaftar">Tanggal Daftar</label>
					<div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
							<input class="form-control" type="text" id="tanggaldaftar" name="tanggaldaftar" placeholder="From" value="{tanggaldaftar}">
							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
							<input class="form-control" type="text" id="tanggaldaftar2" name="tanggaldaftar2" placeholder="To" value="{tanggaldaftar2}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="idkelas">Kelas</label>
					<div class="col-md-8">
						<select tabindex="1" name="idkelas" id="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
							<option value="#" selected>Pilih Semua</option>
							<?foreach ($list_kelas as $row) {?>
								<option value="<?=$row->id?>" <?=($idkelas == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
							<?}?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="idbed">Bed</label>
					<div class="col-md-8">
						<select tabindex="1" name="idbed" id="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
							<option value="#" selected>Pilih Semua</option>
							<?foreach ($list_bed as $row) {?>
								<option value="<?=$row->id?>" <?=($idbed == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
							<?}?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>

		<hr style="margin-top:10px">
		<?php if (UserAccesForm($user_acces_form,array('367'))){ ?>
  		<div style="text-align:right">
  			<a href="{base_url}trawatinap_pendaftaran/printDaftarPasien" target="_blank" class="btn btn-primary"><i class="fa fa-print"></i> Print Daftar Pasien</a>
  		</div>
		<?}?>
		<hr style="margin-top:10px">

		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No.</th>
					<th>Tanggal</th>
					<th>No. Pendaftaran</th>
					<th>No. Medrec</th>
					<th>Nama Pasien</th>
					<th>Alamat</th>
					<th>Kelas</th>
					<th>Bed</th>
					<th>Dokter</th>
					<th>Tipe</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<? } ?>

<div class="modal in" id="AlasanBatalModal" role="dialog" style="text-transform:uppercase;">
	<div class="modal-dialog modal-xs modal-dialog-popout">
		<div class="modal-content">
			<input type="hidden" name="idpendaftaran" id="idpendaftaran" value="">
			<div class="block block-themed remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Alasan Batal</h3>
				</div>
			</div>
			<div class="col-xs-12" style="margin-top: 10px; margin-bottom: 10px;">
				<select name="idalasan" id="idalasan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
					<option value="#" selected>- Alasan -</option>
					<?php foreach ($list_alasan as $row) {?>
						<option value="<?=$row->id?>" <?=($idalasan == $row->id ? 'selected' : '')?>><?=$row->keterangan?></option>
					<?php }?>
				</select>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
				<button class="btn btn-sm btn-success" id="bntSubmitAlasanBatal" type="submit" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
  jQuery(function() {
    BaseTableDatatables.init();
    $('#datatable-simrs').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
          url: '{site_url}trawatinap_pendaftaran/getIndex/' + '<?=$this->uri->segment(2)?>',
          type: "POST",
          dataType: 'json'
        },
        "columns": [
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
          { "class": "details-control", "orderable": false, "defaultContent": "" },
        ],
        "columnDefs": [
          { "width": "5%", "targets": 0, "orderable": true },
          { "width": "10%", "targets": 1, "orderable": true },
          { "width": "10%", "targets": 2, "orderable": true },
          { "width": "10%", "targets": 3, "orderable": true },
          { "width": "10%", "targets": 4, "orderable": true },
          { "width": "10%", "targets": 5, "orderable": true },
          { "width": "5%", "targets": 6, "orderable": true },
          { "width": "5%", "targets": 7, "orderable": true },
          { "width": "10%", "targets": 8, "orderable": true },
          { "width": "5%", "targets": 9, "orderable": true },
          { "width": "5%", "targets": 10, "orderable": true },
          { "width": "10%", "targets": 11, "orderable": true }
        ]
    });
  });

	$(document).ready(function() {
		if ($("#idkelompokpasien").val() == '1'){
			$("#containerPerusahaanAsuransi").show();
		}else{
			$("#containerPerusahaanAsuransi").hide();
		}



  	$("#idkelompokpasien").change(function() {
  		if ($("#idkelompokpasien").val()=='1'){
  			$("#containerPerusahaanAsuransi").show();
  		}else{
  			$("#containerPerusahaanAsuransi").hide();
  		}
  	});

  	$("#idkelas").change(function() {
  		let idkelas = $("#idkelas").val();

  		$.ajax({
  			url: '{site_url}Trawatinap_pendaftaran/getIndexBed',
  			method: "POST",
  			dataType: "json",
  			data: {
  				"idkelas": idkelas
  			},
  			success: function(data) {
  				$("#idbed").empty();
  				$("#idbed").append("<option value=''>Pilih Opsi</option>");
  				data.map((item) => {
            $("#idbed").append(`<option value="${item.id}">${item.nama}</option>`);
          });
  				$("#idbed").selectpicker("refresh");
  			},
  			error: function(data) {
  				alert(data);
  			}
  		});
  	});

  	$("#idtipe").change(function() {
  		$('#idstatus').find('option').remove().val('').trigger("liszt:updated");
  		let content = '';
  		if ($(this).val()=='#'){
  			content = `<option value="#" selected>Pilih Semua</option>
        <option value="10">[Rawat Inap] - Semua Status</option>
        <option value="11">[Rawat Inap] - Dirawat</option>
        <option value="12">[Rawat Inap] - Pembayaran</option>
  			<option value="13">[Rawat Inap] - Pulang</option>
  			<option value="14">[Rawat Inap] - Dibatalkan</option>
  			<option value="20">[ODS] - Semua Status</option>
  			<option value="21">[ODS] - Menunggu Tindakan</option>
  			<option value="22">[ODS] - Telah Ditindak</option>
  			<option value="23">[ODS] - Dibatalkan</option>`;
  		}else if ($(this).val()=='1'){
  			content = `<option value="10">[Rawat Inap] - Semua Status</option>
  			<option value="11">[Rawat Inap] - Dirawat</option>
  			<option value="12">[Rawat Inap] - Pembayaran</option>
  			<option value="13">[Rawat Inap] - Pulang</option>
  			<option value="14">[Rawat Inap] - Dibatalkan</option>`;
  		}else if ($(this).val()=='2'){
  			content = `<option value="20">[ODS] - Semua Status</option>
  			<option value="21">[ODS] - Menunggu Tindakan</option>
  			<option value="22">[ODS] - Telah Ditindak</option>
  			<option value="23">[ODS] - Dibatalkan</option>`;
  		}
  		$('#idstatus').append(content);
  	});

  	$('#bntSubmitAlasanBatal').click(function() {
  		let idalasan = $("#idalasan").val();
  		let idpendaftaran = $("#idpendaftaran").val();

  		$.ajax({
  			url: `{base_url}trawatinap_pendaftaran/delete/${idpendaftaran}`,
  			method: "POST",
  			data: {
  				idalasan: idalasan,
  				idpendaftaran: idpendaftaran
  			},
  			success: function(data) {
  				location.reload();
  			}
  		});
  	});
	});

	function deleteTranscation(id) {
	  $('#idpendaftaran').val(id);
	  $("#AlasanBatalModal").modal();
	}
</script>
