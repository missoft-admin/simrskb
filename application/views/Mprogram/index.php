<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('129'))){ ?>
<div class="block">
	<div class="block-header">
	<?php if (UserAccesForm($user_acces_form,array('130'))){ ?>
		<ul class="block-options">
       
            <a href="{base_url}mprogram/create" type="button" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
       
			</ul>
	<?}?>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:20px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Prespektif</label>
                    <div class="col-md-8">
                        <select id="idprespektif" name="idprespektif" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#">- Semua Prespektif -</option>
							<?foreach(get_all('mprespektif_rka',array('status'=>1),'urutan') as $row){?>
								<option value="<?=$row->id?>"><?=$row->nama?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>	
                                
            </div>
			<div class="col-md-6">
				
                <div class="form-group" style="margin-bottom: 5px;">
                    <div class="col-md-3">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>#</th>
					<th>Kode</th>
					<th>Urutan</th>
					<th>Nama Program</th>
					<th>Prespektif</th>
					<th>Deskripsi</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		</div>
	</div>
</div>
<?}?>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		// alert('sini');
		load_index();
		
	})	
	$("#btn_filter").click(function() {
		// alert('ID');
		table.destroy();
		load_index();
	});
	function load_index(){
		var idprespektif=$("#idprespektif").val();
		// alert(idprespektif);
		table=$('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mprogram/getIndex',
					type: "POST",
					dataType: 'json',
					data : {
						idprespektif:idprespektif,
						
					   }
				},
				"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": true },
					{ "width": "5%", "targets": 1, "orderable": true },
					{ "width": "5%", "targets": 2, "orderable": true },
					{ "width": "20%", "targets": 3, "orderable": true },
					{ "width": "15%", "targets": 4, "orderable": true },
					{ "width": "20%", "targets": 5, "orderable": true },
					{ "width": "15%", "targets": 6, "orderable": false }
				]
			});
	}
</script>
