<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mprogram" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mprogram/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="lokasi">Kode</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="kode" placeholder="Kode" required="" name="kode" value="{kode}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama Program</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama Program" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Prespektif</label>
				<div class="col-md-7">
					<select id="idprespektif" name="idprespektif" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="">- Pilih Prespektif -</option>
						<?foreach(get_all('mprespektif_rka',array('status'=>1),'urutan') as $row){?>
							<option value="<?=$row->id?>" <?=$idprespektif==$row->id?'selected':''?>><?=$row->nama?></option>									
						<?}?>
						
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="lokasi">Urutan Tampil</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="urutan" placeholder="Urutan Tampil" required="" name="urutan" value="{urutan}">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="deskripsi">Deskripsi</label>
				<div class="col-md-7">
					<textarea class="form-control js-summernote" required="" name="deskripsi" id="deskripsi"><?=$deskripsi?></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mprogram" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function(){

	
	})	
	function validate_final(){
		if ($('#idprespektif').val()=='#'){
			sweetAlert("Maaf...", "Tentukan Prespektif", "error");
			return false;

		}
	}
</script>