<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block">
	<ul class="nav nav-tabs" data-toggle="tabs">
		<?php if (UserAccesForm($user_acces_form,array('1730'))){ ?>
		<li class="<?=($tab=='1'?'active':'')?>">
			<a href="#tab_info" onclick="load_spo2()"><i class="fa fa-bar-chart-o"></i> {title}</a>
		</li>
		<?}?>
		
		
	</ul>
	<div class="block-content tab-content">
		<input type="hidden" id="tab" name="tab" value="{tab}">
		<?php if (UserAccesForm($user_acces_form,array('1730'))){ ?>
		<div class="tab-pane fade fade-left <?=($tab=='1'?'active in':'')?>" id="tab_info">
			
			<?php echo form_open('#','class="form-horizontal" id="form-work" target="_blank"') ?>
			<div class="row">
				
				<div class="col-md-12">
						<div class="form-group">
							<label class="col-xs-12" for="satuan_spo2">Satuan Spo2</label>
							<div class="col-xs-4">
								<div class="input-group">
									<input class="form-control" type="text" id="satuan_spo2" name="satuan_spo2" value="{satuan_spo2}" placeholder="Satuan Spo2">
									<span class="input-group-btn">
										<button class="btn btn-success" onclick="update_satuan()" type="button"><i class="fa fa-save"></i>  Update</button>
									</span>
								</div>
								
							</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group" style="margin-bottom: 15px;">
							<div class="table-responsive">
								<table class="table table-bordered table-striped" id="index_spo2">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="40%">Range Spo2</th>
											<th width="25%">Kategori</th>
											<th width="15%">Warna</th>
											<th width="15%">Action</th>										   
										</tr>
										<tr>
											<th>#<input class="form-control" type="hidden" id="spo2_id" name="spo2_id" value="" placeholder="id"></th>
											<th>
												<div class="input-group" style="width:100%">
													<input class="form-control decimal" type="text" id="spo2_1" name="spo2_1" placeholder="<?=$satuan_spo2?>" value="" >
													<span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
													
													<input class="form-control decimal" type="text" id="spo2_2" name="spo2_2" placeholder="<?=$satuan_spo2?>" value="" >
													
													
												</div>
											</th>
											<th>
												<input class="form-control" type="text" style="width:100%" id="kategori_spo2" name="kategori_spo2" placeholder="Kategori" value="" >
											</th>
											<th>
												
												<div class="js-colorpicker input-group colorpicker-element">
													<input class="form-control" type="text" id="warna" name="warna" value="#0000">
													<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
												</div>
											
											</th>
											<th>
												<div class="btn-group">
												<?php if (UserAccesForm($user_acces_form,array('1731'))){ ?>
												<button class="btn btn-primary btn-sm" type="button" id="btn_tambah_spo2" name="btn_tambah_spo2"><i class="fa fa-save"></i> Simpan</button>
												<?}?>
												<button class="btn btn-warning btn-sm" type="button" id="btn_refresh" name="btn_refresh" onclick="clear_spo2()"><i class="fa fa-refresh"></i> Clear</button>
												</div>
											</th>										   
										</tr>
										
									</thead>
									<tbody></tbody>
								</table>
							</div>					
						</div>
					</div>
				</div>
			
			
			</div>
			<?php echo form_close() ?>
			
		</div>
		<?}?>
		
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
var table;
$(document).ready(function(){	
	$(".decimal").number(true,1,'.',',');	
	load_spo2();		
	
})	
function clear_spo2(){
	$("#spo2_id").val('');
	$("#spo2_1").val('');
	$("#spo2_2").val('');
	$("#kategori_spo2").val('');
	$("#warna").val('#000');
	$("#btn_tambah_spo2").html('<i class="fa fa-save"></i> Save');
	
}
function load_spo2(){
	$('#index_spo2').DataTable().destroy();	
	table = $('#index_spo2').DataTable({
            autoWidth: false,
            searching: true,
            pageLength: 100,
            serverSide: true,
			"processing": true,
            "order": [],
            "ordering": false,
			// "columnDefs": [
					// { "width": "5%", "targets": 0,  className: "text-right" },
					// { "width": "10%", "targets": [1,2,4,5],  className: "text-center" },
					// { "width": "12%", "targets": [6,7],  className: "text-center" },
					// { "width": "30%", "targets": 3,  className: "text-left" },
				// ],
            ajax: { 
                url: '{site_url}mspo2/load_spo2', 
                type: "POST" ,
                dataType: 'json',
				data : {
					   }
            }
        });
}
function update_satuan(){
	let satuan_spo2=$("#satuan_spo2").val();
	
	if (satuan_spo2==''){
		sweetAlert("Maaf...", "Tentukan Ssatuan", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mspo2/update_satuan', 
		dataType: "JSON",
		method: "POST",
		data : {
				satuan_spo2:satuan_spo2,
				
				
			},
		complete: function(data) {
			location.reload();
		}
	});
}
$("#btn_tambah_spo2").click(function() {
	let spo2_id=$("#spo2_id").val();
	let spo2_1=$("#spo2_1").val();
	let spo2_2=$("#spo2_2").val();
	let kategori_spo2=$("#kategori_spo2").val();
	let warna=$("#warna").val();
	
	if (spo2_1==''){
		sweetAlert("Maaf...", "Tentukan Range Spo2", "error");
		return false;
	}
	if (spo2_2==''){
		sweetAlert("Maaf...", "Tentukan Range Spo2", "error");
		return false;
	}
	if (kategori_spo2==''){
		sweetAlert("Maaf...", "Tentukan Kategori", "error");
		return false;
	}
	
	$("#cover-spin").show();
	$.ajax({
		url: '{site_url}mspo2/simpan_spo2', 
		dataType: "JSON",
		method: "POST",
		data : {
				spo2_id:spo2_id,
				spo2_1:spo2_1,
				spo2_2:spo2_2,
				kategori_spo2:kategori_spo2,
				warna:warna,
				
			},
		complete: function(data) {
			clear_spo2();
			$('#index_spo2').DataTable().ajax.reload( null, false ); 
			$("#cover-spin").hide();
			$.toaster({priority : 'success', title : 'Succes!', message : ' Simpan'});
		}
	});

});
function edit_spo2(spo2_id){
	$("#spo2_id").val(spo2_id);
	$("#cover-spin").show();
	 $.ajax({
		url: '{site_url}mspo2/find_spo2',
		type: 'POST',
		dataType: "JSON",
		data: {id: spo2_id},
		success: function(data) {
			$("#btn_tambah_spo2").html('<i class="fa fa-save"></i> Edit');
			$("#spo2_id").val(data.id);
			$("#spo2_1").val(data.spo2_1);
			$("#spo2_2").val(data.spo2_2);
			$("#warna").val(data.warna).trigger('change');
			$("#kategori_spo2").val(data.kategori_spo2);
			
			$("#cover-spin").hide();
			
		}
	});
}
function hapus_spo2(id){
	swal({
		title: "Anda Yakin ?",
		text : "Akan Hapus Spo2?",
		type : "success",
		showCancelButton: true,
		confirmButtonText: "Ya",
		confirmButtonColor: "#34a263",
		cancelButtonText: "Batalkan",
	}).then(function() {
		$("#cover-spin").show();
		
		 $.ajax({
				url: '{site_url}mspo2/hapus_spo2',
				type: 'POST',
				data: {id: id},
				complete: function() {
					$('#index_spo2').DataTable().ajax.reload( null, false ); 
					$("#cover-spin").hide();
					$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
					
				}
			});
	});
}

</script>