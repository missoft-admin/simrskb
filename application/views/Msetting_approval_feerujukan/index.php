<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>
<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?php echo form_open('msetting_approval_feerujukan/save') ?>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive">
			<thead>
				<tr>
					<th width="10%">Step</th>
					<th width="10%">Tipe Rujukan</th>
					<th width="10%">Nominal</th>
					<th width="10%">User</th>
					<th width="10%">Jika Setuju</th>
					<th width="10%">Jika Menolak</th>
					<th width="10%">Aksi</th>
				</tr>
				<tr>
					<th>
						<input type="text" class="form-control number" name="step" placeholder="Step" value="">
					</th>
					<th>
						<select class="js-select2 form-control" name="tiperujukan" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Rumah Sakit</option>
							<option value="2">Klinik</option>
						</select>
					</th>
					<th>
						<input type="text" class="form-control number" name="nominal" placeholder="Nominal" value="">
					</th>
					<th>
						<select class="js-select2 form-control" name="userapproval" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<?php foreach (get_all('musers') as $row) { ?>
								<option value="<?=$row->id?>"><?= $row->name?></option>
							<?php } ?>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" name="jikasetuju" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Diberikan</option>
							<option value="2">Menunggu</option>
						</select>
					</th>
					<th>
						<select class="js-select2 form-control" name="jikamenolak" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="">Pilih Opsi</option>
							<option value="1">Diberikan</option>
							<option value="2">Menunggu</option>
						</select>
					</th>
					<th>
						<button type="submit" class="btn btn-primary">Tambahkan</button>
					</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($setting as $row) { ?>
					<tr>
						<td class="text-center"><?=$row->step?></td>
						<td><?=tipeRujukanRS($row->tiperujukan)?></td>
						<td><?=number_format($row->nominal)?></td>
						<td><?=$row->namauser?></td>
						<td><?=GetStatusPenyerahanHonor($row->jikasetuju)?></td>
						<td><?=GetStatusPenyerahanHonor($row->jikamenolak)?></td>
						<td>
							<div class='btn btn-group'>
								<a href='{base_url}msetting_approval_feerujukan/delete/<?=$row->id?>' class='btn btn-danger btn-sm delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php echo form_close() ?>
	</div>
	</div>
</div>

<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
	$(".number").number(true,0,'.',',');
</script>
