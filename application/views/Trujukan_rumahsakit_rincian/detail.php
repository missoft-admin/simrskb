<div class="block">
	<div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}trujukan_rumahsakit_rincian" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">

		<hr style="margin-top:0px">

		<div class="row">
			<?php echo form_open('trujukan_rumahsakit_rincian/filter_detail/{idfeerujukan}', 'class="form-horizontal" id="form-work"') ?>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="example-daterange1">Tanggal</label>
					<div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
							<input class="form-control" type="text" name="tanggal_dari" placeholder="From"
								value="{tanggal_dari}">
							<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
							<input class="form-control" type="text" name="tanggal_sampai" placeholder="To"
								value="{tanggal_sampai}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">No. Medrec</label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="nomedrec" value="{nomedrec}">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Nama Pasien</label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="namapasien" value="{namapasien}">
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Layanan</label>
					<div class="col-md-8">
						<select name="idlayanan" class="js-select2 form-control" style="width: 100%;"
							data-placeholder="Pilih Opsi">
							<option value="0">Semua Layanan</option>
                            <option value="1" <?=($idlayanan == 1 ? 'selected' : '')?>>Rawat Jalan</option>
                            <option value="2" <?=($idlayanan == 2 ? 'selected' : '')?>>Instalasi Gawat Darurat</option>
						</select>
					</div>
				</div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="idasalpasien">Asal Pasien</label>
                    <div class="col-md-8">
                        <select name="idasalpasien" id="idasalpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                            <option value="0">Semua Asal Pasien</option>
                            <option value="1" <?=($idasalpasien == 1 ? 'selected' : '')?>>Datang Sendiri</option>
                            <option value="2" <?=($idasalpasien == 2 ? 'selected' : '')?>>RFKTP</option>
                            <option value="3" <?=($idasalpasien == 3 ? 'selected' : '')?>>RFKRTL</option>
                        </select>
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-8">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>

		<hr style="margin-top:10px">

		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>Tanggal</th>
					<th>No. Medrec</th>
					<th>Nama Pasien</th>
					<th>Layanan</th>
					<th>Asal Pasien</th>
					<th>Asal Rujukan</th>
					<th>Status Rekanan</th>
					<th>Tanggal Tagihan</th>
					<th>Total Transaksi</th>
					<th>Nominal Rujukan</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	jQuery(function () {
		BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
			"autoWidth": false,
			"pageLength": 10,
			"ordering": true,
			"processing": true,
			"serverSide": true,
			"scrollX": true,
			"order": [],
			"ajax": {
				url: '{site_url}trujukan_rumahsakit_rincian/getIndexDetail/' + '<?= $this->uri->segment(2); ?>' + '/' + '{idfeerujukan}',
				type: "POST",
				dataType: 'json'
			},
			"columnDefs": [{
					"width": "5%",
					"targets": 0,
					"orderable": true
				},
				{
					"width": "5%",
					"targets": 1,
					"orderable": true
				},
				{
					"width": "5%",
					"targets": 2,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 3,
					"orderable": true
				},
				{
					"width": "5%",
					"targets": 4,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 5,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 6,
					"orderable": true
				},
				{
					"width": "5%",
					"targets": 7,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 8,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 9,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 10,
					"orderable": true
				},
				{
					"width": "10%",
					"targets": 11,
					"orderable": true
				}
			]
		});
	});
</script>