<div class="modal fade in black-overlay" id="PeriodePembayaranModal" role="dialog" aria-hidden="true"
    style="z-index: 1041;">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width: 50%;">
        <form class="form-horizontal" method="post" action="{base_url}trujukan_rumahsakit_rincian/next_periode">
            <div class="modal-content">
                <div class="block block-themed">
                    <div class="block-header bg-danger">
                        <ul class="block-options">
                            <li>
                                <button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
                            </li>
                        </ul>
                        <h3 class="block-title">Perubahan Tanggal Pembayaran</h3>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <input type="hidden" id="idAsalPasien" name="idasalpasien" value="">
                            <input type="hidden" id="idFeeRujukan" name="idfeerujukan" value="">
                            <input type="hidden" id="idTipe" name="idtipe" value="">
                            <input type="hidden" id="idRujukan" name="idrujukan" value="">
                            <input type="hidden" id="periodePembayaran" name="periode_pembayaran" value="">
                            <input type="hidden" id="periodeJatuhTempo" name="periode_jatuhtempo" value="">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Rujukan</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" id="namaRujukan" name="namarujukan" readonly value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tanggal Asal</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control input-sm" id="tanggalPembayaranAsal" name="periode_sebelumnya" readonly value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tanggal Perubahan</label>
                                    <div class="col-md-9">
                                        <select id="tanggalPembayaranPerubahan" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                            <option value="">Pilih Opsi</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check"></i> Simpan</button>
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', '.actionNextPeriode', function () {
            var idasalpasien = $(this).data('idasalpasien');
            var idtipe = $(this).data('idtipe');
            var idfeerujukan = $(this).data('idfeerujukan');
            var idrujukan = $(this).data('idrujukan');
            var namarujukan = $(this).data('namarujukan');
            var periodePembayaran = $(this).data('periodepembayaran');

            $("#idAsalPasien").val(idasalpasien);
            $("#idTipe").val(idtipe);
            $("#idFeeRujukan").val(idfeerujukan);
            $("#idRujukan").val(idrujukan);
            $("#namaRujukan").val(namarujukan);
            $("#tanggalPembayaranAsal").val(periodePembayaran);
            getPeriodePembayaran(idasalpasien, idrujukan, periodePembayaran);
        });

        $(document).on('change', '#tanggalPembayaranPerubahan', function () {
            setPeriodeFeeRujukan();
        });
    });

    function getPeriodePembayaran(idasalpasien, idrujukan, periodePembayaran) {
        $("#tanggalPembayaranPerubahan").empty();

        $.ajax({
            url: '{base_url}trujukan_rumahsakit_verifikasi/GetPeriodePembayaranFeeRujukanRS/' + idasalpasien + '/' + idrujukan + '/' + periodePembayaran,
            dataType: "json",
            success: function (data) {
                $('#tanggalPembayaranPerubahan').append(data.list_option);
            },
            complete: function (data) {
                setPeriodeFeeRujukan();
            }
        });
    }

    function setPeriodeFeeRujukan() {
        var periodePembayaran = $('#tanggalPembayaranPerubahan option:selected').val();
        var periodeJatuhTempo = $('#tanggalPembayaranPerubahan option:selected').data('jatuhtempo');
        $("#periodePembayaran").val(periodePembayaran);
        $("#periodeJatuhTempo").val(periodeJatuhTempo);
    }
</script>