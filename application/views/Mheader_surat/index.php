<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
	echo ErrorMessage($error);
}?>

<div class="block" >
	<div class="block-header">
		<div class="block-header">
			
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('mheader_surat/save', 'class="form-horizontal push-10-t"') ?>
        <div class="form-group">
			<label class="col-md-2 control-label text-primary" for="kota_form">FORMAT 1</label>
			
		</div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="example-hf-email"></label>
            <div class="col-md-7">

                <img class="img-avatar"  id="output_img" src="{upload_path}app_setting/<?=($logo_form?$logo_form:'no_image.png')?>" />
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-2 control-label" for="example-hf-email">Logo  (100x100)</label>
            <div class="col-md-7">
                <div class="box">
                    <input type="file" id="file-3" class="inputfile inputfile-3" onchange="loadFile_img(event)" style="display:none;" name="logo_form" value="{logo_form}" />
                    <label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
                </div>
            </div>
        </div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="nama_form">Nama Rumah Sakit<span style="color:red;">*</span></label>
			<div class="col-md-10">
				<input tabindex="0" type="text" class="form-control" id="nama_form" placeholder="Nama Rumah Sakit" name="nama_form" value="{nama_form}" required>
			</div>
			
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="alamat_form">Alamat<span style="color:red;">*</span></label>
			<div class="col-md-10">
				<input tabindex="0" type="text" class="form-control" id="alamat_form" placeholder="Alamat" name="alamat_form" value="{alamat_form}" required>
			</div>
			
		</div>
		
		<div class="form-group">
			
			<label class="col-md-2 control-label" for="telepone_form">Phone<span style="color:red;">*</span></label>
			<div class="col-md-4">
				<input tabindex="0" type="text" class="form-control" id="telepone_form" placeholder="Phone" name="telepone_form" value="{telepone_form}" required>
			</div>
			<label class="col-md-2 control-label" for="email_form">Email<span style="color:red;">*</span></label>
			<div class="col-md-4">
				<input tabindex="0" type="text" class="form-control" id="email_form" placeholder="Email" name="email_form" value="{email_form}" required>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="kota_form">Kota<span style="color:red;">*</span></label>
			<div class="col-md-10">
				<input tabindex="0" type="text" class="form-control" id="kota_form" placeholder="Kota" name="kota_form" value="{kota_form}" required>
			</div>
			
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label text-primary" for="kota_form">FORMAT 2</label>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="header_color">Header Color<span style="color:red;">*</span></label>
			<div class="col-md-2">
				<div class="js-colorpicker input-group colorpicker-element">
					<input class="form-control" type="text" id="header_color" name="header_color" value="{header_color}">
					<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label" for="footer_color">Footer Color<span style="color:red;">*</span></label>
			<div class="col-md-2">
				<div class="js-colorpicker input-group colorpicker-element">
					<input class="form-control" type="text" id="footer_color" name="footer_color" value="{footer_color}">
					<span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12 col-md-offset-2">
				<button class="btn btn-success" type="submit">Simpan</button>
				<a href="{base_url}mheader_surat" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
			</div>
		</div>
		<?php echo form_hidden('id', $id); ?>
		<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.number').number(true, 0);
		$('.footer').summernote({
		  height: 130,   //set editable area's height
		  codemirror: { // codemirror options
			theme: 'monokai'
		  }	
		})	
		load_content();
		
	})	
	$("#jenis_isi").change(function() {
		let jenis_isi=$("#jenis_isi").val();
		if (jenis_isi=='1'){
			$(".div_opsi").css("display", "block");
		}else{
			
			$(".div_opsi").css("display", "none");
		}
		
	});

	//add_content
	$("#btn_simpan_content").click(function() {
		if ($("#no").val()==''){
			sweetAlert("Maaf...", "Tentukan No Urut", "error");
			return false;
		}
		if ($("#no").val()=='0'){
			sweetAlert("Maaf...", "Tentukan No Urut", "error");
			return false;
		}

		if ($("#isi").val()==''){
			sweetAlert("Maaf...", "Isi Content ", "error");
			return false;
		}
		var idcontent=$("#idcontent").val();
		var no=$("#no").val();
		var header_id=$("#header_id").val();
		var isi=$("#isi").val();
		var jenis_isi=$("#jenis_isi").val();
		var ref_id=$("#ref_id").val();
		$("#cover-spin").show();
		$.ajax({
			url: '{site_url}mheader_surat/simpan_content/',
			dataType: "json",
			type: "POST",
			data: {
				idcontent:idcontent,
				header_id:header_id,
				jenis_isi:jenis_isi,
				ref_id:ref_id,
				no: no,isi:isi
			},
			success: function(data) {
				$("#modal_content").modal('hide');
				$("#cover-spin").hide();
				load_content();
			}
		});

	});
	$("#add_content").click(function() {
		$("#modal_content").modal('show');
		$("#header_id").val('0');
		$("#idcontent").val('');
		var rowCount = $('#index_content tr').length;
		$("#no").val(rowCount);
		$('#isi').summernote('code','');
		var id='';
		$.ajax({
				url: '{site_url}mheader_surat/load_jawaban',
				type: 'POST',
				dataType: "json",
				data: {id: id},
				success: function(data) {
					console.log(data);
					$("#ref_id").empty();
					$('#ref_id').append(data);
					$("#cover-spin").hide();
				}
		});
	});
	function add_detail(id){
		$("#modal_content").modal('show');
		$("#header_id").val(id);
		$("#idcontent").val('');
		var rowCount = $('#index_content tr').length;
		$("#no").val(rowCount);
		$('#isi').summernote('code','');
		var id='';
		$.ajax({
				url: '{site_url}mheader_surat/load_jawaban',
				type: 'POST',
				dataType: "json",
				data: {id: id},
				success: function(data) {
					console.log(data);
					$("#ref_id").empty();
					$('#ref_id').append(data);
					$("#cover-spin").hide();
				}
		});
	};
	function edit_content(id,header_id){
		$("#idcontent").val(id);
		$("#header_id").val(header_id);
		$("#modal_content").modal('show');
		$("#cover-spin").show();
			
		 $.ajax({
				url: '{site_url}mheader_surat/load_jawaban',
				type: 'POST',
				dataType: "json",
				data: {id: id},
				success: function(data) {
					console.log(data);
					$("#ref_id").empty();
					$('#ref_id').append(data);
					$("#cover-spin").hide();
				}
		});
		$("#cover-spin").show();
		$.ajax({
				url: '{site_url}mheader_surat/get_edit',
				type: 'POST',
				dataType: "json",
				data: {id: id},
				success: function(data) {
					console.log(data);
					$('#isi').summernote('code',data.isi);
					// $("#isi").html(data.isi);
					$("#jenis_isi").val(data.jenis_isi).trigger('change');
					$("#no").val(data.no);
					$("#cover-spin").hide();
					
				}
		});
	}
	function loadFile_img(event){
		// alert('load');
		var output_img = document.getElementById('output_img');
		output_img.src = URL.createObjectURL(event.target.files[0]);
	}
	function load_content(){
		$.ajax({
			url: '{site_url}mheader_surat/load_content/',
			dataType: "json",
			success: function(data) {
				$("#index_content tbody").empty();
				$('#index_content tbody').append(data);
			}
		});

	}
	function hapus(id){
		swal({
			title: "Anda Yakin ?",
			text : "Akan Hapus?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#34a263",
			cancelButtonText: "Batalkan",
		}).then(function() {
			$("#cover-spin").show();
			
			 $.ajax({
					url: '{site_url}mheader_surat/hapus',
					type: 'POST',
					data: {id: id},
					complete: function() {
						load_content();
						$("#cover-spin").hide();
						$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus'});
						
					}
				});
		});
	}
</script>