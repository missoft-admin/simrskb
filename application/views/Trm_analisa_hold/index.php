<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">

		<hr style="margin-top:0px">
		<div class="row">
			<?php echo form_open('trm_analisa_hold/filter','class="form-horizontal" id="form-work"') ?>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Tanggal</label>
					<div class="col-md-8">
						<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" name="tanggal_dari" placeholder="Tanggal Dari" value="{tanggal_dari}">
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" name="tanggal_sampai" placeholder="Tanggal Sampai" value="{tanggal_sampai}">
						</div>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">No. Medrec</label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="nomedrec" value="{nomedrec}" placeholder="No. Medrec">
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Nama Pasien</label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="namapasien" value="{namapasien}" placeholder="Nama Pasien">
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Layanan</label>
					<div class="col-md-8">
						<select name="layanan" class="js-select2 form-control" style="width: 100%;">
							<option value="#" <?=($layanan == '#' ? 'selected' : '')?>>Semua Layanan</option>
							<option value="Rawat Jalan" <?=($layanan == 'Rawat Jalan' ? 'selected' : '')?>>Rawat Jalan</option>
							<option value="Rawat Inap" <?=($layanan == 'Rawat Inap' ? 'selected' : '')?>>Rawat Inap</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Dokter</label>
					<div class="col-md-8">
						<select name="iddokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>Semua Dokter</option>
							<?php foreach (get_all('mdokter', array('status' => 1)) as $row) { ?>
								<option value="<?=$row->id?>" <?=($iddokter == $row->id ? 'selected' : '')?>><?=$row->nama?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="">Status</label>
					<div class="col-md-8">
						<select name="status" id="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" <?=($status == '#' ? 'selected' : '')?>>Semua Status</option>
							<option value="0" <?=($status == '0' ? 'selected' : '')?>>Belum Selesai</option>
							<option value="1" <?=($status == '1' ? 'selected' : '')?>>Sudah Selesai</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-3 control-label" for=""></label>
					<div class="col-md-9">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
			<?php echo form_close() ?>
		</div>
		<hr>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th>No</th>
					<th>Tanggal</th>
					<th>No. Medrec</th>
					<th>Nama Pasien</th>
					<th>Layanan</th>
					<th>Dokter</th>
					<th>Jumlah Lembaran Hold</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}trm_analisa_hold/getIndex/' + '<?= $this->uri->segment(2); ?>',
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "5%", "targets": 0, "orderable": true },
					{ "width": "10%", "targets": 1, "orderable": true },
					{ "width": "10%", "targets": 2, "orderable": true },
					{ "width": "10%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 4, "orderable": true },
					{ "width": "20%", "targets": 5, "orderable": true },
					{ "width": "10%", "targets": 6, "orderable": true },
					{ "width": "10%", "targets": 7, "orderable": true },
					{ "width": "10%", "targets": 8, "orderable": true }
				]
			});
	});
</script>
