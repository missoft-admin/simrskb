<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Kwitansi</title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      table {
        font-size: 21px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      table {
        font-size: 21px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <?php for ($i=0; $i < 3; $i++) { ?>
    <table class="content">

      <tr>
        <td class="border-bottom" rowspan="8" width="50px"><img style="width:40px;" src="assets/upload/logo/logoreport.jpg"></td>
        <td class="border-bottom" rowspan="8" width="40%">
          &nbsp;RSKB HALMAHERA SIAGA<br>
          &nbsp;Jl. LLRE. Martadinata No.28 Bandung<br>
          &nbsp;T. (022) 4206717 F. (022) 4216436
        </td>
        <td class="border-bottom" rowspan="8"><u>KWITANSI</u></td>
      </tr>

    </table>
    <br>
    <table class="content-2">
      <tr>
        <td class="text-italic text-bold">No. Kwitansi</td>
        <td class="text-center" style="width:20px">:</td>
        <td class="text-italic"><?=$no_kwitansi;?></td>
      </tr>
      <tr>
        <td class="text-italic text-bold">Sudah terima dari</td>
        <td class="text-center" style="width:20px">:</td>
        <td class="text-italic"><?=strtoupper($sudahterimadari);?></td>
      </tr>
      <tr>
        <td class="text-italic text-bold">Banyaknya Uang</td>
        <td class="text-center" style="width:20px">:</td>
        <td class="text-italic" style="background: #e2e2e2;"><?=ucwords(numbers_to_words($nominal).' Rupiah');?></td>
      </tr>
      <tr>
        <td class="text-italic text-bold">Untuk Pembayaran</td>
        <td class="text-center" style="width:20px">:</td>
        <td class="border-dotted"><?=$deskripsi;?></td>
      </tr>
      <tr>
        <td colspan="3" class="border-dotted">&nbsp;</td>
      </tr>
    </table>
    <table>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="2" class="text-italic" style="text-align:right">Bandung, <?=date("d").' '.MONTHFormat(date("m")).' '.date("Y");?></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width:60%">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="border-thick-top border-thick-bottom text-italic text-bold text-center">Jumlah</td>
        <td class="border-thick-top border-thick-bottom text-italic text-bold text-right" style="background: #e2e2e2;opacity: 0.6;">Rp <?=number_format($nominal);?></td>
        <td style="width:60%">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width:60%">&nbsp;</td>
        <td class="text-italic"><br><?= $userinput ?></td>
      </tr>
    </table>
    <br>
    <div class="border-dotted"></div>
    <br>
    <? } ?>
  </body>
</html>
