<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Faktur Implant</title>
    <style>
    body {
        -webkit-print-color-adjust: exact;
    }

    @media print {
        * {
            font-size: 14px !important;
        }

        table {
            font-size: 14px !important;
            border-collapse: collapse !important;
            width: 100% !important;
        }

        td {
            padding: 5px;
        }

        .content td {
            padding: 0px;
        }

        .content-2 td {
            margin: 3px;
        }

        /* border-normal */
        .border-full {
            border: 1px solid #000 !important;
        }

        .border-bottom {
            border-bottom: 1px solid #000 !important;
        }

        /* border-thick */
        .border-thick-top {
            border-top: 2px solid #000 !important;
        }

        .border-thick-bottom {
            border-bottom: 2px solid #000 !important;
        }

        .border-dotted {
            border-width: 1px;
            border-bottom-style: dotted;
        }

        /* text-position */
        .text-center {
            text-align: center !important;
        }

        .text-right {
            text-align: right !important;
        }

        /* text-style */
        .text-italic {
            font-style: italic;
        }

        .text-bold {
            font-weight: bold;
        }
    }

    @media screen {
        * {
            font-size: 16px !important;
        }

        table {
            font-size: 16px !important;
            border-collapse: collapse !important;
            width: 100% !important;
        }

        td {
            padding: 5px;
        }

        .content td {
            padding: 0px;
        }

        .content-2 td {
            margin: 3px;
        }

        /* border-normal */
        .border-full {
            border: 1px solid #000 !important;
        }

        .border-bottom {
            border-bottom: 1px solid #000 !important;
        }

        /* border-thick */
        .border-thick-top {
            border-top: 2px solid #000 !important;
        }

        .border-thick-bottom {
            border-bottom: 2px solid #000 !important;
        }

        .border-dotted {
            border-width: 1px;
            border-bottom-style: dotted;
        }

        /* text-position */
        .text-center {
            text-align: center !important;
        }

        .text-right {
            text-align: right !important;
        }

        /* text-style */
        .text-italic {
            font-style: italic;
        }

        .text-bold {
            font-weight: bold;
        }
    }

    </style>
    <script type="text/javascript">
    try {
        this.print();
    } catch (e) {
        window.onload = window.print;
    }

    </script>
</head>

<body>
    <table class="content">
        <tr>
            <td style="text-align:center">
                &nbsp;<b>FAKTUR IMPLANT</b>
            </td>
        </tr>
    </table>
    <br>
    <table class="content-2">
        <tr>
            <td rowspan="6" style="width:10%"><img src="<?= base_url() ?>assets/upload/logo/logo.png" width="100px"></td>
        </tr>
        <tr>
            <td style="width:150px">NO MEDREC</td>
            <td class="text-center" style="width:20px">:</td>
            <td><?=$nomedrec; ?></td>

            <td style="width:150px">DIAGNOSA</td>
            <td class="text-center" style="width:20px">:</td>
            <td><?=$diagnosa; ?></td>
        </tr>
        <tr>
            <td style="width:150px">NAMA PASIEN</td>
            <td class="text-center" style="width:20px">:</td>
            <td><?=$namapasien; ?></td>

            <td style="width:150px">OPERASI</td>
            <td class="text-center" style="width:20px">:</td>
            <td><?=$operasi; ?></td>
        </tr>
        <tr>
            <td style="width:150px">DOKTER</td>
            <td class="text-center" style="width:20px">:</td>
            <td><?=$namadokter; ?></td>

            <td style="width:150px">DISTRIBUTOR</td>
            <td class="text-center" style="width:20px">:</td>
            <td><?=$namadistributor; ?></td>
        </tr>
        <tr>
            <td style="width:150px">TANGGAL OPERASI</td>
            <td class="text-center" style="width:20px">:</td>
            <td><?=$tanggaloperasi; ?></td>

            <td style="width:150px"></td>
            <td class="text-center" style="width:20px"></td>
            <td></td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="text-center border-full">NO</td>
            <td class="text-center border-full">KODE IMPLANT</td>
            <td class="text-center border-full">NAMA IMPLANT</td>
            <td class="text-center border-full">KUANTITAS</td>
            <td class="text-center border-full">HARGA SATUAN</td>
            <td class="text-center border-full">DISKON</td>
            <td class="text-center border-full">JUMLAH</td>
        </tr>
        <?php $number = 1; ?>
        <?php $totalkeseluruhan = 0; ?>
        <?php foreach ($list_tindakan as $row) { ?>
        <tr>
            <td class="text-center border-full"><?=$number++?></td>
            <td class="text-center border-full"><?=strtoupper($row->kode)?></td>
            <td class="text-left border-full"><?=strtoupper($row->namatarif)?></td>
            <td class="text-center border-full"><?=number_format($row->kuantitas)?></td>
            <td class="text-right border-full"><?=number_format($row->hargajual)?></td>
            <td class="text-center border-full"><?=number_format(calcDiscountPercent($row->diskon, $row->hargajual))?></td>
            <td class="text-right border-full"><?=number_format($row->totalkeseluruhan)?></td>
        </tr>
        <?php $totalkeseluruhan += $row->totalkeseluruhan; ?>
        <?php } ?>
        <tr>
            <td class="text-center border-full" colspan="6"></td>
            <td class="text-right border-full"><?=number_format($totalkeseluruhan)?></td>
        </tr>
    </table>
    <br>
    <table>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td style="width:20%" class="text-center">Petugas</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="text-bold" style="float:right"><i>Tanggal & Jam Cetak <?= date('d-m-Y h:i:s') ?></i></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td style="width:20%" class="text-center">( <?=$this->session->userdata('user_name')?> )</td>
        </tr>
    </table>
</body>

</html>
