<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Rincian Deposit</title>
    <style>
    @media print {
       table {
         font-size: 9px !important;
         border-collapse: collapse !important;
         width: 100% !important;
       }
       td {
         padding: 5px;
       }
       .content td {
         padding: 0px;
       }
       .border-full {
         border: 1px solid #000 !important;
       }
       .border-bottom {
         border-bottom:1px solid #000 !important;
       }
       .border-thick-top{
         border-top:2px solid #000 !important;
       }
       .border-thick-bottom{
         border-bottom:2px solid #000 !important;
       }
       .text-center{
         text-align: center !important;
       }
       .text-right{
         text-align: right !important;
       }
    }

    @media screen {
       table {
         font-size: 9px !important;
         border-collapse: collapse !important;
         width: 100% !important;
       }
       td {
         padding: 5px;
       }
       .content td {
         padding: 0px;
       }
       .border-full {
         border: 1px solid #000 !important;
       }
       .border-bottom {
         border-bottom:1px solid #000 !important;
       }
       .border-thick-top{
         border-top:2px solid #000 !important;
       }
       .border-thick-bottom{
         border-bottom:2px solid #000 !important;
       }
       .text-center{
         text-align: center !important;
       }
       .text-right{
         text-align: right !important;
       }
    }
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <table class="content">
      <tr>
        <td rowspan="3" width="10px"><img style="width:40px;" src="assets/upload/logo/logoreport.jpg"></td>
        <td>&nbsp;&nbsp;RSKB HALMAHERA SIAGA</td>
      </tr>
      <tr>
        <td>&nbsp;&nbsp;Jl. LLRE. Martadinata No.28 Bandung</td>
      </tr>
      <tr>
        <td>&nbsp;&nbsp;T. (022) 4206717 F. (022) 4216436</td>
      </tr>
      <tr>
        <td>&nbsp;&nbsp;</td>
      </tr>
    </table>
    <table>
      <tr>
        <td colspan="5" class="text-center"><b><u>RINCIAN DEPOSIT</u></b></td>
      </tr>
      <tr>
        <td>No. Medrec</td>
        <td colspan="4">: <?=$nomedrec?></td>
      </tr>
      <tr>
        <td>Nama Pasien</td>
        <td colspan="4">: <?=$namapasien?></td>
      </tr>
      <tr>
        <td colspan="5" class="text-center">&nbsp;</td>
      </tr>
      <tr>
        <th style="width:10%" class="border-full border-thick-top text-center">NO.</th>
        <th style="width:10%" class="border-full border-thick-top text-center">TANGGAL</th>
        <th style="width:20%" class="border-full border-thick-top text-center">PEMBAYARAN</th>
        <th style="width:20%" class="border-full border-thick-top text-center">JUMLAH</th>
        <th style="width:20%" class="border-full border-thick-top text-center">PETUGAS</th>
      </tr>
      <?php $number = 1; ?>
      <?php $totaldeposit = 0; ?>
      <?php foreach  ($rincian as $row) { ?>
      <?php
        if($row->idmetodepembayaran == 1){
          $metodepembayaran = $row->metodepembayaran;
        }else{
          $metodepembayaran = $row->metodepembayaran.' ( '.$row->namabank.' )';
        }
      ?>
      <tr>
        <td class="border-full text-center"><?=$number++;?></td>
        <td class="border-full text-center"><?=$row->tanggal;?></td>
        <td class="border-full text-center"><?=$metodepembayaran;?></td>
        <td class="border-full text-right">Rp. <?=number_format($row->nominal);?></td>
        <td class="border-full text-center"><?=$row->namapetugas;?></td>
      </tr>
      <?php $totaldeposit = $totaldeposit + $row->nominal; ?>
      <?php } ?>
      <tr>
        <td colspan="3" class="border-full text-right"><b>TOTAL</b></td>
        <td style="border-right:1px solid #fff !important;"class="border-full text-right"><b>Rp. <?=number_format($totaldeposit);?></b></td>
        <td class="border-full">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="5" class="text-right">SIMRSKB Halmahera - Tanggal Cetak <?=date("d/m/Y - h:i:s");?> user <?=$this->session->userdata('user_name');?></td>
      </tr>
    </table>
  </body>
</html>
