<?php
  if ($tipe == '') {
    $reportTitle = 'Biaya Perawatan';
  } else if ($tipe == 'piutang') {
    $reportTitle = 'Piutang';
  } else if ($tipe == 'bpjstk') {
    $reportTitle = 'BPJSTK';
  }
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Rincian Global <?=$reportTitle;?></title>
	<style>
		body {
			font-family: "Courier", Arial, sans-serif;
		}

		@media print {
			table {
				font-size: 18px !important;
				border-collapse: collapse !important;
				width: 100% !important;
			}

			td {
				padding: 5px;
			}

			.content td {
				padding: 0px;
			}

			.border-full {
				border: 1px solid #000 !important;
			}

			.border-bottom {
				border-bottom: 1px solid #000 !important;
			}

			.border-right {
				border-right: 1px solid #000 !important;
			}

			.border-left {
				border-left: 1px solid #000 !important;
			}

			.border-top {
				border-top: 1px solid #000 !important;
			}

			.border-thick-top {
				border-top: 2px solid #000 !important;
			}

			.border-thick-bottom {
				border-bottom: 2px solid #000 !important;
			}

			.text-center {
				text-align: center !important;
			}

			.text-right {
				text-align: right !important;
			}
		}

		@media screen {
			table {
				font-size: 18px !important;
				border-collapse: collapse !important;
				width: 100% !important;
			}

			td {
				padding: 5px;
			}

			.content td {
				padding: 0px;
			}

			.border-full {
				border: 1px solid #000 !important;
			}

			.border-bottom {
				border-bottom: 1px solid #000 !important;
			}

			.border-right {
				border-right: 1px solid #000 !important;
			}

			.border-left {
				border-left: 1px solid #000 !important;
			}

			.border-top {
				border-top: 1px solid #000 !important;
			}

			.border-thick-top {
				border-top: 2px solid #000 !important;
			}

			.border-thick-bottom {
				border-bottom: 2px solid #000 !important;
			}

			.text-center {
				text-align: center !important;
			}

			.text-right {
				text-align: right !important;
			}
		}
	</style>
	<script type="text/javascript">
		try {
			this.print();
		} catch (e) {
			window.onload = window.print;
		}
	</script>
</head>

<body>
	<table>
		<tr>
			<td style="width:20%" rowspan="2" class="border-full text-center">
				<img style="width:40px;" src="assets/upload/logo/logoreport.jpg">
				<br> <b>RSKB HALMAHERA SIAGA</b> <br> Jl. LLRE. Martadinata No.28 Bandung <br> T. (022) 4206717 <br> F. (022) 4216436
			</td>
			<td style="width:40%" colspan="2" class="border-full text-center"><b style="font-size:15px !important;">NOTA RINCIAN</b> <br> <?=strtoupper($reportTitle);?></td>
			<td style="width:40%" colspan="3" class="border-full text-center"><b style="font-size:15px !important;"><?=$nopendaftaran?></b> <br> Tanggal <?=date("d/m/Y")?></td>
		</tr>
		<tr>
			<td>
				Nomor Register / Medrec <br>
				Nama Pasien <br>
				Alamat <br>
				Bagian Kamar
			</td>
			<td colspan="4" class="border-right">
				<b><?=$nopendaftaran?> / <?=$nomedrec?></b> <br>
				<?=$namapasien?> <br>
				<?=$alamatpasien?> <br>
				<?=$namabed?>
			</td>
		</tr>
		<tr>
			<td colspan="3" class="border-top border-left border-bottom">Mulai Dirawat Tanggal : <b><?=$tanggaldaftar?></b></td>
			<td colspan="3" class="border-top border-right border-bottom">Sampai Dengan Tanggal : <b class="text-bold"><?=$tanggalkeluar?></b></td>
		</tr>

		<?php $show = 0;?>
		<?php $totalRanapRuangan = 0;?>
		<?php if ($statusvalidasi == 1) {?>
		<?php $dataRuanganValidasi = $this->Trawatinap_verifikasi_model->viewRincianRanapRuanganValidasi($idpendaftaran);?>
		<?php $jumlahhistory = COUNT($dataRuanganValidasi);?>
		<?php foreach ($dataRuanganValidasi as $row) {?>
		<tr>
			<?php if ($show == 0) {?>
			<td colspan="2" rowspan="<?=$jumlahhistory?>" width="23%" class="border-full text-center">Kelas <br> Perawatan</td>
			<?php }?>

			<td colspan="2" class="border-full"><?=$row->namatarif?> : <?=number_format($row->jumlahhari)?> hari @ Rp. <?=number_format($row->total)?></td>
			<td colspan="2" class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
		</tr>
		<?php $show = 1;?>
		<?php $totalRanapRuangan = $totalRanapRuangan + $row->totalkeseluruhan;?>
		<?php }?>
		<?php } else {?>
		<?php $dataRuanganGroup = $this->Trawatinap_verifikasi_model->viewRincianRanapRuanganGroup($idpendaftaran);?>
		<?php $jumlahhistory = COUNT($dataRuanganGroup);?>
		<?php foreach ($dataRuanganGroup as $row) {?>
		<?php $tarifRanapRuangan = $this->Trawatinap_tindakan_model->getTarifRuangan($row->idkelompokpasien, $row->idruangan, $row->idkelas);?>
		<?php $totalKeseluruhan = $tarifRanapRuangan['total'] * $row->jumlahhari;?>
		<tr>
			<?php if ($show == 0) {?>
			<td colspan="2" rowspan="<?=$jumlahhistory?>" class="border-full text-center">Kelas <br> Perawatan</td>
			<?php }?>

			<td colspan="2" class="border-full">Ruang <?=$row->namaruangan?>, Kelas <?=$row->namakelas?> : <?=number_format($row->jumlahhari)?> hari @ Rp. <?=number_format($tarifRanapRuangan['total'])?></td>
			<td colspan="2" class="border-full text-right">Rp. <?=number_format($totalKeseluruhan)?></td>
		</tr>
		<?php $show = 1;?>
		<?php $totalRanapRuangan = $totalRanapRuangan + $totalKeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalPoli = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRajalTindakanFilteredIGD($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalPoli = $totalPoli + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalPoliObat = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRajalObatFilteredIGD($idpendaftaran, 2) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalPoliObat = $totalPoliObat + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalPoliAlkes = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRajalAlkesFilteredIGD($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalPoliAlkes = $totalPoliAlkes + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalFarmasiObatIGD = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiRajal($idpendaftaran, 3) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalFarmasiObatIGD = $totalFarmasiObatIGD + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalFarmasiAlkesIGD = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiRajal($idpendaftaran, 1) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalFarmasiAlkesIGD = $totalFarmasiAlkesIGD + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalFarmasiRajalReturObat = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRajalObat($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalFarmasiRajalReturObat = $totalFarmasiRajalReturObat + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalFarmasiRajalReturAlkes = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRajalAlkes($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalFarmasiRajalReturAlkes = $totalFarmasiRajalReturAlkes + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalRanapObat = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapObat($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalRanapObat = $totalRanapObat + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalFarmasiObat = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapObat($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalFarmasiObat = $totalFarmasiObat + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalFarmasiReturObat = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapObat($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalFarmasiReturObat = $totalFarmasiReturObat + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalRanapSewaAlat = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 4) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalRanapSewaAlat = $totalRanapSewaAlat + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalOKSewaAlat = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiSewaAlat($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalOKSewaAlat = $totalOKSewaAlat + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalOKSewaKamar = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiKamar($idpendaftaran) as $row) {?>
		<?php $totalOKSewaKamar = $totalOKSewaKamar + $row->totalkeseluruhan;?>
		<?php }?>

		<?php $totalOKObatNarcose = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiNarcose($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalOKObatNarcose = $totalOKObatNarcose + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalOKObat = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiObat($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalOKObat = $totalOKObat + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalOKAlkes = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiAlkes($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalOKAlkes = $totalOKAlkes + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalOKJasaAsisten = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiJasaAsisten($idpendaftaran) as $row) {?>
		<?php $totalOKJasaAsisten = $totalOKJasaAsisten + $row->grandtotal;?>
		<?php }?>

		<?php $totalRanapAlkes = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapAlkes($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalRanapAlkes = $totalRanapAlkes + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalFarmasiReturAlkes = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapAlkes($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalFarmasiReturAlkes = $totalFarmasiReturAlkes + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalFarmasiAlkes = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapAlkes($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalFarmasiAlkes = $totalFarmasiAlkes + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalRadXray = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 1) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalRadXray = $totalRadXray + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalRadUSG = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 2) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalRadUSG = $totalRadUSG + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalRadCTScan = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 3) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalRadCTScan = $totalRadCTScan + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalRadMRI = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 4) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalRadMRI = $totalRadMRI + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalRadBMD = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 5) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalRadBMD = $totalRadBMD + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalFisio = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFisioterapi($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalFisio = $totalFisio + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalLab = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 1) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalLab = $totalLab + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalLabPA = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 2) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalLabPA = $totalLabPA + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalRanapECG = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 2) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalRanapECG = $totalRanapECG + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalRanapFullCare = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 1) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalRanapFullCare = $totalRanapFullCare + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalOKImplan = 0;?>
    <?php if ($tipe != 'bpjstk') { ?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiImplan($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalOKImplan = $totalOKImplan + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>
		<?php }?>

		<?php $totalRanapAlkesBantu = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapAlkesBantu($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalRanapAlkesBantu = $totalRanapAlkesBantu + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalFarmasiAlkesBantu = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapAlkesBantu($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalFarmasiAlkesBantu = $totalFarmasiAlkesBantu + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalFarmasiReturAlkesBantu = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapAlkesBantu($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalFarmasiReturAlkesBantu = $totalFarmasiReturAlkesBantu + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalRanapAmbulance = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 5) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalRanapAmbulance = $totalRanapAmbulance + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalLabPMI = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 3) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalLabPMI = $totalLabPMI + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalRanapLainLain = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 6) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalRanapLainLain = $totalRanapLainLain + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalRanapVisite = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRanapVisite($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalRanapVisite = $totalRanapVisite + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalOKJasaDokterOperator = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiJasaDokterOpertor($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalOKJasaDokterOperator = $totalOKJasaDokterOperator + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php $totalOKJasaDokterAnesthesi = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianOperasiJasaDokterAnesthesi($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalOKJasaDokterAnesthesi = $totalOKJasaDokterAnesthesi + $row->grandtotal;?>
		<?php }?>
		<?php }?>

		<?php $totalAdmRajal = 0;?>
		<?php foreach ($this->Trawatinap_verifikasi_model->viewRincianAdministrasiRawatJalan($idpendaftaran) as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<?php $totalAdmRajal = $totalAdmRajal + $row->totalkeseluruhan;?>
		<?php }?>
		<?php }?>

		<?php
      $totalSebelumAdm = $totalRanapFullCare + $totalRanapECG + $totalRanapVisite + $totalRanapSewaAlat +
      $totalRanapAmbulance + $totalRanapAlkes + $totalRanapAlkesBantu + $totalRanapLainLain + $totalPoli +
      $totalPoliObat + $totalFarmasiObatIGD + $totalFarmasiRajalReturObat + $totalFarmasiAlkesIGD + $totalPoliAlkes + $totalFarmasiRajalReturAlkes + $totalRanapObat + $totalFarmasiObat + $totalFarmasiReturObat + $totalFarmasiAlkes +
      $totalFarmasiReturAlkes + $totalFarmasiAlkesBantu + $totalFarmasiReturAlkesBantu + $totalLab + $totalLabPA + $totalLabPMI +
      $totalRadXray + $totalRadUSG + $totalRadCTScan + $totalRadMRI + $totalRadBMD +
      $totalFisio + $totalOKSewaAlat + $totalOKAlkes + $totalOKObat + $totalOKObatNarcose +
      $totalOKImplan + $totalOKSewaKamar + $totalOKJasaDokterOperator + $totalOKJasaDokterAnesthesi + $totalOKJasaAsisten +
      $totalRanapRuangan + $totalAdmRajal;
    ?>

		<?php $totalAdmRanap = 0;?>
		<?php if ($statusvalidasi == 1) {?>
		<?php $dataAdminRanap = get_all('trawatinap_administrasi', array('idpendaftaran' => $idpendaftaran));?>
		<?php foreach ($dataAdminRanap as $row) {?>
		<?php $totalAdmRanap = $totalAdmRanap + $row->tarifsetelahdiskon;?>
		<?php }?>
		<?php } else {?>
		<?php $dataAdminRanap = $this->db->query("CALL spAdministrasiRanap($idpendaftaran, $totalSebelumAdm)")->result();?>
		<?php foreach ($dataAdminRanap as $row) {?>
		<?php $totalAdmRanap = $totalAdmRanap + $row->tarif;?>
		<?php }?>
		<?php }?>

		<tr>
			<td class="border-full" rowspan="15"><b>Pertolongan dan Pemeriksaan</b></td>
			<td class="border-full" colspan="3">Pertolongan di Poli Gawat Darurat</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalPoli + $totalPoliObat + $totalPoliAlkes + $totalFarmasiObatIGD + $totalFarmasiAlkesIGD + $totalFarmasiRajalReturObat + $totalFarmasiRajalReturAlkes)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">Sewa Alat</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalRanapSewaAlat + $totalOKSewaAlat)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">Kamar Operasi</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalOKSewaKamar)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">Obat Narcose</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalOKObatNarcose)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">Obat Dikamar Operasi</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalOKObat)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">Alat Kesehatan di Kamar Operasi</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalOKAlkes)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">Ass. Dikamar Operasi</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalOKJasaAsisten)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">Obat di Perawatan</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalRanapObat + $totalFarmasiObat + $totalFarmasiReturObat)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">Alat Kesehatan di perawatan</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalRanapAlkes + $totalFarmasiAlkes + $totalFarmasiReturAlkes)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">Radiologi</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalRadXray + $totalRadUSG + $totalRadCTScan + $totalRadMRI + $totalRadBMD)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">Fisioterapi</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalFisio)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">Pemeriksaan Laboratorium</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalLab)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">Pemeriksaan Pathologi Anatomi</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalLabPA)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">ECG</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalRanapECG)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">Tindakan Keperawatan</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalRanapFullCare)?></td>
		</tr>

		<tr>
			<td class="border-full" rowspan="6"><b>Lain-lain</b></td>
			<td class="border-full" colspan="3">Alat Implant / Orthopaedi</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalOKImplan)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">Alat Bantu</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalRanapAlkesBantu + $totalFarmasiAlkesBantu + $totalFarmasiReturAlkesBantu)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">Ambulance</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalRanapAmbulance)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">PMI</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalLabPMI)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">Administrasi</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalAdmRajal + $totalAdmRanap)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="3">Lain-lain</td>
			<td class="border-full" colspan="2">Rp. <?=number_format($totalRanapLainLain)?></td>
		</tr>

		<tr>
			<td class="border-full" colspan="6"><b>Visite :</b></td>
		</tr>

		<?php
        if (mysqli_more_results($this->db->conn_id)) {
            mysqli_next_result($this->db->conn_id);
        }
      ?>

		<?php $show = 0;?>
		<?php $dataRanapVisite = $this->Trawatinap_verifikasi_model->viewRincianRanapVisiteGlobal($idpendaftaran);?>
		<?php $jumlahvisite = COUNT($dataRanapVisite);?>
		<?php foreach ($dataRanapVisite as $row) {?>
		<?php if ($row->statusverifikasi == 1 || $status == 0) {?>
		<tr>
			<?php if ($show == 0) {?>
			<td class="border-full" rowspan="<?=$jumlahvisite?>">Visite Dokter Spesialis</td>
			<?php }?>
			<td class="border-full" colspan="3"><?=$row->namadokter?></td>
			<td class="border-full" colspan="2">Rp. <?=number_format($row->totalkeseluruhan)?></td>
		</tr>
		<?php $show = 1;?>
		<?php }?>
		<?php }?>

		<tr>
			<td class="border-full" colspan="6"><b>Jasa Dokter Operator :</b></td>
		</tr>

		<?php $show = 0;?>
		<?php $dataDokterOperator = $this->Trawatinap_verifikasi_model->viewRincianOperasiJasaDokterOpertor($idpendaftaran);?>
		<?php $jumlahdokter = COUNT($dataDokterOperator);?>
		<?php foreach ($dataDokterOperator as $row) {?>
		<tr>
			<?php if ($show == 0) {?>
			<td class="border-full" rowspan="<?=$jumlahdokter?>">Jasa Dokter Operator</td>
			<?php }?>
			<td class="border-full" colspan="3"><?=$row->namadokter?></td>
			<td class="border-full" colspan="2">Rp. <?=number_format($row->totalkeseluruhan)?></td>
		</tr>
		<?php $show = 1;?>
		<?php }?>

		<tr>
			<td class="border-full" colspan="6"><b>Jasa Dokter Anasthesi :</b></td>
		</tr>

		<?php $show = 0;?>
		<?php $dataDokterAnesthesi = $this->Trawatinap_verifikasi_model->viewRincianOperasiJasaDokterAnesthesi($idpendaftaran);?>
		<?php $jumlahdokter = COUNT($dataDokterAnesthesi);?>
		<?php foreach ($dataDokterAnesthesi as $row) {?>
		<tr>
			<?php if ($show == 0) {?>
			<td class="border-full" rowspan="<?=$jumlahdokter?>">Jasa Dokter Anasthesi</td>
			<?php }?>
			<td class="border-full" colspan="3"><?=$row->namadokter?></td>
			<td class="border-full" colspan="2">Rp. <?=number_format($row->grandtotal)?></td>
		</tr>
		<?php $show = 1;?>
		<?php }?>

		<?php $totalKeseluruhan = ceil(($totalSebelumAdm + $totalAdmRanap) / 100) * 100;?>

		<tr>
			<td colspan="6" class="border-full">&nbsp;</td>
		</tr>

		<tr>
			<td colspan="3" class="border-full">Diagnosa : <br> <?=$diagnosa?></td>
			<td colspan="3" class="border-full">Operasi : <br> <?=$operasi?></td>
		</tr>
		<tr>
			<td rowspan="<?= ($tipe == '' ? 3 : 2); ?>" class="border-full">Catatan : <br> <?=$catatan?><br></td>
			<td colspan="2" class="border-full">TOTAL BIAYA PERAWATAN + DOKTER</td>
			<td colspan="3" class="border-full text-right">Rp. <?=number_format($totalKeseluruhan)?></td>
		</tr>
		<tr>
			<td colspan="2" class="border-full">DISKON</td>
			<td colspan="3" class="border-full text-right">Rp. <?=number_format($diskon_pembayaran)?></td>
		</tr>
		<?php if ($tipe == '') { ?>
		<tr>
			<td colspan="2" class="border-full">UANG MUKA/TELAH DIBAYAR</td>
			<td colspan="3" class="border-full text-right">Rp. <?=number_format($totaldeposit)?></td>
		</tr>
		<?php } ?>
		<tr>
			<td class="border-full">Paraf <br><br><br><br>
				<center><?= $user_input_pembayaran ?></center>
			</td>
			<td colspan="2" class="border-full">KURANG/LEBIH/JUMLAH</td>
			<?php if ($tipe == '') { ?>
			<td colspan="3" class="border-full text-right">Rp. <?php echo ($totalKeseluruhan >= $totaldeposit ? number_format($totalKeseluruhan - $totaldeposit - $diskon_pembayaran) : '(' . number_format($totaldeposit - $totalKeseluruhan - $diskon_pembayaran) . ')'); ?></td>
			<?php } else if ($tipe == 'piutang' || $tipe == 'bpjstk') { ?>
			<td colspan="3" class="border-full text-right">Rp. <?php echo number_format($totalKeseluruhan - $diskon_pembayaran); ?></td>
			<?php } ?>
		</tr>
		<tr>
			<td class="border-full text-center" colspan="6"><b>NOTA PERINCIAN INI BUKAN MERUPAKAN BUKTI PEMBAYARAN</b></td>
		</tr>
		<tr>
			<td colspan="6">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="6" style="font-size: 8px">
				Tanggal Cetak : <?= date("d-M-Y H:i:s") ?>, User Pembayaran : <?= $user_input_pembayaran ?>, User Cetak : <?= $this->session->userdata('user_name') ?>, Cetak Ke : 1
			</td>
		</tr>
	</table>
</body>

</html>
