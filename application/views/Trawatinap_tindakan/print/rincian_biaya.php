<?php
  if ($tipe == '') {
  	$reportTitle = '';
  } elseif ($tipe == 'piutang') {
  	$reportTitle = 'Piutang';
  } elseif ($tipe == 'bpjstk') {
  	$reportTitle = 'BPJSTK';
  }
?>

<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Rincian Biaya <?= $reportTitle; ?></title>
    <style>
    body {
        font-family: "Courier", Arial, sans-serif;
    }

    @media print {
        table {
            font-size: 18px !important;
            border-collapse: collapse !important;
            width: 100% !important;
        }

        td {
            padding: 5px;
        }

        .content td {
            padding: 0px;
        }

        .border-full {
            border: 1px solid #000 !important;
        }

        .border-bottom {
            border-bottom: 1px solid #000 !important;
        }

        .border-right {
            border-right: 1px solid #000 !important;
        }

        .border-left {
            border-left: 1px solid #000 !important;
        }

        .border-top {
            border-top: 1px solid #000 !important;
        }

        .border-thick-top {
            border-top: 2px solid #000 !important;
        }

        .border-thick-bottom {
            border-bottom: 2px solid #000 !important;
        }

        .text-center {
            text-align: center !important;
        }

        .text-right {
            text-align: right !important;
        }

        .text-semi-bold {
            font-size: 15px !important;
            font-weight: 700 !important;
        }

        .text-bold {
            font-size: 15px !important;
            font-weight: 700 !important;
        }
    }

    @media screen {
        table {
            font-size: 18px !important;
            border-collapse: collapse !important;
            width: 100% !important;
        }

        td {
            padding: 5px;
        }

        .content td {
            padding: 0px;
        }

        .border-full {
            border: 1px solid #000 !important;
        }

        .border-bottom {
            border-bottom: 1px solid #000 !important;
        }

        .border-right {
            border-right: 1px solid #000 !important;
        }

        .border-left {
            border-left: 1px solid #000 !important;
        }

        .border-top {
            border-top: 1px solid #000 !important;
        }

        .border-thick-top {
            border-top: 2px solid #000 !important;
        }

        .border-thick-bottom {
            border-bottom: 2px solid #000 !important;
        }

        .text-center {
            text-align: center !important;
        }

        .text-right {
            text-align: right !important;
        }

        .text-semi-bold {
            font-size: 15px !important;
            font-weight: 700 !important;
        }

        .text-bold {
            font-size: 15px !important;
            font-weight: 700 !important;
        }
    }

    </style>
    <script type="text/javascript">
    try {
        this.print();
    } catch (e) {
        window.onload = window.print;
    }

    </script>
</head>

<body>
    <table class="border-full">
        <tr>
            <td style="width:20%" rowspan="6" class="text-center">
                <img style="width:100px;" src="<?=base_url()?>assets/upload/logo/logo.png">
                <br> Jl. LLRE. Martadinata
                <br> No.28 Bandung
                <br> T. (022) 4206717
                <br> F. (022) 4216436
            </td>
            <td colspan="3" class="text-center">
                <b style="font-size:22px !important;">NOTA RINCIAN PEMBAYARAN <?=strtoupper($reportTitle); ?></b>
            </td>
        </tr>
        <tr>
            <td width="15%" style="font-size:18px !important;">
                <b>Nomor Register</b>
            </td>
            <td style="font-size:18px !important;">
                : <?=$nopendaftaran?>
            </td>
            <td width="15%" style="font-size:18px !important;">
                <b>Pelayanan</b>
            </td>
            <td style="font-size:18px !important;">
                : <?=$pelayanan?>
            </td>
        </tr>
        <tr>
            <td width="15%" style="font-size:18px !important;">
                <b>No Rekam Medis</b>
            </td>
            <td style="font-size:18px !important;">
                : <?=$nomedrec?>
            </td>
            <td width="15%" style="font-size:18px !important;">
                <b>R. Perawatan</b>
            </td>
            <td style="font-size:18px !important;">
                : <?=$kelas?> - <?=$bed?>
            </td>
        </tr>
        <tr>
            <td width="15%" style="font-size:18px !important;">
                <b>Nama Pasien</b>
            </td>
            <td style="font-size:18px !important;">
                : <?=$namapasien?>
            </td>
            <td width="15%" style="font-size:18px !important;">
                <b>Alamat</b>
            </td>
            <td style="font-size:18px !important;">
                : <?=$alamatpasien?>
            </td>
        </tr>
        <tr>
            <td width="15%" style="font-size:18px !important;">
                <b>Kelompok Pasien</b>
            </td>
            <td style="font-size:18px !important;">
                : <?=$kelompokpasien?>
            </td>
            <td width="15%" style="font-size:18px !important;">
                <b>Penjamin</b>
            </td>
            <td style="font-size:18px !important;">
                : <?=$rekanan?>
            </td>
        </tr>
        <tr>
            <td width="15%" style="font-size:18px !important;">
                <b>Mulai Dirawat</b>
            </td>
            <td style="font-size:18px !important;">
                : <?=$tanggaldaftar?>
            </td>
            <td width="15%" style="font-size:18px !important;">
                <b>Akhir Dirawat</b>
            </td>
            <td style="font-size:18px !important;">
                : <?=$tanggalkeluar?>
            </td>
        </tr>
    </table>

    <table style="margin-top: -8px;">
        <tr>
            <td style="width: 10%;"></td>
            <td style="width: 13%;"></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td style="width: 20%;"></td>
        </tr>

        <?php $show = 0; ?>
        <?php $totalRanapRuangan = 0; ?>
        <?php if ($idtipe == 1) {?>
        <?php if ($statusvalidasi == 1) {?>
        <?php $dataRuanganValidasi = $this->Trawatinap_verifikasi_model->viewRincianRanapRuanganValidasi($idpendaftaran); ?>
        <?php $jumlahhistory = COUNT($dataRuanganValidasi); ?>
        <?php foreach ($dataRuanganValidasi as $row) {?>
        <tr>
            <?php if ($show == 0) {?>
            <td colspan="2" rowspan="<?=$jumlahhistory?>" class="border-full text-center">Kelas <br> Perawatan</td>
            <?php } ?>

            <td colspan="4" class="border-full"><?=$row->namatarif?> : <?=number_format($row->jumlahhari)?> hari @ Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
        </tr>
        <?php $show = 1; ?>
        <?php $totalRanapRuangan = $totalRanapRuangan + $row->totalkeseluruhan; ?>
        <?php } ?>
        <?php } else {?>
        <?php $dataRuanganGroup = $this->Trawatinap_verifikasi_model->viewRincianRanapRuanganGroup($idpendaftaran); ?>
        <?php $jumlahhistory = COUNT($dataRuanganGroup); ?>
        <?php foreach ($dataRuanganGroup as $row) {?>
        <?php $tarifRanapRuangan = $this->Trawatinap_tindakan_model->getTarifRuangan($row->idkelompokpasien, $row->idruangan, $row->idkelas); ?>
        <?php $totalKeseluruhan = $tarifRanapRuangan['total'] * $row->jumlahhari; ?>
        <tr>
            <?php if ($show == 0) {?>
            <td colspan="2" rowspan="<?=$jumlahhistory?>" class="border-full text-center">Kelas <br> Perawatan</td>
            <?php } ?>

            <td colspan="4" class="border-full">Ruang <?=$row->namaruangan?>, Kelas <?=$row->namakelas?> : <?=number_format($row->jumlahhari)?> hari @ Rp. <?=number_format($tarifRanapRuangan['total'])?></td>
            <td class="border-full text-right">Rp. <?=number_format($totalKeseluruhan)?></td>
        </tr>
        <?php $show = 1; ?>
        <?php $totalRanapRuangan = $totalRanapRuangan + $totalKeseluruhan; ?>
        <?php } ?>
        <?php } ?>
        <?php } ?>
        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;">
                <b class="text-bold">RAWAT INAP [FULL CARE]</b>
            </td>
        </tr>
        <?php $totalRanapFullCare = 0; ?>
        <?php $dataRanapFullCare = $this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 1); ?>
        <?php if (COUNT($dataRanapFullCare) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataRanapFullCare as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full" width="10%"><?=$row->nopendaftaran?></td>
            <td class="border-full" width="10%"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" width="40%" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center" width="5%"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right" width="15%">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right" width="15%">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalRanapFullCare = $totalRanapFullCare + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL RAWAT INAP [FULL CARE]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalRanapFullCare)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">RAWAT INAP [ECG]</b></td>
        </tr>
        <?php $totalRanapECG = 0; ?>
        <?php $dataRanapECG = $this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 2); ?>
        <?php if (COUNT($dataRanapECG) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataRanapECG as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalRanapECG = $totalRanapECG + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL RAWAT INAP [ECG]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalRanapECG)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">RAWAT INAP [VISITE DOKTER]</b></td>
        </tr>
        <?php $totalRanapVisite = 0; ?>
        <?php $dataRanapVisite = $this->Trawatinap_verifikasi_model->viewRincianRanapVisite($idpendaftaran); ?>
        <?php if (COUNT($dataRanapVisite) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataRanapVisite as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namadokter?> (<?=$row->namaruangan?>)</td>
            <td class="border-full text-center">1</td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalRanapVisite = $totalRanapVisite + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL RAWAT INAP [VISITE DOKTER]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalRanapVisite)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">RAWAT INAP [SEWA ALAT]</b></td>
        </tr>
        <?php $totalRanapSewaAlat = 0; ?>
        <?php $dataRanapSewaAlat = $this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 4); ?>
        <?php if (COUNT($dataRanapSewaAlat) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataRanapSewaAlat as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalRanapSewaAlat = $totalRanapSewaAlat + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL RAWAT INAP [SEWA ALAT]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalRanapSewaAlat)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">RAWAT INAP [AMBULANCE]</b></td>
        </tr>
        <?php $totalRanapAmbulance = 0; ?>
        <?php $dataRanapAmbulance = $this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 5); ?>
        <?php if (COUNT($dataRanapAmbulance) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataRanapAmbulance as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalRanapAmbulance = $totalRanapAmbulance + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL RAWAT INAP [AMBULANCE]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalRanapAmbulance)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">RAWAT INAP [OBAT]</b></td>
        </tr>

        <?php $dataRanapObat = $this->Trawatinap_verifikasi_model->viewRincianRanapObat($idpendaftaran); ?>
        <?php $dataFarmasiObat = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapObat($idpendaftaran); ?>
        <?php $dataFarmasiReturObat = $this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapObat($idpendaftaran); ?>
        <?php if (COUNT($dataFarmasiObat) == 0 and COUNT($dataFarmasiReturObat) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>

        <?php $totalRanapObat = 0; ?>
        <?php foreach ($dataRanapObat as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->hargajual)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalRanapObat = $totalRanapObat + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>

        <?php $totalFarmasiObat = 0; ?>
        <?php foreach ($dataFarmasiObat as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopenjualan?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->hargajual)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalFarmasiObat = $totalFarmasiObat + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>

        <?php $totalFarmasiReturObat = 0; ?>
        <?php foreach ($dataFarmasiReturObat as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopengembalian?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->hargajual)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalFarmasiReturObat = $totalFarmasiReturObat + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL RAWAT INAP [OBAT]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalRanapObat + $totalFarmasiObat + $totalFarmasiReturObat)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">RAWAT INAP [ALAT KESEHATAN]</b></td>
        </tr>
        <?php $dataRanapAlkes = $this->Trawatinap_verifikasi_model->viewRincianRanapAlkes($idpendaftaran); ?>
        <?php $dataFarmasiAlkes = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapAlkes($idpendaftaran); ?>
        <?php $dataFarmasiReturAlkes = $this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapAlkes($idpendaftaran); ?>

        <?php if (COUNT($dataRanapAlkes) == 0 and COUNT($dataFarmasiAlkes) == 0 and COUNT($dataFarmasiReturAlkes) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>

        <?php $totalRanapAlkes = 0; ?>
        <?php foreach ($dataRanapAlkes as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->hargajual)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalRanapAlkes = $totalRanapAlkes + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>

        <?php $totalFarmasiAlkes = 0; ?>
        <?php foreach ($dataFarmasiAlkes as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopenjualan?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->hargajual)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalFarmasiAlkes = $totalFarmasiAlkes + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>

        <?php $totalFarmasiReturAlkes = 0; ?>
        <?php foreach ($dataFarmasiReturAlkes as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopengembalian?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->hargajual)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalFarmasiReturAlkes = $totalFarmasiReturAlkes + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL RAWAT INAP [ALAT KESEHATAN]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalRanapAlkes + $totalFarmasiAlkes + $totalFarmasiReturAlkes)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">RAWAT INAP [ALAT KESEHATAN] [ALAT BANTU]</b></td>
        </tr>
        <?php $dataRanapAlkesBantu = $this->Trawatinap_verifikasi_model->viewRincianRanapAlkesBantu($idpendaftaran); ?>
        <?php $dataFarmasiAlkesBantu = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRanapAlkesBantu($idpendaftaran); ?>
        <?php $dataFarmasiReturAlkesBantu = $this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRanapAlkesBantu($idpendaftaran); ?>

        <?php if (COUNT($dataRanapAlkesBantu) == 0 and COUNT($dataFarmasiAlkesBantu) == 0 and COUNT($dataFarmasiReturAlkesBantu) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>

        <?php $totalRanapAlkesBantu = 0; ?>
        <?php foreach ($dataRanapAlkesBantu as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->hargajual)?></td>
            <td class="border-full text-right" colspan="2">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalRanapAlkes = $totalRanapAlkes + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>

        <?php $totalFarmasiAlkesBantu = 0; ?>
        <?php foreach ($dataFarmasiAlkesBantu as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopenjualan?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->harga)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalFarmasiAlkesBantu = $totalFarmasiAlkesBantu + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>

        <?php $totalFarmasiReturAlkesBantu = 0; ?>
        <?php foreach ($dataFarmasiReturAlkesBantu as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopengembalian?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->harga)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalFarmasiReturAlkesBantu = $totalFarmasiReturAlkesBantu + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL RAWAT INAP [ALAT KESEHATAN] [ALAT BANTU]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalRanapAlkesBantu + $totalFarmasiAlkesBantu + $totalFarmasiReturAlkesBantu)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">RAWAT INAP [LAIN-LAIN]</b></td>
        </tr>
        <?php $totalRanapLainLain = 0; ?>
        <?php $dataRanapLainLain = $this->Trawatinap_verifikasi_model->viewRincianRanapTindakan($idpendaftaran, 6); ?>
        <?php if (COUNT($dataRanapLainLain) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataRanapLainLain as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalRanapLainLain = $totalRanapLainLain + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL RAWAT INAP [LAIN-LAIN]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalRanapLainLain)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">IGD</b></td>
        </tr>
        <?php $totalPoli = 0; ?>
        <?php $dataPoli = $this->Trawatinap_verifikasi_model->viewRincianRajalTindakanFilteredIGD($idpendaftaran); ?>
        <?php if (COUNT($dataPoli) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataPoli as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?><?=($row->jasapelayanan > 0 ? ' - ' . $row->namadokter : '')?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalPoli = $totalPoli + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL IGD</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalPoli)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">IGD [OBAT]</b></td>
        </tr>
        <?php $totalPoliObat = 0; ?>
        <?php $totalFarmasiObatIGD = 0; ?>
        <?php $totalFarmasiRajalReturObat = 0; ?>
        <?php $dataPoliObat = $this->Trawatinap_verifikasi_model->viewRincianRajalObatFilteredIGD($idpendaftaran, 2); ?>
        <?php $dataFarmasiObatIGD = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRajal($idpendaftaran, 3); ?>
        <?php $dataFarmasiRajalReturObat = $this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRajalObat($idpendaftaran); ?>
        <?php if (COUNT($dataPoliObat) == 0 && COUNT($dataFarmasiObatIGD) == 0 && COUNT($dataFarmasiRajalReturObat) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataPoliObat as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->hargajual)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalPoliObat = $totalPoliObat + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>

        <?php foreach ($dataFarmasiObatIGD as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopenjualan?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->hargajual)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalFarmasiObatIGD = $totalFarmasiObatIGD + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>

        <?php foreach ($dataFarmasiRajalReturObat as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopengembalian?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->hargajual)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalFarmasiRajalReturObat = $totalFarmasiRajalReturObat + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL IGD [OBAT]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalPoliObat + $totalFarmasiObatIGD + $totalFarmasiRajalReturObat)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">IGD [ALAT KESEHATAN]</b></td>
        </tr>
        <?php $totalPoliAlkes = 0; ?>
        <?php $totalFarmasiALkesIGD = 0; ?>
        <?php $totalFarmasiRajalReturAlkes = 0; ?>
        <?php $dataPoliAlkes = $this->Trawatinap_verifikasi_model->viewRincianRajalAlkesFilteredIGD($idpendaftaran); ?>
        <?php $dataFarmasiAlkesIGD = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRajal($idpendaftaran, 1); ?>
        <?php $dataFarmasiRajalReturAlkes = $this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRajalAlkes($idpendaftaran); ?>

        <?php if (COUNT($dataPoliAlkes) == 0 && COUNT($dataFarmasiAlkesIGD) == 0 && COUNT($dataFarmasiRajalReturAlkes) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataPoliAlkes as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->hargajual)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalPoliAlkes = $totalPoliAlkes + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>

        <?php foreach ($dataFarmasiAlkesIGD as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopenjualan?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->hargajual)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalFarmasiALkesIGD = $totalFarmasiALkesIGD + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>

        <?php foreach ($dataFarmasiRajalReturAlkes as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopengembalian?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->hargajual)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalFarmasiRajalReturAlkes = $totalFarmasiRajalReturAlkes + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL IGD [ALAT KESEHATAN]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalPoliAlkes + $totalFarmasiALkesIGD + $totalFarmasiRajalReturAlkes)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">LABORATORIUM [UMUM]</b></td>
        </tr>
        <?php $totalLab = 0; ?>
        <?php $dataLab = $this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 1); ?>
        <?php if (COUNT($dataLab) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataLab as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->norujukan?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalLab = $totalLab + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL LABORATORIUM [UMUM]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalLab)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">LABORATORIUM [PATHOLOGI ANATOMI]</b></td>
        </tr>
        <?php $totalLabPA = 0; ?>
        <?php $dataLabPA = $this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 2); ?>
        <?php if (COUNT($dataLabPA) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataLabPA as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->norujukan?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalLabPA = $totalLabPA + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL LABORATORIUM [PATHOLOGI ANATOMI]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalLabPA)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">LABORATORIUM [PMI]</b></td>
        </tr>
        <?php $totalLabPMI = 0; ?>
        <?php $dataLabPMI = $this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 3); ?>
        <?php if (COUNT($dataLabPMI) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataLabPMI as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->norujukan?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalLabPMI = $totalLabPMI + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL LABORATORIUM [PMI]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalLabPMI)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">RADIOLOGI [X-RAY]</b></td>
        </tr>
        <?php $totalRadXray = 0; ?>
        <?php $dataRadXray = $this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 1); ?>
        <?php if (COUNT($dataRadXray) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataRadXray as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->norujukan?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalRadXray = $totalRadXray + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL RADIOLOGI [X-RAY]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalRadXray)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">RADIOLOGI [USG]</b></td>
        </tr>
        <?php $totalRadUSG = 0; ?>
        <?php $dataRadUSG = $this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 2); ?>
        <?php if (COUNT($dataRadUSG) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataRadUSG as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->norujukan?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalRadUSG = $totalRadUSG + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL RADIOLOGI [USG]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalRadUSG)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">RADIOLOGI [CT SCAN]</b></td>
        </tr>
        <?php $totalRadCTScan = 0; ?>
        <?php $dataRadCTScan = $this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 3); ?>
        <?php if (COUNT($dataRadCTScan) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataRadCTScan as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->norujukan?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalRadCTScan = $totalRadCTScan + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL RADIOLOGI [CT SCAN]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalRadCTScan)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">RADIOLOGI [MRI]</b></td>
        </tr>
        <?php $totalRadMRI = 0; ?>
        <?php $dataRadMRI = $this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 4); ?>
        <?php if (COUNT($dataRadMRI) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataRadMRI as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->norujukan?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalRadMRI = $totalRadMRI + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL RADIOLOGI [MRI]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalRadMRI)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">RADIOLOGI [BMD]</b></td>
        </tr>
        <?php $totalRadBMD = 0; ?>
        <?php $dataRadBMD = $this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 5); ?>
        <?php if (COUNT($dataRadBMD) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataRadBMD as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->norujukan?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalRadBMD = $totalRadBMD + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL RADIOLOGI [BMD]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalRadBMD)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">FISIOTERAPI</b></td>
        </tr>
        <?php $totalFisio = 0; ?>
        <?php $dataFisio = $this->Trawatinap_verifikasi_model->viewRincianFisioterapi($idpendaftaran); ?>
        <?php if (COUNT($dataFisio) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataFisio as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->norujukan?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalFisio = $totalFisio + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL FISIOTERAPI</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalFisio)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">KAMAR OPERASI [SEWA ALAT]</b></td>
        </tr>
        <?php $totalOKSewaAlat = 0; ?>
        <?php $dataOKSewaAlat = $this->Trawatinap_verifikasi_model->viewRincianOperasiSewaAlat($idpendaftaran); ?>
        <?php if (COUNT($dataOKSewaAlat) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataOKSewaAlat as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggaloperasi)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalOKSewaAlat = $totalOKSewaAlat + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL KAMAR OPERASI [SEWA ALAT]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalOKSewaAlat)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">KAMAR OPERASI [ALAT KESEHATAN]</b></td>
        </tr>
        <?php $totalOKAlkes = 0; ?>
        <?php $dataOKAlkes = $this->Trawatinap_verifikasi_model->viewRincianOperasiAlkes($idpendaftaran); ?>
        <?php if (COUNT($dataOKAlkes) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataOKAlkes as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggaloperasi)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->hargajual)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalOKAlkes = $totalOKAlkes + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL KAMAR OPERASI [ALAT KESEHATAN]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalOKAlkes)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">KAMAR OPERASI [OBAT]</b></td>
        </tr>
        <?php $totalOKObat = 0; ?>
        <?php $dataOKObat = $this->Trawatinap_verifikasi_model->viewRincianOperasiObat($idpendaftaran); ?>
        <?php if (COUNT($dataOKObat) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataOKObat as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggaloperasi)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->hargajual)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalOKObat = $totalOKObat + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL KAMAR OPERASI [OBAT]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalOKObat)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">KAMAR OPERASI [OBAT NARCOSE]</b></td>
        </tr>
        <?php $totalOKObatNarcose = 0; ?>
        <?php $dataOKObatNarcose = $this->Trawatinap_verifikasi_model->viewRincianOperasiNarcose($idpendaftaran); ?>
        <?php if (COUNT($dataOKObatNarcose) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataOKObatNarcose as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggaloperasi)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->hargajual)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalOKObatNarcose = $totalOKObatNarcose + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL KAMAR OPERASI [OBAT NARCOSE]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalOKObatNarcose)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">KAMAR OPERASI [IMPLAN]</b></td>
        </tr>
        <?php $totalOKImplan = 0; ?>
        <?php if ($tipe != 'bpjstk') { ?>
        <?php $dataOKImplan = $this->Trawatinap_verifikasi_model->viewRincianOperasiImplan($idpendaftaran); ?>
        <?php } else { ?>
        <?php $dataOKImplan = []; ?>
        <?php } ?>
        <?php if (COUNT($dataOKImplan) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataOKImplan as $row) {?>
        <?php if ($row->statusverifikasi == 1 || $status == 0) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggaloperasi)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->hargajual)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalOKImplan = $totalOKImplan + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL KAMAR OPERASI [IMPLAN]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalOKImplan)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">KAMAR OPERASI [SEWA KAMAR]</b></td>
        </tr>
        <?php $totalOKSewaKamar = 0; ?>
        <?php $dataOKSewaKamar = $this->Trawatinap_verifikasi_model->viewRincianOperasiKamar($idpendaftaran); ?>
        <?php if (COUNT($dataOKSewaKamar) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataOKSewaKamar as $row) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggaloperasi)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalOKSewaKamar = $totalOKSewaKamar + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL KAMAR OPERASI [SEWA KAMAR]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalOKSewaKamar)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">KAMAR OPERASI [JASA MEDIS]</b></td>
        </tr>
        <?php $totalOKJasaMedis = 0; ?>
        <?php $dataOKJasaMedis = $this->Trawatinap_verifikasi_model->viewRincianOperasiJasaMedisPrint($idpendaftaran); ?>
        <?php if (COUNT($dataOKJasaMedis) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataOKJasaMedis as $row) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggaloperasi)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalOKJasaMedis = $totalOKJasaMedis + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL KAMAR OPERASI [JASA MEDIS]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalOKJasaMedis)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">KAMAR OPERASI [JASA ASISTEN]</b></td>
        </tr>
        <?php $totalOKJasaAsisten = 0; ?>
        <?php $dataOKJasaAsisten = $this->Trawatinap_verifikasi_model->viewRincianOperasiJasaAsisten($idpendaftaran); ?>
        <?php if (COUNT($dataOKJasaAsisten) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataOKJasaAsisten as $row) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggaloperasi)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->grandtotal)?></td>
            <?php $totalOKJasaAsisten = $totalOKJasaAsisten + $row->grandtotal; ?>
        </tr>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL KAMAR OPERASI [JASA ASISTEN]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalOKJasaAsisten)?></td>
        </tr>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">ADMINISTRASI [RAWAT JALAN]</b></td>
        </tr>
        <?php $totalAdmRajal = 0; ?>
        <?php $datAdmRajal = $this->Trawatinap_verifikasi_model->viewRincianAdministrasiRawatJalan($idpendaftaran); ?>
        <?php if (COUNT($datAdmRajal) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($datAdmRajal as $row) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center"><?=number_format($row->kuantitas)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->total)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->totalkeseluruhan)?></td>
            <?php $totalAdmRajal = $totalAdmRajal + $row->totalkeseluruhan; ?>
        </tr>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="4" class="border-full text-semi-bold">SUB TOTAL ADMINISTRASI [RAWAT JALAN]</td>
            <td class="border-full text-semi-bold text-right">Rp. <?=number_format($totalAdmRajal)?></td>
        </tr>

        <?php
	$totalSebelumAdm = $totalRanapFullCare + $totalRanapECG + $totalRanapVisite + $totalRanapSewaAlat +
	$totalRanapAmbulance + $totalRanapAlkes + $totalRanapAlkesBantu + $totalRanapLainLain + $totalPoli +
	$totalPoliObat + $totalFarmasiObatIGD + $totalFarmasiRajalReturObat + $totalPoliAlkes + $totalFarmasiALkesIGD + $totalFarmasiRajalReturAlkes + $totalRanapObat + $totalFarmasiObat + $totalFarmasiReturObat + $totalFarmasiAlkes +
	$totalFarmasiReturAlkes + $totalFarmasiAlkesBantu + $totalFarmasiReturAlkesBantu + $totalLab + $totalLabPA + $totalLabPMI +
	$totalRadXray + $totalRadUSG + $totalRadCTScan + $totalRadMRI + $totalRadBMD +
	$totalFisio + $totalOKSewaAlat + $totalOKAlkes + $totalOKObat + $totalOKObatNarcose +
	$totalOKImplan + $totalOKSewaKamar + $totalOKJasaMedis + $totalOKJasaAsisten +
	$totalRanapRuangan + $totalAdmRajal;
	?>

        <tr>
            <td class="border-full" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">ADMINISTRASI [RAWAT INAP]</b></td>
        </tr>
        <?php $totalAdmRanap = 0; ?>
        <?php if ($statusvalidasi == 1) {?>
        <?php $dataAdminRanap = get_all('trawatinap_administrasi', ['idpendaftaran' => $idpendaftaran]); ?>
        <?php if (COUNT($dataAdminRanap) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataAdminRanap as $row) {?>
        <?php $ranap = get_by_field('id', $row->idpendaftaran, 'trawatinap_pendaftaran')?>
        <tr>
            <td class="border-full"><?=$ranap->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($ranap->tanggaldaftar)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center">1</td>
            <td class="border-full text-right">Rp. <?=number_format($row->tarifsetelahdiskon)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->tarifsetelahdiskon)?></td>
            <?php $totalAdmRanap = $totalAdmRanap + $row->tarifsetelahdiskon; ?>
        </tr>
        <?php } ?>
        <?php } else {?>
        <?php $dataAdminRanap = $this->db->query("CALL spAdministrasiRanap($idpendaftaran, $totalSebelumAdm)")->result(); ?>
        <?php if (COUNT($dataAdminRanap) == 0) {?>
        <tr>
            <td class="border-full" colspan="7">-</td>
        </tr>
        <?} ?>
        <?php foreach ($dataAdminRanap as $row) {?>
        <tr>
            <td class="border-full"><?=$row->nopendaftaran?></td>
            <td class="border-full"><?=DMYFormat($row->tanggal)?></td>
            <td class="border-full" colspan="2"><?=$row->namatarif?></td>
            <td class="border-full text-center">1</td>
            <td class="border-full text-right">Rp. <?=number_format($row->tarif)?></td>
            <td class="border-full text-right">Rp. <?=number_format($row->tarif)?></td>
            <?php $totalAdmRanap = $totalAdmRanap + $row->tarif; ?>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="border-full text-bold"></td>
            <td colspan="3" class="border-full text-semi-bold">SUB TOTAL ADMINISTRASI [RAWAT INAP]</td>
            <td colspan="2" class="border-full text-semi-bold text-right">Rp. <?=number_format($totalAdmRanap)?></td>
        </tr>

        <?php $totalKeseluruhan = ceil(($totalSebelumAdm + $totalAdmRanap) / 100) * 100; ?>

        <tr>
            <td colspan="7" class="border-full">&nbsp;</td>
        </tr>

        <tr>
            <td colspan="6" class="border-full"><b>TOTAL BIAYA PERAWATAN + DOKTER</b></td>
            <td class="border-full text-right">Rp. <?=number_format($totalKeseluruhan)?></td>
        </tr>
        <tr>
            <td colspan="6" class="border-full"><b>DISKON</b></td>
            <td class="border-full text-right">Rp. <?=number_format($diskon_pembayaran)?></td>
        </tr>
        <?php if ($tipe == '') { ?>
        <tr>
            <td colspan="6" class="border-full"><b>UANG MUKA/TELAH DIBAYAR</b></td>
            <td class="border-full text-right">Rp. <?=number_format($totaldeposit)?></td>
        </tr>
        <?php } ?>
        <tr>
            <td colspan="6" class="border-full"><b>KURANG/LEBIH/JUMLAH</b></td>
            <?php if ($tipe == '') { ?>
            <td class="border-full text-right">Rp.
                <?php echo($totalKeseluruhan >= $totaldeposit ? number_format($totalKeseluruhan - $totaldeposit - $diskon_pembayaran) : '(' . number_format($totaldeposit - $totalKeseluruhan - $diskon_pembayaran) . ')'); ?></td>
            <?php } elseif ($tipe == 'piutang' || $tipe == 'bpjstk') { ?>
            <td class="border-full text-right">Rp. <?php echo number_format($totalKeseluruhan - $diskon_pembayaran); ?></td>
            <?php } ?>
        </tr>
        <?php if ($tipe != 'bpjstk') { ?>
        <tr>
            <td class="border-full text-center" colspan="7" style="padding-top: 15px; padding-bottom: 15px;"><b class="text-bold">DETAIL PEMBAYARAN</b></td>
        </tr>
        <tr>
            <td colspan="2" class="border-full"><b>NO PEMBAYARAN</b></td>
            <td class="border-full"><b>TIPE PEMBAYARAN</b></td>
            <td colspan="3" class="border-full"><b>KETERANGAN</b></td>
            <td class="border-full"><b>NOMINAL</b></td>
        </tr>
        <?php $total_deposit = 0; ?>
        <?php if ($tipe == '') { ?>
        <?php foreach ($detail_deposit as $row) {?>
        <?php
			if ($row->idmetodepembayaran == 1) {
				$metode = $row->metodepembayaran;
			} else {
				$metode = $row->metodepembayaran . ' : ' . $row->namabank;
			}
		  ?>
        <tr>
            <td class="border-full" colspan="2"><?=$nopendaftaran?></td>
            <td class="border-full text-center">DEPOSIT</td>
            <td class="border-full" colspan="3"><?=$metode?></td>
            <td class="border-full text-right"><?=number_format($row->nominal)?></td>
        </tr>
        <?php $total_deposit = $total_deposit + $row->nominal; ?>
        <?php } ?>
        <?php } ?>

        <?php $total_pembayaran = 0; ?>
        <?php foreach ($detail_pembayaran as $row) {?>
        <tr>
            <td class="border-full" colspan="2"><?=$nopendaftaran?></td>
            <td class="border-full text-center">PEMBAYARAN</td>
            <td class="border-full" colspan="3"><?=$row->keterangan?></td>
            <td class="border-full text-right"><?=number_format($row->nominal)?></td>
        </tr>
        <?php $total_pembayaran = $total_pembayaran + $row->nominal; ?>
        <?php } ?>
        <tr>
            <td colspan="6" class="border-full text-right"><b>TOTAL TERIMA PEMBAYARAN</b></td>
            <td class="border-full text-right"><b><?= number_format($total_pembayaran + $total_deposit) ?></td>
		</tr>
		<?php } ?>
	</table>

	<table>
		<tr>
			<td class="border-full text-center"><b>Diagnosa</b></td>
            <td class="border-full text-center"><b>Operasi</b></td>
            <td class="border-full text-center"><b>Catatan</b></td>
        </tr>
        <tr>
            <td class="border-full" style="padding:10px">
                <?=$diagnosa?>
            </td>
            <td class="border-full" style="padding:10px">
                <?=$operasi?>
            </td>
            <td class="border-full" style="padding:10px">
                <?=$operasi?>
            </td>
        </tr>
        <tr>
            <td class="text-right" colspan="3">
                Bandung, <?=HumanDate($tanggal_pembayaran)?>
            </td>
        </tr>
        <tr>
            <td class="text-right" colspan="3">
                <br>
                <br>
                <br>
            </td>
        </tr>
        <tr>
            <td class="text-right" colspan="3">
                <br>
                <br>
                <br>
            </td>
        </tr>
        <tr>
            <td class="text-right" colspan="3">
                <?= $this->session->userdata('user_name') ?>
            </td>
        </tr>
        <tr>
            <td class="text-right" colspan="3">
                <br>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="font-size: 10px">
                Tanggal Cetak : <?= date('d-M-Y H:i:s') ?>, User Pembayaran : <?= $user_input_pembayaran ?>, User Cetak : <?= $this->session->userdata('user_name') ?>, Cetak Ke : 1
            </td>
        </tr>
    </table>
</body>

</html>
