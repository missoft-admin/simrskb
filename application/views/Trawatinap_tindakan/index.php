<?php echo ErrorSuccess($this->session) ?>
<?php
	if ($error != '') {
		echo ErrorMessage($error);
	}
?>

<style media="screen">
#datatable-simrs td {
    cursor: pointer;
}

#datatable-checkout td {
    cursor: pointer;
}
</style>

<?php if (UserAccesForm($user_acces_form,array('369','1200'))) { ?>
<div class="block">
    <ul class="nav nav-tabs" data-toggle="tabs">
        <?php if (UserAccesForm($user_acces_form,array('369'))) { ?>
        <li class="<?=($tabActive == 1 ? 'active' : '')?>">
            <a href="#tabs-tindakanRawatInap">{title}</a>
        </li>
        <? } ?>
        <?php if (UserAccesForm($user_acces_form,array('1200'))) { ?>
        <li class="<?=($tabActive == 2 ? 'active' : '')?>">
            <a href="#tabs-pasienCheckout">Pasien Telah Checkout</a>
        </li>
        <? } ?>
    </ul>
    <div class="block-content tab-content">
        <?php if (UserAccesForm($user_acces_form,array('369'))) { ?>
        <div class="tab-pane <?=($tabActive == 1 ? 'active' : '')?>" id="tabs-tindakanRawatInap">
            <hr style="margin-top:0px">
            <?php if (UserAccesForm($user_acces_form,array('370'))) { ?>
            <div class="row">
                <?php echo form_open('trawatinap_tindakan/filter', 'class="form-horizontal"') ?>
                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="nomedrec">Ruangan</label>
                        <div class="col-md-8">
                            <select name="idruangan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php if (UserAccesForm($user_acces_form,array('1336'))) { ?>
                                	<option value="#">Semua Ruangan</option>
                                <? } ?>
                                <?php foreach (get_all('mruangan',array('idtipe'=>'1','status'=>'1')) as $row) { ?>
									<?php if (($row->id=='1' && UserAccesForm($user_acces_form,array('1227'))) || ($row->id=='2' && UserAccesForm($user_acces_form,array('1228'))) || ($row->id=='3' && UserAccesForm($user_acces_form,array('1229'))) || ($row->id=='4' && UserAccesForm($user_acces_form,array('1230')))) { ?>
										<option value="<?=$row->id?>" <?=($idruangan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
									<?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="namapasien">Kelas</label>
                        <div class="col-md-8">
                            <select name="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php if (UserAccesForm($user_acces_form,array('1233'))) { ?>
                                	<option value="#">Semua Kelas</option>
                                <?php } ?>
                                <?php foreach (get_all('mkelas',array('status'=>'1')) as $row) { ?>
									<?php if (($row->id=='1' && UserAccesForm($user_acces_form,array('1235'))) || ($row->id=='2' && UserAccesForm($user_acces_form,array('1236'))) || ($row->id=='3' && UserAccesForm($user_acces_form,array('1237'))) || ($row->id=='4' && UserAccesForm($user_acces_form,array('1234')))) { ?>
										<option value="<?=$row->id?>" <?=($idkelas == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
									<?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="namapasien">Bed</label>
                        <div class="col-md-8">
                            <select name="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="#">Semua Bed</option>
                                <?php foreach (get_all('mbed') as $row) { ?>
                                	<option value="<?=$row->id?>" <?=($idbed == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="">Status</label>
                        <div class="col-md-8">
                            <select name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="#">Semua Status</option>
                                <option value="2" <?=($status == 2 ? 'selected="selected"' : '')?>>Kosong</option>
                                <option value="3" <?=($status == 3 ? 'selected="selected"' : '')?>>Isi</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="namapasien">Nama Pasien</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="namapasien" value="{namapasien}">
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="namapasien">Dokter (DPJP)</label>
                        <div class="col-md-8">
                            <select name="iddokterpenanggungjawab" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="#">Semua Dokter</option>
                                <?php foreach (get_all('mdokter') as $row) { ?>
                                	<option value="<?=$row->id?>" <?=($iddokterpenanggungjawab == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="namapasien">Dokter Perujuk</label>
                        <div class="col-md-8">
                            <select name="iddokterperujuk" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="#">Semua Dokter</option>
                                <?php foreach (get_all('mdokter') as $row) { ?>
                                	<option value="<?=$row->id?>" <?=($iddokterperujuk == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for=""></label>
                        <div class="col-md-8">
                            <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close() ?>
            </div>
            <hr style="margin-top:10px">
            <? } ?>

            <table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Bed</th>
                        <th>No. Medrec</th>
                        <th>Nama Pasien</th>
                        <th>Jenis Kelamin</th>
                        <th>Umur</th>
                        <th>Dokter Penanggung Jawab</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <? } ?>

        <?php if (UserAccesForm($user_acces_form,array('1200'))) { ?>
        <div class="tab-pane <?=($tabActive == 2 ? 'active' : '')?>" id="tabs-pasienCheckout">
            <hr style="margin-top:0px">
            <div class="row">
                <?php echo form_open('trawatinap_tindakan/filterCheckout', 'class="form-horizontal"') ?>
                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="nomedrec">Ruangan</label>
                        <div class="col-md-8">
                            <select name="idruangan" id="idruangan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php if (UserAccesForm($user_acces_form,array('1247'))) { ?>
                                	<option value="#">Semua Ruangan</option>
                                <? } ?>
                                <?php foreach (get_all('mruangan',array('idtipe'=>'1','status'=>'1')) as $row) { ?>
									<?php if (($row->id=='1' && UserAccesForm($user_acces_form,array('1243'))) || ($row->id=='2' && UserAccesForm($user_acces_form,array('1244'))) || ($row->id=='3' && UserAccesForm($user_acces_form,array('1246'))) || ($row->id=='4' && UserAccesForm($user_acces_form,array('1245')))) { ?>
										<option value="<?=$row->id?>" <?=($idruangan == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
									<?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="namapasien">Kelas</label>
                        <div class="col-md-8">
                            <select name="idkelas" id="idkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php if (UserAccesForm($user_acces_form,array('1238'))) { ?>
                                	<option value="#">Semua Kelas</option>
                                <?php } ?>
                                <?php foreach (get_all('mkelas',array('status'=>'1')) as $row) { ?>
									<?php if (($row->id=='1' && UserAccesForm($user_acces_form,array('1240'))) || ($row->id=='2' && UserAccesForm($user_acces_form,array('1241'))) || ($row->id=='3' && UserAccesForm($user_acces_form,array('1242'))) || ($row->id=='4' && UserAccesForm($user_acces_form,array('1239')))) { ?>
										<option value="<?=$row->id?>" <?=($idkelas == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
									<?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="namapasien">Bed</label>
                        <div class="col-md-8">
                            <select name="idbed" id="idbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="#">Semua Bed</option>
                                <?php foreach (get_all('mbed') as $row) { ?>
                                	<option value="<?=$row->id?>" <?=($idbed == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="tanggalrujukan">Tanggal Transaksi</label>
                        <div class="col-md-8">
                            <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                <input class="form-control" type="text" name="tanggaldari_transaksi" placeholder="From" value="">
                                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                <input class="form-control" type="text" name="tanggalsampai_transaksi" placeholder="To" value="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="tanggalrujukan">Tanggal Checkout</label>
                        <div class="col-md-8">
                            <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                                <input class="form-control" type="text" name="tanggaldari_checkout" placeholder="From" value="">
                                <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                                <input class="form-control" type="text" name="tanggalsampai_checkout" placeholder="To" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="namapasien">Nama Pasien</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="namapasien" value="{namapasien}">
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="namapasien">Dokter (DPJP)</label>
                        <div class="col-md-8">
                            <select name="iddokterpenanggungjawab" id="iddokterpenanggungjawab" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="#">Semua Dokter</option>
                                <?php foreach (get_all('mdokter') as $row) { ?>
                                	<option value="<?=$row->id?>" <?=($iddokterpenanggungjawab == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="namapasien">Dokter Perujuk</label>
                        <div class="col-md-8">
                            <select name="iddokterperujuk" id="iddokterperujuk" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="#">Semua Dokter</option>
                                <?php foreach (get_all('mdokter') as $row) { ?>
                                	<option value="<?=$row->id?>" <?=($iddokterperujuk == $row->id ? 'selected="selected"' : '')?>><?=$row->nama?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for="">Status</label>
                        <div class="col-md-8">
                            <select name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="#" <?=($status == '#' ? 'selected="selected"' : '')?>>Semua Status</option>
                                <option value="0" <?=($status == '0' ? 'selected="selected"' : '')?>>Menunggu Transaksi</option>
                                <option value="1" <?=($status == '1' ? 'selected="selected"' : '')?>>Telah Transaksi</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-4 control-label" for=""></label>
                        <div class="col-md-8">
                            <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close() ?>
            </div>
            <hr style="margin-top:10px">

            <table class="table table-bordered table-striped table-responsive" id="datatable-checkout">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Tanggal Checkout</th>
                        <th>No. Pendaftaran</th>
                        <th>No. Medrec</th>
                        <th>Nama Pasien</th>
                        <th>Kelas</th>
                        <th>Bed</th>
                        <th>Dokter</th>
                        <th>Kelompok Pasien</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
		<? } ?>
    </div>
</div>
<? } ?>

<!-- Modal Checkout -->
<div class="modal in" id="CheckoutModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-c">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Checkout</h3>
                </div>
                <div class="block-content">
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Tanggal Keluar</label>
                        <div class="col-md-8" style="margin-bottom: 10px;">
                            <input type="text" id="checkoutTanggalKeluar" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="Tanggal Keluar" value="<?=date("d/m/Y")?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Tanggal Kontrol</label>
                        <div class="col-md-8" style="margin-bottom: 10px;">
                            <input type="text" id="checkoutTanggalKontrol" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="Tanggal Kontrol" value="<?=date("d/m/Y")?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Lama Dirawat</label>
                        <div class="col-md-8" style="margin-bottom: 10px;">
                            <input type="text" id="checkoutLamaDirawat" class="form-control" placeholder="Lama Dirawat" value="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">No. Medrec</label>
                        <div class="col-md-8" style="margin-bottom: 10px;">
                            <input type="text" id="checkoutNoMedrec" class="form-control" placeholder="No. Medrec" value="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Nama Pasien</label>
                        <div class="col-md-8" style="margin-bottom: 10px;">
                            <input type="text" id="checkoutNamapasien" class="form-control" placeholder="Nama Pasien" value="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Sebab Luar</label>
                        <div class="col-md-8" style="margin-bottom: 10px;">
                            <select id="checkoutSebabLuar" class="js-select2 form-control input-sm" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <?php foreach ($list_sebab_luar as $row) { ?>
                                <option value="<?=$row->id?>"><?=$row->nama?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Keadaan Pasien Keluar</label>
                        <div class="col-md-8" style="margin-bottom: 10px;">
                            <select id="checkoutKeadaanPasienKeluar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="1" selected>Hidup</option>
                                <option value="2">Mati</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Kondisi Pasien Keluar</label>
                        <div class="col-md-8" style="margin-bottom: 10px;">
                            <select id="checkoutKondisiPasienKeluar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="">Pilih Opsi</option>
                                <option value="1">Sembuh</option>
                                <option value="2">Membaik</option>
                                <option value="3">Belum Sembuh</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="f_checkoutKondisiPasienSaatIni">
                        <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Kondisi Pasien Saat Ini</label>
                        <div class="col-md-8" style="margin-bottom: 10px;">
                            <input type="text" id="checkoutKondisiPasienSaatIni" class="form-control" placeholder="Kondisi pasien saat ini" value="">
                        </div>
                    </div>
                    <div class="form-group" id="f_checkoutCaraPasienKeluar">
                        <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Cara Pasien Keluar</label>
                        <div class="col-md-8" style="margin-bottom: 10px;">
                            <select id="checkoutCaraPasienKeluar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="">Pilih Opsi</option>
                                <option value="1">Pulang</option>
                                <option value="2">Dirujuk</option>
                                <option value="3">Pindah ke RS Lain</option>
                                <option value="4">Pulang Paksa</option>
                                <option value="5">Lari</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="f_checkoutCaraPasienKeluarRsKlinik" hidden>
                        <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Dirujuk Ke</label>
                        <div class="col-md-8" style="margin-bottom: 10px;">
                            <select id="checkoutCaraPasienKeluarRsKlinik" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="">Pilih Opsi</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="f_checkoutCaraPasienKeluarAlasanBatal" hidden>
                        <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Alasan Batal</label>
                        <div class="col-md-8" style="margin-bottom: 10px;">
                            <select id="checkoutCaraPasienKeluarAlasanBatal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="">Pilih Opsi</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="f_checkoutCaraPasienKeluarKeterangan" hidden>
                        <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Keterangan</label>
                        <div class="col-md-8" style="margin-bottom: 10px;">
                            <textarea id="checkoutCaraPasienKeluarKeterangan" class="form-control" placeholder="Keterangan"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Diagnosa</label>
                        <div class="col-md-8" style="margin-bottom: 10px;">
                            <textarea id="checkoutDiagnosa" class="form-control" placeholder="Diagnosa"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Kelompok Diagnosa</label>
                        <div class="col-md-8" style="margin-bottom: 10px;">
                            <select id="checkoutKelompokDiagnosa" class="form-control input-sm" style="width: 100%;" data-placeholder="Pilih Opsi" multiple>
                                <?php foreach ($list_kelompok_diagnosa as $row) { ?>
                                	<option value="<?=$row->id?>"><?=$row->nama?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button id="saveCheckout" class="btn btn-sm btn-primary" type="button" style="width: 20%;">Checkout</button>
                <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;">Tutup</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Dokter Penanggung Jawab -->
<div class="modal in" id="DokterPJModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-popout">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary">
                    <h3 class="block-title">Dokter Penanggung Jawab</h3>
                </div>
                <div class="block-content">
                    <div id="formInputDokterPJ" class="form-group" style="margin-bottom: 5px;">
                        <label class="col-md-2 control-label" for="" style="margin-top: 5px;">Dokter</label>
                        <div class="col-md-10">
                            <select id="iddokterPJ" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                <option value="">Pilih Opsi</option>
                            </select>
                        </div>
                    </div>
                    <br><br>
                    <h5><i class="fa fa-history"></i> History</h5>
                    <hr style="border-top-width: 2px;margin-top: 8px;">
                    <table class="table table-bordered table-striped" width="100%">
                        <thead id="currentDokterPJ">
                        </thead>
                        <tbody id="historyDokterPJ">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button id="saveDokterPJ" class="btn btn-sm btn-success" type="button" style="width: 20%;"><i class="fa fa-check"></i> Simpan</button>
                <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Pindah Bed -->
<div class="modal in" id="BedModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-popout" style="width:65%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary">
                    <h3 class="block-title">Pindah Bed</h3>
                </div>
                <div class="block-content">
                    <div class="col-md-4">
                        <h5>Ruangan Saat Ini</h5>
                        <hr style="border-top-width: 2px;margin-top: 8px;">
                        <div class="form-group" style="margin-bottom: 8px;">
                            <label class="col-md-4 control-label" for="tglpendaftaran" style="margin-bottom: 5px;">Ruangan</label>
                            <div class="col-md-8" style="margin-bottom: 5px;">
                                <input type="text" id="currentRuangan" class="form-control" value="" readonly="true">
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-4 control-label" for="tglpendaftaran" style="margin-bottom: 5px;">Kelas</label>
                            <div class="col-md-8" style="margin-bottom: 5px;">
                                <input type="text" id="currentKelas" class="form-control" value="" readonly="true">
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-4 control-label" for="tglpendaftaran" style="margin-bottom: 10px;">Bed</label>
                            <div class="col-md-8" style="margin-bottom: 10px;">
                                <input type="text" id="currentBed" class="form-control" value="" readonly="true">
                            </div>
                        </div>

                        <div id="formPindahRuangan">
                            <h5>Pindah Ke Ruangan</h5>
                            <hr style="border-top-width: 2px;margin-top: 8px;">
                            <div class="form-group" style="margin-bottom: 5px;">
                                <label class="col-md-4 control-label" for="" style="margin-bottom: 5px;">Tanggal</label>
                                <div class="col-md-8" style="margin-bottom: 5px;">
                                    <input type="text" id="bedTanggalpindah" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" value="<?=date("d/m/Y")?>">
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 5px;">
                                <label class="col-md-4 control-label" for="" style="margin-bottom: 5px;">Petugas</label>
                                <div class="col-md-8" style="margin-bottom: 5px;">
                                    <select id="bedIdpegawai" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                        <option value="">Pilih Opsi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 5px;">
                                <label class="col-md-4 control-label" for="" style="margin-bottom: 5px;">Ruangan</label>
                                <div class="col-md-8" style="margin-bottom: 5px;">
                                    <select id="bedIdruangan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                        <option value="">Pilih Opsi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 5px;">
                                <label class="col-md-4 control-label" for="" style="margin-bottom: 5px;">Kelas</label>
                                <div class="col-md-8" style="margin-bottom: 5px;">
                                    <select id="bedIdkelas" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                        <option value="">Pilih Opsi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 5px;">
                                <label class="col-md-4 control-label" for="" style="margin-bottom: 5px;">Bed</label>
                                <div class="col-md-8" style="margin-bottom: 5px;">
                                    <select id="bedIdbed" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                        <option value="">Pilih Opsi</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <h5><i class="fa fa-history"></i> History</h5>
                        <hr style="border-top-width: 2px;margin-top: 8px;">
                        <table id="historyPindahBed" class="table table-bordered table-striped" style="margin-top: 10px;">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal Dari</th>
                                    <th>Tanggal Sampai</th>
                                    <th>Ruangan</th>
                                    <th>Kelas</th>
                                    <th>Bed</th>
                                    <th>Nama Petugas</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <?php if (UserAccesForm($user_acces_form,array('375'))) { ?>
                	<button id="savePindahBed" class="btn btn-sm btn-success" type="button" style="width: 20%;"><i class="fa fa-check"></i> Simpan</button>
                <? } ?>
                <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Estimasti Biaya -->
<div class="modal in" id="EstimasiModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-popout" style="width:70%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary">
                    <h3 class="block-title">Estimasi Biaya</h3>
                </div>
                <div class="block-content">
                    <table id="historyEstimasiBiaya" class="table table-bordered table-striped" style="margin-top: 10px;">
                        <thead>
                            <tr>
                                <th>Tanggal</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                    <div id="viewEstimasiBiaya" style="display:none">
                        <div class="col-md-5">
                            <button class="btn btn-sm btn-default closeDetailEstimasiBiaya" href="#"><i class="fa fa-reply"></i> Kembali</button>
                            <div class="col-md-12" style="margin-top: 90px; text-align:center">
                                <a id="vEstimasiScan" class="img-link" href="">
									<i class="fa fa-file-photo-o" style="font-size: 70px;"></i>
								</a>
                            </div>
                        </div>

                        <div class="col-md-7">
                            <div class="input-group" style="width:100%">
                                <span class="input-group-addon" style="width:125px; text-align:left;">No. Medrec</span>
                                <input type="text" id="vEstimasiNomedrec" class="form-control" value="" readonly="true">
                            </div>
                            <div class="input-group" style="margin-top:5px; width:100%">
                                <span class="input-group-addon" style="width:125px; text-align:left;">Nama Pasien</span>
                                <input type="text" id="vEstimasiNamapasien" class="form-control" value="" readonly="true">
                            </div>
                            <div class="input-group" style="margin-top:5px; width:100%">
                                <span class="input-group-addon" style="width:125px; text-align:left;">Umur</span>
                                <input type="text" id="vEstimasiUmur" class="form-control" value="" readonly="true">
                            </div>
                            <div class="input-group" style="margin-top:5px; width:100%">
                                <span class="input-group-addon" style="width:125px; text-align:left;">Dokter</span>
                                <input type="text" id="vEstimasiDokter" class="form-control" value="" readonly="true">
                            </div>
                            <div class="input-group" style="margin-top:5px; width:100%">
                                <span class="input-group-addon" style="width:125px; text-align:left;">Diagnosa</span>
                                <input type="text" id="vEstimasiDiagnosa" class="form-control" value="" readonly="">
                            </div>
                            <div class="input-group" style="margin-top:5px; width:100%">
                                <span class="input-group-addon" style="width:125px; text-align:left;">Operasi</span>
                                <input type="text" id="vEstimasiOperasi" class="form-control" value="" readonly="">
                            </div>
                            <div class="input-group" style="margin-top:5px; width:100%">
                                <span class="input-group-addon" style="width:125px; text-align:left;">Jenis Operasi</span>
                                <input type="text" id="vEstimasiJenisOperasi" class="form-control" value="" readonly="">
                            </div>
                        </div>

                        <div class="col-md-12" style="margin-top:10px">
                            <hr>
                        </div>

                        <div class="col-md-6">
                            <h5><b>KAMAR OPERASI</b></h5>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="" style="margin-top:5px">Sewa Kamar</label>
                                <div class="col-md-8" style="padding-left: 0px;">
                                    <div class="input-group">
                                        <span class="input-group-addon"><center><b>+ -</b></center></span>
                                        <input type="text" id="vEstimasiSewaKamar" class="form-control number" placeholder="Sewa Kamar" value="" readonly="">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="" style="margin-top:5px">Dokter Operator</label>
                                <div class="col-md-8" style="padding-left: 0px;">
                                    <div class="input-group" style="margin-top:5px">
                                        <span class="input-group-addon"><center><b>+ -</b></center></span>
                                        <input type="text" id="vEstimasiJasaMedisOperator" class="form-control number" value="" placeholder="Dokter Operator" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="" style="margin-top:5px">Dokter Anesthesi</label>
                                <div class="col-md-8" style="padding-left: 0px;">
                                    <div class="input-group" style="margin-top:5px">
                                        <span class="input-group-addon"><center><b>+ -</b></center></span>
                                        <input type="text" id="vEstimasiJasaMedisAnesthesi" class="form-control number" value="" placeholder="Dokter Anesthesi" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="" style="margin-top:5px">Assisten</label>
                                <div class="col-md-8" style="padding-left: 0px;">
                                    <div class="input-group" style="margin-top:5px">
                                        <span class="input-group-addon"><center><b>+ -</b></center></span>
                                        <input type="text" id="vEstimasiJasaMedisAssisten" class="form-control number" value="" placeholder="Assisten" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="" style="margin-top:5px">Obat & BHP</label>
                                <div class="col-md-8" style="padding-left: 0px;">
                                    <div class="input-group" style="margin-top:5px">
                                        <span class="input-group-addon"><center><b>+ -</b></center></span>
                                        <input type="text" id="vEstimasiObatBhp" class="form-control number" placeholder="Obat & BHP" value="" readonly="">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <h5><b>PERAWATAN</b></h5>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="">Ruang Perawatan</label>
                                <div class="col-md-8" style="padding-left: 0px;">
                                    <div class="col-md-2" style="padding-left: 0px;"><input type="text" id="vEstimasiQtyRuangPerawatan" class="form-control number" value="" readonly=""></div>
                                    <div class="input-group">
                                        <span class="input-group-addon"><center><b>+ -</b></center></span>
                                        <input type="text" id="vEstimasiRuangPerawatan" class="form-control number" value="" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="" style="margin-top:5px">HCU</label>
                                <div class="col-md-8" style="padding-left: 0px; ">
                                    <div class="col-md-2" style="padding-left: 0px; margin-top:5px"><input type="text" id="vEstimasiQtyHCU" class="form-control number" value="" readonly=""></div>
                                    <div class="input-group" style="margin-top:5px">
                                        <span class="input-group-addon"><center><b>+ -</b></center></span>
                                        <input type="text" id="vEstimasiHCU" class="form-control number" value="" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="" style="margin-top:5px">Obat & Perawatan</label>
                                <div class="col-md-8" style="padding-left: 0px;">
                                    <div class="input-group" style="margin-top:5px">
                                        <span class="input-group-addon"><center><b>+ -</b></center></span>
                                        <input type="text" id="vEstimasiObatPerawatan" class="form-control number" value="" readonly="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="" style="margin-top:5px">Penunjang Lain</label>
                                <div class="col-md-8" style="padding-left: 0px;">
                                    <div class="input-group" style="margin-top:5px">
                                        <span class="input-group-addon"><center><b>+ -</b></center></span>
                                        <input type="text" id="vEstimasiPenunjang" class="form-control number" value="" readonly="">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <hr style="margin-bottom: 8px;">
                            <h5><b>IMPLAN</b></h5>
                            <hr style="margin-bottom: 8px;margin-top: 8px;">
                            <table id="historyEstimasiImplant" class="table table-bordered table-striped" style="margin-top: 10px;">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Harga Satuan</th>
                                        <th>Kuantitas</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Tindakan, BHP, Visite Dokter -->
<div class="modal in" id="AksiModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width:90%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary">
                    <h3 class="block-title">AKSI</h3>
                </div>
                <div class="block-content" style="margin-bottom: 10px;padding-top: 10px;">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tabTindakan" data-toggle="tab">Tindakan</a>
                        </li>
                        <li>
                            <a href="#tabAlkes" data-toggle="tab">Alat Kesehatan</a>
                        </li>
                        <li>
                            <a href="#tabObat" data-toggle="tab">Obat</a>
                        </li>
                        <li>
                            <a href="#tabVisiteDokter" data-toggle="tab">Visite Dokter</a>
                        </li>
                    </ul>

                    <div class="tab-content ">
                        <div class="tab-pane active" id="tabTindakan">
                            <div class="input-group" style="margin-top:15px; width:100%">
                                <span class="input-group-addon" style="width:125px; text-align:left;">No. Medrec</span>
                                <input type="text" class="form-control tindakanNoMedrec" value="" readonly="true">
                            </div>
                            <div class="input-group" style="margin-top:5px; width:100%">
                                <span class="input-group-addon" style="width:125px; text-align:left;">Nama Pasien</span>
                                <input type="text" class="form-control tindakanNamaPasien" value="" readonly="true">
                            </div>
                            <br>
                            <input type="hidden" class="tindakanIdPendaftaran" readonly="true" value="">
                            <input type="hidden" class="tindakanIdKelompokPasien" readonly="true" value="">
                            <table id="historyTindakan" class="table table-bordered table-striped" width="100%" style="margin-bottom: 0px;">
                                <thead>
                                    <tr>
                                        <th style="display:none;">
                                            <input type="text" id="fTindakanRowId" readonly value="">
                                        </th>
                                        <th style="width:10%">
                                            <input type="text" id="fTindakanTanggal" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="Tanggal" value="<?=date("d/m/Y")?>">
                                        </th>
                                        <th style="width:10%">
                                            <?php if (UserAccesForm($user_acces_form,array('381'))) { ?>
                                            <div class="input-group">
											<? } ?>
											<select id="fTindakanId" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
												<option value="">Pilih Opsi</option>
											</select>
											<?php if (UserAccesForm($user_acces_form,array('381'))) { ?>
											<span id="fTindakanSearch" class="btn btn-primary input-group-addon" data-toggle="modal" data-target="#SubTindakanModal"><i class="fa fa-search"></i></span>
											<? } ?>
											<?php if (UserAccesForm($user_acces_form,array('381'))) { ?>
                                            </div>
                                            <? } ?>
                                        </th>
                                        <th style="width:10%">
                                            <input type="hidden" id="fTindakanJasaSarana" value="" readonly="true">
                                            <input type="hidden" id="fTindakanJasaPelayanan" value="" readonly="true">
                                            <input type="hidden" id="fTindakanBHP" value="" readonly="true">
                                            <input type="hidden" id="fTindakanBiayaPerawatan" value="" readonly="true">
                                            <input type="text" class="form-control number" id="fTindakanTarif" placeholder="Tarif" value="" readonly="true">
                                        </th>
                                        <th style="width:10%">
                                            <input type="text" class="form-control number" id="fTindakanKuantitas" placeholder="Kuantitas" value="">
                                        </th>
                                        <th style="width:10%">
                                            <input type="text" class="form-control number" id="fTindakanJumlah" placeholder="Jumlah" value="" readonly="true">
                                        </th>
                                        <th style="width:10%">
                                            <select id="fTindakanIdDokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Dokter">
                                                <option value="">Pilih Dokter</option>
                                            </select>
                                        </th>
                                        <th id="actionDataTindakan" style="width:10%">
                                            <?php if (UserAccesForm($user_acces_form,array('378'))) { ?>
                                            <button id="saveDataTindakan" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</button>
                                            <? } ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Tindakan</th>
                                        <th>Tarif</th>
                                        <th>Kuantitas</th>
                                        <th>Jumlah</th>
                                        <th>Dokter</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr style="background-color:white;">
                                        <td colspan="4" style="text-align:right;text-transform:uppercase;"><b>Total</b></td>
                                        <td id="fTindakanTotal" style="font-weight:bold;"></td>
                                        <td colspan="3" rowspan="3"></td>
                                    </tr>
                                    <tr style="background-color:white;">
                                        <td colspan="4" style="text-align:right;text-transform:uppercase;"><b>Total Deposit</b></td>
                                        <td id="fTindakanTotalDeposit" style="font-weight:bold;"></td>
                                    </tr>
                                    <tr style="background-color:white;">
                                        <td colspan="4" style="text-align:right;text-transform:uppercase;"><b>Sisa Deposit</b></td>
                                        <td id="fTindakanSisaDeposit" style="font-weight:bold;"></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                        <div class="tab-pane" id="tabAlkes">
                            <div class="input-group" style="margin-top:15px; width:100%">
                                <span class="input-group-addon" style="width:125px; text-align:left;">No. Medrec</span>
                                <input type="text" class="form-control tindakanNoMedrec" value="" readonly="true">
                            </div>
                            <div class="input-group" style="margin-top:5px; width:100%">
                                <span class="input-group-addon" style="width:125px; text-align:left;">Nama Pasien</span>
                                <input type="text" class="form-control tindakanNamaPasien" value="" readonly="true">
                            </div>
                            <br>
                            <table id="historyAlkes" class="table table-bordered table-striped" width="100%" style="margin-bottom: 0px;">
                                <thead>
                                    <tr>
                                        <th style="display:none">
                                            <input type="text" id="fAlkesRowId" readonly value="">
                                        </th>
                                        <th style="width:10%">
                                            <input type="text" id="fAlkesTanggal" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="Tanggal" value="<?=date("d/m/Y")?>">
                                        </th>
                                        <th style="width:10%">
                                            <?php if (UserAccesForm($user_acces_form,array('385'))) { ?>
                                            <div class="input-group">
											<? } ?>
											<input type="hidden" id="fAlkesId" value="" readonly="true">
											<input type="text" class="form-control" id="fAlkesNama" placeholder="Alat Kesehatan" value="" readonly="true">
											<?php if (UserAccesForm($user_acces_form,array('385'))) { ?>
                                                <span class="btn btn-primary input-group-addon" id="fAlkesSubModal" data-toggle="modal" data-target="#SubAlkesModal"><i class="fa fa-search"></i></span>
                                            </div>
                                            <? } ?>
                                        </th>
                                        <th style="width:10%">
                                            <input type="hidden" id="fAlkesHargaDasar" value="" readonly="true">
                                            <input type="hidden" id="fAlkesMargin" value="" readonly="true">
                                            <input type="text" class="form-control number" id="fAlkesHargaJual" placeholder="Tarif" value="" readonly="true">
                                        </th>
                                        <th style="width:10%">
                                            <input type="text" class="form-control number" id="fAlkesKuantitas" placeholder="Kuantitas" value="">
                                        </th>
                                        <th style="width:10%">
                                            <input type="text" class="form-control number" id="fAlkesTotal" placeholder="Jumlah" value="" readonly="true">
                                        </th>
                                        <th style="width:10%">
                                            <input type="hidden" id="fAlkesIdUnitPelayanan" value="" readonly="true">
                                            <input type="text" class="form-control" id="fAlkesUnitPelayanan" placeholder="Unit Pelayanan" value="" readonly="true">
                                        </th>
                                        <th id="actionDataAlkes" style="width:10%">
                                            <?php if (UserAccesForm($user_acces_form,array('382'))) { ?>
                                            <button id="saveDataAlkes" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</button>
                                            <? } ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Alat Kesehatan</th>
                                        <th>Tarif</th>
                                        <th>Kuantitas</th>
                                        <th>Jumlah</th>
                                        <th>Unit Pelayanan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr style="background-color:white;">
                                        <td colspan="4" style="text-align:right;text-transform:uppercase;"><b>Total</b></td>
                                        <td id="alkesTotal" style="font-weight:bold;"></td>
                                        <td colspan="2"></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                        <div class="tab-pane" id="tabObat">
                            <div class="input-group" style="margin-top:15px; width:100%">
                                <span class="input-group-addon" style="width:125px; text-align:left;">No. Medrec</span>
                                <input type="text" class="form-control tindakanNoMedrec" value="" readonly="true">
                            </div>
                            <div class="input-group" style="margin-top:5px; width:100%">
                                <span class="input-group-addon" style="width:125px; text-align:left;">Nama Pasien</span>
                                <input type="text" class="form-control tindakanNamaPasien" value="" readonly="true">
                            </div>
                            <br>
                            <table id="historyObat" class="table table-bordered table-striped" width="100%" style="margin-bottom: 0px;">
                                <thead>
                                    <tr>
                                        <th style="display:none">
                                            <input type="text" id="fObatRowId" readonly value="">
                                        </th>
                                        <th style="width:10%">
                                            <input type="text" id="fObatTanggal" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="Tanggal" value="<?=date("d/m/Y")?>">
                                        </th>
                                        <th style="width:10%">
                                            <?php if (UserAccesForm($user_acces_form,array('385'))) { ?>
                                            <div class="input-group">
											<? } ?>
											<input type="hidden" id="fObatId" value="" readonly="true">
											<input type="text" class="form-control" id="fObatNama" placeholder="Obat" value="" readonly="true">
											<?php if (UserAccesForm($user_acces_form,array('385'))) { ?>
											<span class="btn btn-primary input-group-addon" id="fObatSubModal" data-toggle="modal" data-target="#SubObatModal"><i class="fa fa-search"></i></span>
                                            </div>
                                            <? } ?>
                                        </th>
                                        <th style="width:10%">
                                            <input type="hidden" id="fObatHargaDasar" value="" readonly="true">
                                            <input type="hidden" id="fObatMargin" value="" readonly="true">
                                            <input type="text" class="form-control number" id="fObatHargaJual" placeholder="Tarif" value="" readonly="true">
                                        </th>
                                        <th style="width:10%">
                                            <input type="text" class="form-control number" id="fObatKuantitas" placeholder="Kuantitas" value="">
                                        </th>
                                        <th style="width:10%">
                                            <input type="text" class="form-control number" id="fObatTotal" placeholder="Jumlah" value="" readonly="true">
                                        </th>
                                        <th style="width:10%">
                                            <input type="hidden" id="fObatIdUnitPelayanan" value="" readonly="true">
                                            <input type="text" class="form-control" id="fObatUnitPelayanan" placeholder="Unit Pelayanan" value="" readonly="true">
                                        </th>
                                        <th id="actionDataObat" style="width:10%">
                                            <?php if (UserAccesForm($user_acces_form,array('382'))) { ?>
                                            <button id="saveDataObat" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</button>
                                            <? } ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Obat</th>
                                        <th>Tarif</th>
                                        <th>Kuantitas</th>
                                        <th>Jumlah</th>
                                        <th>Unit Pelayanan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr style="background-color:white;">
                                        <td colspan="4" style="text-align:right;text-transform:uppercase;"><b>Total</b></td>
                                        <td id="obatTotal" style="font-weight:bold;"></td>
                                        <td colspan="2"></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                        <div class="tab-pane" id="tabVisiteDokter">
                            <div class="input-group" style="margin-top:15px; width:100%">
                                <span class="input-group-addon" style="width:125px; text-align:left;">No. Medrec</span>
                                <input type="text" class="form-control tindakanNoMedrec" value="" readonly="true">
                            </div>
                            <div class="input-group" style="margin-top:5px; width:100%">
                                <span class="input-group-addon" style="width:125px; text-align:left;">Nama Pasien</span>
                                <input type="text" class="form-control tindakanNamaPasien" value="" readonly="true">
                            </div>
                            <br>
                            <table id="historyVisiteDokter" class="table table-bordered table-striped" width="100%" style="margin-bottom: 0px;">
                                <thead>
                                    <tr>
                                        <th style="display:none">
                                            <input type="text" id="fVisiteRowId" readonly value="">
                                            <input type="text" id="fVisiteIdTarif" readonly value="">
                                        </th>
                                        <th style="width:10%">
                                            <input type="text" id="fVisiteTanggal" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="Tanggal" value="<?=date("d/m/Y")?>">
                                        </th>
                                        <th style="width:10%">
                                            <select id="fVisiteIdDokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Dokter">
                                                <option value="">Pilih Dokter</option>
                                            </select>
                                        </th>
                                        <th style="width:10%">
                                            <input type="hidden" class="form-control" id="fVisiteIdRuangan" placeholder="ID Ruangan" value="" readonly="true">
                                            <input type="hidden" class="form-control" id="fVisiteIdKelas" placeholder="ID Kelas" value="" readonly="true">
                                            <input type="text" class="form-control" id="fVisiteRuangan" placeholder="Ruangan" value="" readonly="true">
                                        </th>
                                        <th style="width:10%">
                                            <input type="hidden" class="form-control" id="fVisiteJasaSarana" placeholder="ID Ruangan" value="" readonly="true">
                                            <input type="hidden" class="form-control" id="fVisiteJasaPelayanan" placeholder="ID Ruangan" value="" readonly="true">
                                            <input type="hidden" class="form-control" id="fVisiteBHP" placeholder="ID Ruangan" value="" readonly="true">
                                            <input type="hidden" class="form-control" id="fVisiteJasaBiayaPerawatan" placeholder="ID Ruangan" value="" readonly="true">
                                            <input type="text" class="form-control number" id="fVisiteTarif" placeholder="Tarif" value="" readonly="true">
                                        </th>
                                        <th id="actionDataVisite" style="width:10%">
                                            <?php if (UserAccesForm($user_acces_form,array('386'))) { ?>
                                            <button id="saveDataVisiteDokter" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</button>
                                            <? } ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Dokter</th>
                                        <th>Ruangan</th>
                                        <th>Tarif</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr style="background-color:white;">
                                        <td colspan="3" style="text-align:right;text-transform:uppercase;"><b>Total</b></td>
                                        <td id="visiteTotal" style="font-weight:bold;"></td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Operasi -->
<div class="modal in" id="OperasiModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" style="width:60%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-c">
                <div class="block-header bg-primary">
                    <h3 class="block-title">Operasi</h3>
                </div>
                <div class="block-content">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tabDaftarOperasi" id="actionTabDaftarOperasi" data-toggle="tab">Daftar Operasi</a>
                        </li>
                        <?php if (UserAccesForm($user_acces_form,array('392'))) { ?>
                        <li>
                            <a href="#tabDataOperasi" id="actionTabDataOperasi" data-toggle="tab">Data Operasi</a>
                        </li>
                        <? } ?>
                    </ul>
                    <div class="tab-content ">
                        <div class="tab-pane active" id="tabDaftarOperasi">
                            <div id="StatusOperasiDisabled" class="alert alert-danger alert-dismissable" style="margin:10px; display:none;">
                                <h3 class="font-w300 push-15">Opps!</h3>
                                <p>Tidak Dapat Mendaftarkan Operasi, Dikarenakan Operasi Tanggal <a id="TanggalOperasiDisabled" class="alert-link" href="javascript:void(0)"></a> belum diproses!</p>
                            </div>

                            <div id="FormOperasi" style="display:none;">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Tanggal Operasi</label>
                                    <div class="col-md-8" style="margin-bottom: 10px;">
                                        <input type="text" id="operasiTanggal" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="Tanggal Operasi" value="<?=date("d/m/Y")?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Nama Pasien</label>
                                    <div class="col-md-8" style="margin-bottom: 10px;">
                                        <input type="hidden" id="operasiIdPasien" value="" readonly>
                                        <input type="text" id="operasiNamapasien" class="form-control" placeholder="Nama Pasien" value="" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Diagnosa</label>
                                    <div class="col-md-8" style="margin-bottom: 10px;">
                                        <input type="text" id="operasiDiagnosa" class="form-control" placeholder="Diagnosa">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Operasi</label>
                                    <div class="col-md-8" style="margin-bottom: 10px;">
                                        <input type="text" id="operasiOperasi" class="form-control" placeholder="Operasi" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Jam Operasi</label>
                                    <div class="col-md-8" style="margin-bottom: 10px;">
                                        <input type="text" id="operasiJam" class="form-control" placeholder="Jam Operasi" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Nama Petugas</label>
                                    <div class="col-md-8" style="margin-bottom: 10px;">
                                        <input type="text" id="operasiNamaPetugas" class="form-control" placeholder="Nama Petugas" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="" style="margin-bottom: 10px;">Catatan</label>
                                    <div class="col-md-8" style="margin-bottom: 10px;">
                                        <textarea id="operasiCatatan" class="form-control" placeholder="Catatan"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if (UserAccesForm($user_acces_form,array('392'))) { ?>
                        <div class="tab-pane" id="tabDataOperasi">
                            <table id="historyDaftarOperasi" class="table table-bordered table-striped" style="margin-top: 10px;">
                                <thead>
                                    <tr>
                                        <th>Tanggal Operasi</th>
                                        <th>Diagnosa</th>
                                        <th>Operasi</th>
                                        <th>Jam Operasi</th>
                                        <th>Nama Petugas</th>
                                        <th>Catatan</th>
                                        <th>Status</th>
                                        <th>Tanggal Disetujui</th>
                                        <th>User Menyetujui</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <? } ?>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <?php if (UserAccesForm($user_acces_form,array('391'))) { ?>
                <button id="saveDaftarOperasi" class="btn btn-sm btn-primary" type="button" style="width: 20%;">Simpan</button>
                <? } ?>
                <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;">Tutup</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Tindakan Rawat Inap -->
<div class="modal fade in black-overlay" id="SubTindakanModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="margin-top: 150px; padding: 0 !important;">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Daftar Tindakan</h3>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-md-12 form-horizontal">
                            <div class="form-val">
                                <label class="col-sm-4" for="idtipe">Tipe</label>
                                <div class="col-sm-8">
                                    <select name="idtipe" id="subTindakanIdTipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                        <option value="1">Full Care</option>
                                        <option value="2">ECG</option>
                                        <option value="4">Sewa Alat</option>
                                        <option value="5">Ambulance</option>
                                        <option value="6">Lain-lain</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table width="100%" id="listTindakanTable" class="table table-striped table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Jasa Sarana</th>
                                <th>Jasa Pelayanan</th>
                                <th>BHP</th>
                                <th>Biaya Perawatan</th>
                                <th>Total Tarif</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Sub Alat Kesehatan -->
<div class="modal fade in black-overlay" id="SubAlkesModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="margin-top: 150px; padding: 0 !important;">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Daftar Alat Kesehatan</h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 form-horizontal">
                            <div class="form-val">
                                <label class="col-sm-4" for="">Unit Pelayanan</label>
                                <div class="col-sm-8">
                                    <select id="subAlkesIdUnit" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                        <?php foreach ($this->Trawatinap_tindakan_model->getListUnitPelayanan() as $row) { ?>
											<?php $idp = $this->Trawatinap_tindakan_model->getDefaultUnitPelayananUser(); ?>
											<?php if ($idp == $row->id) { ?>
												<option value="<?=$row->id?>" selected="selected"><?=$row->nama?></option>
											<?php } else { ?>
												<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <table width="100%" id="listAlkesTable" class="table table-striped table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Kode Alkes</th>
                                <th>Nama Alkes</th>
                                <th>Satuan</th>
                                <th>Tarif</th>
                                <th>Stok</th>
                                <th>Kartu Stok</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Sub Obat -->
<div class="modal fade in black-overlay" id="SubObatModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="margin-top: 150px; padding: 0 !important;">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Daftar Obat</h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 form-horizontal">
                            <div class="form-val">
                                <label class="col-sm-4" for="">Unit Pelayanan</label>
                                <div class="col-sm-8">
                                    <select id="subObatIdUnit" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                        <?php foreach ($this->Trawatinap_tindakan_model->getListUnitPelayanan() as $row) { ?>
											<?php $idp = $this->Trawatinap_tindakan_model->getDefaultUnitPelayananUser()?>
											<?php if ($idp == $row->id) { ?>
												<option value="<?=$row->id?>" selected="selected"><?=$row->nama?></option>
											<?php } else { ?>
												<option value="<?=$row->id?>"><?=$row->nama?></option>
											<?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <table width="100%" id="listObatTable" class="table table-striped table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Kode Obat</th>
                                <th>Nama Obat</th>
                                <th>Satuan</th>
                                <th>Tarif</th>
                                <th>Stok</th>
                                <th>Kartu Stok</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Rincia -->
<div class="modal in" id="RincianModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width:80%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title titleRincian">RINCIAN</h3>
                </div>
                <div class="block-content" style="margin-bottom: 10px;padding-top: 10px;">
                    <ul class="nav nav-tabs">
                        <?php if (UserAccesForm($user_acces_form,array('394'))) { ?>
                        <li class="active">
                            <a href="#tabDeposit" data-toggle="tab">Deposit</a>
                        </li>
                        <? } ?>
                        <?php if (UserAccesForm($user_acces_form,array('399'))) { ?>
                        <li>
                            <a href="#tabEstimasi" data-toggle="tab">Show Estimasi</a>
                        </li>
                        <? } ?>
                    </ul>
                    <?php if (UserAccesForm($user_acces_form,array('394'))) { ?>
                    <div class="tab-content ">
                        <div class="tab-pane active" id="tabDeposit">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group" style="margin-top:15px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">No. Medrec</span>
                                        <input type="text" class="form-control tindakanNoMedrec" value="" readonly="true">
                                    </div>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">Nama Pasien</span>
                                        <input type="text" class="form-control tindakanNamaPasien" value="" readonly="true">
                                    </div>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">No. Telp</span>
                                        <input type="text" class="form-control tindakanNoTelepon" value="" readonly="true">
                                    </div>
                                    <hr>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">Penanggung Jawab</span>
                                        <input type="text" class="form-control tindakanPenanggungJawab" value="" readonly="true">
                                    </div>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">No. Telepon</span>
                                        <input type="text" class="form-control tindakanPenanggungJawabTelepon" value="" readonly="true">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group" style="margin-top:15px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">Kelompok Pasien</span>
                                        <input type="text" class="form-control tindakanKelompokPasien" value="" readonly="true">
                                    </div>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">Nama Perusahaan</span>
                                        <input type="text" class="form-control tindakanNamaPerusahaan" value="" readonly="true">
                                    </div>
                                    <hr>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">Ruangan</span>
                                        <input type="text" class="form-control tindakanRuangan" value="" readonly="true">
                                    </div>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">Kelas</span>
                                        <input type="text" class="form-control tindakanKelas" value="" readonly="true">
                                    </div>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:150px; text-align:left;">Bed</span>
                                        <input type="text" class="form-control tindakanBed" value="" readonly="true">
                                    </div>
                                </div>
                            </div>

							<hr>

                            <div class="form-group" style="margin-bottom: 8px;">
                                <label class="col-md-4 control-label">Tanggal</label>
                                <div class="col-md-8" style="margin-bottom: 5px;">
                                    <input type="text" id="depositTanggal" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="Tanggal" value="<?=date("d/m/Y")?>">
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 8px;">
                                <label class="col-md-4 control-label">Metode Pembayaran</label>
                                <div class="col-md-8" style="margin-bottom: 5px;">
                                    <select class="js-select2 form-control" id="depositIdMetodePembayaran" style="width: 100%;" data-placeholder="Pilih Opsi">
                                        <option value=""></option>
                                        <option value="1">Tunai</option>
                                        <option value="2">Debit</option>
                                        <option value="3">Kredit</option>
                                        <option value="4">Transfer</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" style="display: none;margin-bottom:8px;" id="depositMetodeBank">
                                <label class="col-md-4 control-label">Pembayaran</label>
                                <div class="col-md-8" style="margin-bottom: 5px;">
                                    <select class="js-select2 form-control" id="depositIdBank" style="width: 100%;" data-placeholder="Pilih Opsi">
                                        <option value="">Pilih Opsi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom:8px">
                                <label class="col-md-4 control-label" for="">Nominal</label>
                                <div class="col-md-8" style="margin-bottom: 5px;">
                                    <input type="text" class="form-control number" id="depositNominal" placeholder="Nominal" value="">
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom:8px">
                                <label class="col-md-4 control-label" for="">Terima Dari</label>
                                <div class="col-md-8" style="margin-bottom: 5px;">
                                    <input type="text" class="form-control" id="depositTerimaDari" placeholder="Terima Dari" value="">
                                </div>
                            </div>
                            <?php if (UserAccesForm($user_acces_form,array('395'))) { ?>
                            <div class="form-group" style="margin-bottom:8px">
                                <label class="col-md-4 control-label" for="">&nbsp;</label>
                                <div class="col-md-8" style="margin-bottom: 5px;">
                                    <button id="saveDataDeposit" class="btn btn-sm btn-success" type="button"><i class="fa fa-plus-circle"></i> Tambah</button>
                                </div>
                            </div>
                            <? } ?>

                            <div class="col-md-12">
                                <hr style="margin-top:10px; margin-bottom:5px;">
                            </div>

                            <?php if (UserAccesForm($user_acces_form,array('398'))) { ?>
                            <a href="" target="_blank" id="historyPrintAll" class="btn btn-sm btn-success" style="width: 20%;margin-top: 10px;margin-bottom: 10px;float:right"><i class="fa fa-print"></i> Print All</a>
                            <? } ?>
                            <button class="btn btn-sm btn-success" type="button" style="width: 20%;margin-top: 10px;margin-bottom: 10px;margin-right: 5px;float:right" disabled><i class="fa fa-money"></i> Refund</button>

                            <table id="historyDeposit" class="table table-bordered table-striped" style="margin-top: 10px;">
                                <thead>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Metode</th>
                                        <th>Nominal</th>
                                        <th>Terima Dari</th>
                                        <th>User Input</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr style="background-color:white;">
                                        <td colspan="4" style="text-align:right;text-transform:uppercase;"><b>Total</b></td>
                                        <td id="depositTotal" style="font-weight:bold;"></td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <? } ?>

                        <?php if (UserAccesForm($user_acces_form,array('399'))) { ?>
                        <div class="tab-pane" id="tabEstimasi">
                            <div id="rincianViewHeadEstimasiBiaya">
                                <div class="input-group" style="margin-top:15px; width:100%">
                                    <span class="input-group-addon" style="width:125px; text-align:left;">No. Medrec</span>
                                    <input type="text" class="form-control tindakanNoMedrec" value="" readonly="true">
                                </div>
                                <div class="input-group" style="margin-top:5px; width:100%">
                                    <span class="input-group-addon" style="width:125px; text-align:left;">Nama Pasien</span>
                                    <input type="text" class="form-control tindakanNamaPasien" value="" readonly="true">
                                </div>
                                <br>
                                <?php if (UserAccesForm($user_acces_form,array('400'))) { ?>
                                <button id="rincianTambahEstimasi" class="btn btn-sm btn-primary rOpenDetailEstimasiBiaya" style="width: 20%;float: right; margin-bottom: 8px;"><i class="fa fa-plus-circle"></i> Tambah</button>
                                <? } ?>
                            </div>

                            <table id="rincianHistoryEstimasiBiaya" class="table table-bordered table-striped" style="margin-top: 10px;">
                                <thead>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <div id="rincianViewEstimasiBiaya" style="display:none">
                                <br>
                                <div class="col-md-5">
                                    <button class="btn btn-sm btn-default rincianCloseDetailEstimasiBiaya" href="#"><i class="fa fa-reply"></i> Kembali</button>
                                    <div class="col-md-12" style="margin-top: 90px; text-align:center">
                                        <a id="rEstimasiScan" class="img-link" href="">
											<i class="fa fa-file-photo-o" style="font-size: 70px;"></i>
										</a>
                                        <div id="rEstimasiUpload" style="display:none">
                                            <div id="rEstimasiPriview"></div>
                                            <div class="input-group" style="display: inline-flex; width: 100%;">
                                                <input type="hidden" id="rEstimasiId" value="">
                                                <input type="file" id="rEstimasiFile" name="file" class="form-control" onchange="previewImage(this);">
                                                <div class="input-group-append">
                                                    <span class="btn btn-primary input-group-text" id="rEstimasiActionUpload">Upload</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-7">
                                    <div class="input-group" style="width:100%">
                                        <span class="input-group-addon" style="width:125px; text-align:left;">No. Medrec</span>
                                        <input type="text" id="rEstimasiNomedrec" class="form-control" value="" readonly="true">
                                    </div>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:125px; text-align:left;">Nama Pasien</span>
                                        <input type="text" id="rEstimasiNamapasien" class="form-control" value="" readonly="true">
                                    </div>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:125px; text-align:left;">Umur</span>
                                        <input type="text" id="rEstimasiUmur" class="form-control" value="" readonly="true">
                                    </div>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:125px; text-align:left;">Dokter</span>
                                        <input type="text" id="rEstimasiDokter" class="form-control" value="" readonly="true">
                                    </div>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:125px; text-align:left;">Diagnosa</span>
                                        <input type="text" id="rEstimasiDiagnosa" class="form-control" placeholder="Diagnosa" value="" readonly="">
                                    </div>
                                    <div class="input-group" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:125px; text-align:left;">Operasi</span>
                                        <input type="text" id="rEstimasiOperasi" class="form-control" placeholder="Operasi" value="" readonly="">
                                    </div>
                                    <div class="input-group fEstimasiView" style="margin-top:5px; width:100%">
                                        <span class="input-group-addon" style="width:125px; text-align:left;">Jenis Operasi</span>
                                        <input type="text" id="rEstimasiJenisOperasi" class="form-control" placeholder="Jenis Operasi" value="" readonly="">
                                    </div>
                                    <div class="input-group fEstimasiInput" style="margin-top:5px; width:100%; display:none">
                                        <span class="input-group-addon" style="width:125px; text-align:left;">Jenis Operasi</span>
                                        <select id="rEstimasiIdJenisOperasi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                            <option value="">Pilih Opsi</option>
                                            <?php foreach (get_all('mjenis_operasi') as $rows) { ?>
                                            <option value="<?=$rows->id?>"><?=$rows->nama?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12" style="margin-top:10px">
                                    <hr>
                                </div>

                                <div class="col-md-6">
                                    <h5><b>KAMAR OPERASI</b></h5>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="" style="margin-top:5px">Sewa Kamar</label>
                                        <div class="col-md-8" style="padding-left: 0px;">
                                            <div class="input-group">
                                                <span class="input-group-addon"><center><b>+ -</b></center></span>
                                                <input type="text" id="rEstimasiSewaKamar" class="form-control number" placeholder="Sewa Kamar" value="" readonly="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="" style="margin-top:5px">Dokter Operator</label>
                                        <div class="col-md-8" style="padding-left: 0px;">
                                            <div class="input-group" style="margin-top:5px">
                                                <span class="input-group-addon"><center><b>+ -</b></center></span>
                                                <input type="text" id="rEstimasiJasaMedisOperator" class="form-control number" value="" placeholder="Dokter Operator" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="" style="margin-top:5px">Dokter Anesthesi</label>
                                        <div class="col-md-8" style="padding-left: 0px;">
                                            <div class="input-group" style="margin-top:5px">
                                                <span class="input-group-addon"><center><b>+ -</b></center></span>
                                                <input type="text" id="rEstimasiJasaMedisAnesthesi" class="form-control number" value="" placeholder="Dokter Anesthesi" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="" style="margin-top:5px">Assisten</label>
                                        <div class="col-md-8" style="padding-left: 0px;">
                                            <div class="input-group" style="margin-top:5px">
                                                <span class="input-group-addon"><center><b>+ -</b></center></span>
                                                <input type="text" id="rEstimasiJasaMedisAssisten" class="form-control number" value="" placeholder="Assisten" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="" style="margin-top:5px">Obat & BHP</label>
                                        <div class="col-md-8" style="padding-left: 0px;">
                                            <div class="input-group" style="margin-top:5px">
                                                <span class="input-group-addon"><center><b>+ -</b></center></span>
                                                <input type="text" id="rEstimasiObatBhp" class="form-control number" placeholder="Obat & BHP" value="" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <h5><b>PERAWATAN</b></h5>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="">Ruang Perawatan</label>
                                        <div class="col-md-8" style="padding-left: 0px;">
                                            <div class="col-md-2" style="padding-left: 0px;"><input type="text" id="rEstimasiQtyRuangPerawatan" class="form-control number" value="" readonly=""></div>
                                            <div class="input-group">
                                                <span class="input-group-addon"><center><b>+ -</b></center></span>
                                                <input type="text" id="rEstimasiRuangPerawatan" class="form-control number" value="" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="" style="margin-top:5px">HCU</label>
                                        <div class="col-md-8" style="padding-left: 0px; ">
                                            <div class="col-md-2" style="padding-left: 0px; margin-top:5px"><input type="text" id="rEstimasiQtyHCU" class="form-control number" value="" readonly=""></div>
                                            <div class="input-group" style="margin-top:5px">
                                                <span class="input-group-addon"><center><b>+ -</b></center></span>
                                                <input type="text" id="rEstimasiHCU" class="form-control number" value="" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="" style="margin-top:5px">Obat & Perawatan</label>
                                        <div class="col-md-8" style="padding-left: 0px;">
                                            <div class="input-group" style="margin-top:5px">
                                                <span class="input-group-addon"><center><b>+ -</b></center></span>
                                                <input type="text" id="rEstimasiObatPerawatan" class="form-control number" value="" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="" style="margin-top:5px">Penunjang Lain</label>
                                        <div class="col-md-8" style="padding-left: 0px;">
                                            <div class="input-group" style="margin-top:5px">
                                                <span class="input-group-addon"><center><b>+ -</b></center></span>
                                                <input type="text" id="rEstimasiPenunjang" class="form-control number" value="" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <hr style="margin-bottom: 8px;">
                                    <h5><b>IMPLAN</b></h5>
                                    <hr style="margin-bottom: 8px;margin-top: 8px;">
                                    <table id="rincianHisotryEstimasiImplant" class="table table-bordered table-striped" style="margin-top: 10px;">
                                        <thead>
                                            <tr class="fEstimasiInput" style="display:none">
                                                <th style="width:10%">
                                                    <select id="fImplanId" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                                        <option value="">Pilih Opsi</option>
                                                    </select>
                                                </th>
                                                <th style="width:10%">
                                                    <input type="hidden" id="fImplanHargaDasar" value="" readonly="true">
                                                    <input type="hidden" id="fImplanMargin" value="" readonly="true">
                                                    <input type="text" class="form-control number" id="fImplanHargaJual" placeholder="Tarif" value="" readonly="true">
                                                </th>
                                                <th style="width:10%">
                                                    <input type="text" class="form-control number" id="fImplanKuantitas" placeholder="Kuantitas" value="">
                                                </th>
                                                <th style="width:10%">
                                                    <select id="fImplanStatus" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                                        <option value="">Pilih Opsi</option>
                                                        <option value="Primary">Primary</option>
                                                        <option value="Alternatif">Alternatif</option>
                                                    </select>
                                                </th>
                                                <th style="width:10%">
                                                    <button id="tambahDataImplan" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</button>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>Nama</th>
                                                <th>Harga Satuan</th>
                                                <th>Kuantitas</th>
                                                <th>Status</th>
                                                <th class="fEstimasiInput" style="display:none">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot class="fEstimasiInput" style="display:none">
                                            <tr>
                                                <th colspan="5"><button id="saveDataEstimasi" class="btn btn-success" type="button"><i class="fa fa-check"></i> Simpan</button></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <input type="hidden" id="fImplanDataArray" value="" readonly="true">
                                </div>

                            </div>
                        </div>
                        <? } ?>
                    </div>

                    <div id="tabEstimasi" style="display: none;">
                        <h5 style="margin-top: 10px;">List Estimasi</h5>
                        <table class="table table-bordered table-striped" style="margin-top: 10px;">
                            <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Id Estimasi</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Hapus Deposit -->
<div class="modal fade in black-overlay" id="AlasanHapusDepositModal" role="dialog" style="text-transform:uppercase;">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="margin-top: 150px; padding: 0 !important;">
        <div class="modal-content">
            <div class="block block-themed remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">Alasan Hapus</h3>
                </div>
            </div>
            <div class="col-xs-12" style="margin-top: 10px; margin-bottom: 10px;">
                <textarea class="form-control" id="alasanhapus" placeholder="Alasan Hapus" rows="10" required></textarea>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-danger" id="removeDeposit" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="tempIdRow" name="" value="">
<input type="hidden" id="tempIdRawatInap" name="" value="">
<input type="hidden" id="tempTanggalDaftar" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" value="">
<input type="hidden" id="tempNoMedrec" name="" value="">
<input type="hidden" id="tempIdPasien" name="" value="">
<input type="hidden" id="tempNamaPasien" name="" value="">
<input type="hidden" id="tempUmurPasien" name="" value="">
<input type="hidden" id="tempDokterPJ" name="" value="">

<input type="hidden" id="tempNoTelepon" name="" value="">

<input type="hidden" id="tempRuangan" name="" value="">
<input type="hidden" id="tempKelas" name="" value="">
<input type="hidden" id="tempBed" name="" value="">

<input type="hidden" id="tempIdKelompokPasien" name="" value="">
<input type="hidden" id="tempKelompokPasien" name="" value="">
<input type="hidden" id="tempNamaPerusahaan" name="" value="">

<input type="hidden" id="tempPenanggungJawab" name="" value="">
<input type="hidden" id="tempPenanggungJawabNoTelepon" name="" value="">

<input type="hidden" id="tempStatusCheckout" name="" value="">
<input type="hidden" id="tempStatusPembayaran" name="" value="">

<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<link rel="stylesheet" href="{plugins_path}magnific-popup/magnific-popup.css">
<script src="{plugins_path}magnific-popup/magnific-popup.min.js"></script>

<script type="text/javascript">
jQuery(function() {
    BaseTableDatatables.init();
    var tableRanap = $('#datatable-simrs').DataTable({
        "autoWidth": false,
        "pageLength": 100,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}trawatinap_tindakan/getIndex/' + '<?=$this->uri->segment(2)?>',
            type: "POST",
            dataType: 'json'
        },
        "columns": [{
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
            {
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
            {
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
            {
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
            {
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
            {
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
            {
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
            {
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
        ],
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 2,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 3,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 4,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 5,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 6,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 7,
                "orderable": true
            }
        ]
    });

    var tableRanapCheckout = $('#datatable-checkout').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}trawatinap_tindakan/getIndexCheckout/' + '<?=$this->uri->segment(2)?>',
            type: "POST",
            dataType: 'json'
        },
        "columns": [{
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
            {
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
            {
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
            {
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
            {
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
            {
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
            {
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
            {
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
            {
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
            {
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
            {
                "class": "details-control",
                "orderable": false,
                "defaultContent": ""
            },
        ],
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 2,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 3,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 4,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 5,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 6,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 7,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 8,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 9,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 10,
                "orderable": true
            }
        ]
    });

    // Array to track the ids of the details displayed rows
    var detailRowsRanap = [];
    var detailRowsRanapCheckout = [];

    $('#datatable-simrs tbody').on('click', 'tr td.details-control', function() {
        var tr = $(this).closest('tr');
        var row = tableRanap.row(tr);
        var idx = $.inArray(tr.attr('id'), detailRowsRanap);

        if (row.child.isShown()) {
            tr.removeClass('details');
            row.child.hide();

            // Remove from the 'open' array
            detailRowsRanap.splice(idx, 1);
        } else {
            var idpendaftaran = tr.find("td:eq(0) span").data('idpendaftaran');

            $.ajax({
                url: "{site_url}trawatinap_tindakan/getDataRawatInap/" + idpendaftaran,
                dataType: "json",
                success: function(data) {
                    tr.addClass('details');
                    row.child(format(data)).show();

                    // Add to the 'open' array
                    if (idx === -1) {
                        detailRowsRanap.push(tr.attr('id'));
                    }
                }
            });

        }
    });

    // On each draw, loop over the `detailRows` array and show any child rows
    tableRanap.on('draw', function() {
        $.each(detailRowsRanap, function(i, id) {
            $('#' + id + ' td.details-control').trigger('click');
        });
    });

    $('#datatable-checkout tbody').on('click', 'tr td.details-control', function() {
        var tr = $(this).closest('tr');
        var row = tableRanapCheckout.row(tr);
        var idx = $.inArray(tr.attr('id'), detailRowsRanapCheckout);

        if (row.child.isShown()) {
            tr.removeClass('details');
            row.child.hide();

            // Remove from the 'open' array
            detailRowsRanapChekout.splice(idx, 1);
        } else {
            var idpendaftaran = tr.find("td:eq(0) span").data('idpendaftaran');

            $.ajax({
                url: "{site_url}trawatinap_tindakan/getDataRawatInap/" + idpendaftaran,
                dataType: "json",
                success: function(data) {
                    tr.addClass('details');
                    row.child(formatCheckout(data)).show();

                    // Add to the 'open' array
                    if (idx === -1) {
                        detailRowsRanapCheckout.push(tr.attr('id'));
                    }
                }
            });

        }
    });

    // On each draw, loop over the `detailRows` array and show any child rows
    tableRanapCheckout.on('draw', function() {
        $.each(detailRowsRanapCheckout, function(i, id) {
            $('#' + id + ' td.details-control').trigger('click');
        });
    });
});

</script>
<script type="text/javascript">
$(document).ready(function() {
    var nomedrec = '';
    var namapasien = '';
    var umurpasien = '';
    var dokterpj = '';

    $("#checkoutKelompokDiagnosa").select2({
        tag: true
    });

    $("#checkoutKelompokDiagnosa").on("select2:select", function(evt) {
        var element = evt.params.data.element;
        var $element = $(element);

        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
    });

    $('.img-link').magnificPopup({
        type: 'image'
    });
    $('.number').number(true, 0, '.', ',');

    $(document).on('change', '#checkoutCaraPasienKeluar', function() {
        var val = $(this).val();

        $("#f_checkoutCaraPasienKeluarAlasanBatal label").html('Alasan Batal');

        if (val == '1' || val == '5') {
            $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', true);
            $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', true);
            $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', false);
        } else if (val == '2') {
            $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', false);
            $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', true);
            $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', false);

            $('#checkoutCaraPasienKeluarRsKlinik').select2({
                ajax: {
                    url: '{site_url}trawatinap_tindakan/getRsKlinik',
                    dataType: 'json',
                    processResults: function(data) {
                        var res = data.results.map(function(item) {
                            return {
                                id: item.id,
                                text: item.nama
                            }
                        })
                        return {
                            results: res
                        }
                    },
                }
            })
        } else if (val == '3') {
            $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', false);
            $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', false);
            $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', false);

            $("#f_checkoutCaraPasienKeluarAlasanBatal label").html('Alasan');

            $('#checkoutCaraPasienKeluarRsKlinik').select2({
                ajax: {
                    url: '{site_url}trawatinap_tindakan/getRsKlinik',
                    dataType: 'json',
                    processResults: function(data) {
                        var res = data.results.map(function(item) {
                            return {
                                id: item.id,
                                text: item.nama
                            }
                        })
                        return {
                            results: res
                        }
                    },
                }
            })

            $('#checkoutCaraPasienKeluarAlasanBatal').select2({
                ajax: {
                    url: '{site_url}trawatinap_tindakan/getAlasanBatal',
                    dataType: 'json',
                    processResults: function(data) {
                        var res = data.results.map(function(item) {
                            return {
                                id: item.id,
                                text: item.keterangan
                            }
                        })
                        return {
                            results: res
                        }
                    },
                }
            })
        } else if (val == '4') {
            $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', true);
            $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', false);
            $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', false);
            $('#checkoutCaraPasienKeluarAlasanBatal').select2({
                ajax: {
                    url: '{site_url}trawatinap_tindakan/getAlasanBatal',
                    dataType: 'json',
                    processResults: function(data) {
                        var res = data.results.map(function(item) {
                            return {
                                id: item.id,
                                text: item.keterangan
                            }
                        })
                        return {
                            results: res
                        }
                    },
                }
            })
        } else {
            $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', true);
            $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', true);
            $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', true);
        }
    });

    // Checkout
    $(document).on('click', '#openModalCheckout', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var nomedrec = $("#tempNoMedrec").val();
        var namapasien = $("#tempNamaPasien").val();
        var statuscheckout = $("#tempStatusCheckout").val();

        if (statuscheckout == 1) {
            $.ajax({
                url: '{site_url}trawatinap_tindakan/getDataCheckout/' + idpendaftaran,
                dataType: 'json',
                success: function(data) {
                    $("#checkoutTanggalKeluar").datepicker('setDate', data.tanggalkeluar);
                    $("#checkoutTanggalKontrol").datepicker('setDate', data.tanggalkontrol);
                    $("#checkoutNoMedrec").val(nomedrec);
                    $("#checkoutNamapasien").val(namapasien);
                    $("#checkoutSebabLuar").select2("trigger", "select", {
                        data: {
                            id: data.sebabluar,
                            text: data.sebabluar
                        }
                    });
                    $("#checkoutKeadaanPasienKeluar").select2("trigger", "select", {
                        data: {
                            id: data.keadaanpasienkeluar,
                            text: data.keadaanpasienkeluar
                        }
                    });
                    $("#checkoutKondisiPasienKeluar").select2("trigger", "select", {
                        data: {
                            id: data.kondisipasienkeluar,
                            text: data.kondisipasienkeluar
                        }
                    });
                    $("#checkoutKondisiPasienSaatIni").val(data.kondisipasiensaatini);
                    $("#checkoutCaraPasienKeluar").select2("trigger", "select", {
                        data: {
                            id: data.carapasienkeluar,
                            text: data.carapasienkeluar
                        }
                    });
                    $("#checkoutCaraPasienKeluarRsKlinik").select2("trigger", "select", {
                        data: {
                            id: data.idrumahsakit,
                            text: data.idrumahsakit
                        }
                    });
                    $("#checkoutCaraPasienKeluarAlasanBatal").select2("trigger", "select", {
                        data: {
                            id: data.idalasanbatal,
                            text: data.idalasanbatal
                        }
                    });
                    $("#checkoutCaraPasienKeluarKeterangan").val(data.keterangancarapasienkeluar);
                    $("#checkoutDiagnosa").val(data.diagnosa);
                    $("#checkoutLamaDirawat").val(getLamaDirawat());

                    if (data.keadaanpasienkeluar == 1) {
                        $("#f_checkoutKondisiPasienSaatIni").show();
                        $("#f_checkoutCaraPasienKeluar").show();
                    } else {
                        $("#f_checkoutKondisiPasienSaatIni").hide();
                        $("#f_checkoutCaraPasienKeluar").hide();
                    }

                    $("#f_checkoutCaraPasienKeluarAlasanBatal label").html('Alasan Batal');
                    if (data.carapasienkeluar == '1' || data.carapasienkeluar == '5') {
                        $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', true);
                        $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', true);
                        $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', false);
                    } else if (data.carapasienkeluar == '2') {
                        $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', false);
                        $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', true);
                        $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', false);
                    } else if (data.carapasienkeluar == '3') {
                        $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', false);
                        $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', false);
                        $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', false);
                        $("#f_checkoutCaraPasienKeluarAlasanBatal label").html('Alasan');
                    } else if (data.carapasienkeluar == '4') {
                        $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', true);
                        $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', false);
                        $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', false);
                    } else {
                        $('#f_checkoutCaraPasienKeluarRsKlinik').prop('hidden', true);
                        $('#f_checkoutCaraPasienKeluarAlasanBatal').prop('hidden', true);
                        $('#f_checkoutCaraPasienKeluarKeterangan').prop('hidden', true);
                    }
                }
            })
        } else {
            $.ajax({
                url: '{site_url}trawatinap_tindakan/getDiagnosa/' + idpendaftaran,
                dataType: 'json',
                method: 'post',
                success: function(data) {
                    $("#checkoutDiagnosa").val(data.diagnosa);
                }
            })

            var date = new Date();
            var tanggalkeluar = ("0" + (date.getDate())).slice(-2) + '/' + ("0" + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear();

            $("#checkoutSebabLuar").select2("trigger", "select", {
                data: {
                    id: '',
                    text: ''
                }
            });
            $.ajax({
                url: "{site_url}trawatinap_tindakan/getDataPoliTindakan/" + idpendaftaran,
                dataType: "json",
                success: function(data) {
                    if (data.sebabluar != 0) {
                        $("#checkoutSebabLuar").select2("trigger", "select", {
                            data: {
                                id: data.sebabluar,
                                text: ''
                            }
                        });
                    } else {
                        $("#checkoutSebabLuar").select2("trigger", "select", {
                            data: {
                                id: 1,
                                text: ''
                            }
                        });
                    }
                }
            });

            $("#checkoutTanggalKeluar").datepicker('setDate', tanggalkeluar);
            $("#checkoutTanggalKontrol").datepicker('setDate', tanggalkeluar);
            $("#checkoutLamaDirawat").val(getLamaDirawat());
            $("#checkoutNoMedrec").val(nomedrec);
            $("#checkoutNamapasien").val(namapasien);
            // $("#checkoutKeadaanPasienKeluar").select2("trigger", "select", {data : {id: '',text: ''}});
            $("#checkoutKondisiPasienKeluar").select2("trigger", "select", {
                data: {
                    id: '',
                    text: ''
                }
            });
            $("#checkoutCaraPasienKeluar").select2("trigger", "select", {
                data: {
                    id: '',
                    text: ''
                }
            });
        }
    });

    $("#checkoutTanggalKeluar").datepicker({
        autoclose: true,
    }).on("change", function() {
        $("#checkoutLamaDirawat").val(getLamaDirawat());
    });

    $("#checkoutKeadaanPasienKeluar").change(function() {
        if ($(this).val() == 1) {
            // Kondisi Hidup
            $("#checkoutKondisiPasienKeluar").html('<option value="">Pilih Opsi</option>');
            $("#checkoutKondisiPasienKeluar").append('<option value="1">Sembuh</option>');
            $("#checkoutKondisiPasienKeluar").append('<option value="2">Membaik</option>');
            $("#checkoutKondisiPasienKeluar").append('<option value="3">Belum Sembuh</option>');

            $("#checkoutKondisiPasienKeluar").select2("destroy");
            $("#checkoutKondisiPasienKeluar").select2();

            // Show
            $("#f_checkoutKondisiPasienSaatIni").show();
            $("#f_checkoutCaraPasienKeluar").show();
        } else {
            // Kondisi Mati
            $("#checkoutKondisiPasienKeluar").html('<option value="">Pilih Opsi</option>');
            $("#checkoutKondisiPasienKeluar").append('<option value="4">< 48 Jam</option>');
            $("#checkoutKondisiPasienKeluar").append('<option value="5">>= 48 Jam</option>');

            $("#checkoutKondisiPasienKeluar").select2("destroy");
            $("#checkoutKondisiPasienKeluar").select2();

            // Hide
            $("#f_checkoutKondisiPasienSaatIni").hide();
            $("#f_checkoutCaraPasienKeluar").hide();
        }
    });

    $(document).on('click', '#saveCheckout', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var tanggalkeluar = $("#checkoutTanggalKeluar").val();
        var tanggalkontrol = $("#checkoutTanggalKontrol").val();
        var sebabluar = $("#checkoutSebabLuar").val();
        var kelompokDiagnosa = $("#checkoutKelompokDiagnosa").val();
        var keadaanpasienkeluar = $("#checkoutKeadaanPasienKeluar").val();
        var kondisipasienkeluar = $("#checkoutKondisiPasienKeluar").val();
        var checkoutKondisiPasienSaatIni = $("#checkoutKondisiPasienSaatIni").val();
        var carapasienkeluar = $("#checkoutCaraPasienKeluar").val();
        var diagnosa = $("#checkoutDiagnosa").val();
        var checkoutCaraPasienKeluarRsKlinik = $("#checkoutCaraPasienKeluarRsKlinik").val();
        var checkoutCaraPasienKeluarAlasanBatal = $("#checkoutCaraPasienKeluarAlasanBatal").val();
        var checkoutCaraPasienKeluarKeterangan = $("#checkoutCaraPasienKeluarKeterangan").val();

        if (tanggalkeluar == '') {
            sweetAlert("Maaf...", "Tanggal Keluar tidak boleh kosong !", "error").then((value) => {
                $("#checkoutTanggalKeluar").focus();
            });
            return false;
        }

        if (tanggalkontrol == '') {
            sweetAlert("Maaf...", "Tanggal Kontrol tidak boleh kosong !", "error").then((value) => {
                $("#checkoutTanggalKontrol").focus();
            });
            return false;
        }

        if (sebabluar == '') {
            sweetAlert("Maaf...", "Sebab Luar belum dipilih !", "error").then((value) => {
                $("#checkoutSebabLuar").select2('open');
            });
            return false;
        }

        if (kelompokDiagnosa == '') {
            sweetAlert("Maaf...", "Kelompok Diagnosa belum dipilih !", "error").then((value) => {
                $("#checkoutKelompokDiagnosa").select2('open');
            });
            return false;
        }

        if (keadaanpasienkeluar == '') {
            sweetAlert("Maaf...", "Keadaan Pasien Keluar belum dipilih !", "error").then((value) => {
                $("#checkoutKeadaanPasienKeluar").select2('open');
            });
            return false;
        }

        if (kondisipasienkeluar == '') {
            sweetAlert("Maaf...", "Kondisi Pasien Keluar belum dipilih !", "error").then((value) => {
                $("#checkoutKondisiPasienKeluar").select2('open');
            });
            return false;
        }

        if (carapasienkeluar == '') {
            sweetAlert("Maaf...", "Cara Pasien Keluar belum dipilih !", "error").then((value) => {
                $("#checkoutCaraPasienKeluar").select2('open');
            });
            return false;
        }

        if (diagnosa == '') {
            sweetAlert("Maaf...", "Diagnosa tidak boleh kosong !", "error").then((value) => {
                $("#checkoutDiagnosa").select2('open');
            });
            return false;
        }

        $.ajax({
            url: "{site_url}trawatinap_tindakan/saveCheckout",
            method: 'POST',
            data: {
                idpendaftaran: idpendaftaran,
                tanggalkeluar: tanggalkeluar,
                tanggalkontrol: tanggalkontrol,
                tanggalkontrol: tanggalkontrol,
                sebabluar: sebabluar,
                kelompokdiagnosa: kelompokDiagnosa,
                keadaanpasienkeluar: keadaanpasienkeluar,
                kondisipasienkeluar: kondisipasienkeluar,
                checkoutKondisiPasienSaatIni: checkoutKondisiPasienSaatIni,
                carapasienkeluar: carapasienkeluar,
                diagnosa: diagnosa,
                checkoutCaraPasienKeluarRsKlinik: checkoutCaraPasienKeluarRsKlinik,
                checkoutCaraPasienKeluarAlasanBatal: checkoutCaraPasienKeluarAlasanBatal,
                checkoutCaraPasienKeluarKeterangan: checkoutCaraPasienKeluarKeterangan,
            },
            success: function(data) {
                swal({
                    title: "Berhasil!",
                    text: "Pasien Telah Checkout.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                // location.reload();
            }
        });
    });

    // Dokter Penanggung Jawab
    $(document).on('click', '#openModalDokterPJ', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var statuspembayaran = $("#tempStatusPembayaran").val();

        if (statuspembayaran == 1) {
            $("#formInputDokterPJ").hide();
            $("#saveDokterPJ").hide();
        } else {
            $("#formInputDokterPJ").show();
            $("#saveDokterPJ").show();
        }

        $.ajax({
            url: "{site_url}trawatinap_tindakan/getDokterPJ",
            dataType: "json",
            success: function(data) {
                $("#iddokterPJ").empty();
                $("#iddokterPJ").append("<option value=''>Pilih Opsi</option>");
                for (var i = 0; i < data.length; i++) {
                    $("#iddokterPJ").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
                }
            }
        });

        getHistoryDokterPJ(idpendaftaran);
    });

    $(document).on('click', '#saveDokterPJ', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var iddokterpj = $("#iddokterPJ option:selected").val();

        if (iddokterpj == '') {
            sweetAlert("Maaf...", "Dokter Penanggung Jawab belum dipilih !", "error").then((value) => {
                $("#iddokterPJ").select2('open');
            });
            return false;
        }

        $.ajax({
            url: "{site_url}trawatinap_tindakan/saveDokterPJ",
            method: 'POST',
            data: {
                idpendaftaran: idpendaftaran,
                iddokterpj: iddokterpj
            },
            success: function(data) {
                swal({
                    title: "Berhasil!",
                    text: "Data Dokter Penanggung Jawab Telah Tersimpan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                getHistoryDokterPJ(idpendaftaran)

                // Clear Form
                $("#iddokterPJ").select2("trigger", "select", {
                    data: {
                        id: '',
                        text: ''
                    }
                });
            }
        });
    });

    // Pindah Bed
    $(document).on('click', '#openModalBed', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var statuspembayaran = $("#tempStatusPembayaran").val();

        if (statuspembayaran == 1) {
            $("#formPindahRuangan").hide();
            $("#savePindahBed").hide();
        } else {
            $("#formPindahRuangan").show();
            $("#savePindahBed").show();
        }

        // Get Current Info
        $.ajax({
            url: "{site_url}trawatinap_tindakan/getDataRawatInap/" + idpendaftaran,
            dataType: "json",
            success: function(data) {
                $("#currentRuangan").val(data.namaruangan);
                $("#currentKelas").val(data.namakelas);
                $("#currentBed").val(data.namabed);
            }
        });

        // Get Pegawai Rawat Inap
        $.ajax({
            url: "{site_url}trawatinap_tindakan/getPegawaiRawatInap",
            dataType: "json",
            success: function(data) {
                $("#bedIdpegawai").empty();
                $("#bedIdpegawai").append("<option value=''>Pilih Opsi</option>");
                for (var i = 0; i < data.length; i++) {
                    $("#bedIdpegawai").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
                }
            }
        });

        // Get Ruangan
        $.ajax({
            url: "{site_url}trawatinap_tindakan/getRuangan",
            dataType: "json",
            success: function(data) {
                $("#bedIdruangan").empty();
                $("#bedIdruangan").append("<option value=''>Pilih Opsi</option>");
                for (var i = 0; i < data.length; i++) {
                    $("#bedIdruangan").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
                }
            }
        });

        getHistoryPindahBed(idpendaftaran);
    });

    $(document).on('change', '#bedIdruangan', function() {
        $.ajax({
            url: "{site_url}trawatinap_tindakan/getKelas",
            dataType: "json",
            success: function(data) {
                $("#bedIdkelas").empty();
                $("#bedIdbed").empty();
                $("#bedIdkelas").append("<option value=''>Pilih Opsi</option>");
                for (var i = 0; i < data.length; i++) {
                    $("#bedIdkelas").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
                }
            }
        });
    });

    $(document).on('change', '#bedIdkelas', function() {
        var idruangan = $("#bedIdruangan option:selected").val();
        var idkelas = $("#bedIdkelas option:selected").val();

        $.ajax({
            url: "{site_url}trawatinap_tindakan/getBed/" + idruangan + "/" + idkelas,
            dataType: "json",
            success: function(data) {
                $("#bedIdbed").empty();
                $("#bedIdbed").append("<option value=''>Pilih Opsi</option");
                for (var i = 0; i < data.length; i++) {
                    $("#bedIdbed").append("<option value='" + data[i].id + "' data-status='" + data[i].status + "'>" + data[i].nama + "</option>");
                }
            }
        });
    });

    $(document).on('change', '#bedIdbed', function() {
        var bed = $(this).select2().find(":selected").text();
        var status = $(this).select2().find(":selected").data("status");

        if (status == 0) {
            sweetAlert("Maaf...", `Bed ${bed} tidak bisa dipergunakan`, "error");
            $("#bedIdbed").select2("trigger", "select", {
                data: {
                    id: '',
                    text: ''
                }
            });
        }
    });

    $(document).on('click', '#savePindahBed', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var tanggal = $("#bedTanggalpindah").val();
        var idpegawai = $("#bedIdpegawai option:selected").val();
        var idruangan = $("#bedIdruangan option:selected").val();
        var idkelas = $("#bedIdkelas option:selected").val();
        var idbed = $("#bedIdbed option:selected").val();

        if (tanggal == '') {
            sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error");
            $("#bedTanggalpindah").focus();
            return false;
        }

        if (idpegawai == '') {
            sweetAlert("Maaf...", "Petugas belum dipilih !", "error").then((value) => {
                $("#bedIdpegawai").select2('open');
            });
            return false;
        }

        if (idruangan == '') {
            sweetAlert("Maaf...", "Ruangan belum dipilih !", "error").then((value) => {
                $("#bedIdruangan").select2('open');
            });
            return false;
        }

        if (idkelas == '') {
            sweetAlert("Maaf...", "Kelas belum dipilih !", "error").then((value) => {
                $("#bedIdkelas").select2('open');
            });
            return false;
        }

        if (idbed == '') {
            sweetAlert("Maaf...", "Bed belum dipilih !", "error").then((value) => {
                $("#bedIdbed").select2('open');
            });
            return false;
        }

        $.ajax({
            url: '{site_url}trawatinap_tindakan/savePindahBed',
            method: 'POST',
            data: {
                idpendaftaran: idpendaftaran,
                tanggal: tanggal,
                idpegawai: idpegawai,
                idruangan: idruangan,
                idkelas: idkelas,
                idbed: idbed
            },
            success: function(data) {
                swal({
                    title: "Berhasil!",
                    text: "Data Bed Telah Tersimpan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                // Change Label Current Bed
                $("#currentRuangan").val($("#bedIdruangan option:selected").text());
                $("#currentKelas").val($("#bedIdkelas option:selected").text());
                $("#currentBed").val($("#bedIdbed option:selected").text());

                getHistoryPindahBed(idpendaftaran);

                $("#bedIdpegawai").select2("trigger", "select", {
                    data: {
                        id: '',
                        text: ''
                    }
                });
                $("#bedIdruangan").select2("trigger", "select", {
                    data: {
                        id: '',
                        text: ''
                    }
                });
                $("#bedIdkelas").select2("trigger", "select", {
                    data: {
                        id: '',
                        text: ''
                    }
                });
                $('#bedIdbed').select2("trigger", "select", {
                    data: {
                        id: '',
                        text: ''
                    }
                });
            }
        });
    });

    // Open Estimasi Biaya Modal
    $(document).on('click', '#openModalEstimasi', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();

        getHistoryEstimasiBiaya(idpendaftaran);
    });

    // Estimasi Biaya
    $(document).on('click', '.openDetailEstimasiBiaya', function() {
        var idrow = $(this).data('idrow');
        var nomedrec = $("#tempNoMedrec").val();
        var namapasien = $("#tempNamaPasien").val();
        var umurpasien = $("#tempUmurPasien").val();
        var dokterpj = $("#tempDokterPJ").val();

        $('#historyEstimasiBiaya').hide();
        $('#viewEstimasiBiaya').show();

        // Get Data Estimasi
        $.ajax({
            url: "{site_url}trawatinap_tindakan/getDataEstimasiBiaya/" + idrow,
            dataType: "json",
            success: function(data) {
                $("#vEstimasiScan").attr("href", "{site_url}assets/upload/estimasi/" + data.fotoscan);
                $("#vEstimasiNomedrec").val(nomedrec);
                $("#vEstimasiNamapasien").val(namapasien);
                $("#vEstimasiUmur").val(umurpasien);
                $("#vEstimasiDokter").val(dokterpj);
                $("#vEstimasiDiagnosa").val(data.diagnosa);
                $("#vEstimasiOperasi").val(data.operasi);
                $("#vEstimasiJenisOperasi").val(data.namajenisoperasi);
                $("#vEstimasiSewaKamar").val(data.sewakamar);
                $("#vEstimasiJasaMedisOperator").val(data.jasadokteroperator);
                $("#vEstimasiJasaMedisAnesthesi").val(data.jasadokteranaesthesi);
                $("#vEstimasiJasaMedisAssisten").val(data.jasaassisten);
                $("#vEstimasiObatBhp").val(data.obatbhp);
                $("#vEstimasiQtyRuangPerawatan").val(data.kuantitasruangperawatan);
                $("#vEstimasiRuangPerawatan").val(data.ruangperawatan);
                $("#vEstimasiQtyHCU").val(data.kuantitasruanghcu);
                $("#vEstimasiHCU").val(data.ruanghcu);
                $("#vEstimasiObatPerawatan").val(data.obatperawatan);
                $("#vEstimasiPenunjang").val(data.penunjanglain);
            }
        });

        // Get Data Estimasi Implant
        $.ajax({
            url: "{site_url}trawatinap_tindakan/getDataEstimasiBiayaImplant/" + idrow,
            dataType: "json",
            success: function(data) {
                $('#historyEstimasiImplant tbody').empty();
                for (var i = 0; i < data.length; i++) {
                    $('#historyEstimasiImplant tbody').append('<tr><td>' + data[i].namaimplan + '</td><td>' + $.number(data[i].harga) + '</td><td>' + $.number(data[i].kuantitas) + '</td><td>' + data[i].status + '</td></tr>');
                }
            }
        });
    });

    $(document).on('click', '.closeDetailEstimasiBiaya', function() {
        $('#historyEstimasiBiaya').show();
        $('#viewEstimasiBiaya').hide();
    });

    // Aksi : Tindakan, Alat Kesehatan & Visite DOKTER
    $(document).on('click', '#openModalAksi', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var nomedrec = $("#tempNoMedrec").val();
        var namapasien = $("#tempNamaPasien").val();
        var idkelompokpasien = $("#tempIdKelompokPasien").val();
        var statuspembayaran = $("#tempStatusPembayaran").val();

        if (statuspembayaran == 1) {
            $("#historyTindakan thead tr:eq(0)").hide();
            $("#historyTindakan tbody tr:eq(0)").hide();
            $("#historyAlkes thead tr:eq(0)").hide();
            $("#historyAlkes tbody tr:eq(0)").hide();
            $("#historyObat thead tr:eq(0)").hide();
            $("#historyObat tbody tr:eq(0)").hide();
            $("#historyVisiteDokter thead tr:eq(0)").hide();
            $("#historyVisiteDokter tbody tr:eq(0)").hide();
        } else {
            $("#historyTindakan thead tr:eq(0)").show();
            $("#historyTindakan tbody tr:eq(0)").show();
            $("#historyAlkes thead tr:eq(0)").show();
            $("#historyAlkes tbody tr:eq(0)").show();
            $("#historyObat thead tr:eq(0)").show();
            $("#historyObat tbody tr:eq(0)").show();
            $("#historyVisiteDokter thead tr:eq(0)").show();
            $("#historyVisiteDokter tbody tr:eq(0)").show();
        }


        $(".tindakanIdPendaftaran").val(idpendaftaran);
        $(".tindakanNoMedrec").val(nomedrec);
        $(".tindakanNamaPasien").val(namapasien);
        $(".tindakanIdKelompokPasien").val(idkelompokpasien);

        // Get Dokter
        $.ajax({
            url: "{site_url}trawatinap_tindakan/getDokterPJ",
            dataType: "json",
            success: function(data) {
                $("#fTindakanIdDokter").empty();
                $("#fTindakanIdDokter").append("<option value=''>Pilih Opsi</option>");
                for (var i = 0; i < data.length; i++) {
                    $("#fTindakanIdDokter").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
                }

                $("#fVisiteIdDokter").empty();
                $("#fVisiteIdDokter").append("<option value=''>Pilih Opsi</option>");
                for (var i = 0; i < data.length; i++) {
                    $("#fVisiteIdDokter").append("<option value='" + data[i].id + "' data-idkategori='" + data[i].idkategori + "'>" + data[i].nama + "</option>");
                }
            }
        });

        // Get Ruangan
        getRuanganVisite(idpendaftaran);

        getHistoryTindakan(idpendaftaran);
        getHistoryAlkes(idpendaftaran);
        getHistoryObat(idpendaftaran);
        getHistoryVisiteDokter(idpendaftaran);

        setTimeout(function() {
            disabledModalAksi();
        }, 1000);
    });

    $(document).on('change', '#fVisiteIdDokter', function() {
        var idkategori = $(this).find(':selected').data('idkategori');
        var iddokter = $(this).val();

        var idkelompokpasien = $(".tindakanIdKelompokPasien").val();
        var idruangan = $("#fVisiteIdRuangan").val();
        var idkelas = $("#fVisiteIdKelas").val();

        $.ajax({
            url: '{site_url}trawatinap_tindakan/getTarifVisiteDokter/' + idkategori + '/' + idkelompokpasien + '/' + idruangan + '/' + idkelas,
            method: 'GET',
            success: function(data) {
                $("#fVisiteIdTarif").val(data.idtarif);
                $("#fVisiteJasaSarana").val(data.jasasarana);
                $("#fVisiteJasaPelayanan").val(data.jasapelayanan);
                $("#fVisiteBHP").val(data.bhp);
                $("#fVisiteBiayaPerawatan").val(data.biayaperawatan);
                $("#fVisiteTarif").val(data.total);
            }
        });
    });

    $(document).on('click', '#saveDataVisiteDokter', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var idRow = $('#fVisiteRowId').val();
        var tanggalvisite = $('#fVisiteTanggal').val();
        var iddokter = $('#fVisiteIdDokter option:selected').val();
        var idruangan = $('#fVisiteIdRuangan').val();
        var namaruangan = $('#fVisiteRuangan').val();
        var idpelayanan = $('#fVisiteIdTarif').val();
        var jasasarana = $('#fVisiteJasaSarana').val();
        var jasapelayanan = $('#fVisiteJasaPelayanan').val();
        var bhp = $('#fVisiteBHP').val();
        var biayaperawatan = $('#fVisiteJasaBiayaPerawatan').val();
        var tarif = $('#fVisiteTarif').val();

        var duplicate = false;
        $('#historyVisiteDokter tbody tr').filter(function() {
            var $cells = $(this).children('td');
            if ($cells.eq(0).text() === tanggalvisite && $cells.eq(1).text() === iddokter && $cells.eq(3).text() === idruangan) {
                sweetAlert("Maaf...", $("#fVisiteIdDokter option:selected").text() + " sudah melakukan visite pada tanggal " + tanggalvisite + " pada ruangan " + namaruangan + " !", "error");
                duplicate = true;
                return false;
            }
        });

        if (duplicate == false) {

            if (tanggalvisite == '') {
                sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
                    $('#fAlkesTanggal').focus();
                });
                return false;
            }

            if (iddokter == '') {
                sweetAlert("Maaf...", "Dokter belum dipilih !", "error").then((value) => {
                    $('#fVisiteIdDokter').select2('open');
                });
                return false;
            }

            if (idruangan == '') {
                sweetAlert("Maaf...", "Ruangan tidak boleh kosong !", "error").then((value) => {
                    $('#fVisiteRuangan').focus();
                });
                return false;
            }

            if (tarif == '' || tarif <= 0) {
                sweetAlert("Maaf...", "Tarif tidak boleh kosong !", "error").then((value) => {
                    $('#fVisiteTarif').focus();
                });
                return false;
            }

            $.ajax({
                url: '{site_url}trawatinap_tindakan/saveDataVisiteDokter',
                method: 'POST',
                data: {
                    idpendaftaran: idpendaftaran,
                    idrow: idRow,
                    tanggalvisite: tanggalvisite,
                    idruangan: idruangan,
                    iddokter: iddokter,
                    idpelayanan: idpelayanan,
                    jasasarana: jasasarana,
                    jasapelayanan: jasapelayanan,
                    bhp: bhp,
                    biayaperawatan: biayaperawatan,
                    tarif: tarif,
                },
                success: function(data) {
                    swal({
                        title: "Berhasil!",
                        text: "Data Visite Dokter Telah Tersimpan.",
                        type: "success",
                        timer: 1500,
                        showConfirmButton: false
                    });

                    getHistoryVisiteDokter(idpendaftaran);

                    // Clear Form
                    $('#fVisiteIdDokter').select2("trigger", "select", {
                        data: {
                            id: '',
                            text: ''
                        }
                    });
                    $('#fVisiteTarif').val('');

                    // Get Ruangan
                    getRuanganVisite(idpendaftaran);

                    // Enable Button Action
                    $("#historyVisiteDokter button").prop("disabled", false);
                    $("#actionDataVisite").html('<button id="saveDataVisiteDokter" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</button>');
                }
            });
        }
    });

    $(document).on('click', '.editDataVisiteDokter', function() {
        var idrow = $(this).data('idrow');
        var iddokter = $(this).closest('tr').find("td:eq(1)").html();
        var namadokter = $(this).closest('tr').find("td:eq(2)").html();
        var idruangan = $(this).closest('tr').find("td:eq(3)").html();
        var namaruangan = $(this).closest('tr').find("td:eq(4)").html();
        var tarif = $(this).closest('tr').find("td:eq(5)").html();

        // Disabled Button Action
        $("#historyVisiteDokter button").prop("disabled", true);
        $("#actionDataVisite").html('<div class="btn-group"><button id="saveDataVisiteDokter" class="btn btn-primary"><i class="fa fa-check"> Simpan</i></button> <button id="cancelDataVisiteDokter" class="btn btn-danger"><i class="fa fa-reply"> Batal</i></button></div>');

        // Clear Form
        $('#fVisiteRowId').val(idrow);
        $('#fVisiteIdDokter').select2("trigger", "select", {
            data: {
                id: iddokter,
                text: namadokter
            }
        });
        $('#fVisiteIdRuangan').val(idruangan);
        $('#fVisiteRuangan').val(namaruangan);
        $('#fVisiteTarif').val(tarif);
    });

    $(document).on('click', '#cancelDataVisiteDokter', function() {
        // Enable Button Action
        $("#historyVisiteDokter button").prop("disabled", false);
        $("#actionDataVisite").html('<button id="saveDataVisiteDokter" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</button>');

        // Clear Form
        $('#fVisiteRowId').val('');
        $('#fVisiteIdDokter').select2("trigger", "select", {
            data: {
                id: '',
                text: ''
            }
        });

        var idpendaftaran = $('#tempIdRawatInap').val();
        getRuanganVisite(idpendaftaran);

        $('#fVisiteTarif').val('');
    });

    $(document).on('click', '.removeDataVisiteDokter', function() {
        var idrow = $(this).data('idrow');
        var idrawatinap = $('#tempIdRawatInap').val();

        event.preventDefault();
        swal({
            title: "Apakah anda yakin ingin menyetujui data ini?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {
            // Approve Status
            $.ajax({
                url: '{site_url}trawatinap_tindakan/removeDataVisiteDokter',
                method: 'POST',
                data: {
                    idrow: idrow
                },
                success: function(data) {
                    swal({
                        title: "Berhasil!",
                        text: "Data Visite Telah Dihapus.",
                        type: "success",
                        timer: 1500,
                        showConfirmButton: false
                    });

                    getHistoryVisiteDokter(idrawatinap);
                }
            });
        });
    });

    // SubAlkes Modal : Unit Pelayanan
    $(document).on('click', '#fAlkesSubModal', function() {
        var idunitpelayanan = $("#subAlkesIdUnit option:selected").val();
        var idpendaftaran = $('.tindakanIdPendaftaran').val();

        $('#listAlkesTable').DataTable().destroy();
        $('#listAlkesTable').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                url: '{site_url}trawatinap_tindakan/getListAlkesUnit/' + idpendaftaran + '/' + idunitpelayanan,
                type: "POST",
                dataType: 'json'
            }
        });
    });

    $(document).on('change', '#subAlkesIdUnit', function() {
        var idunitpelayanan = $(this).val();
        var idpendaftaran = $('.tindakanIdPendaftaran').val();

        $('#listAlkesTable').DataTable().destroy();
        $('#listAlkesTable').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                url: '{site_url}trawatinap_tindakan/getListAlkesUnit/' + idpendaftaran + '/' + idunitpelayanan,
                type: "POST",
                dataType: 'json'
            }
        });
    });

    $(document).on('click', '.selectAlkesTable', function() {
        $('#fAlkesId').val($(this).closest('tr').find("td:eq(0) a").data('idalkes'));
        $('#fAlkesNama').val($(this).closest('tr').find("td:eq(1)").html());
        $('#fAlkesHargaDasar').val($(this).closest('tr').find("td:eq(0) a").data('hargadasar'));
        $('#fAlkesMargin').val($(this).closest('tr').find("td:eq(0) a").data('margin'));
        $('#fAlkesHargaJual').val($(this).closest('tr').find("td:eq(3)").html());
        $('#fAlkesKuantitas').val(1);
        $('#fAlkesTotal').val($(this).closest('tr').find("td:eq(3)").html());
        $('#fAlkesIdUnitPelayanan').val($("#subAlkesIdUnit option:selected").val());
        $('#fAlkesUnitPelayanan').val($("#subAlkesIdUnit option:selected").text());
        $('#fAlkesKuantitas').focus();
    });

    $(document).on('keyup', '#fAlkesKuantitas', function() {
        var hargajual = $('#fAlkesHargaJual').val().replace(/,/g, '');
        var kuantitas = $(this).val().replace(/,/g, '');
        $('#fAlkesTotal').val(parseInt(hargajual) * parseInt(kuantitas));
    });

    $(document).on('click', '#saveDataAlkes', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var idRow = $('#fAlkesRowId').val();
        var tanggal = $('#fAlkesTanggal').val();
        var idunitpelayanan = $('#fAlkesIdUnitPelayanan').val();
        var idalkes = $('#fAlkesId').val();
        var hargadasar = $('#fAlkesHargaDasar').val();
        var margin = $('#fAlkesMargin').val();
        var hargajual = $('#fAlkesHargaJual').val();
        var kuantitas = $('#fAlkesKuantitas').val();
        var totalkeseluruhan = $('#fAlkesTotal').val();

        if (tanggal == '') {
            sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
                $('#fAlkesTanggal').focus();
            });
            return false;
        }

        if (idalkes == '') {
            sweetAlert("Maaf...", "Alkes belum dipilih !", "error").then((value) => {
                $('#fAlkesId').focus();
            });
            return false;
        }

        if (hargajual == '' || hargajual <= 0) {
            sweetAlert("Maaf...", "Tarif tidak boleh kosong !", "error").then((value) => {
                $('#fAlkesHargaJual').focus();
            });
            return false;
        }

        if (kuantitas == '' || kuantitas <= 0) {
            sweetAlert("Maaf...", "Kuantitas tidak boleh kosong !", "error").then((value) => {
                $('#fTindakanJumlah').focus();
            });
            return false;
        }

        if (totalkeseluruhan == '' || totalkeseluruhan <= 0) {
            sweetAlert("Maaf...", "Kuantitas tidak boleh kosong !", "error").then((value) => {
                $('#fAlkesTotal').focus();
            });
            return false;
        }

        if (idunitpelayanan == '') {
            sweetAlert("Maaf...", "Unit Pelayanan tidak boleh kosong !", "error").then((value) => {
                $('#fAlkesUnitPelayanan').focus();
            });
            return false;
        }

        $.ajax({
            url: '{site_url}trawatinap_tindakan/saveDataAlkes',
            method: 'POST',
            data: {
                idpendaftaran: idpendaftaran,
                idrow: idRow,
                tanggal: tanggal,
                idunitpelayanan: idunitpelayanan,
                idalkes: idalkes,
                hargadasar: hargadasar,
                margin: margin,
                hargajual: hargajual,
                kuantitas: kuantitas,
                totalkeseluruhan: totalkeseluruhan
            },
            success: function(data) {
                swal({
                    title: "Berhasil!",
                    text: "Data Alkes Telah Tersimpan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                getHistoryAlkes(idpendaftaran);

                // Clear Form
                $('#fAlkesRowId').val('');
                $('#fAlkesId').val('');
                $('#fAlkesNama').val('');
                $('#fAlkesHargaDasar').val('');
                $('#fAlkesMargin').val('');
                $('#fAlkesHargaJual').val('');
                $('#fAlkesKuantitas').val('');
                $('#fAlkesTotal').val('');
                $('#fAlkesIdUnitPelayanan').val('');
                $('#fAlkesUnitPelayanan').val('');

                // Enable Button Action
                $("#historyAlkes button").prop("disabled", false);
                $("#actionDataAlkes").html('<button id="saveDataAlkes" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</button>');
            }
        });
    });

    $(document).on('click', '.editDataAlkes', function() {
        var idrow = $(this).data('idrow');
        var idalkes = $(this).closest('tr').find("td:eq(1)").html();
        var namaalkes = $(this).closest('tr').find("td:eq(2)").html();
        var hargadasar = $(this).closest('tr').find("td:eq(3)").html();
        var margin = $(this).closest('tr').find("td:eq(4)").html();
        var hargajual = $(this).closest('tr').find("td:eq(5)").html();
        var kuantitas = $(this).closest('tr').find("td:eq(6)").html();
        var total = $(this).closest('tr').find("td:eq(7)").html();
        var idunitpelayanan = $(this).closest('tr').find("td:eq(8)").html();
        var namaunitpelayanan = $(this).closest('tr').find("td:eq(9)").html();

        // Disabled Button Action
        $("#historyAlkes button").prop("disabled", true);
        $("#actionDataAlkes").html('<div class="btn-group"><button id="saveDataAlkes" class="btn btn-primary"><i class="fa fa-check"> Simpan</i></button> <button id="cancelDataAlkes" class="btn btn-danger"><i class="fa fa-reply"> Batal</i></button></div>');

        $("#fAlkesRowId").val(idrow);
        $('#fAlkesId').val(idalkes);
        $('#fAlkesNama').val(namaalkes);
        $('#fAlkesHargaDasar').val(hargadasar);
        $('#fAlkesMargin').val(margin);
        $('#fAlkesHargaJual').val(hargajual);
        $('#fAlkesKuantitas').val(kuantitas);
        $('#fAlkesTotal').val(total);
        $('#fAlkesIdUnitPelayanan').val(idunitpelayanan);
        $('#fAlkesUnitPelayanan').val(namaunitpelayanan);
    });

    $(document).on('click', '#cancelDataAlkes', function() {
        // Enable Button Action
        $("#historyAlkes button").prop("disabled", false);
        $("#actionDataAlkes").html('<button id="saveDataAlkes" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</button>');

        // Clear Form
        $("#fAlkesRowId").val('');
        $('#fAlkesId').val('');
        $('#fAlkesNama').val('');
        $('#fAlkesHargaDasar').val('');
        $('#fAlkesMargin').val('');
        $('#fAlkesHargaJual').val('');
        $('#fAlkesKuantitas').val('');
        $('#fAlkesTotal').val('');
        $('#fAlkesIdUnitPelayanan').val('');
        $('#fAlkesUnitPelayanan').val('');
    });

    $(document).on('click', '.removeDataAlkes', function() {
        var idrow = $(this).data('idrow');
        var idrawatinap = $('#tempIdRawatInap').val();

        event.preventDefault();
        swal({
            title: "Apakah anda yakin ingin menyetujui data ini?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {
            // Approve Status
            $.ajax({
                url: '{site_url}trawatinap_tindakan/removeDataAlkes',
                method: 'POST',
                data: {
                    idrow: idrow
                },
                success: function(data) {
                    swal({
                        title: "Berhasil!",
                        text: "Data Alkes Telah Dihapus.",
                        type: "success",
                        timer: 1500,
                        showConfirmButton: false
                    });

                    getHistoryAlkes(idrawatinap);
                }
            });
        });
    });

    // Obat Modal : Unit Pelayanan
    $(document).on('click', '#fObatSubModal', function() {
        var idunitpelayanan = $("#subObatIdUnit option:selected").val();
        var idpendaftaran = $('.tindakanIdPendaftaran').val();

        $('#listObatTable').DataTable().destroy();
        $('#listObatTable').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                url: '{site_url}trawatinap_tindakan/getListObatUnit/' + idpendaftaran + '/' + idunitpelayanan,
                type: "POST",
                dataType: 'json'
            }
        });
    });

    $(document).on('change', '#subObatIdUnit', function() {
        var idunitpelayanan = $(this).val();
        var idpendaftaran = $('.tindakanIdPendaftaran').val();

        $('#listObatTable').DataTable().destroy();
        $('#listObatTable').DataTable({
            "autoWidth": false,
            "pageLength": 10,
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                url: '{site_url}trawatinap_tindakan/getListObatUnit/' + idpendaftaran + '/' + idunitpelayanan,
                type: "POST",
                dataType: 'json'
            }
        });
    });

    $(document).on('click', '.selectObatTable', function() {
        $('#fObatId').val($(this).closest('tr').find("td:eq(0) a").data('idobat'));
        $('#fObatNama').val($(this).closest('tr').find("td:eq(1)").html());
        $('#fObatHargaDasar').val($(this).closest('tr').find("td:eq(0) a").data('hargadasar'));
        $('#fObatMargin').val($(this).closest('tr').find("td:eq(0) a").data('margin'));
        $('#fObatHargaJual').val($(this).closest('tr').find("td:eq(3)").html());
        $('#fObatKuantitas').val(1);
        $('#fObatTotal').val($(this).closest('tr').find("td:eq(3)").html());
        $('#fObatIdUnitPelayanan').val($("#subObatIdUnit option:selected").val());
        $('#fObatUnitPelayanan').val($("#subObatIdUnit option:selected").text());
        $('#fObatKuantitas').focus();
    });

    $(document).on('keyup', '#fObatKuantitas', function() {
        var hargajual = $('#fObatHargaJual').val().replace(/,/g, '');
        var kuantitas = $(this).val().replace(/,/g, '');
        $('#fObatTotal').val(parseInt(hargajual) * parseInt(kuantitas));
    });

    $(document).on('click', '#saveDataObat', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var idRow = $('#fObatRowId').val();
        var tanggal = $('#fObatTanggal').val();
        var idunitpelayanan = $('#fObatIdUnitPelayanan').val();
        var idobat = $('#fObatId').val();
        var hargadasar = $('#fObatHargaDasar').val();
        var margin = $('#fObatMargin').val();
        var hargajual = $('#fObatHargaJual').val();
        var kuantitas = $('#fObatKuantitas').val();
        var totalkeseluruhan = $('#fObatTotal').val();

        if (tanggal == '') {
            sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
                $('#fObatTanggal').focus();
            });
            return false;
        }

        if (idobat == '') {
            sweetAlert("Maaf...", "Obat belum dipilih !", "error").then((value) => {
                $('#fObatId').focus();
            });
            return false;
        }

        if (hargajual == '' || hargajual <= 0) {
            sweetAlert("Maaf...", "Tarif tidak boleh kosong !", "error").then((value) => {
                $('#fObatHargaJual').focus();
            });
            return false;
        }

        if (kuantitas == '' || kuantitas <= 0) {
            sweetAlert("Maaf...", "Kuantitas tidak boleh kosong !", "error").then((value) => {
                $('#fTindakanJumlah').focus();
            });
            return false;
        }

        if (totalkeseluruhan == '' || totalkeseluruhan <= 0) {
            sweetAlert("Maaf...", "Kuantitas tidak boleh kosong !", "error").then((value) => {
                $('#fObatTotal').focus();
            });
            return false;
        }

        if (idunitpelayanan == '') {
            sweetAlert("Maaf...", "Unit Pelayanan tidak boleh kosong !", "error").then((value) => {
                $('#fObatUnitPelayanan').focus();
            });
            return false;
        }

        $.ajax({
            url: '{site_url}trawatinap_tindakan/saveDataObat',
            method: 'POST',
            data: {
                idpendaftaran: idpendaftaran,
                idrow: idRow,
                tanggal: tanggal,
                idunitpelayanan: idunitpelayanan,
                idobat: idobat,
                hargadasar: hargadasar,
                margin: margin,
                hargajual: hargajual,
                kuantitas: kuantitas,
                totalkeseluruhan: totalkeseluruhan
            },
            success: function(data) {
                swal({
                    title: "Berhasil!",
                    text: "Data Obat Telah Tersimpan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                getHistoryObat(idpendaftaran);

                // Clear Form
                $('#fObatRowId').val('');
                $('#fObatId').val('');
                $('#fObatNama').val('');
                $('#fObatHargaDasar').val('');
                $('#fObatMargin').val('');
                $('#fObatHargaJual').val('');
                $('#fObatKuantitas').val('');
                $('#fObatTotal').val('');
                $('#fObatIdUnitPelayanan').val('');
                $('#fObatUnitPelayanan').val('');

                // Enable Button Action
                $("#historyObat button").prop("disabled", false);
                $("#actionDataObat").html('<button id="saveDataObat" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</button>');
            }
        });
    });

    $(document).on('click', '.editDataObat', function() {
        var idrow = $(this).data('idrow');
        var idobat = $(this).closest('tr').find("td:eq(1)").html();
        var namaobat = $(this).closest('tr').find("td:eq(2)").html();
        var hargadasar = $(this).closest('tr').find("td:eq(3)").html();
        var margin = $(this).closest('tr').find("td:eq(4)").html();
        var hargajual = $(this).closest('tr').find("td:eq(5)").html();
        var kuantitas = $(this).closest('tr').find("td:eq(6)").html();
        var total = $(this).closest('tr').find("td:eq(7)").html();
        var idunitpelayanan = $(this).closest('tr').find("td:eq(8)").html();
        var namaunitpelayanan = $(this).closest('tr').find("td:eq(9)").html();

        // Disabled Button Action
        $("#historyObat button").prop("disabled", true);
        $("#actionDataObat").html('<div class="btn-group"><button id="saveDataObat" class="btn btn-primary"><i class="fa fa-check"> Simpan</i></button> <button id="cancelDataObat" class="btn btn-danger"><i class="fa fa-reply"> Batal</i></button></div>');

        $("#fObatRowId").val(idrow);
        $('#fObatId').val(idobat);
        $('#fObatNama').val(namaobat);
        $('#fObatHargaDasar').val(hargadasar);
        $('#fObatMargin').val(margin);
        $('#fObatHargaJual').val(hargajual);
        $('#fObatKuantitas').val(kuantitas);
        $('#fObatTotal').val(total);
        $('#fObatIdUnitPelayanan').val(idunitpelayanan);
        $('#fObatUnitPelayanan').val(namaunitpelayanan);
    });

    $(document).on('click', '#cancelDataObat', function() {
        // Enable Button Action
        $("#historyObat button").prop("disabled", false);
        $("#actionDataObat").html('<button id="saveDataObat" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</button>');

        // Clear Form
        $("#fObatRowId").val('');
        $('#fObatId').val('');
        $('#fObatNama').val('');
        $('#fObatHargaDasar').val('');
        $('#fObatMargin').val('');
        $('#fObatHargaJual').val('');
        $('#fObatKuantitas').val('');
        $('#fObatTotal').val('');
        $('#fObatIdUnitPelayanan').val('');
        $('#fObatUnitPelayanan').val('');
    });

    $(document).on('click', '.removeDataObat', function() {
        var idrow = $(this).data('idrow');
        var idrawatinap = $('#tempIdRawatInap').val();

        event.preventDefault();
        swal({
            title: "Apakah anda yakin ingin menyetujui data ini?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {
            // Approve Status
            $.ajax({
                url: '{site_url}trawatinap_tindakan/removeDataObat',
                method: 'POST',
                data: {
                    idrow: idrow
                },
                success: function(data) {
                    swal({
                        title: "Berhasil!",
                        text: "Data Obat Telah Dihapus.",
                        type: "success",
                        timer: 1500,
                        showConfirmButton: false
                    });

                    getHistoryObat(idrawatinap);
                }
            });
        });
    });

    // Tindakan Modal : Unit Pelayanan
    $("#fTindakanId").select2({
        minimumInputLength: 2,
        ajax: {
            url: '{site_url}trawatinap_tindakan/getTindakanRawatInap',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
            data: function(params) {
                var query = {
                    search: params.term,
                }
                return query;
            },
            processResults: function(data) {
                return {
                    results: $.map(data, function(item) {
                        return {
                            text: item.nama,
                            id: item.id
                        }
                    })
                };
            }
        }
    });

    $(document).on('click', '#fTindakanSearch', function() {
        var idpendaftaran = $('.tindakanIdPendaftaran').val();
        var idtipe = '1';

        getTarifTindakan(idpendaftaran, idtipe);
    });

    $("#subTindakanIdTipe").change(function() {
        var idpendaftaran = $('#tempIdRawatInap').val();
        var idtipe = $(this).val();

        getTarifTindakan(idpendaftaran, idtipe);
    });

    $(document).on('click', '.selectTindakanTable', function() {
        var idpendaftaran = $('.tindakanIdPendaftaran').val();
        var idtindakan = $(this).closest('tr').find("td:eq(0) a").data('idtindakan');

        $.ajax({
            url: "{site_url}trawatinap_tindakan/getDetailTindakanRawatInap/" + idpendaftaran + "/" + idtindakan,
            dataType: "json",
            success: function(data) {
                $("#fTindakanJasaSarana").val(data.jasasarana);
                $("#fTindakanJasaPelayanan").val(data.jasapelayanan);
                $("#fTindakanBHP").val(data.bhp);
                $("#fTindakanBiayaPerawatan").val(data.biayaperawatan);
                $("#fTindakanTarif").val(data.total);
                $("#fTindakanKuantitas").val(1);
                $("#fTindakanJumlah").val(data.total);

                $('#fTindakanId').select2("trigger", "select", {
                    data: {
                        id: idtindakan,
                        text: data.nama
                    }
                });

                $("#fTindakanKuantitas").focus();
            }
        });

    });

    $(document).on('change', '#fTindakanId', function() {
        var idpendaftaran = $('.tindakanIdPendaftaran').val();
        var idtindakan = $(this).val();

        $.ajax({
            url: "{site_url}trawatinap_tindakan/getDetailTindakanRawatInap/" + idpendaftaran + "/" + idtindakan,
            dataType: "json",
            success: function(data) {
                $("#fTindakanJasaSarana").val(data.jasasarana);
                $("#fTindakanJasaPelayanan").val(data.jasapelayanan);
                $("#fTindakanBHP").val(data.bhp);
                $("#fTindakanBiayaPerawatan").val(data.biayaperawatan);
                $("#fTindakanTarif").val(data.total);
                $("#fTindakanJumlah").val(data.total);

                $("#fTindakanKuantitas").focus();
            }
        });
    });

    $(document).on('keyup', '#fTindakanKuantitas', function() {
        var hargajual = $('#fTindakanTarif').val().replace(/,/g, '');
        var kuantitas = $(this).val().replace(/,/g, '');
        $('#fTindakanJumlah').val(parseInt(hargajual) * parseInt(kuantitas));
    });

    $(document).on('click', '#saveDataTindakan', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var idpendaftaran = $("#tempIdRawatInap").val();
        var idRow = $('#fTindakanRowId').val();
        var tanggal = $('#fTindakanTanggal').val();
        var iddokter = $('#fTindakanIdDokter option:selected').val();
        var idpelayanan = $('#fTindakanId option:selected').val();
        var jasasarana = $('#fTindakanJasaSarana').val();
        var jasapelayanan = $('#fTindakanJasaPelayanan').val();
        var bhp = $('#fTindakanBHP').val();
        var biayaperawatan = $('#fTindakanBiayaPerawatan').val();
        var total = $('#fTindakanTarif').val();
        var kuantitas = $('#fTindakanKuantitas').val();
        var totalkeseluruhan = $('#fTindakanJumlah').val();

        if (tanggal == '') {
            sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
                $('#fTindakanTanggal').focus();
            });
            return false;
        }

        if (idpelayanan == '') {
            sweetAlert("Maaf...", "Tindakan belum dipilih !", "error").then((value) => {
                $("#fTindakanId").select2('open');
            });
            return false;
        }

        if (total == '' || total <= 0) {
            sweetAlert("Maaf...", "Tarif tidak boleh kosong !", "error").then((value) => {
                $('#fTindakanTarif').focus();
            });
            return false;
        }

        if (kuantitas == '' || kuantitas <= 0) {
            sweetAlert("Maaf...", "Kuantitas tidak boleh kosong !", "error").then((value) => {
                $('#fTindakanKuantitas').focus();
            });
            return false;
        }

        if (totalkeseluruhan == '' || totalkeseluruhan <= 0) {
            sweetAlert("Maaf...", "Kuantitas tidak boleh kosong !", "error").then((value) => {
                $('#fTindakanJumlah').focus();
            });
            return false;
        }

        $.ajax({
            url: '{site_url}trawatinap_tindakan/saveDataTindakan',
            method: 'POST',
            data: {
                idpendaftaran: idpendaftaran,
                idrow: idRow,
                tanggal: tanggal,
                iddokter: iddokter,
                idpelayanan: idpelayanan,
                jasasarana: jasasarana,
                jasapelayanan: jasapelayanan,
                bhp: bhp,
                biayaperawatan: biayaperawatan,
                subtotal: total,
                kuantitas: kuantitas,
                totalkeseluruhan: totalkeseluruhan,
            },
            success: function(data) {
                swal({
                    title: "Berhasil!",
                    text: "Data Tindakan Telah Tersimpan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                getHistoryTindakan(idpendaftaran);

                // Clear Form
                $("#fTindakanRowId").val('');
                $('#fTindakanIdDokter').select2("trigger", "select", {
                    data: {
                        id: '',
                        text: ''
                    }
                });
                $('#fTindakanId').select2("trigger", "select", {
                    data: {
                        id: '',
                        text: ''
                    }
                });
                $('#fTindakanJasaSarana').val('');
                $('#fTindakanJasaSarana').val('');
                $('#fTindakanBHP').val('');
                $('#fTindakanBiayaPerawatan').val('');
                $('#fTindakanTarif').val('');
                $('#fTindakanKuantitas').val('');
                $('#fTindakanJumlah').val('');

                // Enable Button Action
                $("#historyTindakan button").prop("disabled", false);
                $("#actionDataTindakan").html('<button id="saveDataTindakan" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</button>');
            }
        });
    });

    $(document).on('click', '.editDataTindakan', function() {
        var idrow = $(this).data('idrow');
        var tanggal = $(this).closest('tr').find("td:eq(0)").html();
        var idtarif = $(this).closest('tr').find("td:eq(1)").html();
        var namatarif = $(this).closest('tr').find("td:eq(2)").html();
        var tarif = $(this).closest('tr').find("td:eq(3)").html();
        var kuantitas = $(this).closest('tr').find("td:eq(4)").html();
        var totaltarif = $(this).closest('tr').find("td:eq(5)").html();
        var iddokter = $(this).closest('tr').find("td:eq(6)").html();
        var namadokter = $(this).closest('tr').find("td:eq(7)").html();

        // Disabled Button Action
        $("#historyTindakan button").prop("disabled", true);
        $("#actionDataTindakan").html('<div class="btn-group"><button id="saveDataTindakan" class="btn btn-primary"><i class="fa fa-check"> Simpan</i></button> <button id="cancelDataTindakan" class="btn btn-danger"><i class="fa fa-reply"> Batal</i></button></div>');

        $("#fTindakanRowId").val(idrow);
        $("#fTindakanTanggal").val(tanggal);
        $('#fTindakanId').select2("trigger", "select", {
            data: {
                id: idtarif,
                text: namatarif
            }
        });
        $("#fTindakanTarif").val(tarif);
        $("#fTindakanKuantitas").val(kuantitas);
        $("#fTindakanJumlah").val(totaltarif);
        $('#fTindakanIdDokter').select2("trigger", "select", {
            data: {
                id: iddokter,
                text: namadokter
            }
        });
    });

    $(document).on('click', '#cancelDataTindakan', function() {
        // Enable Button Action
        $("#historyTindakan button").prop("disabled", false);
        $("#actionDataTindakan").html('<button id="saveDataTindakan" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</button>');

        // Clear Form
        $("#fTindakanRowId").val('');
        $('#fTindakanIdDokter').select2("trigger", "select", {
            data: {
                id: '',
                text: ''
            }
        });
        $('#fTindakanId').select2("trigger", "select", {
            data: {
                id: '',
                text: ''
            }
        });
        $('#fTindakanJasaSarana').val('');
        $('#fTindakanJasaSarana').val('');
        $('#fTindakanBHP').val('');
        $('#fTindakanBiayaPerawatan').val('');
        $('#fTindakanTarif').val('');
        $('#fTindakanKuantitas').val('');
        $('#fTindakanJumlah').val('');
    });

    $(document).on('click', '.removeDataTindakan', function() {
        var idrow = $(this).data('idrow');
        var idrawatinap = $('#tempIdRawatInap').val();

        event.preventDefault();
        swal({
            title: "Apakah anda yakin ingin menghapus data ini?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {
            // Approve Status
            $.ajax({
                url: '{site_url}trawatinap_tindakan/removeDataTindakan',
                method: 'POST',
                data: {
                    idrow: idrow
                },
                success: function(data) {
                    swal({
                        title: "Berhasil!",
                        text: "Data Tindakan Telah Dihapus.",
                        type: "success",
                        timer: 1500,
                        showConfirmButton: false
                    });

                    getHistoryTindakan(idrawatinap);
                }
            });
        });
    });

    // Daftar Operasi
    $("#operasiJam").datetimepicker({
        format: "HH:mm",
        stepping: 30
    });

    $(document).on('click', '#actionTabDaftarOperasi', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var idpasien = $("#tempIdPasien").val();
        var namapasien = $("#tempNamaPasien").val();

        $("#operasiIdPasien").val(idpasien);
        $("#operasiNamapasien").val(namapasien);

        // Get Status Operasi
        $.ajax({
            url: "{site_url}trawatinap_tindakan/getDataOperasi/" + idpendaftaran,
            dataType: "json",
            success: function(data) {
                if (data != null) {
                    if (data.status == '2' || data.status == '0' || data.statussetuju == '2') {
                        $("#StatusOperasiDisabled").hide();
                        $("#FormOperasi").show();
                        $("#saveDaftarOperasi").show();
                    } else {
                        $("#StatusOperasiDisabled").show();
                        $("#FormOperasi").hide();
                        $("#TanggalOperasiDisabled").html(data.tanggaloperasi);
                        $("#saveDaftarOperasi").hide();
                    }
                } else {
                    $("#StatusOperasiDisabled").hide();
                    $("#FormOperasi").show();
                    $("#saveDaftarOperasi").show();
                }
            }
        });
    });

    $(document).on('click', '#actionTabDataOperasi', function() {
        $("#saveDaftarOperasi").hide();
    });

    $(document).on('click', '#openModalOperasi', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var namapasien = $("#tempNamaPasien").val();
        var statuspembayaran = $("#tempStatusPembayaran").val();

        if (statuspembayaran == 1) {
            $("#actionTabDaftarOperasi").hide();
            $("#tabDaftarOperasi").hide();
            $("#tabDaftarOperasi").removeClass('active');
            $("#tabDataOperasi").addClass('active');
            $("#saveDaftarOperasi").hide();
        }

        $("#operasiNamapasien").val(namapasien);

        // Get Status Operasi
        $.ajax({
            url: "{site_url}trawatinap_tindakan/getDataOperasi/" + idpendaftaran,
            dataType: "json",
            success: function(data) {
                if (data != null) {
                    if (data.status == '2' || data.status == '0' || data.statussetuju == '2') {
                        $("#StatusOperasiDisabled").hide();
                        $("#FormOperasi").show();
                        $("#saveDaftarOperasi").show();
                    } else {
                        $("#StatusOperasiDisabled").show();
                        $("#FormOperasi").hide();
                        $("#TanggalOperasiDisabled").html(data.tanggaloperasi);
                        $("#saveDaftarOperasi").hide();
                    }
                } else {
                    $("#StatusOperasiDisabled").hide();
                    $("#FormOperasi").show();
                    $("#saveDaftarOperasi").show();
                }
            }
        });

        getHistoryOperasi(idpendaftaran);
    });

    $(document).on('click', '#saveDaftarOperasi', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var tanggaloperasi = $('#operasiTanggal').val();
        var idpasienoperasi = $('#operasiIdPasien').val();
        var namapasienoperasi = $('#operasiNamapasien').val();
        var diagnosaoperasi = $('#operasiDiagnosa').val();
        var operasi = $('#operasiOperasi').val();
        var jamoperasi = $('#operasiJam').val();
        var petugasoperasi = $('#operasiNamaPetugas').val();
        var catatanoperasi = $('#operasiCatatan').val();

        if (tanggaloperasi == '') {
            sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
                $('#operasiTanggal').focus();
            });
            return false;
        }

        if (namapasienoperasi == '') {
            sweetAlert("Maaf...", "Nama Pasien tidak boleh kosong !", "error").then((value) => {
                $('#operasiNamapasien').focus();
            });
            return false;
        }

        if (diagnosaoperasi == '') {
            sweetAlert("Maaf...", "Diagnosa Operasi tidak boleh kosong !", "error").then((value) => {
                $('#operasiDiagnosa').focus();
            });
            return false;
        }

        if (jamoperasi == '') {
            sweetAlert("Maaf...", "Jam Operasi tidak boleh kosong !", "error").then((value) => {
                $('#operasiJam').focus();
            });
            return false;
        }

        if (petugasoperasi == '') {
            sweetAlert("Maaf...", "Petugas Operasi tidak boleh kosong !", "error").then((value) => {
                $('#operasiNamaPetugas').focus();
            });
            return false;
        }

        if (catatanoperasi == '') {
            sweetAlert("Maaf...", "Catatan Operasi tidak boleh kosong !", "error").then((value) => {
                $('#operasiCatatan').focus();
            });
            return false;
        }

        $.ajax({
            url: '{site_url}trawatinap_tindakan/saveDaftarOperasi',
            method: 'POST',
            data: {
                tipe: 1,
                idpendaftaran: idpendaftaran,
                idpasien: idpasienoperasi,
                tanggaloperasi: tanggaloperasi,
                namapasien: namapasienoperasi,
                diagnosaoperasi: diagnosaoperasi,
                operasi: operasi,
                jamoperasi: jamoperasi,
                petugasoperasi: petugasoperasi,
                catatanoperasi: catatanoperasi,
            },
            success: function(data) {
                swal({
                    title: "Berhasil!",
                    text: "Data Operasi Telah Tersimpan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                getHistoryOperasi(idpendaftaran);

                // Disabled Form Daftar Operasi
                $("#saveDaftarOperasi").hide();

                // Clear Form
                $('#operasiNamapasien').val('');
                $('#operasiDiagnosa').val('');
                $('#operasiOperasi').val('');
                $('#operasiJam').val('');
                $('#operasiNamaPetugas').val('');
                $('#operasiCatatan').val('');
            }
        });
    });

    // Rincian : Deposit, Estimasi & Verifikasi
    $(document).on('click', '#openModalRincian', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var nomedrec = $("#tempNoMedrec").val();
        var namapasien = $("#tempNamaPasien").val();

        var notelepon = $("#tempNoTelepon").val();

        var namaruangan = $("#tempRuangan").val();
        var namakelas = $("#tempKelas").val();
        var namabed = $("#tempBed").val();

        var kelompokpasien = $("#tempKelompokPasien").val();
        var namaperusahaan = $("#tempNamaPerusahaan").val();

        var penanggungjawab = $("#tempPenanggungJawab").val();
        var penanggungjawab_notelp = $("#tempPenanggungJawabNoTelepon").val();

        $(".tindakanIdPendaftaran").val(idpendaftaran);
        $(".tindakanNoMedrec").val(nomedrec);
        $(".tindakanNamaPasien").val(namapasien);

        $(".tindakanNoTelepon").val(notelepon);

        $(".tindakanRuangan").val(namaruangan);
        $(".tindakanKelas").val(namakelas);
        $(".tindakanBed").val(namabed);

        $(".tindakanKelompokPasien").val(kelompokpasien);
        $(".tindakanNamaPerusahaan").val(namaperusahaan);

        $(".tindakanPenanggungJawab").val(penanggungjawab);
        $(".tindakanPenanggungJawabTelepon").val(penanggungjawab_notelp);

        $("#historyPrintAll").attr('href', '{site_url}trawatinap_tindakan/print_rincian_deposit/' + idpendaftaran);

		var statusTransaksi = $(this).data('statustransaksi');
		if (statusTransaksi == 1) {
			$("#depositTanggal").prop('disabled', true);
			$("#depositIdMetodePembayaran").prop('disabled', true);
			$("#depositIdBank").prop('disabled', true);
			$("#depositNominal").prop('disabled', true);
			$("#depositTerimaDari").prop('disabled', true);
			$("#saveDataDeposit").prop('disabled', true);
		} else {
			$("#depositTanggal").prop('disabled', false);
			$("#depositIdMetodePembayaran").prop('disabled', false);
			$("#depositIdBank").prop('disabled', false);
			$("#depositNominal").prop('disabled', false);
			$("#depositTerimaDari").prop('disabled', false);
			$("#saveDataDeposit").prop('disabled', false);
		}

        getHistoryDeposit(idpendaftaran);
        getHistoryRincianEstimasiBiaya(idpendaftaran);
    });

    $(document).on('change', '#depositIdMetodePembayaran', function() {
        var idmetodepembayaran = $(this).val();

        if (idmetodepembayaran == 1) {
            $('#depositMetodeBank').hide();
        } else {

            // Get Bank
            $.ajax({
                url: '{site_url}trawatinap_tindakan/getBank',
                dataType: 'json',
                success: function(data) {
                    $("#depositIdBank").empty();
                    $("#depositIdBank").append("<option value=''>Pilih Opsi</option>");
                    for (var i = 0; i < data.length; i++) {
                        $("#depositIdBank").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
                    }
                }
            });

            $('#depositMetodeBank').show();
        }
    });

    $(document).on('click', '#saveDataDeposit', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var tanggal = $('#depositTanggal').val();
        var idmetodepembayaran = $('#depositIdMetodePembayaran option:selected').val();
        var idbank = $('#depositIdBank option:selected').val();
        var nominal = $('#depositNominal').val();
        var terimadari = $('#depositTerimaDari').val();
        var idtipe = 1;

        if (tanggal == '') {
            sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
                $('#depositTanggal').focus();
            });
            return false;
        }

        if (idmetodepembayaran == '') {
            sweetAlert("Maaf...", "Metode Pembayaran belum dipilih !", "error").then((value) => {
                $('#depositIdMetodePembayaran').select2('open');
            });
            return false;
        }

        if (idmetodepembayaran != 1) {
            if (idbank == '') {
                sweetAlert("Maaf...", "Bank belum dipilih !", "error").then((value) => {
                    $('#depositIdBank').select2('open');
                });
                return false;
            }
        }

        if (nominal == '' || nominal <= 0) {
            sweetAlert("Maaf...", "Nominal tidak boleh kosong !", "error").then((value) => {
                $('#depositNominal').focus();
            });
            return false;
        }

        $.ajax({
            url: '{site_url}trawatinap_tindakan/saveDataDeposit',
            method: 'POST',
            data: {
                idrawatinap: idpendaftaran,
                tanggal: tanggal,
                idmetodepembayaran: idmetodepembayaran,
                idbank: idbank,
                nominal: nominal,
                idtipe: idtipe,
                terimadari: terimadari
            },
            success: function(data) {
                swal({
                    title: "Berhasil!",
                    text: "Data Deposit Telah Tersimpan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                getHistoryDeposit(idpendaftaran);

                $('#depositIdMetodePembayaran').select2("trigger", "select", {
                    data: {
                        id: '',
                        text: ''
                    }
                });
                $('#depositMetodeBank').hide();
                $('#depositIdBank').select2("trigger", "select", {
                    data: {
                        id: '',
                        text: ''
                    }
                });
                $('#depositNominal').val('');
                $('#depositTerimaDari').val('');
            }
        });
    });

    $(document).on('click', '.openModalAlasanHapusDeposit', function() {
        $('#tempIdRow').val($(this).data('idrow'));
    });

    $(document).on('click', '#removeDeposit', function() {
        var idrow = $('#tempIdRow').val();
        var idrawatinap = $('#tempIdRawatInap').val();
        var alasanhapus = $('#alasanhapus').val();

        event.preventDefault();

        // Update Status
        $.ajax({
            url: '{site_url}trawatinap_tindakan/removeDataDeposit',
            method: 'POST',
            data: {
                idrow: idrow,
                alasanhapus: alasanhapus,
            },
            success: function(data) {
                swal({
                    title: "Berhasil!",
                    text: "Data Deposit Telah Dihapus.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                $("#alasanhapus").val('');
                getHistoryDeposit(idrawatinap);
            }
        });
    });

    // Rincian : Estimasi Biaya
    $(document).on('click', '.rOpenDetailEstimasiBiaya', function() {
        var idrow = $(this).data('idrow');
        var nomedrec = $("#tempNoMedrec").val();
        var namapasien = $("#tempNamaPasien").val();
        var umurpasien = $("#tempUmurPasien").val();
        var dokterpj = $("#tempDokterPJ").val();

        $('#rincianViewHeadEstimasiBiaya').hide();
        $('#rincianHistoryEstimasiBiaya').hide();
        $('#rincianViewEstimasiBiaya').show();

        // Get Data Estimasi
        $.ajax({
            url: "{site_url}trawatinap_tindakan/getDataEstimasiBiaya/" + idrow,
            dataType: "json",
            success: function(data) {

                $("#rEstimasiNomedrec").val(nomedrec);
                $("#rEstimasiNamapasien").val(namapasien);
                $("#rEstimasiUmur").val(umurpasien);
                $("#rEstimasiDokter").val(dokterpj);

                if (data != null) {
                    if (data.fotoscan == null) {
                        $('#rEstimasiScan').hide();
                        $('#rEstimasiUpload').show();
                        $('#rEstimasiActionUpload').show();
                    }

                    $("#rEstimasiId").val(idrow);
                    $("#rEstimasiScan").attr("href", "{site_url}assets/upload/estimasi/" + data.fotoscan);
                    $("#rEstimasiDiagnosa").val(data.diagnosa);
                    $("#rEstimasiOperasi").val(data.operasi);
                    $("#rEstimasiJenisOperasi").val(data.namajenisoperasi);
                    $("#rEstimasiSewaKamar").val(data.sewakamar);
                    $("#rEstimasiJasaMedisOperator").val(data.jasadokteroperator);
                    $("#rEstimasiJasaMedisAnesthesi").val(data.jasadokteranaesthesi);
                    $("#rEstimasiJasaMedisAssisten").val(data.jasaassisten);
                    $("#rEstimasiObatBhp").val(data.obatbhp);
                    $("#rEstimasiQtyRuangPerawatan").val(data.kuantitasruangperawatan);
                    $("#rEstimasiRuangPerawatan").val(data.ruangperawatan);
                    $("#rEstimasiQtyHCU").val(data.kuantitasruanghcu);
                    $("#rEstimasiHCU").val(data.ruanghcu);
                    $("#rEstimasiObatPerawatan").val(data.obatperawatan);
                    $("#rEstimasiPenunjang").val(data.penunjanglain);

                    $("#rEstimasiDiagnosa").prop('readonly', true);
                    $("#rEstimasiOperasi").prop('readonly', true);
                    $("#rEstimasiJenisOperasi").prop('readonly', true);
                    $("#rEstimasiSewaKamar").prop('readonly', true);
                    $("#rEstimasiJasaMedisOperator").prop('readonly', true);
                    $("#rEstimasiJasaMedisAnesthesi").prop('readonly', true);
                    $("#rEstimasiJasaMedisAssisten").prop('readonly', true);
                    $("#rEstimasiObatBhp").prop('readonly', true);
                    $("#rEstimasiQtyRuangPerawatan").prop('readonly', true);
                    $("#rEstimasiRuangPerawatan").prop('readonly', true);
                    $("#rEstimasiQtyHCU").prop('readonly', true);
                    $("#rEstimasiHCU").prop('readonly', true);
                    $("#rEstimasiObatPerawatan").prop('readonly', true);
                    $("#rEstimasiPenunjang").prop('readonly', true);
                }
            }
        });

        // Rincian : Get Data Estimasi Implant
        $.ajax({
            url: "{site_url}trawatinap_tindakan/getDataEstimasiBiayaImplant/" + idrow,
            dataType: "json",
            success: function(data) {
                $('#rincianHisotryEstimasiImplant tbody').empty();
                for (var i = 0; i < data.length; i++) {
                    $('#rincianHisotryEstimasiImplant tbody').append('<tr><td>' + data[i].namaimplan + '</td><td>' + $.number(data[i].harga) + '</td><td>' + $.number(data[i].kuantitas) + '</td><td>' + data[i].status + '</td></tr>');
                }
            }
        });
    });

    $(document).on('click', '.rApproveEstimasi', function() {
        var idrow = $(this).data('idrow');
        var idrawatinap = $('#tempIdRawatInap').val();

        event.preventDefault();
        swal({
            title: "Apakah anda yakin ingin menyetujui data ini?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {
            // Approve Status
            $.ajax({
                url: '{site_url}trawatinap_tindakan/approveDataEstimasi',
                method: 'POST',
                data: {
                    idrow: idrow
                },
                success: function(data) {
                    swal({
                        title: "Berhasil!",
                        text: "Data Estimasi Telah Disetujui.",
                        type: "success",
                        timer: 1500,
                        showConfirmButton: false
                    });

                    getHistoryRincianEstimasiBiaya(idrawatinap);
                }
            });
        });
    });

    $(document).on('click', '#rincianTambahEstimasi', function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var nomedrec = $("#tempNoMedrec").val();
        var namapasien = $("#tempNamaPasien").val();
        var umurpasien = $("#tempUmurPasien").val();
        var dokterpj = $("#tempDokterPJ").val();

        $("#rincianHisotryEstimasiImplant tbody").html("");

        $("#rEstimasiScan").attr("href", "");
        $("#rEstimasiNomedrec").val(nomedrec);
        $("#rEstimasiNamapasien").val(namapasien);
        $("#rEstimasiUmur").val(umurpasien);
        $("#rEstimasiDokter").val(dokterpj);
        $("#rEstimasiDiagnosa").val("");
        $("#rEstimasiOperasi").val("");
        $("#rEstimasiJenisOperasi").val("");
        $("#rEstimasiSewaKamar").val("");
        $("#rEstimasiJasaMedisOperator").val("");
        $("#rEstimasiJasaMedisAnesthesi").val("");
        $("#rEstimasiJasaMedisAssisten").val("");
        $("#rEstimasiObatBhp").val("");
        $("#rEstimasiQtyRuangPerawatan").val("");
        $("#rEstimasiRuangPerawatan").val("");
        $("#rEstimasiQtyHCU").val("");
        $("#rEstimasiHCU").val("");
        $("#rEstimasiObatPerawatan").val("");
        $("#rEstimasiPenunjang").val("");

        $("#rEstimasiDiagnosa").prop('readonly', false);
        $("#rEstimasiOperasi").prop('readonly', false);
        $("#rEstimasiJenisOperasi").prop('readonly', false);
        $("#rEstimasiSewaKamar").prop('readonly', false);
        $("#rEstimasiJasaMedisOperator").prop('readonly', false);
        $("#rEstimasiJasaMedisAnesthesi").prop('readonly', false);
        $("#rEstimasiJasaMedisAssisten").prop('readonly', false);
        $("#rEstimasiObatBhp").prop('readonly', false);
        $("#rEstimasiQtyRuangPerawatan").prop('readonly', false);
        $("#rEstimasiRuangPerawatan").prop('readonly', false);
        $("#rEstimasiQtyHCU").prop('readonly', false);
        $("#rEstimasiHCU").prop('readonly', false);
        $("#rEstimasiObatPerawatan").prop('readonly', false);
        $("#rEstimasiPenunjang").prop('readonly', false);

        $(".fEstimasiInput").show();
        $(".fEstimasiView").hide();
    });

    $(document).on('click', '.rincianCloseDetailEstimasiBiaya', function() {
        $('#rincianHistoryEstimasiBiaya').show();
        $('#rincianViewHeadEstimasiBiaya').show();
        $('#rincianViewEstimasiBiaya').hide();

        $(".fEstimasiInput").hide();
        $(".fEstimasiView").show();
    });

    $("#fImplanId").select2({
        minimumInputLength: 2,
        ajax: {
            url: '{site_url}trawatinap_tindakan/getImplan',
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
            data: function(params) {
                var query = {
                    search: params.term,
                }
                return query;
            },
            processResults: function(data) {
                return {
                    results: $.map(data, function(item) {
                        return {
                            text: item.nama,
                            id: item.id
                        }
                    })
                };
            }
        }
    });

    $(document).on('change', '#fImplanId', function() {
        $.ajax({
            url: "{site_url}trawatinap_tindakan/getDetailImplan/" + $(this).val(),
            dataType: "json",
            success: function(data) {
                $("#fImplanHargaDasar").val(data.hargadasar);
                $("#fImplanMargin").val(data.margin);
                $("#fImplanHargaJual").val(data.hargajual);
                $("#fImplanKuantitas").val(1);

                $("#fImplanKuantitas").focus();
            }
        });
    });

    $(document).on('click', '#tambahDataImplan', function() {
        var idimplan = $("#fImplanId option:selected").val();
        var hargajual = $("#fImplanHargaJual").val();
        var kuantitas = $("#fImplanKuantitas").val();
        var status = $("#fImplanStatus option:selected").val();

        if (idimplan == '') {
            sweetAlert("Maaf...", "Implan belum dipilih !", "error").then((value) => {
                $('#fImplanId').select2('open');
            });
            return false;
        }

        if (hargajual == '' || hargajual <= 0) {
            sweetAlert("Maaf...", "Harga Jual tidak boleh kosong !", "error").then((value) => {
                $('#fImplanHargaJual').focus();
            });
            return false;
        }

        if (kuantitas == '' || kuantitas <= 0) {
            sweetAlert("Maaf...", "Kuantitas tidak boleh kosong !", "error").then((value) => {
                $('#fImplanKuantitas').focus();
            });
            return false;
        }

        if (status == '') {
            sweetAlert("Maaf...", "Status Implan belum dipilih !", "error").then((value) => {
                $('#fImplanStatus').select2('open');
            });
            return false;
        }

        var content = "";
        content += "<tr>";
        content += "<td style='display:none;'>" + $("#fImplanId option:selected").val() + "</td>";
        content += "<td>" + $("#fImplanId option:selected").text() + "</td>";
        content += "<td style='display:none;'>" + $("#fImplanHargaDasar").val(); + "</td>";
        content += "<td style='display:none;'>" + $("#fImplanMargin").val(); + "</td>";
        content += "<td>" + $("#fImplanHargaJual").val(); + "</td>";
        content += "<td>" + $("#fImplanKuantitas").val(); + "</td>";
        content += "<td>" + $("#fImplanStatus").val(); + "</td>";
        content += "<td><a href='#' class='btn btn-danger hapusDataImplan' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash'></i></a></td>";
        content += "</tr>";

        $('#rincianHisotryEstimasiImplant tbody').append(content);

        $('#fImplanId').select2("trigger", "select", {
            data: {
                id: '',
                text: ''
            }
        });
        $("#fImplanHargaDasar").val('');
        $("#fImplanMargin").val('');
        $("#fImplanHargaJual").val('');
        $("#fImplanKuantitas").val('');
        $('#fImplanStatus').select2("trigger", "select", {
            data: {
                id: '',
                text: ''
            }
        });
    });

    $(document).on("click", ".hapusDataImplan", function() {
        var thisRow = $(this);
        swal({
            title: "Hapus data ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {
            thisRow.closest('td').parent().remove();
        });

        return false;
    });

    $(document).on("click", "#saveDataEstimasi", function() {
        var idpendaftaran = $("#tempIdRawatInap").val();
        var diagnosa = $("#rEstimasiDiagnosa").val();
        var operasi = $("#rEstimasiOperasi").val();
        var jenisoperasi = $("#rEstimasiIdJenisOperasi").val();
        var jenisoperasi = $("#rEstimasiIdJenisOperasi").val();
        var sewakamar = $("#rEstimasiSewaKamar").val();
        var jasadokteroperator = $("#rEstimasiJasaMedisOperator").val();
        var jasadokteranaesthesi = $("#rEstimasiJasaMedisAnesthesi").val();
        var jasaassisten = $("#rEstimasiJasaMedisAssisten").val();
        var obatbhp = $("#rEstimasiObatBhp").val();
        var ruangperawatan = $("#rEstimasiRuangPerawatan").val();
        var kuantitasruangperawatan = $("#rEstimasiQtyRuangPerawatan").val();
        var ruanghcu = $("#rEstimasiHCU").val();
        var kuantitasruanghcu = $("#rEstimasiQtyHCU").val();
        var obatperawatan = $("#rEstimasiObatPerawatan").val();
        var penunjanglain = $("#rEstimasiPenunjang").val();

        var implanTable = $('table#rincianHisotryEstimasiImplant tbody tr').get().map(function(row) {
            return $(row).find('td').get().map(function(cell) {
                return $(cell).html();
            });
        });

        $("#fImplanDataArray").val(JSON.stringify(implanTable));

        $.ajax({
            url: '{site_url}trawatinap_tindakan/saveDataEstimasi',
            method: 'POST',
            data: {
                idpendaftaran: idpendaftaran,
                diagnosa: diagnosa,
                operasi: operasi,
                jenisoperasi: jenisoperasi,
                sewakamar: sewakamar,
                jasadokteroperator: jasadokteroperator,
                jasadokteranaesthesi: jasadokteranaesthesi,
                jasaassisten: jasaassisten,
                obatbhp: obatbhp,
                ruangperawatan: ruangperawatan,
                kuantitasruangperawatan: kuantitasruangperawatan,
                ruanghcu: ruanghcu,
                kuantitasruanghcu: kuantitasruanghcu,
                obatperawatan: obatperawatan,
                penunjanglain: penunjanglain,
                dataImplan: $("#fImplanDataArray").val()
            },
            success: function(data) {
                swal({
                    title: "Berhasil!",
                    text: "Data Estimasi Telah Tersimpan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                getHistoryRincianEstimasiBiaya(idpendaftaran);

                // Back Form to List
                $('#rincianHistoryEstimasiBiaya').show();
                $('#rincianViewHeadEstimasiBiaya').show();
                $('#rincianViewEstimasiBiaya').hide();

                $(".fEstimasiInput").hide();
                $(".fEstimasiView").show();
            }
        });
    });

    $(document).on("click", "#rEstimasiActionUpload", function() {

        var formData = new FormData();
        var fotoscan = $('#rEstimasiFile')[0].files[0];
        formData.append('fotoscan', fotoscan);

        var idestimasi = $("#rEstimasiId").val();

        $.ajax({
            url: '{site_url}trawatinap_tindakan/uploadFotoScan/' + idestimasi,
            method: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                swal({
                    title: "Berhasil!",
                    text: "Data Foto Scan Telah Tersimpan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                // Back Form to List
                $('#rincianHistoryEstimasiBiaya').show();
                $('#rincianViewHeadEstimasiBiaya').show();
                $('#rincianViewEstimasiBiaya').hide();

                $(".fEstimasiInput").hide();
                $(".fEstimasiView").show();

                $('#rEstimasiScan').show();
                $('#rEstimasiUpload').hide();
                $('#rEstimasiActionUpload').hide();
            }
        });
    });

    // RUJUKAN RAWAT INAP
    $(document).on('click', '#rujukFarmasi', function() {
        var idrawatinap = $("#tempIdRawatInap").val();
        var checkboxRujukan = $(this);

        event.preventDefault();
        swal({
            title: "Apakah anda yakin ingin merujuk pasien ke Farmasi ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {
            // Approve Status
            $.ajax({
                url: '{site_url}trawatinap_tindakan/rujukFarmasi',
                method: 'POST',
                data: {
                    idrawatinap: idrawatinap
                },
                success: function(data) {
                    swal({
                        title: "Berhasil!",
                        text: "Pasien Telah Dirujuk Ke Farmasi.",
                        type: "success",
                        timer: 1500,
                        showConfirmButton: false
                    });

                    checkboxRujukan.prop('checked', true);
                    checkboxRujukan.prop('disabled', true);
                }
            });
        });
    });

    $(document).on('click', '#rujukLaboratorium', function() {
        var idrawatinap = $("#tempIdRawatInap").val();
        var checkboxRujukan = $(this);

        event.preventDefault();
        swal({
            title: "Apakah anda yakin ingin merujuk pasien ke Laboratorium ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {
            // Approve Status
            $.ajax({
                url: '{site_url}trawatinap_tindakan/rujukLaboratorium',
                method: 'POST',
                data: {
                    idrawatinap: idrawatinap
                },
                success: function(data) {
                    swal({
                        title: "Berhasil!",
                        text: "Pasien Telah Dirujuk Ke Laboratorium.",
                        type: "success",
                        timer: 1500,
                        showConfirmButton: false
                    });

                    checkboxRujukan.prop('checked', true);
                    checkboxRujukan.prop('disabled', true);
                }
            });
        });
    });

    $(document).on('click', '#rujukRadiologi', function() {
        var idrawatinap = $("#tempIdRawatInap").val();
        var checkboxRujukan = $(this);

        event.preventDefault();
        swal({
            title: "Apakah anda yakin ingin merujuk pasien ke Radiologi ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {
            // Approve Status
            $.ajax({
                url: '{site_url}trawatinap_tindakan/rujukRadiologi',
                method: 'POST',
                data: {
                    idrawatinap: idrawatinap
                },
                success: function(data) {
                    swal({
                        title: "Berhasil!",
                        text: "Pasien Telah Dirujuk Ke Radiologi.",
                        type: "success",
                        timer: 1500,
                        showConfirmButton: false
                    });

                    checkboxRujukan.prop('checked', true);
                    checkboxRujukan.prop('disabled', true);
                }
            });
        });
    });

    $(document).on('click', '#rujukFisioterapi', function() {
        var idrawatinap = $("#tempIdRawatInap").val();
        var checkboxRujukan = $(this);

        event.preventDefault();
        swal({
            title: "Apakah anda yakin ingin merujuk pasien ke Fisioterapi ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function() {
            // Approve Status
            $.ajax({
                url: '{site_url}trawatinap_tindakan/rujukFisioterapi',
                method: 'POST',
                data: {
                    idrawatinap: idrawatinap
                },
                success: function(data) {
                    swal({
                        title: "Berhasil!",
                        text: "Pasien Telah Dirujuk Ke Fisioterapi.",
                        type: "success",
                        timer: 1500,
                        showConfirmButton: false
                    });

                    checkboxRujukan.prop('checked', true);
                    checkboxRujukan.prop('disabled', true);
                }
            });
        });
    });

});

function getTarifTindakan(idpendaftaran, idtipe) {
    $('#listTindakanTable').DataTable().destroy();
    $('#listTindakanTable').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}trawatinap_tindakan/getListTindakanRawatInap/' + idpendaftaran + '/' + idtipe,
            type: "POST",
            dataType: 'json'
        }
    });
}

function getHistoryDokterPJ(idpendaftaran) {
    $.ajax({
        url: '{site_url}trawatinap_tindakan/getCurrentDokterPJ/' + idpendaftaran,
        dataType: 'json',
        success: function(data) {
            $('#currentDokterPJ').empty();
            $('#currentDokterPJ').append('<tr><td>' + data[0].namadokterpenanggungjawab + '<br><span style="font-size: 12px;"> (' + data[0].tanggal + ' - <b>Sekarang</b>)</span></td></tr>');

            $.ajax({
                url: '{site_url}trawatinap_tindakan/getHistoryDokterPJ/' + idpendaftaran,
                method: 'POST',
                dataType: 'json',
                success: function(data) {
                    $('#historyDokterPJ').empty();
                    for (var i = 0; i < data.length; i++) {
                        $('#historyDokterPJ').append('<tr><td>' + data[i].namadokterpenanggungjawab + '<br><span style="font-size: 12px;"> (' + data[i].tanggaldari + ' - ' + data[i].tanggalsampai + ')</span></td></tr>');
                    }
                }
            });
        }
    });
}

function getHistoryPindahBed(idpendaftaran) {
    $.ajax({
        url: '{site_url}trawatinap_tindakan/getHistoryPindahBed/' + idpendaftaran,
        dataType: 'json',
        success: function(data) {
            $('#historyPindahBed tbody').empty();
            for (var i = 0; i < data.length; i++) {
                var number = (i + 1);
                $('#historyPindahBed tbody').append('<tr><td>' + number + '</td><td>' + data[i].tanggaldari + '</td><td>' + data[i].tanggalsampai + '</td><td>' + data[i].namaruangan + '</td><td>' + data[i].namakelas + '</td><td>' + data[i].namabed + '</td><td>' + data[i].namapetugas + '</td></tr>');
            }
        }
    });
}

function getHistoryEstimasiBiaya(idpendaftaran) {
    $.ajax({
        url: '{site_url}trawatinap_tindakan/getHistoryEstimasiBiaya/' + idpendaftaran,
        dataType: 'json',
        success: function(data) {
            $('#historyEstimasiBiaya tbody').empty();
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    var number = (i + 1);
                    $('#historyEstimasiBiaya tbody').append('<tr><td>' + data[i].tanggal + '</td><td>' + statusEstimasi(data[i].status) + '</td><td><a href="#" data-idrow="' + data[i].id + '" class="btn btn-sm btn-primary openDetailEstimasiBiaya"><i class="fa fa-eye"></i> Lihat</a></td></tr>');
                }
            } else {
                $('#historyEstimasiBiaya').attr('class', 'table');
                $('#historyEstimasiBiaya').html('<div class="alert alert-danger alert-dismissable" style="margin:10px"><h3 class="font-w300 push-15">Maaf...</h3><p>Belum terdapat Estimasi Biaya yang dapat ditampilkan !</p></div>');
            }
        }
    });
}

function getHistoryRincianEstimasiBiaya(idpendaftaran) {
    $.ajax({
        url: '{site_url}trawatinap_tindakan/getHistoryEstimasiBiaya/' + idpendaftaran,
        dataType: 'json',
        success: function(data) {
            $('#rincianHistoryEstimasiBiaya tbody').empty();
            for (var i = 0; i < data.length; i++) {
                var number = (i + 1);
                var action = '';

                action = '<div class="btn-group">'; 
				<?php if (UserAccesForm($user_acces_form, array('401'))) { ?>
                    action += '<a href="#" data-idrow="' + data[i].id + '" class="btn btn-sm btn-primary rOpenDetailEstimasiBiaya"><i class="fa fa-eye"></i> Lihat</a>';
				<? } ?>

                if (data[i].status == 1) {
                    <?php if (UserAccesForm($user_acces_form, array('402'))) { ?>
                        action += '<a href="#" data-idrow="' + data[i].id + '" class="btn btn-sm btn-success rApproveEstimasi"><i class="fa fa-check"></i> Setujui</a>';
					<? } ?>
                }
                action += '</div>';

                $('#rincianHistoryEstimasiBiaya tbody').append('<tr><td>' + data[i].tanggal + '</td><td>' + statusEstimasi(data[i].status) + '</td><td>' + action + '</td></tr>');
            }
        }
    });
}

function getHistoryTindakan(idpendaftaran) {
    $.ajax({
        url: '{site_url}trawatinap_tindakan/getHistoryTindakan/' + idpendaftaran,
        dataType: 'json',
        success: function(data) {
            $('#historyTindakan tbody').empty();
            for (var i = 0; i < data.length; i++) {
                var number = (i + 1);
                if (data[i].statusverifikasi == 1) {
                    var action = '<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>';
                } else {
                    var action = '';
					<?php if (UserAccesForm($user_acces_form, array('379'))) { ?>
                        action += '<button type="button" data-idrow="' + data[i].id + '" class="btn btn-sm btn-primary editDataTindakan"><i class="fa fa-pencil"></i> Ubah</button>&nbsp;';
					<? } ?>

                    <?php if (UserAccesForm($user_acces_form, array('380'))) { ?>
                        action += '<button type="button" data-idrow="' + data[i].id + '" class="btn btn-sm btn-danger removeDataTindakan"><i class="fa fa-trash"></i> Hapus</button>'; 
					<? } ?>
                }
                $('#historyTindakan tbody').append('<tr><td>' + data[i].tanggal + '</td><td style="display:none">' + data[i].idpelayanan + '</td><td>' + data[i].namatindakan + '</td><td>' + $.number(data[i].total) + '</td><td>' + $.number(data[i].kuantitas) + '</td><td>' + $.number(data[i].totalkeseluruhan) + '</td><td style="display:none;">' + data[i].iddokter + '</td><td>' + (data[i].namadokter != null ? data[i].namadokter : "-") + '</td><td>' + action + '</td></tr>');
            }

            getTotalHistoryTindakan();
        }
    });
}

function getTotalHistoryTindakan() {
    var idpendaftaran = $('#tempIdRawatInap').val();

    var totalkeseluruhan = 0;
    $('#historyTindakan tbody tr').each(function() {
        totalkeseluruhan += parseFloat($(this).find('td:eq(5)').text().replace(/,/g, ''));
    });

    $.ajax({
        url: "{site_url}trawatinap_tindakan/getTotalDeposit/" + idpendaftaran,
        dataType: "json",
        success: function(data) {
            var deposit = data.totaldeposit;

            $("#fTindakanTotal").html($.number(totalkeseluruhan));
            $("#fTindakanTotalDeposit").html($.number(deposit));
            $("#fTindakanSisaDeposit").html($.number(parseInt(deposit) - parseInt(totalkeseluruhan)));
        }
    });
}

function getHistoryAlkes(idpendaftaran) {
    $.ajax({
        url: '{site_url}trawatinap_tindakan/getHistoryAlkes/' + idpendaftaran,
        dataType: 'json',
        success: function(data) {
            $('#historyAlkes tbody').empty();
            for (var i = 0; i < data.length; i++) {
                var number = (i + 1);
                if (data[i].statusverifikasi == 1) {
                    var action = '<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>';
                } else {
                    var action = '';
					<?php if (UserAccesForm($user_acces_form, array('383'))) { ?>
                        action += '<button type="button" data-idrow="' + data[i].id + '" class="btn btn-sm btn-primary editDataAlkes"><i class="fa fa-pencil"></i> Ubah</button>&nbsp;';
					<? } ?>
                    
					<?php if (UserAccesForm($user_acces_form, array('384'))) { ?>
                        action += '<button type="button" data-idrow="' + data[i].id + '" class="btn btn-sm btn-danger removeDataAlkes"><i class="fa fa-trash"></i> Hapus</button>';
					<? } ?>
                }
                $('#historyAlkes tbody').append('<tr><td>' + data[i].tanggal + '</td><td style="display:none">' + data[i].idalkes + '</td><td>' + data[i].namaalkes + '</td><td style="display:none">' + data[i].hargadasar + '</td><td style="display:none">' + data[i].margin + '</td><td>' + $.number(data[i].hargajual) + '</td><td>' + $.number(data[i].kuantitas) + '</td><td>' + $.number(data[i].totalkeseluruhan) + '</td><td style="display:none">' + data[i].idunit + '</td><td>' + data[i].namaunitpelayanan + '</td><td>' + action + '</td></tr>');
            }

            getTotalHistoryAlkes();
        }
    });
}

function getTotalHistoryAlkes() {
    var totalkeseluruhan = 0;
    $('#historyAlkes tbody tr').each(function() {
        totalkeseluruhan += parseFloat($(this).find('td:eq(7)').text().replace(/,/g, ''));
    });

    $("#alkesTotal").html($.number(totalkeseluruhan));
}

function getHistoryObat(idpendaftaran) {
    $.ajax({
        url: '{site_url}trawatinap_tindakan/getHistoryObat/' + idpendaftaran,
        dataType: 'json',
        success: function(data) {
            $('#historyObat tbody').empty();
            for (var i = 0; i < data.length; i++) {
                var number = (i + 1);
                if (data[i].statusverifikasi == 1) {
                    var action = '<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>';
                } else {
                    var action = '';
					<?php if (UserAccesForm($user_acces_form, array('383'))) { ?>
                        action += '<button type="button" data-idrow="' + data[i].id + '" class="btn btn-sm btn-primary editDataObat"><i class="fa fa-pencil"></i> Ubah</button>&nbsp;';
					<? } ?>
                    
					<?php if (UserAccesForm($user_acces_form, array('384'))) { ?>
                        action += '<button type="button" data-idrow="' + data[i].id + '" class="btn btn-sm btn-danger removeDataObat"><i class="fa fa-trash"></i> Hapus</button>';
					<? } ?>
                }
                $('#historyObat tbody').append('<tr><td>' + data[i].tanggal + '</td><td style="display:none">' + data[i].idobat + '</td><td>' + data[i].namaobat + '</td><td style="display:none">' + data[i].hargadasar + '</td><td style="display:none">' + data[i].margin + '</td><td>' + $.number(data[i].hargajual) + '</td><td>' + $.number(data[i].kuantitas) + '</td><td>' + $.number(data[i].totalkeseluruhan) + '</td><td style="display:none">' + data[i].idunit + '</td><td>' + data[i].namaunitpelayanan + '</td><td>' + action + '</td></tr>');
            }

            getTotalHistoryObat();
        }
    });
}

function getTotalHistoryObat() {
    var totalkeseluruhan = 0;
    $('#historyObat tbody tr').each(function() {
        totalkeseluruhan += parseFloat($(this).find('td:eq(7)').text().replace(/,/g, ''));
    });

    $("#obatTotal").html($.number(totalkeseluruhan));
}

function getHistoryVisiteDokter(idpendaftaran) {
    $.ajax({
        url: '{site_url}trawatinap_tindakan/getHistoryVisiteDokter/' + idpendaftaran,
        dataType: 'json',
        success: function(data) {
            $('#historyVisiteDokter tbody').empty();
            for (var i = 0; i < data.length; i++) {
                var number = (i + 1);
                if (data[i].statusverifikasi == 1) {
                    var action = '<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>';
                } else {
                    var action = '';
					
					<?php if (UserAccesForm($user_acces_form, array('387'))) { ?>
                        action += '<button type="button" data-idrow="' + data[i].id + '" class="btn btn-sm btn-primary editDataVisiteDokter"><i class="fa fa-pencil"></i> Ubah</button>&nbsp;';
					<? } ?>
                    
					<?php if (UserAccesForm($user_acces_form, array('388'))) { ?>
                        action += '<button type="button" data-idrow="' + data[i].id + '" class="btn btn-sm btn-danger removeDataVisiteDokter"><i class="fa fa-trash"></i> Hapus</button>';
					<? } ?>
                }
                $('#historyVisiteDokter tbody').append('<tr><td>' + data[i].tanggal + '</td><td style="display:none">' + data[i].iddokter + '</td><td>' + data[i].namadokter + '</td><td style="display:none">' + data[i].idruangan + '</td><td>' + data[i].namaruangan + '</td><td>' + $.number(data[i].totalkeseluruhan) + '</td><td>' + action + '</td></tr>');
            }

            getTotalHistoryVisiteDokter();
        }
    });
}

function getTotalHistoryVisiteDokter() {
    var totalkeseluruhan = 0;
    $('#historyVisiteDokter tbody tr').each(function() {
        totalkeseluruhan += parseFloat($(this).find('td:eq(5)').text().replace(/,/g, ''));
    });

    $("#visiteTotal").html($.number(totalkeseluruhan));
}

function getHistoryOperasi(idpendaftaran) {
    $.ajax({
        url: '{site_url}trawatinap_tindakan/getHistoryOperasi/' + idpendaftaran,
        dataType: 'json',
        success: function(data) {
            $('#historyDaftarOperasi tbody').empty();
            for (var i = 0; i < data.length; i++) {
                var statusAcc = '';
                var statusDatang = '';
                var statusKamarOperasi = '';

                if (data[i].statussetuju == '1') {
                    statusAcc = '<span class="label label-primary">Disetujui</span>';
                } else {
                    statusAcc = '<span class="label label-default">Persetujuan</span>';
                }

                if (data[i].statusdatang == '1') {
                    statusDatang = '<span class="label label-primary">Pasien Sudah Datang</span>';
                } else {
                    statusDatang = '<span class="label label-default">Pasien Belum Datang</span>';
                }

                if (data[i].status == '1') {
                    statusKamarOperasi = `${statusAcc} ${statusDatang}`;
                } else if (data[i].status == '2') {
                    statusKamarOperasi = '<span class="label label-success">SELESAI</span>';
                } else if (data[i].status == '0') {
                    statusKamarOperasi = '<span class="label label-danger">DIBATALKAN</span>';
                }

                if (data[i].statussetuju == '2') {
                    statusKamarOperasi = '<span class="label label-danger">DITOLAK</span>';
                }

                var number = (i + 1);
                $('#historyDaftarOperasi tbody').append('<tr><td>' + data[i].tanggaloperasi + '</td><td>' + data[i].diagnosa + '</td><td>' + data[i].operasi + '</td><td>' + data[i].waktumulaioperasi + '</td><td>' + data[i].namapetugas + '</td><td>' + data[i].catatan + '</td><td>' + statusKamarOperasi + '</td><td>' + (data[i].tanggal_disetujui ? data[i].tanggal_disetujui : '-') + '</td><td>' + (data[i].user_menyetujui ? data[i].user_menyetujui : '-') + '</td></tr>');
            }
        }
    });
}

function getHistoryDeposit(idpendaftaran) {
    $.ajax({
        url: '{site_url}trawatinap_tindakan/getHistoryDeposit/' + idpendaftaran,
        dataType: 'json',
        success: function(data) {
            $('#historyDeposit tbody').empty();
            for (var i = 0; i < data.length; i++) {

                var number = (i + 1);
                var metode = '';
                if (data[i].idmetodepembayaran == 1) {
                    metode = data[i].metodepembayaran;
                } else {
                    metode = data[i].metodepembayaran + ' ( ' + data[i].namabank + ' )';
                }
                var action = '';
                action = '<div class="btn-group">';
				
				<?php if (UserAccesForm($user_acces_form, array('397'))) { ?>
                    action += '<a href="{site_url}trawatinap_tindakan/print_kwitansi_deposit/' + data[i].id + '" target="_blank" class="btn btn-sm btn-success">Print <i class="fa fa-print"></i></a>';
				<? } ?>
                
				<?php if (UserAccesForm($user_acces_form, array('396'))) { ?>
					if (data[i].statustransaksi == 1) {
						var action = '<button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>';
					} else {
                        action += '<button type="button" data-idrow="' + data[i].id + '" class="btn btn-sm btn-danger openModalAlasanHapusDeposit" data-toggle="modal" data-target="#AlasanHapusDepositModal">Hapus <i class="fa fa-close"></i></button>';
                    }
				<? } ?>

                action += '</div>';

                $('#historyDeposit tbody').append('<tr><td>' + data[i].tanggal + '</td><td>' + metode + '</td><td>' + $.number(data[i].nominal) + '</td><td>' + (data[i].terimadari ? data[i].terimadari : "-") + '</td><td>' + data[i].namapetugas + '</td><td>' + action + '</td></tr>');
            }

            getTotalHistoryDeposit();
        }
    });
}

function getTotalHistoryDeposit() {
    var totalkeseluruhan = 0;
    $('#historyDeposit tbody tr').each(function() {
        totalkeseluruhan += parseFloat($(this).find('td:eq(2)').text().replace(/,/g, ''));
    });

    $("#depositTotal").html($.number(totalkeseluruhan));
}

function statusEstimasi(status) {
    if (status == '2') {
        return '<span class="label label-success">Disetujui</span>';
    } else {
        return '<span class="label label-default">Menunggu Konfirmasi</span>';
    }
}

function getRuanganVisite(idpendaftaran) {
    $.ajax({
        url: "{site_url}trawatinap_tindakan/getRuanganVisite/" + idpendaftaran,
        dataType: "json",
        success: function(data) {
            $("#fVisiteIdRuangan").val(data.idruangan);
            $("#fVisiteIdKelas").val(data.idkelas);
            $("#fVisiteRuangan").val(data.namaruangan);
        }
    });
}

function previewImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#rEstimasiPriview').html('<div class="col-sm-12 animated fadeIn" style="margin-top:-80px"><a id="previewLink" href="#" class="img-link img-thumb"><img id="previewImage" class="img-responsive" src="#" alt=""></a></div>');
            $('#previewLink').attr('href', e.target.result);
            $('#previewImage').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function getLamaDirawat() {
    var tanggaldaftar = $('#tempTanggalDaftar').datepicker('getDate');
    var tanggalkeluar = $('#checkoutTanggalKeluar').datepicker('getDate');
    if (tanggalkeluar != '' && tanggaldaftar != '') {
        var lamadirawat = (tanggalkeluar.getTime() - tanggaldaftar.getTime()) / (24 * 60 * 60 * 1000);
        return lamadirawat + ' Hari';
    }
}

function disabledModalAksi() {
    var statuspembayaran = $("#tempStatusPembayaran").val();

    if (statuspembayaran == 1) {
        $("#historyTindakan tbody button").prop('disabled', true);
        $("#historyAlkes tbody button").prop('disabled', true);
        $("#historyVisiteDokter tbody button").prop('disabled', true);
    } else {
        $("#historyTindakan tbody button").prop('disabled', false);
        $("#historyAlkes tbody button").prop('disabled', false);
        $("#historyVisiteDokter tbody button").prop('disabled', false);
    }
}

function format(data) {
    var rows = '';

    rows += '<div class="col-md-12">';
    rows += '	<div class="col-md-6">';
    rows += '		<table class="table" style="background-color: transparent;">';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>Tanggal Pendaftaran</b></td>';
    rows += '				<td width="70%">' + data.tanggaldaftar + '</td>';
    rows += '			</tr>';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>No. Medrec</b></td>';
    rows += '				<td width="70%">' + data.nomedrec + '</td>';
    rows += '			</tr>';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>Nama Pasien</b></td>';
    rows += '				<td width="70%">' + data.namapasien + '</td>';
    rows += '			</tr>';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>Jenis Kelamin</b></td>';
    rows += '				<td width="70%">' + data.jeniskelamin + '</td>';
    rows += '			</tr>';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>Umur Pasien</b></td>';
    rows += '				<td width="70%">' + data.umur + '</td>';
    rows += '			</tr>';
    rows += '	</table>';
    rows += '	</div>';
    rows += '	<div class="col-md-6">';
    rows += '		<table class="table" style="background-color: transparent;">';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>Ruangan</b></td>';
    rows += '				<td width="70%">' + data.namaruangan + '</td>';
    rows += '			</tr>';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>Kelas</b></td>';
    rows += '				<td width="70%">' + data.namakelas + '</td>';
    rows += '			</tr>';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>Bed</b></td>';
    rows += '				<td width="70%">' + data.namabed + '</td>';
    rows += '			</tr>';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>Dokter Perujuk</b></td>';
    rows += '				<td width="70%">' + data.namadokterperujuk + '</td>';
    rows += '			</tr>';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>Dokter Penganggung Jawab</b></td>';
    rows += '				<td width="70%">' + data.namadokterpenanggungjawab + '</td>';
    rows += '			</tr>';
    rows += '	</table>';
    rows += '	</div>';
    rows += '</div>';
    rows += '<div class="col-md-12" style="margin: 10px;">';
    rows += '	<div class="btn-group">';
	
	<?php if (UserAccesForm($user_acces_form, array('373'))) { ?>
        rows += '		<button ' + (data.statuskasir == 1 ? "disabled" : "") + ' id="openModalDokterPJ" data-toggle="modal" data-target="#DokterPJModal" class="btn btn-primary btn-sm"><i class="fa fa-user-md"></i> Dokter</button>';
	<? } ?>

    <?php if (UserAccesForm($user_acces_form, array('374'))) { ?>
        rows += '		<button ' + (data.statuskasir == 1 ? "disabled" : "") + ' id="openModalBed" data-toggle="modal" data-target="#BedModal" class="btn btn-primary btn-sm"><i class="fa fa-hotel"></i> Pindah Bed</button>';
	<? } ?>
    
	<?php if (UserAccesForm($user_acces_form, array('376'))) { ?>
        rows += '		<button ' + (data.statuskasir == 1 ? "disabled" : "") + ' id="openModalEstimasi" data-toggle="modal" data-target="#EstimasiModal" class="btn btn-primary btn-sm"><i class="fa fa-paste"></i> Estimasi Biaya</button>';
	<? } ?>
    
	<?php if (UserAccesForm($user_acces_form, array('377'))) { ?>
        rows += '		<button ' + (data.statuskasir == 1 ? "disabled" : "") + ' id="openModalAksi" data-toggle="modal" data-target="#AksiModal" class="btn btn-primary btn-sm"><i class="fa fa-stethoscope"></i> Aksi</button>';
	<? } ?>
    
	<?php if (UserAccesForm($user_acces_form, array('390'))) { ?>
        rows += '		<button ' + (data.statuskasir == 1 ? "disabled" : "") + ' id="openModalOperasi" data-toggle="modal" data-target="#OperasiModal" class="btn btn-primary btn-sm"><i class="fa fa-sign-language"></i> Operasi</button>';
	<? } ?>
    
	<?php if (UserAccesForm($user_acces_form, array('393'))) { ?>
        rows += '		<button id="openModalRincian" data-statustransaksi="' + data.statustransaksi + '" data-toggle="modal" data-target="#RincianModal" class="btn btn-success btn-sm"><i class="fa fa-newspaper-o"></i> Rincian</button>';
	<? } ?>
    
	<?php if (UserAccesForm($user_acces_form, array('403'))) { ?>
        rows += '		<a href="{site_url}trawatinap_tindakan/verifikasi/' + data.id + '" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Verifikasi</a>';
	<? } ?>

    rows += '	</div>';
    rows += '	&nbsp;&nbsp;&nbsp;';
	
	<?php if (UserAccesForm($user_acces_form, array('420'))) { ?>
        rows += '	<div class="btn-group">';
        rows += '		<button class="btn btn-danger btn-sm" id="openModalCheckout" data-toggle="modal" data-target="#CheckoutModal" ' + (data.statuskasir == 1 ? "" : "disabled") + '><i class="fa fa-sign-out"></i> Checkout</button>';
        rows += '	</div>';
	<? } ?>

    rows += '	<hr style="margin-bottom:5px; border:1px solid #eeee">';
    rows += '	<div class="control-group">';
    rows += '		<div class="checkbox">';
    rows += '			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">';
    rows += '   		<input type="checkbox" id="rujukFarmasi" ' + (data.rujukfarmasi == 1 ? "checked disabled" : (data.statuskasir == 1 ? "disabled" : "")) + ' value=""><span></span>Farmasi';
    rows += '   	</label>';
    rows += '		</div>';
    rows += '		<div class="checkbox">';
    rows += '			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">';
    rows += '   		<input type="checkbox" id="rujukLaboratorium" ' + (data.rujuklaboratorium == 1 ? "checked disabled" : (data.statuskasir == 1 ? "disabled" : "")) + ' value=""><span></span>Laboratorium';
    rows += '  		</label>';
    rows += '		</div>';
    rows += '		<div class="checkbox">';
    rows += '			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">';
    rows += '   		<input type="checkbox" id="rujukRadiologi" ' + (data.rujukradiologi == 1 ? "checked disabled" : (data.statuskasir == 1 ? "disabled" : "")) + ' value=""><span></span>Radiologi';
    rows += '   	</label>';
    rows += '		</div>';
    rows += '		<div class="checkbox">';
    rows += '			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">';
    rows += '   		<input type="checkbox" id="rujukFisioterapi" ' + (data.rujukfisioterapi == 1 ? "checked disabled" : (data.statuskasir == 1 ? "disabled" : "")) + ' value=""><span></span>Fisioterapi';
    rows += '   	</label>';
    rows += '		</div>';
    rows += '	</div>';
    rows += '</div>';

    $("#tempIdRawatInap").val(data.id);
    $("#tempTanggalDaftar").datepicker('setDate', data.tanggaldaftar);
    $("#tempNoMedrec").val(data.nomedrec);
    $("#tempIdPasien").val(data.idpasien);
    $("#tempNamaPasien").val(data.namapasien);
    $("#tempUmurPasien").val(data.umur);
    $("#tempDokterPJ").val(data.namadokterpenanggungjawab);

    $("#tempNoTelepon").val((data.hp ? data.hp : data.telepon));

    $("#tempRuangan").val(data.namaruangan);
    $("#tempKelas").val(data.namakelas);
    $("#tempBed").val(data.namabed);

    $("#tempIdKelompokPasien").val(data.idkelompokpasien);
    $("#tempKelompokPasien").val(data.namakelompok);
    $("#tempNamaPerusahaan").val(data.namarekanan);

    $("#tempPenanggungJawab").val(data.nama_keluarga);
    $("#tempPenanggungJawabNoTelepon").val(data.telepon_keluarga);

    $("#tempStatusCheckout").val(data.statuscheckout);
    $("#tempStatusPembayaran").val(data.statuspembayaran);

    return rows;
}

function formatCheckout(data) {
    var rows = '';

    rows += '<div class="col-md-12">';
    rows += '	<div class="col-md-6">';
    rows += '		<table class="table" style="background-color: transparent;">';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>Tanggal Pendaftaran</b></td>';
    rows += '				<td width="70%">' + data.tanggaldaftar + '</td>';
    rows += '			</tr>';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>No. Medrec</b></td>';
    rows += '				<td width="70%">' + data.nomedrec + '</td>';
    rows += '			</tr>';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>Nama Pasien</b></td>';
    rows += '				<td width="70%">' + data.namapasien + '</td>';
    rows += '			</tr>';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>Jenis Kelamin</b></td>';
    rows += '				<td width="70%">' + data.jeniskelamin + '</td>';
    rows += '			</tr>';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>Umur Pasien</b></td>';
    rows += '				<td width="70%">' + data.umur + '</td>';
    rows += '			</tr>';
    rows += '	</table>';
    rows += '	</div>';
    rows += '	<div class="col-md-6">';
    rows += '		<table class="table" style="background-color: transparent;">';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>Ruangan</b></td>';
    rows += '				<td width="70%">' + data.namaruangan + '</td>';
    rows += '			</tr>';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>Kelas</b></td>';
    rows += '				<td width="70%">' + data.namakelas + '</td>';
    rows += '			</tr>';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>Bed</b></td>';
    rows += '				<td width="70%">' + data.namabed + '</td>';
    rows += '			</tr>';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>Dokter Perujuk</b></td>';
    rows += '				<td width="70%">' + data.namadokterperujuk + '</td>';
    rows += '			</tr>';
    rows += '			<tr>';
    rows += '				<td width="30%"><b>Dokter Penganggung Jawab</b></td>';
    rows += '				<td width="70%">' + data.namadokterpenanggungjawab + '</td>';
    rows += '			</tr>';
    rows += '	</table>';
    rows += '	</div>';
    rows += '</div>';
    rows += '<div class="col-md-12" style="margin: 10px;">';
    rows += '	<div class="btn-group">';
	
	<?php if (UserAccesForm($user_acces_form, array('1251'))) { ?>
        rows += '		<button ' + (data.statuskasir == 1 ? "disabled" : "") + ' id="openModalDokterPJ" data-toggle="modal" data-target="#DokterPJModal" class="btn btn-primary btn-sm"><i class="fa fa-user-md"></i> Dokter</button>';
	<? } ?>
    
	<?php if (UserAccesForm($user_acces_form, array('1252'))) { ?>
        rows += '		<button ' + (data.statuskasir == 1 ? "disabled" : "") + ' id="openModalBed" data-toggle="modal" data-target="#BedModal" class="btn btn-primary btn-sm"><i class="fa fa-hotel"></i> Pindah Bed</button>';
	<? } ?>
    
	<?php if (UserAccesForm($user_acces_form, array('1254'))) { ?>
        rows += '		<button ' + (data.statuskasir == 1 ? "disabled" : "") + ' id="openModalEstimasi" data-toggle="modal" data-target="#EstimasiModal" class="btn btn-primary btn-sm"><i class="fa fa-paste"></i> Estimasi Biaya</button>';
	<? } ?>
    
	<?php if (UserAccesForm($user_acces_form, array('1255'))) { ?>
        rows += '		<button ' + (data.statuskasir == 1 ? "disabled" : "") + ' id="openModalAksi" data-toggle="modal" data-target="#AksiModal" class="btn btn-primary btn-sm"><i class="fa fa-stethoscope"></i> Aksi</button>';
	<? } ?>
    
	<?php if (UserAccesForm($user_acces_form, array('1268'))) { ?>
        rows += '		<button ' + (data.statuskasir == 1 ? "disabled" : "") + ' id="openModalOperasi" data-toggle="modal" data-target="#OperasiModal" class="btn btn-primary btn-sm"><i class="fa fa-sign-language"></i> Operasi</button>';
	<? } ?>
    
	<?php if (UserAccesForm($user_acces_form, array('1271'))) { ?>
        rows += '		<button ' + (data.statuskasir == 1 ? "disabled" : "") + ' id="openModalRincian" data-statustransaksi="' + data.statustransaksi + '" data-toggle="modal" data-target="#RincianModal" class="btn btn-success btn-sm"><i class="fa fa-newspaper-o"></i> Rincian</button>';
	<? } ?>
    
	<?php if (UserAccesForm($user_acces_form, array('1281'))) { ?>
        rows += '		<a href="{site_url}trawatinap_tindakan/verifikasi/' + data.id + '" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Verifikasi</a>';
	<? } ?>

    rows += '	</div>';
    rows += '	&nbsp;&nbsp;&nbsp;';
    rows += '	<div class="btn-group">';
	
	<?php if (UserAccesForm($user_acces_form, array('1298'))) { ?>
        rows += '		<button class="btn btn-danger btn-sm" id="openModalCheckout" data-toggle="modal" data-target="#CheckoutModal" ' + (data.statustransaksi == 1 ? "" : "disabled") + '><i class="fa fa-sign-out"></i> Data Checkout</button>';
	<? } ?>

    rows += '	</div>';
    rows += '	<hr style="margin-bottom:5px; border:1px solid #eeee">';
    rows += '	<div class="control-group">';
    rows += '		<div class="checkbox">';
    rows += '			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">';
    rows += '   		<input type="checkbox" id="rujukFarmasi" ' + (data.rujukfarmasi == 1 ? "checked disabled" : (data.statuskasir == 1 ? "disabled" : "")) + ' value=""><span></span>Farmasi';
    rows += '   	</label>';
    rows += '		</div>';
    rows += '		<div class="checkbox">';
    rows += '			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">';
    rows += '   		<input type="checkbox" id="rujukLaboratorium" ' + (data.rujuklaboratorium == 1 ? "checked disabled" : (data.statuskasir == 1 ? "disabled" : "")) + ' value=""><span></span>Laboratorium';
    rows += '  		</label>';
    rows += '		</div>';
    rows += '		<div class="checkbox">';
    rows += '			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">';
    rows += '   		<input type="checkbox" id="rujukRadiologi" ' + (data.rujukradiologi == 1 ? "checked disabled" : (data.statuskasir == 1 ? "disabled" : "")) + ' value=""><span></span>Radiologi';
    rows += '   	</label>';
    rows += '		</div>';
    rows += '		<div class="checkbox">';
    rows += '			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">';
    rows += '   		<input type="checkbox" id="rujukFisioterapi" ' + (data.rujukfisioterapi == 1 ? "checked disabled" : (data.statuskasir == 1 ? "disabled" : "")) + ' value=""><span></span>Fisioterapi';
    rows += '   	</label>';
    rows += '		</div>';
    rows += '	</div>';
    rows += '</div>';

    $("#tempIdRawatInap").val(data.id);
    $("#tempTanggalDaftar").datepicker('setDate', data.tanggaldaftar);
    $("#tempNoMedrec").val(data.nomedrec);
    $("#tempNamaPasien").val(data.namapasien);
    $("#tempUmurPasien").val(data.umur);
    $("#tempDokterPJ").val(data.namadokterpenanggungjawab);

    $("#tempNoTelepon").val((data.hp ? data.hp : data.telepon));

    $("#tempRuangan").val(data.namaruangan);
    $("#tempKelas").val(data.namakelas);
    $("#tempBed").val(data.namabed);

    $("#tempIdKelompokPasien").val(data.idkelompokpasien);
    $("#tempKelompokPasien").val(data.namakelompok);
    $("#tempNamaPerusahaan").val(data.namarekanan);

    $("#tempPenanggungJawab").val(data.nama_keluarga);
    $("#tempPenanggungJawabNoTelepon").val(data.telepon_keluarga);

    $("#tempStatusCheckout").val(data.statuscheckout);
    $("#tempStatusPembayaran").val(data.statuspembayaran);

    return rows;
}

</script>
