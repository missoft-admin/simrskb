<!-- Modal Proses Kwitasi -->
<div class="modal in" id="ProsesKwitansiModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-popout" style="width: 80%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <h3 class="block-title">Deskripsi Kwitansi</h3>
                </div>
                <div class="block-content">
                    <div class="form-horizontal">
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-2 control-label" for="" style="margin-top: 5px;">Sudah Terima Dari</label>
                            <div class="col-md-10">
                                <input type="text" id="kwitansi_terima_dari" class="form-control" placeholder="Sudah Terima Dari" value="{last_sudah_terima_dari}">
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-md-2 control-label" for="" style="margin-top: 5px;">Deskripsi</label>
                            <div class="col-md-10">
                                <textarea class="form-control" id="kwitansi_deskripsi" placeholder="Deskripsi" rows="10">{last_deskripsi}</textarea>
                            </div>
                        </div>
                    </div>

                    <br><br>

                    <h5><i class="fa fa-history"></i> History</h5>
                    <hr style="border-top-width: 2px;margin-top: 8px;">
                    <table class="table table-bordered table-striped" width="100%">
                        <tbody>
                            <?php foreach ($historyProsesKwitansi as $row) { ?>
                            <tr>
                                <td>
                                    <?=$row->tanggal?> - User : <?=$row->namauser?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                
                <input type="hidden" id="kwitansi_idmetode" value="">
                <input type="hidden" id="kwitansi_tipe_kontraktor" value="">
                <input type="hidden" id="kwitansi_idkontraktor" value="">

                <button id="cetakProsesKwitansi" class="btn btn-sm btn-success" type="button" style="width: 20%;"><i class="fa fa-check"></i> Cetak</button>
                <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>