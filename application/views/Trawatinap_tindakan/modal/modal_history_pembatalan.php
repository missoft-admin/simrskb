<!-- Modal History Pembatalan -->
<div class="modal in" id="HistoryBatalModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-popout">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-danger">
                    <h3 class="block-title">History Pembatalan</h3>
                </div>
                <div class="block-content">
                    <table class="table table-bordered table-striped" width="100%">
                        <tbody>
                            <?php foreach ($historyBatal as $row) { ?>
                            <tr>
                                <td>
                                    <?=$row->tanggalbatal?>
                                    <br>
                                    <span style="font-size: 12px;"> Alasan : <?=$row->alasanbatal_label?></span>
                                    <br>
                                    <span style="font-size: 12px;"> User : <?=$row->namauser?></span>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal" style="width: 20%;"><i class="fa fa-close"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>