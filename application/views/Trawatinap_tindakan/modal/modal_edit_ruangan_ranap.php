<div class="modal fade in black-overlay" id="EditTransaksiRuangRanapModal" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 70%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">EDIT TRANSAKSI</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">

							<input type="hidden" id="tRuangRanapRowId" readonly value="">
							<input type="hidden" id="tRuangRanapTable" readonly value="">
							<input type="hidden" id="tRuangRanapIdRuangan" readonly value="">
							<input type="hidden" id="tRuangRanapIdKelas" readonly value="">

							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label" for="tRuangRanapNoRegistrasi">No. Registrasi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tRuangRanapNoRegistrasi" placeholder="No. Registrasi" readonly value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="tRuangRanapNamaTarif">Nama Tarif</label>
									<div class="col-md-9">
										<input type="hidden" id="tRuangRanapIdTarif">
										<input type="text" class="form-control input-sm" id="tRuangRanapNamaTarif" placeholder="Nama Tarif" readonly value="">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label" for="tRuangRanapTanggal">Tanggal Dari</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tRuangRanapTanggalDari" placeholder="Tanggal Dari" readonly value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="tRuangRanapIdDokter">Tanggal Sampai</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tRuangRanapTanggalSampai" placeholder="Tanggal Sampai" readonly value="">
									</div>
								</div>
							</div>
						</form>
					</div>
					<table id="tRuangRanapItem" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 25%;">Item Biaya</th>
								<th style="width: 25%;">Harga</th>
								<th style="width: 10%;">Diskon (%)</th>
								<th style="width: 20%;">Diskon (Rp)</th>
								<th style="width: 20%;">Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Jasa Sarana</td>
								<td><input type="text" class="form-control input-sm number tdRuangRanapHarga" id="tRuangRanapJasaSarana" placeholder="Jasa Sarana"></td>
								<td><input type="text" class="form-control input-sm discount tdRuangRanapDiskonPersen" id="tRuangRanapJasaSaranaDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input type="text" class="form-control input-sm number tdRuangRanapDiskonRupiah" id="tRuangRanapJasaSaranaDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tRuangRanapJasaSaranaTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>Jasa Pelayanan</td>
								<td><input type="text" class="form-control input-sm number tdRuangRanapHarga" id="tRuangRanapJasaPelayanan" placeholder="Jasa Pelayanan"></td>
								<td><input type="text" class="form-control input-sm discount tdRuangRanapDiskonPersen" id="tRuangRanapJasaPelayananDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input type="text" class="form-control input-sm number tdRuangRanapDiskonRupiah" id="tRuangRanapJasaPelayananDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tRuangRanapJasaPelayananTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>BHP</td>
								<td><input type="text" class="form-control input-sm number tdRuangRanapHarga" id="tRuangRanapBHP" placeholder="BHP"></td>
								<td><input type="text" class="form-control input-sm discount tdRuangRanapDiskonPersen" id="tRuangRanapBHPDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input type="text" class="form-control input-sm number tdRuangRanapDiskonRupiah" id="tRuangRanapBHPDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tRuangRanapBHPTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>Biaya Perawatan</td>
								<td><input type="text" class="form-control input-sm number tdRuangRanapHarga" id="tRuangRanapBiayaPerawatan" placeholder="Biaya Perawatan"></td>
								<td><input type="text" class="form-control input-sm discount tdRuangRanapDiskonPersen" id="tRuangRanapBiayaPerawatanDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input type="text" class="form-control input-sm number tdRuangRanapDiskonRupiah" id="tRuangRanapBiayaPerawatanDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tRuangRanapBiayaPerawatanTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
						</tbody>
						<tfoot>
							<tr class="bg-light dker">
								<th colspan="3">
									<label class="pull-right"><b>Sub Total (Rp)</b></label>
								</th>
								<th colspan="2">
									<input type="text" class="form-control input-sm number" id="tRuangRanapSubTotal" placeholder="Sub Total (Rp)" readonly value="0"></th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="3">
									<label class="pull-right"><b>Kuantitas</b></label>
								</th>
								<th colspan="2">
									<input type="text" class="form-control input-sm number" id="tRuangRanapKuantitas" placeholder="Kuantitas" value="0"></th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="3"><label class="pull-right"><b>Total (Rp)</b></label></th>
								<th colspan="2">
									<input type="text" class="form-control input-sm number" id="tRuangRanapTotal" placeholder="Total (Rp)" readonly  value="0">
								</th>
							</tr>
							<tr>
								<th style="width: 85%;" colspan="3">
									<label class="pull-right">
										<b>Discount Semua</b>
									</label>
								</th>
								<th style="width: 15%;" colspan="2">
										<div class="input-group">
											<span class="input-group-addon">Rp</span>
											<input class="form-control number input-sm" type="text" id="tRuangRanapDiskonRupiah" placeholder="Discount (Rp)" value="0">
										</div>
										<div class="input-group">
											<span class="input-group-addon"> %. </span>
											<input class="form-control discount input-sm" type="text" id="tRuangRanapDiskonPersen" placeholder="Discount (%)" value="0">
										</div>
								</th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="3">
									<label class="pull-right">
										<b>Grand Total (Rp)</b>
									</label>
								</th>
								<th colspan="2">
									<input type="text" class="form-control input-sm number" id="tRuangRanapGrandTotal" placeholder="Grand Total (Rp)" readonly value="0">
								</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<div class="modal-footer" id="actionDataRuangRanap">
				<button class="btn btn-sm btn-primary" id="saveDataRuangRanap" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Edit</button>
				<button class="btn btn-sm btn-default" id="cancelDataRuangRanap" type="button" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#tRuangRanapKuantitas").keyup(function(){
			var hargajual = $('#tRuangRanapSubTotal').val().replace(/,/g, '');
			var kuantitas = $(this).val().replace(/,/g, '');
			$('#tRuangRanapTotal').val(parseInt(hargajual) * parseInt(kuantitas));

			calculateDiscountRuangRanap();
		});

		$("#tRuangRanapDiskonPersen").keyup(function(){
			if ($("#tRuangRanapDiskonPersen").val() == ''){
				$("#tRuangRanapDiskonPersen").val(0)
			}
			if (parseFloat($(this).val()) > 100){
				$(this).val(100);
			}

			var discount_rupiah = parseFloat($("#tRuangRanapTotal").val() * parseFloat($(this).val())/100);
			$("#tRuangRanapDiskonRupiah").val(discount_rupiah);

			calculateDiscountRuangRanap();
		});

		$("#tRuangRanapDiskonRupiah").keyup(function(){
			if ($("#tRuangRanapDiskonRupiah").val()==''){
				$("#tRuangRanapDiskonRupiah").val(0)
			}
			if (parseFloat($(this).val()) > $("#tRuangRanapTotal").val()){
				$(this).val($("#tRuangRanapTotal").val());
			}

			var discount_percent = parseFloat((parseFloat($(this).val()*100))/$("#tRuangRanapTotal").val());
			$("#tRuangRanapDiskonPersen").val(discount_percent);

			calculateDiscountRuangRanap();
		});

		$(document).on('click', '#saveDataRuangRanap', function() {
			var idrow = $('#tRuangRanapRowId').val();
			var table = $('#tRuangRanapTable').val();
			var idtarif = $('#tRuangRanapIdTarif').val();
			var idruangan = $('#tRuangRanapIdRuangan').val();
			var idkelas = $('#tRuangRanapIdKelas').val();
			var tanggal_dari = $('#tRuangRanapTanggalDari').val();
			var tanggal_sampai = $('#tRuangRanapTanggalSampai').val();
			var jasasarana = $('#tRuangRanapJasaSarana').val();
			var jasasarana_disc = $('#tRuangRanapJasaSaranaDiskonRupiah').val();
			var jasapelayanan = $('#tRuangRanapJasaPelayanan').val();
			var jasapelayanan_disc = $('#tRuangRanapJasaPelayananDiskonRupiah').val();
			var bhp = $('#tRuangRanapBHP').val();
			var bhp_disc = $('#tRuangRanapBHPDiskonRupiah').val();
			var biayaperawatan = $('#tRuangRanapBiayaPerawatan').val();
			var biayaperawatan_disc = $('#tRuangRanapBiayaPerawatanDiskonRupiah').val();
			var subtotal = $('#tRuangRanapSubTotal').val();
			var kuantitas = $('#tRuangRanapKuantitas').val();
			var totalkeseluruhan = $('#tRuangRanapGrandTotal').val();
			var diskon = $('#tRuangRanapDiskonRupiah').val();

			// if(subtotal == '' || subtotal <= 0){
			// 	sweetAlert("Maaf...", "Sub Total tidak boleh kosong !", "error").then((value) => {
			// 		$('#tRuangRanapTarif').focus();
			// 	});
			// 	return false;
			// }

			if(kuantitas == '' || kuantitas <= 0){
				sweetAlert("Maaf...", "Kuantitas tidak boleh kosong !", "error").then((value) => {
					$('#tRuangRanapKuantitas').focus();
				});
				return false;
			}

			// if(totalkeseluruhan == '' || totalkeseluruhan <= 0){
			// 	sweetAlert("Maaf...", "Total tidak boleh kosong !", "error").then((value) => {
			// 		$('#tRuangRanapKuantitas').focus();
			// 	});
			// 	return false;
			// }

			$.ajax({
				url: '{site_url}trawatinap_tindakan/updateDataRuanganRanap',
				method: 'POST',
				data: {
					table: table,
					idrow: idrow,
					idruangan: idruangan,
					idkelas: idkelas,
					tanggal_dari: tanggal_dari,
					tanggal_sampai: tanggal_sampai,
					idtarif: idtarif,
					jasasarana: jasasarana,
					jasasarana_disc: jasasarana_disc,
					jasapelayanan: jasapelayanan,
					jasapelayanan_disc: jasapelayanan_disc,
					bhp: bhp,
					bhp_disc: bhp_disc,
					biayaperawatan: biayaperawatan,
					biayaperawatan_disc: biayaperawatan_disc,
					subtotal: subtotal,
					kuantitas: kuantitas,
					diskon: diskon,
					totalkeseluruhan: totalkeseluruhan
				},
				success: function(data) {
					swal({
						title: "Berhasil!",
						text: "Data Tindakan Telah Tersimpan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					location.reload();
				}
			});
		});

		$(document).on('click', '.editDataRuangRanap', function() {
		  var idrow = $(this).data('idrow');
		  var table = $(this).data('table');
			var nopendaftaran = $(this).closest('tr').find("td:eq(0)").html();
			var idtarif = $(this).closest('tr').find("td:eq(2)").html();
			var namatarif = $(this).closest('tr').find("td:eq(3)").html();
			var idruangan = $(this).closest('tr').find("td:eq(4)").html();
			var idkelas = $(this).closest('tr').find("td:eq(5)").html();
			var tanggal_dari = $(this).closest('tr').find("td:eq(6)").html();
			var tanggal_sampai = $(this).closest('tr').find("td:eq(7)").html();
			var jasasarana = $(this).closest('tr').find("td:eq(8)").html();
			var jasasarana_disc = $(this).closest('tr').find("td:eq(9)").html();
			var jasapelayanan = $(this).closest('tr').find("td:eq(10)").html();
			var jasapelayanan_disc = $(this).closest('tr').find("td:eq(11)").html();
			var bhp = $(this).closest('tr').find("td:eq(12)").html();
			var bhp_disc = $(this).closest('tr').find("td:eq(13)").html();
			var biayaperawatan = $(this).closest('tr').find("td:eq(14)").html();
			var biayaperawatan_disc = $(this).closest('tr').find("td:eq(15)").html();
			var subtotal = $(this).closest('tr').find("td:eq(16)").html();
			var kuantitas = $(this).closest('tr').find("td:eq(17)").html();
			var total = $(this).closest('tr').find("td:eq(18)").html();
			var total_disc_rp = $(this).closest('tr').find("td:eq(19)").html();
			var total_disc_percent = $(this).closest('tr').find("td:eq(20)").html();
			var grandtotal = $(this).closest('tr').find("td:eq(21)").html();

			$("#tRuangRanapRowId").val(idrow);
			$("#tRuangRanapTable").val(table);

			$("#tRuangRanapNoRegistrasi").val(nopendaftaran);
			$("#tRuangRanapTanggalDari").val(tanggal_dari);
			$("#tRuangRanapTanggalSampai").val(tanggal_sampai);

			$('#tRuangRanapIdTarif').val(idtarif);
			$('#tRuangRanapNamaTarif').val(namatarif);

			$('#tRuangRanapIdRuangan').val(idruangan);
			$('#tRuangRanapIdKelas').val(idkelas);

			$("#tRuangRanapJasaSarana").val(jasasarana);
			$("#tRuangRanapJasaSaranaDiskonRupiah").val(jasasarana_disc);

			$("#tRuangRanapJasaPelayanan").val(jasapelayanan);
			$("#tRuangRanapJasaPelayananDiskonRupiah").val(jasapelayanan_disc);

			$("#tRuangRanapBHP").val(bhp);
			$("#tRuangRanapBHPDiskonRupiah").val(bhp_disc);

			$("#tRuangRanapBiayaPerawatan").val(biayaperawatan);
			$("#tRuangRanapBiayaPerawatanDiskonRupiah").val(biayaperawatan_disc);

			$("#tRuangRanapSubTotal").val(subtotal);
			$("#tRuangRanapKuantitas").val(kuantitas);
			$("#tRuangRanapTotal").val(total);

			$("#tRuangRanapDiskonRupiah").val(total_disc_rp);
			$("#tRuangRanapDiskonPersen").val(total_disc_percent);
			$("#tRuangRanapGrandTotal").val(grandtotal);

			calculateDiscountRuangRanap();
		});

		$(document).on('click', '#cancelDataRuangRanap', function() {
			$("#tRuangRanapRowId").val('');
			$("#tRuangRanapTable").val('');

			$("#tRuangRanapNoRegistrasi").val('');
			$("#tRuangRanapTanggalDari").val('');
			$("#tRuangRanapTanggalSampai").val('');

			$('#tRuangRanapIdTarif').val('');
			$('#tRuangRanapNamaTarif').val('');

			$('#tRuangRanapIdRuangan').val('');
			$('#tRuangRanapIdKelas').val('');

			$("#tRuangRanapJasaSarana").val('');
			$("#tRuangRanapJasaSaranaDiskonPersen").val('');

			$("#tRuangRanapJasaPelayanan").val('');
			$("#tRuangRanapJasaPelayananDiskonPersen").val('');

			$("#tRuangRanapBHP").val('');
			$("#tRuangRanapBHPDiskonPersen").val('');

			$("#tRuangRanapBiayaPerawatan").val('');
			$("#tRuangRanapBiayaPerawatanDiskonPersen").val('');

			$("#tRuangRanapSubTotal").val('');
			$("#tRuangRanapKuantitas").val('');
			$("#tRuangRanapTotal").val('');

			$("#tRuangRanapDiskonRupiah").val('');
			$("#tRuangRanapDiskonPersen").val('');
			$("#tRuangRanapGrandTotal").val('');
		});

		$(document).on("keyup", ".tdRuangRanapHarga", function () {
    	if ($(this).val()==''){
    		$(this).val(0);
    	}

    	var beforePrice = $(this).val();
    	var discount = parseFloat(parseFloat($(this).closest('tr').find("td:eq(3) input").val()) * 100) / parseFloat(beforePrice);
    	$(this).closest('tr').find("td:eq(2) input").val(discount);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - parseFloat($(this).closest('tr').find("td:eq(3) input").val()));

    	calculateTotalRuangRanap();
    	return false;
    });

    $(document).on("keyup", ".tdRuangRanapDiskonPersen", function () {
    	if ($(this).val()==''){
    		$(this).val(0);
    	}
    	if (parseFloat($(this).val()) > 100){
    		$(this).val(100);
    	}

    	var afterPrice = 0;
    	var beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
    	afterPrice = (parseFloat(beforePrice) * (parseFloat($(this).val()/100)));

    	$(this).closest('tr').find("td:eq(3) input").val(afterPrice);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - afterPrice);

    	calculateTotalRuangRanap();
    	return false;
    });

    $(document).on("keyup", ".tdRuangRanapDiskonRupiah", function () {//Diskon RP Keyup
    	var beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
    	if ($(this).val()==''){
    		$(this).val(0);
    	}
    	if (parseFloat($(this).val()) > beforePrice){
    		$(this).val(beforePrice);
    	}

    	var afterPrice = 0;
    	afterPrice = parseFloat($(this).val()*100) / parseFloat(beforePrice);

    	$(this).closest('tr').find("td:eq(2) input").val(afterPrice);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - $(this).val());

    	calculateTotalRuangRanap();
    	return false;
    });
});

function calculateTotalRuangRanap() {
	var subTotal, totalTransaction, totalDiscountRupiah;

	subTotal = (parseFloat($("#tRuangRanapJasaSaranaTotal").val()) + parseFloat($("#tRuangRanapJasaPelayananTotal").val()) + parseFloat($("#tRuangRanapBHPTotal").val()) + parseFloat($("#tRuangRanapBiayaPerawatanTotal").val()));
	$("#tRuangRanapSubTotal").val(subTotal);

	total = parseFloat(subTotal) * parseFloat($("#tRuangRanapKuantitas").val());
	$("#tRuangRanapTotal").val(total);

	discountRupiah = parseFloat($("#tRuangRanapDiskonRupiah").val());

	grandTotal = total - discountRupiah;
	$("#tRuangRanapGrandTotal").val(parseFloat(grandTotal));
}

function calculateDiscountRuangRanap(){
	var discount = 0;
	var beforePrice = 0;
	var discountPrice = 0;

	$('#tRuangRanapItem tbody tr').each(function() {
		beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
		discountRupiah = $(this).closest('tr').find("td:eq(3) input").val();
		discountPercent = (discountRupiah / beforePrice) * 100;

		$(this).closest('tr').find("td:eq(2) input").val(discountPercent);
		$(this).closest('tr').find("td:eq(4) input").val(beforePrice - discountRupiah);
	});

	calculateTotalRuangRanap();
}
</script>
