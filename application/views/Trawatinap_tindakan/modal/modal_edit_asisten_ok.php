<div class="modal fade in black-overlay" id="EditTransaksiAsistenOperasiModal" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 70%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">EDIT TRANSAKSI</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<input type="hidden" id="tAsistenOperasiRowId" readonly value="">
							<input type="hidden" id="tAsistenOperasiTable" readonly value="">

							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label" for="tAsistenOperasiNoRegistrasi">No. Registrasi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tAsistenOperasiNoRegistrasi" placeholder="No. Registrasi" readonly value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="tKamarOperasiNamaTarif">Nama Tarif</label>
									<div class="col-md-9">
										<input type="hidden" id="tAsistenOperasiIdTarif">
										<input type="text" class="form-control input-sm" id="tAsistenOperasiNamaTarif" placeholder="Nama Tarif" readonly value="">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label" for="tAsistenOperasiTanggal">Tanggal Transaksi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tAsistenOperasiTanggal" placeholder="Tanggal Transaksi" readonly value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="tAsistenOperasiIdDokter">Nama Dokter</label>
									<div class="col-md-9">
										<input type="hidden" id="tAsistenOperasiIdDokter">
										<input type="text" class="form-control input-sm" id="tAsistenOperasiNamaDokter" placeholder="Nama Tarif" readonly value="">
									</div>
								</div>
							</div>
						</form>
					</div>
					<table id="tAsistenOperasiItem" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 20%;">Nominal Acuan</th>
								<th style="width: 10%;">Persentase (%)</th>
								<th style="width: 20%;">Tarif</th>
								<th style="width: 10%;">Diskon (%)</th>
								<th style="width: 10%;">Diskon (Rp)</th>
								<th style="width: 20%;">Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><input type="text" class="form-control input-sm number tdAsistenOperasiNominalAcuan" readonly id="tAsistenOperasiNominalAcuan" placeholder="Nominal Acuan"></td>
								<td><input type="text" class="form-control input-sm number tdAsistenOperasiPersentase" id="tAsistenOperasiPersentase" placeholder="Persentase (%)"></td>
								<td><input type="text" class="form-control input-sm number" readonly id="tAsistenOperasiTarif" placeholder="Tarif"></td>
								<td><input type="text" class="form-control input-sm discount tdAsistenOperasiDiscountPercent" id="tAsistenOperasiDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input type="text" class="form-control input-sm number tdAsistenOperasiDiscountRupiah" id="tAsistenOperasiDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tAsistenOperasiTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-primary" id="saveDataAsistenOperasi" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Edit</button>
				<button class="btn btn-sm btn-default" id="cancelDataAsistenOperasi" type="button" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$(document).on('click', '.editDataAsistenOperasi', function() {
			var idrow = $(this).data('idrow');
			var table = $(this).data('table');

			var nopendaftaran = $(this).closest('tr').find("td:eq(0)").html();
			var tanggal = $(this).closest('tr').find("td:eq(1)").html();

			var idtarif = $(this).closest('tr').find("td:eq(2)").html();
			var namatarif = $(this).closest('tr').find("td:eq(3)").html();

			var iddokter = $(this).closest('tr').find("td:eq(4)").html();
			var namadokter = $(this).closest('tr').find("td:eq(5)").html();

			var nominal_acuan = $(this).closest('tr').find("td:eq(6)").html();
			var persentase = $(this).closest('tr').find("td:eq(7)").html();
			var tarif = $(this).closest('tr').find("td:eq(8)").html();

			var diskon_rp = $(this).closest('tr').find("td:eq(9)").html();
			var diskon_persen = $(this).closest('tr').find("td:eq(10)").html();

			var total = $(this).closest('tr').find("td:eq(11)").html();

			$("#tAsistenOperasiRowId").val(idrow);
			$("#tAsistenOperasiTable").val(table);

			$("#tAsistenOperasiNoRegistrasi").val(nopendaftaran);
			$("#tAsistenOperasiTanggal").val(tanggal);

			$("#tAsistenOperasiIdTarif").val(idtarif);
			$("#tAsistenOperasiNamaTarif").val(namatarif);

			$("#tAsistenOperasiIdDokter").val(iddokter);
			$("#tAsistenOperasiNamaDokter").val(namadokter);

			$("#tAsistenOperasiNominalAcuan").val(nominal_acuan);
			$("#tAsistenOperasiPersentase").val(persentase);
			$("#tAsistenOperasiTarif").val(tarif);

			$("#tAsistenOperasiDiskonRupiah").val(diskon_rp);
			$("#tAsistenOperasiDiskonPersen").val(diskon_persen);

			$("#tAsistenOperasiTotal").val(total);
		});

		$(document).on("keyup", ".tdAsistenOperasiNominalAcuan, .tdAsistenOperasiPersentase", function () {
    	if ($(this).val() == ''){
    		$(this).val(0);
    	}

    	var nominal_acuan = parseFloat($("#tAsistenOperasiNominalAcuan").val());
    	var persentase = parseFloat($("#tAsistenOperasiPersentase").val());
    	var tarif = nominal_acuan * persentase / 100;
    	var diskonPersen = parseFloat($("#tAsistenOperasiDiskonPersen").val());
    	var diskonRupiah = (tarif * diskonPersen / 100);
    	var total = tarif - diskonRupiah;

    	$("#tAsistenOperasiTarif").val(tarif);
			$("#tAsistenOperasiTotal").val(total);

    	return false;
    });

    $(document).on("keyup", ".tdAsistenOperasiDiscountPercent", function () {
    	if ($(this).val() == ''){
    		$(this).val(0);
    	}

    	if (parseFloat($(this).val()) > 100){
    		$(this).val(100);
    	}

    	var diskonRupiah = 0;
    	var tarif = parseFloat($("#tAsistenOperasiTarif").val());

    	diskonRupiah = tarif * (parseFloat($(this).val()) / 100);

			var total = tarif - diskonRupiah;

    	$("#tAsistenOperasiDiskonRupiah").val(diskonRupiah);
    	$("#tAsistenOperasiTotal").val(total);

    	return false;
    });

    $(document).on("keyup", ".tdAsistenOperasiDiscountRupiah", function () {
    	var tarif = $("#tAsistenOperasiTarif").val();

    	if ($(this).val()==''){
    		$(this).val(0);
    	}

    	if (parseFloat($(this).val()) > hargaJual){
    		$(this).val(hargaJual);
    	}

    	var diskonPersen = 0;
    	diskonPersen = parseFloat($(this).val() * 100) / parseFloat(tarif);

			var total = tarif - $(this).val();

    	$("#tAsistenOperasiDiskonPersen").val(diskonPersen);
    	$("#tAsistenOperasiTotal").val(total);

    	return false;
    });

		$(document).on('click', '#saveDataAsistenOperasi', function() {
			var idrow = $("#tAsistenOperasiRowId").val();
			var table = $("#tAsistenOperasiTable").val();
			var persentase = $('#tAsistenOperasiPersentase').val();
			var tarif = $('#tAsistenOperasiTarif').val();
			var diskon = $('#tAsistenOperasiDiskonRupiah').val();

			$.ajax({
				url: '{site_url}trawatinap_tindakan/updateDataAsistenOperasi',
				method: 'POST',
				data: {
					idrow: idrow,
					table: table,
					persentase: persentase,
					tarif: tarif,
					diskon: diskon
				},
				success: function(data) {
					swal({
						title: "Berhasil!",
						text: "Data Telah Diubah.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					location.reload();
				}
			});
		});
});
</script>
