<div class="modal fade in black-overlay" id="EditTransaksiVisiteModal" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 70%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">EDIT TRANSAKSI</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<input type="hidden" id="tVisiteRowId" readonly value="">
							<input type="hidden" id="tVisiteIdTarif" readonly value="">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label" for="tVisiteNoRegistrasi">No. Registrasi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tVisiteNoRegistrasi" placeholder="No. Registrasi" readonly value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="tVisiteIdRuangan">Ruangan</label>
									<div class="col-md-9">
										<input type="hidden" id="tVisiteIdRuangan">
										<input type="text" class="form-control input-sm" id="tVisiteNamaRuangan" placeholder="Nama Ruangan" readonly value="">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 control-label" for="tVisiteTanggal">Tanggal Transaksi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tVisiteTanggal" placeholder="Tanggal Transaksi" readonly value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="tVisiteIdDokter">Dokter</label>
									<div class="col-md-9">
										<select id="tVisiteIdDokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Dokter">
											<option value="">Pilih Dokter</option>
										</select>
									</div>
								</div>
							</div>
						</form>
					</div>
					<table id="tVisiteItem" class="table table-striped table-bordered" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th style="width: 25%;">Item Biaya</th>
								<th style="width: 25%;">Harga</th>
								<th style="width: 10%;">Diskon (%)</th>
								<th style="width: 20%;">Diskon (Rp)</th>
								<th style="width: 20%;">Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Jasa Sarana</td>
								<td><input type="text" class="form-control input-sm number tdVisiteHarga" id="tVisiteJasaSarana" placeholder="Jasa Sarana"></td>
								<td><input type="text" class="form-control input-sm discount tdVisiteDiskonPersen" id="tVisiteJasaSaranaDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input type="text" class="form-control input-sm number tdVisiteDiskonRupiah" id="tVisiteJasaSaranaDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tVisiteJasaSaranaTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>Jasa Pelayanan</td>
								<td><input type="text" class="form-control input-sm number tdVisiteHarga" id="tVisiteJasaPelayanan" placeholder="Jasa Pelayanan"></td>
								<td><input type="text" class="form-control input-sm discount tdVisiteDiskonPersen" id="tVisiteJasaPelayananDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input type="text" class="form-control input-sm number tdVisiteDiskonRupiah" id="tVisiteJasaPelayananDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tVisiteJasaPelayananTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>BHP</td>
								<td><input type="text" class="form-control input-sm number tdVisiteHarga" id="tVisiteBHP" placeholder="BHP"></td>
								<td><input type="text" class="form-control input-sm discount tdVisiteDiskonPersen" id="tVisiteBHPDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input type="text" class="form-control input-sm number tdVisiteDiskonRupiah" id="tVisiteBHPDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tVisiteBHPTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
							<tr>
								<td>Biaya Perawatan</td>
								<td><input type="text" class="form-control input-sm number tdVisiteHarga" id="tVisiteBiayaPerawatan" placeholder="Biaya Perawatan"></td>
								<td><input type="text" class="form-control input-sm discount tdVisiteDiskonPersen" id="tVisiteBiayaPerawatanDiskonPersen" placeholder="Diskon (%)"></td>
								<td><input type="text" class="form-control input-sm number tdVisiteDiskonRupiah" id="tVisiteBiayaPerawatanDiskonRupiah" placeholder="Diskon (Rp)" value="0"></td>
								<td><input type="text" class="form-control input-sm number" id="tVisiteBiayaPerawatanTotal" placeholder="Total Rp" readonly value="0"></td>
							</tr>
						</tbody>
						<tfoot>
							<tr class="bg-light dker">
								<th colspan="4">
									<label class="pull-right"><b>Sub Total (Rp)</b></label>
								</th>
								<th>
									<input type="text" class="form-control input-sm number" id="tVisiteSubTotal" placeholder="Sub Total (Rp)" readonly value="0"></th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="4">
									<label class="pull-right"><b>Kuantitas</b></label>
								</th>
								<th>
									<input type="text" class="form-control input-sm number" id="tVisiteKuantitas" placeholder="Kuantitas" value="0"></th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="4"><label class="pull-right"><b>Total (Rp)</b></label></th>
								<th>
									<input type="text" class="form-control input-sm number" id="tVisiteTotal" placeholder="Total (Rp)" readonly  value="0">
								</th>
							</tr>
							<tr>
								<th style="width: 85%;" colspan="4">
									<label class="pull-right">
										<b>Discount Semua</b>
									</label>
								</th>
								<th style="width: 15%;">
										<div class="input-group">
											<span class="input-group-addon">Rp</span>
											<input class="form-control number input-sm" type="text" id="tVisiteDiskonRupiah" placeholder="Discount (Rp)" value="0">
										</div>
										<div class="input-group">
											<span class="input-group-addon"> %. </span>
											<input class="form-control discount input-sm" type="text" id="tVisiteDiskonPersen" placeholder="Discount (%)" value="0">
										</div>
								</th>
							</tr>
							<tr class="bg-light dker">
								<th colspan="4">
									<label class="pull-right">
										<b>Grand Total (Rp)</b>
									</label>
								</th>
								<th>
									<input type="text" class="form-control input-sm number" id="tVisiteGrandTotal" placeholder="Grand Total (Rp)" readonly value="0">
								</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<div class="modal-footer" id="actionDataVisiteDokter">
				<button class="btn btn-sm btn-primary" id="saveDataVisiteDokter" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Edit</button>
				<button class="btn btn-sm btn-default" id="cancelDataVisiteDokter" type="button" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$(document).on('keyup', '#tVisiteKuantitas', function() {
			var hargajual = $('#tVisiteSubTotal').val().replace(/,/g, '');
			var kuantitas = $(this).val().replace(/,/g, '');
			$('#tVisiteTotal').val(parseInt(hargajual) * parseInt(kuantitas));
		});

		$(document).on('click', '#saveDataVisiteDokter', function() {
			var idpendaftaran = $("#tempIdRawatInap").val();
			var idrow = $('#tVisiteRowId').val();
			var tanggal = $('#tVisiteTanggal').val();
			var idpelayanan = $('#tVisiteIdTarif').val();
			var iddokter = $('#tVisiteIdDokter option:selected').val();
			var idruangan = $('#tVisiteIdRuangan').val();
			var jasasarana = $('#tVisiteJasaSarana').val();
			var jasasarana_disc = $('#tVisiteJasaSaranaDiskonRupiah').val();
			var jasapelayanan = $('#tVisiteJasaPelayanan').val();
			var jasapelayanan_disc = $('#tVisiteJasaPelayananDiskonRupiah').val();
			var bhp = $('#tVisiteBHP').val();
			var bhp_disc = $('#tVisiteBHPDiskonRupiah').val();
			var biayaperawatan = $('#tVisiteBiayaPerawatan').val();
			var biayaperawatan_disc = $('#tVisiteBiayaPerawatanDiskonRupiah').val();
			var subtotal = $('#tVisiteSubTotal').val();
			var kuantitas = $('#tVisiteKuantitas').val();
			var totalkeseluruhan = $('#tVisiteTotal').val();
			var diskon = $('#tVisiteDiskonRupiah').val();

			if(tanggal == ''){
				sweetAlert("Maaf...", "Tanggal tidak boleh kosong !", "error").then((value) => {
					$('#tVisiteTanggal').focus();
				});
				return false;
			}

			if(idpelayanan == ''){
				sweetAlert("Maaf...", "Tindakan belum dipilih !", "error").then((value) => {
					$("#tVisiteIdTarif").select2('open');
				});
				return false;
			}

			if(subtotal == '' || subtotal <= 0){
				sweetAlert("Maaf...", "Sub Total tidak boleh kosong !", "error").then((value) => {
					$('#tVisiteTarif').focus();
				});
				return false;
			}

			if(kuantitas == '' || kuantitas <= 0){
				sweetAlert("Maaf...", "Kuantitas tidak boleh kosong !", "error").then((value) => {
					$('#tVisiteKuantitas').focus();
				});
				return false;
			}

			if(totalkeseluruhan == '' || totalkeseluruhan <= 0){
				sweetAlert("Maaf...", "Total tidak boleh kosong !", "error").then((value) => {
					$('#tVisiteKuantitas').focus();
				});
				return false;
			}
			
			$.ajax({
				url: '{site_url}trawatinap_tindakan/saveDataVisiteDokter',
				method: 'POST',
				data: {
					idpendaftaran: idpendaftaran,
					idrow: idrow,
					tanggal: tanggal,
					idpelayanan: idpelayanan,
					iddokter: iddokter,
					idruangan: idruangan,
					idpelayanan: idpelayanan,
					jasasarana: jasasarana,
					jasasarana_disc: jasasarana_disc,
					jasapelayanan: jasapelayanan,
					jasapelayanan_disc: jasapelayanan_disc,
					bhp: bhp,
					bhp_disc: bhp_disc,
					biayaperawatan: biayaperawatan,
					biayaperawatan_disc: biayaperawatan_disc,
					total: subtotal,
					kuantitas: kuantitas,
					diskon: diskon,
					totalkeseluruhan: (totalkeseluruhan - diskon)
				},
				success: function(data) {
					swal({
						title: "Berhasil!",
						text: "Data Tindakan Telah Tersimpan.",
						type: "success",
						timer: 1500,
						showConfirmButton: false
					});

					location.reload();
				}
			});
		});

		$(document).on('click', '.editDataVisiteDokter', function() {
			var idrow = $(this).data('idrow');
			var nopendaftaran = $(this).closest('tr').find("td:eq(0)").html();
			var tanggal = $(this).closest('tr').find("td:eq(1)").html();
			var idpelayanan = $(this).closest('tr').find("td:eq(2)").html();
			var iddokter = $(this).closest('tr').find("td:eq(3)").html();
			var namadokter = $(this).closest('tr').find("td:eq(4)").html();
			var idruangan = $(this).closest('tr').find("td:eq(5)").html();
			var namaruangan = $(this).closest('tr').find("td:eq(6)").html();
			var jasasarana = $(this).closest('tr').find("td:eq(7)").html();
			var jasasarana_disc = $(this).closest('tr').find("td:eq(8)").html();
			var jasapelayanan = $(this).closest('tr').find("td:eq(9)").html();
			var jasapelayanan_disc = $(this).closest('tr').find("td:eq(10)").html();
			var bhp = $(this).closest('tr').find("td:eq(11)").html();
			var bhp_disc = $(this).closest('tr').find("td:eq(12)").html();
			var biayaperawatan = $(this).closest('tr').find("td:eq(13)").html();
			var biayaperawatan_disc = $(this).closest('tr').find("td:eq(14)").html();
			var subtotal = $(this).closest('tr').find("td:eq(15)").html();
			var kuantitas = $(this).closest('tr').find("td:eq(16)").html();
			var total = $(this).closest('tr').find("td:eq(17)").html();
			var total_disc_rp = $(this).closest('tr').find("td:eq(18)").html();
			var total_disc_percent = $(this).closest('tr').find("td:eq(19)").html();
			var grandtotal = $(this).closest('tr').find("td:eq(20)").html();

			$("#tVisiteRowId").val(idrow);
			$("#tVisiteNoRegistrasi").val(nopendaftaran);
			$("#tVisiteTanggal").val(tanggal);

			$('#tVisiteIdTarif').val(idpelayanan);
			$('#tVisiteIdRuangan').val(idruangan);
			$('#tVisiteNamaRuangan').val(namaruangan);

			$('#tVisiteIdDokter').select2("trigger", "select", {data : {id: iddokter,text: namadokter}});

			$("#tVisiteJasaSarana").val(jasasarana);
			$("#tVisiteJasaSaranaDiskonRupiah").val(jasasarana_disc);

			$("#tVisiteJasaPelayanan").val(jasapelayanan);
			$("#tVisiteJasaPelayananDiskonRupiah").val(jasapelayanan_disc);

			$("#tVisiteBHP").val(bhp);
			$("#tVisiteBHPDiskonRupiah").val(bhp_disc);

			$("#tVisiteBiayaPerawatan").val(biayaperawatan);
			$("#tVisiteBiayaPerawatanDiskonRupiah").val(biayaperawatan_disc);

			$("#tVisiteSubTotal").val(subtotal);
			$("#tVisiteKuantitas").val(kuantitas);
			$("#tVisiteTotal").val(total);

			$("#tVisiteDiskonRupiah").val(total_disc_rp);
			$("#tVisiteDiskonPersen").val(total_disc_percent);
			$("#tVisiteGrandTotal").val(grandtotal);

			calculateDiscountVisite();
		});

		$(document).on('click', '#cancelDataVisiteDokter', function() {
			$("#tVisiteRowId").val('');
			$("#tVisiteNoRegistrasi").val('');
			$("#tVisiteTanggal").val('');

			$('#tVisiteIdTarif').val('');
			$('#tVisiteIdRuangan').val('');

			$('#tVisiteIdDokter').select2("trigger", "select", {data : {id: '',text: ''}});

			$("#tVisiteJasaSarana").val('');
			$("#tVisiteJasaSaranaDiskonPersen").val('');

			$("#tVisiteJasaPelayanan").val('');
			$("#tVisiteJasaPelayananDiskonPersen").val('');

			$("#tVisiteBHP").val('');
			$("#tVisiteBHPDiskonPersen").val('');

			$("#tVisiteBiayaPerawatan").val('');
			$("#tVisiteBiayaPerawatanDiskonPersen").val('');

			$("#tVisiteSubTotal").val('');
			$("#tVisiteKuantitas").val('');
			$("#tVisiteTotal").val('');
			$("#tVisiteDiskonPersen").val('');
			$("#tVisiteGrandTotal").val('');
		});

		$(document).on('click', '.removeDataVisiteDokter', function() {
		  var idrow = $(this).data('idrow');
		  var idrawatinap = $('#tempIdRawatInap').val();

		  event.preventDefault();
		  swal({
		      title: "Apakah anda yakin ingin menghapus data ini?",
		      type: "warning",
		      showCancelButton: true,
		      confirmButtonColor: "#DD6B55",
		      confirmButtonText: "Ya",
		      cancelButtonText: "Tidak",
		      closeOnConfirm: false,
		      closeOnCancel: false
		    }).then(function() {
		      $.ajax({
		        url: '{site_url}trawatinap_tindakan/removeDataVisiteDokter',
		        method: 'POST',
		        data: {
		          idrow: idrow
		        },
		        success: function(data) {
		          swal({
		            title: "Berhasil!",
		            text: "Data Deposit Telah Dihapus.",
		            type: "success",
		            timer: 1500,
		            showConfirmButton: false
		          });

		          location.reload();
		        }
		      });
		    });
		});

		$("#tVisiteDiskonPersen").keyup(function(){
			if ($("#tVisiteDiskonPersen").val() == ''){
				$("#tVisiteDiskonPersen").val(0)
			}
			if (parseFloat($(this).val()) > 100){
				$(this).val(100);
			}

			var discount_rupiah = parseFloat($("#tVisiteTotal").val() * parseFloat($(this).val())/100);
			$("#tVisiteDiskonRupiah").val(discount_rupiah);

			calculateTotalVisite();
		});

		$("#tVisiteDiskonRupiah").keyup(function(){
			if ($("#tVisiteDiskonRupiah").val()==''){
				$("#tVisiteDiskonRupiah").val(0)
			}
			if (parseFloat($(this).val()) > $("#tVisiteTotal").val()){
				$(this).val($("#tVisiteTotal").val());
			}

			var discount_percent = parseFloat((parseFloat($(this).val()*100))/$("#tVisiteTotal").val());
			$("#tVisiteDiskonPersen").val(discount_percent);

			calculateTotalVisite();
		});

		$("#tVisiteKuantitas").keyup(function(){
			calculateTotalVisite();
		});

		$(document).on("keyup", ".tdVisiteHarga", function () {
    	if ($(this).val()==''){
    		$(this).val(0);
    	}

    	var beforePrice = $(this).val();
    	var discount = parseFloat(parseFloat($(this).closest('tr').find("td:eq(3) input").val()) * 100) / parseFloat(beforePrice);
    	$(this).closest('tr').find("td:eq(2) input").val(discount);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - parseFloat($(this).closest('tr').find("td:eq(3) input").val()));

    	calculateTotalVisite();
    	return false;
    });

    $(document).on("keyup", ".tdVisiteDiskonPersen", function () {
    	if ($(this).val()==''){
    		$(this).val(0);
    	}
    	if (parseFloat($(this).val()) > 100){
    		$(this).val(100);
    	}

    	var afterPrice = 0;
    	var beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
    	afterPrice = (parseFloat(beforePrice) * (parseFloat($(this).val()/100)));

    	$(this).closest('tr').find("td:eq(3) input").val(afterPrice);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - afterPrice);

    	calculateTotalVisite();
    	return false;
    });

    $(document).on("keyup", ".tdVisiteDiskonRupiah", function () {//Diskon RP Keyup
    	var beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
    	if ($(this).val()==''){
    		$(this).val(0);
    	}
    	if (parseFloat($(this).val()) > beforePrice){
    		$(this).val(beforePrice);
    	}

    	var afterPrice = 0;
    	afterPrice = parseFloat($(this).val()*100) / parseFloat(beforePrice);

    	$(this).closest('tr').find("td:eq(2) input").val(afterPrice);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - $(this).val());

    	calculateTotalVisite();
    	return false;
    });

		$(document).on("keyup", ".tdVisiteHargaRanap", function () {
    	if ($(this).val()==''){
    		$(this).val(0);
    	}

    	var beforePrice = $(this).val();
    	var discount = parseFloat(parseFloat($(this).closest('tr').find("td:eq(3) input").val()) * 100) / parseFloat(beforePrice);
    	$(this).closest('tr').find("td:eq(2) input").val(discount);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - parseFloat($(this).closest('tr').find("td:eq(3) input").val()));

    	calculateTotalVisite();
    	return false;
    });

    $(document).on("keyup", ".tdVisiteDiskonPersen", function () {
    	if ($(this).val()==''){
    		$(this).val(0);
    	}
    	if (parseFloat($(this).val()) > 100){
    		$(this).val(100);
    	}

    	var afterPrice = 0;
    	var beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
    	afterPrice = (parseFloat(beforePrice) * (parseFloat($(this).val()/100)));

    	$(this).closest('tr').find("td:eq(3) input").val(afterPrice);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - afterPrice);

    	calculateTotalVisite();
    	return false;
    });

    $(document).on("keyup", ".tdVisiteDiskonRupiah", function () {//Diskon RP Keyup
    	var beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
    	if ($(this).val()==''){
    		$(this).val(0);
    	}
    	if (parseFloat($(this).val()) > beforePrice){
    		$(this).val(beforePrice);
    	}

    	var afterPrice = 0;
    	afterPrice = parseFloat($(this).val()*100) / parseFloat(beforePrice);

    	$(this).closest('tr').find("td:eq(2) input").val(afterPrice);
    	$(this).closest('tr').find("td:eq(4) input").val(beforePrice - $(this).val());

    	calculateTotalVisite();
    	return false;
    });
});

function calculateTotalVisite() {
	var subTotal, totalTransaction, totalDiscountRupiah;

	subTotal = (parseFloat($("#tVisiteJasaSaranaTotal").val()) + parseFloat($("#tVisiteJasaPelayananTotal").val()) + parseFloat($("#tVisiteBHPTotal").val()) + parseFloat($("#tVisiteBiayaPerawatanTotal").val()));
	$("#tVisiteSubTotal").val(subTotal);

	total = parseFloat(subTotal) * parseFloat($("#tVisiteKuantitas").val());
	$("#tVisiteTotal").val(total);

	discountRupiah = parseFloat($("#tVisiteDiskonRupiah").val());

	grandTotal = total - discountRupiah;
	$("#tVisiteGrandTotal").val(parseFloat(grandTotal));
}

function calculateDiscountVisite(){
	var discount = 0;
	var beforePrice = 0;
	var discountPrice = 0;

	$('#tVisiteItem tbody tr').each(function() {
		beforePrice = $(this).closest('tr').find("td:eq(1) input").val();
		discountRupiah = $(this).closest('tr').find("td:eq(3) input").val();
		discountPercent = (discountRupiah / beforePrice) * 100;

		$(this).closest('tr').find("td:eq(2) input").val(discountPercent);
		$(this).closest('tr').find("td:eq(4) input").val(beforePrice - discountRupiah);
	});

	calculateTotalVisite();
}
</script>
