<div class="block">
	<div class="block-content">
		<h5>CATATAN</h5>
		<br>
		<textarea class="form-control js-summernote" placeholder="Catatan" id="catatan" <?=($statuspembayaran ? 'disabled' : '')?> rows="3"><?=($catatan)?></textarea>
		<button type="button" class="btn btn-primary" id="saveNote">Simpan Catatan</button>
		<br><br>
	</div>
</div>