<div class="row">
    <div class="col-md-6">
        <div class="input-group" style="width:100%">
            <span class="input-group-addon" style="width:150px; text-align:left;">No. Medrec</span>
            <input type="text" class="form-control tindakanNoMedrec" value="{nomedrec}" readonly="true">
        </div>
        <div class="input-group" style="margin-top:5px; width:100%">
            <span class="input-group-addon" style="width:150px; text-align:left;">Nama Pasien</span>
            <input type="text" class="form-control tindakanNamaPasien" value="{namapasien}" readonly="true">
        </div>
        <div class="input-group" style="margin-top:5px; width:100%">
            <span class="input-group-addon" style="width:150px; text-align:left;">Alamat Pasien</span>
            <input type="text" class="form-control tindakanAlamatPasien" value="{alamatpasien}" readonly="true">
        </div>
        <div class="input-group" style="margin-top:5px; width:100%">
            <span class="input-group-addon" style="width:150px; text-align:left;">No. Handphone</span>
            <input type="text" class="form-control tindakanNoHandphone" value="{nohp}" readonly="true">
        </div>
        <hr>
        <div class="input-group" style="margin-top:5px; width:100%">
            <span class="input-group-addon" style="width:150px; text-align:left;">Penanggung Jawab</span>
            <input type="text" class="form-control tindakanPenanggungJawab" value="{namapenanggungjawab}"
                readonly="true">
        </div>
        <div class="input-group" style="margin-top:5px; width:100%">
            <span class="input-group-addon" style="width:150px; text-align:left;">No. Telephone</span>
            <input type="text" class="form-control tindakanTelpPenanggungJawab" value="{telppenanggungjawab}"
                readonly="true">
        </div>
    </div>
    <div class="col-md-6">
        <div class="input-group" style="width:100%">
            <span class="input-group-addon" style="width:150px; text-align:left;">Kelompok Pasien</span>
            <input type="text" class="form-control tindakanKelompokPasien" value="{namakelompok}" readonly="true">
        </div>
        <div class="input-group" style="margin-top:5px; width:100%">
            <span class="input-group-addon" style="width:150px; text-align:left;">Nama Perusahaan</span>
            <input type="text" class="form-control tindakanNamaPerusahaan" value="{namaperusahaan}" readonly="true">
        </div>
        <hr>
        <?php if ($page == 'rawatinap') { ?>
        <div class="input-group" style="margin-top:5px; width:100%">
            <span class="input-group-addon" style="width:150px; text-align:left;">Kelas Perawatan</span>
            <input type="text" class="form-control tindakanNamaKelas" value="{namakelas}" readonly="true">
        </div>
        <div class="input-group" style="margin-top:5px; width:100%">
            <span class="input-group-addon" style="width:150px; text-align:left;">Bed</span>
            <input type="text" class="form-control tindakanNamaBed" value="{namabed}" readonly="true">
        </div>
        <hr>
        <?php } ?>
        <div class="input-group" style="margin-top:5px; width:100%">
            <span class="input-group-addon" style="width:150px; text-align:left;">Dokter</span>
            <input type="text" class="form-control tindakanNamaDokter" value="{namadokterpenanggungjawab}"
                readonly="true">
        </div>
    </div>
</div>