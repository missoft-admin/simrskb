<b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
<b><span class="label label-default" style="font-size:12px">X-RAY</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Rujukan</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Dokter</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalRadXray = 0;?>
        <?php $totalRadXrayVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 1) as $row) { ?>
        <tr>
            <td><?=$row->norujukan?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td hidden><?=$row->idradiologi?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td hidden><?=$row->iddokter?></td>
            <td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
            <td>
                <?php if ($row->statusverifikasi == 0) { ?>
                <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal"
                    data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                <button data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } else { ?>
                <button data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalRadXray = $totalRadXray + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalRadXrayVerif = $totalRadXrayVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalRadXray)?></td>
        </tr>
    </tfoot>
</table>

<hr>

<b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
<b><span class="label label-default" style="font-size:12px">USG</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Rujukan</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Dokter</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalRadUSG = 0;?>
        <?php $totalRadUSGVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 2) as $row) { ?>
        <tr>
            <td><?=$row->norujukan?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td hidden><?=$row->idradiologi?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td hidden><?=$row->iddokter?></td>
            <td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
            <td>
                <?php if ($row->statusverifikasi == 0) { ?>
                <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal"
                    data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                <button data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } else { ?>
                <button data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalRadUSG = $totalRadUSG + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalRadUSGVerif = $totalRadUSGVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalRadUSG)?></td>
        </tr>
    </tfoot>
</table>

<hr>

<b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
<b><span class="label label-default" style="font-size:12px">CT SCAN</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Rujukan</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Dokter</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalRadCTScan = 0;?>
        <?php $totalRadCTScanVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 3) as $row) { ?>
        <tr>
            <td><?=$row->norujukan?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td hidden><?=$row->idradiologi?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td hidden><?=$row->iddokter?></td>
            <td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
            <td>
                <?php if ($row->statusverifikasi == 0) { ?>
                <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal"
                    data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                <button data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } else { ?>
                <button data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalRadCTScan = $totalRadCTScan + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalRadCTScanVerif = $totalRadCTScanVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalRadCTScan)?></td>
        </tr>
    </tfoot>
</table>

<hr>

<b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
<b><span class="label label-default" style="font-size:12px">MRI</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Rujukan</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Dokter</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalRadMRI = 0;?>
        <?php $totalRadMRIVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 4) as $row) { ?>
        <tr>
            <td><?=$row->norujukan?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td hidden><?=$row->idradiologi?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td hidden><?=$row->iddokter?></td>
            <td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
            <td>
                <?php if ($row->statusverifikasi == 0) { ?>
                <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal"
                    data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                <button data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } else { ?>
                <button data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalRadMRI = $totalRadMRI + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalRadMRIVerif = $totalRadMRIVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalRadMRI)?></td>
        </tr>
    </tfoot>
</table>

<hr>

<b><span class="label label-success" style="font-size:12px">TINDAKAN RADIOLOGI</span></b>
<b><span class="label label-default" style="font-size:12px">BMD</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Rujukan</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Dokter</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalRadBMD = 0;?>
        <?php $totalRadBMDVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRadiologi($idpendaftaran, 5) as $row) { ?>
        <tr>
            <td><?=$row->norujukan?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td hidden><?=$row->idradiologi?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td hidden><?=$row->iddokter?></td>
            <td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
            <td>
                <?php if ($row->statusverifikasi == 0) { ?>
                <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal"
                    data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                <button data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } else { ?>
                <button data-idrow="<?=$row->iddetail?>" data-table="trujukan_radiologi_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalRadBMD = $totalRadBMD + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalRadBMDVerif = $totalRadBMDVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalRadBMD)?></td>
        </tr>
    </tfoot>
</table>