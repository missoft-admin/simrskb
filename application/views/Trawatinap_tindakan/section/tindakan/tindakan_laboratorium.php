<b><span class="label label-success" style="font-size:12px">TINDAKAN LABORATORIUM</span></b>
<b><span class="label label-default" style="font-size:12px">UMUM</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Rujukan</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Dokter</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalLab = 0;?>
        <?php $totalLabVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 1) as $row) { ?>
        <tr>
            <td><?=$row->norujukan?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td hidden><?=$row->idlaboratorium?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td hidden><?=$row->iddokter?></td>
            <td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
            <td>
                <?php if ($row->statusverifikasi == 0) { ?>
                <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal"
                    data-idrow="<?=$row->iddetail?>" data-table="trujukan_laboratorium_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                <button data-idrow="<?=$row->iddetail?>" data-table="trujukan_laboratorium_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } else { ?>
                <button data-idrow="<?=$row->iddetail?>" data-table="trujukan_laboratorium_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalLab = $totalLab + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalLabVerif = $totalLabVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalLab)?></td>
        </tr>
    </tfoot>
</table>

<hr>

<b><span class="label label-success" style="font-size:12px">TINDAKAN LABORATORIUM</span></b>
<b><span class="label label-default" style="font-size:12px">PATHOLOGI ANATOMI</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Rujukan</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Dokter</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalLabPA = 0;?>
        <?php $totalLabPAVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 2) as $row) { ?>
        <tr>
            <td><?=$row->norujukan?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td hidden><?=$row->idlaboratorium?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td hidden><?=$row->iddokter?></td>
            <td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
            <td>
                <?php if ($row->statusverifikasi == 0) { ?>
                <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal"
                    data-idrow="<?=$row->iddetail?>" data-table="trujukan_laboratorium_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                <button data-idrow="<?=$row->iddetail?>" data-table="trujukan_laboratorium_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } else { ?>
                <button data-idrow="<?=$row->iddetail?>" data-table="trujukan_laboratorium_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalLabPA = $totalLabPA + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalLabPAVerif = $totalLabPAVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalLabPA)?></td>
        </tr>
    </tfoot>
</table>

<hr>

<b><span class="label label-success" style="font-size:12px">TINDAKAN LABORATORIUM</span></b>
<b><span class="label label-default" style="font-size:12px">PMI</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Rujukan</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Dokter</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalLabPMI = 0;?>
        <?php $totalLabPMIVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianLaboratorium($idpendaftaran, 3) as $row) { ?>
        <tr>
            <td><?=$row->norujukan?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td hidden><?=$row->idlaboratorium?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td hidden><?=$row->iddokter?></td>
            <td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
            <td>
                <?php if ($row->statusverifikasi == 0) { ?>
                <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal"
                    data-idrow="<?=$row->iddetail?>" data-table="trujukan_laboratorium_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>
                <button data-idrow="<?=$row->iddetail?>" data-table="trujukan_laboratorium_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } else { ?>
                <button data-idrow="<?=$row->iddetail?>" data-table="trujukan_laboratorium_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalLabPMI = $totalLabPMI + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalLabPMIVerif = $totalLabPMIVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalLabPMI)?></td>
        </tr>
    </tfoot>
</table>