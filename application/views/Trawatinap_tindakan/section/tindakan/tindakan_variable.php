<?php
// Total Keseluruhan Tindakan Transaksi
$totalSebelumAdm = $totalRanapFullCare + $totalRanapECG + $totalRanapVisite + $totalRanapSewaAlat +
$totalRanapAmbulance + $totalRanapAlkes + $totalRanapAlkesBantu + $totalRanapLainLain + $totalPoli +
$totalPoliObat + $totalFarmasiObatIGD + $totalFarmasiRajalReturObat + $totalPoliAlkes + $totalFarmasiAlkesIGD + $totalFarmasiRajalReturAlkes + $totalRanapObat + $totalFarmasiObat + $totalFarmasiReturObat + $totalFarmasiAlkes +
$totalFarmasiReturAlkes + $totalFarmasiAlkesBantu + $totalFarmasiReturAlkesBantu + $totalLab + $totalLabPA + $totalLabPMI +
$totalRadXray + $totalRadUSG + $totalRadCTScan + $totalRadMRI + $totalRadBMD +
$totalFisio + $totalOKSewaAlat + $totalOKAlkes + $totalOKObat + $totalOKObatNarcose +
$totalOKImplan + $totalOKSewaKamar + $totalOKJasaDokterOperator + $totalOKJasaDokterAnesthesi + $totalOKJasaAsisten +
$totalRanapRuangan + $totalAdmRajal;

// Total Keseluruhan Tindakan Transaksi ( Verified Only )
$totalSebelumAdmVerif = $totalRanapFullCareVerif + $totalRanapECGVerif + $totalRanapVisiteVerif + $totalRanapSewaAlatVerif +
$totalRanapAmbulanceVerif + $totalRanapAlkesVerif + $totalRanapAlkesBantuVerif + $totalRanapLainLainVerif + $totalPoliVerif +
$totalPoliObatVerif + $totalRanapObatVerif + $totalFarmasiObatIGDVerif + $totalFarmasiRajalReturObatVerif + $totalPoliAlkesVerif + $totalFarmasiAlkesIGDVerif + $totalFarmasiRajalReturAlkesVerif + $totalFarmasiObatVerif + $totalFarmasiReturObatVerif + $totalFarmasiAlkesVerif +
$totalFarmasiReturAlkesVerif + $totalFarmasiAlkesBantuVerif + $totalFarmasiReturAlkesBantuVerif + $totalLabVerif + $totalLabPAVerif + $totalLabPMIVerif +
$totalRadXrayVerif + $totalRadUSGVerif + $totalRadCTScanVerif + $totalRadMRIVerif + $totalRadBMDVerif +
$totalFisioVerif + $totalOKSewaAlatVerif + $totalOKAlkesVerif + $totalOKObatVerif + $totalOKObatNarcoseVerif +
$totalOKImplanVerif + $totalOKSewaKamarVerif + $totalOKJasaDokterOperatorVerif + $totalOKJasaDokterAnesthesiVerif + $totalOKJasaAsistenVerif +
$totalRanapRuangan + $totalAdmRajal;
