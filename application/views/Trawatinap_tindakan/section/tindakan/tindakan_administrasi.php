<b><span class="label label-success" style="font-size:12px">ADMINISTRASI</span></b>
<b><span class="label label-default" style="font-size:12px">RAWAT JALAN</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Pendaftaran</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:30%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:10%">Diskon (%)</th>
            <th style="width:20%">Tarif Setelah Diskon</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalAdmRajal = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianAdministrasiRawatJalan($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td hidden><?=$row->idtarif?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td hidden><?=number_format($row->subtotal)?></td>
            <td hidden><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td>
                <?php if ($row->statusverifikasi == 0) { ?>
                <button data-toggle="modal" data-target="#ActionEditAdministrasiModal" data-idrow="<?=$row->iddetail?>" data-table="tpoliklinik_administrasi" <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-primary editDataAdministrasi"><i class="fa fa-pencil"></i></button>
                <button data-idrow="<?=$row->iddetail?>" data-table="tpoliklinik_administrasi" <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifAdministrasi"><i class="fa fa-check"></i> Verifikasi</button>
                <?php } else { ?>
                <button data-idrow="<?=$row->iddetail?>" data-table="tpoliklinik_administrasi" <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifAdministrasi"><i class="fa fa-times"></i> Verifikasi</button>
                <?php } ?>
            </td>
        </tr>
        <?php $totalAdmRajal = $totalAdmRajal + $row->totalkeseluruhan;?>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="5"><b>TOTAL</b></td>
            <td class="text-bold" colspan="2"><?=number_format($totalAdmRajal)?></td>
        </tr>
    </tfoot>
</table>

<?php require __DIR__ . '/tindakan_variable.php'; ?>

<b><span class="label label-success" style="font-size:12px">ADMINISTRASI</span></b>
<b><span class="label label-default" style="font-size:12px">RAWAT INAP</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Pendaftaran</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:30%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:10%">Diskon (%)</th>
            <th style="width:20%">Tarif Setelah Diskon</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody id="tempAdministrasiRanap">
        <?php $totalAdmRanap = 0;?>
        <?php if ($statusvalidasi == 1) { ?>
        <!-- Tarif Administrasi Setelah Proses Validasi -->
        <?php foreach (get_all('trawatinap_administrasi', array('idpendaftaran' => $idpendaftaran)) as $row) { ?>
        <?php $ranap = get_by_field('id', $row->idpendaftaran, 'trawatinap_pendaftaran')?>
        <tr>
            <td><?=$ranap->nopendaftaran?></td>
            <td><?=DMYFormat($ranap->tanggaldaftar)?></td>
            <td hidden><?=$row->idadministrasi?></td>
            <td><?=$row->namatarif?></td>
            <td><?=number_format($row->tarif)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->tarif); ?></td>
            <td><?=number_format($row->tarifsetelahdiskon)?></td>
            <td>
                <?php if ($row->statusverifikasi == 0) { ?>
                <button data-toggle="modal" data-target="#ActionEditAdministrasiRanapModal" data-idrow="<?=$row->id?>" data-table="trawatinap_administrasi" <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-primary editDataAdministrasiRanap"><i class="fa fa-pencil"></i></button>
                <button data-idrow="<?=$row->id?>" data-table="trawatinap_administrasi"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifAdministrasi"><i class="fa fa-check"></i> Verifikasi</button>
                <?php } else { ?>
                <button data-idrow="<?=$row->id?>" data-table="trawatinap_administrasi" <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifAdministrasi"><i class="fa fa-times"></i> Verifikasi</button>
                <?php } ?>
            </td>
        </tr>
        <?php $totalAdmRanap = $totalAdmRanap + $row->tarifsetelahdiskon;?>
        <?php } ?>
        <!-- EOF Tarif Administrasi Setelah Proses Validasi -->
        <?php } else { ?>
        <!-- Tarif Administrasi Sebelum Proses Validasi -->
        <?php $dataAdminRanap = $this->db->query("CALL spAdministrasiRanap($idpendaftaran, $totalSebelumAdm)")->result();?>
        <?php foreach ($dataAdminRanap as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td><?=$row->namatarif?></td>
            <td><?=number_format($row->tarif)?></td>
            <td>0</td>
            <td><?=number_format($row->tarif)?></td>
            <td>
                <button disabled class="btn btn-sm btn-danger"><i class="fa fa-refresh"></i> Menunggu Proses
                    Validasi</button>
            </td>
            <td hidden><?=$idpendaftaran?></td> <!-- td:no::7 -->
            <td hidden><?=$row->idtarif?></td> <!-- td:no::8 -->
            <td hidden><?=$row->namatarif?></td> <!-- td:no::9 -->
            <td hidden><?=$row->jasasarana?></td> <!-- td:no::10 -->
            <td hidden><?=$row->jasapelayanan?></td> <!-- td:no::11 -->
            <td hidden><?=$row->bhp?></td> <!-- td:no::12 -->
            <td hidden><?=$row->biayaperawatan?></td> <!-- td:no::13 -->
            <td hidden><?=number_format($row->total)?></td> <!-- td:no::14 -->
            <td hidden><?=number_format($row->mintarif)?></td> <!-- td:no::15 -->
            <td hidden><?=number_format($row->maxtarif)?></td> <!-- td:no::16 -->
            <td hidden><?=number_format($row->persentasetarif)?></td> <!-- td:no::17 -->
            <td hidden><?=number_format($row->tarif)?></td> <!-- td:no::18 -->
        </tr>
        <?php $totalAdmRanap = $totalAdmRanap + $row->tarif;?>
        <?php } ?>
        <!-- EOF Tarif Administrasi Sebelum Proses Validasi -->
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="5"><b>TOTAL</b></td>
            <td class="text-bold" colspan="2"><?=number_format($totalAdmRanap)?></td>
        </tr>
    </tfoot>
</table>