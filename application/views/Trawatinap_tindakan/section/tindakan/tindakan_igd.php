<b><span class="label label-success" style="font-size:12px">TINDAKAN IGD</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Pendaftaran</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Tindakan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Dokter</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalPoli = 0;?>
        <?php $totalPoliVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRajalTindakanFilteredIGD($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td hidden><?=$row->idpelayanan?></td>
            <td><?=$row->namatarif?></td>
            <td hidden><?=$row->jasasarana?></td>
            <td hidden><?=$row->jasasarana_disc?></td>
            <td hidden><?=$row->jasapelayanan?></td>
            <td hidden><?=$row->jasapelayanan_disc?></td>
            <td hidden><?=$row->bhp?></td>
            <td hidden><?=$row->bhp_disc?></td>
            <td hidden><?=$row->biayaperawatan?></td>
            <td hidden><?=$row->biayaperawatan_disc?></td>
            <td><?=number_format($row->subtotal)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->total)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td hidden><?=$row->iddokter?></td>
            <td><?=($row->namadokter != null ? $row->namadokter : "-")?></td>
            <td>
                <?php if ($row->statusverifikasi == 0) { ?>
                <button data-toggle="modal" data-target="#EditTransaksiTindakanGlobalModal"
                    data-idrow="<?=$row->iddetail?>" data-table="tpoliklinik_pelayanan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanGlobal"><i class="fa fa-pencil"></i></button>

                <!-- Action Verifikasi -->
                <button data-idrow="<?=$row->iddetail?>" data-table="tpoliklinik_pelayanan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } else { ?>
                <button data-idrow="<?=$row->iddetail?>" data-table="tpoliklinik_pelayanan"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalPoli = $totalPoli + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalPoliVerif = $totalPoliVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalPoli)?></td>
        </tr>
    </tfoot>
</table>

<hr>

<b><span class="label label-success" style="font-size:12px">TINDAKAN IGD</span></b>
<b><span class="label label-default" style="font-size:12px">OBAT</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Transaksi</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Obat</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Unit Pelayanan</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalPoliObat = 0;?>
        <?php $totalPoliObatVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRajalObatFilteredIGD($idpendaftaran, 2) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none"><?=$row->idunit?></td>
            <td><?=$row->namaunit?></td>
            <td>
                <?php if ($row->statusverifikasi == 0) { ?>
                <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>" data-table="tpoliklinik_obat"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <button data-idrow="<?=$row->iddetail?>" data-table="tpoliklinik_obat"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } else { ?>
                <button data-idrow="<?=$row->iddetail?>" data-table="tpoliklinik_obat"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalPoliObat = $totalPoliObat + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalPoliObatVerif = $totalPoliObatVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>

        <?php $totalFarmasiObatIGD = 0;?>
        <?php $totalFarmasiObatIGDVerif = 0;?>
        <?php $dataFarmasiObatIGD = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRajal($idpendaftaran, 3);?>
        <?php foreach ($dataFarmasiObatIGD as $row) { ?>
        <?php $subtotal = $row->hargajual * $row->kuantitas ?>
        <tr>
            <td><?=$row->nopenjualan?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none">1</td>
            <td>Farmasi</td>
            <td>
                <?php if ($row->statusverifikasi == 0) { ?>
                <?php if ($row->statusracikan == 0) { ?>
                <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>"
                    data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <? } ?>
                <button data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } else { ?>
                <button data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalFarmasiObatIGD = $totalFarmasiObatIGD + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalFarmasiObatIGDVerif = $totalFarmasiObatIGDVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>

        <?php $totalFarmasiRajalReturObat = 0; ?>
        <?php $totalFarmasiRajalReturObatVerif = 0; ?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRajalObat($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->nopengembalian?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none"><?=$row->idunitpelayanan?></td>
            <td><?=$row->unitpelayanan?></td>
            <td>
                <?php if ($row->statusverifikasi == 0) { ?>
                <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>" data-table="tpasien_pengembalian_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <button data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="tpasien_pengembalian_detail" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-success verifTindakan"><i class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } else { ?>
                <button data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="tpasien_pengembalian_detail" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-danger unverifTindakan"><i class="fa fa-times"></i>
                    Verifikasi</button>
                <?php } ?>
            </td>
        </tr>
        <?php
			$totalFarmasiRajalReturObat = $totalFarmasiRajalReturObat + $row->totalkeseluruhan;
			if ($row->statusverifikasi == 1) {
				$totalFarmasiRajalReturObatVerif = $totalFarmasiRajalReturObatVerif + $row->totalkeseluruhan;
			}
			?>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalPoliObat + $totalFarmasiObatIGD + $totalFarmasiRajalReturObat)?></td>
        </tr>
    </tfoot>
</table>

<hr>

<b><span class="label label-success" style="font-size:12px">TINDAKAN IGD</span></b>
<b><span class="label label-default" style="font-size:12px">ALAT KESEHATAN</span></b>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
    <thead>
        <tr>
            <th style="width:10%">No. Transaksi</th>
            <th style="width:10%">Tanggal</th>
            <th style="width:20%">Alat Kesehatan</th>
            <th style="width:10%">Tarif</th>
            <th style="width:5%">Kuantitas</th>
            <th style="width:10%">Jumlah</th>
            <th style="width:5%">Diskon (%)</th>
            <th style="width:10%">Jumlah Setelah Diskon</th>
            <th style="width:10%">Unit Pelayanan</th>
            <th style="width:20%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php $totalPoliAlkes = 0;?>
        <?php $totalPoliAlkesVerif = 0;?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianRajalAlkesFilteredIGD($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->nopendaftaran?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none"><?=$row->idunit?></td>
            <td><?=$row->namaunit?></td>
            <td>
                <?php if ($row->statusverifikasi == 0) { ?>
                <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>" data-table="tpoliklinik_alkes"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <button data-idrow="<?=$row->iddetail?>" data-table="tpoliklinik_alkes"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } else { ?>
                <button data-idrow="<?=$row->iddetail?>" data-table="tpoliklinik_alkes"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalPoliAlkes = $totalPoliAlkes + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalPoliAlkesVerif = $totalPoliAlkesVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>

        <?php $totalFarmasiAlkesIGD = 0;?>
        <?php $totalFarmasiAlkesIGDVerif = 0;?>
        <?php $dataFarmasiAlkesIGD = $this->Trawatinap_verifikasi_model->viewRincianFarmasiRajal($idpendaftaran, 1);?>
        <?php foreach ($dataFarmasiAlkesIGD as $row) { ?>
        <?php $subtotal = $row->hargajual * $row->kuantitas ?>
        <tr>
            <td><?=$row->nopenjualan?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none">1</td>
            <td>Farmasi</td>
            <td>
                <?php if ($row->statusverifikasi == 0) { ?>
                <?php if ($row->statusracikan == 0) { ?>
                <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>"
                    data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <? } ?>
                <button data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-success verifTindakan"><i
                        class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } else { ?>
                <button data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="<?=($row->statusracikan == 0 ? 'tpasien_penjualan_nonracikan' : 'tpasien_penjualan_racikan')?>"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> class="btn btn-sm btn-danger unverifTindakan"><i
                        class="fa fa-times"></i>
                    Verifikasi</button>
                <?php }?>
            </td>
        </tr>
        <?php
            $totalFarmasiAlkesIGD = $totalFarmasiAlkesIGD + $row->totalkeseluruhan;
            if ($row->statusverifikasi == 1) {
                $totalFarmasiAlkesIGDVerif = $totalFarmasiAlkesIGDVerif + $row->totalkeseluruhan;
            }
            ?>
        <?php }?>

        <?php $totalFarmasiRajalReturAlkes = 0; ?>
        <?php $totalFarmasiRajalReturAlkesVerif = 0; ?>
        <?php foreach ($this->Trawatinap_verifikasi_model->viewRincianFarmasiReturRajalAlkes($idpendaftaran) as $row) { ?>
        <tr>
            <td><?=$row->nopengembalian?></td>
            <td><?=DMYFormat($row->tanggal)?></td>
            <td><?=$row->namatarif?></td>
            <td style="display: none"><?=number_format($row->hargadasar)?></td>
            <td style="display: none"><?=number_format($row->margin)?></td>
            <td><?=number_format($row->hargajual)?></td>
            <td><?=number_format($row->kuantitas)?></td>
            <td><?=number_format($row->hargajual * $row->kuantitas)?></td>
            <td hidden><?=number_format($row->diskon, 2)?></td>
            <td><?= calcDiscountPercent($row->diskon, $row->hargajual); ?></td>
            <td><?=number_format($row->totalkeseluruhan)?></td>
            <td style="display: none"><?=$row->idunitpelayanan?></td>
            <td><?=$row->unitpelayanan?></td>
            <td>
                <?php if ($row->statusverifikasi == 0) { ?>
                <button data-toggle="modal" data-target="#EditTransaksiTindakanFarmasiModal"
                    data-idrow="<?=$row->iddetail?>" data-table="tpasien_pengembalian_detail"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-primary editDataTindakanFarmasi"><i class="fa fa-pencil"></i></button>
                <button data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="tpasien_pengembalian_detail" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-success verifTindakan"><i class="fa fa-check"></i>
                    Verifikasi</button>
                <?php } else { ?>
                <button data-idrow="<?=$row->iddetail?>" data-statusracikan="<?=$row->statusracikan?>"
                    data-table="tpasien_pengembalian_detail" <?=($statuspembayaran == 1 ? 'disabled' : '')?>
                    class="btn btn-sm btn-danger unverifTindakan"><i class="fa fa-times"></i>
                    Verifikasi</button>
                <?php } ?>
            </td>
        </tr>
        <?php
			$totalFarmasiRajalReturAlkes = $totalFarmasiRajalReturAlkes + $row->totalkeseluruhan;
			if ($row->statusverifikasi == 1) {
				$totalFarmasiRajalReturAlkesVerif = $totalFarmasiRajalReturAlkesVerif + $row->totalkeseluruhan;
			}
			?>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td align="center" colspan="7"><b>TOTAL</b></td>
            <td class="text-bold" colspan="3"><?=number_format($totalPoliAlkes + $totalFarmasiAlkesIGD + $totalFarmasiRajalReturAlkes)?>
            </td>
        </tr>
    </tfoot>
</table>