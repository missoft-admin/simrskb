<div id="transaction-only" style="display:none;">
    <div class="block">
        <div class="block-content">
            <h5 style="margin-bottom: 10px;">Pembayaran</h5>
            <table id="cara_pembayaran" class="table table-bordered table-striped" style="margin-top: 20px;">
                <tbody>
                    <tr>
                        <th style="width: 85%;" colspan="5"><label class="pull-right"><b>SUB TOTAL Rp.</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
                                <input class="form-control input-sm " readonly type="text" id="gt_all" name="gt_all"
                                    value="<?=number_format($totalKeseluruhan)?>">
                        </th>
                    </tr>
                    <tr>
                        <th style="width: 85%;" colspan="5"><label class="pull-right"><b>DISKON</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
                                <div class="input-group">
                                    <span class="input-group-addon">Rp</span>
                                    <input class="form-control" type="text" id="diskon_rp" name="diskon_rp"
                                        placeholder="Discount Rp" value="{diskon_rp}">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon"> %. </span>
                                    <input class="form-control" type="text" id="diskon_persen" name="diskon_persen"
                                        placeholder="Discount %" value="{diskon_persen}">
                                </div>
                        </th>
                    </tr>
                    <tr>
                        <th style="width: 85%;" colspan="5"><label class="pull-right"><b>TOTAL Rp.</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
                                <input class="form-control input-sm " readonly type="text" id="gt_rp" name="gt_rp"
                                    value="<?=number_format($totalKeseluruhan)?>">
                        </th>
                    </tr>
                    <tr>
                        <th style="width: 85%;" colspan="5"><label class="pull-right"><b>TOTAL DEPOSIT</b></label></th>
                        <th style="width: 15%;" colspan="2"><b>
                                <input class="form-control input-sm " readonly type="text" id="gt_deposit"
                                    name="gt_deposit" value="<?=number_format($totaldeposit)?>">
                        </th>
                    </tr>
                    <tr>
                        <th style="width: 85%;" colspan="5"><label class="pull-right"><b>TOTAL HARUS DIBAYAR</b></label>
                        </th>
                        <th style="width: 15%;" colspan="2"><b>
                                <input class="form-control input-sm " readonly type="text" id="gt_final" name="gt_final"
                                    value="<?=number_format($totalHarusDibayar)?>">
                        </th>
                    </tr>
                    <tr class="bg-light dker">
                        <th colspan="5"></th>
                        <th colspan="2">
                            <?php if (($page!='ods' && UserAccesForm($user_acces_form,array('411')))||($page=='ods' && UserAccesForm($user_acces_form,array('1322')))){ ?>
                            <button class="btn btn-success  btn_pembayaran" id="btn_pembayaran" data-toggle="modal"
                                data-target="#PembayaranModal" type="button">Pilih Cara Pembayaran</button>
                            <?php } ?>
                        </th>
                    </tr>
                </tbody>
            </table>

            <div class="row">
                <div class="col-sm-12">
                    <div class="progress progress-mini">
                        <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100"
                            aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                    </div>
                    <div class="control-group">
                        <?php if($statuspembayaran) { ?>
                        <div class="row" style="margin-top: 10px; margin-bottom: 25px;">
                            <div class="form-group">
                                <label class="col-md-2 control-label" style="margin-top: 10px;">Tanggal Pembayaran
                                    :</label>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <input type="text" class="js-datepicker form-control"
                                                id="tanggal_pembayaran" data-date-format="dd/mm/yyyy"
                                                placeholder="HH/BB/TTTT" value="{tanggal_pembayaran}">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="time-datepicker form-control"
                                                id="waktu_pembayaran" value="{waktu_pembayaran}">
                                        </div>
                                        <div class="col-md-8" id="message_pembayaran">
                                            <button class="btn btn-success" id="btnUpdateTanggalPembayaran"
                                                style="font-size:13px;"><i class="fa fa-filter"></i> Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <? } ?>
                        <div class="col-md-12"
                            style="padding-left: 0; padding-right: 0; margin-left: 0; margin-bottom:20px">
                            <table id="manage_tabel_pembayaran" class="table table-striped table-bordered"
                                style="margin-bottom: 0;">
                                <thead>
                                    <tr>
                                        <th style="width: 5%;">#</th>
                                        <th style="width: 25%;">Jenis Pembayaran</th>
                                        <th style="width: 35%;">Keterangan</th>
                                        <th style="width: 10%;">Nominal</th>
                                        <th style="width: 10%;">Action</th>
                                    </tr>
                                </thead>
                                <input type="hidden" id="rowindex">
                                <input type="hidden" id="nomor">
                                <tbody>
                                    <?php $no = 1;?>
                                    <?php foreach ($detail_pembayaran as $row) { ?>
                                    <tr>
                                        <td hidden><?=$row->idmetode?></td>
                                        <td><?=$no++?></td>
                                        <td><?=metodePembayaran($row->idmetode)?></td>
                                        <td><?=$row->keterangan?></td>
                                        <td hidden><?=$row->nominal?></td>
                                        <td><?=number_format($row->nominal)?></td>
                                        <td hidden>idkasir</td>
                                        <td hidden><?=$row->idmetode?></td>
                                        <td hidden><?=$row->idpegawai?></td>
                                        <td hidden><?=$row->idbank?></td>
                                        <td hidden><?=$row->ket_cc?></td>
                                        <td hidden><?=$row->idkontraktor?></td>
                                        <td hidden><?=$row->keterangan?></td>
                                        <td>
                                            <?php if (($page!='ods' && UserAccesForm($user_acces_form,array('412')))||($page=='ods' && UserAccesForm($user_acces_form,array('1323')))){ ?>
                                            <button type='button' class='btn btn-sm btn-info edit'><i
                                                    class='glyphicon glyphicon-pencil'></i></button>
                                            <?php } ?>
                                            &nbsp;&nbsp;
                                            <?php if (($page!='ods' && UserAccesForm($user_acces_form,array('413')))||($page=='ods' && UserAccesForm($user_acces_form,array('1324')))){ ?>
                                            <button type='button' class='btn btn-sm btn-danger hapus'><i
                                                    class='glyphicon glyphicon-remove'></i></button>
                                            <?php } ?>
                                        </td>
                                        <td hidden><?=$row->jaminan?></td>
                                        <td hidden><?=$row->tracenumber?></td>
                                        <?php if ($row->idmetode =='8') { ?>
                                        <td hidden><?=$row->tipekontraktor?></td>
                                        <?php }else{ ?>
                                        <td hidden>0</td>
                                        <?php } ?>
                                    </tr>
                                    <?php }?>
                                </tbody>
                                <tfoot id="foot_pembayaran">
                                    <tr class="bg-light dker">
                                        <th colspan="3"><label class="pull-right"><b>Diskon Rp.</b></label></th>
                                        <th colspan="2">
                                            <input type="text" class="form-control number input-sm"
                                                id="diskon_pembayaran" readonly value="{diskon_rp}" />
                                        </th>
                                    </tr>
                                    <tr class="bg-light dker">
                                        <th colspan="3"><label class="pull-right"><b>Pembayaran Rp.</b></label></th>
                                        <th colspan="2">
                                            <?php if ($total_pembayaran): ?>
                                            <input type="text" class="form-control input-sm " readonly id="bayar_rp"
                                                name="bayar_rp" value="<?=$total_pembayaran?>" />
                                            <?php else: ?>
                                            <input type="text" class="form-control input-sm " readonly id="bayar_rp"
                                                name="bayar_rp" value="{bayar_rp}" />
                                            <?php endif?>
                                        </th>
                                    </tr>
                                    <tr class="bg-light dker">
                                        <th colspan="3"><label class="pull-right"><b>Sisa Rp.</b></label></th>
                                        <th colspan="2">
                                            <input type="text" class="form-control input-sm " readonly id="sisa_rp"
                                                name="sisa_rp" value="{sisa_rp}" />
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <div id="transaction-only-action" style="display:none;">
        <input type="hidden" id="tempIdRawatInap" name="" value="{idpendaftaran}">
        <input type="hidden" id="pos_tabel_pembayaran" name="pos_tabel_pembayaran">
        <input type="hidden" id="idpendaftaran" value="{idpendaftaran}">
        <div class="modal-footer">
            <?php if (($page!='ods' && UserAccesForm($user_acces_form,array('414')))||($page=='ods' && UserAccesForm($user_acces_form,array('1325')))){ ?>
            <div class="buton">
                <button type="submit" class="btn btn-primary" id="btn_save">Simpan Pembayaran</a>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

<!-- <script type="text/javascript">
    $("#btn_save").click((function() {
        $("#btn_save").attr('disabled', 'disabled');
    }));
</script> -->