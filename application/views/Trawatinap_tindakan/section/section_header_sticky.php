<?php if (($page != 'ods' && UserAccesForm($user_acces_form, ['404', '405', '406', '407'])) || ($page == 'ods' && UserAccesForm($user_acces_form, ['1315', '1316', '1317', '1318']))) { ?>
    <div class="row">
        <div class="col-md-12" style="text-align: end;">
            <div class="btn-group">
                <div class="btn-group">
                    <button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                        <span class="fa fa-file-excel-o"></span> Export Rincian
                    </button>
                    <ul class="dropdown-menu dropdown-menu-left">
                        <?php $this->load->view('Trawatinap_tindakan/section/action_export_rincian'); ?>
                    </ul>
                </div>
            </div>
            <div class="btn-group">
                <div class="btn-group">
                    <button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                        <span class="fa fa-print"></span> Print Rincian
                    </button>
                    <ul class="dropdown-menu dropdown-menu-left">
                        <?php $this->load->view('Trawatinap_tindakan/section/action_print_rincian'); ?>
                    </ul>
                </div>
            </div>
            <hr>
        </div>
    </div>
<?php } ?>

<?php $this->load->view('Trawatinap_tindakan/section/section_informasi_pasien'); ?>

<hr>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group" style="margin-bottom: 10px;">
                <label class="col-md-4 control-label" style="margin-top: 5px;">Tipe Pasien</label>
                <div class="col-md-8">
                    <select id="idtipepasien" class="js-select2 form-control form-select" style="width: 100%;" data-placeholder="Pilih Opsi">
                        <option value="1" <?=($idtipepasien == 1 ? 'selected' : '')?>>Pasien RS</option>
                        <option value="2" <?=($idtipepasien == 2 ? 'selected' : '')?>>Pasien Pribadi</option>
                    </select>
                </div>
            </div>
        </div>
    </div>

<hr>

<?php if ($statuscheckout == 0) { ?>
    Status Checkout : <span class="badge badge-danger" id="status-checkout">Belum Diaktifkan</span>
<?php } else { ?>
    Status Checkout : <span class="badge badge-success" id="status-checkout">Telah Diaktifkan</span>
<?php } ?>

<hr>

<div class="btn-group" id="actionVerifikasi"></div>

<?php if ($statuskasir == 0) { ?>
    <div class="btn-group">
        <?php if (($page != 'ods' && UserAccesForm($user_acces_form, ['418'])) || ($page == 'ods' && UserAccesForm($user_acces_form, ['1329']))) { ?>
            <button data-toggle="modal" data-target="#ActionTindakanModal" class="btn btn-primary" type="button"><i class="fa fa-plus"></i> Tindakan Rawat Inap</button>
        <?php } ?>
        
        <?php if (($page != 'ods' && UserAccesForm($user_acces_form, ['419'])) || ($page == 'ods' && UserAccesForm($user_acces_form, ['1330']))) { ?>
            <button data-toggle="modal" data-target="#ActionVisiteModal" class="btn btn-primary" type="button"><i class="fa fa-plus"></i> Visite Dokter</button>
        <?php } ?>
    </div>
<?php } ?>

<?php if ($bataltransaksi > 0) { ?>
    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#HistoryBatalModal">History Batal</button>
<?php } ?>

    <div class="btn-group">
        <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
            <span class="fa fa-file-o"></span> Print Faktur Implant
        </button>
        <ul class="dropdown-menu dropdown-menu-left">
            <li>
                <a tabindex="-1" href="#" data-toggle="modal" data-target="#CetakFakturImplantModal">Print Faktur Implant</a>
            </li>
            <li>
                <a tabindex="-1" href="#" data-toggle="modal" data-target="#CetakKwitansiImplantModal">Print Kwitansi Implant</a>
            </li>
        </ul>
    </div>

<button id="pinheader" class="btn" style="float:right; color:red"><i class="fa fa-thumb-tack"></i>Freeze</button>