<?php $totalKeseluruhan = $totalSebelumAdm + $totalAdmRanap; ?>
<?php $totalVerifikasi = $totalSebelumAdmVerif + $totalAdmRanap; ?>
<?php $totalHarusDibayar = $totalKeseluruhan - $totaldeposit; ?>

<table class="table table-bordered table-striped" style="margin-top: 20px;">
    <thead>
        <tr>
            <td align="right" style="font-size:15px"><b>TOTAL KESELURUHAN</b></td>
            <td align="left" class="text-bold" id="grandTotal" align="center" style="font-size:15px">
                <?=number_format($totalKeseluruhan)?></td>
        </tr>
        <tr>
            <td align="right" style="font-size:15px"><b>TOTAL VERIFIKASI</b></td>
            <td align="left" class="text-bold" id="grandTotalVerifikasi" align="center" style="font-size:15px">
                <?=number_format($totalVerifikasi)?></td>
        </tr>
        <tr>
            <td align="right" style="font-size:15px"><b>TOTAL DEPOSIT</b></td>
            <td align="left" class="text-bold" id="grandTotalDeposit" align="center" style="font-size:15px">
                <span id="totalDeposit"><?=number_format($totaldeposit)?></span> <button id="openModalRincian"
                    <?=($statuspembayaran == 1 ? 'disabled' : '')?> data-toggle="modal"
                    data-target="#RincianDepositModal" class="btn btn-success btn-sm" style="float:right"><i
                        class="fa fa-newspaper-o"></i> Rincian Deposit</button></td>
        </tr>
        <tr>
            <td align="right" style="font-size:15px"><b>TOTAL HARUS DIBAYAR</b></td>
            <td align="left" class="text-bold" id="grandTotalPembayaran" align="center" style="font-size:15px">
                <?=number_format($totalHarusDibayar)?></td>
        </tr>
    </thead>
</table>