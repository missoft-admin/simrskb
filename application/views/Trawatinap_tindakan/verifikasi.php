<?php
	echo ErrorSuccess($this->session);
	if ($error != '') {
		echo ErrorMessage($error);
	}
?>

<style media="screen">
	.text-bold {
		font-weight: bold;
	}

	.fixed-header {
		padding-top: 1rem;
		position: fixed;
		top: 6rem;
		width: calc(100% - 16rem);
		z-index: 100;
		background: #fff;
	}

	.fixed-header-full {
		padding-top: 1rem;
		position: fixed;
		top: 6rem;
		width: calc(100% - 33rem);
		z-index: 100;
		background: #fff;
		left: 28rem;
	}

	.separator-header {
		margin-top: 450px;
	}

	.modal {
		overflow: auto !important;
	}
</style>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>
				<?php if ($page == 'rawatinap') { ?>
					<a href="{base_url}trawatinap_tindakan/index" class="btn"><i class="fa fa-reply"></i></a>
				<?php } else { ?>
					<a href="{base_url}trawatinap_tindakan_ods/index" class="btn"><i class="fa fa-reply"></i></a>
				<?php } ?>
			</li>
		</ul>
		<h3 class="block-title">{title} </h3>
	</div>
	<div class="block-content">
		<div class="header-freeze">
			<?php require __DIR__ . '/section/section_header_sticky.php'; ?>
		</div>
		<div class="content-verification">
			<hr>
			<?php require __DIR__ . '/section/tindakan/tindakan_ruangan.php'; ?>
			<hr>
			<?php require __DIR__ . '/section/tindakan/tindakan_ranap.php'; ?>
			<hr>
			<?php require __DIR__ . '/section/tindakan/tindakan_igd.php'; ?>
			<hr>
			<?php require __DIR__ . '/section/tindakan/tindakan_laboratorium.php'; ?>
			<hr>
			<?php require __DIR__ . '/section/tindakan/tindakan_radiologi.php'; ?>
			<hr>
			<?php require __DIR__ . '/section/tindakan/tindakan_fisioterapi.php'; ?>
			<hr>
			<?php require __DIR__ . '/section/tindakan/tindakan_operasi.php'; ?>
			<hr>
			<?php require __DIR__ . '/section/tindakan/tindakan_administrasi.php'; ?>
			<hr>
			<?php require __DIR__ . '/section/section_rekapitulasi.php'; ?>
		</div>
	</div>
</div>

<?php require __DIR__ . '/section/section_catatan.php'; ?>
<?php require __DIR__ . '/section/section_pembayaran.php'; ?>

<?php require __DIR__ . '/loader/services.php'; ?>

<script type="text/javascript">
	$(document).ready(function () {
		var idpendaftaran = '{idpendaftaran}';

		initFormInput();
		getActionButton();
		getDokter();

		// Tipe Pasien
		$(document).on('change', '#idtipepasien', function() {
			var idtipepasien = $(this).val();

			$.ajax({
				url: '{site_url}tverifikasi_transaksi/updateTipePasien',
				method: 'POST',
				dataType: 'json',
				data: {
					idpendaftaran: idpendaftaran,
					idtipepasien: idtipepasien,
					table: 'trawatinap_pendaftaran'
				},
				success: function(data) {
					if (data.status) {
						swal({
							title: 'Berhasil',
							text: 'Tipe Pasien berhasil diubah.',
							type: 'success',
							timer: 1500
						});
					}
				}
			});
		});

		// Tanggal & Waktu Pembayaran
		$(document).on('click', '#btnUpdateTanggalPembayaran', function () {
			var tanggalPembayaran = $('#tanggal_pembayaran').val();
			var waktuPembayaran = $('#waktu_pembayaran').val();

			var payload = {
				idpendaftaran: idpendaftaran,
				tanggal_pembayaran: tanggalPembayaran,
				waktu_pembayaran: waktuPembayaran
			}

			updateTanggalPembayaranTransaksi(payload);
		});

		// Catatan Transaksi
		$(document).on('click', '#saveNote', function () {
			var catatan = $('#catatan').val();

			var payload = {
				idpendaftaran: idpendaftaran,
				catatan: catatan
			}

			updateCatatanTransaksi(payload);
		});

		// Lock Kasir
		$(document).on('click', '#lockKasir', function () {
			lockTransaksiKasir(idpendaftaran);
		});

		// UnLock Kasir
		$(document).on('click', '#unlockKasir', function () {
			unlockTransaksiKasir(idpendaftaran);
		});

		// Proses Validasi
		$(document).on('click', '#prosesValidasi', function () {
			var tempRuanganRanap = $('#tempRuanganRanap tr').get().map(function (row) {
				return $(row).find('td').get().map(function (cell) {
					return $(cell).html();
				});
			});

			var tempAdministrasiRanap = $('#tempAdministrasiRanap tr').get().map(function (row) {
				return $(row).find('td').get().map(function (cell) {
					return $(cell).html();
				});
			});

			var payload= {
				idpendaftaran: idpendaftaran,
				ruangranap: JSON.stringify(tempRuanganRanap),
				admranap: JSON.stringify(tempAdministrasiRanap)
			}

			swal({
				title: "Apakah anda yakin ingin melakukan proses validasi?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya",
				cancelButtonText: "Tidak",
				closeOnConfirm: false,
				closeOnCancel: false
			}).then(function () {
				prosesValidasi(payload);
			});
		});

		// Proses Transaksi Kasir
		$(document).on('click', '#transaksiKasir', function () {
			swal({
				title: "Apakah anda yakin ingin melakukan proses transaksi?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya",
				cancelButtonText: "Tidak",
				closeOnConfirm: false,
				closeOnCancel: false
			}).then(function () {
				prosesTransaksiKasir(idpendaftaran);
			});
		});

		// Batal Transaksi
		$('#idalasan_batal').select2({
			ajax: {
				url: '{site_url}trawatinap_tindakan/getAlasanBatal',
				dataType: 'json',
				processResults: function (data) {
					var res = data.results.map(function (item) {
						return {
							id: item.id,
							text: item.keterangan
						}
					})
					return {
						results: res
					}
				},
			}
		});

		// Proses Batal Transaksi
		$(document).on('click', '#saveBatalTransaksi', function () {
			var alasanbatal = $('#idalasan_batal').val();

			var payload = {
				idpendaftaran: idpendaftaran,
				alasanbatal: alasanbatal,
			}

			prosesBatalTransaksi(payload);
		});

		// Proses Cetak Kwitansi
		$(document).on('click', '#cetakProsesKwitansi', function () {
			var idmetode = $('#kwitansi_idmetode').val();
			var sudahterimadari = $('#kwitansi_terima_dari').val();
			var tipeKontraktor = $('#kwitansi_tipe_kontraktor').val();
			var idkontraktor = $('#kwitansi_idkontraktor').val();
			var deskripsi = $('#kwitansi_deskripsi').val();

			var payload = {
				idrawatinap: idpendaftaran,
				tipe_proses: idmetode,
				sudahterimadari: (sudahterimadari != "" ? sudahterimadari : "<?=$namapasien?>"),
				deskripsi: (deskripsi != "" ? deskripsi : "Tindakan dan Rawat Inap Pasien a/n <?=$namapasien?>"),
			}

			prosesCetakKwitansi(payload, tipeKontraktor, idkontraktor);
		});

		// Proses Cetak Kwitansi Implant
		$(document).on('click', '#cetakKwitansiImplant', function () {
			var sudahterimadari = $('#kimplant_terima_dari').val();
			var deskripsi = $('#kimplant_deskripsi').val();

			var payload = {
				idrawatinap: idpendaftaran,
				sudahterimadari: (sudahterimadari != "" ? sudahterimadari : "<?=$namapasien?>"),
				deskripsi: (deskripsi != "" ? deskripsi : "Implant Pasien a/n <?=$namapasien?>"),
			}

			prosesCetakKwitansiImplant(payload);
		});

		// Proses Cetak Faktur Implant
		$(document).on('click', '#cetakFakturImplant', function () {
			var nomedrec = $('#fimplant_nomedrec').val();
			var namapasien = $('#fimplant_namapasien').val();
			var namadokter = $('#fimplant_dokter').val();
			var diagnosa = $('#fimplant_diagnosa').val();
			var tanggaloperasi = $('#fimplant_tanggaloperasi').val();
			var operasi = $('#fimplant_operasi').val();
			var iddistributor = $('#fimplant_distributor').val();

			var payload = {
				idrawatinap: idpendaftaran,
				nomedrec: nomedrec,
				namapasien: namapasien,
				namadokter: namadokter,
				diagnosa: diagnosa,
				tanggaloperasi: tanggaloperasi,
				operasi: operasi,
				iddistributor: iddistributor,
			}

			prosesCetakFakturImplant(payload);
		});

		// Verifikasi Tindakan
		$(document).on('click', '.verifTindakan', function () {
			var idrow = $(this).data('idrow');
			var table = $(this).data('table');
			var statusracikan = $(this).data('statusracikan');
			var statusverifikasi = '1';

			var payload = {
				idrow: idrow,
                table: table,
                statusracikan: statusracikan,
                statusverifikasi: statusverifikasi
			}

			swal({
				title: "Apakah anda yakin ingin memverifikasi data ini?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya",
				cancelButtonText: "Tidak",
				closeOnConfirm: false,
				closeOnCancel: false
			}).then(function () {
				updateStatusVerifikasiTindakan(payload);
			});
		});

		// Unverfikasi Tindakan
		$(document).on('click', '.unverifTindakan', function () {
			var idrow = $(this).data('idrow');
			var table = $(this).data('table');
			var statusracikan = $(this).data('statusracikan');
			var statusverifikasi = '0';

			var payload = {
				idrow: idrow,
                table: table,
                statusracikan: statusracikan,
                statusverifikasi: statusverifikasi
			}

			swal({
				title: "Apakah anda yakin ingin membatalkan verifikasi data ini?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya",
				cancelButtonText: "Tidak",
				closeOnConfirm: false,
				closeOnCancel: false
			}).then(function () {
				updateStatusVerifikasiTindakan(payload);
			});
		});

		// Verifikasi Ruangan Ranap
		$(document).on('click', '.verifRuanganRanap', function () {
			var idrow = $(this).data('idrow');
			var table = $(this).data('table');
			var statusverifikasi = '1';

			var payload = {
                idrow: idrow,
                table: table,
                statusverifikasi: statusverifikasi
            }

			swal({
				title: "Apakah anda yakin ingin memverifikasi data ini?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya",
				cancelButtonText: "Tidak",
				closeOnConfirm: false,
				closeOnCancel: false
			}).then(function () {
				updateStatusVerifikasiRuanganRanap(payload);
			});
		});

		// Unverfikasi Ruangan Ranap
		$(document).on('click', '.unverifRuanganRanap', function () {
			var idrow = $(this).data('idrow');
			var table = $(this).data('table');
			var statusverifikasi = '0';

			var payload = {
                idrow: idrow,
                table: table,
                statusverifikasi: statusverifikasi
            }

			swal({
				title: "Apakah anda yakin ingin membatalkan verifikasi data ini?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya",
				cancelButtonText: "Tidak",
				closeOnConfirm: false,
				closeOnCancel: false
			}).then(function () {
				updateStatusVerifikasiRuanganRanap(payload);
			});
		});

		// Proses Kwitansi
		$(document).on('click', '.prosesKwitansi', function () {
			$('#kwitansi_idmetode').val($(this).data('idmetode'));
			$('#kwitansi_tipe_kontraktor').val($(this).data('tipekontraktor'));
			$('#kwitansi_idkontraktor').val($(this).data('idkontraktor'));
		});

		// Utils
		setInterval(function () {
			getStatusCheckout(idpendaftaran);
		}, 10000);

		var pinStatus = true;
		$("#pinheader").click(function () {
			pinStatus = !pinStatus;
			if (pinStatus) {
				$("#pinheader").html('<i class="fa fa-close"></i> Close');
			} else {
				$("#pinheader").html('<i class="fa fa-thumb-tack"></i> Freeze');

				$('.header-freeze').removeClass('fixed-header');
				$('.header-freeze').removeClass('fixed-header-full');
				$('.content-verification').removeClass('separator-header');
			}
		});

		$(window).scroll(function () {
			if (pinStatus) {
				$("#pinheader").html('<i class="fa fa-close"></i> Close');
				if ($(window).scrollTop() >= 300) {
					if (localStorage.getItem("sidebar") == 'full') {
						$('.header-freeze').addClass('fixed-header-full');
					} else {
						$('.header-freeze').addClass('fixed-header');
					}

					$('.content-verification').addClass('separator-header');
				} else {
					if (localStorage.getItem("sidebar") == 'full') {
						$('.header-freeze').removeClass('fixed-header-full');
					} else {
						$('.header-freeze').removeClass('fixed-header');
					}

					$('.content-verification').removeClass('separator-header');
				}
			} else {
				$("#pinheader").html('<i class="fa fa-thumb-tack"></i> Freeze');
			}
		});
	});
</script>

<!-- Modal Included -->
<?php $this->load->view('Trawatinap_tindakan/modal/modal_action_tindakan') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_action_visite') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_administrasi') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_administrasi_ranap') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_deposit') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_kamar_ok') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_operator_ok') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_asisten_ok') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_ruangan_ranap') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_tindakan_farmasi') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_tindakan_global') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_tindakan_ranap') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_edit_visite_ranap') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_pembayaran') ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_history_pembatalan'); ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_konfirmasi_pembatalan'); ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_proses_kwitansi'); ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_cetak_faktur_implant'); ?>
<?php $this->load->view('Trawatinap_tindakan/modal/modal_kwitansi_implant'); ?>