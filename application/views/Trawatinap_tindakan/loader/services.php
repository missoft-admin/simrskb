<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
    function updateTanggalPembayaranTransaksi(payload) {
        $.ajax({
            url: '{site_url}trawatinap_tindakan/updateTanggalPembayaran',
            method: 'POST',
            data: payload,
            success: function (data) {
                $("#message_pembayaran").html('<div class="alert alert-success" style="padding: 8px 10px 5px 15px;"><p><b>Berhasil!</b> Tanggal Pembayaran Telah di Ubah</p></div>');

                setTimeout(function () {
                    $("#message_pembayaran").html('<button class="btn btn-success" id="btnUpdateTanggalPembayaran" style="font-size:13px;"><i class="fa fa-filter"></i> Simpan</button>');
                }, 3000);
            }
        });
    }
    
    function updateCatatanTransaksi(payload) {
        $.ajax({
            url: '{site_url}trawatinap_tindakan/saveCatatan',
            method: 'POST',
            data: payload,
            success: function (data) {
                swal({
                    title: "Berhasil!",
                    text: "Catatan telah tersimpan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
            }
        });
    }
    
    function prosesValidasi(payload) {
        $.ajax({
            method: 'POST',
            data: payload,
            url: '{site_url}trawatinap_tindakan/prosesValidasi',
            success: function (data) {
                swal({
                    title: "Berhasil!",
                    text: "Transaksi telah di lakukan proses validasi.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                location.reload();
            }
        });
    }
    
    function lockTransaksiKasir(idpendaftaran) {
        $.ajax({
            url: '{site_url}trawatinap_tindakan/lockKasir/' + idpendaftaran,
            success: function (data) {
                swal({
                    title: "Berhasil!",
                    text: "Transaksi Telah Di-Lock.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                location.reload();
            }
        });
    }

    function unlockTransaksiKasir(idpendaftaran) {
        $.ajax({
            url: '{site_url}trawatinap_tindakan/unlockKasir/' + idpendaftaran,
            success: function (data) {
                swal({
                    title: "Berhasil!",
                    text: "Transaksi Telah Di-Lock.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                location.reload();
            }
        });
    }
    
    function prosesTransaksiKasir(idpendaftaran) {
        $.ajax({
            method: 'POST',
            data: {
                idpendaftaran: idpendaftaran,
            },
            url: '{site_url}trawatinap_tindakan/prosesTransaksi',
            success: function (data) {
                swal({
                    title: "Berhasil!",
                    text: "Transaksi telah di proses.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                location.reload();
            }
        });
    }
    
    function prosesBatalTransaksi(payload) {
        $.ajax({
            method: 'POST',
            data: payload,
            url: '{site_url}trawatinap_tindakan/prosesBatalTransaksi',
            success: function (data) {
                swal({
                    title: "Berhasil!",
                    text: "Transaksi telah di batalkan.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                location.reload();
            }
        });
    }
    
    function prosesCetakKwitansi(payload, tipeKontraktor, idkontraktor) {
        $.ajax({
            method: 'POST',
            data: payload,
            url: '{site_url}trawatinap_tindakan/prosesCetakKwitansi',
            success: function (idkwitansi) {
                swal({
                    title: "Berhasil!",
                    text: "Proses Cetak Kwitansi Berhasil.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                setTimeout(function () {
                    var win = window.open('{site_url}trawatinap_tindakan/print_kwitansi/' + idkwitansi + '/' + tipeKontraktor + '/' + idkontraktor, '_blank');
                    win.focus();

                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }, 2000);
            }
        });
    }

    function prosesCetakKwitansiImplant(payload) {
        $.ajax({
            method: 'POST',
            data: payload,
            url: '{site_url}trawatinap_tindakan/prosesCetakKwitansiImplant',
            success: function (idkwitansi) {
                swal({
                    title: "Berhasil!",
                    text: "Proses Cetak Kwitansi Implant Berhasil.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                setTimeout(function () {
                    var win = window.open('{site_url}trawatinap_tindakan/print_kwitansi_implant/' + idkwitansi, '_blank');
                    win.focus();

                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }, 2000);
            }
        });
    }

    function prosesCetakFakturImplant(payload) {
        $.ajax({
            method: 'POST',
            data: payload,
            url: '{site_url}trawatinap_tindakan/prosesCetakFakturImplant',
            success: function (idfaktur) {
                swal({
                    title: "Berhasil!",
                    text: "Proses Cetak Faktur Implant Berhasil.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });

                setTimeout(function () {
                    var win = window.open('{site_url}trawatinap_tindakan/print_faktur_implant/' + idfaktur, '_blank');
                    win.focus();

                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }, 2000);
            }
        });
    }
    
    function updateStatusVerifikasiTindakan(payload) {
        $.ajax({
            url: '{site_url}trawatinap_tindakan/updateStatusTindakanDetail',
            method: 'POST',
            data: payload,
            success: function (data) {
                var message = (payload.statusverifikasi == '1' ? 'Tindakan Telah Diverifikasi' : 'Verifikasi Tindakan Telah Dibatalkan');
                swal({
                    title: "Berhasil!",
                    text: message,
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                location.reload();
            }
        });
    }
    
    function updateStatusVerifikasiRuanganRanap(payload) {
        $.ajax({
            url: '{site_url}trawatinap_tindakan/updateStatusRuanganRanap',
            method: 'POST',
            data: payload,
            success: function (data) {
                var message = (payload.statusverifikasi == '1' ? 'Ruangan Rawat Inap Telah Diverifikasi' : 'Verifikasi Ruangan Rawat Inap Telah Dibatalkan');
                swal({
                    title: "Berhasil!",
                    text: message,
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                location.reload();
            }
        });
    }

	function getActionButton() {
		var action = '';
		var statuskasir = <?= $statuskasir; ?>;
		var statusvalidasi = <?= $statusvalidasi; ?>;
		var statustransaksi = <?= $statustransaksi; ?>;
		var statuspembayaran = <?= $statuspembayaran; ?>;
		
		if (statuskasir == 1) {
			<?php if (($page != 'ods' && UserAccesForm($user_acces_form, array('1201'))) || ($page == 'ods' && UserAccesForm($user_acces_form, array('1338')))) { ?>
				action += '<button id="unlockKasir" ' + (statustransaksi == 1 ? "disabled" : "") + ' class="btn btn-md btn-danger" style="width: 120px;"><i class="fa fa-lock"></i> UnLock</button>';
			<?php } ?>
		} else {
			<?php if (($page != 'ods' && UserAccesForm($user_acces_form, array('408'))) || ($page == 'ods' && UserAccesForm($user_acces_form, array('1319')))) { ?>
				action += '<button id="lockKasir" class="btn btn-md btn-danger" style="width: 120px;"><i class="fa fa-lock"></i> Lock</button>';
			<?php } ?>
		}

		if (statusvalidasi == 1) {
			<?php if (($page != 'ods' && UserAccesForm($user_acces_form, array('409'))) || ($page == 'ods' && UserAccesForm($user_acces_form, array('1320')))) { ?>
				action += '<button disabled class="btn btn-md btn-warning" style="width: 150px;"><i class="fa fa-refresh"></i> Proses Validasi</button>';
			<?php } ?>

			if (statustransaksi == 0) {
				<?php if (($page != 'ods' && UserAccesForm($user_acces_form, array('410'))) || ($page == 'ods' && UserAccesForm($user_acces_form, array('1321')))) { ?>
					action += '<a href="#" id="transaksiKasir" class="btn btn-md btn-success" style="width: 120px;"><i class="fa fa-money"></i> Transaksi</a>';
				<?php } ?>
			}
		} else {
			<?php if (($page != 'ods' && UserAccesForm($user_acces_form, array('409'))) || ($page == 'ods' && UserAccesForm($user_acces_form, array('1320')))) { ?>
				action += '<button id="prosesValidasi" class="btn btn-md btn-warning" style="width: 150px;"><i class="fa fa-refresh"></i> Proses Validasi</button>';
			<?php } ?>

			<?php if (($page != 'ods' && UserAccesForm($user_acces_form, array('410'))) || ($page == 'ods' && UserAccesForm($user_acces_form, array('1321')))) { ?>
				action += '<a href="#" disabled class="btn btn-md btn-success" style="width: 120px;"><i class="fa fa-money"></i> Transaksi</a>';
			<?php } ?>
		}

		if (statustransaksi == 1) {
			$("#transaction-only").show();
			$("#transaction-only-action").show();

			if (statuspembayaran == 0) {
				$('html, body').animate({
					scrollTop: $("#transaction-only").offset().top
				}, 2000);
			}
		} else {
			$("#transaction-only").hide();
			$("#transaction-only-action").hide();
		}

		if (statuspembayaran == 1) {
			action += `<div class="btn-group">
				<div class="btn-group">
					<?php if (($page != 'ods' && UserAccesForm($user_acces_form, ['416'])) || ($page == 'ods' && UserAccesForm($user_acces_form, ['1327']))) { ?>
					<button class="btn btn-success dropdown-toggle" data-toggle="dropdown">
						<span class="fa fa-list"></span> Proses Kwitansi
					</button>
					<?php } ?>
					<?php if (($page != 'ods' && UserAccesForm($user_acces_form, ['417'])) || ($page == 'ods' && UserAccesForm($user_acces_form, ['1328']))) { ?>
					<ul class="dropdown-menu dropdown-menu-left">
						<li>
							<a tabindex="-1" class="prosesKwitansi" target="_blank" data-toggle="modal" data-target="#ProsesKwitansiModal" href="#" data-idmetode="0" data-tipekontraktor="0" data-idkontraktor="0">Proses Kwitansi (All)</a>
							<?php if ($pembayaran_tunai || $deposit_tunai) { ?>
							<a tabindex="-1" class="prosesKwitansi" target="_blank" data-toggle="modal" data-target="#ProsesKwitansiModal" href="#" data-idmetode="1" data-tipekontraktor="0" data-idkontraktor="0">Proses Kwitansi Tunai</a>
							<?php } ?>
							<?php if ($pembayaran_debit || $deposit_debit) { ?>
							<a tabindex="-1" class="prosesKwitansi" target="_blank" data-toggle="modal" data-target="#ProsesKwitansiModal" href="#" data-idmetode="2" data-tipekontraktor="0" data-idkontraktor="0">Proses Kwitansi Debit</a>
							<?php } ?>
							<?php if ($pembayaran_kredit || $deposit_kredit) { ?>
							<a tabindex="-1" class="prosesKwitansi" target="_blank" data-toggle="modal" data-target="#ProsesKwitansiModal" href="#" data-idmetode="3" data-tipekontraktor="0" data-idkontraktor="0">Proses Kwitansi Kredit</a>
							<?php } ?>
							<?php if ($pembayaran_transfer || $deposit_transfer) { ?>
							<a tabindex="-1" class="prosesKwitansi" target="_blank" data-toggle="modal" data-target="#ProsesKwitansiModal" href="#" data-idmetode="4" data-tipekontraktor="0" data-idkontraktor="0">Proses Kwitansi Transfer</a>
							<?php } ?>
							<?php if ($pembayaran_karyawan_pegawai) { ?>
							<a tabindex="-1" class="prosesKwitansi" target="_blank" data-toggle="modal" data-target="#ProsesKwitansiModal" href="#" data-idmetode="5" data-tipekontraktor="0" data-idkontraktor="0">Proses Tagihan Karyawan (Pegawai)</a>
							<?php } ?>
							<?php if ($pembayaran_karyawan_dokter) { ?>
							<a tabindex="-1" class="prosesKwitansi" target="_blank" data-toggle="modal" data-target="#ProsesKwitansiModal" href="#" data-idmetode="6" data-tipekontraktor="0" data-idkontraktor="0">Proses Tagihan Karyawan (Dokter)</a>
							<?php } ?>
							<?php if ($pembayaran_tidak_tertagihkan) { ?>
							<a tabindex="-1" class="prosesKwitansi" target="_blank" data-toggle="modal" data-target="#ProsesKwitansiModal" href="#" data-idmetode="7" data-tipekontraktor="0" data-idkontraktor="0">Proses Tidak Tertagihkan</a>
							<?php } ?>
							<?php if ($pembayaran_kontraktor) { ?>
								<?php foreach ($list_kontraktor as $row) { ?>
									<a tabindex="-1" class="prosesKwitansi" target="_blank" data-toggle="modal" data-target="#ProsesKwitansiModal" href="#" data-idmetode="8" data-tipekontraktor="<?=$row->tipekontraktor; ?>" data-idkontraktor="<?=$row->idkontraktor; ?>">Proses Kontraktor (<?=$row->namakontraktor; ?>)</a>
								<?php } ?>
							<?php } ?>
							<?php if ($pembayaran_excess) { ?>
							<a tabindex="-1" class="prosesKwitansi" target="_blank" data-toggle="modal" data-target="#ProsesKwitansiModal" href="#" data-idmetode="9" data-tipekontraktor="0" data-idkontraktor="0">Proses Kwitansi Excess</a>
							<?php } ?>
							<?php if ($pembayaran_bpjstk) { ?>
							<a tabindex="-1" class="prosesKwitansi" target="_blank" data-toggle="modal" data-target="#ProsesKwitansiModal" href="#" data-idmetode="10" data-tipekontraktor="4" data-idkontraktor="0">Proses Kwitansi BPJSTK</a>
							<?php } ?>
						</li>
					</ul>
					<?php } ?>
				</div>
			</div>`;

			<?php if (($page != 'ods' && UserAccesForm($user_acces_form, array('415'))) || ($page == 'ods' && UserAccesForm($user_acces_form, array('1326')))) { ?>
				<?php if ($status_verifikasi == 0) { ?>
					action += '<button id="batalTransaksi" data-toggle="modal" data-target="#ActionBatalTransaksiModal" class="btn btn-md btn-danger" style="width: 150px;"><i class="fa fa-trash"></i> Batal Transaksi</button>';
				<?php } ?>
			<?php } ?>

			$("#cara_pembayaran").hide();
			$("#transaction-only-action").hide();
			$(".edit").hide();
			$(".hapus").hide();
		}

		$("#actionVerifikasi").html(action);
	}

	function getStatusCheckout(idpendaftaran) {
		$("#status-checkout").html('<i class="fa fa-spinner fa-spin"></i>');

		$.ajax({
			dataType: 'json',
			url: '{site_url}trawatinap_tindakan/statusCheckout/' + idpendaftaran,
			success: function (data) {
				if (data.status == 1) {
					$("#status-checkout").removeClass('badge-danger');
					$("#status-checkout").addClass('badge-success');
					$("#status-checkout").html('Telah Diaktifkan');
				} else {
					$("#status-checkout").removeClass('badge-success');
					$("#status-checkout").addClass('badge-danger');
					$("#status-checkout").html('Belum Diaktifkan');
				}
			}
		});
	}

	function getDokter() {
		$.ajax({
			url: "{site_url}trawatinap_tindakan/getDokterPJ",
			dataType: "json",
			success: function (data) {
				$("#tTindakanIdDokter").empty();
				$("#tTindakanIdDokter").append("<option value=''>Pilih Opsi</option>");
				for (var i = 0; i < data.length; i++) {
					$("#tTindakanIdDokter").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
				}

				$("#tTindakanGlobalIdDokter").empty();
				$("#tTindakanGlobalIdDokter").append("<option value=''>Pilih Opsi</option>");
				for (var i = 0; i < data.length; i++) {
					$("#tTindakanGlobalIdDokter").append("<option value='" + data[i].id + "'>" + data[i].nama + "</option>");
				}

				$("#tVisiteIdDokter").empty();
				$("#tVisiteIdDokter").append("<option value=''>Pilih Opsi</option>");
				for (var i = 0; i < data.length; i++) {
					$("#tVisiteIdDokter").append("<option value='" + data[i].id + "' data-idkategori='" + data[i].idkategori + "'>" + data[i].nama + "</option>");
				}

				$("#fVisiteIdDokter").empty();
				$("#fVisiteIdDokter").append("<option value=''>Pilih Opsi</option>");
				for (var i = 0; i < data.length; i++) {
					$("#fVisiteIdDokter").append("<option value='" + data[i].id + "' data-idkategori='" + data[i].idkategori + "'>" + data[i].nama + "</option>");
				}

				$("#fTindakanIdDokter").empty();
				$("#fTindakanIdDokter").append("<option value=''>Pilih Opsi</option>");
				for (var i = 0; i < data.length; i++) {
					$("#fTindakanIdDokter").append("<option value='" + data[i].id + "' data-idkategori='" + data[i].idkategori + "'>" + data[i].nama + "</option>");
				}
			}
		});
	}

    function initFormInput() {
        $('.number').number(true, 0, '.', ',');
        $('.discount').number(true, 2, '.', ',');
        $(".time-datepicker").datetimepicker({
            format: "HH:mm:ss"
        });
    }
</script>