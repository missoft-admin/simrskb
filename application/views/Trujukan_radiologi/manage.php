<?php echo ErrorSuccess($this->session) ?>
<?php if ($error != '') {
	echo ErrorMessage($error);
} ?>

<div class="block">
    <div class="block-header">
        <?php if (UserAccesForm($user_acces_form, ['1004'])) { ?>
        <ul class="block-options">
            <li>
                <a href="{base_url}trujukan_radiologi/index" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <?}?>
        <h3 class="block-title">{title}</h3>
    </div>

    <div class="block-content">
        <?php echo form_open('trujukan_radiologi/save', 'class="form-horizontal" id="form-work"  onsubmit="return validate_final()"') ?>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <label class="col-md-2" for="tglpendaftaran" style="margin-top: 5px;">Tanggal Rujukan</label>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-6">
                            <input tabindex="2" type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" name="tanggalrujukan" value="{tanggalrujukan}" required>
                        </div>
                        <div class="col-md-6">
                            <input tabindex="3" type="text" class="time-datepicker form-control" name="wakturujukan" value="{wakturujukan}" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <label class="col-md-4">No. Rujukan</label>
                <div class="col-md-8">
                    <label><i>{norujukan}</i></label>
                </div>
            </div>
        </div>
        <br>
        <br>
        <hr>
        <div class="col-sm-6 col-lg-6">
            <div class="block block-rounded block-bordered">
                <div class="block-header">
                    <h3 class="block-title">Data Pasien</h3>
                </div>
                <div class="block-content">
                    <table class="table">
                        <tr>
                            <th width="30%">No. Medrec</th>
                            <td>
                                <div class="col-sm-12">
                                    <input class="form-control input-sm" type="text" value="{nomedrec}" readonly="readonly">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Nama Pasien</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{titlepasien} {namapasien}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Jenis Kelamin</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{jeniskelamin}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Alamat</th>
                            <td>
                                <div class="col-sm-12">
                                    <textarea class="form-control input-sm" readonly="readonly">{alamat}, {provinsi}, {kota}, {kecamatan}, {kelurahan}</textarea>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tanggal Lahir</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{tanggallahir}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Umur</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{umur_tahun} Tahun {umur_bulan} Bulan {umur_hari} Hari">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Asal Pasien</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="<?= GetAsalPasienRujukan($asalrujukan) ?>">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tipe</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="<?= JenisTransaksi($idtipe) ?>">
                                </div>
                            </td>
                        </tr>
                        <?php if ($asalrujukan == 1) { ?>
                        <tr>
                            <th>Poliklinik</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{poliklinik}">
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php if ($asalrujukan == 3) { ?>
                        <tr>
                            <th>Kelas</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{kelas}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Kamar</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{ruangan}">
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <th>Dokter Perujuk</th>
                            <td>
                                <div class="col-sm-12">
                                    <select id="iddokterperujuk" <?=(UserAccesForm($user_acces_form, ['1000']) ? '' : 'disabled')?> name="iddokterperujuk" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                        <option value="#">Pilih Opsi</option>
                                        <?php foreach ($list_dokterperujuk as $row) { ?>
                                        <option value="<?php echo $row->id; ?>" <?=($iddokterperujuk == $row->id ? 'selected' : '')?>><?php echo $row->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-6">
            <div class="block block-rounded block-bordered">
                <div class="block-content">
                    <table class="table">
                        <tr>
                            <th>Kelompok Pasien</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{namakelompok}">
                                </div>
                            </td>
                        </tr>
                        <?php if ($idkelompokpasien == 1) { ?>
                        <tr>
                            <th>Asuransi</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{namarekanan}">
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php if ($idkelompokpasien == 3) { ?>
                        <tr>
                            <th>No. INACBG</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{kodebpjskesehatan} - {tarif_bpjs_kesehatan}">
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php if ($idkelompokpasien == 4) { ?>
                        <tr>
                            <th>No. Paket</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{idtarif_bpjstk} - {tarif_bpjstk}">
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <th>Dokter Radiologi</th>
                            <td>
                                <div class="col-sm-12">
                                    <select id="iddokterradiologi" name="iddokterradiologi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                        <option value="#">Pilih Opsi</option>
                                        <?php foreach ($list_dokterradiologi as $row) { ?>
                                        <option value="<?php echo $row->id; ?>" <?=($iddokterradiologi == $row->id ? 'selected' : '')?>><?php echo $row->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Nomor Foto</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" name="nomorfoto" class="form-control input-sm" readonly value="{nomorfoto}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tanggal Pengambilan</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" name="tanggalpengambilan" class="js-datepicker form-control input-sm" data-date-format="yyyy-mm-dd" value="{tanggalpengambilan}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Jumlah Expose</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" name="jumlahexpose" class="form-control input-sm" value="{jumlahexpose}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Jumlah Film</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" name="jumlahfilm" class="form-control input-sm" value="{jumlahfilm}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>QP</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" name="qp" class="form-control input-sm" value="{qp}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>mAS</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" name="mas" class="form-control input-sm" value="{mas}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Petugas Pemeriksa</th>
                            <td>
                                <div class="col-sm-12">
                                    <select id="idpemeriksa" name="idpemeriksa" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                        <?php foreach ($list_pemeriksa as $row) { ?>
                                        <option value="<?php echo $row->id; ?>" <?=($idpemeriksa == $row->id ? 'selected' : '')?>><?php echo $row->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Posisi</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" name="tindakan" class="form-control input-sm" value="{tindakan}">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="control-group">
                    <div class="col-md-12">
                        <div class="progress progress-mini">
                            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                        </div>
                        <table class="table table-striped table-bordered" id="tindakan-list" style="margin-bottom:0px">
                            <thead>
                                <tr>
                                    <th>Tindakan</th>
                                    <th>Tarif</th>
                                    <th>Kuantitas</th>
                                    <th>Diskon (%)</th>
                                    <th>Jumlah</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($this->uri->segment(2) == 'edit') { ?>
                                <?php foreach ($list_detail as $row) { ?>
                                <tr>
                                    <?php $total = $row->total * $row->kuantitas?>
                                    <td width="10%"><?=$row->namatindakan?></td>
                                    <td width="10%" style="display:none"><?=number_format($row->jasasarana)?></td>
                                    <td width="10%" style="display:none"><?=number_format($row->jasapelayanan)?></td>
                                    <td width="10%" style="display:none"><?=number_format($row->bhp)?></td>
                                    <td width="10%" style="display:none"><?=number_format($row->biayaperawatan)?></td>
                                    <td width="10%"><?=number_format($row->total)?></td>
                                    <td width="10%"><?=number_format($row->kuantitas)?></td>
                                    <td><?= calcDiscountPercent($row->diskon, $row->total); ?></td>
                                    <td width="10%" hidden><?=number_format($row->diskon)?></td>
                                    <td width="10%"><?=number_format($row->totalkeseluruhan)?></td>
                                    <td style="display:none"><?=$row->idradiologi?></td>
                                    <td style="display:none"><?=$row->idtipe?></td>
                                    <td style="display:none"><?=$row->statusverifikasi?></td>
                                    <td width="10%">
                                        <?php if ($statuskasir != 2) { ?>
                                        <?php if ($row->statusverifikasi == 0) { ?>
                                        <div class='btn btn-group'>
                                            <?php if (UserAccesForm($user_acces_form, ['998'])) { ?>
                                            <a href='#' class='btn btn-primary btn-sm tindakan-edit' data-toggle='modal' data-target='#tindakan-modal' id='tindakan-modal-show' data-toggle='tooltip' title='Ubah'><i class='fa fa-pencil'></i></a>
                                            <?}?>
                                            <?php if (UserAccesForm($user_acces_form, ['999'])) { ?>
                                            <a href='#' class='btn btn-danger btn-sm tindakan-delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>
                                    <?}?>
                    </div>
                    <?php } else { ?>
                    <div class='btn btn-group'>
                        <button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>
                    </div>
                    <?php } ?>
                    <?php } else { ?>
                    <div class='btn btn-group'>
                        <button type="button" class="btn btn-sm btn-danger"><i class="fa fa-lock"></i></button>
                    </div>
                    <?php } ?>
                    </td>
                    </tr>
                    <?php } ?>
                    <?php } ?>
                    </tbody>
                    </table>
                    <table class="table table-striped table-bordered">
                        <tbody>
                            <tr>
                                <td colspan="2" class="hidden-phone">
                                    <?php if ($statuskasir != 2 && UserAccesForm($user_acces_form, ['997'])) { ?>
                                    <button data-toggle="modal" data-target="#tindakan-modal" id="tindakan-modal-show" class="btn btn-info btn-xs" type="button">Tambah Tindakan</button>
                                    <?}?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="text-right bg-light lter">
        <button class="btn btn-info" type="submit">Simpan</button>
        <a href="{base_url}trujukan_radiologi/index" class="btn btn-default">Batal</a>
    </div>
    <input type="hidden" id="tindakan-value" name="tindakan-value">

    <input type="hidden" name="tanggal_input_pemeriksaan" value="{tanggal_input_pemeriksaan}">
    <input type="hidden" name="iduser_input_pemeriksaan" value="{iduser_input_pemeriksaan}">
    <br><br>
    <?php echo form_hidden('idkelas', $idkelas); ?>
    <?php echo form_hidden('statuspasien', $statuspasien); ?>
    <?php echo form_hidden('idpendaftaran', $idpendaftaran); ?>
    <?php echo form_hidden('id', $id); ?>
    <?php echo form_close() ?>
</div>
</div>

<!-- Modal Tindakan -->
<div class="modal fade in" id="tindakan-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none; left:30px;">
    <div class="modal-dialog modal-lg modal-dialog-popout" style="width:65%">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-success">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Daftar Tindakan</h3>
                </div>
                <div class="block-content">
                    <div id="tindakan-step-1">
                        <div class="row">
                            <div class="col-md-12 form-horizontal">
                                <div class="form-val">
                                    <label class="col-sm-4" for="idtipe">Tipe</label>
                                    <div class="col-sm-8">
                                        <select name="idtipe" id="idtipe" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                            <option value="1">X-Ray</option>
                                            <option value="2">USG</option>
                                            <option value="3">CT Scan</option>
                                            <option value="4">MRI</option>
                                            <option value="5">BMD</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-val" style="margin-top:50px" id="form_expose">
                                    <label class="col-sm-4" for="idexpose">Expose</label>
                                    <div class="col-sm-8">
                                        <select name="idexpose" id="idexpose" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                            <option value="0">Semua Expose</option>
                                            <?php foreach ($list_expose as $row) { ?>
                                            <option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-val" style="margin-top:100px" id="form_film">
                                    <label class="col-sm-4" for="idfilm">Film</label>
                                    <div class="col-sm-8">
                                        <select name="idexpose" id="idfilm" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                            <option value="0">Semua Film</option>
                                            <?php foreach ($list_film as $row) { ?>
                                            <option value="<?php echo $row->id; ?>"><?php echo $row->nama; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <table width="100%" class="table table-striped table-striped table-hover table-bordered" id="tindakan-table-step-1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Jasa Sarana</th>
                                    <th>Jasa Pelayanan</th>
                                    <th>BHP</th>
                                    <th>Biaya Perawatan</th>
                                    <th>Total Tarif</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div style="display: none;" id="tindakan-step-2">
                        <input type="hidden" class="form-control" id="rowindex">
                        <table class="table table-striped table-bordered table-hover" id="tindakan-table-step-2">
                            <thead>
                                <tr>
                                    <th>Tindakan</th>
                                    <th>Tarif</th>
                                    <th>Kuantitas</th>
                                    <th>Diskon (%)</th>
                                    <th>Diskon (Rp)</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="tindakan-input">
                                    <td id="namatarif"></td>
                                    <td style="display: none" id="jasasarana"></td>
                                    <td style="display: none" id="jasapelayanan"></td>
                                    <td style="display: none" id="bhp"></td>
                                    <td style="display: none" id="biayaperawatan"></td>
                                    <td id="totaltarif"></td>
                                    <td><input class="form-control input-sm number" type="text" id="kuantitas" value="1" autocomplete="off" /></td>
                                    <td><input class="form-control input-sm number-diskon" type="text" id="diskon" value="0" autocomplete="off" /></td>
                                    <td><input class="form-control input-sm number" type="text" id="diskonRp" value="0" autocomplete="off" /></td>
                                    <td><input class="form-control input-sm number" type="text" id="totalkeseluruhan" value="" autocomplete="off" readonly /></td>
                                    <td style="display: none" id="idtarif"></td>
                                    <td style="display: none" id="idtiperadiologi"></td>
                                    <td style="display: none" id="statusverifikasi"></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="modal-footer">
                            <button class="btn btn-sm btn-default" id="tindakan-back" type="button">Batalkan</button>
                            <button class="btn btn-sm btn-primary" type="button" id="tindakan-add">Tambahkan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
// Initialize when page loads
jQuery(function() {
    BaseTableDatatables.init();
    var idkelas = '<?=$idkelas?>';
    var idkelompokpasien = '<?=$idkelompokpasien?>';
    var idrekanan = '<?=($idrekanan ? $idrekanan : 0)?>';
    var idtipe = '1';
    var idexpose = '0';
    var idfilm = '0';

    filterTarif(idkelas, idkelompokpasien, idrekanan, idtipe, idexpose, idfilm);
});

$(document).ready(function() {
    $(".time-datepicker").datetimepicker({
        format: "HH:mm:ss"
    });

    $(".number").number(true, 0, '.', ',');
    $(".number-diskon").number(true, 2, '.', ',');

    $("#idtipe").change(function() {
        var idkelas = '<?=$idkelas?>';
        var idkelompokpasien = '<?=$idkelompokpasien?>';
        var idrekanan = '<?=($idrekanan ? $idrekanan : 0)?>';
        var idtipe = $(this).val();
        var idexpose = $("#idtipe option:selected").val();
        var idfilm = $("#idfilm option:selected").val();

        if (idtipe == 1) {
            $("#form_expose").show();
            $("#form_film").show();
        } else {
            $("#form_expose").hide();
            $("#form_film").hide();
        }

        filterTarif(idkelas, idkelompokpasien, idrekanan, idtipe, idexpose, idfilm);
    });

    $("#idexpose").change(function() {
        var idkelas = '<?=$idkelas?>';
        var idkelompokpasien = '<?=$idkelompokpasien?>';
        var idrekanan = '<?=($idrekanan ? $idrekanan : 0)?>';
        var idtipe = $("#idtipe option:selected").val();
        var idexpose = $(this).val();
        var idfilm = $("#idfilm option:selected").val();

        filterTarif(idkelas, idkelompokpasien, idrekanan, idtipe, idexpose, idfilm);
    });

    $("#idfilm").change(function() {
        var idkelas = '<?=$idkelas?>';
        var idkelompokpasien = '<?=$idkelompokpasien?>';
        var idrekanan = '<?=($idrekanan ? $idrekanan : 0)?>';
        var idtipe = $("#idtipe option:selected").val();
        var idexpose = $("#idexpose option:selected").val();
        var idfilm = $(this).val();

        filterTarif(idkelas, idkelompokpasien, idrekanan, idtipe, idexpose, idfilm);
    });

    $(document).on("keyup", "#kuantitas", function() {
        var totaltarif = $("#totaltarif").text().replace(/,/g, '');
        var kuantitas = $(this).val().replace(/,/g, '');
        var subtotal = parseFloat(totaltarif) * parseFloat(kuantitas);

        var diskonRupiah = parseFloat(subtotal * parseFloat($('#diskon').val()) / 100);
        $("#diskonRp").val(diskonRupiah);

        var totalkeseluruhan = parseFloat(subtotal) - parseFloat(diskonRupiah);
        $("#totalkeseluruhan").val(totalkeseluruhan);
    });

    $(document).on("keyup", "#diskon", function() {
        var totaltarif = $("#totaltarif").html().replace(/,/g, '');
        var kuantitas = $("#kuantitas").val().replace(/,/g, '');
        var subtotal = (parseFloat(totaltarif) * parseFloat(kuantitas));

        if ($("#diskon").val() == '') {
            $("#diskon").val(0)
        }
        if (parseFloat($(this).val()) > 100) {
            $(this).val(100);
        }

        var diskonRupiah = parseFloat(subtotal * parseFloat($(this).val()) / 100);
        $("#diskonRp").val(diskonRupiah);
        
        var totalkeseluruhan = parseFloat(subtotal) - parseFloat(diskonRupiah);
        $("#totalkeseluruhan").val(totalkeseluruhan);
    });

    $(document).on("keyup", "#diskonRp", function() {
        var totaltarif = $("#totaltarif").html().replace(/,/g, '');
        var kuantitas = $("#kuantitas").val().replace(/,/g, '');
        var subtotal = (parseFloat(totaltarif) * parseFloat(kuantitas));

        if ($("#diskonRp").val() == '') {
            $("#diskonRp").val(0)
        }
        if (parseFloat($(this).val()) > subtotal) {
            $(this).val(subtotal);
        }

        var diskonPersen = parseFloat((parseFloat($(this).val() * 100)) / subtotal);
        $("#diskon").val(diskonPersen);

        var totalkeseluruhan = parseFloat(subtotal) - parseFloat($(this).val());
        $("#totalkeseluruhan").val(totalkeseluruhan);
    });

    $("#tindakan-modal-show").click(function() {
        $('#tindakan-step-1').fadeIn('slow');
        $('#tindakan-step-2').hide();
    })

    $("#tindakan-back").click(function() {
        $('#tindakan-step-1').fadeIn('slow');
        $('#tindakan-step-2').fadeOut();
        $("#kuantitas").val('');
        $("#diskon").val('');
        $("#diskonRp").val('');
    })

    $(document).on("click", "#tindakan-select", function() {
        $('#tindakan-step-1').fadeOut();
        $('#tindakan-step-2').fadeIn('slow');
        $('table#tindakan-table-step-2 tbody').empty();

        content = '<tr id="tindakan-input">';
        content += '<td id="namatarif">' + $(this).closest('tr').find("td:eq(1) a").html() + '</td>';
        content += '<td style="display: none" id="jasasarana">' + $(this).closest('tr').find("td:eq(2)").html() + '</td>';
        content += '<td style="display: none" id="jasapelayanan">' + $(this).closest('tr').find("td:eq(3)").html() + '</td>';
        content += '<td style="display: none" id="bhp">' + $(this).closest('tr').find("td:eq(4)").html() + '</td>';
        content += '<td style="display: none" id="biayaperawatan">' + $(this).closest('tr').find("td:eq(5)").html() + '</td>';
        content += '<td id="totaltarif">' + $(this).closest('tr').find("td:eq(6)").html() + '</td>';
        content += '<td><input class="form-control input-sm number" type="text" id="kuantitas" value="1" autocomplete="off"/></td>';
        content += '<td><input class="form-control input-sm number-diskon" type="text" id="diskon" value="0" autocomplete="off"/></td>';
        content += '<td><input class="form-control input-sm number" type="text" id="diskonRp" value="0" autocomplete="off"/></td>';
        content += '<td><input class="form-control input-sm number" type="text" id="totalkeseluruhan" value="' + $(this).closest('tr').find("td:eq(6)").html() + '" autocomplete="off" readonly/></td>';
        content += '<td style="display: none" id="idtarif">' + $(this).data('idtindakan') + '</td>';
        content += '<td style="display: none" id="idtiperadiologi">' + $(this).data('idtiperadiologi') + '</td>';
        content += '<td style="display: none" id="statusverifikasi">0</td>';
        content += '</tr>';

        $("table#tindakan-table-step-2 tbody").html(content);

        $(".number").number(true, 0, '.', ',');
        $(".number-diskon").number(true, 2, '.', ',');

        $("#kuantitas").focus();
    });

    $(document).on("click", "#tindakan-add", function() {
        var valid = validateDetail();
        if (!valid) return false;

        var rowindex;
        var duplicate = false;

        if ($("#rowindex").val() != '') {
            var content = "";
            rowindex = $("#rowindex").val();
        } else {
            var content = "<tr>";
            $('#tindakan-list tbody tr').filter(function() {
                var $cells = $(this).children('td');
                if ($cells.eq(0).text() === $("#namatarif").text()) {
                    sweetAlert("Maaf...", "Tindakan " + $("#namatarif").text() + " sudah ditambahkan.", "error");
                    duplicate = true;
                    return false;
                }
            });
        }

        if (duplicate == false) {
            content += "<td width='10%'>" + $("#namatarif").text(); + "</td>";
            content += "<td style='display: none' width='10%'>" + $("#jasasarana").text(); + "</td>";
            content += "<td style='display: none' width='10%'>" + $("#jasapelayanan").text(); + "</td>";
            content += "<td style='display: none' width='10%'>" + $("#bhp").text(); + "</td>";
            content += "<td style='display: none' width='10%'>" + $("#biayaperawatan").text(); + "</td>";
            content += "<td width='10%'>" + $("#totaltarif").text(); + "</td>";
            content += "<td width='10%'>" + $("#kuantitas").val(); + "</td>";
            content += "<td width='10%'>" + $("#diskon").val(); + "</td>";
            content += "<td width='10%' hidden>" + $("#diskonRp").val(); + "</td>";
            content += "<td width='10%'>" + $.number($("#totalkeseluruhan").val()); + "</td>";
            content += "<td style='display:none'>" + $("#idtarif").text(); + "</td>";
            content += "<td style='display:none'>" + $("#idtiperadiologi").text(); + "</td>";
            content += "<td style='display:none'>" + $("#statusverifikasi").text(); + "</td>";
            content += "<td width='10%'>";
            content += "<div class='btn btn-group'>";
            content += "<a href='#' class='btn btn-primary btn-sm tindakan-edit' data-toggle='modal' data-target='#tindakan-modal' id='tindakan-modal-show' data-toggle='tooltip' title='Ubah'><i class='fa fa-pencil'></i></a>";
            content += "<a href='#' class='btn btn-danger btn-sm tindakan-delete' data-toggle='tooltip' title='Hapus'><i class='fa fa-trash-o'></i></a></td>";
            content += "</div>";
            content += "</td>";

            if ($("#rowindex").val() != '') {
                $('#tindakan-list tbody tr:eq(' + rowindex + ')').html(content);
            } else {
                content += "</tr>";
                $('#tindakan-list tbody').append(content);
            }

            $("#tindakan-back").click();
            detailClear();

            swal({
                type: 'success',
                title: 'Berhasil!',
                text: 'Tindakan telah berhasil ditambahkan.',
                timer: 1000,
                showCancelButton: false,
                showConfirmButton: false
            });
        }
    });

    $(document).on("click", ".tindakan-edit", function() {
        $('#tindakan-step-1').fadeOut();
        $('#tindakan-step-2').fadeIn('slow');

        $("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);

        $("#namatarif").text($(this).closest('tr').find("td:eq(0)").html());
        $("#jasasarana").text($(this).closest('tr').find("td:eq(1)").html());
        $("#jasapelayanan").text($(this).closest('tr').find("td:eq(2)").html());
        $("#bhp").text($(this).closest('tr').find("td:eq(3)").html());
        $("#biayaperawatan").text($(this).closest('tr').find("td:eq(4)").html());
        $("#totaltarif").text($(this).closest('tr').find("td:eq(5)").html());
        $("#kuantitas").val($(this).closest('tr').find("td:eq(6)").html());
        $("#diskon").val($(this).closest('tr').find("td:eq(7)").html());
        $("#diskonRp").val($(this).closest('tr').find("td:eq(8)").html());
        $("#totalkeseluruhan").val($(this).closest('tr').find("td:eq(9)").html());
        $("#idtarif").text($(this).closest('tr').find("td:eq(10)").html());
        $("#idtiperadiologi").text($(this).closest('tr').find("td:eq(11)").html());
        $("#statusverifikasi").text($(this).closest('tr').find("td:eq(12)").html());

        return false;
    });

    $(document).on("click", ".tindakan-delete", function() {
        var row = $(this).closest('td').parent();
        swal({
            title: '',
            text: "Apakah anda yakin akan menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false,
            closeOnCancel: true
        }).then(function() {
            return row.remove();
        })
        return false;
    });

    $("#form-work").submit(function(e) {
        var form = this;

        if ($("#iddokterradiologi").val() == "#") {
            e.preventDefault();
            sweetAlert("Maaf...", "Dokter Radiologi Belum Dipilih!", "error");
            return false;
        }

        var tindakan_tbl = $('table#tindakan-list tbody tr').get().map(function(row) {
            return $(row).find('td').get().map(function(cell) {
                return $(cell).html();
            });
        });

        $("#tindakan-value").val(JSON.stringify(tindakan_tbl));

        swal({
            title: "Berhasil!",
            text: "Proses penyimpanan data.",
            type: "success",
            timer: 1500,
            showConfirmButton: false
        });
    });
});

function detailClear() {
    $("#rowindex").val('');
    $(".detail").val('');
}

function validateDetail() {
    var kuantitas = $("#kuantitas").val();
    if (kuantitas == "" || kuantitas == "0") {
        sweetAlert("Maaf...", "Kuantitas tidak boleh kosong!", "error");
        return false;
    }

    return true;
}

function validate_final() {
    $("*[disabled]").not(true).removeAttr("disabled");
    return true;
}

function filterTarif(idkelas, idkelompokpasien, idrekanan, idtipe, idexpose, idfilm) {
    $('#tindakan-table-step-1').DataTable().destroy();
    $('#tindakan-table-step-1').DataTable({
        "bSort": false,
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}trujukan_radiologi/getTindakan/' + idkelas + '/' + idkelompokpasien + '/' + idrekanan + '/' + idtipe + '/' + idexpose + '/' + idfilm,
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "orderable": true
            },
            {
                "width": "20%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 2,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 3,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 4,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 5,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 6,
                "orderable": true
            }
        ]
    });
}

</script>
