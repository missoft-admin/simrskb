<?php echo ErrorSuccess($this->session) ?>
<?php if ($error != '') {
	echo ErrorMessage($error);
} ?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}trujukan_radiologi/index" class="btn">
					<i class="fa fa-reply"></i>
				</a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?php echo form_open('trujukan_radiologi/save_review', 'class="form-horizontal" id="form-work"') ?>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <label class="col-md-2" for="tglpendaftaran" style="margin-top: 5px;">Tanggal Expertise</label>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-6">
                            <input tabindex="2" type="text" class="js-datepicker form-control" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TTTT" name="tanggalexpertise" value="{tanggalexpertise}" required>
                        </div>
                        <div class="col-md-6">
                            <input tabindex="3" type="text" class="time-datepicker form-control" name="waktuexpertise" value="{waktuexpertise}" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <label class="col-md-4">No. Rujukan</label>
                <div class="col-md-8">
                    <label><i>{norujukan}</i></label>
                </div>
            </div>
        </div>
        <br>
        <br>
        <hr>
        <div class="col-sm-6 col-lg-6">
            <div class="block block-rounded block-bordered">
                <div class="block-header">
                    <h3 class="block-title">Data Pasien</h3>
                </div>
                <div class="block-content">
                    <table class="table">
                        <tr>
                            <th width="30%">No. Medrec</th>
                            <td>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="text" value="{nomedrec}" readonly="readonly">
                                </div>
                                <div class="col-sm-4">
                                    <a class="btn btn-warning" href="{base_url}tpoliklinik_pendaftaran/print_document/{idpoliklinik}/3" target="_blank">Kartu Status</a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Nama Pasien</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{namapasien}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Jenis Kelamin</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{jeniskelamin}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Alamat</th>
                            <td>
                                <div class="col-sm-12">
                                    <textarea class="form-control input-sm" readonly="readonly">{alamat}</textarea>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tanggal Lahir</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{tanggallahir}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Umur</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{umur_tahun} Tahun {umur_bulan} Bulan {umur_hari} Hari-">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Dokter Perujuk</th>
                            <td>
                                <div class="col-sm-12">
                                    <select id="iddokterperujuk" name="iddokterperujuk" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                        <option value="#">Pilih Opsi</option>
                                        <?php foreach ($list_dokterperujuk as $row) { ?>
                                        <option value="<?php echo $row->id; ?>" <?=($iddokterperujuk == $row->id ? 'selected' : '')?>><?php echo $row->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-6">
            <div class="block block-rounded block-bordered">
                <div class="block-content">
                    <table class="table">
                        <tr>
                            <th>Kelompok Pasien</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{namakelompok}">
                                </div>
                            </td>
                        </tr>
                        <?php if ($idkelompokpasien == 1) { ?>
                        <tr>
                            <th>Asuransi</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{namarekanan}">
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php if ($idkelompokpasien == 3) { ?>
                        <tr>
                            <th>No. INACBG</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{kodebpjskesehatan} - {tarif_bpjs_kesehatan}">
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php if ($idkelompokpasien == 4) { ?>
                        <tr>
                            <th>No. Paket</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{idtarif_bpjstk} - {tarif_bpjstk}">
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <th>Dokter Radiologi</th>
                            <td>
                                <div class="col-sm-12">
                                    <select id="iddokterradiologi" name="iddokterradiologi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                                        <option value="#">Pilih Opsi</option>
                                        <?php foreach ($list_dokterradiologi as $row) { ?>
                                        <option value="<?php echo $row->id; ?>" <?=($iddokterradiologi == $row->id ? 'selected' : '')?>><?php echo $row->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Nomor Foto</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" id="nomorfoto" readonly name="nomorfoto" value="{nomorfoto}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tanggal Ambil</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" id="tanggalambil" readonly name="tanggalambil" value="{tanggalpengambilan}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Asal Pasien</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="<?= GetAsalPasienRujukan($asalrujukan) ?>">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Tipe</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="<?= JenisTransaksi($idtipe) ?>">
                                </div>
                            </td>
                        </tr>
                        <?php if ($asalrujukan == 1) { ?>
                        <tr>
                            <th>Poliklinik</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{poliklinik}">
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php if ($asalrujukan == 3) { ?>
                        <tr>
                            <th>Kelas</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{kelas}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Kamar</th>
                            <td>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" readonly="readonly" value="{ruangan}">
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="control-group">
                    <div class="col-md-12">
                        <div class="progress progress-mini">
                            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                        </div>
                        <?php foreach ($list_tindakan as $row) { ?>
                        <table class="table table-striped table-bordered" id="detail_list" style="margin-bottom:0px">
                            <tbody>
                                <tr>
                                    <th>Tipe Tindakan</th>
                                    <th>
                                        <?=GetTipeRadiologi($row->idtipe)?>
                                        <input type="text" id="idtipe" name="idtipe[]" value="<?= $row->idtipe ?>" hidden>
                                        <input type="hidden" id="tanggal_expertise" name="tanggal_expertise[]" value="<?= $row->tanggal_expertise ?>">
                                        <input type="hidden" id="iduser_expertise" name="iduser_expertise[]" value="<?= $row->iduser_expertise ?>">
                                    </th>
                                </tr>
                                <tr>
                                    <th>Nama Tindakan</th>
                                    <th><?=$row->namatindakan?></th>
                                </tr>
                                <tr>
                                    <th>Posisi</th>
                                    <th>
                                        <textarea class="form-control js-summernote-custom-height" id="tindakanyangdilakukan" placeholder="Posisi" name="tindakanyangdilakukan[]" rows="1"><?=($row->tindakanyangdilakukan ? $row->tindakanyangdilakukan : $tindakan) ?></textarea>
                                    </th>
                                </tr>
                                <tr id="form-hasilbacafoto">
                                    <th>Hasil Baca Foto</th>
                                    <th>
                                        <textarea class="form-control js-summernote" id="hasilbacafoto" placeholder="Hasil Baca Foto" name="hasilbacafoto[]" rows="5"><?=$row->hasilbacafoto?></textarea>
                                    </th>
                                </tr>
                            </tbody>
                        </table><br>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="text-right bg-light lter">
            <button class="btn btn-info" type="submit">Simpan</button>
            <a href="{base_url}trujukan_radiologi/index" class="btn btn-default">Batal</a>
        </div>
        <br>
        <br>
        <?php echo form_hidden('idkelas', $idkelas); ?>
        <?php echo form_hidden('statuspasien', $this->uri->segment(4)); ?>
        <?php echo form_hidden('id', $id); ?>
        <?php echo form_close() ?>
    </div>
</div>

<style media="screen">
#form-hasilbacafoto th {
    text-transform: inherit;
}

</style>

<script type="text/javascript">
$(document).ready(function() {
    $(".time-datepicker").datetimepicker({
        format: "HH:mm:ss"
    });
});

</script>
