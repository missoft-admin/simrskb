<style media="screen">
.toggle-column {
    cursor: pointer;
}

.toggle-active {
    color: red;
}

</style>

<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('994'))){ ?>
<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <hr style="margin-top:0px">
        <div class="row">
            <?php echo form_open('trujukan_radiologi/filter','class="form-horizontal" id="form-work"') ?>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="nomedrec">No. Medrec</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="nomedrec" value="{nomedrec}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="namapasien">Nama Pasien</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="namapasien" value="{namapasien}">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="idasalpasien">Asal Pasien</label>
                    <div class="col-md-8">
                        <select name="idasalpasien" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
                            <option value="0" selected>Semua Asal Pasien</option>
                            <option value="1" <?=($idasalpasien == '1' ? 'selected' : '')?>>Rawat Jalan</option>
                            <option value="2" <?=($idasalpasien == '2' ? 'selected' : '')?>>Instalasi Gawat Darurat</option>
                            <option value="3" <?=($idasalpasien == '3' ? 'selected' : '')?>>Rawat Inap</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="idtindakan">Tindakan</label>
                    <div class="col-md-8">
                        <select name="idtindakan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
                            <option value="0" selected>Semua Tindakan</option>
                            <?php foreach  ($list_tindakan as $row) { ?>
                            <option value="<?=$row->id;?>" <?=($idtindakan==$row->id) ? "selected" : "" ?>><?=TreeView($row->level, $row->nama);?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="iddokterperujuk">Dokter Perujuk</label>
                    <div class="col-md-8">
                        <select id="iddokterperujuk" name="iddokterperujuk" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="0">Semua Dokter Perujuk</option>
                            <?php foreach ($list_dokterperujuk as $row) { ?>
                            <option value="<?php echo $row->id; ?>" <?=($iddokterperujuk == $row->id ? 'selected' : '')?>><?php echo $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="iddokterradiologi">Dokter Radiologi</label>
                    <div class="col-md-8">
                        <select id="iddokterradiologi" name="iddokterradiologi" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="0">Semua Dokter Radiologi</option>
                            <?php foreach ($list_dokterradiologi as $row) { ?>
                            <option value="<?php echo $row->id; ?>" <?=($iddokterradiologi == $row->id ? 'selected' : '')?>><?php echo $row->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggalrujukan">Tanggal Rujukan</label>
                    <div class="col-md-8">
                        <div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" name="tanggaldari" placeholder="From" value="{tanggaldari}">
                            <span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
                            <input class="form-control" type="text" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <?php if (UserAccesForm($user_acces_form,array('995'))){ ?>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                    <?}?>
                </div>
            </div>
            <?php echo form_close() ?>
        </div>

        <hr style="margin-top:10px">

        <div class="toggle-column">
            Toggle Column:
            <a class="toggle-vis" data-column="1">Tanggal</a> -
            <a class="toggle-vis" data-column="2">No. Antrian</a> -
            <a class="toggle-vis" data-column="3">No. Rujukan</a> -
            <a class="toggle-vis" data-column="4">No. Pendaftaran</a> -
            <a class="toggle-vis" data-column="5">No. Foto</a> -
            <a class="toggle-vis" data-column="6">No. Medrec</a> -
            <a class="toggle-vis" data-column="7">Nama</a> -
            <a class="toggle-vis" data-column="8">Dokter Perujuk</a> -
            <a class="toggle-vis" data-column="9">Dokter Radiologi</a> -
            <a class="toggle-vis" data-column="10">Asal Pasien</a> -
            <a class="toggle-vis" data-column="11">Status</a>
        </div>

        <hr>
        <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
        <table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>No. Antrian</th>
                    <th>No. Rujukan</th>
                    <th>No. Pendaftaran</th>
                    <th>No. Foto</th>
                    <th>No. Medrec</th>
                    <th>Nama</th>
                    <th>Dokter Perujuk</th>
                    <th>Dokter Radiologi</th>
                    <th>Asal Pasien</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    </div>
</div>
<?}?>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
// Initialize when page loads
jQuery(function() {
    BaseTableDatatables.init();
    var table = $('#datatable-simrs').DataTable({
        "autoWidth": false,
        "pageLength": 10,
        "ordering": true,
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            url: '{site_url}trujukan_radiologi/getIndex/' + '<?=$this->uri->segment(2)?>',
            type: "POST",
            dataType: 'json'
        },
        "columnDefs": [{
                "width": "5%",
                "targets": 0,
                "visible": false,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 1,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 2,
                "visible": false,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 3,
                "visible": false,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 4,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 5,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 6,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 7,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 8,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 9,
                "orderable": true
            },
            {
                "width": "10%",
                "targets": 10,
                "orderable": true
            },
            {
                "width": "5%",
                "targets": 11,
                "orderable": true
            },
            {
                "width": "15%",
                "targets": 12,
                "orderable": true
            }
        ]
    });

    $('a.toggle-vis').on('click', function(e) {
        e.preventDefault();

        // Get the column API object
        var column = table.column($(this).attr('data-column'));

        // Toggle the visibility
        column.visible(!column.visible());

        if ($(this).hasClass("toggle-active")) {
            $(this).removeClass("toggle-active");
        } else {
            $(this).removeClass("toggle-active");
            $(this).addClass("toggle-active");
        }
    });
});

</script>
