<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>

<div class="block">
    <div class="block-header">
        <h3 class="block-title">{title}</h3>
    </div>
    <?php echo form_open_multipart('trujukan_radiologi/print_label', 'class="form-horizontal" id="form-work"') ?>
    <div class="block-content">
        <hr style="margin-top:0px">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="nomedrec">Start Label Ke -</label>
                    <div class="col-md-4">
                        <select id="start_awal" name="start_awal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one.." required>
                            <?php for ($i=1; $i <= 2; $i++) {?>
                            <option value="<?=$i?>" <?=($start_awal == $i ? 'selected' : '')?>><?=$i?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <input type="hidden" id="counter" name="counter" value="0">
                <input type="hidden" id="sisa" name="sisa" value="10">
                <input type="hidden" name="idrujukan" value="{idrujukan}">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-4">
                        <button class="btn btn-success text-uppercase" disabled type="submit" id="button" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-print"></i> Cetak E-Ticket</button>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>

    <hr>

    <?php $no=0;?>
    <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
	<div class="table-responsive">
    <table class="table table-bordered table-striped table-responsive" id="datatable-tindakan">
        <thead>
            <tr>
                <th class="text-center" style="width: 5%;">
                    <label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
                        X
                    </label>
                </th>
                <th style="width: 10%;">Jenis Pemeriksaan</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($list_tindakan as $row) { ?>
            <?php $no +=1; ?>
            <tr>
                <input type="hidden" id="chck_tindakan[<?=$no?>]" name="chck_tindakan[<?=$no?>]" value="0">
                <input type="hidden" id="idtindakan[<?=$no?>]" name="idtindakan[<?=$no?>]" value="<?=$row->idtipe?>">
                <td class="text-center">
                    <label class="css-input css-checkbox css-checkbox-primary">
                        <input type="checkbox" class="chck" id="chck_tindakan[<?=$no?>]" name="chck_tindakan[<?=$no?>]" value="1"><span></span>
                    </label>
                </td>
                <td><?=GetTipeRadiologi($row->idtipe)?></td>
            </tr>
            <?php
            }?>
        </tbody>
    </table>
</div>
    <?php echo form_close() ?>
</div>


<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('input[type="checkbox"]').change(function() {
        var checkedValue = $('input:checkbox:checked').map(function() {
            return this.value;
        }).get();
        $("#counter").val(checkedValue.length);
        hitung_sisa();
    })
});

$("#start_awal").change(function() {
    hitung_sisa();
});

function hitung_sisa() {
    var total = 2;
    total = total - parseFloat($("#start_awal").val()) - parseFloat($("#counter").val()) + 1;
    $("#sisa").val(total);
    if ($("#sisa").val() < 0) {
        $("#button").attr('disabled', true);
        swal({
            title: 'Warning!',
            text: 'Print Tidak bisa dilanjutkan!. Karena Label Tidak akan mencukupi',
            type: 'error',
            timer: 2000,
            showCancelButton: false,
            showConfirmButton: false
        });

    } else {
        if (parseFloat($("#counter").val()) == 0) {
            $("#button").attr('disabled', true);
        } else {
            $("#button").attr('disabled', false);
        }
    }
}

</script>
