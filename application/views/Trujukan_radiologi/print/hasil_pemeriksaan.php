<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Hasil Pemeriksaan Laboratorium</title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      * {
        font-size: 14px !important;
      }
      table {
        font-size: 9px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px dotted #000 !important;
      }
      .border-bottom {
        border-bottom:1px dotted #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px dotted #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px dotted #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      * {
        font-size: 14px !important;
      }
      table {
        font-size: 9px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px dotted #000 !important;
      }
      .border-bottom {
        border-bottom:1px dotted #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px dotted #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px dotted #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <table class="content">
      <tr>
        <td style="text-align:center">
          <img src="<?= base_url() ?>assets/upload/logo/logo.png" width="100px"><br>
          &nbsp;Jl. LLRE. Martadinata No.28 Bandung<br>
          &nbsp;T. (022) 4206717 F. (022) 4216436
        </td>
      </tr>
    </table>
    <br>
    <table class="content-2">
      <tr>
        <td style="width:100px">Tgl Pemeriksaan</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$tanggal_input_pemeriksaan;?></td>

        <td style="width:100px">No. Medrec</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$nomedrec;?></td>
      </tr>
      <tr>
        <td style="width:100px">No. Register</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$norujukan;?></td>

        <td style="width:100px">Kelompok Pasien</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$namakelompok;?></td>
      </tr>
      <tr>
        <td style="width:100px">Nama</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$namapasien;?></td>

        <td style="width:100px">Tanggal Lahir</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=date("Y-m-d", strtotime($tanggallahir));?></td>
      </tr>
      <tr>
        <td style="width:100px">Alamat</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$alamat;?></td>

        <td style="width:100px">Umur</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$umur_tahun;?>.Th</td>
      </tr>
      <tr>
        <td style="width:100px">Dokter</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$namadokterperujuk;?></td>

        <td style="width:100px">Rujukan</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=GetAsalRujukan($asalrujukan);?></td>
      </tr>
      <tr>
        <td style="width:100px">Nomor Foto</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$nomorfoto;?></td>

        <td style="width:100px"></td>
        <td class="text-center" style="width:20px"></td>
        <td></td>
      </tr>
    </table>
    <table>
      <?php $number = 1; ?>
      <?php $idtipe = 0; ?>
      <?php foreach  ($detail_rujukan as $row){ ?>
      <?php if($idtipe != $row->idtipe){ ?>
      <br>
      <tr>
        <td class="border-full" colspan="3"><b>JENIS PEMERIKSAAN <?=strtoupper(GetTipeRadiologi($row->idtipe))?></b></td>
      </tr>
      <?php } ?>
      <tr>
        <td class="border-full" colspan="3"><?=$row->namatindakan ?></td>
      </tr>
      <tr>
        <td class="border-full" colspan="3"><b>POSISI <?=strtoupper(GetTipeRadiologi($row->idtipe))?></b></td>
      </tr>
      <tr>
        <td class="border-full" colspan="3"><?=$row->tindakanyangdilakukan ?></td>
      </tr>
      <tr>
        <td class="border-full" colspan="3"><b>HASIL BACA FOTO <?=strtoupper(GetTipeRadiologi($row->idtipe))?></b></td>
      </tr>
      <tr>
        <td class="border-full" colspan="3"><?=$row->hasilbacafoto ?></td>
      </tr>
      <?php $idtipe = $row->idtipe; ?>
      <?php } ?>
    </table>
    <br>
    <table>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width:40%" class="text-center">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width:40%" class="text-center">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width:40%" class="text-center">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width:40%" class="text-center">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width:40%" class="text-center">( <?=$namadokterradiologi?> )</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width:40%" class="text-center"><?=$nipdokterradiologi?></td>
      </tr>
    </table>
  </body>
</html>
