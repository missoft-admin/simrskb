<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Hasil Pemeriksaan Radiologi</title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      * {
        font-size: 9px !important;
      }
      table {
        font-size: 9px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px dashed #000 !important;
      }
      .border-bottom {
        border-bottom:1px dashed #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px dashed #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px dashed #000 !important;
      }

      .border-dashed{
        border-width: 1px;
        border-bottom-style: dashed;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      * {
        font-size: 9px !important;
      }
      table {
        font-size: 9px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px dashed #000 !important;
      }
      .border-bottom {
        border-bottom:1px dashed #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px dashed #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px dashed #000 !important;
      }

      .border-dashed{
        border-width: 1px;
        border-bottom-style: dashed;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <table class="content">
      <tr>
        <td style="text-align:center">
          &nbsp;<b>RSKB HALMAHERA SIAGA</b><br>
          &nbsp;Jl. LLRE. Martadinata No.28 Bandung<br>
          &nbsp;T. (022) 4206717 F. (022) 4216436
        </td>
      </tr>
    </table>
    <br>
    <table class="content-2">
      <tr>
        <td style="width:100px">No. Register</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$norujukan; ?></td>

        <td style="width:100px">No. Medrec</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$nomedrec; ?></td>
      </tr>
      <tr>
        <td style="width:100px">Nama</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$namapasien; ?></td>

        <td style="width:100px">Kelompok Pasien</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$namakelompok; ?></td>
      </tr>
      <tr>
        <td style="width:100px">Alamat</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$alamat; ?></td>

        <td style="width:100px">Tanggal Lahir</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=date('Y-m-d', strtotime($tanggallahir)); ?></td>
      </tr>
      <tr>
        <td style="width:100px">Dokter</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$namadokterradiologi; ?></td>

        <td style="width:100px">Umur</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$umur_tahun; ?>.Th</td>
      </tr>
      <tr>
        <td style="width:100px">Nomor Foto</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$nomorfoto; ?></td>

        <td style="width:100px">Rujukan</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=GetAsalRujukan($asalrujukan); ?></td>
      </tr>
    </table>
    <p class="text-center "><b>PEMERIKSAAN RADIOLOGI</b></p>
    <table>
      <tr>
        <td class="text-center text-bold border-full">NO.</td>
        <td class="text-center text-bold border-full">PEMERIKSAAN</td>
        <td class="text-center text-bold border-full">TARIF</td>
        <td class="text-center text-bold border-full">KUANTITAS</td>
        <td class="text-center text-bold border-full">TOTAL</td>
      </tr>
      <?php $number = 1; ?>
      <?php $idtipe = 0; ?>
      <?php $grandtotal = 0; ?>
      <?php foreach ($detail_rujukan as $row) { ?>
      <?php if ($idtipe != $row->idtipe) { ?>
      <?php $number = 1; ?>
      <tr>
        <td class="border-full" colspan="5"><b><?=strtoupper(GetTipeRadiologi($row->idtipe))?></b></td>
      </tr>
      <?php } ?>
      <?php $subtotal = $row->total * $row->kuantitas; ?>
      <tr>
        <td class="text-center border-full"><?=$number++ ?></td>
        <td class="border-full"><?=$row->namatindakan ?></td>
        <td class="text-right border-full"><?=number_format($row->total)?></td>
        <td class="text-right border-full"><?=number_format($row->kuantitas)?></td>
        <td class="text-right border-full"><?=number_format($subtotal)?></td>
      </tr>
      <?php $grandtotal = $grandtotal + $subtotal; ?>
      <?php $idtipe = $row->idtipe; ?>
      <?php } ?>
      <tr>
        <td class="text-center border-full" colspan="4"><b>T O T A L</b></td>
        <td class="text-right border-full"><b><?=number_format($grandtotal)?></b></td>
      </tr>
    </table>
    <br>
    <table>
      <tr>
        <td></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width:30%" class="text-center">Bandung, <?=date('d') . ' ' . MONTHFormat(date('m')) . ' ' . date('Y'); ?> <br> Petugas Radiologi</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width:30%" class="text-center">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width:30%" class="text-center">( <?=$this->session->userdata('user_name')?> )</td>
      </tr>
    </table>
  </body>
</html>
