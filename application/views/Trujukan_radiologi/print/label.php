<!DOCTYPE html>
<html>
<!--untuk e-ticket
nanti di printer nya harus di setting kertas nya dulu
dengan ukuran
160mm x 200 mm
Ketas Label 121 Merk FOX
-->

<head>
	<title>Label</title>
	<style type="text/css">
		@page {
			margin-top: 2.8em;
			margin-left: 9em;
			margin-bottom: 2.5em;
		}

		* {
			color: black;

			font-style: normal;
			text-decoration: none;
			font-family: "Arial Rounded MT Bold", sans-serif;
			text-align: left;
			vertical-align: top;
			white-space: pre-wrap;
		}

		table {
			font-size: 24px !important;
			border-collapse: collapse !important;

			font-family: "Segoe UI", Arial, sans-serif;
		}

		td {
			padding: 2px;
		}

		.installasi {
			font-size: 26px !important;
			font-weight: bold;
		}

		.text-center {
			text-align: center !important;
		}

		.text-left {
			text-align: left !important;
		}
		.text-justify {
			text-align: justify !important;
		}

		.text-header {
			font-size: 30px !important;
		}

		.text-right {
			text-align: right !important;
		}

		/* text-style */
		.text-italic {
			font-style: italic;
		}

		.text-bold {
			font-weight: bold;
		}
	</style>
</head>

<body>
	<?php $index = 0; ?>
	<?php for ($i=1; $i <= 2; $i++) { ?>
		<?php $index = $i; ?>
		<?php $condition = ($index >= $start_awal && $index <= $start_akhir) ?>
		<table style="width:160mm;">
			<tr>
				<td>
					<table class="tg" style="width:152mm;" border="0" bgcolor="#FFFFFFF">
						<tr>
							<td class="text-center installasi"><?=($condition) ? 'INSTALASI RADIOLOGI':'&nbsp;'?></td>
						</tr>
						<tr>
							<td class="text-center installasi">&nbsp;</td>
						</tr>
					</table>
					<table class="tg" style="width:152mm;" border="0" bgcolor="#FFFFFFF">
						<tr>
							<td class="text-left" style="width:230px;"><?=($condition) ? 'No Register':'&nbsp;'?></td>
							<td class="text-left"><?=($condition) ? ': '.$header['norujukan']:'&nbsp;'?></td>
						</tr>
						<tr>
							<td class="text-left"><?=($condition) ? 'No RM':'&nbsp;'?></td>
							<td class="text-left"><?=($condition) ? ': '.$header['nomedrec']:'&nbsp;'?></td>
						</tr>
						<tr>
							<td class="text-left"><?=($condition) ? 'Nama':'&nbsp;'?></td>
							<td class="text-left"><?=($condition) ? ': '.$header['namapasien']:'&nbsp;'?></td>
						</tr>
						<tr>
							<td class="text-left"><?=($condition) ? 'Jenis Kelamin':'&nbsp;'?></td>
							<td class="text-left"><?=($condition) ? ': '.GetJenisKelamin($header['jeniskelamin']):'&nbsp;'?></td>
						</tr>
						<tr>
							<td class="text-left"><?=($condition) ? 'Tgl.Lahir / Umur':'&nbsp;'?></td>
							<td class="text-left"><?=($condition) ? ': '.$header['tanggallahir']:'&nbsp;'?></td>
						</tr>
						<tr>
							<td class="text-left"><?=($condition) ? 'Tgl Pemeriksaan':'&nbsp;'?></td>
							<td class="text-left"><?=($condition) ? ': '.$header['tanggalpemeriksaan']:'&nbsp;'?></td>
						</tr>
						<tr>
							<td class="text-left"><?=($condition) ? 'No Foto':'&nbsp;'?></td>
							<td class="text-left"><?=($condition) ? ': '.$header['nomorfoto']:'&nbsp;'?></td>
						</tr>
						<tr>
							<td class="text-left"><?=($condition) ? 'Jenis Pemeriksaan':'&nbsp;'?></td>
							<td class="text-left"><?=($condition) ? ': '.GetTipeRadiologi($label[$index-1]->idtipe):'&nbsp;'?></td>
						</tr>
						<tr>
							<td class="text-left"><?=($condition) ? 'Pemeriksaan':'&nbsp;'?></td>
							<td class="text-justify" style="word-wrap: break-word"><?=($condition) ? ': '.$label[$index-1]->pemeriksaan:'&nbsp;'?></td>
						</tr>
						<tr>
							<td class="text-left"><?=($condition) ? 'Posisi':'&nbsp;'?></td>
							<td class="text-left"><?=($condition) ? ': '.$label[$index-1]->posisi:'&nbsp;'?></td>
						</tr>
						<tr>
							<td class="text-left"><?=($condition) ? 'Asal Pasien':'&nbsp;'?></td>
							<td class="text-left"><?=($condition) ? ': '.$header['asalpasien']:'&nbsp;'?></td>
						</tr>
						<tr>
							<td class="text-left"><?=($condition) ? 'Dokter Perujuk':'&nbsp;'?></td>
							<td class="text-left"><?=($condition) ? ': '.$header['dokterperujuk']:'&nbsp;'?></td>
						</tr>
						<tr>
							<td class="text-left"><?=($condition) ? 'Petugas':'&nbsp;'?></td>
							<td class="text-left"><?=($condition) ? ': '.$header['petugas']:'&nbsp;'?></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="height : 20mm"></td>
			</tr>
		</table>
		<br>
	<?php } ?>
</body>

</html>
