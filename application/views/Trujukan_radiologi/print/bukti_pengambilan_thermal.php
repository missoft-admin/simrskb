<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>BUKTI PENGAMBILAN RADIOLOGI</title>
	<style>
		@font-face {
			font-family: Merchant Copy Regular;
			src: url('<?= base_url() ?>assets/fonts/merchant-copy.regular.ttf');
		}

		@font-face {
			font-family: Fake Receipt;
			src: url('<?= base_url() ?>assets/fonts/fakereceipt.ttf');
		}

		@page {
			margin: 15px;
			0px;
		}

		body {
			-webkit-print-color-adjust: exact;
			font-family: Merchant Copy Regular;
			font-size: 16px;
		}

		table {
			font-size: 16px !important;
			border-collapse: collapse !important;
			width: 95% !important;
		}

		td {
			padding: 2px;
		}

		.header {
			font-family: Fake Receipt;
		}

		.content td {
			margin: 3px;
			border: 0px solid #6033FF;
		}

		/* Border */
		.border-full {
			border: 0px solid #000 !important;
		}

		/* Text Position */
		.text-center {
			text-align: center !important;
		}

		.text-left {
			text-align: left !important;
		}

		.text-right {
			text-align: right !important;
		}

		/* Text Style */
		.text-italic {
			font-style: italic;
		}

		.text-bold {
			font-weight: bold;
		}
	</style>
</head>

<body>
	<div style="width:210">
		<!-- Header -->
		<table style="width:100%;">
			<tr>
				<td class="text-center" rowspan="5" style="padding: 0px; border: 0px solid #6033FF;">
					RSKB HALMAHERA SIAGA<br>
					JL. LLRE. Martadinata No. 28<br>
					Telp. +6222-4206061<br>
					Bandung, 40115
				</td>
			</tr>
		</table>

		<!-- Body -->
		<table class="content" style="width:100%">
			<tr>
				<td colspan="2" class="header text-center">INSTALASI RADIOLOGI</td>
			</tr>
			<tr>
				<td colspan="2" class="text-center">BUKTI PENGAMBILAN FOTO</td>
			</tr>
			<tr>
				<td style="width:30%">NO REGISTER</td>
				<td style="width:70%">: <?=$norujukan?></td>
			</tr>
			<tr>
				<td>TANGGAL PERIKSA</td>
				<td>: <?=$tanggal?></td>
			</tr>
			<tr>
				<td>NO REKAM MEDIS</td>
				<td>: <?=$nomedrec?></td>
			</tr>
			<tr>
				<td>NAMA PASIEN</td>
				<td>: <?=$namapasien?></td>
			</tr>
			<tr>
				<td>TANGGAL LAHIR</td>
				<td>: <?=$tanggallahir?></td>
			</tr>
			<tr>
				<td>UMUR</td>
				<td>: <?=(($umur)? $umur.' th ':'').(($umurbulan)? $umurbulan.' bln ':'').(($umurhari)? $umurhari.' hr ':'')?></td>
			</tr>
			<tr>
				<td>ASAL RUJUKAN</td>
				<td>: <?=$asalrujukan?></td>
			</tr>
			<tr>
				<td>DOKTER PERUJUK</td>
				<td>: <?=$dokterperujuk?></td>
			</tr>
			<tr>
				<td>NOMOR FOTO</td>
				<td>: <?=$nomorfoto?></td>
			</tr>
			<tr>
				<td>TGL AMBIL FOTO</td>
				<td>: <?=$tanggalpengambilan?></td>
			</tr>
		</table>

		<br>

		<!-- Detail -->
		<table class="content" style="width:100%">
			<tr>
				<td colspan="5" class="text-center">Foto X-RAY yang telah dilihat oleh dokter Orthopaedi, diambil kembali setelah 3 hari kedepan untuk di Expertise dokter ahli radiologi (Harap Dibawa Saat Pengambilan)</td>
			</tr>
		</table>

		<br>

		<!-- Footer -->
		<table style="width:100%">
			<tr>
				<td>Petugas Radiologi,</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>(<?=$userinput?>)</td>
			</tr>
			<tr>
				<td>Tanggal & Jam Cetak : <?=date('Y-m-d h:m:s')?></td>
			</tr>
		</table>
	</div>
</body>

</html>
