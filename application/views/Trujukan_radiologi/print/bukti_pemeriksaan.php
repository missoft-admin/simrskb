<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Hasil Pemeriksaan Radiologi</title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
    }
    @media print {
      * {
        font-size: 14px !important;
      }
      table {
        font-size: 14px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }

    @media screen {
      * {
        font-size: 9px !important;
      }
      table {
        font-size: 9px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      td {
        padding: 5px;
      }
      .content td {
        padding: 0px;
      }
      .content-2 td {
        margin: 3px;
      }

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
    }
    </style>
    <script type="text/javascript">
    	try {
    		this.print();
    	}
    	catch(e) {
    		window.onload = window.print;
    	}
    </script>
  </head>
  <body>
    <table class="content">
      <tr>
        <td style="text-align:center">
          &nbsp;<b>BUKTI PEMERIKSAAN RADIOLOGI</b>
        </td>
      </tr>
    </table>
    <br>
    <table class="content-2">
      <tr>
        <td rowspan="6" style="width:10%"><img src="<?= base_url() ?>assets/upload/logo/logo.png" width="100px"></td>
      </tr>
      <tr>
        <td style="width:100px">NO REGISTER</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$norujukan; ?></td>

        <td style="width:100px">UMUR</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$umur_tahun; ?>.Th</td>
      </tr>
      <tr>
        <td style="width:100px">TANGGAL & JAM</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$tanggalrujukan; ?></td>

        <td style="width:100px">ASAL RUJUKAN</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=GetAsalRujukan($asalrujukan); ?></td>
      </tr>
      <tr>
        <td style="width:100px">NO REKAM MEDIS</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$nomedrec; ?></td>

        <td style="width:100px">DOKTER PERUJUK</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$namadokterperujuk; ?></td>
      </tr>
      <tr>
        <td style="width:100px">NAMA PASIEN</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$namapasien; ?></td>

        <td style="width:100px">KELOMPOK PASIEN</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=$namakelompok; ?></td>
      </tr>
      <tr>
        <td style="width:100px">TANGGAL LAHIR</td>
        <td class="text-center" style="width:20px">:</td>
        <td><?=date('Y-m-d', strtotime($tanggallahir)); ?></td>

        <td style="width:100px">TARIF AKTIF</td>
        <td class="text-center" style="width:20px">:</td>
        <td></td>
      </tr>
    </table>
    <p class="text-center "><b>RINCIAN PEMERIKSAAN</b></p>
    <table>
      <tr>
        <td class="text-center border-full">NO</td>
        <td class="text-center border-full">PEMERIKSAAN</td>
        <td class="text-center border-full">TARIF</td>
        <td class="text-center border-full">KUANTITAS</td>
        <td class="text-center border-full">TOTAL</td>
      </tr>
      <?php $number = 1; ?>
      <?php $grandtotal = 0; ?>
      <?php foreach ($list_tindakan as $row) { ?>
        <?php $subtotal = $row->total * $row->kuantitas; ?>
        <tr>
          <td class="text-center border-full"><?=$number++?></td>
          <td class="border-full"><?=strtoupper($row->namatindakan)?></td>
          <td class="text-right border-full"><?=number_format($row->total)?></td>
          <td class="text-right border-full"><?=number_format($row->kuantitas)?></td>
          <td class="text-right border-full"><?=number_format($subtotal)?></td>
        </tr>
        <?php $grandtotal = $grandtotal + $subtotal; ?>
      <?php } ?>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td class="text-center border-full"><b>T O T A L</b></td>
        <td class="text-right border-full"><b><?=number_format($grandtotal)?></b></td>
      </tr>
    </table>
    <br>
    <table>
      <tr>
        <td style="width:20%" class="text-center">Petugas</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td style="width:20%" class="text-center">( <?=$this->session->userdata('user_name')?> )</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="text-bold" style="float:right"><i>Tanggal & Jam Cetak <?= date('d-m-Y h:i:s') ?></i></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="3">
          <p style="font-size: 12px;"><i>Foto yang sudah dilihat dokter orthopaedi diambil kembali setelah 2 hari kedepan untuk di expertise dokter ahli radiologi</i></p>
        </td>
      </tr>
    </table>
  </body>
</html>
