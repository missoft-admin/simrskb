<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mrekapan" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mrekapan/save','class="form-horizontal push-10-t"  onsubmit="return validate_final()"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama Rekapan</label>
				<div class="col-md-7">
					<input type="hidden" class="form-control" id="id" placeholder="Nama Jenis Kas" name="id" value="{id}" required="" aria-required="true">
					<input type="text" class="form-control" id="nama" placeholder="Nama Jenis Kas" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="deskripsi">Jadikan Pendapatan</label>
				<div class="col-md-7">
					<select tabindex="1" id="st_pendapatan" name="st_pendapatan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
						<option value="#" selected>-Pilih-</option>
							<option value="0" <?=($st_pendapatan == 0 ? 'selected' : '')?>>TIDAK</option>
							<option value="1" <?=($st_pendapatan == 1 ? 'selected' : '')?>>YA</option>
					</select>
				</div>
			</div>
			<div class="form-group" id="div_akun">
				<label class="col-md-3 control-label" for="No Akun">No. Akun</label>
				<div class="col-md-7">
					<select tabindex="1" id="idakun" name="idakun" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi" >
						<option value="#" selected>-Pilih-</option>
						<? foreach($list_akun as $row){ ?>	
						<option value="<?=$row->id?>" <?=($idakun==$row->id?'selected':'')?>><?=$row->noakun.' '.$row->namaakun?></option>
						<?}?>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mrekapan" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			
			<?php echo form_close() ?>
	</div>
</div>
<?if ($id){?>
<div class="block">
	<div class="block-header">
		<ul class="block-options">        
            <button class="btn btn-xs btn-success" id="btn_add_variable"><i class="fa fa-plus"></i> Add Variable</button>        
		</ul>
		<h3 class="block-title">VARIABLE</h3>
	</div>
		<div class="block-content">
			<div class="table-responsive">
			<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
			<table class="table table-bordered table-striped" id="tabel_variable" style="width: 100%;">
				<thead>
					<tr>
						<th>X</th>
						<th>Nama Variable</th>
						<th>Aksi</th>
						
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		</div>
</div>
<div class="block">
	<div class="block-header">
		
		<h3 class="block-title">REKENING</h3>
	</div>
		<div class="block-content">
						<input type="hidden" class="form-control" id="tedit" placeholder="No. Rek" value="">
			<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
			<table id="tabel_rekening" class="table table-striped table-bordered" style="margin-bottom: 0;">
				<thead>
					<tr>
						<th>X</th>
						<th>No Rekening</th>
						<th>Atas Nama</th>
						<th>Bank</th>
						<th>Aksi</th>
						<th></th>
					</tr>
					<tr>
						<td></td>
						<td><input type="text" class="form-control" id="norek" placeholder="No. Rek" value="" style="width:100%"></td>
						<td><input type="text" class="form-control" id="atas_nama" placeholder="Nama Rek" value="" style="width:100%"></td>
						<td>
							<select tabindex="1" id="idbank" name="idbank" class="js-select2 form-control" style="width: 100%;" >
								<option value="#" selected>-Pilih-</option>
								<? foreach($list_rekening as $row){ ?>	
								<option value="<?=$row->id?>" ><?=$row->bank?></option>
								<?}?>
							</select>
						</td>
						<td>
							<button title="Simpan" id="btn_simpan_rek" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i></button>
							<button title="Simpan" id="btn_clear" type="button" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i></button>
						</td>
						<td></td>
						
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
</div>
<?}?>
<div class="modal fade in black-overlay" id="modal_variable" role="dialog" style="text-transform:uppercase;">
	<div class="modal-dialog modal-lg modal-dialog-popout">
		<div class="modal-content">
			<div class="block block-themed block-transparent remove-margin-b">
				<div class="block-header bg-success">
					<h3 class="block-title">Variable Gaji</h3>
				</div>
				<div class="block-content">
					<div class="form-horizontal">
						<div class="form-group" style="margin-bottom: 5px;">
							<label class="col-md-4 control-label" for="" style="margin-top: 5px;">Jenis Gaji</label>
							<div class="col-md-8">
								<select id="idjenis" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
									<option value="0" selected>- Semua Jenis Gaji -</option>
									<?foreach($list_jenis as $row){?>
									<option value="<?=$row->id?>"><?=$row->nama?></option>
									<?}?>
								</select>
							</div>
						</div>
					</div>
					<br>
					<div class="table-responsive">
					<table class="table table-bordered table-striped" id="variableData" style="width: 100%;">
						<thead>
							<tr>
								<th>X</th>
								<th>X</th>
								<th>Jenis</th>
								<th>Nama Variable</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
<div id="cover-spin"></div>
<script src="{js_path}custom/basic.js"></script>
<link rel="stylesheet" type="text/css" href="{css_path}costum/timeline_css.css">
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/moment.min.js"></script>
<script src="{js_path}plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		Loadvariable();	
		show_hide_akun();
		load_rek();	
	})	
	//btn_add_variable
	$(document).on("change","#st_pendapatan",function(){	
		show_hide_akun();
	});
	function show_hide_akun(){
		if ($("#st_pendapatan").val()=='1'){
			
			$("#div_akun").show();
		}else{
			$("#div_akun").hide();			
		}
	}
	$(document).on("click","#btn_add_variable",function(){	
		$("#modal_variable").modal('show');
		getVariableRekapan($("#idjenis").val());
	});
	$(document).on("change", "#idjenis", function() {
		getVariableRekapan($(this).val());
	});
	function getVariableRekapan($idjenis) {
		// alert('sini');
		var idjenis=$idjenis;
		var idrekap=$("#id").val();
		
		$('#variableData').DataTable().destroy();
		var table = $('#variableData').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}mrekapan/getVariable/',
			type: "POST",
			dataType: 'json',
			data: {
				idjenis: idjenis,
				idrekap: idrekap,
				
			}
		},
		columnDefs: [{"targets": [0,1], "visible": false },
					 { "width": "40%", "targets": [2],className: "text-left" },
					 { "width": "40%", "targets": [3],className: "text-left" },
					 { "width": "20%", "targets": [4],className: "text-left" }
					]
		});
	}
	function Loadvariable() {
		var idrekap=$("#id").val();
		
		$('#tabel_variable').DataTable().destroy();
		var table = $('#tabel_variable').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}mrekapan/Loadvariable/',
			type: "POST",
			dataType: 'json',
			data: {
				idrekap: idrekap,
				
			}
		},
		columnDefs: [{"targets": [0], "visible": false },
					 { "width": "80%", "targets": [1],className: "text-left" },
					 { "width": "20%", "targets": [2],className: "text-left" }
					]
		});
	}
	function load_rek() {
		var idrekap=$("#id").val();
		
		$('#tabel_rekening').DataTable().destroy();
		var table = $('#tabel_rekening').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": true,
		"order": [],
		"ajax": {
			url: '{site_url}mrekapan/LoadRekening/',
			type: "POST",
			dataType: 'json',
			data: {
				idrekap: idrekap,				
			}
		},
		columnDefs: [{"targets": [0,5], "visible": false },
					 { "width": "25%", "targets": [1],className: "text-left" },
					 { "width": "25%", "targets": [2],className: "text-left" },
					 { "width": "25%", "targets": [3],className: "text-left" },
					 { "width": "25%", "targets": [4],className: "text-left" },
					]
		});
	}
	$(document).on("click",".pilih",function(){	
		var table = $('#variableData').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var idsetting=table.cell(tr,0).data();
		var idrekap=$("#id").val();
		$.ajax({
			url: '{site_url}mrekapan/pilih_variable',
			type: 'POST',
			data: {idsetting: idsetting,idrekap:idrekap},
			complete: function() {					
				swal({
					title: "Berhasil!",
					text: "Variable Berhasil ditambah.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});
				
				$('#variableData').DataTable().ajax.reload(null,false)
				$('#tabel_variable').DataTable().ajax.reload(null,false)
			}
		});
	
	});
	$(document).on("click",".edit_rek",function(){	
		var table = $('#tabel_rekening').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var id=table.cell(tr,0).data();
		var norek=table.cell(tr,1).data();
		var atas_nama=table.cell(tr,2).data();
		var idbank=table.cell(tr,5).data();
		$("#tedit").val(id);
		$("#norek").val(norek);
		$("#atas_nama").val(atas_nama);
		// alert(idbank);
		$("#idbank").val(idbank).trigger('change');
	
	});
	$(document).on("click","#btn_clear",function(){	
		clear_rek();
	});
	$(document).on("click",".hapus_pilih",function(){	
		var table = $('#tabel_variable').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var id=table.cell(tr,0).data();
		$.ajax({
			url: '{site_url}mrekapan/hapus_variable',
			type: 'POST',
			data: {id: id},
			complete: function() {					
				swal({
					title: "Berhasil!",
					text: "Variable Berhasil dihapus.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});
				
				$('#tabel_variable').DataTable().ajax.reload(null,false)
			}
		});
	
	});
	$(document).on("click",".hapus_rek",function(){	
		var table = $('#tabel_rekening').DataTable();
        tr = table.row( $(this).parents('tr') ).index()
		var id=table.cell(tr,0).data();
		$.ajax({
			url: '{site_url}mrekapan/hapus_rek',
			type: 'POST',
			data: {id: id},
			complete: function() {					
				swal({
					title: "Berhasil!",
					text: "Rekening Berhasil Dihapus.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});
				
				$('#tabel_rekening').DataTable().ajax.reload(null,false)
			}
		});
	
	});
	$(document).on("click","#btn_simpan_rek",function(){	
		if (validate_rek()==false){return false}
		$("#cover-spin").show();
		var tedit=$("#tedit").val();
		var idrekap=$("#id").val();
		var norek=$("#norek").val();
		var atas_nama=$("#atas_nama").val();
		var idbank=$("#idbank").val();
		$.ajax({
			url: '{site_url}mrekapan/simpan_rekening',
			type: 'POST',
			data: {
				tedit: tedit,
				idrekap: idrekap,
				norek: norek,
				atas_nama: atas_nama,
				idbank: idbank,
			
			},
			complete: function() {					
				swal({
					title: "Berhasil!",
					text: "Variable Berhasil ditambah.",
					type: "success",
					timer: 500,
					showConfirmButton: false
				});
				$("#cover-spin").hide();
				clear_rek();
				$('#tabel_rekening').DataTable().ajax.reload(null,false)
			}
		});
	
	
	});
	function clear_rek()
	{
		$("#norek").val('');
		$("#atas_nama").val('');
		$("#idbank").val('');
		$("#tedit").val('');
		$("#idbank").val('#').trigger('change');
	}
	function validate_rek()
	{
		if ($("#norek").val()==''){
			sweetAlert("Maaf...", "Tentukan No. Rekening", "error");
			return false;
		}
		if ($("#atas_nama").val()==''){
			sweetAlert("Maaf...", "Tentukan Atas Nama", "error");
			return false;
		}
		if ($("#idbank").val()=='#'){
			sweetAlert("Maaf...", "Tentukan Bank", "error");
			return false;
		}
		
		$("*[disabled]").not(true).removeAttr("disabled");
		// $("#simpan_tko").hide();
		// $("#cover-spin").show();
		return true;
	}
	function validate_final()
	{
		if ($("#nama").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Rekapan", "error");
			return false;
		}
		if ($("#st_pendapatan").val()=='#'){
			sweetAlert("Maaf...", "Tentukan Jenis Pendapatan", "error");
			return false;
		}
		if ($("#st_pendapatan").val()=='1' && $("#idakun").val()=='#'){
			sweetAlert("Maaf...", "Tentukan Akun Pendapatan", "error");
			return false;
		}
		
		$("*[disabled]").not(true).removeAttr("disabled");
		$("#simpan_tko").hide();
		$("#cover-spin").show();
		return true;
	}
</script>