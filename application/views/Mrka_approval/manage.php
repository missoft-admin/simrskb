<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div id="cover-spin"></div>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mrka_approval" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mrka_approval/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Nama Pemohon</label>
				<div class="col-md-4">
					<input type="hidden" class="form-control" id="id" placeholder="Nama RKA" name="id" value="{id}" required="" aria-required="true">
					<input type="text" readonly class="form-control" id="nama_pemohon" <?=$disabel?> placeholder="Nama Pemohon" name="nama_pemohon" value="{nama_pemohon}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="disabel" placeholder="Nama RKA" name="disabel" value="{disabel}" required="" aria-required="true">
					<input type="hidden" class="form-control" id="idrka_kegiatan" placeholder="Nama RKA" name="idrka_kegiatan" value="{idrka_kegiatan}" required="" aria-required="true">
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="bulan" id="bulan" value="<?=$bulan?>" required="" aria-required="true">
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="idlogic" id="idlogic" value="<?=$idlogic?>">
					<input type="hidden" class="form-control" placeholder="Nama RKA" name="status_berjenjang" id="status_berjenjang" value="<?=$status_berjenjang?>">
				</div>
				<label class="col-md-2 control-label" for="nama">Unit Yang Mengajukan</label>
				<div class="col-md-4">
					<select id="idunit_pengaju" <?=$disabel?> <?=($tipe_rka=='1'?'disabled':'')?> name="idunit_pengaju" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Unit -</option>
						<? foreach($list_unit as $row){ ?>
							<option value="<?=$row->idunit?>" <?=($idunit_pengaju==$row->idunit?'selected':'')?> ><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Tanggal Pengajuan</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input" <?=$disabel?> type="text" id="tanggal_pengajuan" name="tanggal_pengajuan" value="<?=$tanggal_pengajuan?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="nama"></label>
				<label class="col-md-2 control-label" for="nama">Tanggal Dibutuhkan</label>
				<div class="col-md-2">
					<div class="input-group date">
					<input class="js-datepicker form-control input" <?=$disabel?> type="text" id="tanggal_dibutuhkan" name="tanggal_dibutuhkan" value="<?=$tanggal_dibutuhkan?>" data-date-format="dd-mm-yyyy"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Untuk  Unit</label>
				<div class="col-md-4">
					<select id="idunit" name="idunit" <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Unit -</option>
						<? foreach(get_all('munitpelayanan',array('status'=>1)) as $row){ ?>
							<option value="<?=$row->id?>" <?=($idunit==$row->id?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
				<label class="col-md-2 control-label" for="nama">Jenis Pengajuan</label>
				<div class="col-md-4">
					<select id="idjenis" name="idjenis"  <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" selected>- Pilih Jenis -</option>
						<? foreach($list_jenis as $row){ ?>
							<option value="<?=$row->id?>" <?=($idjenis==$row->id?'selected':'')?>><?=$row->nama?></option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Tipe Pengajuan</label>
				<div class="col-md-4">
					<select id="tipe_rka" disabled name="tipe_rka" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="1" <?=$tipe_rka=='1'?'selected':''?>>RKA</option>
						<option value="2" <?=$tipe_rka=='2'?'selected':''?>>NON RKA</option>						
					</select>
				</div>
				<label class="col-md-2 control-label" for="nama">Nama Kegiatan</label>
				<div class="col-md-4">
					<? if ($tipe_rka=='1'){ ?>
					<div class="input-group date">
						<input type="text" class="form-control" <?=$disabel?> readonly id="nama_kegiatan" <?=$disabel?> placeholder="Nama Kegiatan" name="nama_kegiatan" value="{nama_kegiatan}">
						<span class="input-group-btn">
							<button class="btn btn-primary" <?=$disabel?> type="button" id="btn_open_modal_kegiatan" title="Terapkan sama rata"><i class="fa fa-search"></i></button>
						</span>
					</div>
					<?}else{?>
					<input type="text" class="form-control" <?=$disabel?> <?=$disabel?> placeholder="Nama Kegiatan" name="nama_kegiatan" value="{nama_kegiatan}">
					<?}?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Vendor</label>
				<div class="col-md-4">
					<div class="input-group">
						<select id="idvendor" name="idvendor" <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" <?=($idvendor=='#'?'selected':'')?>>- Pilih Vendor -</option>
							<option value="0" <?=($idvendor=='0'?'selected':'')?>>Tentukan Vendor Nanti</option>
							<? foreach($list_vendor as $row){ ?>
								<option value="<?=$row->id?>" <?=($idvendor==$row->id?'selected':'')?>><?=$row->nama?></option>
							<?}?>
						</select>
						<span class="input-group-btn">
							<button class="btn btn-success" <?=$disabel?> type="button" id="btn_refresh_vendor" title="Refresh"><i class="fa fa-refresh"></i></button>
							<a href="{base_url}mvendor" target="_blank" class="btn btn-primary" <?=$disabel?> type="button" id="btn_add_vendor" title="Tambah Vendor"><i class="fa fa-plus"></i></a>
						</span>
					</div>
				</div>
				<label class="col-md-2 control-label" for="nama">Catatan</label>
				<div class="col-md-4">
					<textarea class="form-control" rows="2" name="catatan" id="catatan"><?=$catatan?></textarea>
				</div>
			</div>
			<div class="progress progress-mini">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<h5 style="margin-bottom: 10px;">DETAIL PENGAJUAN</h5>
			
			<div class="form-group">
				<table id="tabel_add" class="table table-striped table-bordered" style="margin-bottom: 0;">
					<thead>
						<tr>
							<th style="width: 15%;">Nama Barang</th>
							<th style="width: 15%;">Merk Barang</th>
							<th style="width: 5%;">QTY</th>
							<th style="width: 5%;">Satuan</th>
							<th style="width: 10%;">Harga</th>
							<th style="width: 10%;">Total</th>
							<th style="width: 15%;">Keterangan</th>
							<th style="width: 8%;">Actions</th>
						</tr>
						<tr hidden>							
							<td><input class="form-control input-sm" tabindex="2" type="text" id="nama_barang" /></td>
							<td><input class="form-control input-sm" tabindex="3" type="text" id="merk_barang" /></td>
							<td><input class="form-control input-sm number" tabindex="4" type="text" id="kuantitas" /></td>
							<td><input class="form-control input-sm" tabindex="5" type="text" id="satuan" /></td>
							<td><input class="form-control input-sm number" tabindex="6" type="text" id="harga_satuan" /></td>
							<td><input class="form-control input-sm number" readonly  type="text" id="total_harga" /></td>
							<td><input class="form-control input-sm" tabindex="7" type="text" id="keterangan" /></td>
							<td>
								<button type="button" <?=$disabel?> class="btn btn-sm btn-primary kunci" tabindex="8" id="addjual" title="Masukan Item"><i class="fa fa-level-down"></i></button>&nbsp;&nbsp;
								<button type='button' <?=$disabel?> class='btn btn-sm btn-danger kunci' tabindex="9" id="canceljual" title="Refresh"><i class='fa fa-times'></i></button>
							</td>
						</tr>
					</thead>
					<tbody>
						<? 
						$gt=0;
						$item=0;
						foreach($list_detail as $row){ 
							$gt=$gt + $row->total_harga;
							$item=$item + 1;
						?>
							<tr>
								<td><?=$row->nama_barang?></td>
								<td><?=$row->merk_barang?></td>
								<td><?=$row->kuantitas?></td>
								<td><?=$row->satuan?></td>
								<td><?=number_format($row->harga_satuan)?></td>
								<td><?=number_format($row->total_harga)?></td>
								<td><?=$row->keterangan?></td>
								<td><? echo "<div class='btn-group'><button type='button' ".$disabel." class='btn btn-xs btn-info edit kunci'><i class='glyphicon glyphicon-pencil'></i></button><button type='button' ".$disabel." class='btn btn-xs btn-danger hapus kunci'><i class='glyphicon glyphicon-remove'></i></button></div>"?></td>
								<td style='display:none'><input type='text' name='xiddet[]' value="<?=$row->id?>"></td>
								<td style='display:none'><input type='text' name='xnama_barang[]' value="<?=$row->nama_barang?>"></td>
								<td style='display:none'><input type='text' name='xmerk_barang[]' value="<?=$row->merk_barang?>"></td>
								<td style='display:none'><input type='text' name='xkuantitas[]' value="<?=$row->kuantitas?>"></td>
								<td style='display:none'><input type='text' name='xsatuan[]' value="<?=$row->satuan?>"></td>
								<td style='display:none'><input type='text' name='xharga_satuan[]' value="<?=$row->harga_satuan?>"></td>
								<td style='display:none'><input type='text' name='xtotal_harga[]' value="<?=$row->total_harga?>"></td>
								<td style='display:none'><input type='text' name='xketerangan[]' value="<?=$row->keterangan?>"></td>
								<td style='display:none'><input type='text' name='xstatus[]' value="<?=$row->status?>"></td>
							</tr>
						<?}?>
					</tbody>
					<tfoot id="foot-total-nonracikan">
						<tr>
							<th colspan="5" class="hidden-phone">

								<span class="pull-right"><b>TOTAL</b></span></th>
							<input type="hidden" id="rowindex" name="rowindex" value="">
							<input type="hidden" id="iddet" name="iddet" value="">
							<th><input class="form-control input-sm number" readonly type="text" id="grand_total" name="grand_total" value="<?=$gt?>"/></th>
							<th><input class="form-control input-sm number"  type="hidden" id="total_item" name="total_item" value="<?=$item?>"/></th>
							<th><input class="form-control input-sm number"  type="hidden" id="total_jenis_barang" name="total_jenis_barang" value="<?=$item?>"/></th>
							
						</tr>
					</tfoot>
				</table>
				
			</div>
			<div class="progress progress-mini">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama"><h4><span class="label label-primary">Cara Pembayaran</span></h4></label>
				<div class="col-md-4">
					<select id="cara_pembayaran" name="cara_pembayaran" <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" <?=($cara_pembayaran=='#'?'selected':'')?>>- Pilih Cara Pembayaran -</option>
						<option value="0" <?=($cara_pembayaran=='0'?'selected':'')?>>Tentukan Cara Pembayaran Nanti</option>
						<option value="1" <?=($cara_pembayaran=='1'?'selected':'')?>>Satu Kali Bayar</option>
						<option value="2" <?=($cara_pembayaran=='2'?'selected':'')?>>Cicilan</option>
						<option value="3" <?=($cara_pembayaran=='3'?'selected':'')?>>By Termin</option>
						
					</select>						
				</div>				
			</div>
			<div class="form-group" style="display:block" id="div_satu">
				<label class="col-md-2 control-label" for="nama"><h4><span class="label label-primary">Jenis Pembayaran</span></h4></label>
				<div class="col-md-4">
					<select id="jenis_pembayaran" name="jenis_pembayaran" <?=$disabel?> class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#" <?=($jenis_pembayaran=='#'?'selected':'')?>>- Pilih Jenis Pembayaran -</option>
						<option value="0" <?=($jenis_pembayaran=='0'?'selected':'')?>>Tentukan Jenis Pembayaran Nanti</option>
						<option value="1" <?=($jenis_pembayaran=='1'?'selected':'')?>>Tunai</option>
						<option value="2" <?=($jenis_pembayaran=='2'?'selected':'')?>>Transfer</option>
						<option value="3" <?=($jenis_pembayaran=='3'?'selected':'')?>>Kartu Kredit</option>
						<option value="4" <?=($jenis_pembayaran=='4'?'selected':'')?>>Kontrabon</option>
						
					</select>						
				</div>
				<div class="col-md-1">
					
				</div>	
			</div>
			<div id="div_tf">
				<div class="form-group" style="display:block">
					<label class="col-md-2 control-label" for="nama">No Rekening</label>
					<div class="col-md-4">
						<div class="input-group">
							<input class="form-control input-sm "  type="hidden" readonly id="rekening_id" name="rekening_id" value="{rekening_id}"/>
							<input class="form-control input-sm " <?=$disabel?> type="text" id="norek" name="norek" value="{norek}"/>	
							<span class="input-group-btn">
								<button class="btn btn-primary btn-sm" <?=$disabel?> type="button" id="btn_add_rekening" title="Rekening Vendor"><i class="fa fa-credit-card"></i></button>
							</span>
						</div>		
					</div>
					<label class="col-md-2 control-label" for="nama">Bank</label>
					<div class="col-md-4">
						<input class="form-control input-sm " <?=$disabel?> type="text" id="bank" name="bank" value="{bank}"/>			
					</div>				
				</div>
				<div class="form-group" style="display:block" style="margin-bottom: 10px;">
					<label class="col-md-2 control-label" for="nama">Atas Nama</label>
					<div class="col-md-4">
						<input class="form-control input-sm " <?=$disabel?> type="text" id="atasnama" name="atasnama" value="{atasnama}"/>			
					</div>
								
				</div>
				<div class="form-group" style="display:block">
					<label class="col-md-2 control-label" for="nama">Keterangan</label>
					<div class="col-md-10">
						<textarea class="form-control" rows="2" <?=$disabel?>  name="keterangan_bank" id="keterangan_bank"><?=$keterangan_bank?></textarea>	
					</div>
								
				</div>
			</div>
			<div id="div_kbo">
				<div class="form-group" style="display:block">
					<label class="col-md-2 control-label" for="nama">Tanggal Kontrabon</label>
					<div class="col-md-4">
						<div class="input-group date">
							<input class="js-datepicker form-control input" <?=$disabel?> type="text" id="tanggal_kontrabon" name="tanggal_kontrabon" value="<?=($tanggal_kontrabon?HumanDateShort($tanggal_kontrabon):'')?>" data-date-format="dd-mm-yyyy"/>
							<span class="input-group-addon">
								<span class="fa fa-calendar"></span>
							</span>
						</div>
					</div>
								
				</div>
				
			</div>
			<?$this->load->view('Mrka_pengajuan/div_cicilan')?>
			<?$this->load->view('Mrka_pengajuan/div_termin')?>
				
			<div class="progress progress-mini">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<!-- Static Labels block-opt-hidden -->
					<div class="block block-themed block-opt-hidden" id="div_berkas">
						<div class="block-header bg-primary">
							<ul class="block-options">
								<li>
									<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="si si-user-following"></i>  User Approval</h3>
						</div>
						<div class="block-content block-content">
							

								<input class="form-control"  value="0" type="hidden" readonly id="st_load_berkas">
								<table width="100%" id="tabel_user_proses" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Step</th>
											<th>User</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							
						</div>
					</div>
					<!-- END Static Labels -->
				</div>

			</div>
			<div class="row">
				<div class="col-md-12">
					<!-- Static Labels block-opt-hidden -->
					<div class="block block-themed" id="div_berkas">
						<div class="block-header bg-primary">
							<ul class="block-options">
								<li>
									<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="si si-docs"></i>  MEMO</h3>
						</div>
						<div class="block-content block-content">
							<textarea class="form-control js-summernote" name="memo_bendahara" id="memo_bendahara"><?=$memo_bendahara?></textarea>
						</div>
					</div>
					<!-- END Static Labels -->
				</div>

			</div>
			<div class="row">
				<div class="col-md-12">
					<!-- Static Labels block-opt-hidden -->
					<div class="block block-themed" id="div_berkas">
						<div class="block-header bg-primary">
							<ul class="block-options">
								<li>
									<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="si si-user-following"></i>  Bendahara Proses</h3>
						</div>
						<div class="block-content block-content">
								<table width="100%" id="tabel_user_bendahara" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Bendahara</th>
										</tr>
									</thead>
									<?
										$q="SELECT H.iduser,M.`name` as nama FROM mlogic_user_bendahara H
											LEFT JOIN musers M  ON M.id=H.iduser
											WHERE H.idlogic='$idlogic' AND H.tipe_rka='$tipe_rka' AND H.idjenis='$idjenis'";
										$rows=$this->db->query($q)->result();
										$no=1;
										$jml_bendahara=0;
									?>
									<tbody>
										<?foreach($rows as $row){?>
											<tr><td><?=$no.'.  '.$row->nama?></td></tr>
										<?
										$no=$no+1;
										$jml_bendahara=$jml_bendahara+1;
										
										}?>
									</tbody>
								</table>
								<?if ($jml_bendahara=='0'){ ?>
								<div class="alert alert-danger alert-dismissable">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<p id="label_info"><strong>BENDAHARA BELUM DITENTUKAN</strong></p>
								</div>
								<?}?>
								
							
						</div>
					</div>
					<!-- END Static Labels -->
				</div>

			</div>
			<div class="row">
				<div class="col-md-12">
					<!-- Static Labels block-opt-hidden -->
					<div class="block block-themed" id="div_berkas">
						<div class="block-header bg-primary">
							<ul class="block-options">
								<li>
									<button id="button_up_berkas" type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
								</li>
							</ul>
							<h3 class="block-title"><i class="si si-users"></i>  Unit yang Memproses</h3>
						</div>
						<div class="block-content block-content">
							<div class="form-group">
								<div class="col-md-4">
									<label class="css-input css-checkbox css-checkbox-primary">
										<input type="checkbox" id="st_user_unit" <?=($st_user_unit=='1'?'checked':'')?>><span></span> Unit Yang Mengajukan
									</label>
									<input type="hidden" name="st_user_unit" value="{st_user_unit}">
								</div>
								
							</div>
							<div class="form-group">
								<div class="col-md-2">
									<label class="css-input css-checkbox css-checkbox-primary">
										<input type="checkbox" <?=($st_user_unit_lain=='1'?'checked':'')?> id="st_user_unit_lain"><span></span> Unit Lain
									</label>
									<input type="hidden" name="st_user_unit_lain" value="{st_user_unit_lain}">
								</div>
								<div class="col-md-4">
									<select id="unit_lain" <?=($st_user_unit_lain=='0'?'disabled':'')?> name="unit_lain" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
										<option value="#" selected>- Pilih Unit -</option>
										<? foreach($list_unit_lain as $row){ ?>
											<option value="<?=$row->idunit?>" <?=($unit_lain==$row->idunit?'selected':'')?>><?=$row->nama?></option>
										<?}?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-4">
									<label class="css-input css-checkbox css-checkbox-primary">
										<input type="checkbox" id="st_user_bendahara" <?=($st_user_bendahara=='1'?'checked':'')?> name="st_user_bendahara" value="1"><span></span> Unit Bendahara 
									</label>
									<input type="hidden" name="st_user_bendahara" value="{st_user_bendahara}">
								</div>
								
							</div>
						</div>
					</div>
					<!-- END Static Labels -->
				</div>
			</div>

			
			<div class="form-group">
				<div class="text-right bg-light lter">
					<button class="btn btn-primary" type="submit" id="btn_simpan" name="btn_simpan" value='1'>Simpan</button>
					<button class="btn btn-danger" type="submit" id="btn_simpan" <?=($jml_bendahara=='0'?'disabled':'')?> name="btn_simpan" value='2'>Proses Ke Bendahara</button>
					<a href="{base_url}mrka_approval" class="btn btn-default" type="button">Kembali Ke Index</a>
				</div>
			</div>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<?$this->load->view('Mrka_pengajuan/modal_vendor')?>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		$(".decimal").number(true,2,'.',',');
		$('.number').number(true, 0, '.', ',');
		clear_input();
		show_hide_pembayaran();
		load_user();
		$id=$("#id").val();
		if ($id){
			load_item_barang($id);
			load_list_cicilan();
			load_item_termin();
		}
	})	
	$(document).on("click","#st_user_unit",function(){
		var isi;
		if ($(this).is(':checked')==true){
			isi=1;
		}else{
			isi=0;
		}
		$('input[name="st_user_unit"]').val(isi);
		
	});
	$(document).on("click","#st_user_unit_lain",function(){
		var isi;
		if ($(this).is(':checked')==true){
			isi=1;
			$("#unit_lain").attr('disabled',false);
		}else{
			isi=0;
			$("#unit_lain").attr('disabled',true);
		}
		$('input[name="st_user_unit_lain"]').val(isi);
		
	});
	$(document).on("click","#st_user_bendahara",function(){
		var isi;
		if ($(this).is(':checked')==true){
			isi=1;
		}else{
			isi=0;
		}
		$('input[name="st_user_bendahara"]').val(isi);
		
	});
	function load_user(){
		var idrka=$("#id").val();
		$("#modal_user").modal('show');
		
		// alert(idrka);
		$.ajax({
			url: '{site_url}mrka_pengajuan/list_user/'+idrka,
			dataType: "json",
			success: function(data) {
				$("#tabel_user_proses tbody").empty();
				$("#tabel_user_proses tbody").append(data.detail);
			}
		});
	}
	function show_hide_pembayaran(){
		$('#div_cicilan').hide();
		$('#div_satu').hide();
		$('#div_kbo').hide();
		$('#div_termin').hide();
		$('#div_tf').hide();
		if ($("#cara_pembayaran").val()=='1'){//Satu Kali			
			$('#div_satu').show();
			if ($("#jenis_pembayaran").val()=='2' || $("#jenis_pembayaran").val()=='3'){//TF
				$('#div_tf').show();
			}else if ($("#jenis_pembayaran").val()=='4'){//Konta	
				$('#div_kbo').show();
			}
		}else if ($("#cara_pembayaran").val()=='2'){//Cicilan
			$('#div_tf').show();
			$('#div_cicilan').show();
			hitung_total_cicilan();
		}else if ($("#cara_pembayaran").val()=='3'){//Fix
			$('#div_tf').show();
			$('#div_termin').show();
			hitung_total_termin();
		}
	}
	function clear_input(){
		$("#nama_barang").val('')
		$("#merk_barang").val('')
		$("#kuantitas").val('0')
		$("#satuan").val('')
		$("#harga_satuan").val('0')
		$("#total_harga").val('')
		$("#keterangan").val('')
		$("#rowindex").val('')
		if ($("#disabel").val==''){
			$(".edit").attr('disabled', false);
			$(".hapus").attr('disabled', false);
		}
		// hitung_total();
	}
	$(document).on("change","#jenis_pembayaran,#cara_pembayaran",function(){
		// console.log('sini');
		show_hide_pembayaran();
		
	});
	$(document).on("keyup","#kuantitas,#harga_satuan",function(){
		// console.log('sini');
		perkalian();
		
	});
	function formatNumber(num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}

	function perkalian(){
		var total=0;
		total=parseFloat($("#kuantitas").val()) * parseFloat($("#harga_satuan").val());
		$("#total_harga").val(total)
	}
	// $("#addjual").click(function() {
			// var content = "";
			// if (!validate_detail()) return false;
			// if ($("#rowindex").val() == '') {
				// content += "<tr>";
			// }
			// content += "<td>" + $("#nama_barang").val() + "</td>"; //0 No Urut
			// content += "<td>" + $("#merk_barang").val() + "</td>"; //1 
			// content += "<td>" + $("#kuantitas").val() + "</td>"; //2 
			// content += "<td>" + $("#satuan").val() + "</td>"; //3 
			// content += "<td>" + formatNumber($("#harga_satuan").val()) + "</td>"; //4 
			// content += "<td>" + formatNumber($("#total_harga").val()) + "</td>"; //5 
			// content += "<td>" + $("#keterangan").val() + "</td>"; //6			
			// content +="<td ><div class='btn-group'><button type='button' class='btn btn-xs btn-info edit kunci'><i class='glyphicon glyphicon-pencil'></i></button><button type='button' class='btn btn-xs btn-danger hapus kunci'><i class='glyphicon glyphicon-remove'></i></button></div></td>";
			// content += "<td style='display:none'><input type='text' name='xiddet[]' value='"+$("#iddet").val()+"'></td>"; //8 nomor
			// content += "<td style='display:none'><input type='text' name='xnama_barang[]' value='"+$("#nama_barang").val()+"'></td>"; //9 
			// content += "<td style='display:none'><input type='text' name='xmerk_barang[]' value='"+$("#merk_barang").val()+"'></td>"; //10
			// content += "<td style='display:none'><input type='text' name='xkuantitas[]' value='"+$("#kuantitas").val()+"'></td>"; //11
			// content += "<td style='display:none'><input type='text' name='xsatuan[]' value='"+$("#satuan").val()+"'></td>"; //12
			// content += "<td style='display:none'><input type='text' name='xharga_satuan[]' value='"+$("#harga_satuan").val()+"'></td>"; //13
			// content += "<td style='display:none'><input type='text' name='xtotal_harga[]' value='"+$("#total_harga").val()+"'></td>"; //14
			// content += "<td style='display:none'><input type='text' name='xketerangan[]' value='"+$("#keterangan").val()+"'></td>"; //15
			// content += "<td style='display:none'><input type='text' name='xstatus[]' value='1'></td>"; //16
			
			// if ($("#rowindex").val() != '') {
				// $('#tabel_add tbody tr:eq(' + $("#rowindex").val() + ')').html(content);
			// } else {
				// content += "</tr>";
				// $('#tabel_add tbody').append(content);
			// }
			 // hitung_total();
			// clear_input();
	// });
	// function validate_detail() {
		// if ($("#nama_barang").val() == "") {
			// sweetAlert("Maaf...", "Nama barang Harus Diisi!", "error");
			// $("#nama_barang").focus();
			// return false;
		// }
		// if ($("#merk_barang").val() == "") {
			// sweetAlert("Maaf...", "Merk barang Harus Diisi!", "error");
			// $("#merk_barang").focus();
			// return false;
		// }	
		
		// if ($("#kuantitas").val() < 1) {
			// sweetAlert("Maaf...", "QTY Harus Diisi!", "error");
			// $("#kuantitas").focus();
			// return false;
		// }
		// if ($("#satuan").val() == "") {
			// sweetAlert("Maaf...", "Satuan barang Harus Diisi!", "error");
			// $("#satuan").focus();
			// return false;
		// }
		// if ($("#harga_satuan").val() < 1) {
			// sweetAlert("Maaf...", "Harga Harus Diisi!", "error");
			// $("#harga_satuan").focus();
			// return false;
		// }
		// if ($("#total_harga").val() < 1) {
			// sweetAlert("Maaf...", "Total Harga Harus Diisi!", "error");
			// $("#total_harga").focus();
			// return false;
		// }
		// return true;
	// }
	function validate_final() {
		var rowCount = $('#tabel_add tbody tr').length;
		if (rowCount < 1){
			sweetAlert("Maaf...", "Tidak ada barang yang diajukan!", "error");
			return false;
		}
		if ($("#idunit").val() == "#") {
			sweetAlert("Maaf...", "Unit Harus Diisi!", "error");
			$("#idunit").focus();
			return false;
		}
		if ($("#idunit_pengaju").val() == "#") {
			sweetAlert("Maaf...", "Unit Pengaju Harus Diisi!", "error");
			$("#idunit_pengaju").focus();
			return false;
		}
		if ($("#idjenis").val() == "#") {
			sweetAlert("Maaf...", "Jenis Pengajuan Harus Diisi!", "error");
			$("#idjenis").focus();
			return false;
		}
		if ($("#nama_kegiatan").val() == "") {
			sweetAlert("Maaf...", "Kegiatan Harus Diisi!", "error");
			$("#merk_barang").focus();
			return false;
		}	
		if ($("#idvendor").val() == "#") {
			sweetAlert("Maaf...", "Teruntukan Vendor!", "error");
			$("#cara_pembayaran").focus();
			return false;
		}
		if ($("#cara_pembayaran").val() == "#") {
			sweetAlert("Maaf...", "Cara Pembayaran Harus Diisi!", "error");
			$("#cara_pembayaran").focus();
			return false;
		}
		
		// if ($("#cara_pembayaran").val() != "#" && $("#cara_pembayaran").val() != "0") {
			// if ($("#jenis_pembayaran").val() == "#") {
				// sweetAlert("Maaf...", "Jenis Pembayaran Diisi!", "error");
				// $("#jenis_pembayaran").focus();
				// return false;
			// }	
		// }
		if ($("#cara_pembayaran").val() == "1") {//1 kali
				if ($("#jenis_pembayaran").val()=='2' || $("#jenis_pembayaran").val()=='3'){//TF
					if ($("#norek").val()=='' || $("#bank").val()=='' || $("#atasnama").val()==''){
						sweetAlert("Maaf...", "Data Bank harus Diisi!", "error");
						return false;
					}
				}else if ($("#jenis_pembayaran").val()=='4'){//Konta	
					if ($("#tanggal_kontrabon").val()==''){
						sweetAlert("Maaf...", "tanggal Kontrabon harus Diisi!", "error");
						return false;
					}
				}
		}
		// alert($("#cara_pembayaran").val() + ' : '+ $("#sisa_termin").val());
		if ($("#cara_pembayaran").val() == "2") {//CICILAN
			// if ($("#total_dp_cicilan").val()=='0'){
				// sweetAlert("Maaf...", "DP Belum diinput Harus Diisi!", "error");
				// $("#cara_pembayaran").focus();
				// return false;
			// }else{
				var rowCount = $('#tabel_cicilan tbody tr').length;
				if (rowCount==0){
					sweetAlert("Maaf...", "Cicilan Belum diinput Harus Diisi!", "error");
					return false;
				}
			// }
			
			// return false;
			
		}
		if ($("#cara_pembayaran").val() == "3" && $("#sisa_termin").val()>0) {
			sweetAlert("Maaf...", "Termin Belum selesai Harus Diisi!", "error");
			$("#cara_pembayaran").focus();
			return false;
		}
		if ($('input[name="st_user_unit"]').val()=='0' && $('input[name="st_user_unit_lain"]').val()=='0' && $('input[name="st_user_bendahara"]').val()=='0'){
			sweetAlert("Maaf...", "Tentukan Unit Proses!", "error");
			return false;
		}
		$("#cover-spin").show();
		$("*[disabled]").not(true).removeAttr("disabled");
		return true;
	}
	$(document).on("click", ".edit", function() {
			$(".edit").attr('disabled', true);
			$(".hapus").attr('disabled', true);
			$("#rowindex").val($(this).closest('tr')[0].sectionRowIndex);			
			$("#nama_barang").val($(this).closest('tr').find("td:eq(9) input").val());
			$("#merk_barang").val($(this).closest('tr').find("td:eq(10) input").val());
			$("#kuantitas").val($(this).closest('tr').find("td:eq(11) input").val());
			$("#satuan").val($(this).closest('tr').find("td:eq(12) input").val());
			$("#harga_satuan").val($(this).closest('tr').find("td:eq(13) input").val());
			$("#total_harga").val($(this).closest('tr').find("td:eq(14) input").val());
			$("#keterangan").val($(this).closest('tr').find("td:eq(15) input").val());
			$("#kuantitas").focus();
	});
	$(document).on("click", ".hapus", function() {
		var baris=$(this).closest('td').parent();
		
			if ($("#id").val()==''){
				// alert($("#id").val());
				baris.remove();
				hitung_total();
			}else{
				$(this).closest('tr').find("td:eq(16) input").val(0)
				$(this).closest('tr').find("td:eq(6)").html('<span class="label label-danger">DELETED</span>')
				$(this).closest('tr').find("td:eq(7)").html('<span class="label label-danger">DELETED</span>')
			}
		
		 hitung_total();
	});
	
	function hitung_total(){
		var total_grand = 0;
		var total_item = 0;
		var total_jenis_barang = 0;
		$('#tabel_add tbody tr').each(function() {
			if ($(this).closest('tr').find("td:eq(16) input").val()=='1'){
				total_jenis_barang +=1;
				total_grand += parseFloat($(this).find('td:eq(14) input').val());
				total_item += parseFloat($(this).find('td:eq(11) input').val());
			}
		});
		// alert(total_grand);
		$("#grand_total").val(total_grand);
		$("#total_item").val(total_item);
		$("#total_jenis_barang").val(total_jenis_barang);
	}
	$(document).on("click", "#canceljual", function() {
		clear_input();
	});
	$(document).on("change", "#idrka", function() {
		refresh_program();
	});
	$(document).on("click", "#btn_refresh_vendor", function() {
		refresh_vendor();
	});
	
	function refresh_vendor(){
		$.ajax({
			url: '{site_url}mrka_pengajuan/refresh_vendor/',
			dataType: "json",
			success: function(data) {
				$("#idvendor").empty();
				$('#idvendor').append('<option value="#">- Pilih Vendor -</option><option value="0">Tentukan Vendor Nanti</option>');
				$('#idvendor').append(data.detail);
			}
		});
	}
	function refresh_program(){
		var idrka=$("#idrka").val();
		$.ajax({
			url: '{site_url}mrka_pengajuan/refresh_program/'+idrka,
			dataType: "json",
			success: function(data) {
				$("#idprogram").empty();
				$('#idprogram').append('<option value="#">- Pilih Program -</option>');
				$('#idprogram').append(data.detail);
			}
		});
	}
	
	//Termin
	// $("#add_dp_termin").click(function() {
			// var content = "";
			
			// if (!validate_detail_termin()) return false;
			// if ($("#rowindex_dp_cicilan").val() == '') {
				// content += "<tr>";
			// }
		// content+='<td><input class="form-control input-sm" readonly type="text" value="'+$("#xjenis_termin option:selected").text()+'"/></td>';	
		// content+='<td><input class="form-control input-sm" readonly type="text" name="judul_termin[]" value="'+$("#xjudul_termin").val()+'"</td>';
		// content+='<td><input class="form-control input-sm" readonly type="text" name="deskripsi_termin[]" value="'+$("#xdeskripsi_termin").val()+'"/></td>';
		// content+='<td><input class="form-control input-sm number" readonly type="text" name="nominal_termin[]" value="'+$("#xnominal_termin").val()+'"/></td>';
		// content+='<td>';
			// content+='<div class="input-group date">';
				// content+='<input class="js-datepicker form-control input-sm"  disabled type="text" name="tanggal_termin[]" value="'+$("#xtanggal_termin").val()+'" data-date-format="dd-mm-yyyy"/>';
				// content+='<span class="input-group-addon">';
					// content+='<span class="fa fa-calendar"></span>';
				// content+='</span>';
			// content+='</div>';
		// content+='</td>';
		// content+='<td><button type="button" class="btn btn-sm btn-danger hapus_termin" tabindex="9" id="cancel_dp_termin" title="Refresh"><i class="fa fa-times"></i></button></td>';
		// content+='<td style="display:none"><input class="form-control input-sm" type="text" name="jenis_termin[]" value="'+$("#xjenis_termin").val()+'"/></td>';
		// content+='<td style="display:none"><input class="form-control input-sm" type="text" name="status_dibayar_termin[]" value="0"/></td>';
			
			// if ($("#rowindex").val() != '') {
				// $('#tabel_dp_termin tbody tr:eq(' + $("#rowindex_dp_cicilan").val() + ')').html(content);
			// } else {
				// content += "</tr>";
				// $('#tabel_dp_termin tbody').append(content);
			// }
			// hitung_total_termin();
			// clear_input_termin();
	// });
	// function validate_detail_termin() {
		// // alert('sini');
		// if ($("#xjenis_termin").val() == "#") {
			// sweetAlert("Maaf...", "Tipe Transaksi Harus Diisi!", "error");
			// $("#xjenis_termin").focus();
			// return false;
		// }
		// if ($("#xjudul_termin").val() == "") {
			// sweetAlert("Maaf...", "Judul Harus Diisi!", "error");
			// $("#xjudul_termin").focus();
			// return false;
		// }
		// if ($("#xdeskripsi_termin").val() == "") {
			// sweetAlert("Maaf...", "Deskripsi Harus Diisi!", "error");
			// $("#xdeskripsi_termin").focus();
			// return false;
		// }
		// if ($("#xnominal_termin").val() == "" || $("#xnominal_termin").val() == "0") {
			// sweetAlert("Maaf...", "Nominal Harus Diisi!", "error");
			// $("#xnominal_termin").focus();
			// return false;
		// }
		// if ($("#xtanggal_termin").val() == "") {
			// sweetAlert("Maaf...", "Tanggal Harus Diisi!", "error");
			// $("#xtanggal_termin").focus();
			// return false;
		// }
		// var sisa=0;
		// sisa=parseFloat($("#grand_total").val()) - (parseFloat($("#xnominal_termin").val()) + parseFloat($("#total_dp_termin").val()));
		// if (sisa < 0){
			// sweetAlert("Maaf...", "Pembayaran Melebihi dari tagihan!", "error");
			// return false;
		// }
		
		
		// return true;
	// }
	// $(document).on("click", ".hapus_termin", function() {
		// var baris=$(this).closest('td').parent();		
		// baris.remove();
		// hitung_total_termin();
		// clear_input_termin();
	// });
	
	
	// function hitung_total_termin(){
		// var total_dp = 0;
		// $('#tabel_dp_termin tbody tr').each(function() {
				// console.log($(this).find('td:eq(3) input').val());
				// total_dp += parseFloat($(this).find('td:eq(3) input').val());
		// });
		// $("#total_dp_termin").val(total_dp);
		// $("#sisa_termin").val(parseFloat($("#grand_total").val()) - total_dp);
		
	// }
	// function clear_input_termin(){
		// $("#xjenis_termin").val('#').trigger('change')
		// $("#xjudul_termin").val('')
		// $("#xdeskripsi_termin").val('')
		// $("#xnominal_termin").val('0')
		// $("#xtanggal_termin").val('')
		// $('.number').number(true, 0, '.', ',');
	// }
	
	// // END TERMIN
	$(document).on("click", "#btn_open_modal_kegiatan", function() {
		$("#modal_kegiatan").modal('show');
		
	});
	function load_kegiatan(){
		var idrka=$("#idrka").val();
		var idprespektif=$("#idprespektif").val();
		var idprogram=$("#idprogram").val();
		
		$('#tabel_kegiatan').DataTable().destroy();
		table=$('#tabel_kegiatan').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": false,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mrka_pengajuan/load_kegiatan_reminder',
					type: "POST",
					dataType: 'json',
					data : {
						idrka:idrka,
						idprespektif:idprespektif,
						idprogram:idprogram,
						
					   }
				},
				"columnDefs": [
					{"targets": [0,1,2,3,4], "visible": false },
					{"targets": [10], "class": 'text-center' },
					
				]
			});
	}
	$(document).on("click",".pilih",function(){
		var table = $('#tabel_kegiatan').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,0).data()
		var bulan = table.cell(tr,1).data()
		$('#tabel_add tbody').empty();
		hitung_total();
		$.ajax({
			url: '{site_url}mrka_pengajuan/get_pilih_kegiatan/'+id+'/'+bulan,
			dataType: "json",
			success: function(data) {
				$("#nama_kegiatan").val(data.nama);
				$("#bulan").val(bulan);
		// alert(id+' '+bulan);
				$("#idrka_kegiatan").val(id);
				$("#idunit_pengaju").val(data.idunit).trigger('change');
				$("#idunit").val(data.idunit).trigger('change');
				$("#modal_kegiatan").modal('hide');
			}
		});
		
		
	});
	$(document).on("click","#btn_filter_rka",function(){
		load_kegiatan();
		
	});
</script>