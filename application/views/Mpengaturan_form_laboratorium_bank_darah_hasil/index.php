<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <a href="{base_url}mpengaturan_form_laboratorium_bank_darah_hasil" class="btn"><i class="fa fa-reply"></i></a>
            </li>
        </ul>
        <h3 class="block-title">{title}</h3>
    </div>
    <div class="block-content">
        <?php echo form_open('mpengaturan_form_laboratorium_bank_darah_hasil/save','class="form-horizontal push-10-t" id="form-work"') ?>

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <!-- Formulir untuk Edit Nomor Laboratorium -->
                        <div class="form-group">
                            <label class="col-md-12" for="edit_nomor_laboratorium">Edit Nomor Laboratorium</label>
                            <div class="col-md-12">
                                <select name="edit_nomor_laboratorium" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="1" <?= ($edit_nomor_laboratorium == '1' ? 'selected' : ''); ?>>IZINKAN</option>
                                    <option value="0" <?= ($edit_nomor_laboratorium == '0' ? 'selected' : ''); ?>>TIDAK</option>
                                </select>
                            </div>
                        </div>

                        <!-- Formulir untuk Edit Waktu Pengambilan -->
                        <div class="form-group">
                            <label class="col-md-12" for="edit_waktu_pengambilan">Edit Waktu Pengambilan</label>
                            <div class="col-md-12">
                                <select name="edit_waktu_pengambilan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="1" <?= ($edit_waktu_pengambilan == '1' ? 'selected' : ''); ?>>IZINKAN</option>
                                    <option value="0" <?= ($edit_waktu_pengambilan == '0' ? 'selected' : ''); ?>>TIDAK</option>
                                </select>
                            </div>
                        </div>

                        <!-- Formulir untuk Action Periksa Keluar -->
                        <div class="form-group">
                            <label class="col-md-12" for="action_periksa_keluar">Action Periksa Keluar</label>
                            <div class="col-md-12">
                                <select name="action_periksa_keluar" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="1" <?= ($action_periksa_keluar == '1' ? 'selected' : ''); ?>>IZINKAN</option>
                                    <option value="0" <?= ($action_periksa_keluar == '0' ? 'selected' : ''); ?>>TIDAK</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <!-- Formulir untuk Edit Prioritas Pemeriksaan -->
                        <div class="form-group">
                            <label class="col-md-12" for="edit_prioritas_pemeriksaan">Edit Prioritas Pemeriksaan</label>
                            <div class="col-md-12">
                                <select name="edit_prioritas_pemeriksaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="1" <?= ($edit_prioritas_pemeriksaan == '1' ? 'selected' : ''); ?>>IZINKAN</option>
                                    <option value="0" <?= ($edit_prioritas_pemeriksaan == '0' ? 'selected' : ''); ?>>TIDAK</option>
                                </select>
                            </div>
                        </div>

                        <!-- Formulir untuk Edit Petugas Pengambilan -->
                        <div class="form-group">
                            <label class="col-md-12" for="edit_petugas_pengambilan">Edit Petugas Pengambilan</label>
                            <div class="col-md-12">
                                <select name="edit_petugas_pengambilan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="1" <?= ($edit_petugas_pengambilan == '1' ? 'selected' : ''); ?>>IZINKAN</option>
                                    <option value="0" <?= ($edit_petugas_pengambilan == '0' ? 'selected' : ''); ?>>TIDAK</option>
                                </select>
                            </div>
                        </div>

                        <!-- Formulir untuk Action Flag -->
                        <div class="form-group">
                            <label class="col-md-12" for="action_flag">Action Flag</label>
                            <div class="col-md-12">
                                <select name="action_flag" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="1" <?= ($action_flag == '1' ? 'selected' : ''); ?>>IZINKAN</option>
                                    <option value="0" <?= ($action_flag == '0' ? 'selected' : ''); ?>>TIDAK</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <!-- Formulir untuk Warna Flag -->
                        <div class="form-group">
                            <label class="col-md-12" for="warna_flag">Warna Flag</label>
                            <div class="col-md-12">
                                <div class="js-colorpicker input-group colorpicker-element">
                                    <input class="form-control" type="text" name="warna_flag" value="{warna_flag}">
                                    <span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <!-- Formulir untuk Warna Tidak Normal -->
                        <div class="form-group">
                            <label class="col-md-12" for="warna_tidak_normal">Warna Tidak Normal</label>
                            <div class="col-md-12">
                                <div class="js-colorpicker input-group colorpicker-element">
                                    <input class="form-control" type="text" name="warna_tidak_normal" value="{warna_tidak_normal}">
                                    <span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <!-- Formulir untuk Edit Pasien Puasa -->
                        <div class="form-group">
                            <label class="col-md-12" for="edit_pasien_puasa">Edit Pasien Puasa</label>
                            <div class="col-md-12">
                                <select name="edit_pasien_puasa" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="1" <?= ($edit_pasien_puasa == '1' ? 'selected' : ''); ?>>IZINKAN</option>
                                    <option value="0" <?= ($edit_pasien_puasa == '0' ? 'selected' : ''); ?>>TIDAK</option>
                                </select>
                            </div>
                        </div>
                        
                        <!-- Formulir untuk Action Tidak Normal -->
                        <div class="form-group">
                            <label class="col-md-12" for="action_tidak_normal">Action Tidak Normal</label>
                            <div class="col-md-12">
                                <select name="action_tidak_normal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="1" <?= ($action_tidak_normal == '1' ? 'selected' : ''); ?>>IZINKAN</option>
                                    <option value="0" <?= ($action_tidak_normal == '0' ? 'selected' : ''); ?>>TIDAK</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <!-- Formulir untuk Edit Pengiriman Hasil -->
                        <div class="form-group">
                            <label class="col-md-12" for="edit_pengiriman_hasil">Edit Pengiriman Hasil</label>
                            <div class="col-md-12">
                                <select name="edit_pengiriman_hasil" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="1" <?= ($edit_pengiriman_hasil == '1' ? 'selected' : ''); ?>>IZINKAN</option>
                                    <option value="0" <?= ($edit_pengiriman_hasil == '0' ? 'selected' : ''); ?>>TIDAK</option>
                                </select>
                            </div>
                        </div>

                        <!-- Formulir untuk Action Nilai Kritis -->
                        <div class="form-group">
                            <label class="col-md-12" for="action_nilai_kritis">Action Nilai Kritis</label>
                            <div class="col-md-12">
                                <select name="action_nilai_kritis" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                                    <option value="1" <?= ($action_nilai_kritis == '1' ? 'selected' : ''); ?>>IZINKAN</option>
                                    <option value="0" <?= ($action_nilai_kritis == '0' ? 'selected' : ''); ?>>TIDAK</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <!-- Formulir untuk Jenis Petugas Proses -->
                        <div class="form-group">
                            <label class="col-md-12" for="jenis_petugas_proses">Jenis Petugas Proses</label>
                            <div class="col-md-12">
                                <select name="jenis_petugas_proses[]" class="js-select2 form-control" style="width: 100%;" multiple>
                                    <?php foreach (list_variable_ref(21) as $row) { ?>
                                    <option value="<?php echo $row->id; ?>" <?php echo '' === $jenis_petugas_proses && '1' === $row->st_default ? 'selected' : ''; ?> <?php echo in_array($row->id, $jenis_petugas_proses) ? 'selected' : ''; ?>><?php echo $row->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <!-- Formulir untuk Warna Nilai Kritis -->
                        <div class="form-group">
                            <label class="col-md-12" for="warna_nilai_kritis">Warna Nilai Kritis</label>
                            <div class="col-md-12">
                                <div class="js-colorpicker input-group colorpicker-element">
                                    <input class="form-control" type="text" name="warna_nilai_kritis" value="{warna_nilai_kritis}">
                                    <span class="input-group-addon"><i style="background-color: rgb(33, 57, 88);"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 text-right">
                <button class="btn btn-success" type="submit">Update</button>
                <a href="{base_url}mpengaturan_form_laboratorium_bank_darah_hasil" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
            </div>
        </div>
        <br>
    </div>
</div>
