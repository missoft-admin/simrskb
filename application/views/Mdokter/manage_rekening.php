<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mdokter" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">DOKTER</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mdokter/save','class="form-horizontal push-10-t" onsubmit="return validate_final()"') ?>
			
			<div class="form-group" >
				<label class="col-md-3 control-label" for="nama">Nama</label>
				<div class="col-md-7">
					<input type="text" disabled class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}">
					<input type="hidden" disabled class="form-control" id="iddokter" placeholder="iddokter" name="iddokter" value="{id}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Kategori</label>
				<div class="col-md-7">
					<input type="text" disabled class="form-control" id="nama" placeholder="Nama" name="nama" value="{kategori}">
				</div>
			</div>
			
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>

</div>
<? if ($id !=''){ ?>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
       
    </ul>
		<h3 class="block-title">INFO REKENING</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
			<input type="hidden" class="form-control input-sm number" id="iddokter" name="iddokter"  value="{id}"/>	
			<input type="hidden" class="form-control input-sm number" id="rek_id" name="rek_id"  value=""/>	
			<div class="form-group">
				<div class="col-md-12">
					<div class="table-responsive">
					<table class="table table-bordered table-striped" id="index_list" style="width: 100%;">
						<thead>
							<tr>
								<th hidden>Actions</th>
								<th hidden>Actions</th>
								<th style="width: 25%;">Bank</th>
								<th style="width: 15%;">No Rekening</th>
								<th style="width: 15%;">Atas Nama</th>								
								<th style="width: 10%;">Status</th>								
								<th style="width: 15%;">Keterangan</th>								
								<th style="width: 8%;">Actions</th>
								<th style="width: 8%;">Actions</th>
							</tr>
							<tr>
								<td hidden></td>
								<td hidden></td>
								<td>
									<select name="idbank" id="idbank" tabindex="16"  style="width: 100%" data-placeholder="Bank" class="js-select2 form-control input-sm">
										<option value="#" selected>-Bank-</option>
										<?foreach($list_bank as $row){?>
										<option value="<?=$row->id?>"><?=$row->bank?></option>
										<?}?>
										
									</select>										
								</td>
								<td>
									<input type="text" class="form-control input-sm number" id="norek" name="norek"  value=""/>										
								</td>
								<td>									
									<input type="text" class="form-control input-sm number" id="atas_nama" name="atas_nama"  value="10"/>
								</td>
								
								
								<td>									
									<select name="pilih_default" id="pilih_default" tabindex="16"  style="width: 100%" data-placeholder="Bank" class="js-select2 form-control input-sm">
										<option value="1" selected>Primary</option>
										<option value="0">Secondary</option>
										
									</select>
								</td>
								<td>
									<input type="text" class="form-control input-sm number" id="keterangan" name="keterangan"  value=""/>										
								</td>
								<td>									
									<button type="button" class="btn btn-xs btn-primary" tabindex="8" id="btn_simpan" title="Masukan Item"><i class="fa fa-save"></i> Simpan</button>
									<button type="button" class="btn btn-xs btn-success" tabindex="8" id="btn_update" title="Update"><i class="fa fa-save"></i> Update</button>
									<button type="button" class="btn btn-xs btn-warning" tabindex="8" id="btn_batal" title="Batal"><i class="fa fa-refresh"></i></button>
								</td>
								<td></td>
							</tr>
						</thead>
						<tbody></tbody>							
					</table>
					
				</div>
				</div>
			</div>
			<?php echo form_close() ?>
	</div>

</div>
<?}?>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		clear_all();
		
		LoadRekening();
	})	
	function clear_all(){
		$('#idbank').val('#').trigger('change');
		$('#pilih_default').val('1').trigger('change');
		$('#norek').val('');
		$('#rek_id').val('');
		$('#atas_nama').val('');
		$('#keterangan').val('');
		$("#btn_simpan").show()
		$("#btn_update").hide()
		$("#btn_batal").hide()
	}
	$(document).on("click","#btn_batal",function(){	
		clear_all();
		
	});
	
	$(document).on("click","#btn_simpan",function(){	
		var idbank=$("#idbank").val();
		var norek=$("#norek").val();
		var iddokter=$("#iddokter").val();
		var atas_nama=$("#atas_nama").val();
		if (idbank=='#'){
			sweetAlert("Maaf...", "Bank Harus diisi!", "error");
			return false;
		}
		if (norek==''){
			sweetAlert("Maaf...", "No Rekening Harus diisi!", "error");
			return false;
		}
		if (atas_nama==''){
			sweetAlert("Maaf...", "Atas Nama Harus diisi!", "error");
			return false;
		}
		
		swal({
				title: "Anda Yakin ?",
				text : "Untuk Save Data ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				save_rekening();
			});
	});
	function save_rekening(){
		var idbank=$("#idbank").val();
		var norek=$("#norek").val();
		var iddokter=$("#iddokter").val();
		var atas_nama=$("#atas_nama").val();
		var pilih_default=$("#pilih_default").val();
		var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}mdokter/save_rekening',
			type: 'POST',
			data: {idbank: idbank,norek: norek,iddokter:iddokter,atas_nama:atas_nama,pilih_default:pilih_default},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Save Data'});
				table.ajax.reload( null, false );
				clear_all();
			}
		});
		
	}
	$(document).on("click","#btn_update",function(){	
		var idbank=$("#idbank").val();
		var norek=$("#norek").val();
		var iddokter=$("#iddokter").val();
		var atas_nama=$("#atas_nama").val();
		if (idbank=='#'){
			sweetAlert("Maaf...", "Bank Harus diisi!", "error");
			return false;
		}
		if (norek==''){
			sweetAlert("Maaf...", "No Rekening Harus diisi!", "error");
			return false;
		}
		if (atas_nama==''){
			sweetAlert("Maaf...", "Atas Nama Harus diisi!", "error");
			return false;
		}
		swal({
				title: "Anda Yakin ?",
				text : "Untuk Update Data ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				update_rekening();
			});
	});
	function update_rekening(){
		var rek_id=$("#rek_id").val();
		var idbank=$("#idbank").val();
		var norek=$("#norek").val();
		var iddokter=$("#iddokter").val();
		var atas_nama=$("#atas_nama").val();
		var pilih_default=$("#pilih_default").val();
		var keterangan=$("#keterangan").val();
		var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}mdokter/update_rekening',
			type: 'POST',
			data: {rek_id: rek_id,idbank: idbank,norek: norek,iddokter:iddokter,atas_nama:atas_nama,keterangan:keterangan,pilih_default:pilih_default},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Update Data'});
				table.ajax.reload( null, false );
				clear_all();
				// $("#cover-spin").hide();
				// filter_form();
			}
		});
		
	}
	function LoadRekening() {
		var iddokter=$("#iddokter").val();
		// alert(iddokter);
		
		// $('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 50,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"lengthChange": false,
		"order": [],
		"ajax": {
			url: '{site_url}mdokter/LoadRekening/',
			type: "POST",
			dataType: 'json',
			data: {
				iddokter: iddokter,
			}
		},
		columnDefs: [
					{"targets": [0,1,8], "visible": false },
					 // {  className: "text-right", targets:[2] },
					 {  className: "text-center", targets:[2,5,7] },
					 // { "width": "30%", "targets": [0,1] },
					 // { "width": "20%", "targets": [0,1,2,3,4] },
					 // { "width": "10%", "targets": [2,4] }
					 // { "width": "15%", "targets": [2,13] }

					]
		});
	}
	$(document).on("click",".edit",function(){	
		var table = $('#index_list').DataTable();
        tr = table.row($(this).parents('tr')).index()
		var rek_id=table.cell(tr,0).data();
		var idbank=table.cell(tr,1).data();
		var norek=table.cell(tr,3).data();
		var atas_nama=table.cell(tr,4).data();
		var keterangan=table.cell(tr,6).data();
		var pilih_default=table.cell(tr,8).data();
		$('#rek_id').val(rek_id);
		$('#idbank').val(idbank).trigger('change');
		$('#pilih_default').val(pilih_default).trigger('change');
		$('#norek').val(norek);
		$('#keterangan').val(keterangan);
		$('#atas_nama').val(atas_nama);
		$("#btn_simpan").hide()
		$("#btn_update").show()
		$("#btn_batal").show()
	});
	$(document).on("click",".hapus",function(){	
		var table = $('#index_list').DataTable();
		tr = table.row($(this).parents('tr')).index()
		var rek_id=table.cell(tr,0).data();
		$('#rek_id').val(rek_id);
        swal({
				title: "Anda Yakin ?",
				text : "Untuk Hapus data?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				hapus_data();
			});
	});
	function hapus_data(){
		var rek_id=$("#rek_id").val();
		var table = $('#index_list').DataTable();
		$.ajax({
			url: '{site_url}mdokter/hapus_data',
			type: 'POST',
			data: {rek_id: rek_id},
			complete: function() {
				$.toaster({priority : 'success', title : 'Succes!', message : ' Hapus Data'});
				table.ajax.reload( null, false );
				clear_all();
				// $("#cover-spin").hide();
				// filter_form();
			}
		});
		
	}
	
	function validate_final(){
		var tipe_pemilik=$("#tipe_pemilik").val();
		if ($("#tipe_pemilik").val()=='#'){
			sweetAlert("Maaf...", "Tentukan Tipe Pemilik", "error");
			return false;
		}
		if (tipe_pemilik=='1' && $("#idpegawai").val()=='#'){
			sweetAlert("Maaf...", "Tentukan Pegawai", "error");
			return false;
		}
		if (tipe_pemilik=='2' && $("#iddokter").val()=='#'){
			sweetAlert("Maaf...", "Tentukan Dokter", "error");
			return false;
		}
		if (tipe_pemilik=='3' && $("#nama").val()==''){
			sweetAlert("Maaf...", "Tentukan Nama Pemilik", "error");
			return false;
		}
		if ($("#npwp").val()==''){
			sweetAlert("Maaf...", "Tentukan NPWP Pemilik", "error");
			return false;
		}
		return true;

		
	}
</script>