<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mdokter" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mdokter/save_setting','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-3 control-label" for="">Nama Dokter</label>
				<div class="col-md-7">
					<input type="text" class="form-control" value="{nama_dokter}" disabled>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="">Kategori Dokter</label>
				<div class="col-md-7">
          <input type="text" class="form-control" value="{kategori_dokter}" disabled>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12">
  				<table class="table table-bordered">
            <thead>
              <tr>
                <th>Tanggal</th>
                <th>Jatuh Tempo Bayar</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <select name="tanggal" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
        						<?php for ($i=1; $i<=31; $i++) { ?>
                      <option value="<?=$i?>" <?=($tanggal == $i ? 'selected="selected"':'')?>><?=$i?></option>
                    <? } ?>
        					</select>
                </td>
                <td>
                  <input type="number" class="form-control" name="jatuh_tempo" value="{jatuh_tempo}">
                </td>
                <td>
                  <button class="btn btn-success" type="submit"><?=($id ? 'Ubah':'Tambahkan')?></button>
                </td>
              </tr>
            </tbody>
          </table>
				</div>
			</div>

      <input type="hidden" name="iddokter" value="{iddokter}">

			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>

      <hr>

      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Tanggal Pembayaran</th>
            <th>Jatuh Tempo Bayar</th>
            <th>Last Update</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($listSetting as $row) { ?>
            <tr>
              <td class="text-center"><?=$row->tanggal?></td>
              <td class="text-center"><?=$row->jatuh_tempo?></td>
              <td class="text-center"><?=$row->updated_at?> - <?=$row->updated_by?></td>
              <td>
                <a href="{base_url}mdokter/setting/{iddokter}/<?=$row->id?>" data-toggle="tooltip" title="Ubah" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                <a href="{base_url}mdokter/delete_setting/{iddokter}/<?=$row->id?>" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
              </td>
            </tr>
          <? } ?>
        </tbody>
      </table>
      <br>
      <br>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(".number").number(true, 2, '.', ',');
</script>
