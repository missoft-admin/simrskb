<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block">
	<div class="block-header">
		<?php if (UserAccesForm($user_acces_form,array('8'))): ?>
			<ul class="block-options">
		        <li>
		            <a href="{base_url}mdokter/create" class="btn"><i class="fa fa-plus"></i></a>
		        </li>
		    </ul>
		<?php endif ?>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<?php if (UserAccesForm($user_acces_form,array('7'))): ?>
		<hr style="margin-top:0px">
		<?php echo form_open('mdokter/filter','class="form-horizontal" id="form-work"') ?>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
					<label class="col-md-4 control-label" for="asalpasien">Kategori</label>
					<div class="col-md-8">
						<select name="idkategori" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
							<option value="0">All</option>
							<option value="1" <?=($idkategori == 1 ? 'selected="selected"':'')?>>Umum</option>
							<option value="2" <?=($idkategori == 2 ? 'selected="selected"':'')?>>Spesialis Radiologi</option>
							<option value="3" <?=($idkategori == 3 ? 'selected="selected"':'')?>>Spesialis Patologi Anatomi</option>
							<option value="4" <?=($idkategori == 4 ? 'selected="selected"':'')?>>Spesialis Anesthesi</option>
							<option value="5" <?=($idkategori == 5 ? 'selected="selected"':'')?>>Spesialis Lainnya</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close() ?>
		<hr>
		<?php endif ?>
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="datatable-simrs">
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">NIP</th>
					<th rowspan="2">Nama</th>
					<th rowspan="2">Jenis Kelamin</th>
					<th rowspan="2">Kategori Dokter</th>
					<th rowspan="2">Pajak</th>
					<th class="text-center" colspan="2">Potongan RS</th>
					<th rowspan="2">Aksi</th>
				</tr>
				<tr>
					<th>Pagi</th>
					<th>Siang</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		</div>
	</div>
</div>

<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	// Initialize when page loads
	jQuery(function(){ BaseTableDatatables.init();
		$('#datatable-simrs').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mdokter/getIndex/'+{idkategori},
					type: "POST",
					dataType: 'json'
				},
				"columnDefs": [
					{ "width": "10%", "targets": 0, "orderable": true },
					{ "width": "10%", "targets": 1, "orderable": true },
					{ "width": "15%", "targets": 2, "orderable": true },
					{ "width": "10%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 4, "orderable": true },
					{ "width": "10%", "targets": 5, "orderable": true },
					{ "width": "10%", "targets": 6, "orderable": true },
					{ "width": "10%", "targets": 7, "orderable": true },
					{ "width": "15%", "targets": 8, "orderable": true }
				]
			});
	});
</script>
