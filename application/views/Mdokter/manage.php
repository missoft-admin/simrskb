<?php echo ErrorSuccess($this->session)?>
<?php if ($error != '') {
    echo ErrorMessage($error);
}?>

<div class="block">
	<div class="block-header">
		<ul class="block-options">
        <li>
            <a href="{base_url}mdokter" class="btn"><i class="fa fa-reply"></i></a>
        </li>
    </ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('mdokter/save', 'class="form-horizontal push-10-t"') ?>
			<?php if ($foto != '') { ?>
				<div class="form-group">
				<label class="col-md-3 control-label" for=""></label>
				<div class="col-md-7">
					<img class="img-avatar" src="{upload_path}dokter/{foto}" />
				</div>
			</div>
				<?php } ?>
				<div class="form-group">
					<label class="col-md-3 control-label" for="">Foto</label>
					<div class="col-md-7">
						<div class="box">
							<input type="file" id="file-3" class="inputfile inputfile-3" style="display:none;" name="foto" value="{foto}" />
							<label for="file-3"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
						</div>
					</div>
				</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nip">NIP</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nip" placeholder="NIP" name="nip" value="{nip}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nama">Nama</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="{nama}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="jeniskelamin">Jenis Kelamin</label>
				<div class="col-md-7">
					<select name="jeniskelamin" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($jeniskelamin == 1 ? 'selected="selected"':'')?>>Laki-laki</option>
						<option value="2" <?=($jeniskelamin == 2 ? 'selected="selected"':'')?>>Perempuan</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="alamat">Alamat</label>
				<div class="col-md-7">
					<textarea class="form-control" id="alamat" placeholder="Alamat" name="alamat" required="" aria-required="true">{alamat}</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tempatlahir">Tempat Lahir</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="tempatlahir" placeholder="Tempat Lahir" name="tempatlahir" value="{tempatlahir}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="tanggallahir">Tanggal Lahir</label>
				<div class="col-md-7">
					<input class="js-datepicker form-control" type="text" id="tanggallahir" name="tanggallahir" value="{tanggallahir}" data-date-format="dd/mm/yyyy" placeholder="HH/BB/TT">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="telepon">Telepon</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="telepon" placeholder="Telepon" name="telepon" value="{telepon}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="email">Email</label>
				<div class="col-md-7">
					<input type="text" class="form-control" id="email" placeholder="Email" name="email" value="{email}" required="" aria-required="true">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label" for="idkategori">Kategori Dokter</label>
				<div class="col-md-7">
					<select name="idkategori" <?=(UserAccesForm($user_acces_form, array('12'))=='0' && $id != '')?'disabled':''?> class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
						<option value="">Pilih Opsi</option>
						<option value="1" <?=($idkategori == 1 ? 'selected="selected"':'')?>>Umum</option>
						<option value="2" <?=($idkategori == 2 ? 'selected="selected"':'')?>>Spesialis Radiologi</option>
						<option value="3" <?=($idkategori == 3 ? 'selected="selected"':'')?>>Spesialis Patologi Anatomi</option>
						<option value="4" <?=($idkategori == 4 ? 'selected="selected"':'')?>>Spesialis Anesthesi</option>
						<option value="5" <?=($idkategori == 5 ? 'selected="selected"':'')?>>Spesialis Lainnya</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label" for="nominaltransport">Nominal Transport</label>
				<div class="col-md-7">
					<input type="text"  <?=(UserAccesForm($user_acces_form, array('13'))=='0' && $id != '')?'readonly':''?> class="form-control number-zero-comma" id="nominaltransport" placeholder="Nominal Transport" name="nominaltransport" value="{nominaltransport}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="pajak">Persentase Perujuk (%)</label>
				<div class="col-md-7">
					<input type="text" <?=(UserAccesForm($user_acces_form, array('13'))=='0' && $id != '')?'readonly':''?> class="form-control number" id="persentaseperujuk" placeholder="Persentase Perujuk (%)" name="persentaseperujuk" value="{persentaseperujuk}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-12 control-label"><h5 align="left"><b>Pajak</b></h5></label>
				<div class="col-md-12">
					<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="nip">NPWP</label>
				<div class="col-md-7">
					<input type="text" class="form-control" placeholder="NPWP" name="npwp" value="{npwp}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="pajak">Pajak (%) ID<?=$id?></label>
				<div class="col-md-7">
					<input type="text" <?=(UserAccesForm($user_acces_form, array('14'))=='0' && $id != '')?'readonly':''?> class="form-control number" id="pajak" placeholder="Pajak (%)" name="pajak" value="{pajak}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="potonganusg">Pajak Ranap (%)</label>
				<div class="col-md-7">
					<input type="text"  <?=(UserAccesForm($user_acces_form, array('14'))=='0' && $id != '')?'readonly':''?>  class="form-control number" id="pajakranap" placeholder="Pajak Ranap (%)" name="pajakranap" value="{pajakranap}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="pajakods">Pajak ODS (%)</label>
				<div class="col-md-7">
					<input type="text"  <?=(UserAccesForm($user_acces_form, array('14'))=='0' && $id != '')?'readonly':''?>  class="form-control number" id="pajakods" placeholder="Pajak ODS (%)" name="pajakods" value="{pajakods}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 10px;">
				<label class="col-md-12 control-label"><h5 align="left"><b>Potongan RS</b></h5></label>
				<div class="col-md-12">
					<hr style="margin-top: 10px;margin-bottom: 5px;background: #000;">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="potonganrspagi">Potongan Rajal Pagi (%)</label>
				<div class="col-md-7">
					<input type="text" <?=(UserAccesForm($user_acces_form, array('15'))=='0' && $id != '')?'readonly':''?> class="form-control number" id="potonganrspagi" placeholder="Potongan Pagi (%)" name="potonganrspagi" value="{potonganrspagi}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="potonganrssiang">Potongan Rajal Siang (%)</label>
				<div class="col-md-7">
					<input type="text" <?=(UserAccesForm($user_acces_form, array('15'))=='0' && $id != '')?'readonly':''?>  class="form-control number" id="potonganrssiang" placeholder="Potongan Siang (%)" name="potonganrssiang" value="{potonganrssiang}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="potonganusg">Potongan USG (%)</label>
				<div class="col-md-7">
					<input type="text"  <?=(UserAccesForm($user_acces_form, array('15'))=='0' && $id != '')?'readonly':''?> class="form-control number" id="potonganusg" placeholder="Potongan USG (%)" name="potonganusg" value="{potonganusg}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="potonganusg">Potongan Ranap Pasien RS (%)</label>
				<div class="col-md-7">
					<input type="text"  <?=(UserAccesForm($user_acces_form, array('15'))=='0' && $id != '')?'readonly':''?> class="form-control number" id="potonganrsranap" placeholder="Potongan Ranap Pagi (%)" name="potonganrsranap" value="{potonganrsranap}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="potonganusg">Potongan Ranap Pasien Pribadi (%)</label>
				<div class="col-md-7">
					<input type="text"  <?=(UserAccesForm($user_acces_form, array('15'))=='0' && $id != '')?'readonly':''?> class="form-control number" id="potonganrsranapsiang" placeholder="Potongan Ranap Siang (%)" name="potonganrsranapsiang" value="{potonganrsranapsiang}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="potonganrsods">Potongan ODS Pasien RS (%)</label>
				<div class="col-md-7">
					<input type="text"  <?=(UserAccesForm($user_acces_form, array('15'))=='0' && $id != '')?'readonly':''?> class="form-control number" id="potonganrsods" placeholder="Potongan ODS Pagi (%)" name="potonganrsods" value="{potonganrsods}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="potonganrsodssiang">Potongan ODS Pasien Pribadi (%)</label>
				<div class="col-md-7">
					<input type="text"  <?=(UserAccesForm($user_acces_form, array('15'))=='0' && $id != '')?'readonly':''?> class="form-control number" id="potonganrsodssiang" placeholder="Potongan ODS Siang (%)" name="potonganrsodssiang" value="{potonganrsodssiang}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="potonganfeers">Potongan Fee Rujukan Pasien RS (%)</label>
				<div class="col-md-7">
					<input type="text"  <?=(UserAccesForm($user_acces_form, array('15'))=='0' && $id != '')?'readonly':''?> class="form-control number" id="potonganfeers" placeholder="Potongan Fee Rujukan Pasien RS (%)" name="potonganfeers" value="{potonganfeers}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="potonganfeerspribadi">Potongan Fee Rujukan Pasien Pribadi (%)</label>
				<div class="col-md-7">
					<input type="text"  <?=(UserAccesForm($user_acces_form, array('15'))=='0' && $id != '')?'readonly':''?> class="form-control number" id="potonganfeerspribadi" placeholder="Potongan Fee Rujukan Pasien Pribadi (%)" name="potonganfeerspribadi" value="{potonganfeerspribadi}" required="" aria-required="true">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button class="btn btn-success" type="submit">Simpan</button>
					<a href="{base_url}mdokter" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('id', $id); ?>
			<?php echo form_close() ?>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script src="{plugins_path}masked-inputs/jquery.maskedinput.min.js"></script>
<script src="{plugins_path}jquery-inputmask/jquery.inputmask.bundle.min.js" ></script>
<script type="text/javascript">
	$("#npwp").mask('99.999.999.9-999.999');

	$(".number").number(true, 2, '.', ',');
	$(".number-zero-comma").number(true, 0, '.', ',');
	$("#pajak, #potonganrspagi, #potonganrssiang").keyup(function(){
		if($(this).val() > 100){
			$(this).val(100);
		}
	});
</script>
