<div class="modal fade in black-overlay" id="EditPendapatanNonPelayananModal" role="dialog" aria-hidden="true" style="z-index: 1041;">
	<div class="modal-dialog modal-lg modal-dialog-popout" style="width: 50%;">
		<div class="modal-content">
			<div class="block block-themed">
				<div class="block-header bg-success">
					<ul class="block-options">
						<li>
							<button data-dismiss="modal" type="button" class="close"><i class="si si-close"></i></button>
						</li>
					</ul>
					<h3 class="block-title">PENDAPATAN NON PELAYANAN</h3>
				</div>
				<div class="block-content">
					<div class="row">
						<form class="form-horizontal">
							<input type="hidden" id="tPendapatanNonPelayananRowId" readonly value="">
							<div class="col-md-12">
								<div class="form-group formDokter">
									<label class="col-md-3 control-label">Dokter</label>
									<div class="col-md-9">
                    <select id="tPendapatanNonPelayananIdDokter" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi..">
        							<option value="" selected>Pilih Dokter</option>
        							<?php foreach (get_all('mdokter', array('status' => '1')) as $row) { ?>
        								<option value="<?=$row->id?>"><?=$row->nama?></option>
        							<?php } ?>
        						</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Deskripsi</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-sm" id="tPendapatanNonPelayananDeskripsi" placeholder="Deskripsi" value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Jasa Medis</label>
									<div class="col-md-9">
										<input type="text" class="form-control number input-sm" id="tPendapatanNonPelayananJasaMedis" placeholder="Jasa Medis" value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Potongan RS</label>
									<div class="col-md-9">
										<input type="text" class="form-control number input-sm" id="tPendapatanNonPelayananNominalPotonganRS" placeholder="Potongan RS" value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Pajak Dokter</label>
									<div class="col-md-9">
										<input type="text" class="form-control number input-sm" id="tPendapatanNonPelayananNominalPajakDokter" placeholder="Pajak Dokter" value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Jasa Medis Netto</label>
									<div class="col-md-9">
										<input type="text" class="form-control number input-sm" id="tPendapatanNonPelayananJasaMedisNetto" placeholder="Jasa Medis Netto" readonly value="">
									</div>
								</div>
                <div class="form-group">
									<label class="col-md-3 control-label">Status Pendapatan</label>
									<div class="col-md-9">
                    <select id="tPendapatanNonPelayananStatusPendapatan" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
                      <option value="">Pilih Opsi</option>
                      <option value="1">Rutin</option>
        							<option value="2">Non Rutin</option>
        						</select>
									</div>
								</div>
								<div class="form-group formRutin" hidden>
									<label class="col-md-3 control-label">Status Periode</label>
									<div class="col-md-9">
                    <select id="tPendapatanNonPelayananStatusPeriode" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
        							<option value="">Pilih Opsi</option>
        							<option value="1">Setiap Tanggal</option>
        							<option value="2">Setiap Bulan</option>
        						</select>
									</div>
								</div>
								<div class="form-group formNonRutin" hidden>
									<label class="col-md-3 control-label">Tanggal Pembayaran</label>
									<div class="col-md-9">
                    <select id="tPendapatanNonPelayananTanggalPembayaran" class="form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
											<option value="">Pilih Opsi</option>
										</select>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-primary" id="saveDataPendapatanNonPelayanan" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Simpan</button>
				<button class="btn btn-sm btn-default" id="cancelDataPendapatanNonPelayanan" type="button" data-dismiss="modal">Batal</button>
			</div>
		</div>
	</div>
</div>

<script src="{js_path}core/jquery.min.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('.number').number(true, 0, '.', ',');

    $(document).on('click', '.addPendapatanNonPelayanan', function() {
      $("#tPendapatanNonPelayananRowId").val('');
      $("#tPendapatanNonPelayananIdDokter").val('');
      $("#tPendapatanNonPelayananJasaMedis").val('');
      $("#tPendapatanNonPelayananNominalPotonganRS").val('');
      $("#tPendapatanNonPelayananNominalPajakDokter").val('');
      $("#tPendapatanNonPelayananJasaMedisNetto").val('');
      $("#tPendapatanNonPelayananStatusPendapatan").val('');
      $("#tPendapatanNonPelayananStatusPeriode").val('');
      $("#tPendapatanNonPelayananTanggalPembayaran").val('');
    });

    $(document).on('change', '#tPendapatanNonPelayananIdDokter', function() {
			loadPeriodePembayaranHonorDokter($(this).val());
		});

    $(document).on('change', '#tPendapatanNonPelayananStatusPendapatan', function() {
			if ($(this).val() == '1') {
        $(".formRutin").show();
        $(".formNonRutin").hide();
      } else if ($(this).val() == '2') {
        $(".formRutin").hide();
        $(".formNonRutin").show();
      }
		});

    $(document).on('keyup', '#tPendapatanNonPelayananJasaMedis, #tPendapatanNonPelayananNominalPotonganRS, #tPendapatanNonPelayananNominalPajakDokter', function() {
      var jasamedis = parseFloat($("#tPendapatanNonPelayananJasaMedis").val());
      var nominal_potongan_rs = parseFloat($("#tPendapatanNonPelayananNominalPotonganRS").val());
      var nominal_pajak_dokter = parseFloat($("#tPendapatanNonPelayananNominalPajakDokter").val());
      var jasamedis_netto = (jasamedis ? jasamedis : 0) - (nominal_potongan_rs ? nominal_potongan_rs : 0) - (nominal_pajak_dokter ? nominal_pajak_dokter : 0);
      $("#tPendapatanNonPelayananJasaMedisNetto").val(jasamedis_netto);
    });

    $(document).on('click', '.editPendapatanNonPelayanan', function() {
		  var idrow = $(this).data('idrow');
		  var iddokter = $(this).data('iddokter');
		  var tanggal_pembayaran = $(this).data('tanggal_pembayaran');
		  var tanggal_jatuhtempo = $(this).data('tanggal_jatuhtempo');
		  var status_pendapatan = $(this).data('status_pendapatan');
		  var status_periode = $(this).data('status_periode');
			var deskrpsi, jasamedis, nominal_potongan_rs, nominal_pajak_dokter, jasamedis_netto;

			deskripsi = $(this).closest('tr').find("td:eq(3)").html();
			jasamedis = $(this).closest('tr').find("td:eq(4)").html();
			nominal_potongan_rs = $(this).closest('tr').find("td:eq(5)").html();
			nominal_pajak_dokter = $(this).closest('tr').find("td:eq(6)").html();
			jasamedis_netto = $(this).closest('tr').find("td:eq(7)").html();

			$("#tPendapatanNonPelayananRowId").val(idrow);
			$("#tPendapatanNonPelayananIdDokter").val(iddokter);
			$("#tPendapatanNonPelayananDeskripsi").val(deskripsi);
			$("#tPendapatanNonPelayananJasaMedis").val(jasamedis);
			$("#tPendapatanNonPelayananNominalPotonganRS").val(nominal_potongan_rs);
			$("#tPendapatanNonPelayananNominalPajakDokter").val(nominal_pajak_dokter);
			$("#tPendapatanNonPelayananJasaMedisNetto").val(jasamedis_netto);
			$("#tPendapatanNonPelayananTanggalPembayaran").val(tanggal_pembayaran);
			$("#tPendapatanNonPelayananStatusPendapatan").val(status_pendapatan);
			$("#tPendapatanNonPelayananStatusPeriode").val(status_periode);
		});

    $(document).on('click', '.deletePendapatanNonPelayanan', function() {
  		var idrow = $(this).data('idrow');

  		event.preventDefault();
  		swal({
  				title: "Apakah anda yakin ingin menghapus data ini?",
  				type: "warning",
  				showCancelButton: true,
  				confirmButtonColor: "#DD6B55",
  				confirmButtonText: "Ya",
  				cancelButtonText: "Tidak",
  				closeOnConfirm: false,
  				closeOnCancel: false
  			}).then(function() {
  				$.ajax({
  					url: '{site_url}tpendapatan_nonpelayanan/delete',
  					method: 'POST',
  					data: {
  						idrow: idrow
  					},
  					success: function(data) {
  						swal({
  							title: "Berhasil!",
  							text: "Data Pendapatan Non Pelayanan Telah Dihapus.",
  							type: "success",
  							timer: 1500,
  							showConfirmButton: false
  						});

  						location.reload();
  					}
  				});
  			});
  	});

    $(document).on('click', '#saveDataPendapatanNonPelayanan', function() {
      var idrow = $('#tPendapatanNonPelayananRowId').val();
      var iddokter = $('#tPendapatanNonPelayananIdDokter option:selected').val();
      var namadokter = $('#tPendapatanNonPelayananIdDokter option:selected').text();
      var deskripsi = $('#tPendapatanNonPelayananDeskripsi').val();
      var jasamedis = $('#tPendapatanNonPelayananJasaMedis').val();
      var nominal_potongan_rs = $('#tPendapatanNonPelayananNominalPotonganRS').val();
      var nominal_pajak_dokter = $('#tPendapatanNonPelayananNominalPajakDokter').val();
      var jasamedis_netto = $('#tPendapatanNonPelayananJasaMedisNetto').val();
      var status_pendapatan = $('#tPendapatanNonPelayananStatusPendapatan option:selected').val();
      var status_periode = $('#tPendapatanNonPelayananStatusPeriode option:selected').val();
      var tanggal_pembayaran = $('#tPendapatanNonPelayananTanggalPembayaran option:selected').val();
      var tanggal_jatuhtempo = $('#tPendapatanNonPelayananTanggalPembayaran option:selected').data('jatuhtempo');

      if(iddokter == ''){
        sweetAlert("Maaf...", "Dokter belum dipilih !", "error").then((value) => {
          $('#tPendapatanNonPelayananDeskripsi').focus();
        });
        return false;
      }

      if(deskripsi == ''){
        sweetAlert("Maaf...", "Deskripsi tidak boleh kosong !", "error").then((value) => {
          $('#tPendapatanNonPelayananDeskripsi').focus();
        });
        return false;
      }

      if(jasamedis == ''){
        sweetAlert("Maaf...", "Jasa Medis tidak boleh kosong !", "error").then((value) => {
          $("#tPendapatanNonPelayananJasaMedis").select2('open');
        });
        return false;
      }

      if(nominal_potongan_rs == ''){
        sweetAlert("Maaf...", "Nominal Potongan RS tidak boleh kosong !", "error").then((value) => {
          $("#tPendapatanNonPelayananNominalPotonganRS").select2('open');
        });
        return false;
      }

      if(nominal_pajak_dokter == ''){
        sweetAlert("Maaf...", "Nominal Pajak Dokter tidak boleh kosong !", "error").then((value) => {
          $("#tPendapatanNonPelayananNominalPajakDokter").select2('open');
        });
        return false;
      }

      $.ajax({
        url: '{site_url}tpendapatan_nonpelayanan/save',
        method: 'POST',
        data: {
          idrow: idrow,
          iddokter: iddokter,
          namadokter: namadokter,
          deskripsi: deskripsi,
          jasamedis: jasamedis,
          nominal_potongan_rs: nominal_potongan_rs,
          nominal_pajak_dokter: nominal_pajak_dokter,
          jasamedis_netto: jasamedis_netto,
          status_pendapatan: status_pendapatan,
          status_periode: status_periode,
          tanggal_pembayaran: tanggal_pembayaran,
          tanggal_jatuhtempo: tanggal_jatuhtempo
        },
        success: function(data) {
          swal({
            title: "Berhasil!",
            text: "Data Pendapatan Non Pelayanan Telah Tersimpan.",
            type: "success",
            timer: 1500,
            showConfirmButton: false
          });

          location.reload();
        }
      });
    });
  });

  function loadPeriodePembayaranHonorDokter(iddokter) {
		$.ajax({
			url: '{site_url}tverifikasi_transaksi/GetPeriodePembayaranHonorDokter/' + iddokter,
			dataType: "json",
			success: function(data) {
				$("#tPendapatanNonPelayananTanggalPembayaran").empty();
				$('#tPendapatanNonPelayananTanggalPembayaran').append(data.list_option);
			}
		});
	}
</script>
