<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<?php if (UserAccesForm($user_acces_form,array('1376'))){ ?>
<div class="block">
	<div class="block-header">
	<?php if (UserAccesForm($user_acces_form,array('1377'))){ ?>
		<ul class="block-options">
       
            <a href="{base_url}mrka/create" type="button" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
       
			</ul>
	<?}?>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:20px">
        <div class="row">
            <?php echo form_open('#', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="status">Periode</label>
                    <div class="col-md-8">
                       <select id="periode" name="periode" class="js-select2 form-control" required="" style="width: 100%;" data-placeholder="Choose one..">
							<option value="#" selected>- Semua Periode -</option>
							<?
							$tahun=date('Y');
							for($i=($tahun-1);$i<($tahun+5);$i++){?>
								<option value="<?=$i?>"><?=$i?></option>									
							<?}?>
							
						</select>
                    </div>
                </div>	
                                
            </div>
			<div class="col-md-6">
				
                <div class="form-group" style="margin-bottom: 5px;">
                    <div class="col-md-3">
                        <button class="btn btn-success text-uppercase" type="button" name="btn_filter" id="btn_filter" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
				
			</div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive">
		<table class="table table-bordered table-striped table-responsive" id="table_index">
			<thead>
				<tr>
					<th>#</th>
					<th>Nama Program</th>
					<th>Periode</th>
					<th>Status</th>
					<th>Created By</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>
<?}?>
<script src="{js_path}custom/jquery.toaster.js"></script>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	var table;
	$(document).ready(function(){
		// alert('sini');
		load_index();
		
	})	
	$("#btn_filter").click(function() {
		// alert('ID');
		table.destroy();
		load_index();
	});
	$(document).on("click",".publish",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,6).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Mempublish RKA?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka/update_status/'+id+'/2',
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' RKA Berhasil dipublish'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	$(document).on("click",".hapus",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,6).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menghapus RKA?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka/update_status/'+id+'/0',
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' RKA berhasil dihapus'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	$(document).on("click",".aktif",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,6).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Mengaktifkan RKA?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka/update_status/'+id+'/3',
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' RKA Berhasil diaktifkan'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	$(document).on("click",".selesai",function(){
		var table = $('#table_index').DataTable()
		var tr = $(this).parents('tr')
		var id = table.cell(tr,6).data()
		swal({
			title: "Apakah Anda Yakin ?",
			text : "Menyelesaikan RKA?",
			type : "success",
			showCancelButton: true,
			confirmButtonText: "Ya",
			confirmButtonColor: "#d26a5c",
			cancelButtonText: "Tidak Jadi",
		}).then(function() {
			$.ajax({
				url: '{site_url}mrka/update_status/'+id+'/4',
				type: 'POST',
				complete: function() {
					$.toaster({priority : 'success', title : 'Succes!', message : ' RKA Berhasil diselesaikan'});
					$('#table_index').DataTable().ajax.reload( null, false );
				}
			});
		});
		
		return false;
	});
	function load_index(){
		var periode=$("#periode").val();
		// alert(periode);
		table=$('#table_index').DataTable({
				"autoWidth": false,
				"pageLength": 10,
				"ordering": true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					url: '{site_url}mrka/getIndex',
					type: "POST",
					dataType: 'json',
					data : {
						periode:periode,
						
					   }
				},
				"columnDefs": [
					{"targets": [6], "visible": false },
					{ "width": "5%", "targets": 0, "orderable": true },
					{ "width": "20%", "targets": 1, "orderable": true },
					{ "width": "10%", "targets": 2, "orderable": true },
					{ "width": "10%", "targets": 3, "orderable": true },
					{ "width": "10%", "targets": 4, "orderable": true },
					{ "width": "20%", "targets": 5, "orderable": true },
					{ "width": "20%", "targets": 6, "orderable": true },
				]
			});
	}
</script>
