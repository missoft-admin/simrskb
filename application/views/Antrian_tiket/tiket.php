<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?=$title?></title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
	  font-family: Sans Serif, sans-serif;
        font-size: 13px !important;
        border-collapse: collapse !important;
        width: 100% !important;
		margin-top: 5px;
		margin-left: 5px;
		margin-right: 5px;
		margin-bottom: 5px;
    }
	html { margin: 15px}
   table {
		font-family: Sans Serif, sans-serif;
        font-size: 13px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
      th {
        padding: 5px;
		 
      }
	  td {
        padding: 0px;
		 
      }
      .content td {
        padding: 0px;
		border: 0px solid #6033FF;
      }
	  .content-bawah td {
        padding: 0px;
		border: 0px solid #6033FF;
      }
	  

      /* border-normal */
      
	  .text-header{
		font-size: 14px !important;
      }
	  .text-noantrian{
		font-size: 60px !important;
      }
	  .text-judul{
        font-size: 14px  !important;
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	   .border-bottom-left {
        border-bottom:1px solid #000 !important;
        border-left:1px solid #000 !important;
      }
	   .border-bottom-right {
        border-bottom:1px solid #000 !important;
        border-right:1px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-bottom-top-left {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-bottom-top-right {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-right:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
		border-top-style: dashed;
      }
      .border-thick-bottom{
        border-bottom:2px solid #000 !important;
		border-bottom-style: dashed;
      }

      .border-dotted{
		border-bottom:1px solid #000 !important;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	  .text-top{
		font-size: 14px !important;
      }
	  br {
		   display: block;
		   margin: 10px 0;
		}
	
    </style>
  </head>
  <body onload="window.print();">
 
	<table class="">
		<tr>
			<td  width="100%" class="text-center text-bold text-header"><h1><?=$judul_header?></h1></td>
			
		</tr>
		<tr>
			<td  width="100%" class="text-center text-bold text-header"><h1><?=$judul_sub_header?></h1></td>
			
		</tr>
		<tr>
			<td  width="100%" class="border-thick-bottom border-thick-top text-center text-bold text-header">&nbsp;</td>
			
		</tr>
		<tr>
			<td  width="100%" class="text-center text-bold text-noantrian"><h1><?=$kodeantrian?></h1></td>
			
		</tr>
		<tr>
			<td  width="100%" class="border-thick-bottom border-thick-top text-center text-bold text-header">&nbsp;</td>
			
		</tr>
		<tr>
			<td  width="100%" class="text-center"><h1><?=$nama_pelayanan?></h1></td>
		</tr>
		<tr>
			<td  width="100%" class="text-center h4"><?=($st_tampil_tanggal?'<br>'.tanggal_indo(YMDFormat($waktu_ambil)).' - '.HISTimeFormat($waktu_ambil) :'')?></td>
		</tr>
		
		<tr>
			<td  width="100%" class="border-thick-bottom text-center"><?=$footer?></td>
			
		</tr>
		
	</table>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.0.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			// alert('soo');
			window.print();

		})	
	
	</script>
	
  </body>
</html>
