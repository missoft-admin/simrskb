<!DOCTYPE html>
<html>
<!--untuk e-ticket
nanti di printer nya harus di setting kertas nya dulu
dengan ukuran
160mm x 200 mm
Ketas Label 121 Merk FOX

@page {
            margin-top: 1em;
            margin-left: 7.5em;
            margin-bottom: 0.5em;
        }
-->
<head>
	<title>Label Tabung Darah</title>
	<style type="text/css">
		<? if ($margin=='kiri'){ ?>
		@page {
            margin-top: 4.2mm;
            margin-left: 8mm;
            margin-bottom: 2.1mm;
        }
		<?}else{?>
			@page {
            margin-top: 4.2mm;
            margin-left: 31.5mm;
            margin-bottom: 2.1mm;
        }
		<?}?>
		* {
			color:black;
			font-style:normal;
			text-decoration:none;
			font-family:"Arial Rounded MT Bold", sans-serif;
			text-align:left;
			vertical-align:top;
			white-space:pre-wrap;
		}
		table {
			font-size: 18px !important;
			border-collapse: collapse !important;
			font-family: "Segoe UI", Arial, sans-serif;
		}
		td {
			border: 0px solid black;
			padding: 1.5px;
			padding-left: 1px
			vertical-align: middle;
		}
		.text-center{
			text-align: center !important;
		}
		.text-left{
			text-align: left !important;
		}
		.sembuh{
			font-size: 18px !important;
			font-weight: bold;
		}
		.white{
			color : white;
		}
		.text-right{
			ext-align: right !important;
		}

		/* text-style */
		.text-italic{
			font-style: italic;
		}
		.text-bold{
			font-weight: bold;
		}
	</style>
</head>

<body>
<?php
	$c1=1;
	$c2=1;
	for ($i=1; $i <= ($data_akhir/2); $i++) {
		$c1=cek_c1($i);

	?>
	<table style="width:200mm;<?=($i % 5=='0')?'page-break-after:always;':''?>">
			<tr>
				<td colspan="3" style="height : 3mm"></td>
			</tr>
			<tr style="height:40mm">
				<td>
					<table class="tg" border="1" height="20%;">
						<tr>
							<td class="text-left sembuh <?=($data_array[$c1]=='0')?'white':''?>" rowspan="4">
								<img src="<?= base_url(); ?>qrcode/qr_code_ttd_dokter/<?= $dokter_peminta_id; ?>" width="100px">
							</td>
							<td class="text-left sembuh <?=($data_array[$c1]=='0')?'white':''?>" width='60%'><?=$title.'. '.$namapasien?></td>
							<td class="text-left sembuh <?=($data_array[$c1]=='0')?'white':''?>" width='40%'><?=$nomedrec.' ('.$jeniskelamin.')'?></td>
						</tr>
						<tr>
							<td class="text-left sembuh <?=($data_array[$c1]=='0')?'white':''?>" width='60%'><?=DMYFormat($tanggallahir)?></td>
							<td class="text-left sembuh <?=($data_array[$c1]=='0')?'white':''?>" width='40%'><?=$umurtahun.' Th '.$umurbulan.' Bln '.$umurhari.' Hr'?></td>
						</tr>
						<tr>
							<td class="text-left sembuh <?=($data_array[$c1]=='0')?'white':''?>" width='60%'><?=$tanggaldaftar?></td>
							<td class="text-left sembuh <?=($data_array[$c1]=='0')?'white':''?>" width='40%'><?=$dokter_peminta;?></td>
						</tr>
						<tr>
							<td class="text-left sembuh <?=($data_array[$c1]=='0')?'white':''?>" width='60%'><?=$tujuan_pendaftaran?> - <?=$namapoliklinik?></td>
							<td class="text-left sembuh <?=($data_array[$c1]=='0')?'white':''?>" width='40%'><?=$nomor_laboratorium;?></td>
						</tr>
					</table>
				</td>
				<td style="width:0mm">&nbsp;</td>
				<?$c2=$c1+5;?>
				<td>
					<table class="tg" border="1" height="20%;">
						<tr>
							<td class="text-left sembuh <?=($data_array[$c2]=='0')?'white':''?>" rowspan="4">
								<img src="<?= base_url(); ?>qrcode/qr_code_ttd_dokter/<?= $dokter_peminta_id; ?>" width="100px">
							</td>
							<td class="text-left sembuh <?=($data_array[$c2]=='0')?'white':''?>" width='60%'><?=$title.'. '.$namapasien?></td>
							<td class="text-left sembuh <?=($data_array[$c2]=='0')?'white':''?>" width='40%'><?=$nomedrec.' ('.$jeniskelamin.')'?></td>
						</tr>
						<tr>
							<td class="text-left sembuh <?=($data_array[$c2]=='0')?'white':''?>" width='60%'><?=DMYFormat($tanggallahir)?></td>
							<td class="text-left sembuh <?=($data_array[$c2]=='0')?'white':''?>" width='40%'><?=$umurtahun.' Th '.$umurbulan.' Bln '.$umurhari.' Hr'?></td>
						</tr>
						<tr>
							<td class="text-left sembuh <?=($data_array[$c2]=='0')?'white':''?>" width='60%'><?=$tanggaldaftar?></td>
							<td class="text-left sembuh <?=($data_array[$c2]=='0')?'white':''?>" width='40%'><?=$dokter_peminta;?></td>
						</tr>
						<tr>
							<td class="text-left sembuh <?=($data_array[$c2]=='0')?'white':''?>" width='60%'><?=$tujuan_pendaftaran?> - <?=$namapoliklinik?></td>
							<td class="text-left sembuh <?=($data_array[$c2]=='0')?'white':''?>" width='40%'><?=$nomor_laboratorium;?></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3" style="height : 3mm"></td>
			</tr>
	</table>
<br>

<?php } ?>
</body>

</html>
